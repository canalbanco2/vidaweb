Imports System.Drawing
Imports System.Web.UI.WebControls
Imports System.Data

Partial Class paginacaogrid
    Inherits System.Web.UI.UserControl


    Public _valor As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub

    Public Event ePaginaAlterada(ByVal Argumento As String)
    Public Event ePaginaAlteradaA(ByVal Argumento As String)

    Private Sub mDisparaEvento(ByVal Argumento As String)
        RaiseEvent ePaginaAlterada(Argumento)
    End Sub
    Private Sub mDisparaEventoA(ByVal Argumento As String)
        RaiseEvent ePaginaAlteradaA(Argumento)
    End Sub

    Private Sub mPaginacaoClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles a1.Command, a2.Command, a3.Command, a4.Command, a5.Command, a6.Command, a7.Command
        mDisparaEvento(e.CommandArgument)
    End Sub

    Private Sub aPaginacaoClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles a1.Command, a2.Command, a3.Command, a4.Command, a5.Command, a6.Command, a7.Command
        mDisparaEventoA(e.CommandArgument)
        'Session("indice") = CInt(CType(sender, LinkButton).ID.ToString().Substring(1, 1)) - 2
        'Dim aux As Integer = CInt(CType(sender, LinkButton).ID.ToString().Substring(1, 1)) - 2
    End Sub

    Private Sub mMontaPaginacao(ByVal GridPaginaAtual As Integer, ByVal GridPaginasTotal As Integer)
        Dim paginainicial As Integer
        Dim paginafinal As Integer
        Dim paginaatual As Integer = GridPaginaAtual + 1

        If (paginaatual / 5) > 0 Then
            If (paginaatual Mod 5) <> 0 Then
                paginainicial = (Int(paginaatual / 5) * 5) + 1
            Else
                paginainicial = (Int((paginaatual - 1) / 5) * 5) + 1
            End If
            paginafinal = paginainicial + 4
            If paginafinal > GridPaginasTotal Then paginafinal = GridPaginasTotal
        Else
            paginainicial = 1
            paginafinal = 5
            If paginafinal > GridPaginasTotal Then paginafinal = GridPaginasTotal
        End If

        Me.a1.Visible = False
        Me.a2.Visible = False
        Me.a3.Visible = False
        Me.a4.Visible = False
        Me.a5.Visible = False
        Me.a6.Visible = False
        Me.a7.Visible = False
        If paginaatual > 1 Then
            a1.Text = "Anterior"
            a1.ForeColor = Color.White
            a1.Style.Add("font-family", "verdana")
            a1.Style.Add("font-size", "10px")
            a1.Style.Add("font-weight", "bold")
            a1.Style.Add("text-decoration", "underline")
            a1.Visible = True
        End If
        Dim a As Integer = 2
        For i As Integer = paginainicial To paginafinal
            CType(Me.FindControl("a" + a.ToString), LinkButton).CommandArgument = (i - 1).ToString
            CType(Me.FindControl("a" + a.ToString), LinkButton).Text = i.ToString
            CType(Me.FindControl("a" + a.ToString), LinkButton).Visible = True
            If i = paginaatual Then
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("color", "white")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("text-decoration", "none")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-family", "verdana")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-size", "10px")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-weight", "bold")

            Else
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("color", "white;")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("text-decoration", "underline")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-family", "verdana")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-size", "10px")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-weight", "bold")

            End If
            a += 1
        Next
        If paginaatual <> GridPaginasTotal Then
            a7.Text = "Pr�xima"
            a7.ForeColor = Color.White
            a7.Style.Add("font-family", "verdana")
            a7.Style.Add("font-size", "10px")
            a7.Style.Add("font-weight", "bold")
            a7.Style.Add("text-decoration", "underline")
            a7.Visible = True
        End If
    End Sub

    Public Sub GridDataBind(ByVal Grid As DataGrid, ByVal dt As DataTable)
        'Dim dt2 As New DataTable

        'With dt2
        '    dt2.Columns.Add("segurado_temporario_web_id")
        '    dt2.Columns.Add("operacao")
        '    dt2.Columns.Add("dt_alteracao")
        '    dt2.Columns.Add("dt_inicio_vigencia")
        '    dt2.Columns.Add("nome")
        '    dt2.Columns.Add("tipo")
        '    dt2.Columns.Add("cpf")
        '    dt2.Columns.Add("dt_nascimento")
        '    dt2.Columns.Add("sexo")
        '    dt2.Columns.Add("val_salario")
        '    dt2.Columns.Add("val_capital_segurado")
        'End With
        'Dim tt As Integer = 0

        'While tt < 5
        '    dt2.Rows.Add(dt2.NewRow())
        '    dt2.Rows(tt).Item("segurado_temporario_web_id") = dt.Rows(tt).Item("segurado_temporario_web_id")
        '    dt2.Rows(tt).Item("operacao") = dt.Rows(tt).Item("operacao")
        '    dt2.Rows(tt).Item("dt_alteracao") = dt.Rows(tt).Item("dt_alteracao")
        '    dt2.Rows(tt).Item("nome") = dt.Rows(tt).Item("nome")
        '    dt2.Rows(tt).Item("dt_inicio_vigencia") = dt.Rows(tt).Item("dt_inicio_vigencia")
        '    dt2.Rows(tt).Item("tipo") = dt.Rows(tt).Item("tipo")
        '    dt2.Rows(tt).Item("cpf") = dt.Rows(tt).Item("cpf")
        '    dt2.Rows(tt).Item("dt_nascimento") = dt.Rows(tt).Item("dt_nascimento")
        '    dt2.Rows(tt).Item("sexo") = dt.Rows(tt).Item("sexo")
        '    dt2.Rows(tt).Item("val_salario") = dt.Rows(tt).Item("val_salario")
        '    dt2.Rows(tt).Item("val_capital_segurado") = dt.Rows(tt).Item("val_capital_segurado")

        '    tt += 1
        'End While

        Grid.DataSource = dt


        If Not Session("indice") Is Nothing Then
            Grid.CurrentPageIndex = Session("indice")
        Else
            Grid.CurrentPageIndex = 0
        End If


        Grid.DataBind()

        Me.mMontaPaginacao(Grid.CurrentPageIndex, Grid.PageCount)

        Dim tipo As String
        Dim cell As Web.UI.WebControls.DataGridItem
        Dim pai As Web.UI.WebControls.DataGridItem
        Dim count As Int16 = 0
        Dim tamMax(10) As Integer
        Dim tamMaxPos(10) As String
        Dim pai_id As String = ""
        Dim count2 As Integer = 0
        Dim w As Integer = 1
        Dim t As Integer = 1
        Dim primeiro As Boolean = True

        Dim tamanho(10) As Integer
        Dim textoCell As String = ""
        tamanho(0) = 10
        tamanho(1) = 11
        tamanho(2) = 40
        tamanho(3) = 12
        tamanho(4) = 8
        tamanho(5) = 11
        tamanho(6) = 5
        tamanho(7) = 10
        tamanho(8) = 15
        tamanho(9) = 15

        Dim txtMax(10) As String
        txtMax(0) = ""
        txtMax(1) = ""
        txtMax(2) = ""
        txtMax(3) = ""
        txtMax(4) = ""
        txtMax(5) = ""
        txtMax(6) = ""
        txtMax(7) = ""
        txtMax(8) = ""
        txtMax(9) = ""

        If cUtilitarios.MostraColunaSalario() Then
            w = 1
        Else
            w = 0
        End If

        If cUtilitarios.MostraColunaCapital() Then
            t = 1
        Else
            t = 0
        End If


        If Grid.Items.Count > 0 Then


            For Each x As Web.UI.WebControls.DataGridItem In Grid.Items

                If Not cell Is Nothing Then
                    If x.Cells.Item(4).Text <> "Titular" And cell.Cells.Item(4).Text = "Titular" Then
                        cell.Cells.Item(3).Text = "<img src='../segw0060/Images/imgMenos.gif' onclick=exibeDependentes('" + pai_id + "',this);>&nbsp;&nbsp;" & cell.Cells.Item(3).Text
                    End If
                End If

                cell = x
                If x.Cells.Item(4).Text = "Titular" Then
                    pai = x
                    x.ID = count2
                    pai_id = x.ID

                    x.Attributes.Add("onmouseover", "mO('#E8F1FF', this)")
                    x.Attributes.Add("onmouseout", "mO('#FFFFFF', this)")

                    count = 0
                    count2 += 1
                Else
                    x.Cells.Item(3).Attributes.Add("style", "padding-left:20px;")
                    x.ID = pai_id + "_" + count.ToString
                    x.Attributes.Add("style", "background-color:#f5f5f5;")
                    x.Attributes.Add("onmouseover", "mO('#E8F1FF', this)")
                    x.Attributes.Add("onmouseout", "mO('#f5f5f5', this)")

                    count = count + 1
                End If

                For i As Integer = 0 To x.Cells.Count - 1
                    x.Cells.Item(i).ID = "cell_" & i
                    x.Cells.Item(i).Wrap = False
                Next

                For i As Integer = 0 To x.Cells.Count - 1
                    x.Cells.Item(i).ID = "cell_" & i
                    x.Cells.Item(i).Wrap = False
                    If tamMax(i) <= x.Cells.Item(i).Text.Length Then
                        tamMaxPos(i) = x.ID & "_" & x.Cells.Item(i).ID
                        tamMax(i) = x.Cells.Item(i).Text.Length
                    End If
                Next

                x.Cells.Item(5).Text = cUtilitarios.trataCPF(x.Cells.Item(5).Text)

                If w = 0 Then
                    x.Cells.Item(8).Style.Add("display", "none")
                Else
                    x.Cells.Item(8).Text = cUtilitarios.trataMoeda(x.Cells.Item(8).Text).Trim
                    x.Cells.Item(8).HorizontalAlign = HorizontalAlign.Right
                End If

                If t = 0 Then
                    x.Cells.Item(9).Style.Add("display", "none")
                Else
                    x.Cells.Item(9).Text = cUtilitarios.trataMoeda(x.Cells.Item(9).Text).Trim
                    x.Cells.Item(9).HorizontalAlign = HorizontalAlign.Right
                End If

                If x.Cells.Item(1).Text = "Hist�rico" Then
                    x.Cells.Item(1).Text = "&nbsp;"
                End If

                x.Cells.Item(2).Text = x.Cells.Item(2).Text.ToUpper

                If primeiro Then
                    primeiro = False
                    For i As Integer = 0 To x.Cells.Count - 1
                        x.Cells.Item(i).Text = "<div class='headerFakeDiv'id='headerFake" & i & "'></div>" & x.Cells.Item(i).Text
                    Next
                End If
            Next

            Dim z As Integer = 1
            Dim texto As String

            Dim tamMin(10) As Integer
            tamMin(0) = 9
            tamMin(1) = 9
            tamMin(2) = 10
            tamMin(3) = 40
            tamMin(4) = 5
            tamMin(5) = 13
            tamMin(6) = 10
            tamMin(7) = 4
            tamMin(8) = 15
            tamMin(9) = 15


            Try


                For Each m As String In txtMax


                    texto = txtMax(z).ToString.Trim.Replace(" ", "O").Replace("&atilde;", "a").Replace("&ccedil;", "c").PadRight(tamMin(z), "O")
                    Response.Write("<input type='hidden' value='" & texto & "' id='txtMax" & z & "'>" & vbCrLf)

                    z = z + 1
                Next
            Catch ex As Exception
                'Response.End()
            End Try

        End If


    End Sub

    Public Property CssClass() As String
        Get
            Return Me.lblPaginacao.Attributes.Item("class")
        End Get
        Set(ByVal Value As String)
            Me.lblPaginacao.Attributes.Add("class", Value)
        End Set
    End Property
End Class
