<%@ Page Language="vb" AutoEventWireup="false" Codebehind="FechamentoFatura.aspx.vb" Inherits="segw0069.FechamentoFatura" %>
<%@ Register TagPrefix="uc1" TagName="paginacaogrid" Src="paginacaogrid.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Downloads</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<LINK href="../segw0060/CSS/EstiloBase.css" type="text/css" rel="stylesheet">
			<script>
			
			function confirmaFechamento(nDiasPosFat, msg){
				//valor = dtFim.toString();
				//var dia = valor.substr(0, 2);
				//var mes = parseInt(valor.substr(3, 2));
				//var ano = parseInt(valor.substr(6, 4));	
				
				//if (valor.substr(3, 2) == '08')
				//	mes = 8
					
				//if (valor.substr(3, 2) == '09')
				//	mes = 8
				//var dt = new Date();
								
				//var dt1 = new Date(ano,mes,dia);
				//var dt2 = new Date(dt.getYear(),dt.getMonth(),dt.getDate());
							
				//if ((dt2 - dt1) > 0) {
				if (nDiasPosFat < 0) {
					alert(msg)				
					return false;
				}
				
				var confirma = confirm('Toda a movimenta��o de vidas ser� finalizada, n�o sendo poss�vel incluir / alterar / excluir\nmais nenhuma vida referente � fatura de <%=data%>.\n\nConfirma a finaliza��o da movimenta��o de vidas?');
				if (confirma==true){
					top.exibeaguarde();	
					location.href='?fatura=ok';
					return false;
				}else{
					return false;
				}
			}
			
				
			function exibeDependentes(id,src) {
					
				if (src.src.indexOf('imgMenos') != -1)
					src.src = '../segw0060/Images/imgMais.gif';
				else
					src.src = '../segw0060/Images/imgMenos.gif';
				
				for(i=0;i<=5;i++) {					
					var tr = eval("document.getElementById('grdPesquisa_"+id+"_"+i+"')");
					if (tr == null)
						break;
					if (tr.style.display == 'none') 
						tr.style.display = '';
					else
						tr.style.display = 'none';
				}		
			}	
				//id="grdPesquisa_1_0" 
				
			function escondeDiv(div){
				var idDiv = document.getElementById(div);
				if (idDiv.style.display==''){
					idDiv.style.display='none';
				}else{
					idDiv.style.display='';
				}
			}
			
			function vidaExcluir(numero){
				if (numero)
					location.href="MovimentacaoUmaUm.aspx?excluir=1&expandir=1";
				else
					location.href="MovimentacaoUmaUm.aspx&expandir=1";
			}
			
			function confirmaExclusao(){
				var divExcluir = document.getElementById("lblAcao");
				var nomeSegurado = document.getElementById("nomeAlvo").value;
				var confirmacao = confirm('Confirma a exclus�o do segurado "' + nomeSegurado + '"?\nSe existirem c�njuges para este segurado todos ser�o exclu�dos.')
				
				return confirmacao
			}
			
			function confirmaDesfazer(){
				var confirmacao = confirm('Ser�o desfeitas todas as altera��es realizadas para o Subgrupo "<%= Session("subgrupo")%>".\n\nConfirma a opera��o?');
			}
			
			function getPesquisaLoad(valor) {				
												
				if(valor == "CPF" || valor == "Nome") {
					document.getElementById("divCampoPesquisa").style.display = "block";
				} else {
					document.getElementById("divCampoPesquisa").style.display = "none";
				}				
			}
			function getPesquisa(valor) {				
				
				document.Form1.campoPesquisa.value = "";
				
				if(valor == "CPF" || valor == "Nome") {
					document.getElementById("divCampoPesquisa").style.display = "block";
				} else {
					document.getElementById("divCampoPesquisa").style.display = "none";
				}				
			}
			function buscaDados() {
				var valor = document.Form1.DropDownList2.value;
				
				document.Form1.submit();	
			}
			var teste = 0;
			var id;
			
			function alteracoes(valor, tipo, nome, idTitular, alvo, operacao) {								
																							
				idTemp = "grdPesquisa_" + alvo;
											
				idTemp2 = document.getElementById("alvoTemp").value; 
				
				if(document.getElementById(idTemp2)) {															
					document.getElementById(idTemp2).style.background='#ffffff';
				} 									
				
				document.getElementById(idTemp).style.background='#f8ef00';
					
				document.getElementById("alvo").value = valor;				
				document.getElementById("alvoTemp").value = idTemp;
				document.getElementById("nomeAlvo").value = nome;
								
				if(tipo == "Titular") {
					if (document.getElementById("divBtnIncluiDependente")) {
						document.getElementById("divBtnIncluiDependente").style.display = "block";
					}
					document.getElementById("divBtnAlterar").style.display = "block";
					document.getElementById("divBtnExcluir").style.display = "block";
				} else {
					if (document.getElementById("divBtnIncluiDependente")) {
						document.getElementById("divBtnIncluiDependente").style.display = "none";
					}
					document.getElementById("divBtnAlterar").style.display = "block";
					document.getElementById("divBtnExcluir").style.display = "block";					
				}
				
				if (operacao == 'Exclus�o') {
					document.getElementById("divBtnAlterar").style.display = "none";
					document.getElementById("divBtnExcluir").style.display = "none";					
				}
				
				document.Form1.idTitular.value = idTitular;
			}
			
			function validaMouseOut(src,color) {
				//alert(teste);
				//if (id != src.id)
				src.style.background=color;			
					
				teste = 0;
			}
			function mO(cor, alvo) {												
				if(alvo.style.background != '#f8ef00')
					alvo.style.background=cor;							
			}
			
			function alteraRegistro() {
				if (document.getElementById("divBtnIncluiDependente")) {
					if(document.getElementById("divBtnIncluiDependente").style.display == "none") {
						return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=alterardependente&id=' + document.getElementById("alvo").value + "&idTitular=" + document.Form1.idTitular.value, 240, 450);
					} else {
						return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=alterar&id=' + document.getElementById("alvo").value, 280, 450);
					}
				} else {
					return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=alterar&id=' + document.getElementById("alvo").value, 280, 450);
				}
			}
			function incluiDependente() {
				try {										
					idTemp2 = document.getElementById("alvoTemp").value; 
					
					var dep = document.getElementById(idTemp2 + "_0");
					if (dep != null) {
						alert("Voc� j� informou um c�njuge para o titular escolhido.");
						return false;
					}
					
				} catch(ex) {
				
				}
				
				//alert(document.Form1.idTitular.value);
				
				return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=incluirdependente&id=' + document.getElementById("alvo").value + "&idTitular=" + document.Form1.idTitular.value, 240, 450);
			}
			function excluiRegistro() {	
				if (document.getElementById("divBtnIncluiDependente")) {		
					if(document.getElementById("divBtnIncluiDependente").style.display == "none") {					
						return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluirdependente&id=' + document.getElementById("alvo").value + "&idTitular=" + document.Form1.idTitular.value, 240, 450);				
					} else {
						return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluir&id=' + document.getElementById("alvo").value, 280, 450);				
					}
				} else{
					return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluir&id=' + document.getElementById("alvo").value, 280, 450);				
				}
			}
			
function FormataCPF(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres){
		var wTecla, wVr, wTam;
		 
			//alert(pForm);
		  
		wTecla = pTeclaPres.keyCode;
		wVr = pForm[pCampo].value;
		wVr = wVr.toString().replace( "-", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( "/", "" );
		wTam = wVr.length ;

		if(wTam > pTamMax) {
			wTam = pTamMax;
			wVr = wVr.substr(0, pTamMax)
		}
			
		if (wTam < pTamMax && wTecla != 8) { 
			wTam = wVr.length + 1 ; 
		}

		if (wTecla == 8 ) { 
			wTam = wTam - 1 ; 
		}
		   
		if ( wTecla == 8 || wTecla == 88 || wTecla >= 48 && wTecla <= 57 || wTecla >= 96 && wTecla <= 105 ){
		if ( wTam <= 2 ){
			pForm[pCampo].value = wVr ;
		}
		if (wTam > pPosTraco && wTam <= pTamMax) {
				wVr = wVr.substr(0, wTam - pPosTraco) + '-' + wVr.substr(wTam - pPosTraco, wTam);
		}
		if ( wTam == pTamMax){
				wVr = wVr.substr( 0, wTam - pPos1 ) + '.' + wVr.substr(wTam - pPos1, 3) + '.' + wVr.substr(wTam - pPos2, wTam);
		}
		pForm[pCampo].value = wVr;
		 
		}

		}
		function formataCampo(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres) {
					
		    if(pForm.DropDownList2.value == "CPF") { 
				FormataCPF(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres);
			}		
		}				
			</script>
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<P><asp:label id="lblNavegacao" runat="server" Visible="False" CssClass="Caminhotela">Label</asp:label><BR>
				<asp:label id="lblVigencia" runat="server" CssClass="Caminhotela"></asp:label></P>
			<P><asp:label id="Label17" runat="server" CssClass="Caminhotela">Condi��es do subgrupo</asp:label>
				<TABLE id="Table5" style="BORDER-COLLAPSE: collapse" borderColor="#cccccc" cellSpacing="0"
					cellPadding="1" width="740" border="1"> <!-- PRIMEIRA LINHA -->
					<TR class="titulo-tabela sem-sublinhado">
						<TD>Capital Segurado</TD>
						<TD>Limite&nbsp;M�ximo</TD>
						<TD>M�lt. Salarial</TD>
						<TD>Idade M�n. Mov.</TD>
						<TD>Idade M�x. Mov</TD>
						<TD>C�njuge</TD>
					</TR>
					<TR>
						<TD align="center"><asp:label id="lblCapitalSegurado" Runat="server"></asp:label></TD>
						<TD align="right"><asp:label id="lblLimiteMaximo" Runat="server"></asp:label></TD>
						<TD align="center"><asp:label id="lblMultSalarial" Runat="server"></asp:label></TD>
						<TD align="center"><asp:label id="lblIdadeMinima" Runat="server"></asp:label></TD>
						<TD align="center"><asp:label id="lblIdadeMaxima" Runat="server"></asp:label></TD>
						<TD align="center"><asp:label id="lblConjuge" Runat="server"></asp:label></TD>
					</TR> <!-- SEGUNDA LINHA --> <!-- TERCEIRA LINHA --> <!-- QUARTA LINHA --></TABLE>
				<BR>
				<asp:panel id="pnlAlerta" runat="server" Visible="False" CssClass="fundo-azul-claro" Height="70px"
					HorizontalAlign="Center" Width="60%">
					<BR>
					<asp:Label id="lblListagemEnviada" runat="server" CssClass="Caminhotela"><br>A movimenta��o do per�odo foi conclu�da com sucesso.<br>&nbsp;<br>O aviso da emiss�o de fatura ser� encaminhado para a sua caixa de correio eletr�nico.<br>&nbsp;<br>&nbsp;</asp:Label>
				</asp:panel>
				<asp:panel id="pnlFatura" runat="server" Visible="False" CssClass="fundo-azul-claro" Height="70px"
					HorizontalAlign="Center" Width="60%">
					<BR>
					<asp:Label id="lblFaturaBloqueada" runat="server" CssClass="Caminhotela"><br>Vig�ncia superior a data cadastrada! N�o ser� poss�vel emitir a fatura.<br>&nbsp;<br><br>&nbsp;<br>&nbsp;</asp:Label>
				</asp:panel>
				<STYLE>.headerFixDiv { FONT-WEIGHT: bold; WIDTH: 100%; COLOR: #ffffff; POSITION: absolute; TOP: 2px; BACKGROUND-COLOR: #003399 }
	.headerFake { BORDER-RIGHT: red 1px solid; BORDER-TOP: red 1px solid; BORDER-LEFT: red 1px solid; BORDER-BOTTOM: red 1px solid; POSITION: absolute; TOP: 2px }
	.headerFixDiv2 { VISIBILITY: hidden }
	.headerFakeDiv { VISIBILITY: hidden; LINE-HEIGHT: 0 }
	</STYLE>
			</P>
			<asp:label id="lblListagemVidas" runat="server" CssClass="Caminhotela">Lista das vidas</asp:label>
			<DIV id="divGridMovimentacao" style="OVERFLOW: auto; WIDTH: 735px; HEIGHT: 280px">
				<DIV style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; OVERFLOW: hidden; PADDING-TOP: 0px">
					<TABLE id="gridMovimentacao" style="BORDER-COLLAPSE: collapse" borderColor="#dddddd" cellSpacing="0"
						cellPadding="0" border="1" runat="server">
						<TR class="paddingzero-sembordalateral">
							<TD>
								<DIV style="WIDTH: 780px">
									<TABLE style="BORDER-COLLAPSE: collapse" cellSpacing="0" cellPadding="0" border="1" CssClass="escondeTituloGridTable">
										<TR>
											<TD id="tdHeader1">
												<DIV class="headerFixDiv2" id="header1"></DIV>
												<DIV class="headerFixDiv">Opera��o</DIV>
											</TD>
											<TD id="tdHeader2" wrap="false">
												<DIV class="headerFixDiv2" id="header2"></DIV>
												<DIV class="headerFixDiv">Dt. Opera��o</DIV>
											</TD>
											<TD id="tdHeader3" wrap="false">
												<DIV class="headerFixDiv2" id="header3"></DIV>
												<DIV class="headerFixDiv">Segurado</DIV>
											</TD>
											<TD id="tdHeader4" wrap="false">
												<DIV class="headerFixDiv2" id="header4"></DIV>
												<DIV class="headerFixDiv">Tipo</DIV>
											</TD>
											<TD id="tdHeader5" wrap="false">
												<DIV class="headerFixDiv2" id="header5"></DIV>
												<DIV class="headerFixDiv">CPF</DIV>
											</TD>
											<TD id="tdHeader6" wrap="false">
												<DIV class="headerFixDiv2" id="header6"></DIV>
												<DIV class="headerFixDiv">Dt. Nasc.</DIV>
											</TD>
											<TD id="tdHeader7" wrap="false">
												<DIV class="headerFixDiv2" id="header7"></DIV>
												<DIV class="headerFixDiv">Sexo</DIV>
											</TD>
											<TD id="tdHeader8" wrap="false">
												<DIV class="headerFixDiv2" id="header8"></DIV>
												<DIV class="headerFixDiv">Sal�rio (R$)</DIV>
											</TD>
											<TD id="tdHeader9" wrap="false">
												<DIV class="headerFixDiv2" id="header9"></DIV>
												<DIV class="headerFixDiv">Capital (R$)</DIV>
											</TD>
											<!--TD id="tdHeader11" wrap="false">
												<DIV class="headerFixDiv2" id="header11"></DIV>
												<DIV class="headerFixDiv">Fim de Vig�ncia</DIV>
											</TD--></TR>
										<TR class="paddingzero-sembordalateral">
											<TD colSpan="9">
												<DIV id="divScrollGrid" style="OVERFLOW: auto; HEIGHT: 227px"><!-- In�cio da grid de Usu�rios --><asp:datagrid id="grdPesquisa" runat="server" CssClass="escondeTituloGridTable" PageSize="100"
														BorderColor="#DDDDDD" AllowPaging="True" ShowHeader="False" AutoGenerateColumns="False" CellSpacing="0" CellPadding="0">
														<FooterStyle CssClass="0019GridFooter"></FooterStyle>
														<SelectedItemStyle CssClass="ponteiro"></SelectedItemStyle>
														<AlternatingItemStyle CssClass="ponteiro"></AlternatingItemStyle>
														<ItemStyle CssClass="ponteiro"></ItemStyle>
														<Columns>
															<asp:BoundColumn Visible="False" DataField="segurado_temporario_web_id"></asp:BoundColumn>
															<asp:BoundColumn DataField="operacao" HeaderText="Opera&#231;&#227;o">
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="dt_alteracao" HeaderText="Data da Opera&#231;&#227;o" DataFormatString="{0:dd/MM/yyyy}">
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="nome" HeaderText="Segurado"></asp:BoundColumn>
															<asp:BoundColumn DataField="tipo" HeaderText="Tipo"></asp:BoundColumn>
															<asp:BoundColumn DataField="cpf" HeaderText="CPF"></asp:BoundColumn>
															<asp:BoundColumn DataField="dt_nascimento" HeaderText="Nascimento" DataFormatString="{0:dd/MM/yyyy}">
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="sexo" HeaderText="Sexo"></asp:BoundColumn>
															<asp:BoundColumn DataField="val_salario" HeaderText="Sal&#225;rio (R$)">
																<ItemStyle HorizontalAlign="Right"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="val_capital_segurado" HeaderText="Capital (R$)">
																<ItemStyle HorizontalAlign="Right"></ItemStyle>
															</asp:BoundColumn>
														</Columns>
														<PagerStyle Visible="False"></PagerStyle>
													</asp:datagrid></DIV>
											</TD>
										</TR>
									</TABLE>
								</DIV>
							</TD>
						</TR>
					</TABLE>
				</DIV>
				<center><asp:label id="lblSemUsuario" Visible="False" CssClass="Caminhotela" Runat="server"></asp:label></center>
			</DIV>
			<DIV></DIV>
			<uc1:paginacaogrid id="ucPaginacao" runat="server" Visible="False"></uc1:paginacaogrid><asp:panel class="paginacao" id="Panel1" runat="server" Width="740">P�gina 
-&nbsp; 
<asp:LinkButton id="eAnt" runat="server" Visible="False" Font-Underline="True" ForeColor="White"
					Font-Bold="True">Anterior</asp:LinkButton>&nbsp; 
<asp:linkbutton id="a1" CssClass="TextoRodape" Visible="False" Runat="server" ForeColor="White"
					Font-Bold="True">1</asp:linkbutton>&nbsp; 
<asp:linkbutton id="a2" CssClass="TextoRodape" Visible="False" Runat="server" Font-Underline="True"
					ForeColor="White" Font-Bold="True">2</asp:linkbutton>&nbsp; 
<asp:linkbutton id="a3" CssClass="TextoRodape" Visible="False" Runat="server" Font-Underline="True"
					ForeColor="White" Font-Bold="True">3</asp:linkbutton>&nbsp; 
<asp:linkbutton id="a4" CssClass="TextoRodape" Visible="False" Runat="server" Font-Underline="True"
					ForeColor="White" Font-Bold="True">4</asp:linkbutton>&nbsp; 
<asp:linkbutton id="a5" CssClass="TextoRodape" Visible="False" Runat="server" Font-Underline="True"
					ForeColor="White" Font-Bold="True">5</asp:linkbutton>&nbsp;&nbsp; 
<asp:LinkButton id="ePro" runat="server" Font-Underline="True" ForeColor="White" Font-Bold="True">Pr�xima</asp:LinkButton>&nbsp; 
</asp:panel><asp:panel id="pnlBtnEncerrar" runat="server" Visible="true">
				<DIV style="TEXT-ALIGN: center" align="center">Ao clicar no bot�o "Encerrar 
					movimenta��o" ser� encerrada a movimenta��o das vidas que far�o parte da fatura<BR>
					de
					<asp:Label id="lblPeriodoFatura" Runat="server"></asp:Label>.<BR>
					&nbsp;
					<BR>
					<asp:Button id="btnFecharMovimentacao" runat="server" CssClass="Botao" Width="160px" Text="Encerrar movimenta��o"></asp:Button><BR>
				</DIV>
			</asp:panel><asp:literal id="lblPeriodoFatura2"></asp:literal></form>
		<script>
		
		
			try {
					for(i=1; i<=9; i++) {					
						if(i == 9 && document.getElementById("grdPesquisa_0_cell_9").style.display != "none" ) {
							add = "OO"
						} else if(i == 8 && document.getElementById("grdPesquisa_0_cell_8").style.display != "none" && document.getElementById("grdPesquisa_0_cell_8").style.display == "none") {
							add = "OO"
						} else {
							add = "";
						}
						document.getElementById("header" + i).innerHTML = document.getElementById("txtMax" + i).value + add;
						document.getElementById("headerFake" + i).innerHTML = document.getElementById("txtMax" + i).value;								
					
						
					}														
					
					for(i=1; i<=9; i++) {					
						if(document.getElementById("grdPesquisa_0_cell_" + i)) {
							if(document.getElementById("grdPesquisa_0_cell_" + i).style.display == "none") {
								document.getElementById("tdHeader" + i).style.display = "none";
							}
						}					
					}	
				} catch (ex){ document.write("<span style='color:white;'>" + ex.message + " - 10 - " + "</span>"); }													
		
		/*
			if(document.getElementById("divScrollGrid") && document.getElementById("divScrollGrid").style.width <= 740) {
				document.getElementById("divScrollGrid").style.width = 740;
			}				
				
			if(document.getElementById("grdPesquisa") && document.getElementById("grdPesquisa").style.width <= 715) {
				document.getElementById("grdPesquisa").style.width = 715;
			}
			*/	
				
			try {
				if (document.getElementById("grdPesquisa") && document.getElementById("divGridMovimentacao") && document.getElementById("divScrollGrid")) {
						
					var tam = document.getElementById("grdPesquisa").clientHeight;
					//alert(tam);
						
					if (tam > 260)
						tam = 260;
														
					// Flow 655189 - dgomes - 18/12/2008
					// As linhas abaixo foram comentadas porque estavam 
					// redimensionando de forma errada a altura do grid
					
					//document.getElementById("divGridMovimentacao").style.height = (tam+38);
					//document.getElementById("divScrollGrid").style.height = tam;
				}
			} catch (ex) { document.write("<span style='color:white;'>" + ex.message + " - 30 - " + "</span>"); }
			/*						
			function JSFX_FloatTopDiv() {			
				if(document.getElementById("divScrollGrid")) {
					var newPos = document.getElementById("divScrollGrid").scrollTop;					
					for(i=1;i<=9;i++) {
						if(document.getElementById("header" + i)) {
							document.getElementById("header" + i).style.top = newPos;
						}
					}												
					setTimeout("JSFX_FloatTopDiv()", 5);					
					return true;					
				}
				
				return false;
			}

			JSFX_FloatTopDiv();
				
			try {
				if (apagar == 1)
					document.getElementById('divVidas').style.display = 'none';
			} catch (ex) {
			
			}
			*/
			 top.escondeaguarde();
			
		</script>
	</body>
</HTML>
