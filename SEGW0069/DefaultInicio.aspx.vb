Partial Class DefaultInicio
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim v_sValorParametro As String
        Dim v_oParametro As New Alianca.Seguranca.Web.LinkSeguro
        v_sValorParametro = v_oParametro.GerarParametro("Default.aspx", Alianca.Seguranca.Web.LinkSeguro.TempoExpiracao.Curto, Request.ServerVariables("REMOTE_ADDR"), "ALGRIGORIO")

        v_oParametro = Nothing

        'REMOVER
        Session("ramo") = 93
        Session("apolice") = 13354
        Session("subgrupo_id") = 8
        'REMOVER

        Response.Redirect("FechamentoFatura.aspx?SLinkSeguro=" & v_sValorParametro)

    End Sub

End Class