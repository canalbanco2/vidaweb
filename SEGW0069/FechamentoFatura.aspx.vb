Partial Class FechamentoFatura
    Inherits System.Web.UI.Page

    Public data As String
    Public periodos As String
    Public ibloqueio As Boolean = False

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ucPaginacao As segw0069.paginacaogrid
    Protected WithEvents lblPeriodoFatura2 As System.Web.UI.WebControls.Literal


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            Dim linkseguro As Alianca.Seguranca.Web.LinkSeguro

            'Implementa��o da SABL0101
            linkseguro = New Alianca.Seguranca.Web.LinkSeguro
            Dim usuario As String = linkseguro.LerUsuario("Default.aspx", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))
            If Request.QueryString("fatura") = "" Then
                If usuario = "" Then
                    'Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
                    'Exit Sub
                End If

                Session("usuario_id") = linkseguro.Usuario_ID
                Session("usuario") = linkseguro.Login_REDE
                Session("cpf") = linkseguro.CPF

            End If

            'Response.Write("apolice=" & Session("apolice"))
            ''Response.Write("ramo=" & Session("ramo"))
            'Response.Write("subgrupo_id=" & Session("subgrupo_id"))
            'Response.Write("usuario_id=" & Session("usuario_id"))
            'Response.Write("usuario=" & Session("usuario"))
            'Response.Write("cpf=" & Session("cpf"))
            'Response.End()

            Dim cAmbiente As Alianca.Seguranca.Web.ControleAmbiente
            Dim url As String

            cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
            url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
            cAmbiente.ObterAmbiente(url)
            If Alianca.Seguranca.BancoDados.cCon.configurado Then
                Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
                'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            Else
                Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
                'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            End If

            Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)

            'Me.btnFecharMovimentacao.Attributes.Add("onclick", "onclick='top.escondeaguarde();'")

            If Not IsPostBack Then

                Try

                    For Each m As String In Request.QueryString.Keys
                        Session(m) = Request.QueryString(m)
                    Next

                Catch ex As Exception
                    Dim excp As New clsException(ex)
                End Try
            End If

            Me.periodos = getPeriodoCompetenciaSession()


            If Not IsPostBack Then
                '    '''Pega os dados das condi��es do Subgrupo

                getCondicoesSubgrupo()
                mConsultaUsuarios()
                'lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("nomeSubgrupo") & "<br>Fun��o: Encerrar Movimenta��o "
                lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomesubGrupo") & "<br>Fun��o: Encerrar Movimenta��o "

                lblVigencia.Text = "<br> Per�odo de Compet�ncia: " & periodos

                lblPeriodoFatura.Text = periodos
                data = lblPeriodoFatura.Text

                Dim msgFim As String = periodos

                'Dim dtFim As String = msgFim.Split(" at� ")(1)
                Dim dtFim As String = msgFim.Substring(13, 10)
                Dim nDiasPosFaturamento As Integer   'msgFim.Substring(13, 10)

                If Not IsDate(dtFim) Then
                    Throw New Exception
                End If

                '' acrescentado
                Session("dtFim") = dtFim

                lblNavegacao.Visible = True

                'Autor: pablo.dias (Nova Consultoria) - Data da Altera��o: 04/10/2012
                'Demanda: 12784293 - Item: 12 - Permitir encerrar a fatura ap�s o prazo.
                ' Acesso dado aos administradores de Subgrupo e Master
                'If Session("acesso") = "1" Or Session("acesso") = 6 Then
                If Session("acesso") = "1" Or Session("acesso") = "3" Or Session("acesso") = "6" Or Session("acesso") = "7" Then
                    ' Gabriel Vieira (22/02/2011) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                    ' Inclus�o do perfil de acesso 6 na condi��o.
                    ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                    nDiasPosFaturamento = 0
                Else
                    nDiasPosFaturamento = cUtilitarios.GetDataLimiteFaturamento(True)
                End If

                dtFim = cUtilitarios.GetDataLimiteFaturamento(False)

                'msgFim = "O encerramento da movimenta��o deve ser realizado somente dentro do per�odo de compet�ncia " & msgFim
                'msgFim = "O encerramento da movimenta��o deveria ser realizado at� " & New Date(Now.Year, Now.Month, Now.Day).AddDays(nDiasPosFaturamento)
                msgFim = "O encerramento da movimenta��o deveria ser realizado at� " & dtFim

                '' acrescentado
                Session("msgFim") = dtFim

                btnFecharMovimentacao.Attributes.Add("onclick", "return confirmaFechamento(" & nDiasPosFaturamento & ", '" & msgFim & "')")

            End If

            If Request.QueryString("fatura") <> "" Then
                encerraFatura()
                If ibloqueio = False Then
                    pnlAlerta.Visible = True
                Else
                    pnlFatura.Visible = True
                End If
                Me.lblSemUsuario.Visible = False
                pnlBtnEncerrar.Visible = False
                Me.lblListagemVidas.Visible = False
                cUtilitarios.escreveScript("var apagar = 1;")
            End If


            ' Gabriel Vieira (22/02/2011) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            ' Remo��o de restri��o ao perfil de acesso 6.

                ' Projeto 539202 - Jo�o Ribeiro - 29/04/2009
            'If Session("acesso") = "6" Then
            '    btnFecharMovimentacao.Enabled = False
            'End If
                ' Projeto 539202 - Fim
            ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub


    Private Function getTpAcesso() As String
        Dim ind_acesso As String
        Select Case Session("acesso").ToString
            Case "1"
                ind_acesso = "Administrador da Alian�a do Brasil"
            Case "2"
                ind_acesso = "Administrador de Ap�lice"
            Case "3"
                ind_acesso = "Administrador de Subgrupo"
            Case "4"
                ind_acesso = "Operador"
            Case "5" ' Projeto 539202 - Jo�o Ribeiro - 30/04/2009
                ind_acesso = "Consulta"
            Case "6" ' Projeto 539202 - Jo�o Ribeiro - 30/04/2009
                ind_acesso = "Central de Atendimento"
        End Select

        Return ind_acesso
    End Function


    Private Sub encerraFatura()

        Dim bd As cAcompanhamento
        Dim c_bd As cAcompanhamento

        '########## Jo�o Ribeiro - Flow 485296 - 05/03/2009 ##########
        ' Trecho comentado porque este SP de gravar hist�rio (SEGS5699_SPI) 
        'j� � chamado pelo SP SEGS5766_SPI.

        '''[new code - Objetivo: Gravar log mesmo que transa��o seja encerrada]
        'Try
        '    Dim c_wf_id As Integer = getFaturaAtualSession()
        '    c_bd = New cAcompanhamento(cDadosBasicos.eBanco.vida_web_db)

        '    Dim c_data_inicio As String = Me.periodos.Substring(0, 10)
        '    Dim c_data_fim As String = Me.periodos.Substring(13, 10)

        '    c_bd.GravaHistorico(Session("ramo"), Session("apolice"), Session("subgrupo_id"), 6785, 0, getFaturaAtualSession(), cUtilitarios.trataDataDB(c_data_inicio), cUtilitarios.trataDataDB(c_data_fim), Session("usuario"))
        '    c_bd.ExecutaSQL()

        'Catch ex As Exception
        '    Dim excp As New clsException(ex)
        'End Try

        '''[end code]
        ' Flow 485296 - Fim


        'FLOW 839543 - Jo�o Ribeiro - Confitec Sistemas - Numero da transa��o
        Dim iTran As Integer = -1
        ' Flow 839543 - Fim

        Dim wf_id As Integer = getFaturaAtualSession()
        ibloqueio = False
        bd = New cAcompanhamento(cDadosBasicos.eBanco.vida_web_db)
        Try

            Dim data_inicio As String = Me.periodos.Substring(0, 10)
            Dim data_fim As String = Me.periodos.Substring(13, 10)

            bd.SEGS5766_SPI(getFaturaAtualSession(), Session("ramo"), Session("apolice"), Session("subgrupo_id"), 6785, 0, cUtilitarios.trataDataDB(data_inicio), cUtilitarios.trataDataDB(data_fim), Session("usuario"))

            'cUtilitarios.br(bd.SQL)
            'Response.End()

            'FLOW 839543 - Jo�o Ribeiro - Confitec Sistemas
            '##### Comentado porque, al�m de o DataReader n�o ser usado, ele absorve os erros do SQL Server
            'Dim dr As Data.SqlClient.SqlDataReader
            'dr = bd.ExecutaSQL_DR()
            'dr.Close()
            'dr = Nothing

            ' Executa transa��o
   


            '*******************************************************************************
            '*******************************************************************************
            ' FLOW 4367493 - BLOQUEIO AUTOM�TICO DE FATURAMENTO POR N�O RENOVA��O  
            ' Desenvolvedor: Tiago B. Nascimento - Confitec
            bd.SEGS9517_SPI(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
            Dim dr As Data.SqlClient.SqlDataReader
            dr = bd.ExecutaSQL_DR()

            If dr.Read() Then
                ' incluir alerta de fatura bloqueada
                ibloqueio = True
                Page.ClientScript.RegisterStartupScript(Page.GetType(), Guid.NewGuid().ToString(), "alert('Vig�ncia superior a data cadastrada! N�o ser� poss�vel emitir a fatura!');", True)
                Exit Sub
            End If

            dr.Close()
            dr = Nothing

            bd.SEGS5766_SPI(getFaturaAtualSession(), Session("ramo"), Session("apolice"), Session("subgrupo_id"), 6785, 0, cUtilitarios.trataDataDB(data_inicio), cUtilitarios.trataDataDB(data_fim), Session("usuario"))

            iTran = bd.IniciaTransacao()
            bd.ExecutaSQL_Non_Query(iTran)
            bd.CommitTransacao(iTran)
            iTran = -1

            '** Fim FLOW 4367493************************************************************
            '*******************************************************************************
            '*******************************************************************************

            'FLOW 839543 - Fim
            cUtilitarios.escreveScript("top.document.getElementById('Link5').onclick = function() { alert('" & cUtilitarios.GetDataEncerramento() & "');return false;}")

            If Session("acesso") <> "3" Then
                cUtilitarios.escreveScript("top.document.getElementById('Hyperlink4').onclick = function() { alert('" & cUtilitarios.GetDataEncerramento() & "');return false;}")
            End If

            cUtilitarios.escreveScript("top.document.getElementById('linkCarregarExtratoVidas').onclick = function() { alert('" & cUtilitarios.GetDataEncerramento() & "');return false;}")
            cUtilitarios.escreveScript("top.document.getElementById('Link6').onclick = function() { alert('" & cUtilitarios.GetDataEncerramento() & "');return false;}")
            cUtilitarios.escreveScript("top.document.getElementById('Link4').onclick = function() { alert('" & cUtilitarios.GetDataEncerramento() & "');return false;}")

            montaFaturaAtual(wf_id) 'Atualiza o resumo da fatura atual na default.asp

            Panel1.Visible = False

        Catch ex As Exception
            'Response.Write(bd.SQL)

            'cUtilitarios.escreveScript("top.escondeaguarde()")
            Dim excp As New clsException(ex)


            'FLOW 839543 - Jo�o Ribeiro - Confitec Sistemas
            If iTran >= 0 Then
                Try
                    bd.RollBackTransacao(iTran)
                Catch ex2 As Exception
                End Try
            End If

            If ibloqueio = True Then ' FLOW 4367493
                iTran = -1
                lblFaturaBloqueada.Text = ex.Message
            Else
                iTran = -1
                lblListagemEnviada.Text = ex.Message
                'FLOW 839543 - Fim
            End If

        End Try

    End Sub

    Private Function getFaturaAtualSession() As Integer
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.vida_web_db)
        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

        Try

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
            Dim wf_id As Integer = 0

            If dr.HasRows Then
                While dr.Read()
                    wf_id = CInt(dr.GetValue(0))
                End While
            End If
            dr.Close()
            dr = Nothing
            Return wf_id
        Catch ex As Exception
            Dim excp As New clsException(ex)
            Return Nothing
        End Try
    End Function


    Private Function getPeriodoCompetenciaSession() As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.vida_web_db)
        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

        Dim dr As Data.SqlClient.SqlDataReader

        Try
            dr = bd.ExecutaSQL_DR()

            Dim data_inicio, data_fim, retorno As String

            If dr.HasRows Then
                While dr.Read()

                    data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                    data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)

                End While
            End If
            dr.Close()
            dr = Nothing
            retorno = data_inicio & " a " & data_fim
            Return retorno
        Catch ex As Exception
            Dim excp As New clsException(ex)
            Return ""
        End Try


    End Function

    Private Sub btnFechaPagina_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Response.Redirect("Centro.aspx")

    End Sub


    '''Consulta os usu�rios
    Private Sub mConsultaUsuarios()
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.vida_web_db)

        Dim cpf As String = "null"
        Dim nome As String = "null"
        Dim tipo As String = "null"

        bd.SEGS5655_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), cpf, nome, "null", tipo)

        'Response.Write(bd.SQL)
        Try
            Dim dt As Data.DataTable = bd.ExecutaSQL_DT

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then

                Me.grdPesquisa.CurrentPageIndex = Session("indice")
                Me.grdPesquisa.SelectedIndex = -1

                Me.ucPaginacao._valor = "2"
                Me.ucPaginacao.GridDataBind(Me.grdPesquisa, dt)
                Session("grdDescarte") = dt

                '' acrescentado
                Session("nPaginas") = Math.Ceiling(dt.Rows.Count / grdPesquisa.PageSize)
                '' acrescentado
                ativaLinkPag(Session("nPaginas"))

                Me.lblSemUsuario.Visible = False
                ucPaginacao.Visible = True
                grdPesquisa.Visible = True
                gridMovimentacao.Visible = True
            Else
                Session.Remove("indice")
                Session.Remove("grdDescarte")
                Me.lblSemUsuario.Text = "<br><br><br>Nenhuma Movimenta��o encontrada para o per�odo.<br><br>"
                Me.lblSemUsuario.Visible = True
                ucPaginacao.Visible = False
                grdPesquisa.Visible = False
                gridMovimentacao.Visible = False

            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)

        End Try

    End Sub


    Private Sub getCondicoesSubgrupo()
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.vida_web_db)
        Dim dr As Data.SqlClient.SqlDataReader

        bd.SEGS5705_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

        Try

            dr = bd.ExecutaSQL_DR()


            If dr.Read Then
                Me.lblIdadeMaxima.Text = IIf(dr.GetValue(1).ToString.Trim = "", " --- ", dr.GetValue(1).ToString.Trim)
                Me.lblIdadeMinima.Text = IIf(dr.GetValue(0).ToString.Trim = "", " --- ", dr.GetValue(0).ToString.Trim)
            Else
                Me.lblIdadeMaxima.Text = " --- "
                Me.lblIdadeMinima.Text = " --- "
            End If
            dr.Close()

            bd.SEGS5706_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

            dr = bd.ExecutaSQL_DR()


            If dr.Read Then
                Me.lblMultSalarial.Text = IIf(dr.GetValue(2).ToString.Trim = "", " --- ", dr.GetValue(2).ToString.Trim)
                Me.lblCapitalSegurado.Text = IIf(dr.GetValue(1).ToString.Trim = "", " --- ", dr.GetValue(1).ToString.Trim)
                Me.lblConjuge.Text = IIf(dr.GetValue(0).ToString.Trim = "", " --- ", dr.GetValue(0).ToString.Trim)
            Else
                Me.lblMultSalarial.Text = " --- "
                Me.lblCapitalSegurado.Text = " --- "
                Me.lblConjuge.Text = " --- "
            End If
            dr.Close()
            dr = Nothing

            getLimApolice()


        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try


    End Sub
    Private Sub getLimApolice()
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.vida_web_db)
        Dim dr As Data.SqlClient.SqlDataReader

        Try
            bd.SEGS5744_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

            dr = bd.ExecutaSQL_DR()

            Dim valor_capital_global As String = "0"

            If dr.Read Then
                Me.lblLimiteMaximo.Text = IIf(dr.GetValue(0).ToString.Trim = "", " 0,00 ", cUtilitarios.trataMoeda(dr.GetValue(0).ToString.Trim))
                'Me.LblLimiteMinimo.Text = cUtilitarios.trataMoeda(cUtilitarios.getLimMinCapital())
            End If

            dr.Close()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub montaFaturaAtual(ByVal wf_id As Integer)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.vida_web_db)
        Dim dr As Data.SqlClient.SqlDataReader
        'Mostra Resumo da Fatura Atual
        Dim totalVidaFA As Integer, totalCapitalFA As Double, totalPremioFA As Double
        Try
            bd.ResumoFaturaAtual_Anterior(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Session("usuario"), wf_id)

            dr = bd.ExecutaSQL_DR

            If dr.Read() Then

                totalVidaFA = dr.GetValue(8).ToString
                totalCapitalFA = cUtilitarios.trataMoeda(dr.GetValue(6).ToString)
                totalPremioFA = cUtilitarios.trataMoeda(dr.GetValue(13).ToString)

            End If
            dr.Close()

            'variavel que atualiza na segw0060, o Resumo da Fatura Atual
            Dim script As String = "<script>"


            bd.ResumoFaturaAtual_IAE(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Session("usuario"), wf_id)
            dr = bd.ExecutaSQL_DR

            Dim totalVida As Integer, totalCapital As Double, totalPremio As Double


            script &= "top.document.getElementById('divInclusoes_vidas').innerHTML= '0';" & vbCrLf
            script &= "top.document.getElementById('divInclusoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
            script &= "top.document.getElementById('divInclusoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf

            script &= "top.document.getElementById('divExclusoes_vidas').innerHTML= '0';" & vbCrLf
            script &= "top.document.getElementById('divExclusoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
            script &= "top.document.getElementById('divExclusoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf

            script &= "top.document.getElementById('divAlteracoes_vidas').innerHTML= '0';" & vbCrLf
            script &= "top.document.getElementById('divAlteracoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
            script &= "top.document.getElementById('divAlteracoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf

            script &= "top.document.getElementById('divAcertos_vidas').innerHTML= '0';" & vbCrLf
            script &= "top.document.getElementById('divAcertos_capital').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
            script &= "top.document.getElementById('divAcertos_premio').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf

            script &= "top.document.getElementById('divVidasFT').innerHTML= '" & totalVida + totalVidaFA & "';" & vbCrLf
            script &= "top.document.getElementById('divCapitalFT').innerHTML= '" & cUtilitarios.trataMoeda(totalCapital + totalCapitalFA) & "';" & vbCrLf
            script &= "top.document.getElementById('divPremioFT').innerHTML= '" & cUtilitarios.trataMoeda(totalPremio + totalPremioFA) & "';" & vbCrLf

            While dr.Read()
                If dr.Item("operacao") = "I" Then
                    script &= "top.document.getElementById('divInclusoes_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                    script &= "top.document.getElementById('divInclusoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                    script &= "top.document.getElementById('divInclusoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf
                    'Soma
                    totalVida = totalVida + dr.GetValue(0).ToString
                    totalCapital = totalCapital + cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                    totalPremio = totalPremio + cUtilitarios.trataMoeda(dr.GetValue(2).ToString)

                ElseIf dr.Item("operacao") = "E" Then
                    script &= "top.document.getElementById('divExclusoes_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                    script &= "top.document.getElementById('divExclusoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                    script &= "top.document.getElementById('divExclusoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf
                    'Subtrai
                    totalVida = totalVida - dr.GetValue(0).ToString
                    totalCapital = totalCapital - cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                    totalPremio = totalPremio - cUtilitarios.trataMoeda(dr.GetValue(2).ToString)

                ElseIf dr.Item("operacao") = "A" Then
                    script &= "top.document.getElementById('divAlteracoes_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                    script &= "top.document.getElementById('divAlteracoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                    script &= "top.document.getElementById('divAlteracoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf
                End If

                'totalVida = totalVida + dr.GetValue(0).ToString
                'totalCapital = totalCapital + cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                'totalPremio = totalPremio + cUtilitarios.trataMoeda(dr.GetValue(3).ToString)

                script &= "top.document.getElementById('divVidasFT').innerHTML= '" & totalVida + totalVidaFA & "';" & vbCrLf
                script &= "top.document.getElementById('divCapitalFT').innerHTML= '" & cUtilitarios.trataMoeda(totalCapital + totalCapitalFA) & "';" & vbCrLf
                script &= "top.document.getElementById('divPremioFT').innerHTML= '" & cUtilitarios.trataMoeda(totalPremio + totalPremioFA) & "';" & vbCrLf

            End While

            Response.Write(script & "</script>")

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
        '----------------------------------------------
        dr.Close()
        dr = Nothing
    End Sub

#Region "Eventos"
    Private Sub mGridDataBind(ByVal dt As DataTable)
        Me.ucPaginacao.GridDataBind(grdPesquisa, dt)
    End Sub

    Private Sub mSelecionaGrid(ByVal ItemIndex As Integer)
        Me.grdPesquisa.SelectedIndex = ItemIndex
        Me.mGridDataBind(Session("grdDescarte"))
        CType(Me.grdPesquisa.SelectedItem.Cells(0).Controls(1), RadioButton).Checked = True
    End Sub

    Private Sub mPaginacaoClick(ByVal Argumento As String) Handles ucPaginacao.ePaginaAlterada
        Me.grdPesquisa.SelectedIndex = -1
        Select Case Argumento
            Case "pro"
                Me.grdPesquisa.CurrentPageIndex += 1
            Case "ant"
                Me.grdPesquisa.CurrentPageIndex -= 1
            Case Else
                Me.grdPesquisa.CurrentPageIndex = Argumento
        End Select
        Session("indice") = Me.grdPesquisa.CurrentPageIndex
    End Sub

    Private Sub grdPesquisa_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdPesquisa.ItemDataBound
        'If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
        'CType(e.Item.Cells(0).FindControl("linkCodDoc"), HyperLink).NavigateUrl = "javascript:busca_registro('" & e.Item.Cells(1).Text.Trim & "','" & e.Item.Cells(2).Text.Trim & "','" & e.Item.Cells(4).Text.Trim & "','" & e.Item.Cells(5).Text.Trim & "');"
        'End If
        'If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
        'CType(e.Item.Cells(0).FindControl("linkCodDocDI"), HyperLink).NavigateUrl = "javascript:excluir_DI('" & e.Item.Cells(1).Text.Trim & "','" & e.Item.Cells(2).Text.Trim & "','" & e.Item.Cells(6).Text.Trim & "');"
        'End If
    End Sub

#End Region



    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '
    ' PROBLENA NA PAGINA��O
    '
    ' Como o controle paginacao desta p�gina "paginacaogrid.ascx" � respons�vel t�mbel pelo 
    ' carregamento do grid este foi mantido, mais, sua funcionalidade de pagina��o foi 
    ' desabilitada, sendo esta funcionalidade mantida por um controle de pagina��o construido 
    ' na pr�pria p�gina "FechamentoFatura.aspx".
    '
    ' Para implementar esta mudan�a foi necess�rio acrescentar os seguintes seguintes m�todos:
    '
    '       Public Sub pagina(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '       Public Sub ativaLinkPag(ByVal n As Integer)
    '       Private Sub eAnt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '       Private Sub ePro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '       Private Sub AtivaUnderLine(ByVal b As Boolean)
    '       Private Sub MarcaPaginaunderline()
    '       Private Sub AtivaPaginacaoAntPro()
    '       Private Function RetornaNPagReal()
    '       Private Sub PlotaNPag()
    '
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Public Sub pagina(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles a1.Click, a2.Click, a3.Click, a4.Click, a5.Click
        Session("indice") = CInt(CType(sender, LinkButton).Text) - 1

        Me.ucPaginacao.GridDataBind(Me.grdPesquisa, Session("grdDescarte"))

        PlotaNPag()
        AtivaPaginacaoAntPro()
        AtivaUnderLine(True)

        CType(sender, LinkButton).Font.Underline = False

    End Sub

    Public Sub ativaLinkPag(ByVal n As Integer)

        a1.Visible = False
        a2.Visible = False
        a3.Visible = False
        a4.Visible = False
        a5.Visible = False

        If n >= 1 Then
            a1.Visible = True
        End If
        If n >= 2 Then
            a2.Visible = True
        End If
        If n >= 3 Then
            a3.Visible = True
        End If
        If n >= 4 Then
            a4.Visible = True
        End If
        If n >= 5 Then
            a5.Visible = True
        End If

    End Sub

    Private Sub eAnt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles eAnt.Click
        Session("indice") = Session("indice") - 1
        Me.ucPaginacao.GridDataBind(Me.grdPesquisa, Session("grdDescarte"))

        PlotaNPag()
        AtivaPaginacaoAntPro()
        AtivaUnderLine(True)
        ''''
        MarcaPaginaunderline()

        ativaLinkPag(Session("nPaginas") - a1.Text + 1)
    End Sub

    Private Sub ePro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ePro.Click
        Session("indice") = Session("indice") + 1
        Me.ucPaginacao.GridDataBind(Me.grdPesquisa, Session("grdDescarte"))

        PlotaNPag()
        AtivaPaginacaoAntPro()
        AtivaUnderLine(True)

        MarcaPaginaunderline()

        ativaLinkPag(Session("nPaginas") - a1.Text + 1)
    End Sub

    Private Sub AtivaUnderLine(ByVal b As Boolean)
        a1.Font.Underline = b
        a2.Font.Underline = b
        a3.Font.Underline = b
        a4.Font.Underline = b
        a5.Font.Underline = b
    End Sub

    Private Sub MarcaPaginaunderline()
        If RetornaNPagReal() = 0 Then : a1.Font.Underline = False : Else : a1.Font.Underline = True : End If
        If RetornaNPagReal() = 1 Then : a2.Font.Underline = False : Else : a2.Font.Underline = True : End If
        If RetornaNPagReal() = 2 Then : a3.Font.Underline = False : Else : a3.Font.Underline = True : End If
        If RetornaNPagReal() = 3 Then : a4.Font.Underline = False : Else : a4.Font.Underline = True : End If
        If RetornaNPagReal() = 4 Then : a5.Font.Underline = False : Else : a5.Font.Underline = True : End If
    End Sub

    Private Sub AtivaPaginacaoAntPro()
        If Session("indice") = 0 Then
            eAnt.Visible = False
        Else
            eAnt.Visible = True
        End If

        If Session("indice") >= Session("nPaginas") - 1 Then
            ePro.Visible = False
        Else
            ePro.Visible = True
        End If
    End Sub

    Private Function RetornaNPagReal()
        Dim retorno As Integer = 0
        If Session("indice") = 0 Or Session("indice") Mod 5 = 0 Then
            retorno = 0
        Else
            retorno = Session("indice") - (Math.Ceiling(Session("indice") / 5) * 5 - 5)
        End If
        Return retorno
    End Function

    Private Sub PlotaNPag()
        Dim pag As Integer = (Math.Ceiling((Session("indice") + 1) / 5) * 5 - 5)
        a1.Text = pag + 1
        a2.Text = pag + 2
        a3.Text = pag + 3
        a4.Text = pag + 4
        a5.Text = pag + 5
    End Sub

End Class
