Public Class cUtilitarios

    Public Shared Function getCapitalSeguradoSession() As String

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.vida_web_db)
        bd.SEGS5706_SPS(Web.HttpContext.Current.Session("apolice"), Web.HttpContext.Current.Session("ramo"), 6785, 0, Web.HttpContext.Current.Session("subgrupo_id"))

        Dim capitalSegurado As String = ""

        Try
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR

            Dim count As Int16 = 1
            Dim text As String
            Dim value As String
            Dim temp As String

            dr.Read()

            capitalSegurado = dr.GetValue(1).ToString()

            dr.Close()
            dr = Nothing

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return capitalSegurado

    End Function


    Public Shared Function MostraColunaCapital() As Boolean

        Dim mostraCapital As Boolean = True

        Dim capitalSegurado As String = getCapitalSeguradoSession()

        Select Case capitalSegurado
            'Case "Capital Global"
            '    mostraCapital = False
            'Case "M�ltiplo sal�rio"
            '    mostraCapital = True
            'Case "Capital fixo"
            '    mostraCapital = True
            'Case "Capital informado"
            '    mostraCapital = True
            'Case Else
            '    mostraCapital = True
        End Select

        Return mostraCapital

    End Function


    Public Shared Function MostraColunaSalario() As Boolean

        Dim mostraSalario As Boolean = False

        Dim capitalSegurado As String = getCapitalSeguradoSession()

        Select Case capitalSegurado
            Case "M�ltiplo sal�rio"
                mostraSalario = True

                'Case "Capital Global"
                '    mostraCapital = False
                'Case "M�ltiplo sal�rio"
                '    mostraCapital = True
                'Case "Capital fixo"
                '    mostraCapital = True
                'Case "Capital informado"
                '    mostraCapital = True
                'Case Else
                '    mostraCapital = True
        End Select

        Return mostraSalario

    End Function

    'Trata a data para o seguinte formato dd/mm/aaaa hh:mm
    Public Shared Function trataDataHora(ByVal data As String) As String

        Try
            Dim dt_final As String

            Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim ano As String = CType(data, DateTime).Year

            Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = dia + "/" + mes + "/" + ano


            Return dt_final + " " + hora + ":" + minuto

        Catch ex As Exception

            Return ""

        End Try

    End Function

    'Trata a data para o seguinte formato dd/mm/aaaa 
    Public Shared Function trataData(ByVal data As String) As String

        Try

            Dim dt_arr() As String = data.Split("/")
            Dim dt_final As String

            'Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim dia As String = dt_arr(0).PadLeft(2, "0")
            'Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim mes As String = dt_arr(1).PadLeft(2, "0")
            'Dim ano As String = CType(data, DateTime).Year
            Dim ano As String = dt_arr(2)

            'Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            'Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = dia + "/" + mes + "/" + ano


            Return dt_final

        Catch ex As Exception

            Return ""

        End Try

    End Function

    'Trata a data para o seguinte formato dd/mm/aaaa 
    Public Shared Function trataDataDB(ByVal data As String) As String

        Try

            Dim dt_arr() As String = data.Split("/")
            Dim dt_final As String

            'Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim dia As String = dt_arr(0).PadLeft(2, "0")
            'Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim mes As String = dt_arr(1).PadLeft(2, "0")
            'Dim ano As String = CType(data, DateTime).Year
            Dim ano As String = dt_arr(2)

            'Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            'Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = ano + mes + dia


            Return dt_final

        Catch ex As Exception

            Return ""

        End Try

    End Function

    Public Shared Function trataSmalldate(ByVal data As String) As String

        Try
            Dim dt_final As String

            Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim ano As String = CType(data, DateTime).Year

            Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = ano + mes + dia


            Return dt_final + " " + hora + ":" + minuto

        Catch ex As Exception

            Return ""

        End Try

    End Function

    '''Exibe o valor em um alert na tela
    Public Shared Sub br(ByVal valor As String)

        Dim texto As String

        texto = "<script>" + vbNewLine
        texto &= "alert(""" + valor + """);"
        texto &= "</script>"

        System.Web.HttpContext.Current.Response.Write(texto)
    End Sub

    Public Shared Function destrataCPF(ByVal cpf As String) As String

        If cpf.Length = 14 Then
            Return cpf.Replace(".", "").Replace("-", "")
        Else
            Return cpf
        End If

    End Function

    Public Shared Function trataCPF(ByVal cpf As String) As String

        If cpf.Length = 11 Then
            Return cpf.Substring(0, 3) + "." + cpf.Substring(3, 3) + "." + cpf.Substring(6, 3) + "-" + cpf.Substring(9, 2)
        Else
            Return cpf
        End If

    End Function

    Public Shared Function trataMoeda(ByVal valor As String) As String

        If valor = "&nbsp;" Or valor = "" Then
            Return ""
        Else
            Return String.Format(String.Format("{0:c}", CDbl(valor))).Substring(3)
        End If

    End Function

    Public Shared Sub escreveScript(ByVal valor As String)
        Web.HttpContext.Current.Response.Write("<script>" + valor + "</script>")
    End Sub

    Public Shared Function GetDataEncerramento() As String

        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.vida_web_db)
        Dim dr As Data.SqlClient.SqlDataReader

        BD.SEGS6843_SPS(Web.HttpContext.Current.Session("ramo"), Web.HttpContext.Current.Session("apolice"), Web.HttpContext.Current.Session("subgrupo_id"))

        Try
            dr = BD.ExecutaSQL_DR()

            If dr.Read() Then
                Return "Ap�lice encerrada na data e hora " & cUtilitarios.trataDataHora(dr.GetValue(0).ToString) & "."
            End If

            dr.Close()
            dr = Nothing


        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return ""


    End Function

    Public Shared Function GetDataLimiteFaturamento(ByVal bTipoQtdDias As Boolean)

        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.vida_web_db)
        Dim dr As Data.SqlClient.SqlDataReader
        Dim wf_id As Long

        BD.SEGS5696_SPS(Web.HttpContext.Current.Session("apolice"), Web.HttpContext.Current.Session("ramo"), Web.HttpContext.Current.Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

        Try
            dr = BD.ExecutaSQL_DR()
            If dr.HasRows Then
                While dr.Read()

                    wf_id = dr.GetValue(0).ToString()

                End While
            End If
            dr.Close()
            dr = Nothing
        Catch ex As Exception
            Dim excp As New clsException(ex)
            Return ""
        End Try


        BD.SEGS5722_SPS(Web.HttpContext.Current.Session("apolice"), Web.HttpContext.Current.Session("ramo"), Web.HttpContext.Current.Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID, wf_id)

        Try
            dr = BD.ExecutaSQL_DR()

            If dr.Read() Then
                If bTipoQtdDias Then
                    Return dr.GetValue(0)
                Else
                    Return trataData(dr.GetValue(12))
                End If
            End If

            dr.Close()
            dr = Nothing

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        'Return ""

    End Function
End Class
