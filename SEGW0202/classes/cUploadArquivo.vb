﻿Public Class cUploadArquivo

#Region "Atributos"
    Private _seguradoId As Int32
    Private _motivo As String
    Private _statusDocumentoId As Integer
    Private _apoliceId As Int64
    Private _ramoId As Int16
    Private _subgrupoId As Int16
    Private _nomeArquivo As String
    Private _novoNomeArquivo As String
    Private _strCaminho As String
    Private _tipoDocumentoId As String
    Private _tipoPendencia As String
    Private _usuario As String

#End Region

#Region "Propriedades"
    Public Property SEGURADO_ID() As Int32
        Get
            Return Me._seguradoId
        End Get
        Set(ByVal Value As Int32)
            Me._seguradoId = Value
        End Set
    End Property
    Public Property MOTIVO() As String
        Get
            Return Me._motivo
        End Get
        Set(ByVal Value As String)
            Me._motivo = Value
        End Set
    End Property
    Public Property STATUS_DOCUMENTO() As Integer
        Get
            Return Me._statusDocumentoId
        End Get
        Set(ByVal Value As Int32)
            Me._statusDocumentoId = Value
        End Set
    End Property
    Public Property APOLICEID() As Int64
        Get
            Return Me._apoliceId
        End Get
        Set(ByVal Value As Int64)
            Me._apoliceId = Value
        End Set
    End Property
    Public Property RAMOID() As Int16
        Get
            Return Me._ramoId
        End Get
        Set(ByVal Value As Int16)
            Me._ramoId = Value
        End Set
    End Property
    Public Property SUBGRUPOID() As Int16
        Get
            Return Me._subgrupoId
        End Get
        Set(ByVal Value As Int16)
            Me._subgrupoId = Value
        End Set
    End Property
    Public Property NOMEARQUIVO() As String
        Get
            Return Me._nomeArquivo
        End Get
        Set(ByVal Value As String)
            Me._nomeArquivo = Value
        End Set
    End Property
    Public Property NOVONOMEARQUIVO() As String
        Get
            Return Me._novoNomeArquivo
        End Get
        Set(ByVal Value As String)
            Me._novoNomeArquivo = Value
        End Set
    End Property
    Public Property CAMINHO() As String
        Get
            Return Me._strCaminho
        End Get
        Set(ByVal Value As String)
            Me._strCaminho = Value
        End Set
    End Property
    Public Property USUARIO() As String
        Get
            Return Me._usuario
        End Get
        Set(ByVal Value As String)
            Me._usuario = Value
        End Set
    End Property
    Public Property TIPODOCUMENTOID() As Integer
        Get
            Return Me._tipoDocumentoId
        End Get
        Set(ByVal Value As Integer)
            Me._tipoDocumentoId = Value
        End Set
    End Property
    Public Property TIPOPENDENCIA() As String
        Get
            Return Me._tipoPendencia
        End Get
        Set(ByVal Value As String)
            Me._tipoPendencia = Value
        End Set
    End Property
#End Region


    Public Sub New()

    End Sub

    Public Sub New(ByVal segurado_id As Int32, ByVal motivo As String, ByVal status_documento As Integer, ByVal nomeArquivo As String, ByVal caminho As String)
        Me.SEGURADO_ID = segurado_id
        Me.MOTIVO = motivo
        Me.STATUS_DOCUMENTO = status_documento
        Me.NOMEARQUIVO = nomeArquivo
        Me.CAMINHO = caminho

        If status_documento = 6 Then
            Me.NOVONOMEARQUIVO = "LAUDO_" & Now.Day.ToString.PadLeft(2, "0") & Now.Month.ToString.PadLeft(2, "0") & Now.Year & System.IO.Path.GetExtension(Me.NOMEARQUIVO)
            Me.TIPODOCUMENTOID = 2
        Else
            Me.NOVONOMEARQUIVO = "DPS_" & Now.Day.ToString.PadLeft(2, "0") & Now.Month.ToString.PadLeft(2, "0") & Now.Year & System.IO.Path.GetExtension(Me.NOMEARQUIVO)
            Me.TIPODOCUMENTOID = 1
        End If

    End Sub

#Region "Métodos"
    '''Define os diretório onde será gravado o arquivo
    Public Sub controleArquivo()
        Dim ambiente As New Alianca.Seguranca.Web.ControleAmbiente
        ambiente.ObterAmbiente(System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToUpper)

        Dim post As String = ambiente.Post
        post = Me.CAMINHO & CType(Me.SEGURADO_ID, String).Trim

        If Not IO.Directory.Exists(post) Then
            IO.Directory.CreateDirectory(post)
        End If

        Me.CAMINHO = post
    End Sub
#End Region

End Class

