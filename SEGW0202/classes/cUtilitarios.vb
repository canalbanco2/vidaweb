Public Class cUtilitarios

    'Trata a data para o seuginte formato dd/mm/aaaa hh:mm
    Public Shared Function trataDataHora(ByVal data As String) As String

        Try
            Dim dt_final As String

            Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim ano As String = CType(data, DateTime).Year

            Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = dia + "/" + mes + "/" + ano


            Return dt_final + " " + hora + ":" + minuto

        Catch ex As Exception

            Return ""

        End Try

    End Function

    '''Exibe o valor em um alert na tela
    Public Shared Sub br(ByVal valor As String)

        Dim texto As String

        texto = "<script>" + vbNewLine
        texto &= "alert(""" + valor + """);"
        texto &= "</script>"

        System.Web.HttpContext.Current.Response.Write(texto)
    End Sub

    Public Shared Function trataMoeda(ByVal valor As String) As String
        If valor = "&nbsp;" Or valor = "" Then
            Return ""
        Else
            Return String.Format(String.Format("{0:c}", CDbl(valor))).Substring(3).Replace("(", "").Replace(")", "")
        End If


    End Function


    Public Shared Function trataCPF(ByVal cpf As String) As String

        If cpf.Length = 11 Then
            Return cpf.Substring(0, 3) + cpf.Substring(3, 3) + cpf.Substring(6, 3) + cpf.Substring(9, 2)
        Else
            Return cpf
        End If

    End Function

    Public Shared Function destrataCPF(ByVal cpf As String) As String

        If cpf.Length = 14 Then
            Return cpf.Replace(".", "").Replace("-", "").Replace("'", "")

        Else
            Return cpf
        End If

    End Function

    Public Shared Sub escreveScript(ByVal valor As String)
        HttpContext.Current.Response.Write("<script>" + valor + "</script>")
    End Sub

    Public Shared Sub escreveScript2(ByVal valor As String)
        HttpContext.Current.Response.Write("<script  type='text/javascript' DEFER='DEFER'>" + valor + "</script>")
    End Sub

    Public Shared Sub escreve(ByVal valor As String)
        HttpContext.Current.Response.Write(valor)
    End Sub

    'Trata a data para o seguinte formato dd/mm/aaaa 
    Public Shared Function trataData(ByVal data As String) As String

        Try

            Dim dt_arr() As String = data.Split("/")
            Dim dt_final As String

            'Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim dia As String = dt_arr(0).PadLeft(2, "0")
            'Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim mes As String = dt_arr(1).PadLeft(2, "0")
            'Dim ano As String = CType(data, DateTime).Year
            Dim ano As String = dt_arr(2)

            'Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            'Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = dia + "/" + mes + "/" + ano


            Return dt_final

        Catch ex As Exception

            Return ""

        End Try

    End Function
    'Trata a data para o seguinte formato dd/mm/aaaa 
    Public Shared Function trataDataDB(ByVal data As String) As String

        Try

            Dim dt_arr() As String = data.Split("/")
            Dim dt_final As String

            'Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim dia As String = dt_arr(0).PadLeft(2, "0")
            'Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim mes As String = dt_arr(1).PadLeft(2, "0")
            'Dim ano As String = CType(data, DateTime).Year
            Dim ano As String = dt_arr(2)

            'Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            'Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = ano + mes + dia


            Return dt_final

        Catch ex As Exception

            Return ""

        End Try

    End Function

    Public Shared Function getPathPost(ByVal pAmbiente As Int16) As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SQL = "exec desenv_db..SEGS6880_SPS @pAmbiente = " & pAmbiente

        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()


        If dr.Read Then
            Return IIf(dr.GetString(0).EndsWith("\"), dr.GetString(0), dr.GetString(0) & "\")
        Else
            Throw New Exception("O caminho no qual o arquivo dever� ser copiado n�o foi encontrado. Por favor verificar a stored procedure SEGS6878_SPS.")
        End If

    End Function

    Public Shared Sub MoveArquivo(ByVal pathOrigem As String, ByVal pAmbiente As Int16)

        Dim file As New IO.FileInfo(pathOrigem)

        file.MoveTo(cUtilitarios.getPathPost(pAmbiente) & file.Name)

    End Sub
End Class
