
Public Class cAcompanhamento

#Region "Heran�a"
    Inherits cBanco
#End Region

    Public newSQL As New System.Text.StringBuilder

    Public Sub New(Optional ByVal banco As cDadosBasicos.eBanco = cDadosBasicos.eBanco.VIDA_WEB_DB)
        MyBase.New(banco)
        Alianca.Seguranca.BancoDados.cCon.BancodeDados = banco.ToString

    End Sub

    Function trInt(ByVal valor As Object) As String

        If valor = 0 Then
            Return "null"
        Else
            Return valor
        End If

    End Function

    Function trStr(ByVal valor As Object) As String

        If valor = "" Then
            Return "null"
        Else
            Return "'" & valor.ToString.Replace("'", "''") & "'"
        End If

    End Function
    Public Sub SEGS5696_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal workflow As Integer, ByVal valor_id As Integer)
        SQL = "exec VIDA_WEB_DB..segs5696_sps  @apolice_id = " & apolice_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @tp_wf_id = " & workflow
        SQL &= ", @tp_versao_id = " & valor_id
    End Sub


    Public Sub Busca_Caminho_Arquivo(ByVal Controle_DocumentoId As Integer, ByVal Arquivo_UploadId As Integer)

        SQL = "SELECT caminho "
        SQL &= "FROM [SISAB003].seguros_db.dbo.arquivos_upload_tb WITH (NOLOCK) "
        SQL &= "WHERE controle_documento_id = " & Controle_DocumentoId
        SQL &= "  AND arquivos_upload_id = " & Arquivo_UploadId

    End Sub

    Public Sub Busca_Motivo(ByVal Controle_DocumentoId As Integer, ByVal Arquivo_UploadId As Integer)

        SQL = "SELECT motivo "
        SQL &= "FROM [SISAB003].seguros_db.dbo.arquivos_upload_tb WITH (NOLOCK) "
        SQL &= "WHERE controle_documento_id = " & Controle_DocumentoId
        SQL &= "  AND arquivos_upload_id = " & Arquivo_UploadId

    End Sub
    Public Sub Busca_Status_Tipo_Arquivo(ByVal Controle_DocumentoId As Integer, ByVal Arquivo_UploadId As Integer)

        SQL = "SELECT au.tipo_documento_id, "
        SQL &= "      cd.status_documento_id "
        SQL &= " FROM [SISAB003].seguros_db.dbo.arquivos_upload_tb au WITH (NOLOCK) "
        SQL &= " JOIN [SISAB003].seguros_db.dbo.controle_documento_tb cd WITH (NOLOCK) "
        SQL &= "   ON au.controle_documento_id = cd.controle_documento_id "
        SQL &= "WHERE au.controle_documento_id = " & Controle_DocumentoId
        SQL &= "  AND au.arquivos_upload_id = " & Arquivo_UploadId

    End Sub

    Public Sub Busca_Ultimo_DPS(ByVal Controle_DocumentoId As Integer)

        SQL = "IF NOT EXISTS (SELECT TOP 1 arquivos_upload_id"
        SQL &= "				FROM [SISAB003].seguros_db.dbo.arquivos_upload_tb WITH (NOLOCK) "
        SQL &= "			   WHERE controle_documento_id = " & Controle_DocumentoId
        SQL &= "				 AND tipo_documento_id = 1 "
        SQL &= "			   ORDER BY 1 DESC) "
        SQL &= " BEGIN SELECT 0  END "
        SQL &= " ELSE "
        SQL &= " BEGIN "
        SQL &= "  SELECT TOP 1 arquivos_upload_id "
        SQL &= "    FROM [SISAB003].seguros_db.dbo.arquivos_upload_tb WITH (NOLOCK) "
        SQL &= "   WHERE controle_documento_id = " & Controle_DocumentoId
        SQL &= "     AND tipo_documento_id = 1"
        SQL &= "   ORDER BY 1 DESC "
        SQL &= "  END "

    End Sub
    Public Sub Relacao_Documentos_Pendentes(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal so_medico As String)

        If so_medico <> "" Then
            SQL = "EXEC [SISAB003].seguros_db.dbo.SEGS12166_SPS " & ramo_id & ", " & apolice_id & ", " & subgrupo & ", '" & so_medico & "' "
        Else
            SQL = "EXEC [SISAB003].seguros_db.dbo.SEGS12166_SPS " & ramo_id & ", " & apolice_id & ", " & subgrupo & ", ' ' "
        End If

    End Sub

    Public Sub Busca_Acesso_Perfil(ByVal Controle_DocumentoId As Integer, ByVal Arquivo_UploadId As Integer, ByVal Cpf As String, ByVal Ambiente_Id As Integer)

        SQL = "SELECT ISNULL(ind_acesso, 0), "
        SQL &= "      ISNULL(perfil_id, 0) "
        SQL &= " FROM dbo.usuario_apolice_tb usua WITH (NOLOCK) "
        SQL &= " JOIN [SISAB003].segab_db.dbo.usuario_tb u WITH (NOLOCK) "
        SQL &= "   ON usua.cpf = u.cpf "
        SQL &= " JOIN [SISAB003].segab_db.dbo.verifica_permissao_tb vp WITH (NOLOCK) "
        SQL &= "   ON u.usuario_id = vp.usuario_id  "
        SQL &= " JOIN [SISAB003].seguros_db.dbo.controle_documento_tb cd WITH (NOLOCK)   "
        SQL &= "   ON usua.apolice_id = cd.apolice_id "
        SQL &= "  AND usua.ramo_id = cd.ramo_id "
        SQL &= "  AND usua.subgrupo_id = cd.subgrupo_id "
        SQL &= " JOIN [SISAB003].seguros_db.dbo.arquivos_upload_tb au WITH (NOLOCK)   "
        SQL &= "   ON cd.controle_documento_id = au.controle_documento_id  "
        SQL &= "WHERE au.controle_documento_id = " & Controle_DocumentoId
        SQL &= "  AND au.arquivos_upload_id = " & Arquivo_UploadId
        SQL &= "  AND usua.cpf = '" & Cpf & "' "
        SQL &= "  AND vp.recurso_id = 10922 "
        SQL &= "  AND usua.ind_situacao = 'A' "
        SQL &= "  AND vp.ambiente_id = " & Ambiente_Id

    End Sub

    Public Sub Atualiza_Documento(ByVal Controle_DocumentoId As Integer, ByVal Status_Documento As Integer, ByVal Usuario As String)

        SQL = "EXEC [SISAB003].seguros_db..SEGS12113_SPU "
        SQL &= "@Controle_Documento_Id = " & Controle_DocumentoId & ", "
        SQL &= "@Arquivo_Upload_Id = 0, "
        SQL &= "@usuario = '" & Usuario & "', "
        SQL &= "@Status_Documento_Id = " & Status_Documento & ", "
        SQL &= "@Novo_Nome = NULL, "
        SQL &= "@Motivo = NULL "

    End Sub

    Public Sub Atualiza_Nome(ByVal Controle_DocumentoId As Integer, ByVal Arquivo_UploadId As Integer, ByVal Novo_Nome As String, ByVal Usuario As String)

        SQL = "EXEC [SISAB003].seguros_db..SEGS12113_SPU "
        SQL &= "@Controle_Documento_Id = " & Controle_DocumentoId & ", "
        SQL &= "@Arquivo_Upload_Id = " & Arquivo_UploadId & ", "
        SQL &= "@usuario = '" & Usuario & "', "
        SQL &= "@Status_Documento_Id = NULL, "
        SQL &= "@Novo_Nome = '" & Novo_Nome & "', "
        SQL &= "@Motivo = NULL "

    End Sub

    Public Sub Atualiza_Motivo(ByVal Controle_DocumentoId As Integer, ByVal Arquivo_UploadId As Integer, ByVal Novo_Motivo As String, ByVal Usuario As String)

        SQL = "EXEC [SISAB003].seguros_db..SEGS12113_SPU "
        SQL &= "@Controle_Documento_Id = " & Controle_DocumentoId & ", "
        SQL &= "@Arquivo_Upload_Id = " & Arquivo_UploadId & ", "
        SQL &= "@usuario = '" & Usuario & "', "
        SQL &= "@Status_Documento_Id = NULL, "
        SQL &= "@Novo_Nome = NULL, "
        SQL &= "@Motivo = '" & Novo_Motivo & "' "

    End Sub

    Public Sub Envio_Email(ByVal usuario As String, ByVal corpo_email As String, ByVal destino_email As String)

        SQL = "EXEC [SISAB003].email_db.dbo.SGSS0782_SPI " & _
              "@sigla_recurso = 'SEGW0200', " & _
              "@sigla_sistema = 'SEGBR', " & _
              "@chave = 'SEGW0200'," & _
              "@de = 'alianca@aliancadobrasil.com.br', " & _
              "@para = '" & destino_email & "', " & _
              "@assunto = 'Pend�ncias!', " & _
              "@mensagem = '" & corpo_email & "', " & _
              "@cc = NULL, " & _
              "@anexo = NULL, " & _
              "@formato = '2', " & _
              "@usuario = '" & usuario & "' "

    End Sub

    Public Sub BuscaEmail(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo As String)

        SQL = "SELECT email "
        SQL &= " FROM (SELECT LOWER(email) email "
        SQL &= "             ,(SELECT distinct ind_situacao "
        SQL &= "                 FROM usuario_apolice_tb "
        SQL &= "                WHERE usuario_apolice_tb.apolice_id = usuario_apolice_web_tb.apolice_id "
        SQL &= "                  AND usuario_apolice_tb.ramo_id = usuario_apolice_web_tb.ramo_id "
        SQL &= "                  AND usuario_apolice_tb.cpf = usuario_apolice_web_tb.CPF "
        SQL &= "                  AND usuario_apolice_tb.ind_acesso = '2') AS ind_situacao "
        SQL &= "         FROM usuario_apolice_web_tb "
        SQL &= "        WHERE apolice_id = " & apolice_id & " "
        SQL &= "          AND ramo_id = " & ramo_id & " "
        SQL &= "        UNION ALL "
        SQL &= "       SELECT LOWER(email) email "
        SQL &= "             ,(SELECT distinct ind_situacao "
        SQL &= "                 FROM usuario_apolice_tb "
        SQL &= "                WHERE usuario_apolice_tb.apolice_id = usuario_apolice_web_tb.apolice_id "
        SQL &= "                  AND usuario_apolice_tb.ramo_id = usuario_apolice_web_tb.ramo_id "
        SQL &= "                  AND usuario_apolice_tb.cpf = usuario_apolice_web_tb.CPF "
        SQL &= "                  AND usuario_apolice_tb.ind_acesso = '3') AS ind_situacao "
        SQL &= "         FROM usuario_apolice_web_tb "
        SQL &= "        WHERE apolice_id = " & apolice_id & " "
        SQL &= "          AND ramo_id = " & ramo_id & " "
        SQL &= "        UNION ALL "
        SQL &= "       SELECT LOWER(email) email "
        SQL &= "             ,(SELECT distinct ind_situacao "
        SQL &= "                 FROM usuario_apolice_tb "
        SQL &= "                WHERE usuario_apolice_tb.apolice_id = usuario_apolice_web_tb.apolice_id "
        SQL &= "                  AND usuario_apolice_tb.ramo_id = usuario_apolice_web_tb.ramo_id "
        SQL &= "                  AND usuario_apolice_tb.cpf = usuario_apolice_web_tb.CPF "
        SQL &= "                  AND usuario_apolice_tb.ind_acesso = '4') AS ind_situacao "
        SQL &= "         FROM usuario_apolice_web_tb "
        SQL &= "        WHERE apolice_id = " & apolice_id & " "
        SQL &= "          AND ramo_id = " & ramo_id & ") AS t1 "
        SQL &= "  WHERE ind_situacao = 'A' "

    End Sub

    Public Sub Email_Envio(ByVal secao As String, ByVal iAmbiente_id As Integer)

        SQL = "SELECT valor "
        SQL &= "FROM [SISAB003].controle_sistema_db.dbo.parametro_tb WITH (NOLOCK) "
        SQL &= "WHERE secao = '" & secao & "' "
        SQL &= "  AND ambiente_id = " & iAmbiente_id

    End Sub
    Public Sub Busca_Dados_Segurado(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo As String)

        SQL = "SELECT sbga.nome nome_subgrupo, "
        SQL &= "      c.nome estipulante "
        SQL &= " FROM [SISAB003].seguros_db.dbo.sub_grupo_apolice_tb sbga WITH (NOLOCK) "
        SQL &= " JOIN [SISAB003].seguros_db.dbo.apolice_tb a WITH (NOLOCK) "
        SQL &= "   ON sbga.apolice_id = a.apolice_id "
        SQL &= "  AND sbga.ramo_id = a.ramo_id "
        SQL &= " JOIN [SISAB003].seguros_db.dbo.proposta_tb p WITH (NOLOCK) "
        SQL &= "   ON a.proposta_id = p.proposta_id "
        SQL &= " JOIN [SISAB003].seguros_db.dbo.cliente_tb c WITH (NOLOCK) "
        SQL &= "   ON p.prop_cliente_id = c.cliente_id "
        SQL &= "WHERE sbga.apolice_id = " & apolice_id
        SQL &= "  AND sbga.ramo_id = " & ramo_id
        SQL &= "  AND sbga.sub_grupo_id = " & subgrupo

    End Sub
End Class

