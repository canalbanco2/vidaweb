﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="relacao_pendentes.aspx.vb" Inherits="SEGW0202.relacao_pendentes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Relação de documentos pendentes de análise</title>
    <meta content="JavaScript" name="vs_defaultClientScript">
    <link href="CSS/EstiloBase.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:Panel runat="server" ID="pnltopo">
        <table background="images/imgBannerTopoFundo.gif" border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
                <td align="right" height="60" style="background-position: left top; background-image: url(images/imgBannerTopo.gif);
                    background-repeat: no-repeat" valign="top">
                </td>
            </tr>
        </table>
        <ajax:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" />
        <table background="images/imgFundoMenuTopo.gif" border="0" cellpadding="0" cellspacing="0"
            height="26" width="100%">
            <tr>
                <td>
                    <a class="TituloMenuTopo" href="JavaScript:window.parent.close();">&nbsp; &nbsp; &nbsp;Sair
                        &nbsp; &nbsp;&nbsp;</a> &nbsp;</td>
                <td class="DadosSistemaUsuario" style="width: 250px; text-align: right">
                    <asp:Label ID="lblDataCorrente" runat="server"></asp:Label>&nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>        
    <asp:Panel runat="server"  ID="pnlSegurado" Width="1372px">
            <asp:Label ID="lblSubGrupo" runat="server" Text="SubGrupo" Width="972px" Font-Bold="True" CssClass="CaminhoTela" Height="20px"></asp:Label><br />
            <br />
                <asp:Label ID="lblFuncao" runat="server" Text="Função: Relaçao de DPS para análise" Width="444px" CssClass="CaminhoTela" Height="18px"></asp:Label><br />
                    <br />
                    <asp:Label ID="lblVigencia" runat="server" CssClass="CaminhoTela" Font-Bold="True" Text="Vigencia" Width="810px" Height="20px"></asp:Label>&nbsp;<br />
            <br />
            <br />
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp;<asp:Label ID="lblSemDps" runat="server" CssClass="CaminhoTela" Text=""></asp:Label>
        &nbsp;&nbsp;&nbsp;<br />
    </asp:Panel>
        <br />
        <asp:datagrid ID="grdListaDpsPendente" CssClass="escondeTituloGridTable" runat="server"  CellPadding="0" 
														AutoGenerateColumns="False" ShowHeader="True" PageSize="20" AllowPaging="True" BorderColor="#DDDDDD">
														<FooterStyle CssClass="0019GridFooter"></FooterStyle>
														<SelectedItemStyle CssClass="ponteiro"></SelectedItemStyle>
														<AlternatingItemStyle CssClass="ponteiro"></AlternatingItemStyle>
														<ItemStyle CssClass="ponteiro"></ItemStyle>
														<HeaderStyle Width="600"  Wrap="False" Height="10px" borderColor="#cccccc" BackColor="#003399" Font-Bold="true" ForeColor="white"></HeaderStyle>
            <Columns>
                   <asp:BoundColumn DataField="apolice_id" HeaderText="Apólice">
                        <HeaderStyle HorizontalAlign="Center" /> 
					    <ItemStyle Width="100" HorizontalAlign="Left"/>
				   </asp:BoundColumn>
                   <asp:BoundColumn DataField="subgrupo_id" HeaderText="Nº Subgrupo" Visible="true">
                        <HeaderStyle HorizontalAlign="Center" /> 
					    <ItemStyle Width="80" HorizontalAlign="Center"/>
				   </asp:BoundColumn>
                   <asp:BoundColumn DataField="nome_subgrupo" HeaderText="Nome Subgrupo" Visible="true">
                        <HeaderStyle HorizontalAlign="Center" /> 
					    <ItemStyle Width="300" HorizontalAlign="Left"/>
				   </asp:BoundColumn>				   
                   <asp:BoundColumn DataField="nome_segurado" HeaderText="Segurado" Visible="true">
                        <HeaderStyle HorizontalAlign="Center" /> 
					    <ItemStyle Width="300" HorizontalAlign="Left"/>
				   </asp:BoundColumn>
				   <asp:BoundColumn DataField="descricao" HeaderText="Situação" Visible="true">
                        <HeaderStyle HorizontalAlign="Center" /> 
					    <ItemStyle Width="200" HorizontalAlign="Left"/>
				   </asp:BoundColumn>
                   <asp:BoundColumn DataField="dt_inicio_vigencia" HeaderText="Início Vig." DataFormatString="{0:dd/MM/yyyy}" Visible="true">
                        <HeaderStyle HorizontalAlign="Center" /> 
					    <ItemStyle Width="100" HorizontalAlign="Center"/>
				   </asp:BoundColumn>
                   <asp:BoundColumn DataField="dt_fim_vigencia" HeaderText="Fim Vig." DataFormatString="{0:dd/MM/yyyy}" Visible="true">
                        <HeaderStyle HorizontalAlign="Center" /> 
					    <ItemStyle Width="100" HorizontalAlign="Center"/>
				   </asp:BoundColumn>
                   <asp:BoundColumn DataField="dt_upload" HeaderText="Data Envio" DataFormatString="{0:dd/MM/yyyy}" Visible="true">
                        <HeaderStyle HorizontalAlign="Center" /> 
					    <ItemStyle Width="100" HorizontalAlign="Center"/>
				   </asp:BoundColumn>
                   <asp:BoundColumn DataField="num_agencia" HeaderText="Nº Agência" Visible="true">
                        <HeaderStyle HorizontalAlign="Center" /> 
					    <ItemStyle Width="50" HorizontalAlign="Left"/>
				   </asp:BoundColumn>
                   <asp:BoundColumn DataField="nome_agencia" HeaderText="Nome Agência" Visible="true">
                        <HeaderStyle HorizontalAlign="Center" /> 
					    <ItemStyle Width="150" HorizontalAlign="Left"/>
				   </asp:BoundColumn>
                   <asp:BoundColumn DataField="ddd" HeaderText="DDD" Visible="true">
                        <HeaderStyle HorizontalAlign="Center" /> 
					    <ItemStyle Width="50" HorizontalAlign="Left"/>
				   </asp:BoundColumn>
                   <asp:BoundColumn DataField="telefone" HeaderText="Telefone" Visible="true">
                        <HeaderStyle HorizontalAlign="Center" /> 
					    <ItemStyle Width="100" HorizontalAlign="Left"/>
				   </asp:BoundColumn>
                   <asp:BoundColumn DataField="cpf" HeaderText="CNPJ" Visible="true">
                        <HeaderStyle HorizontalAlign="Center" /> 
					    <ItemStyle Width="150" HorizontalAlign="Left"/>
				   </asp:BoundColumn>
                   <asp:BoundColumn DataField="arquivos_upload_id" HeaderText="" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" /> 
					    <ItemStyle Width="50" HorizontalAlign="Left"/>
				   </asp:BoundColumn>
                   <asp:BoundColumn DataField="controle_documento_id" HeaderText="" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" /> 
					    <ItemStyle Width="50" HorizontalAlign="Left"/>
				   </asp:BoundColumn>      
                   <asp:TemplateColumn>
                      <ItemTemplate>
                        <asp:HyperLink Width="60"  ID="hplVisualiza_Documento" runat="server"></asp:HyperLink>
                      </ItemTemplate>
                   </asp:TemplateColumn> 
            </Columns>
        </asp:datagrid>
        <%--<asp:Button ID="btnExportarExcel" runat="server" Text="Gerar excel" CssClass="Botao_DPS_1" />--%>
    </div>
    </form>

    <script>
      top.escondeaguarde();
    </script>

</body>
</html>
