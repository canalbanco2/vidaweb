﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="visualiza_documento_analise_medico.aspx.vb" Inherits="SEGW0202.visualiza_documento_analise_medico" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Visulização de documento para análise</title>
    <link href="CSS/EstiloBase.css" type="text/css" rel="stylesheet" />
<script>
    function Valida_form(){
        if (document.getElementById('ChkExigencia').checked == true) {
            if (document.getElementById('TxtExigenciaMedica').value.length < 10){
                alert("Necessário preencher a exigência solicitada!");
                return false;
            }
        }
        if (document.getElementById('ChkRejeitar').checked == true) {
            if (document.getElementById('TxtExigenciaMedica').value.length < 10){
                alert("Necessário preencher o motivo da recusa!");
                return false;
            }
        }
        if (document.getElementById('TxtExigenciaMedica').value.length > 255){
            alert("Motivo ultrapassou o limite de 255 caracteres!");
            return false;
        }
        return true;
    }
    
    function Valida_CheckLiberar(){
        if (document.getElementById('ChkLiberarDps').checked == true) {
           document.getElementById('ChkExigencia').checked = false;
           document.getElementById('ChkRejeitar').checked = false;
        }
    }
     
    function Valida_CheckExigencia(){   
        if (document.getElementById('ChkExigencia').checked == true) {
            document.getElementById('ChkLiberarDps').checked = false;
            document.getElementById('ChkRejeitar').checked = false;
        }
    }    
    
    function Valida_CheckRejeitar(){    
        if (document.getElementById('ChkRejeitar').checked == true) {
            document.getElementById('ChkExigencia').checked = false;
            document.getElementById('ChkLiberarDps').checked = false;
        }       
    }
    function fechajanela(){
            window.open('', '_self', '');
            window.close(); 
    }
</script>
</head>
<body onunload="opener.location.reload();">
    <form id="form1" runat="server" onsubmit="return Valida_form();">
        <div><br /><br />
           <table style="width: 800px">
             <tr>
                <td style="width: 480px; height: 602px">
                    <asp:Panel ID="PanelImg" runat="server" Height="590px" Width="480px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="topo-elemento">
                        <img id="Img" alt="" src=""  runat="server" />
                    </asp:Panel>
                    <asp:Panel ID="PanelPdf" runat="server" Height="590px" Width="480px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="topo-elemento">
                        <iframe src="" id="ifrMostrarPdf" width="480px" height="590px" frameborder="0"></iframe>
                    </asp:Panel>
                </td>
                <td valign="top" style="width: 300px; height: 602px;" >
                    <br /> 
                    <asp:Panel ID="PanelMedicoEmAnaliseMedica" runat="server" Height="580px" Width="300px" Wrap="False" BorderStyle="none" ScrollBars="Auto">
                        <table class="tabelaMenu">
                            <tr>
                                <td style="height: 50px">
                                    &nbsp;<asp:Label ID="Label2" runat="server" CssClass="Destaque" Text="Opções de análise de DPS para a área médica"
                                        Width="290px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 50px" align="left">
                                    &nbsp;
                                    <asp:CheckBox ID="ChkLiberarDps" runat="server" CssClass="Check_Opcao" Text="Liberar DPS" AutoPostBack="false" />
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 49px">
                                    &nbsp;
                                    <asp:CheckBox ID="ChkExigencia" runat="server" CssClass="Check_Opcao" Text="Exigência médica" AutoPostBack="false" />
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 118px" align="left">
                                    &nbsp;
                                    <asp:TextBox ID="TxtExigenciaMedica" runat="server" Height="106px" Width="275px" TextMode="MultiLine" />
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 50px">
                                    &nbsp;
                                    <asp:CheckBox ID="ChkRejeitar" runat="server" CssClass="Check_Opcao" Text="Rejeitar DPS" AutoPostBack="false" />
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 50px">
                                    &nbsp;
                                    <asp:Button ID="BtnEnviar" runat="server" CssClass="Botao_DPS_1" Text="Enviar a seguradora" />
                                </td>
                            </tr>
                        </table> 
                    </asp:Panel>
                </td>
             </tr>
             <tr>
                <td style="width: 480px; height: 25px">
                    &nbsp;
                </td>
                <td style="width: 300px; height: 25px;"> 
                    &nbsp; 
                </td>
             </tr>   
             <tr>
                <td style="width: 480px; height: 602px">
                    <asp:Panel ID="PanelImg2" runat="server" Height="590px" Width="480px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="topo-2-elemento">
                        <img id="Img2" alt="" src=""  runat="server" />
                    </asp:Panel>
                    <asp:Panel ID="PanelPdf2" runat="server" Height="590px" Width="480px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="topo-2-elemento">
                        <iframe src="" id="ifrMostrarPdf2" width="480px" height="590px" frameborder="0"></iframe>
                    </asp:Panel>
                </td>
                <td style="width: 300px; height: 602px;"> 
                    &nbsp; 
                </td>
             </tr>   
        </table>
        <br />
        &nbsp;
    </div>
    </form>
</body>
</html>
