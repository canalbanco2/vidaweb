﻿Partial Public Class visualiza_documento_exigencia_medico
    Inherits System.Web.UI.Page
    'Implementação do controle de ambiente
    Dim cAmbiente As New Alianca.Seguranca.Web.ControleAmbiente
    Dim sNome_Servidor As String
    Dim Apolice As String
    Dim Ramo As String
    Dim Subgrupo As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim caminho_arquivo As String
        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
        cAmbiente.ObterAmbiente(url)
        sNome_Servidor = Request.ServerVariables("SERVER_NAME")

        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção)
        Else '
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção)
        End If

        caminho_arquivo = getCaminhoArquivo(Session("Controle_Documento_Id"), Session("Arquivo_Upload_Id"))
        'caminho_arquivo = "./images/_laudomedico_6246711.pdf"

        PanelImg.Visible = False
        PanelPdf.Visible = False
        PanelImg2.Visible = False
        PanelPdf2.Visible = False

        Apolice = Request.QueryString("ApoliceId")
        Ramo = Request.QueryString("RamoId")
        Subgrupo = Request.QueryString("SubgrupoId")

        If System.IO.Path.GetExtension(caminho_arquivo).ToLower = ".pdf" Then
            PanelPdf.Visible = True
            cUtilitarios.escreveScript2("window.onload = function(){document.getElementById('ifrMostrarPdf').src = '" & caminho_arquivo & "';}")
        Else
            PanelImg.Visible = True
            Img.Src = caminho_arquivo
        End If

        If Session("Arquivo_UploadId2") <> 0 Then
            caminho_arquivo = getCaminhoArquivo(Session("Controle_Documento_Id"), Session("Arquivo_UploadId2"))
            'caminho_arquivo = "./images/laudomedico_6246711.jpg"
            If System.IO.Path.GetExtension(caminho_arquivo).ToLower = ".pdf" Then
                PanelPdf2.Visible = True
                cUtilitarios.escreveScript2("window.onload = function(){document.getElementById('ifrMostrarPdf2').src = '" & caminho_arquivo & "';}")
            Else
                PanelImg2.Visible = True
                Img2.Src = caminho_arquivo
            End If

        End If

        TxtExigenciaMedica.Text = getMotivo(Session("Controle_Documento_Id"), Session("Arquivo_UploadId2"))
        TxtExigenciaMedica.Enabled = False

        ChkLiberarDps.Attributes.Add("onclick", "Valida_CheckLiberar();")
        ChkExigencia.Attributes.Add("onclick", "Valida_CheckExigencia();")
        ChkRejeitar.Attributes.Add("onclick", "Valida_CheckRejeitar();")

    End Sub

    Protected Function getCaminhoArquivo(ByVal Controle_DocumentoId As Integer, ByVal Arquivo_UploadId As Integer) As String

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.Busca_Caminho_Arquivo(Controle_DocumentoId, Arquivo_UploadId)

        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
        dr.Read()

        Dim sCaminho As String
        'sCaminho = "file:" & dr.GetValue(0).ToString.Replace("\", "/")
        sCaminho = dr.GetValue(0).ToString

        dr.Close()
        dr = Nothing

        Return sCaminho

    End Function

    Protected Function getMotivo(ByVal Controle_DocumentoId As Integer, ByVal Arquivo_UploadId As Integer) As String

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.Busca_Motivo(Controle_DocumentoId, Arquivo_UploadId)

        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
        dr.Read()

        Dim sMotivo As String
        sMotivo = dr.GetValue(0).ToString
        dr.Close()
        dr = Nothing

        Return sMotivo

    End Function

    Protected Sub BtnEnviar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnEnviar.Click

        If ((ChkLiberarDps.Checked = False) _
        And (ChkRejeitar.Checked = False) _
        And (ChkExigencia.Checked = False)) Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Necessário definir uma operação para o arquivo,\nEscolha uma das opções!');", True)
        Else
            Dim Status_arquivo As Integer

            If ChkLiberarDps.Checked = True Then
                Status_arquivo = 4
            ElseIf ChkRejeitar.Checked = True Then
                Status_arquivo = 5
            Else
                Status_arquivo = 6
            End If

            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
            bd.Atualiza_Documento(Session("Controle_Documento_Id"), Status_arquivo, Session("usuario"))
            bd.ExecutaSQL()

            bd.Atualiza_Nome(Session("Controle_Documento_Id"), Session("Arquivo_Upload_Id"), getNovo_Nome, Session("usuario"))
            bd.ExecutaSQL()

            If ((ChkExigencia.Checked = True) _
            Or (ChkRejeitar.Checked = True)) Then
                bd.Atualiza_Motivo(Session("Controle_Documento_Id"), Session("Arquivo_Upload_Id"), TxtExigenciaMedicaNova.Text, Session("usuario"))
                bd.ExecutaSQL()
            End If

            If ((ChkExigencia.Checked = True) _
            Or (ChkRejeitar.Checked = True)) Then
                Enviar_Email()
            End If

            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Situação do documento atualizada com sucesso!');", True)
            Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "fechajanela();", True)
        End If

    End Sub
    Private Function getNovo_Nome() As String
        Dim Novo_Nome As String

        If ChkLiberarDps.Checked = True Then
            If Session("Tipo_Arquivo") = "1" Then
                Novo_Nome = "DPS_APROV_" & Now.Day.ToString.PadLeft(2, "0") & Now.Month.ToString.PadLeft(2, "0") & Now.Year
            Else
                Novo_Nome = "LAUDO_APROV_" & Now.Day.ToString.PadLeft(2, "0") & Now.Month.ToString.PadLeft(2, "0") & Now.Year
            End If
        ElseIf ChkRejeitar.Checked = True Then
            If Session("Tipo_Arquivo") = "1" Then
                Novo_Nome = "DPS_REJ_" & Now.Day.ToString.PadLeft(2, "0") & Now.Month.ToString.PadLeft(2, "0") & Now.Year
            Else
                Novo_Nome = "LAUDO_REJ_" & Now.Day.ToString.PadLeft(2, "0") & Now.Month.ToString.PadLeft(2, "0") & Now.Year
            End If
        Else
            If Session("Tipo_Arquivo") = "1" Then
                Novo_Nome = "DPS_EXIG_" & Now.Day.ToString.PadLeft(2, "0") & Now.Month.ToString.PadLeft(2, "0") & Now.Year
            Else
                Novo_Nome = "LAUDO_EXIG_" & Now.Day.ToString.PadLeft(2, "0") & Now.Month.ToString.PadLeft(2, "0") & Now.Year
            End If
        End If

        Return Novo_Nome

    End Function
    Private Sub Enviar_Email()

        Dim corpo_email As String

        If ChkRejeitar.Checked = True Then
            corpo_email = "<b>Prezado(s),</b><br /><br />Informamos por meio deste que o arquivo enviado foi recusado.<br />" & _
                      "Apólice - Ramo - Subgrupo: " & Apolice & " - " & Ramo & " - " & Subgrupo & _
                      "<br /><br />" & "Motivo: " & TxtExigenciaMedicaNova.Text & ".<br /><br />Para acessá-los entre na área restrita no link abaixo:<br />" & _
                      "<a herf=''https://" & sNome_Servidor & "/ITE/ITEW0104'' target=_blank>https://" & sNome_Servidor & "/ITE/ITEW0104</a>" & _
                      "<br /><br />Atenciosamente,<br /><br />Aliança do Brasil"
        Else
            corpo_email = "<b>Prezado(s),</b><br /><br />Informamos por meio deste que foi feita uma Exigência Médica para o DPS enviado.<br />" & _
                      "Apólice - Ramo - Subgrupo: " & Apolice & " - " & Ramo & " - " & Subgrupo & _
                      "<br /><br />" & "Motivo: " & TxtExigenciaMedicaNova.Text & ".<br /><br />Para acessá-los entre na área restrita no link abaixo:<br />" & _
                      "<a herf=''https://" & sNome_Servidor & "/ITE/ITEW0104'' target=_blank>https://" & sNome_Servidor & "/ITE/ITEW0104</a>" & _
                      "<br /><br />Atenciosamente,<br /><br />Aliança do Brasil"
        End If

        Dim destino_email As String
        destino_email = getEmails_Enviar()
        'destino_email = "c_jtioma@bbmapfre.com.br"

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.Envio_Email(Session("usuario"), corpo_email, destino_email)

        bd.ExecutaSQL()

    End Sub

    Private Function getEmails_Enviar() As String
        Dim MailTo As String
        Dim cont As Integer

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.BuscaEmail(Apolice, Ramo, Subgrupo)

        cont = 0
        MailTo = ""
        Try
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.HasRows Then
                While dr.Read()
                    If cont > 0 Then
                        MailTo &= "; " & dr.GetValue(0).ToString()
                    Else
                        MailTo &= dr.GetValue(0).ToString()
                    End If
                    cont += 1
                End While
            End If
            dr.Close()
            dr = Nothing

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return MailTo
    End Function

End Class