<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="visualiza_documento_exigencia_tecnico.aspx.vb" Inherits="SEGW0202.visualiza_documento_exigencia_tecnico" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Visuliza��o de documento</title>
    <link href="CSS/EstiloBase.css" type="text/css" rel="stylesheet" />
<script>
    function Valida_form(){
        if (document.getElementById('TxtExigenciaMedica').value.length < 10){
            alert("Necess�rio preencher a exig�ncia solicitada!");
            return false;
        }
        return true;
    }
    
    function fechajanela(){
            window.open('', '_self', '');
            window.close(); 
    }
</script>
</head>
<body onunload="opener.location.reload();">
    <form id="form1" runat="server" onsubmit="return Valida_form();">
        <div><br /><br />
           <table style="width: 800px">
             <tr>
                <td style="width: 480px; height: 602px">
                    <asp:Panel ID="PanelImg" runat="server" Height="590px" Width="480px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="topo-elemento">
                        <img id="Img" alt="" src=""  runat="server" />
                    </asp:Panel>
                    <asp:Panel ID="PanelPdf" runat="server" Height="590px" Width="480px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="topo-elemento">
                        <iframe src="" id="ifrMostrarPdf" width="480px" height="590px" frameborder="0"></iframe>
                    </asp:Panel>
                </td>
                <td valign="top" style="width: 300px; height: 602px;" >
                    <br /> 
                    <asp:Panel ID="PanelMedicoEmExigenciaMedica" runat="server" Height="580px" Width="320px" Wrap="False" BorderStyle="None" ScrollBars="Auto">
                        <table class="tabelaMenu">
                            <tr>
                                <td style="height: 50px; width: 303px;">
                                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Label1" runat="server" CssClass="Destaque" Text="Exig�ncia m�dica"
                                        Width="250px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 118px; width: 303px;" align="center">
                                    &nbsp;
                                    <asp:TextBox ID="TxtExigenciaMedica" runat="server" Height="106px" Width="260px" TextMode="MultiLine" />
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 50px; width: 303px;">
                                    &nbsp;
                                    <asp:Button ID="BtnEnviar" runat="server" CssClass="Botao_DPS_1" Text="Enviar email" />
                                </td>
                            </tr>
                        </table> 
                    </asp:Panel>
                </td>
             </tr>
             <tr>
                <td style="width: 480px; height: 25px">
                    &nbsp;
                </td>
                <td style="width: 300px; height: 25px;"> 
                    &nbsp; 
                </td>
             </tr>   
             <tr>
                <td style="width: 480px; height: 602px">
                    <asp:Panel ID="PanelImg2" runat="server" Height="590px" Width="480px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="topo-2-elemento">
                        <img id="Img2" alt="" src=""  runat="server" />
                    </asp:Panel>
                    <asp:Panel ID="PanelPdf2" runat="server" Height="590px" Width="480px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="topo-2-elemento">
                        <iframe src="" id="ifrMostrarPdf2" width="480px" height="590px" frameborder="0"></iframe>
                    </asp:Panel>
                </td>
                <td style="width: 300px; height: 602px;"> 
                    &nbsp; 
                </td>
             </tr>   
        </table>
        <br />
        &nbsp;
    </div>
    </form>
</body>
</html>
