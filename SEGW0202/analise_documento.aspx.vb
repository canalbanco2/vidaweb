﻿Public Partial Class analise_documento
    Inherits System.Web.UI.Page

    Dim cAmbiente As New Alianca.Seguranca.Web.ControleAmbiente

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim url As String
        Dim vAmbiente_Id As Integer

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"

        vAmbiente_Id = cAmbiente.ObterAmbiente(url)

        Dim linkseguro As New Alianca.Seguranca.Web.LinkSeguro

        Dim usuario As String = linkseguro.LerUsuario("", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))

        Session("usuario_id") = linkseguro.Usuario_ID

        Session("usuario") = linkseguro.Login_WEB

        Session("cpf") = linkseguro.CPF

        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
        Else '
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
        End If

        Session("Controle_Documento_Id") = Request.QueryString("Controle_DocumentoId")
        Session("Arquivo_Upload_Id") = Request.QueryString("Arquivo_UploadId")

        Dim sTipoStatus_Arquivo As String() = getTipoArquivo(Session("Controle_Documento_Id"), Session("Arquivo_Upload_Id")).Split(",")
        Dim sStatus_Arquivo As String
        Session("Tipo_Arquivo") = sTipoStatus_Arquivo(0)
        sStatus_Arquivo = sTipoStatus_Arquivo(1)

        Dim ApoliceRamoSubgrupo As String

        ApoliceRamoSubgrupo = "&ApoliceId=" & Request.QueryString("ApoliceId") & "&RamoId=" & Request.QueryString("RamoId") & "&SubgrupoId=" & Request.QueryString("SubgrupoId")

        If Session("Tipo_Arquivo") = "2" Then
            Session("Arquivo_UploadId2") = getUltimo_Dps_Laudo(Session("Controle_Documento_Id"))
        Else
            Session("Arquivo_UploadId2") = 0
        End If

        Dim sTipo_Nivel_acesso As String() = getAcesso_Nivel(Session("Controle_Documento_Id"), Session("Arquivo_Upload_Id"), Session("cpf"), vAmbiente_Id).Split(",")
        Dim sTipo_Acesso As String
        Dim sNivel_Acesso As String
        sTipo_Acesso = sTipo_Nivel_acesso(0)
        sNivel_Acesso = sTipo_Nivel_acesso(1)

        'sTipo_Acesso = 1 Administrador Aliança
        'sTipo_Acesso = 2 Administrador apolice
        'sTipo_Acesso = 3 Administrador subgrupo
        'sTipo_Acesso = 4 Operador (Técnico)
        If ((sTipo_Acesso = 1) Or (sTipo_Acesso = 2) Or (sTipo_Acesso = 3) Or (sTipo_Acesso = 4) Or (Request.QueryString("SoMedico") = "S") Or (Request.QueryString("SoMedico") = "N")) Then
            'If ((sTipo_Acesso = 1) Or (sTipo_Acesso = 2) Or (sTipo_Acesso = 3) Or (Request.QueryString("SoMedico") = "S") Or (Request.QueryString("SoMedico") = "N")) Then
            If ((sStatus_Arquivo = 2) Or (sStatus_Arquivo = 3)) Then
                If ((Request.QueryString("SoMedico") = "S") And (sStatus_Arquivo = 3)) Then 'Nivel de acesso Medico
                    If Session("Tipo_Arquivo") = "1" Then ' Tipo do arquivo DPS
                        Response.Redirect("visualiza_documento_analise_medico.aspx?SLinkSeguro=" & Request("SLinkSeguro") & ApoliceRamoSubgrupo)
                    Else ' Tipo do arquivo laudo
                        Response.Redirect("visualiza_documento_exigencia_medico.aspx?SLinkSeguro=" & Request("SLinkSeguro") & ApoliceRamoSubgrupo)
                    End If

                ElseIf ((Request.QueryString("SoMedico") = "N") And (sStatus_Arquivo = 2)) Then 'Nivel de acesso Tecnico
                    'ElseIf ((sTipo_Acesso = 4) And (sStatus_Arquivo = 2)) Then 'Nivel de acesso Tecnico
                    If Session("Tipo_Arquivo") = "1" Then ' Tipo do arquivo DPS
                        Response.Redirect("visualiza_documento_analise_tecnico.aspx?SLinkSeguro=" & Request("SLinkSeguro") & ApoliceRamoSubgrupo)
                    Else ' Tipo do arquivo laudo
                        Response.Redirect("visualiza_documento_exigencia_tecnico.aspx?SLinkSeguro=" & Request("SLinkSeguro") & ApoliceRamoSubgrupo)
                    End If
                Else
                    Response.Redirect("visualiza_documento.aspx?SLinkSeguro=" & Request("SLinkSeguro") & "&status_arquivo=" & sStatus_Arquivo)
                End If
            Else
                Response.Redirect("visualiza_documento.aspx?SLinkSeguro=" & Request("SLinkSeguro") & "&status_arquivo=" & sStatus_Arquivo)
            End If
        Else
            Response.Redirect("visualiza_documento.aspx?SLinkSeguro=" & Request("SLinkSeguro") & "&status_arquivo=" & sStatus_Arquivo)
        End If

    End Sub

    Protected Function getTipoArquivo(ByVal Controle_DocumentoId As Integer, ByVal Arquivo_UploadId As Integer) As String

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.Busca_Status_Tipo_Arquivo(Controle_DocumentoId, Arquivo_UploadId)

        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
        dr.Read()

        Dim sTipoArquivoId As String
        Dim sStatusArquivoId As String
        sTipoArquivoId = dr.GetValue(0).ToString
        sStatusArquivoId = dr.GetValue(1).ToString

        Return sTipoArquivoId & "," & sStatusArquivoId

    End Function

    Protected Function getAcesso_Nivel(ByVal Controle_DocumentoId As Integer, ByVal Arquivo_UploadId As Integer, ByVal cpf As String, ByVal Ambiente_id As Integer) As String

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.Busca_Acesso_Perfil(Controle_DocumentoId, Arquivo_UploadId, Cpf, Ambiente_id)

        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

        Dim sInd_Acesso As String
        Dim sNivel_Acesso_id As String
        sInd_Acesso = "0"
        sNivel_Acesso_id = "0"

        If dr.HasRows Then
            dr.Read()
            sInd_Acesso = dr.GetValue(0).ToString
            sNivel_Acesso_id = dr.GetValue(1).ToString
        End If

        dr.Close()
        dr = Nothing

        Return sInd_Acesso & "," & sNivel_Acesso_id

    End Function

    Protected Function getUltimo_Dps_Laudo(ByVal Controle_DocumentoId As Integer) As Integer

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.Busca_Ultimo_DPS(Controle_DocumentoId)

        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
        dr.Read()

        Dim id_arquivo_upload As Integer
        id_arquivo_upload = dr.GetValue(0)

        Return id_arquivo_upload

    End Function

End Class