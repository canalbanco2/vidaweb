﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="visualiza_documento.aspx.vb" Inherits="SEGW0202.visualiza_documento" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Visulização de documento</title>
    <link href="CSS/EstiloBase.css" type="text/css" rel="stylesheet" />

</head>
<body>
    <form id="form1" runat="server">
        <div><br /><br />
           <table style="width: 800px">
             <tr>
                <td style="width: 480px; height: 602px">
                    <asp:Panel ID="PanelImg" runat="server" Height="590px" Width="480px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="topo-elemento">
                        <img id="Img" alt="" src=""  runat="server" />
                    </asp:Panel>
                    <asp:Panel ID="PanelPdf" runat="server" Height="590px" Width="480px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="topo-elemento">
                        <iframe src="" id="ifrMostrarPdf" width="480px" height="590px" frameborder="0"></iframe>
                    </asp:Panel>
                </td>
                <td valign="top" style="width: 300px; height: 602px;" >
                    <br /> 
                    <asp:Panel ID="PanelExigenciaMedica" runat="server" Height="580px" Width="300px" Wrap="False" BorderStyle="none" ScrollBars="Auto">
                        <table class="tabelaMenu">
                            <tr>
                                <td style="height: 50px">
                                    &nbsp;<asp:Label ID="lblMensagem" runat="server" CssClass="Destaque" Text="Exigência médica"
                                        Width="250px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 118px" align="left">
                                    &nbsp;
                                    <asp:TextBox ID="TxtExigenciaMedica" runat="server" Height="106px" Width="275px" Enabled="False" TextMode="MultiLine" />
                                </td>
                            </tr>
                        </table> 
                    </asp:Panel>
                </td>
             </tr>   
             <tr>
                <td style="width: 480px; height: 602px">
                    <asp:Panel ID="PanelImg2" runat="server" Height="590px" Width="480px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="topo-2-elemento">
                        <img id="Img2" alt="" src=""  runat="server" />
                    </asp:Panel>
                    <asp:Panel ID="PanelPdf2" runat="server" Height="590px" Width="480px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="topo-2-elemento">
                        <iframe src="" id="ifrMostrarPdf2" width="480px" height="590px" frameborder="0"></iframe>
                    </asp:Panel>
                </td>
                <td>
                    &nbsp;
                </td>
             </tr>   
        </table>
        <br />
        &nbsp;
    </div>
    </form>
</body>
</html>
