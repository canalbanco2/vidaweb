<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="visualiza_documento_analise_tecnico.aspx.vb" Inherits="SEGW0202.visualiza_documento_analise_tecnico" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Visuliza��o de documento para an�lise</title>
    <link href="CSS/EstiloBase.css" type="text/css" rel="stylesheet" />

<script>
    function Valida_CheckLiberar(){
        if (document.getElementById('ChkLiberarDps').checked == true) {
           document.getElementById('chkEnviarAnalise').checked = false;
           document.getElementById('ChkRejeitar').checked = false;
        }
    }
     
    function Valida_CheckAnalise(){   
        if (document.getElementById('chkEnviarAnalise').checked == true) {
            document.getElementById('ChkLiberarDps').checked = false;
            document.getElementById('ChkRejeitar').checked = false;
        }
    }    
    
    function Valida_CheckRejeitar(){    
        if (document.getElementById('ChkRejeitar').checked == true) {
            document.getElementById('chkEnviarAnalise').checked = false;
            document.getElementById('ChkLiberarDps').checked = false;
        }       
    }
    function fechajanela(){
            window.open('', '_self', '');
            window.close(); 
    }
</script>
</head>
<body onunload="opener.location.reload();">
    <form id="form1" runat="server">
        <div><br /><br />
           <table style="width: 800px">
             <tr>
                <td style="width: 480px; height: 602px">
                    <asp:Panel ID="PanelImg" runat="server" Height="590px" Width="480px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="topo-elemento">
                        <img id="Img" alt="" src=""  runat="server" />
                    </asp:Panel>
                    <asp:Panel ID="PanelPdf" runat="server" Height="590px" Width="480px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="topo-elemento">
                        <iframe src="" id="ifrMostrarPdf" width="480px" height="590px" frameborder="0"></iframe>
                    </asp:Panel>
                </td>
                <td valign="top" style="width: 288px; height: 602px;" >
                    <br /> 
                    <asp:Panel ID="PanelTecnicoAnaliseDps" runat="server" Height="550px" Width="320px" Wrap="False" BorderStyle="None" ScrollBars="Auto" BorderWidth="0px">
                        <table class="tabelaMenu">
                            <tr>
                                <td style="height: 50px; width: 162px;" align="right">
                                    &nbsp;<asp:Label ID="lblTitulo" runat="server" CssClass="Destaque" Text="Op��es de an�lise de DPS para a �rea t�cnica"
                                        Width="290px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 49px; width: 162px;" align="left">
                                    &nbsp;
                                    <asp:CheckBox ID="chkLiberarDps" runat="server" CssClass="Check_Opcao" Text="Liberar DPS" AutoPostBack="false" Width="195px" />
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 49px; width: 162px;">
                                    &nbsp;
                                    <asp:CheckBox ID="chkEnviarAnalise" runat="server" CssClass="Check_Opcao" Text="Enviar DPS para an�lise" AutoPostBack="false" Width="241px" />
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 50px; width: 162px;">
                                    &nbsp;
                                    <asp:CheckBox ID="chkRejeitar" runat="server" CssClass="Check_Opcao" Text="Rejeitar DPS" AutoPostBack="false" Width="195px" />
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 50px; width: 162px;" align="center">
                                    &nbsp;
                                    <asp:Button ID="btnEnviarDps" runat="server" CssClass="Botao_DPS_1" Text="Enviar a seguradora" />
                                </td>
                            </tr>
                        </table> 
                    </asp:Panel>
                </td>
             </tr>
             <tr>
                <td style="width: 480px; height: 25px">
                    &nbsp;
                </td>
                <td style="width: 288px; height: 25px;"> 
                    &nbsp; 
                </td>
             </tr>   
             <tr>
                <td style="width: 480px; height: 602px">
                    <asp:Panel ID="PanelImg2" runat="server" Height="590px" Width="480px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="topo-2-elemento">
                        <img id="Img2" alt="" src=""  runat="server" />
                    </asp:Panel>
                    <asp:Panel ID="PanelPdf2" runat="server" Height="590px" Width="480px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="topo-2-elemento">
                        <iframe src="" id="ifrMostrarPdf2" width="480px" height="590px" frameborder="0"></iframe>
                    </asp:Panel>
                </td>
                <td style="width: 288px; height: 602px;"> 
                    &nbsp; 
                </td>
             </tr>   
        </table>
        <br />
        &nbsp;
    </div>
    </form>
</body>
</html>
