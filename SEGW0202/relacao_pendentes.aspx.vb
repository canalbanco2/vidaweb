﻿'Imports System.IO
'Imports System.Data
'Imports System.Drawing
'Imports System.Configuration
Partial Public Class relacao_pendentes
    Inherits System.Web.UI.Page
    Dim cAmbiente As New Alianca.Seguranca.Web.ControleAmbiente

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim linkseguro As New Alianca.Seguranca.Web.LinkSeguro

        Dim usuario As String = linkseguro.LerUsuario("", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))

        Session("usuario_id") = linkseguro.Usuario_ID

        Session("usuario") = linkseguro.Login_WEB

        Session("cpf") = linkseguro.CPF

        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
        cAmbiente.ObterAmbiente(url)

        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção)
        Else '
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção)
        End If

        pnlSegurado.Visible = False
        pnltopo.Visible = False

        Session("apolice_0202") = Request.QueryString("apolice")
        Session("ramo_0202") = Request.QueryString("ramo")
        Session("subgrupo_id_0202") = Request.QueryString("subgrupo_id")


        If Not IsPostBack Then
            If Session("apolice_0202") <> 0 Then
                inicialiazaSessionVariaveis()
                lblSubGrupo.Text = "Ramo/Apólice: (" & Session("ramo_0202") & ") " & Session("apolice_0202") & " -> Estipulante: " & Session("estipulante_0202") & "<br>" & "Subgrupo: " & Session("subgrupo_id_0202") & " - " & Session("nomeSubgrupo_0202") & "<br>"
                lblVigencia.Text = "<br>" & getPeriodoCompetenciaSession()
                pnlSegurado.Visible = True
                grdListaDpsPendente.Columns(0).Visible = False
                grdListaDpsPendente.Columns(1).Visible = False
                grdListaDpsPendente.Columns(2).Visible = False
            Else
                If Request.QueryString("SoMedico") = "S" Then
                    lblDataCorrente.Text = Format(Date.Now, "dddd, dd " & "DE" & " MMMM " & "DE" & " yyyy").ToLower
                    pnltopo.Visible = True
                End If
            End If
        End If

        Busca_relacao_doc_pendentes(Session("apolice_0202"), Session("ramo_0202"), Session("subgrupo_id_0202"), Request.QueryString("SoMedico"))

    End Sub

    Private Sub Busca_relacao_doc_pendentes(ByVal Apolice_Id As Long, ByVal Ramo_Id As Integer, ByVal Subgrupo_Id As Integer, ByVal So_Medico As String)

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.Relacao_Documentos_Pendentes(Apolice_Id, Ramo_Id, Subgrupo_Id, So_Medico)

        Try
            Dim dt As Data.DataTable = bd.ExecutaSQL_DT

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then

                Me.grdListaDpsPendente.SelectedIndex = -1
                Me.grdListaDpsPendente.CurrentPageIndex = Session("indice")

                Me.grdListaDpsPendente.DataSource = dt
                Me.grdListaDpsPendente.DataBind()
                Me.grdListaDpsPendente.Visible = True

                Me.lblSemDps.Visible = False
                grdListaDpsPendente.Visible = True

            Else
                Session.Remove("indice")
                Me.lblSemDps.Text = "<br><br><br>Nenhum DPS em aberto encontrado.<br><br>"
                Me.grdListaDpsPendente.Visible = False
                Me.lblSemDps.Visible = True
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub
    Private Function getPeriodoCompetenciaSession() As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5696_SPS(Session("apolice_0202"), Session("ramo_0202"), Session("subgrupo_id_0202"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)
        Dim data_inicio As String = ""
        Dim data_fim As String = ""
        Dim retorno As String = ""

        Try
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()


            If dr.HasRows Then
                While dr.Read()

                    data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                    data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)

                End While
            End If
            dr.Close()
            dr = Nothing
            retorno = "Período de Competência: " & data_inicio & " a " & data_fim

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return retorno
    End Function

    Public Sub inicialiazaSessionVariaveis()

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.Busca_Dados_Segurado(Session("apolice_0202"), Session("ramo_0202"), Session("subgrupo_id_0202"))

        Session("estipulante_0202") = ""
        Session("nomeSubgrupo_0202") = ""

        Try
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
            If dr.HasRows Then
                While dr.Read()
                    Session("nomeSubgrupo_0202") = dr.GetValue(0).ToString()
                    Session("estipulante_0202") = dr.GetValue(1).ToString()
                End While
            End If
            dr.Close()
            dr = Nothing
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub


    Protected Sub grdListaDpsPendente_PageIndexChanged(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grdListaDpsPendente.PageIndexChanged
        grdListaDpsPendente.CurrentPageIndex = e.NewPageIndex
        grdListaDpsPendente.DataBind()
    End Sub

    Private Sub grdListaDpsPendente_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdListaDpsPendente.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim v_oVisualiza As HyperLink = CType(e.Item.FindControl("hplVisualiza_Documento"), HyperLink)
            v_oVisualiza.Text = "Abrir"
            v_oVisualiza.Target = "_blank"
            v_oVisualiza.NavigateUrl = "analise_documento.aspx?SLinkSeguro=" & Request.QueryString("SLinkSeguro") & "&Controle_DocumentoId=" & e.Item.DataItem("controle_documento_id") & "&Arquivo_UploadId=" & e.Item.DataItem("arquivos_upload_id") & "&SoMedico=" & Request.QueryString("SoMedico") & "&ApoliceId=" & e.Item.DataItem("apolice_id") & "&RamoId=" & e.Item.DataItem("ramo_id") & "&SubgrupoId=" & e.Item.DataItem("subgrupo_id")
        End If
    End Sub

    Private Sub exportarExcel()
        'export to excel
        'Response.Clear()
        'Response.Buffer = True
        'Response.Charset = "UTF-8"
        'Response.ContentType = "application/vnd.ms-excel"
        ''Response.ContentEncoding = Encoding.Default
        'Response.AppendHeader("content-disposition", "attachment;filename=Rastreio_" & Now.Day.ToString.PadLeft(2, "0") & Now.Month.ToString.PadLeft(2, "0") & Now.Year & "_" & Now.Hour.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0") & Now.Millisecond.ToString.PadLeft(2, "0") & "_" & ".xls")

        'Me.EnableViewState = False
        'Dim oStringWriter As System.IO.StringWriter = New System.IO.StringWriter()
        'Dim oHtmlTextWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(oStringWriter)
        'grdListaDpsPendente.RenderControl(oHtmlTextWriter)

        'Dim style As String = "<style> .textmode { mso-number-format:\@; } </style>"
        'Response.Write(style)
        'Response.Write(oStringWriter.ToString())
        'Response.End()

    End Sub

    'Private Sub btnExportarExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportarExcel.Click
    '    exportarExcel()
    'End Sub
End Class