﻿Partial Public Class visualiza_documento
    Inherits System.Web.UI.Page
    'Implementação do controle de ambiente
    Dim cAmbiente As New Alianca.Seguranca.Web.ControleAmbiente

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim caminho_arquivo As String

        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
        cAmbiente.ObterAmbiente(url)

        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção)
        Else '
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção)
        End If

        Dim iStatus_arquivo As Integer

        iStatus_arquivo = Request.QueryString("Status_arquivo")

        caminho_arquivo = getCaminhoArquivo(Session("Controle_Documento_Id"), Session("Arquivo_Upload_Id"))
        'caminho_arquivo = "./images/_laudomedico_6246711.pdf"

        PanelImg.Visible = False
        PanelPdf.Visible = False
        PanelImg2.Visible = False
        PanelPdf2.Visible = False

        If System.IO.Path.GetExtension(caminho_arquivo).ToLower = ".pdf" Then
            PanelPdf.Visible = True
            cUtilitarios.escreveScript2("window.onload = function(){document.getElementById('ifrMostrarPdf').src = '" & caminho_arquivo & "';}")
        Else
            PanelImg.Visible = True
            Img.Src = caminho_arquivo
        End If

        If (iStatus_arquivo = 6) Then
            lblMensagem.Text = "Exigência Médica"
        Else
            lblMensagem.Text = "Motivo da Recusa"
        End If

        If Session("Arquivo_UploadId2") <> 0 Then
            caminho_arquivo = getCaminhoArquivo(Session("Controle_Documento_Id"), Session("Arquivo_UploadId2"))
            'caminho_arquivo = "./images/laudomedico_6246711.jpg"
            If System.IO.Path.GetExtension(caminho_arquivo).ToLower = ".pdf" Then
                PanelPdf2.Visible = True
                cUtilitarios.escreveScript2("window.onload = function(){document.getElementById('ifrMostrarPdf2').src = '" & caminho_arquivo & "';}")
            Else
                PanelImg2.Visible = True
                Img2.Src = caminho_arquivo
            End If
        End If

        If ((iStatus_arquivo = 5) Or (iStatus_arquivo = 6)) Then
            PanelExigenciaMedica.Visible = True
            TxtExigenciaMedica.Text = getMotivo(Session("Controle_Documento_Id"), Session("Arquivo_Upload_Id"))
        Else
            PanelExigenciaMedica.Visible = False
        End If

    End Sub

    Protected Function getCaminhoArquivo(ByVal Controle_DocumentoId As Integer, ByVal Arquivo_UploadId As Integer) As String

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.Busca_Caminho_Arquivo(Controle_DocumentoId, Arquivo_UploadId)

        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
        dr.Read()

        Dim sCaminho As String
        'sCaminho = "file:" & dr.GetValue(0).ToString.Replace("\", "/")
        sCaminho = dr.GetValue(0).ToString

        dr.Close()
        dr = Nothing

        Return sCaminho

    End Function

    Protected Function getMotivo(ByVal Controle_DocumentoId As Integer, ByVal Arquivo_UploadId As Integer) As String

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.Busca_Motivo(Controle_DocumentoId, Arquivo_UploadId)

        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
        dr.Read()

        Dim sCaminho As String
        sCaminho = dr.GetValue(0).ToString

        Return sCaminho

    End Function

End Class