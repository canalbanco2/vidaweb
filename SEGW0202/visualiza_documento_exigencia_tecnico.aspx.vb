﻿Partial Public Class visualiza_documento_exigencia_tecnico
    Inherits System.Web.UI.Page
    'Implementação do controle de ambiente
    Dim cAmbiente As New Alianca.Seguranca.Web.ControleAmbiente
    Dim sNome_Servidor As String
    Dim Apolice As String
    Dim Ramo As String
    Dim Subgrupo As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim caminho_arquivo As String

        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
        cAmbiente.ObterAmbiente(url)
        sNome_Servidor = Request.ServerVariables("SERVER_NAME")

        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção)
        Else '
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção)
        End If

        caminho_arquivo = getCaminhoArquivo(Session("Controle_Documento_Id"), Session("Arquivo_Upload_Id"))
        'caminho_arquivo = "./images/_laudomedico_6246711.pdf"

        PanelImg.Visible = False
        PanelPdf.Visible = False
        PanelImg2.Visible = False
        PanelPdf2.Visible = False

        Apolice = Request.QueryString("ApoliceId")
        Ramo = Request.QueryString("RamoId")
        Subgrupo = Request.QueryString("SubgrupoId")

        If System.IO.Path.GetExtension(caminho_arquivo).ToLower = ".pdf" Then
            PanelPdf.Visible = True
            cUtilitarios.escreveScript2("window.onload = function(){document.getElementById('ifrMostrarPdf').src = '" & caminho_arquivo & "';}")
        Else
            PanelImg.Visible = True
            Img.Src = caminho_arquivo
        End If

        If Session("Arquivo_UploadId2") <> 0 Then
            caminho_arquivo = getCaminhoArquivo(Session("Controle_Documento_Id"), Session("Arquivo_UploadId2"))
            'caminho_arquivo = "./images/laudomedico_6246711.jpg"
            If System.IO.Path.GetExtension(caminho_arquivo).ToLower = ".pdf" Then
                PanelPdf2.Visible = True
                cUtilitarios.escreveScript2("window.onload = function(){document.getElementById('ifrMostrarPdf2').src = '" & caminho_arquivo & "';}")
            Else
                PanelImg2.Visible = True
                Img2.Src = caminho_arquivo
            End If
        End If

        TxtExigenciaMedica.Text = getMotivo(Session("Controle_Documento_Id"), Session("Arquivo_Upload_Id"))
        TxtExigenciaMedica.Enabled = False

    End Sub

    Protected Function getCaminhoArquivo(ByVal Controle_DocumentoId As Integer, ByVal Arquivo_UploadId As Integer) As String

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.Busca_Caminho_Arquivo(Controle_DocumentoId, Arquivo_UploadId)

        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
        dr.Read()

        Dim sCaminho As String
        'sCaminho = "file:" & dr.GetValue(0).ToString.Replace("\", "/")
        sCaminho = dr.GetValue(0).ToString

        dr.Close()
        dr = Nothing

        Return sCaminho

    End Function
    Protected Function getMotivo(ByVal Controle_DocumentoId As Integer, ByVal Arquivo_UploadId As Integer) As String

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.Busca_Motivo(Controle_DocumentoId, Arquivo_UploadId)

        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
        dr.Read()

        Dim sMotivo As String
        sMotivo = dr.GetValue(0).ToString

        dr.Close()
        dr = Nothing

        Return sMotivo

    End Function

    Protected Sub BtnEnviar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnEnviar.Click

        Try
            Enviar_Email()
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Email enviado com sucesso!');", True)
            Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "fechajanela();", True)
        Catch ex As Exception
            Dim excp As New clsException(ex)
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Erro ao enviar email!/n Tente novamente!');", True)
        End Try

    End Sub

    Private Sub Enviar_Email()

        Dim corpo_email As String

        corpo_email = "<b>Prezado(s),</b><br /><br />Informamos por meio deste que existem <b> DPS " & _
                      "</b> pendentes de análise.<br />" & _
                      "Apólice - Ramo - Subgrupo: " & Apolice & " - " & Ramo & " - " & Subgrupo & _
                      "<br /><br />Para acessá-los entre na área restrita no link abaixo:<br />" & _
                      "<a herf=''https://" & sNome_Servidor & "/ITE/ITEW0104'' target=_blank>https://" & sNome_Servidor & "/ITE/ITEW0104</a>" & _
                      "<br /><br />Atenciosamente,<br /><br />Aliança do Brasil"

        Dim destino_email As String
        destino_email = getEmailEnvio("email_medico", cAmbiente.Ambiente)
        'destino_email = "c_jtioma@bbmapfre.com.br"

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.Envio_Email(Session("usuario"), corpo_email, destino_email)

        bd.ExecutaSQL()

    End Sub

    Protected Function getEmailEnvio(ByVal secao As String, ByVal ambiente_id As Integer) As String

        Dim bd2 As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd2.Email_Envio(secao, ambiente_id)

        Dim dr As Data.SqlClient.SqlDataReader = bd2.ExecutaSQL_DR()
        dr.Read()

        Dim sEmail As String
        sEmail = dr.GetValue(0).ToString

        'Descomentar parte abaixo
        Return sEmail
        'Return "c_jtioma@bbmapfre.com.br"

    End Function
End Class