Imports System.Drawing
Imports System.Web.UI.WebControls
Imports System.Data

Partial Class CarregaExtratoVidas
    Inherits System.Web.UI.Page

    Public data As String
    Protected WithEvents Table1 As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    'Protected WithEvents divGridMovimentacao As System.Web.UI.WebControls.
    Public periodos As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblListagemVidas As System.Web.UI.WebControls.Label
    Protected WithEvents lblListagemEnviada As System.Web.UI.WebControls.Label
    Protected WithEvents pnlAlerta As System.Web.UI.WebControls.Panel
    Protected WithEvents ucPaginacao As segw0080.paginacaoCarregaExtrato
    Protected WithEvents lblPeriodoFatura2 As System.Web.UI.WebControls.Literal


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim linkseguro As Alianca.Seguranca.Web.LinkSeguro

        'Implementa��o da SABL0101
        linkseguro = New Alianca.Seguranca.Web.LinkSeguro
        Dim usuario As String = linkseguro.LerUsuario("", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))
        If Request.QueryString("fatura") = "" Then
            If usuario = "" Then
                Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
            End If

            Session("usuario_id") = linkseguro.Usuario_ID
            Session("usuario") = linkseguro.Login_REDE
            Session("cpf") = linkseguro.CPF

            'Session("usuario_id") = "9382"  'linkseguro.Usuario_ID
            'Session("usuario") = "GRIGORIO" 'linkseguro.Login_WEB
            'Session("nomeUsuario") = "ALESSANDRA MATSUMURA GRIGORIO" 'linkseguro.Nome"
            'Session("cpf") = "29786882890" 'linkseguro.CPF
            'Session("ramo") = "93"
            'Session("apolice") = "16576"
            'Session("estipulante") = "ORSA CELULOSE PAPEL E EMBALAGENS S.A."
            'Session("subgrupo_id") = 5
            'Session("nomesubGrupo") = "ORSA AMAZONIA"

        End If

        Dim cAmbiente As Alianca.Seguranca.Web.ControleAmbiente
        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
        cAmbiente.ObterAmbiente(url)
        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produ��o)
        Else
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produ��o)
        End If

        Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)

        Try

            For Each m As String In Request.QueryString.Keys
                Session(m) = Request.QueryString(m)
            Next

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try


        Try
            'cUtilitarios.br(Request.Form("__EVENTTARGET"))
            If Request.Form("__EVENTTARGET") = "" Then
                If Not Request.Form("gerarXml") Is Nothing AndAlso Request.Form("gerarXml") <> "" Then
                    If Request.Form("gerarXml") = 1 Then
                        geraXmlUsuarios()
                        'cUtilitarios.escreveScript("document.Form1.gerarXml.value='';")
                        Response.End()
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.GetType.ToString <> "System.Threading.ThreadAbortException" Then
                Dim excp As New clsException(ex)
            End If
        End Try


        Me.periodos = getPeriodoCompetenciaSession()

        If Not IsPostBack Then
            '    '''Pega os dados das condi��es do Subgrupo
            getCondicoesSubgrupo()
            mConsultaUsuarios()
            lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomeSubgrupo") & "<br>Fun��o: Extrato Mov. de Vidas"
            lblVigencia.Text = "<br>" & getPeriodoCompetenciaSession()

            lblPeriodoFatura.Text = periodos
            data = lblPeriodoFatura.Text
            'lblPeriodoFatura2.Text = getPeriodoCompetenciaSession()
            Dim msgFim As String = getPeriodoCompetenciaSession()
            Dim dtFim As String = msgFim.Substring(14)
            lblNavegacao.Visible = True

            msgFim = "O encerramento da movimenta��o deve ser realizado somente dentro do per�odo de compet�ncia " & msgFim

            btnFecharMovimentacao.Attributes.Add("onclick", "return confirmaFechamento('" & dtFim & "', '" & msgFim & "')")
        End If

        Me.btnFecharMovimentacao.Visible = False
        Me.lblPeriodoFatura.Visible = False

    End Sub

    Private Function getTpAcesso() As String
        Dim ind_acesso As String
        Select Case Session("acesso")
            Case "1"
                ind_acesso = "Administrador da Alian�a do Brasil"
            Case "2"
                ind_acesso = "Administrador de Ap�lice"
            Case "3"
                ind_acesso = "Administrador de Subgrupo"
            Case "4"
                ind_acesso = "Operador"
        End Select

        Return ind_acesso
    End Function

    Private Function getFaturaAtualSession() As Integer
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)
        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)
        Dim wf_id As Integer = 0

        Try
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.HasRows Then
                While dr.Read()
                    wf_id = CInt(dr.GetValue(0))
                End While
            End If
            dr.Close()
            dr = Nothing

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return wf_id
    End Function


    Private Function getPeriodoCompetenciaSession() As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)
        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)
        Dim data_inicio, data_fim, retorno As String

        Try
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()


            If dr.HasRows Then
                While dr.Read()

                    data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                    data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)

                End While
            End If
            dr.Close()
            dr = Nothing
            retorno = "Per�odo de Compet�ncia: " & data_inicio & " a " & data_fim

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return retorno
    End Function

    Private Sub btnFechaPagina_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("Centro.aspx")
    End Sub

    Private Sub geraXmlUsuarios()
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        Dim cpf As String = "null"
        Dim nome As String = "null"
        Dim tipo As String = "null"

        bd.SEGS5753_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), cpf, nome, "null", tipo, 1)

        Try
            Dim dt As Data.DataTable = bd.ExecutaSQL_DT

            ExportadorXLS.Exportar(dt)
            Exit Sub
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    '''Consulta os usu�rios
    Private Sub mConsultaUsuarios()

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        Dim cpf As String = "null"
        Dim nome As String = "null"
        Dim tipo As String = "null"

        bd.SEGS5753_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), cpf, nome, "null", tipo)

        'cUtilitarios.escreveScript("top.escondeaguarde()")
        'Response.Write(bd.SQL)
        'Response.End()
        Try
            Dim dt As Data.DataTable = bd.ExecutaSQL_DT

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then

                Me.grdPesquisa.CurrentPageIndex = Session("indice")
                Me.grdPesquisa.SelectedIndex = -1


                Me.ucPaginacao._valor = "2"
                Me.ucPaginacao.GridDataBind(Me.grdPesquisa, dt)
                Session("grdDescarte") = dt

                Me.lblSemUsuario.Visible = False
                ucPaginacao.Visible = True
                grdPesquisa.Visible = True
                'gridMovimentacao.Visible = True
            Else
                Session.Remove("indice")
                Session.Remove("grdDescarte")
                Me.lblSemUsuario.Text = "<br><br><br>Nenhuma vida encontrada.<br><br>"
                Me.lblSemUsuario.Visible = True
                cUtilitarios.escreveScript("var inibeXml = '1';")
                ucPaginacao.Visible = False
                grdPesquisa.Visible = False
                gridMovimentacao2.Visible = False

            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub


    Private Sub getCondicoesSubgrupo()
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        bd.SEGS5705_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

        Try
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read Then
                If Not IsDBNull(dr.GetValue(1)) Then
                    'Me.lblIdadeMaxima.Text = ""
                    Me.lblIdadeMaxima.Text = IIf(dr.GetValue(1).ToString.Trim = "", " --- ", dr.GetValue(1).ToString.Trim)
                End If

                If Not IsDBNull(dr.GetValue(0)) Then
                    Me.lblIdadeMinima.Text = IIf(dr.GetValue(0).ToString.Trim = "", " --- ", dr.GetValue(0).ToString.Trim)
                End If
            Else
                Me.lblIdadeMaxima.Text = " --- "
                Me.lblIdadeMinima.Text = " --- "
            End If
            dr.Close()

            bd.SEGS5706_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

            dr = bd.ExecutaSQL_DR()


            If dr.Read Then
                Me.lblMultSalarial.Text = IIf(dr.GetValue(2).ToString.Trim = "", " --- ", dr.GetValue(2).ToString.Trim)
                Me.lblCapitalSegurado.Text = IIf(dr.GetValue(1).ToString.Trim = "", " --- ", dr.GetValue(1).ToString.Trim)
                Me.lblConjuge.Text = IIf(dr.GetValue(0).ToString.Trim = "", " --- ", dr.GetValue(0).ToString.Trim)
            Else
                Me.lblMultSalarial.Text = " --- "
                Me.lblCapitalSegurado.Text = " --- "
                Me.lblConjuge.Text = " --- "
            End If
            dr.Close()


            bd.SEGS5744_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

            dr = bd.ExecutaSQL_DR()

            Dim valor_capital_global As String = "0"

            If dr.Read Then
                Me.lblLimiteMaximo.Text = IIf(dr.GetValue(0).ToString.Trim = "", " --- ", cUtilitarios.trataMoeda(dr.GetValue(0).ToString.Trim))
            Else
                Me.lblLimiteMaximo.Text = " --- "
            End If

            If Me.lblCapitalSegurado.Text = "Capital Global" Then
                Me.td_valor_capital.Visible = True
                Me.td_valor_capital_lbl.Visible = True
            Else
                Me.td_valor_capital.Visible = False
                Me.td_valor_capital_lbl.Visible = False
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Function addCell(ByVal valor As String) As Web.UI.HtmlControls.HtmlTableCell
        Dim td As New Web.UI.HtmlControls.HtmlTableCell
        td.InnerText = valor
        Return td
    End Function



#Region "Eventos"
    Private Sub mGridDataBind(ByVal dt As DataTable)
        Me.ucPaginacao.GridDataBind(grdPesquisa, dt)
    End Sub

    Private Sub mSelecionaGrid(ByVal ItemIndex As Integer)
        Me.grdPesquisa.SelectedIndex = ItemIndex
        Me.mGridDataBind(Session("grdDescarte"))
        CType(Me.grdPesquisa.SelectedItem.Cells(0).Controls(1), RadioButton).Checked = True
    End Sub

    Private Sub mPaginacaoClick(ByVal Argumento As String) Handles ucPaginacao.ePaginaAlterada
        Me.grdPesquisa.SelectedIndex = -1
        Select Case Argumento
            Case "pro"
                Me.grdPesquisa.CurrentPageIndex += 1
            Case "ant"
                Me.grdPesquisa.CurrentPageIndex -= 1
            Case Else
                Me.grdPesquisa.CurrentPageIndex = Argumento
        End Select
        Session("indice") = Me.grdPesquisa.CurrentPageIndex
        Me.mGridDataBind(Session("grdDescarte"))
    End Sub

    Private Sub grdPesquisa_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdPesquisa.ItemDataBound
        'If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
        'CType(e.Item.Cells(0).FindControl("linkCodDoc"), HyperLink).NavigateUrl = "javascript:busca_registro('" & e.Item.Cells(1).Text.Trim & "','" & e.Item.Cells(2).Text.Trim & "','" & e.Item.Cells(4).Text.Trim & "','" & e.Item.Cells(5).Text.Trim & "');"
        'End If
        'If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
        'CType(e.Item.Cells(0).FindControl("linkCodDocDI"), HyperLink).NavigateUrl = "javascript:excluir_DI('" & e.Item.Cells(1).Text.Trim & "','" & e.Item.Cells(2).Text.Trim & "','" & e.Item.Cells(6).Text.Trim & "');"
        'End If
    End Sub

#End Region

End Class
