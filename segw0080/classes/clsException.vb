Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Public Class clsException

    Inherits System.ApplicationException

    Private _Excecp As Exception    'Simboliza o Exception gerado
    Private msg As String = "Ocorreu um erro inesperado, por favor, tente novamente. Caso o problema persista, contate o Administrador do Sistema."


    'Montagem do Sub New, servir� para instanciarmos a Classe
    'E passar o Exception como Parametro
    Sub New(ByRef ex As Exception)
        _Excecp = ex
        AvalException(_Excecp)
    End Sub

    Private Sub AvalException(ByVal ex As Exception)

        System.Web.HttpContext.Current.Response.Write("<!--" & ex.Message & ex.StackTrace & "-->")

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        Dim app As System.Reflection.Assembly
        Dim sigla As String

        app = System.Reflection.Assembly.GetCallingAssembly

        For Each x As Object In app.GetCustomAttributes(False)
            Try
                sigla = x.Title()
            Catch ex1 As Exception
            End Try
        Next

        'Insere o log de erro na tabela web_seguros_db..
        bd.SQL = "exec SEGS5975_SPI @sigla_aplicacao = '" & sigla & "', @descricao= '" & ex.Message.Replace("'", "").Replace("""", "").Replace(vbNewLine, "") & ex.StackTrace.Replace("'", "").Replace("""", "").Replace(vbNewLine, "") & "',@usuario = '" & System.Web.HttpContext.Current.Session("usuario") & "'"
        cUtilitarios.br(bd.SQL)
        'bd.ExecutaSQL()

        'Tratamento dos poss�veis Exceptions
        'Aqui voc� pode configurar suas mensagens de erro de acordo com o exception
        If (TypeOf ex Is System.ArgumentOutOfRangeException) Then
            cUtilitarios.br(msg)
            Exit Sub

        ElseIf (TypeOf ex Is System.IO.FileLoadException) Then
            cUtilitarios.br(msg)
            Exit Sub

        ElseIf (TypeOf ex Is System.IO.FileNotFoundException) Then
            cUtilitarios.br(msg)
            Exit Sub

        ElseIf (TypeOf ex Is System.IO.DirectoryNotFoundException) Then
            cUtilitarios.br(msg)
            Exit Sub

        ElseIf (TypeOf ex Is System.OverflowException) Then
            cUtilitarios.br(msg)
            Exit Sub

        ElseIf (TypeOf ex Is System.StackOverflowException) Then
            cUtilitarios.br(msg)
            Exit Sub

        ElseIf (TypeOf ex Is InvalidCastException) Then
            cUtilitarios.br(msg)
            Exit Sub

        ElseIf (TypeOf ex Is System.NullReferenceException) Then
            cUtilitarios.br(msg)
            Exit Sub

        ElseIf (TypeOf ex Is InvalidOperationException) Then
            cUtilitarios.br(msg)
            Exit Sub

        ElseIf (TypeOf ex Is SqlException) Then

            'Erros de SQL
            Dim ex1 As SqlException = ex
            Select Case ex1.Number

                Case 547
                    cUtilitarios.br(msg)

                Case 208
                    cUtilitarios.br(msg)

                Case 137
                    cUtilitarios.br(msg)

                Case 170
                    cUtilitarios.br(msg)

                Case 207
                    cUtilitarios.br(msg)

                Case Else
                    cUtilitarios.br(msg)

            End Select
            Exit Sub

        Else
            'Caso n�o seja nenhum dos Exceptions declarados
            cUtilitarios.br(msg)
            Exit Sub
        End If
    End Sub

End Class