<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CarregaExtratoVidas.aspx.vb" Inherits="segw0080.CarregaExtratoVidas" %>
<%@ Register TagPrefix="uc1" TagName="paginacaoCarregarExtrato" Src="paginacaoCarregarExtrato.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Downloads</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<script src="scripts/overlib421/overlib.js" type="text/javascript"></script>
		<LINK href="../segw0060/CSS/EstiloBase.css" type="text/css" rel="stylesheet">
			<script>
				
			function confirmaFechamento(dtFim, msg){
				valor = dtFim.toString();
				var dia = valor.substr(0, 2);
				var mes = parseInt(valor.substr(3, 2));
				var ano = parseInt(valor.substr(6, 4));	
				
				if (valor.substr(3, 2) == '08')
					mes = 8
					
				if (valor.substr(3, 2) == '09')
					mes = 8
				var dt = new Date();
							
				var dt1 = new Date(ano,mes-1,dia);
				var dt2 = new Date(dt.getYear(),dt.getMonth(),dt.getDate());
			
				if ((dt2 - dt1) > 0) {
					alert(msg)				
					return false;
				}
				
				var confirma = confirm('Toda a movimenta��o de vidas ser� finalizada, n�o sendo poss�vel incluir / alterar / excluir\nmais nenhuma vida referente � fatura de <%=data%>.\n\nConfirma a finaliza��o da movimenta��o de vidas?');
				if (confirma==true){					
					location.href='?fatura=ok';
					return false;
				}else{
					return false;
				}
			}
			
				
			function exibeDependentes(id,src) {
					
				if (src.src.indexOf('imgMenos') != -1)
					src.src = '../segw0060/Images/imgMais.gif';
				else
					src.src = '../segw0060/Images/imgMenos.gif';
				
				for(i=0;i<=5;i++) {					
					var tr = eval("document.getElementById('grdPesquisa_"+id+"_"+i+"')");
					if (tr == null)
						break;
					if (tr.style.display == 'none') 
						tr.style.display = '';
					else
						tr.style.display = 'none';
				}		
			}	
				//id="grdPesquisa_1_0" 
				
			function escondeDiv(div){
				var idDiv = document.getElementById(div);
				if (idDiv.style.display==''){
					idDiv.style.display='none';
				}else{
					idDiv.style.display='';
				}
			}
			
			function vidaExcluir(numero){
				if (numero)
					location.href="MovimentacaoUmaUm.aspx?excluir=1&expandir=1";
				else
					location.href="MovimentacaoUmaUm.aspx&expandir=1";
			}
			
			function confirmaExclusao(){
				var divExcluir = document.getElementById("lblAcao");
				var nomeSegurado = document.getElementById("nomeAlvo").value;
				var confirmacao = confirm('Confirma a exclus�o do segurado "' + nomeSegurado + '"?\nSe existirem c�njuges para este segurado todos ser�o exclu�dos.')
				
				return confirmacao
			}
			
			function confirmaDesfazer(){
				var confirmacao = confirm('Ser�o desfeitas todas as altera��es realizadas para o Subgrupo "<%= Session("subgrupo")%>".\n\nConfirma a opera��o?');
			}
			
			function getPesquisaLoad(valor) {				
												
				if(valor == "CPF" || valor == "Nome") {
					document.getElementById("divCampoPesquisa").style.display = "block";
				} else {
					document.getElementById("divCampoPesquisa").style.display = "none";
				}				
			}
			function getPesquisa(valor) {				
				
				document.Form1.campoPesquisa.value = "";
				
				if(valor == "CPF" || valor == "Nome") {
					document.getElementById("divCampoPesquisa").style.display = "block";
				} else {
					document.getElementById("divCampoPesquisa").style.display = "none";
				}				
			}
			function buscaDados() {
				var valor = document.Form1.DropDownList2.value;
				
				document.Form1.submit();	
			}
			var teste = 0;
			var id;
			
			function alteracoes(valor, tipo, nome, idTitular, alvo, operacao) {								

				idTemp = "grdPesquisa_" + alvo;
											
				idTemp2 = document.getElementById("alvoTemp").value; 
								
				if(document.getElementById(idTemp2)) {															
					document.getElementById(idTemp2).style.background='#ffffff';
				} 									
								
				document.getElementById(idTemp).style.background='#f8ef00';
				document.getElementById("alvoTemp").value = idTemp;
			}
			
			function validaMouseOut(src,color) {
				//alert(teste);
				//if (id != src.id)
				src.style.background=color;			
					
				teste = 0;
			}
			function mO(cor, alvo) {												
				if(alvo.style.background != '#f8ef00')
					alvo.style.background=cor;							
			}
			
			function alteraRegistro() {
				if (document.getElementById("divBtnIncluiDependente")) {
					if(document.getElementById("divBtnIncluiDependente").style.display == "none") {
						return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=alterardependente&id=' + document.getElementById("alvo").value + "&idTitular=" + document.Form1.idTitular.value, 240, 450);
					} else {
						return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=alterar&id=' + document.getElementById("alvo").value, 280, 450);
					}
				} else {
					return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=alterar&id=' + document.getElementById("alvo").value, 280, 450);
				}
			}
			function incluiDependente() {
				try {										
					idTemp2 = document.getElementById("alvoTemp").value; 
					
					var dep = document.getElementById(idTemp2 + "_0");
					if (dep != null) {
						alert("Voc� j� informou um c�njuge para o titular escolhido.");
						return false;
					}
					
				} catch(ex) {
				
				}
				
				//alert(document.Form1.idTitular.value);
				
				return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=incluirdependente&id=' + document.getElementById("alvo").value + "&idTitular=" + document.Form1.idTitular.value, 240, 450);
			}
			function excluiRegistro() {	
				if (document.getElementById("divBtnIncluiDependente")) {		
					if(document.getElementById("divBtnIncluiDependente").style.display == "none") {					
						return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluirdependente&id=' + document.getElementById("alvo").value + "&idTitular=" + document.Form1.idTitular.value, 240, 450);				
					} else {
						return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluir&id=' + document.getElementById("alvo").value, 280, 450);				
					}
				} else{
					return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluir&id=' + document.getElementById("alvo").value, 280, 450);				
				}
			}
			
function FormataCPF(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres){
		var wTecla, wVr, wTam;
		 
			//alert(pForm);
		  
		wTecla = pTeclaPres.keyCode;
		wVr = pForm[pCampo].value;
		wVr = wVr.toString().replace( "-", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( "/", "" );
		wTam = wVr.length ;

		if(wTam > pTamMax) {
			wTam = pTamMax;
			wVr = wVr.substr(0, pTamMax)
		}
			
		if (wTam < pTamMax && wTecla != 8) { 
			wTam = wVr.length + 1 ; 
		}

		if (wTecla == 8 ) { 
			wTam = wTam - 1 ; 
		}
		   
		if ( wTecla == 8 || wTecla == 88 || wTecla >= 48 && wTecla <= 57 || wTecla >= 96 && wTecla <= 105 ){
		if ( wTam <= 2 ){
			pForm[pCampo].value = wVr ;
		}
		if (wTam > pPosTraco && wTam <= pTamMax) {
				wVr = wVr.substr(0, wTam - pPosTraco) + '-' + wVr.substr(wTam - pPosTraco, wTam);
		}
		if ( wTam == pTamMax){
				wVr = wVr.substr( 0, wTam - pPos1 ) + '.' + wVr.substr(wTam - pPos1, 3) + '.' + wVr.substr(wTam - pPos2, wTam);
		}
		pForm[pCampo].value = wVr;
		 
		}

		}
		function formataCampo(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres) {
					
		    if(pForm.DropDownList2.value == "CPF") { 
				FormataCPF(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres);
			}		
		}
				
		
			</script>
	</HEAD>
	<body MS_POSITIONING="FlowLayout" onload="setWidths();">
		<div id="overDiv" style="Z-INDEX: 1000; VISIBILITY: hidden; POSITION: absolute"></div>
		<form id="Form1" method="post" runat="server">
			<input type="hidden" value="0" name="alvoTemp" id="alvoTemp">
			<asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela">Label</asp:label><BR>
			<asp:Label id="lblVigencia" runat="server" CssClass="Caminhotela"><br>COTEMINAS S.A - Per�odo de compet�ncia: 01/12/2006 at� 31/12/2006</asp:Label><BR>
			<P><asp:label id="Label17" runat="server" CssClass="Caminhotela">Condi��es do subgrupo</asp:label>
				<br>
				<TABLE id="Table5" style="WIDTH: 100%; BORDER-COLLAPSE: collapse" borderColor="#cccccc"
					cellSpacing="0" cellPadding="1" border="1"> <!-- PRIMEIRA LINHA -->
					<TR class="titulo-tabela sem-sublinhado">
						<TD>Capital Segurado</TD>
						<TD id="td_valor_capital" runat="server">Valor Capital</TD>
						<TD>
							Limite&nbsp;M�nimo</TD>
						<TD>Limite&nbsp;M�ximo</TD>
						<TD>M�lt. Salarial</TD>
						<TD>Idade M�n. Mov.</TD>
						<TD>Idade M�x. Mov</TD>
						<TD>C�njuge</TD>
					</TR>
					<TR>
						<TD align="center">
							<asp:Label id="lblCapitalSegurado" Runat="server"></asp:Label></TD>
						<TD id="td_valor_capital_lbl" align="center" runat="server">
							<asp:Label id="lblValorCapital" Runat="server"></asp:Label></TD>
						<TD id="tdLimiteMinimo" align="right" runat="server">
							<asp:Label id="LblLimiteMinimo" Runat="server"></asp:Label></TD>
						<TD id="tdLimiteMaximo" align="right" runat="server">
							<asp:Label id="lblLimiteMaximo" Runat="server"></asp:Label></TD>
						<TD align="center">
							<asp:Label id="lblMultSalarial" Runat="server"></asp:Label></TD>
						<TD align="center">
							<asp:Label id="lblIdadeMinima" Runat="server"></asp:Label></TD>
						<TD align="center">
							<asp:Label id="lblIdadeMaxima" Runat="server"></asp:Label></TD>
						<TD align="center">
							<asp:Label id="lblConjuge" Runat="server"></asp:Label></TD>
					</TR>
				</TABLE>
			</P>
			<p>
				<STYLE>.headerFixDiv { FONT-WEIGHT: bold; WIDTH: 100%; COLOR: #ffffff; POSITION: absolute; TOP: 2px; BACKGROUND-COLOR: #003399 }
	.headerFake { BORDER-RIGHT: red 1px solid; BORDER-TOP: red 1px solid; BORDER-LEFT: red 1px solid; BORDER-BOTTOM: red 1px solid; POSITION: absolute; TOP: 2px }
	.headerFixDiv2 { VISIBILITY: hidden }
	.headerFakeDiv { VISIBILITY: hidden; LINE-HEIGHT: 0 }
	</STYLE></p>
			<DIV id="divGridMovimentacao" style="OVERFLOW-Y: hidden;OVERFLOW-X: auto;WIDTH: 724px;HEIGHT: 300px">
				<DIV style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; OVERFLOW: hidden; PADDING-TOP: 0px">
					<TABLE id="gridMovimentacao2" style="BORDER-COLLAPSE: collapse" borderColor="#dddddd" cellSpacing="0"
						cellPadding="0" border="1" runat="server">
						<TR class="paddingzero-sembordalateral">
							<TD>
								<DIV style="WIDTH: 99%">
									<TABLE style="BORDER-COLLAPSE: collapse" cellSpacing="0" cellPadding="0" border="1" CssClass="escondeTituloGridTable">
										<TR>
											<TD id="tdHeader0">
												<DIV class="headerFixDiv2" id="header0"></DIV>
												<DIV class="headerFixDiv">Opera��o</DIV>
											</TD>
											<TD id="tdHeader1" wrap="false">
												<DIV class="headerFixDiv2" id="header1"></DIV>
												<DIV class="headerFixDiv">Dt. Opera��o</DIV>
											</TD>
											<TD id="tdHeader2" wrap="false">
												<DIV class="headerFixDiv2" id="header2"></DIV>
												<DIV class="headerFixDiv">Segurado</DIV>
											</TD>
											<TD id="tdHeader3" wrap="false">
												<DIV class="headerFixDiv2" id="header3"></DIV>
												<DIV class="headerFixDiv">Tipo</DIV>
											</TD>
											<TD id="tdHeader4" wrap="false">
												<DIV class="headerFixDiv2" id="header4"></DIV>
												<DIV class="headerFixDiv">CPF</DIV>
											</TD>
											<TD id="tdHeader5" wrap="false">
												<DIV class="headerFixDiv2" id="header5"></DIV>
												<DIV class="headerFixDiv">Dt. Nasc.</DIV>
											</TD>
											<TD id="tdHeader6" wrap="false">
												<DIV class="headerFixDiv2" id="header6"></DIV>
												<DIV class="headerFixDiv">Sexo</DIV>
											</TD>
											<TD id="tdHeader7" wrap="false">
												<DIV class="headerFixDiv2" id="header7"></DIV>
												<DIV class="headerFixDiv">Dt. In�cio Vig�ncia</DIV>
											</TD>
											<TD id="tdHeader8" wrap="false">
												<DIV class="headerFixDiv2" id="header8"></DIV>
												<DIV class="headerFixDiv">Dt. Desligamento</DIV>
											</TD>
											<TD id="tdHeader9" wrap="false">
												<DIV class="headerFixDiv2" id="header9"></DIV>
												<DIV class="headerFixDiv">Sal�rio (R$)</DIV>
											</TD>
											<TD id="tdHeader10" wrap="false">
												<DIV class="headerFixDiv2" id="header10"></DIV>
												<DIV class="headerFixDiv">Capital (R$)</DIV>
											</TD>
										</TR>
										<TR>
											<TD colSpan="11">
												<DIV id="divScrollGrid" style="OVERFLOW-Y: auto;OVERFLOW-X: hidden;WIDTH: 100%;HEIGHT: 265px"><!--- In�cio da grid de Usu�rios -->
													<asp:datagrid id="grdPesquisa" runat="server" CssClass="escondeTituloGridTable" BorderColor="#DDDDDD"
														AllowPaging="True" PageSize="50" ShowHeader="False" AutoGenerateColumns="False" CellSpacing="0"
														CellPadding="0">
														<SelectedItemStyle CssClass="ponteiro"></SelectedItemStyle>
														<AlternatingItemStyle CssClass="ponteiro"></AlternatingItemStyle>
														<ItemStyle CssClass="ponteiro"></ItemStyle>
														<FooterStyle CssClass="0019GridFooter"></FooterStyle>
														<Columns>
															<asp:BoundColumn DataField="operacao"></asp:BoundColumn>
															<asp:BoundColumn DataField="dt_alteracao" HeaderText="Opera&ccedil;&atilde;o"></asp:BoundColumn>
															<asp:BoundColumn DataField="nome" HeaderText="Data da Opera&ccedil;&atilde;o" DataFormatString="{0:dd/MM/yyyy}"
																ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
															<asp:BoundColumn DataField="tipo" HeaderText="Segurado"></asp:BoundColumn>
															<asp:BoundColumn DataField="cpf" HeaderText="Tipo"></asp:BoundColumn>
															<asp:BoundColumn DataField="dt_nascimento" HeaderText="CPF"></asp:BoundColumn>
															<asp:BoundColumn DataField="sexo" HeaderText="Nascimento" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
															<asp:BoundColumn DataField="dt_inicio_vigencia" HeaderText="Sexo"></asp:BoundColumn>
															<asp:BoundColumn DataField="dt_fim_vigencia_sbg" HeaderText="Dt. Desligamento" DataFormatString="{0:dd/MM/yyyy}">
																<ItemStyle HorizontalAlign="center"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="val_salario" HeaderText="Sal&#225;rio (R$)">
																<ItemStyle HorizontalAlign="Right"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="val_capital_segurado" HeaderText="Capital (R$)">
																<ItemStyle HorizontalAlign="Right"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="exclusao_agendada" visible="False"></asp:BoundColumn>
															<asp:BoundColumn DataField="acertos" visible="False"></asp:BoundColumn>
															<asp:BoundColumn DataField="ind_operacao_origem" visible="False"></asp:BoundColumn>
														</Columns>
														<PagerStyle Visible="false"></PagerStyle>
													</asp:datagrid></DIV>
											</TD>
										</TR>
									</TABLE>
								</DIV>
							</TD>
						</TR>
					</TABLE>
				</DIV>
				<CENTER>
					<asp:Label id="lblSemUsuario" CssClass="Caminhotela" Runat="server" Visible="False"></asp:Label></CENTER>
			</DIV>
			<DIV></DIV>
			<uc1:paginacaoCarregarExtrato id="ucPaginacao" runat="server"></uc1:paginacaoCarregarExtrato></TD></TR></TABLE>
			<asp:panel id="pnlBtnEncerrar" runat="server" Visible="true">
<DIV align="center">
					<DIV style="TEXT-ALIGN: center">&nbsp;
						<asp:Label id="lblPeriodoFatura" Runat="server"></asp:Label>&nbsp;<BR>
						&nbsp;
						<BR>
						<asp:Button id="btnFecharMovimentacao" runat="server" CssClass="Botao" Width="160px" Text="Encerrar movimenta��o"></asp:Button></DIV>
					<BR>
					<INPUT class="Botao" onclick="document.Form1.gerarXml.value=1;document.Form1.submit();"
						type="button" value="Gerar Excel" name="btnXml">
				</DIV><BR>&nbsp; <INPUT type="hidden" name="gerarXml"> 
<P></P>
<DIV></DIV>
			</asp:panel><asp:literal id="lblPeriodoFatura2"></asp:literal></form>
		<script>			
				document.Form1.gerarXml.value="";
		
				/** Esconde o bot�o de gerar XML **/
				
				try {
					if (inibeXml == '1') {
						document.Form1.btnXml.style.display = 'none';
					}
				} catch (ex) {}
					
				tam = 0;
				if(document.getElementById("grdPesquisa"))	
					tam = document.getElementById("grdPesquisa").clientHeight;		
					
					
					//alert(document.getElementById("divGridMovimentacao").clientHeight); //282
					//alert(document.getElementById("grdPesquisa").clientHeight); //14
					
																									
				try {				
					for(i=0; i<=10; i++) {																												
						if(i == 10 && document.getElementById("grdPesquisa__0_cell_10").style.display != "none" ) {
							if(tam >= 280)
								add = "OO";																
																					
						} else if(i == 9 && document.getElementById("grdPesquisa__0_cell_9").style.display != "none" && document.getElementById("grdPesquisa__0_cell_10").style.display == "none") {
							if(tam >= 280)
								add = "OO";																							
						} else {
							add = "";
						}					
						
						document.getElementById("header" + i).innerHTML = document.getElementById("txtMax" + i).value + add;														
						document.getElementById("headerFake" + i).innerHTML = document.getElementById("txtMax" + i).value;																			
					}														
				} catch (ex){ document.write("<span style='color:white;'>try 1:" + ex.message + " - " + i + "- " + "</span>"); }													
				
				try {											
					for(i=0; i<=10; i++) {										
						if(document.getElementById("grdPesquisa__0_cell_" + i)) {
							if(document.getElementById("grdPesquisa__0_cell_" + i).style.display == "none") {
								document.getElementById("tdHeader" + i).style.display = "none";
							}
						}			
					}
				} catch (ex){ document.write("<span style='color:white;'>try 2:" + ex.message + " - " + i + "- " + "</span>"); }													
				
						
				top.escondeaguarde();
				
				function hideAndSeekHandler(op,w){
					dv = document.getElementById("divGridMovimentacao");
					pag = document.getElementById("ucPaginacao_lblPaginacao");		
					tbl = document.getElementById("Table5");
							
					
					if (op == 'esconder'){
						dv.style.width = w - 20;
						pag.style.width = dv.clientWidth - 35;
						tbl.style.width = dv.clientWidth - 35;
						
					}
					else{
						dv.style.width = "724px";		
						pag.style.width = "100%";
						tbl.style.width = "100%";				
					}
				}
				
				function setWidths(){	
					return false;				
					top.iniciarMenu();
				}
				
				
		</script>
	</body>
</HTML>
