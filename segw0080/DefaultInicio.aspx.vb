﻿Public Partial Class DefaultInicio
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim v_sUsuario As String = IIf(IsNothing(Request("Usuario")), "ALGRIGORIO", Request("Usuario"))

        'Dim v_sUsuario As String = HelperBase.ObterSetting("Desenv_Usuario_Gera_Link_Seguro")

        Dim v_sValorParametro As String
        'v_sUsuario = "ALGRIGORIO"
        Dim v_oParametro As New Alianca.Seguranca.Web.LinkSeguro()
        'Dim v_oParametro As New Alianca.Seguranca.Web.LinkSeguro(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
        v_sValorParametro = v_oParametro.GerarParametro("", Alianca.Seguranca.Web.LinkSeguro.TempoExpiracao.Grande, Request.UserHostAddress, v_sUsuario, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)

        v_oParametro.LerUsuario("", Request.UserHostAddress, v_sValorParametro)

        Session("ramo") = "93"
        Session("apolice") = "292282"
        Session("estipulante") = "----"
        Session("subgrupo_id") = 1
        Session("nomesubGrupo") = "COMPANHIA IMOBILIARIA DE BRASILIA TERRACAP"

        v_oParametro = Nothing

        Response.Redirect("CarregaExtratoVidas.aspx?SLinkSeguro=" & v_sValorParametro & "&expandir=1&")

    End Sub

End Class