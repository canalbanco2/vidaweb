Public Class cUtilitarios

    'Trata a data para o seguinte formato dd/mm/aaaa hh:mm
    Public Shared Function trataDataHora(ByVal data As String) As String

        Try
            Dim dt_final As String

            Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim ano As String = CType(data, DateTime).Year

            Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = dia + "/" + mes + "/" + ano


            Return dt_final + " " + hora + ":" + minuto

        Catch ex as System.Exception

            Return ""

        End Try

    End Function

    'Trata a data para o seguinte formato dd/mm/aaaa 
    Public Shared Function trataData(ByVal data As String) As String

        Try

            Dim dt_arr() As String = data.Split("/")
            Dim dt_final As String

            'Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim dia As String = dt_arr(0).PadLeft(2, "0")
            'Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim mes As String = dt_arr(1).PadLeft(2, "0")
            'Dim ano As String = CType(data, DateTime).Year
            Dim ano As String = dt_arr(2)

            'Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            'Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = dia + "/" + mes + "/" + ano


            Return dt_final

        Catch ex as System.Exception

            Return ""

        End Try

    End Function

    'Trata a data para o seguinte formato dd/mm/aaaa 
    Public Shared Function trataDataDB(ByVal data As String) As String

        Try

            Dim dt_arr() As String = data.Split("/")
            Dim dt_final As String

            'Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim dia As String = dt_arr(0).PadLeft(2, "0")
            'Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim mes As String = dt_arr(1).PadLeft(2, "0")
            'Dim ano As String = CType(data, DateTime).Year
            Dim ano As String = dt_arr(2)

            'Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            'Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = ano + mes + dia


            Return dt_final

        Catch ex as System.Exception

            Return ""

        End Try

    End Function

    Public Shared Function trataSmalldate(ByVal data As String) As String

        Try
            Dim dt_final As String

            Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim ano As String = CType(data, DateTime).Year

            Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = ano + mes + dia


            Return dt_final + " " + hora + ":" + minuto

        Catch ex as System.Exception

            Return ""

        End Try

    End Function

    '''Exibe o valor em um alert na tela
    Public Shared Sub br(ByVal valor As String)

        Dim texto As String

        texto = "<script>" + vbNewLine
        texto &= "alert(""" + valor + """);"
        texto &= "</script>"

        System.Web.HttpContext.Current.Response.Write(texto)
    End Sub

    Public Shared Sub escreveScript(ByVal valor As String)
        Web.HttpContext.Current.Response.Write("<script>" + valor + "</script>")
    End Sub

    Public Shared Function destrataCPF(ByVal cpf As String) As String

        If cpf.Length = 14 Then
            Return cpf.Replace(".", "").Replace("-", "")
        Else
            Return cpf
        End If

    End Function

    Public Shared Function trataCPF(ByVal cpf As String) As String

        If cpf.Length = 11 Then
            Return cpf.Substring(0, 3) + "." + cpf.Substring(3, 3) + "." + cpf.Substring(6, 3) + "-" + cpf.Substring(9, 2)
        Else
            Return cpf
        End If

    End Function

    Public Shared Function trataMoeda(ByVal valor As String) As String
        If valor = "&nbsp;" Or valor = "" Then
            Return ""
        Else
            'Return String.Format(String.Format("{0:c}", CDbl(valor))).Substring(3)
            Return String.Format(String.Format("{0:c}", CDbl(valor))).Substring(3).Replace("(", "").Replace(")", "")
        End If


    End Function


    Public Shared Function deParaDLL(ByVal valor As Integer, ByVal limiteMinIdade As String, ByVal limiteMaxIdade As String, ByVal segcapital As String, ByVal idade As String, ByVal nomeConjuge As String) As String

        Select Case valor
            Case 0
                Return ""
            Case 1
                Return "Data de In�cio de Cobertura menor que o in�cio de vig�ncia da ap�lice."
            Case 2
                Return "Data de Entrada no Subgrupo menor que o in�cio de vig�ncia da ap�lice."
            Case 3
                Return "Capital Segurado diferente do capital do subgrupo."
            Case 4
                Return "Capital Segurado n�o informado."
            Case 6
                Return "Sal�rio do Segurado n�o informado."
            Case 7
                Return "Capital Segurado abaixo do limite permitido para o subgrupo."
            Case 8
                Return "Capital Segurado acima do limite permitido para o subgrupo."
            Case 9
                Return "Titular n�o informado."
            Case 10
                Return "Titular j� associado a outro c�njuge."
            Case 11
                Return "Data de In�cio de Vig�ncia do C�njuge menor que a Data de In�cio de Vig�ncia do Titular."
            Case 14
                Return "N�o existe faixas et�rias para o Subgrupo na vig�ncia do segurado."
            Case 15
                Return "Idade do segurado menor que o limite permitido."
            Case 16
                Return "Idade do segurado maior que o limite permitido."
            Case 17
                Return "Inclus�o de segurado j� existente."
            Case 18
                Return "Data de In�cio de Vig�ncia � menor que a data de fim de vig�ncia anterior."
            Case 19
                Return "As faixas et�rias correspondentes n�o possuem um capital segurado."
            Case 20
                Return "N�o existe faixa et�ria correspondente a idade em quest�o."
            Case 21
                Return "Segurado inativo, deve ser feita incluls�o ao inv�s da altera��o."
            Case 22
                Return "Segurado inativo, n�o � poss�vel a exclus�o."
            Case 23
                Return "Segurado n�o encontrado para altera��o."
            Case 24
                Return "Segurado n�o encontrado para exclus�o."
            Case 25
                Return "C�njuge n�o deve ser informado:."
            Case 26
                Return "J� existe um cliente ativo neste subgrupo com o mesmo CPF, Data de Nascimento e sexo."
            Case Else
                Return "Erro n�o encontrado."
        End Select

    End Function

    Public Function getDadosCoberturaHTML(ByVal componente As String) As String

        Dim html As String = ""
        Dim coberturas As String = ""
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader

        bd.coberturasNew(System.Web.HttpContext.Current.Session("apolice"), System.Web.HttpContext.Current.Session("ramo"), 6785, 0, System.Web.HttpContext.Current.Session("subgrupo_id"), componente)

        Try
            dr = bd.ExecutaSQL_DR()


            If dr.HasRows Then
                html &= "<td>Titular</td></tr><tr class='0034td_dado'><td>Nome Cobertura</td><td>Limite M�nimo</td><td>Limite M�ximo</td></tr>"
                While dr.Read

                    html &= "<tr align='center' class='0034td_dado'><td>"
                    html &= IIf(dr.GetValue(2).ToString.Trim = "", " --- ", dr.GetValue(2).ToString.Trim)
                    html &= "</td><td align='right'>"
                    html &= IIf(dr.GetValue(3).ToString.Trim = "", " --- ", dr.GetValue(3).ToString.Trim)
                    html &= "</td><td align='right'>"
                    html &= IIf(dr.GetValue(4).ToString.Trim = "", " --- ", dr.GetValue(4).ToString.Trim)
                    html &= "</td></tr>"

                End While
            Else
                html &= "<tr align='center' class='0034td_dado'><td> --- </td><td> --- </td><td> --- </td></tr>"

            End If

            dr.Close()
            dr = Nothing

        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        End Try

        html &= "<table cellpadding='0' cellspacing='0' border='0'> <tr><td>" & vbCrLf
        html &= "<DIV style='OVERFLOW: auto; WIDTH: 550px;' DESIGNTIMEDRAGDROP='740'>"
        html &= "<TABLE id='tabelaMovimentacao' style='MARGIN-TOP: 0px; MARGIN-BOTTOM: 0px; MARGIN-LEFT: 0px; BORDER-COLLAPSE: collapse'; borderColor='#cccccc' cellSpacing='0' cellPadding='0' width='540' border='1'>"
        html &= "<TR class='titulo-tabela sem-sublinhado'>"
        html &= "<TD>Nome Cobertura</TD>"
        html &= "<TD width='90'>Limite M�nimo</TD>"
        html &= "<TD width='120'>Limite M�ximo</TD>"
        html &= "</TR>"
        html &= coberturas
        html &= "</TABLE>"
        html &= "</DIV>"
        html &= "</td></tr></table>" & vbCrLf

        Return html

    End Function

    Public Function getCapitalByComponente(ByVal prop_cliente_id As Integer, ByVal tp_componente_id As Integer) As String

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        bd.dados_segurado_sps(prop_cliente_id, System.Web.HttpContext.Current.Session("apolice"), System.Web.HttpContext.Current.Session("ramo"), System.Web.HttpContext.Current.Session("subgrupo_id"))
        Dim dr As Data.SqlClient.SqlDataReader


        Try
            dr = bd.ExecutaSQL_DR()

            Dim val_capital_segurado As String = ""

            If dr.Read() Then
                val_capital_segurado = dr.Item("val_capital_segurado")
            End If

            bd.coberturasNew(System.Web.HttpContext.Current.Session("apolice"), System.Web.HttpContext.Current.Session("ramo"), 6785, 0, System.Web.HttpContext.Current.Session("subgrupo_id"), tp_componente_id)

            Dim class_tp_cobertura As String = ""
            Dim perc_basica As String = ""
            Dim tp_clausula_conjuge As String = ""

            dr = bd.ExecutaSQL_DR()
            If dr.Read() Then
                class_tp_cobertura = dr.Item("class_tp_cobertura")
                perc_basica = dr.Item("perc_basica")
            End If

            bd.getTpClausulaConjuge(System.Web.HttpContext.Current.Session("apolice"), System.Web.HttpContext.Current.Session("ramo"), System.Web.HttpContext.Current.Session("subgrupo_id"))

            dr = bd.ExecutaSQL_DR()
            If dr.Read() Then
                tp_clausula_conjuge = dr.Item("tp_clausula_conjuge")
            End If

            If tp_componente_id = "1" Then

                If class_tp_cobertura = "b" Then
                    Return cUtilitarios.trataMoeda(val_capital_segurado)
                Else
                    Return cUtilitarios.trataMoeda((val_capital_segurado * perc_basica) / 100)
                End If

            ElseIf tp_componente_id = "2" Then
                If tp_clausula_conjuge = "A" Or tp_clausula_conjuge = "F" Then
                    Return cUtilitarios.trataMoeda((val_capital_segurado * perc_basica) / 100)
                Else
                    Return cUtilitarios.trataMoeda(0)
                End If

            Else
                Return cUtilitarios.trataMoeda((val_capital_segurado * perc_basica) / 100)
            End If

        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        End Try

        Return cUtilitarios.trataMoeda(0)
    End Function

    Public Shared Function getCapByTpCobertura(ByVal prop_cliente_id As Integer, ByVal tp_componente_id As Integer, ByVal cobertura As String) As String

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        bd.dados_segurado_sps(prop_cliente_id, System.Web.HttpContext.Current.Session("apolice"), System.Web.HttpContext.Current.Session("ramo"), System.Web.HttpContext.Current.Session("subgrupo_id"))
        Dim dr As Data.SqlClient.SqlDataReader


        Try
            dr = bd.ExecutaSQL_DR()

            Dim val_capital_segurado As String = ""

            If dr.Read() Then
                val_capital_segurado = dr.Item("val_capital_segurado")
            End If

            bd.coberturasNew(System.Web.HttpContext.Current.Session("apolice"), System.Web.HttpContext.Current.Session("ramo"), 6785, 0, System.Web.HttpContext.Current.Session("subgrupo_id"), tp_componente_id, cobertura)

            Dim class_tp_cobertura As String = ""
            Dim perc_basica As String = ""
            Dim tp_clausula_conjuge As String = ""

            dr = bd.ExecutaSQL_DR()
            If dr.Read() Then
                class_tp_cobertura = dr.Item("class_tp_cobertura")
                perc_basica = dr.Item("perc_basica")
            End If

            bd.getTpClausulaConjuge(System.Web.HttpContext.Current.Session("apolice"), System.Web.HttpContext.Current.Session("ramo"), System.Web.HttpContext.Current.Session("subgrupo_id"))

            dr = bd.ExecutaSQL_DR()
            If dr.Read() Then
                tp_clausula_conjuge = dr.Item("tp_clausula_conjuge")
            End If

            If tp_componente_id = "1" Then

                If class_tp_cobertura = "b" Then
                    Return cUtilitarios.trataMoeda(val_capital_segurado)
                Else
                    Return cUtilitarios.trataMoeda((val_capital_segurado * perc_basica) / 100)
                End If

            ElseIf tp_componente_id = "2" Then
                If tp_clausula_conjuge = "A" Or tp_clausula_conjuge = "F" Then
                    Return cUtilitarios.trataMoeda((val_capital_segurado * perc_basica) / 100)
                Else
                    Return cUtilitarios.trataMoeda(0)
                End If

            Else
                Return cUtilitarios.trataMoeda((val_capital_segurado * perc_basica) / 100)
            End If

        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        End Try

        Return cUtilitarios.trataMoeda(0)
    End Function

    Public Shared Function getNumCertificado(ByVal cpf As String, ByVal usuario As String) As String

        Dim apolice As Integer = Web.HttpContext.Current.Session("apolice")
        Dim ramo As Integer = Web.HttpContext.Current.Session("ramo")
        Dim subgrupo_id As Integer = Web.HttpContext.Current.Session("subgrupo_id")

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        bd.segs0000_sps(apolice, ramo, subgrupo_id, cpf, usuario)

        Dim dt As Data.DataTable
        Dim result As Integer

        Try

            dt = bd.ExecutaSQL_DT

            If Not IsDBNull(dt.Rows(0).Item(0)) Then
                result = Int32.Parse(dt.Rows(0).Item(0))
            End If

        Catch ex As System.Exception

            Web.HttpContext.Current.Response.Write("<!-- ERRO:" & ex.Message & "-->")
            Web.HttpContext.Current.Response.Write("<!-- " & ex.StackTrace & "-->")
            Web.HttpContext.Current.Response.Write("<!-- " & bd.SQL & "-->")

        End Try

        Return result

    End Function

    Public Shared Function getCodSusep() As String

        Dim apolice As Integer = Web.HttpContext.Current.Session("apolice")
        Dim ramo As Integer = Web.HttpContext.Current.Session("ramo")
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dt As Data.DataTable
        Dim result As String = "0"

        bd.getCodSusep(apolice, ramo)

        Try

            dt = bd.ExecutaSQL_DT

            result = dt.Rows(0).Item(0)

            Return result

        Catch ex As System.Exception

            Web.HttpContext.Current.Response.Write("<!-- " & ex.Message & "-->")
            Web.HttpContext.Current.Response.Write("<!-- " & ex.StackTrace & "-->")
            Web.HttpContext.Current.Response.Write("<!-- " & bd.SQL & "-->")

        End Try

        Return result

    End Function


    Public Shared Function MostraColunaCapital() As Boolean

        Dim mostraCapital As Boolean = True

        Dim capitalSegurado As String = getCapitalSeguradoSession()

        Select Case capitalSegurado
            'Case "Capital Global"
            '    mostraCapital = False
            'Case "M�ltiplo sal�rio"
            '    mostraCapital = True
            'Case "Capital fixo"
            '    mostraCapital = True
            'Case "Capital informado"
            '    mostraCapital = True
            'Case Else
            '    mostraCapital = True
        End Select

        Return mostraCapital

    End Function


    Public Shared Function MostraColunaSalario() As Boolean

        Dim mostraSalario As Boolean = False

        Dim capitalSegurado As String = getCapitalSeguradoSession()

        Select Case capitalSegurado
            Case "M�ltiplo sal�rio"
                mostraSalario = True

                'Case "Capital Global"
                '    mostraCapital = False
                'Case "M�ltiplo sal�rio"
                '    mostraCapital = True
                'Case "Capital fixo"
                '    mostraCapital = True
                'Case "Capital informado"
                '    mostraCapital = True
                'Case Else
                '    mostraCapital = True
        End Select

        Return mostraSalario

    End Function
    Public Shared Function getCapitalSeguradoSession() As String

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5706_SPS(Web.HttpContext.Current.Session("apolice"), Web.HttpContext.Current.Session("ramo"), 6785, 0, Web.HttpContext.Current.Session("subgrupo_id"))

        Dim capitalSegurado As String = ""

        Try
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR
            Dim count As Int16 = 1

            dr.Read()
            capitalSegurado = dr.GetValue(1).ToString()

            dr.Close()
            dr = Nothing

        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        End Try

        Return capitalSegurado

    End Function
End Class
