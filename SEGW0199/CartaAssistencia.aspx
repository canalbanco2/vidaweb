<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CartaAssistencia.aspx.vb"
    Inherits="segw0071.CartaAssistencia" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Carta Assist�ncia</title>
    <link href="../segw0060/CSS/EstiloBase.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript">
        function Confirm() {
            var existeAlteracao;
            
            var hdn1 = document.getElementById('hdnNumAssisFuneral');
            var txt1 = document.getElementById('txtNumAssisFuneral'); 
            var hdn2 = document.getElementById('hdnAssisFuneral');
            var txt2 = document.getElementById('txtAssisFuneral'); 
            var hdn3 = document.getElementById('hdnCentralSinistro');
            var txt3 = document.getElementById('txtCentralSinistro');
            var hdn4 = document.getElementById('hdnCestaBasica');
            var txt4 = document.getElementById('txtCestaBasica');
            var hdn5 = document.getElementById('hdnLigacoesExterior');
            var txt5 = document.getElementById('txtLigacoesExterior');
            if((hdn1.value == txt1.value) && (hdn2.value == txt2.value) && (hdn3.value == txt3.value) && (hdn4.value == txt4.value) && (hdn5.value == txt5.value)) {
                existeAlteracao = 0
            }
            else{
                existeAlteracao = 1
            }
            
            if(existeAlteracao == 1) {
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";
                if (confirm("Confirma cancelar e perder as altera��es realizadas?")) {
                    confirm_value.value = "Yes";
                } else {
                    confirm_value.value = "No";
                }
                document.forms[0].appendChild(confirm_value);
            }
        }
    </script>

    <script language="javascript" type="text/javascript">
        function PrintImage() {
            printWindow = window.open("", "mywindow", "location=1,status=1,scrollbars=1,width=600,height=600");
            printWindow.document.write("<div style='width:100%;'>");
            printWindow.document.write("<img id='imgCartaoImpressao' src='" + document.getElementById('hdnCartaoImpressao').value + "' width='874' height='1238' />");
            printWindow.document.write("</div>");
            printWindow.document.close();
            printWindow.print();
        }        
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <p>
                <asp:Label ID="lblNavegacao" runat="server" CssClass="Caminhotela"></asp:Label>
                <br />
            </p>
            <asp:Panel ID="pnlBotoes" runat="server">
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnUploadLogo" runat="server" Text="Upload da Logomarca" Height="50"
                                Width="180" CssClass="Botao" />
                        </td>
                        <td>
                            <asp:Button ID="btnExclusaoLogo" runat="server" Text="Exclus�o da Logomarca" Height="50"
                                Width="180" CssClass="Botao" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnVisualizaCartao" runat="server" Text="Visualizar Cart�o" Height="50"
                                Width="180" CssClass="Botao" />
                        </td>
                        <td>
                            <asp:Button ID="btnTelefones" runat="server" Text="Telefones" Height="50" Width="180" CssClass="Botao" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <script type="text/javascript">
                    try{top.escondeaguarde();}catch(err){}
            </script>

            <asp:Panel ID="pnlUpload" runat="server" Visible="false">
                <table>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input class="Botao2" id="fileProcurar" type="file" size="45" name="fileProcurar"
                                runat="server" enableviewstate="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnCancelaImportacao" runat="server" Text="Cancela" CssClass="Botao" />
                        </td>
                        <td>
                            <asp:Button ID="btnImportar" runat="server" CssClass="Botao" Text="Enviar Arquivo" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnlTelefone" runat="server" Visible="false">
                <table>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblNumAssisFuneral" runat="server" Text="Assist�ncia Funeral:" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtNumAssisFuneral" runat="server" MaxLength="20" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblAssisFuneral" runat="server" Text="Texto Assist�ncia Funeral:" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtAssisFuneral" runat="server" MaxLength="20" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblCentralSinistro" runat="server" Text="Central Sinistros:" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtCentralSinistro" runat="server" MaxLength="20" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblCestaBasica" runat="server" Text="Cesta B�sica:" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtCestaBasica" runat="server" MaxLength="20" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblLigacoesExterior" runat="server" Text="Liga��es do Exterior:" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtLigacoesExterior" runat="server" MaxLength="20" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <center>
                                <asp:Button ID="btnCancelaTelefones" runat="server" Text="Cancela" OnClientClick="Confirm()" CssClass="Botao" />
                                <asp:Button ID="btnConfirmaTelefones" runat="server" Text="Confirma" CssClass="Botao" />
                            </center>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnlConfirmaExclusaoLogo" runat="server" Visible="false">
                <br />
                <table>
                    <tr>
                        <td colspan="2">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <center>
                                <asp:Label ID="lblTextoConfirmaExclusaoLogo" runat="server" Text="Confirma a exclus�o da logomarca?" />
                            </center>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <center>
                                <asp:Image ID="imgLogo" runat="server" />
                            </center>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <center>
                                <asp:Button ID="btnCancelaExclusaoLogo" runat="server" Text="Cancela" CssClass="Botao" />
                                <asp:Button ID="btnConfirmaExclusaoLogo" runat="server" Text="Confirma" CssClass="Botao" />
                            </center>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnlVisualizaCartao" runat="server" Visible="false">
                <br />
                <table>
                    <tr>
                        <td colspan="2">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <center>
                                <asp:Label ID="lbl" runat="server" Text="Deseja imprimir o cart�o?" />
                            </center>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <center>
                                <asp:Image ID="imgCartao" runat="server" Visible="false"/>
                            </center>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <center>
                                <asp:Button ID="btnCancelaImpressaoCartao" runat="server" Text="Cancela" CssClass="Botao" />
                                <asp:Button ID="btnConfirmaImpressaoCartao" runat="server" Text="Confirma" CssClass="Botao" />
                            </center>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:HiddenField ID="hdnNumAssisFuneral" runat="server" Value="" />
            <asp:HiddenField ID="hdnAssisFuneral" runat="server" Value="" />
            <asp:HiddenField ID="hdnCentralSinistro" runat="server" Value="" />
            <asp:HiddenField ID="hdnCestaBasica" runat="server" Value="" />
            <asp:HiddenField ID="hdnLigacoesExterior" runat="server" Value="" />
            <asp:HiddenField ID="hdnCartaoImpressao" runat="server" Value="" />
        </div>
    </form>
</body>
</html>
