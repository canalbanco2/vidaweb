Partial Public Class CartaAssistencia
    Inherits System.Web.UI.Page

    Dim bErro As Boolean
    Dim sMensagemErro As String
    'Dim strPath As String = "C:\\logomarcas\\"
    Dim strPath As String
    Dim sucursal_seguradora_id As Integer
    Dim seguradora_cod_susep As Integer
    Dim users As ArrayList
    Dim usuarioAutorizado As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Trace.Write("In�cio do Page Load")

        'Implementa��o do LinkSeguro
        Dim linkseguro As New Alianca.Seguranca.Web.LinkSeguro
        Dim usuario As String = linkseguro.LerUsuario("", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))

        Session("usuario_id") = linkseguro.Usuario_ID
        Session("usuario") = linkseguro.Login_WEB
        Session("cpf") = linkseguro.CPF

        Session("apolice") = Request("apolice")
        Session("ramo") = Request("ramo")
        Session("estipulante") = Request("estipulante")
        Session("subgrupo_id") = Request("subgrupo_id")
        Session("nomeSubgrupo") = Request("nomeSubgrupo")
        Session("CNPJCartao") = Request("CNPJCartao")
        Session("usuario") = Request("usuario").ToUpper()

        'cUtilitarios.escreveScript("alert(' USUARIO: " + Request("usuario").ToUpper()()() + "')")
        'Session("usuario") = "C_GFARIA"

        'Implementa��o do controle de ambiente
        Dim cAmbiente As New Alianca.Seguranca.Web.ControleAmbiente

        Try
            If Session("apolice") = "" Then
                Trace.Write("Apolice NULL na Sessao")
                cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
            End If
        Catch ex As Exception
            cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
            Response.End()
        End Try

        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
        cAmbiente.ObterAmbiente(url)
        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produ��o)
        Else '
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produ��o)
        End If

        Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)

        If Not Page.IsPostBack Then
            'Inicio do desenvolvimento da p�gina
            lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomeSubgrupo") & "<br>Fun��o: Impress�o Cart�o Assist�ncia"
        End If

        CarregaUsuarios()
        ConfiguraBotoes()
        RecuperaDiretorioLogomarcas(cAmbiente.Ambiente)
        Trace.Write("Fim do Page Load")

        'strPath = "~/logomarcas/"
        'Dim alert As String = MapPath(strPath)
        'Response.Write("caminho logomarcas: ")
        'Response.Write(alert)

        'cUtilitarios.escreveScript("alert('caminho logomarcas:')")
        'Dim alertJS As String = "alert(' " & alert & "')"
        'alertJS = alertJS.Replace("\", "\\")
        'cUtilitarios.escreveScript(alertJS)

    End Sub

    Private Sub CarregaUsuarios()
        users = New ArrayList
        Dim usuariosPermitidos As String = ConfigurationSettings.AppSettings("USUARIOSPERMITIDOS")
        Dim usuariosPermitidosArray As String() = usuariosPermitidos.Split(",")
        users.AddRange(usuariosPermitidosArray)

        'users.Add("RPROIETI")
        'users.Add("ANDREISILVA")
        'users.Add("KATIASOUZA")
        'users.Add("LAISSOUSA")
        'users.Add("AAGOMES")
        'users.Add("EDGSANTOS")
        'users.Add("C_CCARDOZO")
        'users.Add("ALGUIMARAES")
        'users.Add("C_FSHIDA")
        'users.Add("LUPALUDETO")
        'users.Add("EDUOLIVEIRA")
        'users.Add("EMORENO")
        'users.Add("C_MMACHADO")
        'users.Add("APAIVA")
        'users.Add("C_MBARRA")
        'users.Add("C_CAIOV")
        'users.Add("MFRASCA")
        'users.Add("RACRAS")
        'users.Add("GWERNECK")
        'users.Add("JOFILHO")
        'users.Add("C_DFREIT")
        'users.Add("C_GFARIA")
        'users.Add("C_EAPAULA")
    End Sub

    Private Function VerificaAcessoDiretorioLogomarcas() As Boolean
        If (Not String.IsNullOrEmpty(Session("GLAMBIENTE_ID"))) Then
            'verifica acesso ao diretorio de logomarcas
            If (RecuperaDiretorioLogomarcas(CInt(Session("GLAMBIENTE_ID")))) Then
                Try
                    'tenta ler um arquivo que sempre ir� ter na pasta
                    Dim logoBB As Bitmap = System.Drawing.Image.FromFile(ResolveUrl(String.Concat(MapPath(strPath), "logo_bb.jpg")))
                    Return True
                Catch ex As System.IO.DirectoryNotFoundException
                    Return False
                Catch ex As System.IO.FileNotFoundException
                    Return False
                Catch ex As Exception
                    Return False
                End Try
            End If
        Else
            cUtilitarios.escreveScript("alert('Vari�vel de ambiente n�o foi configurada corretamente!')")
        End If
    End Function


    Private Sub ConfiguraBotoes()
        'verifica se usuario est� na lista de usuarios
        'se nao tiver, Ao entrar na op��o do menu (Impress�o Cart�o de Assist�ncia) somente os bot�es [Visualizar Cart�o e Telefones] estar�o habilitados, os demais desabilitados.
        'Ao entrar na tela [Telefones] o bot�o [Confirma] estar� desabilitado, impossibilitando a manuten��o dos telefones para usu�rios n�o autorizados.
        If (users.Contains(Session("usuario"))) Then
            'recupera chave primaria da apolice
            'para depois verificar se j� tem logo associada
            'se n�o tiver, habilita o bot�o upload logo e desabilita o exclusao logo
            'se tiver, habilita exclusao e desabilita upload
            If (RecuperaChavePrimariaApolice(Session("apolice"), Session("ramo"), sucursal_seguradora_id, seguradora_cod_susep)) Then

                If (VerificaLogoAssociada(Session("apolice"), sucursal_seguradora_id, seguradora_cod_susep)) Then
                    btnUploadLogo.Enabled = False
                    btnExclusaoLogo.Enabled = True
                    Session("logoAssociada") = "1"
                Else
                    btnUploadLogo.Enabled = True
                    btnExclusaoLogo.Enabled = False
                    Session("logoAssociada") = "0"
                End If

                btnVisualizaCartao.Enabled = True
                btnTelefones.Enabled = True
                usuarioAutorizado = True
            End If
        Else
            RecuperaChavePrimariaApolice(Session("apolice"), Session("ramo"), sucursal_seguradora_id, seguradora_cod_susep)
            If (VerificaLogoAssociada(Session("apolice"), sucursal_seguradora_id, seguradora_cod_susep)) Then
                Session("logoAssociada") = "1"
            Else
                Session("logoAssociada") = "0"
            End If

            btnVisualizaCartao.Enabled = True
            btnTelefones.Enabled = True
            btnUploadLogo.Enabled = False
            btnExclusaoLogo.Enabled = False
            usuarioAutorizado = False

        End If

        
    End Sub

    Protected Sub btnUploadLogo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUploadLogo.Click
        Try
            DesabilitaBotoes()
            pnlUpload.Visible = True
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Protected Sub btnImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportar.Click
        Try
            If (String.IsNullOrEmpty(fileProcurar.PostedFile.FileName)) Then
                cUtilitarios.escreveScript("alert('Escolha um arquivo!');")
            ElseIf (fileProcurar.PostedFile.ContentType <> "image/pjpeg") Then
                cUtilitarios.escreveScript("alert('Formato de arquivo incorreto, s� s�o permitidos arquivos .JPEG!');")
            Else
                If (VerificaAcessoDiretorioLogomarcas()) Then
                    'upload da imagem para a pasta logomarcas
                    fileProcurar.PostedFile.SaveAs(String.Concat(MapPath(strPath), Session("apolice"), ".jpg"))

                    'redimensionamento da imagem
                    Dim bm As Bitmap = System.Drawing.Image.FromFile(String.Concat(MapPath(strPath), Session("apolice"), ".jpg"))
                    Dim Resized As Bitmap = New Bitmap(70, 35)
                    Dim g As Graphics = Graphics.FromImage(Resized)
                    g.DrawImage(bm, New Rectangle(0, 0, Resized.Width, Resized.Height), 0, 0, bm.Width, bm.Height, GraphicsUnit.Pixel)
                    Resized.Save(String.Concat(MapPath(strPath), Session("apolice"), "_thumbnail.jpg"))

                    'libera os objetos
                    g.Dispose()
                    bm.Dispose()
                    Resized.Dispose()

                    If (RecuperaDadosCartaoAssistencia(Session("apolice"), sucursal_seguradora_id, seguradora_cod_susep)) Then
                        'update
                        If (AtualizaLogoCartaoAssistencia(Session("apolice"), sucursal_seguradora_id, seguradora_cod_susep, 1)) Then
                            cUtilitarios.escreveScript("alert('Upload realizado com SUCESSO!');")
                        End If
                    Else
                        'insert
                        If (InsereLogoCartaoAssistencia(Session("apolice"), sucursal_seguradora_id, seguradora_cod_susep)) Then
                            cUtilitarios.escreveScript("alert('Upload realizado com SUCESSO!');")
                        Else
                            cUtilitarios.escreveScript("alert('ERRO no upload do arquivo!');")
                        End If
                    End If
                Else
                    cUtilitarios.escreveScript("alert('N�o foi poss�vel acessar o diret�rio de logomarcas!');")
                End If

            End If
        Catch ex As Exception
            Trace.Write("Erro m�todo btnImportar_Click")
            Trace.Write(ex.Message)
            cUtilitarios.escreveScript("alert('ERRO no upload do arquivo!');")
        Finally
            btnUploadLogo.Enabled = True
            btnExclusaoLogo.Enabled = True
            btnVisualizaCartao.Enabled = True
            btnTelefones.Enabled = True
            pnlUpload.Visible = False
            ConfiguraBotoes()
        End Try
    End Sub

    Protected Sub DesabilitaBotoes()
        btnUploadLogo.Enabled = False
        btnTelefones.Enabled = False
        btnExclusaoLogo.Enabled = False
        btnVisualizaCartao.Enabled = False
    End Sub

    Protected Sub btnTelefones_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTelefones.Click
        Try
            'recuperar dados j� parametrizados, se existirem
            If (RecuperaDadosCartaoAssistencia(Session("apolice"), sucursal_seguradora_id, seguradora_cod_susep)) Then
                txtNumAssisFuneral.Text = Session("assistencia_funeral")
                txtAssisFuneral.Text = Session("txt_assistencia_funeral")
                txtCentralSinistro.Text = Session("central_sinistros")
                txtCestaBasica.Text = Session("cesta_basica")
                txtLigacoesExterior.Text = Session("ligacoes_exterior")
            End If
            DesabilitaBotoes()
            pnlTelefone.Visible = True

            If (Not usuarioAutorizado) Then
                btnConfirmaTelefones.Enabled = False
                txtNumAssisFuneral.Enabled = False
                txtAssisFuneral.Enabled = False
                txtCentralSinistro.Enabled = False
                txtCestaBasica.Enabled = False
                txtLigacoesExterior.Enabled = False
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Protected Sub btnCancelaTelefones_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelaTelefones.Click
        Try
            Dim confirmValue As String = Request.Form("confirm_value")
            If confirmValue <> "No" Then
                pnlTelefone.Visible = False
                txtNumAssisFuneral.Text = IIf(String.IsNullOrEmpty(Session("assistencia_funeral")), String.Empty, Session("assistencia_funeral"))
                txtAssisFuneral.Text = IIf(String.IsNullOrEmpty(Session("txt_assistencia_funeral")), String.Empty, Session("txt_assistencia_funeral"))
                txtCentralSinistro.Text = IIf(String.IsNullOrEmpty(Session("central_sinistros")), String.Empty, Session("central_sinistros"))
                txtCestaBasica.Text = IIf(String.IsNullOrEmpty(Session("cesta_basica")), String.Empty, Session("cesta_basica"))
                txtLigacoesExterior.Text = IIf(String.IsNullOrEmpty(Session("ligacoes_exterior")), String.Empty, Session("ligacoes_exterior"))
            Else
                DesabilitaBotoes()
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Protected Sub btnExclusaoLogo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExclusaoLogo.Click
        Try
            If (VerificaAcessoDiretorioLogomarcas()) Then
                DesabilitaBotoes()
                'Dim f As New IO.FileInfo(String.Concat(MapPath(strPath), Session("apolice"), ".jpg"))
                imgLogo.ImageUrl = String.Concat(strPath, Session("apolice"), "_thumbnail.jpg")
                imgLogo.Width = 70
                imgLogo.Height = 35
                pnlConfirmaExclusaoLogo.Visible = True
            Else
                cUtilitarios.escreveScript("alert('N�o foi poss�vel acessar o diret�rio de logomarcas!');")
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Protected Sub btnConfirmaExclusaoLogo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmaExclusaoLogo.Click
        Try
            If (VerificaAcessoDiretorioLogomarcas()) Then
                'update para tirar a associa��o do cartao com a logo
                If (AtualizaLogoCartaoAssistencia(Session("apolice"), sucursal_seguradora_id, seguradora_cod_susep, 0)) Then
                    'exclui miniatura
                    Dim f As New IO.FileInfo(String.Concat(MapPath(strPath), Session("apolice"), "_thumbnail.jpg"))
                    f.Delete()
                    'exclui a imagem original
                    Dim fi As New IO.FileInfo(String.Concat(MapPath(strPath), Session("apolice"), ".jpg"))
                    fi.Delete()

                    Session("logoAssociada") = "0"
                    cUtilitarios.escreveScript("alert('Arquivo exclu�do com sucesso!')")
                Else
                    cUtilitarios.escreveScript("alert('ERRO ao excluir o arquivo!');")
                End If
            Else
                cUtilitarios.escreveScript("alert('N�o foi poss�vel acessar o diret�rio de logomarcas!');")
            End If
        Catch ex As Exception
            cUtilitarios.escreveScript("alert('ERRO ao excluir o arquivo!');")
        Finally
            pnlConfirmaExclusaoLogo.Visible = False
        End Try
        ConfiguraBotoes()
    End Sub

    Private Function RecuperaChavePrimariaApolice(ByVal apolice_id As Integer, ByVal ramo_id As Integer, _
                                                  ByRef sucursal_seguradora_id As Integer, _
                                                  ByRef seguradora_cod_susep As Integer) As Boolean

        Dim retorno As Boolean

        'Dim bd As New cAcompanhamento(eBanco.web_seguros_db)
        'Dim dt As Data.DataTable = Nothing

        'bd.GetChavePrimariaApolice(apolice_id, ramo_id)

        Try
            'dt = bd.ExecutaSQL_DT()

            'If dt.Rows.Count > 0 Then
            '    sucursal_seguradora_id = dt.Rows(0)("sucursal_seguradora_id").ToString()
            '    seguradora_cod_susep = dt.Rows(0)("seguradora_cod_susep").ToString()
            '    retorno = True
            'Else
            '    retorno = False
            'End If
            sucursal_seguradora_id = 0
            seguradora_cod_susep = 6785
            retorno = True
        Catch ex As Exception
            Dim excp As New clsException(ex)
            retorno = False
            'Finally
            '    dt.Dispose()
        End Try

        Return retorno

    End Function

    Private Function RecuperaDiretorioLogomarcas(ByVal ambiente_id As Integer) As Boolean
        Try
            strPath = "~/logomarcas/"
            Return True
        Catch ex As Exception
            Dim excp As New clsException(ex)
            Return False
        End Try

        'diretorio n�o ser� mais o de rede

        'Dim bd As New cAcompanhamento(eBanco.web_seguros_db)
        'Dim dt As Data.DataTable = Nothing

        'bd.GetDiretorioLogomarcas(ambiente_id)

        'Try
        '    dt = bd.ExecutaSQL_DT()

        '    If dt.Rows.Count > 0 Then
        '        strPath = dt.Rows(0)("valor").ToString()
        '        retorno = True
        '    Else
        '        retorno = False
        '    End If

        'Catch ex As Exception
        '    Dim excp As New clsException(ex)
        '    retorno = False
        'Finally
        '    dt.Dispose()
        'End Try
    End Function

    Private Function RecuperaDadosCartaoAssistencia(ByVal apolice_id As Integer, _
                                                    ByVal sucursal_seguradora_id As Integer, _
                                                    ByVal seguradora_cod_susep As Integer) As Boolean

        Dim retorno As Boolean

        Dim bd As New cAcompanhamento(eBanco.web_seguros_db)
        Dim dt As Data.DataTable = Nothing

        bd.GetDadosCartaoAssistencia(apolice_id, sucursal_seguradora_id, seguradora_cod_susep)

        Try
            dt = bd.ExecutaSQL_DT()

            '----------------------------------------------------------
            ' Autor: Leandro Amaral - Confitec                         
            ' Data: 29/08/2014
            ' Incidente: INC000004396117
            ' Descri��o Dados do cart�o estava sendo apresentado vazio 
            '----------------------------------------------------------
            ' IM00156693 - 08/12/2017 - INICIO
            If dt.Rows.Count > 0 Then
                Session("existe_dados_parametrizados") = "1"

                Session("assistencia_funeral") = dt.Rows(0)("assistencia_funeral").ToString()
                If String.IsNullOrEmpty(Session("assistencia_funeral")) Then
                    Session("assistencia_funeral") = "0800 707 7967"
                    Session("existe_dados_parametrizados") = "1"
                End If

                Session("txt_assistencia_funeral") = dt.Rows(0)("txt_assistencia_funeral").ToString()
                If String.IsNullOrEmpty(Session("txt_assistencia_funeral")) Then
                    Session("txt_assistencia_funeral") = "Op��o 1 � Pessoas"
                    Session("existe_dados_parametrizados") = "1"
                End If

                Session("central_sinistros") = dt.Rows(0)("central_sinistros").ToString()
                If String.IsNullOrEmpty(Session("central_sinistros")) Then
                    Session("central_sinistros") = "0800 729 7123"
                    Session("existe_dados_parametrizados") = "1"
                End If

                Session("cesta_basica") = dt.Rows(0)("cesta_basica").ToString()
                If String.IsNullOrEmpty(Session("cesta_basica")) Then
                    Session("cesta_basica") = "4134 0000"
                    Session("existe_dados_parametrizados") = "1"
                End If

                Session("ligacoes_exterior") = dt.Rows(0)("ligacoes_exterior").ToString()
                If String.IsNullOrEmpty(Session("ligacoes_exterior")) Then
                    Session("ligacoes_exterior") = "(55 11 ) 4134 5325"
                    Session("existe_dados_parametrizados") = "1"
                End If

                hdnNumAssisFuneral.Value = Session("assistencia_funeral")
                hdnAssisFuneral.Value = Session("txt_assistencia_funeral")
                hdnCentralSinistro.Value = Session("central_sinistros")
                hdnCestaBasica.Value = Session("cesta_basica")
                hdnLigacoesExterior.Value = Session("ligacoes_exterior")

                'Session("existe_dados_parametrizados") = "1"
                retorno = True
                ' IM00156693 - 08/12/2017 - FIM
            Else
                Session("existe_dados_parametrizados") = "0"

                Session("assistencia_funeral") = "0800 707 7967"
                Session("txt_assistencia_funeral") = "Op��o 1 � Pessoas"
                Session("central_sinistros") = "0800 729 7123"
                Session("cesta_basica") = "4134 0000"
                Session("ligacoes_exterior") = "(55 11 ) 4134 5325"

                hdnNumAssisFuneral.Value = Session("assistencia_funeral")
                hdnAssisFuneral.Value = Session("txt_assistencia_funeral")
                hdnCentralSinistro.Value = Session("central_sinistros")
                hdnCestaBasica.Value = Session("cesta_basica")
                hdnLigacoesExterior.Value = Session("ligacoes_exterior")

                txtNumAssisFuneral.Text = Session("assistencia_funeral")
                txtAssisFuneral.Text = Session("txt_assistencia_funeral")
                txtCentralSinistro.Text = Session("central_sinistros")
                txtCestaBasica.Text = Session("cesta_basica")
                txtLigacoesExterior.Text = Session("ligacoes_exterior")

                Return False
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
            retorno = False
        Finally
            dt.Dispose()
        End Try

        Return retorno

    End Function

    Private Function VerificaLogoAssociada(ByVal apolice_id As Integer, _
                                           ByVal sucursal_seguradora_id As Integer, _
                                           ByVal seguradora_cod_susep As Integer) As Boolean

        Dim retorno As Boolean
        Dim logo_associada As Integer

        Dim bd As New cAcompanhamento(eBanco.web_seguros_db)
        Dim dt As Data.DataTable = Nothing

        bd.GetLogoAssociadaApolice(apolice_id, sucursal_seguradora_id, seguradora_cod_susep)

        Try
            dt = bd.ExecutaSQL_DT()

            If dt.Rows.Count > 0 Then

                logo_associada = IIf(String.IsNullOrEmpty(dt.Rows(0)("logo_associada").ToString()), 0, dt.Rows(0)("logo_associada").ToString())

                If (logo_associada = 0) Then
                    retorno = False
                Else
                    retorno = True
                End If

            Else
                retorno = False
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
            retorno = False
        Finally
            dt.Dispose()
        End Try

        Return retorno

    End Function

    Private Function InsereDadosCartaoAssistencia(ByVal apolice_id As Integer, _
                                                  ByVal sucursal_seguradora_id As Integer, _
                                                  ByVal seguradora_cod_susep As Integer, _
                                                  ByVal assistencia_funeral As String, _
                                                  ByVal txt_assistencia_funeral As String, _
                                                  ByVal central_sinistros As String, _
                                                  ByVal cesta_basica As String, _
                                                  ByVal ligacoes_exterior As String, _
                                                  ByVal login_alteracao As String) As Boolean

        Dim retorno As Boolean

        Dim bd As New cAcompanhamento(eBanco.web_seguros_db)

        bd.SaveDadosCartaoAssitencia(apolice_id, sucursal_seguradora_id, seguradora_cod_susep, assistencia_funeral, _
                                     txt_assistencia_funeral, central_sinistros, cesta_basica, ligacoes_exterior, _
                                     login_alteracao)

        Try
            bd.ExecutaSQL()
            retorno = True
        Catch ex As Exception
            Dim excp As New clsException(ex)
            retorno = False
        End Try

        Return retorno

    End Function

    Private Function AtualizaDadosCartaoAssistencia(ByVal apolice_id As Integer, _
                                                    ByVal sucursal_seguradora_id As Integer, _
                                                    ByVal seguradora_cod_susep As Integer, _
                                                    ByVal assistencia_funeral As String, _
                                                    ByVal txt_assistencia_funeral As String, _
                                                    ByVal central_sinistros As String, _
                                                    ByVal cesta_basica As String, _
                                                    ByVal ligacoes_exterior As String, _
                                                    ByVal login_alteracao As String) As Boolean

        Dim retorno As Boolean

        Dim bd As New cAcompanhamento(eBanco.web_seguros_db)

        bd.UpdateDadosCartaoAssitencia(apolice_id, sucursal_seguradora_id, seguradora_cod_susep, assistencia_funeral, _
                                     txt_assistencia_funeral, central_sinistros, cesta_basica, ligacoes_exterior, _
                                     login_alteracao)

        Try
            bd.ExecutaSQL()
            retorno = True
        Catch ex As Exception
            Dim excp As New clsException(ex)
            retorno = False
        End Try

        Return retorno

    End Function

    Private Function AtualizaLogoCartaoAssistencia(ByVal apolice_id As Integer, _
                                                    ByVal sucursal_seguradora_id As Integer, _
                                                    ByVal seguradora_cod_susep As Integer, _
                                                    ByVal associa_ou_desassocia As Integer) As Boolean

        Dim retorno As Boolean

        Dim bd As New cAcompanhamento(eBanco.web_seguros_db)

        bd.UpdateLogoCartaoAssistencia(apolice_id, sucursal_seguradora_id, seguradora_cod_susep, associa_ou_desassocia)

        Try
            bd.ExecutaSQL()
            retorno = True
        Catch ex As Exception
            Dim excp As New clsException(ex)
            retorno = False
        End Try

        Return retorno

    End Function

    Protected Sub InsereDadosCartao()
        If (InsereDadosCartaoAssistencia(CInt(Session("apolice")), sucursal_seguradora_id, seguradora_cod_susep, _
                        txtNumAssisFuneral.Text, txtAssisFuneral.Text, txtCentralSinistro.Text, txtCestaBasica.Text, _
                        txtLigacoesExterior.Text, Session("usuario"))) Then
            cUtilitarios.escreveScript("alert('Informa��es atualizadas com sucesso!');")
        Else
            cUtilitarios.escreveScript("alert('Erro ao atualizar as informa��es!');")
        End If
    End Sub

    Protected Sub AtualizaDadosCartao()
        If (AtualizaDadosCartaoAssistencia(CInt(Session("apolice")), sucursal_seguradora_id, seguradora_cod_susep, _
                                        txtNumAssisFuneral.Text, txtAssisFuneral.Text, txtCentralSinistro.Text, txtCestaBasica.Text, _
                                        txtLigacoesExterior.Text, Session("usuario"))) Then
            cUtilitarios.escreveScript("alert('Informa��es atualizadas com sucesso!');")
        Else
            cUtilitarios.escreveScript("alert('Erro ao atualizar as informa��es!');")
        End If
    End Sub
    Protected Sub btnConfirmaTelefones_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmaTelefones.Click
        'valida se todos os campos foram preenchidos
        Try
            If ((String.IsNullOrEmpty(txtNumAssisFuneral.Text)) Or (String.IsNullOrEmpty(txtAssisFuneral.Text)) Or _
                (String.IsNullOrEmpty(txtCentralSinistro.Text)) Or (String.IsNullOrEmpty(txtCestaBasica.Text)) Or _
                (String.IsNullOrEmpty(txtLigacoesExterior.Text))) Then
                cUtilitarios.escreveScript("alert('Favor preencher todos os campos!');")
            Else
                If (String.IsNullOrEmpty(Session("existe_dados_parametrizados"))) Then
                    InsereDadosCartao()
                Else
                    If (Session("existe_dados_parametrizados").Equals("1")) Then
                        AtualizaDadosCartao()
                    Else
                        InsereDadosCartao()
                    End If
                End If
                pnlTelefone.Visible = False
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Private Function InsereLogoCartaoAssistencia(ByVal apolice_id As Integer, _
                                                 ByVal sucursal_seguradora_id As Integer, _
                                                 ByVal seguradora_cod_susep As Integer) As Boolean

        Dim retorno As Boolean

        Dim bd As New cAcompanhamento(eBanco.web_seguros_db)

        bd.SaveLogoCartaoAssistencia(apolice_id, sucursal_seguradora_id, seguradora_cod_susep)

        Try
            bd.ExecutaSQL()
            retorno = True
        Catch ex As Exception
            Dim excp As New clsException(ex)
            retorno = False
        End Try

        Return retorno

    End Function

    Protected Sub btnCancelaExclusaoLogo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelaExclusaoLogo.Click
        pnlConfirmaExclusaoLogo.Visible = False
    End Sub

    Private Function RecuperaAssistenciasApolice(ByVal apolice_id As Integer, _
                                                 ByVal sucursal_seguradora_id As Integer, _
                                                 ByVal seguradora_cod_susep As Integer, _
                                                 ByVal subgrupo_id As Integer, _
                                                 ByRef assistencias As ArrayList) As Boolean

        Dim retorno As Boolean
        assistencias = New ArrayList

        Dim bd As New cAcompanhamento(eBanco.web_seguros_db)
        Dim dr As Data.SqlClient.SqlDataReader

        bd.GetAssistenciasApolice(apolice_id, sucursal_seguradora_id, seguradora_cod_susep, subgrupo_id)

        Try
            dr = bd.ExecutaSQL_DR()

            If dr.HasRows Then

                While dr.Read()
                    assistencias.Add(dr.GetValue(0).ToString())
                End While

                retorno = True
            Else
                retorno = False
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
            retorno = False
        End Try

        Return retorno

    End Function

    Protected Sub btnVisualizaCartao_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVisualizaCartao.Click
        If (VerificaAcessoDiretorioLogomarcas()) Then
            'verifica se a Ap�lice e o subgrupo selecionado tem �Assist�ncia Funeral� e/ou �Assist�ncia Cesta B�sica�.
            DesabilitaBotoes()

            Dim assistencias As ArrayList = New ArrayList

            If (RecuperaAssistenciasApolice(CInt(Session("apolice")), sucursal_seguradora_id, seguradora_cod_susep, _
                                            CInt(Session("subgrupo_id")), assistencias)) Then
                ConfiguraCartao(assistencias)
                pnlVisualizaCartao.Visible = True
            Else
                cUtilitarios.escreveScript("alert('N�o h� assist�ncias para impress�o do Cart�o de Assist�ncia');")
                ConfiguraBotoes()
            End If
        Else
            cUtilitarios.escreveScript("alert('N�o foi poss�vel acessar o diret�rio de logomarcas!');")
        End If

    End Sub

    'Protected Sub ConfiguraCartao(ByVal assistencias As ArrayList)
    '    'strPath = strPath.Replace("\\", "\")
    '    Try
    '        Dim strAssistenciaFuneral As String = "Assist�ncia Funeral"
    '        Dim numAssistenciaFuneral As String
    '        Dim txtAssistenciaFuneral As String
    '        Dim strCestaBasica As String = "Cesta B�sica"
    '        Dim numCestaBasica As String
    '        Dim strCNPJ As String = "CNPJ"
    '        Dim numCNPJ As String
    '        Dim strApolice As String = "Ap�lice"
    '        Dim numApolice As String
    '        Dim strLigCobrarExt As String = "Para liga��es a cobrar do exterior:"
    '        Dim numLigCobrarExt As String
    '        Dim strCentralSinistros As String = "Central de Sinistros"
    '        Dim numCentralSinistros As String
    '        Dim txtGrupoSegurador As String = "GRUPO SEGURADOR"

    '        Dim logoBB As Bitmap = System.Drawing.Image.FromFile(ResolveUrl(String.Concat(MapPath(strPath), "logo_bb.jpg")))
    '        Dim logoMapfre As Bitmap = System.Drawing.Image.FromFile(ResolveUrl(String.Concat(MapPath(strPath), "logo_mapfre.jpg")))
    '        Dim logoApolice As Bitmap

    '        If (Not String.IsNullOrEmpty(Session("logoAssociada"))) Then
    '            If (Session("logoAssociada").Equals("1")) Then
    '                logoApolice = System.Drawing.Image.FromFile(ResolveUrl(String.Concat(MapPath(strPath), Session("apolice"), "_thumbnail.jpg")))
    '            End If
    '        End If

    '        Dim img As Bitmap = System.Drawing.Image.FromFile(ResolveUrl(String.Concat(MapPath(strPath), "layout_cartao.JPG")))
    '        Dim objectoFonte12 As New Font("Tahoma", 12)
    '        Dim objectoFonte10 As New Font("Tahoma", 10)
    '        Dim objectoFonte9 As New Font("Tahoma", 9)
    '        Dim objectoFonte8 As New Font("Tahoma", 8)
    '        Dim objectoFonte6 As New Font("Tahoma", 6)

    '        RecuperaDadosCartaoAssistencia(Session("apolice"), sucursal_seguradora_id, seguradora_cod_susep)

    '        numAssistenciaFuneral = IIf(String.IsNullOrEmpty(Session("assistencia_funeral")), String.Empty, Session("assistencia_funeral"))
    '        txtAssistenciaFuneral = IIf(String.IsNullOrEmpty(Session("txt_assistencia_funeral")), String.Empty, Session("txt_assistencia_funeral"))
    '        numCestaBasica = IIf(String.IsNullOrEmpty(Session("cesta_basica")), String.Empty, Session("cesta_basica"))

    '        If (Not String.IsNullOrEmpty(Session("CNPJCartao"))) Then
    '            numCNPJ = String.Format("{0:00\.000\.000\/0000\-00}", Long.Parse(Session("CNPJCartao")))
    '        Else
    '            numCNPJ = String.Empty
    '        End If

    '        numApolice = IIf(String.IsNullOrEmpty(Session("apolice")), String.Empty, Session("apolice"))
    '        numLigCobrarExt = IIf(String.IsNullOrEmpty(Session("ligacoes_exterior")), String.Empty, Session("ligacoes_exterior"))
    '        numCentralSinistros = IIf(String.IsNullOrEmpty(Session("central_sinistros")), String.Empty, Session("central_sinistros"))

    '        '---------------
    '        'INC000004448429
    '        '---------------
    '        'se tiver ass. funeral e ass. cesta basica
    '        If (assistencias.Count = 2) Then
    '            Try
    '                strAssistenciaFuneral = "Assist�ncia Funeral"

    '                If assistencias.Contains("1") Then
    '                    strAssistenciaFuneral = "Assist�ncia 24 Horas"
    '                End If
    '            Catch ex As Exception
    '                strAssistenciaFuneral = "Assist�ncia Funeral"
    '            End Try

    '            Using g As Graphics = Graphics.FromImage(img)
    '                g.DrawString(strAssistenciaFuneral, objectoFonte12, Brushes.Black, 80, 135)
    '                g.DrawString(numAssistenciaFuneral, objectoFonte12, Brushes.Black, 95, 158)
    '                g.DrawString(txtAssistenciaFuneral, objectoFonte8, Brushes.Black, 105, 180)
    '                g.DrawLine(Pens.Black, 240, 135, 240, 195)
    '                g.DrawString(strCestaBasica, objectoFonte12, Brushes.Black, 275, 142)
    '                g.DrawString(numCestaBasica, objectoFonte12, Brushes.Black, 277, 167)
    '                g.DrawString(strCNPJ, objectoFonte10, Brushes.Black, 90, 200)
    '                g.DrawString(numCNPJ, objectoFonte10, Brushes.Black, 125, 200)
    '                g.DrawString(strApolice, objectoFonte10, Brushes.Black, 270, 200)
    '                g.DrawString(numApolice, objectoFonte10, Brushes.Black, 320, 200)
    '                g.DrawString(strLigCobrarExt, objectoFonte9, Brushes.Black, 80, 218)
    '                g.DrawString(numLigCobrarExt, objectoFonte9, Brushes.Black, 270, 218)
    '                If (String.IsNullOrEmpty(Session("logoAssociada"))) Then
    '                    g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 205, 238)
    '                    g.DrawImage(logoBB, 157, 252)
    '                    g.DrawImage(logoMapfre, 244, 252)
    '                ElseIf (Session("logoAssociada").Equals("1")) Then
    '                    g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 125, 236)
    '                    g.DrawImage(logoBB, 82, 250)
    '                    g.DrawImage(logoMapfre, 169, 250)
    '                    g.DrawImage(logoApolice, 323, 237)
    '                Else
    '                    g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 205, 238)
    '                    g.DrawImage(logoBB, 157, 252)
    '                    g.DrawImage(logoMapfre, 244, 252)
    '                End If
    '            End Using
    '        ElseIf (assistencias.Contains("4")) Then 'ASSIST�NCIA FUNERAL
    '            Using g As Graphics = Graphics.FromImage(img)
    '                g.DrawString(strAssistenciaFuneral, objectoFonte12, Brushes.Black, 80, 135)
    '                g.DrawString(numAssistenciaFuneral, objectoFonte12, Brushes.Black, 95, 158)
    '                g.DrawString(txtAssistenciaFuneral, objectoFonte8, Brushes.Black, 105, 180)
    '                g.DrawLine(Pens.Black, 240, 135, 240, 195)
    '                g.DrawString(strCentralSinistros, objectoFonte12, Brushes.Black, 260, 142)
    '                g.DrawString(numCentralSinistros, objectoFonte12, Brushes.Black, 277, 167)
    '                g.DrawString(strCNPJ, objectoFonte10, Brushes.Black, 90, 200)
    '                g.DrawString(numCNPJ, objectoFonte10, Brushes.Black, 125, 200)
    '                g.DrawString(strApolice, objectoFonte10, Brushes.Black, 270, 200)
    '                g.DrawString(numApolice, objectoFonte10, Brushes.Black, 320, 200)
    '                g.DrawString(strLigCobrarExt, objectoFonte9, Brushes.Black, 80, 218)
    '                g.DrawString(numLigCobrarExt, objectoFonte9, Brushes.Black, 270, 218)

    '                If (String.IsNullOrEmpty(Session("logoAssociada"))) Then
    '                    g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 205, 238)
    '                    g.DrawImage(logoBB, 157, 252)
    '                    g.DrawImage(logoMapfre, 244, 252)
    '                ElseIf (Session("logoAssociada").Equals("1")) Then
    '                    g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 125, 236)
    '                    g.DrawImage(logoBB, 82, 250)
    '                    g.DrawImage(logoMapfre, 169, 250)
    '                    g.DrawImage(logoApolice, 323, 237)
    '                Else
    '                    g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 205, 238)
    '                    g.DrawImage(logoBB, 157, 252)
    '                    g.DrawImage(logoMapfre, 244, 252)
    '                End If
    '            End Using
    '        ElseIf (assistencias.Contains("5")) Then 'ASSIST�NCIA CESTA B�SICA
    '            Using g As Graphics = Graphics.FromImage(img)
    '                g.DrawString(strCestaBasica, objectoFonte12, Brushes.Black, 110, 142)
    '                g.DrawString(numCestaBasica, objectoFonte12, Brushes.Black, 115, 165)
    '                g.DrawLine(Pens.Black, 240, 135, 240, 195)
    '                g.DrawString(strCentralSinistros, objectoFonte12, Brushes.Black, 260, 142)
    '                g.DrawString(numCentralSinistros, objectoFonte12, Brushes.Black, 272, 165)
    '                g.DrawString(strCNPJ, objectoFonte10, Brushes.Black, 90, 200)
    '                g.DrawString(numCNPJ, objectoFonte10, Brushes.Black, 125, 200)
    '                g.DrawString(strApolice, objectoFonte10, Brushes.Black, 270, 200)
    '                g.DrawString(numApolice, objectoFonte10, Brushes.Black, 320, 200)
    '                g.DrawString(strLigCobrarExt, objectoFonte9, Brushes.Black, 80, 218)
    '                g.DrawString(numLigCobrarExt, objectoFonte9, Brushes.Black, 270, 218)

    '                If (String.IsNullOrEmpty(Session("logoAssociada"))) Then
    '                    g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 205, 238)
    '                    g.DrawImage(logoBB, 157, 252)
    '                    g.DrawImage(logoMapfre, 244, 252)
    '                ElseIf (Session("logoAssociada").Equals("1")) Then
    '                    g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 125, 236)
    '                    g.DrawImage(logoBB, 82, 250)
    '                    g.DrawImage(logoMapfre, 169, 250)
    '                    g.DrawImage(logoApolice, 323, 237)
    '                Else
    '                    g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 205, 238)
    '                    g.DrawImage(logoBB, 157, 252)
    '                    g.DrawImage(logoMapfre, 244, 252)
    '                End If
    '            End Using
    '        ElseIf (assistencias.Contains("1")) Then 'ASSIST�NCIA 24 HORAS
    '            Using g As Graphics = Graphics.FromImage(img)
    '                g.DrawString("Assist�ncia 24 Horas", objectoFonte12, Brushes.Black, 80, 135)
    '                g.DrawString(numAssistenciaFuneral, objectoFonte12, Brushes.Black, 95, 158)
    '                g.DrawString(txtAssistenciaFuneral, objectoFonte8, Brushes.Black, 105, 180)
    '                g.DrawLine(Pens.Black, 240, 135, 240, 195)
    '                g.DrawString(strCentralSinistros, objectoFonte12, Brushes.Black, 260, 142)
    '                g.DrawString(numCentralSinistros, objectoFonte12, Brushes.Black, 277, 167)
    '                g.DrawString(strCNPJ, objectoFonte10, Brushes.Black, 90, 200)
    '                g.DrawString(numCNPJ, objectoFonte10, Brushes.Black, 125, 200)
    '                g.DrawString(strApolice, objectoFonte10, Brushes.Black, 270, 200)
    '                g.DrawString(numApolice, objectoFonte10, Brushes.Black, 320, 200)
    '                g.DrawString(strLigCobrarExt, objectoFonte9, Brushes.Black, 80, 218)
    '                g.DrawString(numLigCobrarExt, objectoFonte9, Brushes.Black, 270, 218)

    '                If (String.IsNullOrEmpty(Session("logoAssociada"))) Then
    '                    g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 205, 238)
    '                    g.DrawImage(logoBB, 157, 252)
    '                    g.DrawImage(logoMapfre, 244, 252)
    '                ElseIf (Session("logoAssociada").Equals("1")) Then
    '                    g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 125, 236)
    '                    g.DrawImage(logoBB, 82, 250)
    '                    g.DrawImage(logoMapfre, 169, 250)
    '                    g.DrawImage(logoApolice, 323, 237)
    '                Else
    '                    g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 205, 238)
    '                    g.DrawImage(logoBB, 157, 252)
    '                    g.DrawImage(logoMapfre, 244, 252)
    '                End If
    '            End Using
    '        End If
    '        '---------------
    '        'INC000004448429
    '        '---------------

    '        'cria o diretorio para a apolice
    '        Dim DirDiretorio As System.IO.DirectoryInfo
    '        Try
    '            DirDiretorio = New System.IO.DirectoryInfo(MapPath(String.Concat(strPath, Session("apolice"))))
    '            DirDiretorio.Create()
    '        Catch ex As Exception
    '            cUtilitarios.escreveScript("alert('Sem permiss�o de escrita no diret�rio!')")
    '        End Try

    '        'limpa o diretorio da apolice
    '        Dim oFileInfoCollection() As System.IO.FileInfo
    '        Dim oFileInfo As System.IO.FileInfo
    '        Try
    '            oFileInfoCollection = DirDiretorio.GetFiles("*.jpg")
    '            Dim i As Integer
    '            For i = 0 To oFileInfoCollection.Length() - 1
    '                oFileInfo = oFileInfoCollection.GetValue(i)
    '                System.IO.File.Delete(oFileInfo.FullName)
    '            Next
    '        Catch ex As Exception
    '            cUtilitarios.escreveScript("alert('Sem permiss�o para excluir arquivos no diret�rio!')")
    '        End Try

    '        'salva imagem do cartao no diretorio da apolice
    '        Dim random As Random = New Random()
    '        Dim nomeImagemCartao As String = random.Next()
    '        Try
    '            img.Save(ResolveUrl(String.Concat(MapPath(String.Concat(strPath, Session("apolice"), "/")), nomeImagemCartao, ".jpg")), System.Drawing.Imaging.ImageFormat.Jpeg)
    '        Catch ex As Exception
    '            cUtilitarios.escreveScript("alert('Sem permiss�o para gravar o arquivo do cart�o no diret�rio!')")
    '        End Try

    '        imgCartao.ImageUrl = ResolveUrl(String.Concat(String.Concat(strPath, Session("apolice"), "/"), nomeImagemCartao, ".jpg"))
    '        imgCartao.Width = 437
    '        imgCartao.Height = 307
    '        imgCartao.Visible = True

    '        logoBB.Dispose()
    '        logoMapfre.Dispose()
    '        If (Not String.IsNullOrEmpty(Session("logoAssociada"))) Then
    '            If (Session("logoAssociada").Equals("1")) Then
    '                logoApolice.Dispose()
    '            End If
    '        End If

    '        Dim MergedImage As System.Drawing.Image
    '        Dim bm As New Bitmap(874, 1238)
    '        Dim gr As Graphics = Graphics.FromImage(bm)
    '        gr.DrawImage(img, 0, 0)
    '        gr.DrawImage(img, 437, 0)
    '        gr.DrawImage(img, 0, 307)
    '        gr.DrawImage(img, 437, 307)
    '        gr.DrawImage(img, 0, 614)
    '        gr.DrawImage(img, 437, 614)
    '        gr.DrawImage(img, 0, 921)
    '        gr.DrawImage(img, 437, 921)
    '        MergedImage = bm
    '        gr.Dispose()
    '        img.Dispose()

    '        'salva a imagem de impressao no diretorio da apolice
    '        Dim nomeImagemCartaoA4 As String = random.Next()
    '        Try
    '            MergedImage.Save(ResolveUrl(String.Concat(MapPath(String.Concat(strPath, Session("apolice"), "/")), nomeImagemCartaoA4, ".jpg")), System.Drawing.Imaging.ImageFormat.Jpeg)
    '            Session("pathCartao") = ResolveUrl(String.Concat(String.Concat(strPath, Session("apolice"), "/"), nomeImagemCartaoA4, ".jpg"))
    '        Catch ex As Exception
    '            cUtilitarios.escreveScript("alert('Sem permiss�o para gravar o arquivo de impress�o no diret�rio!')")
    '        End Try

    '        MergedImage.Dispose()
    '    Catch ex As Exception
    '        cUtilitarios.escreveScript("alert('Erro ao configurar o cart�o!')")
    '    End Try
    'End Sub

    Protected Sub ConfiguraCartao(ByVal assistencias As ArrayList)
        'strPath = strPath.Replace("\\", "\")
        Try
            Dim strAssistenciaFuneral As String = "Assist�ncia Funeral"
            Dim numAssistenciaFuneral As String
            Dim txtAssistenciaFuneral As String
            Dim strCestaBasica As String = "Cesta B�sica"
            Dim numCestaBasica As String
            Dim strCNPJ As String = "CNPJ"
            Dim numCNPJ As String
            Dim strApolice As String = "Ap�lice"
            Dim numApolice As String
            Dim strLigCobrarExt As String = "Para liga��es a cobrar do exterior:"
            Dim numLigCobrarExt As String
            Dim strCentralSinistros As String = "Central de Sinistros"
            Dim numCentralSinistros As String
            Dim txtGrupoSegurador As String = "GRUPO SEGURADOR"

            Dim logoBB As Bitmap = System.Drawing.Image.FromFile(ResolveUrl(String.Concat(MapPath(strPath), "logo_bb.jpg")))
            Dim logoMapfre As Bitmap = System.Drawing.Image.FromFile(ResolveUrl(String.Concat(MapPath(strPath), "logo_mapfre.jpg")))
            Dim logoApolice As Bitmap

            If (Not String.IsNullOrEmpty(Session("logoAssociada"))) Then
                If (Session("logoAssociada").Equals("1")) Then
                    logoApolice = System.Drawing.Image.FromFile(ResolveUrl(String.Concat(MapPath(strPath), Session("apolice"), "_thumbnail.jpg")))
                End If
            End If

            Dim img As Bitmap = System.Drawing.Image.FromFile(ResolveUrl(String.Concat(MapPath(strPath), "layout_cartao.JPG")))
            Dim objectoFonte12 As New Font("Tahoma", 12)
            Dim objectoFonte10 As New Font("Tahoma", 10)
            Dim objectoFonte9 As New Font("Tahoma", 9)
            Dim objectoFonte8 As New Font("Tahoma", 8)
            Dim objectoFonte6 As New Font("Tahoma", 6)

            RecuperaDadosCartaoAssistencia(Session("apolice"), sucursal_seguradora_id, seguradora_cod_susep)

            numAssistenciaFuneral = IIf(String.IsNullOrEmpty(Session("assistencia_funeral")), String.Empty, Session("assistencia_funeral"))
            txtAssistenciaFuneral = IIf(String.IsNullOrEmpty(Session("txt_assistencia_funeral")), String.Empty, Session("txt_assistencia_funeral"))
            numCestaBasica = IIf(String.IsNullOrEmpty(Session("cesta_basica")), String.Empty, Session("cesta_basica"))

            If (Not String.IsNullOrEmpty(Session("CNPJCartao"))) Then
                numCNPJ = String.Format("{0:00\.000\.000\/0000\-00}", Long.Parse(Session("CNPJCartao")))
            Else
                numCNPJ = String.Empty
            End If

            numApolice = IIf(String.IsNullOrEmpty(Session("apolice")), String.Empty, Session("apolice"))
            numLigCobrarExt = IIf(String.IsNullOrEmpty(Session("ligacoes_exterior")), String.Empty, Session("ligacoes_exterior"))
            numCentralSinistros = IIf(String.IsNullOrEmpty(Session("central_sinistros")), String.Empty, Session("central_sinistros"))

            '---------------
            'INC000004448429
            '---------------
            Dim lBln2Assist As Integer
            Dim lStrAssist As String

            Dim lIntConta As Integer

            lBln2Assist = False

            If assistencias.Count > 1 Then
                lStrAssist = ";"
                For lIntConta = 0 To (assistencias.Count - 1)
                    lStrAssist = lStrAssist + assistencias.Item(lIntConta).ToString().Trim() & ";"
                Next lIntConta

                lBln2Assist = False
                'SE APOLICE TIVER CESTA BASICA E ASSISTENCIA 24 HORAS OU ASSISTENCIA FUNERAL
                If InStr(lStrAssist, ";5;") > 0 And (InStr(lStrAssist, ";1;") > 0 Or InStr(lStrAssist, ";4;") > 0) Then
                    lBln2Assist = True
                End If
            End If

            If lBln2Assist Then
                Using g As Graphics = Graphics.FromImage(img)
                    g.DrawString(strAssistenciaFuneral, objectoFonte12, Brushes.Black, 80, 135)
                    g.DrawString(numAssistenciaFuneral, objectoFonte12, Brushes.Black, 95, 158)
                    g.DrawString(txtAssistenciaFuneral, objectoFonte8, Brushes.Black, 105, 180)
                    g.DrawLine(Pens.Black, 240, 135, 240, 195)
                    g.DrawString(strCestaBasica, objectoFonte12, Brushes.Black, 275, 142)
                    g.DrawString(numCestaBasica, objectoFonte12, Brushes.Black, 277, 167)
                    g.DrawString(strCNPJ, objectoFonte10, Brushes.Black, 90, 200)
                    g.DrawString(numCNPJ, objectoFonte10, Brushes.Black, 125, 200)
                    g.DrawString(strApolice, objectoFonte10, Brushes.Black, 270, 200)
                    g.DrawString(numApolice, objectoFonte10, Brushes.Black, 320, 200)
                    g.DrawString(strLigCobrarExt, objectoFonte9, Brushes.Black, 80, 218)
                    g.DrawString(numLigCobrarExt, objectoFonte9, Brushes.Black, 270, 218)
                    If (String.IsNullOrEmpty(Session("logoAssociada"))) Then
                        g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 205, 238)
                        g.DrawImage(logoBB, 157, 252)
                        g.DrawImage(logoMapfre, 244, 252)
                    ElseIf (Session("logoAssociada").Equals("1")) Then
                        g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 125, 236)
                        g.DrawImage(logoBB, 82, 250)
                        g.DrawImage(logoMapfre, 169, 250)
                        g.DrawImage(logoApolice, 323, 237)
                    Else
                        g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 205, 238)
                        g.DrawImage(logoBB, 157, 252)
                        g.DrawImage(logoMapfre, 244, 252)
                    End If
                End Using
            Else
                If (assistencias.Contains("1")) Or (assistencias.Contains("4")) Then 'ASSIST�NCIA FUNERAL OU ASSIST�NCIA 24 HORAS
                    Using g As Graphics = Graphics.FromImage(img)
                        g.DrawString(strAssistenciaFuneral, objectoFonte12, Brushes.Black, 80, 135)
                        g.DrawString(numAssistenciaFuneral, objectoFonte12, Brushes.Black, 95, 158)
                        g.DrawString(txtAssistenciaFuneral, objectoFonte8, Brushes.Black, 105, 180)
                        g.DrawLine(Pens.Black, 240, 135, 240, 195)
                        g.DrawString(strCentralSinistros, objectoFonte12, Brushes.Black, 260, 142)
                        g.DrawString(numCentralSinistros, objectoFonte12, Brushes.Black, 277, 167)
                        g.DrawString(strCNPJ, objectoFonte10, Brushes.Black, 90, 200)
                        g.DrawString(numCNPJ, objectoFonte10, Brushes.Black, 125, 200)
                        g.DrawString(strApolice, objectoFonte10, Brushes.Black, 270, 200)
                        g.DrawString(numApolice, objectoFonte10, Brushes.Black, 320, 200)
                        g.DrawString(strLigCobrarExt, objectoFonte9, Brushes.Black, 80, 218)
                        g.DrawString(numLigCobrarExt, objectoFonte9, Brushes.Black, 270, 218)

                        If (String.IsNullOrEmpty(Session("logoAssociada"))) Then
                            g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 205, 238)
                            g.DrawImage(logoBB, 157, 252)
                            g.DrawImage(logoMapfre, 244, 252)
                        ElseIf (Session("logoAssociada").Equals("1")) Then
                            g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 125, 236)
                            g.DrawImage(logoBB, 82, 250)
                            g.DrawImage(logoMapfre, 169, 250)
                            g.DrawImage(logoApolice, 323, 237)
                        Else
                            g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 205, 238)
                            g.DrawImage(logoBB, 157, 252)
                            g.DrawImage(logoMapfre, 244, 252)
                        End If
                    End Using
                ElseIf (assistencias.Contains("5")) Then 'ASSIST�NCIA CESTA B�SICA
                    Using g As Graphics = Graphics.FromImage(img)
                        g.DrawString(strCestaBasica, objectoFonte12, Brushes.Black, 110, 142)
                        g.DrawString(numCestaBasica, objectoFonte12, Brushes.Black, 115, 165)
                        g.DrawLine(Pens.Black, 240, 135, 240, 195)
                        g.DrawString(strCentralSinistros, objectoFonte12, Brushes.Black, 260, 142)
                        g.DrawString(numCentralSinistros, objectoFonte12, Brushes.Black, 272, 165)
                        g.DrawString(strCNPJ, objectoFonte10, Brushes.Black, 90, 200)
                        g.DrawString(numCNPJ, objectoFonte10, Brushes.Black, 125, 200)
                        g.DrawString(strApolice, objectoFonte10, Brushes.Black, 270, 200)
                        g.DrawString(numApolice, objectoFonte10, Brushes.Black, 320, 200)
                        g.DrawString(strLigCobrarExt, objectoFonte9, Brushes.Black, 80, 218)
                        g.DrawString(numLigCobrarExt, objectoFonte9, Brushes.Black, 270, 218)

                        If (String.IsNullOrEmpty(Session("logoAssociada"))) Then
                            g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 205, 238)
                            g.DrawImage(logoBB, 157, 252)
                            g.DrawImage(logoMapfre, 244, 252)
                        ElseIf (Session("logoAssociada").Equals("1")) Then
                            g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 125, 236)
                            g.DrawImage(logoBB, 82, 250)
                            g.DrawImage(logoMapfre, 169, 250)
                            g.DrawImage(logoApolice, 323, 237)
                        Else
                            g.DrawString(txtGrupoSegurador, objectoFonte6, Brushes.Black, 205, 238)
                            g.DrawImage(logoBB, 157, 252)
                            g.DrawImage(logoMapfre, 244, 252)
                        End If
                    End Using
                End If
            End If

            'cria o diretorio para a apolice
            Dim DirDiretorio As System.IO.DirectoryInfo
            Try
                DirDiretorio = New System.IO.DirectoryInfo(MapPath(String.Concat(strPath, Session("apolice"))))
                DirDiretorio.Create()
            Catch ex As Exception
                cUtilitarios.escreveScript("alert('Sem permiss�o de escrita no diret�rio!')")
            End Try

            'limpa o diretorio da apolice
            Dim oFileInfoCollection() As System.IO.FileInfo
            Dim oFileInfo As System.IO.FileInfo
            Try
                oFileInfoCollection = DirDiretorio.GetFiles("*.jpg")
                Dim i As Integer
                For i = 0 To oFileInfoCollection.Length() - 1
                    oFileInfo = oFileInfoCollection.GetValue(i)
                    System.IO.File.Delete(oFileInfo.FullName)
                Next
            Catch ex As Exception
                cUtilitarios.escreveScript("alert('Sem permiss�o para excluir arquivos no diret�rio!')")
            End Try

            'salva imagem do cartao no diretorio da apolice
            Dim random As Random = New Random()
            Dim nomeImagemCartao As String = random.Next()
            Try
                img.Save(ResolveUrl(String.Concat(MapPath(String.Concat(strPath, Session("apolice"), "/")), nomeImagemCartao, ".jpg")), System.Drawing.Imaging.ImageFormat.Jpeg)
            Catch ex As Exception
                cUtilitarios.escreveScript("alert('Sem permiss�o para gravar o arquivo do cart�o no diret�rio!')")
            End Try

            imgCartao.ImageUrl = ResolveUrl(String.Concat(String.Concat(strPath, Session("apolice"), "/"), nomeImagemCartao, ".jpg"))
            imgCartao.Width = 437
            imgCartao.Height = 307
            imgCartao.Visible = True

            logoBB.Dispose()
            logoMapfre.Dispose()
            If (Not String.IsNullOrEmpty(Session("logoAssociada"))) Then
                If (Session("logoAssociada").Equals("1")) Then
                    logoApolice.Dispose()
                End If
            End If

            Dim MergedImage As System.Drawing.Image
            Dim bm As New Bitmap(874, 1238)
            Dim gr As Graphics = Graphics.FromImage(bm)

            gr.DrawImage(img, 0, 0)
            gr.DrawImage(img, 437, 0)
            gr.DrawImage(img, 0, 307)
            gr.DrawImage(img, 437, 307)
            gr.DrawImage(img, 0, 614)
            gr.DrawImage(img, 437, 614)
            gr.DrawImage(img, 0, 921)
            gr.DrawImage(img, 437, 921)
            MergedImage = bm
            gr.Dispose()
            img.Dispose()

            'salva a imagem de impressao no diretorio da apolice
            Dim nomeImagemCartaoA4 As String = random.Next()
            Try
                MergedImage.Save(ResolveUrl(String.Concat(MapPath(String.Concat(strPath, Session("apolice"), "/")), nomeImagemCartaoA4, ".jpg")), System.Drawing.Imaging.ImageFormat.Jpeg)
                Session("pathCartao") = ResolveUrl(String.Concat(String.Concat(strPath, Session("apolice"), "/"), nomeImagemCartaoA4, ".jpg"))
            Catch ex As Exception
                cUtilitarios.escreveScript("alert('Sem permiss�o para gravar o arquivo de impress�o no diret�rio!')")
            End Try

            MergedImage.Dispose()
        Catch ex As Exception
            cUtilitarios.escreveScript("alert('Erro ao configurar o cart�o!')")
        End Try
    End Sub

    Protected Sub btnConfirmaImpressaoCartao_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmaImpressaoCartao.Click
        If (VerificaAcessoDiretorioLogomarcas()) Then
            pnlVisualizaCartao.Visible = False
            Response.Write("<script>")
            Response.Write("window.open('ImpressaoCartao.aspx','_blank')")
            Response.Write("</script>")
        Else
            cUtilitarios.escreveScript("alert('N�o foi poss�vel acessar o diret�rio de logomarcas!');")
        End If
    End Sub

    Protected Sub btnCancelaImportacao_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelaImportacao.Click
        pnlUpload.Visible = False
    End Sub

    Protected Sub btnCancelaImpressaoCartao_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelaImpressaoCartao.Click
        pnlVisualizaCartao.Visible = False
    End Sub

    Public Enum eBanco As Byte
        web_intranet_db = 1
        segab_db = 2
        Imagem_db = 3
        Interface_db = 4
        web_seguros_db = 5
    End Enum


End Class
