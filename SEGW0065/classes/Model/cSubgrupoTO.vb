Namespace Model



Public Class cSubgrupoTO

#Region "Atributos"
        Private _idadeMinima As Int16 = -1
        Private _idadeMaxima As Int16 = -1
        Private _fatorMultiploSalario As Int16
        Private _tipoCapitalSegurado As String
        Private _tipoConjuge As String
        Private _limiteMinimo As Double = 0
        Private _limiteMaximo As Double = 0
        Private _valorCapital As Double = 0
#End Region

#Region "Propriedades"
        Public Property IdadeMinima() As Int16
            Get
                Return Me._idadeMinima
            End Get
            Set(ByVal Value As Int16)
                Me._idadeMinima = Value
            End Set
        End Property
        Public Property IdadeMaxima() As Int16
            Get
                Return Me._idadeMaxima
            End Get
            Set(ByVal Value As Int16)
                Me._idadeMaxima = Value
            End Set
        End Property
        Public Property FatorMultiploSalario() As Int16
            Get
                Return Me._fatorMultiploSalario
            End Get
            Set(ByVal Value As Int16)
                Me._fatorMultiploSalario = Value
            End Set
        End Property
        Public Property TipoCapitalSegurado() As String
            Get
                Return Me._tipoCapitalSegurado
            End Get
            Set(ByVal Value As String)
                Me._tipoCapitalSegurado = Value
            End Set
        End Property
        Public Property TipoConjuge() As String
            Get
                Return Me._tipoConjuge
            End Get
            Set(ByVal Value As String)
                Me._tipoConjuge = Value
            End Set
        End Property
        Public Property LimiteMinimo() As Double
            Get
                Return Me._limiteMinimo
            End Get
            Set(ByVal Value As Double)
                Me._limiteMinimo = Value
            End Set
        End Property
        Public Property LimiteMaximo() As Double
            Get
                Return Me._limiteMaximo
            End Get
            Set(ByVal Value As Double)
                Me._limiteMaximo = Value
            End Set
        End Property
        Public Property ValorCapital() As Double
            Get
                Return Me._valorCapital
            End Get
            Set(ByVal Value As Double)
                Me._valorCapital = Value
            End Set
        End Property
#End Region

        Public Function isCapitalGlobal() As Boolean
            Return Me.TipoCapitalSegurado.Trim = "Capital Global"
        End Function

        Public Function isCapitalFixo() As Boolean
            Return Me.TipoCapitalSegurado.Trim = "Capital fixo"
        End Function

        Public Function isConjugeFacultativo() As Boolean
            Return Me.TipoConjuge.Trim = "C�njuge facultativo"
        End Function

    End Class

End Namespace
