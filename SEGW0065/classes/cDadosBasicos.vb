Imports System
Imports System.Web
Imports System.Data
Imports System.Data.SqlClient

Public MustInherit Class cDadosBasicos

    Private sSQL As String = String.Empty
    Private Conn As SqlClient.SqlConnection
    Private Trans As SqlClient.SqlTransaction
    Private oDS As DataSet

    Public Sub IniciaTransacao()
        If Conn Is Nothing Then Conn = mConexao()
        Trans = Conn.BeginTransaction
    End Sub

    Public Sub CommitTransacao()
        Trans.Commit()
    End Sub

    Public Sub RollBackTransacao()
        Trans.Rollback()
    End Sub

    Public Property DS() As DataSet
        Get
            Return oDS
        End Get
        Set(ByVal Value As DataSet)
            oDS = Value
        End Set
    End Property

    Public Property SQL() As String
        Get
            Return sSQL
        End Get
        Set(ByVal Value As String)
            sSQL = Value
        End Set
    End Property

    Public Overridable Sub ExecutaSQL()
        'If Conn Is Nothing Then Conn = mConexao()
        'Dim SqlComm As New SqlClient.SqlCommand(sSQL, Conn, Trans)
        'SqlComm.CommandTimeout = 0
        'Dim SqlDA As New SqlClient.SqlDataAdapter(SqlComm)
        'If Not oDS Is Nothing Then oDS.Dispose()
        'oDS = New DataSet

        'SqlDA.Fill(oDS)

        'SqlDA.Dispose()
        'SqlComm.Dispose()
        'SqlDA = Nothing
        'SqlComm = Nothing
        Alianca.Seguranca.BancoDados.cCon.ExecuteDataset(CommandType.Text, sSQL)

    End Sub

    Private Function mConexao() As SqlConnection
        Dim Conn As SqlClient.SqlConnection
        Conn = New SqlClient.SqlConnection(stringConexao())
        Conn.Open()
        Return Conn
    End Function

    Public Enum eBanco As Byte
        web_intranet_db = 1
        segab_db = 2
        Imagem_db = 3
        Interface_db = 4
        VIDA_WEB_DB = 5
    End Enum

    Dim _banco As String

    Public Property Banco() As eBanco
        Get
            Return _banco
        End Get
        Set(ByVal Value As eBanco)
            _banco = Value
        End Set
    End Property

    Public Sub New(ByVal Bd As eBanco)
        Banco = Bd
        Alianca.Seguranca.BancoDados.cCon.BancodeDados = Banco.ToString
    End Sub

    Private Function stringConexao() As String
        Dim StrConn As String = ""
        If Banco = eBanco.web_intranet_db Then
            StrConn = stringConexaoXML("web_intranet_db")
        ElseIf Banco = eBanco.segab_db Then
            StrConn = stringConexaoXML("segab_db")
        ElseIf Banco = eBanco.Imagem_db Then
            StrConn = stringConexaoXML("Imagem_db")
        ElseIf Banco = eBanco.Interface_db Then
            StrConn = stringConexaoXML("Interface_db")
        End If


        Return StrConn.Replace(Chr(13), "").Replace(Chr(10), "").Replace(vbNewLine, "").Replace(vbTab, "")
    End Function

    Function stringConexaoXML(ByVal bd As String) As String
        'Dim NomeArquivo As String = Replace(Application.StartupPath, "bin", "") & "conexao.xml"
        Dim NomeArquivo As String = "C:\Windows\System32\" & "conexao.xml"
        'HttpContext.Current.Server.MapPath("\_conexao\") & "conexao.xml"

        If Not IO.File.Exists(NomeArquivo) Then
            Throw New IO.FileNotFoundException("N�o foi poss�vel localizar o arquivo de conex�o.")
        End If

        Dim Usuario As String = ""
        Dim Senha As String = ""
        Dim Servidor As String = ""

        Dim XML As New Xml.XmlDocument
        Try
            XML.Load(NomeArquivo)
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Dim Lista As Xml.XmlNodeList = XML.GetElementsByTagName("BD")
        For Each nd As Xml.XmlNode In Lista
            If nd.InnerText = bd Then
                Usuario = nd.Attributes("usuario").Value
                Senha = nd.Attributes("senha").Value
                Servidor = nd.Attributes("servidor").Value
                Exit For
            End If
        Next
        If Usuario.Trim = "" Or Senha.Trim = "" Or Servidor.Trim = "" Then
            Throw New ArgumentException("N�o foi poss�vel localizar a conex�o.")
        End If

        Dim CnStr As String = "Data Source=" & Servidor & ";Initial Catalog=" & bd & ";User ID=" & Usuario & ";Password=" & Senha
        Return CnStr 'System.Web.HttpContext.Current.Session("ConnString")
    End Function

    Private Function usuario_logado() As String
        Dim user As String
        user = "" 'HttpContext.Current.Request.ServerVariables("LOGON_USER")
        'HttpContext.Current.Response.Write(user)
        If InStr(user, "\") = 0 Then
            user = UCase(Mid(user, (InStr(user, "/") + 1), (Len(user) - InStr(user, "/"))))
        Else
            user = UCase(Mid(user, (InStr(user, "\") + 1), (Len(user) - InStr(user, "\"))))
        End If

        'comentar ifs abaixo.
        If user = "" Or UCase(user) = "JBAIAO" Then
            user = "JBAIAO"
            user = "acintra"
        End If

        Return user
    End Function

#Region "Propriedades Comuns"

    Private _Usuario As String
    Public Property Usuario() As String
        Get
            Return usuario_logado()
        End Get
        Set(ByVal Value As String)
            _Usuario = Value
        End Set
    End Property

    Private _Situacao As String
    Public Property Situacao() As String
        Get
            Return _Situacao
        End Get
        Set(ByVal Value As String)
            _Situacao = Value
        End Set
    End Property

#End Region

#Region "M�todos Utilitarios"

    Public Sub fPrint(ByVal msg As String)
        ' Web.HttpContext.Current.Response.Write(msg & "**<br>" & vbNewLine)
        'Web.HttpContext.Current.Response.Flush()
    End Sub

    Public Function TrataString(ByVal Str As String) As String
        If Str Is Nothing Then
            Return "null"
        Else
            If Str.ToString.Trim = String.Empty Then
                Return "null"
            Else
                Return "'" & Str.Replace("'", "`") & "'"
            End If
        End If
    End Function

    Public Function TrataInteger(ByVal Str As Object) As String
        If Str Is Nothing Then
            Return "null"
        Else
            If Str.ToString.Trim = String.Empty Then
                Return "null"
            Else
                Return Str.ToString.Replace(".", "").Replace(",", ".")
            End If
        End If
    End Function

    Public Function TrataData(ByVal Dia As String, ByVal Mes As String, ByVal Ano As String) As String

        If Dia = "" And Mes = "" And Ano = "" Then
            Return "null"
        Else
            Dim Data As Double
            Data = Integer.Parse(Dia)
            Data += Integer.Parse(Mes) * 100
            Data += Integer.Parse(Ano) * 10000

            Return "'" & Data & "'"

        End If
    End Function

    Public Function TrataData(ByVal data As String) As String
        Dim dia, mes, ano, Adata() As String
        dia = ""
        mes = ""
        ano = ""
        If data.Trim <> "" Then
            Adata = data.Split(" ")(0).Split("/")
            If Adata.Length >= 3 Then
                dia = Adata(0)
                mes = Adata(1)
                ano = Adata(2)
            End If
        End If
        Return TrataData(dia, mes, ano)
    End Function
#End Region

    Protected Overrides Sub Finalize()
        If Not IsNothing(Conn) Then
            Try
                Conn.Dispose()
            Catch ex As Exception
                Dim excp As New clsException(ex)
            End Try
            Conn = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class