
Public Class cAcompanhamento

#Region "Heran�a"
    Inherits cBanco
#End Region
    ''' <summary>
    ''' Recuperad dados da segurado_temporario_web_espelho_tb
    ''' apolice_id, ramo_id, sub_grupo_id, prop_cliente_id, val_salario, val_capital_segurado
    ''' </summary>
    ''' <param name="apolice_id"></param>
    ''' <param name="ramo_id"></param>
    ''' <param name="seg_cod_susep"></param>
    ''' <param name="suc_seg_id"></param>
    ''' <param name="subgrupo"></param>
    ''' <param name="propclienteid"></param>
    ''' <remarks></remarks>
    Public Sub SEGS7594_SPS(ByVal apolice_id As Integer, _
                            ByVal ramo_id As Integer, _
                            ByVal seg_cod_susep As Integer, _
                            ByVal suc_seg_id As Integer, _
                            ByVal subgrupo As Integer, _
                            ByVal propclienteid As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS7594_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @prop_cliente_id = " & propclienteid
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id

    End Sub

    Public Sub SEGS7596_SPS()
        SQL = "exec [sisab003].seguros_db.dbo.SEGS7596_SPS "
    End Sub

    Public Sub GetDadosSubGrupoSPS(ByVal apolice_id As Integer, _
                                    ByVal ramo_id As Integer, _
                                    ByVal subgrupo As Integer, _
                                    ByVal seg_cod_susep As Integer, _
                                    ByVal suc_seg_id As Integer)
        SQL = "exec [sisab003].seguros_db.dbo.sel_sub_grupo_sps "
        SQL &= " @sucursal = " & suc_seg_id
        SQL &= ", @seguradora = " & seg_cod_susep
        SQL &= ", @ramo = " & ramo_id
        SQL &= ", @apolice = " & apolice_id
        SQL &= ", @subgrupo = " & subgrupo
    End Sub

    Public Sub SEGS6570_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer)
        SQL = "exec VIDA_WEB_DB..SEGS6570_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub

    Public Sub Recalcula_Resumo_Operacoes(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal usuario As String)
        SQL = "exec VIDA_WEB_DB..SEGS7048_SPI "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @usuario = '" & usuario & "'"
    End Sub

    'Busca o Resumo da Fatura atual (anterior) da tela de abertura do Vida
    Public Sub ResumoFaturaAtual_Anterior(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal usuario As String, ByVal wf_id As Int64)
        'Busca Resumo da Fatura Atual Anterior
        SQL = "exec VIDA_WEB_DB..SEGS5732_SPS "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @subgrupo_id=" & subgrupo_id
        SQL &= ", @usuario=" & trStr(usuario)
        SQL &= ", @wf_id = " & wf_id
    End Sub

    Public Sub getCapitalFaixaEtaria(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal dtInicioVigencia As String, ByVal dt_nascimento As String)
        SQL = "exec VIDA_WEB_DB..SEGS5782_SPS "
        SQL &= " @lApoliceId=" & apolice_id
        SQL &= ", @iRamoId=" & ramo_id
        SQL &= ", @iSubGrupoId=" & subgrupo_id
        SQL &= ", @dtInicioVigencia=" & dtInicioVigencia
        SQL &= ", @dt_nascimento=" & dt_nascimento
    End Sub

    'Retorna o per�odo da ap�lice
    Public Sub GetDadosSubgrupoE1(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)
        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5751_SPS "
        SQL &= "  @apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @subgrupo_id=" & trInt(subgrupo_id)
    End Sub

    'Busca o Resumo da Fatura atual (Inclusoes, Altera��es e Exclusoes) da tela de abertura do Vida
    Public Sub ResumoFaturaAtual_IAE(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal usuario As String, ByVal wf_id As Int64)
        'Busca Resumo da Fatura Atual: IAE
        SQL = "exec VIDA_WEB_DB..SEGS5733_SPS "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @subgrupo_id=" & subgrupo_id
        SQL &= ", @usuario=" & trStr(usuario)
        SQL &= ", @wf_id = " & wf_id
    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String)
        SQL = "exec VIDA_WEB_DB..SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @cpf = '" & cpf & "'"
        SQL &= ", @nome = " & nome
    End Sub

    Public Sub SEGS5696_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal workflow As Integer, ByVal valor_id As Integer)
        SQL = "exec VIDA_WEB_DB..segs5696_sps  @apolice_id = " & apolice_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @tp_wf_id = " & workflow
        SQL &= ", @tp_versao_id = " & valor_id
    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String, ByVal id As Integer)
        SQL = "exec VIDA_WEB_DB..SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @cpf = " & cpf
        SQL &= ", @nome = " & nome
        SQL &= ", @id = " & id
    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String, ByVal id As String, ByVal tipo As String)
        SQL = "exec VIDA_WEB_DB..SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @cpf = " & cpf
        SQL &= ", @nome = " & nome
        SQL &= ", @id = " & id
        SQL &= ", @tipo = " & tipo
    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String, ByVal id As String, ByVal tipo As String, ByVal ordernaAlterado As Integer)
        SQL = "exec VIDA_WEB_DB..SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @cpf = " & cpf
        SQL &= ", @nome = " & nome
        SQL &= ", @id = " & id
        SQL &= ", @tipo = " & tipo
        SQL &= ", @ordemUltAlterado = " & ordernaAlterado
    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String, ByVal id As String, ByVal tipo As String, ByVal flag As String)
        SQL = "exec VIDA_WEB_DB..SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @cpf = " & cpf
        SQL &= ", @nome = " & nome
        SQL &= ", @id = " & id
        SQL &= ", @tipo = " & tipo
        SQL &= ", @flag = " & flag
    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal id As Integer)
        Dim suc_seg_id As Integer = 6785
        Dim seg_cod_susep As Integer = 0

        SQL = "exec VIDA_WEB_DB..SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @cpf = null "
        SQL &= ", @nome = null"
        SQL &= ", @id = " & id
        SQL &= ", @tipo = null "
        SQL &= ", @flag = 'A' "
    End Sub

    Public Sub SEGS5658_SPD(ByVal id As Integer, ByVal dt As String, ByVal usuario As String)
        ''Flow 599077 - dgomes - CONFITEC - 08/12/2008
        Dim opn As String = IIf(cUtilitarios.getCapitalSeguradoSession() = "Capital Global" And dt <> "", "'E'", "null")

        SQL = "exec VIDA_WEB_DB..SEGS5658_SPD "
        SQL &= " @id = " & id
        SQL &= ", @dtDesligamento  = " & "'" & dt & "'"
        SQL &= ", @usuario  = " & "'" & usuario & "'"
        SQL &= ", @ind_operacao_nova  = " & opn
    End Sub

    Public Sub SEGS5657_SPU(ByVal id As Integer, ByVal seq_cliente_id As Integer, ByVal seq_faturamento As Integer, ByVal apolice_id As Integer, ByVal sucursal_seguradora_id As Integer, ByVal seguradora_cod_susep As Integer, ByVal ramo_id As Integer, ByVal sub_grupo_id As Integer, ByVal dt_inicio_vigencia_sbg As String, ByVal nome As String, ByVal tp_componente_id As Integer, ByVal cpf As String, ByVal dt_nascimento As String, ByVal sexo As String, ByVal val_capital_segurado As String, ByVal val_salario As String, ByVal dt_inclusao As String, ByVal usuario As String, ByVal pendente As String, ByVal permanente As String, ByVal alteracoes As String, ByVal val_capital_segurado_origem As String) '22/01/2019 - Confitec SP - Cleiton Queiroz - Demanda MU-2018-070494 - Ajuste de regra SEGP 1118
        apolice_id = Int(apolice_id)
        ramo_id = Int(ramo_id)
        seguradora_cod_susep = Int(seguradora_cod_susep)
        sucursal_seguradora_id = Int(sucursal_seguradora_id)
        sub_grupo_id = Int(sub_grupo_id)
        nome = "'" & nome.Replace("'", "�") & "'"
        cpf = "'" & cUtilitarios.destrataCPF(cpf) & "'"
        dt_nascimento = "'" & cUtilitarios.trataDataDB(dt_nascimento) & "'"
        sexo = "'" & sexo.Substring(0, 1) & "'"
        seq_cliente_id = Int(seq_cliente_id)
        tp_componente_id = Int(tp_componente_id)
        dt_inicio_vigencia_sbg = "'" & cUtilitarios.trataDataDB(dt_inicio_vigencia_sbg) & "'"
        usuario = "'" & usuario & "'"
        val_capital_segurado = val_capital_segurado.Replace(",", ".")
        val_salario = val_salario.Replace(",", ".")
        val_capital_segurado_origem = val_capital_segurado_origem.Replace(",", ".") '22/01/2019 - Confitec SP - Cleiton Queiroz - Demanda MU-2018-070494 - Ajuste de regra SEGP 1118

        'SQL = "exec VIDA_WEB_DB..SEGS5657_SPU @id=" & id
        SQL = "exec VIDA_WEB_DB..SEGS5657_SPU @id=" & id
        SQL &= ",  @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seguradora_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & sucursal_seguradora_id
        SQL &= ", @sub_grupo_id = " & sub_grupo_id
        SQL &= ", @nome = " & nome
        SQL &= ", @cpf = " & cpf
        SQL &= ", @dt_nascimento = " & dt_nascimento
        SQL &= ", @sexo = " & sexo
        SQL &= ", @val_capital_segurado = " & val_capital_segurado
        SQL &= ", @val_salario = " & val_salario
        SQL &= ", @seq_faturamento = " & seq_faturamento
        SQL &= ", @seq_cliente_id = " & seq_cliente_id
        SQL &= ", @tp_componente_id = " & tp_componente_id
        SQL &= ", @dt_inicio_vigencia_sbg = " & dt_inicio_vigencia_sbg
        SQL &= ", @pendente = " & pendente
        SQL &= ", @permanente = " & permanente
        SQL &= ", @usuario = " & usuario & " "
        SQL &= ", @alteracoes = '" & alteracoes & "'"
        SQL &= ", @val_capital_segurado_origem = " & val_capital_segurado_origem '22/01/2019 - Confitec SP - Cleiton Queiroz - Demanda MU-2018-070494 - Ajuste de regra SEGP 1118
    End Sub

    Public Sub SEGS5656_SPI(ByVal seq_cliente_id As Integer, ByVal seq_faturamento As Integer, ByVal apolice_id As Integer, ByVal sucursal_seguradora_id As Integer, ByVal seguradora_cod_susep As Integer, ByVal ramo_id As Integer, ByVal sub_grupo_id As Integer, ByVal dt_inicio_vigencia_sbg As String, ByVal nome As String, ByVal tp_componente_id As Integer, ByVal cpf As String, ByVal dt_nascimento As String, ByVal sexo As String, ByVal val_capital_segurado As String, ByVal val_salario As String, ByVal dt_inclusao As String, ByVal usuario As String, ByVal pendente As String, ByVal permanente As String, ByVal id As Integer)
        apolice_id = Int(apolice_id)
        ramo_id = Int(ramo_id)
        seguradora_cod_susep = Int(seguradora_cod_susep)
        sucursal_seguradora_id = Int(sucursal_seguradora_id)
        sub_grupo_id = Int(sub_grupo_id)
        nome = "'" & nome.Replace("'", "�") & "'"
        cpf = "'" & cUtilitarios.destrataCPF(cpf) & "'"
        dt_nascimento = "'" & cUtilitarios.trataDataDB(dt_nascimento) & "'"
        sexo = "'" & sexo.Substring(0, 1) & "'"
        seq_cliente_id = Int(seq_cliente_id)
        tp_componente_id = Int(tp_componente_id)
        dt_inicio_vigencia_sbg = "'" & cUtilitarios.trataDataDB(dt_inicio_vigencia_sbg) & "'"
        usuario = "'" & usuario & "'"
        val_salario = val_salario.Replace(",", ".")
        val_capital_segurado = val_capital_segurado.Replace(",", ".")

        SQL = "exec VIDA_WEB_DB..SEGS5656_SPI "
        SQL &= "  @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seguradora_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & sucursal_seguradora_id
        SQL &= ", @sub_grupo_id = " & sub_grupo_id
        SQL &= ", @nome = " & nome
        SQL &= ", @cpf = " & cUtilitarios.destrataCPF(cpf)
        SQL &= ", @dt_nascimento = " & dt_nascimento
        SQL &= ", @sexo = " & sexo
        SQL &= ", @val_capital_segurado = " & val_capital_segurado
        SQL &= ", @val_salario = " & val_salario
        SQL &= ", @seq_faturamento = " & seq_faturamento
        SQL &= ", @seq_cliente_id = " & seq_cliente_id
        SQL &= ", @tp_componente_id = " & tp_componente_id
        SQL &= ", @dt_inicio_vigencia_sbg = " & dt_inicio_vigencia_sbg
        SQL &= ", @pendente = " & pendente
        SQL &= ", @permanente = " & permanente
        SQL &= ", @usuario = " & usuario & " "
        SQL &= ", @idAnterior = " & id & " "
    End Sub

    ''Consulta as condi��es do subgrupo
    Public Sub SEGS5705_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)
        SQL = "exec VIDA_WEB_DB..SEGS5705_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub

    ''Consulta as condi��es do subgrupo
    Public Sub SEGS5706_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)
        SQL = "exec VIDA_WEB_DB..SEGS5706_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub

    ''Consulta as condi��es do subgrupo (valor lim�te m�ximo)
    Public Sub SEGS5744_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)
        SQL = "exec VIDA_WEB_DB..SEGS5744_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub

    '''Consulta as condi��es do subgrupo SEGS5750_SPS
    Public Sub coberturas(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer, ByVal componente As String)
        SQL = "exec VIDA_WEB_DB..SEGS5750_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @componente = " & componente
        SQL &= ", @ind_acesso = 0"
        SQL &= ", @cpf_id = '0'"
        SQL &= ", @usuario = 0"
        SQL &= ", @seguradora_cod_susep = 6785"
        SQL &= ", @sucursal_seguradora_id = 0 "
    End Sub

    Public Sub getDataIniVigenciaApolice(ByVal apolice_id As String, ByVal ramo_id As String)
        SQL = "select convert(varchar,dt_inicio_vigencia,103) as data "
        SQL &= " from [sisab003].seguros_db.dbo.apolice_tb"
        SQL &= " where apolice_id = " + apolice_id + " and"
        SQL &= " ramo_id = " + ramo_id + " and "
        SQL &= " sucursal_seguradora_id = 0 and"
        SQL &= " seguradora_cod_susep = 6785"
    End Sub

    Public Sub getTpApoliceConjuge(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As String)
        SQL = "select isnull(tp_clausula_conjuge,'N')"
        SQL &= " from [sisab003].seguros_db.dbo.sub_grupo_apolice_tb"
        SQL &= " where apolice_id = " & apolice_id & " and"
        SQL &= " sucursal_seguradora_id = 0 and"
        SQL &= " seguradora_cod_susep = 6785 and"
        SQL &= " ramo_id = " & ramo_id & " and"
        SQL &= " sub_grupo_id = " & subgrupo_id
    End Sub

    Public Sub SEGS5744_SPS(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As String)
        SQL = "exec VIDA_WEB_DB..SEGS5744_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @cod_susep = 6785"
        SQL &= ", @seguradora_id = 0"
        SQL &= ", @subgrupo_id = " & subgrupo_id
        SQL &= ", @usuario = ''"
        SQL &= ", @ind_acesso = ''"
        SQL &= ", @cpf_id = ''"
    End Sub

    Public Sub SEGS5773_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)
        SQL = "exec VIDA_WEB_DB..SEGS5773_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @subgrupo_id = " & subgrupo
    End Sub

    Public Sub getCapitalConjuge(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As String)
        SQL = "Select isnull(a.perc_basica, 0) / 100"
        SQL &= " FROM [sisab003].seguros_db.dbo.escolha_sub_grp_tp_cob_comp_tb a(NOLOCK)"
        SQL &= " INNER JOIN [sisab003].seguros_db.dbo.tp_cob_comp_tb cc (NOLOCK)"
        SQL &= " ON cc.tp_cob_comp_id = a.tp_cob_comp_id"
        SQL &= " WHERE a.apolice_id = " & apolice_id
        SQL &= " AND a.sucursal_seguradora_id = 0"
        SQL &= " AND a.seguradora_cod_susep = 6785"
        SQL &= " AND a.ramo_id = " & ramo_id
        SQL &= " AND a.sub_grupo_id = " & subgrupo_id
        SQL &= " AND isnull(a.class_tp_cobertura, 'a') = 'b'"
        SQL &= " AND cc.tp_componente_id = 3"
    End Sub

    Public Sub getLmtCapValIs(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As String)
        SQL = "Select Case val_lim_cap_global = isnull(VAL_LIM_MAX_IS, 0),  val_is_basica = isnull(escolha_sub_grp_tp_cob_comp_tb.val_is, 0), APOLICE_ID "
        SQL &= " FROM [sisab003].seguros_db.dbo.escolha_sub_grp_tp_cob_comp_tb a(NOLOCK)"
        SQL &= " INNER JOIN [sisab003].seguros_db.dbo.tp_cob_comp_tb cc (NOLOCK)"
        SQL &= " ON cc.tp_cob_comp_id = a.tp_cob_comp_id"
        SQL &= " WHERE a.apolice_id = " & apolice_id
        SQL &= " AND a.ramo_id = " & ramo_id
        SQL &= " AND a.sub_grupo_id = " & subgrupo_id
        SQL &= " AND isnull(a.class_tp_cobertura, 'a') = 'b'"
        SQL &= " AND cc.apolice_id = a.apolice_id "
    End Sub

    Public Sub getCountSegTempTb(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As String, ByVal dt_ini_vig_fat As String, ByVal dt_fim_vig_fat As String)
        SQL = "SELECT COUNT(*)�as num_seg� "
        SQL &= " FROM VIDA_WEB_DB..segurado_temporario_web_tb "
        SQL &= " WHERE apolice_id = " & apolice_id
        SQL &= "    AND ramo_id = " & ramo_id
        SQL &= "    AND sub_grupo_id = " & subgrupo_id
        SQL &= "    AND dt_inicio_vigencia_sbg <= " & dt_fim_vig_fat
        SQL &= "    AND (dt_fim_vigencia_sbg is null "
        SQL &= "      OR dt_fim_vigencia_sbg >= " & dt_ini_vig_fat & " ) "
        SQL &= "    AND pendente = 'n'� "
        SQL &= "    AND recusado = 'n' "
    End Sub

    Public Sub getVidasPendentes(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal usuario As String)
        SQL = "select count(*)  "
        SQL &= " from VIDA_WEB_DB..segurado_temporario_web_tb  "
        SQL &= "    WHERE"
        SQL &= "    pendente = 's' and  "
        SQL &= "    apolice_id   = " & apolice_id & "   AND  "
        SQL &= "    sub_grupo_id   = " & subgrupo_id & "   AND  "
        SQL &= "    ramo_id    = " & ramo_id & "   AND  "
        SQL &= "    dt_transferencia is null and "
        SQL &= "    not ind_operacao is null "
    End Sub

    Public Sub SEGS8379_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal sub_grupo_id As Integer, ByVal cpf As String)
        SQL = "exec [sisab003].seguros_db.dbo.SEGS8379_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @sub_grupo_id = " & sub_grupo_id
        SQL &= ", @cpf = '" & cpf & "'"
    End Sub

    Public Sub New(Optional ByVal banco As cDadosBasicos.eBanco = cDadosBasicos.eBanco.VIDA_WEB_DB)
        MyBase.New(banco)
        If Not (banco.ToString Is Nothing) Then
            Alianca.Seguranca.BancoDados.cCon.BancodeDados = banco.ToString
        Else
            Alianca.Seguranca.BancoDados.cCon.BancodeDados = cDadosBasicos.eBanco.VIDA_WEB_DB
        End If
    End Sub

    Function trInt(ByVal valor As Object) As String

        If valor = 0 Then
            Return "null"
        Else
            Return valor
        End If

    End Function

    Function trStr(ByVal valor As Object) As String

        If valor = "" Then
            Return "null"
        Else
            Return "'" & valor.ToString.Replace("'", "''") & "'"
        End If

    End Function

    Public Sub getAliquotaIOF(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)
        SQL = "exec VIDA_WEB_DB..SEGS7717_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id  = " & ramo_id
        SQL &= ", @subgrupo_id = " & subgrupo_id
    End Sub

    'Busca data de vig�ncia para faturas anteriores - pmarques (confitec) 25/05/2010
    Public Sub SEGS8680_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal sub_grupo_id As Integer, ByVal usuario As String)
        SQL = "exec [sisab003].seguros_db.dbo.SEGS8680_SPS "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @sub_grupo_id=" & sub_grupo_id
        SQL &= ", @usuario=" & trStr(usuario)
    End Sub

    Public Sub SEGS9391_SPS(ByVal p_sCPF As String, ByVal p_lApliceID As Long, ByVal p_iRamoId As Integer)
        SQL = "exec vida_web_db..SEGS9391_SPS "
        SQL &= " @apolice_id = " & p_lApliceID
        SQL &= ", @ramo_id  = " & p_iRamoId
        SQL &= ", @CPF = '" & p_sCPF & "'"
    End Sub
    'Autor: pablo.cardoso (Nova Consultoria) 
    'Data da Altera��o: 16/01/2012
    'Demanda AB: 12784293 
    'Item: 7 - Permitir incluir vida que j� participou da ap�lice em algum momento, inclusive se tiver idade superior.
    Public Sub getProponenteApolice(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal cpf As String)
        SQL = "SELECT TOP 1 svsbg.prop_cliente_id FROM [sisab003].seguros_db.dbo.seguro_vida_sub_grupo_tb svsbg "
        SQL &= "INNER JOIN [sisab003].seguros_db.dbo.pessoa_fisica_tb pf "
        SQL &= "ON svsbg.prop_cliente_id = pf.pf_cliente_id "
        SQL &= "WHERE svsbg.ramo_id = " & ramo_id
        SQL &= " AND svsbg.apolice_id = " & apolice_id
        SQL &= " AND svsbg.seguradora_cod_susep = " & seg_cod_susep
        SQL &= " AND svsbg.sucursal_seguradora_id = " & suc_seg_id
        SQL &= " AND pf.cpf ='" & cpf & "'"

    End Sub

    'Autor: pablo.cardoso (Nova Consultoria) 
    'Data da Altera��o: 17/01/2012
    'Demanda AB: 12784293 
    'Item: 11 - N�o permitir a exclus�o de vidas com data anterior ao sinistro.
    Public Sub getSinistroDataAnterior(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal cpf As String, ByVal data As Date)
        SQL = "Select sv.sinistro_id FROM [sisab003].seguros_db.dbo.sinistro_vida_tb sv "
        SQL &= "INNER JOIN [sisab003].seguros_db.dbo.sinistro_tb sn "
        SQL &= "ON sv.sinistro_id = sn.sinistro_id "
        SQL &= "WHERE sv.apolice_id = " & apolice_id
        SQL &= " AND sv.ramo_id = " & ramo_id
        SQL &= " AND sv.cpf ='" & cpf & "'"
        SQL &= " AND '" & Format(data, "yyyyMMdd") & "' <= sn.dt_ocorrencia_sinistro"
    End Sub

    'vitor.cabral - Nova Consultoria - 17/12/2013
    '17884362 - Lista de Melhorias SEGBR (PPE) - In�cio
    Public Sub recuperaPeriodoVigenciaApl(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal sub_grupo_id As Integer)
        apolice_id = Int(apolice_id)
        ramo_id = Int(ramo_id)
        sub_grupo_id = Int(sub_grupo_id)

        SQL = "SELECT apolice_workflow_tb.dt_inicio_vigencia_apl, "
        SQL &= "       apolice_workflow_tb.dt_fim_vigencia_apl "
        SQL &= "  FROM SISAB003.seguros_db.dbo.apolice_workflow_tb apolice_workflow_tb "
        SQL &= " WHERE apolice_workflow_tb.apolice_id = " & apolice_id
        SQL &= "   AND apolice_workflow_tb.ramo_id = " & ramo_id
        SQL &= "   AND apolice_workflow_tb.subgrupo_id = " & sub_grupo_id
        SQL &= "   AND apolice_workflow_tb.tp_wf_id = 209 "
        SQL &= "   AND apolice_workflow_tb.tp_versao_id = 1"
        SQL &= "   AND apolice_workflow_tb.wf_id IN (SELECT MAX(aw.wf_id) "
        SQL &= "                                       FROM SISAB003.seguros_db.dbo.apolice_workflow_tb aw, "
        SQL &= "                                            SISAB003.workflow_db.dbo.atividade_tb a "
        SQL &= "                                      WHERE a.tp_ativ_id = 2 "
        SQL &= "                                        AND aw.wf_id = a.wf_id "
        SQL &= "                                        AND aw.tp_wf_id = apolice_workflow_tb.tp_wf_id "
        SQL &= "                                        AND aw.tp_versao_id = apolice_workflow_tb.tp_versao_id "
        SQL &= "                                        AND aw.apolice_id = apolice_workflow_tb.apolice_id "
        SQL &= "                                        AND aw.subgrupo_id = apolice_workflow_tb.subgrupo_id "
        SQL &= "                                        AND aw.ramo_id = apolice_workflow_tb.ramo_id) "

    End Sub

    Public Sub SEGS11643_SPI(ByVal ramo_id As Integer, ByVal apolice_id As Integer, ByVal sub_grupo_id As Integer, ByVal cpf As String, ByVal tp_componente_id As Integer, ByVal dtInicioVgcApl As String, ByVal dtFimVgcApl As String, ByVal dt_inicio_vigencia_sbg As String, ByVal usuario As String, ByVal operacao As String, Optional ByVal tipo As Integer = 1)

        apolice_id = Int(apolice_id)
        ramo_id = Int(ramo_id)
        sub_grupo_id = Int(sub_grupo_id)
        tipo = Int(tipo)
        cpf = "'" & cUtilitarios.destrataCPF(cpf) & "'"
        tp_componente_id = Int(tp_componente_id)
        dtInicioVgcApl = "'" & cUtilitarios.trataDataDB(dtInicioVgcApl) & "'"
        dtFimVgcApl = "'" & cUtilitarios.trataDataDB(dtFimVgcApl) & "'"
        dt_inicio_vigencia_sbg = "'" & cUtilitarios.trataDataDB(dt_inicio_vigencia_sbg) & "'"
        usuario = "'" & usuario & "'"
        operacao = "'" & operacao & "'"

        SQL = " EXEC [SISAB003].seguros_db.dbo.SEGS11643_SPI "
        SQL &= " @seguradora_cod_susep = 6785 "
        SQL &= ", @sucursal_seguradora_id = 0 "
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @apolice_id = " & apolice_id
        SQL &= ", @sub_grupo_id = " & sub_grupo_id
        SQL &= ", @cpf = " & cpf
        SQL &= ", @tp_componente_id = " & tp_componente_id
        SQL &= ", @dt_inicio_competencia = " & dtInicioVgcApl
        SQL &= ", @dt_fim_competencia = " & dtFimVgcApl
        SQL &= ", @dt_inicio_vigencia = " & dt_inicio_vigencia_sbg
        SQL &= ", @usuario = " & usuario
        SQL &= ", @tipo = " & tipo
        SQL &= ", @operacao = " & operacao

    End Sub
    'vitor.cabral - Nova Consultoria - 17/12/2013 - Fim

End Class


