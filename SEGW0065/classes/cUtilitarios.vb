Public Class cUtilitarios

    'Trata a data para o seguinte formato dd/mm/aaaa hh:mm
    Public Shared Function trataDataHora(ByVal data As String) As String
        Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
        Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
        Dim ano As String = CType(data, DateTime).Year
        Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
        Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")
        Dim cRetorno As String = ""

        Try
            cRetorno = dia + "/" + mes + "/" + ano + " " + hora + ":" + minuto
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return cRetorno
        
    End Function

    Public Shared Function TrataNulo(ByVal oData As Object) As String
        Dim cRetorno As String = ""

        If Not IsDBNull(oData) Then
            cRetorno = oData
        End If

        Return cRetorno
    End Function

    Public Shared Function getIdadeMinApolice() As Date
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        bd.SEGS5705_SPS(Web.HttpContext.Current.Session("apolice"), _
                            Web.HttpContext.Current.Session("ramo"), _
                            6785, _
                            0, _
                            Web.HttpContext.Current.Session("subgrupo_id"))

        Dim idadeMin As Integer = 14
        Dim dataMin As Date = Now.AddYears(-idadeMin)

        Try
            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    idadeMin = System.Int32.Parse(dr.GetValue(0).ToString.Trim())
                End If
            End If

            dataMin = Now.AddYears(-idadeMin)
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return dataMin

    End Function

    Public Shared Function getIntIdadeMinApolice() As Int16
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        bd.SEGS5705_SPS(Web.HttpContext.Current.Session("apolice"), _
                            Web.HttpContext.Current.Session("ramo"), _
                            6785, _
                            0, _
                            Web.HttpContext.Current.Session("subgrupo_id"))

        Dim idadeMin As Integer = 14

        Try
            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    idadeMin = System.Int32.Parse(dr.GetValue(0).ToString.Trim())
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return idadeMin

    End Function

    Public Shared Function getIdadeMaxApolice() As Date
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dataMax As Date
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim idadeMax As Integer = 60

        bd.SEGS5705_SPS(Web.HttpContext.Current.Session("apolice"), _
                            Web.HttpContext.Current.Session("ramo"), _
                            6785, _
                            0, _
                            Web.HttpContext.Current.Session("subgrupo_id"))

        Try
            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    idadeMax = System.Int32.Parse(dr.GetValue(1).ToString.Trim())
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
            dataMax = Now.AddYears(-idadeMax)
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return dataMax

    End Function

    Public Shared Function getIntIdadeMaxApolice() As Int16
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        bd.SEGS5705_SPS(Web.HttpContext.Current.Session("apolice"), _
                                Web.HttpContext.Current.Session("ramo"), _
                                6785, _
                                0, _
                                Web.HttpContext.Current.Session("subgrupo_id"))

        Dim idadeMax As Integer = 60

        Try

            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    idadeMax = System.Int32.Parse(dr.GetValue(1).ToString.Trim())
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return idadeMax

    End Function

    'Trata a data para o seguinte formato dd/mm/aaaa 
    Public Shared Function trataData(ByVal data As String) As String
        Dim dt_arr() As String = data.Split("/")
        Dim dt_final As String = ""
        Dim dia As String = dt_arr(0).PadLeft(2, "0")
        Dim mes As String = dt_arr(1).PadLeft(2, "0")
        Dim ano As String = dt_arr(2)

        Try
            dt_final = dia + "/" + mes + "/" + ano
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return dt_final

    End Function

    'Trata a data para o seguinte formato aaaa/mm/dd 
    Public Shared Function trataDataDB(ByVal data As String) As String
        If (data = Nothing Or data = "") Then
            Return ""
        End If

        Dim dt_arr() As String = data.Split("/")
        Dim dt_final As String = ""

        Dim dia As String = dt_arr(0).PadLeft(2, "0")
        Dim mes As String = dt_arr(1).PadLeft(2, "0")
        Dim ano As String = dt_arr(2)

        Try
            dt_final = ano + mes + dia
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
        Return dt_final

    End Function

    Public Shared Function trataSmalldate(ByVal data As String) As String
        Dim dt_final As String = ""

        Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
        Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
        Dim ano As String = CType(data, DateTime).Year

        Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
        Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

        Try
            dt_final = ano + mes + dia
            dt_final &= " " + hora + ":" + minuto
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return dt_final

    End Function

    ''Exibe o valor em um alert na tela
    Public Shared Sub br(ByVal valor As String)
        Dim texto As String

        texto = "<script>" + vbNewLine
        texto &= "alert(""" + valor + """);"
        texto &= "</script>"

        System.Web.HttpContext.Current.Response.Write(texto)
    End Sub

    Public Shared Sub sair()
        System.Web.HttpContext.Current.Response.End()
    End Sub

    Public Shared Sub escreveScript(ByVal valor As String)
        System.Web.HttpContext.Current.Response.Write("<script>" + valor + "</script>")
    End Sub

    Public Shared Function destrataCPF(ByVal cpf As String) As String

        If cpf.Length = 14 Then
            Return cpf.Replace(".", "").Replace("-", "")
        Else
            Return cpf
        End If

    End Function

    Public Shared Function trataCPF(ByVal cpf As String) As String

        If cpf.Length = 11 Then
            Return cpf.Substring(0, 3) + "." + cpf.Substring(3, 3) + "." + cpf.Substring(6, 3) + "-" + cpf.Substring(9, 2)
        Else
            Return cpf
        End If

    End Function

    Public Shared Function trataMoeda(ByVal valor As String) As String
        If valor = "&nbsp;" Or valor = "" Then
            Return ""
        Else
            'Return String.Format(String.Format("{0:c}", CDbl(valor))).Substring(3)
            Dim minus As String = ""

            If String.Format(String.Format("{0:c}", CDbl(valor))).Substring(3).IndexOf(")") > 0 Then
                minus = "-"
            End If

            Return minus & String.Format(String.Format("{0:c}", CDbl(valor))).Substring(3).Replace("(", "").Replace(")", "").Replace(" ", "")
        End If
    End Function

    Public Shared Function deParaDLL(ByVal valor As Integer, _
                                        ByVal limiteMinIdade As String, _
                                        ByVal limiteMaxIdade As String, _
                                        ByVal segcapital As String, _
                                        ByVal idade As String, _
                                        ByVal nomeConjuge As String) As String

        Select Case valor
            Case 0
                Return ""
            Case 1
                Return "Data de In�cio de Cobertura menor que o in�cio de vig�ncia da ap�lice."
            Case 2
                Return "Data de Entrada no Subgrupo menor que o in�cio de vig�ncia da ap�lice."
            Case 3
                Return "Capital Segurado diferente do capital do subgrupo."
            Case 4
                Return "Capital Segurado n�o informado."
            Case 6
                Return "Sal�rio do Segurado n�o informado."
            Case 7
                Return "Capital Segurado abaixo do limite permitido para o subgrupo."
            Case 8
                Return "Capital Segurado acima do limite permitido para o subgrupo."
            Case 9
                Return "Titular n�o informado."
            Case 10
                Return "Titular j� associado a outro c�njuge."
            Case 11
                Return "Data de In�cio de Vig�ncia do C�njuge menor que a Data de In�cio de Vig�ncia do Titular."
            Case 14
                Return "N�o existe faixas et�rias para o Subgrupo na vig�ncia do segurado."
            Case 15
                Return "Proponente com idade inferior ao limite permitido do subgrupo."
            Case 16
                Return "Proponente com idade superior ao limite permitido do subgrupo."
            Case 17
                Return "Inclus�o de segurado j� existente."
            Case 18
                Return "Data de In�cio de Vig�ncia � menor que a data de fim de vig�ncia anterior."
            Case 19
                Return "As faixas et�rias correspondentes n�o possuem um capital segurado."
            Case 20
                Return "N�o existe faixa et�ria correspondente a idade em quest�o."
            Case 21
                Return "Segurado inativo, deve ser feita incluls�o ao inv�s da altera��o."
            Case 22
                Return "Segurado inativo, n�o � poss�vel a exclus�o."
            Case 23
                Return "Segurado n�o encontrado para altera��o."
            Case 24
                Return "Segurado n�o encontrado para exclus�o."
            Case 25
                Return "C�njuge n�o deve ser informado:."
            Case 26
                Return "J� existe um cliente ativo neste subgrupo com o mesmo CPF, Data de Nascimento e sexo."
            Case Else
                Return "Erro n�o encontrado."
        End Select

    End Function

    Public Shared Function MostraColunaCapital() As Boolean

        Dim mostraCapital As Boolean = True

        Dim capitalSegurado As String = getCapitalSeguradoSession()

        Select Case capitalSegurado
            'Case "Capital Global"
            '    mostraCapital = False
            'Case "M�ltiplo sal�rio"
            '    mostraCapital = True
            'Case "Capital fixo"
            '    mostraCapital = True
            'Case "Capital informado"
            '    mostraCapital = True
            'Case Else
            '    mostraCapital = True
        End Select

        Return mostraCapital

    End Function

    Public Shared Function MostraColunaSalario() As Boolean

        Dim mostraSalario As Boolean = False

        Dim capitalSegurado As String = getCapitalSeguradoSession()

        Select Case capitalSegurado
            Case "M�ltiplo sal�rio"
                mostraSalario = True

                'Case "Capital Global"
                '    mostraCapital = False
                'Case "M�ltiplo sal�rio"
                '    mostraCapital = True
                'Case "Capital fixo"
                '    mostraCapital = True
                'Case "Capital informado"
                '    mostraCapital = True
                'Case Else
                '    mostraCapital = True
        End Select

        Return mostraSalario

    End Function

    Public Shared Function MostraCapitalFaixaEtaria() As Boolean

        Dim show As Boolean = True

        Dim capitalSegurado As String = getCapitalSeguradoSession()

        Select Case capitalSegurado
            Case "Capital por faixa et�ria"
                'Case "Capital informado"
                show = True
            Case Else
                show = False
        End Select

        Return show

    End Function

    Public Shared Function verificaUsuarioHistorico(ByVal id As String) As Boolean

        Return True

        'Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        'bd.SQL = " exec SEGS5848_SPS " & id

        'Try
        '    Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR
        '    'Web.HttpContext.Current.Response.Write(bd.SQL)

        '    If dr.HasRows Then
        '        While dr.Read()
        '            'cUtilitarios.br(dr.GetValue(0).ToString())
        '            Return True
        '            'cUtilitarios.br(dr.GetValue(1).ToString())
        '        End While
        '        Return True
        '    End If
        '    dr.Close()
        '    dr = Nothing
        'Catch ex As Exception
        '    'Dim excp As New clsException(ex)
        'End Try
        'Return False

    End Function

    Public Shared Function getCapitalSeguradoSession() As String

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        bd.SEGS5706_SPS(Web.HttpContext.Current.Session("apolice"), _
                        Web.HttpContext.Current.Session("ramo"), _
                        6785, _
                        0, _
                        Web.HttpContext.Current.Session("subgrupo_id"))

        Dim capitalSegurado As String = ""

        Try
            dr = bd.ExecutaSQL_DR
            If Not dr Is Nothing Then
                If dr.Read Then
                    capitalSegurado = dr.GetValue(1).ToString()
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return capitalSegurado

    End Function

    Public Shared Function getLimMaxCapital() As Double
        Dim limMax As Double = 0
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim valor_capital_global As String = "0"

        bd.SEGS5744_SPS(Web.HttpContext.Current.Session("apolice"), _
                        Web.HttpContext.Current.Session("ramo"), _
                        Web.HttpContext.Current.Session("subgrupo_id"))

        Try

            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    limMax = IIf(dr.GetValue(0).ToString.Trim = "", "0", CType(dr.GetValue(0).ToString.Trim, Double))

                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return limMax

    End Function

    Public Shared Function getLimMinCapital() As Double
        Dim limMin As Double = 0
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim valor_capital_global As String = "0"

        bd.SEGS5744_SPS(Web.HttpContext.Current.Session("apolice"), _
                            Web.HttpContext.Current.Session("ramo"), _
                            Web.HttpContext.Current.Session("subgrupo_id"))

        Try
            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    limMin = IIf(dr.GetValue(5).ToString.Trim = "", "0", CType(dr.GetValue(5).ToString.Trim, Double))
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return limMin

    End Function

    Public Shared Sub Exportar(ByVal dt As System.Data.DataTable)
        Dim g As New Guid
        Dim oResponse As System.Web.HttpResponse _
         = System.Web.HttpContext.Current.Response
        oResponse.Clear()
        oResponse.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250")
        oResponse.AddHeader("Content-Type", "application/xls charset=iso-8859-1")
        oResponse.AddHeader("Pragma", "public")
        oResponse.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
        oResponse.AddHeader("Content-Disposition", "attachment; filename=Export.xls")
        'oResponse.ContentType = "application/vnd.ms-excel"
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)

        Dim dg As New System.Web.UI.WebControls.DataGrid
        dg.DataSource = dt
        dg.DataBind()
        dg.RenderControl(htmlWrite)
        oResponse.Write(stringWrite.ToString)
        'oResponse.End()
        Exit Sub
    End Sub
End Class
