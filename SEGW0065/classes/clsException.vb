Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Public Class clsException

    Inherits System.ApplicationException

    Private _Excecp As Exception    'Simboliza o Exception gerado
    Private msg As String = "Ocorreu um erro inesperado, por favor, tente novamente. Caso o problema persista, contate o Administrador do Sistema."
    Public sql As String = ""

    'Montagem do Sub New, servir� para instanciarmos a Classe
    'E passar o Exception como Parametro
    Sub New(ByRef ex As Exception)
        _Excecp = ex
        AvalException(_Excecp)
    End Sub

    Sub New(ByRef ex As Exception, ByVal _sql As String)
        _Excecp = ex
        sql = _sql
        AvalException(_Excecp)
    End Sub

    Private Sub AvalException(ByVal ex As Exception)

        System.Web.HttpContext.Current.Response.Write("<!--" & ex.Message & ex.StackTrace & "-->")

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim sigla As String = "SEGW0065"

        'Insere o log de erro na tabela VIDA_WEB_DB..
        bd.SQL = "exec VIDA_WEB_DB..SEGS5975_SPI @sigla_aplicacao = '" & sigla & "', @descricao= '" & ex.Message & Replace(ex.StackTrace, "'", "�") & sql.Replace("""", "") & "',@usuario = '" & System.Web.HttpContext.Current.Session("usuario") & "'"

        ''Flow 659932 - dgomes - CONFITEC
        '' A altera��o abaixo fez com que a procedure retornasse o id do log gerado.
        '' Este id ser� exibido no alert junto com a mensagem padr�o.
        Dim dt As DataTable = Nothing
        Dim idLog As Integer = 0

        Try
            'dt = bd.ExecutaSQL_DT
            If Not (dt Is Nothing) Then
                If dt.Rows.Count > 0 Then
                    idLog = Convert.ToInt32(dt.Rows(0)(0))
                End If
            End If

        Catch ex1 As Exception
            Dim excp As New clsException(ex1)
        End Try

        msg = msg & " \nLog do erro gerado: (" & idLog & ")"

        'Tratamento dos poss�veis Exceptions
        'Aqui voc� pode configurar suas mensagens de erro de acordo com o exception
        If (TypeOf ex Is System.ArgumentOutOfRangeException) Then
            cUtilitarios.br(msg)
            Exit Sub

        ElseIf (TypeOf ex Is System.IO.FileLoadException) Then
            cUtilitarios.br(msg)
            Exit Sub

        ElseIf (TypeOf ex Is System.IO.FileNotFoundException) Then
            cUtilitarios.br(msg)
            Exit Sub

        ElseIf (TypeOf ex Is System.IO.DirectoryNotFoundException) Then
            cUtilitarios.br(msg)
            Exit Sub

        ElseIf (TypeOf ex Is System.OverflowException) Then
            cUtilitarios.br(msg)
            Exit Sub

        ElseIf (TypeOf ex Is System.StackOverflowException) Then
            cUtilitarios.br(msg)
            Exit Sub

        ElseIf (TypeOf ex Is InvalidCastException) Then
            cUtilitarios.br(msg)
            Exit Sub

        ElseIf (TypeOf ex Is System.NullReferenceException) Then
            cUtilitarios.br(msg)
            Exit Sub

        ElseIf (TypeOf ex Is InvalidOperationException) Then
            cUtilitarios.br(msg)
            Exit Sub

        ElseIf (TypeOf ex Is SqlException) Then

            'Erros de SQL
            Dim ex1 As SqlException = ex
            Select Case ex1.Number

                Case 547
                    cUtilitarios.br(msg)

                Case 208
                    cUtilitarios.br(msg)

                Case 137
                    cUtilitarios.br(msg)

                Case 170
                    cUtilitarios.br(msg)

                Case 207
                    cUtilitarios.br(msg)

                Case Else
                    cUtilitarios.br(msg)

            End Select
            Exit Sub

        Else
            'Caso n�o seja nenhum dos Exceptions declarados
            cUtilitarios.br(msg)
            Exit Sub
        End If
    End Sub

End Class