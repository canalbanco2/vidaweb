Namespace DAO


    Public Class cSubgrupoDAO
        Inherits cBanco

        Public Sub New(Optional ByVal banco As cDadosBasicos.eBanco = cDadosBasicos.eBanco.web_seguros_db)
            MyBase.New(banco)
            Alianca.Seguranca.BancoDados.cCon.BancodeDados = banco.ToString
        End Sub

        Public Function getDadosSubgrupo(ByVal pRamo_id As Int16, ByVal pApolice_id As Integer, ByVal pSubgrupo_id As Int16) As Model.cSubgrupoTO

            Dim subgrupoTO As New Model.cSubgrupoTO

            '''1� Parte - Obtem os valores M�nimo e M�ximo para a Idade
            Me.SQL = "exec web_seguros_db..SEGS5705_SPS "
            Me.SQL &= " @apolice_id = " & pApolice_id
            Me.SQL &= ", @ramo_id = " & pRamo_id
            Me.SQL &= ", @seguradora_cod_susep = 6785"
            Me.SQL &= ", @sucursal_seguradora_id = 0"
            Me.SQL &= ", @sub_grupo_id = " & pSubgrupo_id

            Dim dr As Data.SqlClient.SqlDataReader = Me.ExecutaSQL_DR()

            If dr.Read Then
                subgrupoTO.IdadeMaxima = IIf(IsDBNull(dr.GetInt16(1)), -1, dr.GetInt16(1))
                subgrupoTO.IdadeMinima = IIf(IsDBNull(dr.GetInt16(0)), -1, dr.GetInt16(0))
            End If
            dr.Close()
            '''Fim 1� Parte - Obtem os valores M�nimo e M�ximo para a Idade

            '''2� Parte - Obtem os valores do Tipo de conjuge, Tipo do capital segurado e fator de m�ltiplo sal�rio
            Me.SQL = "exec web_seguros_db..SEGS5706_SPS "
            Me.SQL &= " @apolice_id = " & pApolice_id
            Me.SQL &= ", @ramo_id = " & pRamo_id
            Me.SQL &= ", @seguradora_cod_susep = 6785"
            Me.SQL &= ", @sucursal_seguradora_id = 0"
            Me.SQL &= ", @sub_grupo_id = " & pSubgrupo_id

            dr = Me.ExecutaSQL_DR()


            If dr.Read Then
                subgrupoTO.FatorMultiploSalario = IIf(IsDBNull(dr.GetInt16(2)), -1, dr.GetInt16(2))
                subgrupoTO.TipoCapitalSegurado = IIf(IsDBNull(dr.GetString(1)), "", dr.GetString(1))
                subgrupoTO.TipoConjuge = IIf(IsDBNull(dr.GetString(0)), "", dr.GetString(0))
            End If
            dr.Close()
            '''Fim 2� Parte - Obtem os valores do Tipo de conjuge, Tipo do capital segurado e fator de m�ltiplo sal�rio

            ''' 3� Parte - Obtem os valores de limite m�nimo e m�ximo
            Me.SQL = "exec web_seguros_db..SEGS5744_SPS "
            Me.SQL &= " @apolice_id = " & pApolice_id
            Me.SQL &= ", @ramo_id = " & pRamo_id
            Me.SQL &= ", @cod_susep = 6785"
            Me.SQL &= ", @seguradora_id = 0"
            Me.SQL &= ", @subgrupo_id = " & pSubgrupo_id
            Me.SQL &= ", @usuario = ''"
            Me.SQL &= ", @ind_acesso = ''"
            Me.SQL &= ", @cpf_id = ''"

            dr = Me.ExecutaSQL_DR()

            If dr.Read Then
                subgrupoTO.LimiteMaximo = IIf(IsDBNull(dr.GetDouble(0)), -1, dr.GetDouble(0))
                subgrupoTO.LimiteMinimo = cUtilitarios.getLimMinCapital()
                'If Me.LblLimiteMinimo.Text = 0 Or Me.LblLimiteMinimo.Text = "" Then
                '    Me.LblLimiteMinimo.Text = "0,00"
                'End If
                subgrupoTO.ValorCapital = IIf(IsDBNull(dr.GetDouble(4)), -1, dr.GetDouble(4))
            End If
            dr.Close()
            ''' 3� Parte - Obtem os valores de limite m�nimo e m�ximo
            dr = Nothing

            Return subgrupoTO

        End Function
    End Class

End Namespace
