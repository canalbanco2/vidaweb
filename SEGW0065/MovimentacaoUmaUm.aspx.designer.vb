'------------------------------------------------------------------------------
' <gerado automaticamente>
'     Este código foi gerado por uma ferramenta.
'
'     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
'     o código for recriado
' </gerado automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class MovimentacaoUmaUm

    '''<summary>
    '''Controle Form1.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''Controle Panel1.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Panel1 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Controle lblNavegacao.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblNavegacao As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblVigencia.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblVigencia As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle Label17.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Label17 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle td_valor_capital.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents td_valor_capital As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''Controle lblCapitalSegurado.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblCapitalSegurado As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle td_valor_capital_lbl.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents td_valor_capital_lbl As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''Controle lblValorCapital.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblValorCapital As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle tdLimiteMinimo.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents tdLimiteMinimo As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''Controle LblLimiteMinimo.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents LblLimiteMinimo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle tdLimiteMaximo.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents tdLimiteMaximo As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''Controle lblLimiteMaximo.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblLimiteMaximo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblMultSalarial.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblMultSalarial As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblIdadeMinima.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblIdadeMinima As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblIdadeMaxima.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblIdadeMaxima As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblConjuge.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblConjuge As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle Panel2.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Panel2 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Controle Label1.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Label1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle DropdownSelComponente.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents DropdownSelComponente As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Controle tabelaMovimentacao.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents tabelaMovimentacao As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''Controle pnlResumo.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents pnlResumo As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Controle Imagebutton2.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Imagebutton2 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Controle Label4.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Label4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle Label5.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Label5 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblVidasFA.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblVidasFA As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblCapitalFA.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblCapitalFA As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblPremioFA.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblPremioFA As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle btnInclusoes.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnInclusoes As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Controle lblInclusoes_vidas.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblInclusoes_vidas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblInclusoes_capital.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblInclusoes_capital As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblInclusoes_premio.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblInclusoes_premio As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle btnExclusoes.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnExclusoes As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Controle lblExclusoes_vidas.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblExclusoes_vidas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblExclusoes_capital.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblExclusoes_capital As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblExclusoes_premio.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblExclusoes_premio As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle btnAlteracoes.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnAlteracoes As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Controle lblAlteracoes_vidas.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblAlteracoes_vidas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblAlteracoes_capital.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblAlteracoes_capital As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblAlteracoes_premio.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblAlteracoes_premio As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle btnAcertos.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnAcertos As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Controle lblAcertos_vidas.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblAcertos_vidas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblAcertos_capital.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblAcertos_capital As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblAcertos_premio.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblAcertos_premio As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle btnDpsPendente.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnDpsPendente As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Controle lblDPS_vidas.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblDPS_vidas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblDPS_capital.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblDPS_capital As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblDPS_premio.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblDPS_premio As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle btnExcAgen.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnExcAgen As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Controle lblExcAgen_vidas.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblExcAgen_vidas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblExcAgen_capital.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblExcAgen_capital As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblExcAgen_premio.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblExcAgen_premio As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblVidasFT.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblVidasFT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblCapitalFT.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblCapitalFT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblPremioFT.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblPremioFT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle pnlAcoes.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents pnlAcoes As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Controle divBtnIncluiDependente.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents divBtnIncluiDependente As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle Button1.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Button1 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Controle btnIncluiDependente.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnIncluiDependente As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Controle btnAlterar.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnAlterar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Controle btnExcluir2.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnExcluir2 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Controle btnDesfazAlteracoes.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnDesfazAlteracoes As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Controle btnFechaPagina.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnFechaPagina As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Controle lblListagem.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblListagem As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lbltiposeleção.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lbltiposeleção As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lbllistacriterio.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lbllistacriterio As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle DropDownList2.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents DropDownList2 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Controle btnFiltrar.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnFiltrar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Controle lblFiltro.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblFiltro As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle gridMovimentacao.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents gridMovimentacao As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''Controle grdPesquisa.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents grdPesquisa As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''Controle lblSemUsuario.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblSemUsuario As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle idTitular.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents idTitular As Global.System.Web.UI.HtmlControls.HtmlInputHidden
End Class
