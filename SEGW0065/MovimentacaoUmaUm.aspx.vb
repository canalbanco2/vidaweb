Partial Class MovimentacaoUmaUm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ucPaginacao As paginacaogrid


    Protected WithEvents lblTopUsuario As System.Web.UI.WebControls.Label
    Protected WithEvents lblTopCPF As System.Web.UI.WebControls.Label





    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Try
        Dim linkseguro As Alianca.Seguranca.Web.LinkSeguro

        'Implementa��o da SABL0101
        linkseguro = New Alianca.Seguranca.Web.LinkSeguro
        Dim usuario As String = linkseguro.LerUsuario("MovimentacaoUmaUm.aspx", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))
        If Request.QueryString("fatura") = "" Then
            If usuario = "" Then
                'Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
            End If

            Session("usuario_id") = linkseguro.Usuario_ID
            Session("usuario") = linkseguro.Login_WEB
            Session("cpf") = linkseguro.CPF
        End If

        Dim cAmbiente As Alianca.Seguranca.Web.ControleAmbiente
        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"

        cAmbiente.ObterAmbiente(url)
        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
        Else
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
        End If
        Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)

        'Inicio do desenvolvimento da p�gina
        If Not Page.IsPostBack Then
            'GPTI - DEMANDA 539202
            If (Session("auxPagina") = Nothing) Then
                Session("pagina") = 0
            End If
            'FIM

            For Each m As String In Request.QueryString.Keys
                If m <> "" Then
                    Session(m) = Request.QueryString(m)
                End If
            Next

            Dim tipo_capital As String = cUtilitarios.getCapitalSeguradoSession()
            Dim excluir As String = Request.QueryString("excluir")
            Dim expandir As String = Request.QueryString("expandir")

            'Pega os dados das condi��es do Subgrupo
            getCondicoesSubgrupo()

            'Configura o filtro
            Dim filtraPesquisa As String = Request.QueryString("acao")

            Me.configFiltro(filtraPesquisa)
            mConsultaUsuarios("", "", "")

        Else
            Dim tipo_capital As String = cUtilitarios.getCapitalSeguradoSession()

            'Me.getCoberturas(Me.DropdownSelComponente.SelectedValue)

            Dim cpf As String = ""
            Dim nome As String = ""
            Dim tipo As String = ""
            Dim ddl As String = ""

            ddl = Request.Form("DropDownList2")
            If ddl.Length > 0 Then
                If ddl = "CPF" Then
                    cpf = Request.Form("campoPesquisa").Replace(".", "").Replace("-", "")
                ElseIf ddl = "Nome" Then
                    nome = Request.Form("campoPesquisa")
                ElseIf ddl = "Inclusoes" Or ddl = "Alteracoes" Or ddl = "Exclusoes" Or ddl = "Historico" Then
                    tipo = ddl.Substring(0, 1)
                End If
            End If

            'Verifica se o Post dado na tela foi causado pela pagina��o
            '� pagina��o j� executa esse metodo
            If Request("__EVENTTARGET").IndexOf("ucPaginacao") = -1 Then
                mConsultaUsuarios(cpf, nome, tipo)
            End If
        End If

        Try
            'cUtilitarios.br(Request.Form("__EVENTTARGET"))
            If Request.Form("__EVENTTARGET") = "" Then
                If Not Request.Form("gerarXml") Is Nothing AndAlso Request.Form("gerarXml") <> "" Then
                    If Request.Form("gerarXml") = 1 Then
                        geraXml()
                        Response.End()
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.GetType.ToString <> "System.Threading.ThreadAbortException" Then
                Dim excp As New clsException(ex)
            End If
        End Try

        'Recalcular o resumo das opera��es - VIDA_WEB_DB..SEGS7048_SPI
        Alianca.Seguranca.BancoDados.cCon.ConnectionTimeout = Alianca.Seguranca.BancoDados.cCon.TimeTimeout.Infinito
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.Recalcula_Resumo_Operacoes(Session("apolice"), _
                                      Session("ramo"), _
                                      Session("subgrupo_id"), _
                                      Session("usuario"))

        Try
            bd.ExecutaSQL()
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        faturaAtual()
        'montaFaturaAtual()

        lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomesubGrupo") & "<br>Fun��o: Movimenta��o on-line"
        Response.Write("<input type='hidden' value='" & Session("acesso") & "' id='ind_acesso' name='ind_acesso'>")

        ''In�cio JavaScript
        btnAlterar.Attributes.Add("onclick", "return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=alterar', 280, 450)")
        btnIncluiDependente.Attributes.Add("onclick", "return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=incluirdependente', 280, 450)")
        btnExcluir2.Attributes.Add("onclick", "return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluir', 280, 450)")
        DropDownList2.Attributes.Add("onChange", "getPesquisa(this.value)")
        btnFiltrar.Attributes.Add("onClick", "buscaDados()")
        btnFiltrar.Attributes.Add("style", "padding-right:0;margin-right:0;")
        btnDesfazAlteracoes.Attributes.Add("onclick", "return confirmaDesfazer()")

        lblVigencia.Text = "<br>" & getPeriodoCompetenciaSession()
        'Me.DropdownSelComponente.Attributes.Add("onchange", "top.exibeaguarde();")
        Me.btnFiltrar.Attributes.Add("onclick", "top.exibeaguarde();")

        Session.Timeout = 90
        'Catch ex As Exception
        '    Dim excp As New clsException(ex)
        'End Try
    End Sub

    Private Sub geraXml()
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        Dim cpf As String = "null"
        Dim nome As String = "null"
        Dim tipo As String = "null"

        bd.SEGS5655_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "NULL", "NULL", "null", "null", 1)

        Try
            Dim dt As Data.DataTable = bd.ExecutaSQL_DT

            cUtilitarios.Exportar(dt)
            Exit Sub
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Private Function getTpAcesso() As String
        Try
            Dim ind_acesso As String = ""
            Select Case Session("acesso").ToString
                Case "1"
                    ind_acesso = "Administrador da Alian�a do Brasil"
                Case "2"
                    ind_acesso = "Administrador de Ap�lice"
                Case "3"
                    ind_acesso = "Administrador de Subgrupo"
                Case "4"
                    ind_acesso = "Operador"
                Case "5" ' Projeto 539202 - Jo�o Ribeiro - 29/04/2009
                    ind_acesso = "Consulta"
                Case "6" ' Projeto 539202 - Jo�o Ribeiro - 29/04/2009
                    ind_acesso = "Central de Atendimento"
            End Select

            Response.Write("<input type='hidden' value='" & Session("acesso") & "' id='ind_acesso' name='ind_acesso'>")

            Return ind_acesso
        Catch ex As Exception
            Dim excp As New clsException(ex)
            Return ""
        End Try

    End Function

    Private Sub faturaAtual()
        'Pega wf_id para Fatura Atual
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim wf_id As String = ""

        Try

            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    wf_id = dr.GetValue(0).ToString()
                End If
                If Not dr.IsClosed Then dr.Close()
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        If wf_id <> "" Then
            'Mostra Resumo da Fatura Atual
            bd.ResumoFaturaAtual_Anterior(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Session("usuario"), wf_id)
            Dim totalVidaFA As Integer, totalCapitalFA As Double, totalPremioFA As Double

            Try
                dr = bd.ExecutaSQL_DR

                If Not dr Is Nothing Then
                    If dr.Read() Then
                        totalVidaFA = dr.GetValue(8).ToString
                        totalCapitalFA = cUtilitarios.trataMoeda(dr.GetValue(6).ToString)
                        '539202 - CONFITEC
                        'totalPremioFA = cUtilitarios.trataMoeda(dr.GetValue(7).ToString)
                        totalPremioFA = cUtilitarios.trataMoeda(dr("val_bruto"))


                        lblVidasFA.Text = dr.GetValue(8).ToString
                        lblCapitalFA.Text = cUtilitarios.trataMoeda(dr.GetValue(6).ToString)
                        '539202 - CONFITEC
                        'lblPremioFA.Text = cUtilitarios.trataMoeda(dr.GetValue(7).ToString)
                        lblPremioFA.Text = cUtilitarios.trataMoeda(dr("val_bruto"))

                    Else
                        lblVidasFA.Text = "0"
                        lblCapitalFA.Text = "0,00"
                        lblPremioFA.Text = "0,00"
                    End If
                    If Not dr.IsClosed Then dr.Close()
                End If

            Catch ex As Exception
                Dim excp As New clsException(ex)
            End Try
            '----------------------------------------------

            'variavel que atualiza na segw0060, o Resumo da Fatura Atual
            Dim script As String = "<script>"

            script &= "top.document.getElementById('divInclusoes_vidas').innerHTML= '0';" & vbCrLf
            script &= "top.document.getElementById('divInclusoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
            script &= "top.document.getElementById('divInclusoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf

            script &= "top.document.getElementById('divExclusoes_vidas').innerHTML= '0';" & vbCrLf
            script &= "top.document.getElementById('divExclusoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
            script &= "top.document.getElementById('divExclusoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf

            script &= "top.document.getElementById('divAlteracoes_vidas').innerHTML= '0';" & vbCrLf
            script &= "top.document.getElementById('divAlteracoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
            script &= "top.document.getElementById('divAlteracoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf

            script &= "top.document.getElementById('divAcertos_vidas').innerHTML= '0';" & vbCrLf
            script &= "top.document.getElementById('divAcertos_capital').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
            script &= "top.document.getElementById('divAcertos_premio').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf

            script &= "top.document.getElementById('divDPS_vidas').innerHTML= '0';" & vbCrLf
            script &= "top.document.getElementById('divDPS_capital').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
            script &= "top.document.getElementById('divDPS_premio').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf

            script &= "top.document.getElementById('divSubTotal_vidas').innerHTML= '0';" & vbCrLf
            script &= "top.document.getElementById('divSubTotal_capital').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
            script &= "top.document.getElementById('divSubTotal_premio').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf

            script &= "top.document.getElementById('divExcAgen_vidas').innerHTML= '0';" & vbCrLf
            script &= "top.document.getElementById('divExcAgen_capital').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
            script &= "top.document.getElementById('divExcAgen_premio').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf

            'RECUPERA ALIQUOTA DE IOF
            Dim v_dPercIOF As Decimal = 0
            bd.getAliquotaIOF(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
            Dim v_oDtIOF As Data.DataTable = bd.ExecutaSQL_DT()
            If v_oDtIOF.Rows.Count > 0 Then v_dPercIOF = v_oDtIOF.Rows(0)("perc_iof")

            Dim totalVida As Integer = 0
            Dim totalCapital As Double = 0
            Dim totalPremio As Double = 0

            script &= "top.document.getElementById('divVidasFT').innerHTML= '" & totalVida + totalVidaFA & "';" & vbCrLf
            script &= "top.document.getElementById('divCapitalFT').innerHTML= '" & cUtilitarios.trataMoeda(totalCapital + totalCapitalFA) & "';" & vbCrLf
            script &= "top.document.getElementById('divPremioFT').innerHTML= '" & cUtilitarios.trataMoeda(totalPremio + totalPremioFA) & "';" & vbCrLf

            Dim qtdVidasSub As Integer = Int32.Parse(totalVidaFA)
            Dim valCapSubTotal As Double = Double.Parse(totalCapitalFA)
            Dim valPremioSubTotal As Double = Double.Parse(totalPremioFA)

            bd.ResumoFaturaAtual_IAE(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Session("usuario"), wf_id)
            dr = bd.ExecutaSQL_DR

            If Not dr Is Nothing Then
                While dr.Read
                    If dr.Item("operacao") = "I" Then
                        Try
                            qtdVidasSub = qtdVidasSub + Int32.Parse(dr.GetValue(0))
                            valCapSubTotal = valCapSubTotal + Double.Parse(dr.GetValue(1))
                            valPremioSubTotal = valPremioSubTotal + Double.Parse(dr.GetValue(2))
                        Catch ex As Exception
                            Dim excp As New clsException(ex)
                        End Try

                        script &= "top.document.getElementById('divInclusoes_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                        script &= "top.document.getElementById('divInclusoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                        script &= "top.document.getElementById('divInclusoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf

                        lblInclusoes_vidas.Text = dr.GetValue(0).ToString
                        lblInclusoes_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        '539202 - confitec
                        'lblInclusoes_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                        Dim v_dVlIncPremioBruto As Decimal = dr.GetValue(2) + (dr.GetValue(2) * v_dPercIOF)
                        lblInclusoes_premio.Text = cUtilitarios.trataMoeda(v_dVlIncPremioBruto.ToString)

                        'Soma
                        totalVida = Int32.Parse(totalVida) + Int32.Parse(dr.GetValue(0).ToString)
                        totalCapital = Double.Parse(totalCapital) + Double.Parse(dr.GetValue(1).ToString)
                        totalPremio = Double.Parse(totalPremio) + Double.Parse(v_dVlIncPremioBruto.ToString)

                    ElseIf dr.Item("operacao") = "E" Then
                        Try
                            qtdVidasSub = qtdVidasSub - Int32.Parse(dr.GetValue(0))
                            valCapSubTotal = valCapSubTotal - Double.Parse(dr.GetValue(1))
                            valPremioSubTotal = valPremioSubTotal - Double.Parse(dr.GetValue(2))
                        Catch ex As Exception
                            Dim excp As New clsException(ex)
                        End Try

                        script &= "top.document.getElementById('divExclusoes_vidas').innerHTML= '" & dr.GetValue(0).ToString & " ';" & vbCrLf

                        If cUtilitarios.trataMoeda(dr.GetValue(1)) <> "0,00" Then
                            script &= "top.document.getElementById('divExclusoes_capital').innerHTML= ' -" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & " ';" & vbCrLf
                        Else
                            script &= "top.document.getElementById('divExclusoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & " ';" & vbCrLf
                        End If

                        Dim v_dVlExcPremioBruto As Decimal = dr.GetValue(2) + (dr.GetValue(2) * v_dPercIOF)
                        If cUtilitarios.trataMoeda(dr.GetValue(2).ToString) <> "0,00" Then
                            '539202 - confitec
                            'script &= "top.document.getElementById('divExclusoes_premio').innerHTML= ' -" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & " ';" & vbCrLf
                            script &= "top.document.getElementById('divExclusoes_premio').innerHTML= ' -" & cUtilitarios.trataMoeda(v_dVlExcPremioBruto.ToString) & " ';" & vbCrLf
                        Else
                            script &= "top.document.getElementById('divExclusoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & " ';" & vbCrLf
                        End If

                        lblExclusoes_vidas.Text = dr.GetValue(0).ToString * -1

                        If cUtilitarios.trataMoeda(dr.GetValue(1).ToString) <> "0,00" Then
                            lblExclusoes_capital.Text = cUtilitarios.trataMoeda(cUtilitarios.trataMoeda(dr.GetValue(1).ToString))
                            'lblExclusoes_premio.Text = cUtilitarios.trataMoeda(cUtilitarios.trataMoeda(dr.GetValue(2).ToString))
                            lblExclusoes_premio.Text = "-" & cUtilitarios.trataMoeda(v_dVlExcPremioBruto.ToString)
                        Else
                            lblExclusoes_capital.Text = cUtilitarios.trataMoeda(cUtilitarios.trataMoeda(dr.GetValue(1).ToString) * -1)
                            lblExclusoes_premio.Text = cUtilitarios.trataMoeda(cUtilitarios.trataMoeda(dr.GetValue(2).ToString) * -1)
                        End If

                        'Subtrai
                        totalVida = totalVida - dr.GetValue(0).ToString
                        totalCapital = totalCapital - cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        totalPremio = totalPremio - cUtilitarios.trataMoeda(v_dVlExcPremioBruto.ToString)

                    ElseIf dr.Item("operacao") = "A" Then
                        Dim v_dVlAltPremioBruto As Decimal = dr.GetValue(2) + (dr.GetValue(2) * v_dPercIOF)

                        script &= "top.document.getElementById('divAlteracoes_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                        script &= "top.document.getElementById('divAlteracoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                        '539202 - CONFITEC
                        'script &= "top.document.getElementById('divAlteracoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf
                        script &= "top.document.getElementById('divAlteracoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(v_dVlAltPremioBruto.ToString) & "';" & vbCrLf

                        lblAlteracoes_vidas.Text = dr.GetValue(0).ToString
                        lblAlteracoes_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        '539202 - CONFITEC
                        'lblAlteracoes_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                        lblAlteracoes_premio.Text = cUtilitarios.trataMoeda(v_dVlAltPremioBruto.ToString)








                    ElseIf dr.Item("operacao") = "P" Then
                        Dim v_dVlDPSPremioBruto As Decimal = dr.GetValue(2) + (dr.GetValue(2) * v_dPercIOF)

                        script &= "top.document.getElementById('divDPS_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                        script &= "top.document.getElementById('divDPS_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                        '539202 - CONFITEC
                        'script &= "top.document.getElementById('divDPS_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf
                        script &= "top.document.getElementById('divDPS_premio').innerHTML= '" & cUtilitarios.trataMoeda(v_dVlDPSPremioBruto.ToString) & "';" & vbCrLf

                        lblExcAgen_vidas.Text = dr.GetValue(0).ToString
                        lblExcAgen_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        '539202 - CONFITEC
                        'lblExcAgen_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                        lblExcAgen_premio.Text = cUtilitarios.trataMoeda(v_dVlDPSPremioBruto.ToString)
                    ElseIf dr.Item("operacao") = "R" Then
                        Dim v_dVlAcertosPremioBruto As Decimal = dr.GetValue(2) + (dr.GetValue(2) * v_dPercIOF)


                        script &= "top.document.getElementById('divAcertos_vidas').innerHTML= '" & dr.GetValue(0).ToString() & "';" & vbCrLf
                        script &= "top.document.getElementById('divAcertos_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                        '539202 - CONFITEC
                        'script &= "top.document.getElementById('divAcertos_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf
                        script &= "top.document.getElementById('divAcertos_premio').innerHTML= '" & cUtilitarios.trataMoeda(v_dVlAcertosPremioBruto.ToString) & "';" & vbCrLf

                        lblAcertos_vidas.Text = dr.GetValue(0).ToString
                        lblAcertos_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        '539202 - CONFITEC
                        'lblAcertos_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                        lblAcertos_premio.Text = cUtilitarios.trataMoeda(v_dVlAcertosPremioBruto.ToString)
                    ElseIf dr.Item("operacao") = "B" Then
                        Dim v_dVlExcAgendadaPremioBruto As Decimal = dr.GetValue(2) + (dr.GetValue(2) * v_dPercIOF)

                        script &= "top.document.getElementById('divExcAgen_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                        script &= "top.document.getElementById('divExcAgen_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                        '539202 - CONFITEC
                        'script &= "top.document.getElementById('divExcAgen_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf
                        script &= "top.document.getElementById('divExcAgen_premio').innerHTML= '" & cUtilitarios.trataMoeda(v_dVlExcAgendadaPremioBruto.ToString) & "';" & vbCrLf

                        lblDPS_vidas.Text = dr.GetValue(0).ToString
                        lblDPS_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        '539202 - CONFITEC
                        'lblDPS_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                        lblDPS_premio.Text = cUtilitarios.trataMoeda(v_dVlExcAgendadaPremioBruto.ToString)

                        '539202
                        'ElseIf dr.Item("operacao") = "T" Then
                    ElseIf dr.Item("operacao") = "F" Then
                        script &= "top.document.getElementById('divVidasFT').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                        script &= "top.document.getElementById('divCapitalFT').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                        script &= "top.document.getElementById('divPremioFT').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf

                        lblVidasFT.Text = dr.GetValue(0).ToString
                        lblCapitalFT.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        lblPremioFT.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)

                    End If

                End While

                script &= "top.document.getElementById('divSubTotal_vidas').innerHTML= '" & qtdVidasSub & "';" & vbCrLf
                script &= "top.document.getElementById('divSubTotal_capital').innerHTML= '" & cUtilitarios.trataMoeda(valCapSubTotal) & "';" & vbCrLf
                script &= "top.document.getElementById('divSubTotal_premio').innerHTML= '" & cUtilitarios.trataMoeda(valPremioSubTotal) & "';" & vbCrLf

                TrataLabelsVazios()

                bd = New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
                bd.getVidasPendentes(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Session("usuario"))

                Dim dr2 As Data.SqlClient.SqlDataReader = Nothing

                Try
                    dr2 = bd.ExecutaSQL_DR()
                    If Not dr2 Is Nothing Then
                        If dr2.Read() Then
                            If CInt(dr2.GetValue(0).ToString) > 0 Then
                                script &= "top.document.getElementById('link80').style.color = 'red';"
                            Else
                                script &= "top.document.getElementById('link80').style.color = '#555555';"
                            End If
                        End If
                        If Not dr2.IsClosed Then dr2.Close()
                        dr2 = Nothing
                    End If
                Catch ex As Exception
                    Dim excp As New clsException(ex)
                End Try

            End If
            script &= ""

            Response.Write(script & "</script>")
            'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "script", jScript, True)
        End If

    End Sub

    Private Sub TrataLabelsVazios()
        'Trata valores (label) vazios
        If lblVidasFA.Text = "" Then
            lblVidasFA.Text = "0"
        End If
        If lblCapitalFA.Text = "" Then
            lblCapitalFA.Text = "0,00"
        End If
        If lblPremioFA.Text = "" Then
            lblPremioFA.Text = "0,00"
        End If

        If lblInclusoes_vidas.Text = "" Then
            lblInclusoes_vidas.Text = "0"
        End If
        If lblInclusoes_capital.Text = "" Then
            lblInclusoes_capital.Text = "0,00"
        End If
        If lblInclusoes_premio.Text = "" Then
            lblInclusoes_premio.Text = "0,00"
        End If

        If lblExclusoes_vidas.Text = "" Then
            lblExclusoes_vidas.Text = "0"
        End If
        If lblExclusoes_capital.Text = "" Then
            lblExclusoes_capital.Text = "0,00"
        End If
        If lblExclusoes_premio.Text = "" Then
            lblExclusoes_premio.Text = "0,00"
        End If

        If lblAlteracoes_vidas.Text = "" Then
            lblAlteracoes_vidas.Text = "0"
        End If
        If lblAlteracoes_capital.Text = "" Then
            lblAlteracoes_capital.Text = "0,00"
        End If
        If lblAlteracoes_premio.Text = "" Then
            lblAlteracoes_premio.Text = "0,00"
        End If

        If lblDPS_vidas.Text = "" Then
            lblDPS_vidas.Text = "0"
        End If
        If lblDPS_capital.Text = "" Then
            lblDPS_capital.Text = "0,00"
        End If
        If lblDPS_premio.Text = "" Then
            lblDPS_premio.Text = "0,00"
        End If

        If lblAcertos_vidas.Text = "" Then
            lblAcertos_vidas.Text = "0"
        End If

        If lblAcertos_capital.Text = "" Then
            lblAcertos_capital.Text = "0,00"
        End If

        If lblAcertos_capital.Text = "" Then
            lblAcertos_capital.Text = "0"
        End If

        If lblExcAgen_capital.Text = "" Then
            lblExcAgen_capital.Text = "0,00"
        End If

        If lblExcAgen_premio.Text = "" Then
            lblExcAgen_premio.Text = "0,00"
        End If

        If lblAcertos_premio.Text = "" Then
            lblAcertos_premio.Text = "0,00"
        End If

        If lblVidasFT.Text = "" Then
            lblVidasFT.Text = lblVidasFA.Text
        End If
        If lblCapitalFT.Text = "" Then
            lblCapitalFT.Text = lblCapitalFA.Text
        End If
        If lblPremioFT.Text = "" Then
            lblPremioFT.Text = lblPremioFA.Text
        End If
    End Sub

    ''Configura o filtro de pesquisa
    Private Sub configFiltro(ByVal filtraPesquisa As String)
        Try
            If filtraPesquisa = "incl" Or filtraPesquisa = "excl" Or filtraPesquisa = "excagen" Or filtraPesquisa = "alter" Or filtraPesquisa = "acer" Or filtraPesquisa = "dps" Then
                Dim valor As String
                If filtraPesquisa = "incl" Then
                    valor = "Inclus�es"
                    DropDownList2.SelectedValue = "Inclusoes"
                ElseIf filtraPesquisa = "excl" Then
                    valor = "Exclus�es"
                    DropDownList2.SelectedValue = "Exclusoes"
                ElseIf filtraPesquisa = "alter" Then
                    valor = "Altera��es"
                    DropDownList2.SelectedValue = "Alteracoes"
                ElseIf filtraPesquisa = "acer" Then
                    valor = "Acertos"
                    DropDownList2.SelectedValue = "Acertos"
                ElseIf filtraPesquisa = "excagen" Then
                    valor = "Exclus�es Agendadas"
                    DropDownList2.SelectedValue = "ExclusaoAgendada"
                ElseIf filtraPesquisa = "acer" Then
                    valor = "Acertos"
                    DropDownList2.SelectedValue = "Acertos"
                Else
                    valor = "DPS Pendente"
                    DropDownList2.SelectedValue = "DPS"
                End If

                lblFiltro.Text = "<div align='left'>Pesquisado por: " & valor & "</div>"
                lblFiltro.Visible = True

            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Private Function getPeriodoCompetenciaSession() As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim data_inicio As String = ""
        Dim data_fim As String = ""
        Dim retorno As String = ""

        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

        Try

            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then

                    data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                    data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)
                    Session("dt_inicio_vigencia") = data_inicio
                    Session("dt_fim_vigencia") = data_fim
                    cUtilitarios.escreveScript("var dt_ini_comp = '" & data_inicio & "';")
                    cUtilitarios.escreveScript("var dt_fim_comp = '" & data_fim & "';")
                    cUtilitarios.escreveScript("var dt_fim_comp_2_meses = '" & CType(data_inicio, DateTime).AddMonths(-2) & "';")

                    retorno = "Per�odo de Compet�ncia: " & data_inicio & " a " & data_fim
                Else
                    Try
                        If Session("sessionValidate") Is Nothing Then
                            If Request.QueryString.Keys.Count > 0 Then
                                For Each m As String In Request.QueryString.Keys
                                    If m <> "" Then
                                        Session(m) = Request.QueryString(m)
                                    End If
                                Next

                                Session("sessionValidate") = 1

                                If (Not Session("apolice") Is Nothing) And (Not Session("ramo") Is Nothing) And (Not Session("subgrupo_id") Is Nothing) Then
                                    getPeriodoCompetenciaSession()
                                    'Response.End()
                                End If
                            End If
                        End If
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try

                    'Session.Abandon()
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
            Return retorno
        Catch ex As Exception
            Dim excp As New clsException(ex)
            Return ""
        End Try

    End Function

    ''Consulta os usu�rios
    Private Sub mConsultaUsuarios(ByVal cpf As String, ByVal nome As String, ByVal tipo As String)
        Try
            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

            If Trim(cpf).Length <= 0 Then
                cpf = "null"
            Else
                cpf = "'%" + cpf + "%'"
            End If

            If Trim(nome).Length <= 0 Then
                nome = "null"
            Else
                nome = "'%" & nome & "%'"
            End If

            If Trim(tipo).Length <= 0 Then
                tipo = "null"
            End If

            bd.SEGS5655_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), cpf, nome, "null", tipo, 1)

            Try
                'Flow 659932 - Altera��o pra carregar a grid com a listagem de vidas 
                'Pmarques - data altera��o: 30/01/2009
                'Alianca.Seguranca.BancoDados.cCon.ConnectionTimeout = Alianca.Seguranca.BancoDados.cCon.TimeTimeout.Infinito
                Alianca.Seguranca.BancoDados.cCon.ConnectionTimeout = Alianca.Seguranca.BancoDados.cCon.TimeTimeout.Grande
                Dim dt As Data.DataTable = bd.ExecutaSQL_DT

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    Me.grdPesquisa.CurrentPageIndex = Session("pagina")
                    Me.grdPesquisa.SelectedIndex = -1
                    Me.ucPaginacao.valor = "2"

                    Session("grdDescarte") = dt
                    Me.ucPaginacao.GridDataBind(Me.grdPesquisa, dt)

                    Me.lblSemUsuario.Visible = False
                    ucPaginacao.Visible = True
                    grdPesquisa.Visible = True
                    gridMovimentacao.Visible = True
                Else
                    Session.Remove("indice")
                    Session.Remove("grdDescarte")
                    Me.lblSemUsuario.Text = "<br><br><br>Nenhuma Vida encontrada.<br><br>"
                    Me.lblSemUsuario.Visible = True
                    ucPaginacao.Visible = False
                    grdPesquisa.Visible = False
                    gridMovimentacao.Visible = False
                End If
            Catch ex As Exception
                Dim excp As New clsException(ex)
            End Try
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Private Sub btnDesfazAlteracoes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("MovimentacaoUmaUm.aspx")
    End Sub

    Private Sub DropDownList2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DropDownList2.SelectedIndexChanged

        Try
            If DropDownList2.SelectedValue = "Todos" Then
                lblFiltro.Visible = False
            ElseIf DropDownList2.SelectedValue = "Inclusoes" Or DropDownList2.SelectedValue = "Exclusoes" Or DropDownList2.SelectedValue = "Alteracoes" Or DropDownList2.SelectedValue = "ExclusaoAgendada" Or DropDownList2.SelectedValue = "Acertos" Then
                lblFiltro.Visible = False
            Else
                lblFiltro.Visible = False
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Sub btnFechaPagina_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("Centro.aspx")
    End Sub

    Private Sub getCondicoesSubgrupo()
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        bd.SEGS5705_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

        Try

            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    Me.lblIdadeMaxima.Text = IIf(dr.GetValue(1).ToString.Trim = "", " --- ", dr.GetValue(1).ToString.Trim)
                    Me.lblIdadeMinima.Text = IIf(dr.GetValue(0).ToString.Trim = "", " --- ", dr.GetValue(0).ToString.Trim)
                Else
                    Me.lblIdadeMaxima.Text = " --- "
                    Me.lblIdadeMinima.Text = " --- "
                End If
                If Not dr.IsClosed Then dr.Close()
            End If

            bd.SEGS5706_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    Me.lblMultSalarial.Text = IIf(dr.GetValue(2).ToString.Trim = "", " --- ", dr.GetValue(2).ToString.Trim)
                    Me.lblCapitalSegurado.Text = IIf(dr.GetValue(1).ToString.Trim = "", " --- ", dr.GetValue(1).ToString.Trim)
                    Me.lblConjuge.Text = IIf(dr.GetValue(0).ToString.Trim = "", " --- ", dr.GetValue(0).ToString.Trim)
                Else
                    Me.lblMultSalarial.Text = " --- "
                    Me.lblCapitalSegurado.Text = " --- "
                    Me.lblConjuge.Text = " --- "
                End If

                If Me.lblCapitalSegurado.Text = "Capital Global" Then
                    Me.td_valor_capital.Visible = True
                    Me.td_valor_capital_lbl.Visible = True
                Else
                    Me.td_valor_capital.Visible = False
                    Me.td_valor_capital_lbl.Visible = False
                End If
                If Not dr.IsClosed Then dr.Close()
            End If

            bd.SEGS5744_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

            dr = bd.ExecutaSQL_DR()

            Dim valor_capital_global As String = "0"

            If Not dr Is Nothing Then
                If dr.Read Then
                    Me.lblLimiteMaximo.Text = IIf(dr.GetValue(0).ToString.Trim = "", " 0,00 ", cUtilitarios.trataMoeda(dr.GetValue(0).ToString.Trim))
                    Me.LblLimiteMinimo.Text = cUtilitarios.trataMoeda(cUtilitarios.getLimMinCapital())
                    If Me.LblLimiteMinimo.Text = 0 Or Me.LblLimiteMinimo.Text = "" Then
                        Me.LblLimiteMinimo.Text = "0,00"
                    End If
                    Me.lblValorCapital.Text = IIf(dr.GetValue(4).ToString.Trim = "", " --- ", cUtilitarios.trataMoeda(dr.GetValue(4).ToString.Trim))
                Else
                    Me.tdLimiteMaximo.Align = "center"
                    Me.lblLimiteMaximo.Text = " 0,00 "
                    Me.tdLimiteMinimo.Align = "center"
                    Me.LblLimiteMinimo.Text = " 0,00 "
                    Me.lblValorCapital.Text = " --- "
                End If

                ''Se capital fixo, entao limite min vai ser igual ao maximo [13/04/07]
                If Me.lblCapitalSegurado.Text = "Capital fixo" Then
                    Me.LblLimiteMinimo.Text = Me.lblLimiteMaximo.Text
                End If
                ''-----------------------------------------------------
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If

            If Me.lblConjuge.Text <> "C�njuge facultativo" Then
                Me.divBtnIncluiDependente.Visible = False
            Else
                Me.divBtnIncluiDependente.Visible = True
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Private Sub getCoberturas(ByVal componente As String)
        Try
            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

            bd.coberturas(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), componente)

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If Not dr Is Nothing Then
                If dr.Read Then
                    Dim tr As New Web.UI.HtmlControls.HtmlTableRow

                    tr.Cells.Add(addCell(IIf(dr.GetValue(2).ToString.Trim = "", " --- ", dr.GetValue(2).ToString.Trim)))
                    tr.Cells.Add(addCell(IIf(dr.GetValue(3).ToString.Trim = "", " --- ", cUtilitarios.trataMoeda(dr.GetValue(3).ToString.Trim))))
                    tr.Cells.Add(addCell(IIf(dr.GetValue(4).ToString.Trim = "", " --- ", cUtilitarios.trataMoeda(dr.GetValue(4).ToString.Trim))))

                    tr.Cells.Item(1).Align = "right"
                    tr.Cells.Item(2).Align = "right"

                    Me.tabelaMovimentacao.Rows.Add(tr)
                End If


                If Not dr.HasRows Then
                    Dim tr As New Web.UI.HtmlControls.HtmlTableRow

                    tr.Cells.Add(addCell(" --- "))
                    tr.Cells.Add(addCell(" --- "))
                    tr.Cells.Add(addCell(" --- "))

                    tr.Cells.Item(1).Align = "center"
                    tr.Cells.Item(2).Align = "center"

                    Me.tabelaMovimentacao.Rows.Add(tr)
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Function addCell(ByVal valor As String) As Web.UI.HtmlControls.HtmlTableCell
        Dim td As New Web.UI.HtmlControls.HtmlTableCell
        td.InnerText = valor
        Return td
    End Function

#Region "Eventos"
    Private Sub mGridDataBind(ByVal dt As DataTable)
        Me.ucPaginacao.GridDataBind(grdPesquisa, dt)
    End Sub

    Private Sub mSelecionaGrid(ByVal ItemIndex As Integer)
        Try
            Me.grdPesquisa.SelectedIndex = ItemIndex
            Me.mGridDataBind(Session("grdDescarte"))
            CType(Me.grdPesquisa.SelectedItem.Cells(0).Controls(1), RadioButton).Checked = True
            'Me.getCoberturas(Me.DropdownSelComponente.SelectedValue)
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Sub mPaginacaoClick(ByVal Argumento As String) Handles ucPaginacao.ePaginaAlterada
        Try
            Me.grdPesquisa.SelectedIndex = -1
            'Try
            '    Select Case Argumento
            '        Case "pro"
            '            If Me.grdPesquisa.CurrentPageIndex + 1 >= Me.grdPesquisa.PageCount Then
            '                Me.grdPesquisa.CurrentPageIndex = Me.grdPesquisa.PageCount - 1
            '            Else
            '                Me.grdPesquisa.CurrentPageIndex += 1
            '            End If
            '        Case "ant"
            '            If Me.grdPesquisa.CurrentPageIndex - 1 >= 0 Then
            '                Me.grdPesquisa.CurrentPageIndex = 0
            '            Else
            '                Me.grdPesquisa.CurrentPageIndex -= 1
            '            End If
            '        Case Else
            '            If Argumento <= 0 Then
            '                Argumento = 0
            '            ElseIf Argumento >= Me.grdPesquisa.PageCount Then
            '                Argumento = Me.grdPesquisa.PageCount - 1
            '            End If
            '            Me.grdPesquisa.CurrentPageIndex = Argumento
            '    End Select
            'Catch ex As Exception
            '    Me.grdPesquisa.CurrentPageIndex = 0
            'End Try

            'Session("indice") = Me.grdPesquisa.CurrentPageIndex

            Me.ucPaginacao.GridDataBind(grdPesquisa, Session("grdDescarte"))
            'Me.getCoberturas(Me.DropdownSelComponente.SelectedValue)
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Sub grdPesquisa_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdPesquisa.ItemDataBound
        'If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
        'CType(e.Item.Cells(0).FindControl("linkCodDoc"), HyperLink).NavigateUrl = "javascript:busca_registro('" & e.Item.Cells(1).Text.Trim & "','" & e.Item.Cells(2).Text.Trim & "','" & e.Item.Cells(4).Text.Trim & "','" & e.Item.Cells(5).Text.Trim & "');"
        'End If
        'If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
        'CType(e.Item.Cells(0).FindControl("linkCodDocDI"), HyperLink).NavigateUrl = "javascript:excluir_DI('" & e.Item.Cells(1).Text.Trim & "','" & e.Item.Cells(2).Text.Trim & "','" & e.Item.Cells(6).Text.Trim & "');"
        'End If
    End Sub

#End Region

    Private Sub DropdownSelComponente_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DropdownSelComponente.SelectedIndexChanged
        'Try
        '    Me.getCoberturas(Me.DropdownSelComponente.SelectedValue)
        '    Me.ucPaginacao.GridDataBind(Me.grdPesquisa, Session("grdDescarte"))
        'Catch ex As Exception
        '    Dim excp As New clsException(ex)
        'End Try

    End Sub

    Private Sub btnFiltrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFiltrar.Click

        Try
            'Me.getCoberturas(Me.DropdownSelComponente.SelectedValue)

            Dim aux As String = ""
            Dim cpf As String = ""
            Dim nome As String = ""
            Dim tipo As String = ""
            Dim ddl As String = ""

            ddl = Request.Form("DropDownList2")
            If ddl.Length > 0 Then

                If ddl = "CPF" Then
                    cpf = Request.Form("campoPesquisa").Replace(".", "").Replace("-", "")
                    aux = Request.Form("campoPesquisa").Replace(".", "").Replace("-", "")
                ElseIf ddl = "Nome" Then
                    nome = Request.Form("campoPesquisa")
                    aux = Request.Form("campoPesquisa")
                ElseIf ddl = "Inclusoes" Or ddl = "Alteracoes" Or ddl = "Exclusoes" Or ddl = "Historico" Or ddl = "Pendente" Then
                    tipo = ddl.Substring(0, 1)
                ElseIf ddl = "ExclusaoAgendada" Then
                    tipo = "B"
                ElseIf ddl = "Acertos" Then
                    tipo = "R"
                    'Edilson Silva - Demanda 14582228 - 12/09/2012
                ElseIf ddl = "ExclusaoSinistro" Then
                    tipo = "S"
                    ''''''''''''''''''''''''''''''''''''''''''''''
                End If

                Select Case ddl
                    Case "Historico"
                        ddl = "Hist�rico"
                    Case "Exclusoes"
                        ddl = "Exclus�es"
                    Case "Alteracoes"
                        ddl = "Alterac�es"
                    Case "Inclusoes"
                        ddl = "Inclus�es"
                        'Edilson Silva - Demanda 14582228 - 12/09/2012
                    Case "ExclusaoSinistro"
                        ddl = "Exclus�o / Sinistro"
                        '''''''''''''''''''''''''''''''''''''''''''''''
                End Select

                lbllistacriterio.Text = ddl & "  " & aux 'Iury

            End If

            mConsultaUsuarios(cpf, nome, tipo)
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Sub montaFaturaAtual()
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        Try
            Imagebutton2.Attributes.Add("onclick", "return popup('')")
            Imagebutton2.Attributes.Add("style", "cursor:pointer")

            dr = bd.ExecutaSQL_DR()
            Dim wf_id As String = 0

            If Not dr Is Nothing Then
                If dr.Read Then
                    wf_id = dr.GetValue(0).ToString()
                End If
                If Not dr.IsClosed Then dr.Close()
            End If
            'Mostra Resumo da Fatura Atual

            bd.ResumoFaturaAtual_Anterior(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Session("usuario"), wf_id)
            dr = bd.ExecutaSQL_DR
            If Not dr Is Nothing Then
                If dr.Read() Then
                    lblVidasFA.Text = dr.GetValue(8).ToString
                    lblCapitalFA.Text = cUtilitarios.trataMoeda(dr.GetValue(6).ToString)
                    lblPremioFA.Text = cUtilitarios.trataMoeda(dr.GetValue(7).ToString)
                Else
                    lblVidasFA.Text = "0"
                    lblCapitalFA.Text = "0,00"
                    lblPremioFA.Text = "0,00"
                End If
                If Not dr.IsClosed Then dr.Close()
            End If

            'Resumo de Inclusoes, Altera��es e Exclus�es
            bd.ResumoFaturaAtual_IAE(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Session("usuario"), wf_id)
            dr = bd.ExecutaSQL_DR

            If Not dr Is Nothing Then
                If dr.Read Then
                    If dr.Item("operacao") = "I" Then
                        lblInclusoes_vidas.Text = dr.GetValue(0).ToString
                        lblInclusoes_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        lblInclusoes_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                        'Soma
                        'totalVida = totalVida + dr.GetValue(0).ToString
                        'totalCapital = totalCapital + cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        'totalPremio = totalPremio + cUtilitarios.trataMoeda(dr.GetValue(2).ToString)

                    ElseIf dr.Item("operacao") = "E" Then
                        lblExclusoes_vidas.Text = dr.GetValue(0).ToString * -1

                        If cUtilitarios.trataMoeda(dr.GetValue(1).ToString) <> "0,00" Then
                            lblExclusoes_capital.Text = cUtilitarios.trataMoeda(cUtilitarios.trataMoeda(dr.GetValue(1).ToString))
                            lblExclusoes_premio.Text = cUtilitarios.trataMoeda(cUtilitarios.trataMoeda(dr.GetValue(2).ToString))
                        Else
                            lblExclusoes_capital.Text = cUtilitarios.trataMoeda(cUtilitarios.trataMoeda(dr.GetValue(1).ToString) * -1)
                            lblExclusoes_premio.Text = cUtilitarios.trataMoeda(cUtilitarios.trataMoeda(dr.GetValue(2).ToString) * -1)
                        End If

                    ElseIf dr.Item("operacao") = "A" Then
                        lblAlteracoes_vidas.Text = dr.GetValue(0).ToString
                        lblAlteracoes_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        lblAlteracoes_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                    ElseIf dr.Item("operacao") = "P" Then
                        lblDPS_vidas.Text = dr.GetValue(0).ToString
                        lblDPS_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        lblDPS_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                    ElseIf dr.Item("operacao") = "B" Then
                        lblExcAgen_vidas.Text = dr.GetValue(0).ToString
                        lblExcAgen_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        lblExcAgen_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                    ElseIf dr.Item("operacao") = "R" Then
                        lblAcertos_vidas.Text = dr.GetValue(0).ToString
                        lblAcertos_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        lblAcertos_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                    ElseIf dr.Item("operacao") = "T" Then
                        lblVidasFT.Text = dr.GetValue(0).ToString
                        lblCapitalFT.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        lblPremioFT.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                    End If

                End If

                'Trata valores (label) vazios
                If lblVidasFA.Text = "" Then
                    lblVidasFA.Text = "0"
                End If
                If lblCapitalFA.Text = "" Then
                    lblCapitalFA.Text = "0,00"
                End If
                If lblPremioFA.Text = "" Then
                    lblPremioFA.Text = "0,00"
                End If

                If lblInclusoes_vidas.Text = "" Then
                    lblInclusoes_vidas.Text = "0"
                End If
                If lblInclusoes_capital.Text = "" Then
                    lblInclusoes_capital.Text = "0,00"
                End If
                If lblInclusoes_premio.Text = "" Then
                    lblInclusoes_premio.Text = "0,00"
                End If

                If lblExclusoes_vidas.Text = "" Then
                    lblExclusoes_vidas.Text = "0"
                End If
                If lblExclusoes_capital.Text = "" Then
                    lblExclusoes_capital.Text = "0,00"
                End If
                If lblExclusoes_premio.Text = "" Then
                    lblExclusoes_premio.Text = "0,00"
                End If

                If lblAlteracoes_vidas.Text = "" Then
                    lblAlteracoes_vidas.Text = "0"
                End If
                If lblAlteracoes_capital.Text = "" Then
                    lblAlteracoes_capital.Text = "0,00"
                End If
                If lblAlteracoes_premio.Text = "" Then
                    lblAlteracoes_premio.Text = "0,00"
                End If

                If lblDPS_vidas.Text = "" Then
                    lblDPS_vidas.Text = "0"
                End If
                If lblDPS_capital.Text = "" Then
                    lblDPS_capital.Text = "0,00"
                End If
                If lblDPS_premio.Text = "" Then
                    lblDPS_premio.Text = "0,00"
                End If

                If lblAcertos_vidas.Text = "" Then
                    lblAcertos_vidas.Text = "0"
                End If

                If lblAcertos_capital.Text = "" Then
                    lblAcertos_capital.Text = "0,00"
                End If

                If lblAcertos_capital.Text = "" Then
                    lblAcertos_capital.Text = "0"
                End If

                If lblExcAgen_capital.Text = "" Then
                    lblExcAgen_capital.Text = "0,00"
                End If

                If lblExcAgen_premio.Text = "" Then
                    lblExcAgen_premio.Text = "0,00"
                End If

                If lblAcertos_premio.Text = "" Then
                    lblAcertos_premio.Text = "0,00"
                End If

                If lblVidasFT.Text = "" Then
                    lblVidasFT.Text = lblVidasFA.Text
                End If
                If lblCapitalFT.Text = "" Then
                    lblCapitalFT.Text = lblCapitalFA.Text
                End If
                If lblPremioFT.Text = "" Then
                    lblPremioFT.Text = lblPremioFA.Text
                End If
                '-------------------------------------------
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Try
            Session("alterado") = 0
            Session.Remove("alterado")
            Response.Write("<input type='hidden' value='alterado' id='alterado'>")
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Sub ucPaginacao_ePaginaAlterada(ByVal Argumento As String) Handles ucPaginacao.ePaginaAlterada
        Try
            paginacaogrid.iNumPaginas = grdPesquisa.PageCount
            grdPesquisa.CurrentPageIndex = Session("pagina")
            Session.Add("auxPagina", Session("pagina").ToString())    'GPTI - DEMANDA 539202
            grdPesquisa.DataBind()

            Dim cell As Web.UI.WebControls.DataGridItem = Nothing
            Dim pai As Web.UI.WebControls.DataGridItem
            Dim count As Int16 = 0
            Dim pai_id As String = ""
            Dim count2 As Integer = 0
            Dim w As Integer = 1
            Dim t As Integer = 1
            Dim primeiro As Boolean = True

            Dim tamanho(17) As Integer
            Dim textoCell As String = ""
            Dim nIndex As Integer = 0

            tamanho(0) = 10
            tamanho(1) = 11
            tamanho(2) = 40
            tamanho(3) = 12
            tamanho(4) = 8
            tamanho(5) = 11
            tamanho(6) = 5
            tamanho(7) = 10
            tamanho(8) = 15
            tamanho(9) = 10
            tamanho(10) = 10
            For nIndex = 11 To 17
                tamanho(nIndex) = 15
            Next

            Dim txtMax(17) As String
            For nIndex = 0 To 17
                txtMax(nIndex) = ""
            Next

            w = IIf(cUtilitarios.MostraColunaSalario(), 1, 0)
            t = IIf(cUtilitarios.MostraColunaCapital(), 1, 0)

            If grdPesquisa.Items.Count > 0 Then

                For Each x As Web.UI.WebControls.DataGridItem In grdPesquisa.Items

                    If Not cell Is Nothing Then
                        If x.Cells.Item(4).Text <> "Titular" And cell.Cells.Item(4).Text = "Titular" Then
                            cell.Cells.Item(3).Text = "<img src='../segw0060/Images/imgMenos.gif' onclick=exibeDependentes('" + pai_id + "',this);>&nbsp;&nbsp;" & cell.Cells.Item(3).Text
                        End If
                    End If

                    cell = x
                    If x.Cells.Item(4).Text = "Titular" Then
                        pai = x
                        x.ID = count2
                        pai_id = x.ID
                        x.Attributes.Add("onmouseover", "mO('#E8F1FF', this)")
                        x.Attributes.Add("onmouseout", "mO('#FFFFFF', this)")
                        count = 0
                        count2 += 1
                    Else
                        x.Cells.Item(3).Attributes.Add("style", "padding-left:20px;")
                        x.ID = pai_id + "_" + count.ToString
                        x.Attributes.Add("style", "background-color:#f5f5f5;")
                        x.Attributes.Add("onmouseover", "mO('#E8F1FF', this)")
                        x.Attributes.Add("onmouseout", "mO('#f5f5f5', this)")
                        count = count + 1
                    End If

                    For i As Integer = 0 To x.Cells.Count - 1
                        x.Cells.Item(i).ID = "cell_" & i
                        x.Cells.Item(i).Wrap = False
                    Next

                    Dim exclusao_agendada As Integer = 0
                    Dim acertos As Integer = 0
                    Dim ind_operacao_origem As String = ""
                    Dim ultimo_alterado As Integer = 0
                    Dim pendente_aceito As Integer = 0
                    Dim recusado As String = "n"

                    Try
                        If Not (String.IsNullOrEmpty(x.Cells.Item(12).Text.Trim)) Then
                            If (x.Cells.Item(12).Text.Trim = "" Or x.Cells.Item(12).Text.Trim = "&nbsp;") Then
                                exclusao_agendada = 0
                            Else
                                exclusao_agendada = Int32.Parse(x.Cells.Item(12).Text.Trim)
                            End If
                        End If
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try

                    Try
                        If Not (String.IsNullOrEmpty(x.Cells.Item(13).Text.Trim)) Then
                            If (x.Cells.Item(13).Text.Trim = "" Or x.Cells.Item(13).Text.Trim = "&nbsp;") Then
                                acertos = 0
                            Else
                                acertos = Int32.Parse(x.Cells.Item(13).Text.Trim)
                            End If
                        End If
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try

                    Try
                        If Not (String.IsNullOrEmpty(x.Cells.Item(14).Text.Trim)) Then
                            If (x.Cells.Item(14).Text.Trim = "" Or x.Cells.Item(14).Text.Trim = "&nbsp;") Then
                                ind_operacao_origem = 0
                            Else
                                ind_operacao_origem = x.Cells.Item(14).Text.Trim
                            End If
                        End If
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try

                    Try
                        If Not (String.IsNullOrEmpty(x.Cells.Item(15).Text.Trim)) Then
                            If (x.Cells.Item(15).Text.Trim = "" Or x.Cells.Item(15).Text.Trim = "&nbsp;") Then
                                ultimo_alterado = 0
                            Else
                                ultimo_alterado = x.Cells.Item(15).Text.Trim
                            End If
                        End If
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try

                    Try
                        If Not (String.IsNullOrEmpty(x.Cells.Item(16).Text.Trim)) Then
                            If (x.Cells.Item(16).Text.Trim = "" Or x.Cells.Item(16).Text.Trim = "&nbsp;") Then
                                pendente_aceito = 0
                            Else
                                pendente_aceito = x.Cells.Item(16).Text.Trim
                            End If
                        End If

                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try

                    Try
                        If (x.Cells.Count >= 18) Then
                            If Not (String.IsNullOrEmpty(x.Cells.Item(17).Text.Trim)) Then
                                If (x.Cells.Item(17).Text.Trim = "" Or x.Cells.Item(17).Text.Trim = "&nbsp;") Then
                                    recusado = 0
                                Else
                                    recusado = x.Cells.Item(17).Text.Trim
                                End If
                            End If
                        End If
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try

                    If ultimo_alterado = 0 Then
                        Response.Write("<input type='hidden' value='grdPesquisa_" & x.ID & "' id='ultimo_alterado' name='ultimo_alterado' >")
                    End If

                    If x.Cells.Item(1).Text = "Hist&oacute;rico" Then
                        x.Cells.Item(1).Text = "&nbsp;"

                        If (exclusao_agendada > 0) Then
                            x.Cells.Item(1).Text = "Exclus&atilde;o Agendada"
                        End If

                        If (recusado.ToLower = "s") Then
                            x.Cells.Item(1).Text = x.Cells.Item(1).Text & "/Recusado"
                        End If
                    Else
                        If (acertos > 0) Then
                            x.Cells.Item(1).Text = x.Cells.Item(1).Text & "/Acertos"
                        End If
                        'Demanda 14582228 - Edilson Silva - 22/11/2012''''''''''''''''''''''''''''''''''
                        'If (exclusao_agendada > 0) Then
                        'x.Cells.Item(1).Text = x.Cells.Item(1).Text & "/Exclus&atilde;o Agendada"
                        'End If
                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        If (exclusao_agendada > 0) And x.Cells.Item(1).Text.Trim() <> "Exclus&atilde;o Sinistro" Then
                            x.Cells.Item(1).Text = x.Cells.Item(1).Text & "/Exclus&atilde;o Agendada"
                        End If
                        If (recusado.ToLower = "s") Then
                            x.Cells.Item(1).Text = x.Cells.Item(1).Text & "/Recusado"
                        End If
                    End If

                    If x.Cells.Item(1).Text.IndexOf("Pend") <> -1 Then
                        If (ind_operacao_origem = "I") Then
                            x.Cells.Item(1).Text = x.Cells.Item(1).Text & "/Inclus�o"
                        ElseIf (ind_operacao_origem = "E") Then
                            x.Cells.Item(1).Text = x.Cells.Item(1).Text & "/Exclus�o"
                        Else
                            x.Cells.Item(1).Text = x.Cells.Item(1).Text & "/Altera��o"
                        End If
                    End If

                    For i As Integer = 0 To x.Cells.Count - 1
                        textoCell = x.Cells.Item(i).Text.ToString.Replace("&atilde;", "a").Replace("&ccedil;", "c")
                        If tamanho(i) <= textoCell.Length Then
                            tamanho(i) = textoCell.Length
                            txtMax(i) = x.Cells.Item(i).Text
                        End If

                        If i = 3 Then
                            tamanho(i) = 25
                            txtMax(i) = "".PadLeft(25, "O")
                        End If
                    Next

                    x.Cells.Item(5).Text = cUtilitarios.trataCPF(x.Cells.Item(5).Text)

                    If w = 0 Then
                        x.Cells.Item(9).Style.Add("display", "none")
                    Else
                        x.Cells.Item(9).Text = cUtilitarios.trataMoeda(x.Cells.Item(9).Text).Trim
                        x.Cells.Item(9).HorizontalAlign = HorizontalAlign.Right
                    End If

                    If t = 0 Then
                        x.Cells.Item(10).Style.Add("display", "none")
                    Else
                        x.Cells.Item(10).Text = cUtilitarios.trataMoeda(x.Cells.Item(10).Text).Trim
                        x.Cells.Item(10).HorizontalAlign = HorizontalAlign.Right
                    End If

                    x.Attributes.Add("onclick", "alteracoes('" & x.Cells.Item(0).Text & "', '" & x.Cells.Item(4).Text & "', '" & x.Cells.Item(3).Text & "','" + x.Cells.Item(0).Text + "', '" & x.ID & "', '" + x.Cells.Item(1).Text & "','" + IIf(x.Cells.Item(11).Text = 1, x.Cells.Item(11).Text, "0") + "', " & pendente_aceito & ")")

                    x.Cells.Item(2).Text = x.Cells.Item(2).Text.ToUpper

                    If (x.Cells.Item(3).Text.Length > 25) Then
                        x.Cells.Item(3).Text = "<span onmouseover=""return overlib('" & x.Cells.Item(3).Text & "');"" onmouseout='return nd();' style='width:100%'>" & x.Cells.Item(3).Text.Substring(0, 25) & "...</span>"
                    Else
                        x.Cells.Item(3).Text = "<span onmouseover=""return overlib('" & x.Cells.Item(3).Text & "');"" onmouseout='return nd();' style='width:100%'>" & x.Cells.Item(3).Text & "</span>"
                    End If

                    If primeiro Then
                        primeiro = False
                        For i As Integer = 0 To x.Cells.Count - 1
                            x.Cells.Item(i).Text = "<div class='headerFakeDiv'id='headerFake" & i & "'></div>" & x.Cells.Item(i).Text
                        Next
                    End If
                Next

                Dim z As Integer = 0
                Dim texto As String

                Dim tamMin(17) As Integer
                tamMin(0) = 9
                tamMin(1) = 9
                tamMin(2) = 10
                tamMin(3) = 12
                tamMin(4) = 5
                tamMin(5) = 13
                tamMin(6) = 10
                tamMin(7) = 4
                tamMin(8) = 15
                tamMin(9) = 12
                tamMin(10) = 12
                For nIndex = 11 To 17
                    tamMin(nIndex) = 15
                Next

                Try

                    For Each m As String In txtMax
                        texto = txtMax(z).ToString.Trim.Replace(" ", "O").Replace("-", "O").Replace("&atilde;", "a").Replace("&ccedil;", "c").PadRight(tamMin(z), "O")
                        Response.Write("<input type='hidden' value='" & texto & "' id='txtMax" & z & "'>" & vbCrLf)
                        z = z + 1
                    Next
                Catch ex As Exception
                    'Response.End()
                    Dim excp As New clsException(ex)
                End Try
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

End Class
