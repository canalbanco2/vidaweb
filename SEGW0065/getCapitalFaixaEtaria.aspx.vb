Partial Class getCapitalFaixaEtaria
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim linkseguro As Alianca.Seguranca.Web.LinkSeguro

            ''Implementação da SABL0101
            linkseguro = New Alianca.Seguranca.Web.LinkSeguro

            Dim cAmbiente As Alianca.Seguranca.Web.ControleAmbiente
            Dim url As String

            cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
            url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
            cAmbiente.ObterAmbiente(url)
            If Alianca.Seguranca.BancoDados.cCon.configurado Then
                Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            Else
                Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            End If
            Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)

            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

            Dim valorCapital As String = "0,00"
            bd.getCapitalFaixaEtaria(Session("apolice"), Session("ramo"), Session("subGrupo_id"), "'" & cUtilitarios.trataDataDB(Request.Form("txtTitAdmissao")) & "'", "'" & cUtilitarios.trataDataDB(Request.Form("txtTitNascimento")) & "'")
            Dim dr As Data.SqlClient.SqlDataReader = Nothing
            Try
                dr = bd.ExecutaSQL_DR()
                If Not dr Is Nothing Then
                    If dr.HasRows Then
                        valorCapital = cUtilitarios.trataMoeda(dr.Item("val_is"))
                    End If
                    If Not dr.IsClosed Then dr.Close()
                End If
            Catch ex As Exception
                Dim excp As New clsException(ex)
            End Try

            cUtilitarios.escreveScript("parent.document.getElementById('txtTitCapital').value='" & valorCapital & "'")
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

End Class
