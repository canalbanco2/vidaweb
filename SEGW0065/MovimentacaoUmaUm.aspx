<%@ Page Language="vb" AutoEventWireup="false" Codebehind="MovimentacaoUmaUm.aspx.vb" Inherits="segw0065.MovimentacaoUmaUm" %>
<%@ Register TagPrefix="uc1" TagName="paginacaogrid" Src="paginacaogrid.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>MovimentacaoUmaUm</title>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema" />
		<link media="all" href="../segw0060/scripts/box/box.css" type="text/css" rel="stylesheet" />
			<script src="../segw0065/scripts/box/AJS.js" type="text/javascript"></script>
			<script src="../segw0065/scripts/box/box.js" type="text/javascript"></script>
			<script type="text/javascript" src="../segw0065/scripts/overlib421/mini/overlibY.js"></script>
			<script type="text/javascript">
			var GB_IMG_DIR = "scripts/box/";
			GreyBox.preloadGreyBoxImages();
			</script>
			<link href="../segw0060/CSS/EstiloBase.css" type="text/css" rel="stylesheet" />
				<link href="CSS/Base0065.css" type="text/css" rel="stylesheet" />
					<script type="text/javascript">
				
				//Pega o tipo de acesso do usu�rio em quest&atilde;o
				var parametros = window.location.search.split("&");
				var acesso = parametros[parametros.length-2].split("=")[1];
				
				
			function popup(numero){
				var tamW = (screen.width/2)-200;
				var tamH = (screen.height/2)-200;
				//1
				window.open('../segw0060/ajudaF.aspx','',"resizable=1,scrollbars=1,width=400,height=400,top="+tamH+",left="+tamW+",toolbar=0,location=0,directories=0,status=0,menubar=0");
				return false
			}
								
			function exibeDependentes(id,src) {
					
				if (src.src.indexOf('imgMenos') != -1)
					src.src = '../segw0060/Images/imgMais.gif';
				else
					src.src = '../segw0060/Images/imgMenos.gif';
				
				for(i=0;i<=5;i++) {					
					var tr = eval("document.getElementById('grdPesquisa_"+id+"_"+i+"')");
					if (tr == null)
						break;
					if (tr.style.display == 'none') 
						tr.style.display = '';
					else
						tr.style.display = 'none';
				}		
			}	
				//id="grdPesquisa_1_0" 
				
			function exibePesquisa() {
				document.getElementById("divSelectCampoPesquisa").style.display = "block";
			}
			
			function ocultaPesquisa() { 
				//document.getElementById("divSelectCampoPesquisa").style.display = "none";
			}
				
			function escondeDiv(div){
				var idDiv = document.getElementById(div);
				if (idDiv.style.display==''){
					idDiv.style.display='none';
				}else{
					idDiv.style.display='';
				}
			}
			
			function vidaExcluir(numero){
				if (numero)
					location.href="MovimentacaoUmaUm.aspx?excluir=1&expandir=1";
				else
					location.href="MovimentacaoUmaUm.aspx&expandir=1";
			}
			
			function confirmaExclusao(){
				var divExcluir = document.getElementById("lblAcao");
				var nomeSegurado = document.getElementById("nomeAlvo").value;
				var confirmacao = confirm('Confirma a exclus&atilde;o do segurado "' + nomeSegurado + '"?\nSe existirem c�njuges para este segurado todos ser&atilde;o exclu�dos.')
				
				return confirmacao
			}
			
			function confirmaDesfazer(){
				var confirmacao = confirm('Ser&atilde;o desfeitas todas as altera&ccedil;�es realizadas para o Subgrupo "<%= Session("subgrupo")%>".\n\nConfirma a opera&ccedil;&atilde;o?');
			}
			
			function getPesquisaLoad(valor) {				
												
				if(valor == "CPF" || valor == "Nome") {
					document.getElementById("divCampoPesquisa").style.display = "block";
				} else {
					document.getElementById("divCampoPesquisa").style.display = "none";
				}				
			}
			function getPesquisa(valor) {				
				
				//document.Form1.campoPesquisa.value = "";
				document.getElementById("campoPesquisa").value = "";
				
				if(valor == "CPF" || valor == "Nome") {
					document.getElementById("divCampoPesquisa").style.display = "block";
				} else {
					document.getElementById("divCampoPesquisa").style.display = "none";
				}				
			}
			function buscaDados() {
				var valor = document.Form1.DropDownList2.value;
				
				document.Form1.submit();	
			}
			var teste = 0;
			var id;



//
var btn = new Array();
var dePara = new Array();

//Monta o array associativo.
dePara[0] = "Apagar";
dePara[1] = "Incluir";	
dePara[2] = "Excluir";
dePara[3] = "Alterar";	
dePara[4] = "Dep";

//por padr�o todos s�o desabilitados.	
btn[0] = false;		//Apagar
btn[1] = false;		//Inserir
btn[2] = false;		//Excluir
btn[3] = false;		//Alterar
btn[4] = false;		//Dep

/*/func��o que controla a exibi��o dos bot�es de acordo com o nivel de acesso//*/
function  controlaAcesso(ind_acesso) {
	
	if (acesso == 2 || acesso == 3 || ind_acesso == 5) {
		//retira tudo para que n�o � operador ou administrador
		btn[0] = false;		//Apagar
		btn[1] = false;		//Inserir
		btn[2] = false;		//Excluir
		btn[3] = false;		//Alterar
		btn[4] = false;		//Dep
		document.getElementById("txtBtnSalvar").disabled = true;
		
	} else {
		btn[1] = true;		//Inserir
		
	}		
	
	//atualiza os bot�es
	for(i=0; i<btn.length; i++) {		
		enableBtn(dePara[i], btn[i]);	
	}
}



/*/func��o que controla a exibi��o dos bot�es de acordo com as opera��es //*/
function btnControllersOperacao(operacao, tipo, ind_acesso, capitalglobal) {
	btn[0] = false;		//Apagar
	btn[1] = false;		//Inserir
	btn[2] = false;		//Excluir
	btn[3] = false;		//Alterar
	btn[4] = false;		//Dep

	document.getElementById("divBtnExcluirAlert").style.display = "none";
	document.getElementById("divBtnExcluir").style.display = "block"; 

	//retira todos para refor�ar o controle
	for(i=0; i<btn.length; i++) {
		enableBtn(dePara[i], btn[i]);	
	}
	
	operacao=operacao.toUpperCase()
	//Valdenyo Marques - 31/10/2008 - Confitec Sistemas
	//Nesta IF, foi inserida uma outra op��o de valida��o, quando o valor alocado na vari�vel "operacao" for igual a "Altera��o",
	//porque este mesmo valor alocado, n�o estava sendo interpretado o caracter cedilha e o til.
    //if(compara(operacao, "Hist") || operacao == "" || operacao == "&nbsp;") {
    //if(compara(operacao, "Hist") || operacao == "" || operacao == "&nbsp;" || operacao == "Altera&ccedil;&atilde;o" || operacao == "Altera��o") {
	//Historico pode ser excluido ou alterado
	//	btn[2] = true; 
	//	btn[3] = true;							
	//} else 
	if(compara(operacao, "EXC")) {
		//Exclusoes s� podem ser Apagadas (desfeitas)
		btn[0] = true;		
	} else if(compara(operacao, "INC")) {
		//Inser��es podem ser excluidas (agendada) ou apagadas
		btn[0] = true;
		btn[2] = true;	
	} else if(compara(operacao, "ALT")) {
		//Altera��es podem ser excluidas ou apagadas
		if (capitalglobal != 1) {
			btn[0] = true;
		}
		else{
			btn[0] = false;
		}
		btn[2] = true;
		
	}else{
	//Flow 655601 - dgomes - 10/12/2008 - CONFITEC
		btn[2] = true; 
		btn[3] = true;		
	}
	
	//incluir sempre habilitado
	btn[1] = true;
	
	//controla a inclus�o de conjuges
	controlaIncluirConjuge(operacao, tipo);
	
	//atualiza os bot�es
	for(i=0; i<btn.length; i++) {			
		enableBtn(dePara[i], btn[i]);	
	}
	
	//controla os pendentes
	controlaPendente(operacao);	
	
	//controla pelo acesso
	controlaAcesso(ind_acesso);
}

function controlaPendente(operacao) {
	if(!compara(operacao, "Pend"))
		return false;
	
	var acesso = document.getElementById('ind_acesso').value;
	
	btn[0] = true;		//Apagar	
	btn[2] = true;		//Excluir				
	
	if (acesso != 5)
	{
		document.getElementById("divBtnExcluirAlert").style.display = "block";
		document.getElementById("divBtnExcluir").style.display = "none"; 
	}
	//atualiza os bot�es
	for(i=0; i<btn.length; i++) {
		enableBtn(dePara[i], btn[i]);	
	}	
}

function controlaIncluirConjuge(operacao, tipo) {
	if(!document.getElementById("divBtnIncluiDependente")) {
		btn[4] = false;		
		return false;
	}	
	
	if(tipo != "Titular") {
		btn[4] = false;	
		return false;
	}		
}

function compara(operacao, valor) {
	var ret = false;
	
	if(operacao.indexOf(valor) != -1)
		ret = true;
			
	return ret;
}


//func��o que habilita/desabilita um bot�o espec�fico
function enableBtn(btn, enabled) {
	var botao = document.getElementById("btn" + btn);
	var div = document.getElementById("txtBtn" + btn);

	if(!div || !botao)
		return false;
	
	if(enabled) {
		div.className="txtBtnEnabled";
		botao.disabled = false;
	} else {	 
		div.className="txtBtnDisabled";
		botao.disabled = true;
	}
}
			
function alteracoes(valor, tipo, nome, idTitular, alvo, operacao, historico, pendente_aceito, capitalglobal) {								
				
	operacao = operacao.replace("<div nowrap>","");
	operacao = operacao.replace("</div>","");
	
	try {
		if(pendente_aceito == 1) {
			msg = "N�o � poss�vel movimentar uma vida que tiver pend�ncia aceita nesta vig�ncia.";
								
			document.getElementById('btnApagar').click = function () { alert(msg); };	
			document.getElementById('txtBtnApagar').click = function () { alert(msg); };																
			document.getElementById('txtBtnAlterar').click = function () { alert(msg); };
			document.getElementById('btnAlterar').click = function  () { alert(msg); }; 
			document.getElementById('txtBtnExcluir').click = function  () { alert(msg); }; 
			document.getElementById('btnExcluir').click = function  () { alert(msg); }; 						
		} else {
			document.getElementById('txtBtnApagar').click = function () { document.getElementById('btnApagar').click() };
			document.getElementById('btnApagar').click = function () { if(!document.getElementById('btnApagar').disabled) { setAcaoClickada('apagar');excluiRegistro();} };
			document.getElementById('txtBtnAlterar').click = function () { document.getElementById('btnAlterar').click(); };
			document.getElementById('btnAlterar').click = function  () { if(!document.getElementById('btnAlterar').disabled) { setAcaoClickada('altera');alteraRegistro() } };
			document.getElementById('txtBtnExcluir').click = function  () { document.getElementById('btnExcluir').click() };
			document.getElementById('btnExcluir').click = function () { if(!document.getElementById('btnExcluir').disabled) { setAcaoClickada('exclui');excluiRegistro()} }
		}
		
	} catch(ex) {
		alert(ex.message);	
	}
	
	var acesso = document.getElementById('ind_acesso').value;						
	idTemp = "grdPesquisa_" + alvo;
	
	top.document.id_selecionado_SEGW0065 = alvo;
	//setAcaoClickada(operacao);
												
	idTemp2 = document.getElementById("alvoTemp").value; 
	
	if(document.getElementById(idTemp2)) {															
		document.getElementById(idTemp2).style.background='#ffffff';
	}		
	
	document.getElementById(idTemp).style.background='#f8ef00';
		
	document.getElementById("alvo").value = valor;				
	document.getElementById("alvoTemp").value = idTemp;
	document.getElementById("nomeAlvo").value = nome;
	document.getElementById("historico").value = historico;						
						
	document.Form1.idTitular.value = idTitular;		
	
	btnControllersOperacao(operacao, tipo, acesso, capitalglobal);
}
			
			function validaMouseOut(src,color) {
				//alert(teste);
				//if (id != src.id)
				src.style.background=color;			
					
				teste = 0;
			}
			function mO(cor, alvo) {												
				if(alvo.style.background != '#f8ef00')
					alvo.style.background=cor;							
			}
			
			function alteraRegistro() {
				if (document.getElementById("divBtnIncluiDependente")) {					
					if(document.Form1.btnIncluiDep.disabled == true){
						return GB_show('Movimenta&ccedil;&atilde;o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=alterardependente&id=' + document.getElementById("alvo").value + "&idTitular=" + document.Form1.idTitular.value, 240, 450);
					} else {
						return GB_show('Movimenta&ccedil;&atilde;o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=alterar&id=' + document.getElementById("alvo").value, 280, 450);
					}
				} else {
					return GB_show('Movimenta&ccedil;&atilde;o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=alterar&id=' + document.getElementById("alvo").value, 280, 450);
				}
			}
			function incluiDependente() {
				try {										
					idTemp2 = document.getElementById("alvoTemp").value; 
					
					var dep = document.getElementById(idTemp2 + "_0");
					if (dep != null) {
						alert("Voc� j� informou um c�njuge para o titular escolhido.");
						return false;
					}
					
				} catch(ex) {
				
				}
				
				return GB_show('Movimenta&ccedil;&atilde;o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=incluirdependente&id=' + document.getElementById("alvo").value + "&idTitular=" + document.Form1.idTitular.value, 240, 450);
			}
			
function excluiRegistro() {
	historico = document.getElementById("historico").value;
	
	try {
		if (document.Form1.btnIncluiDep.disabled == true) {		
		    if (compara(document.getElementById("acaoClickada").value, "pag")) //mesmo a inclus�o passa a liberar a exclus�o para agendamentos
					return GB_show('Movimenta&ccedil;&atilde;o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluirdependente&id=' + document.getElementById("alvo").value + "&idTitular=" + document.Form1.idTitular.value, 100, 200);
				else
					return GB_show('Movimenta&ccedil;&atilde;o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluirdependente&id=' + document.getElementById("alvo").value + "&idTitular=" + document.Form1.idTitular.value, 240, 450);
		} else {
		    if (historico != 1 && false) //mesmo a inclus�o passa a liberar a exclus�o para agendamentos
				return GB_show('Movimenta&ccedil;&atilde;o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluir&id=' + document.getElementById("alvo").value, 100, 200);				
			else
				return GB_show('Movimenta&ccedil;&atilde;o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluir&id=' + document.getElementById("alvo").value, 280, 450);
		}
		
	} catch(ex) {
		if (compara(document.getElementById("acaoClickada").value, "pag"))	  //mesmo a inclus�o passa a liberar a exclus�o para agendamentos
			return GB_show('Movimenta&ccedil;&atilde;o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluir&acaoMsg=' + document.getElementById("acaoClickada").value + '&id=' + document.getElementById("alvo").value, 100, 200);								
		else
			return GB_show('Movimenta&ccedil;&atilde;o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluir&acaoMsg=' + document.getElementById("acaoClickada").value + '&id=' + document.getElementById("alvo").value, 280, 450);
	}
}
			
function FormataCPF(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres) {
		var wTecla, wVr, wTam;
		 
			//alert(pForm);
		  
		wTecla = pTeclaPres.keyCode;
		wVr = pForm[pCampo].value;
		wVr = wVr.toString().replace( "-", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( "/", "" );
		wTam = wVr.length ;

		if(wTam > pTamMax) {
			wTam = pTamMax;
			wVr = wVr.substr(0, pTamMax)
		}
			
		if (wTam < pTamMax && wTecla != 8) { 
			wTam = wVr.length + 1 ; 
		}

		if (wTecla == 8 ) { 
			wTam = wTam - 1 ; 
		}
		   
		if ( wTecla == 8 || wTecla == 88 || wTecla >= 48 && wTecla <= 57 || wTecla >= 96 && wTecla <= 105 ){
		if ( wTam <= 2 ){
			pForm[pCampo].value = wVr ;
		}
		if (wTam > pPosTraco && wTam <= pTamMax) {
				wVr = wVr.substr(0, wTam - pPosTraco) + '-' + wVr.substr(wTam - pPosTraco, wTam);
		}
		if ( wTam == pTamMax){
				wVr = wVr.substr( 0, wTam - pPos1 ) + '.' + wVr.substr(wTam - pPos1, 3) + '.' + wVr.substr(wTam - pPos2, wTam);
		}
		pForm[pCampo].value = wVr;
		 
		}

		}
		function formataCampo(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres) {
					
		    if(pForm.DropDownList2.value == "CPF") { 
				FormataCPF(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres);
			}		
		}
		
		function GB_showConfirm() {
			alert("Segurado inclu�do com sucesso."); 
			GB_hide();			
			GB_show('Movimenta&ccedil;&atilde;o on-line', '../../../segw0065/confirm.aspx', 100, 200);					
		}			
		
		function GB_showConfirmP() {
			alert("Inclus�o aguardando aceite da Seguradora."); 	
			GB_hide();			
			GB_show('Movimenta&ccedil;&atilde;o on-line', '../../../segw0065/confirm.aspx', 100, 200);					
		}	
		
		
		function GB_showConfirmPost() {
			GB_hide();			
			GB_show('Movimenta&ccedil;&atilde;o on-line', '../../../segw0065/confirmPost.aspx?reloaded=1', 100, 200);					
		}				
		
		function GB_showConfirmPostSemAction() {			
			top.exibeaguarde();
			GB_reloadOnClose('true');
			GB_hide();			
			//GB_show('Movimenta&ccedil;&atilde;o on-line', '../../../segw0065/confirmPost.aspx?reloaded=1', 100, 200);		
		}				
		
		function GB_showConfirmSim() {
			GB_hide();
			GB_show('Movimenta&ccedil;&atilde;o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=incluir', 280, 450);
		}
		function GB_showConfirmNao() {						
			GB_reloadOnClose('true');
			GB_hide();
			parent.parent.top.exibeaguarde();
		}				
	
		function excluiVida() 
		{	
			try 
			{		
					return GB_show('Movimenta&ccedil;&atilde;o on-line', '../../../segw0065/confirmPost.aspx?reloaded=1&Apagar=' + document.Form1.idTitular.value, 100, 200);
			} 
			catch(ex) {}
		}
		
		function setAcaoClickada(acao) {
			document.Form1.acaoClickada.value = acao;
		}
                    </script>
	</head>
	<body MS_POSITIONING="FlowLayout" onload="setWidths();">
		<div id="overDiv" style="Z-INDEX:1000; VISIBILITY:hidden; POSITION:absolute"></div>
		<form id="Form1" method="post" runat="server">
			<input type="hidden" value="" name='acaoClickada' id="acaoClickada" />
			<br />
			<asp:panel id="Panel1" runat="server" HorizontalAlign="Left" BorderWidth="0">
				<table cellspacing="0" cellpadding="0" width="100%" border="0">
					<tr>
						<td id="td1" align="left" colSpan="4" height="24"><!-- Painel de Cadastro geral -->
							<table id="Table4" cellspacing="0" cellpadding="0" border="0">
								<tr>
									<td>
										<asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela">Label</asp:label><BR>
										<asp:Label id="lblVigencia" runat="server" CssClass="Caminhotela"></asp:Label></td>
									<td align="right"></td>
								</tr>
							</table>
							<BR>
							<asp:Label id="Label17" runat="server" CssClass="Caminhotela">Condi&ccedil;�es do subgrupo</asp:Label>
							<table id="Table6" style="BORDER-COLLAPSE: collapse" bordercolor="#cccccc" cellspacing="0"
							
								cellpadding="1" width="100%" border="1"> <!-- PRIMEIRA LINHA -->
								<tr class="titulo-tabela sem-sublinhado">
									<td>Capital Segurado</td>
									<td id="td_valor_capital" runat="server">Valor Capital</td>
									<td>Limite&nbsp;M�nimo</td>
									<td>Limite&nbsp;M�ximo</td>
									<td>M�lt. Salarial</td>
									<td>Idade M�n. Mov.</td>
									<td>Idade M�x. Mov</td>
									<td>C�njuge</td>
								</tr>
								<tr>
									<td align="center">
										<asp:Label id="lblCapitalSegurado" Runat="server"></asp:Label></td>
									<td id="td_valor_capital_lbl" align="center" runat="server">
										<asp:Label id="lblValorCapital" Runat="server"></asp:Label></td>
									<td id="tdLimiteMinimo" align="right" runat="server">
										<asp:Label id="LblLimiteMinimo" Runat="server"></asp:Label></td>
									<td id="tdLimiteMaximo" align="right" runat="server">
										<asp:Label id="lblLimiteMaximo" Runat="server"></asp:Label></td>
									<td align="center">
										<asp:Label id="lblMultSalarial" Runat="server"></asp:Label></td>
									<td align="center">
										<asp:Label id="lblIdadeMinima" Runat="server"></asp:Label></td>
									<td align="center">
										<asp:Label id="lblIdadeMaxima" Runat="server"></asp:Label></td>
									<td align="center">
										<asp:Label id="lblConjuge" Runat="server"></asp:Label></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colSpan="4" height="5"></td>
					</tr>
					<tr>
						<td colSpan="4">
							<DIV style="DISPLAY: none">
								<asp:panel id="Panel2" runat="server" HorizontalAlign="Left">
									<table id="tabelaMovimentacao2" style="MARGIN-TOP: 10px; MARGIN-BOTTOM: 10px; MARGIN-LEFT: 0px; BORDER-COLLAPSE: collapse"
										borderColor="#cccccc" cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td vAlign="bottom">
												<asp:Label id="Label1" runat="server" CssClass="Caminhotela">Coberturas</asp:Label></td>
										</tr>
										<tr>
											<td vAlign="middle" height="20">Selecione o Componente:
												<asp:DropDownList id="DropdownSelComponente" runat="server" AutoPostBack="True" Width="113px">
													<asp:ListItem Value="1">001 - Titular</asp:ListItem>
													<asp:ListItem Value="2">002 - Filhos</asp:ListItem>
													<asp:ListItem Value="3">003 - C�njuge</asp:ListItem>
												</asp:DropDownList></td>
										</tr>
										<tr>
											<td>
												<DIV style="OVERFLOW: auto; WIDTH: 550px" DESIGNTIMEDRAGDROP="740">
													<DIV style="OVERFLOW: hidden" DESIGNTIMEDRAGDROP="900">
														<table id="tabelaMovimentacao" style="MARGIN-TOP: 0px; MARGIN-BOTTOM: 0px; MARGIN-LEFT: 0px; BORDER-COLLAPSE: collapse"
															borderColor="#cccccc" cellspacing="0" cellpadding="0" width="540" border="1" runat="server">
															<tr class="titulo-tabela sem-sublinhado">
																<td>Nome Cobertura
																</td>
																<td width="90">Limite M�nimo</td>
																<td width="120">Limite M�ximo</td>
															</tr>
														</table>
													</div>
												</div>
											</td>
										</tr>
									</table>
								</asp:panel></div>
							<BR>
							&nbsp;
							<DIV id="divResumoFatura" style="DISPLAY: none">
								<asp:panel id="pnlResumo" Runat="server" Height="204px">
									<table style="MARGIN-TOP: 5px" cellspacing="0" cellpadding="0" width="100%" border="0">
										<tr>
											<td colSpan="2"></td>
										</tr>
										<tr>
											<td align="center" width="0" CssClass="Caminhotela"></td>
											<td>
												<table style="MARGIN-TOP: 5px" cellspacing="0" cellpadding="0" width="420" align="center"
													border="0">
													<tr height="20">
														<td width="7" background="../segw0060/Images/imgTopoMenuMeio.gif" height="20"><IMG src="../segw0060/Images/imgTopoMenuCanto.gif"></td>
														<td class="font_verdana_10_preto_bold" vAlign="middle" align="left" width="80%" background="../segw0060/Images/imgTopoMenuMeio.gif"
															height="20"><STRONG>:: Resumo da fatura atual ::</STRONG></td>
														<td class="font_verdana_10_preto_bold" vAlign="middle" align="right" width="15%" background="../segw0060/Images/imgTopoMenuMeio.gif"
															height="20">
															<asp:imagebutton id="Imagebutton2" onmouseover="return overlib('Clique aqui para obter ajuda deste menu.');"
																onmouseout="return nd();" runat="server" ImageUrl="../segw0060/Images/help_azul.gif" ImageAlign="right"></asp:imagebutton></td>
														<td align="right" width="20" background="../segw0060/Images/imgTopoMenuMeio.gif" height="20"><IMG src="../segw0060/Images/imgTopoMenuCurva.gif"></td>
													</tr>
												</table>
												<DIV class="divPopup" id="relatorio">
													<table class="CamposResumo" id="Table5" style="BORDER-RIGHT: #d4c601 1px solid; BORDER-BOTTOM: #d4c601 1px solid"
														cellspacing="2" cellpadding="0" width="420" align="center" bgColor="#e4e4e4" border="0"> <!--240-->
														<tr>
															<td width="150">&nbsp;</td>
															<td align="center" width="15%">Vidas</td>
															<td noWrap align="center" width="25%">Capital
																<asp:Label id="Label4" runat="server" Font-Size="Larger">(R$)</asp:Label></td>
															<td noWrap align="center" width="25%">Pr�mio
																<asp:Label id="Label5" runat="server" Font-Size="Larger">(R$)</asp:Label></td>
														</tr>
														<tr>
															<td class="quadro-resumo-borda" noWrap align="left" width="110">Fatura Anterior<SUP>1</SUP></td>
															<td class="quadro-resumo-borda" align="center">
																<DIV id="divVidasFA">
																	<asp:Label id="lblVidasFA" runat="server"></asp:Label></div>
															</td>
															<td class="quadro-resumo-borda" align="right">
																<DIV id="divCapitalFA">
																	<asp:Label id="lblCapitalFA" runat="server"></asp:Label></div>
															</td>
															<td class="quadro-resumo-borda" align="right">
																<DIV id="divPremioFA">
																	<asp:Label id="lblPremioFA" runat="server"></asp:Label></div>
															</td>
														</tr>
														<tr>
															<td align="left" width="150">
																<asp:HyperLink id="btnInclusoes" runat="server" cssClass="hand2" Target="IFrameConteudoPrincipal">Inclus�es</asp:HyperLink></td>
															<td align="center">
																<DIV id="divInclusoes_vidas">
																	<asp:Label id="lblInclusoes_vidas" runat="server"></asp:Label></div>
															</td>
															<td align="right">
																<DIV id="divInclusoes_capital">
																	<asp:Label id="lblInclusoes_capital" runat="server"></asp:Label></div>
															</td>
															<td align="right">
																<DIV id="divInclusoes_premio">
																	<asp:Label id="lblInclusoes_premio" runat="server"></asp:Label></div>
															</td>
														</tr>
														<tr>
															<td align="left" width="150">
																<asp:HyperLink id="btnExclusoes" runat="server" cssClass="hand2" Target="IFrameConteudoPrincipal">Exclus�es</asp:HyperLink></td>
															<td align="center" bgColor="#ffffff">
																<DIV id="divExclusoes_vidas">
																	<asp:Label id="lblExclusoes_vidas" runat="server"></asp:Label></div>
															</td>
															<td align="right" bgColor="#ffffff">
																<DIV id="divExclusoes_capital">
																	<asp:Label id="lblExclusoes_capital" runat="server"></asp:Label></div>
															</td>
															<td noWrap align="right" bgColor="#ffffff">
																<DIV id="divExclusoes_premio">
																	<asp:Label id="lblExclusoes_premio" runat="server"></asp:Label></div>
															</td>
														</tr>
														<tr>
															<td noWrap align="left" width="150">
																<asp:HyperLink id="btnAlteracoes" runat="server" cssClass="hand2" Target="IFrameConteudoPrincipal">Altera&ccedil;�es de Capital</asp:HyperLink></td>
															<td noWrap align="center">
																<DIV id="divAlteracoes_vidas">
																	<asp:Label id="lblAlteracoes_vidas" runat="server"></asp:Label></div>
															</td>
															<td noWrap align="right">
																<DIV id="divAlteracoes_capital">
																	<asp:Label id="lblAlteracoes_capital" runat="server"></asp:Label></div>
															</td>
															<td noWrap align="right">
																<DIV id="divAlteracoes_premio">
																	<asp:Label id="lblAlteracoes_premio" runat="server"></asp:Label></div>
															</td>
														</tr>
														<tr>
															<td noWrap align="left" width="150">
																<asp:HyperLink id="btnAcertos" runat="server" cssClass="hand2" Target="IFrameConteudoPrincipal">Acertos</asp:HyperLink></td>
															<td noWrap align="center" bgColor="#ffffff">
																<DIV id="divAcertos_vidas">
																	<asp:Label id="lblAcertos_vidas" runat="server"></asp:Label></div>
															</td>
															<td noWrap align="right" bgColor="#ffffff">
																<DIV id="divAcertos_capital">
																	<asp:Label id="lblAcertos_capital" runat="server"></asp:Label></div>
															</td>
															<td noWrap align="right" bgColor="#ffffff">
																<DIV id="divAcertos_premio">
																	<asp:Label id="lblAcertos_premio" runat="server"></asp:Label></div>
															</td>
														</tr>
														<tr>
															<td noWrap align="left" width="90">
																<asp:HyperLink id="btnDpsPendente" runat="server" cssClass="hand2" Target="IFrameConteudoPrincipal">Pendente</asp:HyperLink></td>
															<td noWrap align="center">
																<DIV id="divDPS_vidas">
																	<asp:Label id="lblDPS_vidas" runat="server"></asp:Label></div>
															</td>
															<td noWrap align="right">
																<DIV id="divDPS_capital">
																	<asp:Label id="lblDPS_capital" runat="server"></asp:Label></div>
															</td>
															<td noWrap align="right">
																<DIV id="divDPS_premio">
																	<asp:Label id="lblDPS_premio" runat="server"></asp:Label></div>
															</td>
														</tr>
														<tr>
															<td noWrap align="left" width="90">
																<asp:HyperLink id="btnExcAgen" runat="server" cssClass="hand2" Target="IFrameConteudoPrincipal">Pendente</asp:HyperLink></td>
															<td noWrap align="center">
																<DIV id="divExcAgen_vidas">
																	<asp:Label id="lblExcAgen_vidas" runat="server"></asp:Label></div>
															</td>
															<td noWrap align="right">
																<DIV id="divExcAgen_capital">
																	<asp:Label id="lblExcAgen_capital" runat="server"></asp:Label></div>
															</td>
															<td noWrap align="right">
																<DIV id="divExcAgen_premio">
																	<asp:Label id="lblExcAgen_premio" runat="server"></asp:Label></div>
															</td>
														</tr>
														<tr>
															<td class="quadro-resumo-borda" noWrap align="left" width="90">Fatura Atual<SUP>2</SUP></td>
															<td class="quadro-resumo-borda" noWrap align="center">
																<DIV id="divVidasFT">
																	<asp:Label id="lblVidasFT" runat="server"></asp:Label></div>
															</td>
															<td class="quadro-resumo-borda" noWrap align="right">
																<DIV id="divCapitalFT">
																	<asp:Label id="lblCapitalFT" runat="server"></asp:Label></div>
															</td>
															<td class="quadro-resumo-borda" noWrap align="right">
																<DIV id="divPremioFT">
																	<asp:Label id="lblPremioFT" runat="server"></asp:Label></div>
															</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
								</asp:panel></div>
						</td>
					</tr>
					<tr>
						<td colSpan="4">
							<asp:Panel id="pnlAcoes" runat="server" HorizontalAlign="Center">
								<table cellspacing="1" cellpadding="5" align="center">
									<tr>
										<td>
											<DIV id="divBtnApagar" onmouseover="ocultaPesquisa(); return overlib('Desfaz todas as altera��es deste registro nesta vida.');"
												style="DISPLAY: block; POSITION: relative" onmouseout="exibePesquisa(); return nd();">
												<DIV class="txtBtnDisabled" id="txtBtnApagar" onclick="document.getElementById('btnApagar').click()">Desfazer</div>
												<input class="Botao" id="btnApagar" style="WIDTH: 71px" disabled onclick="setAcaoClickada('apagar');excluiRegistro();"
													type="button" name="btnApagar">
											</div>
										</td>
										<td>
											<DIV id="divBtnIncluir" onmouseover="ocultaPesquisa();return overlib('Clique para inserir um novo segurado');"
												style="BORDER-RIGHT: black 0px solid; BORDER-TOP: black 0px solid; DISPLAY: block; BORDER-LEFT: black 0px solid; BORDER-BOTTOM: black 0px solid"
												onmouseout="exibePesquisa();return nd();">
												<DIV class="txtBtnDisabled" id="txtBtnIncluir" style="DISPLAY: none" onclick="document.getElementById('btnIncluir').click()">Incluir</div>
												<input class="Botao" id="btnIncluir" onclick="setAcaoClickada('insere');return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=incluir', 280, 450)"
													type="button" value="Incluir" name="btnIncluir">
											</div>
										</td>
										<td>
											<DIV id="divBtnIncluiDependente" onmouseover="ocultaPesquisa(); return overlib('Para incluir um c�njuge clique no titular na lista abaixo');"
												style="DISPLAY: block; FLOAT: left; POSITION: relative" onmouseout="exibePesquisa(); return nd();"
												runat="server"><input class="Botao" disabled onclick="setAcaoClickada('inseredep');incluiDependente()"
													type="button" value="Incluir C�njuge" name="btnIncluiDep"> &nbsp;&nbsp;
											</div>
											<DIV id="divBtnAlterar" onmouseover="ocultaPesquisa();return overlib('Para alterar um segurado clique na lista abaixo');"
												style="DISPLAY: block; Z-INDEX: 10; FLOAT: left; POSITION: relative" onmouseout="exibePesquisa();return nd();">
												<DIV class="txtBtnDisabled" id="txtBtnAlterar" onclick="document.getElementById('btnAlterar').click()">Alterar</div>
												<input class="Botao" style="WIDTH: 63px" disabled onclick="setAcaoClickada('altera');alteraRegistro()"
													type="button" name="btnAlterar">
											</div>
										</td>
										<td>
											<DIV id="divBtnExcluir" onmouseover="ocultaPesquisa(); return overlib('Para excluir um segurado clique na lista abaixo');"
												style="DISPLAY: block; POSITION: relative" onmouseout="exibePesquisa(); return nd();">
												<DIV class="txtBtnDisabled" id="txtBtnExcluir" onclick="document.getElementById('btnExcluir').click()">Excluir</div>
												<input class="Botao" id="btnExcluir" style="WIDTH: 60px" disabled onclick="setAcaoClickada('exclui');excluiRegistro()"
													type="button" name="btnExcluir">
											</div>
											<DIV id="divBtnExcluirAlert" onmouseover="ocultaPesquisa(); return overlib('Para excluir um segurado clique na lista abaixo');"
												style="DISPLAY: none; POSITION: relative" onmouseout="exibePesquisa(); return nd();">
												<DIV class="txtBtnEnabled" id="txtBtnExcluirAlert" onclick="document.getElementById('btnExcluirAlert').click()">Excluir</div>
												<input class="Botao" id="btnExcluirAlert" style="WIDTH: 60px" onclick="alert('N�o ser� poss�vel realizar esta exclus�o. A seguradora est� aguardando os necess�rios documentos para an�lise, somente ap�s o recebimento e an�lise desses documentos ser� poss�vel realizar a opera��o');return false;"
													type="button" name="btnExcluirAlert">
											</div>
										</td>
										<td>
											<DIV id="divBtnGravar" onmouseover="ocultaPesquisa(); return overlib('Clique para salvar a(s) inclus�o(�es), altera��o(�es) e exclus�o (�es) realizada(s)');"
												style="DISPLAY: block; FLOAT: left; POSITION: relative" onmouseout="exibePesquisa();return nd();">
												<DIV class="txtBtnEnabled" id="txtBtnSalvar" onclick="alert('Opera��es realizadas foram salvas com sucesso.');">Salvar</div>
												<input class="Botao" style="WIDTH: 60px" onclick="setAcaoClickada('grava');alert('Opera��es realizadas foram salvas com sucesso.');"
													type="button" name="btnGravar">
											</div>
										</td>
										<td>
    										<DIV id="div1" onmouseover="ocultaPesquisa(); return overlib('Clique para gerar uma rela��o em Excel das vidas');"
												style="DISPLAY: block; FLOAT: left; POSITION: relative" onmouseout="exibePesquisa();return nd();">
												<input class="Botao" onclick="document.Form1.gerarXml.value=1;document.Form1.submit();" type="button" value="Gerar Excel" name="btnXml">
											</div>
										</td>
									</tr>
								</table>
								<asp:Button id="Button1" runat="server" CssClass="Botao" Width="60px" Visible="False" Text="Incluir"></asp:Button>
								<asp:Button id="btnIncluiDependente" runat="server" CssClass="Botao" Width="125px" Visible="False"
									Text="Incluir c�njuge"></asp:Button>
								<asp:Button id="btnAlterar" runat="server" CssClass="Botao" Width="60px" Visible="False" Text="Alterar"></asp:Button>
								<asp:Button id="btnExcluir2" runat="server" CssClass="Botao" Width="60px" Visible="False" Text="Excluir"></asp:Button>
								<asp:Button id="btnDesfazAlteracoes" runat="server" CssClass="Botao" Width="258px" Visible="False"
									Text="Desfazer todas altera��es deste subgrupo"></asp:Button>
								<asp:Button id="btnFechaPagina" runat="server" CssClass="Botao" Width="56px" Visible="False"
									Text="Fechar"></asp:Button>
							</asp:Panel><BR>
						</td>
					</tr>
					<tr>
						<td vAlign="bottom" width="250">
							<asp:Label id="lblListagem" runat="server" CssClass="Caminhotela">Lista das vidas</asp:Label></td>
						<td vAlign="bottom" align="center" width="10">&nbsp;
						</td>
						<td vAlign="bottom" noWrap align="left" colSpan="1" height="20">
							<DIV style="RIGHT: 0px; POSITION: relative" align="right">
								<table style="PADDING-BOTTOM: 3px" height="20" cellspacing="0" cellpadding="0" width="100%"
									border="0">
									<tr>
										<td vAlign="bottom" align="right" width="470" >
											<asp:Label id="lbltiposele��o" CssClass="Caminhotela" Runat="server">Filtrado por: </asp:Label>
											<asp:label id="lbllistacriterio" runat="server" CssClass="Caminhotela"></asp:label></td>
										<td vAlign="bottom" width="120" height="30">
											<DIV id="divSelectCampoPesquisa">
												<asp:DropDownList id="DropDownList2" runat="server" Width="113px" name="DropDownList2">
													<asp:ListItem Value="Todos" Selected="True">Todos</asp:ListItem>
													<asp:ListItem Value="CPF">CPF</asp:ListItem>
													<asp:ListItem Value="Nome">Nome</asp:ListItem>
													<asp:ListItem Value="Inclusoes">Inclus�es</asp:ListItem>
													<asp:ListItem Value="Alteracoes">Altera&ccedil;�es</asp:ListItem>
													<asp:ListItem Value="Exclusoes">Exclus�es</asp:ListItem>
													<asp:ListItem Value="Pendente">Pendente</asp:ListItem>
													<asp:ListItem Value="Historico">Hist�rico</asp:ListItem>
													<asp:ListItem Value="Acertos">Acertos</asp:ListItem>
													<asp:ListItem Value="ExclusaoAgendada">Exclus�es Agendadas</asp:ListItem>
												    <asp:ListItem Value="ExclusaoSinistro">Exclus�o / Sinistro</asp:ListItem>
												</asp:DropDownList></div>
										</td>
										<td vAlign="bottom" align="left">
											<DIV id="divCampoPesquisa" style="DISPLAY: none"><input id="campoPesquisa" value="<%=Request("campoPesquisa")%>" onkeyup="formataCampo(document.getElementById('Form1'),'campoPesquisa',11,8,5,2,event)"
													type="text" maxLength="14" name="campoPesquisa"></div>
										</td>
										<td vAlign="bottom" align="right">
											<asp:Button id="btnFiltrar" runat="server" CssClass="Botao" Text="Pesquisar"></asp:Button>
											<asp:Label id="lblFiltro" runat="server" Visible="False" ForeColor="Red"></asp:Label></td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td colSpan="4">
							<STYLE>.headerFixDiv {
	FONT-WEIGHT: bold; WIDTH: 100%; COLOR: #ffffff; POSITION: absolute; TOP: 2px; BACKGROUND-COLOR: #003399
}
.headerFake {
	BORDER-RIGHT: red 1px solid; BORDER-TOP: red 1px solid; BORDER-LEFT: red 1px solid; BORDER-BOTTOM: red 1px solid; POSITION: absolute; TOP: 2px
}
.headerFixDiv2 {
	VISIBILITY: hidden
}
.headerFakeDiv {
	VISIBILITY: hidden; LINE-HEIGHT: 0
}
.headerGrid {BACKGROUND-COLOR: #003399; FONT-WEIGHT: bold;COLOR: #ffffff;}
</STYLE>
							<!-- DIV id="divGridMovimentacao" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; OVERFLOW: auto; WIDTH: 830px; PADDING-TOP: 0px; HEIGHT: 298px" -->
							<DIV id="divGridMovimentacao" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; OVERFLOW: auto; PADDING-TOP: 0px;>
								<DIV style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; OVERFLOW: hidden; PADDING-TOP: 0px">
									<table id="gridMovimentacao" style="BORDER-COLLAPSE: collapse" borderColor="#dddddd" cellspacing="0"
										cellpadding="0" border="1" runat="server">
										<tr class="paddingzero-sembordalateral">
											<td>
												<DIV style="WIDTH: 100%">
													<table style="BORDER-COLLAPSE: collapse" cellspacing="0" cellpadding="0" border="1">
														<!-- TR>
															<td id="">
																<DIV class="headerFixDiv2" id="header1"></div>
																<DIV class="headerFixDiv">Opera��o</div>
															</td>
															<td id="tdHeader2" wrap="false">
																<DIV class="headerFixDiv2" id="header2"></div>
																<DIV class="headerFixDiv">Dt. Opera��o</div>
															</td>
															<td id="tdHeader3" wrap="false">
																<DIV class="headerFixDiv2" id="header3"></div>
																<DIV class="headerFixDiv">Segurado</div>
															</td>
															<td id="tdHeader4" wrap="false">
																<DIV class="headerFixDiv2" id="header4"></div>
																<DIV class="headerFixDiv">Tipo</div>
															</td>
															<td id="tdHeader5" wrap="false">
																<DIV class="headerFixDiv2" id="header5"></div>
																<DIV class="headerFixDiv">CPF</div>
															</td>
															<td id="tdHeader6" wrap="false">
																<DIV class="headerFixDiv2" id="header6"></div>
																<DIV class="headerFixDiv">Dt. Nasc.</div>
															</td>
															<td id="tdHeader7" wrap="false">
																<DIV class="headerFixDiv2" id="header7"></div>
																<DIV class="headerFixDiv">Sexo</div>
															</td>
															<td id="tdHeader8" wrap="false">
																<DIV class="headerFixDiv2" id="header8"></div>
																<DIV class="headerFixDiv">Dt. In�cio de Vig�ncia</div>
															</td>
															<td id="tdHeader9" wrap="false">
																<DIV class="headerFixDiv2" id="header9"></div>
																<DIV class="headerFixDiv">Sal�rio (R$)</div>
															</td>
															<td id="0" wrap="false">
																<DIV class="headerFixDiv2" id="header10"></div>
																<DIV class="headerFixDiv">Capital (R$)</div>
															</TD --> <!--TD id="tdHeader11" wrap="false">
																<DIV class="headerFixDiv2" id="header11"></div>
																<DIV class="headerFixDiv">Fim de Vig�ncia</div>
															</TD--><!-- /TR -->
														<tr>
															<td colSpan="11">
															<DIV id="lista" style="WIDTH:860px">
																<DIV id="divScrollGrid">
																	<asp:datagrid id="grdPesquisa" runat="server" cellpadding="0"
																		cellspacing="0" AutoGenerateColumns="False" ShowHeader="True" PageSize="50" AllowPaging="True"
																		BorderColor="#DDDDDD" HeaderStyle-CssClass="th">
																		<SelectedItemStyle CssClass="ponteiro"></SelectedItemStyle>
																		<AlternatingItemStyle CssClass="ponteiro"></AlternatingItemStyle>
																		<ItemStyle CssClass="ponteiro" ></ItemStyle>
																		<FooterStyle CssClass="0019GridFooter"></FooterStyle>
																		<Columns>
																			<asp:BoundColumn Visible="False" DataField="segurado_temporario_web_id"></asp:BoundColumn>
																			<asp:BoundColumn DataField="operacao" HeaderText="Opera&#231;&#227;o">
																				<ItemStyle HorizontalAlign="Center" Width="60px"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="dt_alteracao"  HeaderText="Dt. Opera&#231;&#227;o" DataFormatString="{0:dd/MM/yyyy}">
																				<ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="nome" HeaderText="Segurado">
																				<ItemStyle Width="225px"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="tipo" HeaderText="Tipo">
																				<ItemStyle Width="50px"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="cpf" HeaderText="CPF">
																				<ItemStyle Width="110px"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="dt_nascimento" HeaderText="Dt. Nasc." DataFormatString="{0:dd/MM/yyyy}">
																				<ItemStyle HorizontalAlign="Center" Width="80"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="sexo" HeaderText="Sexo">
																				<ItemStyle Width="40px"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="dt_inicio_vigencia"   HeaderText="Dt. Ini. Vig&#234;ncia" DataFormatString="{0:dd/MM/yyyy}">
																				<ItemStyle HorizontalAlign="Center" Width="95px" ></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="val_salario" HeaderText="Sal&#225;rio (R$)">
																				<ItemStyle HorizontalAlign="Right" Width="75px"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="val_capital_segurado" HeaderText="Capital (R$)">
																				<ItemStyle HorizontalAlign="Right" Width="70px"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn Visible="False" DataField="Historico"></asp:BoundColumn>
																			<asp:BoundColumn Visible="False" DataField="exclusao_agendada"></asp:BoundColumn>
																			<asp:BoundColumn Visible="False" DataField="acertos"></asp:BoundColumn>
																			<asp:BoundColumn Visible="False" DataField="ind_operacao_origem"></asp:BoundColumn>
																			<asp:BoundColumn Visible="False" DataField="ultimo_alterado"></asp:BoundColumn>
																			<asp:BoundColumn Visible="False" DataField="pendente_aceito"></asp:BoundColumn>
																		</Columns>
																		<PagerStyle Visible="False"></PagerStyle>
																	</asp:datagrid>

																</div>
																</div>
															</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
								</div>
								<center>
									<asp:Label id="lblSemUsuario" CssClass="Caminhotela" Runat="server" Visible="False"></asp:Label></CENTER>
							</div>
							<!-- DIV></DIV -->
							<uc1:paginacaogrid id="ucPaginacao" runat="server"></uc1:paginacaogrid></td>
					</tr>
				</table>
				<input id="alvo" type="hidden" name="alvo"> <input id="alvoTemp" type="hidden" value="0" name="alvo">
				<input id="nomeAlvo" type="hidden" name="nomeAlvo"> <input id="idTitular" type="hidden" name="idTitular" runat="server">
				<input id="historico" type="hidden" name="historico">
				<input type="hidden" name="gerarXml"> 
			
			</asp:panel><!-- Fim do painel de cadastro -->
		</form>
		<form name="contentForm" action="" method="post">
			<div id='content' style='DISPLAY:none'>
			</div>
		</form>
		<script>
				
				try { getPesquisaLoad(document.Form1.DropDownList2.value);} catch (ex){ document.write("<span style='color:white;'>" + ex.message + " - 0 - " + "</span>");}																	
					
				//try {				
					if (acesso == 2 || acesso == 3)
					{
						document.Form1.btnIncluir.disabled = true;
						document.Form1.btnGravar.disabled = true;
					}								
					
					if(document.getElementById('ind_acesso')) {
						if (document.getElementById('ind_acesso').value == 2) {
							document.Form1.btnIncluir.disabled = true;
							document.Form1.btnGravar.disabled = true;
						} else if(document.getElementById('ind_acesso').value == 3) {
							document.Form1.btnIncluir.disabled = true;
							document.Form1.btnGravar.disabled = true;
						} else if(document.getElementById('ind_acesso').value == 4) {
							
						} else if(document.getElementById('ind_acesso').value == 5) {
							document.Form1.btnIncluir.disabled = true;
						}
					}																	
				//} catch (ex){ document.write("<span style='color:white;'>" + ex.message + " - 5 - " + "</span>"); }													
				
				tam = "";
				if(document.getElementById("grdPesquisa"))	
					tam = document.getElementById("grdPesquisa").clientHeight;											
					
				try {					
					add = "";																	
					for(i=1; i<=10; i++) {										
						if(i == 10 && document.getElementById("grdPesquisa_0_cell_10").style.display != "none" ) {
							if (tam >= 260) {
								add = "OO";
							}
						} else if(i == 9 && document.getElementById("grdPesquisa_0_cell_9").style.display != "none" && document.getElementById("grdPesquisa_0_cell_9").style.display == "none") {
								if (tam >= 260) {
									add = "OO";
								}
						} else {
							add = "";
						}
						document.getElementById("header" + i).innerHTML = document.getElementById("txtMax" + i).value + add;
						document.getElementById("headerFake" + i).innerHTML = document.getElementById("txtMax" + i).value;								
					
						
					}														
					
					for(i=1; i<=10; i++) {					
						if(document.getElementById("grdPesquisa_0_cell_" + i)) {
							if(document.getElementById("grdPesquisa_0_cell_" + i).style.display == "none") {
								document.getElementById("tdHeader" + i).style.display = "none";
							}
						}					
					}	
				} catch (ex){ document.write("<span style='color:white;'>" + ex.message + " - 10 - " + "</span>"); }													
								
				try 
				{				
					if(document.getElementById("alterado") && top.document.id_selecionado_SEGW0065 >= 0)
						//document.getElementById('grdPesquisa_0').click();											
						document.getElementById(document.getElementById('ultimo_alterado').value).click();
						document.getElementById(document.getElementById('ultimo_alterado').value).focus();
						
						//alert(document.getElementById('ultimo_alterado').value.split("_")[1] * 13);						
						document.getElementById("divScrollGrid").scrollTop = document.getElementById('ultimo_alterado').value.split("_")[1] * 14;
						
				} catch (ex) { document.write("<span style='color:white;'>" + ex.message + " - 20 - " + "</span>"); }
				
				top.document.id_selecionado_SEGW0065 = 0;
				
				//if(document.getElementById("divScrollGrid") && document.getElementById("divScrollGrid").style.width <= 740) {
				//	document.getElementById("divScrollGrid").style.width = 740;
				//}				
				
				//if(document.getElementById("grdPesquisa") && document.getElementById("grdPesquisa").style.width <= 715) {
				//	document.getElementById("grdPesquisa").style.width = 715;
				//}
								
				top.escondeaguarde();
				
				if(document.getElementById('divResumoFatura'))
					document.getElementById('divResumoFatura').style.display = 'none';
				
				try {
					if (document.getElementById("grdPesquisa") && document.getElementById("divGridMovimentacao") && document.getElementById("divScrollGrid")) {
						
						if (tam > 215)
							tam = 215;
														
						//document.getElementById("divGridMovimentacao").style.height = (tam+35);
						//document.getElementById("divScrollGrid").style.height = tam;
					}
				} catch (ex) { document.write("<span style='color:white;'>" + ex.message + " - 30 - " + "</span>"); }
				
				function hideAndSeekHandler(op,w){
					return false;
					dv = document.getElementById("divGridMovimentacao");
					pag = document.getElementById("ucPaginacao_lblPaginacao");					
					
					if (op == 'esconder'){
						dv.style.width = w + 5;
						pag.style.width = dv.clientWidth;
						tp.style.width = "100%";
					}
					else{
						dv.style.width = "752px";
						pag.style.width = "752px";						
					}
				}
				
				function setWidths(){
					top.iniciarMenu();
				}
				
				
				controlaAcesso(document.getElementById('ind_acesso').value);
				
				// Projeto 539202 - Jo�o Ribeiro - 29/04/2009
				if(document.getElementById('ind_acesso').value == 6) {
				    document.Form1.btnGravar.disabled = true;
				    document.getElementById("txtBtnSalvar").disabled = true;
				}
		        // Projeto 539202 - Fim
								
		</script>
	</body>
</html>
