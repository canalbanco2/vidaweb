<%@ Page Language="vb" debug="true" AutoEventWireup="false" Codebehind="MovimentacaoUmaUm_Form.aspx.vb" Inherits="segw0065.MovimentacaoUmaUm_Form" EnableEventValidation="false" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>MovimentacaoUmaUm_Form</title>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<LINK href="css/EstiloBase.css" type="text/css" rel="stylesheet">
			<script src="scripts/script.js"></script>
			
<script>
		
var dt_ini_comp = parent.parent.dt_ini_comp;
var dt_fim_comp = parent.parent.dt_fim_comp;
//var dt_ini_comp_2_meses = parent.parent.dt_fim_comp_2_meses;

var valor_capital;
valor_capital = ''

var valor_salario;
valor_salario = ''
			
function guardaValor(src,tipo) {
	if (tipo == 'S')
		if (valor_salario == '')
			valor_salario = src.value;
		
	if (tipo == 'C')
		if (valor_capital == '')
			valor_capital = src.value;
		
}
			
function RetiraAspas(e){
    var str = document.Form1.txtTitNome.value;
	str = str.replace(/'/gi, "");
	document.Form1.txtTitNome.value = str;
}

function comparaValor(src,tipo) {

	if (tipo == 'S') {
		if (valor_salario != src.value) {
			document.Form1.controleAlteracaoSalario.value = 1;
		}
		else {
			document.Form1.controleAlteracaoSalario.value = 0;
		}
	}
		
	if (tipo == 'C') {
		if (valor_capital != src.value) {
			document.Form1.controleAlteracaoCapital.value = 1;
		}
		else {
			document.Form1.controleAlteracaoCapital.value = 0;
		}
	}
			
}
			
function isEmpty(str) 
{ 
	if (str==null) return true
	
	for (var intLoop = 0; intLoop < str.length; intLoop++)
		if (" " != str.charAt(intLoop))
			return false;            
	return true; 
}

function verifica_cpf(valor) 
{
    if (isEmpty(valor))
      return false;

	valor = valor.toString()
	
	valor = valor.replace( "-", "" );
	valor = valor.replace( "-", "" );
	valor = valor.replace( ".", "" );
	valor = valor.replace( ".", "" );
	valor = valor.replace( ".", "" );		

	if ((isNaN(valor)) && (valor.length != 11))
      return false
	
	Mult1 = 10   
	Mult2 = 11
	dig1 = 0
	dig2 = 0
	
	for(var i=0;i<=8;i++)
	{
	    ind = valor.charAt(i)
		dig1 += ((parseFloat(ind))* Mult1)
		Mult1--
	}
	
	for(var i=0;i<=9;i++)
	{
	    ind = valor.charAt(i)
		dig2 += ((parseFloat(ind))* Mult2)
		Mult2--
	}

	dig1 = (dig1 * 10) % 11   
	dig2 = (dig2 * 10) % 11   
	
	if (dig1 == 10)
      dig1 = 0
      
	if(dig2 == 10)
      dig2 = 0
	 
	if (parseFloat(valor.charAt(9)) != dig1)
		return false   
	if (parseFloat(valor.charAt(10)) != dig2)
		return false   
	
	//Verificar a digita��o de CPF com todos os d�gitos iguais
	igual = new Array();
	for (var i=0;i<=10;i++)
	{
		if (i == 0) {
			anterior = valor.substring(i,i+1);
		}
		else {
			if (anterior == valor.substring(i,i+1)) {
				igual[i] = "sim";
			}
			anterior = valor.substring(i,i+1);
		}
	}
	var cont = 1;
	for (var i=0;i<=10;i++)
	{
		if (igual[i] == "sim") {
			cont++;
		}
	}
	if (cont == 11)
		return false
	
	return true
}
	
	/*
	Tipo = 1, valida data de nascimento maior que a data atual
		
	*/		
	function validaData(valor, tipo) {
				
		valor = valor.toString();
		
		//var dia = parseInt(valor.substr(0, 2));
		var dia = valor.substr(0, 2);
		var mes = parseInt(valor.substr(3, 2));
		var ano = parseInt(valor.substr(6, 4));	
		
		if (valor.substr(3, 2) == '08')
			mes = 8
			
		if (valor.substr(3, 2) == '09')
			mes = 8
		//alert(valor.substr(0, 2));
		
		/*** Se tipo for igual a 1, verifica se a data � menor que a data de hoje ***/
		
		if (tipo == 1) {
			var dt = new Date();
		
			var dt1 = new Date(ano,mes-1,dia);
			var dt2 = new Date(dt.getYear(),dt.getMonth(),dt.getDate());
		
			if ((dt1 - dt2) > 0) {
				return false;
			}
		}
		
		/*** ***/
		
		//verificac�o b�sica
		if(!dia || !mes || !ano || dia > 31 || mes > 12 || ano > 2050 || dia < 0 || ano < 1800 || mes < 0) {
			return false
		}
		
		//fevereiro
		if(mes == 2 && ano%4 == 0) {
		
			if(dia > 29) {
				return false;
			}
		
		} else if( mes == 2 ){
		 
			if(dia > 28) {
				return false;
			}
		}
		
		var meses = new Array(1, 3, 5, 7, 8, 10, 12);		
		
		var meslongo = false;
		
		for(i=0; i< meses.length ; i++) {
			if(meses[i] == mes ) {				
				meslongo = true;				
			}			
		}
		
		if(!meslongo && dia > 30){		
			return false;
		}
			
		return true;		
	}
	
	function HabilitaBotoes(valor) {
	
		if (document.Form1.btnTitDesligar) {			
			document.Form1.btnTitDesligar.disabled=(valor==0);
		}
		
		if (document.Form1.btnTitGravar) {			
			document.Form1.btnTitGravar.disabled=(valor==0);
		}
				
		if (document.Form1.btnDepGravar) {			
			document.Form1.btnDepGravar.disabled=(valor==0);
		}
	}
	
	function validaCampos() {

		var v_oTipo = document.getElementById('<%=hdnTipoIS.ClientID%>');
		var v_VlCapitalCalculado = 0;
		var v_oUltCapSegurado = document.getElementById('<%=hdnUltCapSegurado.ClientID%>');
		var v_oFatorMultiplicador = document.getElementById('<%=hdnFatorMultiplicador.ClientID%>');
		/*var v_oPercLimite = document.getElementById('<%=hdnPercLimite.ClientID%>');*/
		var v_oPercLimite = 0;
			
		if(document.Form1.txtTitSalario) {
            var salario = document.Form1.txtTitSalario.value.replace('.', '').replace(',', '.');
        } 
        else {
			var salario = -1;
        }        
        
        if(document.Form1.txtTitCapital) {
		    var capital_segurado = document.Form1.txtTitCapital.value.replace('.', '').replace(',', '.');
		} 
		else {
		    var capital_segurado = -1;
        }
        
        var v_iMulti = v_oFatorMultiplicador.value;
        
		if (v_oTipo.value == 'M�ltiplo sal�rio') {
		    var v_oNovoCapital = parseFloat(salario) * (v_oFatorMultiplicador.value * 1.0);
		
		}
		else if (v_oTipo.value == 'Capital informado') {
		    var v_oNovoCapital = capital_segurado * 1.0;		    
		}
		
		var v_dCapitalMaximo = (v_oUltCapSegurado.value * 1.0) + ( (v_oUltCapSegurado.value * 1.0) * ( (v_oPercLimite.value * 1.0) / 100) );
		
		//alert('v_oNovoCapital: ' + v_oNovoCapital + ' v_dCapitalMaximo: ' + v_dCapitalMaximo)
                               //Permite-se a inclus�o de qualquer valor, entretanto quando exceder o limite, ele coloca como pendente
                               //e atribui ao capital o valor m�ximo permitido . . .
                               /*if (v_oTipo.value == 'M�ltiplo sal�rio' || v_oTipo.value == 'Capital informado') {                       
                                   if (v_oNovoCapital > v_dCapitalMaximo) {
                                       alert('Capital segurado informado/calculado superior ao limite t�cnico da companhia.')
                return false;
            }
            else {
                //alert('aprovado');
                //return false;
            }
                    }*/
	    	
		HabilitaBotoes(0);
		
		//cpf ok!! verifica se ele j� existe									
		if(document.Form1.acao.value != "incluirdependente" && document.Form1.acao.value != "alterardependente" && document.Form1.acao.value != "excluirdependente") {			
			var cpf = document.Form1.txtTitCPF.value;
		} else {
			var cpf = document.Form1.txtDepCPF.value;
		}
				
		if (window.XMLHttpRequest) { // Mozilla, Safari,...
			http_request = new XMLHttpRequest();				
		} else if (window.ActiveXObject) { // IE
			try {
				http_request = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {}
			}
		}

		if (!http_request) {
			alert('N�o foi poss�vel criar XMLHTTP Request');
			return false;
		}

		http_request.open('post', "ajaxVerifCpfExiste.aspx", true);
		
		http_request.setRequestHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		http_request.setRequestHeader("Cache-Control", "post-check=0, pre-check=0");
		http_request.setRequestHeader("Pragma", "no-cache");

		http_request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		http_request.send("&cpf=" + cpf );
		
		http_request.onreadystatechange = function() {				
			
			if (http_request.readyState == 4) {											
				document.getElementById('debug').value = http_request.responseText;
				if(http_request.responseText.toString().length > 0) {
					document.getElementById("txtCpfExiste").value = 1;																							
				} else {
					document.getElementById("txtCpfExiste").value = 0;							
				}	
				
				
			    if(validaCampos2()) {	
				    document.getElementById("Form1").onSubmit = function() { return true; };
				    document.getElementById("Form1").submit();							
			    } else {
			        HabilitaBotoes(1);
				    trataBotao();
				    http_request.onreadystatechange = function() { return false; };																												
					
				    if (window.XMLHttpRequest) { // Mozilla, Safari,...
					    http_request = new XMLHttpRequest();				
				    } else if (window.ActiveXObject) { // IE
					    try {
						    http_request = new ActiveXObject("Msxml2.XMLHTTP");
					    } catch (e) {
						    try {
							    http_request = new ActiveXObject("Microsoft.XMLHTTP");
						    } catch (e) {}
					    }
				    }
			    }
				
				
				return false;
			}
		};
		
		return false;
	}
	
	function GetXmlHttpObject() { 
        var xmlHttp = null; 
        try { 
            // Firefox, Opera 8.0+, Safari 
            xmlHttp = new XMLHttpRequest(); 
        } 
        catch(e) { 
            // Internet Explorer 
            try { 
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); 
            } 
            catch(e) { 
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP"); 
            } 
        } 
        return xmlHttp; 
    }    
	
	function validaCampos2() {		
				
		if(document.getElementById("lockValidation").value == "1")
			return true;			
			
	    //cpf ok!! verifica se ele j� existe									
		if(document.Form1.acao.value != "incluirdependente" && document.Form1.acao.value != "alterardependente" && document.Form1.acao.value != "excluirdependente") {			
			var v_cpf = document.Form1.txtTitCPF.value;
		} else {
			var v_cpf = document.Form1.txtDepCPF.value;
		}
			
	    if (document.Form1.acao.value.indexOf('inclui') > -1) {
            if (!validaCpfOutroSubGrupo(v_cpf)) {
			    return false;
			}
        }
		
		if(document.Form1.acao.value != "incluirdependente" && document.Form1.acao.value != "alterardependente" && document.Form1.acao.value != "excluirdependente") {
			var nome = document.Form1.txtTitNome.value;				
			var cpf = document.Form1.txtTitCPF.value;
					
			
			
			
			
			
			var dtAdmissao = document.Form1.txtTitAdmissao.value;
			var sexo = document.Form1.ddlTitSexo.value
			var dt_nascimento = document.Form1.txtTitNascimento.value;
			if(document.Form1.txtTitSalario) {
				var salario = document.Form1.txtTitSalario.value;
			} else {
				var salario = -1;
			}			
			if(document.Form1.txtTitSalario) {
				var salario = document.Form1.txtTitSalario.value;
			} else {
				var salario = -1;
			}
			if(document.Form1.txtTitCapital) {
				var capital_segurado = document.Form1.txtTitCapital.value;
			} else {
				var capital_segurado = -1;
			}
			var error = 0;		
								
			//alert(document.Form1.txtTitNome.readOnly);	
			if(nome == "") {	
				if (!document.Form1.txtTitNome.readOnly) {					
					alert('Informe o nome.');
					trataBotao();
					return false;				
				}
			}
			
			if(!verifica_cpf(cpf)) {										
				if (!document.Form1.txtTitCPF.readOnly) {
					alert('Informe um CPF v�lido.');
					trataBotao();
					return false;				
				}
			}  else {
				//cpf ok, verifica se ele existe
				if (!document.Form1.txtTitCPF.readOnly) {
					if(document.getElementById("txtCpfExiste").value == 1) {
						alert("J� existe um segurado com o mesmo CPF. Verificar se o segurado j� foi inclu�do.");
						return false;
					}
				}
			}
		
		
		
		
		
			if(!validaData(dt_nascimento,1)) {
				if (!document.Form1.txtTitNascimento.readOnly) {							
					alert('Informe a Data de Nascimento.');
					trataBotao();
					return false;	
				}
			} else {
				if (!document.Form1.txtTitNascimento.readOnly) {
					/*try {*/
						var dtNascMin = eval(document.getElementById("dataMaxNascimento").innerHTML);
						var dtNascMax = eval(document.getElementById("dataMinNascimento").innerHTML);
						
						var intNascMax = parseInt(document.getElementById("intMaxNascimento").innerHTML);
						var intNascMin = parseInt(document.getElementById("intMinNascimento").innerHTML);
						
						var dados;
						dados = dt_nascimento.split('/');																		
						dados2 = dtAdmissao.split('/');
								
						var dtNasc = new Date(dados[2],dados[1],dados[0]);
						
						if (validaData(dtAdmissao,2)) {														
							
							var idadeSeg = dados2[2] - dados[2];
							
							if(parseInt(dados[1],10) > parseInt(dados2[1],10)) {								
								idadeSeg = idadeSeg - 1;
							} else if(parseInt(dados[1],10) == parseInt(dados2[1],10) && parseInt(dados[0],10) > parseInt(dados2[0],10)) {								
								idadeSeg = idadeSeg - 1;
							}
							
							if(intNascMin > idadeSeg) {
							    alert('Proponente com idade inferior ao limite permitido do subgrupo.');
							    trataBotao();
							    return false;
						    } 
							
							//ercosta - 6234933
							/* VALIDAR SE JA EXISTE EM OUTRO SUBGRUPO */
						    http_request = GetXmlHttpObject(); 

                            if (!http_request) {
	                            alert('N�o foi poss�vel criar XMLHTTP Request');
	                            return false;
                            }
                    		
                            var url = "VerificaCpfSubGrupo.aspx";
                            var params = ("cpf=" + cpf); 
							
							http_request.open('post',url, false);		
		                    //Send the proper header information along with the request 
                            http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
                            http_request.setRequestHeader("Content-length", params.length); 
                            http_request.setRequestHeader("Connection", "close"); 
                            http_request.send(params); 
                            
                            if (http_request.readyState == 4) {
                                //SE N�O EXISTIR EM OUTRO SUBGRUPO TERA Q VALIDAR A IDADE MAXIMA
                                //alert(http_request.responseText.toString().length);
	                            if(http_request.responseText.toString().length <= 0) {
	                                
	                                if(intNascMax < idadeSeg) 
	                                {
	                                    //Autor: pablo.cardoso (Nova Consultoria) 
                                        //Data da Altera��o: 16/01/2012
                                        //Demanda AB: 12784293 
                                        //Item: 7 - Permitir incluir vida que j� participou da ap�lice em algum momento, inclusive se tiver idade superior.
                                        verificarParticipouApolice(cpf);
                                        //alert(document.getElementById("txtCpfParticipouApolice").value);
    	                                
	                                    if(document.getElementById("txtCpfParticipouApolice").value == "0")
	                                    {
	                                        alert('Proponente com idade superior ao limite permitido do subgrupo.');
							                trataBotao();
							                return false;
	                                    }
	                                    //fim pablo.cardoso
							            
						            } 
	                            }
	                            else {
	                                 //alert('CPF encontrado em outro SG, idade superior ao limite.');
	                            }
                            }
						}
						

					/*} catch (e) {
						alert("arg");
					}*/
				}
			}
			if(sexo == '0') {						
				if(!document.Form1.ddlTitSexo.readOnly && !document.Form1.ddlTitSexo.disabled) {
					alert('Informe o sexo.');
					trataBotao();
					return false;	
				}
			}
			
			
			
			if(!validaData(dtAdmissao,2)) {		
				if (!document.Form1.txtTitAdmissao.readOnly) {								
					alert('Informe a Data de inclus�o no seguro.');
					trataBotao();
					return false;
				}
			} else {
				if (!document.Form1.txtTitAdmissao.readOnly) {
					try {
						var dados;
						dados = dt_fim_comp.split('/');
						
						var dados1;
						dados1 = document.Form1.txtTitAdmissao.value.split('/');
						
						var dados_ini;
						dados_ini = dt_ini_comp.split('/');
						
						//if (hf_dt_inicio_ultima_fatura.value != null)
						    //{
						       //var dados_inicio_ult_fatura;
						       //dados_inicio_ultima_fatura = hf_dt_inicio_ultima_fatura.split('/');
						    //}
						
										
							
						var dt1 = new Date(dados[2],dados[1]-1,dados[0]);                 //Data fim comp
						var dt2 = new Date(dados1[2],dados1[1]-1,dados1[0]);              //Data inclus�o no seguro
						var dt_ini = new Date(dados_ini[2],dados_ini[1]-1,dados_ini[0]);  //Data inicio comp						
						
						if (parent.parent.document.getElementById('lblCapitalSegurado').innerHTML == 'Capital Global') 
						{
							/* Foi solicitado pela Alian�a do Brasil, para que fosse removido tempor�riamente esta l�gica, 
								e que implementasse a possibilidade de permitir que a vida possa ser inclu�da com at� 30 
								dias anteiores ao in�cio de vig�ncia
							if ( ((dt2 - dt1) > 0) || ((dt_ini - dt2) > 0) ) {
								alert('Data de inclus�o no seguro deve estar dentro do per�odo de vig�ncia.');
								return false;
							} 
							*/
							/* 
							FLOW 599077 - dgomes - CONFITEC - 03/12/2008
							O c�digo abaixo precisou ser comentado e o c�digo do bloco comentado
							acima precisou ser novamente liberado para atendimento ao Flow citado.
							Segue c�digo abaixo deste bloco comentado.

							dt_ini = new Date(dt_ini - (30 * 24 * 60 * 60 * 1000));
							
							if ( ((dt2 - dt1) > 0) || ((dt_ini - dt2) > 0) ) 
							{
								alert('Data de inclus�o no seguro deve ser no m�ximo at� 30 dias anteriores ao per�odo de vig�ncia.');
							}
							*/

							//if ( ((dt2 - dt1) > 0) || ((dt_ini - dt2) > 0) ) {
								//alert('Data de inclus�o no seguro deve estar dentro do per�odo de vig�ncia.');
								//return false;
							//} 
							
							if ((dt_ini - dt2) > 0)  {
								alert('Data de inclus�o no seguro deve estar dentro do per�odo de vig�ncia.');
								return false;
							}
							 
						} 
						else 
						{	
							if ((dt2 - dt1) > 0) {
								alert('Data de inclus�o no seguro maior que a data fim de compet�ncia.');
								trataBotao();
								return false;
							}	
						}
						
						
						
					} catch(ex) {alert(ex);trataBotao();return false;}
				}
			}
			
			
			
			if(salario == "") {	
				if(document.Form1.txtTitSalario) {
					if (!document.Form1.txtTitSalario.readOnly) {										
						alert('Informe o sal�rio.');
						trataBotao();
						return false;
					}
				}
			}					
			if(capital_segurado == "") {			
				if(document.Form1.txtTitCapital) {
					if (!document.Form1.txtTitCapital.readOnly) {										
						alert('Informe o capital.');
						trataBotao();
						return false;	
					}
				}
			}
			valor = ""
			try {
				valor = parent.parent.document.getElementById('acaoClickada').value;						
			} catch (e) { }
															
			if (document.Form1.txtTitDesligamento && valor.indexOf("apagar") == -1) {
			    var dtValidar = document.Form1.txtTitDesligamento.value;
			    
				if(!validaData(dtValidar,2)) 
				{
					alert('Informe a Data de Exclus�o.');
					trataBotao();
					return false;
				} 
				else 
				{
				    //Autor: pablo.cardoso (Nova Consultoria) 
                    //Data da Altera��o: 17/01/2012
                    //Demanda AB: 12784293 
                    //Item: 11 - N�o permitir a exclus�o de vidas com data anterior ao sinistro.
				    verificarSinistro(dtValidar, v_cpf);
				    
				    if(document.getElementById("txtExisteSinistroDataCpf").value == 1)
				    {
				        alert('Exclus�o n�o pode ser efetuada com data anterior ao sinistro ou evento.');
				        return false;
				    }
				    //fim pablo.cardoso
				    
					var dados;
					dados = dt_fim_comp.split('/');
					
					var dados1;
					dados1 = document.Form1.txtTitDesligamento.value.split('/');
					
					var dados_ini;
					dados_ini = dt_ini_comp.split('/');
												
					var dt1		= new Date(dados[2],dados[1]-1,dados[0]);					//data de Fim de compet�ncia
					var dt2		= new Date(dados1[2],dados1[1]-1,dados1[0]);				//data de desligamento
					var dt_ini	= new Date(dados_ini[2],dados_ini[1]-1,dados_ini[0]);		//inicio de competencia
					var dt3		= new Date(dados_ini[2],dados_ini[1]-2,dados_ini[0]);		//inicio de competencia - 1 mes
					var dtAtual = new Date();												//Data Atual
					if (parent.parent.document.getElementById('lblCapitalSegurado').innerHTML == 'Capital Global') 
					{
						if ( (dt2 - dt1) > 0) {
						//if ( ((dt2 - dt1) > 0) || ((dt_ini - dt2) > 0) ) {
							alert('Data de Desligamento deve ser inferior ao final do per�odo de vig�ncia.');
							return false;
						}
						
						if ( (dt2 - dt3) < 0 || (dt2 - dt_ini) >= 0) {
						//if ( ((dt2 - dt1) > 0) || ((dt_ini - dt2) > 0) ) {
							alert('Somente s�o permitidas exclus�es simples em ap�lices do tipo Capital Global.');
							return false;
						}						
					} 
				}
									
				if (dt_entrada != '')
				{				
					//Tratamento para Altera��es
					ind_operacao = document.Form1.acao.value;					
					if (ind_operacao.indexOf("alterar") != -1){
						try {
							var dt = new Date();
							var dt1 = new Date();
							var dt2 = new Date();
							var dados;
							var dados2;					
							var dados1;					
							
							dados = dt_entrada.split('/');
							dados1 = document.Form1.txtTitDesligamento.value.split('/');						
							dados2 = dt_ini_comp_2_meses.split('/')							
							dados3 = dt_fim_comp.split('/');	
								
							dt1 = new Date(dados[2],dados[1]-1,dados[0]);		//data de inicio de competencia							
							dt2 = new Date(dados1[2],dados1[1]-1,dados1[0]);	//data de desligamento
							dt3 = new Date(dados3[2],dados3[1]-1,dados3[0]);		//data de fim de competencia													
							
							if((dt1 - dt2) <= 0) {
								//alert('A Data de exclus�o deve ser maior que o in�cio do per�odo de compet�ncia (Exclus�o Agendada).');
								trataBotao();
								return false;
							}
						}
						catch(ex) {alert(ex); trataBotao(); return false;}						
					}
					//alert('aqui');
					//Exclus�o normal									
						try {						
															
							var dt = new Date();
							var dados;
							dados = dt_entrada.split('/');
																			
							var dados1;
							dados1 = document.Form1.txtTitDesligamento.value.split('/');
							dados3 = dt_fim_comp.split('/');	
								
							var dt1 = new Date(dados[2],dados[1]-1,dados[0]);		//data de inclus�o no seguro
							var dt2 = new Date(dados1[2],dados1[1]-1,dados1[0]);	//data de desligamento
							var dt3 = new Date(dados3[2],dados3[1]-1,dados3[0]);	//data de fim de competencia							
							
							if ((dt1 - dt2) > 0) {
								alert('Data de exclus�o menor que Data de Entrada');
								trataBotao();
								return false;
							}
							
							dados = dt_ini_comp.split('/');
							dados4 = dt_fim_comp.split('/');
							
							dados1 = document.Form1.txtTitDesligamento.value.split('/');						
							dados2 = dt_ini_comp_2_meses.split('/')
								
							dt1 = new Date(dados[2],dados[1]-1,dados[0]);		//data de inicio de competencia
							dt2 = new Date(dados1[2],dados1[1]-1,dados1[0]);	//data de desligamento
							dt3 = new Date(dados2[2],dados2[1]-1,dados2[0]);	//data de inicio de competencia - 2 meses
							dt4 = new Date(dados4[2],dados4[1],dados4[0]);		//Data Fim de compet�ncia + 1 mes
							dt5 = new Date(dados4[2],dados4[1]-1,dados4[0]);	//Data Fim de compet�ncia mes
							dtAtual = new Date();								//Data Atual
								
							if(dt2 - dt5 > 0) {
								//alert('Data de exclus�o maior que a data fim de compet�ncia.');
								alert('Data de exclus�o superior ao fim de vig�ncia do pr�ximo per�odo.');
								trataBotao();
								return false;
							}
									
							/*if( dt2 - dt1 > 0 ) {								
								alert('Data de exclus�o maior que a data de in�cio de compet�ncia.');
								trataBotao();
								return false;
							}
							
							if( ( dtAtual - dt5 > 0 || dt1 - dtAtual > 0 ) && dt2 - dt5 <= 0 ) {
								if(dt2 - dt1 >= 0) {							
									alert('Data de exclus�o maior que a data de in�cio de compet�ncia.');
									trataBotao();
									return false;
								}			
							} else {
								if(dt2 - dt4 > 0) {							
								//if(dt2 - dt5 > 0) {
									//alert('Data de exclus�o maior que a data fim de compet�ncia.');
									alert('Data de exclus�o superior ao fim de vig�ncia do pr�ximo per�odo.');
									trataBotao();
									return false;
								}							
							}
							
							if ((dt2 - dtAtual >= 0) && (dt4 - dt1 > 0)) {							
								alert('Data de exclus�o superior ao fim de vig�ncia do pr�ximo per�odo.');
								trataBotao();
								return false;
							}*/													
													
							
						} catch(ex) {alert(ex); trataBotao(); return false;}
					}
				
			}			
		} else {								
			var nome = document.Form1.txtDepNome.value;				
			var cpf = document.Form1.txtDepCPF.value;			
			var dt_nascimento = document.Form1.txtDepNascimento.value;
			
			if(nome == "") {
				if (!document.Form1.txtDepNome.readOnly) {																
					alert('Informe o nome.');
					trataBotao();
					return false;
				}				
			}
						
			if(!verifica_cpf(cpf)) {			
				if (!document.Form1.txtDepCPF.readOnly) {
					alert('Informe um CPF v�lido.');
					trataBotao();
					return false;		
				}		
			} else {
				//cpf ok, verifica se ele existe
				if (!document.Form1.txtDepCPF.readOnly) {
					if(document.getElementById("txtCpfExiste").value == 1) {
						alert("J� existe um proponente com o cpf informado.");
						return false;
					}
				}
			}
						
			if(!validaData(dt_nascimento,1)) {
				if (!document.Form1.txtDepNascimento.readOnly) {		
					alert('Informe a Data de Nascimento.');
					trataBotao();
				 	return false;	
				}
			}
			
		
			
			
		//----------- INFORMATA DATA RETROATIVA ----------------------------------
			
			
			
			
			//29062010 - Douglas - Confitec
            //FLOW 4192841 - FATURAMENTO WEB - IMPOSSIBILIDADE DE REALIZAR EXCLUS�O (Fw: FLOW WEB - IMPOSSIBILIDADE DE EXCLUIR VIDAS)
            //Remontagem da string de data
            
 			/*
            var dataIniVig = document.getElementById("HiddenDtInicio").value;
            var dataFimVig = document.getElementById("HiddenDtFim").value;
                  
            var nova_dataIniVig = parseInt(dataIniVig.split("/")[2].toString() + dataIniVig.split("/")[1].toString() + dataIniVig.split("/")[0].toString());
            var nova_dataFimVig = parseInt(dataFimVig.split("/")[2].toString() + dataFimVig.split("/")[1].toString() + dataFimVig.split("/")[0].toString());
            
            var dataFimUltimaFatura = document.getElementById("hf_dt_fim_ultima_fatura").value;
            var nova_dataFimUltimaFatura = parseInt(dataFimUltimaFatura.split("/")[2].toString() + dataFimUltimaFatura.split("/")[1].toString() + dataFimUltimaFatura.split("/")[0].toString());
            
            var dataFimPenultimaFatura = document.getElementById("hf_dt_fim_penultima_fatura").value;            
            var nova_dataFimPenultimaFatura = parseInt(dataFimPenultimaFatura.split("/")[2].toString() + dataFimPenultimaFatura.split("/")[1].toString() + dataFimPenultimaFatura.split("/")[0].toString());
            
            var mostraMsgRetroativa = 0;
                        
            if (document.getElementById("HiddenAcaoMsg").value.indexOf('apagar') == -1)
            {
                if (document.Form1.acao.value.indexOf('inclui') > -1)
                {
                    var dataAdmissao = document.Form1.txtTitAdmissao.value;
                    var nova_dataAdmissao = parseInt(dataAdmissao.split("/")[2].toString() + dataAdmissao.split("/")[1].toString() + dataAdmissao.split("/")[0].toString());
                    
                    if (nova_dataAdmissao < nova_dataFimUltimaFatura)
                    {
                        mostraMsgRetroativa = 1;
                    }                
                }
                if (document.Form1.acao.value.indexOf('excluir') > -1)
                {
                    var dataExclusao = document.Form1.txtTitDesligamento.value
                    var nova_dataExclusao = parseInt(dataExclusao.split("/")[2].toString() + dataExclusao.split("/")[1].toString() + dataExclusao.split("/")[0].toString());                   
                    
                    if (nova_dataExclusao < nova_dataFimPenultimaFatura)
                    {
                        mostraMsgRetroativa = 1;
                    }                   
                }
            }
            if (mostraMsgRetroativa == 1)
            {
                if (confirm("A altera��o refletir� no valor total da fatura. Confirma a opera��o retroativa?")) {
                    document.getElementById("HiddenAcaoOperacao").value = "S";
                } else {
                    return false;
                }
            }
            */
            //-----------------------------------------------------------------------------
			
            
            
			
            var dataIniVig = document.getElementById("HiddenDtInicio").value;
            var dataFimVig = document.getElementById("HiddenDtFim").value;
                  
            var nova_dataIniVig = new Date(dataIniVig.split("/")[2].toString() + "/" + dataIniVig.split("/")[1].toString() +  "/" + dataIniVig.split("/")[0].toString());
             
            var nova_dataFimVig = new Date(dataFimVig.split("/")[2].toString() + "/" +  dataFimVig.split("/")[1].toString() +  "/" + dataFimVig.split("/")[0].toString());
            
            
            var dataFimUltimaFatura = document.getElementById("hf_dt_fim_ultima_fatura").value;
            
            if (isEmpty(dataFimUltimaFatura) == false)
                {
                  var nova_dataFimUltimaFatura = new Date(dataFimUltimaFatura.split("/")[2].toString() + "/" +  dataFimUltimaFatura.split("/")[1].toString() + "/" +  dataFimUltimaFatura.split("/")[0].toString());
                }
            
            
            
            var dataFimPenultimaFatura = document.getElementById("hf_dt_fim_penultima_fatura").value;            
                      
            if (isEmpty(dataFimPenultimaFatura) == false) 
                {                            
                  var nova_dataFimPenultimaFatura = new Date(dataFimPenultimaFatura.split("/")[2].toString() + "/" +  dataFimPenultimaFatura.split("/")[1].toString() + "/" +  dataFimPenultimaFatura.split("/")[0].toString());
                }
            
                
            var mostraMsgRetroativa = 0;
                        
            if (document.getElementById("HiddenAcaoMsg").value.indexOf('apagar') == -1)
             {
                if (document.Form1.acao.value.indexOf('inclui') > -1)
                 {
                    var dataAdmissao = document.Form1.txtTitAdmissao.value;
                    var nova_dataAdmissao = new Date(dataAdmissao.split("/")[2].toString() + "/" + dataAdmissao.split("/")[1].toString() + "/" + dataAdmissao.split("/")[0].toString());
                  
                    if (isEmpty(dataFimUltimaFatura) == false)
                     {
                      if (nova_dataAdmissao < nova_dataFimUltimaFatura)
                       {
                          mostraMsgRetroativa = 1;
                       }
                     }
                  }
                  
                if (document.Form1.acao.value.indexOf('excluir') > -1)
                 {
                    var dataExclusao = document.Form1.txtTitDesligamento.value
                    var nova_dataExclusao = new Date(dataExclusao.split("/")[2].toString() + "/" + dataExclusao.split("/")[1].toString() + "/" + dataExclusao.split("/")[0].toString());                   
                    
                    if (isEmpty(dataFimPenultimaFatura) == false)
                     {
                      if (nova_dataExclusao < nova_dataFimPenultimaFatura)
                       {
                          mostraMsgRetroativa = 1;
                       }
                     }   
                  }
              }


            if (mostraMsgRetroativa == 1)
            {
                if (confirm("A altera��o refletir� no valor total da fatura. Confirma a opera��o retroativa?")) {
                    document.getElementById("HiddenAcaoOperacao").value = "S";
                    
                } else {
                    return false;
                }
            }
            //-----------------------------------------------------------------------------	
			
						
									
		}
		
		
		if (error > 0) {
			trataBotao();
			return false;
		}		
		
		if(document.Form1.acao.value != "incluirdependente" && document.Form1.acao.value != "incluir") {	
			if(document.Form1.acao.value != "alterardependente" && document.Form1.acao.value != "excluirdependente") {												
				if (document.Form1.txtTitCPF.readOnly) {
					//alert("assas");
					document.Form1.action="confirmPost.aspx?destino=MovimentacaoUmaUm_Form.aspx";
				}			
			} else {			
				if (document.Form1.txtDepCPF.readOnly) {
					//alert("QWQW");
					document.Form1.action="confirmPost.aspx?destino=MovimentacaoUmaUm_Form.aspx";
				}						
			}		
		}		
		
		//alert(document.Form1.action);
				
		return true;				
	}
	
	function validaCpfOutroSubGrupo(cpf)
	{
	    http_request = GetXmlHttpObject(); 
	    
		if (!http_request) {
			alert('N�o foi poss�vel criar XMLHTTP Request');
			return false;
		}
		
		var url = "VerificaCpfSubGrupo.aspx";
		var params = ("cpf=" + cpf); 
 
		http_request.open('post',url, false);		
		//Send the proper header information along with the request 
        http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
        http_request.setRequestHeader("Content-length", params.length); 
        http_request.setRequestHeader("Connection", "close"); 
        http_request.send(params); 

		if (http_request.readyState == 4) 
	    {			
	            if(http_request.responseText.toString().length > 0) 
	            {
	                if (confirm(http_request.responseText.toString())) //se retornar algum texto
	                {
	                    return true;
                    } 
                    else
                    {
                        HabilitaBotoes(1);
	                    trataBotao();
	                    http_request.onreadystatechange = function() { return false; };
                        return false;                        
                    }
	            } 
        }
        return true;	
        http_request.onreadystatechange = function() { return false; };	   
    }
	
	function trataBotao() {		
						
		if (document.Form1.btnTitDesligar) {			
			document.Form1.btnTitDesligar.onclick = function () {validaCampos()};
		}
		
		if (document.Form1.btnTitGravar) {			
			document.Form1.btnTitGravar.onclick = function () {validaCampos()};
		}
				
		if (document.Form1.btnDepGravar) {			
			document.Form1.btnDepGravar.onclick = function () {validaCampos()};
		}
		
		if (document.Form1.btnTitApagar) {
			document.Form1.btnTitApagar.onclick = 'this.onclick = function(){return false;};';
		}
	}
	
	function escondeDiv(div){
		var idDiv = document.getElementById(div);
		if (idDiv.style.display=='block'){
			idDiv.style.display='none';
		}else{
			idDiv.style.display='block';
		}
	}

	function FormataData(pForm,pCampo,pTamMax,pPos1,pPos2,pTeclaPres){
		var wTecla, wVr, wTam;

		wTecla = pTeclaPres.keyCode;
		wVr = pForm[pCampo].value;
		
		wTam = wVr.length ;	
		ult	= wVr.substring(wTam-1,wTam);
		vl = ult.charCodeAt(0);
		//trava caracteres n�o num�ricos
		if (vl < 48 || vl > 57){
			wVr = wVr.replace( ult.valueOf() , "" );
			pForm[pCampo].value = wVr;
		}
		
		wVr = wVr.toString().replace( "-", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( "/", "" );
		wVr = wVr.toString().replace( "/", "" );
		wVr = wVr.toString().replace( "/", "" );
		wVr = wVr.toString().replace( "\\", "" );
		wTam = wVr.length ;

		if(wTam >= pTamMax) {
			wTam = pTamMax;
			wVr = wVr.substr(0, pTamMax);		
		}					

		if (wTam < pTamMax && wTecla != 8) { 
			wTam = wVr.length + 1 ; 
		}

		if (wTecla == 8 ) { 
			wTam = wTam - 1 ; 
		}
		   
		if ( wTecla == 8 || wTecla == 88 || wTecla >= 48 && wTecla <= 57 || wTecla >= 96 && wTecla <= 105 ){
		if ( wTam <= 2 ){
			pForm[pCampo].value = wVr ;
		}
		if (wTam > pPos1 && wTam < pPos2) {
				wVr = wVr.substr(0, wTam - pPos1) + '/' + wVr.substr(wTam - pPos1, wTam);
		}
		if( wTam >= pPos2) {		
						
			wVr = wVr.substr( 0, pPos1 ) + '/' + wVr.substr(pPos1, 2) + '/' + wVr.substr(pPos1 + 2, wTam);
		}
		
		pForm[pCampo].value = wVr;
		 
		}

		}	
	function FormataCPF(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres){
		var wTecla, wVr, wTam;
		 
			//alert(pForm);
		  
		wTecla = pTeclaPres.keyCode;
		wVr = pForm[pCampo].value;
		
		wTam = wVr.length ;	
		ult	= wVr.substring(wTam-1,wTam);
		vl = ult.charCodeAt(0);
		//trava caracteres n�o num�ricos
		if (vl < 48 || vl > 57){
			wVr = wVr.replace( ult.valueOf() , "" );
			pForm[pCampo].value = wVr;
		}
		
		wVr = wVr.toString().replace( "-", "" );		
		wVr = wVr.toString().replace( "-", "" );
		wVr = wVr.toString().replace( "-", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( "/", "" );
		wTam = wVr.length ;			

		if(wTam > pTamMax) {
			wTam = pTamMax;
			wVr = wVr.substr(0, pTamMax)
		}
			
		if (wTam < pTamMax && wTecla != 8) { 
			wTam = wVr.length + 1 ; 
		}

		if (wTecla == 8 ) { 
			wTam = wTam - 1 ; 
		}
		   
		if ( wTecla == 8 || wTecla == 88 || wTecla >= 48 && wTecla <= 57 || wTecla >= 96 && wTecla <= 105 ){
		if ( wTam <= 2 ){
			pForm[pCampo].value = wVr ;
		}
		if (wTam > pPosTraco && wTam <= pTamMax) {
				wVr = wVr.substr(0, wTam - pPosTraco) + '-' + wVr.substr(wTam - pPosTraco, wTam);
		}
		if ( wTam == pTamMax){
				wVr = wVr.substr( 0, wTam - pPos1 ) + '.' + wVr.substr(wTam - pPos1, 3) + '.' + wVr.substr(wTam - pPos2, wTam);
		}
		
		
		pForm[pCampo].value = wVr;
		 
		}

		}
		
		function getCapitalFaixaEtaria() {
			var acao = document.Form1.action;
			var alvo = document.Form1.target;
			document.getElementById("lockValidation").value="1";
			document.Form1.action = "getCapitalFaixaEtaria.aspx";
			document.Form1.target = "processo";						
			
			document.Form1.submit();
			
			document.getElementById("lockValidation").value="0";
			document.Form1.action = acao;
			document.Form1.target = alvo;									
		}
		
		//Autor: pablo.cardoso (Nova Consultoria) 
        //Data da Altera��o: 16/01/2012
        //Demanda AB: 12784293 
        //Item: 7 - Permitir incluir vida que j� participou da ap�lice em algum momento, inclusive se tiver idade superior.
		function verificarParticipouApolice(cpf) 
        {
            
	        var req;
	        var resposta = false;
         
	        req = GetXmlHttpObject();
        	
	        req.onreadystatechange = function() 
	        {
	            
	            
		        if (req.readyState == 4 && req.status == 200) 
		        {
			        document.getElementById("txtCpfParticipouApolice").value = req.responseText;
		        }
	        }
         
	        var url = "VerificarCpfParticipouApolice.aspx?cpf=" + cpf;
	        req.open("GET", url, false);
	        req.send();
        }
        
        //Autor: pablo.cardoso (Nova Consultoria) 
        //Data da Altera��o: 17/01/2012
        //Demanda AB: 12784293 
        //Item: 11 - N�o permitir a exclus�o de vidas com data anterior ao sinistro.
        function verificarSinistro(data, cpf)
        {
            var req;
	        var resposta = false;
         
	        req = GetXmlHttpObject();
        	
	        req.onreadystatechange = function() 
	        {
		        if (req.readyState == 4 && req.status == 200) 
		        {
			        document.getElementById("txtExisteSinistroDataCpf").value = req.responseText;
		        }
	        }
	        
	        var url = "VerificarCpfSinistro.aspx?cpf=" + cpf + "&data=" + data;
	        req.open("GET", url, false);
	        req.send();
        }
	
		
</script>
</HEAD>
	<body>		
		<div id="overDiv" style="Z-INDEX:1000; VISIBILITY:hidden; POSITION:absolute"></div>
		<form id="Form1" name="form" method="post" runat="server">
            <asp:HiddenField ID="HiddenDtInicio" runat="server" />
            <asp:HiddenField ID="HiddenDtFim" runat="server" />
            <asp:HiddenField ID="HiddenAcaoOperacao" runat="server" />
            <asp:HiddenField ID="HiddenAcaoMsg" runat="server" />
            		<asp:Label id="lblAcaoRetroativa" runat="server" CssClass="Caminhotela" Visible="false">dfdfdf</asp:Label>
		<textarea name="debug" id="debug" style="display:none;"></textarea>
			<asp:panel id="pnlCadastro" runat="server" Visible="False" Width="100%" HorizontalAlign="Left">
<TABLE id="Table2" cellSpacing="0" cellPadding="2" width="100%" border="0">
  <TR>
    <TD vAlign="middle" align="left" width="20%" colSpan="2" height="28">
            <asp:Label id="lblTitulo" runat="server" Width="200px" CssClass="TituloUsuario" Font-Bold="True">:: Cadastro de vida individual</asp:Label>&nbsp;
            </TD>
    <TD vAlign="top" align="left" width="80%" colSpan="5" height="28">
<asp:Label id="lblAcaoIndividual" runat="server" CssClass="Caminhotela"></asp:Label></TD></TR></TABLE>
<asp:Panel id="pnlCadastroTitular" runat="server" HorizontalAlign="Center" Width="100%" Wrap="False">
<TABLE class="DescricaoCampo_SEGW0065" id="Table3" height="190" cellSpacing="1" 
cellPadding="1" width="100%" align="left" border="0">
  <TR>
    <TD width="30"></TD>
    <TD align="left" width="30%">
<asp:Label id="Label1" runat="server" CssClass="DescricaoCampo">Nome:</asp:Label></TD>
    <TD align="left" width="60%">
<asp:TextBox id="txtTitNome" runat="server" Width="280px" onkeyup="RetiraAspas(event);" ></asp:TextBox>&nbsp; 
      <IMG onmouseover="return overlib('Digitar nome completo.');" onmouseout="return nd();"  
      alt="" src="../segw0060/Images/help.gif" id="img1"> 
      <DIV class="divError" id="divTxtTitNome">*</DIV></TD></TR>
  <TR>
    <TD width="30"></TD>
    <TD align="left" width="30%">
<asp:Label id="Label8" runat="server" CssClass="DescricaoCampo">CPF:</asp:Label></TD>
    <TD align="left" width="60%">
<asp:TextBox id="txtTitCPF" onkeyup="FormataCPF(document.getElementById('Form1'),'txtTitCPF',11,8,5,2,event);" runat="server"></asp:TextBox>&nbsp;
<IMG  onmouseover="return overlib('Digitar o CPF sem pontua��o e h�fen.');" onmouseout="return nd();" alt="" src="../segw0060/Images/help.gif"  id="img2"> 
      <DIV class="divError" id="divTxtTitCPF">*</DIV></TD></TR>
  <TR>
    <TD width=30></TD>
    <TD align=left width="30%">
<asp:Label id=Label9 runat="server" CssClass="DescricaoCampo">Nascimento:</asp:Label></TD>
    <TD align=left width="60%">
<asp:TextBox id=txtTitNascimento onkeyup="FormataData(document.getElementById('Form1'),'txtTitNascimento',8,2,5,event);" runat="server" Width="72px"></asp:TextBox>&nbsp;<IMG 
      onmouseover="return overlib('Digitar dia/m�s/ano, sem barras ou pontos.');" onmouseout="return nd();" alt="" 
      src="../segw0060/Images/help.gif" id="img3"> 
      <DIV class=divError id=divTxtTitNascimento>*</DIV></TD></TR>
  <TR>
    <TD width=30></TD>
    <TD align=left width="30%">
<asp:Label id=Label10 runat="server" CssClass="DescricaoCampo">Sexo:</asp:Label></TD>
    <TD align=left width="60%">
<asp:DropDownList id=ddlTitSexo runat="server">
									<asp:ListItem Value="0">Selecione</asp:ListItem>
									<asp:ListItem Value="masc.">Masculino</asp:ListItem>
									<asp:ListItem Value="fem.">Feminino</asp:ListItem>									
								</asp:DropDownList>&nbsp;<IMG 
      onmouseover="return overlib('Selecionar o sexo correspondente.');" onmouseout="return nd();" alt="" 
      src="../segw0060/Images/help.gif"  id="img4"> 
      <DIV class=divError id=divDdlTitSexo>*</DIV></TD></TR>
  <TR>
    <TD width=30></TD>
    <TD align=left width="30%">
<asp:Label id=Label11 runat="server" CssClass="DescricaoCampo">Data de Inclus�o no Seguro:</asp:Label></TD>
    <TD align=left width="60%">
<asp:TextBox id=txtTitAdmissao onkeyup="FormataData(document.getElementById('Form1'),'txtTitAdmissao',8,2,5,event);" runat="server" Width="72px"></asp:TextBox>&nbsp;<IMG 
      onmouseover="return overlib('Informar a data de inclus�o no seguro. Em caso de seguro facultativo, informar a data de ades�o no seguro.');" onmouseout="return nd();" alt="" 
      src="../segw0060/Images/help.gif"  id="img5"> 
      <DIV class=divError id=divTxtTitAdmissao>*</DIV></TD></TR>
<asp:Panel id=pnlSalario Visible="false" Runat="server">
  <TR>
    <TD width=30></TD>
    <TD align=left width="30%" ?>
<asp:Label id=Label13 runat="server" CssClass="DescricaoCampo">Sal�rio:</asp:Label>
      <DIV class=divError id="">*</DIV>
      
    <TD align=left width="60%">
<asp:TextBox id=txtTitSalario onblur="comparaValor(this,'S');" onkeyup=FormatValorContinuo(this,2) onfocus="guardaValor(this,'S');" runat="server" CssClass="text-align-right"></asp:TextBox>&nbsp;<IMG 
      onmouseover="return overlib('Informe o sal�rio nominal do proponente');" onmouseout="return nd();" alt="" 
      src="../segw0060/Images/help.gif"> 
      <DIV class=divError 
  id=divTxtTitSalario>*</DIV></TD></TR></asp:Panel>
<asp:Panel id=pnlCapital Visible="false" Runat="server">
  <TR>
    <TD width=30></TD>
    <TD noWrap align=left width="30%">
<asp:Label id=Label12 runat="server" CssClass="DescricaoCampo">Capital Segurado:</asp:Label></TD>
    <TD align=left width="60%" height=2>
<asp:TextBox id=txtTitCapital onblur="comparaValor(this,'C');" onkeyup=FormatValorContinuo(this,2) onfocus="guardaValor(this,'C');" runat="server" CssClass="text-align-right" MaxLength="13"></asp:TextBox>&nbsp;<IMG 
      onmouseover="return overlib('[Ajuda]');" onmouseout="return nd();" alt="" 
      src="../segw0060/Images/help.gif"  id="img6"> 
      <DIV class=divError id=DivTxtTitCapital>*</DIV></TD></TR></asp:Panel>
  <TR>
    <TD width=30></TD>
    <TD noWrap align=left width="30%">
<asp:Label id=Label15 runat="server" CssClass="DescricaoCampo">Exclus�o:</asp:Label></TD>
    <TD align=left width="60%" height=2>
<asp:TextBox id=txtTitDesligamento onkeyup="FormataData(document.getElementById('Form1'),'txtTitDesligamento',8,2,5,event);" runat="server" Width="72px" MaxLength="10"></asp:TextBox>&nbsp; 
<asp:Image id="img_help_desligamento" onmouseover="return overlib('Digitar a data de demiss�o ou data de t�rmino de v�nculo do funcion�rio com o estipulante (dd/mm/aaaa � sem barras e sem pontos).');" onmouseout="return nd();" runat="server" ImageUrl="../segw0060/Images/help.gif"></asp:Image></TD></TR>
  <TR>
    <TD align=center colSpan=4>
      <DIV id=confirmaCpf style="DISPLAY: none"><INPUT type=checkbox value=1 
      name=checkcpf>Confirmo estar ciente de que n�o forneci o cpf </DIV></TD></TR>
  <TR>
    <TD align=left width="20%" colSpan=4 height=5></TD></TR>
  <TR>
    <TD width=30></TD>
    <TD align=center colSpan=3 height=18>
    <table cellpadding="2" cellspacing="0" border="0">
    <tr>
		<td>
			<asp:Panel id="pnlBtnGravar" runat="server" Visible="True" CssClass="sameLine">    
				<input type="button" name="btnTitGravar" value="Gravar" id="btnTitGravar" class="Botao" onclick="validaCampos();" style="width:65px;" />	
			</asp:Panel>
		</td>				
		<td>
			<asp:Panel id="pnlTitDesligar" runat="server" Visible="True" CssClass="sameLine">    			
				<input type="button" name="btnTitDesligar" value="Gravar" id="btnTitDesligar" class="Botao" onclick="validaCampos();" style="width:65px;" />	
			</asp:Panel>
		</td>
		<td>
			<asp:Button id=btnDesfazerAlteracao runat="server" Width="229px" Visible="False" CssClass="Botao" Text="Desfazer altera��es deste proponente"></asp:Button>
			<asp:Button id=btnTitApagar runat="server" Width="66px" CssClass="Botao" Text="Apagar"></asp:Button><INPUT class=Botao id=btnCancelaTitular onclick="parent.parent.GB_reloadOnClose('true');parent.parent.top.exibeaguarde();parent.parent.GB_hide();" type=button value=Cancelar name=btnCancelaTitular Width="62px">
		</td>
	</tr>
	</table>
	</TD></TR>	
  <TR>
    <TD align=center colSpan=3></TD></TR></TABLE></asp:Panel><!-- Fim do painel de cadastro do Titular --><!-- Painel de cadastro do Dependente -->
<asp:Panel id=pnlCadastroDependente runat="server">
<TABLE id=Table1 height=142 cellSpacing=1 cellPadding=1 width="100%" border=0>
  <TR>
    <TD noWrap align=left width="20%">
<asp:Label id=Label4 runat="server" CssClass="DescricaoCampo">Nome do titular:</asp:Label></TD>
    <TD width="80%">
<asp:Label id=Label6 runat="server" CssClass="DescricaoCampo">Luiz Jos� de Souza</asp:Label></TD></TR>
  <TR>
    <TD align=left width="20%">
<asp:Label id=Label2 runat="server" CssClass="DescricaoCampo">CPF:</asp:Label></TD>
    <TD width="80%">
<asp:Label id=Label14 runat="server" CssClass="DescricaoCampo">xxx.xxx.xxx-xx</asp:Label></TD></TR>
  <TR>
    <TD align=left width="20%">
<asp:Label id=Label7 runat="server" CssClass="DescricaoCampo">Nome:</asp:Label></TD>
    <TD width="80%">
<asp:TextBox id=txtDepNome runat="server" Width="300px"></asp:TextBox>&nbsp;<IMG 
      onmouseover="return overlib('[Ajuda]');" onmouseout="return nd();" alt="" 
      src="../segw0060/Images/help.gif"></TD></TR>
  <TR>
    <TD align=left width="20%">
<asp:Label id=Label3 runat="server" CssClass="DescricaoCampo">CPF:</asp:Label></TD>
    <TD width="80%">
<asp:TextBox id=txtDepCPF onkeyup="FormataCPF(document.getElementById('Form1'),'txtDepCPF',11,8,5,2,event);" runat="server"></asp:TextBox>&nbsp;<IMG 
      onmouseover="return overlib('[Ajuda]');" onmouseout="return nd();" alt="" 
      src="../segw0060/Images/help.gif"></TD></TR>
  <TR>
    <TD align=left width="20%">
<asp:Label id=Label5 runat="server" CssClass="DescricaoCampo">Nascimento:</asp:Label></TD>
    <TD width="80%">
<asp:TextBox id=txtDepNascimento onkeyup="FormataData(document.getElementById('Form1'),'txtDepNascimento',8,2,5,event);" runat="server" Width="72px"></asp:TextBox>&nbsp;<IMG 
      onmouseover="return overlib('[Ajuda]');" onmouseout="return nd();" alt="" 
      src="../segw0060/Images/help.gif"></TD></TR>
  <TR>
    <TD noWrap align=left width="20%" height=2>
<asp:Label id=lblDepDesligamento runat="server" Visible="false" CssClass="DescricaoCampo">Desligamento:</asp:Label></TD>
    <TD align=left width="80%" height=2>
<asp:TextBox id=txtDepDesligamento onkeyup="FormataData(document.getElementById('Form1'),'txtDepDesligamento',8,2,5,event);" runat="server" Width="72px" Visible="false" MaxLength="10"></asp:TextBox>&nbsp; 
<asp:Image id=Image1 onmouseover="return overlib('[Ajuda]');" onmouseout="return nd();" runat="server" Visible="false" ImageUrl="../segw0060/Images/help.gif"></asp:Image></TD></TR>
  <TR>
    <TD align=left width="20%" colSpan=2 height=13></TD></TR>
  <TR>
    <TD align=center width="20%" colSpan=2>
    
    <table cellpadding="2" cellspacing="0" border="0">
    <tr>
		<td>
		<asp:Panel id="pnlBtnDepGravar" runat="server" Visible="false" CssClass="sameLine">    
			<input type="button" name="btnDepGravar" value="Gravar" id="btnDepGravar" class="Botao" onclick="validaCampos();" style="width:65px;" />				
		</asp:Panel>
		</td>
		<td>		
		<asp:Button id=btnDepDesligar runat="server" Width="66px" Visible="false" CssClass="Botao" Text="Excluir"></asp:Button>
		<asp:Button id=btnDepApagar runat="server" Width="66px" Visible="false" CssClass="Botao" Text="Apagar"></asp:Button>
		<asp:Button id=Button15 runat="server" Width="229px" Visible="False" CssClass="Botao" Text="Desfazer altera��es deste proponente"></asp:Button>
		<INPUT class=Botao id=btnFecharDependente onclick="parent.parent.GB_reloadOnClose('true');parent.parent.GB_hide();" type=button value=Cancelar name=btnFecharDependente >
		</td>
	</tr>
	</table>
	
	</TD></TR></TABLE></asp:Panel><!-- Fim do painel de cadastro do dependente --></asp:panel>
				<input type="hidden" value="0" name="lockValidation" id="lockValidation">
				<iframe src="blank.htm" id="processo" name="processo" width="0" frameborder="0" height="0"></iframe>
				<input type="hidden" value="<%=Request.QueryString("idTitular")%>" name="idTitular">
				<input type="hidden" value="<%=Request.QueryString("acao")%>" name="acao">
				<input type="hidden" value="<%=Request.QueryString("id")%>" name="idSolicitado">
				<input type="hidden" value="<%=Request.QueryString("acaoMsg")%>" name="acaoMsg">
				<input type="hidden" value="0" name="controleAlteracaoSalario">		
				<input type="hidden" value="-1" name="txtCpfExiste">								
				<input type="hidden" value="-1" name="txtCpfParticipouApolice">
				<input type="hidden" value="-1" name="txtExisteSinistroDataCpf"> <!--1=Existe Sinistro | 0=N�o existe -->
				<input type="hidden" value="0" name="controleAlteracaoCapital">
				<asp:TextBox id="txtSeqClienteID" runat="server" style="DISPLAY:none"></asp:TextBox>
                <asp:HiddenField ID="hdnAcesso" runat="server" />
                <asp:HiddenField ID="hdnUltCapSegurado" runat="server" />
                <asp:HiddenField ID="hdnPropClienteId" runat="server" />
                <asp:HiddenField ID="hdnPercLimite" runat="server" />
                <asp:HiddenField ID="hdnFatorMultiplicador" runat="server" />
                <asp:HiddenField ID="hdnTipoIS" runat="server" />
                <asp:HiddenField ID="hf_dt_inicio_ultima_fatura" runat="server" />
                <asp:HiddenField ID="hf_dt_fim_ultima_fatura" runat="server" />
                <asp:HiddenField ID="hf_dt_inicio_penultima_fatura" runat="server" />
                <asp:HiddenField ID="hf_dt_fim_penultima_fatura" runat="server" />
		</form>
		
		<script>

		// Demanda 7753718
		// Douglas - Confitec
		// Comentado para igualar o perfil Central de Atendimento com Administrador Alian�a		
		
		
		// Projeto 539202 - Jo�o Ribeiro - 29/04/2009
		//if (document.getElementById('hdnAcesso').value == '6') {
		    //HabilitaBotoes(0);
		//}
		// Projeto 539202 - Fim
		
		try 
		{		
			/*alert(valor.indexOf("apagar"));
			alert(validaCampos2());
			alert(document.Form1.acao.value);*/
			
			valor = parent.parent.document.getElementById('acaoClickada').value;						
			
			
			//Ajuste para ocultar imagens de ajuda quando for exclusao (s� deixar um help) 13.11.07
			if (valor == 'exclui') 
				{
				document.getElementById('img1').style.display = 'none';
				document.getElementById('img2').style.display = 'none';
				document.getElementById('img3').style.display = 'none';
				document.getElementById('img4').style.display = 'none';
				document.getElementById('img5').style.display = 'none';
				document.getElementById('img6').style.display = 'none';
				//document.getElementById('img_help_desligamento').style.display = 'block';
				
				} 
			
			if (valor.indexOf("apagar") != -1)  
				if(validaCampos2() != false)					
					document.Form1.submit();					
		}
		catch(e){ 			
		}
		</script>
		
	</body>
</HTML>
