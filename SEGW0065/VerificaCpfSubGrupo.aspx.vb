Public Partial Class VerificaCpfSubGrupo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim cpf As String = ""
        Dim msg As String

        Try
            cpf = Request("cpf")
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        If cpf <> "" Then

            If cpf.Trim.Length > 0 Then

                msg = verifCPFExisteOutroSubGrupo(cpf)

                If msg.ToString().Length > 0 Then
                    Response.Write(msg)
                Else
                    Response.Write("")
                End If

            Else
                Response.Write("")
            End If

        End If

        Response.End()
    End Sub

    Public Function verifCPFExisteOutroSubGrupo(ByVal cpf As String) As String

        Try
            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

            bd.SEGS8379_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cUtilitarios.destrataCPF(cpf))

            Dim dt As Data.DataTable

            dt = bd.ExecutaSQL_DT()
            If dt.Rows.Count > 0 Then
                Return "O segurado j� pertence ao Subgrupo " & dt.Rows(0)("sub_grupo_id") & " - " & dt.Rows(0)("nome") & ". Mesmo assim deseja continuar? Obs.: Se sim, favor informar a data de transfer�ncia e n�o a data de inclus�o no seguro."
            Else
                Return ""
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
            Return False
        End Try
    End Function

End Class