Imports System.Reflection

Partial Class MovimentacaoUmaUm_Form
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents btnFecharDependente As System.Web.UI.WebControls.Button
    'Protected WithEvents btnDepGravar As System.Web.UI.WebControls.Button
    Protected WithEvents ddlDepTipo As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Label25 As System.Web.UI.WebControls.Label
    Protected WithEvents ddlDepSexo As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Label23 As System.Web.UI.WebControls.Label
    Protected WithEvents lblAcao As System.Web.UI.WebControls.Label


    'Protected WithEvents btnTitGravar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnCancelaTitular As System.Web.UI.WebControls.Button
    'Protected WithEvents imgTitNome As System.Web.UI.HtmlControls.HtmlImage
    'Protected WithEvents imgTitCPF As System.Web.UI.HtmlControls.HtmlImage


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    'vitor.cabral - Nova Consultoria - Inicio
    Private _indPPE As Integer
    'vitor.cabral - Nova Consultoria - Fim

    Public Structure Segurado
        Public dt_entrada As String
        Public capital As String
    End Structure

    'vitor.cabral - Nova Consultoria - Inicio
    Public Property isPPE() As Integer
        Get
            Return _indPPE
        End Get
        Set(ByVal value As Integer)
            _indPPE = value
        End Set
    End Property
    'vitor.cabral - Nova Consultoria - Fim

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim requestAcao As String = Request("acao")
        Dim requestId As String = Request("id")
        Dim formata As Boolean = True
        Dim passouVerificacao As Boolean = False
        Dim idTitular As String = 0
        Session("acaoMsg") = Request("acaoMsg")

        Try

            HiddenAcaoMsg.Value = Session("acaoMsg")

            Dim linkseguro As Alianca.Seguranca.Web.LinkSeguro = New Alianca.Seguranca.Web.LinkSeguro
            Dim cAmbiente As Alianca.Seguranca.Web.ControleAmbiente
            Dim url As String

            cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
            url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
            cAmbiente.ObterAmbiente(url)
            If Alianca.Seguranca.BancoDados.cCon.configurado Then
                Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            Else
                Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            End If

            Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)

            Response.Write("<div id='dataMaxNascimento' style='display:none'>new Date(" & cUtilitarios.getIdadeMinApolice().Year.ToString & "," & cUtilitarios.getIdadeMinApolice().Month.ToString & "," & cUtilitarios.getIdadeMinApolice().Day.ToString & ")</div>")
            Response.Write("<div id='dataMinNascimento' style='display:none'>new Date(" & cUtilitarios.getIdadeMaxApolice().Year.ToString & "," & cUtilitarios.getIdadeMaxApolice().Month.ToString & "," & cUtilitarios.getIdadeMaxApolice().Day.ToString & ")</div>")
            Response.Write("<div id='intMaxNascimento' style='display:none'>" & cUtilitarios.getIntIdadeMaxApolice() & "</div>")
            Response.Write("<div id='intMinNascimento' style='display:none'>" & cUtilitarios.getIntIdadeMinApolice() & "</div>")

            If Request.QueryString("idTitular") > 0 Then
                idTitular = Request.QueryString("idTitular")
            Else
                idTitular = Request.Form("idTitular")
            End If

            If getTipoOperacaoUsuario(requestId) <> "Inclus&atilde;o" And requestAcao <> "incluirdependente" And requestAcao <> "incluir" Then
                disable(txtTitCPF)
                disable(txtDepCPF)
                disable(txtTitAdmissao)
            End If



            'pega faturas anteriores ------ pmarques (confitec) - 25/05/2010
            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
            bd.SEGS8680_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Session("usuario"))
            Dim dr As Data.SqlClient.SqlDataReader = Nothing

            Try

                dr = bd.ExecutaSQL_DR()
                If Not dr Is Nothing Then
                    If dr.Read Then



                        ''29062010 - Douglas - Confitec
                        ''FLOW 4192841 - FATURAMENTO WEB - IMPOSSIBILIDADE DE REALIZAR EXCLUS�O (Fw: FLOW WEB - IMPOSSIBILIDADE DE EXCLUIR VIDAS)

                        'Me.hf_dt_inicio_ultima_fatura.Value = Left(dr.Item("dt_inicio_ultima_fatura").ToString, 10)
                        'Me.hf_dt_fim_ultima_fatura.Value = Left(dr.Item("dt_fim_ultima_fatura").ToString, 10)
                        'Me.hf_dt_inicio_penultima_fatura.Value = Left(dr.Item("dt_inicio_penultima_fatura").ToString, 10)
                        'Me.hf_dt_fim_penultima_fatura.Value = Left(dr.Item("dt_fim_penultima_fatura").ToString, 10)


                        If Not IsDBNull(dr.Item("dt_inicio_ultima_fatura")) Then
                            Me.hf_dt_inicio_ultima_fatura.Value = dr.Item("dt_inicio_ultima_fatura")
                        Else
                            Me.hf_dt_inicio_ultima_fatura.Value = ""
                        End If

                        If Not IsDBNull(dr.Item("dt_fim_ultima_fatura")) Then
                            Me.hf_dt_fim_ultima_fatura.Value = dr.Item("dt_fim_ultima_fatura")
                        Else
                            Me.hf_dt_fim_ultima_fatura.Value = ""
                        End If

                        If Not IsDBNull(dr.Item("dt_inicio_penultima_fatura")) Then
                            Me.hf_dt_inicio_penultima_fatura.Value = dr.Item("dt_inicio_penultima_fatura")
                        Else
                            Me.hf_dt_inicio_penultima_fatura.Value = ""
                        End If

                        If Not IsDBNull(dr.Item("dt_fim_penultima_fatura")) Then
                            Me.hf_dt_fim_penultima_fatura.Value = dr.Item("dt_fim_penultima_fatura")
                        Else
                            Me.hf_dt_fim_penultima_fatura.Value = ""
                        End If

                    End If
                    If Not dr.IsClosed Then dr.Close()
                End If

            Catch ex As Exception
                Dim excp As New clsException(ex)
            End Try
            '---------------------------------------------------------

            HiddenDtInicio.Value = Session("dt_inicio_vigencia")
            HiddenDtFim.Value = Session("dt_fim_vigencia")
            HiddenAcaoOperacao.Value = "N"

            If requestAcao = "alterar" Then
                disable(txtTitCPF)
                disable(txtDepCPF)
                disable(txtDepNascimento)
                disable(txtTitNascimento)
                'vitor.cabral - Nova Consultoria - 09/01/2014 - Inicio
                'enable(txtTitCPF)
                'enable(txtTitNascimento)
                'enable(txtTitAdmissao)
                'vitor.cabral - Nova Consultoria - 09/01/2014 - Fim

                Try
                    getDadosUsuario(requestId)
                Catch ex As Exception
                    Dim excp As New clsException(ex)
                End Try

                Me.hdnPercLimite.Value = getLimiteAlteracaoVida().ToString().Replace(",", ".")
                'Me.hdnUltCapSegurado.Value = getCapitalSeguradoUltimoFaturamento().ToString().Replace(",", ".")
                Me.hdnFatorMultiplicador.Value = Me.getDtDadosSubGrupo().Rows(0)("qtd_salarios").ToString().Replace(",", ".")
                Me.hdnTipoIS.Value = getCapitalSeguradoSession()


                lblAcaoIndividual.Text = "- Alterar"
                pnlCadastroDependente.Visible = False
                pnlCadastro.Visible = True
                pnlCadastroTitular.Visible = True
                txtTitDesligamento.Visible = False
                Label15.Visible = False
                img_help_desligamento.Visible = False
                pnlTitDesligar.Visible = False
                btnTitApagar.Visible = False

                btnTitApagar.Visible = False
                btnDepApagar.Visible = False
                btnTitApagar.Visible = False

            ElseIf requestAcao = "incluirdependente" Then

                lblAcaoIndividual.Text = "- Incluir C�njuge"
                pnlCadastro.Visible = True
                pnlCadastroDependente.Visible = True
                pnlCadastroTitular.Visible = False
                txtTitAdmissao.Text = ""
                txtTitCapital.Text = ""
                txtTitCPF.Text = ""
                txtTitNascimento.Text = ""
                txtTitNome.Text = ""
                txtTitSalario.Text = ""
                ddlTitSexo.SelectedValue = "0"

                btnTitApagar.Visible = False
                btnDepApagar.Visible = False
                pnlTitDesligar.Visible = False
                btnTitApagar.Visible = False
                Me.pnlBtnDepGravar.Visible = True

            ElseIf requestAcao = "alterardependente" Then
                disable(txtTitCPF)
                disable(txtDepCPF)
                disable(txtDepNascimento)
                disable(txtTitNascimento)

                Try
                    getDadosConjuge(requestId, idTitular)
                Catch ex As Exception
                    Dim excp As New clsException(ex)
                End Try

                lblAcaoIndividual.Text = "- Alterar C�njuge"
                pnlCadastro.Visible = True
                pnlCadastroDependente.Visible = True
                pnlCadastroTitular.Visible = False
                txtTitAdmissao.Text = ""
                txtTitCapital.Text = ""
                txtTitCPF.Text = ""
                txtTitNascimento.Text = ""
                txtTitNome.Text = ""
                txtTitSalario.Text = ""
                ddlTitSexo.SelectedValue = "0"

                btnTitApagar.Visible = False
                btnDepApagar.Visible = False
                pnlTitDesligar.Visible = False
                btnTitApagar.Visible = False
                Me.pnlBtnDepGravar.Visible = True

            ElseIf requestAcao = "excluirdependente" Then

                Try
                    getDadosConjuge(requestId, idTitular)
                Catch ex As Exception
                    Dim excp As New clsException(ex)
                End Try

                lblAcaoIndividual.Text = "- Excluir C�njuge"
                pnlCadastro.Visible = True
                pnlCadastroDependente.Visible = True
                pnlCadastroTitular.Visible = False
                txtTitAdmissao.Text = ""
                txtTitCapital.Text = ""
                txtTitCPF.Text = ""
                txtTitNascimento.Text = ""
                txtTitNome.Text = ""

                txtTitSalario.Visible = False
                Me.pnlSalario.Visible = False

                ddlTitSexo.SelectedValue = "0"
                txtDepDesligamento.Enabled = True
                txtDepDesligamento.Visible = True
                txtDepDesligamento.Text = ""
                lblDepDesligamento.Visible = True

                disable(txtDepCPF)
                disable(txtDepNome)
                disable(txtDepNascimento)
                pnlBtnGravar.Visible = False

                If getTipoOperacaoUsuario(requestId) = "Inclus&atilde;o" Then
                    btnDepDesligar.Visible = False
                    btnDepApagar.Visible = True
                    Me.txtDepDesligamento.Visible = False
                    Me.txtTitDesligamento.Visible = False
                    Me.lblDepDesligamento.Visible = False
                    Me.Label15.Visible = False
                    img_help_desligamento.Visible = False

                Else
                    btnDepApagar.Visible = False
                    btnDepDesligar.Visible = True

                End If

                Me.btnDepApagar.Attributes.Add("onclick", "this.onclick = function(){return false;};")
                Me.btnDepDesligar.Attributes.Add("onclick", "this.onclick = function(){return false;};")

            ElseIf requestAcao = "excluir" Then
                Try
                    getDadosUsuario(requestId)
                Catch exA As Exception
                    Dim excp As New clsException(exA)
                End Try

                Try
                    Dim seg As Segurado = Me.getDadosSegurado(requestId)
                    cUtilitarios.escreveScript("var dt_entrada = '" + seg.dt_entrada + "'")
                Catch exB As Exception
                    Dim excp As New clsException(exB)
                End Try

                Try
                    getPeriodoApolice()
                Catch exC As Exception
                    Dim excp As New clsException(exC)
                End Try

                lblAcaoIndividual.Text = "- Excluir"
                pnlCadastroDependente.Visible = False
                pnlCadastro.Visible = True
                pnlCadastroTitular.Visible = True
                disable(txtTitAdmissao)
                disable(txtTitCapital)
                disable(txtTitCPF)
                disable(txtTitNascimento)
                disable(txtTitNome)

                disable(txtTitSalario)
                txtTitSalario.Visible = False
                Me.pnlSalario.Visible = False

                ddlTitSexo.Enabled = False
                '----Exibe campo Data Desligamento e botoes
                txtTitDesligamento.Visible = True
                Label15.Visible = True
                img_help_desligamento.Visible = True

                Me.btnTitApagar.Attributes.Add("onclick", "this.onclick = function(){return false;};")

                pnlTitDesligar.Visible = True
                btnTitApagar.Visible = True
                pnlBtnGravar.Visible = False

                formata = False
                Try
                    If getTipoOperacaoUsuario(requestId).IndexOf("Agendada") = -1 And getTipoOperacaoUsuario(requestId) <> "Inclus&atilde;o" And getTipoOperacaoUsuario(requestId) <> "Pendente" Then
                        pnlTitDesligar.Visible = True
                        btnTitApagar.Visible = False
                    ElseIf getTipoOperacaoUsuario(requestId).IndexOf("Agendada") = -1 Then
                        btnTitApagar.Visible = False
                        pnlTitDesligar.Visible = True
                        Me.txtDepDesligamento.Visible = False
                        Me.txtTitDesligamento.Visible = True
                        Me.lblDepDesligamento.Visible = True
                        Me.Label15.Visible = True
                    Else
                        btnTitApagar.Visible = True
                        pnlTitDesligar.Visible = False
                        Me.txtDepDesligamento.Visible = True
                        Me.txtTitDesligamento.Visible = True
                        Me.lblDepDesligamento.Visible = False
                        Me.Label15.Visible = False

                        Me.txtDepDesligamento.Text = getDtDesligamentoAgendado(requestId)
                        Me.txtTitDesligamento.Text = getDtDesligamentoAgendado(requestId)
                    End If
                Catch ex As Exception
                    Dim excp As New clsException(ex)
                End Try

            Else
                lblAcaoIndividual.Text = "- Incluir"
                pnlCadastro.Visible = True
                pnlCadastroDependente.Visible = False
                pnlCadastroTitular.Visible = True
                If Request.Form("btnTitGravar") = "" Then
                    If Not Page.IsPostBack Then
                        txtTitAdmissao.Text = ""
                        txtTitCapital.Text = "0,00"
                        txtTitCPF.Text = ""
                        txtTitNascimento.Text = ""
                        txtTitNome.Text = ""
                        txtTitSalario.Text = ""
                        ddlTitSexo.SelectedValue = "0"
                        Me.txtTitCapital.Text = ""
                    End If

                End If
                txtTitDesligamento.Visible = False
                Label15.Visible = False
                img_help_desligamento.Visible = False
                pnlTitDesligar.Visible = False
                btnTitApagar.Visible = False
                lblDepDesligamento.Visible = False

            End If

            If cUtilitarios.MostraCapitalFaixaEtaria() Then
                txtTitAdmissao.Attributes.Add("onblur", "getCapitalFaixaEtaria()")
            End If

            Try
                If requestAcao.IndexOf("dependente") <> -1 Then
                    Label14.Text = getCpfTitular(idTitular)
                    Label6.Text = getNomeTitular(idTitular)
                End If
            Catch ex As Exception
                Dim excp As New clsException(ex)
            End Try

            If Page.IsPostBack Then

                Dim tpCapitalSegurado As String = ""
                Dim valorCapitalSegurado As Double = 0
                Try
                    valorCapitalSegurado = IIf(Request.Form("txtTitCapital") = "", "0", Request.Form("txtTitCapital"))
                Catch ex As Exception
                    Dim excp As New clsException(ex)
                End Try

                Dim strSalario As String = IIf(Request.Form("txtTitSalario") = "", "0", Request.Form("txtTitSalario"))
                Dim valorSalario As String = 0
                Try
                    valorSalario = strSalario.Replace(".", "")
                Catch ex As Exception
                    Dim excp As New clsException(ex)
                End Try

                tpCapitalSegurado = getCapitalSeguradoSession()

                If tpCapitalSegurado = "M�ltiplo sal�rio" Then
                    valorCapitalSegurado = getCapitalBySalario(valorSalario)
                ElseIf tpCapitalSegurado = "Capital por faixa et�ria" Then
                    valorCapitalSegurado = IIf(Request.Form("txtTitCapital") = "" Or Request.Form("txtTitCapital") = "---", "10000", Request.Form("txtTitCapital"))
                ElseIf tpCapitalSegurado = "Capital Global" Then
                    Dim param() As String = getCondicoesSubgrupo()
                    valorCapitalSegurado = param(6)
                Else
                    valorCapitalSegurado = IIf(Request.Form("txtTitCapital") = "", "0", Request.Form("txtTitCapital"))
                End If

                Dim sTpIsSubGrupo As String = FillTpCapitalSegurado(tpCapitalSegurado)
                Dim NomeSegurado As String = ""

                If Request.Form("acao") = "incluir" Then

                    Try
                        Dim blnPassouSubGrupo As Boolean = True

                        Dim param() As String = getCondicoesSubgrupo()
                        Dim dt() As String = Request.Form("txtTitNascimento").Split("/")

                        If param(0).Trim = "---" Then
                            param(0) = 0
                        End If

                        If param(1).Trim = "---" Then
                            param(1) = 0
                        End If

                        

                        Dim val_capital As String = ""
                        val_capital = getValCapital(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

                        If getCapitalSeguradoSession() = "Capital fixo" Then
                            valorCapitalSegurado = val_capital
                            blnPassouSubGrupo = VerificarDataSubGrupo()
                        ElseIf getCapitalSeguradoSession() = "Capital Global" Then
                            valorCapitalSegurado = val_capital
                        ElseIf getCapitalSeguradoSession() = "M�ltiplo sal�rio" Then
                            If Not VerificarDataSubGrupo() Then
                                blnPassouSubGrupo = False
                                Exit Sub
                            Else
                                If valorCapitalSegurado > cUtilitarios.getLimMaxCapital Then
                                    valorCapitalSegurado = cUtilitarios.getLimMaxCapital
                                    cUtilitarios.br("Capital Segurado acima do limite permitido para o subgrupo. \nSer� considerado o capital de acordo com as condi��es da ap�lice.")
                                End If

                                If valorCapitalSegurado < cUtilitarios.getLimMinCapital Then
                                    valorCapitalSegurado = cUtilitarios.getLimMinCapital
1:                                  cUtilitarios.br("Capital Segurado abaixo do limite permitido para o subgrupo. \nSer� considerado o capital de acordo com as condi��es da ap�lice.")
                                End If

                                passouVerificacao = True
                            End If

                        End If

                        'FLOW 404720 - Marcelo Ferreira - Confitec Sistemas - 2008-06-09
                        'Rotina para c�lculo da idade utilizando a data de nascimento e data de inclus�o no seguro.
                        '---------------------------------------------------------------------------------------------------------------------
                        Dim iIdade As Integer = 0
                        iIdade = DateDiff(DateInterval.Year, CDate(Request.Form("txtTitNascimento")), CDate(Request.Form("txtTitAdmissao")))

                        If Month(CDate(Request.Form("txtTitNascimento"))) > Month(CDate(Request.Form("txtTitAdmissao"))) Then
                            iIdade -= 1
                        Else
                            If Month(CDate(Request.Form("txtTitNascimento"))) = Month(CDate(Request.Form("txtTitAdmissao"))) Then
                                If Day(CDate(Request.Form("txtTitNascimento"))) > Day(CDate(Request.Form("txtTitAdmissao"))) Then
                                    iIdade -= 1
                                End If
                            End If
                        End If

                        '23122010 - ercosta
                        Dim v_bSeguradoExistente As Boolean = Me.UsuarioExiste(cUtilitarios.destrataCPF(Request.Form("txtTitCPF")), Session("apolice"), Session("ramo"), Session("subgrupo_id"))

                        'FLOW 404720 - Marcelo Ferreira - Confitec Sistemas - 2008-06-09
                        'Substituindo c�lculo da idade por DATEDIFF pela vari�vel iIdade
                        'Dim x As Integer = ValidarDetalhe(sTpIsSubGrupo, Request.Form("txtTitAdmissao"), getdataApolice(), valorCapitalSegurado, Request.Form("txtTitSalario"), 1, Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "i", Request.Form("txtTitNascimento"), DateDiff(DateInterval.Year, New Date(dt(2), dt(1), dt(0)), Now), Now, "", cUtilitarios.destrataCPF(Request.Form("txtTitCPF")), CType(Request.Form("ddlTitSexo"), String).Substring(0, 1), val_capital, 0, 0, param(0), param(1), Request.Form("txtTitAdmissao"))
                        '---------------------------------------------------------------------------------------------------------------------
                        Dim x As Integer = ValidarDetalhe(sTpIsSubGrupo, Request.Form("txtTitAdmissao"), getdataApolice(), valorCapitalSegurado, Request.Form("txtTitSalario"), 1, Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "i", Request.Form("txtTitNascimento"), iIdade, Now, "", cUtilitarios.destrataCPF(Request.Form("txtTitCPF")), CType(Request.Form("ddlTitSexo"), String).Substring(0, 1), val_capital, 0, 0, Convert.ToInt32(param(0)), Convert.ToInt32(param(1)), Request.Form("txtTitAdmissao"), , , , , , v_bSeguradoExistente)

                        If x <> 0 And ((x = 7 Or x = 8) And Not passouVerificacao) Then
                            cUtilitarios.br(cUtilitarios.deParaDLL(x, param(0), param(1), Request.Form("txtTitCapital"), DateDiff(DateInterval.Year, New Date(dt(2), dt(1), dt(0)), Now), Request.Form("txtTitNome")))
                            Session("valid") = "1"
                        Else

                            If blnPassouSubGrupo AndAlso VerificarIdadeMaxMin(Convert.ToDouble(param(1)), Convert.ToDouble(param(0)), v_bSeguradoExistente) Then
                                NomeSegurado = Request.Form("txtTitNome").Replace("'", "�")
                                Try
                                    'vitor.cabral - Nova Consultoria - 08/01/2014 - Inicio
                                    mInsereSeguradoTemporario(Session("apolice"), Session("ramo"), Session("subGrupo_id"), NomeSegurado, Request.Form("txtTitCPF"), Request.Form("txtTitNascimento"), Request.Form("txtTitAdmissao"), Request.Form("ddlTitSexo"), valorCapitalSegurado, valorSalario, getOperacao())
                                    'vitor.cabral - Nova Consultoria - 08/01/2014 - Fim
                                Catch ex As Exception
                                    Dim excp As New clsException(ex)
                                End Try
                            End If
                        End If
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try

                End If

                If Request.Form("acao") = "alterardependente" Then
                    Dim retorno As String = ""

                    Dim param() As String = getCondicoesSubgrupo()
                    Dim dt() As String = Request.Form("txtDepNascimento").Split("/")

                    If param(0).Trim = "---" Then
                        param(0) = 0
                    End If
                    If param(1).Trim = "---" Then
                        param(1) = 0
                    End If

                    'Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
                    'Dim dr As Data.SqlClient.SqlDataReader = Nothing
                    Try
                        bd.getTpApoliceConjuge(Session("apolice"), Session("ramo"), Session("subGrupo_id"))
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try

                    Try
                        dr = bd.ExecutaSQL_DR()

                        If Not dr Is Nothing Then
                            If dr.Read() Then
                                retorno = dr.GetValue(0).ToString()
                            End If
                            If Not dr.IsClosed Then dr.Close()
                        End If
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try

                    Dim sexo As String = getSexoConjuge(idTitular)

                    Dim capital_segurado As String = Me.getCapitalConjuge(Session("apolice"), Session("ramo"), Session("subGrupo_id"), idTitular)

                    If getCapitalSeguradoSession() = "M�ltiplo sal�rio" Then
                        If capital_segurado > cUtilitarios.getLimMaxCapital Then
                            capital_segurado = cUtilitarios.getLimMaxCapital
                        End If

                        If capital_segurado < cUtilitarios.getLimMinCapital Then
                            capital_segurado = cUtilitarios.getLimMinCapital
                        End If

                        passouVerificacao = True
                    End If

                    Dim x As Integer = ValidarDetalhe(sTpIsSubGrupo, Now, getdataApolice(), "0", 0, 1, Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "a", Request.Form("txtDepNascimento"), DateDiff(DateInterval.Year, New Date(dt(2), dt(1), dt(0)), Now), Now, "", cUtilitarios.destrataCPF(Request.Form("txtDepCPF")), sexo, param(5), 0, 0, CInt(param(0)), CInt(param(1)), Now, , , Now, retorno)
                    If x <> 0 And ((x = 7 Or x = 8) And Not passouVerificacao) Then
                        Try
                            cUtilitarios.br(cUtilitarios.deParaDLL(x, param(0), param(1), valorCapitalSegurado, DateDiff(DateInterval.Year, New Date(dt(2), dt(1), dt(0)), Now), Request.Form("txtTitNome")))
                        Catch ex As Exception
                            Dim excp As New clsException(ex)
                        End Try
                    Else
                        NomeSegurado = Request.Form("txtDepNome").Replace("'", "�")
                        Try
                            atualizaSegurado(Request.Form("idSolicitado"), Session("apolice"), Session("ramo"), Session("subGrupo_id"), NomeSegurado, Request.Form("txtDepCPF"), Request.Form("txtDepNascimento"), "null", sexo, capital_segurado, "null", "0")
                            'vitor.cabral - Nova Consultoria - 09/01/2014 - Inicio
                            'mInsereSeguradoTemporario(Session("apolice"), Session("ramo"), Session("subGrupo_id"), NomeSegurado, Request.Form("txtTitCPF"), Request.Form("txtTitNascimento"), Request.Form("txtTitAdmissao"), Request.Form("ddlTitSexo"), valorCapitalSegurado, valorSalario, getOperacao())
                            'mInsereSeguradoPPE(Session("ramo"), Session("apolice"), Session("subGrupo_id"), Request.Form("txtTitCPF"), 1, Request.Form("txtTitAdmissao"), Session("usuario"), getOperacao())
                            'If isPPE <> Nothing And isPPE > 0 Then
                            '    cUtilitarios.br("Vida alterada com restri��o de PPE(Pessoa Politicamente Exposta).\nSomente uma pessoa autorizada, poder� aceitar essa altera��o.")
                            'End If
                            'vitor.cabral - Nova Consultoria - 09/01/2014 - Fim
                        Catch ex As Exception
                            Dim excp As New clsException(ex)
                        End Try
                    End If

                End If

                If Request.Form("acao") = "alterar" Then

                    Dim param() As String = getCondicoesSubgrupo()
                    Dim dt() As String = Request.Form("txtTitNascimento").Split("/")

                    If param(0).Trim = "---" Then
                        param(0) = 0
                    End If

                    If param(1).Trim = "---" Then
                        param(1) = 0
                    End If

                    If param(5).Trim = "---" Then
                        param(5) = 0
                    End If

                    Dim val_capital As String = getValCapital(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
                    If getCapitalSeguradoSession() = "Capital fixo" Or getCapitalSeguradoSession() = "Capital Global" Then
                        valorCapitalSegurado = val_capital
                    ElseIf getCapitalSeguradoSession() = "M�ltiplo sal�rio" Then
                        If valorCapitalSegurado > cUtilitarios.getLimMaxCapital Then
                            valorCapitalSegurado = cUtilitarios.getLimMaxCapital
                        End If

                        If valorCapitalSegurado < cUtilitarios.getLimMinCapital Then
                            valorCapitalSegurado = cUtilitarios.getLimMinCapital
                        End If
                    End If

                    Dim x As Integer = ValidarDetalhe(sTpIsSubGrupo, Request.Form("txtTitAdmissao"), getdataApolice(), valorCapitalSegurado, Request.Form("txtTitSalario"), 1, Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "a", Request.Form("txtTitNascimento"), DateDiff(DateInterval.Year, New Date(dt(2), dt(1), dt(0)), Now), Now, "", cUtilitarios.destrataCPF(Request.Form("txtTitCPF")), CType(Request.Form("ddlTitSexo"), String).Substring(0, 1), param(5), 0, 0, CInt(param(0)), CInt(param(1)), Request.Form("txtTitAdmissao"))
                    If x <> 0 Then
                        cUtilitarios.br(cUtilitarios.deParaDLL(x, param(0), param(1), valorCapitalSegurado, DateDiff(DateInterval.Year, New Date(dt(2), dt(1), dt(0)), Now), Request.Form("txtTitNome")))
                    Else
                        NomeSegurado = Request.Form("txtTitNome").Replace("'", "�")
                        Try
                            atualizaSegurado(Request.Form("idSolicitado"), Session("apolice"), Session("ramo"), Session("subGrupo_id"), NomeSegurado, Request.Form("txtTitCPF"), Request.Form("txtTitNascimento"), Request.Form("txtTitAdmissao"), Request.Form("ddlTitSexo"), valorCapitalSegurado, valorSalario, "0")
                            'vitor.cabral - Nova Consultoria - 09/01/2014 - Inicio
                            'mInsereSeguradoTemporario(Session("apolice"), Session("ramo"), Session("subGrupo_id"), NomeSegurado, Request.Form("txtTitCPF"), Request.Form("txtTitNascimento"), Request.Form("txtTitAdmissao"), Request.Form("ddlTitSexo"), valorCapitalSegurado, valorSalario, getOperacao())
                            'mInsereSeguradoPPE(Session("ramo"), Session("apolice"), Session("subGrupo_id"), Request.Form("txtTitCPF"), 1, Request.Form("txtTitAdmissao"), Session("usuario"), getOperacao())
                            'If isPPE <> Nothing And isPPE > 0 Then
                            '    cUtilitarios.br("Vida alterada com restri��o de PPE(Pessoa Politicamente Exposta).\nSomente uma pessoa autorizada, poder� aceitar essa altera��o.")
                            'End If
                            'vitor.cabral - Nova Consultoria - 09/01/2014 - Fim
                        Catch ex As Exception
                            Dim excp As New clsException(ex)
                        End Try
                    End If

                End If

                If Request.Form("acao") = "incluirdependente" Then

                    Dim param() As String = getCondicoesSubgrupo()
                    Dim dt() As String = Request.Form("txtDepNascimento").Split("/")

                    If param(0).Trim = "---" Then
                        param(0) = 0
                    End If
                    If param(1).Trim = "---" Then
                        param(1) = 0
                    End If
                    If param(5).Trim = "---" Then
                        param(5) = 0
                    End If

                    'Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

                    bd.getTpApoliceConjuge(Session("apolice"), Session("ramo"), Session("subGrupo_id"))
                    'Dim dr As Data.SqlClient.SqlDataReader = Nothing
                    Dim retorno As String = ""
                    Dim sexo As String = ""

                    Try
                        dr = bd.ExecutaSQL_DR()
                        If Not dr Is Nothing Then
                            If dr.Read() Then
                                retorno = dr.GetValue(0).ToString()
                            End If
                            If Not dr.IsClosed Then dr.Close()
                            dr = Nothing
                        End If
                        sexo = getSexoConjuge(idTitular)

                        Dim capital_segurado As String = Me.getCapitalConjuge(Session("apolice"), Session("ramo"), Session("subGrupo_id"), idTitular)

                        If getCapitalSeguradoSession() = "M�ltiplo sal�rio" Then
                            If valorCapitalSegurado > cUtilitarios.getLimMaxCapital Then
                                valorCapitalSegurado = cUtilitarios.getLimMaxCapital
                                cUtilitarios.br("Capital Segurado acima do limite permitido para o subgrupo. \nSer� considerado o capital de acordo com as condi��es da ap�lice.")
                            End If

                            If valorCapitalSegurado < cUtilitarios.getLimMinCapital Then
                                valorCapitalSegurado = cUtilitarios.getLimMinCapital
                                cUtilitarios.br("Capital Segurado abaixo do limite permitido para o subgrupo. \nSer� considerado o capital de acordo com as condi��es da ap�lice.")
                            End If

                            passouVerificacao = True
                        End If

                        Dim x As Integer = ValidarDetalhe(sTpIsSubGrupo, Now, getdataApolice(), "0", 0, 3, Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "i", Request.Form("txtDepNascimento"), DateDiff(DateInterval.Year, New Date(dt(2), dt(1), dt(0)), Now), Now, "", cUtilitarios.destrataCPF(Request.Form("txtDepCPF")), sexo, param(5), 0, 0, CInt(param(0)), CInt(param(1)), Now, , getSeqClienteID(idTitular), Now, retorno)
                        If x <> 0 And ((x = 7 Or x = 8) And Not passouVerificacao) Then
                            cUtilitarios.br(cUtilitarios.deParaDLL(x, param(0), param(1), valorCapitalSegurado, DateDiff(DateInterval.Year, New Date(dt(2), dt(1), dt(0)), Now), Request.Form("txtTitNome")))
                        Else
                            NomeSegurado = Request.Form("txtDepNome").Replace("'", "�")
                            Try
                                mInsereSeguradoConjuge(Session("apolice"), Session("ramo"), Session("subGrupo_id"), NomeSegurado, Request.Form("txtDepCPF"), Request.Form("txtDepNascimento"), "null", sexo, capital_segurado, "null", Request.Form("idSolicitado"))
                                'vitor.cabral - Nova Consultoria - 08/01/2014 - Inicio
                                mInsereSeguradoTemporario(Session("apolice"), Session("ramo"), Session("subGrupo_id"), NomeSegurado, Request.Form("txtTitCPF"), Request.Form("txtTitNascimento"), Request.Form("txtTitAdmissao"), Request.Form("ddlTitSexo"), valorCapitalSegurado, valorSalario, getOperacao())
                                'vitor.cabral - Nova Consultoria - 08/01/2014 - Fim
                            Catch ex As Exception
                                Dim excp As New clsException(ex)
                            End Try
                        End If
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try
                End If

                If Request.Form("acao") = "excluir" Or Request.Form("acao") = "excluirdependente" Then
                    Dim dtDesligamento As String

                    dtDesligamento = IIf(Request.Form("acao") = "excluir", Request.Form("txtTitDesligamento"), Request.Form("txtDepDesligamento"))

                    Try
                        mDeleteRegistro(Request.Form("idSolicitado"), dtDesligamento)
                        'vitor.cabral - Nova Consultoria - 08/01/2014 - Inicio
                        'mInsereSeguradoTemporario(Session("apolice"), Session("ramo"), Session("subGrupo_id"), NomeSegurado, Request.Form("txtTitCPF"), Request.Form("txtTitNascimento"), Request.Form("txtTitAdmissao"), Request.Form("ddlTitSexo"), valorCapitalSegurado, valorSalario, getOperacao())
                        'mInsereSeguradoPPE(Session("ramo"), Session("apolice"), Session("subGrupo_id"), Request.Form("txtTitCPF"), 1, Request.Form("txtTitAdmissao"), Session("usuario"), getOperacao())
                        'If isPPE <> Nothing And isPPE > 0 Then
                        '    cUtilitarios.br("Vida excluida com restri��o de PPE(Pessoa Politicamente Exposta).\nSomente uma pessoa autorizada, poder� aceitar essa exclus�o.")
                        'End If
                        'vitor.cabral - Nova Consultoria - 08/01/2014 - Fim
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try
                End If

            End If

            If formata = True Then
                If requestAcao <> "alterardependente" Then
                    formaSalarioCapital()
                Else
                    pnlCapital.Visible = False
                    pnlSalario.Visible = False
                End If

            Else
                If requestAcao = "excluir" Or requestAcao = "excluirdependente" Then
                    If getCapitalSeguradoSession() = "Capital informado" Then
                        pnlSalario.Visible = False
                    End If
                Else
                    pnlSalario.Visible = True
                End If
                pnlCapital.Visible = True
                disable(txtTitSalario)
                disable(txtTitCapital)
            End If

            Try
                If Request.Form("postExterno") = 1 Then

                    Dim tpCapitalSegurado As String

                    Dim valorCapitalSegurado As Double = IIf(Request.Form("txtTitCapital") = "", "0", Request.Form("txtTitCapital"))
                    Dim strSalario As String = IIf(Request.Form("txtTitSalario") = "", "0", Request.Form("txtTitSalario"))
                    Dim valorSalario As String = 0

                    Try
                        valorSalario = strSalario.Replace(".", "")
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try

                    tpCapitalSegurado = getCapitalSeguradoSession()

                    If tpCapitalSegurado = "M�ltiplo sal�rio" Then
                        valorCapitalSegurado = getCapitalBySalario(valorSalario)
                    Else
                        valorCapitalSegurado = IIf(Request.Form("txtTitCapital") = "", "0", Request.Form("txtTitCapital"))
                    End If

                    Dim sTpIsSubGrupo As String = FillTpCapitalSegurado(tpCapitalSegurado)

                    If Request.Form("acao") = "alterardependente" Then
                        Dim retorno As String = ""

                        Dim param() As String = getCondicoesSubgrupo()
                        Dim dt() As String = Request.Form("txtDepNascimento").Split("/")

                        If param(0).Trim = "---" Then
                            param(0) = 0
                        End If
                        If param(1).Trim = "---" Then
                            param(1) = 0
                        End If

                        If param(5).Trim = "---" Then
                            param(5) = 0
                        End If

                        'Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

                        bd.getTpApoliceConjuge(Session("apolice"), Session("ramo"), Session("subGrupo_id"))
                        'Dim dr As Data.SqlClient.SqlDataReader = Nothing
                        Try
                            dr = bd.ExecutaSQL_DR()
                            If Not dr Is Nothing Then
                                If dr.Read() Then
                                    retorno = dr.GetValue(0).ToString()
                                End If
                                If Not dr.IsClosed Then dr.Close()
                                dr = Nothing
                            End If
                        Catch ex As Exception
                            Dim excp As New clsException(ex)
                        End Try

                        Dim sexo As String = getSexoConjuge(idTitular)

                        Dim capital_segurado As String = Me.getCapitalConjuge(Session("apolice"), Session("ramo"), Session("subGrupo_id"), idTitular)

                        If getCapitalSeguradoSession() = "M�ltiplo sal�rio" Then
                            If valorCapitalSegurado > cUtilitarios.getLimMaxCapital Then
                                valorCapitalSegurado = cUtilitarios.getLimMaxCapital
                            End If

                            If valorCapitalSegurado < cUtilitarios.getLimMinCapital Then
                                valorCapitalSegurado = cUtilitarios.getLimMinCapital
                            End If

                            passouVerificacao = True
                        End If

                        Dim x As Integer = ValidarDetalhe(sTpIsSubGrupo, Now, getdataApolice(), "0", 0, 1, Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "a", Request.Form("txtDepNascimento"), DateDiff(DateInterval.Year, New Date(dt(2), dt(1), dt(0)), Now), Now, "", cUtilitarios.destrataCPF(Request.Form("txtDepCPF")), sexo, param(5), 0, 0, CInt(param(0)), CInt(param(1)), Now, , , Now, retorno)
                        If x <> 0 And ((x = 7 Or x = 8) And Not passouVerificacao) Then
                            cUtilitarios.br(cUtilitarios.deParaDLL(x, param(0), param(1), valorCapitalSegurado, DateDiff(DateInterval.Year, New Date(dt(2), dt(1), dt(0)), Now), Request.Form("txtTitNome")))
                        Else
                            Try
                                atualizaSegurado(Request.Form("idSolicitado"), Session("apolice"), Session("ramo"), Session("subGrupo_id"), Request.Form("txtDepNome"), Request.Form("txtDepCPF"), Request.Form("txtDepNascimento"), "null", sexo, capital_segurado, "null", "0")
                                'vitor.cabral - Nova Consultoria - 09/01/2014 - Inicio
                                'mInsereSeguradoPPE(Session("ramo"), Session("apolice"), Session("subGrupo_id"), Request.Form("txtTitCPF"), 1, Request.Form("txtTitAdmissao"), Session("usuario"), getOperacao())
                                'If isPPE <> Nothing And isPPE > 0 Then
                                '    cUtilitarios.br("Vida incluida com restri��o de PPE(Pessoa Politicamente Exposta).\nSomente uma pessoa autorizada, poder� aceitar essa inclus�o.")
                                'End If
                                'vitor.cabral - Nova Consultoria - 09/01/2014 - Fim
                            Catch ex As Exception
                                Dim excp As New clsException(ex)
                            End Try
                        End If

                    ElseIf Request.Form("acao") = "alterar" Then

                        Dim altSal As String = Request.Form("controleAlteracaoSalario")
                        Dim altCap As String = Request.Form("controleAlteracaoCapital")

                        Dim alteracoes As String

                        If altSal = 1 Or altCap = 1 Then
                            alteracoes = 1
                        Else
                            alteracoes = 0
                        End If

                        Dim param() As String = getCondicoesSubgrupo()
                        Dim dt() As String = Request.Form("txtTitNascimento").Split("/")

                        If param(0).Trim = "---" Then
                            param(0) = 0
                        End If
                        If param(1).Trim = "---" Then
                            param(1) = 0
                        End If

                        If param(5).Trim = "---" Then
                            param(5) = 0
                        End If

                        'Inicializa vari�veis
                        Dim valCapitalAnterior As Double = Me.getCapitalSeguradoUsuario(Request.Form("idSolicitado"))
                        Dim valCapitalNovo As Double = valorCapitalSegurado
                        Dim valLimMinApolice As Double = cUtilitarios.getLimMinCapital
                        Dim valLimMaxApolice As Double = cUtilitarios.getLimMaxCapital

                        Select Case getCapitalSeguradoSession()

                            Case "Capital fixo", "Capital Global"
                                'Flow 580891 - dgomes - CONFITEC - 09/12/2008
                                ' valorCapitalSegurado = getValCapital(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
                                valorCapitalSegurado = getValCapitalParaAlteracao(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Request.Form("idSolicitado"))
                                ''
                                valCapitalNovo = valorCapitalSegurado

                            Case "M�ltiplo sal�rio"

                                If valCapitalNovo <= valLimMinApolice Then
                                    valCapitalNovo = valLimMinApolice
                                ElseIf valCapitalNovo >= valLimMaxApolice Then
                                    valCapitalNovo = valLimMaxApolice
                                End If

                                passouVerificacao = True

                        End Select

                        Dim x As Integer = ValidarDetalhe(sTpIsSubGrupo, Request.Form("txtTitAdmissao"), getdataApolice(), valCapitalNovo, Request.Form("txtTitSalario"), 1, Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "a", Request.Form("txtTitNascimento"), DateDiff(DateInterval.Year, New Date(dt(2), dt(1), dt(0)), Now), Now, "", cUtilitarios.destrataCPF(Request.Form("txtTitCPF")), CType(Request.Form("ddlTitSexo"), String).Substring(0, 1), param(5), 0, 0, CInt(param(0)), CInt(param(1)), Request.Form("txtTitAdmissao"))
                        If x <> 0 And ((x = 7 Or x = 8) And Not passouVerificacao) Then
                            cUtilitarios.br(cUtilitarios.deParaDLL(x, param(0), param(1), valCapitalNovo, DateDiff(DateInterval.Year, New Date(dt(2), dt(1), dt(0)), Now), Request.Form("txtTitNome")))
                        Else

                            'ALTERA��O DA VIDA
                            'N�o marca a vida como altera��o, inicialmente
                            alteracoes = "0"

                            If valCapitalAnterior <= valLimMinApolice Then

                                If valCapitalNovo <= valLimMinApolice Then
                                    valCapitalNovo = valLimMinApolice
                                ElseIf valCapitalNovo > valLimMinApolice And valCapitalNovo < valLimMaxApolice Then
                                    alteracoes = "1"
                                ElseIf valCapitalNovo >= valLimMaxApolice Then
                                    valCapitalNovo = valLimMaxApolice
                                    alteracoes = "1"
                                End If

                            ElseIf valCapitalAnterior >= valLimMaxApolice Then

                                If valCapitalNovo >= valLimMaxApolice Then
                                    valCapitalNovo = valLimMaxApolice
                                ElseIf valCapitalNovo > valLimMinApolice And valCapitalNovo < valLimMaxApolice Then
                                    alteracoes = "1"
                                ElseIf valCapitalNovo <= valLimMinApolice Then
                                    valCapitalNovo = valLimMinApolice
                                    alteracoes = "1"
                                End If

                            Else

                                If valCapitalNovo <= valLimMinApolice Then
                                    valCapitalNovo = valLimMinApolice
                                ElseIf valCapitalNovo >= valLimMaxApolice Then
                                    valCapitalNovo = valLimMaxApolice
                                End If
                                If (valCapitalAnterior <> valCapitalNovo) Then
                                    alteracoes = "1"
                                End If
                            End If

                            Try
                                atualizaSegurado(Request.Form("idSolicitado"), Session("apolice"), Session("ramo"), Session("subGrupo_id"), Request.Form("txtTitNome"), Request.Form("txtTitCPF"), Request.Form("txtTitNascimento"), Request.Form("txtTitAdmissao"), Request.Form("ddlTitSexo"), valCapitalNovo, valorSalario, alteracoes, valCapitalAnterior) ''22/01/2019 - Confitec SP - Cleiton Queiroz - Demanda MU-2018-070494 - Ajuste de regra SEGP 1118
                                'vitor.cabral - Nova Consultoria - 09/01/2014 - Inicio
                                mInsereSeguradoPPE(Session("ramo"), Session("apolice"), Session("subGrupo_id"), Request.Form("txtTitCPF"), 1, Request.Form("txtTitAdmissao"), Session("usuario"), getOperacao())
                                If isPPE <> Nothing And isPPE > 0 Then
                                    cUtilitarios.br("Vida alterada com restri��o de PPE(Pessoa Politicamente Exposta).\nSomente uma pessoa autorizada, poder� aceitar essa altera��o.")
                                End If
                                'vitor.cabral - Nova Consultoria - 09/01/2014 - Fim
                            Catch ex As Exception
                                Dim excp As New clsException(ex)
                            End Try
                        End If

                    ElseIf Request.Form("acao") = "excluir" Or Request.Form("acao") = "excluirdependente" Then

                        Dim dtDesligamento As String

                        dtDesligamento = IIf(Request.Form("acao") = "excluir", Request.Form("txtTitDesligamento"), Request.Form("txtDepDesligamento"))

                        Try
                            mDeleteRegistro(Request.Form("idSolicitado"), dtDesligamento)
                            'vitor.cabral - Nova Consultoria - 09/01/2014 - Inicio
                            mInsereSeguradoPPE(Session("ramo"), Session("apolice"), Session("subGrupo_id"), Request.Form("txtTitCPF"), 1, Request.Form("txtTitAdmissao"), Session("usuario"), getOperacao())
                            If isPPE <> Nothing And isPPE > 0 Then
                                cUtilitarios.br("Vida exclu�da com restri��o de PPE(Pessoa Politicamente Exposta).\nSomente uma pessoa autorizada, poder� aceitar essa exclus�o.")
                            End If
                            'vitor.cabral - Nova Consultoria - 09/01/2014 - Fim
                        Catch ex As Exception
                            Dim excp As New clsException(ex)
                        End Try
                    End If
                End If
            Catch ex As Exception
                Dim excp As New clsException(ex)
            End Try

            Try
                If Not (Session("apolice") = Nothing) Then

                    ''29062010 - Douglas - Confitec
                    ''FLOW 4192841 - FATURAMENTO WEB - IMPOSSIBILIDADE DE REALIZAR EXCLUS�O (Fw: FLOW WEB - IMPOSSIBILIDADE DE EXCLUIR VIDAS)


                    'If (Session("apolice") = "") Then
                    '    cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
                    '    'Response.End()
                    'End If

                    Dim vlApolice As String = Session("apolice").ToString
                    If vlApolice = "" Then
                        cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
                        'Response.End()
                    End If


                End If
            Catch ex As Exception
                Dim excp As New clsException(ex)
                cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
                'Response.End()
            End Try

            ' Projeto 539202 - Jo�o Ribeiro - 29/04/2009
            hdnAcesso.Value = Session("acesso")

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Public Function ConsultarCoberturaBasica(ByVal iSeguradoraCodSusep As Integer, _
                                             ByVal iSucursalSeguradoraId As Integer, _
                                             ByVal iRamoId As Integer, _
                                             ByVal lApoliceId As Long, _
                                             ByVal iSubGrupoId As Integer, _
                                             Optional ByVal iTpComponenteId As Integer = 1) As Data.SqlClient.SqlDataReader

        Dim sSql As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dsDetalhe As Data.SqlClient.SqlDataReader = Nothing

        sSql = ""
        sSql &= "EXEC [sisab003].seguros_db.dbo.sel_cob_basica_sps"
        sSql &= "  " & iSeguradoraCodSusep
        sSql &= ", " & iSucursalSeguradoraId
        sSql &= ", " & iRamoId
        sSql &= ", " & lApoliceId
        sSql &= ", " & iSubGrupoId
        sSql &= ", " & iTpComponenteId
        bd.SQL = sSql

        Try
            dsDetalhe = bd.ExecutaSQL_DR()
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return dsDetalhe

    End Function

    Public Function ConsultarLimiteIdade(ByVal iSeguradoraCodSusep As Integer, _
                                     ByVal iSucursalSeguradoraId As Integer, _
                                     ByVal iRamoId As Integer, _
                                     ByVal lApoliceId As Long, _
                                     ByVal iSubGrupoId As Integer, _
                                     ByVal dtLimite As Date, _
                                     ByRef iQtdFatura As Integer) As Data.SqlClient.SqlDataReader

        Dim sSql As String = ""
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dsLimite As Data.SqlClient.SqlDataReader = Nothing
        Dim dsConsulta As Data.SqlClient.SqlDataReader = Nothing

        sSql = ""
        sSql &= "EXEC [sisab003].seguros_db.dbo.sel_conta_faturas_sps"
        sSql &= "  " & iSeguradoraCodSusep
        sSql &= ", " & iSucursalSeguradoraId
        sSql &= ", " & iRamoId
        sSql &= ", " & lApoliceId
        sSql &= ", " & iSubGrupoId

        bd.SQL = sSql

        Try
            dsLimite = bd.ExecutaSQL_DR()
            If Not dsLimite Is Nothing Then
                If dsLimite.Read Then
                    iQtdFatura = dsLimite(0)
                Else
                    iQtdFatura = 0
                End If
                If Not dsLimite.IsClosed Then dsLimite.Close()
                dsLimite = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        sSql = ""
        sSql &= "EXEC [sisab003].seguros_db.dbo.sel_limite_idade_sps"
        sSql &= "  " & iSeguradoraCodSusep
        sSql &= ", " & iSucursalSeguradoraId
        sSql &= ", " & iRamoId
        sSql &= ", " & lApoliceId
        sSql &= ", " & iSubGrupoId
        sSql &= ", '" & Format(dtLimite, "yyyyMMdd") & "'"
        bd.SQL = sSql

        Try
            dsConsulta = bd.ExecutaSQL_DR()
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return dsConsulta

    End Function

    Public Function ValidarDetalhe(ByVal sTpIsSubGrupo As String, ByVal dtInicioVigenciaCobertura As Date, _
                                   ByVal dtInicioVigenciaApolice As Date, ByVal cValCapitalSegurado As Decimal, _
                                   ByVal cValSalario As Decimal, ByVal iTpComponenteId As Integer, _
                                   ByVal lApoliceId As Long, ByVal iRamoId As Integer, _
                                   ByVal iSeguradoraCodSusep As Integer, ByVal iSucursalSeguradoraId As Integer, _
                                   ByVal iSubGrupoId As Integer, ByVal sTpOperacao As String, _
                                   ByVal dtNascimento As Date, ByVal iIdade As Integer, _
                                   ByVal dtLimite As Date, ByVal sTextoBeneficiario As String, _
                                   ByVal sCPF As String, ByVal sSexo As String, _
                                   ByVal cValCapitalFixo As Decimal, ByRef cValLimiteMinIs As Decimal, _
                                   ByRef cValLimiteMaxIs As Decimal, ByRef iLimiteMinIdade As Integer, _
                                   ByRef iLimiteMaxIdade As Integer, ByVal dtEntradaSbg As Date, _
                                   Optional ByRef lSeqClienteId As Long = 0, _
                                   Optional ByVal lSeqClienteIdTitular As Long = 0, _
                                   Optional ByVal dtInicioVigenciaCoberturaTitular As Date = #1/1/1900#, _
                                   Optional ByVal sTpClausulaConjuge As String = "", _
                                   Optional ByVal sNomeConjugue As String = "", _
                                   Optional ByVal p_bSeguradoExistente As Boolean = False) As Integer

        Try
            Dim iQtdFatura As Integer = 0
            Dim oSubGrupo As SEGL0285.cls00460
            Dim dsDetalhe As Data.SqlClient.SqlDataReader = Nothing
            Dim iNumErro As Integer = 0
            Dim dtFimVigenciaCobAnterior As Date
            Dim bAtivo As Boolean = True

            Const CAPITAL_FIXO As String = "F"
            Const CAPITAL_INFORMADO As String = "C"
            Const CAPITAL_FAIXA As String = "E"
            Const CAPITAL_GLOBAL As String = "G"
            Const MULTIPLO_SALARIAL As String = "S"
            Const TP_COMPONENTE_TITULAR As Integer = 1
            Const TP_COMPONENTE_CONJUGE As Integer = 3

            Try
                oSubGrupo = New SEGL0285.cls00460

                ' Verificando se a data do in�cio da cobertura � maior que o in�cio de vig�ncia da ap�lice ''''''''''''''''''''''''
                If DateDiff("d", dtInicioVigenciaCobertura, dtInicioVigenciaApolice) > 0 Then
                    ValidarDetalhe = 1
                    oSubGrupo = Nothing
                    dsDetalhe = Nothing
                    Exit Function
                End If

                ' Verificando se a data de entrada do subgrupo � maior que o in�cio de vig�ncia da ap�lice ''''''''''''''''''''''''
                If DateDiff("d", dtEntradaSbg, dtInicioVigenciaApolice) > 0 Then
                    ValidarDetalhe = 2
                    oSubGrupo = Nothing
                    dsDetalhe = Nothing
                    Exit Function
                End If

                ' Verificando a exist�ncia do segurado ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                If VerificarExistenciaDetalhe(iSeguradoraCodSusep, iSucursalSeguradoraId, _
                                              iRamoId, lApoliceId, _
                                              iSubGrupoId, iTpComponenteId, _
                                              sCPF, dtNascimento, _
                                              sSexo, bAtivo, _
                                              dtFimVigenciaCobAnterior, _
                                              lSeqClienteId) Then

                    If UCase(sTpOperacao) = "I" Then

                        If bAtivo Then
                            ValidarDetalhe = 17
                            oSubGrupo = Nothing
                            dsDetalhe = Nothing
                            Exit Function
                        Else
                            If DateDiff("d", dtFimVigenciaCobAnterior, dtInicioVigenciaCobertura) <= 0 Then
                                ValidarDetalhe = 18
                                oSubGrupo = Nothing
                                dsDetalhe = Nothing
                                Exit Function
                            End If
                        End If

                    ElseIf UCase(sTpOperacao) = "A" Then

                        If Not bAtivo Then
                            ValidarDetalhe = 21
                            oSubGrupo = Nothing
                            dsDetalhe = Nothing
                            Exit Function
                        End If

                    ElseIf UCase(sTpOperacao) = "E" Then

                        If Not bAtivo Then
                            ValidarDetalhe = 22
                            oSubGrupo = Nothing
                            dsDetalhe = Nothing
                            Exit Function
                        End If

                    End If

                Else

                    If UCase(sTpOperacao) = "A" Then
                        ValidarDetalhe = 23
                        oSubGrupo = Nothing
                        dsDetalhe = Nothing
                        Exit Function
                    ElseIf (sTpOperacao) = "E" Then
                        ValidarDetalhe = 24
                        oSubGrupo = Nothing
                        dsDetalhe = Nothing
                        Exit Function
                    End If

                End If

                ' Verificando se o tipo de componente � c�njuge 
                If iTpComponenteId = TP_COMPONENTE_TITULAR Then

                    If UCase(sTpOperacao) <> "E" Then

                        Select Case sTpIsSubGrupo

                            Case CAPITAL_FIXO

                                If cValCapitalSegurado <= 0 Then
                                    ValidarDetalhe = 4
                                    oSubGrupo = Nothing
                                    dsDetalhe = Nothing
                                    Exit Function
                                End If

                                If cValCapitalSegurado <> cValCapitalFixo Then
                                    ValidarDetalhe = 3
                                    oSubGrupo = Nothing
                                    dsDetalhe = Nothing
                                    Exit Function
                                End If

                            Case CAPITAL_INFORMADO

                                If cValCapitalSegurado <= 0 Then
                                    ValidarDetalhe = 4
                                    oSubGrupo = Nothing
                                    dsDetalhe = Nothing
                                    Exit Function
                                End If

                            Case CAPITAL_FAIXA

                                If cValCapitalSegurado <= 0 Then
                                    ValidarDetalhe = 4
                                    oSubGrupo = Nothing
                                    dsDetalhe = Nothing
                                    Exit Function
                                End If

                                Try
                                    iNumErro = oSubGrupo.ValidarFaixaEtaria(iSeguradoraCodSusep, iSucursalSeguradoraId, _
                                                                            lApoliceId, iRamoId, iSubGrupoId, _
                                                                            cValCapitalSegurado, iIdade, _
                                                                            dtInicioVigenciaCobertura)
                                Catch ex As Exception
                                    Dim excp As New clsException(ex)
                                End Try

                                If iNumErro > 0 Then
                                    ValidarDetalhe = iNumErro
                                    oSubGrupo = Nothing
                                    dsDetalhe = Nothing
                                    Exit Function
                                End If

                            Case MULTIPLO_SALARIAL

                                If Val(cValSalario) <= 0 Then
                                    ValidarDetalhe = 6
                                    oSubGrupo = Nothing
                                    dsDetalhe = Nothing
                                    Exit Function
                                End If

                        End Select

                        If sTpIsSubGrupo <> CAPITAL_GLOBAL Then

                            Try
                                ' Obtendo os limites de IS 
                                dsDetalhe = ConsultarCoberturaBasica(iSeguradoraCodSusep, iSucursalSeguradoraId, _
                                                       iRamoId, lApoliceId, iSubGrupoId)
                            Catch ex As Exception
                                Dim excp As New clsException(ex)
                            End Try

                            If dsDetalhe.Read Then
                                cValLimiteMinIs = dsDetalhe("val_lim_min_is")
                                cValLimiteMaxIs = dsDetalhe("val_lim_max_is")
                                dsDetalhe = Nothing
                            Else
                                cValLimiteMinIs = 0
                                cValLimiteMaxIs = 0
                            End If

                            If cValLimiteMinIs > 0 Then
                                If cValCapitalSegurado < cValLimiteMinIs Then
                                    ValidarDetalhe = 7
                                    oSubGrupo = Nothing
                                    dsDetalhe = Nothing
                                    Exit Function
                                End If
                            End If

                            If cValLimiteMaxIs > 0 Then
                                If cValCapitalSegurado > cValLimiteMaxIs Then
                                    ValidarDetalhe = 8
                                    oSubGrupo = Nothing
                                    dsDetalhe = Nothing
                                    Exit Function
                                End If
                            End If
                            ' Validando o capital contra os limites de IS 

                        End If

                    End If

                End If

                ' Verificando se o tipo de componente � c�njuge 
                If iTpComponenteId = TP_COMPONENTE_CONJUGE Then
                    If lSeqClienteIdTitular = 0 Then
                        ValidarDetalhe = 9
                        oSubGrupo = Nothing
                        dsDetalhe = Nothing
                        Exit Function
                    End If

                    If UCase(sTpClausulaConjuge) = "A" Or UCase(sTpClausulaConjuge) = "N" Then
                        If Trim(sNomeConjugue) <> "" Then
                            ValidarDetalhe = 25
                            oSubGrupo = Nothing
                            dsDetalhe = Nothing
                            Exit Function
                        End If
                    End If

                    If lSeqClienteIdTitular > 0 Then

                        Try
                            If VerificarExistenciaOutroConjuge(lApoliceId, iRamoId, _
                                                               iSeguradoraCodSusep, iSucursalSeguradoraId, _
                                                               iSubGrupoId, iTpComponenteId, _
                                                               lSeqClienteIdTitular, lSeqClienteId) Then
                                ValidarDetalhe = 10
                                oSubGrupo = Nothing
                                dsDetalhe = Nothing
                                Exit Function
                            End If
                        Catch ex As Exception
                            Dim excp As New clsException(ex)
                        End Try

                    End If

                    If DateDiff("d", dtInicioVigenciaCoberturaTitular, dtInicioVigenciaCobertura) < 0 Then
                        ValidarDetalhe = 11
                        oSubGrupo = Nothing
                        dsDetalhe = Nothing
                        Exit Function
                    End If

                End If

                If UCase(sTpOperacao) = "I" Then

                    Try
                        dsDetalhe = ConsultarLimiteIdade(iSeguradoraCodSusep, iSucursalSeguradoraId, _
                                                               iRamoId, lApoliceId, iSubGrupoId, _
                                                               dtLimite, iQtdFatura)
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try

                    If dsDetalhe.Read Then
                        If iQtdFatura > 0 Then
                            iLimiteMinIdade = dsDetalhe("lim_min_movimentacao")
                            iLimiteMaxIdade = dsDetalhe("lim_max_movimentacao")
                        Else
                            iLimiteMinIdade = dsDetalhe("lim_min_inclusao")
                            iLimiteMaxIdade = dsDetalhe("lim_max_inclusao")
                        End If
                        dsDetalhe = Nothing
                    Else
                        iLimiteMinIdade = 0
                        iLimiteMaxIdade = 0
                    End If

                    ' Validando a idade 
                    If iIdade <= 0 Then
                        ValidarDetalhe = 5
                        oSubGrupo = Nothing
                        dsDetalhe = Nothing
                        Exit Function
                    End If

                    '23122010 - ercosta
                    'Dim v_bSeguradoExistente As Boolean = Me.UsuarioExiste(sCPF, lApoliceId, iRamoId, iSubGrupoId)

                    If iLimiteMinIdade > 0 Then
                        If iIdade < iLimiteMinIdade Then
                            ValidarDetalhe = 15
                            oSubGrupo = Nothing
                            dsDetalhe = Nothing
                            Exit Function
                        End If
                    End If

                    If Not p_bSeguradoExistente Then
                        If iLimiteMaxIdade > 0 Then
                            If iIdade > iLimiteMaxIdade Then
                                ValidarDetalhe = 16
                                oSubGrupo = Nothing
                                dsDetalhe = Nothing
                                Exit Function
                            End If
                        End If
                    End If

                End If

                ' Verificando caso de reativa��o 
                If UCase(sTpOperacao) = "R" Then

                    Try
                        If VerificarExistenciaDetalhe(iSeguradoraCodSusep, iSucursalSeguradoraId, _
                                                      iRamoId, lApoliceId, _
                                                      iSubGrupoId, iTpComponenteId, _
                                                      sCPF, dtNascimento, sSexo, _
                                                      bAtivo, dtFimVigenciaCobAnterior, _
                                                      lSeqClienteId, "S") Then
                            ValidarDetalhe = 26
                            oSubGrupo = Nothing
                            dsDetalhe = Nothing
                            Exit Function
                        End If
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try

                End If

                ValidarDetalhe = 0
                dsDetalhe = Nothing
                oSubGrupo = Nothing

            Catch ex As Exception
                Dim excp As New clsException(ex)
                'Throw New Exception("SEGL0285.cls00459.ValidarDetalhe - " & ex.Message)
            End Try
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Function

    Private Function UsuarioExiste(ByVal p_sCPF As String, ByVal p_lApliceID As Long, ByVal p_iRamoId As Integer, ByVal p_iSubGrupo As Integer) As Boolean
        Dim v_bExiste As Boolean = False

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        'bd.SEGS9391_SPS(p_sCPF, p_lApliceID, p_iRamoId)
        bd.SEGS8379_SPS(p_lApliceID, p_iRamoId, p_iSubGrupo, p_sCPF)

        Dim v_oDtDados As DataTable = Nothing

        Try
            v_oDtDados = bd.ExecutaSQL_DT

            If Not IsNothing(v_oDtDados) Then
                If v_oDtDados.Rows.Count > 0 Then
                    v_bExiste = True
                End If
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return v_bExiste
    End Function

    Private Function VerificarExistenciaOutroConjuge(ByVal lApoliceId As Long, ByVal iRamoId As Integer, _
                                                         ByVal iSeguradoraCodSusep As Integer, ByVal iSucursalSeguradoraId As Integer, _
                                                         ByVal iSubGrupoId As Integer, ByVal iTpComponenteIdConjuge As Integer, _
                                                         ByVal lSeqClienteIdTitular As Long, _
                                                         Optional ByVal lSeqClienteIdConjuge As Long = 0) As Boolean

        Dim sSql As String = ""
        Dim dsDetalhe As DataSet = Nothing
        Dim cRetorno As Boolean = False

        sSql = ""
        sSql &= "SELECT COUNT(*)"
        sSql &= "  FROM VIDA_WEB_DB..segurado_temporario_web_tb"
        sSql &= " WHERE apolice_id = " & lApoliceId
        sSql &= "   AND ramo_id = " & iRamoId
        sSql &= "   AND seguradora_cod_susep = " & iSeguradoraCodSusep
        sSql &= "   AND sucursal_seguradora_id = " & iSucursalSeguradoraId
        sSql &= "   AND sub_grupo_id = " & iSubGrupoId
        sSql &= "   AND tp_componente_id = " & iTpComponenteIdConjuge
        sSql &= "   AND prop_cliente_titular_id = " & lSeqClienteIdTitular
        If lSeqClienteIdConjuge > 0 Then
            sSql &= "   AND seq_cliente_id <> " & lSeqClienteIdConjuge
        End If

        Try
            dsDetalhe = Alianca.Seguranca.BancoDados.cCon.ExecuteDataset(sSql)

            If dsDetalhe.Tables(0).Rows.Count > 0 Then
                If dsDetalhe.Tables(0).Rows(0).Item(0) > 0 Then
                    cRetorno = True
                End If
            End If

            dsDetalhe = Nothing
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return cRetorno
    End Function

    Private Function VerificarExistenciaDetalhe(ByVal iSeguradoraCodSusep As Integer, _
                                                    ByVal iSucursalSeguradoraId As Integer, _
                                                    ByVal iRamoId As Integer, _
                                                    ByVal lApoliceId As Long, _
                                                    ByVal iSubGrupoId As Integer, _
                                                    ByVal iTpComponenteId As Integer, _
                                                    ByVal sCPF As String, _
                                                    ByVal dtNascimento As Date, _
                                                    ByVal sSexo As String, _
                                                    ByRef bAtivo As Boolean, _
                                                    ByRef dtFimVigenciaSbg As Date, _
                                                    ByRef lSeqClienteId As Long, _
                                                    Optional ByVal sVerificaAtivo As String = "") As Boolean
        Dim sSql As String
        Dim cRetorno As Boolean = False
        Dim dsDetalhe As Data.SqlClient.SqlDataReader = Nothing

        'Dim dsDetalhe As New  'DataSet
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        sSql = ""
        sSql &= "SELECT seq_cliente_id,"
        sSql &= "       dt_fim_vigencia_sbg"
        sSql &= "  FROM VIDA_WEB_DB..segurado_temporario_web_tb"
        sSql &= " WHERE apolice_id = " & lApoliceId
        sSql &= "   AND seguradora_cod_susep = " & iSeguradoraCodSusep
        sSql &= "   AND sucursal_seguradora_id = " & iSucursalSeguradoraId
        sSql &= "   AND ramo_id = " & iRamoId
        sSql &= "   AND sub_grupo_id = " & iSubGrupoId
        sSql &= "   AND cpf = '" & sCPF & "'"
        sSql &= "   AND dt_nascimento = '" & Format(dtNascimento, "yyyyMMdd") & "'"
        'sSql &= "   AND dt_nascimento = '" & CDate(dtNascimento) & "'")
        'sSql &= "   AND dt_nascimento = '" & trataDataDB(dtNascimento) & "'")
        '    adSQL sSql, "   AND sexo = '" & sSexo & "'"     'Solicitado por Daniel L. em 15/08/2003
        sSql &= "   AND tp_componente_id = " & iTpComponenteId

        ' Se for preciso olhar para a data de fim de vig�ncia da cobertura ''''''''''''''''''''''''''''''''''''''''''''''''
        If UCase(sVerificaAtivo) = "S" Then
            sSql &= "   AND dt_fim_vigencia_sbg IS NULL"
        ElseIf UCase(sVerificaAtivo) = "N" Then
            sSql &= "   AND dt_fim_vigencia_sbg IS NOT NULL"
        End If
        sSql &= " ORDER BY dt_fim_vigencia_sbg"
        bd.SQL = sSql

        Try

            dsDetalhe = bd.ExecutaSQL_DR()
            If Not dsDetalhe Is Nothing Then
                If dsDetalhe.Read Then
                    lSeqClienteId = dsDetalhe("seq_cliente_id")
                    If Not IsDBNull(dsDetalhe("dt_fim_vigencia_sbg")) Then
                        dtFimVigenciaSbg = CDate(dsDetalhe("dt_fim_vigencia_sbg"))
                        bAtivo = False
                    Else
                        dtFimVigenciaSbg = CDate("01/01/1900")
                        bAtivo = True
                    End If
                    cRetorno = True
                Else
                    cRetorno = False
                End If
                If Not dsDetalhe.IsClosed Then dsDetalhe.Close()
                dsDetalhe = Nothing
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return cRetorno

    End Function

    Private Function VerificarDataSubGrupo() As Boolean
        Dim cRetorno As Boolean = False

        Try
            If DateTime.Compare(RetornaDtInicioSubGrupo(), DateTime.Parse(Request.Form("txtTitAdmissao"))) >= 0 Then
                cUtilitarios.br("A data de inclus�o no seguro tem que ser maior que a data de in�cio do subgrupo.")
            Else
                cRetorno = True
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return cRetorno

    End Function

    Private Function VerificarIdadeMaxMin(ByVal idadeMin As Double, ByVal idadeMax As Double, Optional ByVal v_bSeguradoExistente As Boolean = False) As Boolean
        Dim cRetorno As Boolean = False

        idadeMax = 0
        idadeMin = 0
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        bd.SEGS5705_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Try
            Dim param(6) As String

            Try
                dr = bd.ExecutaSQL_DR()
                If Not dr Is Nothing Then
                    If dr.Read Then
                        idadeMax = IIf(dr.GetValue(1).ToString.Trim.Length = 0, 0, Convert.ToInt32(dr.GetValue(1).ToString.Trim))
                        idadeMin = IIf(dr.GetValue(0).ToString.Trim.Length = 0, 0, Convert.ToInt32(dr.GetValue(0).ToString.Trim))
                    End If
                    If Not dr.IsClosed Then dr.Close()
                    dr = Nothing
                End If
            Catch ex As Exception
                Dim excp As New clsException(ex)
            End Try

            If idadeMin <= 0 Or idadeMax <= 0 Then
                Dim excp As New clsException(New Exception("Idade Minima e Maxima n�o encontradas"))
                Return False
            End If

            Dim tmpDt As String = Request.Form("txtTitNascimento")
            Dim dt_arr() As String = tmpDt.Split("/")
            Dim dt_final As String

            Dim dia As String = dt_arr(0).PadLeft(2, "0")
            Dim mes As String = dt_arr(1).PadLeft(2, "0")
            Dim ano As String = dt_arr(2)

            dt_final = ano & "/" & mes & "/" & dia

            '************ 30/11/2007 *******************
            Dim tmpDt1 As String = Request.Form("txtTitAdmissao")
            Dim dt_arr1() As String = tmpDt1.Split("/")
            Dim dt_final1 As String

            Dim dia1 As String = dt_arr1(0).PadLeft(2, "0")
            Dim mes1 As String = dt_arr1(1).PadLeft(2, "0")
            Dim ano1 As String = dt_arr1(2)

            dt_final1 = ano1 & "/" & mes1 & "/" & dia1
            Dim DataEscolhida1 As DateTime = Convert.ToDateTime(dt_final1)
            '********************************************

            Dim DataEscolhida As DateTime = Convert.ToDateTime(dt_final)
            Dim anos As Integer

            Response.Write("<!-- dt Escolhida - " & DataEscolhida & "-->" & vbCrLf)

            anos = DateDiff(DateInterval.Year, DataEscolhida, DataEscolhida1)

            If DataEscolhida.Month > DataEscolhida1.Month Then
                anos = anos - 1
            ElseIf DataEscolhida.Month = DataEscolhida1.Month Then
                If (DataEscolhida.Day > DataEscolhida1.Day) Then
                    anos = anos - 1
                End If
            End If

            Response.Write("<!-- idade - " & anos & "-->" & vbCrLf)
            Response.Write("<!-- min - " & idadeMin & "-->" & vbCrLf)
            Response.Write("<!-- max - " & idadeMax & "-->" & vbCrLf)

            If anos < idadeMin Then
                cUtilitarios.br("Essa vida tem idade menor que a idade m�nima permitida.")

                ' pablo.cardoso
                ' Data da Altera��o: 13/02/2012
                ' Demanda AB: 12784293 
                ' Item: 7 - Permitir incluir vida que j� participou da ap�lice em algum momento, inclusive se tiver idade superior.
                ' Obs: Como a valida��o de idade m�xima(para quem nunca participou) ocorre via ajax, n�o se faz necess�rio apresentar esse "ElseIf"

                'ElseIf anos > idadeMax Then
                'If v_bSeguradoExistente = False Then cUtilitarios.br("Essa vida tem idade maior que a idade m�xima permitida.")
            End If

            cRetorno = True

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return cRetorno
    End Function

    Public Sub getPeriodoApolice()
        Try
            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
            Dim dr As Data.SqlClient.SqlDataReader = Nothing
            Dim periodo As String = "0"

            'Pega primeira parte dos dados
            bd.GetDadosSubgrupoE1(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

            Try
                dr = bd.ExecutaSQL_DR()
                If Not dr Is Nothing Then
                    If dr.Read Then
                        'Linha 2
                        periodo = dr.GetValue(2).ToString

                        Select Case periodo
                            Case "MENSAL"
                                periodo = 2
                            Case "BIMESTRAL"
                                periodo = 4
                            Case "TRIMESTRAL"
                                periodo = 6
                            Case "QUADRIMESTRAL"
                                periodo = 8
                            Case "SEMESTRAL"
                                periodo = 12
                            Case "ANUAL"
                                periodo = 24
                            Case "QUINZENAL"
                                periodo = 1
                            Case Else
                                periodo = 0
                        End Select
                    Else
                        periodo = "0"
                    End If
                    If Not dr Is Nothing Then
                        If Not dr.IsClosed Then dr.Close()
                    End If
                    dr = Nothing
                End If

            Catch ex As Exception
                Dim excp As New clsException(ex)
                periodo = "0"
            End Try

            bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

            Try
                dr = bd.ExecutaSQL_DR()
                Dim data_inicio, data_fim As String

                If Not dr Is Nothing Then
                    If dr.Read Then

                        data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                        data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)
                        cUtilitarios.escreveScript("var dt_ini_comp_2_meses = '" & CType(data_inicio, DateTime).AddMonths(periodo * -1) & "';")

                    Else
                        Session.Abandon()
                        'Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
                    End If
                    If Not dr.IsClosed Then dr.Close()
                    'dr = Nothing
                End If

            Catch ex As Exception
                Dim excp As New clsException(ex)
            End Try
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Private Function RetornaDtInicioSubGrupo() As DateTime
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dtiniciosubgrupo As DateTime = Date.Now
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        dtiniciosubgrupo.AddMonths(-1)
        bd.SEGS5706_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

        Try
            dr = bd.ExecutaSQL_DR
            If Not dr Is Nothing Then
                If dr.Read Then
                    dtiniciosubgrupo = CType(dr.GetValue(3), DateTime)
                End If

                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return dtiniciosubgrupo

    End Function

    Public Function getCapitalSeguradoSession() As String

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim count As Int16 = 1

        bd.SEGS5706_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

        Dim capitalSegurado As String = ""

        Try
            dr = bd.ExecutaSQL_DR
            If Not dr Is Nothing Then
                If dr.Read() Then
                    capitalSegurado = dr.GetValue(1).ToString()
                End If

                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return capitalSegurado

    End Function

    Private Function getSeqClienteID(ByVal id As Integer) As String
        Dim cRetorno As String = ""
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        bd.SEGS5655_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "null", "null", id)

        Try
            dr = bd.ExecutaSQL_DR

            If Not dr Is Nothing Then
                If dr.Read() Then
                    cRetorno = dr.GetValue(10).ToString
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return cRetorno
    End Function

    Private Function getQtdSalariosSession() As String
        Dim qtd_salarios As String = ""
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim count As Int16 = 1
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        bd.SEGS5706_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

        Try
            dr = bd.ExecutaSQL_DR()

            If Not dr Is Nothing Then
                If dr.Read Then
                    qtd_salarios = dr.GetValue(2).ToString()
                End If

                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return qtd_salarios

    End Function

    Private Sub formaSalarioCapital()
        Try
            Dim capitalSegurado As String = getCapitalSeguradoSession()

            Select Case capitalSegurado
                Case "M�ltiplo sal�rio"
                    pnlSalario.Visible = True
                    pnlCapital.Visible = False
                    enable(txtTitSalario)
                    disable(txtTitCapital)
                Case "Capital por faixa et�ria"
                    pnlSalario.Visible = True
                    pnlCapital.Visible = True

                    enable(txtTitSalario)
                    disable(txtTitCapital)
                    'txtTitCapital.Text = "---"
                Case "Capital fixo"
                    pnlSalario.Visible = False
                    pnlCapital.Visible = False
                    disable(txtTitSalario)
                    enable(txtTitCapital)
                Case "Capital informado"
                    pnlSalario.Visible = False
                    pnlCapital.Visible = True
                    enable(txtTitSalario)
                    enable(txtTitCapital)
                Case "Capital Global"
                    pnlSalario.Visible = False
                    pnlCapital.Visible = False
                    disable(txtTitSalario)
                    disable(txtTitCapital)
                Case Else
                    pnlSalario.Visible = True
                    pnlCapital.Visible = True
                    enable(txtTitSalario)
                    enable(txtTitCapital)
            End Select
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    ''' <summary>
    ''' Recupera valor de pct limite em seguros_db..limite_alteracao_vida_tb
    ''' </summary>
    ''' <returns>perc_limite</returns>
    ''' <remarks></remarks>
    Public Function getLimiteAlteracaoVida() As Decimal

        Dim cRetorno As Decimal = 0
        Dim v_oDtLimite As DataTable = Nothing
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS7596_SPS()

        Try

            v_oDtLimite = bd.ExecutaSQL_DT

            If Not IsNothing(v_oDtLimite) Then cRetorno = v_oDtLimite.Rows(0)("perc_limite")

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return cRetorno

    End Function

    ' modifica o capital para salvar no banco
    Public Function getCapitalBySalario(ByVal salario As String) As Double
        Dim cRetorno As Double = 0

        Try
            Dim capitalSegurado As String = getCapitalSeguradoSession()
            Select Case capitalSegurado
                Case "M�ltiplo sal�rio"
                    cRetorno = Double.Parse(salario) * getQtdSalariosSession()
            End Select
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return cRetorno

    End Function

    Private Sub getDadosConjuge(ByVal id As Integer, ByVal idTitular As String)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim count As Int16 = 1
        Dim temp As String

        bd.SEGS5655_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "null", "null", id)

        Try

            dr = bd.ExecutaSQL_DR
            If Not dr Is Nothing Then
                If dr.Read Then

                    txtDepCPF.Text = cUtilitarios.trataCPF(dr.GetValue(2).ToString())
                    txtDepNome.Text = dr.GetValue(3).ToString()

                    temp = dr.GetValue(5).ToString().Split(" ")(0)
                    txtDepNascimento.Text = cUtilitarios.trataData(temp)

                    Me.txtSeqClienteID.Text = dr.GetValue(10).ToString

                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Private Function getTipoOperacaoUsuario(ByVal id As Integer) As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5655_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "null", "null", id)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim retorno As String = ""

        Try
            dr = bd.ExecutaSQL_DR

            If Not dr Is Nothing Then
                If dr.Read Then

                    retorno = dr.GetValue(0).ToString()
                    If (retorno = "Pendente") Then
                        If Not cUtilitarios.verificaUsuarioHistorico(id) Then
                            retorno = "Inclus&atilde;o"
                        End If
                    End If

                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return retorno
    End Function

    Public Function getSexoConjuge(ByVal id_titular As Integer) As String
        Dim cRetorno As String = ""
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5655_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "null", "null", id_titular)
        Dim dr As Data.SqlClient.SqlDataReader

        Try
            dr = bd.ExecutaSQL_DR

            If Not dr Is Nothing Then
                If dr.Read Then

                    Select Case dr.GetValue(6).ToString()
                        Case "masc."
                            cRetorno = "F"
                        Case Else
                            cRetorno = "M"
                    End Select
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return cRetorno

    End Function

    Private Function getDtDadosSubGrupo() As DataTable
        Dim v_oDtResultado As DataTable = Nothing

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.GetDadosSubGrupoSPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), 6785, 0)

        Try

            v_oDtResultado = bd.ExecutaSQL_DT

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return v_oDtResultado
    End Function

    Private Function getDtDesligamentoAgendado(ByVal id As Integer) As String
        Dim dt As String = ""
        Dim temp As String = ""
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        bd.SEGS5655_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), id)

        Try

            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    temp = dr.Item("dt_fim_vigencia_sbg").ToString().Split(" ")(0)
                    dt = cUtilitarios.trataData(temp)
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return dt

    End Function

    Private Sub getDadosUsuario(ByVal id As Integer)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5655_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "null", "null", Int32.Parse(id))
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim count As Int16 = 1
        Dim temp As String = ""
        Dim cValor As String = ""

        Try
            dr = bd.ExecutaSQL_DR

            If Not dr Is Nothing Then
                If dr.Read Then

                    cValor = cUtilitarios.TrataNulo(dr.GetValue(2))
                    txtTitCPF.Text = IIf(cValor <> "", cUtilitarios.trataCPF(cValor), "")

                    cValor = cUtilitarios.TrataNulo(dr.GetValue(3))
                    txtTitNome.Text = IIf(cValor <> "", cValor.ToString(), "")

                    cValor = cUtilitarios.TrataNulo(dr.GetValue(5))
                    temp = cValor.ToString().Split(" ")(0)
                    txtTitNascimento.Text = cUtilitarios.trataData(temp)

                    cValor = cUtilitarios.TrataNulo(dr.GetValue(6))
                    If cValor.ToString() = "masc." Or cValor.ToString() = "fem." Or _
                        cValor.ToString.ToLower = "n" Then
                        ddlTitSexo.SelectedValue = cValor.ToString().ToLower
                    Else
                        ddlTitSexo.SelectedValue = 0
                    End If

                    cValor = cUtilitarios.TrataNulo(dr.GetValue(7))
                    txtTitSalario.Text = IIf(cValor <> "", cUtilitarios.trataMoeda(cValor.ToString()), "")

                    cValor = cUtilitarios.TrataNulo(dr.GetValue(8))
                    txtTitCapital.Text = IIf(cValor <> "", cUtilitarios.trataMoeda(cValor.ToString()), "")

                    cValor = cUtilitarios.TrataNulo(dr.GetValue(10))
                    Me.txtSeqClienteID.Text = IIf(cValor <> "", cValor.ToString, "")

                    cValor = cUtilitarios.TrataNulo(dr.GetValue(9))
                    temp = IIf(cValor <> "", cValor.ToString().Split(" ")(0), "")
                    txtTitAdmissao.Text = cUtilitarios.trataData(temp)

                    cValor = cUtilitarios.TrataNulo(dr("prop_cliente_id"))
                    Me.hdnPropClienteId.Value = IIf(cValor <> "", cValor.ToString().Split(" ")(0), 0)
                    Me.hdnUltCapSegurado.Value = Me.getCapitalSeguradoUltimoFaturamento().ToString().Replace(",", ".")

                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Function getCpfTitular(ByVal idTitular As String) As String
        Dim cpf As String = ""
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5655_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "null", "null", idTitular)

        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        Try
            dr = bd.ExecutaSQL_DR
            If Not dr Is Nothing Then
                If dr.Read Then
                    cpf = cUtilitarios.trataCPF(dr.GetValue(2).ToString())
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return cpf
    End Function

    Private Function getNomeTitular(ByVal idTitular As String) As String
        Dim nome As String = ""
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5655_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "null", "null", idTitular)

        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        Try
            dr = bd.ExecutaSQL_DR()

            If Not dr Is Nothing Then
                If dr.Read Then
                    nome = dr.GetValue(3).ToString()
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return nome
    End Function

    Private Sub mDeleteRegistro(ByVal id As Integer, ByVal dt As String)

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        Try
            dt = cUtilitarios.trataDataDB(dt)
        Catch ex As Exception
            Dim excp As New clsException(ex)
            dt = "null"
        End Try

        bd.SEGS5658_SPD(id, dt, Session("usuario"))
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        Try

            dr = bd.ExecutaSQL_DR()
            Session("alterado") = 1
            If Not dr Is Nothing Then
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
            cUtilitarios.escreveScript("parent.parent.GB_reloadOnClose('true');")
            cUtilitarios.escreveScript("parent.parent.top.exibeaguarde();")
            cUtilitarios.escreveScript("parent.parent.GB_hide();")
            If Session("acaoMsg").ToString = "apagar" Then
                cUtilitarios.br("Opera��o realizada com sucesso!")
            Else
                cUtilitarios.br("Segurado Exclu�do com sucesso")
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Public Function VerificaCapitalAlteracao(ByVal id As String, ByVal capital_novo As String) As Boolean

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim capital As Double = 0
        Dim capital_alter As Double = Double.Parse(capital_novo)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        bd.SEGS5655_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "null", "null", id, "null", "1")

        Try
            dr = bd.ExecutaSQL_DR
            If Not dr Is Nothing Then
                If dr.Read Then
                    If Not IsDBNull(dr.Item("val_capital_segurado")) Then
                        capital = Double.Parse(dr.Item("val_capital_segurado"))
                    End If
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        If capital > 500000 Then
            Return False
        ElseIf capital > 100000 And capital <= 500000 And capital_alter > 500000 Then
            cUtilitarios.br("Esclaremos que ser� necess�rio o preenchimento do Cart�o Proposta com Declara��o Pessoal de Sa�de (DPS) e da Ficha Financeira para a inclus�o do referido proponente.  Para isso imprima o modelo no menu Download e siga as instru��es contidas no corpo do documento. Somente ap�s a an�lise das informa��es do proponente em quest�o, bem como o parecer favor�vel da Companhia, ser� autorizada essa inclus�o.")
            Return False
        ElseIf capital <= 100000 And capital_alter > 100000 Then
            cUtilitarios.br("Esclarecemos que ser� necess�rio o preenchimento do Cart�o Proposta com Declara��o Pessol de Sa�de (DPS) para a inclus�o do referido proponente.  Para isso imprima o modelo no menu Download e siga as instru��es contidas no corpo do documento.\n\nSomente ap�s a an�lise das informa��es do proponente em quest�o, bem como o parecer favor�vel da Companhia, ser� autorizada essa inclus�o.")
            Return False
        End If

        Return False

    End Function

    Public Function VerificaCapitalAlteracao(ByVal p_dCapitalNovo As Decimal) As Boolean
        Dim v_sTpCapitalSegurado As String = getCapitalSeguradoSession()
        Dim v_dLimitePct As Decimal = getLimiteAlteracaoVida()
        Dim v_dVlCapiltalSegurado As Decimal = getCapitalSeguradoUltimoFaturamento()
        Dim v_dNovoCapitalCalculado As Decimal = 0

        Select Case v_sTpCapitalSegurado
            Case "M�ltiplo sal�rio"
                Dim v_dFatorMultiplicador As Decimal = Me.getDtDadosSubGrupo().Rows(0)("qtd_salarios")

                v_dNovoCapitalCalculado = p_dCapitalNovo * v_dFatorMultiplicador

            Case "Capital informado"
                v_dNovoCapitalCalculado = p_dCapitalNovo
            Case "Capital por faixa et�ria", "Capital Global", "Capital fixo"
                Return True
            Case Else
                Return True
        End Select

        'Regra para Valida��o
        '====================================================
        'capital_alterado > val_capital_segurado + (val_capital_segurado * perc_limite) 
        'val_capital_segurado = web_seguros_db..segurado_temporario_web_espelho_tb.val_capital_segurado 
        'perc_limite = seguros_db..limite_alteracao_vida_tb.perc_limite
        If v_dNovoCapitalCalculado > v_dVlCapiltalSegurado + (v_dVlCapiltalSegurado * v_dLimitePct) Then
            Return False
        Else
            Return True
        End If

    End Function


Private Sub atualizaSegurado(ByVal id As Integer, ByVal apolice As Integer, ByVal ramo As Integer, ByVal sub_grupo_id As Integer, ByVal nome As String, ByVal cpf As String, ByVal dt_nascimento As String, ByVal dt_admissao As String, ByVal sexo As String, ByVal val_capital_segurado As String, ByVal val_salario As String, ByVal alteracoes As String, Optional ByVal val_capital_segurado_origem As String = "0") '22/01/2019 - Confitec SP - Cleiton Queiroz - Demanda MU-2018-070494 - Ajuste de regra SEGP 1118

        VerificaCapitalAlteracao(id, val_capital_segurado)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        bd.SEGS5657_SPU(id, Request.Form("txtSeqClienteID"), 0, apolice, 0, 6785, ramo, sub_grupo_id, dt_admissao, nome, 1, cpf, dt_nascimento, sexo, val_capital_segurado, val_salario, "getDate()", Session("usuario"), "0", "0", alteracoes, val_capital_segurado_origem) '22/01/2019 - Confitec SP - Cleiton Queiroz - Demanda MU-2018-070494 - Ajuste de regra SEGP 1118

        Try
            bd.ExecutaSQL()

            Session("alterado") = 1
            cUtilitarios.escreveScript("top.document.id_alterado_SEGW0065 = true;")
            cUtilitarios.escreveScript("parent.parent.GB_reloadOnClose('true');")
            cUtilitarios.escreveScript("parent.parent.top.exibeaguarde();")
            cUtilitarios.escreveScript("parent.parent.GB_hide();")
            cUtilitarios.br("Segurado Atualizado com sucesso.")
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Private Sub mInsereSeguradoTemporario(ByVal apolice As Integer, ByVal ramo As Integer, ByVal sub_grupo_id As Integer, ByVal nome As String, ByVal cpf As String, ByVal dt_nascimento As String, ByVal dt_admissao As String, ByVal sexo As String, ByVal val_capital_segurado As String, ByVal val_salario As String, ByVal operacao As Char)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        bd.SEGS5655_SPS(apolice, ramo, 6785, 0, sub_grupo_id, cUtilitarios.destrataCPF(cpf), "null")

        Try
            dr = bd.ExecutaSQL_DR()

            If Not dr Is Nothing Then
                If dr.Read Then

                    If dr.GetValue(4).ToString = "Titular" And dr.GetValue(2).ToString = cUtilitarios.destrataCPF(cpf) Then
                        cUtilitarios.br("J� existe um titular com o cpf informado.")

                    Else
                        bd.SEGS5656_SPI(0, 0, apolice, 0, 6785, ramo, sub_grupo_id, dt_admissao, nome, 1, cpf, dt_nascimento, sexo, val_capital_segurado, val_salario, "getDate()", Session("usuario"), "n", "n", 0)

                        Try
                            bd.ExecutaSQL()

                            'vitor.cabral - Nova Consultoria - 17/12/2013
                            '17884362 - Lista de Mlehorias SEGBR (PPE) - In�cio
                            mInsereSeguradoPPE(ramo, apolice, sub_grupo_id, cpf, 1, dt_admissao, Session("usuario"), getOperacao())
                            If isPPE <> Nothing And isPPE > 0 Then
                                cUtilitarios.br("Vida incluida com restri��o de PPE(Pessoa Politicamente Exposta).\nSomente uma pessoa autorizada, poder� aceitar essa inclus�o.")
                            End If
                            'vitor.cabral - Nova Consultoria - 17/12/2013 - Fim

                            Session("alterado") = 1
                            cUtilitarios.escreveScript("top.document.id_selecionado_SEGW0065 = 1;")
                            If IsPendente(cpf) = False Then
                                cUtilitarios.escreveScript("parent.parent.GB_showConfirm();")
                            Else
                                cUtilitarios.escreveScript("parent.parent.GB_showConfirmP();")
                            End If

                        Catch ex As Exception
                            Dim excp As New clsException(ex)
                        End Try
                    End If

                Else
                    bd.SEGS5656_SPI(0, 0, apolice, 0, 6785, ramo, sub_grupo_id, dt_admissao, nome, 1, cpf, dt_nascimento, sexo, val_capital_segurado, val_salario, "getDate()", Session("usuario"), "n", "n", 0)

                    Try
                        bd.ExecutaSQL()

                        'vitor.cabral - Nova Consultoria - 17/12/2013
                        '17884362 - Lista de Mlehorias SEGBR (PPE) - In�cio
                        mInsereSeguradoPPE(ramo, apolice, sub_grupo_id, cpf, 1, dt_admissao, Session("usuario"), getOperacao())
                        If isPPE <> Nothing And isPPE > 0 Then
                            cUtilitarios.br("Vida incluida com restri��o de PPE(Pessoa Politicamente Exposta).\nSomente uma pessoa autorizada, poder� aceitar essa inclus�o.")
                        End If
                        'vitor.cabral - Nova Consultoria - 17/12/2013 - Fim

                        Session("alterado") = 1
                        cUtilitarios.escreveScript("top.document.id_selecionado_SEGW0065 = 1;")

                        If IsPendente(cpf) = False Then
                            cUtilitarios.escreveScript("parent.parent.GB_showConfirm();")
                        Else
                            cUtilitarios.escreveScript("parent.parent.GB_showConfirmP();")
                        End If

                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If

        Catch ex As Exception
            If Not dr Is Nothing Then
                If Not dr.IsClosed Then dr.Close()
            End If
            dr = Nothing
            Dim excp As New clsException(ex)
        End Try

    End Sub




    Sub PreencheDataSetDeDataReader(ByVal ds As DataSet, ByVal table As String, ByVal dr As IDataReader)
        ' Cria um  xxxDataAdapter do mesmo tipo de um DataReader
        Dim tipoDataReader As Type = CObj(dr).GetType
        Dim nomeTipo As String = tipoDataReader.FullName.Replace("DataReader", "DataAdapter")
        Dim tipoDataAdapter As Type = tipoDataReader.Assembly.GetType(nomeTipo)
        Dim da As Object = Activator.CreateInstance(tipoDataAdapter)
        ' invoca o m�todo protegido Fill que toma um objeto IDataReader
        Dim args() As Object = {ds, table, dr, 0, 999999}
        tipoDataAdapter.InvokeMember("Fill", BindingFlags.InvokeMethod Or BindingFlags.NonPublic Or BindingFlags.Instance, Nothing, da, args)
        ' fecha o DataReader
        dr.Close()
    End Sub

    Private Function IsPendente(ByVal cpf As String) As Boolean
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim result As Boolean = False
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        bd.SEGS6570_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

        Try
            dr = bd.ExecutaSQL_DR()
            Dim v_oDados As New Data.DataSet
            PreencheDataSetDeDataReader(v_oDados, "DadosCpfsPendentes", dr)

            If Not IsNothing(v_oDados.Tables(0)) Then
                For Each v_oRow As Data.DataRow In v_oDados.Tables(0).Rows
                    If v_oRow("cpf") = cUtilitarios.destrataCPF(cpf.Trim) Then
                        result = True
                    End If
                Next
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return result

    End Function

    Private Sub mInsereSeguradoConjuge(ByVal apolice As Integer, ByVal ramo As Integer, ByVal sub_grupo_id As Integer, ByVal nome As String, ByVal cpf As String, ByVal dt_nascimento As String, ByVal dt_admissao As String, ByVal sexo As String, ByVal val_capital_segurado As String, ByVal val_salario As String, ByVal idAnterior As Integer)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5656_SPI(0, 0, apolice, 0, 6785, ramo, sub_grupo_id, dt_admissao, nome, 3, cpf, dt_nascimento, sexo, val_capital_segurado, val_salario, "getDate()", Session("usuario"), "n", "n", idAnterior)

        Try

            bd.ExecutaSQL()
            Session("alterado") = 1
            cUtilitarios.escreveScript("top.document.id_selecionado_SEGW0065() = 1;")
            cUtilitarios.escreveScript("parent.parent.GB_reloadOnClose('true');")
            cUtilitarios.escreveScript("parent.parent.GB_hide();")
            cUtilitarios.br("C�njuge inclu�do com sucesso")
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Public Function getCondicoesSubgrupo() As String()
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim param(6) As String

        bd.SEGS5705_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

        Try

            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    param(0) = IIf(dr.GetValue(1).ToString.Trim = "", " --- ", dr.GetValue(1).ToString.Trim)
                    param(1) = IIf(dr.GetValue(0).ToString.Trim = "", " --- ", dr.GetValue(0).ToString.Trim)
                Else
                    param(0) = " --- "
                    param(1) = " --- "
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        bd.SEGS5706_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

        Try
            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    param(2) = IIf(dr.GetValue(2).ToString.Trim = "", " --- ", dr.GetValue(2).ToString.Trim)
                    param(3) = IIf(dr.GetValue(1).ToString.Trim = "", " --- ", dr.GetValue(1).ToString.Trim)
                    param(4) = IIf(dr.GetValue(0).ToString.Trim = "", " --- ", dr.GetValue(0).ToString.Trim)
                Else
                    param(2) = " --- "
                    param(3) = " --- "
                    param(4) = " --- "
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        bd.SEGS5706_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

        Try
            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    param(2) = IIf(dr.GetValue(2).ToString.Trim = "", " --- ", dr.GetValue(2).ToString.Trim)
                    param(3) = IIf(dr.GetValue(1).ToString.Trim = "", " --- ", dr.GetValue(1).ToString.Trim)
                    param(4) = IIf(dr.GetValue(0).ToString.Trim = "", " --- ", dr.GetValue(0).ToString.Trim)
                Else
                    param(2) = " --- "
                    param(3) = " --- "
                    param(4) = " --- "
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        bd.SEGS5744_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
        Dim valor_capital_global As String = "0"

        Try
            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    param(5) = IIf(dr.GetValue(0).ToString.Trim = "", " --- ", cUtilitarios.trataMoeda(dr.GetValue(0).ToString.Trim))
                    param(6) = IIf(dr.GetValue(4).ToString.Trim = "", " --- ", cUtilitarios.trataMoeda(dr.GetValue(4).ToString.Trim))
                Else
                    param(5) = " --- "
                    param(6) = " --- "
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return param

    End Function

    Public Function getdataApolice() As String
        Dim cRetorno As String = ""
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        bd.getDataIniVigenciaApolice(Session("apolice"), Session("ramo"))

        Try

            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    Return dr.GetValue(0).ToString
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return cRetorno
    End Function

    Private Function getDadosSegurado(ByVal id As String) As Segurado
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        Try

            Try
                If Not (id = Nothing) Then
                    If (id.Length = 0) Then
                        id = 0
                    End If
                Else
                    id = 0
                End If
            Catch ex As Exception
                Dim excp As New clsException(ex)
                id = 0
            End Try

            bd.SEGS5655_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "null", "null", id, "null", "1")

            Dim seg As New Segurado
            Dim dr As Data.SqlClient.SqlDataReader = Nothing

            Try
                dr = bd.ExecutaSQL_DR

                If Not dr Is Nothing Then
                    If dr.Read Then
                        ' In�cio Altera��o - Monica Randolfi - INC000004527394
                        ' Corre��o para obter a data de in�cio de vig�ncia do subgrupo
                        'If Not IsDBNull(dr.Item("dt_entrada_sbg")) Then
                        If Not IsDBNull(dr.Item("dt_inicio_vigencia_sbg")) Then
                            'seg.dt_entrada = dr.Item("dt_entrada_sbg").ToString().Split(" ")(0)                                
                            seg.dt_entrada = dr.Item("dt_inicio_vigencia_sbg").ToString().Split(" ")(0)
                        Else
                            seg.dt_entrada = ""
                        End If
                        ' Fim Altera��o - INC000004527394

                        If Not IsDBNull(dr.Item("val_capital_segurado")) Then
                            seg.capital = dr.Item("val_capital_segurado")
                        Else
                            seg.capital = ""
                        End If

                    End If
                    If Not dr.IsClosed Then dr.Close()
                    dr = Nothing
                End If
            Catch ex As Exception
                Dim excp As New clsException(ex)
            End Try

            Return seg
        Catch ex As Exception
            Dim excp As New clsException(ex)
            Return Nothing
        End Try
    End Function

    Public Function getValCapital(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As String) As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim val_capital As String = "0.00"
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        Dim val_lim_max_is As Double
        Dim val_lim_cap_global As Double
        Dim val_is_basica As Double
        Dim num_segurados As Double

        Try

            If getCapitalSeguradoSession() = "Capital Global" Then
                bd.SEGS5744_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

                Try
                    dr = bd.ExecutaSQL_DR()
                    If Not dr Is Nothing Then
                        val_capital = "10000"
                        If dr.Read Then
                            Try
                                val_lim_max_is = dr.GetValue(0).ToString.Trim()
                                val_lim_cap_global = dr.GetValue(1).ToString.Trim()
                                val_is_basica = dr.GetValue(2).ToString.Trim()
                                num_segurados = dr.GetValue(3).ToString.Trim()

                                If num_segurados > 0 Then

                                    val_capital = val_is_basica / (num_segurados + 1)

                                    If val_lim_max_is > 0 And val_lim_max_is < val_capital Then
                                        val_capital = val_lim_max_is
                                    End If
                                End If
                            Catch ex As Exception
                                Dim excp As New clsException(ex)
                            End Try
                        End If
                        If Not dr.IsClosed Then dr.Close()
                        dr = Nothing
                    End If

                    Return val_capital
                Catch ex As Exception
                    Dim excp As New clsException(ex)
                    Return "10000"
                End Try
            Else
                val_capital = "10000"
                Try
                    bd.SEGS5773_SPS(apolice_id, ramo_id, 6785, 0, subgrupo_id)

                    dr = bd.ExecutaSQL_DR()

                    If Not dr Is Nothing Then
                        If dr.Read Then
                            val_capital = dr.GetValue(0).ToString

                            If val_capital <= 0 Then
                                bd.SEGS5744_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

                                dr = bd.ExecutaSQL_DR()

                                If dr.Read Then
                                    Try
                                        val_capital = dr.GetValue(0).ToString.Trim()
                                    Catch ex As Exception
                                        Dim excp As New clsException(ex)
                                    End Try
                                End If
                            End If
                        End If
                        If Not dr.IsClosed Then dr.Close()
                        dr = Nothing
                    End If
                Catch ex As Exception
                    Dim excp As New clsException(ex)
                End Try
            End If

            Return val_capital

        Catch ex As Exception
            Dim excp As New clsException(ex)
            Return Nothing
        End Try
    End Function

    Public Function getValCapitalParaAlteracao(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As String, ByVal id As Integer) As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        Dim val_capital As String = "0.00"
        Dim dr As Data.SqlClient.SqlDataReader

        Try
            If getCapitalSeguradoSession() = "Capital Global" Then
                Try
                    bd.SEGS5655_SPS(apolice_id, ramo_id, 6785, 0, subgrupo_id, "null", "null", id)

                    dr = bd.ExecutaSQL_DR()

                    If Not dr Is Nothing Then
                        If dr.Read Then
                            val_capital = dr.GetValue(8).ToString
                        End If
                        If Not dr.IsClosed Then dr.Close()
                        dr = Nothing
                    End If
                Catch ex As Exception
                    Dim excp As New clsException(ex)
                End Try
                Return val_capital
            Else
                Try
                    bd.SEGS5773_SPS(apolice_id, ramo_id, 6785, 0, subgrupo_id)

                    dr = bd.ExecutaSQL_DR()

                    If Not dr Is Nothing Then
                        If dr.Read Then
                            val_capital = dr.GetValue(0).ToString

                            If val_capital <= 0 Then
                                bd.SEGS5744_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

                                dr = bd.ExecutaSQL_DR()
                                If dr.Read Then
                                    Try
                                        val_capital = dr.GetValue(0).ToString.Trim()
                                    Catch ex As Exception
                                        Dim excp As New clsException(ex)
                                        val_capital = "10000"
                                    End Try
                                Else
                                    val_capital = "10000"
                                End If

                            End If
                        End If
                        If Not dr.IsClosed Then dr.Close()
                        dr = Nothing
                    End If
                Catch ex As Exception
                    Dim excp As New clsException(ex)
                End Try
            End If

            Return val_capital
        Catch ex As Exception
            Dim excp As New clsException(ex)
            Return Nothing
        End Try
    End Function

    Private Function getCapitalConjuge(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As String, ByVal titular_id As String) As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        bd.getCapitalConjuge(apolice_id, ramo_id, subgrupo_id)
        Dim segurado As Segurado = getDadosSegurado(titular_id)

        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        Try
            dr = bd.ExecutaSQL_DR()

            If Not dr Is Nothing Then
                If dr.Read Then
                    Return segurado.capital * dr.GetValue(0).ToString
                Else
                    Return 0
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
        Return 0
    End Function

    Public Sub disable(ByVal alvo As Web.UI.WebControls.TextBox)
        alvo.Style.Add("color", "#AAAAAA")
        alvo.Style.Add("background-color", "#FFFFFF")
        alvo.ReadOnly = True
    End Sub

    Public Sub enable(ByVal alvo As Web.UI.WebControls.TextBox)
        alvo.Style.Add("color", "#000000")
        alvo.Style.Add("background-color", "#FFFFFF")
        alvo.ReadOnly = False
    End Sub

    Private Function VerificaCapitalSegurado() As Boolean
        Try
            'Faz a verificacao do capital, ver documento Regra de Alert do DPS no \\web\wwwroot
            If txtTitCapital.Text <> String.Empty Then
                Dim capital As Double = Convert.ToDouble(txtTitCapital.Text)
                If capital > 100000 And capital <= 500000 Then
                    ''Novo texto. 30/04/07
                    cUtilitarios.br("Em raz�o do capital segurado pretendido, esclarecemos que ser� necess�rio o preenchimento de Proposta de Ades�o com Declara��o Pessoal de Sa�de para este proponente. Para baixar os modelos, clique no Menu=>Download e sigas as instru��es.")

                    Return False
                ElseIf capital > 500000 Then
                    cUtilitarios.br("Esclaremos que ser� necess�rio o preenchimento do Cart�o Proposta com Declara��o Pessoal de Sa�de (DPS) e da Ficha Financeira para a inclus�o do referido proponente.  Para isso imprima o modelo no menu Download e siga as instru��es contidas no corpo do documento. Somente ap�s a an�lise das informa��es do proponente em quest�o, bem como o parecer favor�vel da Companhia, ser� autorizada essa inclus�o.")
                    Return False

                End If
            End If
            Return True
        Catch ex As Exception
            Dim excp As New clsException(ex)
            Return False
        End Try

    End Function

    Private Function VerificaCapitalSegurado(ByVal valor_capital As String) As Boolean
        Try
            'Faz a verificacao do capital, ver documento Regra de Alert do DPS no \\web\wwwroot
            If valor_capital <> String.Empty Then
                Dim capital As Double = Convert.ToDouble(valor_capital)
                If capital > 100000 And capital <= 500000 Then
                    cUtilitarios.br("Esclarecemos que ser� necess�rio o preenchimento do Cart�o Proposta com Declara��o Pessol de Sa�de (DPS) para a inclus�o do referido proponente.  Para isso imprima o modelo no menu Download e siga as instru��es contidas no corpo do documento.\n\nSomente ap�s a an�lise das informa��es do proponente em quest�o, bem como o parecer favor�vel da Companhia, ser� autorizada essa inclus�o.")
                    Return False
                ElseIf capital > 500000 Then
                    cUtilitarios.br("Esclaremos que ser� necess�rio o preenchimento do Cart�o Proposta com Declara��o Pessoal de Sa�de (DPS) e da Ficha Financeira para a inclus�o do referido proponente.  Para isso imprima o modelo no menu Download e siga as instru��es contidas no corpo do documento. Somente ap�s a an�lise das informa��es do proponente em quest�o, bem como o parecer favor�vel da Companhia, ser� autorizada essa inclus�o.")
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            Dim excp As New clsException(ex)
            Return False
        End Try
    End Function

    Function getIdadeMinApolice() As Integer
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim idadeMin As Integer = 0
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        bd.SEGS5705_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

        Try

            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    idadeMin = IIf(dr.GetValue(0).ToString.Trim = "", "0", dr.GetValue(0).ToString.Trim)
                End If

                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
        Return idadeMin
    End Function

    Private Function getCapitalSeguradoUltimoFaturamento() As Decimal
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS7594_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), Me.hdnPropClienteId.Value)

        Dim v_oDtValores As DataTable = bd.ExecutaSQL_DT
        If v_oDtValores.Rows.Count > 0 Then
            If Not IsNothing(v_oDtValores) Then
                Return v_oDtValores.Rows(0)("val_capital_segurado")
            Else
                Return 0
            End If
        Else
            Return 0
        End If
    End Function

    Public Function getCapitalSeguradoUsuario(ByVal id As Integer) As Double
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5655_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "null", "null", id)
        Dim count As Int16 = 1
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        Try
            dr = bd.ExecutaSQL_DR
            If Not dr Is Nothing Then
                If dr.Read Then
                    Return dr.GetValue(8).ToString()
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Function

    Public Function FillTpCapitalSegurado(ByVal tpCapitalSegurado As String) As String
        Dim cRetorno As String = ""
        Try
            Select Case tpCapitalSegurado
                Case "M�ltiplo sal�rio"
                    cRetorno = "S"
                Case "Capital fixo"
                    cRetorno = "F"
                Case "Capital informado"
                    cRetorno = "C"
                Case "Capital Global"
                    cRetorno = "G"
                Case "Capital por faixa et�ria"
                    cRetorno = "E"
            End Select
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
        Return cRetorno
    End Function

    'vitor.cabral - Nova Consultoria - 17/12/2013 - In�cio
    Public Sub mInsereSeguradoPPE(ByVal ramo As Integer, ByVal apolice As Integer, ByVal sub_grupo_id As Integer, ByVal cpf As String, ByVal tp_componente_id As Integer, ByVal dt_inicio_vigencia_sbg As String, ByVal usuario As String, ByVal operacao As String)

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim dtInicioVigenciaApl As String = Nothing
        Dim dtFimVigenciaApl As String = Nothing

        bd.recuperaPeriodoVigenciaApl(apolice, ramo, sub_grupo_id)
        Try
            dr = bd.ExecutaSQL_DR
            If Not dr Is Nothing Then
                If dr.Read Then
                    dtInicioVigenciaApl = dr.GetDateTime(0)
                    dtFimVigenciaApl = dr.GetDateTime(1)
                    dr = Nothing
                End If
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        bd.SEGS11643_SPI(ramo, apolice, sub_grupo_id, cpf, tp_componente_id, dtInicioVigenciaApl, dtFimVigenciaApl, dt_inicio_vigencia_sbg, usuario, operacao)
        Try
            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read() Then
                    isPPE = dr.GetInt32(0)
                End If
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub
    'vitor.cabral - Nova Consultoria - 17/12/2013 - Fim

    'vitor.cabral - Nova Consultoria - 08/01/2014 - Inicio
    Public Function getOperacao() As String
        Dim operacao As String = Request("acao").ToString

        If operacao = "incluir" Or operacao = "incluirdependente" Then
            operacao = "I"

        ElseIf operacao = "alterar" Or operacao = "alterardependente" Then
            operacao = "A"

        ElseIf operacao = "excluir" Or operacao = "excluirdependente" Or operacao = "exclui" Then
            operacao = "E"

        Else
            operacao = " "

        End If

        Return operacao

    End Function
    'vitor.cabral - Nova Consultoria - 08/01/2014 - Fim

End Class
