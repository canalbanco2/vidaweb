Partial Class ajaxVerifCpfExiste
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        'Try
        Dim cpf As String = ""

        Try
            cpf = Request("cpf")
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        If cpf <> "" Then
            If cpf.Trim.Length > 0 And Not verifCPFExiste(cpf) Then
                Response.Write("")
            ElseIf cpf.Trim.Length > 0 And verifCPFExiste(cpf) Then
                Response.Write("EXISTE")
            Else
                Response.Write("")
            End If
        End If

        Response.End()
        'Catch ex As Exception
        '    Dim excp As New clsException(ex)
        'End Try

    End Sub

    Public Function verifCPFExiste(ByVal cpf As String) As Boolean

        Try
            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

            bd.SEGS5655_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subGrupo_id"), cUtilitarios.destrataCPF(cpf), "null")

            Dim dr As Data.SqlClient.SqlDataReader = Nothing

            Try
                dr = bd.ExecutaSQL_DR()
                If Not dr Is Nothing Then
                    If dr.Read Then
                        If dr.GetValue(4).ToString = "Titular" And dr.GetValue(2).ToString = cUtilitarios.destrataCPF(cpf) Then
                            Return True
                        End If
                    End If

                    If Not dr.IsClosed Then dr.Close()
                    dr = Nothing
                End If
            Catch ex As Exception
                Dim excp As New clsException(ex)
            End Try

            Return False

        Catch ex As Exception
            Dim excp As New clsException(ex)
            Return False
        End Try
    End Function

End Class
