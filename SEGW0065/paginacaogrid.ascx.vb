Imports System.Drawing
Imports System.Web.UI.WebControls
Imports System.Data

Partial Class paginacaogrid
    Inherits System.Web.UI.UserControl

    Public Shared iNumPaginas As Integer

    Public valor As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub

    Public Event ePaginaAlterada(ByVal Argumento As String)
    Public Event ePaginaAlteradaA(ByVal Argumento As String)

    Private Sub mDisparaEvento(ByVal Argumento As String)
        RaiseEvent ePaginaAlterada(Argumento)
    End Sub
    Private Sub mDisparaEventoA(ByVal Argumento As String)
        RaiseEvent ePaginaAlteradaA(Argumento)
    End Sub

    Private Sub mPaginacaoClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles a1.Command, a2.Command, a3.Command, a4.Command, a5.Command, a6.Command, a7.Command

        If CType(sender, LinkButton).Text = "Pr�xima" Then
            Session("pagina") = CType(Session("pagina"), Integer) + 1
        ElseIf CType(sender, LinkButton).Text = "Anterior" Then
            Session("pagina") = CType(Session("pagina"), Integer) - 1
        Else
            Session("pagina") = CType(CType(sender, LinkButton).Text, Integer) - 1
        End If

        mDisparaEvento(e.CommandArgument)

        mMontaPaginacao(Session("pagina"), iNumPaginas)

    End Sub

    Private Sub aPaginacaoClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles a1.Command, a2.Command, a3.Command, a4.Command, a5.Command, a6.Command, a7.Command
        mDisparaEventoA(e.CommandArgument)
    End Sub

    Private Sub mMontaPaginacao(ByVal GridPaginaAtual As Integer, ByVal GridPaginasTotal As Integer)
        Dim paginainicial As Integer
        Dim paginafinal As Integer
        Dim paginaatual As Integer = GridPaginaAtual + 1

        If (paginaatual / 5) > 0 Then
            If (paginaatual Mod 5) <> 0 Then
                paginainicial = (Int(paginaatual / 5) * 5) + 1
            Else
                paginainicial = (Int((paginaatual - 1) / 5) * 5) + 1
            End If
            paginafinal = paginainicial + 4
            If paginafinal > GridPaginasTotal Then paginafinal = GridPaginasTotal
        Else
            paginainicial = 1
            paginafinal = 5
            If paginafinal > GridPaginasTotal Then paginafinal = GridPaginasTotal
        End If

        Me.a1.Visible = False
        Me.a2.Visible = False
        Me.a3.Visible = False
        Me.a4.Visible = False
        Me.a5.Visible = False
        Me.a6.Visible = False
        Me.a7.Visible = False
        If paginaatual > 1 Then
            a1.Text = "Anterior"
            a1.ForeColor = Color.White
            a1.Style.Add("font-family", "verdana")
            a1.Style.Add("font-size", "10px")
            a1.Style.Add("font-weight", "bold")
            a1.Style.Add("text-decoration", "underline")
            a1.Visible = True
        End If
        Dim a As Integer = 2
        For i As Integer = paginainicial To paginafinal
            CType(Me.FindControl("a" + a.ToString), LinkButton).CommandArgument = (i - 1).ToString
            CType(Me.FindControl("a" + a.ToString), LinkButton).Text = i.ToString
            CType(Me.FindControl("a" + a.ToString), LinkButton).Visible = True
            If i = paginaatual Then
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("color", "white")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("text-decoration", "none")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-family", "verdana")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-size", "10px")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-weight", "bold")

            Else
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("color", "white;")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("text-decoration", "underline")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-family", "verdana")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-size", "10px")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-weight", "bold")

            End If
            a += 1
        Next
        If paginaatual <> GridPaginasTotal Then
            a7.Text = "Pr�xima"
            a7.ForeColor = Color.White
            a7.Style.Add("font-family", "verdana")
            a7.Style.Add("font-size", "10px")
            a7.Style.Add("font-weight", "bold")
            a7.Style.Add("text-decoration", "underline")
            a7.Visible = True
        End If
    End Sub

    Public Sub GridDataBind(ByVal Grid As DataGrid, ByVal dt As DataTable)
        Try
            Grid.DataSource = dt
            Grid.DataBind()
        Catch ex As Exception
            'n�o enviar nenhum tratamento, pois a p�gina do grid exibida n�o vale para o conte�do do DataTable
        End Try

        Try
            If Grid.PageCount > 0 Then
                If (CInt(Session("pagina"))) > (Grid.PageCount - 1) Then
                    Session("pagina") = Grid.PageCount - 1
                End If
            Else
                Session("pagina") = 0
            End If
        Catch ex As Exception
            Session("pagina") = 0
        End Try

        Try
            Me.mMontaPaginacao(Session("pagina"), Grid.PageCount)

            'Dim tipo As String
            Dim cell As Web.UI.WebControls.DataGridItem = Nothing
            Dim pai As Web.UI.WebControls.DataGridItem
            Dim count As Int16 = 0
            Dim pai_id As String = ""
            Dim count2 As Integer = 0
            Dim w As Integer = 1
            Dim t As Integer = 1
            Dim primeiro As Boolean = True
            Dim CapitalGlobal As Boolean = False

            Dim tamanho(17) As Integer
            Dim textoCell As String = ""
            Dim txtMax(17) As String
            Dim nIndex As Integer = 0

            tamanho(0) = 10
            tamanho(1) = 11
            tamanho(2) = 40
            tamanho(3) = 12
            tamanho(4) = 8
            tamanho(5) = 11
            tamanho(6) = 5
            tamanho(7) = 10
            tamanho(8) = 15
            tamanho(9) = 10
            tamanho(10) = 10
            For nIndex = 11 To 17
                tamanho(nIndex) = 15
            Next

            For nIndex = 0 To 17
                txtMax(nIndex) = ""
            Next

            CapitalGlobal = (cUtilitarios.getCapitalSeguradoSession() = "Capital Global")
            w = IIf(cUtilitarios.MostraColunaSalario(), 1, 0)
            t = IIf(cUtilitarios.MostraColunaCapital(), 1, 0)

            If Grid.Items.Count > 0 Then

                For Each x As Web.UI.WebControls.DataGridItem In Grid.Items

                    If Not cell Is Nothing Then
                        If x.Cells.Item(4).Text <> "Titular" And cell.Cells.Item(4).Text = "Titular" Then
                            cell.Cells.Item(3).Text = "<img src='../segw0060/Images/imgMenos.gif' onclick=exibeDependentes('" + pai_id + "',this);>&nbsp;&nbsp;" & cell.Cells.Item(3).Text
                        End If
                    End If

                    cell = x
                    If x.Cells.Item(4).Text = "Titular" Then
                        pai = x
                        x.ID = count2
                        pai_id = x.ID

                        x.Attributes.Add("onmouseover", "mO('#E8F1FF', this)")
                        x.Attributes.Add("onmouseout", "mO('#FFFFFF', this)")

                        count = 0
                        count2 += 1
                    Else
                        x.Cells.Item(3).Attributes.Add("style", "padding-left:20px;")
                        x.ID = pai_id + "_" + count.ToString
                        x.Attributes.Add("style", "background-color:#f5f5f5;")
                        x.Attributes.Add("onmouseover", "mO('#E8F1FF', this)")
                        x.Attributes.Add("onmouseout", "mO('#f5f5f5', this)")

                        count = count + 1
                    End If

                    For i As Integer = 0 To x.Cells.Count - 1
                        x.Cells.Item(i).ID = "cell_" & i
                        x.Cells.Item(i).Wrap = False
                    Next

                    Dim exclusao_agendada As Integer = 0
                    Dim acertos As Integer = 0
                    Dim ind_operacao_origem As String = ""
                    Dim ultimo_alterado As Integer = 0
                    Dim pendente_aceito As Integer = 0
                    Dim recusado As String = "n"

                    Try
                        If Not (String.IsNullOrEmpty(x.Cells.Item(12).Text.Trim)) Then
                            If (x.Cells.Item(12).Text.Trim = "" Or x.Cells.Item(12).Text.Trim = "&nbsp;") Then
                                exclusao_agendada = 0
                            Else
                                exclusao_agendada = Int32.Parse(x.Cells.Item(12).Text.Trim)
                            End If
                        End If
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try

                    Try
                        If Not (String.IsNullOrEmpty(x.Cells.Item(13).Text.Trim)) Then
                            If (x.Cells.Item(13).Text.Trim = "" Or x.Cells.Item(13).Text.Trim = "&nbsp;") Then
                                acertos = 0
                            Else
                                acertos = Int32.Parse(x.Cells.Item(13).Text.Trim)
                            End If
                        End If
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try

                    Try
                        If Not (String.IsNullOrEmpty(x.Cells.Item(14).Text.Trim)) Then
                            If (x.Cells.Item(14).Text.Trim = "" Or x.Cells.Item(14).Text.Trim = "&nbsp;") Then
                                ind_operacao_origem = 0
                            Else
                                ind_operacao_origem = x.Cells.Item(14).Text.Trim
                            End If
                        End If
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try

                    Try
                        If Not (String.IsNullOrEmpty(x.Cells.Item(15).Text.Trim)) Then
                            If (x.Cells.Item(15).Text.Trim = "" Or x.Cells.Item(15).Text.Trim = "&nbsp;") Then
                                ultimo_alterado = 0
                            Else
                                ultimo_alterado = x.Cells.Item(15).Text.Trim
                            End If
                        End If
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try

                    Try
                        If Not (String.IsNullOrEmpty(x.Cells.Item(16).Text.Trim)) Then
                            If (x.Cells.Item(16).Text.Trim = "" Or x.Cells.Item(16).Text.Trim = "&nbsp;") Then
                                pendente_aceito = 0
                            Else
                                pendente_aceito = x.Cells.Item(16).Text.Trim
                            End If
                        End If
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try

                    Try
                        If (x.Cells.Count >= 18) Then
                            If Not (String.IsNullOrEmpty(x.Cells.Item(17).Text.Trim)) Then
                                If (x.Cells.Item(17).Text.Trim = "" Or x.Cells.Item(17).Text.Trim = "&nbsp;") Then
                                    recusado = 0
                                Else
                                    recusado = x.Cells.Item(17).Text.Trim
                                End If
                            End If
                        End If
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try

                    If ultimo_alterado = 0 Then
                        Response.Write("<input type='hidden' value='grdPesquisa_" & x.ID & "' id='ultimo_alterado' name='ultimo_alterado' >")
                    End If


                    If x.Cells.Item(1).Text = "Hist&oacute;rico" Then
                        x.Cells.Item(1).Text = "&nbsp;"

                        If (exclusao_agendada > 0) Then
                            x.Cells.Item(1).Text = "Exclus&atilde;o Agendada"
                        End If
                        If (recusado.ToLower = "s") Then
                            x.Cells.Item(1).Text = x.Cells.Item(1).Text & "/Recusado"
                        End If
                    Else
                        If (acertos > 0) Then
                            x.Cells.Item(1).Text = x.Cells.Item(1).Text & "/Acertos"
                        End If
                        'If (exclusao_agendada > 0) Then
                        '    x.Cells.Item(1).Text = x.Cells.Item(1).Text & "/Exclus&atilde;o Agendada"
                        'End If
                        'Demanda 14582228 - Edilson Silva - 22/11/2012''''''''''''''''''''''''''''''''''
                        If (exclusao_agendada > 0) And x.Cells.Item(1).Text.Trim() <> "Exclus&atilde;o Sinistro" Then
                            x.Cells.Item(1).Text = x.Cells.Item(1).Text & "/Exclus&atilde;o Agendada"
                        End If
                        If (recusado.ToLower = "s") Then
                            x.Cells.Item(1).Text = x.Cells.Item(1).Text & "/Recusado"
                        End If
                    End If

                    If x.Cells.Item(1).Text.IndexOf("Pend") <> -1 Then
                        If (ind_operacao_origem = "I") Then
                            x.Cells.Item(1).Text = x.Cells.Item(1).Text & "/Inclus�o"
                        ElseIf (ind_operacao_origem = "E") Then
                            x.Cells.Item(1).Text = x.Cells.Item(1).Text & "/Exclus�o"
                        Else
                            x.Cells.Item(1).Text = x.Cells.Item(1).Text & "/Altera��o"
                        End If
                    End If

                    x.Cells.Item(1).Text = "<div nowrap>" + x.Cells.Item(1).Text + "</div>"

                    For i As Integer = 0 To x.Cells.Count - 1
                        textoCell = x.Cells.Item(i).Text.ToString.Replace("&atilde;", "a").Replace("&ccedil;", "c")
                        If tamanho(i) <= textoCell.Length Then
                            tamanho(i) = textoCell.Length
                            txtMax(i) = x.Cells.Item(i).Text
                        End If
                        If i = 3 Then
                            tamanho(i) = 25
                            txtMax(i) = "".PadLeft(25, "O")
                        End If
                    Next

                    x.Cells.Item(5).Text = cUtilitarios.trataCPF(x.Cells.Item(5).Text)

                    If w = 0 Then
                        x.Cells.Item(9).Style.Add("display", "none")
                    Else
                        x.Cells.Item(9).Text = cUtilitarios.trataMoeda(x.Cells.Item(9).Text).Trim
                        x.Cells.Item(9).HorizontalAlign = HorizontalAlign.Right
                    End If

                    If t = 0 Then
                        x.Cells.Item(10).Style.Add("display", "none")
                    Else
                        x.Cells.Item(10).Text = cUtilitarios.trataMoeda(x.Cells.Item(10).Text).Trim
                        x.Cells.Item(10).HorizontalAlign = HorizontalAlign.Right
                    End If

                    x.Attributes.Add("onclick", "alteracoes('" & x.Cells.Item(0).Text & "', '" & x.Cells.Item(4).Text & "', '" & x.Cells.Item(3).Text & "','" + x.Cells.Item(0).Text + "', '" & x.ID & "', '" + x.Cells.Item(1).Text & "','" + IIf(x.Cells.Item(11).Text = 1, x.Cells.Item(11).Text, "0") + "', " & pendente_aceito & ", " & IIf(CapitalGlobal = True, 1, 0) & ")")

                    x.Cells.Item(2).Text = x.Cells.Item(2).Text.ToUpper

                    If (x.Cells.Item(3).Text.Length > 25) Then
                        x.Cells.Item(3).Text = "<span onmouseover=""return overlib('" & x.Cells.Item(3).Text & "');"" onmouseout='return nd();' style='width:100%'>" & x.Cells.Item(3).Text.Substring(0, 25) & "...</span>"
                    Else
                        x.Cells.Item(3).Text = "<span onmouseover=""return overlib('" & x.Cells.Item(3).Text & "');"" onmouseout='return nd();' style='width:100%'>" & x.Cells.Item(3).Text & "</span>"
                    End If

                    If primeiro Then
                        primeiro = False
                        For i As Integer = 0 To x.Cells.Count - 1
                            x.Cells.Item(i).Text = "<div class='headerFakeDiv'id='headerFake" & i & "'></div>" & x.Cells.Item(i).Text
                        Next
                    End If

                Next

                If w = 0 Then
                    Grid.Columns.Item(9).HeaderStyle.CssClass = "displayNone"
                End If

                Dim z As Integer = 0
                Dim texto As String

                Dim tamMin(17) As Integer
                tamMin(0) = 9
                tamMin(1) = 9
                tamMin(2) = 10
                tamMin(3) = 12
                tamMin(4) = 5
                tamMin(5) = 13
                tamMin(6) = 10
                tamMin(7) = 4
                tamMin(8) = 15
                tamMin(9) = 12
                tamMin(10) = 12
                For nIndex = 11 To 17
                    tamMin(nIndex) = 15
                Next

                Try
                    For Each m As String In txtMax
                        texto = txtMax(z).ToString.Trim.Replace(" ", "O").Replace("-", "O").Replace("&atilde;", "a").Replace("&ccedil;", "c").PadRight(tamMin(z), "O")
                        Response.Write("<input type='hidden' value='" & texto & "' id='txtMax" & z & "'>" & vbCrLf)
                        z = z + 1
                    Next
                Catch ex As Exception
                    Dim excp As New clsException(ex)
                End Try
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Public Property CssClass() As String
        Get
            Return Me.lblPaginacao.Attributes.Item("class")
        End Get
        Set(ByVal Value As String)
            Me.lblPaginacao.Attributes.Add("class", Value)
        End Set
    End Property

End Class
