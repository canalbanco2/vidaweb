Partial Class query
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim linkseguro As Alianca.Seguranca.Web.LinkSeguro

            'Implementação da SABL0101
            linkseguro = New Alianca.Seguranca.Web.LinkSeguro
            Dim usuario As String = linkseguro.LerUsuario("", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))
            If Request.QueryString("fatura") = "" Then
                If usuario = "" Then
                    'Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
                End If

                Session("usuario_id") = linkseguro.Usuario_ID
                Session("usuario") = linkseguro.Login_WEB
                Session("cpf") = linkseguro.CPF
            End If

            Dim cAmbiente As Alianca.Seguranca.Web.ControleAmbiente
            Dim url As String

            cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
            url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"

            cAmbiente.ObterAmbiente(url)
            If Alianca.Seguranca.BancoDados.cCon.configurado Then
                Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            Else
                Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            End If

            Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
        
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As SqlClient.SqlDataReader = Nothing

        If Me.TextBox1.Text <> "" Then
            bd.SQL = Me.TextBox1.Text

            Try

                dr = bd.ExecutaSQL_DR()

                If Not dr Is Nothing Then
                    Me.dg.DataSource = dr
                    Me.dg.DataBind()
                    If Not dr.IsClosed Then dr.Close()
                    dr = Nothing
                End If
            Catch ex As Exception
                Dim excp As New clsException(ex)
            End Try
        End If
    End Sub
End Class
