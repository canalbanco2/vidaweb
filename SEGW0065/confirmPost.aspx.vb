Partial Class confirmPost
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Try
        'Put user code to initialize the page here

        Dim script As String = "<script>" & vbCrLf
        Dim tamanho As Integer = script.Length
        Dim content As String = ""
        Dim altSal As String = Request.Form("controleAlteracaoSalario")
        Dim altCap As String = Request.Form("controleAlteracaoCapital")

        Try
            If Request.QueryString("reloaded") = "1" Then
                pnlConfirm.Visible = True

                script &= "function load() { "
                script &= " document.Form1.action= '" & Session("actionConfirm") & "';"
                script &= " document.Form1.__VIEWSTATE.name = '__VIEWSTATE_temp';"
                'document.getElementById('__VIEWSTATE').id = "__VIEWSTATE_temp";
                script &= " document.getElementById('content').innerHTML = '" & Session("contentConfirm") & "'"

                Try
                    Dim temp As String = Session("acaoConfirm")

                    Dim temp2 As String = Session("acaoMsg")

                    If temp.IndexOf("excluir") <> "-1" Then
                        If temp2.ToString.Trim.Equals("apagar") Then
                            msg.Text = "Deseja descartar a opera��o realizada?"
                        Else
                            msg.Text = "Confirma a exclus�o deste segurado?"
                        End If
                    ElseIf temp.IndexOf("alterar") <> "-1" Then
                        msg.Text = "Confirma a altera��o deste segurado?"
                    Else
                        msg.Text = "Confirma a exclus�o deste segurado?"
                    End If
                Catch ex As Exception
                    Dim excp As New clsException(ex)
                    Try
                        Dim temp As String = Session("desperato")

                        If temp.IndexOf("excluir") <> "-1" Then
                            msg.Text = "Confirma a exclus�o deste segurado?"
                        ElseIf temp.IndexOf("alterar") <> "-1" Then
                            cUtilitarios.br("2")
                            msg.Text = "Confirma a altera��o deste segurado?"
                        Else
                            msg.Text = "Confirma a exclus�o deste segurado?"
                        End If
                    Catch ex2 As Exception
                        msg.Text = "Confirma a altera��o deste segurado?"
                        Dim excp1 As New clsException(ex)
                    End Try
                End Try

            Else
                Dim tpCapitalSegurado As String

                Dim MovimentacaoUmaUm_Form As New MovimentacaoUmaUm_Form

                Dim valorCapitalSegurado As Double = IIf(Request.Form("txtTitCapital") = "", "0", Request.Form("txtTitCapital"))
                Dim strSalario As String = IIf(Request.Form("txtTitSalario") = "", "0", Request.Form("txtTitSalario"))
                Dim valorSalario As String

                Try
                    valorSalario = strSalario.Replace(".", "")
                Catch ex As Exception
                    valorSalario = 0
                    Dim excp As New clsException(ex)
                End Try

                tpCapitalSegurado = MovimentacaoUmaUm_Form.getCapitalSeguradoSession()

                If tpCapitalSegurado = "M�ltiplo sal�rio" Then
                    valorCapitalSegurado = MovimentacaoUmaUm_Form.getCapitalBySalario(valorSalario)
                Else
                    valorCapitalSegurado = IIf(Request.Form("txtTitCapital") = "", "0", Request.Form("txtTitCapital"))
                End If

                Dim sTpIsSubGrupo As String = ""

                Select Case tpCapitalSegurado
                    Case "M�ltiplo sal�rio"
                        sTpIsSubGrupo = "S"
                    Case "Capital fixo"
                        sTpIsSubGrupo = "F"
                    Case "Capital informado"
                        sTpIsSubGrupo = "C"
                    Case "Capital Global"
                        sTpIsSubGrupo = "G"
                    Case "Capital por faixa et�ria"
                        sTpIsSubGrupo = "E"
                End Select

                Dim erro As Boolean = False
                Dim passouVerificacao As Boolean = False
                Dim forcaSubmit As Boolean = False

                content = ""
                For Each m As String In Request.Form
                    If m <> "__VIEWSTATE" Then
                        If m = "idTitular" And Request.Form(m) = "" Then
                            content &= "<input type=""hidden"" value=""" & Request.Form("idSolicitado") & """ name=""idTitular"">"
                        Else
                            content &= "<input type=""hidden"" value=""" & Request.Form(m) & """ name=""" & m & """>"
                        End If
                    End If
                Next

                Session("contentConfirm") = content
                Session("desperato") = Request.Form("acao")

                If Request.Form("acao") = "alterardependente" Then

                    Dim teste As New SEGL0285.cls00459
                    Dim param() As String = MovimentacaoUmaUm_Form.getCondicoesSubgrupo()
                    Dim dt() As String = Request.Form("txtDepNascimento").Split("/")

                    If param(0).Trim = "---" Then
                        param(0) = 0
                    End If
                    If param(1).Trim = "---" Then
                        param(1) = 0
                    End If

                    If param(5).Trim = "---" Then
                        param(5) = 0
                    End If

                    If param(6).Trim = "---" Then
                        param(6) = 0
                    End If

                    Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

                    bd.getTpApoliceConjuge(Session("apolice"), Session("ramo"), Session("subGrupo_id"))

                    Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR

                    Dim retorno As String = ""
                    If dr.Read() Then
                        retorno = dr.GetValue(0).ToString()
                    End If

                    Dim idTitular As String = Request("idTitular")

                    Dim sexo As String = MovimentacaoUmaUm_Form.getSexoConjuge(idTitular)

                    Dim x As Integer = MovimentacaoUmaUm_Form.ValidarDetalhe(sTpIsSubGrupo, Now, MovimentacaoUmaUm_Form.getdataApolice(), "0", 0, 1, Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "a", Request.Form("txtDepNascimento"), DateDiff(DateInterval.Year, New Date(dt(2), dt(1), dt(0)), Now), Now, "", cUtilitarios.destrataCPF(Request.Form("txtDepCPF")), sexo, param(5), 0, 0, CInt(param(0)), CInt(param(1)), Now, , , Now, retorno)
                    If x <> 0 Then
                        cUtilitarios.br(cUtilitarios.deParaDLL(x, param(0), param(1), valorCapitalSegurado, DateDiff(DateInterval.Year, New Date(dt(2), dt(1), dt(0)), Now), Request.Form("txtTitNome")))

                        Dim strTitular As String = ""
                        If idTitular.Length > 0 Then
                            strTitular = "&idTitular=" & idTitular
                        End If

                        script &= "function load() { "
                        script &= " document.Form1.action= 'movimentacaoUmaUm_Form.aspx?acao=" & Request.Form("acao") & strTitular & "&id=" & Request.Form("idSolicitado") & "';"
                        script &= " document.getElementById('content').innerHTML = '" & Session("contentConfirm") & "';"
                        script &= " document.Form1.submit();"

                        erro = True
                    End If

                ElseIf Request.Form("acao") = "alterar" Then

                    Dim teste As New SEGL0285.cls00459

                    Dim param() As String = MovimentacaoUmaUm_Form.getCondicoesSubgrupo()
                    Dim dt() As String = Request.Form("txtTitNascimento").Split("/")

                    If param(0).Trim = "---" Then
                        param(0) = 0
                    End If
                    If param(1).Trim = "---" Then
                        param(1) = 0
                    End If

                    If param(5).Trim = "---" Then
                        param(5) = 0
                    End If

                    If param(6).Trim = "---" Then
                        param(6) = 0
                    End If

                    'vitor.cabral - Nova Consultoria - 16/12/2013
                    '17884362 - Lista de Melhorias SEGBR (PPE) - In�cio
                    If MovimentacaoUmaUm_Form.isPPE <> Nothing And MovimentacaoUmaUm_Form.isPPE > 0 Then
                        cUtilitarios.br("Vida alterada com restri��o de PPE(Pessoa Politicamente Exposta).\nSomente uma pessoa autorizada, poder� aceitar essa inclus�o.")
                    End If
                    'vitor.cabral - Nova Consultoria - Fim

                    Dim val_capital As String = MovimentacaoUmaUm_Form.getValCapital(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
                    If MovimentacaoUmaUm_Form.getCapitalSeguradoSession() = "Capital fixo" Or MovimentacaoUmaUm_Form.getCapitalSeguradoSession() = "Capital Global" Then
                        valorCapitalSegurado = val_capital
                    ElseIf MovimentacaoUmaUm_Form.getCapitalSeguradoSession() = "M�ltiplo sal�rio" Then

                        If valorCapitalSegurado > cUtilitarios.getLimMaxCapital Then
                            valorCapitalSegurado = cUtilitarios.getLimMaxCapital
                            If altSal = 1 Or altCap = 1 Then
                                cUtilitarios.br("Capital Segurado acima do limite permitido para o subgrupo. \nSer� considerado o capital de acordo com as condi��es da ap�lice.")
                            End If
                        End If
                        If valorCapitalSegurado < cUtilitarios.getLimMinCapital Then
                            valorCapitalSegurado = cUtilitarios.getLimMinCapital
                            If altSal = 1 Or altCap = 1 Then
                                cUtilitarios.br("Capital Segurado abaixo do limite permitido para o subgrupo. \nSer� considerado o capital de acordo com as condi��es da ap�lice.")
                            End If

                        End If

                        passouVerificacao = True

                        Dim valcapitalanterior As Double = MovimentacaoUmaUm_Form.getCapitalSeguradoUsuario(Request.Form("idSolicitado"))

                    End If

                    Dim x As Integer = 0

                    x = MovimentacaoUmaUm_Form.ValidarDetalhe(sTpIsSubGrupo, Request.Form("txtTitAdmissao"), MovimentacaoUmaUm_Form.getdataApolice(), valorCapitalSegurado, Request.Form("txtTitSalario"), 1, Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), "a", Request.Form("txtTitNascimento"), DateDiff(DateInterval.Year, New Date(dt(2), dt(1), dt(0)), Now), Now, "", cUtilitarios.destrataCPF(Request.Form("txtTitCPF")), CType(Request.Form("ddlTitSexo"), String).Substring(0, 1), param(5), 0, 0, CInt(param(0)), CInt(param(1)), Request.Form("txtTitAdmissao"))

                    If x <> 0 Then
                        If (x <> 7 Or x <> 8) And Not passouVerificacao Then

                            Dim strTitular As String = ""
                            If Request.Form("idTitular").Length > 0 Then
                                strTitular = "&idTitular=" & Request.Form("idTitular")
                            End If


                            script &= "function load() { "
                            script &= " document.Form1.action= 'movimentacaoUmaUm_Form.aspx?acao=" & Request.Form("acao") & strTitular & "&id=" & Request.Form("idSolicitado") & "';"
                            script &= " document.getElementById('content').innerHTML = '" & Session("contentConfirm") & "';"
                            script &= " document.Form1.submit();"

                            erro = True
                        End If
                    End If

                    If Not erro Then
                        content = ""
                        For Each m As String In Request.Form
                            If m <> "__VIEWSTATE" Then
                                If m = "idTitular" And Request.Form(m) = "" Then
                                    content &= "<input type=""hidden"" value=""" & Request.Form("idSolicitado") & """ name=""idTitular"">"
                                Else
                                    content &= "<input type=""hidden"" value=""" & Request.Form(m) & """ name=""" & m & """>"
                                End If
                            End If
                        Next

                        If Not forcaSubmit Then
                            script &= "function load() { parent.parent.GB_showConfirmPost();" & vbCrLf
                        Else
                            script &= "function load() { parent.parent.GB_showConfirmPostSemAction();" & vbCrLf
                        End If

                        Session("contentConfirm") = content
                        Session("actionConfirm") = Request.QueryString("destino")
                        Session("acaoConfirm") = Request.Form("acao")
                    End If
                Else
                    content = ""
                    For Each m As String In Request.Form
                        If m <> "__VIEWSTATE" Then
                            If m = "idTitular" And Request.Form(m) = "" Then
                                content &= "<input type=""hidden"" value=""" & Request.Form("idSolicitado") & """ name=""idTitular"">"
                            Else
                                content &= "<input type=""hidden"" value=""" & Request.Form(m) & """ name=""" & m & """>"
                            End If
                        End If
                    Next

                    If Not forcaSubmit Then
                        script &= "function load() { parent.parent.GB_showConfirmPost();" & vbCrLf
                    Else
                        script &= "function load() { parent.parent.GB_showConfirmPostSemAction();" & vbCrLf
                    End If

                    Session("contentConfirm") = content
                    Session("actionConfirm") = Request.QueryString("destino")
                    Session("acaoConfirm") = Request.Form("acao")
                End If
            End If
        Catch ex As Exception
            cUtilitarios.br(ex.Message)
            Dim excp As New clsException(ex)
        End Try

        If script.Length = tamanho Then
            script &= " function load() { send(); "
        End If

        script &= "} </script>"

        Response.Write(script)

        Try
            Dim vlapolice As String = Session("apolice")
            If vlapolice = "" Then
                cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
                'Response.End()
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
            cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
            'Response.End()
        End Try

        'Catch ex As Exception
        '    Dim excp As New clsException(ex)
        'End Try
    End Sub

End Class
