//AJS JavaScript library (minify'ed version)
//Copyright (c) 2006 Amir Salihefendic. All rights reserved.
//Copyright (c) 2005 Bob Ippolito. All rights reserved.
//License: http://www.opensource.org/licenses/mit-license.php
//Visit http://orangoo.com/AmiNation/AJS for full version.
AJSIframe = {
BASE_URL: "",
drag_obj: null,
drag_elm: null,
_drop_zones: [],
_cur_pos: null,

_unloadListeners: function() {
if(AJSIframe.listeners)
AJSIframe.map(AJSIframe.listeners, function(elm, type, fn) {AJSIframe.removeEventListener(elm, type, fn)});
AJSIframe.listeners = [];
},
getElement: function(id) {
if(AJSIframe.isString(id) || AJSIframe.isNumber(id))
return IFrameConteudoPrincipal.document.getElementById(id);
else
return id;
},
getScrollTop: function() {
//From: http://www.quirksmode.org/js/doctypes.html
var t;
if (IFrameConteudoPrincipal.document.documentElement && IFrameConteudoPrincipal.document.documentElement.scrollTop)
t = IFrameConteudoPrincipal.document.documentElement.scrollTop;
else if (IFrameConteudoPrincipal.document.body)
t = IFrameConteudoPrincipal.document.body.scrollTop;
return t;
},
isArray: function(obj) {
return obj instanceof Array;
},
removeElement: function(/*elm1, elm2...*/) {
var args = AJSIframe.flattenList(arguments);
AJSIframe.map(args, function(elm) { AJSIframe.swapDOM(elm, null); });
},
isDict: function(o) {
var str_repr = String(o);
return str_repr.indexOf(" Object") != -1;
},
isString: function(obj) {
return (typeof obj == 'string');
},
getIndex: function(elm, list/*optional*/, eval_fn) {
for(var i=0; i < list.length; i++)
if(eval_fn && eval_fn(list[i]) || elm == list[i])
return i;
return -1;
},
createDOM: function(name, attrs) {
var i=0, attr;
elm = IFrameConteudoPrincipal.document.createElement(name);
if(AJSIframe.isDict(attrs[i])) {
for(k in attrs[0]) {
attr = attrs[0][k];
if(k == "style")
elm.style.cssText = attr;
else if(k == "class" || k == 'className')
elm.className = attr;
else {
elm.setAttribute(k, attr);
}
}
i++;
}
if(attrs[0] == null)
i = 1;
AJSIframe.map(attrs, function(n) {
if(n) {
if(AJSIframe.isString(n) || AJSIframe.isNumber(n))
n = AJSIframe.TN(n);
elm.appendChild(n);
}
}, i);
return elm;
},
isIe: function() {
return (navigator.userAgent.toLowerCase().indexOf("msie") != -1 && navigator.userAgent.toLowerCase().indexOf("opera") == -1);
},
addEventListener: function(elm, type, fn, /*optional*/listen_once, cancle_bubble) {
if(!cancle_bubble)
cancle_bubble = false;
var elms = AJSIframe.$A(elm);
AJSIframe.map(elms, function(elmz) {
if(listen_once)
fn = AJSIframe._listenOnce(elmz, type, fn);
if(AJSIframe.isIn(type, ['submit', 'load', 'scroll', 'resize'])) {
var old = elm['on' + type];
elm['on' + type] = function() {
if(old) {
fn(arguments);
return old(arguments);
}
else
return fn(arguments);
};
return;
}
if (elmz.attachEvent) {
//FIXME: We ignore cancle_bubble for IE... hmmz
elmz.attachEvent("on" + type, fn);
}
else if(elmz.addEventListener)
elmz.addEventListener(type, fn, cancle_bubble);
AJSIframe.listeners = AJSIframe.$A(AJSIframe.listeners);
AJSIframe.listeners.push([elmz, type, fn]);
});
},
swapDOM: function(dest, src) {
dest = AJSIframe.getElement(dest);
var parent = dest.parentNode;
if (src) {
src = AJSIframe.getElement(src);
parent.replaceChild(src, dest);
} else {
parent.removeChild(dest);
}
return src;
},
getLast: function(list) {
if(list.length > 0)
return list[list.length-1];
else
return null;
},
map: function(list, fn,/*optional*/ start_index, end_index) {
var i = 0, l = list.length;
if(start_index)
i = start_index;
if(end_index)
l = end_index;
for(i; i < l; i++)
fn.apply(null, [list[i]]);
},
getElementsByTagAndClassName: function(tag_name, class_name, /*optional*/ parent) {
var class_elements = [];
if(!AJSIframe.isDefined(parent))
parent = IFrameConteudoPrincipal.document;
if(!AJSIframe.isDefined(tag_name))
tag_name = '*';
var els = parent.getElementsByTagName(tag_name);
var els_len = els.length;
var pattern = new RegExp("(^|\\s)" + class_name + "(\\s|$)");
for (i = 0, j = 0; i < els_len; i++) {
if ( pattern.test(els[i].className) || class_name == null ) {
class_elements[j] = els[i];
j++;
}
}
return class_elements;
},
isOpera: function() {
return (navigator.userAgent.toLowerCase().indexOf("opera") != -1);
},
setLeft: function(/*elm1, elm2..., left*/) {
var args = AJSIframe.flattenList(arguments);
var l = AJSIframe.getLast(args);
AJSIframe.map(args, function(elm) { elm.style.left = AJSIframe.getCssDim(l)}, 0, args.length-1);
},
getBody: function() {
return AJSIframe.$bytc('body')[0]
},
getWindowSize: function() {
var win_w, win_h;
if (self.innerHeight) {
win_w = self.innerWidth;
win_h = self.innerHeight;
} else if (IFrameConteudoPrincipal.document.documentElement && IFrameConteudoPrincipal.document.documentElement.clientHeight) {
win_w = IFrameConteudoPrincipal.document.documentElement.clientWidth;
win_h = IFrameConteudoPrincipal.document.documentElement.clientHeight;
} else if (IFrameConteudoPrincipal.document.body) {
win_w = IFrameConteudoPrincipal.document.body.clientWidth;
win_h = IFrameConteudoPrincipal.document.body.clientHeight;
}
return {'w': win_w, 'h': win_h};
},
showElement: function(/*elms...*/) {
var args = AJSIframe.flattenList(arguments);
AJSIframe.map(args, function(elm) { elm.style.display = ''});
},
removeEventListener: function(elm, type, fn, /*optional*/cancle_bubble) {
if(!cancle_bubble)
cancle_bubble = false;
if(elm.removeEventListener) {
elm.removeEventListener(type, fn, cancle_bubble);
if(AJSIframe.isOpera())
elm.removeEventListener(type, fn, !cancle_bubble);
}
else if(elm.detachEvent)
elm.detachEvent("on" + type, fn);
},
_getRealScope: function(fn, /*optional*/ extra_args, dont_send_event, rev_extra_args) {
var scope = window;
extra_args = AJSIframe.$A(extra_args);
if(fn._cscope)
scope = fn._cscope;
return function() {
//Append all the orginal arguments + extra_args
var args = [];
var i = 0;
if(dont_send_event)
i = 1;
AJSIframe.map(arguments, function(arg) { args.push(arg) }, i);
args = args.concat(extra_args);
if(rev_extra_args)
args = args.reverse();
return fn.apply(scope, args);
};
},
_createDomShortcuts: function() {
var elms = [
"ul", "li", "td", "tr", "th",
"tbody", "table", "input", "span", "b",
"a", "div", "img", "button", "h1",
"h2", "h3", "br", "textarea", "form",
"p", "select", "option", "iframe", "script",
"center", "dl", "dt", "dd", "small",
"pre"
];
var createDOM = AJSIframe.createDOM;
var extends_ajs = function(elm) {
var c_dom = "return createDOM.apply(null, ['" + elm + "', arguments]);";
var c_fun_dom = 'function() { ' + c_dom + '  }';
eval("AJSIframe." + elm.toUpperCase() + "=" + c_fun_dom);
}
AJSIframe.map(elms, extends_ajs);
AJSIframe.TN = function(text) { return IFrameConteudoPrincipal.document.createTextNode(text) };
},
isNumber: function(obj) {
return (typeof obj == 'number');
},
bind: function(fn, scope, /*optional*/ extra_args, dont_send_event, rev_extra_args) {
fn._cscope = scope;
return AJSIframe._getRealScope(fn, extra_args, dont_send_event, rev_extra_args);
},
setTop: function(/*elm1, elm2..., top*/) {
var args = AJSIframe.flattenList(arguments);
var t = AJSIframe.getLast(args);
AJSIframe.map(args, function(elm) { elm.style.top = AJSIframe.getCssDim(t)}, 0, args.length-1);
},
appendChildNodes: function(elm/*, elms...*/) {
if(arguments.length >= 2) {
AJSIframe.map(arguments, function(n) {
if(AJSIframe.isString(n))
n = AJSIframe.TN(n);
if(AJSIframe.isDefined(n))
elm.appendChild(n);
}, 1);
}
return elm;
},
isDefined: function(o) {
return (o != "undefined" && o != null)
},
isIn: function(elm, list) {
var i = AJSIframe.getIndex(elm, list);
if(i != -1)
return true;
else
return false;
},
setHeight: function(/*elm1, elm2..., height*/) {
var args = AJSIframe.flattenList(arguments);
var h = AJSIframe.getLast(args);
AJSIframe.map(args, function(elm) { elm.style.height = AJSIframe.getCssDim(h)}, 0, args.length-1);
},
hideElement: function(elm) {
var args = AJSIframe.flattenList(arguments);
AJSIframe.map(args, function(elm) { elm.style.display = 'none'});
},
createArray: function(v) {
if(AJSIframe.isArray(v) && !AJSIframe.isString(v))
return v;
else if(!v)
return [];
else
return [v];
},
setWidth: function(/*elm1, elm2..., width*/) {
var args = AJSIframe.flattenList(arguments);
var w = AJSIframe.getLast(args);
AJSIframe.map(args, function(elm) { elm.style.width = AJSIframe.getCssDim(w)}, 0, args.length-1);
},
getCssDim: function(dim) {
if(AJSIframe.isString(dim))
return dim;
else
return dim + "px";
},
_listenOnce: function(elm, type, fn) {
var r_fn = function() {
AJSIframe.removeEventListener(elm, type, r_fn);
fn(arguments);
}
return r_fn;
},
flattenList: function(list) {
var r = [];
var _flatten = function(r, l) {
AJSIframe.map(l, function(o) {
if (AJSIframe.isArray(o))
_flatten(r, o);
else
r.push(o);
});
}
_flatten(r, list);
return r;
}
}

AJSIframe.$ = AJSIframe.getElement;
AJSIframe.$$ = AJSIframe.getElements;
AJSIframe.$f = AJSIframe.getFormElement;
AJSIframe.$b = AJSIframe.bind;
AJSIframe.$A = AJSIframe.createArray;
AJSIframe.DI = AJSIframe.documentInsert;
AJSIframe.ACN = AJSIframe.appendChildNodes;
AJSIframe.RCN = AJSIframe.replaceChildNodes;
AJSIframe.AEV = AJSIframe.addEventListener;
AJSIframe.REV = AJSIframe.removeEventListener;
AJSIframe.$bytc = AJSIframe.getElementsByTagAndClassName;

AJSIframe.addEventListener(window, 'unload', AJSIframe._unloadListeners);
AJSIframe._createDomShortcuts()

AJSIframeDeferred = function(req) {
this.callbacks = [];
this.errbacks = [];
this.req = req;
};
AJSIframeDeferred.prototype = {
excCallbackSeq: function(req, list) {
var data = req.responseText;
while (list.length > 0) {
var fn = list.pop();
var new_data = fn(data, req);
if(new_data)
data = new_data;
}
},
callback: function () {
this.excCallbackSeq(this.req, this.callbacks);
},
errback: function() {
if(this.errbacks.length == 0)
alert("Error encountered:\n" + this.req.responseText);
this.excCallbackSeq(this.req, this.errbacks);
},
addErrback: function(fn) {
this.errbacks.unshift(fn);
},
addCallback: function(fn) {
this.callbacks.unshift(fn);
},
addCallbacks: function(fn1, fn2) {
this.addCallback(fn1);
this.addErrback(fn2);
},
sendReq: function(data) {
if(AJSIframe.isObject(data)) {
var post_data = [];
for(k in data) {
post_data.push(k + "=" + AJSIframe.urlencode(data[k]));
}
post_data = post_data.join("&");
this.req.send(post_data);
}
else if(AJSIframe.isDefined(data))
this.req.send(data);
else {
this.req.send("");
}
}
}