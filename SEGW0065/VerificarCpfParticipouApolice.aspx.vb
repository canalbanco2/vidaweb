Imports System.Diagnostics

Public Partial Class VerificarCpfParticipouApolice
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim cpf As String = ""
        Dim Exists As Boolean = False

        Try
            cpf = Request("cpf")
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        If cpf <> "" Then

            If cpf.Trim.Length > 0 Then

                Exists = VerificaCPFParticipouApolice(cpf)

                If Exists Then
                    Response.Write("1")
                Else
                    Response.Write("0")
                End If

            Else
                Response.Write("0")
            End If

        End If

        Response.End()
    End Sub
    Public Function VerificaCPFParticipouApolice(ByVal cpf As String) As Boolean

        Try
            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

            bd.getProponenteApolice(Session("apolice"), Session("ramo"), 6785, 0, cUtilitarios.destrataCPF(cpf))

            Dim dr As Data.SqlClient.SqlDataReader = Nothing

            Try
                dr = bd.ExecutaSQL_DR()
                If Not dr Is Nothing Then
                    If dr.Read Then
                        Return True
                    End If

                    If Not dr.IsClosed Then dr.Close()
                    dr = Nothing
                End If
            Catch ex As Exception
                Dim excp As New clsException(ex)
            End Try

            Return False

        Catch ex As Exception
            Dim excp As New clsException(ex)
            Return False
        End Try
    End Function

End Class