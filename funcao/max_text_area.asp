<SCRIPT LANGUAGE="JavaScript">

//trata o tamanho maximo de uma textarea
//Exemplo: onkeypress="conta_texto(this.value,120,'obs')"
//nome do campo do contador
var conta_texto_contador = 'contador';
//nome do formulario
var conta_texto_formulario = 'form';
function conta_texto(valor,tamanho,campo){
	if (document[conta_texto_formulario][campo].value.length > tamanho){
		document[conta_texto_formulario][campo].value = document[conta_texto_formulario][campo].value.substring(0,tamanho);
		alert('O n�mero m�ximo de caracteres foi atingido.');
	}
	if (typeof(document[conta_texto_formulario][conta_texto_contador]) != 'undefined'){
		document[conta_texto_formulario][conta_texto_contador].value = document[conta_texto_formulario][campo].value.length;
	}
}
function alteraContaTexto(valor){
	conta_texto_contador = valor;
	return void(0);
}

var mensagem_anterior = '';
var mensagem_posterior = ' caracteres restantes.';

function conta_texto_regressivo(campo,quant_total,id_campo_mensagem){
    var campo_mensagem = document.getElementById(id_campo_mensagem);
    var quant_atual = (quant_total/1)-(campo.value.length/1);
    
    if((quant_atual/1)< 0){
        quant_atual = 0
        campo.value = campo.value.substring(0,quant_total);
        alert('O n�mero m�ximo de caracteres foi atingido.');
    }
   
    if(typeof(campo_mensagem.value) == 'string'){
        campo_mensagem.value = mensagem_anterior + quant_atual + mensagem_posterior;
    }
    if(typeof(campo_mensagem.value) == 'undefined'){
        campo_mensagem.innerHTML = mensagem_anterior + quant_atual + mensagem_posterior;
    }
}
</SCRIPT>