<%
function Verifica_Data(d,m,a)
    if  (((isNull(d)) or (isNull(m)) or (isNull(a))) and ((not isNull(d)) and (not isNull(m)) and (not isNull(a)))) then 
        Verifica_Data = false
        exit function
    end if
    
    if ( (not isnumeric(d) and trim(d) <> "") or (not isnumeric(m) and trim(m) <> "") or (not isnumeric(a) and trim(a) <> "") ) then 
        Verifica_Data = false
        exit function
    end if
    
    if ( (m<1 or m >12) and (trim(m) <> "") ) then
        Verifica_Data = false
        exit function
    end if
    
    if ( (d<1 or d >31) and (d <> "") ) then 
        Verifica_Data = false
        exit function
    end if
    
    if ( (a<1900 or a>2078) and (a <> "") ) then
        Verifica_Data = false
        exit function
    end if
    
    if (d = 31) then
        if ( (m = 2) or (m = 4) or (m = 6) or (m = 9) or (m = 11) ) then 
            Verifica_Data = false
            exit function
        end if
    end if
    
    if (m = 2) then
        if( (cint(a) mod 4 <> 0 and d = 29) or (d = 30) ) then 
            Verifica_Data = false
            exit function
        end if
    end if
    Verifica_Data= true
end function%>