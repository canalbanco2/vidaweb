<%
Function txtPageCounter
	Dim tmpHitCount,textObject,pagename,fhandle
	' get unique pagename for this page...
	' this pagename has .txt appended so it's unique on the site
	' (nothing special about the ".txt" -- could be anything)
	'
	pagename = Request.ServerVariables("PATH_TRANSLATED") & ".txt" 'need that so we can use both!          
        pagina = pagename
	If Session(pagename) = "" Then
		Application.Lock
		Set textObject	= CreateObject("Scripting.FileSystemObject")
		If textObject.FileExists(pagina) Then
			' file exists, read old hit count
			'
			Set fhandle		= textObject.OpenTextFile(pagina)
			tmpHitCount = fhandle.readline
			fhandle.close
		Else
			' file does not exist, 0 will increment below to 1.
			'
			tmpHitCount = 0
		End If
		' either way, write a new file and
		' save the hit count in the session variable
		'
		tmpHitcount = tmpHitCount + 1
		Set fhandle	= textObject.CreateTextFile(pagina)
		fhandle.writeline (tmpHitCount)
		fhandle.close
		Application.UnLock
		Session(pagename) = tmpHitCount
	Else
		' already counted this session...
		' get the hit count that was saved
		'
		tmpHitCount = Session(pagename)
	End If
	' tmpHitCount has the hit count,
	' return it as the value of the function
	'
	txtPageCounter = tmpHitCount
End Function 'txtPageCounter
%>
