<%
Function SE(Condicao, valorTrue, valorFalse)
    if condicao then
        SE = valorTrue
    else
        SE = valorFalse
    end if
end function

Function FazXOR(Cadeia, Indice)
  If Indice > 1 Then
    FazXOR = Asc(Mid(Cadeia, Indice, 1)) Xor Asc(Mid(Cadeia, Indice - 1, 1))
  Else
    FazXOR = Asc(Mid(Cadeia, Indice, 1)) Xor Len(Cadeia)
  End If
End Function

Function mMID(Cadeia, Indice, valorNovo)
    mMID = Left(Cadeia, Indice - 1)
    mMID = mMID & valorNovo
    mMID = mMID & Mid(Cadeia, Indice + 1)
End Function

Function Criptografia(Cadeia, Chave, Passo)
    'Passo, Positivo para criptografar, Negativo para desincriptografar
  If Passo = 0 Then
    Passo = 1
  End If
  Inicio = 0
  Fim = 0
  Inicio = Se(Passo > 0, 1, Len(Cadeia))
  Fim = Se(Passo > 0, Len(Cadeia), 1)
  Indice = 0
  If Chave = "" Then
    For Indice = Inicio To Fim Step Passo
      Cadeia = mMID(Cadeia, Indice, Chr(FazXOR(Cadeia, Indice)))
    Next
  Else
    For Indice = Inicio To Fim Step Passo
      Cadeia = mMID(Cadeia, Indice, Chr(FazXOR(Cadeia, Indice) Xor Asc(Mid(Chave, ((Indice - 1) Mod Len(Chave)) + 1, 1))))
    Next
  End If
  Criptografia = Cadeia
End Function
%>