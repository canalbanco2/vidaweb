<script language=JScript>	
//Criado por Danielle Cardoso em 29/03/1999
function msg_vhora(obrig,campo,h,m)
{	
//obrig: se o campo � obrigatorio ou nao
//campo: nome do campo que aparecer� na mensagem
//valor: valor a ser testado
//ATENCAO: as funcoes msg_vnull() ,isEmpty() e verifica_hora() est�o em arquivos externos

function f_hora(cvg,vh,vm)
{
	if (! Verifica_Hora(vh,vm))	 
		return "\n" + "O campo '" + cvg + "' � inv�lido." 
	return " "
}

if (obrig)
{	
	m_valid = msg_vnull(campo,h)
	if (! isEmpty(m_valid)) 
		return m_valid
	
	m_valid = msg_vnull(campo,m)
	if (! isEmpty(m_valid)) 
		return m_valid
	
	return f_hora(campo,h,m)
}
else
	return f_hora(campo,h,m)

}
</script>