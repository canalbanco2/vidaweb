<SCRIPT LANGUAGE=VBSCRIPT RUNAT=SERVER>
function ShowChart(ByRef aValues, ByRef aLabels, ByRef strTitle, ByRef strXAxisLabel, ByRef strYAxisLabel)
	if isEmpty(aValues) or isEmpty(aLabels) then exit function
    ' Some user changable graph defining constants
	' All units are in screen pixels
	Const GRAPH_WIDTH  = 450  ' The width of the body of the graph
	Const GRAPH_HEIGHT = 250  ' The heigth of the body of the graph
	Const GRAPH_BORDER = 2    ' The size of the black border
	Const GRAPH_SPACER = 2    ' The size of the space between the bars
    const PATH_IMG     = "\_IMG\VERSAO3\"
	' Debugging constant so I can eaasily switch on borders in case
	' the tables get messed up.  Should be left at zero unless you're
	' trying to figure out which table cells doing what.
	Const TABLE_BORDER = 0
	'Const TABLE_BORDER = 10

	' Declare our variables
	Dim I
	Dim iMaxValue
	Dim iBarWidth
	Dim iBarHeight

	' Get the maximum value in the data set
	iMaxValue = 0
	For I = 0 To UBound(aValues)
		If cint(iMaxValue) < cint(aValues(I)) Then iMaxValue = aValues(I)
	Next 'I
	'Response.Write iMaxValue ' Debugging line


	' Calculate the width of the bars
	' Take the overall width and divide by number of items and round down.
	' I then reduce it by the size of the spacer so the end result
	' should be GRAPH_WIDTH or less!
	iBarWidth = (GRAPH_WIDTH \ (UBound(aValues) + 1)) - GRAPH_SPACER
	'Response.Write iBarWidth ' Debugging line


	' Start drawing the graph
	
	STR = "<center><TABLE BORDER=" & TABLE_BORDER & " CELLSPACING=0 CELLPADDING=0> "
	STR = STR & "<TR>"
	STR = STR & "<TD COLSPAN=3 ALIGN=center><H2><font face=verdana>" &  strTitle & "</font></H2></TD>"
	STR = STR & "</TR>"
	STR = STR & "<TR>"
	STR = STR & "<TD VALIGN=center><font face=verdana><B>" &  strYAxisLabel & "</font></B></TD>"
	STR = STR & "<TD VALIGN=top>"
	STR = STR & "<TABLE BORDER=" &  TABLE_BORDER & " CELLSPACING=0 CELLPADDING=0>"
    STR = STR & "<TR>"
    STR = STR & "<TD ROWSPAN=2><IMG SRC=" & PATH_IMG & "spacer.gif BORDER=0 WIDTH=1 HEIGHT=" &  GRAPH_HEIGHT & "></TD>"
    STR = STR & "<TD VALIGN=top ALIGN=right>" &  iMaxValue & "&nbsp;</TD>"
    STR = STR & "</TR>"
    STR = STR & "<TR>"
    STR = STR & "<TD VALIGN=bottom ALIGN=right>0&nbsp;</TD>"
    STR = STR & "</TR>"
    STR = STR & "</TABLE>"
    STR = STR & "</TD>"
    STR = STR & "<TD>"
    STR = STR & "<TABLE BORDER=" &  TABLE_BORDER & " CELLSPACING=0 CELLPADDING=0>"
    STR = STR & "<TR>"
    STR = STR & "<TD VALIGN=bottom><IMG SRC=" & PATH_IMG & "spacer_black.gif BORDER=0 WIDTH=" &  GRAPH_BORDER & " HEIGHT=" &  GRAPH_HEIGHT & "></TD>"
					' We're now in the body of the chart.  Loop through the data showing the bars!
					For I = 0 To UBound(aValues)
						iBarHeight = Int((aValues(I) / iMaxValue) * GRAPH_HEIGHT)

						' This is a hack since browsers ignore a 0 as an image dimension!
						If iBarHeight = 0 Then iBarHeight = 1
	STR = STR & "<TD VALIGN=bottom><IMG SRC=" & PATH_IMG & "spacer.gif BORDER=0 WIDTH=" &  GRAPH_SPACER & " HEIGHT=1></TD>"
	STR = STR & "<TD VALIGN=bottom><IMG SRC=" & PATH_IMG & "spacer_red.gif BORDER=0 WIDTH=" &  iBarWidth & " HEIGHT=" &  iBarHeight & " ALT=" &  aValues(I) & "></A></TD>"
					Next 'I
	STR = STR & "</TR>"
	STR = STR & "<TR>"
	STR = STR & "<TD COLSPAN=" &  (2 * (UBound(aValues) + 1)) + 1 & "><IMG SRC=" & PATH_IMG & "spacer_black.gif BORDER=0 WIDTH=" &  GRAPH_BORDER + ((UBound(aValues) + 1) * (iBarWidth + GRAPH_SPACER)) & " HEIGHT=" &  GRAPH_BORDER & "></TD>"
	STR = STR & "</TR>"
				' The label array is optional and is really only useful for small data sets with very short labels! & "
				If IsArray(aLabels) Then 
	STR = STR & "<TR><TD></TD>"
					For I = 0 To UBound(aValues)  
	STR = STR & "<TD></TD>"
	STR = STR & "<TD ALIGN=center><FONT SIZE=1>" &  aLabels(I) & "</FONT></TD>"
					Next 'I & "
	STR = STR & "</TR>"
				End If 
	STR = STR & "</TABLE>"
	STR = STR & "</TD>"
	STR = STR & "</TR>"
	STR = STR & "<TR>"
	STR = STR & "<TD COLSPAN=2></TD>"
	STR = STR & "<TD ALIGN=center><BR><B><font face=verdana>" &  strXAxisLabel & "</font></B></TD>"
	STR = STR & "</TR>"
	STR = STR & "</TABLE></center><br><br>"
    ShowChart = str
End function
</SCRIPT>