if (document.all)    {n=0;ie=1;fShow="visible";fHide="hidden";}
if (document.layers) {n=1;ie=0;fShow="show";   fHide="hide";}

window.onerror=new Function("return true")
/* *************** */
/* Function Menu() */
/* *************** */
rightX = 0;
function Menu()
{
	this.bgColor     = "#29458F"; /* #008080 */
	if (ie) this.menuFont = "xx-small Verdana";
	if (n)  this.menuFont = "x-small Verdana";
	this.fontColor   = "#FFFFFF";

	this.addItem    = addItem;
	this.addSubItem = addSubItem;
	this.showMenu   = showMenu;
	this.mainPaneBorder = 0; /* 1 */
	this.subMenuPaneBorder = 0; /* 1 */

	this.subMenuPaneWidth = 87; /* 150 */

	lastMenu = null;
	
	rightY = 0;
	leftY = 0;
	leftX = 0;

	HTMLstr = "";
	HTMLstr += "<!-- MENU PANE DECLARATION BEGINS -->\n";
	HTMLstr += "\n";
	if (ie) HTMLstr += "<div id='MainTable' style='position:relative'>\n";
//	if (n)  HTMLstr += "<layer name='MainTable'>\n";
	HTMLstr += "<table width='100%' bgcolor='"+this.bgColor+"' border='"+this.mainPaneBorder+"'>\n";
	HTMLstr += "<tr>";
	if (n) HTMLstr += "<td>&nbsp;";
	HTMLstr += "<!-- MAIN MENU STARTS -->\n";
	HTMLstr += "<!-- MAIN_MENU -->\n";
	HTMLstr += "<!-- MAIN MENU ENDS -->\n";
	if (n) HTMLstr += "</td>";
	HTMLstr += "</tr>\n";
	HTMLstr += "</table>\n";
	HTMLstr += "\n";
	HTMLstr += "<!-- SUB MENU STARTS -->\n";
	HTMLstr += "<!-- SUB_MENU -->\n";
	HTMLstr += "<!-- SUB MENU ENDS -->\n";
	HTMLstr += "\n";
	if (ie) HTMLstr+= "</div>\n";
//	if (n)  HTMLstr+= "</layer>\n";
	HTMLstr += "<!-- MENU PANE DECALARATION ENDS -->\n";
}

function addItem(idItem, text, hint, location, altLocation)
{
	var Lookup = "<!-- ITEM "+idItem+" -->";
	if (HTMLstr.indexOf(Lookup) != -1)
	{
		alert(idParent + " already exist");
		return;
	}
	var MENUitem = "";
	MENUitem += "\n<!-- ITEM "+idItem+" -->\n";
	if (n)
	{
		MENUitem += "<ilayer name="+idItem+">";
		MENUitem += "<a href='.' class=clsMenuItemNS onmouseover=\"displaySubMenu('"+idItem+"')\" onclick=\"return false;\">";
		MENUitem += "|&nbsp;";
		MENUitem += text;
		MENUitem += "</a>";
		MENUitem += "</ilayer>";
	}
	if (ie)
	{
		MENUitem += "<td>\n";
		MENUitem += "<div id='"+idItem+"' style='position:relative; font: "+this.menuFont+";'>\n";
		MENUitem += "<a ";
//		MENUitem += "class=clsMenuItemIE ";
		MENUitem += "style='text-decoration: none; font: "+this.menuFont+"; color: "+this.fontColor+"; cursor: hand;' ";
		if (hint != null)
			MENUitem += "title='"+hint+"' ";
		if (location != null)
		{
			MENUitem += "href='"+location+"' ";
			MENUitem += "onmouseover=\"hideAll()\" ";
		}
		else
		{
			if (altLocation != null)
				MENUitem += "href='"+altLocation+"' ";
			else
				MENUitem += "href='.' ";
			MENUitem += "onmouseover=\"displaySubMenu('"+idItem+"')\" ";
			MENUitem += "onclick=\"return false;\" "
		}
		MENUitem += ">";
		MENUitem += "|\n";
		MENUitem += text;
		MENUitem += "</a>\n";
		MENUitem += "</div>\n";
		MENUitem += "</td>\n";
	}
	MENUitem += "<!-- END OF ITEM "+idItem+" -->\n\n";
	MENUitem += "<!-- MAIN_MENU -->\n";

	HTMLstr = HTMLstr.replace("<!-- MAIN_MENU -->\n", MENUitem);
}

function addSubItem(idParent, text, hint, location, full)
{
	var MENUitem = "";
	Lookup = "<!-- ITEM "+idParent+" -->";
	if (HTMLstr.indexOf(Lookup) == -1)
	{
		alert(idParent + " not found");
		return;
	}
	Lookup = "<!-- NEXT ITEM OF SUB MENU "+ idParent +" -->";
	if (HTMLstr.indexOf(Lookup) == -1)
	{
		if (n)
		{
			MENUitem += "\n";
			MENUitem += "<layer id='"+idParent+"submenu' visibility=hide bgcolor='"+this.bgColor+"'>\n";
			MENUitem += "<table border='"+this.subMenuPaneBorder+"' bgcolor='"+this.bgColor+"' width="+this.subMenuPaneWidth+">\n";
			MENUitem += "<!-- NEXT ITEM OF SUB MENU "+ idParent +" -->\n";
			MENUitem += "</table>\n";
			MENUitem += "</layer>\n";
			MENUitem += "\n";
		}
		if (ie)
		{
			MENUitem += "\n";
			MENUitem += "<div id='"+idParent+"submenu' style='position:absolute; visibility: hidden; width: "+this.subMenuPaneWidth+"; font: "+this.menuFont+"; top: -300;'>\n";
			MENUitem += "<table border='"+this.subMenuPaneBorder+"' bgcolor='"+this.bgColor+"' width="+this.subMenuPaneWidth+">\n";
			MENUitem += "<!-- NEXT ITEM OF SUB MENU "+ idParent +" -->\n";
			MENUitem += "</table>\n";
			MENUitem += "</div>\n";
			MENUitem += "\n";
		}
		MENUitem += "<!-- SUB_MENU -->\n";
		HTMLstr = HTMLstr.replace("<!-- SUB_MENU -->\n", MENUitem);
	}

	Lookup = "<!-- NEXT ITEM OF SUB MENU "+ idParent +" -->\n";
	if (n) {
		if (full == 'S') {
			MENUitem = "<tr><td style='border-bottom: yellow thin solid'><a class=clsMenuItemNS title='"+hint+"' href='"+location+"' target='_top'>"+text+"</a><br></td></tr>\n";
		}
		else {
			MENUitem = "<tr><td style='border-bottom: yellow thin solid'><a class=clsMenuItemNS title='"+hint+"' href='"+location+"'>"+text+"</a><br></td></tr>\n";
		}
	}
//	if (ie) MENUitem = "<tr><td style='border-bottom: yellow thin solid'><a class=clsMenuItemIE title='"+hint+"' href='"+location+"'>"+text+"</a><br></td></tr>\n";
	if (ie) {
		if (full == 'S') {
			MENUitem = "<tr><td style='border-bottom: yellow thin groove'><a style='font: "+this.menuFont+"; color: "+this.fontColor+"; text-decoration: none; cursor: hand;' title='"+hint+"' href='"+location+"' target='_top'>"+text+"</a><br></td></tr>\n";
		}
		else {
			MENUitem = "<tr><td style='border-bottom: yellow thin groove'><a style='font: "+this.menuFont+"; color: "+this.fontColor+"; text-decoration: none; cursor: hand;' title='"+hint+"' href='"+location+"'>"+text+"</a><br></td></tr>\n";
		}
	}
	MENUitem += Lookup;
	HTMLstr = HTMLstr.replace(Lookup, MENUitem);
}

function showMenu()
{
	document.writeln(HTMLstr);
}

/* ******************* */
/* Private declaration */
/* ******************* */
function displaySubMenu(idMainMenu)
{
	var menu;
	var submenu;
	if (n)
	{
		submenu = document.layers[idMainMenu+"submenu"];
		if (lastMenu != null && lastMenu != submenu) hideAll();
		submenu.left = document.layers[idMainMenu].pageX;
		submenu.top  = document.layers[idMainMenu].pageY + 25;
		submenu.visibility = fShow;

		leftX  = document.layers[idMainMenu+"submenu"].left;
		rightX = leftX + document.layers[idMainMenu+"submenu"].clip.width;
		leftY  = document.layers[idMainMenu+"submenu"].top+
			document.layers[idMainMenu+"submenu"].clip.height;
		rightY = leftY;
	} else if (ie) {
		menu = eval(idMainMenu);
		submenu = eval(idMainMenu+"submenu.style");
		submenu.left = calculateSumOffset(menu, 'offsetLeft');
//		submenu.top  = calculateSumOffset(menu, 'offsetTop') + 30;
		submenu.top  = menu.style.top+23;
		submenu.visibility = fShow;
		if (lastMenu != null && lastMenu != submenu) hideAll();

		leftX  = document.all[idMainMenu+"submenu"].style.posLeft;
		rightX = leftX + document.all[idMainMenu+"submenu"].offsetWidth;

		leftY  = document.all[idMainMenu+"submenu"].style.posTop+
			document.all[idMainMenu+"submenu"].offsetHeight;
		rightY = leftY;
	}
	lastMenu = submenu;
}

function hideAll()
{
	if (lastMenu != null) {lastMenu.visibility = fHide;lastMenu.left = 0;}
}

function calculateSumOffset(idItem, offsetName)
{
	var totalOffset = 0;
	var item = eval('idItem');
	do
	{
		totalOffset += eval('item.'+offsetName);
		item = eval('item.offsetParent');
	} while (item != null);
	return totalOffset;
}

function updateIt(e)
{
	if (ie)
	{
		var x = window.event.clientX;
		var y = window.event.clientY;

		if (x > rightX || x < leftX) hideAll();
		else if (y > rightY) hideAll();
	}
	if (n)
	{
		var x = e.pageX;
		var y = e.pageY;

		if (x > rightX || x < leftX) hideAll();
		else if (y > rightY) hideAll();
	}
}

if (document.all)
{
	document.body.onclick=hideAll;
	document.body.onscroll=hideAll;
	document.body.onmousemove=updateIt;
}
if (document.layers)
{
	document.onmousedown=hideAll;
	window.captureEvents(Event.MOUSEMOVE);
	window.onmousemove=updateIt;
}

/* ********************** */
/* Function showTollbar() */
/* ********************** */
function showToolbar(no_pai, no_pai_restrito)
{
	menu = new Menu();
	
	/* Menu p�blico */
	fim = false;
	posicao_inicial = 0;
	
	while (! fim) {
		posicao = no_pai.indexOf(';');
		if (posicao >= 0) {
			substr = no_pai.substring(posicao_inicial, posicao);
			posicao2 = substr.indexOf(',');
			item_id = substr.substring(0, posicao2);
			posicao3 = substr.indexOf('(');
			nome = substr.substring((posicao2 + 1), posicao3);
			menu.addItem("Menu"+item_id, nome, nome,  null, null);
			substr = substr.substring(posicao3, substr.length);
			//listando filhos
			fim_filho = false;
			substr = substr.substring(1, (substr.length - 1));
			while (! fim_filho){
				posicao4 = substr.indexOf('|');
				if (posicao4 >= 0) {
					substr_filho = substr.substring(posicao_inicial, posicao4);
					posicao5 = substr_filho.indexOf(',');
					item_filho_id = substr_filho.substring(0, posicao5);
					substr_filho = substr_filho.substring((posicao5 + 1), substr_filho.length);
					posicao5 = substr_filho.indexOf(',');
					nome_filho = substr_filho.substring(0, posicao5);
					substr_filho = substr_filho.substring((posicao5 + 1), substr_filho.length);
					posicao5 = substr_filho.indexOf(',');
					endereco_filho = substr_filho.substring(0, posicao5);
					pagina_full = substr_filho.substring((posicao5 + 1), posicao4);
					pagina_full = pagina_full.toUpperCase();
					if (pagina_full == 'S') {
						menu.addSubItem("Menu"+item_id, nome_filho, nome_filho, endereco_filho, 'S');
					}
					else {
						menu.addSubItem("Menu"+item_id, nome_filho, nome_filho, endereco_filho, 'N');
					}
					substr = substr.substring((posicao4 + 1), substr.length);
				}
				else {
					fim_filho = true;
				}
			}
			no_pai = no_pai.substring((posicao + 1), no_pai.length);
		}
		else {
			fim = true;
		}
	}

	/* Menu restrito */
	menu.addItem("restritos", "Restritos", "Restritos",  null, null);
	fim = false;
	posicao_inicial = 0;
	
	while (! fim) {
		posicao = no_pai_restrito.indexOf(';');
		if (posicao >= 0) {
			substr = no_pai_restrito.substring(posicao_inicial, posicao);
			posicao2 = substr.indexOf(',');
			item_restrito_id = substr.substring(0, posicao2);
			nome_restrito = substr.substring((posicao2 + 1), substr.length);
			menu.addSubItem("restritos", nome_restrito, nome_restrito, "http://" + document.location.hostname + "/frame.asp?item="+item_restrito_id, 'S');
			no_pai_restrito = no_pai_restrito.substring((posicao + 1), no_pai_restrito.length);
		}
		else {
			fim = true;
		}
	}

	menu.showMenu();
}


/* O R I G I N A L */

/* ********************** */
/* Function showTollbar() */
/* ********************** */
/*
function showToolbar()
{
// AddItem(id, text, hint, location, alternativeLocation);
// AddSubItem(idParent, text, hint, location);

	menu = new Menu();
	menu.addItem("webmasterid", "Web Building Sites", "Web Building Sites",  null, null);
	menu.addItem("newsid", "News Sites", "News Sites",  null, null);
	menu.addItem("freedownloadid", "Free Downloads", "Free Downloads",  null, null);
	menu.addItem("searchengineid", "Search Engines", "Search Engines",  null, null);
	menu.addItem("miscid", "Miscellaneous", "Miscellaneous",  null, null);

	menu.addSubItem("webmasterid", "Dynamic Drive", "Dynamic Drive",  "http://www.dynamicdrive.com/");
	menu.addSubItem("webmasterid", "Website Abstraction", "Website Abstraction",  "http://www.wsabstract.com/");
	menu.addSubItem("webmasterid", "Web Review", "Web Review",  "http://www.webreview.com/");
	menu.addSubItem("webmasterid", "Developer.com", "Developer.com",  "http://www.developer.com/");
	menu.addSubItem("webmasterid", "Freewarejava.com", "Freewarejava.com",  "http://www.freewarejava.com/");
	menu.addSubItem("webmasterid", "Web Monkey", "Web Monkey",  "http://www.webmonkey.com/");
	menu.addSubItem("webmasterid", "Jars", "Jars",  "http://www.jars.com/");
	menu.addSubItem("webmasterid", "Intro DHTML Guide", "Intro DHTML Guide",  "http://members.tripod.com/~toolmandavid");

	menu.addSubItem("newsid", "CNN", "CNN",  "http://www.cnn.com");
	menu.addSubItem("newsid", "ABC News", "ABC News",  "http://www.abcnews.com");
	menu.addSubItem("newsid", "MSNBC", "MSNBC",  "http://www.msnbc.com");
	menu.addSubItem("newsid", "CBS news", "CBS News",  "http://www.cbsnews.com");
	menu.addSubItem("newsid", "News.com", "News.com",  "http://news.com");
	menu.addSubItem("newsid", "Wired News", "Wired News",  "http://www.wired.com");
	menu.addSubItem("newsid", "TechWeb", "TechWeb",  "http://www.techweb.com");

	menu.addSubItem("freedownloadid", "Dynamic Drive", "Dynamic Drive",  "http://www.dynamicdrive.com/");
	menu.addSubItem("freedownloadid", "Download.com", "Download.com",  "http://download.com/");
	menu.addSubItem("freedownloadid", "Jumbo", "Jumbo",  "http://www.jumbo.com/");
	menu.addSubItem("freedownloadid", "Tucows", "Tucows",  "http://tucows.com/");
	menu.addSubItem("freedownloadid", "WinFiles.com", "WinFiles.com",  "http://winfiles.com/");

	menu.addSubItem("searchengineid", "Yahoo", "Yahoo",  "http://www.yahoo.com/");
	menu.addSubItem("searchengineid", "Infoseek", "Infoseek",  "http://www.infoseek.com/");
	menu.addSubItem("searchengineid", "Excite", "Excite", "http://www.excite.com");
	menu.addSubItem("searchengineid", "HotBot", "HotBot",  "http://www.hotbot.com");

	menu.addSubItem("miscid", "Hitbox.com", "Hitbox.com",  "http://www.hitbox.com/");
	menu.addSubItem("miscid", "Cnet", "Cnet",  "http://www.cnet.com/");
	menu.addSubItem("miscid", "Andover.net", "Andover.net",  "http://www.andover.net/");
	menu.addSubItem("miscid", "RealAudio", "RealAudio",  "http://www.realaudio.com/");
	menu.addSubItem("miscid", "MP3.com", "MP3.com",  "http://www.mp3.com/");

	menu.showMenu();
}
*/