<script language=JScript>
function verifica_cgc(valor)
{
	if ((isNaN(valor)) && (valor.length != 14))
		return false
		
	Mult1 = "543298765432"
	Mult2 = "6543298765432"
	dig1=0
	dig2=0
		
	for(var i=0;i<=11;i++)   
	{
		ind=valor.charAt(i)
		M=Mult1.charAt(i)
		dig1 += ((parseFloat(ind)) *  (parseFloat(M)))
	}
	
	for( var i=0;i<=12;i++)   
	{
		ind=valor.charAt(i)
		M=Mult2.charAt(i)
		dig2 += ((parseFloat(ind)) *  (parseFloat(M)))
	}
		
	dig1 = (dig1 * 10) % 11
	dig2 = (dig2 * 10) % 11
	
	if (dig1 == 10)
		dig1 = 0
		
	if (dig2 == 10) 
		dig2 = 0

	if (dig1 != (parseFloat(valor.charAt(12))))
		return false
	
	if (dig2 != (parseFloat(valor.charAt(13))))
		return false
		
	return true
}
</script>