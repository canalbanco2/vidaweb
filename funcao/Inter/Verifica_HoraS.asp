<%
function Verifica_Hora(h,m)
 if  ( ( (isEmpty(h)) or (isEmpty(m)) ) and ( (not isEmpty(h)) and (not isEmpty(m)) ) ) then
    verifica_hora= false
    exit function
 end if
 if ( (not isnumeric(h) and (not isEmpty(h)) ) or (not isnumeric(m) and (not isEmpty(m)) ) ) then
    verifica_hora = false
    exit function
 end if
 if ( (h<0 or h >24) and (not isEmpty(h)) ) then
    verifica_hora =false
    exit function
 end if
 if ( (m<0 or m >59) and (not isEmpty(m)) ) then
    verifica_hora =false
    exit function
 end if
 if ( (h=24) and (m <> 0) ) then 
    verifica_hora = false
    exit function
 end if
 verifica_hora = true
end function%>
 