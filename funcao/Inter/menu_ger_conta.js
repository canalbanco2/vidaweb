if (document.all)    {n=0;ie=1;fShow="visible";fHide="hidden";}
if (document.layers) {n=1;ie=0;fShow="show";   fHide="hide";}

window.onerror=new Function("return true")

////////////////////////////////////////////////////////////////////////////
// Fun��o para direcionar os links principais do menu Home                //
////////////////////////////////////////////////////////////////////////////
var sURL;
function direciona(flag_nav)
{
	if (flag_nav == "fale") {
		window.top.location = "http://" + document.location.hostname + "/frame_novo.asp?path=/internet/inser/mail/default.asp";
	}
	else {
		if (flag_nav == "prod") {
			window.top.location = "http://" + document.location.hostname + "/frame_novo.asp?path=/internet/serv/produto/default.asp";
		}
		else {
			if (flag_nav == "inst") {
				window.top.location = "http://" + document.location.hostname + "/frame_novo.asp?path=/internet/serv/institucional/default.asp";
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////
// Function Menu()                                                        //
////////////////////////////////////////////////////////////////////////////
rightX = 0;
function Menu()
{
	this.bgColor     = "#28468F";
	if (ie) this.menuFont = "12px Arial";
	if (n)  this.menuFont = "12px Arial";
	this.fontColor   = "white";

	this.addItem    = addItem;
	this.addSubItem = addSubItem;
	this.showMenu   = showMenu;
	this.mainPaneBorder = 0;
	this.subMenuPaneBorder = 0;
	this.subMenuStyle = "#28458F 1px solid";
	this.subMenuPaneWidth = 160;

	lastMenu = null;
	
	rightY = 0;
	leftY = 0;
	leftX = 0;

	HTMLstr = "";
	HTMLstr += "<!-- MENU PANE DECLARATION BEGINS -->\n";
	HTMLstr += "\n";
//if (ie) HTMLstr += "<div id='MainTable' style='position:relative'>\n";
//	if (n)  HTMLstr += "<layer name='MainTable'>\n";
	HTMLstr += "<table width='775px' align='center' bgcolor='"+this.bgColor+"' border='"+this.mainPaneBorder+"'>\n";
	HTMLstr += "<tr>";
	if (n) HTMLstr += "<td>&nbsp;";
	HTMLstr += "<!-- MAIN MENU STARTS -->\n";
	HTMLstr += "<!-- MAIN_MENU -->\n";
	HTMLstr += "<!-- MAIN MENU ENDS -->\n";
	if (n) HTMLstr += "</td>";
	HTMLstr += "</tr>\n";
	HTMLstr += "</table>\n";
	HTMLstr += "\n";
	HTMLstr += "<!-- SUB MENU STARTS -->\n";
	HTMLstr += "<!-- SUB_MENU -->\n";
	HTMLstr += "<!-- SUB MENU ENDS -->\n";
	HTMLstr += "\n";
//	if (ie) HTMLstr+= "</div>\n";
//	if (n)  HTMLstr+= "</layer>\n";
	HTMLstr += "<!-- MENU PANE DECALARATION ENDS -->\n";
}

function addItem(idItem, text, hint, location, altLocation)
{
	var Lookup = "<!-- ITEM "+idItem+" -->";
	if (HTMLstr.indexOf(Lookup) != -1)
	{
		alert(idParent + " already exist");
		return;
	}
	var MENUitem = "";
	MENUitem += "\n<!-- ITEM "+idItem+" -->\n";
	if (n)
	{
		MENUitem += "<ilayer name="+idItem+">";
		MENUitem += "<a href='.' class=clsMenuItemNS onmouseover=\"displaySubMenu('"+idItem+"')\" onclick=\"return false;\">";
		MENUitem += "&nbsp;";
		MENUitem += text;
		MENUitem += "</a>";
		MENUitem += "</ilayer>";
	}
	if (ie)
	{
		MENUitem += "<td>\n";
		MENUitem += "<div id='"+idItem+"' style='position:relative; font: "+this.menuFont+";'>\n";
		MENUitem += "<a ";
//		MENUitem += "class=clsMenuItemIE ";
		MENUitem += "style='text-decoration: none; font: "+this.menuFont+"; color: "+this.fontColor+"; cursor: hand;' ";
		if (hint != null)
			MENUitem += "title='"+hint+"' ";
		if (location != null)
		{
			MENUitem += "href='"+location+"' target=body_index ";
			MENUitem += "onmouseover=\"hideAll()\" ";
		}
		else
		{
			if (altLocation != null)
				MENUitem += "href='"+altLocation+"' ";
			else
				MENUitem += "href='.' ";
			MENUitem += "onmouseover=\"displaySubMenu('"+idItem+"')\" ";
			MENUitem += "onclick=\"return false;\" "
		}
		MENUitem += ">";
		MENUitem += "&nbsp;\n";
		MENUitem += text;
		MENUitem += "</a>\n";
		MENUitem += "</div>\n";
		MENUitem += "</td>\n";
	}
	MENUitem += "<!-- END OF ITEM "+idItem+" -->\n\n";
	MENUitem += "<!-- MAIN_MENU -->\n";

	HTMLstr = HTMLstr.replace("<!-- MAIN_MENU -->\n", MENUitem);
}

function addSubItem(idParent, text, hint, location)
{
	var MENUitem = "";
	Lookup = "<!-- ITEM "+idParent+" -->";
	if (HTMLstr.indexOf(Lookup) == -1)
	{
		alert(idParent + " not found");
		return;
	}
	Lookup = "<!-- NEXT ITEM OF SUB MENU "+ idParent +" -->";
	if (HTMLstr.indexOf(Lookup) == -1)
	{
		if (n)
		{
			MENUitem += "\n";
			MENUitem += "<layer id='"+idParent+"submenu' visibility=hide bgcolor='"+this.bgColor+"'>\n";
			MENUitem += "<table border='"+this.subMenuPaneBorder+"' bgcolor='"+this.bgColor+"' width="+this.subMenuPaneWidth+">\n";
			MENUitem += "<!-- NEXT ITEM OF SUB MENU "+ idParent +" -->\n";
			MENUitem += "</table>\n";
			MENUitem += "</layer>\n";
			MENUitem += "\n";
		}
		if (ie)
		{
			MENUitem += "\n";
			MENUitem += "<div id='"+idParent+"submenu' style='position:absolute; visibility: hidden; width: "+this.subMenuPaneWidth+"; font: "+this.menuFont+"; top: -300;'>\n";
			MENUitem += "<table border='"+this.subMenuPaneBorder+"' bgcolor='"+this.bgColor+"' width="+this.subMenuPaneWidth+">\n";
			MENUitem += "<!-- NEXT ITEM OF SUB MENU "+ idParent +" -->\n";
			MENUitem += "</table>\n";
			MENUitem += "</div>\n";
			MENUitem += "\n";
		}
		MENUitem += "<!-- SUB_MENU -->\n";
		HTMLstr = HTMLstr.replace("<!-- SUB_MENU -->\n", MENUitem);
	}

	Lookup = "<!-- NEXT ITEM OF SUB MENU "+ idParent +" -->\n";
	if (n)  MENUitem = "<tr><td><a class=clsMenuItemNS title='"+hint+"' href='"+location+"' target=body_index>"+text+"</a><br></td></tr>\n";
//	if (ie) MENUitem = "<tr><td><a class=clsMenuItemIE title='"+hint+"' href='"+location+"' target=body_index>"+text+"</a><br></td></tr>\n";
	if (ie) MENUitem = "<tr><td><a style='font: "+this.menuFont+"; color: "+this.fontColor+"; text-decoration: none; cursor: hand;' title='"+hint+"' href='"+location+"' target=body_index>"+text+"</a><br></td></tr>\n";
	MENUitem += Lookup;
	HTMLstr = HTMLstr.replace(Lookup, MENUitem);

}

function showMenu()
{
	document.writeln(HTMLstr);
}

////////////////////////////////////////////////////////////////////////////
// Private declaration
function displaySubMenu(idMainMenu)
{
	var menu;
	var submenu;
	if (n)
	{
		submenu = document.layers[idMainMenu+"submenu"];
		if (lastMenu != null && lastMenu != submenu) hideAll();
		submenu.left = document.layers[idMainMenu].pageX;
		submenu.top  = document.layers[idMainMenu].pageY + 25;
		submenu.visibility = fShow;

		leftX  = document.layers[idMainMenu+"submenu"].left;
		rightX = leftX + document.layers[idMainMenu+"submenu"].clip.width;
		leftY  = document.layers[idMainMenu+"submenu"].top+
			document.layers[idMainMenu+"submenu"].clip.height;
		rightY = leftY;
	} else if (ie) {
		menu = eval(idMainMenu);
		submenu = eval(idMainMenu+"submenu.style");
		submenu.left = calculateSumOffset(menu, 'offsetLeft');
//		submenu.top  = calculateSumOffset(menu, 'offsetTop') + 30;
		submenu.top  = menu.style.top+23;
		submenu.visibility = fShow;
		if (lastMenu != null && lastMenu != submenu) hideAll();

		leftX  = document.all[idMainMenu+"submenu"].style.posLeft;
		rightX = leftX + document.all[idMainMenu+"submenu"].offsetWidth;

		leftY  = document.all[idMainMenu+"submenu"].style.posTop+
			document.all[idMainMenu+"submenu"].offsetHeight;
		rightY = leftY;
	}
	lastMenu = submenu;
}

function hideAll()
{
	if (lastMenu != null) {lastMenu.visibility = fHide;lastMenu.left = 0;}
}

function calculateSumOffset(idItem, offsetName)
{
	var totalOffset = 0;
	var item = eval('idItem');
	do
	{
		totalOffset += eval('item.'+offsetName);
		item = eval('item.offsetParent');
	} while (item != null);
	return totalOffset;
}

function updateIt(e)
{
	if (ie)
	{
		var x = window.event.clientX;
		var y = window.event.clientY;

		if (x > rightX || x < leftX) hideAll();
		else if (y > rightY) hideAll();
	}
	if (n)
	{
		var x = e.pageX;
		var y = e.pageY;

		if (x > rightX || x < leftX) hideAll();
		else if (y > rightY) hideAll();
	}
}

if (document.all)
{
	document.body.onclick=hideAll;
	document.body.onscroll=hideAll;
	document.body.onmousemove=updateIt;
}
if (document.layers)
{
	document.onmousedown=hideAll;
	window.captureEvents(Event.MOUSEMOVE);
	window.onmousemove=updateIt;
}


function showToolbar(str)
{
// AddItem(id, text, hint, location, alternativeLocation);
// AddSubItem(idParent, text, hint, location);
	menu = new Menu();
	menu.addItem("home", "Home", "Home",  null, null);
//	menu.addItem("agendar", "Agendar", "Agendar",  null, null);
//	menu.addItem("modificar", "Modificar", "Modificar",  null, null);
//	menu.addItem("consultar", "Consultar", "Consultar",  null, null);

//	menu.addItem("Incluir", "Incluir", "Realiza a inclus�o do Agendamento",  "https://" +  document.location.hostname + "/internet/inser/aventure_se/agendamento/default.asp" + str);

//	menu.addItem("Alterar", "Alterar", "Realiza a altera��o do Agendamento",  "https://" +  document.location.hostname + "/internet/alt/aventure_se/agendamento/default.asp" + str);

//	menu.addItem("Consultar", "Consultar", "Realiza a consulta do Agendamento",  "https://" + document.location.hostname + "/internet/serv/aventure_se/agendamento/default.asp" + str);


//	menu.addItem("ranking", "Ranking", "Ranking",  null, null);
	menu.addItem("ajuda", "Ajuda", "Ajuda",  "https://" +  document.location.hostname + "/internet/serv/aventure_se/ajuda/default.asp" + str, null);

	//As vari�veis abaixo foram utilizadas porque na chamada do comando addSubItem 
	//n�o pode-se utilizar ''.
	flag1 = "inst";
	flag2 = "prod";
	flag3 = "fale";
	menu.addSubItem("home", "Institucional", "Institucional", "javascript:direciona(flag1)");
	menu.addSubItem("home", "Produtos", "Produtos", "javascript:direciona(flag2)");
	menu.addSubItem("home", "Servi�os", "Servi�os", "http://" + document.location.hostname + "/internet/serv/menu/default.asp?flag_nav=serv");
	menu.addSubItem("home", "Corretor", "Corretor", "http://" + document.location.hostname + "/internet/serv/menu/default.asp?flag_nav=corr");
	menu.addSubItem("home", "Extranet", "Extranet", "http://" + document.location.hostname + "/internet/serv/menu/default.asp?flag_nav=extra");
	menu.addSubItem("home", "Intranet", "Intranet", "http://" + document.location.hostname + "/internet/serv/menu/default.asp?flag_nav=intra");
	menu.addSubItem("home", "Fale Conosco", "Fale Conosco", "javascript:direciona(flag3)");

//	menu.addSubItem("agendar", "Realizar Agendamento", "Realizar Agendamento",  "https://" +  document.location.hostname + "/internet/inser/aventure_se/agendamento/default.asp" + str);

//	menu.addSubItem("modificar", "Alterar Agendamento", "Alterar Agendamento",  "https://" +  document.location.hostname + "/internet/alt/aventure_se/agendamento/default.asp" + str);

//	menu.addSubItem("consultar", "Listar Agendamento", "Listar Agendamento",  "https://" + document.location.hostname + "/internet/serv/aventure_se/agendamento/default.asp" + str);

//	menu.addSubItem("ranking", "Gerente de Contas", "Gerente de Contas",  "http://" +  document.location.hostname + "/internet/serv/aventure_se/ranking_ger_conta/default.asp" + str);
//	menu.addSubItem("ranking", "Gerente de Neg�cios", "Gerente de Neg�cios",  "http://" +  document.location.hostname + "/internet/serv/aventure_se/ranking_ger_negocio/default.asp" + str);
//	menu.addSubItem("ranking", "Ag�ncias", "Ag�ncias",  "https://" + document.location.hostname + "/internet/serv/aventure_se/ranking_agencia/default.asp" + str);
//	menu.addSubItem("ranking", "Super", "Super",  "https://" + document.location.hostname + "/internet/serv/aventure_se/ranking_super/default.asp" + str);
	
	menu.showMenu();
}
