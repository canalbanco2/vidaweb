<script language="JavaScript">
function valida_filho(valor,caixa)
{
	if ((valor == '33-Manuais de Normas - MANOR') || (valor == '21-Manual de Procedimentos Operacionais Padrões - MAPOP'))
	{
		document.form1.site_filho.disabled = false;
		
		//apagando os dados do combo de site_filho
		total = caixa.options.length
		while (total > 1)
		{
			caixa.options[total-1] = null
			total = total - 1
		}
		if (valor == '33-Manuais de Normas - MANOR')
		{
			val = '12-Administração Geral - ADGER'
			teste = new Option('Administração Geral - ADGER', val)
			caixa.options[1] = teste
			
			val = '13-Comercialização e Publicidade - ADCOM'
			teste = new Option('Comercialização e Publicidade - ADCOM', val)
			caixa.options[2] = teste

			val = '14-Tecnologia da Informação e Telecomunicações - ADTIN'
			teste = new Option('Tecnologia da Informação e Telecomunicações - ADTIN', val)
			caixa.options[3] = teste

			val = '32-Administração Financeira - ADFIN'
			teste = new Option('Administração Financeira - ADFIN', val)
			caixa.options[4] = teste

			val = '15-Cotação e Emissão - ADEMI'
			teste = new Option('Cotação e Emissão - ADEMI', val)
			caixa.options[5] = teste

			val = '16-Regulação e Liquidação - ADLIQ'
			teste = new Option('Regulação e Liquidação - ADLIQ', val)
			caixa.options[6] = teste

			val = '17-Reservas e Limites Técnicos - ADRES'
			teste = new Option('Reservas e Limites Técnicos - ADRES', val)
			caixa.options[7] = teste

			val = '18-Planos de Resseguro e Cosseguro - ADREC'
			teste = new Option('Planos de Resseguro e Cosseguro - ADREC', val)
			caixa.options[8] = teste

			val = '19-Produtos - ADPRO'
			teste = new Option('Produtos - ADPRO', val)
			caixa.options[9] = teste

			val = '20-Técnico-Operacional - ADTOP'
			teste = new Option('Técnico-Operacional - ADTOP', val)
			caixa.options[10] = teste
		}
		else
		{
			if (valor == '21-Manual de Procedimentos Operacionais Padrões - MAPOP')
			{
				val = '22-Administração Geral - PPGER'
				teste = new Option('Administração Geral - PPGER', val)
				caixa.options[1] = teste
			
				val = '23-Comercialização e Publicidade - PPCOM'
				teste = new Option('Comercialização e Publicidade - PPCOM', val)
				caixa.options[2] = teste

				val = '24-Tecnologia da Informação e Telecomunicações - PPTIN'
				teste = new Option('Tecnologia da Informação e Telecomunicações - PPTIN', val)
				caixa.options[3] = teste

				val = '25-Administração Financeira - PPFIN'
				teste = new Option('Administração Financeira - PPFIN', val)
				caixa.options[4] = teste

				val = '26-Cotação e Emissão - PPEMI'
				teste = new Option('Cotação e Emissão - PPEMI', val)
				caixa.options[5] = teste

				val = '27-Regulação e Liquidação - PPLIQ'
				teste = new Option('Regulação e Liquidação - PPLIQ', val)
				caixa.options[6] = teste

				val = '28-Reservas e Limites Técnicos - PPRES'
				teste = new Option('Reservas e Limites Técnicos - PPRES', val)
				caixa.options[7] = teste

				val = '29-Planos de Resseguro e Cosseguro - PPREC'
				teste = new Option('Planos de Resseguro e Cosseguro - PPREC', val)
				caixa.options[8] = teste

				val = '30-Produtos - PPPRO'
				teste = new Option('Produtos - PPPRO', val)
				caixa.options[9] = teste

				val = '31-Técnico-Operacional - PPTOP'
				teste = new Option('Técnico-Operacional - PPTOP', val)
				caixa.options[10] = teste
			}
		}
		caixa.selectedIndex = 0
	}
	else
	{
		document.form1.site_filho.disabled = true;
	}
}
</script>
