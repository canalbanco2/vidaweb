<%
function f_num(cn,vn)
	if isnumeric(vn) = false or instr(1,vn,",") <> 0 or instr(1,vn,".") <> 0 or instr(1,vn,",") <> 0 or instr(1,vn,"E") <> 0 or instr(1,vn,"+") <> 0 or instr(1,vn,"-") <> 0 then
		f_num =   vbcrlf & "O campo '" & cn & "' deve ser num�rico." 
	end if
end function

function msg_vnum(obrig,campo,valor)
	if (obrig) then
		m_valid = msg_vnull(campo,valor)
		if (not isEmpty(m_valid)) then
			msg_vnum = m_valid 
			exit function
		end if
		msg_vnum = f_num(campo,valor)
	else
		if trim(valor) <> "" then
			msg_vnum = f_num(campo,valor)
		end if
	end if
end function
%>