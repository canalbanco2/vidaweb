<%
function verifica_cpf(valor) 
    if trim(valor) = "" then
        verifica_cpf = true
        exit function
    end if
	if ((not isnumeric(valor)) or (len(valor) <> 11)) then
        verifica_cpf = false
        exit function
    end if
    
	Mult1 = 10   
	Mult2 = 11
	dig1 = 0
	dig2 = 0
	
	for i = 0 to 8
		ind = mid(valor,i+1,1)
		dig1 = dig1 + (cdbl(ind) * Mult1)
		Mult1 = Mult1 - 1
	next
	
	for i = 0 to 9
		ind = mid(valor,i+1,1)
		dig2 = dig2 + (cdbl(ind) * Mult2)
		Mult2 = Mult2 - 1
	next

	dig1 = (dig1 * 10) mod 11   
	dig2 = (dig2 * 10) mod 11   

	if (dig1 = 10) then dig1 = 0
      
	if (dig2 = 10) then dig2 = 0
	 
	if (cdbl(mid(valor,10,1)) <> dig1) then
		verifica_cpf = false   
		exit function
	end if
	
	if (cdbl(mid(valor,11,1)) <> dig2) then
		verifica_cpf = false   
		exit function
	end if
	verifica_cpf = true

	'Verificar a digita��o de CPF com todos os d�gitos iguais
	redim igual(11)
	for i = 1 to 11
		if i = 1 then
			anterior = mid(valor,i,1)
		else
			if anterior = mid(valor,i,1) then
				igual(i) = "sim"
			end if
			anterior = mid(valor,i,1)
		end if
	next
	cont = 1
	for i = 1 to 11
		if igual(i) = "sim" then
			cont = cont + 1
		end if
	next
	if cont = ubound(igual) then verifica_cpf = false
end function
%>