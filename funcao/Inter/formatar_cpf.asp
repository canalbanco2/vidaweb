<%
function formatar_cpf(cpf)
    if len(trim(cpf)) <> 11 then
        exit function
    end if
    
    part1 = left(cpf,3)
    part2 = mid(cpf,4,3)
    part3 = mid(cpf,7,3)
    part4 = right(cpf,2)
    
    formatar_cpf = part1 & "." & part2 & "." & part3 & "-" & part4   
end function%>
