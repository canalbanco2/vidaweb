<%
function f_cpf(cvg,vcg)
	if not verifica_cpf(vcg) then
		f_cpf = vbcrlf & "O campo '" & cvg & "' � inv�lido." 
	end if
end function

function msg_vcpf(obrig,campo,valor)
	if obrig then
		m_valid = msg_vnull(campo,valor)
		if trim(m_valid) <> "" then
			msg_vcpf = m_valid
		else
		    msg_vcpf = f_cpf(campo,valor)
		end if
	else
		msg_vcpf = f_cpf(campo,valor)
	end if
end function
%>