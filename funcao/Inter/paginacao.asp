<%
' Declare our vars
Dim iPageSize       'How big our pages are
Dim iPageCount      'The number of pages we get back
Dim iPageCurrent    'The page we want to show

' Vari�veis recebidas por par�metro para executar a pagina��o
' iPageSize - quantidade de registros apresentados
' Labels - array com os labels a serem apresentados
' Link - link que ser� utilizado na navega��o (sintaxe = <nome do arquivo>.asp?<parametros>&)
' TableWidth - tamanho da tabela
' LinkProxima1 - primeiro peda�o do link que o primeiro item deve receber para ir para a p�gina com os dados (sintaxe = "<a href='[nome do arquivo].asp?[parametro]=" ou "<a onclick='[nome da funcao](")
' LinkProxima2 - segundo peda�o do link citado do item anterior (sintaxe = "'>" ou ")'>")

' Observa��es importantes:
' 1. Na query que ser� utilizada na pagina��o, devem conter apenas os campos que ser�o utilizados.
' 2. O primeiro campo da query sempre deve ser o ID.
' 3. O segundo campo da query receber� o link para a pr�xima p�gina.

' Retrieve page to show or default to 1

If Request.QueryString("page") = "" Then
	iPageCurrent = 1
Else
	iPageCurrent = CInt(Request.QueryString("page"))
End If

' Now we finally get to the DB work...
' Create and open our connection
set conex = Server.CreateObject("ADODB.CONNECTION")
conex.CursorLocation = 3
conex.open ConnectString

' Create recordset and set the page size
Set rs = Server.CreateObject("ADODB.Recordset")
rs.PageSize = iPageSize

' You can change other settings as with any RS
rs.CacheSize = iPageSize

' Open RS
rs.open sql, conex

' Get the count of the pages using the given page size
iPageCount = rs.PageCount

' If the request page falls outside the acceptable range,
' give them the closest match (1 or max)
If iPageCurrent > iPageCount Then iPageCurrent = iPageCount
If iPageCurrent < 1 Then iPageCurrent = 1

' Check page count to prevent bombing when zero results are returned!
If iPageCount = 0 Then
	Response.Write "<table class='escolha' align='center' cellspacing='0' style='width: " & TableWidth & "'><tr><td class='td_dado' style='text-align:center'>Nenhuma informa��o encontrada.</td></tr></table>" & vbCrLf
Else
	' Move to the selected page
	rs.AbsolutePage = iPageCurrent

	' Start output with a page x of n line
'***	Response.Write "<table class='escolha' align='center' cellspacing='0' style='width: " & TableWidth & "'>" & vbCrLf
'***	Response.Write "<tr><td class='td_dado'>P�gina&nbsp;<strong>" & iPageCurrent & "</strong>"
'***	Response.Write "&nbsp;de&nbsp;<strong>" & iPageCount & "</strong></td></tr>" & vbCrLf
'***	Response.Write "</table><br>"

	' Spacing
	Response.Write vbCrLf

	' Continue with a title row in our table
	Response.Write "<table class='escolha' align='center' cellspacing='0' cellpadding='2' style='width: " & TableWidth & "'>" & vbCrLf

	' Show field names in the top row
	Response.Write "<tr>"
	for I = 1 to ubound(Labels)
		If (InStrRev(Labels(I), "#valor") <> 0) then 
			tamanho = len("#valor")
			Response.Write "<td class='td_label' style='TEXT-ALIGN:right'><b>" & left(Labels(I), len(Labels(I)) - tamanho) & "</b></td>" & vbCrLf
		Else
			Response.Write "<td class='td_label'><b>" & Labels(I) & "</b></td>" & vbCrLf
		End If
	next
	Response.Write "</tr>"

	' Loop through our records and ouput 1 row per record
	iRecordsShown = 0
	Do While iRecordsShown < iPageSize And Not rs.EOF
		Response.Write vbTab & "<tr>" & vbCrLf
		For I = 1 To rs.Fields.Count - 1
			If (InStrRev(Labels(I), "#valor") <> 0) then
			    Response.Write vbTab & vbTab & "<td class='td_dado' style='TEXT-ALIGN:right'>"
			Else
				Response.Write vbTab & vbTab & "<td class='td_dado'>"
			End If
		    ' Quando for o 1� campo deve conter o link para a p�gina seguinte
		    If not (trim(rs.Fields(I)="")) then
				If I = 1 and LinkProxima1 <> "" and LinkProxima2 <> "" Then
					Response.Write LinkProxima1 & rs.Fields(0) & LinkProxima2
			    End If
				If (InStrRev(Labels(I), "#valor") <> 0) then
					Response.Write formatnumber(rs.Fields(I),2)
				Else
					Response.Write rs.Fields(I)
				End If
			Else
				Response.Write "---"
			End If
		    If I = 1 and LinkProxima1 <> "" and LinkProxima2 <> "" Then
				Response.Write "</a>"
		    End If
		    Response.Write "</td>" & vbCrLf
		Next 'I
		Response.Write vbTab & "</tr>" & vbCrLf

		' Increment the number of records we've shown
		iRecordsShown = iRecordsShown + 1
		' Can't forget to move to the next record!
		rs.MoveNext
	Loop

	' All done - close table
	Response.Write "</table>" & vbCrLf
End If

' Close DB objects and free variables
rs.Close
Set rs = Nothing
conex.Close
Set conex = Nothing

' Show "previous" and "next" page links which pass the page to view
' and any parameters needed to rebuild the query.  You could just as
' easily use a form but you'll need to change the lines that read
' the info back in at the top of the script.
if trim(iPageCount) <> "" and trim(iPageCount) <> "0" then
	Response.Write "<br><table class='escolha' align='center' cellspacing='0' cellpadding='2' style='width: " & TableWidth & "'><tr><td class='td_dado' style='text-align:center'>P�gina&nbsp;-" & vbCrLf
	If iPageCurrent > 1 Then
		Response.Write "<a class='escolha_dado' href='" & Link & "page=" & iPageCurrent - 1 & "'>[<< Anterior]</a>  "
	End If
end if

' You can also show page numbers:
For I = 1 To iPageCount
	If I = iPageCurrent Then
		Response.Write I
	Else
		Response.Write "<a class='escolha_dado' href='" & Link & "page=" & I & "'>" & I & "</a>  "
	End If
Next 'I

If iPageCurrent < iPageCount Then
	Response.Write "<a class='escolha_dado' href='" & Link & "page=" & iPageCurrent + 1 & "'>[Pr�xima >>]</a>  "
End If
Response.Write "</td></tr></table>"
%>
