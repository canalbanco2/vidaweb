<%
function DataCorrente()
	Dim dia_a
	Dim mes_a
	Dim ano_a
	Dim textoFinal

	dia_a = day(date())
	mes_a = month(date())
	'mes_a = day(date())
	'dia_a = month(date())
	ano_a = year(date())
	
	if len(dia_a) < 2 then dia_a = "0" & dia_a end if
	if len(mes_a) < 2 then mes_a = "0" & mes_a end if
	
	textoFinal = dia_a & "/" & mes_a & "/" & ano_a
	DataCorrente = Trim(textoFinal)
end function

function DataHoraCorrente()
	Dim dia_a
	Dim mes_a
	Dim ano_a
	Dim hora_a
	Dim min_a
	Dim textoFinal

	dia_a = day(date())
	mes_a = month(date())
	'mes_a = day(date())
	'dia_a = month(date())
	ano_a = year(date())
	hora_a = hour(now())
	min_a = minute(now())
	
	if len(dia_a) < 2 then dia_a = "0" & dia_a end if
	if len(mes_a) < 2 then mes_a = "0" & mes_a end if
	if len(hora_a) < 2 then hora_a = "0" & hora_a end if
	if len(min_a) < 2 then min_a = "0" & min_a end if
	
	textoFinal = dia_a & "/" & mes_a & "/" & ano_a & "&nbsp;" & hora_a & ":" & min_a
	DataHoraCorrente = Trim(textoFinal)
end function
%>