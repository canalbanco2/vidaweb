<%
' Declare our vars
Dim iPageSize       'How big our pages are
Dim iPageCount      'The number of pages we get back
Dim iPageCurrent    'The page we want to show
Dim iRecordCount

Dim IndiceJavaScript
IndiceJavaScript = 0

' Vari�veis recebidas por par�metro para executar a pagina��o
' iPageSize - quantidade de registros apresentados
' Labels - array com os labels a serem apresentados
' Link - link que ser� utilizado na navega��o (sintaxe = <nome do arquivo>.asp?<parametros>&)
' TableWidth - tamanho da tabela

' Observa��es importantes:
' 1. Na query que ser� utilizada na pagina��o, devem conter apenas os campos que ser�o utilizados.
' 2. O primeiro campo da query sempre deve ser o ID.
' 3. O segundo campo da query receber� o link para a pr�xima p�gina.

' Retrieve page to show or default to 1
If Request.QueryString("page") = "" Then
	If Request.Form("page") = "" Then
		iPageCurrent = 1
	Else
		iPageCurrent = CInt(Request.Form("page"))
	End if
Else
	iPageCurrent = CInt(Request.QueryString("page"))
End If

' Now we finally get to the DB work...
' Create and open our connection
set conex = Server.CreateObject("ADODB.CONNECTION")
conex.CursorLocation = 3

conex.open ConnectString

' Create recordset and set the page size
Set rs = Server.CreateObject("ADODB.Recordset")
rs.PageSize = iPageSize

' You can change other settings as with any RS
rs.CacheSize = iPageSize

' Open RS
rs.open sql, conex
rs.ActiveConnection = Nothing

If IsObject(rs) Then
	If rs.State = 1 Then
		iRecordCount = rs.RecordCount
		' Get the count of the pages using the given page size
		iPageCount = rs.PageCount
	Else
		iRecordCount = 0 
	End IF
Else
	iRecordCount = 0
End If 


' If the request page falls outside the acceptable range,
' give them the closest match (1 or max)
If iPageCurrent > iPageCount Then iPageCurrent = iPageCount
If iPageCurrent < 1 Then iPageCurrent = 1

' Check page count to prevent bombing when zero results are returned!
If iPageCount = 0 Then
	Response.Write "<table class='escolha' align='center' cellspacing='0' style='width: 100%'><tr><td class='td_dado' style='text-align:center'>Nenhuma informa��o encontrada.</td></tr></table>" & vbCrLf
Else
	' Move to the selected page
	rs.AbsolutePage = iPageCurrent

	' Start output with a page x of n line
	' Spacing
	Response.Write vbCrLf
	paginas =  rs.PageSize
	if not iPageCurrent < iPageCount then
		paginas = (iPageCurrent - 1) * rs.PageSize + 1
		paginas_fim = rs.RecordCount
	else
		paginas = (iPageCurrent - 1) * rs.PageSize + 1
		paginas_fim = paginas + rs.PageSize - 1
	end if
	' Continue with a title row in our table
	Response.Write "<table width='100%' class='escolha' cellspacing='1' cellpadding='2' border=0>" & vbCrLf
	Response.Write "<tr>" & vbCrLf
	Response.Write "<td align=left><span style=""color:DarkRed;font-weight:bold;font-size:12px;width:100%; text-align:left"">Selecione a ""Solicita��o de Cota��o"" abaixo:</span></td>" & vbCrLf
	Response.Write "<td class=""data"">&nbsp;" & paginas & "-" & paginas_fim & " de " & rs.RecordCount & " solicita��es&nbsp;</td>" & vbCrLf
	Response.Write "</tr>" & vbCrLf
	Response.Write "</table>" & vbCrLf
%>
	<script language="JavaScript">
	var x = screen.width;
	var y = screen.height;
	
	if ( (x > 1152) || (y > 864) )
		document.write("<div style='position:relative; overflow:scroll; width:825px; height:165px; left:2px; top:0px;' id='divInstancias'>");
	else if ( (x > 1024) || (y > 768) )
		document.write("<div style='position:relative; overflow:scroll; width:825px; height:165px; left:2px; top:0px;' id='divInstancias'>");
	else if ( (x > 800) || (y > 600) )
		document.write("<div style='position:relative; overflow:scroll; width:825px; height:165px; left:2px; top:0px;' id='divInstancias'>");
	else if ( (x > 640) || (y > 480) )
		document.write("<div style='position:relative; overflow:scroll; width:825px; height:165px; left:2px; top:0px;' id='divInstancias'>");
	</script>
<%
	Response.Write "<table align=""center"" class='escolha' cellspacing='0' cellpadding='2' border='1' bordercolor='#EEEEEE' style='width: " & TableWidth & "'>" & vbCrLf

	' Show field names in the top row
	
	Response.Write "<tr>"
	for I = 1 to ubound(Labels)
		If (InStrRev(Labels(I), "#valor") <> 0) then 
			tamanho = len("#valor")
			Response.Write "<td class='td_label' style='TEXT-ALIGN:right' nowrap>" & left(Labels(I), len(Labels(I)) - tamanho)
		ElseIf (InStrRev(Labels(I), "#cnpj") <> 0) then 
			tamanho = len("#cnpj")
			Response.Write "<td class='td_label' nowrap>" & left(Labels(I), len(Labels(I)) - tamanho)
		Else
			Response.Write "<td class='td_label' nowrap>" & Labels(I)
		End If
		if trim(strLink) = "" then
			Response.Write "<a href='corpo2a.asp?ord=" & OrdAsc(I) & "'><IMG SRC='\img\seta_cima_bnb.gif' border='0'></a>"
			Response.Write "<a href='corpo2a.asp?ord=" & OrdDesc(I) & "'><IMG SRC='\img\seta_baixo_bnb.gif' border='0'></a>"
		else
			Response.Write "<a href='corpo2a.asp?" & strLink & "&ord=" & OrdAsc(I) & "'><IMG SRC='\img\seta_cima.gif' border='0'></a>"
			Response.Write "<a href='corpo2a.asp?" & strLink & "&ord=" & OrdDesc(I) & "'><IMG SRC='\img\seta_baixo.gif' border='0'></a>"
		end if
		Response.Write "</td>" & vbCrLf
	next
	Response.Write "</tr>"

	' Loop through our records and output 1 row per record
	iRecordsShown = 0
	Do While iRecordsShown < iPageSize And Not rs.EOF
		Response.Write vbTab & "<tr>" & vbCrLf
		For I = 1 To ubound(Labels) - 1 
			If (InStrRev(lcase(Labels(I)), "valor") <> 0) then
			    Response.Write vbTab & vbTab & "<td class='td_dado' style='TEXT-ALIGN:right' nowrap>"
			Else
				Response.Write vbTab & vbTab & "<td class='td_dado' nowrap>"
			End If
		    ' Quando for o 1� campo deve conter o link para a p�gina seguinte
		    
		    If not (trim(rs.Fields(NomeCampos(I))="")) then
				select case rs("situacao")
					case "Ativa", "Ativo"
						sit = "1"
					case "Encerrado"
						sit = "2"
					case "Suspenso"
						sit = "3"
					case "Cancelado"
						sit = "9"
				end select
				If I = 1 then
					'Quando o resultado da query retorna apenas 1 registro,
					'n�o pode utilizar o radio como um array.
					if iRecordCount = 1 then%>
						<INPUT TYPE="radio" NAME="instancias" value="<%=rs("Numero")%>" OnClick="exibeaguarde();document.frmInstancias.wf_id.value = '<%=rs("Numero")%>';document.frmInstancias.tp_wf_id.value = '<%=rs("tp_wf_id")%>';document.frmInstancias.instancias2.checked=true;MostraAtividade('<%=rs("Numero")%>','<%=link%>','<%=sit%>','<%=iPageCurrent%>','<%=rs("tp_wf_id")%>');"<%if trim(request("wf_id")) = trim(rs("Numero")) then%> checked<%end if%>>
					<%else%>
						<INPUT TYPE="radio" NAME="instancias" value="<%=rs("Numero")%>" OnClick="exibeaguarde();document.frmInstancias.wf_id.value = '<%=rs("Numero")%>';document.frmInstancias.tp_wf_id.value = '<%=rs("tp_wf_id")%>';document.frmInstancias.instancias2[<%=indiceJavascript%>].checked=true;MostraAtividade('<%=rs("Numero")%>','<%=link%>','<%=sit%>','<%=iPageCurrent%>','<%=rs("tp_wf_id")%>');"<%if trim(request("wf_id")) = trim(rs("Numero")) then%> checked<%end if%>>
					<%end if
				End If

				If (InStrRev(lcase(Labels(I)), "valor") <> 0) then
					Response.Write formatnumber(rs.Fields(NomeCampos(I)),2) 
				ElseIf (InStrRev(Labels(I), "#cnpj") <> 0) then
					Response.Write formataCNPJ(rs.Fields(NomeCampos(I)))
				ElseIf (InStrRev(Labels(I), "Dt") <> 0) then
					Response.Write rs.Fields(NomeCampos(I)) 
				ElseIf (InStrRev(Labels(I), "Ag�ncia") <> 0) then
					Response.Write rs.Fields("agencia") 
					
					'*********************************************************************************************
					'ALTERADO EM 18/05/2006 DEVIDO � DUPLICIDADE DOS DADOS TRAZIDO PELA SP
					'if not isnull(rs.Fields("agencia_nome")) and trim(rs.Fields("agencia_nome")) <> "" then
					'	response.Write " - " & rs.Fields("agencia_nome")
					'end if
					'*********************************************************************************************
					
				Else
					Response.Write rs.Fields(NomeCampos(I))
				End If

			Else
				Response.Write "---"
			End If
		    Response.Write "</td>" & vbCrLf
		Next 'I
		'Quando o resultado da query retorna apenas 1 registro,
		'n�o pode utilizar o radio como um array.
		if iRecordCount = 1 then%>
			<Td class='td_dado' nowrap><INPUT TYPE="radio" NAME="instancias2" value="<%=rs("Numero")%>" OnClick="exibeaguarde();document.frmInstancias.wf_id.value = '<%=rs("Numero")%>';document.frmInstancias.tp_wf_id.value = '<%=rs("tp_wf_id")%>';document.frmInstancias.instancias.checked=true;MostraAtividade('<%=rs("Numero")%>','<%=link%>','<%=sit%>','<%=iPageCurrent%>','<%=rs("tp_wf_id")%>');"<%if trim(request("wf_id")) = trim(rs("Numero")) then%> checked<%end if%>><%=rs("Numero")%></td>
		<%else%>
			<Td class='td_dado' nowrap><INPUT TYPE="radio" NAME="instancias2" value="<%=rs("Numero")%>" OnClick="exibeaguarde();document.frmInstancias.wf_id.value = '<%=rs("Numero")%>';document.frmInstancias.tp_wf_id.value = '<%=rs("tp_wf_id")%>';document.frmInstancias.instancias[<%=indiceJavascript%>].checked=true;MostraAtividade('<%=rs("Numero")%>','<%=link%>','<%=sit%>','<%=iPageCurrent%>','<%=rs("tp_wf_id")%>');"<%if trim(request("wf_id")) = trim(rs("Numero")) then%> checked<%end if%>><%=rs("Numero")%></td><%end if
		indiceJavascript = indiceJavascript + 1
		Response.Write vbTab & "</tr>" & vbCrLf

		' Increment the number of records we've shown
		iRecordsShown = iRecordsShown + 1
		' Can't forget to move to the next record!
		rs.MoveNext
	Loop

	' All done - close table
	Response.Write "</table>" & vbCrLf
End If
Response.Write "</div>" & vbCrLf

' Close DB objects and free variables
Set rs = Nothing
Set conex = Nothing

' Show "previous" and "next" page links which pass the page to view
' and any parameters needed to rebuild the query.  You could just as
' easily use a form but you'll need to change the lines that read
' the info back in at the top of the script.
if trim(iPageCount) <> "" and trim(iPageCount) <> "0" then
	Response.Write "<br><table class='escolha' align='center' cellspacing='0' cellpadding='0'><tr><td class='td_dado' style='text-align:center' nowrap>P�gina&nbsp;-" & vbCrLf
	If iPageCurrent > 1 Then
		Response.Write "<a class='escolha_dado' href='" & Link & "page=" & iPageCurrent - 1 & "'>[<< Anterior]</a>  "
	End If
end if

' You can also show page numbers:

Dim i 
Dim paginaInicial, paginaFinal 
paginaIni = 1
paginaFim = iPageCount

if iPageCount > 10 then
	paginaIni = max (1,iPageCurrent -4)
	paginaFim = min (iPageCount,iPageCurrent +4)
	
	if paginaIni = 1 then
		paginaFim = 10
	elseif paginaFim = iPageCount then
		paginaIni = iPageCount - 10
	end if
end if

For I = paginaIni To paginaFim
	If I = iPageCurrent Then
		Response.Write "<b>" & I & "</b>  "
	Else
		Response.Write "<a class='escolha_dado' href='" & Link & "page=" & I & "'>" & I & "</a>  "
	End If
Next 'I

If iPageCurrent < iPageCount Then
	Response.Write "<a class='escolha_dado' href='" & Link & "page=" & iPageCurrent + 1 & "'>[Pr�xima >>]</a>  "
End If

function max(vlr1,vlr2)
	if vlr1 > vlr2 then
		max = vlr1
	else
		max = vlr2
	end if
end function

function min(vlr1,vlr2)
	if vlr1 < vlr2 then
		min = vlr1
	else
		min = vlr2
	end if
end function

Response.Write "</td></tr></table>"
%>