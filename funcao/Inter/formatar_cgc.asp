<%
function formatar_cgc(cgc)
    if len(trim(cgc)) <> 14 then
        exit function
    end if
    
    part1 = left(cgc,2)
    part2 = mid(cgc,3,3)
    part3 = mid(cgc,6,3)
    part4 = mid(cgc,9,4)
    part5 = right(cgc,2)
    
    formatar_cgc = part1 & "." & part2 & "." & part3 & "/" & part4  & "-" & part5 
end function%>
