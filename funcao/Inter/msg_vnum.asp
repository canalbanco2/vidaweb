<script language=JScript>	
function msg_vnum(obrig,campo,valor)
{	
  function f_num(cn,vn)
  {
    strVn = vn.toString(10)
    if ((isNaN(vn)) || ((strVn.indexOf(",") != -1) || (strVn.indexOf(".") != -1) || (strVn.indexOf("E") != -1) || (strVn.indexOf("+") != -1) || (strVn.indexOf("-") != -1)))
      return  "\n" + "O valor '" + cn + "' deve ser num�rico."
    return "  "
  }

  if (obrig)
  {	
    m_valid = msg_vnull(campo,valor)
    if (! isEmpty(m_valid)) 
      return m_valid
    return f_num(campo,valor)
  }
  else
	return f_num(campo,valor)

}
</script>