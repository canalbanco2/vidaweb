<script>
<!--
function check_edicao()
{
    var colunas = document.all.tags("input");
    if(colunas!=null)
       if(check_interno(colunas)==false) return false; 

    var colunas = document.all.tags("select");
    if(colunas!=null)
       if(check_interno(colunas)==false) return false;     

    var colunas = document.all.tags("textarea");
    if(colunas!=null)
       if(check_interno(colunas)==false) return false; 
    return true;
}

function check_interno(coll)
{
    for(i=0;i<coll.length;i++){
        var strVn = (coll[i].value.toString()).replace(' ','');
        var tam = strVn.length;

        if(coll[i].obrig=='ya')
           if(tam==0){
              if(coll[i].erro==undefined) alert("O valor do campo deve ser preenchido.");
              else                        alert("O valor '" + coll[i].erro + "' deve ser preenchido.");
              eval('document.'+coll[i].form.name+'.'+coll[i].name+'.focus()');
              return false;
           }

        if(coll[i].zero=='no')
           if(tam==0||coll[i].value==0){
              if(coll[i].erro==undefined) alert("O valor do campo n�o deve ser zerado.");
              else                        alert("O valor '" + coll[i].erro + "' n�o deve ser zerado.");
              eval('document.'+coll[i].form.name+'.'+coll[i].name+'.focus()');
              return false;
           }

        if(coll[i].funcao!=undefined){
           var mensagem = eval(coll[i].funcao);  
           if(mensagem!=''){
              alert(mensagem);
              eval('document.'+coll[i].form.name+'.'+coll[i].name+'.focus()');
              return false;
           } 
        }
 
        if(tam>0){
           if(coll[i].validacao=="cnpj")
              if(verifica_cnpj(strVn)==false){  
                 alert("N�mero de CNPJ inv�lido.");
                 eval('document.'+coll[i].form.name+'.'+coll[i].name+'.focus()');
                 return false;
              } 
               
           if((coll[i].validacao=="inteiro")||(coll[i].validacao=="numerico")||(coll[i].validacao=="data1")||(coll[i].validacao=="data2"))
           {
              if(coll[i].validacao=="numerico") strVn = (strVn.replace('.','')).replace(',','');
              else{
                 if(strVn.indexOf('+')>-1||strVn.indexOf('-')>-1||strVn.indexOf('e')>-1){
                    if(coll[i].erro==undefined) alert("O valor do campo deve ser inteiro.");
                    else                        alert("O valor '" + coll[i].erro + "' deve ser inteiro.");
                    eval('document.'+coll[i].form.name+'.'+coll[i].name+'.focus()');
                    return false;
                 }
              }

              if(parseFloat(strVn)!=strVn){
                 if(coll[i].erro==undefined) alert("O valor do campo deve ser num�rico.");
                 else                        alert("O valor '" + coll[i].erro + "' deve ser num�rico.");
                 eval('document.'+coll[i].form.name+'.'+coll[i].name+'.focus()');
                 return false;
              }
           }

           if(coll[i].validacao=="data1"||coll[i].validacao=="data2"){
              var dia = eval('document.'+coll[i].form.name+'.'+coll[i].cpoDia+'.value');
              var mes = eval('document.'+coll[i].form.name+'.'+coll[i].cpoMes+'.value');

              if((mes==2&&dia>29)||((mes==4||mes==6||mes==9||mes==11)&&dia>30)||(dia>31)){
                 if(coll[i].erro==undefined) alert("O valor do campo � inv�lido.");
                 else                        alert("O valor '" + coll[i].erro + "' � inv�lido.");
                 eval('document.'+coll[i].form.name+'.'+coll[i].name+'.focus()');
                 return false;
              }
           }
               
           if(coll[i].validacao=="data2"){               
              var diax = eval('document.'+coll[i].form.name+'.'+coll[i].cpoDiaI+'.value');
              if(diax!=''){
                 var mesx = eval('document.'+coll[i].form.name+'.'+coll[i].cpoMesI+'.value');
                 var anox = eval('document.'+coll[i].form.name+'.'+coll[i].cpoAnoI+'.value');
                 var datax = parseInt(anox)*10000+parseInt(mesx)*100+parseInt(diax);
                 var datay = parseInt(strVn)*10000+parseInt(mes)*100+parseInt(dia);
                 if(parseFloat(datax)>parseFloat(datay)){
                     alert("Data final deve ser maior que Data inicial.");
                     eval('document.'+coll[i].form.name+'.'+coll[i].name+'.focus()');
                     return false;
                 }
              } 
           }

           if(~isNaN(coll[i].qtdMax))
              if(tam>coll[i].qtdMax){
                 if(coll[i].erro==undefined) alert("O valor do campo excedeu o n�mero m�ximo de caracteres.");
                 else                        alert("O valor '" + coll[i].erro + "' excedeu o n�mero m�ximo de caracteres.");
                 eval('document.'+coll[i].form.name+'.'+coll[i].name+'.focus()');
                 return false;
              }

           if(~isNaN(coll[i].qtdMin))
              if(tam<coll[i].qtdMin){
                 if(coll[i].erro==undefined) alert("O valor do campo n�o possui o n�mero m�nimo de caracteres.");
                 else                        alert("O valor '" + coll[i].erro + "' n�o possui o n�mero m�nimo de caracteres.");
                 eval('document.'+coll[i].form.name+'.'+coll[i].name+'.focus()');
                 return false;
              }

           if(~isNaN(coll[i].valMax))
              if(parseFloat(coll[i].value.replace(',','.'))>parseFloat(coll[i].valMax)){
                 if(coll[i].erro==undefined) alert("O valor do campo � maior que o permitido ("+coll[i].valMax+").");
                 else                        alert("O valor '" + coll[i].erro + "' � maior que o permitido ("+coll[i].valMax+").");
                 eval('document.'+coll[i].form.name+'.'+coll[i].name+'.focus()');
                 return false;
              }
           }
       }
}

function verifica_cnpj(valor)
{
	if ((isNaN(valor)) && (valor.length != 14))
		return false
		
	Mult1 = "543298765432"
	Mult2 = "6543298765432"
	dig1=0
	dig2=0
		
	for(var i=0;i<=11;i++)   
	{
		ind=valor.charAt(i)
		M=Mult1.charAt(i)
		dig1 += ((parseFloat(ind)) *  (parseFloat(M)))
	}
	
	for( var i=0;i<=12;i++)   
	{
		ind=valor.charAt(i)
		M=Mult2.charAt(i)
		dig2 += ((parseFloat(ind)) *  (parseFloat(M)))
	}
		
	dig1 = (dig1 * 10) % 11
	dig2 = (dig2 * 10) % 11
	
	if (dig1 == 10)
		dig1 = 0
		
	if (dig2 == 10) 
		dig2 = 0

	if (dig1 != (parseFloat(valor.charAt(12))))
		return false
	
	if (dig2 != (parseFloat(valor.charAt(13))))
		return false
		
	return true;
}
//-->
</script>