<%'casas_decimais=numero de casas decimais que dever�o ser exibidas
function formata_moeda(valor,casas_decimais)
on error resume next
	valor_new		= CCur(valor) 
	parte_inteira	= int(valor_new)
	cont = 1
	for i = 1 to len(cstr(parte_inteira))
		aux = mid( parte_inteira,len(parte_inteira)+1-i,1)
		aux2 =  aux & aux2 
		if (cont mod 3 ) = 0 then
			aux2 = "." & aux2
		end if
		cont = cont + 1
	next
	if mid(aux2,1,1) = "." then 
		num = right(aux2,len(aux2)-1)
	else
		num = aux2
	end if
	parte_decimal	= cstr(valor_new - parte_inteira)
	if parte_decimal=0 then
		formata_moeda	= num & "," & string(casas_decimais,"0")
	else
		parte_decimal	= right(cstr(parte_decimal),len(valor_new - parte_inteira)-2)
		formata_moeda	= num & "," & parte_decimal & string(casas_decimais-len(parte_decimal),"0")
	end if
if err.number<>0 then
	formata_moeda=valor
	on error goto 0
end if
end function
%>