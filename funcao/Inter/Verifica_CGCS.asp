<%
function verifica_cgc(valor)
	if ((not isnumeric( valor)) or (len(valor) <> 14)) then
		verifica_cgc= false
		exit function
	end if
	
	Mult1 = "543298765432"
	Mult2 = "6543298765432"
	dig1=0
	dig2=0
	
	for i = 0 to 11	
		ind = mid(valor,i+1,1)
		M = mid(mult1,i+1,1)
		dig1 = dig1 + (cdbl(ind) * cdbl(M))
	next
	
	for i = 0 to 12
		ind = mid(valor,i+1,1)
		M = mid(mult2,i+1,1)
		dig2 = dig2 + (cdbl(ind) * cdbl(m))
	next
		
	dig1 = (dig1 * 10) mod 11
	dig2 = (dig2 * 10) mod 11
	
	if dig1 = 10 then dig1 = 0
		
	if dig2 = 10 then dig2 = 0

	if (dig1 <> (cdbl( mid(valor,13,1)))) then
		verifica_cgc= false
		exit function
	end if
	
	if (dig2 <> (cdbl(mid(valor,14,1)))) then
		verifica_cgc= false
		exit function
	end if
		
	verifica_cgc= true
end function
%>