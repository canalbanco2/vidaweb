<SCRIPT LANGUAGE=VBSCRIPT RUNAT=SERVER>
function ShowChart(ByRef aValues, ByRef aValues2, ByRef aLabels, ByRef strTitle, ByRef strXAxisLabel, ByRef strYAxisLabel)
	if isEmpty(aValues) or isEmpty(aLabels) then exit function
    ' Some user changable graph defining constants
	' All units are in screen pixels
	Const GRAPH_WIDTH  = 100  ' The width of the body of the graph
	Const GRAPH_HEIGHT = 50  ' The heigth of the body of the graph
	Const GRAPH_BORDER = 0    ' The size of the black border
	Const GRAPH_SPACER = 1    ' The size of the space between the bars

    const PATH_IMG     = "\_IMG\VERSAO3\"
	' Debugging constant so I can eaasily switch on borders in case
	' the tables get messed up.  Should be left at zero unless you're
	' trying to figure out which table cells doing what.
	Const TABLE_BORDER = 0
	'Const TABLE_BORDER = 10

	' Declare our variables
	Dim I
	Dim iMaxValue
	Dim iBarWidth
	Dim iBarHeight
	Dim iBarHeight2

	' Get the maximum value in the data set
	iMaxValue = 0
	For I = 0 To UBound(aValues)
		If cdbl(iMaxValue) < cdbl(aValues(I)) Then iMaxValue = aValues(I)
		If cdbl(iMaxValue) < cdbl(aValues2(I)) Then iMaxValue = aValues2(I)
	Next 'I

	' Calculate the width of the bars
	' Take the overall width and divide by number of items and round down.
	' I then reduce it by the size of the spacer so the end result
	' should be GRAPH_WIDTH or less!
	iBarWidth = (GRAPH_WIDTH \ (UBound(aValues) + 1)) - GRAPH_SPACER

	' Start drawing the graph
	' BGCOLOR=#E2E2E5
	
	ano = ano
	ano_a = ano-1 
	cont = 1
    
	STR = "<center><TABLE BORDER=" & TABLE_BORDER & " CELLSPACING=0 CELLPADDING=0 BGCOLOR=white>"
	
	STR = STR & "<TR>"
	legenda = "&nbsp;&nbsp;-&nbsp;&nbsp;"
	
	STR = STR & "<TD COLSPAN=2 ALIGN=center nowrap><font class=numeros>" &  strTitle &  Legenda & "<IMG SRC=" & PATH_IMG & "spacer_red.gif BORDER=0 WIDTH=7 HEIGHT=" &  7 & ">" & ano & "<IMG SRC=" & PATH_IMG & "spacer_green.gif BORDER=0 WIDTH=7 HEIGHT=" &  7 & ">" & ano_a & "</font><p></TD>"
	
	STR = STR & "<TR>"
	STR = STR & "<TD COLSPAN=2 ALIGN=left><font class=numeros>" &  iMaxValue & "</font></TD>"
	STR = STR & "</TR>"
	
	STR = STR & "</TR>"
	STR = STR & "<TR>"
	
	STR = STR & "</TR>"
	STR = STR & "<TR>"
	STR = STR & "<TD VALIGN=top>"
	STR = STR & "<TABLE BORDER=" &  TABLE_BORDER & " CELLSPACING=0 CELLPADDING=0 >"
    STR = STR & "<TR>"
    STR = STR & "<TD ROWSPAN=2><IMG SRC=" & PATH_IMG & "spacer.gif BORDER=0 WIDTH=1 HEIGHT=" &  GRAPH_HEIGHT & "></TD>"
    STR = STR & "</TR>"
    STR = STR & "<TR>"
    STR = STR & "</TR>"
    STR = STR & "</TABLE>"
    STR = STR & "</TD>"
    STR = STR & "<TD>"
    
    STR = STR & "<TABLE BORDER=" &  TABLE_BORDER & " CELLSPACING=0 CELLPADDING=0 BGCOLOR=WHITE>"
    STR = STR & "<TR>"
    STR = STR & "<TD VALIGN=bottom><IMG SRC=" & PATH_IMG & "spacer_black.gif BORDER=0 WIDTH=" &  GRAPH_BORDER & " HEIGHT=" &  GRAPH_HEIGHT & "></TD>" 
	' We're now in the body of the chart.  Loop through the data showing the bars!
    For I = 0 To UBound(aValues)
	iBarHeight = Int((aValues(I) / iMaxValue) * GRAPH_HEIGHT)
        iBarHeight2 = Int((aValues2(I) / iMaxValue) * GRAPH_HEIGHT)
	' This is a hack since browsers ignore a 0 as an image dimension!
	If iBarHeight = 0 Then iBarHeight = 1
	If iBarHeight2 = 0 Then iBarHeight2 = 1
	STR = STR & "<TD VALIGN=bottom><IMG SRC=" & PATH_IMG & "spacer.gif BORDER=0 WIDTH=" &  GRAPH_SPACER & " HEIGHT=1></TD>"
    mes = aLabels(I)
    'Response.Write "mes : " & mes
    link = "/estrut/serv/graficos/vendas.asp?ano="&ano&"&mes="&mes
    
    STR = STR & "<TD VALIGN=bottom><IMG SRC=" & PATH_IMG  & "spacer_red.gif BORDER=0 WIDTH=" &  iBarWidth & " HEIGHT=" &  iBarHeight & " ALT=" &  aValues(I) & " ONCLICK=javascript:window.open('"&link&"','','left=100,top=180,resizable=1,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=auto,width=600,height=280')"&" style='CURSOR: hand' ></TD>"	
	STR = STR & "<TD VALIGN=bottom><IMG SRC=" & PATH_IMG & "spacer_green.gif BORDER=0 WIDTH=" &  iBarWidth & " HEIGHT=" & iBarHeight2 & " ALT=" &  aValues2(I) & " ONCLICK=javascript:window.open('"&link&"','','left=100,top=180,resizable=1,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=auto,width=600,height=280')"&" style='CURSOR: hand'></TD>"
    Next 'I
    STR = STR & "</TR>"
    STR = STR & "<TR>"
    STR = STR & "<TD COLSPAN=" &  (4 * (UBound(aValues) + 1)) + 1 & "><IMG SRC=" & PATH_IMG & "spacer_black.gif BORDER=0 WIDTH=" &  GRAPH_BORDER + ((UBound(aValues) + 3 + UBound(aValues2)) * (iBarWidth + GRAPH_SPACER)) & " HEIGHT=" &  GRAPH_BORDER & "></TD>"
    STR = STR & "</TR>"
   ' The label array is optional and is really only useful for small data sets with very short labels! & "
    If IsArray(aLabels) Then 
	STR = STR & "<TR><TD></TD>"
	For I = 0 To UBound(aValues)  
	   STR = STR & "<TD></TD>"
	   STR = STR & "<TD ALIGN=center colspan=2><FONT SIZE=1>" &  aLabels(I) & "</FONT></TD>"
	Next 'I & "
	STR = STR & "</TR>"
    End If 
    STR = STR & "</TABLE>"
    STR = STR & "</TD>"
    STR = STR & "</TR>"
    STR = STR & "<TR>"
    STR = STR & "<TD></TD>"
    STR = STR & "<TD ALIGN=center valign=top><BR><B><font class=legenda>" & strYAxisLabel & " x " &                  strXAxisLabel & "</font></B></TD>"
    STR = STR & "</TR>"
    STR = STR & "</TABLE></center>"
    ShowChart = str
End function
</SCRIPT>