<script language=JScript>	

function mensagem_validacao(tipo,obrig,campo,valor)
{	
function isEmpty(str) 
{ 
	for (var intLoop = 0; intLoop < str.length; intLoop++)
		if (" " != str.charAt(intLoop))
			return false;            
	return true; 
}

//Criado por Alessandro Ricardo Lima em 16/03/1999
function verifica_cgc(valor)
{
	if ((isNaN(valor)) && (valor.length != 14))
		return false
		
	Mult1 = "543298765432"
	Mult2 = "6543298765432"
	dig1=0
	dig2=0
		
	for(var i=0;i<=11;i++)   
	{
		ind=valor.charAt(i)
		M=Mult1.charAt(i)
		dig1 += ((parseFloat(ind)) *  (parseFloat(M)))
	}
	
	for( var i=0;i<=12;i++)   
	{
		ind=valor.charAt(i)
		M=Mult2.charAt(i)
		dig2 += ((parseFloat(ind)) *  (parseFloat(M)))
	}
		
	dig1 = (dig1 * 10) % 11
	dig2 = (dig2 * 10) % 11
	
	if (dig1 == 10)
		dig1 = 0
		
	if (dig2 == 10) 
		dig2 = 0

	if (dig1 != (parseFloat(valor.charAt(12))))
		return false
	
	if (dig2 != (parseFloat(valor.charAt(13))))
		return false
		
	return true
}


function verifica_cpf(valor) 
{
	if ((isNaN(valor)) && (valor.length != 11))
      return false
	
	Mult1 = 10   
	Mult2 = 11
	dig1=0
	dig2=0
	
	valor= valor.toString()
	for(var i=0;i<=8;i++)
	{
	    ind=valor.charAt(i)
		dig1 += ((parseFloat(ind))* Mult1)
		Mult1--
	}
	alert ("dig1 -" + dig1)
	
	for(var i=0;i<=9;i++)
	{
	    ind=valor.charAt(i)
		dig2 += ((parseFloat(ind))* Mult2)
		Mult2--
	}
	alert ("dig2 - " + dig2)
	
	dig1 = (dig1 * 10) % 11   
	dig2 = (dig2 * 10) % 11   
	
	if (dig1 == 10)
      dig1 = 0
      
	if(dig2 == 10)
      dig2 = 0
	 
	if (parseFloat(valor.charAt(9)) != dig1)
		return false   
	if (parseFloat(valor.charAt(10)) != dig2)
		return false   
	return true  
}

function verifica_data(d,m,a)
 {
 if ( ( (d == '') || (m == '') || (a == '') )  && ( (d != '') && (m != '') && (a != '') ) ) return false; 
 if ( (isNaN(d) && d != '') || (isNaN(m) && m != '') || (isNaN(a) && a != '') ) return false
 if ( (m<1 || m >12) && (m != '') ) return false
 if ( (d<1 || d >31) && (d != '') ) return false
 if ( (a<1900 || a>2078) && (a !='') ) return false
 if (d == 31)
 if( (m == 2) || (m == 4) || (m == 6) || (m == 9) || (m == 11)) return false
 if (m ==2)
 if( (parseInt(a)%4 != 0 && d ==29) || (d == 30) )return false
 return true
 }

function f_num(cn,vn)
{
	if (isNaN(vn))
		return "O campo " + cn + " � num�rico."	
	return "000"
}

function f_vazio(cv,vv)
{	
	if (isEmpty(vv))
		 return "O campo " + cv + " � obrigat�rio."
	return "000"
}

function f_cgc(cvg,vcg)
{
	if (! verifica_cgc(vcg))	 
		return "O campo " + cvg + " possui CGC inv�lido."
	return 
}

function f_cpf(cvg,vcg)
{
	if (! verifica_cpf(vcg))	 
		return "O campo " + cvg + " possui CPF inv�lido."
	return
}

function f_data(cvg,vcg)
{
	if (! verifica_data(vcg))	 
		return "O campo " + cvg + " � inv�lido."
	return
}

function f_tipo(tp,cp,vl)
{
	if (tp == "cgc") 
		f_cgc(cp,vl)
	if (tp == "cpf")
		f_cpf(cp,vl)
	if (tp == "num")  
		f_num(cp,vl)
	if (tp == "data")  
		f_data(cp,vl)		
}

if (obrig)
{	
	m_valid = f_vazio(campo,valor)
	if (m_valid != "000") 
		return m_valid
	f_tipo(tipo,campo,valor)
}
else
	f_tipo(tipo,campo,valor)
}

</script>