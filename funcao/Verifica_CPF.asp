<script language=JScript>
function verifica_cpf(valor) 
{
    if (isEmpty(valor))
      return true

	if ((isNaN(valor)) && (valor.length != 11))
      return false
	
	Mult1 = 10   
	Mult2 = 11
	dig1 = 0
	dig2 = 0
	
	valor = valor.toString()
	for(var i=0;i<=8;i++)
	{
	    ind = valor.charAt(i)
		dig1 += ((parseFloat(ind))* Mult1)
		Mult1--
	}
	
	for(var i=0;i<=9;i++)
	{
	    ind = valor.charAt(i)
		dig2 += ((parseFloat(ind))* Mult2)
		Mult2--
	}

	dig1 = (dig1 * 10) % 11   
	dig2 = (dig2 * 10) % 11   
	
	if (dig1 == 10)
      dig1 = 0
      
	if(dig2 == 10)
      dig2 = 0
	 
	if (parseFloat(valor.charAt(9)) != dig1)
		return false   
	if (parseFloat(valor.charAt(10)) != dig2)
		return false   
	
	//Verificar a digita��o de CPF com todos os d�gitos iguais
	igual = new Array();
	for (var i=0;i<=10;i++)
	{
		if (i == 0) {
			anterior = valor.substring(i,i+1);
		}
		else {
			if (anterior == valor.substring(i,i+1)) {
				igual[i] = "sim";
			}
			anterior = valor.substring(i,i+1);
		}
	}
	var cont = 1;
	for (var i=0;i<=10;i++)
	{
		if (igual[i] == "sim") {
			cont++;
		}
	}
	if (cont == 11)
		return false
	
	return true
}
</script>