<%
function inverter_data(data)
 if trim(data) <> "" then
  if IsDate("20/Fev/99") then
   dia_data = DatePart("d", data)
   mes_data = DatePart("m", data) 
  else
   mes_data = mid(data,1,2)
   dia_data = mid(data,4,2)
  end if   
  ano_data = DatePart("yyyy" , data)
  if len(dia_data) = 1 then
   dia_data = 0&dia_data
  end if 
  if len(mes_data) = 1 then
   mes_data = 0&mes_data
  end if 
  data_invertida = ano_data & mes_data &  dia_data 
  inverter_data = data_invertida
 else
  inverter_data = ""
 end if
end function %>