<script language=JScript>
function Verifica_Hora(h,m)
{
 if  ( ( (isEmpty(h)) || (isEmpty(m)) ) && ( (!isEmpty(h)) && (!isEmpty(m)) ) ) return false; 
 if ( (isNaN(h) && (!isEmpty(h)) ) || (isNaN(m) && (!isEmpty(m)) ) ) return false
 if ( (h<0 || h >24) && (!isEmpty(h)) ) return false
 if ( (m<0 || m >59) && (!isEmpty(m)) ) return false
 if ( (h==24) && (m !=0) ) return false

 return true
}
</script>
 