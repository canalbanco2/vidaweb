<SCRIPT LANGUAGE=VBSCRIPT RUNAT=SERVER>
function ShowChart(ByRef Mes, ByRef aValues, ByRef aValues2, ByRef aValues3, ByRef aValues4, ByRef aValues5, ByRef aValues6, ByRef aLabels, ByRef strTitle, ByRef strXAxisLabel, ByRef strYAxisLabel)
	if isEmpty(aValues) or isEmpty(aLabels) then exit function
    'Some user changable graph defining constants
	'All units are in screen pixels
	Const GRAPH_WIDTH  = 300  'The width of the body of the graph (100)
	Const GRAPH_HEIGHT = 300  'The heigth of the body of the graph (50)
	Const GRAPH_BORDER = 0    'The size of the black border
	Const GRAPH_SPACER = 1    'The size of the space between the bars

    const PATH_IMG     = "\_IMG\VERSAO4\"
	'Debugging constant so I can eaasily switch on borders in case
	'the tables get messed up.  Should be left at zero unless you're
	'trying to figure out which table cells doing what.
	Const TABLE_BORDER = 0

	'Declare our variables
	Dim I
	Dim iMaxValue
	Dim iBarWidth
	Dim iBarHeight
	Dim iBarHeight2
	Dim iBarHeight3
	Dim iBarHeight4
	Dim iBarHeight5
	Dim iBarHeight6

	'Get the maximum value in the data set
	iMaxValue = 0
	For I = 0 To UBound(aValues)
		If cdbl(iMaxValue) < cdbl(aValues(I)) Then iMaxValue = aValues(I)
		If cdbl(iMaxValue) < cdbl(aValues2(I)) Then iMaxValue = aValues2(I)
		If cdbl(iMaxValue) < cdbl(aValues3(I)) Then iMaxValue = aValues3(I)
		If cdbl(iMaxValue) < cdbl(aValues4(I)) Then iMaxValue = aValues4(I)
		If cdbl(iMaxValue) < cdbl(aValues5(I)) Then iMaxValue = aValues5(I)
		If cdbl(iMaxValue) < cdbl(aValues6(I)) Then iMaxValue = aValues6(I)
	Next 'I

	'Calculate the width of the bars
	'Take the overall width and divide by number of items and round down.
	'I then reduce it by the size of the spacer so the end result
	'should be GRAPH_WIDTH or less!
	iBarWidth = (GRAPH_WIDTH \ (UBound(aValues) + 1)) - GRAPH_SPACER

	'Start drawing the graph
	'BGCOLOR=#E2E2E5
	
	leg1 = "Info.Ag�ncia&nbsp;&nbsp;"
	leg2 = "Info.Segurado&nbsp;&nbsp;"
	leg3 = "Sin.Ag�ncia&nbsp;&nbsp;"
	leg4 = "Sin.Segurado&nbsp;&nbsp;"
	leg5 = "Proj.Fenabb&nbsp;&nbsp;"
	leg6 = "Venda Ourovida"
	cont = 1
	'legenda = "&nbsp;&nbsp;-&nbsp;&nbsp;"

	STR = "<center><TABLE BORDER=" & TABLE_BORDER & " CELLSPACING=0 CELLPADDING=0 BGCOLOR=white>"
	STR = STR & "<TR>"
	STR = STR & "<TD WIDTH=33% ALIGN=center nowrap><font class=numeros>" &  strTitle & "</font><BR><font class=numeros><IMG SRC=" & PATH_IMG & "spacer_red.gif BORDER=0 WIDTH=7 HEIGHT=7>" & leg1 & "<IMG SRC=" & PATH_IMG & "spacer_green.gif BORDER=0 WIDTH=7 HEIGHT=7>" & leg2
	STR = STR & "<font class=numeros><IMG SRC=" & PATH_IMG & "spacer_blue.gif BORDER=0 WIDTH=7 HEIGHT=7>" & leg3 & "<IMG SRC=" & PATH_IMG & "spacer_roxo.gif BORDER=0 WIDTH=7 HEIGHT=7>" & leg4
	STR = STR & "<font class=numeros><IMG SRC=" & PATH_IMG & "spacer_gray.gif BORDER=0 WIDTH=7 HEIGHT=7>" & leg5 & "<IMG SRC=" & PATH_IMG & "spacer_yellow.gif BORDER=0 WIDTH=7 HEIGHT=7>" & leg6 & "</font><p></TD>"
	STR = STR & "<TD WIDTH=33% ALIGN=center nowrap><font class=numeros>" &  strTitle & "</font><BR><font class=numeros><IMG SRC=" & PATH_IMG & "spacer_red.gif BORDER=0 WIDTH=7 HEIGHT=7>" & leg1 & "<IMG SRC=" & PATH_IMG & "spacer_green.gif BORDER=0 WIDTH=7 HEIGHT=7>" & leg2
	STR = STR & "<font class=numeros><IMG SRC=" & PATH_IMG & "spacer_blue.gif BORDER=0 WIDTH=7 HEIGHT=7>" & leg3 & "<IMG SRC=" & PATH_IMG & "spacer_roxo.gif BORDER=0 WIDTH=7 HEIGHT=7>" & leg4
	STR = STR & "<font class=numeros><IMG SRC=" & PATH_IMG & "spacer_gray.gif BORDER=0 WIDTH=7 HEIGHT=7>" & leg5 & "<IMG SRC=" & PATH_IMG & "spacer_yellow.gif BORDER=0 WIDTH=7 HEIGHT=7>" & leg6 & "</font><p></TD>"
	STR = STR & "<TD WIDTH=33% ALIGN=center nowrap><font class=numeros>" &  strTitle & "</font><BR><font class=numeros><IMG SRC=" & PATH_IMG & "spacer_red.gif BORDER=0 WIDTH=7 HEIGHT=7>" & leg1 & "<IMG SRC=" & PATH_IMG & "spacer_green.gif BORDER=0 WIDTH=7 HEIGHT=7>" & leg2
	STR = STR & "<font class=numeros><IMG SRC=" & PATH_IMG & "spacer_blue.gif BORDER=0 WIDTH=7 HEIGHT=7>" & leg3 & "<IMG SRC=" & PATH_IMG & "spacer_roxo.gif BORDER=0 WIDTH=7 HEIGHT=7>" & leg4
	STR = STR & "<font class=numeros><IMG SRC=" & PATH_IMG & "spacer_gray.gif BORDER=0 WIDTH=7 HEIGHT=7>" & leg5 & "<IMG SRC=" & PATH_IMG & "spacer_yellow.gif BORDER=0 WIDTH=7 HEIGHT=7>" & leg6 & "</font><p></TD>"
	STR = STR & "<TR>"
	STR = STR & "<TD COLSPAN=3 ALIGN=left><font class=numeros>" &  iMaxValue & "</font></TD>"
	STR = STR & "</TR>"
	STR = STR & "<TR>"
	STR = STR & "<TD COLSPAN=3 VALIGN=top>"

    STR = STR & "<TABLE BORDER=" &  TABLE_BORDER & " CELLSPACING=0 CELLPADDING=0 BGCOLOR=WHITE>"
    STR = STR & "<TR>"
    STR = STR & "<TD VALIGN=bottom><IMG SRC=" & PATH_IMG & "spacer_black.gif BORDER=0 WIDTH=" &  GRAPH_BORDER & " HEIGHT=" &  GRAPH_HEIGHT & "></TD>" 
	'We're now in the body of the chart.  Loop through the data showing the bars!
    For I = 0 To UBound(aValues)
	   iBarHeight = Int((aValues(I) / iMaxValue) * GRAPH_HEIGHT)
       iBarHeight2 = Int((aValues2(I) / iMaxValue) * GRAPH_HEIGHT)
       iBarHeight3 = Int((aValues3(I) / iMaxValue) * GRAPH_HEIGHT)
       iBarHeight4 = Int((aValues4(I) / iMaxValue) * GRAPH_HEIGHT)
       iBarHeight5 = Int((aValues5(I) / iMaxValue) * GRAPH_HEIGHT)
       iBarHeight6 = Int((aValues6(I) / iMaxValue) * GRAPH_HEIGHT)
	   'This is a hack since browsers ignore a 0 as an image dimension!
	   If iBarHeight = 0 Then iBarHeight = 1
	   If iBarHeight2 = 0 Then iBarHeight2 = 1
	   If iBarHeight3 = 0 Then iBarHeight3 = 1
	   If iBarHeight4 = 0 Then iBarHeight4 = 1
	   If iBarHeight5 = 0 Then iBarHeight5 = 1
	   If iBarHeight6 = 0 Then iBarHeight6 = 1
	   STR = STR & "<TD VALIGN=bottom><IMG SRC=" & PATH_IMG & "spacer.gif BORDER=0 WIDTH=" &  GRAPH_SPACER & " HEIGHT=1></TD>"
       STR = STR & "<TD VALIGN=bottom><IMG SRC=" & PATH_IMG  & "spacer_red.gif BORDER=0 WIDTH=" &  iBarWidth & " HEIGHT=" &  iBarHeight & " ALT=" &  aValues(I) & " style='CURSOR: hand'></TD>"
       STR = STR & "<TD VALIGN=bottom><IMG SRC=" & PATH_IMG & "spacer_green.gif BORDER=0 WIDTH=" &  iBarWidth & " HEIGHT=" & iBarHeight2 & " ALT=" &  aValues2(I) & " style='CURSOR: hand'></TD>"
       STR = STR & "<TD VALIGN=bottom><IMG SRC=" & PATH_IMG & "spacer_blue.gif BORDER=0 WIDTH=" &  iBarWidth & " HEIGHT=" & iBarHeight3 & " ALT=" &  aValues3(I) & " style='CURSOR: hand'></TD>"
       STR = STR & "<TD VALIGN=bottom><IMG SRC=" & PATH_IMG & "spacer_roxo.gif BORDER=0 WIDTH=" &  iBarWidth & " HEIGHT=" & iBarHeight4 & " ALT=" &  aValues4(I) & " style='CURSOR: hand'></TD>"
       STR = STR & "<TD VALIGN=bottom><IMG SRC=" & PATH_IMG & "spacer_gray.gif BORDER=0 WIDTH=" &  iBarWidth & " HEIGHT=" & iBarHeight5 & " ALT=" &  aValues5(I) & " style='CURSOR: hand'></TD>"
       STR = STR & "<TD VALIGN=bottom><IMG SRC=" & PATH_IMG & "spacer_yellow.gif BORDER=0 WIDTH=" &  iBarWidth & " HEIGHT=" & iBarHeight6 & " ALT=" &  aValues6(I) & " style='CURSOR: hand'></TD>"
    Next 'I
    STR = STR & "</TR>"
    STR = STR & "<TR>"
    STR = STR & "<TD COLSPAN=" & (7 * (UBound(aValues) + 1)) + 1 & "><IMG SRC=" & PATH_IMG & "spacer_black.gif BORDER=0 WIDTH=" &  GRAPH_BORDER + ((UBound(aValues) + 3 + UBound(aValues2) + 3 + UBound(aValues3) + 3 + UBound(aValues4) + 3 + UBound(aValues5) + 3 + UBound(aValues6)) * (iBarWidth + GRAPH_SPACER)) & " HEIGHT=" &  GRAPH_BORDER & "></TD>"
    STR = STR & "</TR>"
    'The label array is optional and is really only useful for small data sets with very short labels!
    If IsArray(aLabels) Then 
	   STR = STR & "<TR><TD></TD>"
	   For I = 0 To UBound(aValues)
	      STR = STR & "<TD></TD>"
	      STR = STR & "<TD ALIGN=center colspan=6><FONT SIZE=1>" & aLabels(I) & "</FONT></TD>"
	   Next 'I
	   STR = STR & "</TR>"
    End If 
    STR = STR & "</TABLE>"

    STR = STR & "</TD>"
    STR = STR & "</TR>"
    STR = STR & "<TR>"
    If Mes = "01" Then
       Mes = "Janeiro"
    ElseIf Mes = "02" Then
       Mes = "Fevereiro"
    ElseIf Mes = "03" Then
       Mes = "Mar�o"
    ElseIf Mes = "04" Then
       Mes = "Abril"
    ElseIf Mes = "05" Then
       Mes = "Maio"
    ElseIf Mes = "06" Then
       Mes = "Junho"
    ElseIf Mes = "07" Then
       Mes = "Julho"
    ElseIf Mes = "08" Then
       Mes = "Agosto"
    ElseIf Mes = "09" Then
       Mes = "Setembro"
    ElseIf Mes = "10" Then
       Mes = "Outubro"
    ElseIf Mes = "11" Then
       Mes = "Novembro"
    ElseIf Mes = "12" Then
       Mes = "Dezembro"
    End If
    STR = STR & "<TD WIDTH=33% ALIGN=center valign=top><BR><B><font class=legenda>" & strYAxisLabel & "&nbsp;x&nbsp;" & strXAxisLabel & "<br>( " & Mes & " )</font></B></TD>"
    STR = STR & "<TD WIDTH=33% ALIGN=center valign=top><BR><B><font class=legenda>" & strYAxisLabel & "&nbsp;x&nbsp;" & strXAxisLabel & "<br>( " & Mes & " )</font></B></TD>"
    STR = STR & "<TD WIDTH=33% ALIGN=center valign=top><BR><B><font class=legenda>" & strYAxisLabel & "&nbsp;x&nbsp;" & strXAxisLabel & "<br>( " & Mes & " )</font></B></TD>"
    STR = STR & "</TR>"
    STR = STR & "</TABLE></center>"
    ShowChart = str
End function
</SCRIPT>