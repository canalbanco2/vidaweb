<%
FUNCTION ULCase(texto)
	Dim textoFinal
	Dim vetorPrep(20)
	Dim vetorExc(10)
	Dim vetorMaius(40)

	vetorPrep(1) = "DA"
	vetorPrep(2) = "DAS"
	vetorPrep(3) = "DE"
	vetorPrep(4) = "DES"
	vetorPrep(5) = "DI"
	vetorPrep(6) = "DIS"
	vetorPrep(7) = "DO"
	vetorPrep(8) = "DOS"
	vetorPrep(9) = "DU"
	vetorPrep(10) = "DUS"
	vetorPrep(11) = "E"
	vetorPrep(12) = "PARA"
	vetorPrep(13) = "COM"
	vetorPrep(14) = "OS"
	vetorPrep(15) = "�"
	vetorPrep(16) = "AO"
	vetorPrep(17) = "A"
	vetorPrep(18) = "O"

	vetorExc(1) = "/"
	vetorExc(2) = "-"
	vetorExc(3) = "."
	vetorExc(4) = "\"
	vetorExc(5) = "#"

	vetorMaius(1) =  "VP"
	vetorMaius(2) =  "BB"
	vetorMaius(3) =  "AC" 
	vetorMaius(4) =  "AL" 
	vetorMaius(5) =  "AM" 
	vetorMaius(6) =  "AP" 
	vetorMaius(7) =  "BA" 
	vetorMaius(8) =  "CE" 
	vetorMaius(9) =  "DF" 
	vetorMaius(10) = "ES" 
	vetorMaius(11) = "FN" 
	vetorMaius(12) = "GO" 
	vetorMaius(13) = "MA" 
	vetorMaius(14) = "MG" 
	vetorMaius(15) = "MS" 
	vetorMaius(16) = "MT" 
	vetorMaius(17) = "PA" 
	vetorMaius(18) = "PB" 
	vetorMaius(19) = "PE" 
	vetorMaius(20) = "PI" 
	vetorMaius(21) = "PR" 
	vetorMaius(22) = "RJ" 
	vetorMaius(23) = "RN" 
	vetorMaius(24) = "RO" 
	vetorMaius(25) = "RR" 
	vetorMaius(26) = "RS" 
	vetorMaius(27) = "SC" 
	vetorMaius(28) = "SE" 
	vetorMaius(29) = "SP"
	vetorMaius(30) = "TO"
	vetorMaius(31) = "SISSEG"
	vetorMaius(32) = "SISE"
    vetorMaius(33) = "PRI"
	vetorMaius(34) = "CAP"
	vetorMaius(35) = "DCA"
	vetorMaius(36) = "DDE"
	vetorMaius(37) = "CA"
	vetorMaius(38) = "PRESI"
	textoFinal = ""
	Texto2 = texto & " "
	tamanho = Len(texto2)
	posEspaco = 0
	ponteiro = 1
	posEspaco = InStr(texto2, " ")
	numPalavras = 0
	DO WHILE (ponteiro - 1) < tamanho
		palavra = Mid (texto2, ponteiro , ((posEspaco ) - ponteiro) )
		IF palavra <> "" then
			palavra = Trim(Ucase(String(1, palavra)) + LCase(Mid(palavra, 2)) )
			numPalavras = numPalavras + 1
			temPonto = 0
			i = 1
			tamPalavra = len(palavra)
			contPalavra = 1
			i =1
			DO WHILE Not IsEmpty(vetorExc(i))
				posPonto = InStr(palavra, vetorExc(i) )
				IF posPonto <> 0 then
					EXIT DO
				ELSE
					i = i + 1
				END IF
			LOOP
			IF posPonto <> 0 then
				temPonto = 1
				posLetra = posPonto + 1
				letra = Ucase(Mid (palavra, posLetra, 1) )
				IF letra <> "" then
					palavra2 = Ucase(String(1, palavra)) + Mid(palavra, 2, posPonto - 1) + letra + LCase(Mid(palavra, posLetra + 1))
					palavra = palavra2
					DO WHILE posPonto <> 0
						inicio = posPonto + 1
						i =1
						DO WHILE Not IsEmpty(vetorExc(i))
							posPonto = InStr(inicio, palavra, vetorExc(i) )
							IF posPonto <> 0 then
								exit do
							ELSE
								i = i + 1
							END IF
						LOOP
						IF posPonto <> 0 then
							posLetra = posPonto + 1
							letra = Ucase(Mid (palavra, posLetra, 1) )
							IF letra = "" then
								EXIT DO
							ELSE
								palavra2 = Mid(palavra, 1, posPonto) + letra + LCase(Mid(palavra, posLetra + 1))
								palavra = palavra2
							END IF
						END IF
					LOOP
				END IF
				textoFinal = textoFinal + " " + palavra
			ELSE
				cont = 1
				ePrep = 0
				DO WHILE Not IsEmpty(vetorPrep(cont))
					IF StrComp(Ucase(palavra), vetorPrep(cont)) = 0 then
						ePrep = 1
						if numPalavras <> 1 then
							textoFinal = textoFinal + " " + LCase(palavra)
						else
							textoFinal = textoFinal + " " + palavra
						end if
						EXIT DO
					ELSE
					END IF
					cont = cont + 1
				LOOP
				if ePrep = 0 then
					cont = 1
					DO WHILE Not IsEmpty(vetorMaius(cont))
						IF StrComp(Ucase(palavra), vetorMaius(cont)) = 0 then
							ePrep = 1
							textoFinal = textoFinal + " " + UCase(palavra)
							EXIT DO
						ELSE
						END IF
						cont = cont + 1
					LOOP
				end if
				IF ePrep = 0 then
					textoFinal = textoFinal + " " + UCase(String(1, palavra)) + LCase(Mid(palavra, 2))
				END IF
			END IF
		END IF
		ponteiro = posEspaco + 1
		posEspaco = InStr(ponteiro , texto2, " ")
	LOOP
	ULCase = Trim(textoFinal)
END FUNCTION
%>