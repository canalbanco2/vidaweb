<%
Server.ScriptTimeout = 5400

Const ForWriting = 2
Const TristateTrue = -1
CrLf = Chr(13) & Chr(10)
Function GetFieldName(infoStr)
	sPos = InStr(infoStr, "name=")
	EndPos = InStr(sPos + 6, infoStr, Chr(34) & ";")
	If EndPos = 0 Then
		EndPos = inStr(sPos + 6, infoStr, Chr(34))
	End If
	GetFieldName = Mid(infoStr, sPos + 6, endPos - _
		(sPos + 6))
End Function
Function GetFileName(infoStr)
	sPos = InStr(infoStr, "filename=")
	EndPos = InStr(infoStr, Chr(34) & CrLf)
	GetFileName = Mid(infoStr, sPos + 10, EndPos - _
		(sPos + 10))
End Function
Function GetFileType(infoStr)
	sPos = InStr(infoStr, "Content-Type: ")
	GetFileType = Mid(infoStr, sPos + 14)
End Function

PostData = ""
Dim biData
biData = Request.BinaryRead(Request.TotalBytes)
For nIndex = 1 to LenB(biData)
	PostData = PostData & Chr(AscB(MidB(biData,nIndex,1)))
Next
ContentType = Request.ServerVariables("HTTP_CONTENT_TYPE")
ctArray = Split(ContentType, ";")
dim FileCount
If Trim(ctArray(0)) = "multipart/form-data" Then
	ErrMsg = ""
	bArray = Split(Trim(ctArray(1)), "=")
	Boundary = Trim(bArray(1))
	FormData = Split(PostData, Boundary)
	redim  myRequestFiles(4, 0)
	Set myRequest = CreateObject("Scripting.Dictionary")
	redim tipo(0)
	FileCount = 0
	For x = 0 to UBound(FormData)
		InfoEnd = InStr(FormData(x), CrLf & CrLf)
		If InfoEnd > 0 Then
			varInfo = Mid(FormData(x), 3, InfoEnd - 3)
			varValue = Mid(FormData(x), InfoEnd + 4,Len(FormData(x)) - InfoEnd - 7)
			redim preserve tipo(x)
			redim preserve anome(x)
			If (InStr(varInfo, "filename=") > 0) Then
				tipo(x) = "file"
				anome(x) = GetFieldName(varInfo)
				redim preserve myRequestFiles(4,FileCount)
				myRequestFiles(0, FileCount) = GetFieldName(varInfo)
				myRequestFiles(1, FileCount) = varValue
				myRequestFiles(2, FileCount) = GetFileName(varInfo)
				myRequestFiles(3, FileCount) = GetFileType(varInfo)
				myRequestFiles(4, FileCount) = x
				myRequest.add GetFieldName(varInfo), Mid(GetFileName(varInfo), InstrRev(GetFileName(varInfo), "\") + 1)
				FileCount = FileCount + 1
			Else
				tipo(x) = "input"
				anome(x) = GetFieldName(varInfo)
				myRequest.add GetFieldName(varInfo), varValue
			End If
		End If
	Next
Else
	response.write("� necess�rio uilizar ""multipart/form-data"" na encripta��o do formul�rio.")
	response.end
End If

function retornaIndice(valor)
	retornaIndice = ""
	for f=0 to ubound(anome)
		if anome(f) = valor then
			retornaIndice = f
			exit function
		end if		
	next
end function

function saveFile(nome,path,indice)
	if right(path,1) <> "\" and right(path,1) <> "/" then
		path = path & "\"
	end if
	saveFile = ""
	'on error resume next
	Set lf = server.createObject("Scripting.FileSystemObject")
	SavePath = path & nome
	if not lf.FolderExists(path) then
		saveFile = "Caminho especificado n�o existe ou n�o encontrado."
	else
		if ubound(myRequestFiles,2) < indice then
			saveFile = "Indice n�o encontrado para o arquivo requerido."
		else		
			if trim(myRequestFiles(2, indice)) <> "" then
				Set SaveFilef = lf.CreateTextFile(SavePath, True)
				SaveFilef.Write(myRequestFiles(1, indice))
				SaveFilef.Close
				set SaveFilef = nothing
			else
				saveFile = "Arquivo n�o encontrado."
			end if
		end if
	end if
	if err.number <> 0 then
		saveFile = "Erro na tentativa de gravar o arquivo.\n" & err.description
	end if
	set lf = nothing
	'on error goto 0
end function

function saveAllFiles(path)
	if right(path,1) <> "\" and right(path,1) <> "/" then
		path = path & "\"
	end if
	saveAllFiles = ""
	on error resume next
	Set lf = server.createObject("Scripting.FileSystemObject")
	if not lf.FolderExists(path) then
		saveAllFiles = "Caminho especificado n�o existe ou n�o encontrado."
	else
		for indice = 0 to ubound(myRequestFiles,2)
			if trim(myRequestFiles(2, indice)) <> "" then
				nome = Mid(myRequestFiles(2,indice), InstrRev(myRequestFiles(2,indice), "\") + 1)
				SavePath = path & nome
				Set SaveFile = lf.CreateTextFile(SavePath, True)
				SaveFile.Write(myRequestFiles(1, indice))
				SaveFile.Close
				set SaveFile = nothing
			end if
		next
	end if
	if err.number<>0 then
		saveAllFiles = "Erro na tentativa de gravar o arquivo.\n" & err.description
	end if
	set lf = nothing
	on error goto 0
end function
' Array myRequestFiles(tipo,arquivo) cont�m todos os campo de File do formul�rio
' Dictionary myRequest cont�m os campos de formul�rio normais
%>