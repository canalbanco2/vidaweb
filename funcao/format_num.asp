<script language=VBScript runat=server>
function format_num(valor)
	if trim(valor) = "" then exit function
	valor = trim(valor)
	cont = 1
	for i = 1 to len(valor)
		aux = mid( valor,len(valor)+1-i,1)
		aux2 =  aux & aux2 
		if (cont mod 3 ) = 0 then
			aux2 = "." & aux2
		end if
		cont = cont + 1
	next
	if mid(aux2,1,1) = "." then 
		format_num = right(aux2,len(aux2)-1)
	else
		format_num = aux2
	end if
end function
</script>