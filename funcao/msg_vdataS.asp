<%
function f_data(cvg,vd,vm,vy)
	if (not Verifica_Data(vd,vm,vy)) then
		f_data = vbcrlf & "O campo '" & cvg & "' � inv�lido." 
	end if
end function

function msg_vdata(obrig,campo,d,m,y)

if (obrig) then
	m_valid = msg_vnull(campo,d)
	if (trim(m_valid)<>"") then
		msg_vdata= m_valid
		exit function
    end if
	m_valid = msg_vnull(campo,m)
	if (trim(m_valid) <> "") then
		msg_vdata = m_valid
		exit function
	end if
	m_valid = msg_vnull(campo,y)
	if (trim(m_valid) <> "") then
		msg_vdata= m_valid
		exit function
	end if
	msg_vdata= f_data(campo,d,m,y)
else
	msg_vdata = f_data(campo,d,m,y)
end if
end function
%>