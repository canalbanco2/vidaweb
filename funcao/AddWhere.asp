<SCRIPT LANGUAGE=VBScript RUNAT=SERVER>
function AddWhere(ValorCampo,NomeCampo,Condicao,Argumento,Aspas)
	condicao=trim(ucase(condicao))
	valorcampo = trim(valorcampo)
	if trim(valorcampo)= "#" then valorcampo = ""
	if trim(valorcampo)<>""  and trim(condicao)<>"" then
		if condicao="LIKE" THEN
			addwhere =nomecampo & " like '%" & valorcampo & "%'"
		elseif condicao = "=" or condicao = ">" or condicao = "<" or condicao = "<=" or condicao = "=<" or condicao = ">=" or condicao = "=>" then
				if not aspas then
					addwhere=nomecampo & " " & condicao & " " & valorcampo
				else
					if isdate(valorcampo) then valorcampo = f_add_data(valorcampo)
					addwhere=nomecampo & " " & condicao & " '" & valorcampo & "'"
				end if
		elseif condicao = "BETWEEN" then
				i    = instr(valorcampo,"#")
				var  = mid(valorcampo,1,i-1)
				var2 = mid(valorcampo,i+1,len(valorcampo))
				if not aspas then
					addwhere=nomecampo & " " & condicao & " " & var & " and " & var2
				else
					if isdate(var)and isdate(var2) then 
					    var  = f_add_data(var)
					    var2 = f_add_data(var2)
					end if
					addwhere=nomecampo & " " & condicao & " '" & var & "' and  '" & var2 & "'"
				end if
		end if
		select case argumento
			case 1: if trim(addwhere)<>"" then addwhere = " and " & addwhere
			case 2: if trim(addwhere)<>"" then addwhere = " or " & addwhere
		end select
	end if
end function
function f_add_data(data)
    ano_data = DatePart("yyyy" , data)
    'quando o formato de data for o brasileiro
    if IsDate("20/Fev/99") then
        dia_data = DatePart("d", data)
        mes_data = DatePart("m", data)
        data = dia_data & "/" & mes_data & "/" & ano_data 
    else
        mes_data = mid(data,1,2)
        dia_data = mid(data,4,2)
        data = mes_data & "/" & dia_data & "/" & ano_data
    end if   
    data=dateserial(year(data),month(data),day(data))
	if len(datepart("d",data)) = 1 then 
		dia = 0 & datepart("d",data)
	else
		dia =datepart("d",data)
	end if
	if len(datepart("m",data)) = 1 then 
		mes = 0 & datepart("m",data)
	else
		mes =datepart("m",data)
	end if
	f_add_data = datepart("yyyy",data) & mes & dia
end function				
</SCRIPT>