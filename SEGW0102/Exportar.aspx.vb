Imports System.IO
'Imports Microsoft.Office.Interop

Public Class Exportar
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If IsNothing(Session("ramo")) Then
            Response.Write("<script>alert('N�o � possivel visualizar o arquivo.'); window.close();</script>")
            Exit Sub
        End If

        Dim cAmbiente As Alianca.Seguranca.Web.ControleAmbiente
        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
        cAmbiente.ObterAmbiente(url)

        If Not IsPostBack Then
            Dim v_sAmbiente As String = cAmbiente.Ambiente.ToString()
            Dim v_sCaminho As String = cUtilitarios.getParametros("SEGBR", "ARQUIVO_WEB", "CAMINHO", v_sAmbiente)

            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

            bd.SEGS7599_SPS(Request("DocumentoVidaWebId"))

            Dim v_oDtDocumento As Data.DataTable = bd.ExecutaSQL_DT
            If v_oDtDocumento.Rows.Count > 0 Then
                GerarArquivo(v_oDtDocumento.Rows(0)("Arquivo"), v_sCaminho)
            End If
            'GerarArquivo("Word00.doc", "D:\\Confitec_Inetpub\\SEG\\POST\VIDA_WEB\\")
        End If
    End Sub

    Private Sub GerarArquivo(ByVal p_sArquivoDoc As String, ByVal p_sCaminho As String)
        Dim v_sDocumento As String = p_sCaminho & "\" & p_sArquivoDoc

        If Not File.Exists(v_sDocumento) Then
            Response.Write("<script>alert('" & v_sDocumento.ToString() & "Arquivo n�o encontrado.'); //window.close();</script>")
            Exit Sub
        Else
            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
            Dim v_oDtResultado As New Data.DataTable

            bd.SEGS7561_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

            v_oDtResultado = bd.ExecutaSQL_DT()

            If v_oDtResultado.Rows.Count > 0 Then
                Dim objWordApp As New Word.Application
                Dim objDoc As Word.Document
                Dim wdSaveFormat As Word.WdSaveFormat
                Dim v_sNovoNomeDocumento As String = "DOC_" & Session("apolice") & "_" & Session("ramo") & "_" & Session("subgrupo_id") & "_" & Now.Year().ToString & "-" & Now.Month.ToString & "-" & Now.Day.ToString & "  " & Now.Hour.ToString & " " & Now.Minute.ToString & " " & Now.Second.ToString & ".doc"

                'Show the Word application window if checked.
                objWordApp.Visible = False

                'Open an existing document.
                objDoc = objWordApp.Documents.Open(v_sDocumento.ToString(), True, True, False, , , , , , , , , True)

                objDoc.SaveAs(p_sCaminho & "\" & v_sNovoNomeDocumento)

                objDoc = objWordApp.Documents.Open(p_sCaminho & "\" & v_sNovoNomeDocumento, True, False, False, , , , , , , , , True)

                Dim myFind As Object = objDoc.Content.Find
                Dim missing As Object = System.Reflection.Missing.Value

                Dim findText As Object = ""
                Dim replaceText As Object = ""

                'Find and replace [APOLICE].
                findText = "[APOLICE]"
                replaceText = v_oDtResultado.Rows(0)("APOLICE")
                ExecuteReplaceCommand(myFind, findText, replaceText)

                'Find and replace [RAMO]
                findText = "[RAMO]"
                replaceText = v_oDtResultado.Rows(0)("RAMO")
                ExecuteReplaceCommand(myFind, findText, replaceText)

                'Find and replace [SUBGRUPO]
                findText = "[SUBGRUPO]"
                replaceText = v_oDtResultado.Rows(0)("SUBGRUPO")
                ExecuteReplaceCommand(myFind, findText, replaceText)

                'Find and replace [SUBESTIPULANTE]
                findText = "[SUBESTIPULANTE]"
                replaceText = v_oDtResultado.Rows(0)("SUBESTIPULANTE")
                ExecuteReplaceCommand(myFind, findText, replaceText)

                'Find and replace [NOMESUBGRUPO]
                findText = "[NOMESUBGRUPO]"
                replaceText = v_oDtResultado.Rows(0)("NOMESUBGRUPO")
                ExecuteReplaceCommand(myFind, findText, replaceText)

                'Find and replace [NOMESUBESTIPULANTE]
                findText = "[NOMESUBESTIPULANTE]"
                replaceText = v_oDtResultado.Rows(0)("NOMESUBESTIPULANTE")
                ExecuteReplaceCommand(myFind, findText, replaceText)

                'Find and replace [CNPJESTIPULANTE]
                findText = "[CNPJESTIPULANTE]"
                replaceText = v_oDtResultado.Rows(0)("CNPJESTIPULANTE")
                ExecuteReplaceCommand(myFind, findText, replaceText)

                'Find and replace [ENDERECOSUBGRUPO]
                findText = "[ENDERECOSUBGRUPO]"
                replaceText = v_oDtResultado.Rows(0)("ENDERECOSUBGRUPO")
                ExecuteReplaceCommand(myFind, findText, replaceText)

                'Find and replace [CNPJSUBGRUPO]
                findText = "[CNPJSUBGRUPO]"
                replaceText = v_oDtResultado.Rows(0)("CNPJSUBGRUPO")
                ExecuteReplaceCommand(myFind, findText, replaceText)

                'Find and replace [INICIOVIGENCIAAPOLICE]
                findText = "[INICIOVIGENCIAAPOLICE]"
                replaceText = v_oDtResultado.Rows(0)("INICIOVIGENCIAAPOLICE")
                ExecuteReplaceCommand(myFind, findText, replaceText)

                'Find and replace [FIMVIGENCIAAPOLICE]
                findText = "[FIMVIGENCIAAPOLICE]"
                replaceText = v_oDtResultado.Rows(0)("FIMVIGENCIAAPOLICE")
                ExecuteReplaceCommand(myFind, findText, replaceText)

                'Find and replace [AGENCIA]
                findText = "[AGENCIA]"
                replaceText = v_oDtResultado.Rows(0)("AGENCIA")
                ExecuteReplaceCommand(myFind, findText, replaceText)

                'Save and close the document.
                objWordApp.Documents.Item(1).SaveAs(p_sCaminho & "\" & v_sNovoNomeDocumento, wdSaveFormat.wdFormatDocument)
                objWordApp.Documents.Close(Word.WdSaveOptions.wdDoNotSaveChanges)
                objWordApp.Quit()
                objWordApp = Nothing

                Dim MyFileStream As New System.IO.FileStream(p_sCaminho & "\" & v_sNovoNomeDocumento, System.IO.FileMode.Open)
                Dim FileSize As Long
                FileSize = MyFileStream.Length
                Dim Buffer As Byte() = New Byte(CInt(FileSize) - 1) {}
                MyFileStream.Read(Buffer, 0, CInt(MyFileStream.Length))
                MyFileStream.Close()

                Response.Clear()
                Response.AddHeader("Content-Disposition", "inline; filename=" & v_sNovoNomeDocumento)
                Response.Charset = "iso-8859-1"
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("iso-8859-1")
                Response.ContentType = "application/msword"
                Response.BinaryWrite(Buffer)
            End If
        End If
    End Sub

    Private Sub ExecuteReplaceCommand(ByRef p_oMyFind As Object, ByVal p_sFindText As String, ByVal p_sReplaceText As String)
        Try
            Dim Parameters(14) As Object
            Dim missing As Object = System.Reflection.Missing.Value

            Parameters(0) = p_sFindText
            Parameters(1) = missing
            Parameters(2) = missing
            Parameters(3) = missing
            Parameters(4) = missing
            Parameters(5) = missing
            Parameters(6) = missing
            Parameters(7) = missing
            Parameters(8) = missing
            Parameters(9) = p_sReplaceText
            Parameters(10) = Word.WdReplace.wdReplaceAll
            Parameters(11) = missing
            Parameters(12) = missing
            Parameters(13) = missing
            Parameters(14) = missing

            p_oMyFind.GetType().InvokeMember("Execute", Reflection.BindingFlags.InvokeMethod, Nothing, p_oMyFind, Parameters)

        Catch ex As Exception
            Response.Write("<script>alert('Erro: " & p_sFindText & ".'); //window.close();</script>")
        End Try
    End Sub
End Class
