Imports System.IO
Imports System.Web
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls

Public Class HelperBase

#Region " Constantes "
  Const m_cNomePastaLogAcesso = "Log"
  Const m_cNomePastaLogErro = "Log"
  Const m_cNomePastaLogBanco = "Log"

  Const m_cNomeArquivoLogAcesso = "LogAcesso.txt"
  Const m_cNomeArquivoLogErro = "LogErros.txt"
  Const m_cNomeArquivoLogBanco = "LogBanco.txt"
#End Region

#Region " Enumerados "
  Public Enum eTipoDado
    Real
    Inteiro
    Texto
    Data
    Logico
    InteiroNulo
    CNPJ
    CPF
    CEP
  End Enum

  Public Enum eTipoLog
    _Acesso
    _Erro
    _Banco
  End Enum

  Public Enum eSqlFormatoData
    Data
    DataHoraMinuto
    DataHoraMinutoSegundo
  End Enum

  Public Enum eFormatoDateTime
    _12MM
    _12MMSS
    _24MM
    _24MMSS
    DDMMAA
    DDMMAAAA
    DDMMAA_12MM
    DDMMAAAA_12MM
    DDMMAA_12MMSS
    DDMMAAAA_12MMSS
    DDMMAA_24MM
    DDMMAAAA_24MM
    DDMMAA_24MMSS
    DDMMAAAA_24MMSS
  End Enum
#End Region

#Region " Montar Objetos "
  Public Sub MontarDropDownList(ByRef p_oDropDownList As DropDownList, ByRef p_oDataTable As DataTable, ByVal p_sValue As String, ByVal p_sText As String)
    p_oDropDownList.Items.Clear()

    p_oDropDownList.DataSource = p_oDataTable
    p_oDropDownList.DataValueField = p_sValue
    p_oDropDownList.DataTextField = p_sText
    p_oDropDownList.DataBind()
  End Sub

  Public Sub InserirLinhaSelecione(ByRef p_oDataTable As DataTable, ByVal p_sValue As String, ByVal p_sText As String)
    Dim oRow As DataRow

    oRow = p_oDataTable.NewRow
    oRow.Item(p_sValue) = -1
    oRow.Item(p_sText) = "Selecione"

    p_oDataTable.Rows.InsertAt(oRow, 0)
  End Sub

  Public Sub InserirLinhaTodos(ByRef p_oDataTable As DataTable, ByVal p_sValue As String, ByVal p_sText As String)
    Dim oRow As DataRow

    oRow = p_oDataTable.NewRow
    oRow.Item(p_sValue) = -1
    oRow.Item(p_sText) = "Todos"

    p_oDataTable.Rows.InsertAt(oRow, 0)
  End Sub
#End Region

#Region " M�todos para DataGrid "
  Public Sub AjustarPaginacaoDataGrid(ByRef p_oDataGrid As DataGrid, ByVal p_iNumeroRegistros As Integer)
    If p_oDataGrid.CurrentPageIndex >= (p_iNumeroRegistros / p_oDataGrid.PageSize) Then
      p_oDataGrid.CurrentPageIndex = (p_iNumeroRegistros / p_oDataGrid.PageSize) - 1
    End If
  End Sub
#End Region

#Region " M�todo para acesso ao Web.Config "
    Public Shared Function ObterSetting(ByVal p_sKey As String) As String
        ObterSetting = System.Configuration.ConfigurationSettings.AppSettings(p_sKey)
    End Function
#End Region

#Region " M�todos de tratamento de Strings "
  Public Function ContarString(ByVal p_sString1 As String, ByVal p_sString2 As String) As Integer
    ContarString = 0

    If InStr(p_sString1, p_sString2) Then
      For v_iCount As Integer = 1 To p_sString1.Length
        If Mid(p_sString1, v_iCount, p_sString2.Length) = p_sString2 Then
          ContarString += 1
        End If
      Next
    End If
  End Function

  Public Function LimparString(ByVal p_sValor As String) As String
    If p_sValor <> "" Then
      LimparString = p_sValor.Replace("'", "")
    End If
  End Function

  Public Function LimitarString(ByVal p_sValor As String, ByVal p_iTamanho As Integer, Optional ByVal p_bLegenda As Boolean = True) As String
    If p_sValor.Length > p_iTamanho Then
      If p_bLegenda Then
        Return "<font title=""" & p_sValor & """>" & Left(p_sValor, p_iTamanho) & "...</font>"
      Else
        Return Left(p_sValor, p_iTamanho) & "..."
      End If
    Else
      Return p_sValor
    End If
  End Function

  Public Function LimparTelefone(ByVal p_sNumeroTelefone As String) As String
    If p_sNumeroTelefone <> "" Then
      LimparTelefone = Trim(p_sNumeroTelefone.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", ""))
    End If
  End Function

  Public Function MontarTelefone(ByVal p_sNumeroTelefone As String) As String
    If p_sNumeroTelefone <> "" Then
      MontarTelefone = "(" & Left(p_sNumeroTelefone, 2) & ") " & Mid(p_sNumeroTelefone, 3, 4) & "-" & Right(p_sNumeroTelefone, 4)
    End If
  End Function

  Public Function VerificarLetraNumero(ByVal p_sValor As String) As Boolean
    'SE S� TIVER N�MEROS
    If IsNumeric(p_sValor) Then
      Return False
    End If

    'SE S� TIVER LETRAS
    Dim v_iCount As Integer
    Dim v_iQtd As Integer
    For v_iCount = 0 To 9
      If InStr(p_sValor, v_iCount) Then
        v_iQtd += 1
      End If
    Next

    If v_iQtd = 0 Then
      Return False
    End If

    Return True
  End Function
#End Region

#Region " M�todos de tratamento de Integers "
  Public Function TratarInteiro(ByVal p_oValor As Object) As Integer
    If IsDBNull(p_oValor) OrElse Not IsNumeric(p_oValor) Then Return Nothing

    Return p_oValor
  End Function
#End Region

#Region " M�todos para criar scripts "
  Public Sub GerarAlert(ByVal p_oPage As UI.Page, ByVal p_sMensagem As String, ByVal p_sChave As String)
    Dim v_sScript As String

        v_sScript &= "<!--__MENSAGEM_INI<script language=""JavaScript"">" & vbCrLf
    v_sScript &= "  alert(""" & p_sMensagem & """);" & vbCrLf
        v_sScript &= "</script>__MENSAGEM_FIM-->" & vbCrLf

    p_oPage.RegisterStartupScript(p_sChave, v_sScript)
  End Sub

  Public Sub GerarScript(ByVal p_oPage As UI.Page, ByVal p_sScript As String, ByVal p_sChave As String)
    Dim v_sScript As String

    v_sScript &= "<script language=""JavaScript"">" & vbCrLf
    v_sScript &= p_sScript & vbCrLf
    v_sScript &= "</script>" & vbCrLf

    p_oPage.RegisterStartupScript(p_sChave, v_sScript)
  End Sub
#End Region

#Region " M�todos de Sauda��o "
  Public Function ObterSaudacao(Optional ByVal p_iHora As Integer = 0) As String
    If p_iHora = 0 Then p_iHora = Now().Hour

    If p_iHora < 12 Then
      Return "Bom dia"
    ElseIf p_iHora >= 12 And p_iHora < 18 Then
      Return "Boa tarde"
    ElseIf p_iHora >= 18 Then
      Return "Boa noite"
    End If
  End Function

  Public Function ObterDiaSemana(Optional ByVal p_iDiaSemana As Integer = 0) As String
    If p_iDiaSemana = 0 Then p_iDiaSemana = Now().DayOfWeek

    Select Case p_iDiaSemana
      Case DayOfWeek.Monday
        Return "Segunda-Feira"
      Case DayOfWeek.Tuesday
        Return "Ter�a-Feira"
      Case DayOfWeek.Wednesday
        Return "Quarta-Feira"
      Case DayOfWeek.Thursday
        Return "Quinta-Feira"
      Case DayOfWeek.Friday
        Return "Sexta-Feira"
      Case DayOfWeek.Saturday
        Return "S�bado"
      Case DayOfWeek.Sunday
        Return "Domingo"
    End Select
  End Function

  Public Function ObterMes(Optional ByVal p_iMes As Integer = 0) As String
    If p_iMes = 0 Then p_iMes = Now().Month

    Select Case p_iMes
      Case 1
        Return "Janeiro"
      Case 2
        Return "Fevereiro"
      Case 3
        Return "Mar�o"
      Case 4
        Return "Abril"
      Case 5
        Return "Maio"
      Case 6
        Return "Junho"
      Case 7
        Return "Julho"
      Case 8
        Return "Agosto"
      Case 9
        Return "Setembro"
      Case 10
        Return "Outubro"
      Case 11
        Return "Novembro"
      Case 12
        Return "Dezembro"
    End Select
  End Function

  Public Function GerarHorario(Optional ByVal p_iDia As Integer = 0, Optional ByVal p_iMes As Integer = 0, Optional ByVal p_iAno As Integer = 0, Optional ByVal p_iDiaSemana As Integer = 0) As String
    If p_iDia = 0 Then p_iDia = Now().Day
    If p_iAno = 0 Then p_iAno = Now().Year

    Return ObterDiaSemana(p_iDiaSemana) & ", " & p_iDia & " de " & ObterMes(p_iMes) & " de " & p_iAno & "."
  End Function
#End Region

#Region " M�todos para convers�o de valores entre Aplica��o/Banco "
  Public Function AppBanco(ByVal valor As Object, ByVal tipo As eTipoDado) As Object
    Select Case tipo
      Case eTipoDado.Real '=====================================================
        Try
          If Trim(valor) = "" Or Trim(valor).ToLower = "&nbsp;" Then
            valor = "NULL"
          Else
            If (InStr(1, valor, ".") <> 0) And (InStr(1, valor, ",") <> 0) Then
              If InStr(1, valor, ".") < InStr(1, valor, ",") Then
                valor = Replace(valor, ",", "V")
                valor = Replace(valor, ".", "")
                valor = Replace(valor, "V", ".")
              Else
                valor = Replace(valor, ",", "")
              End If
            ElseIf InStr(1, valor, ".") <> 0 Then
              valor = valor
            ElseIf InStr(1, valor, ",") <> 0 Then
              valor = Replace(valor, ",", ".")
            Else
              valor = valor
            End If
          End If
        Catch ex As Exception
          valor = "NULL"
        End Try

      Case eTipoDado.Inteiro '==================================================
        If Trim(valor) = "" Or Not IsNumeric(valor) Then
          valor = 0
        End If

      Case eTipoDado.Texto '====================================================
        valor = Replace(valor, "'", "''")
        If Trim(valor) = "" Then
          valor = "''" 'DBNull.Value
        Else
          valor = "'" & Trim(valor) & "'"
        End If

      Case eTipoDado.Data '=====================================================
        Try
          'If IsDate(valor) Then
          '  valor = "'" & Format(CType(valor, Date), "yyyyMMdd") & "'"
          'Else
          '  valor = "NULL"
          'End If
        Catch ex As Exception
          valor = "NULL"
        End Try

      Case eTipoDado.Logico '===================================================
        If valor = True Then
          valor = "'S'"
        Else
          valor = "'N'"
        End If

      Case eTipoDado.InteiroNulo '==============================================
        If Trim(valor) = "" Or Not IsNumeric(valor) Then
          valor = "Null"
        Else
          If valor = 0 Then
            valor = "Null"
          End If
        End If

      Case eTipoDado.CEP
        Return ("'" & Mid(valor, 1, 5) & Mid(valor, 7, 3) & "'")

      Case eTipoDado.CPF
        Return ("'" & Mid(valor, 1, 3) & Mid(valor, 5, 3) & Mid(valor, 9, 3) & Mid(valor, 13, 2) & "'")

      Case eTipoDado.CNPJ
        valor = ("'" & Mid(valor, 1, 2) & Mid(valor, 4, 3) & Mid(valor, 8, 3) & _
                Mid(valor, 12, 4) & Mid(valor, 17, 2) & "'")
    End Select

    Return (valor)
  End Function

  Public Function BancoApp(ByVal valor As Object, ByVal tipo As eTipoDado) As String
    Select Case tipo
      Case eTipoDado.Real '=====================================================
        Try
          If IsDBNull(valor) Then
            valor = ""
          Else
            valor = Format(Val(valor), "#,###0.00")
          End If
        Catch ex As Exception
          valor = ""
        End Try

      Case eTipoDado.Inteiro '==================================================
        If IsDBNull(valor) Then
          valor = 0
        End If

      Case eTipoDado.InteiroNulo '==============================================
        If IsDBNull(valor) Then
          valor = ""
        End If

      Case eTipoDado.Texto '====================================================
        If IsDBNull(valor) Then
          valor = ""
        Else
          valor = Trim(valor)
        End If

      Case eTipoDado.Data '=====================================================
        If IsDBNull(valor) Then
          valor = ""
        Else
          valor = CType(valor, Date)
        End If

      Case eTipoDado.Logico '===================================================
        If IsDBNull(valor) Then
          valor = False
        Else
          If Trim(valor) = "S" Then
            valor = True
          Else
            valor = False
          End If
        End If

      Case eTipoDado.CEP
        If IsDBNull(valor) Then
          valor = ""
        Else
          valor = (Mid(valor, 1, 5) & "-" & Mid(valor, 6, 3))
        End If

      Case eTipoDado.CNPJ
        If IsDBNull(valor) Then
          valor = ""
        Else
          valor = (Mid(valor, 1, 2) & "." & _
                  Mid(valor, 3, 3) & "." & _
                  Mid(valor, 6, 3) & "/" & _
                  Mid(valor, 9, 4) & "-" & _
                  Mid(valor, 13, 2))
        End If

      Case eTipoDado.CPF
        If IsDBNull(valor) Then
          valor = ""
        Else
          valor = (Mid(valor, 1, 3) & "." & _
                  Mid(valor, 4, 3) & "." & _
                  Mid(valor, 7, 3) & "-" & _
                  Mid(valor, 10, 2))
        End If

    End Select

    Return (valor)
  End Function

  Public Function BancoValor(ByVal valor As Object, ByVal tipo As eTipoDado) As String
    Select Case tipo
      Case eTipoDado.Real '=====================================================
        Try
          If IsDBNull(valor) Then
            valor = 0
          Else
            valor = CType(valor, Decimal)
          End If
        Catch ex As Exception
          valor = 0
        End Try

      Case eTipoDado.Inteiro '==================================================
        If IsDBNull(valor) Then
          valor = 0
        End If

      Case eTipoDado.InteiroNulo '==============================================
        If IsDBNull(valor) Then
          valor = 0
        End If

      Case eTipoDado.Texto '====================================================
        If IsDBNull(valor) Then
          valor = ""
        Else
          valor = Trim(valor)
        End If

      Case eTipoDado.Data '=====================================================
        If IsDBNull(valor) Then
          valor = ""
        Else
          valor = CType(valor, Date)
        End If

      Case eTipoDado.Logico '===================================================
        If IsDBNull(valor) Then
          valor = False
        Else
          If Trim(valor) = "S" Then
            valor = True
          Else
            valor = False
          End If
        End If

    End Select

    Return (valor)
  End Function

  Public Function AppValor(ByVal valor As Object, ByVal tipo As eTipoDado) As Object
    Select Case tipo
      Case eTipoDado.Texto '====================================================
        If valor = "&nbsp;" Then
          valor = ""
        End If

      Case eTipoDado.Real '=====================================================
        Try
          If IsNumeric(valor) Then
            If Trim(valor) = "" Then
              valor = 0.0
            Else
              If (InStr(1, valor, ".") <> 0) And (InStr(1, valor, ",") <> 0) Then
                If InStr(1, valor, ".") < InStr(1, valor, ",") Then
                  valor = Replace(valor, ".", "")
                Else
                  valor = Replace(valor, ",", "")
                  valor = Replace(valor, ".", ",")
                End If
              ElseIf InStr(1, valor, ".") <> 0 Then
                valor = Replace(valor, ".", ",")
              ElseIf InStr(1, valor, ",") <> 0 Then
                valor = valor
              Else
                valor = valor
              End If
            End If
          Else
            valor = 0.0
          End If
        Catch ex As Exception
          valor = 0.0
        End Try
        valor = CType(valor, Decimal)
      Case eTipoDado.Inteiro '==================================================
        If Trim(valor) = "" Or Not IsNumeric(valor) Then
          valor = 0
        End If

      Case eTipoDado.Data '=====================================================
        Try
          'If Not IsDate(valor) Then
          '  valor = ""
          'End If
        Catch ex As Exception
          valor = ""
        End Try

      Case eTipoDado.Logico '===================================================
        'If valor = True Then
        '  valor = "'S'"
        'Else
        '  valor = "'N'"
        'End If

      Case eTipoDado.InteiroNulo '==============================================
        If Trim(valor) = "" Or Not IsNumeric(valor) Then
          valor = ""
        Else
          If valor = 0 Then
            valor = ""
          End If
        End If
    End Select

    Return (valor)
  End Function

  Public Function ValorApp(ByVal valor As Object, ByVal tipo As eTipoDado) As Object
    Select Case tipo
      Case eTipoDado.Real '=====================================================
        Try
          If IsDBNull(valor) Then
            valor = ""
          Else
            valor = Format(CType(valor, Decimal), "#,###0.00")
          End If
        Catch ex As Exception
          valor = ""
        End Try

      Case eTipoDado.Inteiro '==================================================
        If IsNumeric(valor) Then
          If Trim(valor) = "" Then
            valor = 0
          End If
        Else
          valor = 0
        End If

      Case eTipoDado.InteiroNulo '==============================================
        If IsNumeric(valor) Then
          If Trim(valor) = "" Or valor = 0 Then
            valor = ""
          End If
        Else
          valor = ""
        End If

      Case eTipoDado.Texto '====================================================
        valor = Replace(Trim(valor), vbNewLine, "<BR>")

      Case eTipoDado.Data '=====================================================
        'If IsDate(valor) Then
        '  valor = CType(valor, Date)
        'Else
        '  valor = ""
        'End If

      Case eTipoDado.Logico '===================================================
        'If IsDBNull(valor) Then
        '  valor = False
        'Else
        '  If Trim(valor) = "S" Then
        '    valor = True
        '  Else
        '    valor = False
        '  End If
        'End If
    End Select

    Return (valor)
  End Function
#End Region

#Region " M�todo para Gravar Log "
  Public Sub GerarLog(ByVal p_sTexto As String, Optional ByVal p_eTipoLog As eTipoLog = eTipoLog._Erro)
    Try
      Dim v_sCaminhoArquivo As String = System.Web.HttpContext.Current.Server.MapPath(".")

      Select Case p_eTipoLog
        Case eTipoLog._Acesso
          v_sCaminhoArquivo &= "\" & m_cNomePastaLogAcesso & "\" & m_cNomeArquivoLogAcesso
        Case eTipoLog._Erro
          v_sCaminhoArquivo &= "\" & m_cNomePastaLogErro & "\" & m_cNomeArquivoLogErro
        Case eTipoLog._Banco
          v_sCaminhoArquivo &= "\" & m_cNomePastaLogBanco & "\" & m_cNomeArquivoLogBanco
      End Select

      CriarArquivoTXT(v_sCaminhoArquivo, p_sTexto)

    Catch ex As Exception
      Throw
    End Try
  End Sub
#End Region

#Region " M�todos para trabalhar com arquivos e pastas "
  Public Sub CriarArquivoTXT(ByVal p_sPastaNomeArquivo As String, Optional ByVal p_sConteudo As String = "", Optional ByVal p_bAutoDelete As Boolean = False)
    Dim v_oArquivo As File
    Dim v_oFileWriter As StreamWriter

    If p_bAutoDelete Then
      If v_oArquivo.Exists(p_sPastaNomeArquivo) Then
        v_oArquivo.Delete(p_sPastaNomeArquivo)
      End If
    End If

    If Not v_oArquivo.Exists(p_sPastaNomeArquivo) Then
      v_oFileWriter = v_oArquivo.CreateText(p_sPastaNomeArquivo)
    Else
      If p_sConteudo <> String.Empty Then
        v_oFileWriter = v_oArquivo.AppendText(p_sPastaNomeArquivo)
      End If
    End If

    If p_sConteudo <> String.Empty Then
      v_oFileWriter.Write(p_sConteudo)
      v_oFileWriter.Flush()
      v_oFileWriter.Close()
    End If

    v_oArquivo = Nothing
    v_oFileWriter = Nothing
  End Sub

  Public Sub CriarPasta(ByVal p_sNomePasta As String, Optional ByVal p_bAutoDelete As Boolean = False)
    Dim oDirectoryInfo As New DirectoryInfo(p_sNomePasta)

    If p_bAutoDelete AndAlso oDirectoryInfo.Exists() Then
      DeletarArquivoTodos(p_sNomePasta)
      oDirectoryInfo.Delete()
    End If

    oDirectoryInfo.Create()

    oDirectoryInfo = Nothing
  End Sub

  Public Sub DeletarPasta(ByVal p_sNomePasta As String)
    Dim oDirectoryInfo As New DirectoryInfo(p_sNomePasta)

    If oDirectoryInfo.Exists() Then
      DeletarArquivoTodos(p_sNomePasta)
      oDirectoryInfo.Delete()
    End If

    oDirectoryInfo = Nothing
  End Sub

  Public Function GerarNomeArquivoUnico(ByVal p_sCaminhoNomeArquivo As String) As String
    Dim v_sInicioNomeArquivo As String
    Dim v_sExtensaoArquivo As String
    Dim v_sIdentificadorUnico As String

    v_sInicioNomeArquivo = Left(p_sCaminhoNomeArquivo, InStr(p_sCaminhoNomeArquivo, ".") - 1)
    v_sExtensaoArquivo = Right(p_sCaminhoNomeArquivo, Len(p_sCaminhoNomeArquivo) - InStr(p_sCaminhoNomeArquivo, ".") + 1)
    v_sIdentificadorUnico = "_" & Now().ToString("yyyyMMddhhmmss") & "_" & Environment.TickCount

    Return v_sInicioNomeArquivo & v_sIdentificadorUnico & v_sExtensaoArquivo
  End Function

#Region " Deletar Arquivos "
  Public Sub DeletarArquivoTodos(ByVal p_sPastaArquivo As String, Optional ByVal p_sExtensao As String = "*.*")
    Dim oDirectoryInfo As New DirectoryInfo(p_sPastaArquivo)

    For Each oFileInfo As FileInfo In oDirectoryInfo.GetFiles(p_sExtensao)
      If oFileInfo.Exists Then
        oFileInfo.Delete()
      End If
    Next

    oDirectoryInfo = Nothing
  End Sub

  Public Sub DeletarArquivoTempo(ByVal p_iMinutos As Integer, ByVal p_sPastaArquivo As String, Optional ByVal p_sExtensao As String = "*.*")
    Dim v_oDirectoryInfo As New DirectoryInfo(p_sPastaArquivo)

    For Each v_oFileInfo As FileInfo In v_oDirectoryInfo.GetFiles(p_sExtensao)
      If v_oFileInfo.Exists Then
        If v_oFileInfo.LastAccessTime.AddMinutes(p_iMinutos) < Now() Then
          v_oFileInfo.Delete()
        End If
      End If
    Next

    v_oDirectoryInfo = Nothing
  End Sub

  Public Sub DeletarArquivo(ByVal p_sPastaNomeArquivo As String)
    Dim v_oArquivo As File

    If v_oArquivo.Exists(p_sPastaNomeArquivo) Then
      v_oArquivo.Delete(p_sPastaNomeArquivo)
    End If

    v_oArquivo = Nothing
  End Sub
#End Region

#End Region

#Region " M�todo para fazer UPLOAD de arquivos "
  Public Function UploadArquivos(ByVal p_sPastaDestinoArquivo As String) As Integer
    Dim v_oFiles As HttpFileCollection = System.Web.HttpContext.Current.Request.Files
    Dim v_iTotalSucesso As Integer = 0

    Try
      For v_iCount As Integer = 0 To v_oFiles.Count - 1
        Dim c_oFile As HttpPostedFile = v_oFiles(v_iCount)

        Try
          If c_oFile.ContentLength > 0 Then
            c_oFile.SaveAs(p_sPastaDestinoArquivo & Path.GetFileName(c_oFile.FileName))
          End If

          v_iTotalSucesso += 1

        Catch ex As Exception
          Throw New Exception("Erro na classe Helper - UploadArquivos: " & vbNewLine & ex.Message)

        Finally
          c_oFile = Nothing
        End Try
      Next

    Catch ex As Exception
      Throw

    Finally
      v_oFiles = Nothing
      UploadArquivos = v_iTotalSucesso

    End Try
  End Function
#End Region

#Region " M�todo para Datas "
    Public Shared Function DataIsNull(ByVal p_dtData As Date) As Boolean
        If p_dtData = #12:00:00 AM# Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Function DataIsNull(ByVal p_dtData As String) As Boolean
        If Trim(p_dtData) = "12:00:00 AM" Or Left(Trim(p_dtData), 10) = "01/01/0001" Or Left(Trim(p_dtData), 10) = "0001/01/01" Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function FormatarDataHora(ByVal p_oData As Object, Optional ByVal p_eFormatoDateTime As eFormatoDateTime = Nothing, Optional ByVal p_sFormatoProprio As String = "") As String
        If IsDBNull(p_oData) OrElse IsNothing(p_oData) Then Return Nothing

        p_oData = ConverterDataHora(p_oData)

        If DataIsNull(p_oData) Then Return Nothing

        If p_sFormatoProprio <> String.Empty Then Return Format(p_oData, p_sFormatoProprio)

        Dim v_sFormato As String

        Select Case p_eFormatoDateTime
            Case eFormatoDateTime._12MM
                v_sFormato = "hh:mm"
            Case eFormatoDateTime._12MMSS
                v_sFormato = "hh:mm:ss"
            Case eFormatoDateTime._24MM
                v_sFormato = "HH:mm"
            Case eFormatoDateTime._24MMSS
                v_sFormato = "HH:mm:ss"
            Case eFormatoDateTime.DDMMAA
                v_sFormato = "dd/MM/yy"
            Case eFormatoDateTime.DDMMAA_12MM
                v_sFormato = "dd/MM/yy hh:mm"
            Case eFormatoDateTime.DDMMAA_12MMSS
                v_sFormato = "dd/MM/yy hh:mm:ss"
            Case eFormatoDateTime.DDMMAA_24MM
                v_sFormato = "dd/MM/yy HH:mm"
            Case eFormatoDateTime.DDMMAA_24MMSS
                v_sFormato = "dd/MM/yy HH:mm:ss"
            Case eFormatoDateTime.DDMMAAAA
                v_sFormato = "dd/MM/yyyy"
            Case eFormatoDateTime.DDMMAAAA_12MM
                v_sFormato = "dd/MM/yyyy hh:mm"
            Case eFormatoDateTime.DDMMAAAA_12MMSS
                v_sFormato = "dd/MM/yyyy hh:mm:ss"
            Case eFormatoDateTime.DDMMAAAA_24MM
                v_sFormato = "dd/MM/yyyy HH:mm"
            Case eFormatoDateTime.DDMMAAAA_24MMSS
                v_sFormato = "dd/MM/yyyy HH:mm:ss"
        End Select

        Return Format(p_oData, v_sFormato)
    End Function

    Public Function ConverterDataHora(ByVal p_oData As Object) As Date
        Try
            Return Convert.ToDateTime(p_oData)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
#End Region

#Region " M�todos para Queries SQL "
  Public Function SqlTratarPlic(ByVal p_sValor As String) As String
    If IsNothing(p_sValor) Then
      Return Nothing
    Else
      Return p_sValor.Replace("'", "''")
    End If
  End Function

  Public Function SqlTratarPercent(ByVal p_sValor As String) As String
    If IsNothing(p_sValor) Then
      Return Nothing
    Else
      Return p_sValor.Replace("%", "[%]")
    End If
  End Function

  Public Function SqlTratarUnderScore(ByVal p_sValor As String) As String
    If IsNothing(p_sValor) Then
      Return Nothing
    Else
      Return p_sValor.Replace("_", "[_]")
    End If
  End Function

  Public Function SqlTratarColchete(ByVal p_sValor As String) As String
    If IsNothing(p_sValor) Then
      Return Nothing
    Else
      Return p_sValor.Replace("[", "[[]")
    End If
  End Function

  Public Function SqlTratarLike(ByVal p_sValor As String) As String
    Return SqlTratarUnderScore(SqlTratarPercent(SqlTratarColchete(SqlTratarPlic(p_sValor))))
  End Function

  Public Function SqlAdicionarPlic(ByVal p_sValor As String) As String
    If Not IsNothing(p_sValor) Then
      Return "'" & SqlTratarPlic(p_sValor) & "'"
    Else
      Return "NULL"
    End If
  End Function

  Public Function FormataSqlData(ByVal p_dtData As Date, ByVal p_eSqlFormatoData As eSqlFormatoData) As String
    If Not (DataIsNull(p_dtData)) Then
      Select Case p_eSqlFormatoData
        Case eSqlFormatoData.Data
          Return "'" & Format(p_dtData, "yyyy-MM-dd") & "'"
        Case eSqlFormatoData.DataHoraMinuto
          Return "'" & Format(p_dtData, "yyyy-MM-dd ") & Hour(p_dtData) & Format(p_dtData, ":mm") & "'"
        Case eSqlFormatoData.DataHoraMinutoSegundo
          Return "'" & Format(p_dtData, "yyyy-MM-dd ") & Hour(p_dtData) & Format(p_dtData, ":mm:ss") & "'"
      End Select
    Else
      Return "NULL"
    End If
  End Function
#End Region

#Region " M�todo para convers�o de valores "
  'TODO: PENSAR ONDE COLOCAR ESSE M�TODO DE CONVERS�O
  Public Function ConverterValor(ByVal p_oValor As Object) As Object
    If IsDBNull(p_oValor) Then Return Nothing
    If p_oValor.GetType Is GetType(UInt16) Then Return Convert.ToInt16(p_oValor)
    If p_oValor.GetType Is GetType(UInt32) Then Return Convert.ToInt32(p_oValor)
    If p_oValor.GetType Is GetType(UInt64) Then Return Convert.ToInt64(p_oValor)

    If p_oValor.GetType Is GetType(SByte) Then Return Convert.ToBoolean(p_oValor)

    'TODO: RETIRADO MYSQL
    'If p_oValor.GetType Is GetType(MySql.Data.Types.MySqlDateTime) Then
    '  Try
    '    Return Convert.ToDateTime(p_oValor)
    '  Catch ex As Exception
    '    Return Nothing
    '  End Try
    'End If

    Return p_oValor
  End Function
#End Region

#Region " M�todo para Montar Array de Par�metros "
  Public Function MontarArrayParametros(ByVal p_oCommand As SqlCommand) As SqlParameter()
    Dim v_oParametros(p_oCommand.Parameters.Count - 1) As SqlParameter

    For v_iCount As Integer = 0 To p_oCommand.Parameters.Count - 1
      v_oParametros(v_iCount) = New SqlParameter(p_oCommand.Parameters(v_iCount).ParameterName, p_oCommand.Parameters(v_iCount).Value)
    Next

    Return v_oParametros
  End Function
#End Region

End Class
