Public Class Helper
    Inherits FrameWork.HelperBase

    Public Sub PrencheComboProcesso(ByVal p_iCodigoProcesso As Integer, ByVal p_iCodigoAdvogado As Integer, ByRef p_oCombo As DropDownList, Optional ByVal p_eLinhaExtra As LinhaExtra = LinhaExtra.Nenhuma)

        Dim v_oRN As New clsProcessoRN

        PreencherCombo(p_oCombo, v_oRN.ListarProcesso(p_iCodigoAdvogado), "PASTA", "XPROCESSO", p_eLinhaExtra)

    End Sub


    Public Sub PreencheComboEscritorio(ByRef p_oCombo As DropDownList, Optional ByVal p_eLinhaExtra As LinhaExtra = LinhaExtra.Nenhuma)

        Dim v_oRN As New clsProcessoRN

        PreencherCombo(p_oCombo, v_oRN.ListarEscritorio, "USERID", "USUARIO", p_eLinhaExtra)

    End Sub

    Public Sub PreencheComboProcesso(ByRef p_oCombo As DropDownList, Optional ByVal p_eLinhaExtra As LinhaExtra = LinhaExtra.Nenhuma)

        Dim v_oRN As New clsProcessoRN

        PreencherCombo(p_oCombo, v_oRN.ListarProcessoIntermediario, "PASTA", "XPROCESSO", p_eLinhaExtra)

    End Sub

    Public Sub PrencheComboPrognostico(ByRef p_oCombo As DropDownList, Optional ByVal p_eLinhaExtra As LinhaExtra = LinhaExtra.Nenhuma)

        Dim v_oRN As New clsPrognosticoRN
        PreencherCombo(p_oCombo, v_oRN.Listar(, "PROGNOSTICO").Tables(0), "PROGNOSTICO", "XPROGNOSTICO", p_eLinhaExtra)

    End Sub

    Public Sub PrencheComboPedido(ByRef p_oCombo As DropDownList, Optional ByVal p_eLinhaExtra As LinhaExtra = LinhaExtra.Nenhuma)

        Dim v_oRN As New clsObjetoRN
        PreencherCombo(p_oCombo, v_oRN.Listar(, "OBJETO").Tables(0), "OBJETO", "XOBJETO", p_eLinhaExtra)

    End Sub

    Public Sub PreencheComboSituacaoItemProjuris(ByVal p_oCombo As DropDownList, ByVal p_ePerfil As Enumerados.ePerfil, Optional ByVal p_eLinhaExtra As LinhaExtra = LinhaExtra.Todos)

        Dim v_oRN As New clsSituacaoItemIntermediarioRN

        PreencherCombo(p_oCombo, v_oRN.Listar().Tables(0), "SITUACAO_ITEM_INTERMEDIARIO", "XSITUACAO_ITEM_INTERMEDIARIO", p_eLinhaExtra)

        Select Case p_ePerfil
            Case Enumerados.ePerfil.Administrador
                p_oCombo.SelectedValue = 2
            Case Enumerados.ePerfil.Advogado
                p_oCombo.SelectedValue = 1
        End Select


    End Sub

    Public Sub PrencheComboLancamento(ByRef p_oCombo As DropDownList, Optional ByVal p_eLinhaExtra As LinhaExtra = LinhaExtra.Nenhuma)

        Dim v_oRN As New clsLancamentoRN
        PreencherCombo(p_oCombo, v_oRN.Listar(, "LANCAMENTO").Tables(0), "LANCAMENTO", "XLANCAMENTO", p_eLinhaExtra)

        p_oCombo.SelectedValue = 9 'Custas judiciais

    End Sub

    Public Sub PrencheComboGarantia(ByRef p_oCombo As DropDownList, Optional ByVal p_eLinhaExtra As LinhaExtra = LinhaExtra.Nenhuma)

        Dim v_oRN As New clsGarantiaJuridicoRN
        PreencherCombo(p_oCombo, v_oRN.Listar(, "GARANTIA").Tables(0), "GARANTIA", "XGARANTIA", p_eLinhaExtra)

    End Sub


    Public Sub PreencheComboAutor(ByRef p_oCombo As DropDownList, Optional ByVal p_eLinhaExtra As LinhaExtra = LinhaExtra.Nenhuma)

        p_oCombo.Items.Clear()
        Dim v_sTexto As String = ""
        Select Case p_eLinhaExtra
            Case LinhaExtra.Selecione
                v_sTexto = "Selecione"
            Case LinhaExtra.Todos
                v_sTexto = "Todos"
        End Select

        If Not v_sTexto = "" Then
            Dim v_oItem0 As New ListItem(v_sTexto, "-1")
            p_oCombo.Items.Add(v_oItem0)
        End If
        p_oCombo.Items.Add(New ListItem("Adverso", "A"))
        p_oCombo.Items.Add(New ListItem("Cliente", "C"))

    End Sub

    Public Function ObterSituacaoProcesso(ByVal p_iCodigoProcesso As Integer) As Integer

        Dim v_oRN As New clsProcessoRN
        v_oRN.DAO.AdicionarParametro(New ParametroDB("@XPROCESSO", p_iCodigoProcesso))
        Return v_oRN.DAO.ExecSP_Scalar(ObjetoBancoDados.StoredProcedures.SP_OBTER_SITUACAO_PROCESSO)

    End Function

    Public Sub PreencheComboSituacao(ByRef p_oCombo As DropDownList, Optional ByVal p_eLinhaExtra As LinhaExtra = LinhaExtra.Nenhuma)

        Dim v_oRN As New clsSituacaoRN
        PreencherCombo(p_oCombo, v_oRN.Listar(, "SITUACAO").Tables(0), "SITUACAO", "XSITUACAO", p_eLinhaExtra)

    End Sub

    Public Sub PrencheComboEvento(ByRef p_oCombo As DropDownList, Optional ByVal p_eLinhaExtra As LinhaExtra = LinhaExtra.Nenhuma)

        Dim v_oRN As New clsEventoJuridicoRN
        PreencherCombo(p_oCombo, v_oRN.Listar(, "EVENTO").Tables(0), "EVENTO", "XEVENTO", p_eLinhaExtra)

    End Sub

    Public Sub PrencheComboMotivo(ByRef p_oCombo As DropDownList, Optional ByVal p_eLinhaExtra As LinhaExtra = LinhaExtra.Nenhuma)

        Dim v_oRN As New clsEncerramentoRN
        PreencherCombo(p_oCombo, v_oRN.Listar(, "ENCERRAMENTO").Tables(0), "ENCERRAMENTO", "XENCERRAMENTO", LinhaExtra.Selecione)

    End Sub

    Public Function PreenchePartes(ByRef p_oGrid As DataGrid, ByVal p_iCodigoProcesso As Integer, Optional ByVal p_sOrdenacao As String = "NOME")

        Dim v_oRN As New clsPartesRN
        Dim v_oArrayParametros As New ArrayList
        v_oArrayParametros.Add(New ParametroDB("XPROCESSO", p_iCodigoProcesso))
        Dim v_oDados As DataTable = v_oRN.Listar(v_oArrayParametros, p_sOrdenacao).Tables(0)
        Me.AcertaDataSource(v_oDados, p_oGrid)
        p_oGrid.DataSource = v_oDados
        p_oGrid.DataBind()

    End Function

    Public Sub PreencheComboDecisao(ByRef p_oCombo As DropDownList, ByVal p_eLinhaExtra As LinhaExtra)

        Dim v_oRN As New clsDecisaoRN
        PreencherCombo(p_oCombo, v_oRN.Listar(, "DECISAO").Tables(0), "DECISAO", "XDECISAO", p_eLinhaExtra)

    End Sub

    Public Sub PreencheComboJurisdicao(ByRef p_oCombo As DropDownList, ByVal p_eLinhaExtra As LinhaExtra)

        Dim v_oRN As New clsJurisdicaoRN
        PreencherCombo(p_oCombo, v_oRN.Listar(, "JURISDICAO").Tables(0), "JURISDICAO", "XJURISDICAO", p_eLinhaExtra)

    End Sub

    Public Sub PreencheComboFase(ByRef p_oCombo As DropDownList, ByVal p_eLinhaExtra As LinhaExtra)

        Dim v_oRN As New clsFaseRN
        PreencherCombo(p_oCombo, v_oRN.Listar(, "FASE").Tables(0), "FASE", "XFASE", p_eLinhaExtra)

    End Sub

    Public Sub PreencheComboForo(ByRef p_oCombo As DropDownList, ByVal p_eLinhaExtra As LinhaExtra)

        Dim v_oRN As New clsForoRN
        Dim v_oDados As DataTable = v_oRN.Listar(, "FORO").Tables(0)

        For Each v_oLinha As DataRow In v_oDados.Rows

            v_oLinha.Item("FORO") &= " - " & v_oLinha.Item("ESTADO")

        Next

        PreencherCombo(p_oCombo, v_oDados, "FORO", "XFORO", p_eLinhaExtra)

    End Sub

    Public Sub PrencheComboUsuarioSEGAB(ByRef p_oCombo As DropDownList, Optional ByVal p_eLinhaExtra As LinhaExtra = LinhaExtra.Nenhuma)

        Dim v_oRN As New clsAcessoRN
        PreencherCombo(p_oCombo, v_oRN.ListarUsuariosSEGAB(), "NOME", "USUARIO_ID", p_eLinhaExtra)

    End Sub

    Public Sub PrencheComboUsuarioJuridico(ByRef p_oCombo As DropDownList, Optional ByVal p_eLinhaExtra As LinhaExtra = LinhaExtra.Nenhuma)

        Dim v_oRN As New clsAcessoRN
        PreencherCombo(p_oCombo, v_oRN.ListarUsuariosJuridico(), "USUARIO", "XUSUARIO", p_eLinhaExtra)

    End Sub

    Public Enum SituacaoProcesso
        EmEdicao = 1
        Pendente = 2
        Aceito = 3
        Recusado = 4
        Encerrado = 96
    End Enum


End Class
