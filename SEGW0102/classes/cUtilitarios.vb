Public Class cUtilitarios

    'Trata a data para o seuginte formato dd/mm/aaaa hh:mm
    Public Shared Function trataDataHora(ByVal data As String) As String

        Try
            Dim dt_final As String

            Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim ano As String = CType(data, DateTime).Year

            Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = dia + "/" + mes + "/" + ano


            Return dt_final + " " + hora + ":" + minuto

        Catch ex As Exception

            Return ""

        End Try
        
    End Function

    '''Exibe o valor em um alert na tela
    Public Shared Sub br(ByVal valor As String)

        Dim texto As String

        texto = "<script>" + vbNewLine
        texto &= "alert(""" + valor + """);"
        texto &= "</script>"

        System.Web.HttpContext.Current.Response.Write(texto)
    End Sub


    Public Shared Function trataMoeda(ByVal valor As String) As String
        If valor = "&nbsp;" Or valor = "" Then
            Return ""
        Else            
            Return String.Format(String.Format("{0:c}", CDbl(valor))).Substring(3).Replace("(", "").Replace(")", "")
        End If


    End Function


    Public Shared Function trataCPF(ByVal cpf As String) As String

        If cpf.Length = 11 Then
            Return cpf.Substring(0, 3) + "." + cpf.Substring(3, 3) + "." + cpf.Substring(6, 3) + "-" + cpf.Substring(9, 2)
        Else
            Return cpf
        End If

    End Function

    Public Shared Function destrataCPF(ByVal cpf As String) As String

        If cpf.Length = 14 Then
            Return cpf.Replace(".", "").Replace("-", "")
        Else
            Return cpf
        End If

    End Function

    Public Shared Sub escreveScript(ByVal valor As String)
        Web.HttpContext.Current.Response.Write("<script>" + valor + "</script>")
    End Sub

    Public Shared Sub escreve(ByVal valor As String)
        Web.HttpContext.Current.Response.Write(valor)
    End Sub


    'Fun��o n�o utilit�ria, mas repetitiva... com isso n�o fica sendo necess�rio reescreve-la
    'Fun��o para validar o periodo de competencia
    Public Shared Function getPeriodoCompetencia(ByVal apolice As Integer, ByVal ramo As Integer, ByVal subgrupo_id As Integer)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5696_SPS(apolice, ramo, subgrupo_id, cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
        Dim data_inicio, data_fim, retorno As String

        If dr.HasRows Then
            While dr.Read()

                data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)

            End While

            retorno = "Per�odo de Compet�ncia: " & data_inicio & " at� " & data_fim
        Else
            'cUtilitarios.br("N�o existe nenhuma Compet�ncia de Fatura em aberto")
            'System.Web.HttpContext.Current.Response.Write("<script>parent.window.location=parent.window.location;</script>")
            'Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
            System.Web.HttpContext.Current.Response.Write("<script>parent.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp'</script>")
            System.Web.HttpContext.Current.Response.End()
            retorno = ""
        End If
        dr.Close()
        dr = Nothing
        Return retorno
    End Function

    'Trata a data para o seguinte formato dd/mm/aaaa 
    Public Shared Function trataData(ByVal data As String) As String

        Try

            Dim dt_arr() As String = data.Split("/")
            Dim dt_final As String

            'Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim dia As String = dt_arr(0).PadLeft(2, "0")
            'Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim mes As String = dt_arr(1).PadLeft(2, "0")
            'Dim ano As String = CType(data, DateTime).Year
            Dim ano As String = dt_arr(2)

            'Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            'Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = dia + "/" + mes + "/" + ano


            Return dt_final

        Catch ex As Exception

            Return ""

        End Try

    End Function
    'Trata a data para o seguinte formato dd/mm/aaaa 
    Public Shared Function trataDataDB(ByVal data As String) As String

        Try

            Dim dt_arr() As String = data.Split("/")
            Dim dt_final As String

            'Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim dia As String = dt_arr(0).PadLeft(2, "0")
            'Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim mes As String = dt_arr(1).PadLeft(2, "0")
            'Dim ano As String = CType(data, DateTime).Year
            Dim ano As String = dt_arr(2)

            'Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            'Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = ano + mes + dia


            Return dt_final

        Catch ex As Exception

            Return ""

        End Try

    End Function
    Public Shared Function MostraColunaCapital() As Boolean

        Dim mostraCapital As Boolean = True

        Dim capitalSegurado As String = getCapitalSeguradoSession()

        Select Case capitalSegurado
            Case "Capital Global"
                mostraCapital = False
                '    Case "M�ltiplo sal�rio"                
                '        mostraCapital = True 
                '    Case "Capital fixo"
                '        mostraCapital = True
                '    Case "Capital informado"
                '        mostraCapital = True
                '    Case Else
                '        mostraCapital = True
        End Select

        Return mostraCapital

    End Function


    Public Shared Function MostraColunaSalario() As Boolean

        Dim mostraSalario As Boolean = True

        Dim capitalSegurado As String = getCapitalSeguradoSession()

        Select Case capitalSegurado
            Case "Capital fixo"
                mostraSalario = False
            Case "Capital Global"
                mostraSalario = False
            Case "Capital informado"
                mostraSalario = False
            Case Else
                mostraSalario = True
        End Select

        Return mostraSalario

    End Function
    Public Shared Function getCapitalSeguradoSession() As String

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5706_SPS(Web.HttpContext.Current.Session("apolice"), Web.HttpContext.Current.Session("ramo"), 6785, 0, Web.HttpContext.Current.Session("subgrupo_id"))

        Dim capitalSegurado As String = ""

        Try
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR

            Dim count As Int16 = 1
            Dim text As String
            Dim value As String
            Dim temp As String

            dr.Read()

            capitalSegurado = dr.GetValue(1).ToString()

            dr.Close()
            dr = Nothing

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return capitalSegurado

    End Function

    Public Shared Function getParametros(ByVal p_sSiglaSistema As String, ByVal p_sSecao As String, ByVal p_sCampo As String, ByVal p_sAmbiente As String) As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.obter_parametros_sps(p_sSiglaSistema, p_sSecao, p_sCampo, p_sAmbiente)

        Try
            Dim v_oDtResultado As Data.DataTable = bd.ExecutaSQL_DT()

            If v_oDtResultado.Rows.Count > 0 Then
                Return v_oDtResultado.Rows(0)("Valor")
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Function

End Class
