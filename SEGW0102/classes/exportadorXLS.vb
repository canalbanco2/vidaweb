Public Class ExportadorXLS
    Public Shared Sub Exportar(ByVal dt As System.Data.DataTable)
        Dim g As New Guid
        Dim oResponse As System.Web.HttpResponse _
         = System.Web.HttpContext.Current.Response
        oResponse.Clear()
        oResponse.ContentEncoding = System.Text.Encoding.GetEncoding("iso-8859-1")
        oResponse.AddHeader("Content-Type", "application/xls charset=iso-8859-1")
        oResponse.AddHeader("Pragma", "public")
        oResponse.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
        oResponse.AddHeader("Content-Disposition", "attachment; filename=Export.xls")
        'oResponse.ContentType = "application/vnd.ms-excel"
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
        
        Dim dg As New System.Web.UI.WebControls.DataGrid
        dg.DataSource = dt
        dg.DataBind()
        dg.RenderControl(htmlWrite)
        oResponse.Write(stringWrite.ToString)
        'oResponse.End()
        Exit Sub
    End Sub
End Class

