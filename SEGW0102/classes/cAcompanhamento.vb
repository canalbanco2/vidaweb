Public Class cAcompanhamento

#Region "Heran�a"
    Inherits cBanco
#End Region

    Public Sub SEGS7561_SPS(ByVal p_iApoliceId As Integer, _
                            ByVal p_iRamoId As Integer, _
                            ByVal p_iSubGrupo As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS7561_SPS "
        SQL &= " @apolice_id = " & p_iApoliceId
        SQL &= ", @ramo_id = " & p_iRamoId
        SQL &= ", @subgrupo_id = " & p_iSubGrupo

    End Sub

    Public Sub SEGS7599_SPS(ByVal p_iTipoDocVidaWebId As Integer)
        SQL = "exec VIDA_WEB_DB..SEGS7599_SPS "
        SQL &= " @tipo_doc_vida_web_id = " & p_iTipoDocVidaWebId
    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String, ByVal id As String, ByVal tipo As String)

        SQL = "exec VIDA_WEB_DB..SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @cpf = " & cpf
        SQL &= ", @nome = " & nome
        SQL &= ", @id = " & id
        SQL &= ", @tipo = " & tipo

    End Sub

    Public Sub SEGS5744_SPS(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As String)
        SQL = "exec VIDA_WEB_DB..SEGS5744_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @cod_susep = 6785"
        SQL &= ", @seguradora_id = 0"
        SQL &= ", @subgrupo_id = " & subgrupo_id
        SQL &= ", @usuario = ''"
        SQL &= ", @ind_acesso = ''"
        SQL &= ", @cpf_id = ''"
    End Sub

    Public Sub SEGS6570_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal sUpload_Dps As String)

        SQL = "exec VIDA_WEB_DB..SEGS6570_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @upload_dps = " & sUpload_Dps

    End Sub

    Public Sub SEGS5753_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String, ByVal id As String, ByVal tipo As String, ByVal excell As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5753_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @cpf = " & cpf
        SQL &= ", @nome = " & nome
        SQL &= ", @id = " & id
        SQL &= ", @tipo = " & tipo
        SQL &= ", @excell = 1"

    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String)

        SQL = "exec VIDA_WEB_DB..SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @cpf = " & cpf
        SQL &= ", @nome = " & nome

    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String, ByVal id As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @cpf = " & cpf
        SQL &= ", @nome = " & nome
        SQL &= ", @id = " & id
    End Sub

    Public Sub SEGS5653_SPI(ByVal wf_id As Integer)

        SQL = "exec [sisab003].seguros_db..SEGS5653_SPI @wf_id = " & wf_id

    End Sub


    Public Sub SEGS5393_SPI(ByVal wf_id As Integer, ByVal tp_wf_id As Integer, ByVal tp_tarefa_id As Integer, ByVal tp_ativ_id As Integer)

        SQL = "exec [sisab003].seguros_db..SEGS5393_SPI "
        SQL &= " @wf_id = " & wf_id
        SQL &= ", @tp_wf_id = " & tp_wf_id
        SQL &= ", @tp_tarefa_id = " & tp_tarefa_id
        SQL &= ", @tp_ativ_id = " & tp_ativ_id

    End Sub

    Public Sub SEGS5699_SPI(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal wf_id As String, ByVal dt_inicio_competencia As String, ByVal dt_fim_competencia As String, ByVal usuario As String)

        SQL = "exec VIDA_WEB_DB..SEGS5699_SPI "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @seguradora_cod_susep = 6785"
        SQL &= ", @sucursal_seguradora_id = 0"
        SQL &= ", @wf_id = " & wf_id
        SQL &= ", @dt_inicio_competencia = '" & dt_inicio_competencia & "'"
        SQL &= ", @dt_fim_competencia = '" & dt_fim_competencia & "'"
        SQL &= ", @desc_historico = 'Realizado o Encerramento da Fatura'"
        SQL &= ", @aplicacao = 'SEGW0069'"
        SQL &= ", @usuario = " & usuario



    End Sub

    Public Sub executar_atividade_lote_spu()
        SQL = " exec [sisab003].workflow_db.dbo.executar_atividade_lote_spu "
    End Sub

    '''Consulta as condi��es do subgrupo
    Public Sub SEGS5705_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5705_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub

    '''Consulta as condi��es do subgrupo
    Public Sub SEGS5706_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5706_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub

    '''Consulta os dados b�sicos da ap�lice
    Public Sub dados_basico_apolice_sps(ByVal apolice_id As Int16, ByVal ramo_id As Int16, ByVal cod_susep As Int16, ByVal seguradora_id As Int16)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5660_SPS "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @seguradora_cod_susep=" & trInt(cod_susep)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @sucursal_seguradora_id=" & seguradora_id

    End Sub

    '''Consulta os log dos �ltimos 10 acessos
    Public Sub segs5644_sps(ByVal usuario As String)

        SQL = "exec VIDA_WEB_DB..segs5644_sps '" & usuario & "'"

    End Sub

    Public Sub SGSS0782_SPI(ByVal sigla_recurso As String, ByVal sigla_sistema As String, ByVal chave As String, ByVal de As String, ByVal para As String, ByVal assunto As String, ByVal mensagem As String, ByVal cc As String, ByVal anexo As String, ByVal formato As String, ByVal id As String)

        SQL = " exec email_db..SGSS0782_SPI "
        SQL &= " @sigla_recurso ='" & sigla_recurso
        SQL &= "', @sigla_sistema ='" & sigla_sistema
        SQL &= "', @chave ='" & chave
        SQL &= "', @de ='" & de
        SQL &= "', @para ='" & para
        SQL &= "', @assunto ='" & assunto
        SQL &= "', @mensagem ='" & mensagem
        SQL &= "', @cc ='" & cc
        SQL &= "', @anexo ='" & anexo
        SQL &= "', @formato ='" & formato
        SQL &= "', @usuario ='" & id & "'"

    End Sub
    'Busca o Resumo da Fatura atual (anterior) da tela de abertura do Vida
    Public Sub ResumoFaturaAtual_Anterior(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal usuario As String, ByVal wf_id As Int64)

        'Busca Resumo da Fatura Atual Anterior
        SQL = "exec VIDA_WEB_DB..SEGS5732_SPS "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @subgrupo_id=" & subgrupo_id
        SQL &= ", @usuario=" & trStr(usuario)
        SQL &= ", @wf_id = " & wf_id

    End Sub

    'Busca o Resumo da Fatura atual (Inclusoes, Altera��es e Exclusoes) da tela de abertura do Vida
    Public Sub ResumoFaturaAtual_IAE(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal usuario As String, ByVal wf_id As Int64)

        'Busca Resumo da Fatura Atual: IAE
        SQL = "exec VIDA_WEB_DB..SEGS5733_SPS "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @subgrupo_id=" & subgrupo_id
        SQL &= ", @usuario=" & trStr(usuario)
        SQL &= ", @wf_id = " & wf_id

    End Sub

    '''Insere um novo registro no log de acesso
    Public Sub TEMPinsereUltimoAcesso(ByVal apolice_id As Integer, ByVal subgrupo_id As Integer, ByVal ramo_id As Integer, ByVal usuario As String)

        SQL = "exec VIDA_WEB_DB..SEGS5665_SPI "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @subgrupo_id=" & trInt(subgrupo_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @usuario=" & trStr(usuario)
        SQL &= ", @seguradora_cod_susep=6785"
        SQL &= ", @sucursal_seguradora_id=0"

    End Sub

    '''Consulta as ap�lices e ramos dispon�veis para o usu�rio logado
    Public Sub consultar_apolice_ramos_sps(ByVal id_usuario As String)

        SQL = "exec VIDA_WEB_DB..segs5659_sps "
        SQL &= "@cpf=" & id_usuario



    End Sub

    '''Consulta os subgrupos da ap�lice de acordo com um CPF
    Public Sub consulta_subgrupo_sps(ByVal apolice_id As Int16, ByVal ramo_id As Int16, ByVal cpf As String)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5661_SPS "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @cpf='" & cpf & "'"

    End Sub

    '''Consulta todos os subgrupos da ap�lice
    Public Sub consulta_subgrupo_sps(ByVal apolice_id As Int16, ByVal ramo_id As Int16)

        SQL = "set nocount on exec web_intranet_db..SEGS5645_SPS "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)

    End Sub


    '''Consulta os usu�rio v�lidos para uma ap�lice-ramo-subgrupo
    Public Sub consultar_usuarios_SPS(ByVal apolice_id As Int16, ByVal ramo_id As Int16, ByVal subgrupo_id As Int16)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5662_SPS "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @subgrupo_id=" & trInt(subgrupo_id)
        SQL &= ", @seguradora_id=0"
        SQL &= ", @cod_susep=6785"

    End Sub

    '''Consulta os usu�rio v�lidos para uma ap�lice-ramo-subgrupo
    Public Sub consultar_usuarios_web_seguros_SPS()

        SQL = "exec segs5712_sps "

    End Sub

    Public Sub consultar_usuarios_web_seguros_SPS(ByVal apolice_id As String, ByVal ramo_id As String)

        SQL = "exec segs5712_sps "
        SQL &= " @apolice = " & apolice_id
        SQL &= ", @ramo = " & ramo_id

    End Sub

    '''Consulta os dados de um usu�rio
    Public Sub consultar_usuarios_SPS(ByVal apolice_id As Int16, ByVal ramo_id As Int16, ByVal subgrupo_id As Int16, ByVal cpf As String)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5662_SPS "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @subgrupo_id=" & trInt(subgrupo_id)
        SQL &= ", @seguradora_id=0"
        SQL &= ", @cod_susep=6785"
        SQL &= ", @cpf=" & cpf

    End Sub

    '''Realiza a atualiza��o dos dados do usu�rio de uma ap�lice, ramo e subgrupo
    Public Sub atualizarUsuario(ByVal apolice_id As Int16, ByVal ramo_id As Int16, ByVal subgrupo_id As Int16 _
                                , ByVal cpf As String, ByVal nome As String, ByVal login As String _
                                , ByVal acesso As String, ByVal situacao As String, ByVal email As String)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5674_SPU "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @subgrupo_id=" & trInt(subgrupo_id)
        SQL &= ", @seguradora_id=0"
        SQL &= ", @cod_susep=6785"
        SQL &= ", @cpf=" & cpf
        SQL &= ", @nome=" & trStr(nome)
        SQL &= ", @login=" & login
        SQL &= ", @ind_acesso=" & acesso
        SQL &= ", @ind_situacao=" & situacao
        SQL &= ", @email=" & trStr(email)
        SQL &= ", @usuario=" & Web.HttpContext.Current.Session("usuario")

    End Sub

    '''Realiza a exclus�o l�gica do usu�rio
    Public Sub excluiUsuario_spd(ByVal apolice_id As Int16, ByVal ramo_id As Int16, ByVal subgrupo_id As Int16, ByVal cpf As String)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5673_SPD "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @subgrupo_id=" & trInt(subgrupo_id)
        SQL &= ", @seguradora_id=0"
        SQL &= ", @cod_susep=6785"
        SQL &= ", @cpf=" & cpf
        SQL &= ", @usuario=" & Web.HttpContext.Current.Session("usuario")

    End Sub

    '''Verifica se o usu�rio j� existe atrav�s de seu CPF
    Public Sub verificaUsuario(ByVal cpf As String)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5663_SPS "
        SQL &= "@cpf=" & cpf

    End Sub

    '''Realiza a inclus�o de um novo usu�rio
    Public Sub incluiUsuario(ByVal apolice_id As Int16, ByVal ramo_id As Int16, ByVal subgrupo_id As Int16 _
                                , ByVal cpf As String, ByVal nome As String, ByVal login As String _
                                , ByVal acesso As String, ByVal situacao As String, ByVal email As String)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5666_SPI "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @subgrupo_id=" & trInt(subgrupo_id)
        SQL &= ", @seguradora_id=0"
        SQL &= ", @cod_susep=6785"
        SQL &= ", @cpf=" & cpf
        SQL &= ", @nome=" & trStr(nome)
        SQL &= ", @login=" & trStr(login)
        SQL &= ", @ind_acesso=" & acesso
        SQL &= ", @ind_situacao=" & situacao
        SQL &= ", @email=" & trStr(email)
        SQL &= ", @usuario=" & Web.HttpContext.Current.Session("usuario")

    End Sub


    '''Consulta o prazo restante
    Public Sub prazo_limite(ByVal apolice_id As Int16, ByVal ramo_id As Int16, ByVal subgrupo_id As Int16, ByVal wf_id As Integer)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5722_SPS "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @sub_grupo_id=" & trInt(subgrupo_id)
        SQL &= ", @tp_wf_id=" & cConstantes.CWORKFLOW
        SQL &= ", @tp_versao_id=" & cConstantes.CVERSAOID
        SQL &= ", @wf_id=" & wf_id

    End Sub

    Public Sub New(Optional ByVal banco As cDadosBasicos.eBanco = cDadosBasicos.eBanco.web_intranet_db)
        MyBase.New(banco)
        Alianca.Seguranca.BancoDados.cCon.BancodeDados = banco.ToString
    End Sub

    Function trInt(ByVal valor As Object) As String

        If valor = 0 Then
            Return "null"
        Else
            Return valor
        End If

    End Function

    Function trStr(ByVal valor As Object) As String

        If valor = "" Then
            Return "null"
        Else
            Return "'" & valor.ToString.Replace("'", "''") & "'"
        End If

    End Function

    Public Sub obter_parametros_sps(ByVal p_sSiglaSistema As String, ByVal p_sSecao As String, ByVal p_sCampo As String, ByVal p_sAmbiente As String)
        SQL = "exec controle_sistema_db..obter_parametros_sps @SIGLA_SISTEMA = '" & p_sSiglaSistema & "'"
        SQL &= ", @SECAO = '" & p_sSecao & "'"
        SQL &= ", @CAMPO = '" & p_sCampo & "'"
        SQL &= ", @AMBIENTE = '" & p_sAmbiente & "'"
    End Sub

    Public Sub SEGS5696_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal workflow As Integer, ByVal valor_id As Integer)
        SQL = "exec VIDA_WEB_DB..segs5696_sps  @apolice_id = " & apolice_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @tp_wf_id = " & workflow
        SQL &= ", @tp_versao_id = " & valor_id
    End Sub

    Public Sub delete_usuario_apolice_spu(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal usuario As String)

        SQL = " exec VIDA_WEB_DB..SEGS5715_SPU @apolice_id = " & apolice_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @cpf = " & cpf
        SQL &= ", @usuario = " & usuario
        SQL &= ", @ind_situacao = I"

    End Sub

    Public Sub insere_usuario_apolice_spi(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal usuario As String)

        SQL = " exec VIDA_WEB_DB..SEGS5713_SPI @apolice_id = " & apolice_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @cpf = " & cpf
        SQL &= ", @usuario = " & usuario

    End Sub

    Public Sub getHistorico_sps(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal usuario As String)

        SQL = " exec VIDA_WEB_DB..segs5711_sps @apolice_id = " & apolice_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @usuario = " & usuario

    End Sub

    Public Sub getEstadoAtividade(ByVal wf_id As String, ByVal tp_ativ As String)

        SQL = " exec VIDA_WEB_DB..SEGS5783_SPS "
        SQL &= "@wf_id = " & wf_id
        SQL &= ", @tp_ativ_id = " & tp_ativ

    End Sub


    '''Consulta as condi��es do subgrupo SEGS5750_SPS
    Public Sub coberturas(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer, ByVal componente As String)

        SQL = "SELECT DISTINCT t.tp_componente_id, "
        SQL &= " c.tp_cobertura_id, "
        SQL &= " c.nome, "
        SQL &= " isnull(e.val_lim_min_is, 0) val_lim_min_is, "
        SQL &= " isnull(e.val_lim_max_is, 0) val_lim_max_is "
        SQL &= " FROM [sisab003].seguros_db..escolha_sub_grp_tp_cob_comp_tb e (nolock)  "
        SQL &= " INNER JOIN [sisab003].seguros_db..tp_cob_comp_tb t (nolock)    "
        SQL &= " ON  t.tp_cob_comp_id = e.tp_cob_comp_id  "
        SQL &= " INNER JOIN [sisab003].seguros_db..tp_cobertura_tb c    (nolock)   "
        SQL &= " ON  c.tp_cobertura_id = t.tp_cobertura_id "
        SQL &= " WHERE(e.apolice_id = " + apolice_id.ToString + ")"
        SQL &= " AND e.sucursal_seguradora_id = " + seguradora_id.ToString
        SQL &= " AND e.seguradora_cod_susep = " + cod_susep.ToString
        SQL &= " AND e.ramo_id = " + ramo_id.ToString
        SQL &= " AND e.sub_grupo_id = " + subgrupo.ToString
        SQL &= " and t.tp_componente_id = " + componente.ToString
        SQL &= " ORDER BY c.nome desc, t.tp_componente_id"

    End Sub
End Class
