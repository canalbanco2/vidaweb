<%@ Page Language="vb" AutoEventWireup="false" Codebehind="LayoutArquivo.aspx.vb" Inherits="segw0081.LayoutArquivo"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>LayoutArquivo</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<LINK href="../segw0060/CSS/EstiloBase.css" type="text/css" rel="stylesheet">
			<script type="text/javascript" src="../segw0060/scripts/overlib421/overlib.js"></script>
			<script>
	function getTpArquivo() {
	    var i=0;
	    for(i=0;i<=2;i++)
	       if(document.getElementById("radioTipo_" + i).checked)
	          var valor = document.getElementById("radioTipo_" + i).value;
	          
		document.getElementById("valorRadio").value = valor;		
	    document.Form1.submit();
	}
	
	function incluirCampo() {
	
		var ddl1 = document.getElementById('ddl1');
		var ddl2 = document.getElementById('ddl2');
		var sel = ddl1.selectedIndex;
		
		
		//alert(ddl2.options[0].value)
		if(ddl2.options[0] && ddl2.options[0].value == -1) {
			ddl2.options[0] = null;
		}
		
		var value = ""
		var text = ""
		if (sel != -1) {
			value = ddl1[sel].value;
			text = ddl1[sel].text;
			
			//if (value == 'Sexo' || value == 'sexo' || value == 'Sexo') {
			if (value == 'sexo') {
				document.getElementById('ttSexo').style.display = 'block';
				document.getElementById('ttSexoFem').style.display = 'block';
				document.getElementById('ttSexoMasc').style.display = 'block';
				document.getElementById('ttSexoFemCampos').style.display = 'block';
				document.getElementById('ttSexoMascCampos').style.display = 'block';
				document.getElementById('td_labelInfo').style.display = 'block';
			}
			
			//ADICIONADO ABAIXO PARA TRATAR COMPONENTE
			if (value == 'tipo_componente') {
				document.getElementById('ttComponente').style.display = 'block';				
				document.getElementById('ttTitular').style.display = 'block';				
				document.getElementById('ttTitularCampos').style.display = 'block';				
				document.getElementById('ttConjuge').style.display = 'block';				
				document.getElementById('ttConjugeCampos').style.display = 'block';
				//document.getElementById('ttExclusao').style.display = 'block';				
				//document.getElementById('ttExclusaoCampos').style.display = 'block';
				document.getElementById('td_labelInfo').style.display = 'block';
				}
			//--------------------------------------------------------------------
			//ADICIONADO ABAIXO PARA TRATAR OPERA��O
			if (value == 'operacao') {
				document.getElementById('ttOperacao').style.display = 'block';				
				document.getElementById('ttInclusao').style.display = 'block';				
				document.getElementById('ttInclusaoCampos').style.display = 'block';				
				document.getElementById('ttAlteracao').style.display = 'block';				
				document.getElementById('ttAlteracaoCampos').style.display = 'block';
				document.getElementById('ttDesligamento').style.display = 'block';				
				document.getElementById('ttDesligamentoCampos').style.display = 'block';
				document.getElementById('td_labelInfo').style.display = 'block';
				}
			//--------------------------------------------------------------------
			
			
			var opcao = new Option(text, value, true, true);
			
			if (!verificaValor(value, ddl2) || value == '--descartar--') {
				ddl2[ddl2.length] = opcao;
				if (value != '--descartar--')
					ddl1[sel] = null;
			}
		}
		return false;
	}
	
	function retirarCampo() {
	
		var ddl1 = document.getElementById('ddl1');
		var ddl2 = document.getElementById('ddl2');
		var sel = ddl2.selectedIndex;
		
		var value = ""
		var text = ""
		if (sel != -1 && ddl2[sel].value != -1) {
			value = ddl2[sel].value;
			text = ddl2[sel].text;
			
			if (value == 'cpf_titular') {
				alert('Esta informa��o � obrigat�ria.');
				return false;
			}
			
			//if (value == 'Sexo' || value == 'Sexo') {
			if (value == 'sexo') {
				document.getElementById('ttSexo').style.display = 'none';				
				document.getElementById('ttSexoMasc').style.display = 'none';				
				document.getElementById('ttSexoFem').style.display = 'none';				
				document.getElementById('ttSexoMascCampos').style.display = 'none';				
				document.getElementById('ttSexoFemCampos').style.display = 'none';
				
				if (document.getElementById('ttComponente').style.display == 'none')
					document.getElementById('td_labelInfo').style.display = 'none';
			}
			
			//ADICIONADO ABAIXO PARA TRATAR COMPONENTE
			if (value == 'tipo_componente') {
				document.getElementById('ttComponente').style.display = 'none';				
				document.getElementById('ttTitular').style.display = 'none';				
				document.getElementById('ttTitularCampos').style.display = 'none';				
				document.getElementById('ttConjuge').style.display = 'none';				
				document.getElementById('ttConjugeCampos').style.display = 'none';
				//document.getElementById('ttExclusao').style.display = 'none';				
				//document.getElementById('ttExclusaoCampos').style.display = 'none';
				document.getElementById('td_labelInfo').style.display = 'none';
				
				if (document.getElementById('ttSexoFemCampos').style.display == 'none')
					document.getElementById('td_labelInfo').style.display = 'none';
				}
			//--------------------------------------------------------------------
			//ADICIONADO ABAIXO PARA TRATAR OPERA��O
			if (value == 'operacao') {
				document.getElementById('ttOperacao').style.display = 'none';				
				document.getElementById('ttInclusao').style.display = 'none';				
				document.getElementById('ttInclusaoCampos').style.display = 'none';				
				document.getElementById('ttAlteracao').style.display = 'none';				
				document.getElementById('ttAlteracaoCampos').style.display = 'none';				
				document.getElementById('ttDesligamento').style.display = 'none';				
				document.getElementById('ttDesligamentoCampos').style.display = 'none';				
				document.getElementById('td_labelInfo').style.display = 'none';				
				}
			//--------------------------------------------------------------------
			
			var opcao = new Option(text, value, true, true);
			
			if (!verificaValor(value, ddl1)) {
				ddl1[ddl1.length] = opcao;
				ddl2[sel] = null;
			} else {
				ddl2[sel] = null;
			}
		}
		
		if(ddl2.options.length == 0) {
			ddl2.options[0] = new Option("--vazio--", "-1", true, true);
		}		
		
		return false;		
	}
	
	function verificaValor(valor, objeto) {
		
		for (cont=0;cont<objeto.options.length;cont++)
		{
			if (objeto.options[cont].value == valor)
				return true;
		}
		return false;
		
	}
	function checkAll() {					
	}
	
	function up() {
		var alvo = document.Form1.ddl2.options.selectedIndex;
		if(alvo > 0) {
			var valor_novo = document.Form1.ddl2.options[alvo].value;			
			var valor_antigo = document.Form1.ddl2.options[alvo - 1].value;			
			document.Form1.ddl2.options[alvo - 1] = new Option(valor_novo, valor_novo);
			document.Form1.ddl2.options[alvo] = new Option(valor_antigo, valor_antigo);
			document.Form1.ddl2.options[alvo - 1].selected = true;			
		}
	}
	function down() {
		var alvo = document.Form1.ddl2.options.selectedIndex;
		if(alvo < document.Form1.ddl2.options.length - 1) {
			var valor_novo = document.Form1.ddl2.options[alvo].value;			
			var valor_antigo = document.Form1.ddl2.options[alvo + 1].value;			
			document.Form1.ddl2.options[alvo + 1] = new Option(valor_novo, valor_novo);
			document.Form1.ddl2.options[alvo] = new Option(valor_antigo, valor_antigo);			
			
			document.Form1.ddl2.options[alvo + 1].selected = true;
		}
	}
	
	function validaCampos() {		
		document.Form1.campos.value = "";
		for (cont=0;cont<document.Form1.ddl2.options.length;cont++) {					
			document.Form1.campos.value += "||" + document.Form1.ddl2.options[cont].value;
		}
		valor = document.Form1.campos.value.toString();
		document.Form1.campos.value = valor.substring(2);
		
		if(document.Form1.txtLayoutArquivo.value == "") {
			alert("Informe o nome do Layout do arquivo.")
			return false;
		}	
		
		if(document.Form1.campos.value == "-1" || document.Form1.campos.value == "") {
			alert("Defina a ordem dos campos do arquivo.")
			return false;
		} 
		
		if(!validaCamposObrig()) {
			return false;
		}
		
		if (document.getElementById('ttSexo').style.display != 'none')
		{
			var TextBox3 = document.getElementById("TextBox3").value;		
			var TextBox2 = document.getElementById("TextBox2").value; 		
		
				
			if(TextBox3 == "" || TextBox2 == "") {
				alert("Informe como est�o as informa��es de sexo no arquivo.")
				return false;
			} 
		}	
		
		
		if (document.getElementById('ttComponente').style.display != 'none')
		{
			var TextBox1 = document.getElementById("TextBox1").value;		
			var TextBox4 = document.getElementById("TextBox4").value; 		
			var TextBox5 = document.getElementById("TextBox5").value; 		
	
			
			if(TextBox1 == "" || TextBox4 == "" || TextBox5 == "") 
			{
				alert("Descreva como est�o as informa��es do tipo de movimenta��o no seu arquivo.")
				return false;
			} 
		}
			
		document.Form1.acao.value = "I";		
		
		//top.exibeaguarde();
		
		return true;		
	}
	
	function mTrocaLabel()
	{
			alert(document.getElementByID('lblSeparador').innerHTML);
	}
	function loader() {
		try {		
			showDiv() 
		} catch (ex) {
		
		}
	}
	
			</script>
</HEAD>
	<body MS_POSITIONING="FlowLayout" onload="loader()">
		<div id="overDiv" style="Z-INDEX:1000; VISIBILITY:hidden; POSITION:absolute"></div>
		<form id="Form1" onsubmit="return validaCampos()" method="post" runat="server">
			<input id="valorRadio" type="hidden" name="valorRadio">
			<P><asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela">Label</asp:label><BR>
				<asp:label id="lblVigencia" runat="server" CssClass="Caminhotela"><br>COTEMINAS S.A - Per�odo de compet�ncia: 01/12/2006 at� 31/12/2006</asp:label></P>
			<P><asp:panel id="pnlCadastroTemplate" runat="server">
<TABLE cellSpacing=0 cellPadding=2 width=600 border=0><!--TR>
							<TD align="left" width="25%" colSpan="3" height="30">
								<asp:Label id="lblTitulo" runat="server" CssClass="Caminhotela" Visible=False>:: Configurar layout do arquivo de vidas</asp:Label></TD>
						</TR-->
  <TR>
    <TD vAlign=top align=left colSpan=2>
<asp:Label id=Label1 runat="server" Font-Size="X-Small">Defina o tipo de arquivo:</asp:Label>&nbsp; 
      &nbsp;&nbsp;&nbsp; 
<asp:RadioButtonList id=radioTipo runat="server" Font-Size="X-Small" Width="170px" RepeatLayout="Flow" RepeatDirection="Horizontal">
									<asp:ListItem Value="Excel">Excel     </asp:ListItem>
									<asp:ListItem Value="CSV" Selected="True">CSV     </asp:ListItem>
									<asp:ListItem Value="TXT">TXT</asp:ListItem>
								</asp:RadioButtonList>
<asp:Image id=Image4 onmouseover="return overlib('[Ajuda]');" onmouseout="return nd();" runat="server" ImageUrl="../segw0060/Images/help.gif"></asp:Image>
<asp:RadioButtonList id=tipoArquivo runat="server" Width="192px" RepeatLayout="Flow" RepeatDirection="Horizontal" Visible="False">
									<asp:ListItem Value="sobre">Sobreposi&#231;&#227;o </asp:ListItem>
									<asp:ListItem Value="incr" Selected="True">Incremental </asp:ListItem>
								</asp:RadioButtonList></TD></TR>
  <TR>
    <TD vAlign=top align=left colSpan=2>
<asp:Panel id=pnlSeparador Visible="true" Runat="server">
<asp:Label id=lblSeparador runat="server" Font-Size="X-Small">Informe o separador dos campos:</asp:Label>&nbsp; 
<asp:TextBox id=txtSeparador runat="server" Width="16px"></asp:TextBox>&nbsp; 
<asp:Image id=Image3 onmouseover="return overlib('[Ajuda]');" onmouseout="return nd();" runat="server" ImageUrl="../segw0060/Images/help.gif"></asp:Image></asp:Panel></TD></TR>
  <TR>
    <TD vAlign=middle align=left colSpan=2>&nbsp;</TD></TR>
  <TR>
    <TD vAlign=middle align=left colSpan=3>
      <TABLE class=Caminhotela cellSpacing=0 cellPadding=2 width="80%" 
        border=0>
        <TR style="FONT-WEIGHT: normal; COLOR: black">
          <TD width=180>Nome layout arquivo: </TD>
          <TD>
<asp:DropDownList id=ddlFormatosArquivo Width="250" Runat="server" EnableViewState="False" AutoPostBack="True">
												<asp:ListItem Value="0">Selecione</asp:ListItem>
											</asp:DropDownList></TD></TR>
        <TR style="FONT-WEIGHT: normal; COLOR: black">
          <TD vAlign=middle align=left width=180>Novo nome layout arquivo: </TD>
          <TD>
<asp:TextBox id=txtLayoutArquivo Width="250" Runat="server" EnableViewState="False"></asp:TextBox></TD></TR></TABLE></TD></TR>
  <TR>
    <TD vAlign=middle align=left colSpan=2>&nbsp;</TD></TR>
  <TR>
    <TD align=left colSpan=2>
<asp:Label id=Label2 runat="server" Font-Size="X-Small">Informe a ordem dos campos do seu arquivo:</asp:Label>&nbsp; 
<asp:Image id=Image5 onmouseover="return overlib('[Ajuda]');" onmouseout="return nd();" runat="server" ImageUrl="../segw0060/Images/help.gif"></asp:Image></TD></TR>
  <TR>
    <TD vAlign=middle colSpan=2 height=171><BR>
      <TABLE id=Table1 height=108 cellSpacing=1 cellPadding=1 width=288 
border=0>
        <TR>
          <TD width=90>
<asp:ListBox id=ddl1 Width="180px" Runat="server" EnableViewState="False" BackColor="Transparent" Height="170px">
</asp:ListBox></TD>
          <TD width=99>
<asp:Button id=btnIncluir runat="server" CssClass="Botao" Width="90px" Text="Incluir ->" CausesValidation="False"></asp:Button><BR>
<asp:Button id=btnRetirar runat="server" CssClass="Botao" Width="90px" Text="<- Retirar" CausesValidation="False"></asp:Button><BR><BR><!--
											<asp:Button id="btnDescartar" runat="server" CssClass="Botao" Width="90px" CausesValidation="False"
												Text="Descartar -"></asp:Button> --></TD>
          <TD>
            <TABLE cellSpacing=0 cellPadding=0 border=0>
              <TR>
                <TD>
<asp:ListBox id=ddl2 Width="180px" Runat="server" EnableViewState="False" BackColor="Transparent" Height="167px">
						<asp:ListItem Value="cpf_titular">cpf_titular</asp:ListItem>
					</asp:ListBox></TD>
                <TD><A onclick=up() href="#"><IMG src="images/imgup.gif" 
                  width=20 border=0></A> <BR><A onclick=down() href="#"><IMG 
                  src="images/imgdown.gif" width=20 border=0></A> 
            </TD></TR></TABLE></TD></TR></TABLE></TD></TR>
  <TR>
    <TD id=td_labelInfo vAlign=top align=left colSpan=2 height=15>
<asp:Label id=Label3 runat="server" Font-Size="X-Small">Descreva como est�o as informa��es abaixo no seu arquivo:</asp:Label>&nbsp; 
<asp:Image id=Image6 onmouseover="return overlib('[Ajuda]');" onmouseout="return nd();" runat="server" ImageUrl="../segw0060/Images/help.gif"></asp:Image></TD></TR>
  <TR>
    <TD vAlign=top align=left width=15 colSpan=2 height=109>
      <TABLE id=Table2 height=66 cellSpacing=1 cellPadding=1 width=512 
        border=0>
        <TR>
          <TD width=57>
            <DIV id=ttSexo><STRONG>Sexo:&nbsp; </STRONG>
<asp:Image id=Image1 onmouseover="return overlib('[Ajuda]');" onmouseout="return nd();" runat="server" ImageUrl="../segw0060/Images/help.gif"></asp:Image></DIV></TD>
          <TD width=159></TD>
          <TD width=193 colSpan=2>
            <DIV id=ttComponente <STRONG><B>Tipo componente:</B>&nbsp; </STRONG>
<asp:Image id=Image2 onmouseover="return overlib('[Ajuda]');" onmouseout="return nd();" runat="server" ImageUrl="../segw0060/Images/help.gif"></asp:Image></DIV></TD>
          <TD width=120 colSpan=3>
            <DIV id=ttOperacao <STRONG><B>Opera��o:</B>&nbsp; </STRONG>
<asp:Image id=Image7 onmouseover="return overlib('[Ajuda]');" onmouseout="return nd();" runat="server" ImageUrl="../segw0060/Images/help.gif"></asp:Image></DIV></TD></TR>
        <TR>
          <TD width=57 height=20>
            <DIV id=ttSexoMasc>masculino:</DIV></TD>
          <TD width=159 height=20>
            <DIV id=ttSexoMascCampos>
<asp:TextBox id=TextBox3 runat="server" Width="80px" EnableViewState="False"></asp:TextBox>
<asp:RequiredFieldValidator id=RequiredFieldValidator2 runat="server" ErrorMessage="*" ControlToValidate="TextBox3"></asp:RequiredFieldValidator></DIV></TD>
          <TD>
            <DIV id=ttTitular>titular.:</DIV></TD>
          <TD width=137>
            <DIV id=ttTitularCampos>
<asp:TextBox id=TextBox1 runat="server" Width="80px" EnableViewState="False"></asp:TextBox></DIV></TD>
          <TD width=76>
            <DIV id=ttInclusao>Inclus�o.:</DIV></TD>
          <TD>
            <DIV id=ttInclusaoCampos>
<asp:TextBox id=Textbox5 runat="server" Width="80px" EnableViewState="False"></asp:TextBox></DIV></TD></TR>
        <TR>
          <TD width=57>
            <DIV id=ttSexoFem>feminino:</DIV></TD>
          <TD width=159>
            <DIV id=ttSexoFemCampos>
<asp:TextBox id=TextBox2 runat="server" Width="80px" EnableViewState="False"></asp:TextBox>
<asp:RequiredFieldValidator id=RequiredFieldValidator3 runat="server" ErrorMessage="*" ControlToValidate="TextBox2"></asp:RequiredFieldValidator></DIV></TD>
          <TD>
            <DIV id=ttConjuge>conjug�.:</DIV></TD>
          <TD width=137>
            <DIV id=ttConjugeCampos>
<asp:TextBox id=TextBox4 runat="server" Width="80px" EnableViewState="False"></asp:TextBox></DIV></TD>
          <TD>
            <DIV id=ttAlteracao>Altera��o.:</DIV></TD>
          <TD width=137>
            <DIV id=ttAlteracaoCampos>
<asp:TextBox id=Textbox6 runat="server" Width="80px" EnableViewState="False"></asp:TextBox></DIV></TD></TR>
        <TR>
          <TD width=57></TD>
          <TD width=159></TD>
          <TD width=159></TD>
          <TD width=159></TD>
          <TD>
            <DIV id=ttDesligamento>Desligamento.:</DIV></TD>
          <TD width=137>
            <DIV id=ttDesligamentoCampos>
<asp:TextBox id=Textbox7 runat="server" Width="80px" EnableViewState="False"></asp:TextBox></DIV></TD></TR></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=center colSpan=2><BR>
<asp:Button id=btnAplicar runat="server" CssClass="Botao" Visible="FALSE" Text="Gravar"></asp:Button><INPUT class=Botao onclick=validaCampos type=submit value=Gravar> 
<asp:Button id=btnFechaPagina runat="server" CssClass="Botao" Width="56px" Text="Fechar" CausesValidation="False"></asp:Button></TD></TR></TABLE>
				</asp:panel></P>
			<input type="hidden" name="campos"> <input type="hidden" name="acao">
		</form>
		<script>
		top.escondeaguarde();
		
		document.getElementById('td_labelInfo').style.display = 'none';				
		document.getElementById('ttSexo').style.display = 'none';				
		document.getElementById('ttSexoMasc').style.display = 'none';				
		document.getElementById('ttSexoFem').style.display = 'none';				
		document.getElementById('ttSexoMascCampos').style.display = 'none';				
		document.getElementById('ttSexoFemCampos').style.display = 'none';
		
		document.getElementById('ttComponente').style.display = 'none';				
		document.getElementById('ttTitular').style.display = 'none';				
		document.getElementById('ttTitularCampos').style.display = 'none';				
		document.getElementById('ttConjuge').style.display = 'none';				
		document.getElementById('ttConjugeCampos').style.display = 'none';
		
		document.getElementById('ttOperacao').style.display = 'none';				
		document.getElementById('ttInclusao').style.display = 'none';				
		document.getElementById('ttInclusaoCampos').style.display = 'none';				
		document.getElementById('ttAlteracao').style.display = 'none';				
		document.getElementById('ttAlteracaoCampos').style.display = 'none';				
		document.getElementById('ttDesligamento').style.display = 'none';				
		document.getElementById('ttDesligamentoCampos').style.display = 'none';				
		document.getElementById('td_labelInfo').style.display = 'none';				
		
		</script>
	</body>
</HTML>
