<%@ Page Language="vb" AutoEventWireup="false" Codebehind="LayoutArquivo.aspx.vb" Inherits="segw0081.LayoutArquivo" Validaterequest="false" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>LayoutArquivo</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name=GENERATOR>
		<meta content="Visual Basic .NET 7.1" name=CODE_LANGUAGE>
		<meta content=JavaScript name=vs_defaultClientScript>
		<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema>
		<LINK href="../segw0060/CSS/EstiloBase.css" type=text/css rel=stylesheet >
		<script src="../segw0060/scripts/overlib421/overlib.js" type=text/javascript></script>
		<script src="../segw0060/scripts/script.js" type=text/javascript></script>
<script>
	
	function OcultaDiv()
	{
	//[new code 01.06.07]- 
	//document.getElementById('painelTamCampo').style.display = 'none';	
	/*
	NOTA: A linha acima foi removida (ap�s ser adicionada) porque vimos que n�o seria poss�vel
	editar o tamanhos dos campos, pois os mesmos sumiriam.
	*/
	//[end code 01.06.07]
	}
	
	function AjustaLayout(opcao)
	{
		
		if (opcao == 'Excel') 
		{
		document.getElementById('label2').innerHTML = 'Informe o conte�do das colunas no arquivo Excel.';
		document.getElementById('pnlSeparador').style.display = 'none';
		document.getElementById('txtSeparador').value='';
		document.getElementById('pnlSubgrupo').style.display = 'block';
		}
		
		if (opcao == 'CSV')
		{
		document.getElementById('label2').innerHTML = 'Informe a posi��o dos campos no arquivo CSV.';
		document.getElementById('pnlSeparador').style.display = 'block';
		document.getElementById('pnlSubgrupo').style.display = 'none';
		}
		
		if (opcao == 'TXT')
		{
		document.getElementById('label2').innerHTML = 'Informe a posi��o dos campos no arquivo TXT.';
		document.getElementById('pnlSeparador').style.display = 'none';
		document.getElementById('txtSeparador').value='';
		document.getElementById('pnlSubgrupo').style.display = 'none';
		}
	
	}
	
	function ValidaNomeLayout()
	{
			for(i=0; i<document.Form1.ddlFormatosArquivo.options.length; i++) 
			{
			if(document.Form1.ddlFormatosArquivo.options[i].text.toUpperCase() == document.Form1.txtLayoutArquivo.value.toUpperCase()) 
				{
				alert('O novo nome de Layout de Arquivo escolhido j� existe.');
				return false;
				}
			}
		//alert('Item n�o encontrado. Pode cadastrar!');
	}
	
	function getTpArquivo() {
	    var i=0;
	    for(i=0;i<=2;i++)
	       if(document.getElementById("radioTipo_" + i).checked)
	          var valor = document.getElementById("radioTipo_" + i).value;
	          
		document.getElementById("valorRadio").value = valor;		
	    
	    //document.Form1.submit(); //Removido para evitar o post
	    
	    //[new code 01.06.07]- Objetivo: ocultar painel

   		    AjustaLayout(valor);
   		
		    if (document.getElementById("valorRadio").value != "" )
			document.getElementById('painelTamCampo').style.display = 'none';	
		    
		//[end code]
		 //if (document.getElementById("valorRadio").value = "Excel" )
		//	document.getElementById('pnlSubgrupo').style.display = 'none';
			
		// if (document.getElementById("valorRadio").value != "Excel" )
		//	document.getElementById('pnlSubgrupo').style.display = 'block';
		    
		    
	}

	function Data_Inclusao_Seguro(flag){ //Alterado o nome da fun��o "DataRisco" para "Data_Inclusao_Seguro"

			var ddl1 = document.getElementById('ddl1');
			var ddl2 = document.getElementById('ddl2');
			var sel = "Data de Inclus�o no Seguro"

			var o = false;
			var d = false;			
			
			if(document.getElementById("tipoArquivo_1").checked != true){
				for(i=0;i<ddl2.options.length;i++)
					if (ddl2.options[i].value == sel){
						ddl2.options[i] = null;
					}	
			}

			for(i=0;i<ddl1.options.length;i++)
				if (ddl1.options[i].value == sel)
					ddl1.options[i] = null;
				
			if(flag == 2){		
				for(i=0;i<ddl1.options.length;i++)
					if (ddl1.options[i].value == sel) 
						o = true;		
				if(!o){
					ddl1.options[ddl1.options.length] = new Option("Data de Inclus�o no Seguro", "Data de Inclus�o no Seguro");
				}
			}
		
			for(i=0;i<ddl1.options.length;i++)
				if (ddl1.options[i].value == sel){
					for(j=0;j<ddl2.options.length;j++){
						if (ddl2.options[j].value == sel){
							ddl1.options[i] = null;	
						}
					}
				}		
	}

	function pegaTipo(){
		if(document.getElementById("tipoArquivo_0").checked == true){	
			Data_Inclusao_Seguro(1)
			removeData();			
		}
		else{		
			Data_Inclusao_Seguro(2)
			adicionaData();
		}
	}
	function adicionaData() {
		var ddl1 = document.getElementById('ddl1');
		var ddl2 = document.getElementById('ddl2');
		//var sel = "data_desligamento"
		var sel = "DataDesligamento"
		var existe = false;
		
		for(i=0;i<ddl2.options.length;i++)
			if (ddl2.options[i].value == sel)
				existe = true;
		
		for(i=0;i<ddl1.options.length;i++)
			if (ddl1.options[i].value == sel)
				existe = true;
				
		
		if(!existe)
			{
			ddl1.options[ddl1.options.length] = new Option("DataDesligamento",sel);
			ddl1.options[ddl1.options.length] = new Option("Opera��o", "Opera��o");
			
			document.getElementById('ttOperacao').style.display = 'none';				
			document.getElementById('ttInclusao').style.display = 'none';				
			document.getElementById('ttInclusaoCampos').style.display = 'none';				
			document.getElementById('ttAlteracao').style.display = 'none';				
			document.getElementById('ttAlteracaoCampos').style.display = 'none';				
			document.getElementById('ttDesligamento').style.display = 'none';				
			document.getElementById('ttDesligamentoCampos').style.display = 'none';
			}
			
		return false;		
	}
	
	function removeData(){
	
		var ddl1 = document.getElementById('ddl1');
		var ddl2 = document.getElementById('ddl2');
		//var sel = "data_desligamento"
		var sel = "DataDesligamento"
		var  welcome = "Opera��o"
		var  welcomeDif = "operacao"
		
		
		for(i=0;i<ddl2.options.length;i++)
			//alert(ddl2.options[i].value + " == " + sel);
			if (ddl2.options[i].value == sel)
				ddl2.options[i] = null;
				
		for(i=0;i<ddl1.options.length;i++)
			if (ddl1.options[i].value == sel)
				ddl1.options[i] = null;		
				
				//Remove operacao
				for(i=0;i<ddl2.options.length;i++)
					if (ddl2.options[i].value == welcome || ddl2.options[i].value == welcomeDif)
					ddl2.options[i] = null;
				
				for(i=0;i<ddl1.options.length;i++)
					if (ddl1.options[i].value == welcome)
					ddl1.options[i] = null;	
				//-------------------------------------------------
				
			document.getElementById('ttOperacao').style.display = 'none';				
			document.getElementById('ttInclusao').style.display = 'none';				
			document.getElementById('ttInclusaoCampos').style.display = 'none';				
			document.getElementById('ttAlteracao').style.display = 'none';				
			document.getElementById('ttAlteracaoCampos').style.display = 'none';				
			document.getElementById('ttDesligamento').style.display = 'none';				
			document.getElementById('ttDesligamentoCampos').style.display = 'none';	

		return false;		
	}
	
	function incluirCampo() {
	
		var ddl1 = document.getElementById('ddl1');
		var ddl2 = document.getElementById('ddl2');
		var sel = ddl1.selectedIndex;
		
		
		//alert(ddl2.options[0].value)
		if(ddl2.options[0] && ddl2.options[0].value == -1) {
			ddl2.options[0] = null;
		}
		
		var value = ""
		var text = ""
		if (sel != -1) {
			value = ddl1[sel].value;
			text = ddl1[sel].text;
			
			//if (value == 'Sexo' || value == 'sexo' || value == 'Sexo') {
			if (value == 'Sexo') {
				document.getElementById('ttSexo').style.display = 'block';
				document.getElementById('ttSexoFem').style.display = 'block';
				document.getElementById('ttSexoMasc').style.display = 'block';
				document.getElementById('ttSexoFemCampos').style.display = 'block';
				document.getElementById('ttSexoMascCampos').style.display = 'block';
				document.getElementById('td_labelInfo').style.display = 'block';
			}
			
			//if (value == 'Sexo' || value == 'sexo' || value == 'Sexo') {
			if (value == 'DataDesligamento' || value == 'Data de Nascimento' || value == 'Data de Inclus�o no Seguro') {
				document.getElementById('painelFormatoCamposData').style.display = 'block';
			}
			
			//ADICIONADO ABAIXO PARA TRATAR COMPONENTE
			if (value == 'TipoComponente') {
				document.getElementById('ttComponente').style.display = 'block';				
				document.getElementById('ttTitular').style.display = 'block';				
				document.getElementById('ttTitularCampos').style.display = 'block';				
				document.getElementById('ttConjuge').style.display = 'block';				
				document.getElementById('ttConjugeCampos').style.display = 'block';
				//document.getElementById('ttExclusao').style.display = 'block';				
				//document.getElementById('ttExclusaoCampos').style.display = 'block';
				document.getElementById('td_labelInfo').style.display = 'block';
				}
			//--------------------------------------------------------------------
			//ADICIONADO ABAIXO PARA TRATAR OPERA��O
			
			if (value == 'Opera��o' || value == 'operacao') {
				document.getElementById('ttOperacao').style.display = 'block';				
				document.getElementById('ttInclusao').style.display = 'block';				
				document.getElementById('ttInclusaoCampos').style.display = 'block';				
				document.getElementById('ttAlteracao').style.display = 'block';				
				document.getElementById('ttAlteracaoCampos').style.display = 'block';
				document.getElementById('ttDesligamento').style.display = 'block';				
				document.getElementById('ttDesligamentoCampos').style.display = 'block';
				document.getElementById('td_labelInfo').style.display = 'block';
				}
			//--------------------------------------------------------------------
			
			
			var opcao = new Option(text, value, true, true);
								
			if (!verificaValor(value, ddl2) || value == '--descartar--') {
				ddl2[ddl2.length] = opcao;
				if (value != '--descartar--')
					ddl1[sel] = null;									
			}
			
			if(document.getElementById("radioTipo_2").checked  == true) {
				mostraTamanhoCampo(text);
			}
		}
		else
		{
			alert('Selecione um dos campos dispon�veis para inclui-lo.');
		}
		return false;
	}
	
	function mostraTamanhoCampo(texto) {
		if(document.getElementById("radioTipo_2").checked  == true) {						
			
			//[new code 01.06.07]-Mostra a divTamanhoCampo
			document.getElementById('painelTamCampo').style.display = 'block';	
			document.getElementById('pnlTamCampo').style.display = 'block';
			//[end code 01.06.07]
			
			document.getElementById("txtTamanhoCampo").innerHTML = '"' + texto + '"';		
			
			if(texto.toString().indexOf("--descartar--") > 0 ) {
				document.getElementById("txtTamanhoCampo").innerHTML = "--descartar--";	
			}
			
			if(texto == "--descartar--") {	
				var ddl2 = document.getElementById('ddl2');			
				var z = 0;				
				for(i=0;i<ddl2.length;i++) {
					if(ddl2.options[i].text && ddl2.options[i].text == "--descartar--") {							
						ddl2.options[i].value = z + "--descartar--";
						
						z++;
					}	
				}
				try {
					texto = ddl2.options[ddl2.options.selectedindex].value;
				} catch(e) {
					texto = ddl2.options[ddl2.length - 1].value;
				}
			}
			
			if(document.getElementById("tam_" + texto)) {
				document.getElementById("tamAtual").value = document.getElementById("tam_" + texto).value;			
			} else {
				document.getElementById("tamAtual").value = "";
				document.getElementById("tamanhoCampos").innerHTML += "<input type='text' value='' name='tam_" + texto + "' id='tam_" + texto + "'>";
			}		
		}		
	}
	function mudaTamanhoCampo(valor) {		
		var campo = document.Form1.ddl2.options[document.Form1.ddl2.options.selectedIndex].value;
		
		document.getElementById("tam_" + campo).value = valor;
	}
	
	function retirarCampo() {
	
		var ddl1 = document.getElementById('ddl1');
		var ddl2 = document.getElementById('ddl2');
		var sel = ddl2.selectedIndex;
		
		var value = ""
		var text = ""
		if (sel != -1)
		{
			if (sel != -1 && ddl2[sel].value != -1) {
				value = ddl2[sel].value;
				text = ddl2[sel].text;
				
				if (value == 'CPF do Titular') {
					//alert('Esta informa��o � obrigat�ria.');
					//return false;
				}
				
				//if (value == 'Sexo' || value == 'Sexo') {
				if (value == 'Sexo') {
					document.getElementById('ttSexo').style.display = 'none';				
					document.getElementById('ttSexoMasc').style.display = 'none';				
					document.getElementById('ttSexoFem').style.display = 'none';				
					document.getElementById('ttSexoMascCampos').style.display = 'none';				
					document.getElementById('ttSexoFemCampos').style.display = 'none';
					
					var esconde = 0;
					if (document.getElementById('ttComponente').style.display == 'none')
						esconde = parseInt(esconde) + 1;
					
					if (document.getElementById('ttOperacao').style.display == 'none')
						esconde = parseInt(esconde) + 1;
						
					if (esconde == 2)
						document.getElementById('td_labelInfo').style.display = 'none';
				}
				
				//ADICIONADO ABAIXO PARA TRATAR COMPONENTE
				if (value == 'TipoComponente') {
					document.getElementById('ttComponente').style.display = 'none';				
					document.getElementById('ttTitular').style.display = 'none';				
					document.getElementById('ttTitularCampos').style.display = 'none';				
					document.getElementById('ttConjuge').style.display = 'none';				
					document.getElementById('ttConjugeCampos').style.display = 'none';
					//document.getElementById('ttExclusao').style.display = 'none';				
					//document.getElementById('ttExclusaoCampos').style.display = 'none';
										
					//if (document.getElementById('ttSexoFemCampos').style.display != 'block' && document.getElementById('ttOperacao').style.display != 'block')
					//document.getElementById('td_labelInfo').style.display = 'none';
					
					var esconde = 0;
					if (document.getElementById('ttSexo').style.display == 'none')
						esconde = parseInt(esconde) + 1;
					
					if (document.getElementById('ttOperacao').style.display == 'none')
						esconde = parseInt(esconde) + 1;
					
					if (esconde == 2)
						document.getElementById('td_labelInfo').style.display = 'none';
						
					}
			var CamposDatas=0;
		
			for(i=0;i < document.getElementById('ddl2').options.length;i++){
				if (document.getElementById('ddl2').options[i].value == "DataDesligamento" || document.getElementById('ddl2').options[i].value == "Data de Nascimento"  || document.getElementById('ddl2').options[i].value == "Data de Inclus�o no Seguro")
					CamposDatas=CamposDatas+1;
			}

					
				if (CamposDatas <=1 ) {
					document.getElementById('painelFormatoCamposData').style.display = 'none';
				}
				
					
				//--------------------------------------------------------------------
				//ADICIONADO ABAIXO PARA TRATAR OPERA��O
				if (value == 'Opera��o' || value == 'operacao'){
					document.getElementById('ttOperacao').style.display = 'none';				
					document.getElementById('ttInclusao').style.display = 'none';				
					document.getElementById('ttInclusaoCampos').style.display = 'none';				
					document.getElementById('ttAlteracao').style.display = 'none';				
					document.getElementById('ttAlteracaoCampos').style.display = 'none';				
					document.getElementById('ttDesligamento').style.display = 'none';				
					document.getElementById('ttDesligamentoCampos').style.display = 'none';				
										
					var esconde = 0;
					if (document.getElementById('ttSexo').style.display == 'none')
						esconde = parseInt(esconde) + 1;
					
					if (document.getElementById('ttComponente').style.display == 'none')
						esconde = parseInt(esconde) + 1;
						
					if (esconde == 2)
						document.getElementById('td_labelInfo').style.display = 'none';
					
					}
				//--------------------------------------------------------------------
				
				var opcao = new Option(text, value, true, true);
				
				if (!verificaValor(value, ddl1)) {
					if(text.indexOf('descartar') <= 0)
						ddl1[ddl1.length] = opcao;
						
					ddl2[sel] = null;
				} else {
					ddl2[sel] = null;
				}
			}
		}
		else
		{
			alert('Selecione um dos campos da lista de selecionados para retir�-lo.');
		}
		
		if(ddl2.options.length == 0) {
			ddl2.options[0] = new Option("--vazio--", "-1", true, true);
		}		
		
		return false;		
	}
	
	function verificaValor(valor, objeto) {
		
		for (cont=0;cont<objeto.options.length;cont++)
		{
			if (objeto.options[cont].value == valor)
				return true;
		}
		return false;
		
	}
	function checkAll() {					
	}
	
	function up() {
		var alvo = document.Form1.ddl2.options.selectedIndex;
		if(alvo > 0) {
			var valor_novo = document.Form1.ddl2.options[alvo].value;			
			var texto_novo = document.Form1.ddl2.options[alvo].text;	
			var valor_antigo = document.Form1.ddl2.options[alvo - 1].value;			
			var texto_antigo = document.Form1.ddl2.options[alvo - 1].text;			
			document.Form1.ddl2.options[alvo - 1] = new Option(texto_novo, valor_novo);
			document.Form1.ddl2.options[alvo] = new Option(texto_antigo, valor_antigo);
			document.Form1.ddl2.options[alvo - 1].selected = true;			
		}
	}
	function down() {
		
		var alvo = document.Form1.ddl2.options.selectedIndex;
		
		if(alvo < document.Form1.ddl2.options.length - 1 && alvo != -1) {
			var valor_novo = document.Form1.ddl2.options[alvo].value;			
			var texto_novo = document.Form1.ddl2.options[alvo].text;		
			var valor_antigo = document.Form1.ddl2.options[alvo + 1].value;			
			var texto_antigo = document.Form1.ddl2.options[alvo + 1].value;	
			document.Form1.ddl2.options[alvo + 1] = new Option(texto_novo, valor_novo);
			document.Form1.ddl2.options[alvo] = new Option(texto_antigo, valor_antigo);			
			document.Form1.ddl2.options[alvo + 1].selected = true;
		}
		
	}
	
	function validaCampos() {		
		//[New code 01/06/07]
		if (ValidaNomeLayout() == false)
			return false;
		//[End code 01/06/07]
				
		document.Form1.campos.value = "";
		var atual;
		
		if (document.Form1.ddl2.selectedIndex != -1)
		{
			if(document.getElementById("radioTipo_2").checked  == true) {
				atual = document.Form1.ddl2.options[document.Form1.ddl2.selectedIndex].value;
				
				if(!parseInt(document.getElementById("tam_" + atual).value)) {
					if(atual.toString().indexOf("--descartar--") > 0 ) {
						atual = "--descartar--";	
					}
						
					alert('Informe o tamanho do campo "' + atual + '"');
					return false;
				}
			}
		}
			
		for (cont=0;cont<document.Form1.ddl2.options.length;cont++) {					
			
			if(document.getElementById("radioTipo_2").checked  == true) {				
				var valor = document.Form1.ddl2.options[cont].value;
				var text = document.Form1.ddl2.options[cont].text;
				//alert(valor);	
				try {					
					if(valor && valor != "") {			
						
						if (document.getElementById("tam_" + valor))
							document.Form1.campos.value += "||" + valor + "#" + document.getElementById("tam_" + valor).value;
						
						if(!parseInt(document.getElementById("tam_" + valor).value)) {
							
							if(valor.toString().indexOf("--descartar--") > 0 || text.toString().indexOf("--descartar--") > 0) {
								valor = "--descartar--";	
							}
								
							alert('Informe o tamanho do campo "' + valor + '"');
							return false;
						}
					}
				} catch(ex) {
					//alert(ex.message);
					if(valor.toString().indexOf("--descartar--") > 0 ) {
						valor = "--descartar--";	
					}
					if(valor && valor != "") {
						alert('Informe o tamanho do campo "' + valor + '"');
						return false;					
					}
				}
			} else {
				document.Form1.campos.value += "||" + document.Form1.ddl2.options[cont].value;
			}
			
		}
		
				
		valor = document.Form1.campos.value.toString();
		document.Form1.campos.value = valor.substring(2);
		
		if(document.Form1.txtLayoutArquivo.value == "") {
			alert("Informe o nome do Layout do arquivo.")
			return false;
		}	
		
		
		if(document.Form1.campos.value == "-1" || document.Form1.campos.value == "") {
			alert("Defina a ordem dos campos do arquivo.")
			return false;
		} 
		
		if(!validaCamposObrig()) {
			return false;
		}

			var NSegurado =0;
			var DNascimento =0;
			var CTitular =0;
			var VSexo =0;
			
			for(i=0;i < document.getElementById('ddl2').options.length;i++){
				if (document.getElementById('ddl2').options[i].value == "Nome do Segurado")
					NSegurado=1;
				if (document.getElementById('ddl2').options[i].value == "Data de Nascimento")
					DNascimento=1;
				if (document.getElementById('ddl2').options[i].value == "CPF do Titular")
					CTitular=1;
				if (document.getElementById('ddl2').options[i].value == "Sexo")
					VSexo=1;
			}
			if (NSegurado==0)
				{
					alert("O campo 'Nome do Segurado' � obrigat�rio"); 
					return false;
				}
			if (DNascimento==0)
				{
					alert("O campo 'Data de Nascimento' � obrigat�rio");
					return false;
				}
			if (CTitular==0)
				{
					alert("O campo 'CPF do Titular' � obrigat�rio"); 
					return false;
				}
			if (VSexo==0)
				{
					alert("O campo 'Sexo' � obrigat�rio"); 
					return false;
				}
				
				
			NSegurado =0;
			DNascimento =0;
			for(i=0;i < document.getElementById('ddl1').options.length;i++){
				if (document.getElementById('ddl1').options[i].value == "Valor do Sal�rio")
					NSegurado=1;
				if (document.getElementById('ddl1').options[i].value == "Capital Segurado")
					DNascimento=1;
			}
			if (NSegurado==1)
				{
					alert("O campo 'Valor do Sal�rio' � obrigat�rio"); 
					return false;
				}
			if (DNascimento==1)
				{
					alert("O campo 'Capital Segurado' � obrigat�rio");
					return false;
				}
					
		//Pog: vamos obrigar a selecionar opera��o somente para Sobreposi��o
			if(document.getElementById("tipoArquivo_0").checked == false)			
				{
				var selecionou = false
				var alvo = "Opera��o"
				var alvo2 = "operacao"
				var ddl2 = document.getElementById('ddl2');
				
					for(i=0;i<ddl2.options.length;i++)
						if (ddl2.options[i].value == alvo ||ddl2.options[i].value == alvo2)
						selecionou = true;
		
				if(!selecionou)
					{
					alert("O campo 'opera��o' � obrigat�rio");
					return false;
					}
				}
		
		//-----------------------------------------------
		
		if (document.getElementById('ttSexo').style.display != 'none')
		{
			var TextBox3 = document.getElementById("TextBox3").value;		
			var TextBox2 = document.getElementById("TextBox2").value; 		
		
				
			if(TextBox3 == "" || TextBox2 == "") {
				alert('Digite como est� definida a informa��o do "sexo" no layout do seu arquivo.')
				return false;
			} 
		}	
		
		
		if (document.getElementById('ttOperacao').style.display != 'none')
		{
			var TextBox5 = document.getElementById("TextBox5").value;		
			var TextBox6 = document.getElementById("TextBox6").value; 		
			var TextBox7 = document.getElementById("TextBox7").value; 		
	
			
			if(TextBox5 == "" || TextBox6 == "" || TextBox7 == "") 
			{
				alert('Digite como est� definida a informa��o do "Tipo de movimenta��o" no layout do seu arquivo.')
				return false;
			} 
		}
		
		if (document.getElementById('ttComponente').style.display != 'none')
		{
			var TextBox1 = document.getElementById("TextBox1").value;		
			var TextBox4 = document.getElementById("TextBox4").value; 		
			
			if (document.getElementById("pogao_vitao").value != "")
				{
					if(TextBox1 == "" || TextBox4 == "") 
					{
					alert('Digite como est� definida a informa��o do "TipoComponente" no layout do seu arquivo.')
					return false;
					} 
				}
			else
				{
					if(TextBox1 == "") 
					{
					alert('Digite como est� definida a informa��o do "TipoComponente" no layout do seu arquivo.')
					return false;
					} 
				}
				
			
		}
			
		document.Form1.acao.value = "I";		
		document.Form1.pog.value = "";
		
		//top.exibeaguarde();
		
		return true;		
	}
	
	function mTrocaLabel()
	{
			alert(document.getElementByID('lblSeparador').innerHTML);
	}
	function loader() {
		try {		
			showDiv() 
		} catch (ex) {
		
		}
	}

		</script>
</HEAD>
<body onload=loader() MS_POSITIONING="FlowLayout">
<div id=overDiv style="Z-INDEX: 1000; VISIBILITY: hidden; POSITION: absolute"></div>
<form id=Form1 onsubmit="return validaCampos()" method=post 
runat="server"><input id=valorRadio type=hidden name=valorRadio> 
<P>
    <asp:HiddenField ID="hdnAcesso" runat="server" />
<asp:label id=lblNavegacao runat="server" CssClass="Caminhotela">Label</asp:label><BR><asp:label id=lblVigencia runat="server" CssClass="Caminhotela"><br>COTEMINAS S.A - Per�odo de compet�ncia: 01/12/2006 at� 31/12/2006</asp:label></P><asp:panel 
id=pnlQualidade Visible="true" Runat="server">
<asp:panel id=pnlMovimentacao runat="server" CssClass="fundo-azul-claro" Visible="False" HorizontalAlign="Center" Width="60%" Height="80px"><BR>
<asp:Label id=lblListagemEnviada runat="server" CssClass="Caminhotela"><br>Seu arquivo foi enviado para a Alian�a do Brasil.<br>&nbsp;<br>Voc� receber� um e-mail da Alian�a do Brasil, no qual ser�o informadas as vidas aceitas e as vidas recusadas.<br>&nbsp;<br>O n�mero do protocolo �: 1850&nbsp;&nbsp;<img src="../segw0060/Images/help.gif"><br>&nbsp;</asp:Label></asp:panel>
<P>
<asp:panel id=pnlCadastroTemplate runat="server">
<TABLE cellSpacing=0 cellPadding=2 width=600 border=0><!--TR>
							<TD align="left" width="25%" colSpan="3" height="30">
								<asp:Label id="lblTitulo" runat="server" CssClass="Caminhotela" Visible=False>:: Configurar layout do arquivo de vidas</asp:Label></TD>
						</TR-->
  <TR>
    <TD vAlign=top align=left colSpan=2>
<asp:RadioButtonList id=tipoArquivo runat="server" CssClass="Caminhotela" Visible="true" Width="350px" RepeatDirection="Horizontal" RepeatLayout="Flow">
				<asp:ListItem Value="sobre">Sobreposi&#231;&#227;o &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</asp:ListItem>
				<asp:ListItem Value="incr" Selected="True">Incremental </asp:ListItem>
		</asp:RadioButtonList></TD></TR>
  <TR>
    <TD vAlign=top align=left colSpan=2>
<asp:Label id=Label1 runat="server" Font-Size="X-Small">Defina o tipo de arquivo:</asp:Label>&nbsp; 
      &nbsp;&nbsp;&nbsp; 
<asp:RadioButtonList id=radioTipo runat="server" Width="170px" RepeatDirection="Horizontal" RepeatLayout="Flow" Font-Size="X-Small">
									<asp:ListItem Value="Excel" Selected="True">Excel     </asp:ListItem>
									<asp:ListItem Value="CSV">CSV     </asp:ListItem>
									<asp:ListItem Value="TXT">TXT</asp:ListItem>
</asp:RadioButtonList>
<asp:Image id=Image4 onmouseover="return overlib('Selecionar o formato do arquivo de sua prefer�ncia.');" onmouseout="return nd();" runat="server" ImageUrl="../segw0060/Images/help.gif"></asp:Image></TD></TR>
    <tr>
        <td align="left" colspan="2" valign="top">
            <asp:Panel ID="pnlSubgrupo" runat="server" Height="50px" Visible="False" Width="125px">
                <table style="width: 200px; text-align: left">
                    <tr>
                        <td class="fonte-destaque-azul" colspan="1" style="width: 16px; height: 2px" valign="top">
                            Layout definido:
                        </td>
                        <td colspan="2" style="width: 200px; height: 2px">
                            <asp:RadioButtonList ID="rblSelecao" runat="server" Height="20px" RepeatDirection="Horizontal"
                                Width="295px">
                                <asp:ListItem Selected="True" Value="Atual">Somente o subgrupo selecionado</asp:ListItem>
                                <asp:ListItem Value="Todos">Todos os subgrupos</asp:ListItem>
                            </asp:RadioButtonList></td>
                        <td style="width: 17px; height: 2px" valign="top">
                            </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
  <TR>
    <TD vAlign=top align=left colSpan=2>
<asp:Panel id=pnlSeparador Runat="server" Visible="true">
<asp:Label id=lblSeparador runat="server" Font-Size="X-Small">Informe o separador dos campos:</asp:Label>&nbsp; 
<asp:TextBox id=txtSeparador runat="server" Width="16px" MaxLength="1"></asp:TextBox>&nbsp; 
<asp:Image id=Image3 onmouseover="return overlib('Colocar o separador dos campos selecionados do layout.');" onmouseout="return nd();" runat="server" ImageUrl="../segw0060/Images/help.gif"></asp:Image></asp:Panel></TD></TR>
  <TR>
    <TD vAlign=middle align=left colSpan=2>&nbsp;</TD></TR>
  <TR>
    <TD vAlign=middle align=left colSpan=3>
      <TABLE class=Caminhotela height=56 cellSpacing=0 cellPadding=2 width=544 
      border=0>
        <TR style="FONT-WEIGHT: normal; COLOR: black">
          <TD width=304>Nome do layout de arquivo definido: </TD>
          <TD width=246>
<asp:TextBox id=txtNomeArquivoAntigo runat="server" Width="180" Enabled="False"></asp:TextBox><!--removido c�digo ao lado, style="DISPLAY: none" -->
<asp:DropDownList id=ddlFormatosArquivo style="DISPLAY: none" Runat="server" Width="100" AutoPostBack="True" EnableViewState="False">
					<asp:ListItem Value="0">Selecione</asp:ListItem>
</asp:DropDownList></TD></TR>
        <TR style="DISPLAY: none; FONT-WEIGHT: normal; COLOR: black">
          <TD width=304 colSpan=2 height=17>Ou</TD></TR>
        <TR style="FONT-WEIGHT: normal; COLOR: black">
          <TD vAlign=middle align=left width=304>Informe o novo nome do layout 
            de arquivo: </TD>
          <TD width=246>
<asp:TextBox id=txtLayoutArquivo Runat="server" Width="180" MaxLength="20" EnableViewState="False"></asp:TextBox></TD></TR></TABLE></TD></TR>
  <TR>
    <TD vAlign=middle align=left colSpan=2>&nbsp;</TD></TR>
  <TR>
    <TD align=left colSpan=2>
<asp:Label id=Label2 runat="server" Font-Size="X-Small">Informe a ordem dos campos do seu arquivo:</asp:Label>&nbsp; 
<asp:Image id=Image5 onmouseover="return overlib('Informe a ordem dos campos do seu arquivo.',WIDTH, 265);" onmouseout="return nd();" runat="server" ImageUrl="../segw0060/Images/help.gif"></asp:Image></TD></TR>
  <TR>
    <TD vAlign=middle colSpan=2 height=171><BR>
      <TABLE id=Table1 height=108 cellSpacing=1 cellPadding=1 width=288 
border=0>
        <TR>
          <TD>Campos Dispon�veis:<BR>
<asp:ListBox id=ddl1 Runat="server" Width="180px" EnableViewState="false" Rows="11" BackColor="Transparent">
				</asp:ListBox></TD>
          <TD width=99>
<asp:Button id=btnIncluir runat="server" CssClass="Botao" Width="90px" CausesValidation="False" Text="Incluir ->"></asp:Button><BR>
<asp:Button id=btnRetirar runat="server" CssClass="Botao" Width="90px" CausesValidation="False" Text="<- Retirar"></asp:Button><BR><BR></TD>
          <TD>
            <TABLE cellSpacing=0 cellPadding=0 border=0>
              <TR>
                <TD>Campos Selecionados:<BR>
<asp:ListBox id=ddl2 Runat="server" Width="180px" EnableViewState="false" Rows="11" BackColor="Transparent">
						
						</asp:ListBox></TD>
                <TD><A onclick="up();return false;" href=""><IMG 
                  src="images/imgup.gif" width=20 border=0></A> <BR><A 
                  onclick="down();return false;" href="#"><IMG 
                  src="images/imgdown.gif" width=20 border=0></A> 
            </TD></TR></TABLE></TD></TR></TABLE>&nbsp; 
      <DIV id=painelTamCampo style="DISPLAY: none">
<asp:Panel id=pnlTamCampo Runat="server">
      <TABLE cellSpacing=0 cellPadding=0 border=0>
        <TR>
          <TD>Informe o tamanho do campo&nbsp;</TD>
          <TD>
            <DIV id=txtTamanhoCampo 
            style="FLOAT: left; POSITION: relative"></DIV></TD>
          <TD>: <INPUT id=tamAtual onkeyup=mudaTamanhoCampo(this.value); 
            type=text maxLength=2 name=tamAtual> 
<asp:Image id=Image8 onmouseover="return overlib('Informe a quantidade de caracteres utilizados pelo campo indicado.');" onmouseout="return nd();" runat="server" ImageUrl="../segw0060/Images/help.gif"></asp:Image></TD></TR></TABLE></asp:Panel></DIV>&nbsp; 

      <DIV id=painelFormatoCamposData style="DISPLAY: none">
<asp:Panel id=panFormatoCamposData Runat="server">
      <TABLE cellSpacing=0 cellPadding=0 border=0>
        <TR>
          <TD>Informe o formato para os campos data&nbsp;</TD>
          <TD>: 
<asp:DropDownList id=DDLFormatoData runat="server" Width="113px"></asp:DropDownList></TD></TR></TABLE></asp:Panel></DIV>
  <TR>
  <TR>
    <TD id=td_labelInfo vAlign=top align=left colSpan=2 height=15>
<asp:Label id=Label3 runat="server" Font-Size="X-Small">Descreva como est�o as informa��es abaixo no seu arquivo:</asp:Label></TD></TR>
  <TR>
    <TD vAlign=top align=left width=15 colSpan=2 height=109>
      <TABLE id=Table2 height=66 cellSpacing=1 cellPadding=1 width=512 
        border=0>
        <TR>
          <TD width=57>
            <DIV id=ttSexo><STRONG>Sexo:&nbsp; </STRONG>
<asp:Image id=Image1 onmouseover="return overlib('Definir o sexo correspondente (ex: F ou M; Fem ou Masc; 1 ou 2).');" onmouseout="return nd();" runat="server" ImageUrl="../segw0060/Images/help.gif"></asp:Image></DIV></TD>
          <TD width=159></TD>
          <TD width=193 colSpan=2>
            <DIV id=ttComponente ><STRONG><B>Tipo componente:</B>&nbsp; </STRONG>
<asp:Image id=Image2 onmouseover="return overlib('[Ajuda]');" onmouseout="return nd();" runat="server" ImageUrl="../segw0060/Images/help.gif"></asp:Image></DIV></TD>
          <TD width=120 colSpan=3>
            <DIV id=ttOperacao ><STRONG><B>Opera��o:</B>&nbsp; </STRONG>
<asp:Image id=Image7 onmouseover="return overlib('Definir o c�digo da opera��o (Ex: Exclus�o=1&nbsp;&nbsp;&nbsp; Altera��o=2&nbsp;&nbsp; Inclus�o=3).');" onmouseout="return nd();" runat="server" ImageUrl="../segw0060/Images/help.gif"></asp:Image></DIV></TD></TR>
        <TR>
          <TD width=57 height=20>
            <DIV id=ttSexoMasc>masculino:</DIV></TD>
          <TD width=159 height=20>
            <DIV id=ttSexoMascCampos>
<asp:TextBox id=TextBox3 runat="server" Width="80px" MaxLength="10" EnableViewState="False"></asp:TextBox></DIV></TD>
          <TD>
            <DIV id=ttTitular>titular:</DIV></TD>
          <TD width=137>
            <DIV id=ttTitularCampos>
<asp:TextBox id=TextBox1 runat="server" Width="80px" EnableViewState="False"></asp:TextBox></DIV></TD>
          <TD width=76>
            <DIV id=ttInclusao>Inclus�o:</DIV></TD>
          <TD>
            <DIV id=ttInclusaoCampos>
<asp:TextBox id=Textbox5 runat="server" Width="80px" EnableViewState="False"></asp:TextBox></DIV></TD></TR>
        <TR>
          <TD width=57>
            <DIV id=ttSexoFem>feminino:</DIV></TD>
          <TD width=159>
            <DIV id=ttSexoFemCampos>
<asp:TextBox id=TextBox2 runat="server" Width="80px" MaxLength="10" EnableViewState="False"></asp:TextBox></DIV></TD>
          <TD>
            <DIV id=ttConjuge>c�njuge:</DIV></TD>
          <TD width=137>
            <DIV id=ttConjugeCampos>
<asp:TextBox id=TextBox4 runat="server" Width="80px" EnableViewState="False"></asp:TextBox></DIV></TD>
          <TD>
            <DIV id=ttAlteracao>Altera��o:</DIV></TD>
          <TD width=137>
            <DIV id=ttAlteracaoCampos>
<asp:TextBox id=Textbox6 runat="server" Width="80px" EnableViewState="False"></asp:TextBox></DIV></TD></TR>
        <TR>
          <TD width=57></TD>
          <TD width=159></TD>
          <TD width=159></TD>
          <TD width=159></TD>
          <TD>
            <DIV id=ttDesligamento>Desligamento:</DIV></TD>
          <TD width=137>
            <DIV id=ttDesligamentoCampos>
<asp:TextBox id=Textbox7 runat="server" Width="80px" EnableViewState="False"></asp:TextBox></DIV></TD></TR></TABLE></TD></TR>
  <TR>
    <TD vAlign=top align=center colSpan=2><!--BR-->
        <asp:Button id=btnAplicar runat="server" CssClass="Botao" Visible="FALSE" Text="Gravar">
        </asp:Button>
        <INPUT id="btnEnviar" name="btnEnviar" class=Botao onclick="return validaCampos()" type=submit value=Gravar>
        <asp:Button id=btnFechaPagina runat="server" CssClass="Botao" Width="56px" CausesValidation="False" Text="Fechar">
        </asp:Button>
     </TD>
   </TR>
   </TABLE></asp:panel></P><INPUT 
type=hidden name=campos> <INPUT type=hidden name=acao> <INPUT type=hidden name=pog> <!--INPUT type=hidden name=pogao_vitao-->
<DIV id=tamanhoCampos style="DISPLAY: none"></DIV>
<SCRIPT>
		top.escondeaguarde();
		try {
			document.getElementById('td_labelInfo').style.display = 'none';				
			document.getElementById('ttSexo').style.display = 'none';				
			document.getElementById('ttSexoMasc').style.display = 'none';				
			document.getElementById('ttSexoFem').style.display = 'none';				
			document.getElementById('ttSexoMascCampos').style.display = 'none';				
			document.getElementById('ttSexoFemCampos').style.display = 'none';
			
			document.getElementById('ttComponente').style.display = 'none';				
			document.getElementById('ttTitular').style.display = 'none';				
			document.getElementById('ttTitularCampos').style.display = 'none';				
			document.getElementById('ttConjuge').style.display = 'none';				
			document.getElementById('ttConjugeCampos').style.display = 'none';
			
			document.getElementById('ttOperacao').style.display = 'none';				
			document.getElementById('ttInclusao').style.display = 'none';				
			document.getElementById('ttInclusaoCampos').style.display = 'none';				
			document.getElementById('ttAlteracao').style.display = 'none';				
			document.getElementById('ttAlteracaoCampos').style.display = 'none';				
			document.getElementById('ttDesligamento').style.display = 'none';				
			document.getElementById('ttDesligamentoCampos').style.display = 'none';				
			document.getElementById('td_labelInfo').style.display = 'none';				
			
			/*var temCPF do Titular = false; 		
			for(i=0; i<document.Form1.ddl2.options.length; i++) {
				if(document.Form1.ddl2.options[i].value == "cpf_titular") {
					temCPF do Titular = true;
				}
			}
			
			if(!temCPF do Titular) {
				document.Form1.ddl2.options[document.Form1.ddl2.options.length] = new Option("cpf_titular", "cpf_titular");
			}*/
		} catch (e) {
		}
		
		document.getElementById('ddl1').ondblclick = function() { return incluirCampo();	};
		document.getElementById('ddl2').ondblclick = function() { return retirarCampo();	};
		
		
	
	// Projeto 539202 - Jo�o Ribeiro - 29/04/2009
	if (document.getElementById('hdnAcesso').value == "6") {
	    document.Form1.btnEnviar.disabled = true;
    }
	// Projeto 539202 - Fim

</SCRIPT>
</asp:panel></form>
<script language=javascript>
	pegaTipo();
</script>


	</body>
</HTML>
