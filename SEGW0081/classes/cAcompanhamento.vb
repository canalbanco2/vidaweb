
Public Class cAcompanhamento

#Region "Heran�a"
    Inherits cBanco
#End Region

    Public Sub SEGS5696_SPS(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal workflow As Integer, ByVal valor_id As Integer)
        SQL = "exec vida_web_db.dbo.segs5696_sps  @apolice_id = " & apolice_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @tp_wf_id = " & workflow
        SQL &= ", @tp_versao_id = " & valor_id
    End Sub


    Public Sub getDadosLayout(ByVal layout_arquivo_id As Integer, ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)

        SQL = " EXEC vida_web_db.dbo.SEGS5763_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id  = " & ramo_id
        SQL &= ", @subgrupo_id = " & subgrupo_id
        SQL &= ", @layout_arquivo_id = " & layout_arquivo_id

    End Sub

    Public Sub getCamposLayout(ByVal layout_arquivo_id As Integer, ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)
        SQL = " EXEC vida_web_db.dbo.SEGS5763_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id  = " & ramo_id
        SQL &= ", @subgrupo_id = " & subgrupo_id
        SQL &= ", @layout_arquivo_id = " & layout_arquivo_id
        SQL &= ", @so_campos = 1 "

    End Sub

    Public Sub getLayoutArquivo(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)
        SQL = "exec vida_web_db.dbo.SEGS5763_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id  = " & ramo_id
        SQL &= ", @subgrupo_id = " & subgrupo_id
    End Sub

    Public Sub getSobreposicao(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)
        SQL = "Select top 1 sobreposicao FROM vida_web_db.dbo.layout_arquivo_vida_tb "
        SQL &= "where  apolice_id = " & apolice_id
        SQL &= " AND ramo_id  = " & ramo_id
        SQL &= " AND subgrupo_id = " & subgrupo_id
        SQL &= " order by layout_id desc "
    End Sub



    Public Sub New(Optional ByVal banco As cDadosBasicos.eBanco = cDadosBasicos.eBanco.web_seguros_db)
        MyBase.New(banco)
        Alianca.Seguranca.BancoDados.cCon.BancodeDados = banco.ToString

    End Sub

    Function trInt(ByVal valor As Object) As String

        If valor = 0 Then
            Return "null"
        Else
            Return valor
        End If

    End Function

    Function trStr(ByVal valor As Object) As String

        If valor = "" Then
            Return "null"
        Else
            Return "'" & valor.ToString.Replace("'", "''") & "'"
        End If

    End Function

    Public Sub CriaLayoutTodosSubgrupos(ByVal apolice_id As Long, ByVal ramo_id As Integer)

        SQL = " select distinct sub_grupo_id, nome " & _
              " from [SISAB051\qualid].seguros_db.dbo.sub_grupo_apolice_tb   " & _
              " where ramo_id = " & trInt(ramo_id) & _
              " and apolice_id = " & trInt(apolice_id) & _
              " and dt_fim_vigencia_sbg Is null " & _
              " ORDER BY sub_grupo_id "


    End Sub





    Public Sub SEGS5765_SPI(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal layout_arquivo_id As Integer, ByVal cod_susep As Integer, ByVal sucursal_id As Integer, ByVal separador As String, ByVal tipo_arquivo As String, ByVal sobreposicao As String, ByVal nome_layout As String, ByVal str_nome_campo As String, ByVal dominio_titular As String, ByVal dominio_conjuge As String, ByVal dominio_masc As String, ByVal dominio_fem As String, ByVal dom_inclusao As String, ByVal dom_alteracao As String, ByVal dom_exclusao As String, ByVal usuario As String, ByVal formato_data_ID As Integer)

        SQL = " EXEC dbo.SEGS5765_SPI "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id  = " & ramo_id
        SQL &= ", @subgrupo_id = " & subgrupo_id
        SQL &= ", @layout_arquivo_id = " & layout_arquivo_id
        SQL &= ", @separador = " & separador
        SQL &= ", @tipo_arquivo = " & tipo_arquivo
        SQL &= ", @nome_layout = " & nome_layout
        SQL &= ", @str_nome_campo = " & str_nome_campo
        SQL &= ", @dom_titular = " & dominio_titular
        SQL &= ", @dom_conjuge = " & dominio_conjuge
        SQL &= ", @dominio_masc = " & dominio_masc
        SQL &= ", @dominio_fem = " & dominio_fem
        SQL &= ", @dom_inclusao = " & dom_inclusao
        SQL &= ", @dom_alteracao = " & dom_alteracao
        SQL &= ", @dom_exclusao = " & dom_exclusao
        SQL &= ", @usuario = " & usuario
        SQL &= ", @sobreposicao = " & sobreposicao
        SQL &= ", @formato_data_ID = " & formato_data_ID


    End Sub

    Public Sub GetDadosSubGrupoE3(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)

        SQL = "exec vida_web_db.dbo.SEGS5706_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub


    'Consulta as condi��es do subgrupo
    Public Sub SEGS5706_SPS(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)

        SQL = "exec vida_web_db.dbo.SEGS5706_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub

    Public Sub GetDadosFormatodata()

        SQL = "exec vida_web_db.dbo.SEGS6977_SPS "

    End Sub


    ' Projeto 539202 - Jo�o Ribeiro - 27/05/2009
    'Consulta os subgrupos da ap�lice de acordo com um CPF
    Public Sub consulta_subgrupo_sps(ByVal apolice_id As Long, ByVal ramo_id As Int16, ByVal cpf As String)

        SQL = "set nocount on exec vida_web_db.dbo.SEGS5661_SPS "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @cpf='" & cpf & "'"

    End Sub
    ' Projeto 539202 - FIM

End Class
