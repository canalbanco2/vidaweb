Partial Class LayoutArquivo
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents TextBox4 As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnDescartar As System.Web.UI.WebControls.Button
    Protected WithEvents RequiredFieldValidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents RequiredFieldValidator4 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Image6 As System.Web.UI.WebControls.Image
    Protected WithEvents Dropdownlist1 As System.Web.UI.WebControls.DropDownList


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim valor As Integer = 0
    Dim Val As String
    Dim numerico As Boolean = True
    Dim Alf() As String = {"A-Z"}
    Dim iDescarte As Integer = 0
    Dim nomeDia As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim linkseguro As Alianca.Seguranca.Web.LinkSeguro

        'Implementa��o do LinkSeguro
        linkseguro = New Alianca.Seguranca.Web.LinkSeguro
        Dim usuario As String = linkseguro.LerUsuario("LayoutArquivo.aspx", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))
        'If usuario = "" Then
        'Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
        'End If


        'Implementa��o do controle de ambiente
        Dim cAmbiente As Alianca.Seguranca.Web.ControleAmbiente
        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & Request.ServerVariables("URL") & "/"
        cAmbiente.ObterAmbiente(url)


        'configura��o da conex�o com o banco de dados
        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produ��o)
        Else
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produ��o)
        End If

        Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)

        'If cAmbiente.Ambiente = Alianca.Seguranca.BancoDados.cCon.Ambientes.Produ��o Then
        '    pnlProducao.Visible = True
        '    pnlQualidade.Visible = False
        'Else
        'pnlProducao.Visible = False
        pnlQualidade.Visible = True
        'End If
        'Inicio do desenvolvimento da p�gina
        If Not Page.IsPostBack Then
            For Each m As String In Request.QueryString.Keys
                If m <> "" Then
                    Session(m) = Request.QueryString(m)
                End If
            Next
            'pnlSeparador.Visible = True
            pnlSeparador.Attributes.CssStyle.Add("display", "block")


        End If


        ' Projeto 539202 - Jo�o Ribeiro - 27/05/2009
        ConsultaAcesso(Session("apolice"), Session("ramo"), Session("cpf"))
        ' Projeto 539202 - FIM


        montaDllCampos()
        montaFuncValCampoObr()
        montaDropDownFormatoData()



        Try
            If Page.IsPostBack And Request.Form("acao") = "I" Then
                If Me.rblSelecao.SelectedValue = "Atual" Then
                    incluiLayout()
                Else
                    IncluiTodosSubgrupos()
                End If

                cUtilitarios.escreveScript("location.href=location.href")
            End If
        Catch ex As Exception

        End Try

        Dim iCount As Integer = 1
        'lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & "-> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("nomeSubgrupo") & "<br>Fun��o: Configurar Layout do Arquivo"
        lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & "-> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomesubGrupo") & "<br>Fun��o: Configurar Layout do Arquivo"
        lblVigencia.Text = "" & "" & " <br> " & getPeriodoCompetenciaSession()
        lblVigencia.Visible = True
        Me.lblListagemEnviada.Text = "P�gina em constru��o."
        Me.lblListagemEnviada.Visible = True
        pnlMovimentacao.Visible = False
        pnlCadastroTemplate.Visible = True

        radioTipo.Attributes.Add("onclick", "return getTpArquivo();")
        tipoArquivo.Attributes.Add("onclick", "return pegaTipo();")

        Me.btnIncluir.Attributes.Add("onclick", "return incluirCampo();")
        Me.btnRetirar.Attributes.Add("onclick", "return retirarCampo();")
        Me.ddlFormatosArquivo.Attributes.Add("onchange", "top.exibeaguarde();")
        'Me.btnAplicar.Attributes.Add("onclick", "top.exibeaguarde();")

        '--------- Adicionado habilidade de duplo clique nos lists ddl1 e ddl2 ------

        Me.ddl1.Attributes.Add("ondblclick", "return incluirCampo();")
        Me.ddl2.Attributes.Add("ondblclick", "return retirarCampo();")

        '----------------------------------------------------------------------------

        Dim tpArquivo As String = Request.Form("valorRadio")

        If tpArquivo Is Nothing Then
            tpArquivo = "Excel"
        End If

        If (tpArquivo <> "CSV") Then
            'pnlSeparador.Visible = False
            pnlSeparador.Attributes.CssStyle.Add("display", "none")
            If tpArquivo = "" Then
                'pnlSeparador.Visible = True
                pnlSeparador.Attributes.CssStyle.Add("display", "block")
            End If
        Else
            'pnlSeparador.Visible = True
            pnlSeparador.Attributes.CssStyle.Add("display", "block")
        End If

        If tpArquivo = "TXT" Then
            'pnlTamCampo.Visible = True
            pnlTamCampo.Attributes.CssStyle.Add("display", "block")
            'pnlSeparador.Visible = False
            pnlSeparador.Attributes.CssStyle.Add("display", "none")
        Else
            'pnlTamCampo.Visible = False
            pnlTamCampo.Attributes.CssStyle.Add("display", "none")
        End If

        If tpArquivo = "Excel" Then
            numerico = False
        End If


        Dim passou As Boolean = False
        For i As Integer = 0 To ddl1.Items.Count - 1
            If ddl1.Items(i).Value.IndexOf("descartar") > 0 Then
                passou = True
            End If
        Next

        ddl2.Attributes.Add("onchange", "mostraTamanhoCampo(this.value)")


        If ddl2.Items.Count > 0 Then
            If ddl2.Items(0).ToString <> "--vazio--" Then
                For i As Integer = 0 To ddl2.Items.Count - 1
                    valor = i + 1

                    If Not numerico Then
                        Val = Alf(i)
                    Else
                        Val = valor
                    End If

                    If ddl2.Items(i).Value.IndexOf("-") > 0 Then
                        ddl2.Items(i).Value = ddl2.Items(i).Value.Substring(ddl2.Items(i).Value.IndexOf("-") + 2)
                    End If

                Next
            End If
        End If


        'If Not Page.IsPostBack Then 'N�o adiantou
        montaDdlFormatosArquivo()
        ddlFormatosArquivo.SelectedValue = Request.Form("ddlFormatosArquivo")
        'End If


        If Not Page.IsPostBack Then
            Try
                ddl2.Items.Clear()

                carregaDados(ddlFormatosArquivo.Items(1).Value)

                txtNomeArquivoAntigo.Text = ddlFormatosArquivo.Items(1).Text

            Catch ex As Exception
                'Response.Write(ex.message)
                'Response.Write(ex.StackTrace)
                'Dim excp As New clsException(ex)
            End Try
        End If

        '[New code 04/06] Objetivo: Manter os campos selecionados ao mudar de tipo de layout
        'If Request.Form("pog") = "A" Then 'A = alterado
        'End If
        '[End code]


        '[New code 01/06/07]
        txtLayoutArquivo.Attributes.Add("onblur", "return ValidaNomeLayout();")
        ddl2.Attributes.Add("onblur", "return OcultaDiv();")
        '[End code 01/06/07]

        'New code 03.08.07
        VerificaTPcomponente()
        'end code

        Me.btnFechaPagina.Visible = False


        ' Projeto 539202 - Jo�o Ribeiro - 27/04/2009
        hdnAcesso.Value = Session("acesso")
        ' Projeto 539202 - Fim

        If Me.radioTipo.SelectedValue = "Excel" Then
            pnlSubgrupo.Visible = True
        Else
            pnlSubgrupo.Visible = False
        End If

    End Sub

    Private Sub VerificaTPcomponente()
        ''Vamos buscar o tp de componente da ap�lice para saber se vamos obrigar o preenchimento do tipo conjuge
        Dim possuiConjuge As String = ""

        Try
            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

            bd.SEGS5706_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read Then
                possuiConjuge = dr.GetValue(0).ToString.Trim
            Else
                possuiConjuge = " --- "
            End If

            If possuiConjuge = "C�njuge facultativo" Then
                'obrigaremos a preencher conjuge, marcando algum valor para pogao_vitao
                Response.Write("<input type='hidden' id='pogao_vitao' value='A' name='pogao_vitao'>")
            Else
                Response.Write("<input type='hidden' id='pogao_vitao' value='' name='pogao_vitao'>")
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Private Function getPeriodoCompetenciaSession()
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)
        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)



        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
        Dim data_inicio, data_fim, retorno As String

        If dr.HasRows Then
            While dr.Read()

                data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)
                cUtilitarios.escreveScript("var dt_ini_comp = '" & data_inicio & "';")
                cUtilitarios.escreveScript("var dt_fim_comp = '" & data_fim & "';")

            End While

            retorno = "Per�odo de Compet�ncia: " & data_inicio & " a " & data_fim
        Else
            Session.Abandon()
            'cUtilitarios.br("N�o existe nenhuma Compet�ncia de Fatura em aberto")
            'System.Web.HttpContext.Current.Response.Write("<script>parent.window.location=parent.window.location;</script>")
            'Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
            System.Web.HttpContext.Current.Response.Write("<script>parent.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp'</script>")
            Response.End()
        End If
        dr.Close()
        dr = Nothing
        Return retorno
    End Function

    Private Sub montaDdlFormatosArquivo()
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)
        bd.getLayoutArquivo(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

        Try

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
            ddlFormatosArquivo.Items.Clear()
            Dim tempSelecione As New Web.UI.WebControls.ListItem("Selecione", 0)
            ddlFormatosArquivo.Items.Add(tempSelecione)


            While dr.Read
                If ddlFormatosArquivo.Items.FindByText(dr.GetValue(0)) Is Nothing Then
                    ddlFormatosArquivo.Items.Add(dr.GetValue(0))
                    Me.ddlFormatosArquivo.Items(Me.ddlFormatosArquivo.Items.Count - 1).Value = dr.GetValue(1)
                End If

            End While

            DefineSobreposicao()

        Catch ex As Exception
            'cUtilitarios.br("sse")
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Sub DefineSobreposicao()

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)
        bd.getSobreposicao(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

        Try

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            While dr.Read
                If Not IsDBNull(dr.GetValue(0)) Then
                    If dr.GetValue(0).ToString = "1" Then
                        tipoArquivo.Items(1).Selected = False
                        tipoArquivo.Items(0).Selected = True
                    Else
                        tipoArquivo.Items(0).Selected = False
                        tipoArquivo.Items(1).Selected = True
                    End If
                Else
                    'Desmarco os dois   
                    tipoArquivo.Items(0).Selected = False
                    tipoArquivo.Items(1).Selected = False
                End If

            End While

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Sub tipoArquivo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If ddl1.SelectedValue = "" Then
            ddl1.SelectedIndex = 0
        End If

        If ddl1.SelectedValue = "--vazio--" Or ddl1.SelectedValue = "" Then
            Response.Write("<script>alert('Todos os itens j� foram posicionados no menu � direita.')</script>")
        Else
            ddl2.Items.Add(ddl1.SelectedValue)
            ddl1.Items.Remove(ddl1.SelectedValue)
        End If
        'If ddl1.Items.Count = 0 Then
        '    ddl1.Items.Add("--vazio--")
        'End If

        ddl1.SelectedIndex = 0

    End Sub

    Private Sub carregaDados(ByVal layout_arquivo_id As Integer)

        If layout_arquivo_id = 0 Then
            Exit Sub
        End If

        Try
            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

            bd.getDadosLayout(layout_arquivo_id, Session("apolice"), Session("ramo"), Session("subgrupo_id"))
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            Dim tipo_Arquivo As String
            Dim sobreposicao As String
            Dim nome_layout As String
            Dim dom_titular As String
            Dim dom_conjuge As String
            Dim dom_masc As String
            Dim dom_fem As String
            Dim dom_mov_alter As String
            Dim dom_mov_incl As String
            Dim dom_mov_excl As String

            Dim separador As String
            Dim coluna As String
            Dim temData As Boolean = False
            Dim temSexo As Boolean = False
            Dim temTpMovimentacao As Boolean = False
            Dim temTpComponente As Boolean = False
            Dim script As String = ""
            Dim strColunas As String = ""
            Dim i = 0
            Dim tamanho(1000) As String  'EDUARDO.BORGES -CONFITEC - FLOW 11523054 - 21/06/2011
            Dim z = 0
            Dim ValDdl = ""
            Dim textoDdl = ""


            If dr.HasRows Then
                While dr.Read()
                    DDLFormatoData.SelectedIndex = IIf(IsDBNull(dr.GetValue(7)), 0, dr.GetValue(7) - 1)

                    ddl2.Items.Remove(dr.GetValue(0))
                    'cUtilitarios.escreve(dr.GetValue(0).ToString.Trim & "<br>")
                    coluna = dr.GetValue(0)
                    If coluna.Trim.Length > 0 Then
                        If strColunas.IndexOf(coluna.Trim) = -1 Or coluna.Trim = "--descartar--" Then
                            strColunas &= coluna.Trim & ";"
                            tamanho(z) = dr.Item("tamanho")
                            z = z + 1
                        End If

                        If coluna.IndexOf("exo") > 0 Then
                            temSexo = True
                        End If

                        If coluna.IndexOf("per") > 0 Then
                            temTpMovimentacao = True
                        End If

                        If coluna.IndexOf("Data") = 0 Then
                            temData = True
                        End If

                        If coluna.IndexOf("omp") > 0 Then
                            temTpComponente = True
                        End If
                        If dr.GetValue(0).Trim <> "--descartar--" Then
                            If coluna.IndexOf("exo") > 0 Then
                                ddl1.Items.Remove(dr.GetValue(0).ToString.Trim)
                            Else

                                ddl1.Items.Remove(dr.GetValue(0).ToString.Trim)

                                If dr.GetValue(0).ToString.Trim.ToLower = "Data de Nascimento" Then
                                    ddl1.Items.Remove("Data de Nascimento")
                                End If

                                If dr.GetValue(0).ToString.Trim.ToLower = "CPF do Titular" Then
                                    ddl1.Items.Remove("CPF do Titular")
                                End If

                                ' pablo.dias (Nova Consultoria) - 01/08/2011
                                ' 9420650 - Melhorias no Sistema Vida Web 
                                ' Modifica��o de layout: DataRisco -> Data de Inclus�o no Seguro
                                If dr.GetValue(0).ToString.Trim.ToLower = "Data de Inclus�o no Seguro" Then 
                                    ddl1.Items.Remove("Data de Inclus�o no Seguro") 
                                End If

                                If dr.GetValue(0).ToString.Trim.ToLower = "Nome do Segurado" Then
                                    ddl1.Items.Remove("Nome do Segurado")
                                End If

                                If dr.GetValue(0).ToString.Trim.ToLower = "Valor do Sal�rio" Then
                                    ddl1.Items.Remove("ValorSalario")
                                End If
                            End If
                        End If
                    End If

                    Try
                        tipo_Arquivo = dr.GetValue(3)
                        nome_layout = dr.GetValue(5)
                        separador = dr.GetValue(4)
                    Catch ex As Exception

                    End Try

                    Select Case dr.GetValue(1)
                        Case "sexo_masculino"
                            dom_masc = dr.GetValue(2)
                        Case "sexo_feminino"
                            dom_fem = dr.GetValue(2)
                        Case "tipo_movimentacao_exclusao"
                            dom_mov_excl = dr.GetValue(2)
                        Case "tipo_movimentacao_alteracao"
                            dom_mov_alter = dr.GetValue(2)
                        Case "tipo_movimentacao_inclusao"
                            dom_mov_incl = dr.GetValue(2)
                        Case "dom_titular"
                            dom_titular = dr.GetValue(2)
                        Case "dom_conjuge"
                            dom_conjuge = dr.GetValue(2)
                    End Select
                    i = i + 1
                End While

                Dim adicionouDescartar As Boolean = False
                z = 0

                ddl2.Items.Clear()

                For Each m As String In strColunas.Split(";")
                    Response.Write("<input type='hidden' value='" & tamanho(z) & "' id='tam_" & m & "'>")
                    z = z + 1

                    'If m <> "--descartar--" Then
                    '    ddl2.Items.Add(New System.Web.UI.WebControls.ListItem(m))
                    'Else
                    '    If adicionouDescartar Then
                    '        adicionouDescartar = False
                    '    Else                            
                    '        ddl2.Items.Add(New System.Web.UI.WebControls.ListItem(m))
                    '        adicionouDescartar = True
                    '    End If
                    'End If
                    If m.Trim.Length > 0 Then
                        m = m.Trim
                        If m.IndexOf("--descartar--") > 0 Then
                            ValDdl = m
                            textoDdl = "--descartar--"
                        Else
                            ValDdl = m
                            textoDdl = m
                        End If
                        ddl2.Items.Add(New System.Web.UI.WebControls.ListItem(textoDdl, ValDdl))
                    End If
                Next

            End If

            bd.getCamposLayout(layout_arquivo_id, Session("apolice"), Session("ramo"), Session("subgrupo_id"))

            dr = bd.ExecutaSQL_DR()

            ddl2.DataSource = dr

            ddl2.DataTextField = "Nome"
            ddl2.DataValueField = "Nome"

            ddl2.DataBind()



            Me.deParaCampoNome(Me.ddl2)

            Me.deParaCampoNome(Me.ddl1)




            script &= "<script> function showDiv() { " & vbCrLf

            script &= exibeCamposSexo(temSexo)

            script &= exibeTpMovimentacao(temTpMovimentacao)

            script &= exibeCampoData(temData)

            script &= exibeTpComponente(temTpComponente)

            script &= "} </script>" & vbCrLf

            Response.Write(script)

            'txtLayoutArquivo.Text = nome_layout
            Textbox6.Text = dom_mov_alter
            Textbox5.Text = dom_mov_incl
            Textbox7.Text = dom_mov_excl
            TextBox3.Text = dom_masc
            TextBox2.Text = dom_fem
            TextBox1.Text = dom_titular
            TextBox4.Text = dom_conjuge
            txtSeparador.Text = separador

            'Response.Write(tipo_Arquivo)
            Try
                Select Case tipo_Arquivo
                    Case "E"
                        Me.radioTipo.SelectedIndex = 0
                        'pnlSeparador.Visible = False
                        pnlSeparador.Attributes.CssStyle.Add("display", "none")
                        Me.pnlSubgrupo.Visible = True
                    Case "C"
                        Me.radioTipo.SelectedIndex = 1
                        'pnlSeparador.Visible = True
                        pnlSeparador.Attributes.CssStyle.Add("display", "block")
                        Me.pnlSubgrupo.Visible = False
                    Case "T"
                        Me.radioTipo.SelectedIndex = 2
                        'pnlTamCampo.Visible = True
                        pnlTamCampo.Attributes.CssStyle.Add("display", "block")
                        'pnlSeparador.Visible = False
                        pnlSeparador.Attributes.CssStyle.Add("display", "none")
                        Me.pnlSubgrupo.Visible = False
                    Case Else
                        Me.pnlSubgrupo.Visible = True

                End Select
            Catch ex As Exception
                cUtilitarios.br(ex.message)
            End Try

        Catch ex As Exception

            Dim excp As New clsException(ex)

        End Try

    End Sub

    Private Sub deParaCampoNome(ByVal ddl As Web.UI.WebControls.ListBox)

        Dim remover As String = ""

        If ddl.Items.Count > 0 Then

            For m As Integer = 0 To ddl.Items.Count - 1
                Select Case ddl.Items(m).Text.Trim.ToLower
                    Case "CPF do Titular"
                        ddl.Items(m).Text = "CPF do Titular"
                    Case "CPF do Titular"
                        ddl.Items(m).Text = "CPF do Titular"
                    Case "Nome do Segurado"
                        ddl.Items(m).Text = "Nome do Segurado"
                    ' pablo.dias (Nova Consultoria) - 01/08/2011
                    ' 9420650 - Melhorias no Sistema Vida Web 
                    ' Modifica��o de layout: DataRisco -> Data de Inclus�o no Seguro
                    Case "Data de Inclus�o no Seguro" 
                        ddl.Items(m).Text = "Data de Inclus�o no Seguro" 
                    Case "Opera��o"
                        ddl.Items(m).Text = "Opera��o"
                    Case "Valor do Sal�rio"
                        If Not cUtilitarios.MostraColunaSalario Then
                            remover &= ";" & m
                        Else
                            ddl.Items(m).Text = "Valor do Sal�rio"
                        End If
                    Case "Capital Segurado"
                        If Not cUtilitarios.MostraColunaCapital Then
                            remover &= ";" & m
                        Else
                            ddl.Items(m).Text = "Capital Segurado"
                        End If
                    Case "Sexo"
                        ddl.Items(m).Text = "Sexo"
                    Case "Data de Nascimento"
                        ddl.Items(m).Text = "Data de Nascimento"
                    Case "TipoComponente"
                        If Not Me.mostraConjuge Then
                            remover &= ";" & m
                        Else
                            ddl.Items(m).Text = "TipoComponente"
                        End If
                    Case "CPFConjuge"
                        If Not Me.mostraConjuge Then
                            remover &= ";" & m
                        Else
                            ddl.Items(m).Text = "CPFConjuge"
                        End If
                    Case "DataDesligamento"
                        ddl.Items(m).Text = "DataDesligamento"

                End Select

                'Response.Write("Para [" & ddl.Items(m).Text & "] <br>")
            Next

        End If




        If (remover.Length > 0) Then
            remover = remover.Substring(1)

            Dim z As Integer = 0

            If remover.Split(";").Length > 0 Then
                For Each m As Integer In remover.Split(";")
                    m = m - z

                    ddl.Items.RemoveAt(m)

                    'Decrementa m para continuar removendo e n�o perder os indices
                    z = z + 1
                Next
            End If

        End If


    End Sub

    Public Function mostraConjuge() As Boolean

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        Dim possuiConjuge As String = ""

        bd.SEGS5706_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

        Try
            dr = bd.ExecutaSQL_DR()

            If dr.Read Then
                possuiConjuge = dr.GetValue(0).ToString.Trim
            End If
        Catch ex As Exception

        End Try


        If possuiConjuge = "C�njuge facultativo" Then
            Return True
        Else
            Return False
        End If

        Return True

    End Function

    Public Shared Function getCapitalSeguradoSession() As String

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)
        bd.SEGS5706_SPS(Web.HttpContext.Current.Session("apolice"), Web.HttpContext.Current.Session("ramo"), 6785, 0, Web.HttpContext.Current.Session("subgrupo_id"))

        Dim capitalSegurado As String = ""

        Try
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR

            Dim count As Int16 = 1
            Dim text As String
            Dim value As String
            Dim temp As String

            dr.Read()

            capitalSegurado = dr.GetValue(1).ToString()

            dr.Close()
            dr = Nothing

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return capitalSegurado

    End Function

    Private Function exibeTpComponente(Optional ByVal exibe As Boolean = False) As String
        Dim script As String = ""

        If exibe Then
            script &= "document.getElementById('ttComponente').style.display = 'block';" & vbCrLf
            script &= "document.getElementById('ttTitular').style.display = 'block';" & vbCrLf
            script &= "document.getElementById('ttConjuge').style.display = 'block';" & vbCrLf
            script &= "document.getElementById('ttConjugeCampos').style.display = 'block';" & vbCrLf
            script &= "document.getElementById('ttTitularCampos').style.display = 'block';" & vbCrLf
        Else
            script &= "document.getElementById('ttComponente').style.display = 'none';" & vbCrLf
            script &= "document.getElementById('ttTitular').style.display = 'none';" & vbCrLf
            script &= "document.getElementById('ttConjuge').style.display = 'none';" & vbCrLf
            script &= "document.getElementById('ttConjugeCampos').style.display = 'none';" & vbCrLf
            script &= "document.getElementById('ttTitularCampos').style.display = 'none';" & vbCrLf
        End If

        Return script
    End Function

    Private Function exibeCamposSexo(Optional ByVal exibe As Boolean = False) As String
        Dim script As String = ""

        If exibe Then
            script &= "document.getElementById('ttSexo').style.display = 'block';" & vbCrLf
            script &= "document.getElementById('ttSexoMascCampos').style.display = 'block';" & vbCrLf
            script &= "document.getElementById('ttSexoFemCampos').style.display = 'block';" & vbCrLf
            script &= "document.getElementById('td_labelInfo').style.display = 'block';" & vbCrLf
            script &= "document.getElementById('ttSexoMasc').style.display = 'block';" & vbCrLf
            script &= "document.getElementById('ttSexoFem').style.display = 'block';" & vbCrLf
        Else
            script &= "document.getElementById('ttSexo').style.display = 'none';" & vbCrLf
            script &= "document.getElementById('ttSexoMascCampos').style.display = 'none';" & vbCrLf
            script &= "document.getElementById('ttSexoFemCampos').style.display = 'none';" & vbCrLf
            script &= "document.getElementById('td_labelInfo').style.display = 'none';" & vbCrLf
            script &= "document.getElementById('ttSexoMasc').style.display = 'none';" & vbCrLf
            script &= "document.getElementById('ttSexoFem').style.display = 'none';" & vbCrLf
        End If

        Return script
    End Function

    Private Function exibeTpMovimentacao(Optional ByVal exibe As Boolean = False) As String
        Dim script As String = ""

        If exibe Then
            script &= "document.getElementById('ttOperacao').style.display = 'block';" & vbCrLf
            script &= "document.getElementById('ttInclusao').style.display = 'block';" & vbCrLf
            script &= "document.getElementById('ttInclusaoCampos').style.display = 'block';" & vbCrLf
            script &= "document.getElementById('ttAlteracao').style.display = 'block';" & vbCrLf
            script &= "document.getElementById('ttAlteracaoCampos').style.display = 'block';" & vbCrLf
            script &= "document.getElementById('ttDesligamento').style.display = 'block';" & vbCrLf
            script &= "document.getElementById('ttDesligamentoCampos').style.display = 'block';" & vbCrLf
        Else
            script &= "document.getElementById('ttOperacao').style.display = 'none';" & vbCrLf
            script &= "document.getElementById('ttInclusao').style.display = 'none';" & vbCrLf
            script &= "document.getElementById('ttInclusaoCampos').style.display = 'none';" & vbCrLf
            script &= "document.getElementById('ttAlteracao').style.display = 'none';" & vbCrLf
            script &= "document.getElementById('ttAlteracaoCampos').style.display = 'none';" & vbCrLf
            script &= "document.getElementById('ttDesligamento').style.display = 'none';" & vbCrLf
            script &= "document.getElementById('ttDesligamento').style.display = 'none';" & vbCrLf
        End If

        Return script
    End Function

    Private Function exibeCampoData(Optional ByVal exibe As Boolean = False) As String
        Dim script As String = ""

        If exibe Then
            script &= "document.getElementById('painelFormatoCamposData').style.display = 'block';" & vbCrLf
        Else
            script &= "document.getElementById('painelFormatoCamposData').style.display = 'none';" & vbCrLf
        End If

        Return script
    End Function

    Private Sub incluiLayout()
        Try
            If Request.Form("acao") = "I" Then
                Dim colunas As String = "'" & Request.Form("campos").Replace("||", ",") & "'"
                ''colunas = colunas.Replace("�", "o").Replace("�", "a") ''.Replace(" ", "")

                Dim tipoArquivo As String
                Dim sobreposicao As String
                Dim nome_layout As String
                Dim dom_titular As String
                Dim dom_conjuge As String
                Dim dom_masc As String
                Dim dom_fem As String
                Dim separador As String
                Dim dom_inclusao As String
                Dim dom_alteracao As String
                Dim dom_exclusao As String


                If Request.Form("txtSeparador") Is Nothing Then
                    separador = "''"
                Else
                    separador = "'" & Request.Form("txtSeparador").Trim & "'"
                End If

                If Request.Form("radioTipo") Is Nothing Then
                    tipoArquivo = "'txt'"
                Else
                    tipoArquivo = "'" & Request.Form("radioTipo") & "'"
                End If

                If Request.Form("tipoArquivo") Is Nothing Then
                    sobreposicao = 1
                ElseIf Request.Form("tipoArquivo") = "sobre" Then
                    sobreposicao = 1
                Else
                    sobreposicao = 0 'incremental
                End If

                '[new code 01.06.07]: Remover ap�strofo
                nome_layout = "'" & Request.Form("txtLayoutArquivo").Replace("'", "") & "'"

                '[end code]

                'dom_conjuge = "''"
                dom_inclusao = "'" & Request.Form("TextBox5") & "'"
                dom_alteracao = "'" & Request.Form("TextBox6") & "'"
                dom_exclusao = "'" & Request.Form("TextBox7") & "'"
                dom_titular = "'" & Request.Form("TextBox1") & "'"
                dom_conjuge = "'" & Request.Form("TextBox4") & "'"
                dom_masc = "'" & Request.Form("TextBox3") & "'"
                dom_fem = "'" & Request.Form("TextBox2") & "'"


                Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

                'bd.SEGS5765_SPI(Session("apolice"), Session("ramo"), Session("subgrupo_id"), 0, 6785, 0, separador, tipoArquivo, sobreposicao, nome_layout, colunas, dom_titular, dom_conjuge, dom_masc, dom_fem)
                bd.SEGS5765_SPI(Session("apolice"), Session("ramo"), Session("subgrupo_id"), 0, 6785, 0, separador.Trim, tipoArquivo, sobreposicao, nome_layout, colunas, dom_titular, dom_conjuge, dom_masc, dom_fem, dom_inclusao, dom_alteracao, dom_exclusao, Session("usuario"), Request.Form("DDLFormatoData"))

                Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

                dr.Close()
                dr = Nothing



                'CONFITEC - APEIXOTO - 28/11/2008
                ' Alter��o para corre��o de bug do Internet Explorer 6 --> Mensagem: Esta p�gina cont�m alguns �tens que n�o s�o seguros....
                'cUtilitarios.escreveScript("window.location = 'http://" & Request.ServerVariables("SERVER_NAME") & "/SEG/SEGW0060/centro.aspx';")

                If Me.rblSelecao.SelectedValue = "Atual" Then
                    cUtilitarios.br("Layout definido com sucesso.")
                    cUtilitarios.escreveScript("window.location = '../SEGW0060/centro.aspx';")

                Else

                End If

                montaDdlFormatosArquivo()

                Me.txtLayoutArquivo.Text = ""
                Me.TextBox1.Text = ""
                Me.TextBox2.Text = ""
                Me.TextBox3.Text = ""
                Me.TextBox4.Text = ""
                Me.Textbox5.Text = ""
                Me.Textbox6.Text = ""
                Me.Textbox7.Text = ""

            End If
        Catch ex As Exception
            Response.Write(ex.Message) '<--remove
            Dim excp As New clsException(ex)
            Response.End()
        End Try
    End Sub

    Private Sub btnDescartar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        For i As Integer = 0 To ddl2.Items.Count - 1
            If (Left(ddl2.Items.Item(i).Text, 11) = "--descartar") Then
                iDescarte += 1
                ddl2.Items.Item(i).Text = "--descartar (" & iDescarte & ")--"
                ddl2.Items.Item(i).Value = "--descartar (" & iDescarte & ")--"
            End If
        Next
        iDescarte += 1
        ddl2.Items.Add("--descartar (" & iDescarte & ")--")

    End Sub

    Private Sub btnFechaPagina_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("Centro.aspx")
    End Sub

    Private Sub ddlFormatosArquivo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'montaDdlFormatosArquivo()
    End Sub

    Private Sub radioTipo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radioTipo.SelectedIndexChanged

        Select Case Me.radioTipo.SelectedValue
            Case "TXT"
                Me.Label2.Text = "Informe a posi��o dos campos no arquivo TXT:"
                'ddl2.Items.Add(New Web.UI.WebControls.ListItem("cpf_titular", "cpf_titular"))
                'Me.ddl1.Items.Remove("--Descartar--")
                Me.pnlSubgrupo.Visible = False
            Case "CSV"
                Me.Label2.Text = "Informe a posi��o dos campos no arquivo CSV:"
                'ddl2.Items.Add(New Web.UI.WebControls.ListItem("cpf_titular", "cpf_titular"))
                'Me.ddl1.Items.Remove("--Descartar--")
                Me.pnlSubgrupo.Visible = False
            Case "Excel"
                Me.Label2.Text = "Informe o conte�do das colunas no arquivo Excel:"
                Me.pnlSubgrupo.Visible = True
                'ddl2.Items.Add(New Web.UI.WebControls.ListItem("cpf_titular", "cpf_titular"))
        End Select

    End Sub

    Private Sub montaDllCampos()
        Try
            Dim strCampos(12) As String
            strCampos(0) = "CPF do Titular"
            strCampos(1) = "CPFConjuge"
            strCampos(2) = "DataDesligamento"
            strCampos(3) = "Data de Nascimento"
            ' pablo.dias (Nova Consultoria) - 01/08/2011
            ' 9420650 - Melhorias no Sistema Vida Web 
            ' Modifica��o de layout: DataRisco -> Data de Inclus�o no Seguro
            strCampos(4) = "Data de Inclus�o no Seguro" 
            strCampos(5) = "Nome do Segurado"
            strCampos(6) = "Opera��o"
            strCampos(7) = "Sexo"
            strCampos(8) = "TipoComponente"
            strCampos(9) = "Valor do Sal�rio"
            strCampos(10) = "Capital Segurado"
            strCampos(11) = "--descartar--"
            strCampos(12) = "Subgrupo"

            For Each m As String In strCampos
                Select Case m
                    Case "Valor do Sal�rio"
                        If cUtilitarios.MostraColunaSalario Then
                            Me.ddl1.Items.Add(m)
                        End If
                    Case "Capital Segurado"
                        If cUtilitarios.MostraColunaCapital Then
                            Me.ddl1.Items.Add(m)
                        End If
                    Case "TipoComponente"
                        If Me.mostraConjuge Then
                            Me.ddl1.Items.Add(m)
                        End If
                    Case "CPFConjuge"
                        If Me.mostraConjuge Then
                            Me.ddl1.Items.Add(m)
                        End If
                    Case Else
                        Me.ddl1.Items.Add(m)
                End Select
            Next

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Private Sub montaFuncValCampoObr()

        Dim campo As String = "var campo = new Array();" & vbCrLf

        Dim strCampos As String = "CPF do Titular;"

        If checkTpConjugeInexistente() Then
            strCampos &= "data_desligamento;"
            strCampos &= "Data de Nascimento;"
            ' pablo.dias (Nova Consultoria) - 01/08/2011
            ' 9420650 - Melhorias no Sistema Vida Web 
            ' Modifica��o de layout: DataRisco -> Data de Inclus�o no Seguro
            strCampos &= "Data de Inclus�o no Seguro;" 
            strCampos &= "Nome do Segurado;"
            'strCampos &= "operacao;" 
            strCampos &= "Sexo;"
        Else
            strCampos &= "data_desligamento;"
            strCampos &= "Data de Nascimento;"
            ' pablo.dias (Nova Consultoria) - 01/08/2011
            ' 9420650 - Melhorias no Sistema Vida Web 
            ' Modifica��o de layout: DataRisco -> Data de Inclus�o no Seguro
            strCampos &= "Data de Inclus�o no Seguro;" 
            strCampos &= "Nome do Segurado;"
            'strCampos &= "operacao;" 
            strCampos &= "Sexo;"
        End If

        If Me.mostraConjuge Then
            strCampos &= "TipoComponente;"
        End If

        If cUtilitarios.MostraColunaCapital() Then
            strCampos &= "Capital Segurado;"
        End If

        If cUtilitarios.MostraColunaSalario() Then
            strCampos &= "Valor do Sal�rio;"
        End If

        Dim i As Integer = 0
        For Each m As String In strCampos.Split(";")
            If m.Length > 0 Then
                campo &= "campo[" & i & "] = '" & m & "';" & vbCrLf
                i = i + 1
            End If
        Next

        Dim script As String = "function validaCamposObrig() {" & vbCrLf
        script &= campo
        script &= "     for(i=0; i<campo.length; i++) { " & vbCrLf
        script &= "         valorEncontrado = false;" & vbCrLf
        script &= "         for (cont=0; cont<document.Form1.ddl2.options.length; cont++) { " & vbCrLf
        script &= "             if(document.Form1.ddl2.options[cont].value.toUpper == campo[i].toUpper) {" & vbCrLf
        script &= "                 valorEncontrado = true;" & vbCrLf
        script &= "             }" & vbCrLf
        script &= "         }" & vbCrLf
        script &= "         if(!valorEncontrado) { " & vbCrLf
        script &= "             if(campo[i] != 'data_desligamento' || (!document.getElementById('tipoArquivo_0').checked)) { " & vbCrLf
        script &= "                 alert('O campo ' + campo[i] + ' � obrigat�rio'); " & vbCrLf
        script &= "                 return false; " & vbCrLf
        script &= "             }" & vbCrLf
        script &= "         }" & vbCrLf
        script &= "     }" & vbCrLf
        script &= "     return true;" & vbCrLf

        script &= "}" & vbCrLf

        cUtilitarios.escreveScript(script)

    End Sub

    Function checkTpConjugeInexistente() As Boolean

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        'Pega restante dos dados
        bd.GetDadosSubGrupoE3(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

        Try
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR
            If dr.Read Then

                If dr.GetValue(0).ToString.Trim = "C�njuge inexistente" Then
                    Return True
                End If
            End If

            dr.Close()
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return False
    End Function

    Private Sub montaDropDownFormatoData()
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        bd.GetDadosFormatodata()

        Try
            bd.ExecutaSQL()
            Dim ds As Data.DataSet = bd.DS()

            DDLFormatoData.DataSource = ds.Tables(0)
            DDLFormatoData.DataValueField = "FORMATO_DATA_ID"
            DDLFormatoData.DataTextField = "FORMATO_DATA"
            DDLFormatoData.DataBind()

            ds.Clear()

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub



    ' Projeto 539202 - Jo�o Ribeiro - 27/05/2009
    Sub ConsultaAcesso(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal cpf As String)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        bd.consulta_subgrupo_sps(apolice_id, ramo_id, cpf)

        Try
            dr = bd.ExecutaSQL_DR()

            If Not dr Is Nothing Then
                If dr.Read() Then
                    Session("Acesso") = dr.GetValue(2).ToString()
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub
    ' Projeto 539202 - FIM



    Sub IncluiTodosSubgrupos()
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        bd.consulta_subgrupo_sps(Session("apolice"), Session("ramo"), Session("cpf"))

        Try

            Session.Add("Subgrupo_Original", Session("subgrupo_id"))


            dr = bd.ExecutaSQL_DR()

            If Not dr Is Nothing Then
                If dr.Read() Then

                    Do
                        Session("subgrupo_id") = dr.GetValue(1).ToString()
                        incluiLayout()
                    Loop Until Not dr.Read()

                    Session("subgrupo_id") = Session("Subgrupo_Original")

                    cUtilitarios.br("Layout definido com sucesso.")
                    cUtilitarios.escreveScript("window.location = '../SEGW0060/centro.aspx';")

                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    
End Class