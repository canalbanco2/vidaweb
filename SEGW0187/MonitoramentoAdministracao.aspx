<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MonitoramentoAdministracao.aspx.vb" Inherits="SEGW0187._Default" ValidateRequest="false" EnableEventValidation="false" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head runat="server">
    <title>Monitoramento Vida Web - Administra��o</title>
    <link href="css/EstiloBase.css" type="text/css" rel="stylesheet" />
    <link href="css/PainelControle.css" type="text/css" rel="stylesheet" />
    
</head>

<body>
    <form id="form1" runat="server">
   
        <asp:panel id="pnlCabecalho" Runat="server">
	        <P><br /><asp:label id="lblNavegacao" runat="server"/><br /></P>
	    </asp:Panel>
        <br />
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="false"/>
    <div>
        <asp:Panel ID="painel1" runat="server" Width="800px">
            <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                    <HeaderTemplate>
                        Definir par�metros
                    </HeaderTemplate>
                <ContentTemplate>
                    <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <ContentTemplate>
                       <asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0">
                            <asp:View id="View1" runat="server">
                                <asp:GridView id="gvParametros" runat="server" Width="100%" CssClass="grid-view" 
                                     EmptyDataText="Nenhum par�metro encontrado." BorderWidth="1px" 
                                     DataSourceID="obsParametros" AutoGenerateColumns="False" AllowPaging="True" 
                                     OnRowDataBound="OcultarColuna">
                                    
                                <HeaderStyle Font-Bold="False" CssClass="header" />
                                    
                                <Columns>                                    
                                    <asp:BoundField DataField="titulo" HeaderText="T&#237;tulo" SortExpression="titulo">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField DataField="criterio" />
                                    
                                    <asp:TemplateField HeaderText="Crit&#233;rio">
                                        <ItemTemplate>
                                                                                   
                                        <asp:DropDownList ID="ddlCriterio" runat="server">                    
	                                         <asp:ListItem Value="1">&gt;</asp:ListItem>
	                                         <asp:ListItem Value="2">&lt;</asp:ListItem>
	                                         <asp:ListItem Value="3">=</asp:ListItem>
	                                         <asp:ListItem Value="4">&gt;=</asp:ListItem>
	                                         <asp:ListItem Value="5">&lt;=</asp:ListItem>
                                        </asp:DropDownList>
                                        
                                        </ItemTemplate>
                                        
                                        <HeaderStyle HorizontalAlign="Center" Width="120px" />
                                        <ItemStyle HorizontalAlign="Left" Width="120px" />
                                        <ControlStyle CssClass="TemplateControl" />
                                    </asp:TemplateField>
                                    
                                    <asp:BoundField DataField="quantidade" />
                                    
                                    <asp:TemplateField HeaderText="Quantidade">
                                        <ItemTemplate>
                                        
                                        <asp:TextBox ID="txtQuantidade" runat="server" MaxLength="6"
                                             Text='<%# Eval("quantidade") %>' />
					                    <cc1:FilteredTextBoxExtender ID="ftxtQtde" runat="server" FilterType="Numbers" 
						                     ValidChars="," TargetControlID="txtQuantidade" />
                                                                                    
                                        </ItemTemplate>
                                        
                                        <HeaderStyle HorizontalAlign="Center" Width="120px" />
                                        <ItemStyle HorizontalAlign="Left" Width="120px" />                                            
                                        <ControlStyle CssClass="TemplateControl" />
                                    </asp:TemplateField>
                                    
                                    <asp:BoundField DataField="monitoramento_parametro_id" />
                                    
                                </Columns>
                                    
                                <PagerStyle CssClass="header" ForeColor="White" />
                                <SelectedRowStyle BackColor="DeepSkyBlue" Font-Bold="True" Font-Italic="False" />
                                <EmptyDataRowStyle Font-Bold="True" ForeColor="Red" />
                                <RowStyle CssClass="normal" />
                                <AlternatingRowStyle CssClass="alternate" />
                                <FooterStyle ForeColor="#C0FFFF" />
                                    
                                </asp:GridView>
                                <asp:ObjectDataSource ID="obsParametros" runat="server" SelectMethod="Select_DataTableParametros"
                                    TypeName="SEGW0187.cMonitoramento+ParametrosDal">
                                </asp:ObjectDataSource>
                                <br />
                                <div align="right">
                                    <asp:Button id="btnGravar" runat="server" Width="80px" CssClass="Botao" Text="Gravar" OnClick="btnGravar_Click" />
                                    <asp:Button id="btnSair" runat="server" Width="80px" CssClass="Botao" Text="Sair" OnClick="btnSair_Click" />
                                    
                                </div>                                
                            </asp:View>
                        </asp:MultiView>                                 
                    </ContentTemplate>
                    </asp:UpdatePanel> 
                </ContentTemplate>
                     
                </cc1:TabPanel>
            
                <cc1:TabPanel ID="tabUsuarios" runat="server" HeaderText="Definir Usu�rios">
                        <HeaderTemplate>
                            Definir Usu�rios
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="updGridUsuarios" runat="server">
                                <ContentTemplate>
                                    <asp:MultiView ID="MultiViewUsuarios" runat="server" ActiveViewIndex="0">
                                        <asp:View ID="ViewGridUsuarios" runat="server">
                                          <asp:GridView ID="gvUsuarios" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                DataSourceID="odsUsuarios" Width="100%" BorderWidth="1px"
                                                EmptyDataText="Nenhum usu�rio encontrado." CssClass="grid-view"
                                                DataKeyNames="monitoramento_usuario_id" >
                                                <HeaderStyle Font-Bold="False" CssClass="header" />
                                                <Columns>
                                                    <asp:BoundField DataField="login" HeaderText="Login">
                                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                                        <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="nome" HeaderText="Nome">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="CPF">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtCPF" runat="server" Text='<%# Bind("CPF") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCPF" runat="server" Text='<%# CPF_Format(DataBinder.Eval(Container,"DataItem.CPF")) %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="120px" />
                                                        <ItemStyle HorizontalAlign="Left" Width="120px" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="email" HeaderText="E-mail">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    
                                                     <asp:TemplateField HeaderText="Consulta">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="ckbConsultaUsuario"  runat="server"  checked='<%# format_checkbox(DataBinder.Eval(Container,"DataItem.permissao_consulta"))%>' Enabled="false"  />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="60px" />
                                                    </asp:TemplateField>         
                                                    
                                                    <asp:TemplateField HeaderText="Administra&#231;&#227;o">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="ckbAdministracaoUsuario" runat="server" checked='<%# format_checkbox(DataBinder.Eval(Container,"DataItem.permissao_administracao"))%>' Enabled="false"  />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="60px" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="monitoramento_usuario_id" Visible="False"/>
                                                                                 
                                                </Columns>
                                                <PagerStyle CssClass="header" ForeColor="White" />
                                                <SelectedRowStyle BackColor="DeepSkyBlue" Font-Bold="True" Font-Italic="False" />
                                                <EmptyDataRowStyle Font-Bold="True" ForeColor="Red" />
                                                <RowStyle CssClass="normal" />
                                                <AlternatingRowStyle CssClass="alternate" />
                                                <FooterStyle ForeColor="#C0FFFF" />
                                            </asp:GridView>
                                            <asp:ObjectDataSource ID="odsUsuarios" runat="server" SelectMethod="Select_DataTableUsuarios"
                                                TypeName="SEGW0187.cMonitoramento+UsuariosDal" UpdateMethod="Alterar_Usuario">
                                            </asp:ObjectDataSource>
                                            <br />
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                                                <tr>
                                                    <td align="right" style="height: 17px">
                                                        <asp:Button ID="btnIncluir" runat="server" CssClass="Botao" Text="Incluir" Width="80px" CommandName="Insert" OnClientClick="top.exibeaguarde()"/>
                                                        <asp:Button ID="btnSairUsuarios" runat="server" CssClass="Botao" Text="Sair" Width="80px" OnClick="btnSair_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                        <asp:View ID="ViewCadastroUsuarios" runat="server">
                                            <asp:Panel ID="pnUsuarios" runat="server" GroupingText="Usu�rio" Width="100%" ForeColor="#003399">
                                                <asp:UpdatePanel ID="updCadastroUsuarios" runat="server">
                                                    <ContentTemplate>
                                                        <asp:FormView ID="fvUsuario"  DataKeyNames="login" runat="server" DefaultMode="Insert" Width="100%" DataSourceID="odsFvUsuario">
                                                            <InsertItemTemplate>
                                                                <table border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td width="130" align="right">
                                                                            <asp:Label ID="lblNome" runat="server" Text="Nome:" CssClass="input"></asp:Label>
                                                                            <asp:RequiredFieldValidator ID="rfvNome" runat="server" ControlToValidate="txtNome"
                                                                                Display="None" ErrorMessage=" - Nome � obrigat�rio." SetFocusOnError="True" ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator></td>
                                                                        <td width="10">
                                                                        </td>
                                                                        <td width="450">
                                                                            <asp:TextBox ID="txtNome" runat="server" MaxLength="100" Text="<%# bind('nome') %>"
                                                                                Width="90%"></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:Label ID="lblCPF" runat="server" Text="CPF:" CssClass="input"></asp:Label>
                                                                            <asp:RequiredFieldValidator ID="rfvCPF" runat="server" ControlToValidate="txtCPF"
                                                                                Display="None" ErrorMessage=" - CPF � obrigat�rio." SetFocusOnError="True" ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtCPF" runat="server" MaxLength="11" OnTextChanged="txtCPF_TextChanged" AutoPostBack="true"
                                                                                Text="<%# bind('CPF') %>" Width="120px"></asp:TextBox>
                                                                            <asp:CustomValidator ID="cvCPF" runat="server" ClientValidationFunction="valida_CPFCNPJ"
                                                                                ControlToValidate="txtCPF" ErrorMessage=" - CPF inv�lido." SetFocusOnError="True"
                                                                                ValidateEmptyText="True" ValidationGroup="grpUsuario">Inv�lido.</asp:CustomValidator></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:Label ID="lblLogin" runat="server" Text="Login:" CssClass="input"></asp:Label>
                                                                            <asp:RequiredFieldValidator ID="rfvLogin" runat="server" ControlToValidate="txtLogin"
                                                                                Display="None" ErrorMessage=" - Login � obrigat�rio." SetFocusOnError="True"
                                                                                ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator></td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtLogin" runat="server" MaxLength="32" Text="<%# bind('login') %>"
                                                                                Width="100px"></asp:TextBox>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:Label ID="lblEmail" runat="server" Text="E-mail:" CssClass="input"></asp:Label>
                                                                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                                                                                Display="None" ErrorMessage=" - E-mail � obrigat�rio." SetFocusOnError="True"
                                                                                ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator></td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="60" Text="<%# bind('Email') %>"
                                                                                Width="90%"></asp:TextBox>
                                                                            <asp:RegularExpressionValidator ID="revEmail" runat="server" 
                                                                                 ErrorMessage="- E-mail inv�lido." ControlToValidate="txtEmail" 
                                                                                 Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                                 ValidationGroup="grpUsuario">Informe um e-mail v�lido</asp:RegularExpressionValidator></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:Label ID="lblEmail2" runat="server" Text="Confirma e-mail:" CssClass="input"></asp:Label>
                                                                            <asp:RequiredFieldValidator ID="rfvEmail2" runat="server" ControlToValidate="txtEmail2"
                                                                                Display="None" ErrorMessage=" - Confirma��o de email � obrigat�rio." SetFocusOnError="True"
                                                                                ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator><asp:CompareValidator
                                                                                    ID="cvEmail" runat="server" ControlToCompare="txtEmail2" ControlToValidate="txtEmail"
                                                                                    ErrorMessage="Emails diferentes." SetFocusOnError="True" ValidationGroup="grpUsuario">*</asp:CompareValidator></td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtEmail2" runat="server" MaxLength="60" Width="90%"></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:Label ID="lblConsulta" runat="server" Text="Consulta"></asp:Label>
                                                                        </td> 
                                                                        <td>
                                                                        </td>  
                                                                        <td><asp:CheckBox ID="ckbConsultaUsuario" runat="server"  /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:Label ID="lblAdministracao" runat="server" Text="Administra��o"></asp:Label>
                                                                        </td> 
                                                                        <td>
                                                                        </td>  
                                                                        <td><asp:CheckBox ID="ckbAdministracaoUsuario" runat="server" /></td>
                                                                    </tr>
                                                                </table>
                                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List"
                                                                    HeaderText="Campos com erro de preenchimento ou de  obrigat�riedade:" ValidationGroup="grpUsuario"
                                                                    ShowMessageBox="True" />
                                                                &nbsp;<br />
                                                                <br />
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="right">
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:Button ID="btnIncluir" OnClick="btnIncluirClick" runat="server" Text="Incluir" Width="80px" CommandName="Insert"
                                                                                CssClass="Botao" ValidationGroup="grpUsuario"/>
                                                                            <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" Width="80px" CssClass="Botao"
                                                                                OnClick="btnCancelar_Click" /></td>                                                                             
                                                                            <asp:TextBox runat="server" id="valida" Text='<%# Session("valida") %>' style="display:none;" />
                                                                    </tr>          
                                                                </table>
                                                            </InsertItemTemplate>
                                                            <EditItemTemplate>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td width="130" align="right">
                                                                            <asp:Label ID="lblNome" runat="server" Text="Nome:"></asp:Label>
                                                                            <asp:RequiredFieldValidator ID="rfvNome" runat="server" ControlToValidate="txtNome"
                                                                                Display="None" ErrorMessage=" - Nome � obrigat�rio." SetFocusOnError="True" ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator></td>
                                                                        <td width="10" style="color: #000000">
                                                                        </td>
                                                                        <td style="color: #000000">
                                                                            <asp:TextBox ID="txtNome" runat="server" MaxLength="100" Text="<%# bind('nome') %>"
                                                                                Width="90%"></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr style="color: #000000">
                                                                        <td align="right">
                                                                            <asp:Label ID="lblCPF" runat="server" Text="CPF:"></asp:Label>
                                                                            <asp:RequiredFieldValidator ID="rfvCPF" runat="server" ControlToValidate="txtCPF"
                                                                                Display="None" ErrorMessage=" - CPF � obrigat�rio." SetFocusOnError="True" ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator></td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtCPF" runat="server" Enabled="False" MaxLength="11" Text="<%# bind('CPF') %>" Width="120px"></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:Label ID="lblLogin" runat="server" Text="Login:"></asp:Label>
                                                                            <asp:RequiredFieldValidator ID="rfvLogin" runat="server" ControlToValidate="txtLogin"
                                                                                Display="None" ErrorMessage=" - Login � obrigat�rio." SetFocusOnError="True"
                                                                                ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator></td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtLogin" runat="server" Enabled="False" MaxLength="10" Text="<%# bind('login') %>"
                                                                                Width="100px"></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:Label ID="lblEmail" runat="server" Text="E-mail:"></asp:Label>
                                                                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                                                                                Display="None" ErrorMessage=" - E-mail � obrigat�rio." SetFocusOnError="True"
                                                                                ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator></td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="60" Text="<%# bind('Email') %>"
                                                                                Width="90%"></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:Label ID="lblEmail2" runat="server" Text="Confirma e-mail:"></asp:Label>
                                                                            <asp:RequiredFieldValidator ID="rfvEmail2" runat="server" ControlToValidate="txtEmail2"
                                                                                Display="None" ErrorMessage=" - Confirma��o de email � obrigat�rio." SetFocusOnError="True"
                                                                                ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator>
                                                                            <asp:CompareValidator ID="cvEmail" runat="server" ControlToCompare="txtEmail" ControlToValidate="txtEmail2"
                                                                                ErrorMessage="Emails diferentes." SetFocusOnError="True" ValidationGroup="grpUsuario">*</asp:CompareValidator></td>
                                                                        <td>
                                                                        </td>
                                                                        <td>                                                                           
                                                                            <asp:TextBox ID="txtEmail2" runat="server" MaxLength="60" Text='<%# Eval("Email") %>'
                                                                                Width="90%"></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                           <asp:CheckBox ID="ckbConsultaUsuario"  checked='<%# format_checkbox(DataBinder.Eval(Container,"DataItem.permissao_consulta"))%>' runat="server" /></td>
                                                                        </td> 
                                                                        <td>
                                                                        </td>  
                                                                        <td><asp:Label ID="lblConsulta" runat="server" Text="Consulta"></asp:Label>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:CheckBox   ID="ckbAdministracaoUsuario" runat="server" checked='<%# format_checkbox(DataBinder.Eval(Container,"DataItem.permissao_administracao"))%>' />
                                                                        </td>
                                                                        <td>
                                                                        </td>   
                                                                        <td> 
                                                                           <asp:Label ID="lblAdministracaoUsuario" runat="server" Text="Administra��o"></asp:Label>
                                                                        </td>
                                                                     </tr>
                                                                </table>
                                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List"
                                                                    HeaderText="Campos com erro de preenchimento ou de  obrigatoriedade:" ValidationGroup="grpUsuario" />
                                                                <br />
                                                                <br />
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="right">
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:Button ID="btnIncluir" runat="server" Text="Alterar" Width="80px" CommandName="Update"
                                                                                CssClass="Botao" ValidationGroup="grpUsuario" />                                                                        
                                                                            <asp:Button ID="btnExcluir"  runat="server" Text="Excluir" Width="80px" CommandName="Delete"
                                                                                CssClass="Botao"  OnClientClick='<%# Eval("login","return confirm(""Deseja excluir o login {0} de nosso cadastro?"")")%>' />                                                                                                    
                                                                            <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" Width="80px" CssClass="Botao"
                                                                                OnClick="btnCancelar_Click" />
                                                                        </td>                                                                       
                                                                    </tr>
                                                                </table>
                                                                <br />
                                                                <br />
                                                                <br />
                                                                <br />
                                                            </EditItemTemplate>
                                                        </asp:FormView>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="gvUsuarios" EventName="SelectedIndexChanged" />
                                                        <asp:AsyncPostBackTrigger ControlID="btnIncluir" EventName="Click" /> 
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                                <asp:ObjectDataSource ID="odsFvUsuario" runat="server" 
                                                    SelectMethod="Select_DataTableFvUsuario" TypeName="SEGW0187.cMonitoramento+UsuariosDal" DeleteMethod="Apagar_Usuario" InsertMethod="Incluir_Usuario" UpdateMethod="Alterar_Usuario">
                                                    <DeleteParameters>
                                                         <asp:Parameter Name="login" Type="String" />
                                                    </DeleteParameters>
                                                    <SelectParameters>
                                                        <asp:ControlParameter ControlID="gvUsuarios" Name="MonitoramentoUsuarioId" PropertyName="SelectedDataKey(0)"
                                                            Type="Int32" DefaultValue="NULL" />
                                                    </SelectParameters>
                                                    <UpdateParameters>
                                                        <asp:Parameter Name="nome" Type="String" />
                                                        <asp:Parameter Name="CPF" Type="String" />
                                                        <asp:Parameter Name="email" Type="String" />
                                                        <asp:Parameter Name="login" Type="String" />
                                                    </UpdateParameters>
                                                    <InsertParameters>
                                                        <asp:Parameter Name="nome" Type="String" />
                                                        <asp:Parameter Name="CPF" Type="String" />
                                                        <asp:Parameter Name="email" Type="String" />
                                                        <asp:Parameter Name="login" Type="String" />
                                                    </InsertParameters>
                                                </asp:ObjectDataSource>
                                            </asp:Panel>
                                        </asp:View>
                                    </asp:MultiView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </cc1:TabPanel>
            </cc1:TabContainer> 
        </asp:Panel>
        <asp:HiddenField ID="hdnAmbienteId" runat="server" />
    </div>
                                            
    </form>
</body>

<script language="javascript" type="text/javascript">
    if (typeof top.escondeaguarde === "function") {
        top.escondeaguarde();
    }
</script>

<script language="javascript" type="text/javascript">
            var valida = document.getElementById("TabContainer1_tabUsuarios_fvUsuario_valida");
            
            if (valida != null){
                if (valida.value === 'sim'){
                    if (!confirm('Usu�rio cadastrado com sucesso. Efetuar novo cadastro?')){document.getElementById('TabContainer1_tabUsuarios_fvUsuario_btnCancelar').click()}else{document.getElementById('TabContainer1_tabUsuarios_fvUsuario_valida').value = 'nao';}
                }
            }
       
        function ValidaCPF1(p_oTxtCPF) {
            v_sValue = p_oTxtCPF.value;
            
            v_sValue = v_sValue.toString().replace( "-", "" );
	        v_sValue = v_sValue.toString().replace( ".", "" );
	        v_sValue = v_sValue.toString().replace( ".", "" );
	        v_sValue = v_sValue.toString().replace( "/", "" );   
	        
	        if (v_sValue != '11111111111'){
	            return true;	            
	        }
	        else {
	            p_oTxtCPF.value = '';
	            p_oTxtCPF.focus();
	            alert('CPF com valor 11111111111 n�o pode ser cadastrado.')
	            return false;
	        }
        }

        // Valida��o de CPF e CNPJ
        function valida_CPFCNPJ(oSrc,args){
            s = args.Value;
            s = s.toString().replace( "-", "" );
	        s = s.toString().replace( ".", "" );
	        s = s.toString().replace( ".", "" );
	        s = s.toString().replace( "/", "" );
	        	        
            if (s.length == 11) {
                valida_CPF(oSrc,args);
            }
            else if(s.length == 11) {
               valida_CNPJ(oSrc, args);
            }
            else {
                return args.IsValid = false;
            }
        }

        //Valida��o de CPF
        function valida_CPF(oSrc,args){
            
            s = args.Value;
            //args.isValid = (s >= 3);
            //document.write(oSrc.Value + ',' + args.Value);
            
            s = s.toString().replace( "-", "" );
	        s = s.toString().replace( ".", "" );
	        s = s.toString().replace( ".", "" );
	        s = s.toString().replace( "/", "" );        
           
            if (isNaN(s)) {
                return args.IsValid = false;
            }

            var i;
            var c = s.substr(0,9);
            var dv = s.substr(9,2);
            var d1 = 0;

            for (i = 0; i < 9; i++) {
                d1 += c.charAt(i)*(10-i);
            }

            if (d1 == 0){
                return args.IsValid = false;
            } 

            d1 = 11 - (d1 % 11);

            if (d1 > 9) d1 = 0; 

            if (dv.charAt(0) != d1) {
                return args.IsValid = false; 
            }

            d1 *= 2;

            for (i = 0; i < 9; i++) {
                d1 += c.charAt(i)*(11-i);
            }

            d1 = 11 - (d1 % 11);

            if (d1 > 9) d1 = 0;

            if (dv.charAt(1) != d1) {
                return args.IsValid = false;
            }

            __doPostBack('TabContainer1_TabPanel1_fvUsuario_txtCPF', '');
            return args.IsValid = true;
            
            alert(args.value);
        } 

        //Valida��o de CNPJ
        function valida_CNPJ(oSrc, args){
            s = args.Value;
            
            s = s.toString().replace( "-", "" );
	        s = s.toString().replace( ".", "" );
	        s = s.toString().replace( ".", "" );
	        s = s.toString().replace( "/", "" );
	        
            if (isNaN(s)) {
                return args.IsValid = false;
            }
            alert(s);
            var i;
            var c = s.substr(0,12);
            var dv = s.substr(12,2);
            var d1 = 0;

            for (i = 0; i <12; i++){
                d1 += c.charAt(11-i)*(2+(i % 8));
            }

            if (d1 == 0) 
                return args.IsValid = false;

            d1 = 11 - (d1 % 11);

            if (d1 > 9) d1 = 0;

            if (dv.charAt(0) != d1){
                return args.IsValid = false;
            }

            d1 *= 2;

            for (i = 0; i < 12; i++){
                d1 += c.charAt(11-i)*(2+((i+1) % 8));
            }

            d1 = 11 - (d1 % 11);

            if (d1 > 9) 
            d1 = 0;

            if (dv.charAt(1) != d1){
                return args.IsValid = false;
            }

            return args.IsValid = true;

        } 

        function onKeyDown() {
          // current pressed key
          var pressedKey = String.fromCharCode(event.keyCode).toLowerCase();
            
          if (event.ctrlKey && (pressedKey == "c" || 
                                pressedKey == "v")) {
                                //alert(pressedKey);
            // disable key press porcessing
            event.returnValue = false;
          }

        } // onKeyDown
        
        function LimparCPF(p_oCPF) {
            var wVr = p_oCPF.value;
            
            wVr = wVr.toString().replace( "-", "" );
	        wVr = wVr.toString().replace( ".", "" );
	        wVr = wVr.toString().replace( ".", "" );
	        wVr = wVr.toString().replace( "/", "" );
	        
	        p_oCPF.value = wVr;	        
        }
        
        function FormataCPF_Onblur(p_oCPF) {
             var wVr = p_oCPF.value;
             var v_sCPF = wVr.substr(0,3) + '.' + wVr.substr(3,3) + '.' + wVr.substr(6,3) + '-' + wVr.substr(9,2);
             p_oCPF.value = v_sCPF;
        }
        
        function CPF_TextChanged(p_oTextCPF){
            var wVr = p_oTextCPF.value;
            
            wVr = wVr.toString().replace( "-", "" );
	        wVr = wVr.toString().replace( ".", "" );
	        wVr = wVr.toString().replace( ".", "" );
	        wVr = wVr.toString().replace( "/", "" );
                alert(wVr);	        
            if (wVr.lenght == 11){
                if (wVr != '11111111111'){
                    alert(p_oTextCPF.id)
                    __doPostBack('TabContainer1_TabPanel1_fvUsuario_txtCPF', 'TextChanged');
                }
                else {
                    alert('CPF com valor 11111111111 n�o pode ser cadastrado.');
                }
            }        
        }
        
    
</script>
</html>