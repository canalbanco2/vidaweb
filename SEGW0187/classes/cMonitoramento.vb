Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports SEGL0315
Imports Alianca
Imports Alianca.FrameWork
Imports Alianca.Seguranca.BancoDados.cCon

Public Class cMonitoramento

    Public Class Parametros

#Region "Membros Privados"

        Private m_oRN As ClsParametroMonitoramentoWebRN

#End Region

#Region "Propriedades P�blicas"

        Public Shadows ReadOnly Property RN() As ClsParametroMonitoramentoWebRN
            Get
                If IsNothing(m_oRN) Then
                    m_oRN = New ClsParametroMonitoramentoWebRN
                End If
                Return m_oRN
            End Get
        End Property

#End Region


    End Class

    Public Class ParametrosDal

        Public Function Select_DataTableParametros() As DataTable
            Dim v_aArrayParametros As New ArrayList
            Dim v_oDtParametros As DataTable

            Dim parametro As New Parametros

            v_oDtParametros = parametro.RN.Listar(v_aArrayParametros).Tables(0)

            Return v_oDtParametros

        End Function

        Public Sub Update_Parametros(ByVal MonitoramentoParametroId As Integer, ByVal Criterio As String, _
                                     ByVal Quantidade As Integer)
            Dim parametro As New Parametros

            parametro.RN.MonitoramentoParametroId = MonitoramentoParametroId
            parametro.RN.Criterio = Criterio
            parametro.RN.Quantidade = Quantidade

            parametro.RN.Alterar()

        End Sub

    End Class

    Public Class Usuarios

#Region "Membros Privados"

        Private m_oRN As ClsUsuarioMonitoramentoWebRN

#End Region

#Region "Propriedades P�blicas"

        Public Shadows ReadOnly Property RN() As ClsUsuarioMonitoramentoWebRN
            Get
                If IsNothing(m_oRN) Then
                    m_oRN = New ClsUsuarioMonitoramentoWebRN
                End If
                Return m_oRN
            End Get
        End Property

#End Region

    End Class

    Public Class UsuariosDal

        Public Function Select_DataTableUsuarios() As DataTable
            Dim v_aArrayParametros As New ArrayList
            Dim v_oDtUsuarios As DataTable

            Dim usuario As New Usuarios

            v_oDtUsuarios = usuario.RN.Listar(v_aArrayParametros).Tables(0)

            Return v_oDtUsuarios

        End Function

        Public Function Select_DataTableFvUsuario(ByVal MonitoramentoUsuarioId As Integer) As DataTable

            Dim v_aArrayParametros As New ArrayList
            Dim v_oDtUsuarios As DataTable

            Dim usuario As New Usuarios

            v_aArrayParametros.Add(New ParametroDB("@monitoramento_usuario_id", MonitoramentoUsuarioId))

            v_oDtUsuarios = usuario.RN.Listar(v_aArrayParametros).Tables(0)

            Return v_oDtUsuarios

        End Function

        Public Sub Alterar_Usuario(ByVal nome As String, _
                                   ByVal CPF As String, _
                                   ByVal email As String, _
                                   ByVal Usuario As String, _
                                   ByVal permissaoAdministracao As String, _
                                   ByVal permissaoConsulta As String, _
                                   ByVal login As String)

            Dim _usuario As New Usuarios
            Dim v_oClsUsuarioMonitoramentoWebRN As New ClsUsuarioMonitoramentoWebRN()
            v_oClsUsuarioMonitoramentoWebRN.CPF = CPF_Limpar(CPF)

            _usuario.RN.DAO.IniciarTransacao(Me)
            Try
                Dim v_oClsUsuarioAlterarRN As New ClsUsuarioRN(_usuario.RN.DAO)
                v_oClsUsuarioMonitoramentoWebRN.Nome = nome
                v_oClsUsuarioMonitoramentoWebRN.CPF = CPF_Limpar(CPF)
                v_oClsUsuarioMonitoramentoWebRN.Login = login
                v_oClsUsuarioMonitoramentoWebRN.Email = email
                v_oClsUsuarioMonitoramentoWebRN.Usuario = Usuario
                v_oClsUsuarioMonitoramentoWebRN.permissaoConsulta = permissaoConsulta
                v_oClsUsuarioMonitoramentoWebRN.permissaoAdministracao = permissaoAdministracao
                v_oClsUsuarioMonitoramentoWebRN.Alterar()

                _usuario.RN.DAO.FinalizarTransacao(True, Me)

            Catch ex As Exception
                _usuario.RN.DAO.FinalizarTransacao(False, Me)
                Throw ex
            End Try
        End Sub
        Public Sub Apagar_Usuario(ByVal Usuario As String, _
                                  ByVal login As String)

            Dim _usuario As New Usuarios
            Dim v_oClsUsuarioMonitoramentoWebRN As New ClsUsuarioMonitoramentoWebRN()
            _usuario.RN.DAO.IniciarTransacao(Me)
            Try
                Dim v_oClsUsuarioAlterarRN As New ClsUsuarioRN(_usuario.RN.DAO)
                v_oClsUsuarioMonitoramentoWebRN.Login = login
                v_oClsUsuarioMonitoramentoWebRN.Usuario = Usuario
                v_oClsUsuarioMonitoramentoWebRN.Excluir()

                _usuario.RN.DAO.FinalizarTransacao(True, Me)
            Catch ex As Exception
                 _usuario.RN.DAO.FinalizarTransacao(False, Me)
                Throw ex
            End Try
        End Sub

        Public Sub Incluir_Usuario(ByVal nome As String, _
                           ByVal CPF As String, _
                           ByVal email As String, _
                           ByVal Usuario As String, _
                           ByVal permissaoAdministracao As String, _
                           ByVal permissaoConsulta As String, _
                           ByVal login As String)



            Dim _usuario As New Usuarios
            Dim v_oClsUsuarioMonitoramentoWebRN As New ClsUsuarioMonitoramentoWebRN()
            v_oClsUsuarioMonitoramentoWebRN.CPF = CPF_Limpar(CPF)

            _usuario.RN.DAO.IniciarTransacao(Me)
            Try
                Dim v_oClsUsuarioAlterarRN As New ClsUsuarioRN(_usuario.RN.DAO)
                v_oClsUsuarioMonitoramentoWebRN.Nome = nome
                v_oClsUsuarioMonitoramentoWebRN.CPF = CPF_Limpar(CPF)
                v_oClsUsuarioMonitoramentoWebRN.Login = login
                v_oClsUsuarioMonitoramentoWebRN.Email = email
                v_oClsUsuarioMonitoramentoWebRN.Usuario = Usuario
                v_oClsUsuarioMonitoramentoWebRN.permissaoConsulta = permissaoConsulta
                v_oClsUsuarioMonitoramentoWebRN.permissaoAdministracao = permissaoAdministracao
                v_oClsUsuarioMonitoramentoWebRN.Incluir()

                _usuario.RN.DAO.FinalizarTransacao(True, Me)

            Catch ex As Exception
                _usuario.RN.DAO.FinalizarTransacao(False, Me)
                Throw ex
            End Try
        End Sub

        Private Function CPF_Limpar(ByVal cpf As String) As String
            Try
                cpf = cpf.ToString().Replace("-", "")
                cpf = cpf.ToString().Replace(".", "")
                cpf = cpf.ToString().Replace(".", "")
                cpf = cpf.ToString().Replace("/", "")

                Return cpf

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        
    End Class

   

End Class
