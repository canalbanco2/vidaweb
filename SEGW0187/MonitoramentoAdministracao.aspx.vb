Imports System.Data
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Globalization
Imports SEGL0315
Imports Alianca.FrameWork
Imports Alianca.Seguranca.BancoDados
Imports System.Security.Cryptography

Partial Public Class _Default
    Inherits PaginaBaseSEGW

#Region " Membros "
    Private m_oRN As ClsUsuarioMonitoramentoWebRN
#End Region

#Region " Propriedades "

    Public Shadows ReadOnly Property RN() As ClsUsuarioMonitoramentoWebRN
        Get
            If IsNothing(m_oRN) Then
                m_oRN = New ClsUsuarioMonitoramentoWebRN
            End If
            Return m_oRN
        End Get
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Implementa��o do LinkSeguro
        Dim linkseguro As New Alianca.Seguranca.Web.LinkSeguro

        Dim usuario As String = linkseguro.LerUsuario("MonitoramentoAdministracao.aspx", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))

        '------alterar ao terminar
        If usuario = "" Then
            Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
        End If

        Session("usuario") = linkseguro.Login_WEB
        '--------------------------------------------------

        'Implementa��o do controle de ambiente
        Dim cAmbiente As New Alianca.Seguranca.Web.ControleAmbiente

        If Not Page.IsPostBack Then
            Session("indice") = 0
            For Each m As String In Request.QueryString.Keys
                If m <> "" Then
                    Session(m) = Request.QueryString(m)
                End If
            Next
        Else
            Session("valida") = "nao"
        End If

        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
        cAmbiente.ObterAmbiente(url)
        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
        Else
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
        End If

        Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)

        If Not Page.IsPostBack Then
            lblNavegacao.Text = "Fun��o: Administra��o do painel de monitoramento do sistema Vida Web"
            TabContainer1.ActiveTabIndex = 1
        End If

        Session("user_inserted") = ""

        Me.hdnAmbienteId.Value = Me.AmbienteID

   
    End Sub

#Region " Eventos Botao "

    Protected Sub btnIncluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIncluir.Click
        Me.fvUsuario.ChangeMode(FormViewMode.Insert)
        Me.HelperSEGW.LimparForm(Me.fvUsuario.Controls)
        Me.MultiViewUsuarios.SetActiveView(Me.ViewCadastroUsuarios)

        DesativarEventoColar()
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.gvUsuarios.SelectedIndex = -1
        Me.gvUsuarios.DataBind()
        Me.MultiViewUsuarios.SetActiveView(Me.ViewGridUsuarios)
    End Sub

    Protected Sub btnGravar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim BancoDados As New cMonitoramento.ParametrosDal
        Dim GridRow As GridViewRow

        Try
            For Each GridRow In gvParametros.Rows
                If GridRow.RowType = DataControlRowType.DataRow Then

                    Dim ddlCriterio As DropDownList = gvParametros.Rows(GridRow.RowIndex()).FindControl("ddlCriterio")
                    Dim txtQuantidade As TextBox = gvParametros.Rows(GridRow.RowIndex()).FindControl("txtQuantidade")

                    Dim MonitoramentoParametroId As Integer = CType(GridRow.Cells(5).Text, Integer)
                    Dim Criterio As String = CType(ddlCriterio.Items.FindByValue(ddlCriterio.Text).Text, String)
                    Dim Quantidade As Integer = CType(txtQuantidade.Text, Integer)

                    BancoDados.Update_Parametros(MonitoramentoParametroId, Criterio, Quantidade)

                End If
            Next

            Me.HelperSEGW.GerarAlert(Me.Page, "Par�metros gravados com sucesso.", "ParametrosGravados")

        Catch ex As Exception

            Throw New Exception(ex.Message)

        End Try

    End Sub

    Public Sub btnIncluirClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim txtValida As TextBox = fvUsuario.FindControl("valida")
        txtValida.Text = "sim"
        Session("valida") = txtValida.Text
    End Sub

    Protected Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("../SEGW0060/Centro.aspx?usuario=" & Session("usuario"), True)
    End Sub

#End Region

#Region " Eventos GridView "

    Private Sub gvParametros_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvParametros.RowDataBound
        Dim ddlCriterio As DropDownList = e.Row.FindControl("ddlCriterio")

        Try
            Select Case e.Row.RowType

                Case DataControlRowType.DataRow

                    Dim Status As String = CType(e.Row.Cells(1).Text, String)

                    ddlCriterio.Items.FindByText(HttpUtility.HtmlDecode(Status)).Selected = True

            End Select

        Catch ex As Exception

            Throw New Exception(ex.Message)

        End Try

    End Sub
    Protected Sub gvUsuarios_RowDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvUsuarios.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Me.HelperSEGW.CarregarTrocaCorLinhaGridView(e, , HelperSEGW.EnumTipoCursor.Hand)
            e.Row.Attributes.Add("onClick", ClientScript.GetPostBackEventReference(Me.gvUsuarios, "Select$" + e.Row.RowIndex.ToString()))
        End If

        Dim v_olblCPF As Label = e.Row.FindControl("lblCPF")
        If Not IsNothing(v_olblCPF) Then
            v_olblCPF.Attributes.Add("style", "color: #000000;")
        End If

    End Sub

    Private Sub gvUsuarios_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvUsuarios.SelectedIndexChanged
        If Me.gvUsuarios.SelectedIndex >= 0 Then
            Me.MultiViewUsuarios.SetActiveView(Me.ViewCadastroUsuarios)

            Me.fvUsuario.ChangeMode(FormViewMode.Edit)
            Me.fvUsuario.DataBind()
            DesativarEventoColar()
        End If
    End Sub
#End Region

#Region " Eventos FormView "

    Private Sub odsFvUsuario_Updating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceMethodEventArgs) Handles odsFvUsuario.Updating
        SetarPermissoes(e)
    End Sub

    Private Sub odsFvUsuario_Inserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceMethodEventArgs) Handles odsFvUsuario.Inserting
        SetarPermissoes(e)
    End Sub

    Private Sub SetarPermissoes(ByVal e As ObjectDataSourceMethodEventArgs)

        Dim ckbConsulta As CheckBox = fvUsuario.FindControl("ckbConsultaUsuario")
        Dim ckbAdministracao As CheckBox = fvUsuario.FindControl("ckbAdministracaoUsuario")

        If ckbAdministracao.Checked Then
            e.InputParameters.Add("permissaoAdministracao", "s")
        Else
            e.InputParameters.Add("permissaoAdministracao", "n")
        End If

        If ckbConsulta.Checked Then
            e.InputParameters.Add("permissaoConsulta", "s")
        Else
            e.InputParameters.Add("permissaoConsulta", "n")
        End If
        e.InputParameters.Add("Usuario", Session("usuario"))
    End Sub

    Private Sub fvUsuario_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewDeletedEventArgs) Handles fvUsuario.ItemDeleted
        If IsNothing(e.Exception) Then
            Me.MultiViewUsuarios.SetActiveView(Me.ViewGridUsuarios)
            Me.gvUsuarios.SelectedIndex = -1
            Me.gvUsuarios.DataBind()
        Else
            Me.HelperSEGW.GerarAlert(Me.Page, "Ocorreu um erro durante a exclus�o deste usu�rio.", "ErroExclusao")
            e.ExceptionHandled = True
        End If
    End Sub

    Private Sub odsFvUsuario_Deleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceMethodEventArgs) Handles odsFvUsuario.Deleting
        e.InputParameters.Add("Usuario", Session("usuario"))
        Dim btnExcluir As Button = fvUsuario.FindControl("btnExcluir")
        Dim txtLogin As TextBox = fvUsuario.FindControl("txtLogin")
        btnExcluir.Attributes.Add("onClick", "javascript:if(!confirm('Deseja excluir o login " & txtLogin.Text & "de nosso cadastro?')) return false;")
    End Sub

    Private Sub fvUsuario_ItemInserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewInsertEventArgs) Handles fvUsuario.ItemInserting
        Me.RN.CPF = CPF_Limpar(DirectCast(DirectCast(sender, FormView).FindControl("txtCPF"), TextBox).Text)
        Me.RN.Login = DirectCast(DirectCast(sender, FormView).FindControl("txtLogin"), TextBox).Text
        Dim txtValida As TextBox = fvUsuario.FindControl("valida")
        If Me.RN.Verificar_CPF Then
            Me.HelperSEGW.GerarAlert(Me.Page, "Esse CPF j� consta em nosso cadastro.", "CPFExiste")
            txtValida.Text = "nao"
            e.Cancel = True
        ElseIf Me.RN.Verificar_Login Then
            Me.HelperSEGW.GerarAlert(Me.Page, "Esse login j� consta em nosso cadastro.", "LoginExiste")
            txtValida.Text = "nao"
            e.Cancel = True
        End If
    End Sub

    Private Sub fvUsuario_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewUpdatedEventArgs) Handles fvUsuario.ItemUpdated
        If IsNothing(e.Exception) Then
            Me.MultiViewUsuarios.SetActiveView(Me.ViewGridUsuarios)
            Me.gvUsuarios.SelectedIndex = -1
            Me.gvUsuarios.DataBind()
            ScriptManager.RegisterClientScriptBlock(Me.updCadastroUsuarios, Me.updCadastroUsuarios.GetType(), "JavaScript", "alert('Altera��o efetuada com sucesso.');", True)
        Else
            Me.HelperSEGW.GerarAlert(Me.Page, "Ocorreu um erro durante a atualiza��o deste usu�rio.", "ErroAtualiza��o")
            e.ExceptionHandled = True
            e.KeepInEditMode = True
        End If
    End Sub

    Private Sub fvUsuario_ModeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fvUsuario.ModeChanged
        DesativarEventoColar()
    End Sub

#End Region

#Region " Outros Metodos "

    Public Sub OcultarColuna(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvParametros.RowDataBound

        If e.Row.RowType = DataControlRowType.Header Or _
           e.Row.RowType = DataControlRowType.DataRow Or _
           e.Row.RowType = DataControlRowType.Footer Then

            e.Row.Cells(1).Visible = False
            e.Row.Cells(3).Visible = False
            e.Row.Cells(5).Visible = False

        End If

    End Sub
    Protected Sub txtCPF_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim v_otxtCPF As TextBox = DirectCast(sender, TextBox)
        If v_otxtCPF.Text.Length = 11 Then
            If Not v_otxtCPF.Text.Equals("11111111111") Then
                Dim v_aArrayParametros As New ArrayList
                Dim v_oClsUsuarioRN As New ClsUsuarioRN
                v_oClsUsuarioRN.CPF = v_otxtCPF.Text

                If v_oClsUsuarioRN.Obter Then
                    DirectCast(Me.fvUsuario.FindControl("txtLogin"), TextBox).Text = v_oClsUsuarioRN.LoginRede
                    DirectCast(Me.fvUsuario.FindControl("txtEmail"), TextBox).Text = v_oClsUsuarioRN.Email
                    DirectCast(Me.fvUsuario.FindControl("txtEmail2"), TextBox).Text = v_oClsUsuarioRN.Email
                Else
                    DirectCast(Me.fvUsuario.FindControl("txtLogin"), TextBox).Text = ""
                    DirectCast(Me.fvUsuario.FindControl("txtEmail"), TextBox).Text = ""
                    DirectCast(Me.fvUsuario.FindControl("txtEmail2"), TextBox).Text = ""
                End If

                v_otxtCPF.Text = CPF_Format(v_otxtCPF.Text)
            Else
                v_otxtCPF.Text = ""
                v_otxtCPF.Focus()
                ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel1, Me.UpdatePanel1.GetType(), "JavaScript", "//alert('CPF com valor 11111111111 n�o pode ser cadastrado.'); document.getElementById('" & v_otxtCPF.ClientID & "').focus();", True)
            End If
        End If
    End Sub

    Public Function CPF_Format(ByVal cpf As String) As String
        Try
            'Return cpf
            Dim iCpf As Int64 = Convert.ToInt64(cpf)

            Return String.Format("{0:000\.###\.###-##}", iCpf)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function CPF_Limpar(ByVal cpf As String) As String
        Try
            cpf = cpf.ToString().Replace("-", "")
            cpf = cpf.ToString().Replace(".", "")
            cpf = cpf.ToString().Replace(".", "")
            cpf = cpf.ToString().Replace("/", "")

            Return cpf

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function format_checkbox(ByVal valor As String) As Boolean
        Return valor = "s"
    End Function

    Public Function GerarSenha() As ArrayList
        Dim senha As String = Now.Hour.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0") & Now.Millisecond.ToString.PadLeft(2, "0")
        senha = senha.Substring(0, 8)

        Dim senhahash As String = GenerateHash(senha)
        Dim v_oArray As New ArrayList

        v_oArray.Add(New String(senha))
        v_oArray.Add(New String(senhahash))

        Return v_oArray

    End Function
    Private Function GenerateHash(ByVal SourceText As String) As String
        Dim Md5 As Object = Server.CreateObject("MD5Hash.MD5")
        Dim senhahash As String = Md5.Hash(SourceText & "")

        Return senhahash
    End Function

    Private Sub DesativarEventoColar()
        Dim v_oTxtEmail As TextBox = fvUsuario.FindControl("txtEmail")
        If Not IsNothing(v_oTxtEmail) Then
            v_oTxtEmail.Attributes.Add("onkeydown", "onKeyDown()")
        End If
        Dim v_oTxtEmail2 As TextBox = fvUsuario.FindControl("txtEmail2")
        If Not IsNothing(v_oTxtEmail2) Then
            v_oTxtEmail2.Attributes.Add("onkeydown", "onKeyDown()")
        End If
    End Sub

#End Region

End Class