//FUN��O PARA VALIDAR HORA DIGITADA
function ValidarHora(p_oObj) {
	try {
		if (p_oObj.value.length != 0) {
			if ((p_oObj.value.length > 5) || (p_oObj.value.length < 5)) {
				throw "Hora inv�lida.";
			}
			else {
				var v_iHoras = p_oObj.value.substring(0, 2);
				var v_iMinutos = p_oObj.value.substring(3, 5);

				if ((v_iHoras > 24) || (v_iMinutos > 59)) {
					throw "Hora inv�lida.";
				}
			}
		}
	}
	catch(e) {
		alert(e);
		p_oObj.value = "";
		p_oObj.focus();
	}
}

//FUN��O PARA VALIDAR QUANTIDADE DE HORAS DIGITADA
function ValidarQuantidadeHoras(p_oObj) {
	try {
		if (p_oObj.value.length != 0) {
			if (p_oObj.value.length < 3) {
				throw "Quantidade de Horas inv�lida.";
			}
			else {
				var v_iMinutos = p_oObj.value.substring(p_oObj.value.indexOf(":") + 1, p_oObj.value.length);

				if (v_iMinutos > 59) {
					throw "Quantidade de Horas inv�lida.";
				}
			}
		}
	}
	catch(e) {
		alert(e);
		p_oObj.value = "";
		p_oObj.focus();
	}
}

//FUN��O PARA VALIDAR A DATA
function ValidarData(p_oObjeto) {	
	try {
		if (p_oObjeto.value != "")
			if (ExecutarValidacaoData(p_oObjeto.value) == null)
				throw "Data inv�lida.";
	}
	catch(e) {
		alert(e);
		p_oObjeto.value = "";
		p_oObjeto.focus();
	}
}

//SE FOR UMA DATA INV�LIDA, ENT�O A FUN��O RETORNAR� NULL
function ExecutarValidacaoData(p_sData) {	
	return CompararDatas(p_sData, "01/01/1753");
}

//FUN��O QUE FAZ A COMPARA��O ENTRA AS DATAS PASSADAS.
//SE A DATA MAIOR ESTIVER MENOR, ENT�O A FUN��O RETORNAR� NULO.
function CompararDatas(p_sDataMaior, p_sDataMenor) {
	try {
		var v_sData = new String(p_sDataMaior);
		var v_sDataFormatada = parseInt(v_sData.substr(3,2), 10) + "/" + parseInt(v_sData.substr(0,2), 10) + "/" + parseInt(v_sData.substr(6,4), 10);
		var v_oObjData = new Date(v_sDataFormatada);

		//SE p_sDataMenor FOR PASSADO, ENT�O VERIFICAR SE A DATA MAIOR REALMENTE � A MAIOR,
		//SE N�O FOR, ENT�O RETORNAR� NULL
		if (p_sDataMenor != "" && p_sDataMenor != null) {
			var v_sDataMenor = new String(p_sDataMenor);
			var v_sDataMenorFormatada = parseInt(v_sDataMenor.substr(3,2), 10) + "/" + parseInt(v_sDataMenor.substr(0,2), 10) + "/" + parseInt(v_sDataMenor.substr(6,4), 10);
			var v_oObjDataMenor = new Date(v_sDataMenorFormatada);

			if (v_oObjData < v_oObjDataMenor)
				return null;
		}
		if (((v_oObjData.getUTCMonth() + 1) + "/" + v_oObjData.getUTCDate() + "/" + v_oObjData.getUTCFullYear()) != v_sDataFormatada)
			return null;
		else 
			return v_oObjData;
	}
	catch(e) {
		return null;
	}
}
