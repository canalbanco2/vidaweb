//GERAL
function Navegacao(p_sURL) {
	document.all.IFrameConteudoPrincipal.src = p_sURL;
}

function EscreverCaminho(p_sCaminho) {
	try {
		if (p_sCaminho != null && ObjExist(document.all.lblCaminhoTela)) {
			document.all.lblCaminhoTela.innerHTML = unescape(p_sCaminho);
			return;
		}
		if (ObjExist(document.all.lblCaminhoTela) && ObjExist(top.frames[0].document.all._Menu_hdnItemMenuCorrete)) {
			if (document.all.lblCaminhoTela.innerHTML.indexOf("<SCRIPT") == 2) {
				var v_sNomeProduto = top.frames[0].document.all._Menu_hdnItemMenuCorrete.value;

				v_sNomeProduto = v_sNomeProduto.substring(v_sNomeProduto.indexOf(" -"), v_sNomeProduto.length);
				v_sNomeProduto = top.frames[0].document.all.hdnNomeProduto.value + v_sNomeProduto;

				//document.all.lblCaminhoTela.innerHTML = top.frames[0].document.all._Menu_hdnItemMenuCorrete.value;
				document.all.lblCaminhoTela.innerHTML = v_sNomeProduto;
			}
		}
	}
	catch(e) {}
}




//ABRIR AS MODAIS DE PESQUISA
function AbrirPesquisa(p_sUrl, p_sPesquisaDefault, p_sTituloTela, p_sChave) {
	return window.showModalDialog(p_sUrl + "?pqsPesquisaDefault=" + p_sPesquisaDefault + "&pqsTituloTela=" + p_sTituloTela + "&pqsSeguranca=" + p_sChave, window.self, "dialogHeight:350px;dialogWidth:785px;px;help:off;resizable:on;status:off;scroll:off;center:on"); 
}

//FECHAR APLICACAO
function Sair() {
	if (Confirmar('Confirma o fechamento da aplica��o?')) {
		CloseWindow('');
	}
}

function ControlarPost() {
	if (ObjExist(document.all.spanAguardePagina)) {
		document.all.spanAguardePagina.style.display = "block";
	}

	DesabilitarBotoesFormulario();
}

function SelecionarItem(p_iIdRegistro) {
	document.forms[0].hdnIdRegistro.value = p_iIdRegistro;
	HabilitarBotoes();
}

function VerificarSelecaoItem() {
	if (Trim(document.forms[0].hdnIdRegistro.value).length <= 0) {
		alert("Selecione um registro!");
		return false;
	}

	return true;
}

function Gravar() {
	return VerificarPreenchimentoObrigatorio();
}

function ExcluirRegistro() {
	if (VerificarSelecaoItem()) {
		if (confirm("Deseja realmente excluir este registro?")) {
			return true;
		}
	}

	return false;
}

function EnterConsulta() {
	if (ObjExist(document.forms[0].btnConsultar)) {
		FireByEnter(document.forms[0].btnConsultar);
	}
	else if (ObjExist(document.forms[0].btnConsultaAvancadaConsultar)) {
		FireByEnter(document.forms[0].btnConsultaAvancadaConsultar);
	}
}

function HabilitarBotoes() {
	
	if (ObjExist(document.forms[0].btnConsultarRegistro))
		document.forms[0].btnConsultarRegistro.disabled = false;
	
	if (ObjExist(document.forms[0].btnAlterarRegistro))
		document.forms[0].btnAlterarRegistro.disabled = false;

	if (ObjExist(document.forms[0].btnExcluirRegistro)) {
		document.forms[0].btnExcluirRegistro.disabled = false;
	}
}

function ExibirAguarde() {
	if (ObjExist(document.all.spanAguardePagina)) {
		document.all.spanAguardePagina.style.display = "block";
	}
}

function EsconderAguarde() {
	if (ObjExist(document.all.spanAguardePagina)) {
		document.all.spanAguardePagina.style.display = "none";
	}
}

//DESABILITA A VISUALIZACAO DO C�DIGO FONTE
if (window.Event)
document.captureEvents(Event.MOUSEUP); 
function nocontextmenu() 
{ 
event.cancelBubble = true 
event.returnValue = false; 

return false; 
} 
function norightclick(e) 
{ 

if (window.Event) 
{ 
if (e.which == 2 || e.which == 3) 
return false; 
} 
else 
if (event.button == 2 || event.button == 3) 
{ 
event.cancelBubble = true 
event.returnValue = false; 
return false;
}  
}
/**/
document.oncontextmenu = nocontextmenu; 
document.onmousedown = norightclick;