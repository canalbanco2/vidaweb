//FUN��O PARA IDENTIFICAR SE O BROWSER N�O � O INTERNET EXPLORER
function VerificarBrowserIE() {
	if (navigator.appName == "Microsoft Internet Explorer") {
		return true;
	}
	else {
		return false;
	}
}

//FUN��O PARA VERIFICAR SE UM N�MERO � MENOR QUE O VALOR DESEJADO
function MenorQue(p_sNumero, p_sNumeroComparacao) {
	var v_fNumero = ConverteFloat(p_sNumero);
	var v_fNumeroComparacao = ConverteFloat(p_sNumeroComparacao);

	if (isNaN(v_fNumero) || isNaN(v_fNumeroComparacao)) return;

	if (v_fNumero < v_fNumeroComparacao) return true;

	return false;
}

//FUN��O PARA VERIFICAR SE UM N�MERO � MENOR QUE O VALOR DESEJADO
function MaiorQue(p_sNumero, p_sNumeroComparacao) {
	var v_fNumero = ConverteFloat(p_sNumero);
	var v_fNumeroComparacao = ConverteFloat(p_sNumeroComparacao);

	if (isNaN(v_fNumero) || isNaN(v_fNumeroComparacao)) return;

	if (v_fNumero > v_fNumeroComparacao) return true;

	return false;
}

//FUN��O PARA TRANSFORMAR O N�MERO EM FLOAT
function ConverteFloat(p_sNumero) {
	return parseFloat(ReplaceString(ReplaceString(p_sNumero, ".", ""), ",", "."));
}

//FUN��O PARA CONFIRMAR SE A STRING PASSADA � UM N�MERO
function ConfirmaNumero(p_sString) {
	var v_iValorTestado = parseFloat(p_sString);

	if (isNaN(v_iValorTestado)) return false;

	return true;
}

//FUN��O QUE OBTEM O CODIGO ASCII DA TECLA PRESSIONADA NO TECLADO
function ObterCodigoTecla(p_eEvento) {
	if (VerificarBrowserIE()) {
		return window.event.keyCode;
	}
	else {
		return p_eEvento.which;
	}
}

//FUN��O PARA REDIRECIONAR PARA OUTRA P�GINA
function Redirecinamento(p_sPaginaDestino) {
	self.document.location.href = p_sPaginaDestino;
}

//FUN��O PARA ABRIR JANELA MODAL
function OpenModalWindow(p_sUrl, p_sNome, p_sHeight, p_sWidth, p_bCenterAlign, p_sScroll) {
	//VERIFICO ANTES SE O BROWSER � O INTERNET EXPLORER,
	//SE N�O FOR, ENT�O ABRE UMA JANELA NORMAL
	if (VerificarBrowserIE()) {
		return window.showModalDialog(p_sUrl, p_sNome, "dialogHeight:" + p_sHeight + "px;dialogWidth:" + p_sWidth + "px;help:off;resizable:on;status:off;scroll:" + p_sScroll + ";center:on");
	}
	else {
		OpenWindow(p_sUrl, p_sNome, p_sHeight, parseInt(p_sWidth) + 20, p_bCenterAlign, 'no');
	}
}

//FUN��O PARA ABRIR JANELA MODAL
function OpenWindow(p_sUrl, p_sNome, p_sHeight, p_sWidth, p_bCenterAlign, p_sScroll) {
	var v_iTop = 0;
	var v_iLeft = 0;

	if (p_bCenterAlign) {
		v_iTop = CentroTelaVertical(p_sHeight);
		v_iLeft = CentroTelaHorizontal(p_sWidth);
	}

	window.open(p_sUrl, p_sNome, "status=no, fullscreen=no, toolbar=no, menubar=no, location=no, titlebar=no, resizable=yes, scrollbars=" + p_sScroll + ", width=" + p_sWidth + ", height=" + p_sHeight + ", top=" + v_iTop + ", left=" + v_iLeft);
}

//FUN��O PARA ACHAR O CENTRO HORIZONTAL DA TELA
function CentroTelaHorizontal(p_sWidth) {
	var v_iLarguraDisponivel = screen.availWidth;
	v_iCentroLargura = parseInt((parseInt(v_iLarguraDisponivel) / 2) - (parseInt(p_sWidth) / 2));

	return v_iCentroLargura;
}

//FUN��O PARA ACHAR O CENTRO VERTICAL DA TELA
function CentroTelaVertical(p_sHeight) {
	var v_iAlturaDisponivel = screen.availHeight;
	v_iCentroAltura = parseInt((parseInt(v_iAlturaDisponivel) / 2) - (parseInt(p_sHeight) / 2));

	return v_iCentroAltura;
}

//FUN��O PARA FECHAR JANELA
function CloseWindow(p_sAlert) {
	if (p_sAlert != "") {
		alert(p_sAlert);
	}

	try {
		(opener=this).close();
		window.parent.parent.close();
	}
	catch(e) {
		(opener=this).close();
	}
}

//FUN��O PARA CONFIRMAR
function Confirmar(p_sTexto) {
	if (confirm(p_sTexto)) {
		return true;
	}
	else {
		return false;
	}
}

//FUN��O PARA SETAR O FOCO EM OBJETOS
function SetFocus(p_oObj) {
	if (ObjExist(p_oObj)) {
		p_oObj.focus();
	}
}

//FUN��O PARA DISPARAR CLIQUES DE BOT�ES ATRAV�S DA TECLA "ENTER"
function FireByEnter(p_oActionObject) {
	event.cancelBubble = true;

	if (event.keyCode == 13) {
		p_oActionObject.click();
		event.returnValue = false;
	}
}

//FUN��O PARA VERIFICAR SE O OBJECT EXISTE
function ObjExist(p_oObj) {
	try {
		if ((p_oObj.name != "") || (p_oObj.id != "")) {
			return true;
		}
	}
	catch(e) {
		return false;
	}
}

//FUN��O PARA EXIBIR E ESCONDER SPAN
function ShowHideSpan(p_sNomeSpan) {
	if (eval("document.all." + p_sNomeSpan + ".style.display") == "none") {
		eval("document.all." + p_sNomeSpan + ".style.display = 'block';");
	}
	else {
		eval("document.all." + p_sNomeSpan + ".style.display = 'none';");
	}
}

//FUN��O QUE RETIRA TODOS OS ESPA�OS DA STRING
function Trim(p_sString){ 
	return(p_sString.replace( /^\s+|\s+$/gi, "").replace( /\s{2,}/gi, " " ))
}

//FUN��O QUE FAZ O REPLACE EM STRINGS. O REPLACE PADR�O DO JAVASCRIPT S� FAZ REPLACE DE UM POR VEZ.
function ReplaceString(p_sString, p_sStringFind, p_sStringReplace) {
	var v_iTamanhoString = p_sString.length;

	for (var v_iCount = 0; v_iCount < v_iTamanhoString; v_iCount++) {
		if (p_sString.indexOf(p_sStringFind) > -1) {
			p_sString = p_sString.replace(p_sStringFind, p_sStringReplace);
		}
	}

	return p_sString;
}

//FUN��O PARA CONTAR QUANTAS VEZES UMA DETERMINADA STRING SE REPETE DENTRO DE OUTRA
function ContarString(p_sString, p_sStringContar) {
	var v_iTotal = 0;

	if (p_sString.indexOf(p_sStringContar) > -1) {
		for (var v_iCount = 0; v_iCount < p_sString.length; v_iCount++) {
			if (p_sString.substring(v_iCount, v_iCount + p_sStringContar.length) == p_sStringContar) {
				v_iTotal++;
			}
		}
	}

	return v_iTotal;
}

//FUN��O PARA REPETIR UMA STRING QUANTAS VEZES SE DESEJAR
function RepetirString(p_sString, p_iNumeroVezes) {
	var v_sStringResultado = "";

	for (var v_iCount = 0; v_iCount < p_iNumeroVezes; v_iCount++) {
		v_sStringResultado += p_sString;
	}

	return v_sStringResultado;
}

//FUN��O PARA MARCAR UMA TD MUDANDO O BGCOLOR
function MarcarTD(p_sObjTD, p_sCor, p_bLimparTodos) {
	if (p_bLimparTodos == "true") {
		LimparTD();
	}

	var v_oObjTD;
	v_oObjTD = eval("document.all." + p_sObjTD);
	v_oObjTD.bgColor = p_sCor;
}

//FUN��O PARA LIMPAR O BGCOLOR DAS TDs
function LimparTD() {
	for (i = 0; i < document.all.length; i++) {
		if (document.all[i].id.substring(0, 2) == "td") {
			document.all[i].bgColor = "";
		}
	}
}

//FUN��O PARA TROCAR A COR DE UMA TD, TR OU QUALQUER OBJETO QUE TENHA O ATRIBUTO bgColor
function TrocarCorTD(p_oObjTD, p_sCor) {
	p_oObjTD.bgColor = p_sCor;
}

//FUN��O PARA TROCAR A COR DE UMA TD, TR OU QUALQUER OBJETO QUE TENHA O ATRIBUTO bgColor
function AlternarCor(p_oObjeto, p_sCor) {
	try {
		if (p_oObjeto.TagCorSelecionada != "" && p_sCor == "") {
			p_sCor = p_oObjeto.TagCorSelecionada;
		}
	}
	catch(e){}

	p_oObjeto.bgColor = p_sCor;
}

//FUN��O PARA SER USADA COM DATAGRID PARA SELE��O DE LINHAS
function AplicarCorSelecionada(p_oObjeto, p_sCor) {
	//LIMPANDO OS OBJETOS QUE J� EST�O SELECIONADOS
	for (i = 0; i < document.all.length; i++) {
		if (document.all[i].TagCorSelecionada == p_sCor) {
			document.all[i].TagCorSelecionada = "";
			document.all[i].bgColor = "";
		}
	}

	p_oObjeto.TagCorSelecionada = p_sCor;
	p_oObjeto.bgColor = p_sCor;
}

//FUN��O QUE VERIFICA SE HOUVE ALTERA��O NO FORMUL�RIO
function VerificarAlteracao() {
	if (ObjExist(document.forms[0].hdnFormularioAlterado)) {
		if (document.forms[0].hdnFormularioAlterado.value == "1") {
			var v_sMensagem = "";

			v_sMensagem = "Alguns dados do formul�rio podem ter sido alterados e poder�o ser perdidos.\n";
			v_sMensagem = v_sMensagem + "Deseja continuar?";
			
			//v_sMensagem = v_sMensagem + "     [OK] - Para continuar a opera��o sem salvar.\n";
			//v_sMensagem = v_sMensagem + "     [Cancelar] - Para voltar e revisar a opera��o.";

			if (!confirm(v_sMensagem)) {
				return false;
			}
		}
	}

	return true;
}

//FUN��O PARA OBTER O VALOR DE UM OBJETO E GUARD�-LO PARA FUTURA COMPARA��O PARA VERIFICAR 
//SE O VALOR DO CAMPO FOI ALTERADO.
//� NECESS�RIO QUE OS CAMPOS ABAIXO EXISTAM NO FORMUL�RIO:
//	<input type="hidden" name="hdnValorComparacao">
//	<input type="hidden" id="hdnFormularioAlterado" name="hdnFormularioAlterado" runat="server">
function VerificaAlteracao(p_oObjeto, p_sEvento) {
	if (p_sEvento == "onfocus") {
			document.forms[0].hdnValorComparacao.value = p_oObjeto.value;
	}
	else if (p_sEvento == "onchange") {
			document.forms[0].hdnFormularioAlterado.value = 1;
	}
	else if (p_sEvento == "onclick") {
		if ((p_oObjeto.type == "checkbox") || (p_oObjeto.type == "radio")) {
			document.forms[0].hdnFormularioAlterado.value = 1;
		}
	}
	else if (p_sEvento == "onblur") {
		if (document.forms[0].hdnValorComparacao.value != p_oObjeto.value) {
			document.forms[0].hdnFormularioAlterado.value = 1;
		}
	}
}

//FUN��O PARA RETORNAR SE O FORMUL�RIO FOI ALTERADO
function FormularioAlterado() {
	if (document.forms[0].hdnFormularioAlterado.value != "") {
		return true;
	}

	return false;
}

//FUN��O QUE DESABILITA QUALQUER BOT�O DO FORMUL�RIO PARA EVITAR CLICKS DESNECESS�RIOS
function DesabilitarBotoesFormulario() {
	window.setTimeout("ExecutarDesabilitarBotoesFormulario()", 50);
}

//FUN��O QUE DESABILITA QUALQUER BOT�O DO FORMUL�RIO PARA EVITAR CLICKS DESNECESS�RIOS
function ExecutarDesabilitarBotoesFormulario() {
	for (i = 0; i < document.all.length; i++) {
		if ((document.all[i].type == "button") || (document.all[i].type == "submit") || (document.all[i].type == "image")) {
			document.all[i].disabled = true;
		}
	}
}

//FUN��O PARA SELECIONAR IMAGENS NO CLIENTE
function SelecionarImagem(p_oFile, p_oImagem, p_sImagemPadrao) {
	var v_sValorSelecionado = Trim(p_oFile.value).toUpperCase();

	if ((v_sValorSelecionado != "") && ((v_sValorSelecionado.indexOf(".JPG") != -1) || (v_sValorSelecionado.indexOf(".GIF") != -1) || (v_sValorSelecionado.indexOf(".JPEG") != -1) || (v_sValorSelecionado.indexOf(".PNG") != -1))) {
		eval("document.all." + p_oImagem + ".src = p_oFile.value");
	}
	else {
		eval("document.all." + p_oImagem + ".src = '" + p_sImagemPadrao + "'");
	}
}