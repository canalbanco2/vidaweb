/* FUNCOES PARA CRIAR MASCARAS DINAMICAMENTE PARA OS OBJETOS DESEJADOS */

/* VARIAVEL QUE POSSUI O CODIGO ASCII DE TECLAS ESPECIAIS COMO "SHIFT", "CRTL", "HOME", "END"... */
var m_sTeclasEspeciais = "[16][17][18][20][35][36][37][38][39][40]";

/*
FUN��O PARA GERAR M�SCARAS
DEVER� SER CHAMADA NO EVENTO "onkeyup" DO OBJETO DESEJADO.
p_oObjeto = OBJETO QUE SER� APLICADA A M�SCARA. SE FOR O PR�PRIO OBJETO USAR "this".
p_sMascara = A M�SCARA DESEJADA, FORMADA POR #.
p_sDigitacaoPermitida = SOMENTE N�MERO (N), SOMENTE LETRAS (L), QUALQUER CARACTER (A).
p_sOrientacao = DIREITA (D) OU ESQUERDA (E).
p_eEvento = USADO PRINCIPALMENTE PARA FUNCIONAR NO FIRE FOX. SEMPRE PASSAR "event".

EXEMPLO: PARA FORMATAR UM CAMPO DATA (DD/MM/AAAA)
	<input type="text" name="txtData" onkeyup="JavaScript:Mascara(this, '##/##/####', 'N', 'E', event);">.
*/
function Mascara(p_oObjeto, p_sMascara, p_sDigitacaoPermitida, p_sOrientacao, p_eEvento) {
	var v_sDigitoMascara = "#";
	var v_iKeyCode = ObterCodigoTecla(p_eEvento);
	var v_sConteudoCampo = p_oObjeto.value;

	if (m_sTeclasEspeciais.indexOf("[" + v_iKeyCode + "]") > -1) return false;

	//RETIRAR TODOS OS CARACTERES DA M�SCARA
	var v_sCaracteresMascara = ReplaceString(p_sMascara, v_sDigitoMascara, "")

	for (var v_iCount = 0; v_iCount < v_sCaracteresMascara.length; v_iCount++) {
		v_sConteudoCampo = ReplaceString(v_sConteudoCampo, v_sCaracteresMascara.substring(v_iCount, v_iCount + 1), "");
	}

	var v_sCaracterTestado = new String;

	for (var v_iCount = 0; v_iCount < v_sConteudoCampo.length; v_iCount++) {
		v_sCaracterTestado = v_sConteudoCampo.substring(v_iCount, v_iCount + 1);

		if (!CheckDigits(p_sDigitacaoPermitida, v_sCaracterTestado.charCodeAt(0), p_eEvento)) {
			v_sConteudoCampo = ReplaceString(v_sConteudoCampo, v_sCaracterTestado, "");
		}
	}

	var v_sConteudoFinal = "";

	//VERIFICANDO A ORIENTA��O DO CAMPO TEXTO
	if (p_sOrientacao == "D") {
		for (var v_iCount = p_sMascara.length; v_iCount > 0; v_iCount--) {
			if (v_sConteudoCampo != "") {
				//SE O CARACTER DA M�SCARA FOR IGUAL AO DIGITO DE M�SCARA, ENT�O PEGAR O
				//VALOR DO CONTE�DO DIGITADO
				if (p_sMascara.substring(v_iCount - v_sDigitoMascara.length, v_iCount) == v_sDigitoMascara) {
					v_sConteudoFinal = v_sConteudoCampo.substring(v_sConteudoCampo.length - 1, v_sConteudoCampo.length) + v_sConteudoFinal;

					v_sConteudoCampo = v_sConteudoCampo.substring(0, v_sConteudoCampo.length - 1);
				}
				else {
					v_sConteudoFinal = p_sMascara.substring(v_iCount - v_sDigitoMascara.length, v_iCount) + v_sConteudoFinal;
				}
			}
		}
	}
	else {
		for (var v_iCount = 0; v_iCount < p_sMascara.length; v_iCount++) {
			if (v_sConteudoCampo != "") {
				//SE O CARACTER DA M�SCARA FOR IGUAL AO DIGITO DE M�SCARA, ENT�O PEGAR O
				//VALOR DO CONTE�DO DIGITADO
				if (p_sMascara.substring(v_iCount, v_iCount + v_sDigitoMascara.length) == v_sDigitoMascara) {
					v_sConteudoFinal += v_sConteudoCampo.substring(0, 1);

					v_sConteudoCampo = v_sConteudoCampo.substring(1, v_sConteudoCampo.length);
				}
				else {
					v_sConteudoFinal += p_sMascara.substring(v_iCount, v_iCount + v_sDigitoMascara.length);
				}
			}
		}
	}

	p_oObjeto.value = v_sConteudoFinal;
}

//FUN��O PARA PERMITIR DIGITA��O DE SOMENTE N�MEROS
function OnlyNumbersDot(p_iKeyCode, p_eEvento) {
	if (p_iKeyCode == null) {
		p_iKeyCode = ObterCodigoTecla(p_eEvento);
	}

	if (m_sTeclasEspeciais.indexOf("[" + p_iKeyCode + "]") > -1) return false;

	if (!(p_iKeyCode > 45 && p_iKeyCode < 58 || p_iKeyCode == 13)) {
		window.event.keyCode = null;
		return false;
	}
	else {
		return true;
	}
}

//FUN��O PARA PERMITIR DIGITA��O DE SOMENTE N�MEROS
function OnlyNumbers(p_iKeyCode, p_eEvento) {
	if (p_iKeyCode == null) {
		p_iKeyCode = ObterCodigoTecla(p_eEvento);
	}

	if (m_sTeclasEspeciais.indexOf("[" + p_iKeyCode + "]") > -1) return false;

	if (!(p_iKeyCode > 47 && p_iKeyCode < 58 || p_iKeyCode == 13)) {
		window.event.keyCode = null;
		return false;
	}
	else {
		return true;
	}
}

//FUN��O PARA PERMITIR DIGITA��O DE SOMENTE LETRAS
function OnlyLetters(p_iKeyCode, p_eEvento) {
	if (p_iKeyCode == null) {
		p_iKeyCode = ObterCodigoTecla(p_eEvento);
	}

	if (m_sTeclasEspeciais.indexOf("[" + p_iKeyCode + "]") > -1) return false;

	if (!((p_iKeyCode > 64 && p_iKeyCode < 91) || (p_iKeyCode > 96 && p_iKeyCode < 123))) {
		window.event.keyCode = null;
		return false;
	}
	else {
		return true;
	}
}

//FUN��O PARA VERIFICAR QUAIS OS FILTROS DE DIGITA��O SER�O USADOS
function CheckDigits(p_sOnlyDigits, p_iKeyCode, p_eEvento) {
	if (p_sOnlyDigits == "N") {
		return OnlyNumbers(p_iKeyCode, p_eEvento);
	}
	if (p_sOnlyDigits == "L") {
		return OnlyLetters(p_iKeyCode, p_eEvento);
	}

	return true;
}

//FUN��O PARA LIMITAR O TAMANHO DO OBJETO TEXTAREA
function MaxLengthTextArea(p_oObjeto, p_iTamanho) {
	if (p_oObjeto.length >= p_iTamanho) {
		p_oObjeto.value = p_oObjeto.value.substring(0, p_iTamanho);
	}

	return true;
}
