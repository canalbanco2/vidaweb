﻿<%@ Page Language="C#" MasterPageFile="~/TutorialVidaWeb.Master" AutoEventWireup="true" CodeBehind="usuario-o-que-e.aspx.cs" Inherits="WebApp.usuario_o_que_e" Title="Untitled Page" %>
<%@ Register src="UC/OutrosRecursos.ascx" tagname="OutrosRecursos" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:OutrosRecursos ID="OutrosRecursos1" runat="server" />
    <div class="centroint">
    	<div class="conteudo">
        <p>
	<a href="javascript:history.back(-1);" >Voltar</a>
        </p>        
            <h1 class="title02">O que são tipos de usuário?</h1>
            <p class="pesqResult">Para cada tipo de usuário, o Vida WEB disponibiliza funções diferentes. Você poderá selecionar o tipo de usuário para visualizar somente os tutoriais que lhe sejam necessários. </p>
            <p class="pesqResult"><strong>Administrador da Apólice</strong>: é o indicado pelo responsável da empresa, no termo de responsabilidade. Concede o acesso ao Administrador do Subgrupo e revoga a senha dos demais usuários (Administrador do Subgrupo e Operador). Possui acesso ao sistema de consulta e, portanto, não realiza inclusões, exclusões ou alterações. </p>
            <p class="pesqResult"><strong>Administrador do Subgrupo</strong>: é o responsável pela validação e envio da movimentação de vidas dentro do prazo para a seguradora, referente ao(s) subgrupo(s) e concede o acesso ao Operador. Possui acesso ao sistema de consulta e encerra a movimentação, que permite o faturamento ser emitido pela seguradora. Não realiza inclusões, exclusões ou alterações. </p>
            <p class="pesqResult"><strong>Operador:</strong> é o responsável por realizar a movimentação de vidas (inclusões, exclusões ou alterações) da apólice, referente ao(s) subgrupo(s) ao(s) qual(is) esteja(m) vinculado(s) e submeter a validação do Administrador do Subgrupo para encerrar a movimentação.</p>
        </div>     
    </div>
</asp:Content>