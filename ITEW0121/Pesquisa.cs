﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace WebApp
{
    public class Pesquisa
    {
        public Pesquisa()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataTable SearchTutorial(string sKey)
        {
            DataTable dtVideos = (new Videos()).Get();
            DataTable dtResultado = new DataTable();

            dtResultado.Columns.Add(new DataColumn("ID"));
            dtResultado.Columns.Add(new DataColumn("Titulo"));
            dtResultado.Columns.Add(new DataColumn("Retorno"));



            foreach (DataRow dr in dtVideos.Rows)
            {
                bool isFinded = false;
                string[] arrKey = (sKey + " ").Split(' ');
                foreach (string nKey in arrKey)
                {
                    if (nKey != "" && nKey != " ")
                    {
                        if (dr["titulo"].ToString().ToLower().Contains(nKey.ToLower()) || dr["obs"].ToString().ToLower().Contains(nKey.ToLower()))
                        {
                            isFinded = true;
                        }
                    }
                }

                if (isFinded)
                {
                    DataRow drResultado = dtResultado.NewRow();
                    drResultado["ID"] = dr["id"];
                    drResultado["Titulo"] = dr["titulo"];
                    drResultado["Retorno"] = "";

                    dtResultado.Rows.Add(drResultado);
                }
            }

            return dtResultado;
        }

    }
}
