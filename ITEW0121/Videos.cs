﻿using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

    public class Videos
    {
        string PATH_VIDEOS;

        public Videos()
        {
            //Mathayde - 12054014 - AUALIZA�O MANUAL E TUTORIAL VIDA WEB
            //PATH_VIDEOS = HttpContext.Current.Server.MapPath("../../_tutorialVidaWeb/Movies/");
            
            //laurent silva
            PATH_VIDEOS = HttpContext.Current.Server.MapPath("../../_tutorialVidaWeb/Movies/");
            //PATH_VIDEOS = HttpContext.Current.Server.MapPath("~/teste/");

            //PATH_VIDEOS = HttpContext.Current.Server.MapPath("~/cloud/Movies/");
        }

        public DataTable Get()
        {
            string[] arrDiretorios = Directory.GetDirectories(PATH_VIDEOS);
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("ID"));
            dt.Columns.Add(new DataColumn("Titulo"));
            dt.Columns.Add(new DataColumn("Obs"));
            int x = 1;
            foreach (string sDiretorio in arrDiretorios)
            {
                DataSet ds = new DataSet();
                //HttpContext.Current.Response.Write(sDiretorio + "<br>");
                ds.ReadXml(sDiretorio + "/config-aula.xml");
                DataRow dr = dt.NewRow();
                //if (x == 7)
                //{
                //    dr["ID"] = "7/8";
                //}
                //else if (x == 13)
                //{
                //    dr["ID"] = "13/14";
                //}
                //else if (x == 16)
                //{
                //    dr["ID"] = "16/17";
                //}
                //else if (x == 20)
                //{
                //    dr["ID"] = "20/21";
                //}
                //else if (x == 23)
                //{
                //    dr["ID"] = "23/24";
                //}
                //else
                //{
                //    dr["ID"] = x;
                //}

                dr["ID"] = x;

                dr["Titulo"] = ds.Tables[0].Rows[0]["titulo"].ToString();
                dr["Obs"] = ds.Tables[0].Rows[0].Table.Rows[0].ItemArray[1].ToString();

                //if (x != 8 & x != 14 & x != 17 & x != 21 & x != 24)
                //    dt.Rows.Add(dr);
                

                dt.Rows.Add(dr);

                x++;
            }
            
            return dt;
        }
    }
