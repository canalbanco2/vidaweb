﻿<%@ Page Language="C#" MasterPageFile="~/TutorialVidaWeb.Master" AutoEventWireup="true" CodeBehind="pesquisa.aspx.cs" Inherits="WebApp.pesquisa" Title="Untitled Page" %>
<%@ Register src="UC/OutrosRecursos.ascx" tagname="OutrosRecursos" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:OutrosRecursos ID="OutrosRecursos1" runat="server" />
    <div class="centroint">
    	<div class="conteudo">
        <p>
	<a href="javascript:history.back(-1);" >Voltar</a>
        </p>        
            <h1 class="title02">Pesquisa</h1>
            <p>Sua pesquisa por &quot;<strong><asp:Literal ID="ltKey" runat="server"></asp:Literal></strong>&quot; retornou os seguintes resultados:</p>
            <h2 class="headUnderline">Em Tutorial</h2>
            <asp:Repeater ID="rptResultadoTutorial" runat="server">
                <ItemTemplate>
                    <p class="pesqResult"><a href="Default.aspx?id=<%# DataBinder.Eval(Container.DataItem, "ID") %>"><%# DataBinder.Eval(Container.DataItem, "titulo") %></a></p>
                </ItemTemplate>
            </asp:Repeater>

        </div>
        <div class="paginacao">
        	<div class="paginaLink">
            	
            </div>
        	<p class="paginaTxt">Não encontrou o que procurava? <a href="javascript:void();" alt="">Clique aqui</a> e envie um e-mail.</p>
        </div>
           
    </div>
</asp:Content>