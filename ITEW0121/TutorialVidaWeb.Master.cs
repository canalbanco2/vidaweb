﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace WebApp
{
    public partial class TutorialVidaWeb : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = "BB Seguros - Tutorial Vida Web";
            if (!IsPostBack)
            {
                ddlAcessoRapido.DataSource = (new Videos()).Get();
                ddlAcessoRapido.DataTextField = "Titulo";
                ddlAcessoRapido.DataValueField = "ID";
                ddlAcessoRapido.DataBind();

                ddlAcessoRapido.Items.Insert(0, new ListItem(""));
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pesquisa.aspx?key=" + txtBusca.Text);
        }
    }
}
