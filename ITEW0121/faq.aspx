﻿<%@ Page Language="C#" MasterPageFile="~/TutorialVidaWeb.Master" AutoEventWireup="true" CodeBehind="faq.aspx.cs" Inherits="WebApp.faq" Title="Untitled Page" %>
<%@ Register src="UC/OutrosRecursos.ascx" tagname="OutrosRecursos" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:OutrosRecursos ID="OutrosRecursos1" runat="server" />
    <div class="centroint">
    	<div class="conteudo">
        <p>
        <a href="javascript:history.back(-1);" >Voltar</a>
        </p>        
            <h1 class="title02">Perguntas Frequentes</h1>            
            <p>Encontre aqui as respostas para as dúvidas mais freqüentes sobre o processo de faturamento pelo sistema Vida WEB. </p>
            <h2 class="title03">1.	O que é “Faturamento WEB”?</h2>
            <p class="faqResp">É uma ferramenta na web que permite ao Estipulante gerenciar  sua (s) apólice (s), efetuar as movimentações dos seguros de pessoas e imprimir boletos e faturas.</p>
            <p class="faqResp">Permite também a consulta e impressão de certificados, relação de vidas seguradas, entre outros documentos.</p>
            <h2 class="title03">2.	O que é necessário para ter o acesso ao “Faturamento WEB”?</h2>
            <p class="faqResp">Basta entrar em contato com  a Central de Atendimento aos Clientes - 0800 729 7000, opção 5 e/ou com o consultor responsável da Aliança que o atende para verificar se sua (s) apólice (s) possui (possuem) as condições técnicas para migração e o Termo de Migração para o Faturamento WEB.</p>
            <h2 class="title03">3.	Que fatores técnicos são necessários para ter o acesso ao “Faturamento WEB”?</h2>
            <ul>
                <li>É imprescindível utilizar o navegador de Internet Explorer versão 6 ou superior.</li>
                <li>É recomendável para a utilização e bom desempenho do sistema os itens abaixo: 
                	<ul>
                        <li>Microcomputador Pentium III; </li>
                        <li>800 MHZ; </li>
                        <li>256 MB RAM; </li>
                        <li>Conexão Internet banda larga 256 Kb e resolução de tela 1.024 por 768 pixels.</li>
                    </ul>
                </li>
                <li>Além disso, é necessário:
                	<ul>
                        <li>Instalar o aplicativo JAVA (utilizado em páginas financeiras);</li>
                        <li>Ativar o ACTIVE X para impressão de documentos;</li>
                        <li>Desativar bloqueador de pop-ups para habilitar o login ao sistema.</li>
                    </ul>
                </li>
            </ul>
            <h2 class="title03">4.	Qual a principal função do Administrador de Apólice?</h2>
            <p class="faqResp">O Administrador da Apólice é o indicado pelo responsável da empresa, no termo de responsabilidade. Sua principal função  é conceder o acesso ao Administrador do Subgrupo e revogar a senha dos demais usuários (Administrador do Subgrupo e Operador). Possui acesso ao sistema de consulta e, portanto, não realiza inclusões, exclusões ou alterações.</p>
            <h2 class="title03">5.	Qual a principal função do Administrador de Subgrupo?</h2>
            <p class="faqResp">O Administrador do Subgrupo é o responsável pela validação e envio da movimentação de vidas, referentes ao(s) subgrupo(a), dentro do prazo para a seguradora, Além disso, concede o acesso ao Operador. Possui acesso ao sistema de consulta e encerra a movimentação que permite o faturamento ser emitido pela seguradora. Não realiza inclusões, exclusões ou alterações.</p>
            <h2 class="title03">6.	Qual a principal função do Operador?</h2>
            <p class="faqResp">O Operador é o responsável por realizar a movimentação de vidas (inclusões, exclusões ou alterações) da apólice referente ao(s) subgrupo(s), ao(s) qual(is) esteja(m) vinculado(s). Além disso, submetem a movimentação para a validação do Administrador do Subgrupo para encerrar a movimentação.</p>
            <h2 class="title03">7.	 O que faço no caso da minha senha não permitir o acesso ao sistema?</h2>
            <p class="faqResp">Veja a orientação de acordo com sua função na gestão da apólice:</p>
            <ul>
            	<li><strong>Administrador de Apólice</strong> – entrar em contato com a Central de Atendimento aos Clientes - 0800 729 7000, opção 5 e/ou com o consultor responsável da Aliança que o atende, que farão o reset da senha.</li>
                <li><strong>Administrador de Subgrupo</strong> – solicitar ao administrador da apólice que faça o reset de sua senha.</li>
                <li><strong>Operador</strong> - solicitar ao administrador de subgrupo que faça o reset de sua senha.</li>
            </ul>
            <h2 class="title03">8.	Encerrei a movimentação e não consigo imprimir o boleto, fatura e/ou relação de vidas. O que faço?</h2>
            <ul>
            	<li>Verifique no menu “Histórico e Próximos Passos” o “protocolo do encerramento”, confirmado por meio da frase: <em class="underline">DD/MM/AAAA - Realizado o Encerramento da Fatura - Por: XXXXXX</em></li>
            </ul>
            <p class="faqResp">Verifique a data limite para a movimentação. Se você antecipou o encerramento e precisa da fatura deverá entrar em contato com a Central de Atendimento aos Clientes - 0800 729 7000, opção 5 e/ou com o consultor responsável da Aliança que o atende e solicite a geração da fatura, uma vez que o sistema libera a fatura automaticamente apenas no dia seguinte à data limite para o encerramento da movimentação. </p>
            <ul>
            	<li>Em qualquer outra situação entre em contato com a Central de Atendimento aos Clientes - 0800 729 7000, opção 5 e/ou com o consultor responsável da Aliança que o atende.</li>
            </ul>
            <h2 class="title03">9.	Como posso ter certeza de que o comando de encerramento foi acatado pela Companhia?</h2>
            <p class="faqResp">Verifique se no menu “Histórico e Próximos Passos” há o “protocolo do encerramento”, confirmado por meio da frase: <em class="underline">DD/MM/AAAA - Realizado o Encerramento da Fatura - Por: XXXXXX.</em>  Se sim, o comando foi aceito pela Companhia; se não, entre em contato com a Central de Atendimento aos Clientes - 0800 729 7000, opção 5 e/ou com o consultor responsável da Aliança que o atende</p>
            <h2 class="title03">10.	Se eu errar ao incluir, excluir e alterar as vidas como posso corrigir antes do comando de encerramento?</h2>
            <p class="faqResp">Você poderá “apagar” lançamento por lançamento utilizando a função “desfazer” ou “carregar a relação anterior” para apagar todos os lançamentos de uma só vez.</p>
            <h2 class="title03">11.	Enviei a movimentação por arquivo e a encerrei. Por que há movimentações que não foram consideradas nesta fatura?</h2>
            <p class="faqResp">Verifique as orientações de acordo com a operação realizada:</p>
            <ul>
            	<li>Inclusões e Alterações 
                    <ul>
                        <li>Veja se há necessidade de remessa de documentos prévios para a Companhia, no menu “pendências”</li>
                    </ul>
                </li>
                <li>Exclusões
                    <ul>
                        <li>Com data retroativa anterior há duas vigências, exigem a remessa prévia de comprovação legal do desligamento do segurado</li>
                        <li>Com data de desligamento dentro do mês da vigência que está sendo movimentada será acatada como exclusão agendada. O segurado será excluído a partir da próxima competência.</li>
                    </ul>
                </li>
            </ul>
            <h2 class="title03">12.	Fiz exclusões e elas não foram processadas na fatura. Por quê?</h2>
            <ul>
            	<li>Exclusões
                	<ul>
                    	<li>Com data retroativa anterior há duas vigências, exigem a remessa prévia de comprovação legal do desligamento do segurado</li>
                        <li>Com data de desligamento dentro do mês da vigência que está sendo movimentada será acatada como exclusão agendada. O segurado será excluído a partir da próxima competência.</li>
                    </ul>
                </li>
            </ul>
            <h2 class="title03">13.	Qual a diferença entre movimentação por arquivo e online?</h2>
            <p class="faqResp">A escolha do tipo de movimentação que irá trabalhar, Online ou Arquivo, será sua e de acordo com o número de vidas que serão movimentadas na vigência.</p>
            <ul>
            	<li>Online as movimentações são feitas uma a uma. </li>
                <li>O Arquivo proporciona dois tipos de movimentação: 
                	<ul>
                    	<li>Incremental – arquivo contendo somente as inclusões, alterações e exclusões daquela competência</li>
                        <li>Sobrevivência – arquivo contendo todo o grupo segurado incluindo os segurados que foram desligados durante a competência que está sendo movimentada.</li>
                    </ul>
                </li>
            </ul>
            <h2 class="title03">14.	Como posso conferir as movimentações antes de encerrar a movimentação?</h2>
            <p class="faqResp">Utilize o menu “Extrato de Movimentação” que contempla as movimentações efetuadas na vigência atual.</p>
            <h2 class="title03">15.	Quando as movimentações efetuadas ficam na pendência, quais as providências que o Estipulante deve tomar?</h2>
            <p class="faqResp">Você deverá enviar os documentos indicados ao lado de cada proponente. As orientações para a remessa podem ser obtidas na função “Download – Clique aqui”.</p>
            <h2 class="title03">16.	Tenho mais de uma apólice e recebi apenas um login e uma senha provisória. Como faço para acessar as outras apólices?</h2>
            <p class="faqResp">Você vai acessar todas as suas apólices com o mesmo login e senha.</p>
            <h2 class="title03">17.	Como encontrar explicações para as dúvidas que surgirem no momento de efetuar as movimentações?</h2>
            <p class="faqResp">Existem algumas ferramentas como: Manual Vida WEB, Tutorial Vida WEB, a nossa Central de Atendimento aos Clientes - 0800 729 7000, opção 5 e/ou com o consultor responsável da Aliança que o atende.</p>
            <h2 class="title03">18.	É possível fazer acertos com data retroativa? Quais os critérios?</h2>
            <p class="faqResp">Sim, é possível. Veja os critérios:</p>
            <ul>
            	<li>Inclusões e alterações – movimentações com uma vigência comparada com a atual. As mais antigas podem ser gravadas, porém o sistema indicará a necessidade de documentação para o processo de análise de risco (itens 11 e 15)</li>
                <li>Exclusões - movimentações com duas vigências comparada com a atual. As mais antigas podem ser gravadas, porém o sistema indicará a necessidade de documentação para o processo de análise de risco (itens 11, 12 e 15)</li>
            </ul>
            <h2 class="title03">19.	Como altero dados cadastrais?</h2>
            <p class="faqResp">Do segurado: podem ser alterados no próprio sistema: nome, capital (até o limite contratado). </p>
            <p class="faqResp">Para alterar data de nascimento e CPF do segurado e dados cadastrais do Estipulante você deve solicitar um endosso a sua agência de relacionamento, ao consultor que o atende e/ou para a nossa Central de Atendimento aos Clientes - 0800 729 7000, opção 5 e/ou com o consultor responsável da Aliança que o atende.</p>
            <h2 class="title03">20.	Como faço para trocar minha senha?</h2>
            <p class="faqResp">Acesse o site <a href="http://www.aliancadobrasil.com.br/">www.aliancadobrasil.com.br</a>, “Área exclusiva”, e o menu “alterar a senha”. Você receberá em alguns minutos um e-mail com a nova senha provisória que deverá ser trocada no primeiro acesso.</p>
            <p>
                &nbsp;</p>
            <p>
                &nbsp;</p>
            <p>
                &nbsp;</p>
            <p>
                &nbsp;</p>
            <p>
                &nbsp;</p>
        </div>
    </div>    
</asp:Content>