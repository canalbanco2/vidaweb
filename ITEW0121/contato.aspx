﻿<%@ Page Language="C#" MasterPageFile="~/TutorialVidaWeb.Master" AutoEventWireup="true" CodeBehind="contato.aspx.cs" Inherits="WebApp.contato" Title="Untitled Page" %>
<%@ Register src="UC/OutrosRecursos.ascx" tagname="OutrosRecursos" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">    
    <link rel="stylesheet" type="text/css" href="common/css/Contato.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:OutrosRecursos ID="OutrosRecursos1" runat="server" />    
    <div class="centroint">
    	<div class="conteudo">
            <h1 class="title02">Contato</h1>
            <p>Preencha os campos abaixo com seus dados e mande suas dúvidas e/ou sugestões sobre o Tutorial Vida WEB.</p>
            <div class="formContato">
            	<span class="latEsq">Nome:</span>
                <span class="latDir"><input type="text" name="txtNome" id="txtNome" class="txt01" /></span>
                <span class="latEsq">E-mail:</span>
                <span class="latDir"><input type="text" name="txtEmail" id="txtEmail" class="txt01" /></span>
                <span class="latEsq">Assunto:</span>
                <span class="latDir">
                	<select name="ddm" id="ddm" class="ddm01" >
                    	<option>Selecione</option>
                        <option>um</option>
                        <option>dois</option>
                        <option>tum</option>
                        <option>qum</option>
                        <option>cum</option>
                        <option>sum</option>
                        <option>sum</option>
                    </select>
                </span>
                <span class="latEsq">Mensagem:</span>
                <span class="latDir"><textarea rows="6" cols="20" class="txt02"></textarea></span>
                <span class="latBtn"><input type="button" name="btnEnviar" id="btnEnviar" class="btnEnviar" /></span>
                <div class="clr"></div>
            </div>
        </div>
    </div>    
</asp:Content>