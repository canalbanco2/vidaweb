﻿<%@ Page Language="C#" MasterPageFile="~/TutorialVidaWeb.Master" AutoEventWireup="true" CodeBehind="glossario.aspx.cs" Inherits="WebApp.glossario" Title="Untitled Page" %>
<%@ Register src="UC/OutrosRecursos.ascx" tagname="OutrosRecursos" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:OutrosRecursos ID="OutrosRecursos1" runat="server" />
    <div class="centroint">
    	<div class="conteudo">
        <p>
	<a href="javascript:history.back(-1);" >Voltar</a>
        </p>
            <h1 class="title02">Glossário</h1>
            <h2 class="headUnderline">A</h2>
            <p class="pesqResult">Apólice: documento emitido pela sociedade seguradora que formaliza a aceitação da cobertura solicitada pelo estipulante. </p>
            <h2 class="headUnderline">B</h2>
            <p class="pesqResult">Beneficiário: pessoa física ou jurídica designada para receber os valores dos capitais segurados, na hipótese de ocorrência de sinistro coberto. </p>
            <p class="pesqResult">Boa-Fé: princípio básico de qualquer contrato, principalmente no contrato de seguro, pois é indis¬pensável que haja confiança mútua entre as partes envolvidas. Esse princípio obriga as partes a agir com a máxima honestidade e em fiel cumprimento às leis e ao contrato do seguro. Nesse conceito, inclui-se a obrigação do segurado em prestar informações verdadeiras na proposta de adesão e durante a vigência de todo o contrato, declarando, também no decorrer da apólice, qualquer alteração no risco.</p>
            <h2 class="headUnderline">C</h2>
            <p class="pesqResult">Capital Segurado: valor máximo para a cobertura contratada a ser pago pela sociedade seguradora, no caso de ocorrência do sinistro coberto pela apólice vigente na data do evento.</p>
            <p class="pesqResult">Certificado Individual: documento destinado ao segurado, emitido pela sociedade seguradora no caso de contratação coletiva, quando da aceitação do proponente, da renovação do seguro ou da alteração de valores de capital segurado ou prêmio. </p>
            <p class="pesqResult">Companheiro(a): pessoa que se une a outra e que se apresente à sociedade como se fosse legiti¬mamente casada, formando uma entidade familiar. </p>
            <p class="pesqResult">Condições Contratuais: conjunto de disposições que regem a contratação, incluindo as constantes da proposta de contratação, das condições gerais, das condições especiais, da apólice e, quando for o caso de plano coletivo, do contrato, da proposta de adesão e do certificado individual.</p>
            <p class="pesqResult">Contrato: instrumento jurídico firmado entre o estipulante e a sociedade seguradora, que estabelece as peculiaridades da contratação do plano coletivo e fixa os direitos e as obrigações do estipulante, da sociedade seguradora, dos segurados, e dos beneficiários. </p>
            <p class="pesqResult">
            	Custeio do Seguro: de acordo com a opção feita pelo estipulante, o custeio poderá ser: 
                <ul>
                	<li>Contributário: os segurados participam do pagamento do prêmio, total ou parcialmente. </li>
                    <li>Não-Contributário: os segurados não pagam prêmio, recaindo o ônus do seguro total¬mente sobre o estipulante. </li>
                </ul>
            </p>
            <h2 class="headUnderline">D</h2>
            <p class="pesqResult">Declaração Pessoal de Saúde e Atividade: declaração legal e formal, na qual o proponente presta as informações e declarações sobre o seu estado de saúde e de atividade profissional exercida, sob sua responsabilidade e sob as penas previstas no artigo 766 do Código Civil Brasileiro, para avalia¬ção de risco pela sociedade seguradora. </p>
            <p class="pesqResult">Doenças e Lesões Preexistentes e suas Conseqüências: doenças ou lesões, inclusive as con¬gênitas, contraídas pelo segurado, anteriormente à data de sua adesão ao seguro, caracterizando-se pela existência de sinais, sintomas e quaisquer alterações evidentes em seu estado de saúde, e que eram de seu prévio conhecimento na data da contratação de seguro e não-declaradas na proposta de contratação ou de adesão ao seguro. Caracteriza-se, ainda, quando o segurado omite tratamento realizado na contratação do seguro. </p>
            <p class="pesqResult">Dolo: toda espécie de artifício, engano ou manejo astucioso promovido por uma pessoa, com a intenção de induzir a outrem à prática de um ato jurídico, em prejuízo deste e em proveito próprio ou de outrem. Ou seja, é um ato de má-fé, fraudulento, visando a prejuízo preconcebido, seja físico ou financeiro. </p>
            <h2 class="headUnderline">E</h2>
            <p class="pesqResult">Estipulante: pessoa física ou jurídica que propõe a contratação de plano coletivo, ficando investida de poderes de representação do segurado, nos termos da legislação e regulação em vigor, sendo identificado como estipulante-instituidor quando participar total ou parcialmente do custeio do plano, e como estipulante-averbador quando não participar do custeio. </p>                    <h2 class="headUnderline">G</h2>
            <p class="pesqResult">Grupo Segurado: totalidade do grupo segurável efetivamente aceito e incluído na apólice coletiva. </p>
            <p class="pesqResult">Grupo Segurável: totalidade das pessoas físicas vinculadas ao estipulante que reúnem as condições para inclusão na apólice coletiva. Início de Vigência: data a partir da qual as coberturas de risco propostas serão garantidas pela so-ciedade seguradora. </p>
            <h2 class="headUnderline">I</h2>
            <p class="pesqResult">Início de Vigência da Cobertura Individual: data a partir da qual a sociedade seguradora assume a cobertura dos eventos previstos nestas condições gerais para cada segurado. Movimentação on-line: inclusão, exclusão ou alteração que é realizada pelo envio de arquivo e reco¬mendada para movimentações superiores a 50 mudanças mensais. </p>
            <h2 class="headUnderline">M</h2>
            <p class="pesqResult">
            	Movimentação por arquivo: feita por meio de remessa de arquivo, configurado de acordo com o permitido no sistema e recomendada para movimentações mensais superiores a 50 vidas.
                <ul>
                	<li>Método Incremental: disponibiliza apenas as inclusões, exclusões e alterações de vidas ocorridas no mês de competência. </li>
                    <li>Método por Sobreposição: disponibiliza arquivo contendo todas as vidas a serem segura¬das em substituição ao arquivo faturado na competência anterior. </li>
                </ul>
            </p>
            <h2 class="headUnderline">P</h2>
            <p class="pesqResult">Prêmio: valor correspondente a cada um dos pagamentos destinados ao custeio do seguro. </p>
            <p class="pesqResult">Proponente: interessado em contratar a cobertura (ou coberturas), ou aderir ao contrato, no caso de contratação coletiva. </p>
            <p class="pesqResult">Proposta de Adesão: documento com declaração dos elementos essenciais do interesse a ser ga¬rantido e do risco, em que o proponente, pessoa física, expressa a intenção de aderir à contratação coletiva, manifestando pleno conhecimento das condições contratuais. </p>
            <h2 class="headUnderline">R</h2>
            <p class="pesqResult">Riscos Excluídos: riscos previstos nas condições gerais e/ou especiais, que não serão cobertos pelo plano. </p>
            <h2 class="headUnderline">S</h2>
            <p class="pesqResult">Segurado: pessoa física com interesse segurável, sobre a qual se procederá a avaliação do risco e se estabelecerá o seguro. </p>
            <p class="pesqResult">Seguradora: Companhia de Seguros Aliança do Brasil S.A., doravante designada sociedade segu¬radora, que se responsabiliza pela cobertura do seguro mediante recebimento de prêmio, conforme estabelecido nestas condições. </p>
            <h2 class="headUnderline">V</h2>
            <p class="pesqResult">Vigência da Cobertura Individual: período em que, na apólice em vigor, o segurado tem direito à(s) cobertura(s) do seguro. </p>
            <p class="pesqResult">Vigência do Seguro: período de cobertura no qual a apólice do seguro está em vigor.</p>
        </div>
        <br clear="all" />
    </div>   
</asp:Content>