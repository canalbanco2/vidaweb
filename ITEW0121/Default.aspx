﻿<%@ Page Language="C#" MasterPageFile="~/TutorialVidaWeb.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApp.Default" Title="BB Seguros - Tutorial Vida Web" %>
<%@ Register src="UC/OutrosRecursos.ascx" tagname="OutrosRecursos" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>BB Seguros - Tutorial Vida Web</title>		
	<link rel="stylesheet" type="text/css" href="common/css/home.css" />
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">    

    <div class="meio">
        <uc1:OutrosRecursos ID="OutrosRecursos1" runat="server" />        
        <div class="centro">
            <div class="menuesq">                        
                <div class="vertical">
                    <h1>Selecione a video-aula:</h1>
                    <button class="prev" type="button"><span>Voltar</span></button>                            
                    <div class="jCarouselLite">
                        <ul>
                            <asp:Repeater ID="rptMenu" runat="server">
                                <ItemTemplate>
                                    <li><%# DataBinder.Eval(Container.DataItem, "ID") %>. <a href="javascript:void(0);" class="aula" id="opt<%# DataBinder.Eval(Container.DataItem, "ID") %>"><%# DataBinder.Eval(Container.DataItem, "Titulo") %></a></li>
                                </ItemTemplate>
                            </asp:Repeater>   
                        </ul>
                    </div>
                    <button class="next" type="button"><span>Próximo</span></button>
                </div>                
              <p class="obs">Não encontrou o que procurava? <br/> <a href="https://www.bbseguros.com.br/seguradora/atendimento/formularios-uteis/fale-conosco-demais-produtos.jsp" target="_blank">Clique aqui</a> e envie um e-mail.</p>
            </div>
            <div class="contesq">
                <div class="conteudo">
                    <h1 class="title01">Configuração</h1>
                    <div class="video" id="boxVideo">
                	    <a href="javascript:displaySWF('1');">
                	        <img src="/_tutorialVidaWeb/movies/01/foto.jpg" id="img1" alt="" />
                	        <img src="/_tutorialVidaWeb/movies/02/foto.jpg" id="img2" style="display:none" alt="" />
                	        <img src="/_tutorialVidaWeb/movies/03/foto.jpg" id="img3" style="display:none" alt="" />
                	        <img src="/_tutorialVidaWeb/movies/04/foto.jpg" id="img4" style="display:none" alt="" />
                	        <img src="/_tutorialVidaWeb/movies/05/foto.jpg" id="img5" style="display:none" alt="" />
                	        <img src="/_tutorialVidaWeb/movies/06/foto.jpg" id="img6" style="display:none" alt="" />
                	        <img src="/_tutorialVidaWeb/movies/07/foto.jpg" id="img7" style="display:none" alt="" />
                	        <img src="/_tutorialVidaWeb/movies/08/foto.jpg" id="img8" style="display:none" alt="" />
                	        <img src="/_tutorialVidaWeb/movies/09/foto.jpg" id="img9" style="display:none" alt="" />
                	        <img src="/_tutorialVidaWeb/movies/10/foto.jpg" id="img10" style="display:none" alt="" />
                	        <img src="/_tutorialVidaWeb/movies/11/foto.jpg" id="img11" style="display:none" alt="" />
                	        <img src="/_tutorialVidaWeb/movies/12/foto.jpg" id="img12" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/13/foto.jpg" id="img13" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/14/foto.jpg" id="img14" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/15/foto.jpg" id="img15" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/16/foto.jpg" id="img16" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/17/foto.jpg" id="img17" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/18/foto.jpg" id="img18" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/19/foto.jpg" id="img19" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/20/foto.jpg" id="img20" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/21/foto.jpg" id="img21" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/22/foto.jpg" id="img22" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/23/foto.jpg" id="img23" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/24/foto.jpg" id="img24" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/25/foto.jpg" id="img25" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/26/foto.jpg" id="img26" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/27/foto.jpg" id="img27" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/28/foto.jpg" id="img28" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/29/foto.jpg" id="img29" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/30/foto.jpg" id="img30" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/31/foto.jpg" id="img31" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/32/foto.jpg" id="img32" style="display:none"  alt="" />
                	        <img src="/_tutorialVidaWeb/movies/33/foto.jpg" id="img33" style="display:none"  alt="" />
                	    </a>
                    </div>
                    <div class="videodesc">
                	    <div class="bordertop"></div>
                        <div class="desc">
                            <p><strong>Descrição</strong></p>
                            <p>Usar o Vida WEB ficou mais fácil ainda. Agora você tem disponível todo o conteúdo do tutorial em vídeo, com todas as telas que você precisará interagir. Clique no vídeo ao lado e veja.</p>
                        </div>
                        <div class="bottom">
                    	    <p>Download Completo (19.2Mb)<a href="common/pdf/tutorial.pdf" id="linkPDF" target="_blank"><img src="common/img/pdf_ico.jpg" alt="" /></a></p>
                        </div>
                    </div>
                    <asp:Literal ID="ltScript" runat="server"></asp:Literal>                    
                </div>
            </div>
            
        </div>
    </div>    
</asp:Content>