﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace WebApp
{
    public partial class pesquisa : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                LoadResultados();
            }
        }

        protected void LoadResultados()
        {
            Pesquisa objPesquisa = new Pesquisa();

            ltKey.Text = Request.QueryString["key"];

            DataTable dt = objPesquisa.SearchTutorial(Request.QueryString["key"]);

            rptResultadoTutorial.DataSource = dt;
            rptResultadoTutorial.DataBind();
        }
    }
}
