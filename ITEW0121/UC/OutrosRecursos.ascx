﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OutrosRecursos.ascx.cs" Inherits="WebApp.UC.OutrosRecursos" %>
<div class="topodir">
    <p>Outros Recursos</p>
    <ul>
    	<li>
            <asp:HyperLink ID="HyperLink1" CssClass="m01" NavigateUrl="~/glossario.aspx" runat="server">
                <span>GLOSSÁRIO</span>
            </asp:HyperLink>
    	</li>
        <li>
            <asp:HyperLink ID="HyperLink2" CssClass="m02" NavigateUrl="~/faq.aspx" runat="server">
                <span>PERGUNTAS FREQUENTES</span>
            </asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink CssClass="m03" ID="HyperLink3" NavigateUrl="~/usuario-o-que-e.aspx" runat="server">
                <span>TIPOS DE USUÁRIOS</span>
            </asp:HyperLink>            
        </li>
    </ul>
    <div class="borderbottom"></div>
</div>
<div class="minheight"></div>