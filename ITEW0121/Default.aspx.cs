﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace WebApp
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                LoadVideos();

            if (Request.QueryString["id"] != null)
            {
                ltScript.Text = "<script type=\"text/javascript\">changeVideo(" + Request.QueryString["id"].ToString() + "); </script>";
            }
        }

        protected void LoadVideos()
        {
            Videos objVideos = new Videos();

            rptMenu.DataSource = objVideos.Get();
            rptMenu.DataBind();


        }
    }
}
