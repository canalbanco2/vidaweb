<%
If opcaoPorContrato = "T" Then
    Set rsDados = objCon.mLerDadosFinanceirosContrato()
Else
    Set rsDados = objCon.mLerDadosFinanceiros()
End If
rsDados.ActiveConnection = Nothing
If NOT rsDados.EOF Then
   objCon.pNR_CTR_SGRO = NR_CTR_CTC
   objCon.pNR_VRS_EDS = 1
   Set rsValorPago = objCon.mLerValorPagoApolice()
   rsValorPago.ActiveConnection = Nothing
   vl_premio = rsValorPago("val_pago")
   If IsNull(vl_premio) Then
      vl_premio = 0
   End If
   
   Set rsEndosso = objCon.mLerDadosEndosso()
   rsEndosso.ActiveConnection = Nothing
   If not rsEndosso.EOF Then
      qtd_dias_endosso = datediff("d", rsEndosso("DT_INC_VGC_CTC"), rsEndosso("DT_FIM_VGC_CTC"))
   End If

   Set rsEndossoCotacao = objCon.mLerDadosEndossoCotacao()
   rsEndossoCotacao.ActiveConnection = Nothing
   If rsEndossoCotacao.EOF Then
      valor_premio_minimo = testaDadoVazioN(rsEndossoCotacao,"VL_PREM_MIN_EDS")
   End If
   If valor_premio_minimo = "" Then
      valor_premio_minimo = 0
   End If
%>
	<tr>
		<td nowrap class="td_label">&nbsp;Moeda do seguro da cota��o</td>
		<td class="td_dado" width="800">&nbsp;
			<%=testaDadoVazio(rsDados,"CD_MOE_SGRO_CTC")%> - <%=testaDadoVazio(rsDados,"NOME_MOE_SGRO")%>
		</td>
	</tr>												
	<tr>
		<td nowrap class="td_label">&nbsp;Moeda de origem da cota��o</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazio(rsDados,"CD_MOE_OGM_CTC")%> - <%=testaDadoVazio(rsDados,"NOME_MOE_OGM")%>
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Percentual do custo de ap�lice</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"PC_CST_APLC_CTC")%>
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Percentual de corretagem</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"PC_CRE_CTC")%>
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Percentual desconto t�cnico</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"PC_DSC_TCN_CTC")%>
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Margem Comercial&nbsp;</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"Margem_Comercial")%>
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Qtd m�xima parcelas pgto do pr�mio</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"QT_PCL_PGTO_CTC")%>
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Total dias de vig�ncia do endosso</td>
		<td class="td_dado">&nbsp;
			<%=qtd_dias_endosso%>
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Pr�mio m�nimo para endosso</td>
		<td class="td_dado">&nbsp;
			<%=FormatNumber(valor_premio_minimo,2)%>
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
<%
If isnull(rsDados("VL_IPTC_MOEN_CTC")) then 
   VL_IPTC_MOEN_CTC = 0
Else
   VL_IPTC_MOEN_CTC = rsDados("VL_IPTC_MOEN_CTC")
End If

If isnull(rsDados("VL_PREM_MOEN_CTC")) then 
   VL_PREM_MOEN_CTC = 0
Else
   VL_PREM_MOEN_CTC = rsDados("VL_PREM_MOEN_CTC")
End If

If opcaoPorContrato = "T" Then
    Set rsSubgrupo = Objcon.mLerSubgrupoCotacao(True)
Else
    Set rsSubgrupo = Objcon.mLerSubgrupoCotacao(False)
End If
rsSubgrupo.ActiveConnection = Nothing
If NOT rsSubgrupo.EOF Then
   
   Do While not rsSubgrupo.EOF

      Set rsDados = Nothing
	  objCon.pNR_CTC_SGRO = rsSubgrupo("NR_CTC_SGRO")
	  objCon.pNR_VRS_CTC =  rsSubgrupo("NR_VRS_CTC")

      If opcaoPorContrato = "T" Then
         Set rsDados = Objcon.mLerDadosFinanceirosContrato(1)
      Else
         Set rsDados = Objcon.mLerDadosFinanceiros(1)
      End If
	  rsDados.ActiveConnection = Nothing
	  If NOT rsDados.EOF Then
		if not isnull(rsDados("VL_IPTC_MOEN_CTC")) then
			VL_IPTC_MOEN_CTC = cdbl(VL_IPTC_MOEN_CTC) + cdbl(rsDados("VL_IPTC_MOEN_CTC"))
		end if

		if not isnull(rsDados("VL_PREM_MOEN_CTC")) then
			VL_PREM_MOEN_CTC = cdbl(VL_PREM_MOEN_CTC) + cdbl(rsDados("VL_PREM_MOEN_CTC"))
		end if
	  End If

	  rsSubgrupo.MoveNext
   Loop
End If

valor_difer      = VL_PREM_MOEN_CTC - vl_premio
VL_IPTC_MOEN_CTC = FormatNumber(VL_IPTC_MOEN_CTC,2)
%>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor IS</td>
		<td class="td_dado">&nbsp;<%=VL_IPTC_MOEN_CTC%>
				<%
				'Response.Write "<br>" & objcon.pCD_PRD & ", " & objcon.pCD_MDLD & ", " & objcon.pCD_ITEM_MDLD
				'Response.Write "<P>"
				retorno = objCon.mValidarCapitalSegurado(CStr(VL_IPTC_MOEN_CTC))
				if isnumeric(retorno) then
					Response.Write "<font color=red>"
					if cint(retorno) = -1 then
						Response.Write " O Valor IS � menor que o m�nimo cadastrado"
					elseif cint(retorno) = 1 then
						Response.Write " O Valor IS � maior que o m�ximo cadastrado"
					end if
						Response.Write "</font>"
				elseif trim(retorno) <> "" then
					Response.Write "<p>OCORREU UM ERRO.<BR>" & retorno
					'Response.redirect("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
					Response.End 
				end if
				%>		
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Movimenta��o</td>
		<td class="td_dado">&nbsp;
			<%movim = testaDadoVazioN(rsDados,"CD_MVTC_FNCR_PREM")
			  If movim = "" Then
			     movim = 0
			  End If
			  If movim = 1 Then
			     Response.Write "sem movimenta��o"
			  ElseIf movim = 2 Then
			     Response.Write "com cobran�a"
			  ElseIf movim = 3 Then
			     Response.Write "com devolu��o"
			  Else
			     Response.Write "ainda n�o cotado"
			  End If
			%>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor pr�mio NET anual devido</td>
		<td class="td_dado">&nbsp;
			<%=FormatNumber(VL_PREM_MOEN_CTC,2)%>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor pr�mio NET anual pago</td>
		<td class="td_dado">&nbsp;
			<%=FormatNumber(vl_premio,2)%>
	</tr>
	<%If movim > 0 Then%>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor diferen�a</td>
		<td class="td_dado">&nbsp;
			<%=FormatNumber(valor_difer,2)%>
	</tr>
	<%End If%>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor desconto t�cnico</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"VL_DSC_TCN_CTC")%>
		</td>
	</tr>
	<%If movim > 0 Then%>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor pr�mio NET com desconto t�cnico</td>
		<td class="td_dado">&nbsp;
			<%
			if trim(rsDados("VL_DSC_TCN_CTC")) <> "" then
				Response.Write FormatNumber(cdbl(VL_PREM_MOEN_CTC) - cdbl(rsDados("VL_DSC_TCN_CTC")),2)
			else
				Response.Write "0,00"
			end if
			%>
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor do pr�mio NET do endosso</td>
		<td class="td_dado">&nbsp;
			<%If movim = 3 Then Response.Write "- "
			  Response.Write testaDadoVazioN(rsDados,"VL_MVTC_FNCR_PREM")%>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor do pr�mio l�quido</td>
		<td class="td_dado">&nbsp;
			<%If movim = 3 Then Response.Write "- "
			  Response.Write testaDadoVazioN(rsDados,"VL_LQDO_MOEN_CTC")%>
	</tr>
	<%End If
      If movim = 2 Then%>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor do custo da ap�lice</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"VL_CST_APLC_CTC")%>
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor IOF</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"VL_IOF_CTC")%>
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor do pr�mio bruto</td>
		<td class="td_dado">&nbsp;
			<%
			premio_bruto = 0

			if trim(rsDados("VL_LQDO_MOEN_CTC")) <> "" then
				premio_bruto = premio_bruto + cdbl(rsDados("VL_LQDO_MOEN_CTC"))
			end if
			if trim(rsDados("VL_CST_APLC_CTC")) <> "" then
				premio_bruto = premio_bruto + cdbl(rsDados("VL_CST_APLC_CTC"))
			end if
			if trim(rsDados("VL_IOF_CTC")) <> "" then
				premio_bruto = premio_bruto + cdbl(rsDados("VL_IOF_CTC"))
			end if

			Response.Write FormatNumber(premio_bruto,2)
			%></td></tr><%
     End If
End If

Set rsDados = Nothing
%>