<!--#include virtual = "/padrao.asp"-->
<!--#include virtual = "/funcao/data_corrente.asp"-->
<!--#include virtual = "/funcao/msg_vnulls.asp"-->
<!--#include virtual = "/funcao/formatar_data.asp"-->
<!--#include virtual = "/funcao/verifica_datas.asp"-->
<!--#include virtual = "/funcao/msg_vdatas.asp"-->
<!--#include virtual = "/funcao/verifica_cgcs.asp"-->
<!--#include virtual = "/funcao/msg_vcgcs.asp"-->
<!--#include virtual = "/funcao/formata_cgc.asp"-->
<!--#include virtual = "/funcao/jscript.asp"-->
<!--#include virtual = "/funcao/abre_janela.asp"-->
<!--#include virtual = "/internet/serv/siscot3/request_geral.asp"-->
<!--#include virtual = "/internet/serv/siscot3/objWorkflowNegocio.asp"-->
<%
function VerNulo(str)
	VerNulo = str
end function

if Trim(TP_RAMO_ID) = "" then
	Aux = Aux & "Tipo de Ramo: <font color='black'>Todos</font>&nbsp;&nbsp;&nbsp;"
else
	vRamoId = NomeTipoRamo(TP_RAMO_ID)
	Aux = Aux & "Tipo de Ramo: <font color='black'>"& vRamoId &"</font>&nbsp;&nbsp;&nbsp;"
end if

if Trim(F_ramo) = "" then
	Aux = Aux & "Ramo: <font color='black'>Todos</font>&nbsp;&nbsp;&nbsp;"
else
	Aux = Aux & "Ramo: <font color='black'>"& F_ramo &"</font>&nbsp;&nbsp;&nbsp;"
end if

if Trim(F_SubRamo) = "" then
	Aux = Aux & "Subramo: <font color='black'>Todos</font>&nbsp;&nbsp;&nbsp;"
else
	Aux = Aux & "Subramo: <font color='black'>"& F_SubRamo &"</font>&nbsp;&nbsp;&nbsp;"
end if

select case Ucase(F_canal)
	case ""
		Aux = Aux & "Canal: <font color='black'>Todos</font>&nbsp;&nbsp;&nbsp;"
	case "NULL"
		Aux = Aux & "Canal: <font color='black'>Todos</font>&nbsp;&nbsp;&nbsp;"
	case "1"
		Aux = Aux & "Canal: <font color='black'>Corretor independente</font>&nbsp;&nbsp;&nbsp;"
	case "2"
		Aux = Aux & "Canal: <font color='black'>Ag�ncia</font>&nbsp;&nbsp;&nbsp;"
end select

select case Ucase(F_operacao)
	case ""
		Aux = Aux & "Opera��o: <font color='black'>Todas</font>&nbsp;&nbsp;&nbsp;"
	case "NULL"
		Aux = Aux & "Opera��o: <font color='black'>Todas</font>&nbsp;&nbsp;&nbsp;"
	case "1"
		Aux = Aux & "Opera��o: <font color='black'>Contrata��o</font>&nbsp;&nbsp;&nbsp;"
	case "2"
		Aux = Aux & "Opera��o: <font color='black'>Renova��o</font>&nbsp;&nbsp;&nbsp;"
	case "3"
		Aux = Aux & "Opera��o: <font color='black'>Endosso</font>&nbsp;&nbsp;&nbsp;"
	case "4"
		Aux = Aux & "Opera��o: <font color='black'>Endosso de cancelamento</font>&nbsp;&nbsp;&nbsp;"
end select

'Manter apenas Cota��o BB
'if trim(F_cotacao) = "" then
'	Aux = Aux & "Cota��o: <font color='black'>Todas</font>&nbsp;&nbsp;&nbsp;"
'else
'	Aux = Aux & "Cota��o: <font color='black'>" & F_Cotacao & "</font>&nbsp;&nbsp;&nbsp;"
'end if

If trim(F_cotacao_bb) = "" then
	Aux = Aux & "Cota��o BB: <font color='black'>Todas</font>&nbsp;&nbsp;&nbsp;"
Else
	Aux = Aux & "Cota��o BB: <font color='black'>" & F_Cotacao_BB & "</font>&nbsp;&nbsp;&nbsp;"
End If

If trim(F_cnpj) = "" Then
	Aux = Aux & "<br>CNPJ: <font color='black'>Todos</font>&nbsp;&nbsp;&nbsp;"
Else
	Aux = Aux & "<br>CNPJ: <font color='black'>" & F_CNPJ & "</font>&nbsp;&nbsp;&nbsp;"
End If

If trim(F_contrato_bb) = "" Then
	Aux = Aux & "Contrato BB: <font color='black'>Todos</font>&nbsp;&nbsp;&nbsp;"
Else
	Aux = Aux & "Contrato BB: <font color='black'>" & F_contrato_bb & "</font>&nbsp;&nbsp;&nbsp;"
End If

If trim(F_corretor) = "" Then
	Aux = Aux & "Corretor: <font color='black'>Todos</font>&nbsp;&nbsp;&nbsp;"
Else
	Aux = Aux & "Corretor: <font color='black'>" & F_corretor & "</font>&nbsp;&nbsp;&nbsp;"
End If

If trim(F_super) = "" Then
	Aux = Aux & "Superintend�ncia: <font color='black'>Todas</font>&nbsp;&nbsp;&nbsp;"
Else
	Aux = Aux & "Superintend�ncia: <font color='black'>" & F_super & "</font>&nbsp;&nbsp;&nbsp;"
End If

If trim(F_segmento) = "" Then
	Aux = Aux & "Segmento: <font color='black'>Todos</font>&nbsp;&nbsp;&nbsp;"
Else
	Aux = Aux & "Segmento: <font color='black'>" & F_segmento & "</font>&nbsp;&nbsp;&nbsp;"
End If

if Trim(F_Agencia)  = "" Then
	Aux = Aux & "Ag�ncia: <font color='black'>Todos</font>&nbsp;&nbsp;&nbsp;"
else
	Aux = Aux & "Ag�ncia: <font color='black'>"& F_Agencia &"</font>&nbsp;&nbsp;&nbsp;"	
end if

If F_tp_ini = "P" Then
	if ( isnull(F_DiaIni) and isnull(F_MesIni) and not isnull(F_AnoIni) ) or _
	( isnull(F_DiaIni) and not isnull(F_MesIni) and isnull(F_AnoIni) ) or _
	( not isnull(F_DiaIni) and isnull(F_MesIni) and isnull(F_AnoIni) ) then
		strerror = strerror & "<LI>A data inicial est� incorreta."
	End If

	if ( isnull(F_DiaFim) and isnull(F_MesFim) and not isnull(F_AnoFim) ) or _
	( isnull(F_DiaFim) and not isnull(F_MesFim) and isnull(F_AnoFim) ) or _
	( not isnull(F_DiaFim) and isnull(F_MesFim) and isnull(F_AnoFim) ) then
		strerror = strerror & "<LI>A data final est� incorreta."
	End If
	
	if strerror = "" then
		if verifica_data(F_DiaIni,F_MesIni,F_AnoIni) = "False" then
			'strerror = strerror & "<li>" & "O campo 'Data Inicial' � inv�lido."
		end if

		if verifica_data(F_DiaFim,F_MesFim,F_AnoFim) = "False" then
			'strerror = strerror & "<li>" & "O campo 'Data Final' � inv�lido."
		end if
	end if

	DtInicio = F_MesIni & "/" & F_DiaIni & "/" & F_AnoIni
	DtFim = F_MesFim & "/" & F_DiaFim & "/" & F_AnoFim

	if strerror = "" and Len(F_MesIni & F_DiaIni & F_AnoIni) > 0 and Len(F_MesFim & F_DiaFim & F_AnoFim) > 0 then
		if CDate(F_DtInicio) > CDate(F_DtFim) then
				strerror = strerror & "<li>" & "A Data Inicial deve ser anterior a Data Final."
		end if
	end if
	
	If Len(F_MesIni & F_DiaIni & F_AnoIni) > 0 Then
		If Len(F_MesFim & F_DiaFim & F_AnoFim) > 0 Then
			Aux = Aux & "<br>Per�odo de vig�ncia: <font color='black'>" & dtInicio & " at� " & F_DiaIni & "/" & F_MesIni & "/" & F_AnoIni & "</font>&nbsp;&nbsp;&nbsp;"
		Else
			Aux = Aux & "<br>Per�odo de vig�ncia: <font color='black'>" & F_DiaIni  & "/" & F_MesIni & "/" & F_AnoIni & "</font>&nbsp;&nbsp;&nbsp;"
		End If
	Else
		If Len(F_MesFim & F_DiaFim & F_AnoFim) > 0 Then
			Aux = Aux & "<br>Per�odo de vig�ncia: <font color='black'>" & F_DiaFim & "/" & F_MesFim & "/" & F_AnoFim & "</font>&nbsp;&nbsp;&nbsp;"
		End If
	End If

else
	Aux = Aux & "<br>Per�odo de Vig�ncia: <font color='black'>Todos</font>&nbsp;&nbsp;&nbsp;"
end if

if trim(F_instancia) = "" then
	Aux = Aux & "Inst�ncia: <font color='black'>Todas</font>&nbsp;&nbsp;&nbsp;"
else
	Aux = Aux & "Inst�ncia: <font color='black'>" & instancia & "</font>&nbsp;&nbsp;&nbsp;"
end if

If F_tp_iniInst = "P" Then
	if ( isnull(F_DiaIniInst) and isnull(F_MesIniInst) and not isnull(F_AnoIniInst) ) or _
	( isnull(F_DiaIniInst) and not isnull(F_MesIniInst) and isnull(F_AnoIniInst) ) or _
	( not isnull(F_DiaIniInst) and isnull(F_MesIniInst) and isnull(F_AnoIniInst) ) then
		strerror = strerror & "<LI>A data inicial est� incorreta."
	End If

	if ( isnull(F_DiaFimInst) and isnull(F_MesFimInst) and not isnull(F_AnoFimInst) ) or _
	( isnull(F_DiaFimInst) and not isnull(F_MesFimInst) and isnull(F_AnoFimInst) ) or _
	( not isnull(F_DiaFimInst) and isnull(F_MesFimInst) and isnull(F_AnoFimInst) ) then
		strerror = strerror & "<LI>A data final est� incorreta."
	End If
	
	if strerror = "" then
		if verifica_data(F_DiaIniInst,F_MesIniInst,F_AnoIniInst) = "False" then
			'strerror = strerror & "<li>" & "O campo 'Data Inicial' � inv�lido."
		end if

		if verifica_data(F_DiaFimInst,F_MesFimInst,F_AnoFimInst) = "False" then
			'strerror = strerror & "<li>" & "O campo 'Data Final' � inv�lido."
		end if
	end if

	DtInicioInst = F_MesIniInst & "/" & F_DiaIniInst & "/" & F_AnoIniInst
	DtFimInst = F_MesFimInst & "/" & F_DiaFimInst & "/" & F_AnoFimInst

	if strerror = "" and Len(F_MesIniInst & F_DiaIniInst & F_AnoIniInst) > 0 and Len(F_MesFimInst & F_DiaFimInst & F_AnoFimInst) > 0 then
		if CDate(F_DtInicioInst) > CDate(F_DtFimInst) then
				strerror = strerror & "<li>" & "A Data Inicial deve ser anterior a Data Final."
		end if
	end if
	
	If Len(F_MesIniInst & F_DiaIniInst & F_AnoIniInst) > 0 Then
		If Len(F_MesFimInst & F_DiaFimInst & F_AnoFimInst) > 0 Then
			Aux = Aux & "Iniciadas em: <font color='black'>" & dtInicioInst & " at� " & F_DiaIniInst & "/" & F_MesIniInst & "/" & F_AnoIniInst & "</font>&nbsp;&nbsp;&nbsp;"
		Else
			Aux = Aux & "Iniciadas em: <font color='black'>" & F_DiaIniInst  & "/" & F_MesIniInst & "/" & F_AnoIniInst & "</font>&nbsp;&nbsp;&nbsp;"
		End If
	Else
		If Len(F_MesFim & F_DiaFim & F_AnoFim) > 0 Then
			Aux = Aux & "Iniciadas em: <font color='black'>" & F_DiaFimInst & "/" & F_MesFimInst & "/" & F_AnoFimInst & "</font>&nbsp;&nbsp;&nbsp;"
		End If
	End If

else
	Aux = Aux & "Iniciadas em: <font color='black'>Todos</font>&nbsp;&nbsp;&nbsp;"
end if

select case Ucase(F_wf_situacao)
	case ""
		Aux = Aux & "Situa��o da Inst�ncia: <font color='black'>Todos</font>&nbsp;&nbsp;&nbsp;"
	case "NULL"
		Aux = Aux & "Situa��o da Inst�ncia: <font color='black'>Todos</font>&nbsp;&nbsp;&nbsp;"
	case "1"
		Aux = Aux & "Situa��o da Inst�ncia: <font color='black'>Ativa</font>&nbsp;&nbsp;&nbsp;"
	case "2"
		Aux = Aux & "Situa��o da Inst�ncia: <font color='black'>Encerrada</font>&nbsp;&nbsp;&nbsp;"
	case "3"
		Aux = Aux & "Situa��o da Inst�ncia: <font color='black'>Suspensa</font>&nbsp;&nbsp;&nbsp;"
	case "9"
		Aux = Aux & "Situa��o da Inst�ncia: <font color='black'>Cancelada</font>&nbsp;&nbsp;&nbsp;"
end select

if trim(F_tp_ativ_id_informado) = "" then
	Aux = Aux & "Atividade: <font color='black'>Todas</font>&nbsp;&nbsp;&nbsp;"
else
	objcon.pTP_ATIV_ID = F_tp_ativ_id_informado
	objcon.pTP_WF_ID = tp_wf_id
	set rsNomeAtividade = objcon.mLerNomeAtividade()
	Aux = Aux & "Atividade: <font color='black'>" & F_tp_ativ_id_informado & " - " & rsNomeAtividade("nome") & "</font>&nbsp;&nbsp;&nbsp;"
end if

select case Ucase(F_ativ_situacao)
	case ""
		Aux = Aux & "Situa��o da Atividade: <font color='black'>Todos</font>&nbsp;&nbsp;&nbsp;"
	case "NULL"
		Aux = Aux & "Situa��o da Atividade: <font color='black'>Todos</font>&nbsp;&nbsp;&nbsp;"
	case "1"
		Aux = Aux & "Situa��o da Atividade: <font color='black'>Habilitada</font>&nbsp;&nbsp;&nbsp;"
	case "5"
		Aux = Aux & "Situa��o da Atividade: <font color='black'>Executada</font>&nbsp;&nbsp;&nbsp;"
end select

'if trim(TP_RAMO_ID) = "" then
	'strerror = strerror & "<li>� necess�rio escolher um 'Tipo de Ramo'.</li>"
'end if

if trim(instancia) <> "" then
	if not isnumeric(trim(instancia)) then
		strerror = strerror & "<li>O campo 'Inst�ncia' � inv�lido.</li>"
	end if
end if

if trim(agencia) <> "" and rb_agencia="N" then
	if not isnumeric(trim(agencia)) then
		strerror = strerror & "<li>" & "O campo 'Ag�ncia' � inv�lido.</li>"
	end if
end if

if trim(strerror) <> "" then
	%><!--#include virtual = "/funcao/msg_erro_gestao_cotacao.asp"--><%
	response.end
end if

ord = request("ord")
if ord = "" then
	ord = "1"
else
	ord = ord
end if

acao = request("acao")
if trim(ucase(acao)) = "S" then
	sql = "exec s_dados_usuario_sps " & usuario_id & ", " & unidade_id
	%><!--#include virtual = "/scripts/segab_db.asp"--><%
	set rs_usuario = conex.execute(sql)
	cpf_usuario_id = rs_usuario("cpf")
	set rs_usuario = Nothing
	%><!--#include virtual = "/funcao/close_conex.asp"--><%
	%><!--#include virtual = "/scripts/workflow_db.asp"--><%
	wf_estado = request("wf_estado")
	wf_id = request("wf_id")
	sql = "set ANSI_WARNINGS off exec wf_estado_spu " & wf_id & ", " & wf_estado & ", " & usuario_id & ", '', " & "'" & cpf_usuario_id & "'"
	conex.execute(sql)
	%><!--#include virtual = "/funcao/conexao_erro.asp"--><%
end if
%>
<script language="JavaScript">
<!--
function MostraAtividade(id, link, sit, page, tp_wf_id)
{
	document.frmInstancias.wf_id.value = id;
	if (sit == 1){
		document.frmInstancias.wf_estado.value = sit;
		document.frmInstancias.ver_atividade.value = 'S';
		document.frmInstancias.page.value = page;
		document.frmInstancias.tp_wf_id.value = tp_wf_id;
		
		HabilitaBotoes(sit);
		document.frmInstancias.submit();
	} else {
		
		var vRet = window.confirm('A inst�ncia solicitada n�o est� ativa. Deseja consult�-la?');
		if (vRet)
		{
			ConsultaInstancia( id );
		}
	}
}

function HabilitaBotoes(sit)
{
	if (sit == 3) {
		document.frmInstancias.btnAtivar.disabled = false;
		document.frmInstancias.btnEncerrar.disabled = true;
		document.frmInstancias.btnSuspender.disabled = true;
	}
	else {
		if ((sit == 9) || (sit == 2)) {
			document.frmInstancias.btnAtivar.disabled = true;
			document.frmInstancias.btnEncerrar.disabled = true;
			document.frmInstancias.btnSuspender.disabled = true;
		}
		else {
			document.frmInstancias.btnAtivar.disabled = true;
			document.frmInstancias.btnEncerrar.disabled = false;
			document.frmInstancias.btnSuspender.disabled = false;
		}
	}
	
	document.frmInstancias.btnConsultar.disabled = false;
}

function AtualizaEstadoInstancia(wf_estado)
{
	if (wf_estado == 3)
	{
		if (!confirm('Confirma suspens�o da cota��o?'))
			return;
	}
	else if (wf_estado == 9)
	{
		if (!confirm('Confirma encerramento da cota��o?'))
			return;
	}
	
	document.frmInstancias.action = 'corpo2a.asp';
	document.frmInstancias.target = 'instancias';
	document.frmInstancias.wf_estado.value = wf_estado;
	document.frmInstancias.acao.value = 'S';
	document.frmInstancias.ver_atividade.value = '';
	window.parent.atividades.location = "branco.htm";
	document.frmInstancias.submit();
}

function ConsultaInstancia( id, tp )
{
	if (id == undefined)
	{
		id = document.frmInstancias.wf_id.value;
		tp = document.frmInstancias.tp_wf_id.value;
	}
	window.launchFull('redireciona_atividade.asp?ConsultaInstancia=S&wf_id=' + id + '&tp_wf_id=' + tp + '&usuario_id= <%=arrParametro(7)%>','janelaNova');
	parent.atividades.location = 'branco.htm';
}

function ExportarInstancia()
{
     var wnd = window.open('geraxls_instancia.asp','Exportar',
               "resizable=yes,scrollbars=yes,width=700,height=500,toolbar=1,location=0,directories=0,status=0,menubar=1");
	
}
//-->
</script>
<HTML>
<head>
<LINK href="/internet/serv/aventure_se/acomp/estilo_acomp.css" rel="STYLESHEET" title="style" type="text/css">
<title>Companhia de Seguros Alian�a do Brasil</title>
</HEAD>
<BODY BGCOLor="#FFFFFF" LINK="#0000FF" VLINK="#800080" TEXT="#000000" TOPMARGIN="0" LEFTMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0">
<form name="frmInstancias" method="post" action="corpo2b.asp" target="atividades">
<input type="hidden" name="wf_id" value="<%=request("wf_id")%>">
<input type="hidden" name="wf_estado" value="">
<input type="hidden" name="acao" value="">
<input type="hidden" name="ver_atividade" value="">
<input type="hidden" name="page" value="">
<input type="hidden" name="tp_wf_id" value="">
<input type="hidden" name="ord" value="<%=ord%>">

<script language="JavaScript">
<!--
var x = screen.width;
var y = screen.height;
	
if ( ((x > 1152) || (y > 864)) || ((x > 1024) || (y > 768)) || ((x > 800) || (y > 600)) )
	document.write("<table align='center' cellpadding='2' cellspacing='1' border='0' width='850px' class='escolha'>");
else if ( (x > 640) || (y > 480) )
	document.write("<table align='center' cellpadding='2' cellspacing='1' border='0' width='750px' class='escolha'>");
//-->
</script>

	<tr>
		<td style="font-size:5px" colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td class="td_titulo" colspan="2">Lista de Solicita��es de Cota��es</td>
	</tr>
	<tr>
		<td class="data" colspan="2">Neg�cios com atividades atribu�das ao usu�rio - <% = DataCorrente()%></td>
	</tr>
	<tr>
		<td align="center" class="td_dado" colspan="2"><font color="navy">Resultado da Pesquisa<BR><% = Aux%></font></td>
	</tr>

<script language="JavaScript">
<!--
var x = screen.width;
var y = screen.height;
	
if ( ((x > 1152) || (y > 864)) || ((x > 1024) || (y > 768)) || ((x > 800) || (y > 600)) )
	document.write("<tr valign='top'><td>");
else if ( (x > 640) || (y > 480) )
	document.write("</table>");
//-->
</script>

<!--In�cio INST�NCIAS-->
<center>
<%
'Periodo de Vig�ncia do Seguro
if F_AnoIni = "" then
   dtInicioVig = "null"
else
   dtInicioVig = "'" & F_AnoIni & "-" & F_MesIni & "-" & F_DiaIni & "'"
end if

if F_AnoFim = "" then
   dtFimVig = "null"
else
   dtFimVig = "'" & F_AnoFim & "-" & F_MesFim & "-" & F_DiaFim & "'"
end if

'Periodo de cria��o da instancia
if F_AnoIniInst = "" then
   dtInicio = "null"
else
   dtInicio = "'" & F_AnoIniInst & "-" & F_MesIniInst & "-" & F_DiaIniInst & "'"
end if

if F_AnoFimInst = "" then
   dtFim = "null"
else
   dtFim = "'" & F_AnoFimInst & "-" & F_MesFimInst & "-" & F_DiaFimInst & "'"
end if

iPageSize = 100
sql = "set nocount on exec als_operacao_db..w_busca_instancias_siscot_sps "
if trim(F_ramo) <> "" then
	sql = sql & F_ramo & ","
else
	sql = sql & "null,"
end if
if trim(F_subramo) <> "" then
	sql = sql & F_subramo & ","
else
	sql = sql & "null,"
end if
if trim(F_cotacao) <> "" then
	sql = sql & F_cotacao & ","
else
	sql = sql & "null,"
end if
'retirado a pedido DITEC
if trim(F_cotacao_bb) <> "" then
	sql = sql & F_cotacao_bb & ","
else
	sql = sql & "null,"
end if
if trim(F_situacao) <> "" then
	sql = sql & "'" & F_situacao & "',"
else
	sql = sql & "null,"
end if
if trim(F_agencia) <> "" then
	sql = sql & F_agencia & ","
else
	sql = sql & "null,"
end if
if trim(F_canal) <> "" then
	sql = sql & F_canal & ","
else
	sql = sql & "null,"
end if
if trim(tp_ramo_id) <> "" then
	sql = sql & tp_ramo_id & ","
else
	sql = sql & "null,"
end if

if trim(tp_wf_id) <> "" then
    If Request("F_canal") = "" Then
       If tp_wf_id = Application("WF_RE") Then
          tp_wf_id = "'" &tp_wf_id & "," & Application("WF_RE_CI")& "'"
       ElseIf tp_wf_id = Application("WF_VG") Then
          tp_wf_id = "'" &tp_wf_id & "," & Application("WF_VG_CI")& "'"
       ElseIf tp_wf_id = Application("WF_REEDS") Then
          tp_wf_id = "'" &tp_wf_id & "," & Application("WF_REEDS_CI")& "'"
       ElseIf tp_wf_id = Application("WF_VGEDS") Then
          tp_wf_id = "'" &tp_wf_id & "," & Application("WF_VGEDS_CI")& "'"
       ElseIf tp_wf_id = Application("WF_EDSC_CANCELAMENTO") Then
          tp_wf_id = "'" & tp_wf_id & "," & Application("WF_EDSC_CANCELAMENTO_CI") & "'"
       End If
    End If
 	sql = sql & tp_wf_id & ","
else
	sql = sql & "null,"
end if
sql = sql & " null," 'usuario_id & ","
sql = sql & dtInicio & ","
sql = sql & dtFim & ","
if trim(F_wf_situacao) <> "" then
	sql = sql & F_wf_situacao & ","
else
	sql = sql & "null,"
end if
if trim(F_instancia) <> "" then
	sql = sql & F_instancia & ","
else
	sql = sql & "null,"
end if
if trim(F_tp_ativ_id_informado) <> "" then
	sql = sql & F_tp_ativ_id_informado & ","
else
	sql = sql & "null,"
end if
if trim(F_ativ_situacao) <> "" then
	sql = sql & F_ativ_situacao & ","
else
	sql = sql & "null,"
end if
if trim(F_operacao) <> "" then
	sql = sql & F_operacao & ","
else
	sql = sql & "null,"
end if

'Ordena��o
sql = sql & ord & ", "

If Trim(F_CNPJ) <> "" Then
	sql = sql & "'" & F_CNPJ & "',"
Else
	sql = sql & "null,"
End If

sql = sql & dtInicioVig & ","
sql = sql & dtFimVig 

'if trim(F_Contrato_BB) <> "" then
'	sql = sql & F_Contrato_BB & ","
'else
'	sql = sql & "null,"
'end if
'
'if trim(F_Corretor) <> "" then
'	sql = sql & F_Corretor & ","
'else
'	sql = sql & "null,"
'end if

'if trim(F_Super) <> "" then
'	sql = sql & F_Super & ","
'else
'	sql = sql & "null,"
'end if

'if trim(F_Segmento) <> "" then
'	sql = sql & F_Segmento & ","
'else
'	sql = sql & "null,"
'end if

'Response.Write "<font color=black>" & sql & "</font>"
'Response.End

Set strConex = server.CreateObject("conexao_alianca_ole.Connection")
site = Request.ServerVariables("HTTP_HOST")
'buscando a conex�o para o ambiente utilizado (desenvolvimento, qualidade ou produ��o)
if ucase(mid(site,1,3)) = "QLD" then
   ConnectString = strConex.Internet_Qld_Web_Seguros
elseif ucase(mid(site,1,4)) = "WWW2" then
   ConnectString = strConex.Internet_Qld_Web_Seguros   
elseif ucase(mid(site,1,3)) = "WWW" then
   ConnectString = strConex.Internet_Prod_Web_Seguros
else
   ConnectString = strConex.Internet_Des_Web_Seguros
end if
set strConex = Nothing

redim Labels(15)
redim OrdAsc(15)
redim OrdDesc(15)

Labels(1) = "N�mero"
OrdAsc(1) = "1"
OrdDesc(1) = "2"

Labels(2) = "Cota��o"
OrdAsc(2) = "3"
OrdDesc(2) = "4"

Labels(3) = "Canal"
OrdAsc(3) = "5"
OrdDesc(3) = "6"

Labels(4) = "Ag�ncia"
OrdAsc(4) = "5"
OrdDesc(4) = "6"

'Labels(4) = "Cota��o BB"
'OrdAsc(4) = "7"
'OrdDesc(4) = "8"

Labels(5) = "Dt. Pedido"
OrdAsc(4) = "9"
OrdDesc(5) = "10"

Labels(6) = "Subramo"
OrdAsc(6) = "11"
OrdDesc(6) = "12"

Labels(7) = "Qt. Obj. Risco"
OrdAsc(7) = "13"
OrdDesc(7) = "14"

Labels(8) = "Valor IS"
OrdAsc(8) = "15"
OrdDesc(8) = "16"

Labels(9) = "Dt. In�cio Vig."
OrdAsc(9) = "17"
OrdDesc(9) = "18"

Labels(10) = "Nome Proponente"
OrdAsc(10) = "19"
OrdDesc(10) = "20"

Labels(11) = "CNPJ"
OrdAsc(11) = "27"
OrdDesc(11) = "28"

Labels(12) = "�ltima Atividade"
OrdAsc(12) = "21"
OrdDesc(12) = "22"

Labels(13) = "Situa��o"
OrdAsc(13) = "23"
OrdDesc(13) = "24"

Labels(14) = "Opera��o"
OrdAsc(14) = "25"
OrdDesc(14) = "26"

Labels(15) = "N�mero"
OrdAsc(15) = "1"
OrdDesc(15) = "2"


Link = "corpo2a.asp?ord=" & ord & "&"
TableWidth = "950px"
%><!--#include virtual = "/funcao/paginacao_siscot.asp"-->
</center>

<script language="JavaScript">
var x = screen.width;
var y = screen.height;
	
if ( ((x > 1152) || (y > 864)) || ((x > 1024) || (y > 768)) || ((x > 800) || (y > 600)) )
	document.write("</td><td align='right' style='font-size:8px'>");
else if ( (x > 640) || (y > 480) )
	document.write("<center>");
</script>

<%If iPageCount > 0 Then%>
	<br>
	<script language="JavaScript">
	var x = screen.width;
	var y = screen.height;
		
	if ( ((x > 1152) || (y > 864)) || ((x > 1024) || (y > 768)) || ((x > 800) || (y > 600)) ) {
		document.write("<br><INPUT style='width:100px' TYPE='button' Value=' Encerrar ' onclick='AtualizaEstadoInstancia(9)' name='btnEncerrar' id='btnEncerrar' disabled><br><br>");
		document.write("<INPUT style='width:100px' TYPE='button' Value=' Suspender ' onclick='AtualizaEstadoInstancia(3)' name='btnSuspender' id='btnSuspender' disabled><br><br>");
		document.write("<INPUT style='width:100px' TYPE='button' Value=' Ativar ' onclick='AtualizaEstadoInstancia(1)' name='btnAtivar' id='btnAtivar' disabled><br><br>");
		document.write("<INPUT style='width:100px' TYPE='button' Value=' Consultar ' onclick='ConsultaInstancia()' name='btnConsultar' id='btnConsultar'><br><br>");
		document.write("<INPUT style='width:100px' TYPE='button' Value=' Exportar Excel ' onclick='ExportarInstancia()' name='btnExportar' id='btnExportar'><br><br>");
		document.write("<INPUT style='width:100px' TYPE='Button' Value='<%=btVolta%>' OnClick=document.frmInstancias.target='corpo';document.frmInstancias.action='corpo2.asp';document.frmInstancias.submit();>");
	}
	else if ( (x > 640) || (y > 480) ) {
		document.write("<INPUT TYPE='button' Value=' Encerrar ' onclick='AtualizaEstadoInstancia(9)' name='btnEncerrar' id='btnEncerrar'>&nbsp;");
		document.write("<INPUT TYPE='button' Value=' Suspender ' onclick='AtualizaEstadoInstancia(3)' name='btnSuspender' id='btnSuspender'>&nbsp;");
		document.write("<INPUT TYPE='button' Value=' Ativar ' onclick='AtualizaEstadoInstancia(1)' name='btnAtivar' id='btnAtivar' disabled>&nbsp;");
		document.write("<INPUT TYPE='button' Value=' Consultar ' onclick='ConsultaInstancia()' name='btnConsultar' id='btnConsultar'>&nbsp;");
		document.write("<INPUT TYPE='button' Value=' Exportar Excel ' onclick='ExportarInstancia()' name='btnExportar' id='btnExportar'>&nbsp;");
		document.write("<INPUT TYPE='Button' Value='<%=btVolta%>' OnClick=document.frmInstancias.target='corpo';document.frmInstancias.action='corpo2.asp';document.frmInstancias.submit();>&nbsp;");
		document.write("</center>");
	}
	</script>
<%Else%>
	<center>
	<br><br><br>
	<!--INPUT TYPE="Button" Value="<%=btVolta%>" OnClick="document.frmInstancias.target='corpo';document.frmInstancias.action='corpo2.asp';document.frmInstancias.submit();"-->
	<INPUT TYPE="Button" Value="<%=btVolta%>" OnClick="javascript:history.back();">
	</center>
<%End If%>

<script language="JavaScript">
var x = screen.width;
var y = screen.height;
	
if ( ((x > 1152) || (y > 864)) || ((x > 1024) || (y > 768)) || ((x > 800) || (y > 600)) )
	document.write("</td></tr></table>");

<%if trim(wf_estado) <> "" then%>
	sit = <%=trim(wf_estado)%>;
	HabilitaBotoes(sit);
<%end if%>
</script>
</form>

<!--Fim INST�NCIAS-->

</BODY>
</HTML>
<!--include virtual = "/funcao/close_conex.asp"-->