<%
   Set rs = objcon.mLerDadosFinanceiros(2)
   rs.ActiveConnection = Nothing
   num_cotacao = objCon.pNR_CTC_SGRO

   If Trim(request("salvarfin")) = "S" Then
	   IN_FAT_UNCO_SGR = trim(request("IN_FAT_UNCO_SGR"))
	   PC_CRE_CTC = trim(request("PC_CRE_CTC"))
	   PC_IOF_CTC = trim(request("PC_IOF_CTC"))
	   VL_IOF_CTC = trim(request("VL_IOF_CTC"))
	   QT_PCL_PGTO_CTC = trim(request("QT_PCL_PGTO_CTC"))
	   PC_ETLE_CTC = trim(request("PC_ETLE_CTC"))
	   VL_LQDO_MOEN_CTC = trim(request("VL_LQDO_MOEN_CTC"))
	   PC_DA_CTC = trim(request("PC_DA_CTC"))
	   VL_PREM_PURO = trim(request("VL_PREM_PURO"))
	   VL_SGRA_MOEN_CTC = trim(request("VL_SGRA_MOEN_CTC"))
	   VL_IPTC_MOEN_CTC = trim(request("VL_IPTC_MOEN_CTC"))
	   VL_IOF_CTC = trim(request("VL_IOF_CTC"))
	   PC_MED_SGR = trim(request("PC_MED_SGR"))
       CD_MVTC_FNCR_PREM = Trim(Request("CD_MVTC_FNCR_PREM"))
       VL_MVTC_FNCR_PREM = Trim(Request("VL_MVTC_FNCR_PREM"))
   Else
	   IN_FAT_UNCO_SGR = Trim(trata_sim(testaDadoVazio(rs,"IN_FAT_UNCO_SGR")))
	   PC_CRE_CTC = testaDadoVazioN(rs,"PC_CRE_CTC")
	   PC_IOF_CTC = testaDadoVazioN(rs,"PC_IOF_CTC")
	   VL_IOF_CTC = testaDadoVazioN(rs,"VL_IOF_CTC")
	   QT_PCL_PGTO_CTC = testaDadoVazioN(rs,"QT_PCL_PGTO_CTC")
	   PC_ETLE_CTC = testaDadoVazioN(rs,"PC_ETLE_CTC")
	   VL_LQDO_MOEN_CTC = testaDadoVazioN(rs,"VL_LQDO_MOEN_CTC")
	   PC_DA_CTC = testaDadoVazioN(rs,"PC_DA_CTC")
	   VL_PREM_PURO = testaDadoVazioN(rs,"VL_PREM_PURO")
	   VL_SGRA_MOEN_CTC = testaDadoVazioN(rs,"VL_SGRA_MOEN_CTC")
	   VL_IPTC_MOEN_CTC = testaDadoVazioN(rs,"VL_IPTC_MOEN_CTC")
	   VL_IOF_CTC = testaDadoVazioN(rs,"VL_IOF_CTC")
 	   PC_MED_SGR = testaDadoVazioN(rs,"PC_MED_SGR")
	   CD_MVTC_FNCR_PREM = testaDadoVazioN(rs,"CD_MVTC_FNCR_PREM")
	   VL_MVTC_FNCR_PREM = testaDadoVazioN(rs,"VL_MVTC_FNCR_PREM")
   End If

   Set rsCotacao = objcon.mLerCotacao()
   rsCotacao.ActiveConnection = Nothing
   data_fim_vigencia = rsCotacao("DT_INC_VGC_CTC")
   data_inicio_vigencia = rsCotacao("DT_FIM_VGC_CTC")
   NR_CTR_CTC = rsCotacao("nr_ctr_ctc")
   Set rsCotacao = Nothing

  'Regra sobre parcelas 
   If not isnull(data_fim_vigencia) and not isnull(data_inicio_vigencia) Then
	  dias = datediff("d",data_fim_vigencia,data_inicio_vigencia)
	  parcelas = int(dias/30)
   Else
	  dias = 0
	  parcelas = QT_PCL_PGTO_CTC
   End If
  'parcelas maximas
   objCon.pTP_PESQ = "BB" 
   objCon.pCD_TIP_OPR_CTC = "3"
   num_parc_max_pmt = objCon.mLerQtdMaxPclSeguro()
   If num_parc_max_pmt < parcelas Then
      parcelas = num_parc_max_pmt
      If parcelas = 0 Then
         parcelas = 1
      End If
      QT_PCL_PGTO_CTC = parcelas
   End If

   valor_premio_minimo = Trim(Request("valor_premio_minimo"))
   valor_prem_min_prpt = 0
   percentual_iof = 0

   Set rsEndossoCotacao = objCon.mLerDadosEndossoCotacao()
   rsEndossoCotacao.ActiveConnection = Nothing
   If rsEndossoCotacao.EOF Then
	  valor_premio_minimo = 0
   Else
      valor_prem_min_prpt   = testaDadoVazioN(rsEndossoCotacao,"VL_PREM_MIN_PRPT")
 	  valor_premio_minimo   = testaDadoVazioN(rsEndossoCotacao,"VL_PREM_MIN_EDS")
      If IsNull(rs("PC_IOF_CTC")) Then
         percentual_iof = testaDadoVazioN(rsEndossoCotacao,"PC_IOF")
      End If
   End If

   Set rsEndosso = objCon.mLerDadosEndosso()
   rsEndosso.ActiveConnection = Nothing
   If not rsEndosso.EOF Then
	  qtd_dias_endosso = datediff("d", rsEndosso("DT_INC_VGC_CTC"), rsEndosso("DT_FIM_VGC_CTC"))
   End If

   objCon.pNR_CTR_SGRO = NR_CTR_CTC
   objCon.pNR_VRS_EDS  = 1

   Set rsCTR = objcon.mLerDadosFinanceirosContrato(2) 
   rsCTR.ActiveConnection = Nothing
   If NOT rsCTR.EOF Then
       If PC_ETLE_CTC = "" Then
	      percentual_pro_labore = testaDadoVazioN(rsCTR,"PC_ETLE_CTC")
	      PC_CRE_CTC  = testaDadoVazioN(rsCTR,"PC_CRE_CTC")	
	   End If
       valor_LQDO_Anterior = testaDadoVazioN(rsCTR,"VL_LQDO_MOEN_CTC")
   End If
   Set rsCTR = Nothing

   If vlr_LQDO_CTR = "" Then
      vlr_LQDO_CTR     = "0,00"
   Else
      vlr_LQDO_CTR     = FormatNumber(vlr_LQDO_CTR,2)
   End If
   
   If Trim(VL_IPTC_MOEN_CTC) = "" Then
	   VL_IPTC_MOEN_CTC = "0,00"
   End If
   If Trim(VL_PREM_MOEN_CTC) = "" Then
	   VL_PREM_MOEN_CTC = "0,00"
   End If

   If IN_FAT_UNCO_SGR = "Sim" Then
	  estilo_subgrupo_edicao_a = "style=""text-align:right; background-color:#EEEEEE;"" readonly"
	  estilo_subgrupo_edicao_b = "style=""text-align:right"""
   Else
	  estilo_subgrupo_edicao_a = "style=""text-align:right"""
	  estilo_subgrupo_edicao_b = "style=""text-align:right; background-color:#EEEEEE;"" readonly"
   End If

  'Verifica se o produto aceita averba��o
   objCon.pTP_PESQ = "BB"
   vAceitaAverbacao = UCase(objCon.mLerIndAceitaAverbacao())
%>
<script language="JavaScript">
<!--
function FormatNumber(vr,tamdec,truncate){
	//A fun��o espera receber casas decimais com V�RGULA
	// truncate = 1 - n�o altera o valor de casas decimais, caso seja maior do que o tamanho decidido
	// truncate = 2 - remove os valores superiores as casa decimais
	// truncate = 3 - remove os valores superiores as casa decimais e arredonda caso haja necessidade
	vp = '';
	for(f=0;f<vr.length;f++){
		if(((vr.charCodeAt(f)>=48)&&(vr.charCodeAt(f)<=57))||(vr.charCodeAt(f)==44)){
			vp=vp+vr.charAt(f);
		}
	}
	a=1;
	vr = vp;
	if(vr.length>0){
		vr = vr.split(',');
		vrIni = vr[0];
		vrIniTemp = '';
		for(f=0;f<vrIni.length;f++){
			vrIniTemp = vrIni.charAt(vrIni.length-f-1) + vrIniTemp;
			if((a==3)&&(vrIni.length!=f+1)){
				vrIniTemp = '.' + vrIniTemp;
				a=0;
			}
			a++;
		}
		vrIni = vrIniTemp;
		vrFim = "";
		if(vr.length>1){
			vrFim = vr[1];
			if(vrFim.length>tamdec){
				if(truncate==3){
					valorFinal = vrFim.substring(tamdec,tamdec + 1);
					vrFim = vrFim.substring(0,tamdec);
					if(valorFinal>4){
						vrFim = vrFim/1 + 1;
					}
				} else if(truncate==2){
					vrFim = vrFim.substring(0,tamdec);
				}
			} else {
				for(f=vrFim.length;f<tamdec;f++){
					vrFim = vrFim + '0';
				}
			}
		} else {
			for(f=0;f<tamdec;f++){
				vrFim = vrFim + '0';
			}
		}
		vr = vrIni + ',' + vrFim;
		return vr;
	}
}

function AtribuirPagtoParcelaHidden()
{
	document.formProc.QT_PCL_PGTO_CTC.value = document.formProc.rQT_PCL_PGTO_CTC.value;
}

function VerificarParcelamento(qtde_dias)
{
	AtribuirPagtoParcelaHidden();
	
	var qtde_meses = parseFloat((qtde_dias / 30));
	
	if (qtde_meses.toString().indexOf('.') >= 0)
		qtde_meses = parseInt(qtde_meses.toString().substring(0,qtde_meses.toString().indexOf('.')));
	
	if (document.formProc.QT_PCL_PGTO_CTC.value > qtde_meses)
		alert('A quantidade de parcelas � maior que os meses de vig�ncia do seguro');
}

function preencheCampos(iElemento,pDA)
{
	habilitaCampos(true);

	document.formProc.elemento.value = iElemento;
	var per_fat_seguro = document.getElementById('per_fat_seguro' + iElemento).innerHTML;
	document.formProc.VL_IOF_CTC.value = document.getElementById('valor_iof' + iElemento).innerHTML;
	document.formProc.VL_SGRA_MOEN_CTC.value = document.getElementById('valor_premio_net' + iElemento).innerHTML;
	document.formProc.PC_MED_SGR.value = document.getElementById('taxa_comercial' + iElemento).innerHTML;

	document.formProc.VL_IPTC_MOEN_CTC.value = document.getElementById('VL_IPTC_MOEN_CTC' + iElemento).innerHTML;
	if(pDA!='Sim') document.formProc.PC_DA_CTC.value = document.getElementById('PC_DA_CTC' + iElemento).innerHTML;
	document.formProc.PC_ETLE_CTC.value = document.getElementById('PC_ETLE_CTC' + iElemento).innerHTML;
	document.formProc.VL_PREM_PURO.value = document.getElementById('VL_PREM_PURO' + iElemento).innerHTML;
	document.formProc.VL_LQDO_MOEN_CTC.value = document.getElementById('VL_LQDO_MOEN_CTC' + iElemento).innerHTML;
	document.formProc.valor_pro_labore.value = document.getElementById('valor_pro_labore' + iElemento).innerHTML;
    document.formProc.VL_MVTC_FNCR_PREM.value = document.getElementById('VL_MVTC_FNCR_PREM' + iElemento).innerHTML;
    document.formProc.valor_premio_pago_total.value = document.getElementById('VL_LQDO_ANTERIOR' + iElemento).innerHTML;

	var mov_endosso = document.getElementById('CD_MVTC_FNCR_PREM' + iElemento).innerHTML;
	mov_endosso = parseInt(mov_endosso) - 1;
	document.formProc.CD_MVTC_FNCR_PREM.selectedIndex  = mov_endosso;

	eval('document.formProc.NR_CTC_SGRO_subgrupo.value = document.formProc.NR_CTC_SGRO_subgrupo' + iElemento + '.value');
	eval('document.formProc.NR_VRS_CTC_subgrupo.value = document.formProc.NR_VRS_CTC_subgrupo' + iElemento + '.value');

	calculaTotais('C');
	escreveFaturamento(per_fat_seguro,'<%=vAceitaAverbacao%>');
}

var valor_is_total = 0;
var valor_premio_puro_total = 0;
var valor_premio_liquido_total = 0;
var valor_premio_net_total = 0;

function calculaTotais(tipo)
{
	if (tipo == 'C') //carregar dados
	{
		document.formProc.valor_is_total.value = document.formProc.valor_is_total_or.value;
		document.formProc.valor_premio_puro_total.value = document.formProc.valor_premio_puro_total_or.value;
		document.formProc.valor_premio_liquido_total.value = document.formProc.valor_premio_liquido_total_or.value
		document.formProc.taxa_media_mensal.value = document.formProc.taxa_media_mensal_or.value

		valor_is_total = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_is_total.value)) -
						 parseFloat('0' + converteNumeroJavascript(document.formProc.VL_IPTC_MOEN_CTC.value));

		valor_premio_puro_total = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_premio_puro_total.value)) -
								  parseFloat('0' + converteNumeroJavascript(document.formProc.VL_PREM_PURO.value));

		valor_premio_liquido_total = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_premio_liquido_total.value)) -
									 parseFloat('0' + converteNumeroJavascript(document.formProc.VL_LQDO_MOEN_CTC.value));

		valor_premio_net_total = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_premio_net_total.value)) -
								 parseFloat('0' + converteNumeroJavascript(document.formProc.VL_SGRA_MOEN_CTC.value));

	} 
	else if (tipo == 'S') //salvar dados
	{
		var valor_is_total_temp = parseFloat(valor_is_total) +
								  parseFloat('0' + converteNumeroJavascript(document.formProc.VL_IPTC_MOEN_CTC.value));
		
		document.formProc.valor_is_total.value = FormatNumber(valor_is_total_temp.toString().replace(".", ","), 2, 2);

		var valor_premio_puro_total_temp = parseFloat(valor_premio_puro_total) +
										   parseFloat('0' + converteNumeroJavascript(document.formProc.VL_PREM_PURO.value));

		document.formProc.valor_premio_puro_total.value = FormatNumber(valor_premio_puro_total_temp.toString().replace(".", ","), 2, 2);

		var valor_premio_liquido_total_temp = parseFloat(valor_premio_liquido_total) +
											  parseFloat('0' + converteNumeroJavascript(document.formProc.VL_LQDO_MOEN_CTC.value));

		document.formProc.valor_premio_liquido_total.value = FormatNumber(valor_premio_liquido_total_temp.toString().replace(".", ","), 2, 2);

		var valor_premio_net_total_temp = parseFloat(valor_premio_net_total) +
										  parseFloat('0' + converteNumeroJavascript(document.formProc.VL_SGRA_MOEN_CTC.value));

		document.formProc.valor_premio_net_total.value = FormatNumber(valor_premio_net_total_temp.toString().replace(".", ","), 2, 2);

		if (valor_is_total_temp > 0)
			var taxa_media_mensal_temp = parseFloat((valor_premio_liquido_total_temp / valor_is_total_temp) * 1000);
		else
			var taxa_media_mensal_temp = 0;

		document.formProc.taxa_media_mensal.value = FormatNumber(taxa_media_mensal_temp.toString().replace(".", ","), 4, 3);
	}
}

var opcao = '#'; // se entrar com valor da diferen�a, esse campo n�o ser� mais re-calculado.
function calculaValores()
{
    var valor_premio_liquido = 0;
    var valor_premio_net  = 0;

	var per_fat_seguro = parseFloat('0' + converteNumeroJavascript(document.formProc.per_fat_seguro.value));  // p/subgrupo
	var percentual_corretagem = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_CRE_CTC.value));
	var percentual_iof = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_IOF_CTC.value));
	var percentual_da = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_DA_CTC.value));      // p/subgrupo
	var percentual_pro_labore = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_ETLE_CTC.value)); // p/subgrupo
	var valor_is = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_IPTC_MOEN_CTC.value));
    var valor_anterior = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_premio_pago_total.value));
    var valor_premio_puro = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_PREM_PURO.value));
    var valor_difer = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_MVTC_FNCR_PREM.value.replace('-','')));
    if (document.formProc.VL_MVTC_FNCR_PREM.value.indexOf('-')) valor_difer = valor_difer * (-1);

    if (valor_premio_puro > 0 && percentual_da > 0)
        valor_premio_net = valor_premio_puro / (1 - (percentual_da/100));

	if (valor_premio_net > 0 && (percentual_corretagem > 0 || percentual_pro_labore > 0))
		valor_premio_liquido = valor_premio_net / (1 - ((percentual_pro_labore/100) + (percentual_corretagem/100)));
    else valor_premio_liquido = valor_premio_net;

    if (valor_difer==0||opcao=='#')  valor_difer = valor_premio_liquido - valor_anterior;

	if (percentual_pro_labore > 0)
		var valor_pro_labore = valor_premio_net * (( 1 / ( 1 - (percentual_pro_labore / 100))) - 1);
	else
		var valor_pro_labore = 0;
	
	if ((valor_is > 0) && (valor_premio_liquido > 0) && (per_fat_seguro > 0))
		var taxa_comercial = ((valor_premio_liquido * per_fat_seguro) / valor_is) * 1000;
	else
		var taxa_comercial = 0;

    // se valor premio for menor que minimo, prevalece o maior
    if (valor_premio_liquido>0){
        var val_prem_min = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_premio_minimo.value));
        if((valor_premio_liquido<val_prem_min)&&((valor_premio_liquido+val_prem_min)>0)) valor_premio_liquido = val_prem_min;  
    }

	if ((valor_premio_net > 0) && (percentual_iof > 0))
		var valor_iof = valor_premio_net * percentual_iof / 100;
	else
		var valor_iof = 0;
	
	if(valor_difer < 0)
		var mov_endosso = 2;
	else if(valor_difer > 0)
		var mov_endosso = 1;
	else
		var mov_endosso = 0;

    // calculo do numero de parcelas
    var val_min        = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_prem_min_prpt.value));
    var par_por_vig    = parseInt(converteNumeroJavascript(document.formProc.qtd_parc_por_vigencia.value));
    val_min            = valor_premio_liquido / val_min;
    val_min            = parseInt(val_min);
    if(val_min<par_por_vig) par_por_vig=val_min;
    if(par_por_vig <= 0)    par_por_vig=1;
    document.formProc.QT_PCL_PGTO_CTC.value = par_por_vig.toString();

    var adic_frac = parseFloat('0' + converteNumeroJavascript(document.formProc.adic_fracionamento.value));

    document.formProc.VL_LQDO_MOEN_CTC.value           = FormatNumber(valor_premio_liquido.toString().replace(".", ","), 2, 2);
    document.formProc.valor_premio_liquido_total.value = FormatNumber(valor_premio_liquido.toString().replace(".", ","), 2, 2);
	document.formProc.PC_MED_SGR.value                 = FormatNumber(taxa_comercial.toString().replace(".", ","), 4, 2);
	document.formProc.valor_pro_labore.value           = FormatNumber(valor_pro_labore.toString().replace(".", ","), 4, 2);
	document.formProc.VL_SGRA_MOEN_CTC.value           = FormatNumber(valor_premio_net.toString().replace(".", ","), 4, 2);
	document.formProc.VL_IOF_CTC.value                 = FormatNumber(valor_iof.toString().replace(".", ","), 2, 2);
    document.formProc.VL_MVTC_FNCR_PREM.value          = FormatNumber(valor_difer.toString().replace(".", ","), 2, 2);
	document.formProc.CD_MVTC_FNCR_PREM.selectedIndex  = mov_endosso;

	processaMovimentacao('');
	calculaTotais('S');
}

function processaMovimentacao(op)
{
    if (op!='') opcao = op;
	var iCodMov = parseInt('0' + document.formProc.CD_MVTC_FNCR_PREM.value);
    var valor_difer = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_MVTC_FNCR_PREM.value));
 
	if (iCodMov==1) document.formProc.VL_MVTC_FNCR_PREM.value = '0,00';

   	if (iCodMov==2) document.formProc.VL_MVTC_FNCR_PREM.value = FormatNumber(valor_difer.toString().replace(".",","), 2, 2);

   	if (iCodMov==3&&document.formProc.VL_MVTC_FNCR_PREM.value!='0,00') document.formProc.VL_MVTC_FNCR_PREM.value = '-' + document.formProc.VL_MVTC_FNCR_PREM.value;
   	//if (op!='') calculaValores();
}

function ValidaPercentual(valor,tipo)
{
    valor=valor.replace('.','');
    valor=valor.replace(',','.');
	if(parseFloat(valor)>100){
		alert(tipo+' n�o pode ser superior a 100,00');
		return false;
	}
	return true;
}

function escreveFaturamento(faturamento,averbacao)
{
	var faturamento_label = 'total do per�odo';
	if (averbacao=='S'){
	    switch (parseInt(faturamento))
	    {
		case 1: faturamento_label = 'mensal'; document.formProc.per_fat_seguro.selectedIndex = 1; break;
		case 3: faturamento_label = 'trimestral'; document.formProc.per_fat_seguro.selectedIndex = 2; break;
		case 6: faturamento_label = 'semestral'; document.formProc.per_fat_seguro.selectedIndex = 3; break;
		case 12: faturamento_label = 'anual'; document.formProc.per_fat_seguro.selectedIndex = 4; break;
	    }        
	}
	else document.formProc.periodicidade_faturamento.value = faturamento_label;

	document.all.faturamento.innerHTML = faturamento_label;
}

function converteNumeroJavascript(valor)
{
	valor = valor.split(',');
	valorNovo = '';
	for(f=0;f<valor[0].length;f++){
		if((valor[0].charCodeAt(f)>=48)&&(valor[0].charCodeAt(f)<=57)){
			valorNovo=valorNovo+valor[0].charAt(f);
		}
	}
	if(valor.length>1){
		valorNovo = valorNovo + '.' + valor[1];
	}
	return valorNovo;
}

function habilitaCampos(bHabilita)
{
	if(document.formProc.alterar.disabled==!bHabilita) return;

	document.formProc.alterar.disabled = !bHabilita;
	document.formProc.calcular.disabled = !bHabilita;
	document.formProc.PC_DA_CTC.disabled = !bHabilita;
	document.formProc.PC_ETLE_CTC.disabled = !bHabilita;
	document.formProc.VL_PREM_PURO.disabled = !bHabilita;
	document.formProc.VL_IPTC_MOEN_CTC.disabled = !bHabilita;
	document.formProc.PC_MED_SGR.disabled = !bHabilita;
	document.formProc.valor_pro_labore.disabled = !bHabilita;
	document.formProc.per_fat_seguro.disabled = !bHabilita;
}

function submitdadosfinanceiros()
{
	document.formProc.action = 'liberar_cotacao_fin.asp';
	document.formProc.submit();
}

function alterarDados()
{
	if (validaform())
	{
		document.formProc.salvarfin.value = 'S';
		submitdadosfinanceiros();
	}
}

function validaform()
{
	if (parseFloat('0' + converteNumeroJavascript(document.formProc.PC_DA_CTC.value)) <= 0)
	{
		alert('O Percentual DA deve ser maior que 0.');
		return false;
	}
	return true;
}
//-->
</script>
	<input type="hidden" name="salvarfin" value="">
<%
strMensagem = ""

Set rsSubgrupo = objCon.mLerSubgrupoCotacao
rsSubGrupo.ActiveConnection = Nothing
If rsSubgrupo.EOF Then
	bSubgrupo = False
Else
	bSubgrupo = True
	Set rsSubGpCTR = objCon.mLerSubgrupoCotacao(True)
	rsSubGpCTR.ActiveConnection = Nothing
End If

If PC_ETLE_CTC = "" Then
   PC_ETLE_CTC = "0"
End If
If PC_DA_CTC = "" Then
   PC_DA_CTC = "0"
End If
If VL_MVTC_FNCR_PREM = "" Then
   VL_MVTC_FNCR_PREM = "0"
End If

If Trim(Request("salvarfin")) = "S" and ((Trim(Request("NR_CTC_SGRO_subgrupo")) <> "" and trim(request("NR_VRS_CTC_subgrupo")) <> "") or not bSubgrupo) then
	Set rsCotacao = objCon.mLerCotacao
	rsCotacao.ActiveConnection = Nothing

	If UCase(rsCotacao("CD_ULT_EVT_CTC")) = "T" Then
        bAtualizaCotacao = False
    Else
		objcon.pCD_ULT_EVT_CTC = "T"
		bAtualizaCotacao = True
	End If

	chave
    If vAceitaAverbacao = "S" Then
       objCon.pPER_FAT_SEGURO = Request("per_fat_seguro")
    Else
       objCon.pPER_FAT_SEGURO = 1
    End If	
	objCon.pNR_CTC_SGRO       = num_cotacao
    objCon.pCD_MVTC_FNCR_PREM = CD_MVTC_FNCR_PREM
    objCon.pVL_MVTC_FNCR_PREM = Abs(VL_MVTC_FNCR_PREM)
	objCon.pVL_IOF_CTC        = Abs(VL_IOF_CTC)
	objCon.pQT_PCL_PGTO_CTC   = QT_PCL_PGTO_CTC
	objCon.pPC_ETLE_CTC       = PC_ETLE_CTC
	objCon.pPC_DA_CTC         = PC_DA_CTC
	objCon.pVL_PREM_PURO      = Abs(VL_PREM_PURO)
	objCon.pVL_SGRA_MOEN_CTC  = Abs(VL_SGRA_MOEN_CTC)
	objCon.pVL_IPTC_MOEN_CTC  = Abs(VL_IPTC_MOEN_CTC)
	objCon.pVL_LQDO_MOEN_CTC  = Abs(VL_LQDO_MOEN_CTC)
	objCon.pPC_MED_SGR        = Abs(PC_MED_SGR)

	If bSubgrupo Then
		NR_CTC_SGRO_subgrupo  = Request("NR_CTC_SGRO_subgrupo")
		NR_VRS_CTC_subgrupo   = Request("NR_VRS_CTC_subgrupo")
	Else
		NR_CTC_SGRO_subgrupo  = ""
		NR_VRS_CTC_subgrupo   = ""
	End If

	objCon.pWF_ID = WF_ID
	retorno = objCon.mAtualizarDadosFinanceiros(NR_CTC_SGRO_subgrupo, _
												NR_VRS_CTC_subgrupo, _
												trim(Abs(request("valor_is_total"))), _
												trim(Abs(request("valor_premio_puro_total"))), _
												trim(Abs(request("valor_premio_liquido_total"))), _
												trim(Abs(request("valor_premio_net_total"))), _
												trim(Abs(request("taxa_media_mensal"))), _
												bAtualizaCotacao)
	If Trim(retorno) <> "" Then
	    Response.Write objCon.perro
		'Response.redirect("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
		Response.End 
	End If

	chave
	If not bSubgrupo Then
		Set rs = objcon.mLerDadosFinanceiros(2) '2 = VIDA
		rs.ActiveConnection = Nothing
	End If
End If
If testaDadoVazioN(rs,"PC_CRE_CTC") <> "" Then
    PC_CRE_CTC = testaDadoVazioN(rs,"PC_CRE_CTC")
End If
%><br>
	<table cellpadding="2" cellspacing="1" border="0" width="850" class="escolha" height="545">
		<tr>
			<td width="219" nowrap class="td_label" height="23">&nbsp;Moeda do seguro da cota��o</td>
			<td width="147" class="td_dado" height="23">&nbsp;
				<%=testaDadoVazio(rs,"CD_MOE_SGRO_CTC") & " - " & testaDadoVazio(rs,"NOME_MOE_SGRO")%>
			</td>
			<td nowrap class="td_label" width="245" height="23">&nbsp;Taxa Personalizada por subgrupo</td>
			<td class="td_dado" width="177" height="23">&nbsp;
				<input type="text" name="IN_FAT_UNCO_SGR" size="3" value="<%=IN_FAT_UNCO_SGR%>" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="219" height="23">&nbsp;Moeda de origem da cota��o</td>
			<td class="td_dado" width="147" height="23">&nbsp;
				<%=testaDadoVazio(rs,"CD_MOE_OGM_CTC") & " - " & testaDadoVazio(rs,"NOME_MOE_OGM")%>
			</td>
		   <%If IN_FAT_UNCO_SGR = "Sim" Then%>
   		    <td colspan="2" width="36">&nbsp;</td>
		   <%Else%>
			<td nowrap class="td_label" width="245" height="23">&nbsp;Percentual DA</td>
			<td class="td_dado" width="177" height="23">&nbsp;&nbsp;<input type="text" name="PC_DA_CTC" size="16" value="<%=FormatNumber(PC_DA_CTC,2)%>" onKeyUp="FormatValorContinuo(this,2);" onBlur="if(ValidaPercentual(this.value,'Percentual DA')==false) this.value='0.00';calculaValores();" maxlength="16" style="text-align:right;"></td>
           <%End If%>
		</tr>
		<tr>
			<td nowrap class="td_label" width="219" height="23">&nbsp;Percentual de corretagem</td>
			<td class="td_dado" width="147" height="23">&nbsp;
				<input type="text" name="PC_CRE_CTC" size="16" value="<%=PC_CRE_CTC%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td><td colspan="2" width="427"></td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="219" height="23">&nbsp;Percentual IOF</td>
			<td class="td_dado" width="147" height="23">&nbsp;
				<input type="text" name="PC_IOF_CTC" size="16" value="<%=testaDadoVazioN(rs,"PC_IOF_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
			<td colspan="2" height="23" width="427"></td>
		</tr>
<%
		If NOT bSubgrupo Then
			percentual_iof = cdbl("0" & testaDadoVazioN(rs,"PC_IOF_CTC"))
			percentual_da = cdbl("0" & testaDadoVazioN(rs,"PC_DA_CTC"))
			If testaDadoVazioN(rs,"PC_ETLE_CTC") <> "" Then
		 	    percentual_pro_labore = cdbl("0" & testaDadoVazioN(rs,"PC_ETLE_CTC"))
			End If
			valor_premio_puro = cdbl("0" & testaDadoVazioN(rs,"VL_PREM_PURO"))
			valor_is = cdbl("0" & testaDadoVazioN(rs,"VL_IPTC_MOEN_CTC"))
			valor_premio_liquido = cdbl("0" & testaDadoVazioN(rs,"VL_LQDO_MOEN_CTC"))
			valor_premio_net = cdbl("0" & testaDadoVazioN(rs,"VL_SGRA_MOEN_CTC"))
			per_fat_seguro =  cdbl("0" & testaDadoVazioN(rs,"PER_FAT_SEGURO"))
            movimento = cdbl("0" & testaDadoVazioN(rs,"CD_MVTC_FNCR_PREM"))

			If percentual_pro_labore > 0 and valor_premio_liquido > 0 Then
	            'valor_pro_labore = valor_premio_liquido * percentual_pro_labore/100
				'valor_pro_labore = CortaCasasDecimais(valor_pro_labore,5)
			    valor_pro_labore = CortaCasasDecimais(valor_premio_net * (( 1 / ( 1 - (percentual_pro_labore / 100))) - 1),5)
			Else
				valor_pro_labore = 0
			End If

            If movimento = 3 Then
                valor_premio_liquido = valor_premio_liquido * (-1)
                valor_premio_net = valor_premio_net * (-1)
            End If

			valor_iof = CortaCasasDecimais(((valor_premio_puro * percentual_iof) / 100), 2)

			If valor_premio_liquido > 0 and valor_is > 0 and per_fat_seguro > 0 Then
				taxa_comercial = CortaCasasDecimais(((valor_premio_liquido * per_fat_seguro) / valor_is) * 1000, 4)
			Else
				taxa_comercial = 0
			End If

			valor_premio_puro_total = valor_premio_puro
			valor_premio_liquido_total = valor_premio_liquido
			valor_premio_net_total = valor_premio_net
			valor_is_total = valor_is
			
			If valor_is_total > 0 Then
				taxa_media_mensal = CortaCasasDecimais(((valor_premio_liquido_total / valor_is_total) * 1000), 4)
			Else
				taxa_media_mensal = 0
			End If
		End If
%>
		<tr>
			<td nowrap class="td_label" width="219" height="23">&nbsp;Valor IOF</td>
			<td class="td_dado" width="147" height="23">&nbsp;
				<input type="text" name="VL_IOF_CTC" size="16" value="<%=testaDadoVazioN(rs,"VL_IOF_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
			<td colspan="2" height="23" width="427">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4" width="803" height="19">&nbsp;</td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="219" height="23">&nbsp;Percentual pro-labore</td>
			<td class="td_dado" width="147" height="23">&nbsp;
				<input type="text" name="PC_ETLE_CTC" size="16" value="<%=FormatNumber(percentual_pro_labore,2)%>" onKeyUp="FormatValorContinuo(this,2);" onBlur="if(ValidaPercentual(this.value,'Percentual Pro Labore')==false) this.value='0.00';calculaValores();" maxlength="16" style="text-align:right;">
			</td>
			<td nowrap class="td_label" width="245" height="23">&nbsp;Tipo de Capital Segurado</td>
			<td class="td_dado" width="177" height="23">&nbsp;
				<input type="text" name="QSTNTipoCapitalSegurado" size="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="219" height="23">&nbsp;Valor pro-labore</td>
			<td class="td_dado" width="147" height="23">&nbsp;
				<input type="text" name="valor_pro_labore" size="16" value="<%=FormatNumber(valor_pro_labore,4)%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
			<td nowrap class="td_label" width="245" height="23">&nbsp;Valor IS</td>
			<td class="td_dado" width="177" height="23">&nbsp;
				<input type="text" name="VL_IPTC_MOEN_CTC" size="16" value="<%=FormatNumber(valor_is,2)%>" onKeyUp="FormatValorContinuo(this,2);" onBlur="calculaValores();" maxlength="16" style="text-align:right;">
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="219" height="23">&nbsp;Valor pr�mio puro</td>
			<td class="td_dado" width="147" height="23">&nbsp;
				<input type="text" name="VL_PREM_PURO" size="16" value="<%=FormatNumber(valor_premio_puro,2)%>" onKeyUp="FormatValorContinuo(this,2);" onBlur="calculaValores();" maxlength="16" style="text-align:right;">
			</td>
			<td nowrap class="td_label" width="245" height="23">&nbsp;Taxa comercial
				<span id="faturamento">
                <%
				If NOT bSubgrupo Then
					Response.Write RetornaPeriodicidade(per_fat_seguro,vAceitaAverbacao)
				End If
				%>
				</span>
			</td>
			<td class="td_dado" width="177" height="23">&nbsp;
				<input type="text" name="PC_MED_SGR" size="16" value="<%=FormatNumber(taxa_comercial, 4)%>" onBlur="if(ValidaPercentual(this.value,'Taxa Comercial')==false){calculaValores();}" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="VL_LQDO_MOEN_CTC" value="<%=FormatNumber(valor_premio_liquido, 2)%>">
				<input type="hidden" name="VL_SGRA_MOEN_CTC" value="<%=FormatNumber(valor_premio_net, 2)%>">
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="219" height="23">&nbsp;Periodicidade de Faturamento</td>
			<td class="td_dado" width="147" height="23">&nbsp;
			   <%If vAceitaAverbacao = "N" Then%><input type="text" name="periodicidade_faturamento" size="16" value="<%=RetornaPeriodicidade(per_fat_seguro, vAceitaAverbacao)%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="per_fat_seguro" value="<%=FormatNumber(per_fat_seguro, 0)%>">
			   <%Else%>	
				<select name="per_fat_seguro">
				    <option value="">--Escolha abaixo--</option>
					<option value="1" <%If per_fat_seguro = "1" Then Response.Write "selected"%>>Mensal</option>
					<option value="3" <%If per_fat_seguro = "3" Then Response.Write "selected"%>>Trimestral</option>
					<option value="6" <%If per_fat_seguro = "6" Then Response.Write "selected"%>>Semestral</option>
					<option value="12" <%If per_fat_seguro = "12" Then Response.Write "selected"%>>Anual</option>
				</select>
			   <%End If%>	
			</td>
			<td width="245" height="23">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
			<td width="177" height="23">&nbsp;
				<input type="button" name="alterar" value="Gravar" style="width:80px;font-size:10px;margin: 2px 2px 2px 2px;" onclick="alterarDados()"></td>
		</tr>
		<%If IN_FAT_UNCO_SGR = "Sim" Then%>
		<tr>
			<td nowrap class="td_label" width="219">&nbsp;Percentual DA</td>
			<td class="td_dado" width="147">&nbsp;
				<input type="text" name="PC_DA_CTC" size="16" value="<%=PC_DA_CTC%>" onKeyUp="FormatValorContinuo(this,2);" onBlur="if(ValidaPercentual(this.value,'Percentual DA')==false) this.value='0.00';calculaValores();" maxlength="16" style="text-align:right;">
			</td>
			<td width="245"></td>
			<td width="177"></td>
		</tr><input type="button" name="calcular" value="Calcular" style="width:80px;font-size:10px;margin: 2px 2px 2px 2px;" onclick="calculaValores()">
		<%End If%>
		<tr>
			<td colspan="4" height="5" width="803"></td>
		</tr>
		<%If bSubgrupo Then%>
		<tr>
			<td colspan="4" width="800" height="87">
				<div style="position:relative; overflow:scroll; width:100%; height:110px;">
					<input type="hidden" name="subgrupo" value="">
					<table cellpadding="1" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
						<tr>
							<td class="td_label" width="2%">&nbsp;</td>
							<td class="td_label" width="8%"><div align="center"><b>Subgrupo</b></div></td>
							<td class="td_label" width="16%"><div align="center"><b>Valor IS</b></div></td>
							<td class="td_label" width="16%"><div align="center"><b>Pr�mio Puro</b></div></td>
							<td class="td_label" width="16%"><div align="center"><b>Pr�mio L�quido</b></div></td>
							<td class="td_label" width="14%"><div align="center"><b>DA (%)</b></div></td>
							<td class="td_label" width="14%"><div align="center"><b>Periodicidade</b></div></td>
							<td class="td_label" width="14%"><div align="center"><b>Pro-labore (%)</b></div></td>
						</tr>
		   <%If NOT rs.EOF Then
		 	    iContador = 0

			    percentual_iof = cdbl("0" & testaDadoVazioN(rs,"PC_IOF_CTC"))

			    valor_premio_puro_total = 0
			    valor_premio_liquido_total = 0
			    valor_is_total = 0
			    valor_premio_net_total = 0
			    taxa_media_mensal = 0

			    Do while not rsSubgrupo.EOF
			   	   alterna_cor
				   iContador = iContador + 1

				   objCon.pNR_CTC_SGRO = rsSubgrupo("NR_CTC_SGRO")
				   objCon.pNR_VRS_CTC  = rsSubgrupo("NR_VRS_CTC")

				   Set rsDadosFin = objCon.mLerDadosFinanceiros(2)
				   rsDadosFin.ActiveConnection = Nothing

                   If NOT rsSubGpCTR.EOF Then
                      objCon.pNR_CTR_SGRO = rsSubGpCTR("NR_CTC_SGRO")
                      objCon.pNR_VRS_EDS  = 1

                      Set rsCTR = objcon.mLerDadosFinanceirosContrato(2) 
                      rsCTR.ActiveConnection = Nothing
                      If NOT rsCTR.EOF Then
                         If testaDadoVazioN(rsDadosFin,"PC_ETLE_CTC") = "" Then
                             percentual_pro_labore = testaDadoVazioN(rsCTR,"PC_ETLE_CTC")
	 	 	             End If
                         vlr_LQDO_CTR = testaDadoVazioN(rsCTR,"VL_LQDO_MOEN_CTC")
                      End If
                      Set rsCTR = Nothing
                      rsSubGpCTR.MoveNext
                   End If

				   percentual_da = cdbl("0" & testaDadoVazioN(rsDadosFin,"PC_DA_CTC"))

  			       If testaDadoVazioN(rsDadosFin,"PC_ETLE_CTC") <> "" Then
                      percentual_pro_labore = cdbl("0" & testaDadoVazioN(rsDadosFin,"PC_ETLE_CTC"))
	 	           End If			   

				   valor_premio_puro = cdbl("0" & testaDadoVazioN(rsDadosFin,"VL_PREM_PURO"))
			 	   valor_premio_net = cdbl("0" & testaDadoVazioN(rsDadosFin,"VL_SGRA_MOEN_CTC"))
				   valor_is  = cdbl("0" & testaDadoVazioN(rsDadosFin,"VL_IPTC_MOEN_CTC"))
				   valor_premio_liquido = cdbl("0" & testaDadoVazioN(rsDadosFin,"VL_LQDO_MOEN_CTC"))
				   per_fat_seguro = cdbl("0" & testaDadoVazioN(rsDadosFin,"PER_FAT_SEGURO"))
                   valor_diferenca = rsDadosFin("VL_MVTC_FNCR_PREM")

                   movimento = cdbl("0" & testaDadoVazioN(rsDadosFin,"CD_MVTC_FNCR_PREM"))
                   If movimento = 3 Then
                      valor_premio_net = valor_premio_net * (-1)
                      valor_premio_liquido = valor_premio_liquido * (-1)
                   End If
				   valor_iof = CortaCasasDecimais(((valor_premio_puro * percentual_iof) / 100), 2)

				   If percentual_pro_labore > 0 and valor_premio_liquido <> 0 then
                    ' valor_pro_labore = (valor_premio_net / (1 - percentual_pro_labore/100)) * percentual_pro_labore/100
				    ' valor_pro_labore = CortaCasasDecimais(valor_pro_labore,5)
        			  valor_pro_labore = CortaCasasDecimais(valor_premio_net * (( 1 / ( 1 - (percentual_pro_labore / 100))) - 1),5)

				      If valor_pro_labore < 0 Then
				         valor_pro_labore = valor_pro_labore *(-1)
				      End If
				   Else
				      valor_pro_labore = 0
				   End If

				   If valor_premio_liquido > 0 and valor_is > 0 and per_fat_seguro > 0 Then
				      taxa_comercial = CortaCasasDecimais(((valor_premio_liquido * per_fat_seguro) / valor_is) * 1000, 4)
				   Else
				      taxa_comercial = 0
				   End If

				   If taxa_comercial > 0 Then
				      If per_fat_seguro <> 1 Then
					     taxa_media_mensal = CortaCasasDecimais(cdbl(taxa_media_mensal) + ((valor_premio_liquido / valor_is) * 1000), 4)
				      Else
					     taxa_media_mensal = CortaCasasDecimais(cdbl(taxa_media_mensal) + cdbl(taxa_comercial), 4)
				      End If
				   End If

				   valor_premio_puro_total = valor_premio_puro_total + valor_premio_puro
				   valor_premio_liquido_total = valor_premio_liquido_total + valor_premio_liquido
				   valor_premio_net_total = valor_premio_net_total + valor_premio_net
				   valor_is_total = valor_is_total + valor_is %>

						<tr style="<%=cor_zebra%>">
							<td class="td_dado"><div align="center"><input type="radio" name="elemento" value="" onClick="javascript:preencheCampos(<%=iContador%>,'<%=IN_FAT_UNCO_SGR%>')"></div></td>
							<td class="td_dado"><div align="center" id="SUBGRUPO_ID<%=iContador%>"><%=FormatNumber(rsSubgrupo("SUBGRUPO_ID"), 0)%></div></td>
							<td class="td_dado"><div align="right" id="VL_IPTC_MOEN_CTC<%=iContador%>"><%=FormatNumber(valor_is, 2)%></div></td>
							<td class="td_dado"><div align="center"><%=FormatNumber(CortaCasasDecimais((valor_premio_puro * per_fat_seguro), 2), 2)%></div></td>
							<td class="td_dado"><div align="right"><%=FormatNumber(CortaCasasDecimais((valor_premio_liquido * per_fat_seguro), 2), 2)%></div></td>
							<td class="td_dado"><div align="right" id="PC_DA_CTC<%=iContador%>"><%=FormatNumber(percentual_da, 2)%></div></td>
							<td class="td_dado"><div align="center"><%=RetornaPeriodicidade(per_fat_seguro,"S")%></div></td>
							<td class="td_dado"><div align="right" id="PC_ETLE_CTC<%=iContador%>"><%=FormatNumber(percentual_pro_labore, 2)%></div>
							
							<div style="display:none" id="VL_LQDO_MOEN_CTC<%=iContador%>"><%=FormatNumber(valor_premio_liquido, 2)%></div>
							<div style="display:none" id="VL_PREM_PURO<%=iContador%>"><%=FormatNumber(valor_premio_puro, 2)%></div>
							<div style="display:none" id="valor_iof<%=iContador%>"><%=FormatNumber(valor_iof, 2)%></div>
							<div style="display:none" id="taxa_comercial<%=iContador%>"><%=FormatNumber(taxa_comercial, 4)%></div>
							<div style="display:none" id="valor_pro_labore<%=iContador%>"><%=FormatNumber(valor_pro_labore, 4)%></div>
							<div style="display:none" id="per_fat_seguro<%=iContador%>"><%=FormatNumber(per_fat_seguro, 0)%></div>
							<div style="display:none" id="valor_premio_net<%=iContador%>"><%=FormatNumber(valor_premio_net, 2)%></div>
							<div style="display:none" id="CD_MVTC_FNCR_PREM<%=iContador%>"><%=movimento%></div>
							<div style="display:none" id="VL_MVTC_FNCR_PREM<%=iContador%>"><%=valor_diferenca%></div>
                            <div style="display:none" id="VL_LQDO_ANTERIOR<%=iContador%>"><%=vlr_LQDO_CTR%></div>  
							<input type="hidden" name="NR_CTC_SGRO_subgrupo<%=iContador%>" value="<%=FormatNumber(rsSubgrupo("NR_CTC_SGRO"), 0)%>">
							<input type="hidden" name="NR_VRS_CTC_subgrupo<%=iContador%>" value="<%=FormatNumber(rsSubgrupo("NR_VRS_CTC"), 0)%>">
							</td>
						</tr>
						<%
								rsSubgrupo.MoveNext
							Loop

							If valor_is_total > 0 Then
								taxa_media_mensal = CortaCasasDecimais(((valor_premio_liquido_total / valor_is_total) * 1000), 4)
							Else
								taxa_media_mensal = 0
							End If
						End If
						%>
					</table>
					<input type="hidden" name="NR_CTC_SGRO_subgrupo" value="">
					<input type="hidden" name="NR_VRS_CTC_subgrupo" value="">
				</div>
			</td>
		</tr>
		<%End If%>
		<p>
		<tr>
			<td colspan="4" height="5" width="800">
	        <div style="position:relative; overflow:scroll; width:100%; height:110px;">
    <!--#include virtual = "/internet/serv/siscot3/dados_agd.asp"-->
           </div></td></tr>
		<p>
		<tr>
			<td nowrap class="td_label" width="219" height="23">&nbsp;Valor total de IS</td>
			<td class="td_dado" width="147" height="23">&nbsp;
				<input type="text" name="valor_is_total" size="16" value="<%=FormatNumber(valor_is_total, 2)%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="valor_is_total_or" value="<%=FormatNumber(valor_is_total, 2)%>">
			</td>
			<td nowrap class="td_label" width="245" height="23">&nbsp;Total de dias de vig�ncia do endosso</td>
			<td class="td_dado" width="177" height="23">&nbsp;
				<input type="text" name="qtd_dias_endosso" size="2" value="<%=qtd_dias_endosso%>" maxlength="2" style="text-align:right; background-color:#EEEEEE;" readonly>
           </td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="219" height="23">&nbsp;Valor total do pr�mio puro</td>
			<td class="td_dado" width="147" height="23">&nbsp;
				<input type="text" name="valor_premio_puro_total" size="16" value="<%=FormatNumber(valor_premio_puro_total, 2)%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="valor_premio_puro_total_or" value="<%=FormatNumber(valor_premio_puro_total, 2)%>">
			</td>
			<td nowrap class="td_label" width="245" height="23">&nbsp;Pr�mio m�nimo para endosso</td>
			<td class="td_dado" width="177" height="23">&nbsp;
				<input type="text" name="valor_premio_minimo" size="16" value="<%=FormatNumber(valor_premio_minimo,2)%>" style="text-align:right; background-color:#EEEEEE;" readonly></td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="219" height="23">&nbsp;Valor total do pr�mio l�quido</td>
			<td class="td_dado" width="147" height="23">&nbsp;
				<input type="text" name="valor_premio_liquido_total" size="16" value="<%=FormatNumber(valor_premio_liquido_total, 2)%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="valor_premio_liquido_total_or" value="<%=FormatNumber(valor_premio_liquido_total, 2)%>">
			</td>
			<td nowrap class="td_label" width="245" height="23">&nbsp;Movimenta��o do endosso</td>
			<td class="td_dado" width="177" height="23"> 
				&nbsp; 
				<select name="CD_MVTC_FNCR_PREM" onchange="processaMovimentacao('@');">
					<option value="1"<%If Trim(CD_MVTC_FNCR_PREM) = "1" Then Response.Write " selected"%>>
                    Sem movimenta��o</option>
					<option value="2"<%If Trim(CD_MVTC_FNCR_PREM) = "2" Then Response.Write " selected"%>>
                    Com cobran�a</option>
					<option value="3"<%If Trim(CD_MVTC_FNCR_PREM) = "3" Then Response.Write " selected"%>>
                    Com devolu��o</option>
				</select></td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="219" height="23">&nbsp;Valor total do 
            pr�mio pago</td>
			<td class="td_dado" width="147" height="23">&nbsp;
				<input type="text" name="valor_premio_pago_total" size="16" value="<%=FormatNumber(valor_LQDO_Anterior,2)%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
			<td nowrap class="td_label" height="23" width="245">&nbsp;Valor da diferen�a</td>
        	<td class="td_dado" height="23" width="177">&nbsp;
				<input type="text" name="VL_MVTC_FNCR_PREM" size="16" maxlength="16" value="<%=FormatNumber(VL_MVTC_FNCR_PREM,2)%>" onKeyUp="FormatValorContinuo(this,2);" onBlur="processaMovimentacao('#');" style="text-align:right;">
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="219" height="23">&nbsp;Taxa m�dia mensal</td>
			<td class="td_dado" width="147" height="23">&nbsp;
				<input type="text" name="taxa_media_mensal" size="16" value="<%=FormatNumber(taxa_media_mensal, 4)%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="taxa_media_mensal_or" value="<%=FormatNumber(taxa_media_mensal, 4)%>">
				<input type="hidden" name="valor_premio_net_total" size="16" value="<%=FormatNumber(valor_premio_net_total, 2)%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="valor_premio_net_total_or" value="<%=FormatNumber(valor_premio_net_total, 2)%>">
			</td>
			<td nowrap class="td_label" width="245" height="23">&nbsp;Qtd m�xima de parcelas pgto</td>
			<td class="td_dado" width="177" height="23">&nbsp;
				<input type="text" name="QT_PCL_PGTO_CTC" size="2" value="<%=parcelas%>" maxlength="2" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="qtd_parc_por_vigencia" value="<%=parcelas%>">
				<input type="hidden" name="valor_prem_min_prpt" value="<%=valor_prem_min_prpt%>">
                <input type="hidden" name="adic_fracionamento" value="<%=adicFrac%>">
     		</td>
		</tr>
	</table>
	<%If bSubGrupo Then	
	     Response.Write "<script language=""JavaScript"">habilitaCampos(false);</script>"
	  End If%>