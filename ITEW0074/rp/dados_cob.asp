<!--#include virtual = "/padrao.asp"-->

<script>
	var taxaNet = "";
	function rValorFocus(){
		taxaNet = document.formProc.pc_prem_cbt_ctc.value;
	}
	function rValorBlur(){
		if(taxaNet!=document.formProc.pc_prem_cbt_ctc.value){
			recalculaPremioNet();
		}
	}
	function recalculaPremioNet(){
		vl_iptc_cbt_ctc = document.formProc.vl_iptc_cbt_ctc.value;
		pc_prem_cbt_ctc = document.formProc.pc_prem_cbt_ctc.value;
		if((vl_iptc_cbt_ctc!="")&&(pc_prem_cbt_ctc!="")){
			vl_iptc_cbt_ctc = converteNumeroJavascript(vl_iptc_cbt_ctc) * 10000;
			pc_prem_cbt_ctc = converteNumeroJavascript(pc_prem_cbt_ctc) * 10000;
			vl_prem_cbt_ctc = (vl_iptc_cbt_ctc * pc_prem_cbt_ctc)/10000000000;
			vl_prem_cbt_ctc = converteNumeroJavascriptTruncado(vl_prem_cbt_ctc, 2);
			vl_prem_cbt_ctc = FormatNumber(vl_prem_cbt_ctc.replace('.',','), 2, 3);
			pc_prem_cbt_ctc = document.formProc.pc_prem_cbt_ctc.value;
			pc_prem_cbt_ctc = FormatNumber(pc_prem_cbt_ctc.replace('.',','), 4);
			document.formProc.pc_prem_cbt_ctc.value = pc_prem_cbt_ctc;
			document.formProc.vl_prem_cbt_ctc.value = vl_prem_cbt_ctc;
		}
	}
	function converteNumeroJavascriptTruncado(valor,casas){
		valor = valor + '';
		valor = valor.split('.');
		valorNovo = '';
		for(f=0;f<valor[0].length;f++){
			if((valor[0].charCodeAt(f)>=48)&&(valor[0].charCodeAt(f)<=57)){
				valorNovo=valorNovo+valor[0].charAt(f);
			}
		}
		valorNovo = valorNovo + '.';
		
		for(f=0;f<casas;f++){
			if(valor.length>1){
				if(valor[1].length>f){
					valorNovo = valorNovo + valor[1].charAt(f);
				} else {
					valorNovo = valorNovo + '0';
				}
			} else {
				valorNovo = valorNovo + '0';
			}
		}
		return valorNovo;
	}
	function converteNumeroJavascript(valor){
		valor = valor.split(',');
		valorNovo = '';
		for(f=0;f<valor[0].length;f++){
			if((valor[0].charCodeAt(f)>=48)&&(valor[0].charCodeAt(f)<=57)){
				valorNovo=valorNovo+valor[0].charAt(f);
			}
		}
		if(valor.length>1){
			valorNovo = valorNovo + '.' + valor[1];
		}
		return valorNovo;
	}
	function preencheCampos(numero){
		document.formProc.numero.value = numero;
		document.formProc.CD_CBT_ALT.value = document.formProc["CD_CBT" + numero].value;
		document.formProc.DT_INC_VCL_ALT.value = document.formProc["DT_INC_VCL" + numero].value;
		document.formProc.nome.value = document.formProc["nome" + numero].value;
		document.formProc.vl_iptc_cbt_ctc.value = document.formProc["vl_iptc_cbt_ctc" + numero].value;
		aFranquia = franquias[numero].split('|');
		apagaOptions(document.formProc.tx_rgr_frqu);
		document.formProc.tx_rgr_frqu_edicao.value = '';
		
		colocaOption(document.formProc.tx_rgr_frqu,'--Escolha Abaixo--');
		for(f=0;f<aFranquia.length;f++){
			colocaOption(document.formProc.tx_rgr_frqu,aFranquia[f]);
		}
		selecionaOption(document.formProc.tx_rgr_frqu,document.formProc["tx_rgr_frqu" + numero].value);
				
		document.formProc.tx_rgr_frqu_edicao.value = document.formProc["tx_rgr_frqu" + numero].value;
			
		document.formProc.pc_prem_cbt_ctc.value = document.formProc["pc_prem_cbt_ctc" + numero].value;
		document.formProc.vl_prem_cbt_ctc.value = document.formProc["vl_prem_cbt_ctc" + numero].value;
		for(x=0;x<document.formProc.contador.value;x++) {
			document.getElementById('questionario' + x).style.display = 'none';
		}
		document.getElementById('questionario' + numero).style.display = '';
		
		document.formProc.numero_atual.value = numero;
		
		if (document.formProc["LIMITE_CAPITAL" + numero].value == "-1")
			document.all.aviso.innerHTML = 'O Valor IS � menor que o m�nimo cadastrado';
		else if (document.formProc["LIMITE_CAPITAL" + numero].value == "-1")
			document.all.aviso.innerHTML = 'O Valor IS � maior que o m�ximo cadastrado';
		else
			document.all.aviso.innerHTML = '';
	}
	function selecionaOption(obj,valor){
		for(f=0;f<obj.length;f++){
			if(obj[f].value == valor){
				obj[f].selected = true;
			}
		}
	}
	function colocaOption(obj,valor){
		option = new Option('value','text');
		option.value = valor;
		option.text = valor;
		obj[obj.length] = option;
	}
	function apagaOptions(obj){
		while(obj.length>0){
			obj[0] = null;
		}
	}
	
	function calculaNET(tipo)
	{
		var valor_is = parseFloat('0' + converteNumeroJavascript(document.formProc.vl_iptc_cbt_ctc.value));
	
		if (tipo == 'T') //calcula a taxa
		{
			var premio_net = parseFloat('0' + converteNumeroJavascript(document.formProc.vl_prem_cbt_ctc.value));
			var taxa_net = converteNumeroJavascriptTruncado(((premio_net / valor_is) * 100), 5);
			
			document.formProc.pc_prem_cbt_ctc.value = FormatNumber(taxa_net.toString().replace(".", ","), 4);
		}
		else if (tipo == 'P') //calcula o premio
		{
			var taxa_net = parseFloat('0' + converteNumeroJavascript(document.formProc.pc_prem_cbt_ctc.value));
			var premio_net = converteNumeroJavascriptTruncado(((taxa_net * valor_is) / 100), 2);
			
			document.formProc.vl_prem_cbt_ctc.value = FormatNumber(premio_net.toString().replace(".", ","), 2);
		}
	}
	
</script>
<input type="hidden" name="salvarcob" value="N">
<input type="hidden" name="numero_selecao" value="">
<input type="hidden" name="numero_atual" value="">
<br><br>
<%
strError = ""

iContador = 0
set rs = objcon.mLerSubgrupoCotacao

if trim(request("salvarcob")) = "S" and trim(request("NR_SEQL_CBT_CTC")) <> "" and trim(request("CD_CBT_ALT")) <> "" then
	set rsCotacao = objCon.mLerCotacao

	if ucase(rsCotacao("CD_ULT_EVT_CTC")) <> "T" then
		objcon.pCD_ULT_EVT_CTC = "T"
		bAtualizaCotacao = true
	else
		bAtualizaCotacao = false
	end if

	if trim(request("NM_PRPN_CTC")) <> "" then
		arrSubGrupoCotacao = split(trim(request("NM_PRPN_CTC")),"|")
		NR_CTC_SGRO_subgrupo = arrSubGrupoCotacao(0)
		NR_VRS_CTC_subgrupo = arrSubGrupoCotacao(1)
	else
		NR_CTC_SGRO_subgrupo = NR_CTC_SGRO
		NR_VRS_CTC_subgrupo = NR_VRS_CTC
	end if

	objCon.pCD_CBT = trim(request("CD_CBT_ALT"))
	objCon.pNR_SEQL_CBT_CTC = trim(request("NR_SEQL_CBT_CTC"))
	objCon.pDT_INC_VCL = trim(request("DT_INC_VCL_ALT"))
	objCon.ptx_rgr_frqu = trim(request("tx_rgr_frqu_edicao"))
	objCon.ppc_prem_cbt_ctc = trim(request("pc_prem_cbt_ctc"))
	objCon.pvl_prem_cbt_ctc = trim(request("vl_prem_cbt_ctc"))
	
	objCon.pWF_ID = WF_ID
	
	retorno = objCon.mAtualizarCobertura(NR_CTC_SGRO_subgrupo, NR_VRS_CTC_subgrupo, bAtualizaCotacao)

	if trim(retorno) <> "" then
		Response.redirect("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
		Response.End 
	end if

	chave
end if

%>
<script>
function processasubgrupo()
{
	try { document.formProc.NR_SEQL_CBT_CTC.selectedIndex = 0; } catch (ex) { }
	submitcobertura();
}

function alterarDados()
{
	if (validaform())
	{
		document.formProc.salvarcob.value = 'S';
		submitcobertura();
	}
}

function validaform()
{
	if (document.formProc.numero_selecao.value != '')
	{
		if (Trim(document.formProc.tx_rgr_frqu_edicao.value) == '')
		{
			document.all.msgerro.innerHTML = '� necess�rio escolher uma franquia<br><br>';
			return false;
		}
	}
	
	return true;
}

function submitcobertura()
{
	document.formProc.action = 'liberar_cotacao_cob.asp';
	document.formProc.submit();
}

function alterasequencial()
{
	document.formProc.numero_selecao.value = '';
	submitcobertura();
}

function selecionarcobertura(numero)
{
	if (!validaform())
	{
		try 
		{
			document.formProc.R[document.formProc.numero_selecao.value].checked = true; 
		}
		catch (ex) 
		{
			document.formProc.R.checked = true;
		}
		return;
	}

	if ((document.formProc.numero_selecao.value == '') || (document.formProc.numero_selecao.value == numero))
	{
		preencheCampos(numero);
		
		if (document.formProc.numero_selecao.value == '')
			document.formProc.numero_selecao.value = numero;

		try 
		{
			if (!document.formProc.R[numero].checked)
				document.formProc.R[numero].checked = true;
		}
		catch (ex) 
		{
			if (!document.formProc.R.checked)
				document.formProc.R.checked = true;
		}
	}
	else
	{
		document.formProc.R[document.formProc.numero_selecao.value].checked = true;
		document.formProc.numero_selecao.value = numero;
		alterarDados();
	}
}
</script>
<table cellspacing="0" border="0" width="100%" class="escolha" height="10">
	<tr>
		<td colspan="2" class="td_dado"><font color="red"><span id="msgerro"></span></font></td>
	</tr>
<%
if rs.eof then
	bSubgrupo = false
else
	bSubgrupo = true
%>
	<tr>
		<td class="td_label" width="150">
			Subgrupo
		</td>
		<td class="td_dado">&nbsp;
			<select name="NM_PRPN_CTC" onchange="processasubgrupo();">
			<option value="">--Escolha abaixo--</option>
			<%
			do while not rs.eof
				if trim(request("NM_PRPN_CTC")) = trim(rs("NR_CTC_SGRO")) & "|" & trim(rs("NR_VRS_CTC")) then
					sSelecionado = " selected"
				else
					sSelecionado = ""
				end if
				%>
				<option value="<%=trim(rs("NR_CTC_SGRO")) & "|" & trim(rs("NR_VRS_CTC"))%>"<%=sSelecionado%>><%=right(00 & rs("subgrupo_id"),2) & " (" & trim(rs("NR_CTC_SGRO")) & ")"%> - <%=trim(rs("NM_PRPN_CTC"))%></option>
				<%
				rs.movenext
			loop
			%>
			</select>
		</td>
	</tr>
<%
	if trim(request("NM_PRPN_CTC")) <> "" then 
		arrSubGrupoCotacao = split(trim(request("NM_PRPN_CTC")),"|")

		objCon.pNR_CTC_SGRO = arrSubGrupoCotacao(0)
		objCon.pNR_VRS_CTC = arrSubGrupoCotacao(1)
		NR_CTC_SGRO = objCon.pNR_CTC_SGRO
		NR_VRS_CTC = objCon.pNR_VRS_CTC
	end if
end if

if not bSubgrupo or trim(request("NM_PRPN_CTC")) <> "" then 
	set rsSeqlCobertura = objcon.mLerSeqlCobertura()
%>	
	<tr>
		<td class="td_label" width="150">
			Objeto do Risco
		</td>
		<td class="td_dado">&nbsp;
			<%If Not rsSeqlCobertura.EOF Then
				If rsSeqlCobertura("NR_SEQL_CBT_CTC") <> "9999" then vLMI = false Else vLMI = True End If
			End If%>
			<select name="NR_SEQL_CBT_CTC" onchange="alterasequencial();"<%If vLMI Then Response.Write " style='width: 100px;'" End If%>>
				<option value="">--Escolha abaixo--</option>
				<%ArrayJS=""
				do while not rsSeqlCobertura.eof%>
					<option value="<% = rsSeqlCobertura("NR_SEQL_CBT_CTC") %>"<%if trim(request("NR_SEQL_CBT_CTC")) = trim(rsSeqlCobertura("NR_SEQL_CBT_CTC")) then Response.Write " selected "%>>
						<%If Not vLMI Then 
							Response.Write rsSeqlCobertura("NR_SEQL_CBT_CTC")
						Else
							Response.Write " LMI "
						End If%></option>
					<%ArrayJS = ArrayJS & "," & rsSeqlCobertura("NR_SEQL_CBT_CTC")
					rsSeqlCobertura.movenext
				loop
				ArrayJS = right(ArrayJS,len(ArrayJS)-1)%>
			</select>
		</td>
	</tr>
</table>
<script>
	var franquias = new Array();
	var valoresCobertura = new Array(<% = ArrayJS %>);
	function apagaCelulas(){
		for(x=0;x<document.formProc.contador.value;x++) {
			for(f=0;f<valoresCobertura.length;f++){
				if(document.getElementById('tr' +  valoresCobertura[f] + '_' + x)!=null) {
					document.getElementById('tr' +  valoresCobertura[f] + '_' + x).style.display = 'none';
				}
			}
		}
		for(x=0;x<document.formProc.contador.value;x++) {
			document.getElementById('questionario' + x).style.display = 'none';
			eval('document.forms[0].exibe' + x + '.value = "N"');
		}
	}
	function acendeCelulas(ID){
		apagaCelulas();
		if(ID!=""){
			for(x=0;x<document.formProc.contador.value;x++) {
				if(document.getElementById('tr' + ID + '_' + x)!=null) {
					document.getElementById('tr' + ID + '_' + x).style.display = '';
					eval('document.forms[0].exibe' + x + '.value = "S"');
				}
			}
		}
	}
</script>
<%
if trim(request("NR_SEQL_CBT_CTC")) <> "" then
	objcon.pNR_SEQL_CBT_CTC = trim(request("NR_SEQL_CBT_CTC"))
	set rsCobertura = objcon.mLerDadosCobertura()
	if not rsCobertura.eof then
		
		valor_is_total = 0
		valor_premio_net_total = 0
%>
<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
	<tr>
		<td class="td_label" width="20"></td>
		<td class="td_label" width="25"><b>C�d. Cob</b></td>
		<td class="td_label" width="260"><b>Cobertura</b></td>
		<td class="td_label" width="65"><b>Valor IS</b></td>
		<td class="td_label" width="256"><b>Franquia</b></td>
		<td class="td_label" width="60"><b>Taxa NET</b></td>
		<td class="td_label"><b>Pr�mio NET</b></td>
	</tr>
</table>
<DIV style="OVERFLOW: scroll; WIDTH: 100%; HEIGHT: 254px">
<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
	<%
	do while not rsCobertura.eof
		alterna_cor
		pc_prem_cbt_ctc = testaDadoVazioFormataN(rsCobertura,"pc_prem_cbt_ctc",5)
		vl_prem_cbt_ctc = testaDadoVazioFormataN(rsCobertura,"vl_prem_cbt_ctc",2)

		if testaDadoVazioFormataN(rsCobertura,"vl_iptc_cbt_ctc",2) <> "" and not isnull(rsCobertura("ACUMULA_IS")) then
			if ucase(trim(rsCobertura("ACUMULA_IS"))) = "S" then
				valor_is_total = cdbl(valor_is_total) + cdbl(testaDadoVazioFormataN(rsCobertura,"vl_iptc_cbt_ctc",2))
			end if
		end if
		if trim(vl_prem_cbt_ctc) <> "" then
			valor_premio_net_total = cdbl(valor_premio_net_total) + cdbl(vl_prem_cbt_ctc)
		end if
		%>
		<tr id="tr<%=testaDadoVazioN(rsCobertura,"NR_SEQL_CBT_CTC")%>_<%=iContador%>">
			<td width="20" class="td_dado" style="<%=cor_zebra%>">
				<input type="radio" name="R" onClick="preencheCampos(<%= iContador%>);" style="cursor:hand;">
			</td>		
			<td width="25" class="td_dado" style="<%=cor_zebra%>">
				<input type="hidden" name="CD_CBT<%= iContador%>" value="<% = testaDadoVazioN(rsCobertura,"CD_CBT") %>">
				&nbsp;<%= testaDadoVazioN(rsCobertura,"CD_CBT")%>
			</td>
			<td width="260" class="td_dado" style="<%=cor_zebra%>">
				<input type="hidden" name="nome<%= iContador%>" value="<%= testaDadoVazioN(rsCobertura,"nome")%>">
				<input type="hidden" name="NR_SEQL_CBT_CTC<%= iContador%>" value="<%= testaDadoVazioN(rsCobertura,"NR_SEQL_CBT_CTC")%>">
				<input type="hidden" name="DT_INC_VCL<%= iContador%>" value="<%= testaDadoVazioN(rsCobertura,"DT_INC_VCL")%>">
				&nbsp;<%= testaDadoVazioN(rsCobertura,"nome")%>
			</td>
			<td width="65" class="td_dado" style="<%=cor_zebra%>">
				<input type="hidden" name="vl_iptc_cbt_ctc<%= iContador%>" value="<%= testaDadoVazioFormataN(rsCobertura,"vl_iptc_cbt_ctc",2)%>">
				&nbsp;<%= testaDadoVazioFormataN(rsCobertura,"vl_iptc_cbt_ctc",2)%>
				<input type="hidden" name="pc_prem_cbt_ctc<%= iContador%>" value="<%= pc_prem_cbt_ctc %>">
				<input type="hidden" name="vl_prem_cbt_ctc<%= iContador%>" value="<%= vl_prem_cbt_ctc %>">
				<input type="hidden" name="LIMITE_CAPITAL<%= iContador%>" value="<%=testaDadoVazioN(rsCobertura,"LIMITE_CAPITAL") %>">
			</td>
			<td width="256" class="td_dado" style="<%=cor_zebra%>">
			<%
			objcon.pCD_CBT = rsCobertura("CD_CBT")
			set rsFranquia = objcon.mLerDadosFranquia
			tx_rgr_frquFim = ""
			tx_rgr_frquTotal = ""
			if not rsFranquia.eof then
				do while not rsFranquia.eof
					tx_rgr_frqu = (trim(rsFranquia("tx_rgr_frqu")))
					tx_rgr_frquTotal = tx_rgr_frquTotal & "|" & tx_rgr_frqu
					if tx_rgr_frquAux = "" then
						if ucase(trim(rsFranquia("in_frqu_pdro"))) = "S" then
							tx_rgr_frquAux = tx_rgr_frqu
						end if
					end if
					if tx_rgr_frquAux = tx_rgr_frqu then
						tx_rgr_frquFim = tx_rgr_frqu
					end if
					rsFranquia.movenext
				loop
				tx_rgr_frquTotal = right(tx_rgr_frquTotal,len(tx_rgr_frquTotal)-1)
				if tx_rgr_frquFim = "" then
					rsFranquia.movefirst
					tx_rgr_frquFim = server.HTMLEncode(trim(rsFranquia("tx_rgr_frqu")))
				end if
				%>
				<input type="hidden" name="tx_rgr_frqu<%= iContador%>" value="<%= testaDadoVazioN(rsCobertura,"TX_FRQU_CTC")%>">
				<span align="right" id="ltx_rgr_frqu<%= iContador%>"><%= testaDadoVazio(rsCobertura,"TX_FRQU_CTC")%></span>
			<%else%>
				<input type="hidden" name="tx_rgr_frqu<%= iContador%>" value="">
				<span align="right" id="ltx_rgr_frqu<%= iContador%>">-- N�o h� franquias --</span>
			<%end if%>
				<script>
					franquias[<%= iContador%>] = '<% = tx_rgr_frquTotal %>';
				</script>
			</td>
			<td width="60" class="td_dado" style="<%=cor_zebra%>">
				<div align="right" id="lpc_prem_cbt_ctc<%= iContador%>"><%= pc_prem_cbt_ctc %></div>
			</td>
			<td class="td_dado" style="<%=cor_zebra%>">
				<div align="right" id="lvl_prem_cbt_ctc<%= iContador%>"><%= vl_prem_cbt_ctc %></div>
			</td>
		</tr>
		<input type="hidden" value="" name="exibe<%=iContador%>">
		<%
		iContador = iContador + 1
		rsCobertura.movenext
	loop%>
	</table>
	<br>
	<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
		<tr>
			<td class="td_label_negrito" width="150">Valor IS Total:</td>
			<td class="td_dado"><%=formatnumber(valor_is_total,2)%></td>
		</tr>
		<tr>
			<td class="td_label_negrito" width="150">Pr�mio NET Total:</td>
			<td class="td_dado"><%=formatnumber(valor_premio_net_total,2)%></td>
		</tr>
	</table>
	</div>
	<br>
	<input type="hidden" name="numero">
	<input type="hidden" name="contador" value="<% = iContador %>">
	<table cellpadding="1" cellspacing="4" border="0" width="100%" class="escolha">
		<tr>
			<td class="td_label" width="150">&nbsp;Cobertura
			<input type="hidden" name="CD_CBT_ALT">
			<input type="hidden" value="" name="DT_INC_VCL_ALT">
			</td>
			<td class="td_dado" >&nbsp;<input type="text" name="nome" readonly style="background-color:#EEEEEE;" size="80"></td>
		</tr>
		<tr>
			<td class="td_label" width="150">&nbsp;Valor IS</td>
			<td class="td_dado">&nbsp;<input type="text" name="vl_iptc_cbt_ctc" readonly style="background-color:#EEEEEE;text-align:right;" size="19">
			&nbsp;&nbsp;&nbsp;<font color="red"><span id="aviso"></span></font>
			</td>
		</tr>
		<tr>
			<td class="td_label" width="150">&nbsp;Franquia</td>
			<td class="td_dado" >&nbsp;<select name="tx_rgr_frqu" style="width:250px" readonly style="background-color:#EEEEEE;text-align:right;" disabled>
			</select>&nbsp;<input type="text" name="tx_rgr_frqu_edicao" size="38" maxlength="60" readonly style="background-color:#EEEEEE;text-align:right;"></td>
		</tr>
		<tr>
			<td class="td_label" width="150">&nbsp;Taxa NET</td>
			<td class="td_dado" >&nbsp;<input type="text" name="pc_prem_cbt_ctc" onKeyUp="FormatValorContinuo(this,5);" style="text-align:right;" size="9" maxlength="9" readonly style="background-color:#EEEEEE;text-align:right;"></td>
		</tr>
		<tr>
			<td class="td_label" width="150">&nbsp;Pr�mio NET</td>
			<td class="td_dado">&nbsp;<input type="text" name="vl_prem_cbt_ctc" onKeyUp="FormatValorContinuo(this,2);" style="text-align:right;" size="19" maxlength="19" readonly style="background-color:#EEEEEE;text-align:right;"></td>
		</tr>
	</table>
	<br>
	<!-- #include virtual = "/internet/serv/siscot3/funcoes_dados_qst.asp"-->
	<%
	htmlExibir = ""
	rsCobertura.movefirst
	contador = 0
	do while not rsCobertura.eof
		objcon.pCD_CBT = rsCobertura("CD_CBT")
		objcon.pCD_TIP_QSTN = "3"
		set rsQST = objcon.mLerQuestionarioId()
		%><table border="0" id="questionario<% = contador %>" style="display:none;"><tr><td><%
		if not rsQST.eof then
			questionario_id = rsQST("NR_QSTN")
			objcon.pNR_QSTN = questionario_id
			set rsNomeQSTN = objcon.mLerLocalRisco()		
			if not rsNomeQSTN.eof then
				if trim(rsNomeQSTN("NR_CD_TIP_QSTN_CTC")) <> "0" and trim(rsNomeQSTN("NR_CD_TIP_QSTN_CTC")) <> "" then
					NR_SEQL_QSTN_CTC = rsNomeQSTN("NR_SEQL_QSTN_CTC")
					htmlExibir = "<font face=""arial"" size=""2""><b>" & rsNomeQSTN("nome") & "</b></font>"
					%>
					<!-- #include virtual = "/internet/serv/siscot3/dados_qst.asp"-->
					<%
				end if
			end if
		end if%>
		</td></tr></table>
		<%
		rsCobertura.movenext
		contador = contador + 1
	loop
	rsCobertura.movefirst
	%>

	<%if trim(request("numero_selecao")) <> "" and trim(strError) = "" then%>
	<script>selecionarcobertura(<%=trim(request("numero_selecao"))%>);</script>
	<%elseif trim(strError) <> "" then%>
	<script>selecionarcobertura(<%=trim(request("numero_atual"))%>);</script>
	<%end if%>
<%else%>
		<tr>
			<td class="td_label">&nbsp;Nenhuma cobertura relacionada.</td>
		</tr>
	</table>
<%end if%>
<%end if%>
<%end if%>
