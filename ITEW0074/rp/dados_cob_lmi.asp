<!--#include virtual = "/padrao.asp"-->
<script>
	var taxaNet = "";

	function rValorFocus(valor){
		taxaNet = valor;
	}
	function rValorBlur(){
		if(taxaNet!=document.formProc.pc_prem_cbt_ctc.value){
			recalculaPremioNet();
		}
	}
	function recalculaPremioNet(){
		vl_iptc_cbt_ctc = document.formProc.vl_iptc_cbt_ctc.value;
		pc_prem_cbt_ctc = document.formProc.pc_prem_cbt_ctc.value;
		if((vl_iptc_cbt_ctc!="")&&(pc_prem_cbt_ctc!="")){
			vl_iptc_cbt_ctc = converteNumeroJavascript(vl_iptc_cbt_ctc) * 10000;
			pc_prem_cbt_ctc = converteNumeroJavascript(pc_prem_cbt_ctc) * 10000;
			vl_prem_cbt_ctc = (vl_iptc_cbt_ctc * pc_prem_cbt_ctc)/10000000000;
			vl_prem_cbt_ctc = converteNumeroJavascriptTruncado(vl_prem_cbt_ctc, 2);
			vl_prem_cbt_ctc = FormatNumber(vl_prem_cbt_ctc.replace('.',','), 2, 3);
			pc_prem_cbt_ctc = document.formProc.pc_prem_cbt_ctc.value;
			pc_prem_cbt_ctc = FormatNumber(pc_prem_cbt_ctc.replace('.',','), 4);
			document.formProc.pc_prem_cbt_ctc.value = pc_prem_cbt_ctc;
			document.formProc.vl_prem_cbt_ctc.value = vl_prem_cbt_ctc;
		}
	}
	function converteNumeroJavascriptTruncado(valor,casas){
		valor = valor + '';
		valor = valor.split('.');
		valorNovo = '';
		for(f=0;f<valor[0].length;f++){
			if((valor[0].charCodeAt(f)>=48)&&(valor[0].charCodeAt(f)<=57)){
				valorNovo=valorNovo+valor[0].charAt(f);
			}
		}
		valorNovo = valorNovo + '.';
		
		for(f=0;f<casas;f++){
			if(valor.length>1){
				if(valor[1].length>f){
					valorNovo = valorNovo + valor[1].charAt(f);
				} else {
					valorNovo = valorNovo + '0';
				}
			} else {
				valorNovo = valorNovo + '0';
			}
		}
		return valorNovo;
	}
	function converteNumeroJavascript(valor){
		valor = valor.split(',');
		valorNovo = '';
		for(f=0;f<valor[0].length;f++){
			if((valor[0].charCodeAt(f)>=48)&&(valor[0].charCodeAt(f)<=57)){
				valorNovo=valorNovo+valor[0].charAt(f);
			}
		}
		if(valor.length>1){
			valorNovo = valorNovo + '.' + valor[1];
		}
		return valorNovo;
	}
	function selecionaOption(obj,valor){
		for(f=0;f<obj.length;f++){
			if(obj[f].value == valor){
				obj[f].selected = true;
			}
		}
	}
	function colocaOption(obj,valor){
		option = new Option('value','text');
		option.value = valor;
		option.text = valor;
		obj[obj.length] = option;
	}
	function apagaOptions(obj){
		while(obj.length>0){
			obj[0] = null;
		}
	}
	function calculaNET(tipo)
	{
		var valor_is = parseFloat('0' + converteNumeroJavascript(document.formProc.vl_iptc_cbt_ctc.value));
	
		if (tipo == 'T') //calcula a taxa
		{
			var premio_net = parseFloat('0' + converteNumeroJavascript(document.formProc.vl_prem_cbt_ctc.value));
			var taxa_net = converteNumeroJavascriptTruncado(((premio_net / valor_is) * 100), 5);
	        if (taxaNet!=premio_net) document.formProc.altval.value=1;

			document.formProc.pc_prem_cbt_ctc.value = FormatNumber(taxa_net.toString().replace(".", ","), 4);
		}
		else if (tipo == 'P') //calcula o premio
		{
			var taxa_net = parseFloat('0' + converteNumeroJavascript(document.formProc.pc_prem_cbt_ctc.value));
			var premio_net = converteNumeroJavascriptTruncado(((taxa_net * valor_is) / 100), 2);
	        if (taxaNet!=taxa_net)   document.formProc.altval.value=1;

			document.formProc.vl_prem_cbt_ctc.value = FormatNumber(premio_net.toString().replace(".", ","), 2);
		}
	}

    function habilita_nr_seq(acao,b){
        document.formProc.alterar.disabled=b;
        document.formProc.NR_SEQL_CBT_CTC.style.display='';
        document.formProc.NR_SEQL_CBT_CTC.selectedIndex = 0;
        document.formProc.guardaSel.value=acao;
    }
</script>
<br>
<table cellspacing="0" border="0" width="807" class="escolha" height="10">
	<tr>
		<td colspan="2" class="td_dado" width="805"><font color="red"><span id="msgerro"></span></font>
<input type="hidden" name="salvarcob" value="N">
<input type="hidden" name="numero_selecao" value="">
<input type="hidden" name="numero_atual" value=""></td></tr>
<%
Set rs = objcon.mLerSubgrupoCotacao
rs.ActiveConnection = Nothing
If rs.EOF Then
	bSubgrupo = False
Else
	bSubgrupo = True
%>
	<tr>
		<td colspan="2" class="td_label" width="805">
			Subgrupo&nbsp;
			<select name="NM_PRPN_CTC" onchange="processasubgrupo();">
			<option value="">--Escolha abaixo--</option>
			<%
			Do while not rs.EOF
				If Trim(request("NM_PRPN_CTC")) = Trim(rs("NR_CTC_SGRO")) & "|" & Trim(rs("NR_VRS_CTC")) then
					sSelecionado = " selected"
				Else
					sSelecionado = ""
				End If
				%>
				<option value="<%=trim(rs("NR_CTC_SGRO")) & "|" & trim(rs("NR_VRS_CTC"))%>"<%=sSelecionado%>><%=right(00 & rs("subgrupo_id"),2) & " (" & trim(rs("NR_CTC_SGRO")) & ")"%> - <%=trim(rs("NM_PRPN_CTC"))%></option>
				<%
				rs.movenext
			Loop
			%>
			</select>
		</td>
	</tr>
<%
	If Trim(Request("NM_PRPN_CTC")) <> "" Then 
		arrSubGrupoCotacao = Split(Trim(Request("NM_PRPN_CTC")),"|")

		objCon.pNR_CTC_SGRO = arrSubGrupoCotacao(0)
		objCon.pNR_VRS_CTC  = arrSubGrupoCotacao(1)
		NR_CTC_SGRO = objCon.pNR_CTC_SGRO
		NR_VRS_CTC  = objCon.pNR_VRS_CTC
	End If
End If
rs.Close
Set rs = Nothing

seq_maior = 0
If NOT bSubgrupo or Trim(Request("NM_PRPN_CTC")) <> "" Then 
	Set rsSeqlCobertura = objcon.mLerSeqlCobertura()
	rsSeqlCobertura.ActiveConnection = Nothing%>	
<script>
	function preencheCampos(numero){
		try{document.formProc.R[numero].checked = true;}
		catch(ex){document.formProc.R.checked=true;}

		document.formProc.altval.value              = 0;
		document.formProc.CD_CBT_ALT.value          = document.formProc["CD_CBT" + numero].value;
		document.formProc.DT_INC_VCL_ALT.value      = document.formProc["DT_INC_VCL" + numero].value;
		document.formProc.nome.value                = document.formProc["nome" + numero].value;
		document.formProc.vl_iptc_cbt_ctc.value     = FormatNumber(document.formProc["vl_iptc_cbt_ctc" + numero].value,2);
        document.formProc.nr_seql_cbt_ctc_lst.value = document.formProc["nr_sequencial_ctc" + numero].value;
		document.formProc.pc_prem_cbt_ctc.value     = document.formProc["pc_prem_cbt_ctc" + numero].value;
		document.formProc.vl_prem_cbt_ctc.value     = FormatNumber(document.formProc["vl_prem_cbt_ctc" + numero].value,2);
  	    document.formProc.numero_atual.value        = numero;
		
		if (document.formProc["limite_capital" + numero].value == "-1")
			document.all.aviso.innerHTML = 'O Valor � menor que o m�nimo cadastrado';
		else if (document.formProc["limite_capital" + numero].value == "-1")
			document.all.aviso.innerHTML = 'O Valor � maior que o m�ximo cadastrado';
		else
			document.all.aviso.innerHTML = '';

        var var_local = document.formProc["nr_sequencial_ctc" + numero].value+', ';
        var locais    = var_local.split(", ");
        // Valor de franquia ja gravada
		var franq_local = document.formProc["TXT_FRANQUIA" + numero].value.split("@");

		// Valores de franquia por cobertura
        var franq_cob   = document.formProc["TXT_FRAN_COB" + numero].value.split("@");

		var ex_franq = '<table cellpadding="1" cellspacing="4" border="0" width="550" class="escolha"><tr><td class="td_label" width="150">&nbsp;<b>C�d.Obj.</b></td><td class="td_label">&nbsp;<b>Franquia</b></td></tr>';
		var branco = 'B';

	    for(i=0;i<locais.length-1;i++){
		    branco = '';
		    ex_franq = ex_franq + '<tr><td class="td_dado" width="50"';
		    if(i%2) ex_franq = ex_franq + ' style="background-color:#f1F1f1"';
		    ex_franq = ex_franq + '><center>'+locais[i]+'</center></td><td class="td_dado"'
		    if(i%2) ex_franq = ex_franq + ' style="background-color:#f1F1f1"';
			ex_franq = ex_franq + '>&nbsp;<select name="tx_rgr_frqu'+i+'" style="width:250px" onChange="if (this.selectedIndex > 0) document.formProc.tx_rgr_frqu_edicao'+i+'.value=this.value; else document.formProc.tx_rgr_frqu_edicao'+i+'.value=branco">';

			// Franquias da cobertura
			aFranquia = franq_cob[i].split('|');
            for(f=0;f<aFranquia.length-1;f++){
			    ex_franq = ex_franq + '<option value="' + aFranquia[f] + '">' + aFranquia[f] + '</option>';
			}
      		ex_franq = ex_franq + '</select>&nbsp;<input type="text" name="tx_rgr_frqu_edicao'+i+'" onChange="document.formProc.altval.value=1;document.formProc.FRANQUIAS_'+i+'.value=this.value;" size="38" maxlength="60" value="';

      		if(franq_local[i]==""){
      		    ex_franq = ex_franq + aFranquia[0] +'"></td></tr>'
			    // Guarda para gravacao
 			    eval('document.formProc.FRANQUIAS_' + i + '.value="' + aFranquia[0] +'"')
      		} else{
      		    ex_franq = ex_franq + franq_local[i] +'"></td></tr>'
			    // Guarda para gravacao
 			    eval('document.formProc.FRANQUIAS_' + i + '.value="' + franq_local[i] +'"')
			}
        }

        if(branco=='B')
           document.all.div_franq.innerHTML = '<table cellpadding="1" cellspacing="4" border="0" width="850" class="escolha"><tr><td>&nbsp;</td></tr></table>'
        else
           document.all.div_franq.innerHTML = ex_franq+'</table>';

        document.formProc.pc_prem_cbt_ctc.focus();
	}
    </script>
<%' Codifica��o da grava��o
rsSeqlCobertura.Close
Set rsSeqlCobertura  = Nothing
    
strError = ""
iContador = 0

Locais = Split(Trim(Request("nr_seql_cbt_ctc_lst"))&", ",", ")

If Trim(Request("salvarcob")) = "S" and Trim(Request("CD_CBT_ALT")) <> "" And Request("guardaSel") <> "1" And Ubound(Locais) > 0 Then

	Set rsCotacao = objCon.mLerCotacao
	rsCotacao.ActiveConnection = Nothing

	If UCase(rsCotacao("CD_ULT_EVT_CTC")) <> "T" Then
		objcon.pCD_ULT_EVT_CTC = "T"
		bAtualizaCotacao = True
	Else
		bAtualizaCotacao = False
	End If

	If Trim(Request("NM_PRPN_CTC")) <> "" Then
		arrSubGrupoCotacao = split(trim(request("NM_PRPN_CTC")),"|")
		NR_CTC_SGRO_subgrupo = arrSubGrupoCotacao(0)
		NR_VRS_CTC_subgrupo = arrSubGrupoCotacao(1)
	Else
		NR_CTC_SGRO_subgrupo = NR_CTC_SGRO
		NR_VRS_CTC_subgrupo = NR_VRS_CTC
	End If

    For i = 0 To UBound(Locais) - 1
        chave
        variavel = Trim("FRANQUIAS_" & i)

	    objCon.pCD_CBT          = trim(request("CD_CBT_ALT"))
	    objCon.pNR_SEQL_CBT_CTC = trim(Locais(i))
	    objCon.pDT_INC_VCL      = trim(request("DT_INC_VCL_ALT"))
	    objCon.ptx_rgr_frqu     = trim(request(variavel))
	    objCon.ppc_prem_cbt_ctc = trim(request("pc_prem_cbt_ctc"))
	    objCon.pvl_prem_cbt_ctc = trim(request("vl_prem_cbt_ctc"))

 	    retorno = objCon.mAtualizarCobertura(NR_CTC_SGRO_subgrupo, NR_VRS_CTC_subgrupo, bAtualizaCotacao)
        bAtualizaCotacao = False
	Next

	If Trim(retorno) <> "" Then
		Response.redirect("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
		Response.End 
	End If

	chave
End If
%>
<script>
function processasubgrupo()
{
	try{document.formProc.NR_SEQL_CBT_CTC.selectedIndex = 0;} catch(ex){}
	submitcobertura();
}

function alterarDados()
{
	if (validaform())
	{
		document.formProc.salvarcob.value = 'S';
		submitcobertura();
	}
}

function validaform()
{
	var locais = document.formProc.nr_seql_cbt_ctc_lst.value.split(',');
    for(x=0;x<locais.length;x++){
  	    if(Trim(eval('document.formProc.FRANQUIAS_'+x+'.value')) == "")
		{
		   document.all.msgerro.innerHTML = '� necess�rio escolher uma franquia<br><br>';
		   document.formProc.altval.value = 0;
		   document.formProc.numero_selecao.value = 999;
		   return false;
		}
	}
	return true;
}

function submitcobertura(){
	if(document.formProc.numero_selecao.value=="") var cod_cobertura = "";
    else var cod_cobertura = eval('document.formProc.CD_CBT'+document.formProc.numero_selecao.value+'.value');
	document.formProc.action = 'liberar_cotacao_cob.asp?cod_param='+cod_cobertura;
    document.formProc.cursor = 'wait';
	document.formProc.submit();
}

function alterasequencial()
{
	document.formProc.numero_selecao.value = '';
	submitcobertura();
}

function selecionarcobertura(numero)
{   
	if ((document.formProc.guardaSel.value==1 || document.formProc.numero_selecao.value=='') && (document.formProc.altval.value==0))
	{   
		document.formProc.numero_selecao.value = numero;
        submitcobertura();
	}
    else
	{
		document.formProc.numero_selecao.value = numero;
		alterarDados();
	}
}

	var franquias = new Array();
	var valoresCobertura = new Array(<% = ArrayJS %>);
	function apagaCelulas(){
		for(x=0;x<document.formProc.contador.value;x++) {
			for(f=0;f<valoresCobertura.length;f++){
				if(document.getElementById('tr' +  valoresCobertura[f] + '_' + x)!=null) {
					document.getElementById('tr' +  valoresCobertura[f] + '_' + x).style.display = 'none';
				}
			}
		}
		for(x=0;x<document.formProc.contador.value;x++) {
			document.getElementById('questionario' + x).style.display = 'none';
			eval('document.forms[0].exibe' + x + '.value = "N"');
		}
	}
	function acendeCelulas(ID){
		apagaCelulas();
		if(ID!=""){
			for(x=0;x<document.formProc.contador.value;x++) {
				if(document.getElementById('tr' + ID + '_' + x)!=null) {
					document.getElementById('tr' + ID + '_' + x).style.display = '';
					eval('document.forms[0].exibe' + x + '.value = "S"');
				}
			}
		}
	}
    </script>
<%
    Redim aCobertASP(100,11)
    If Trim(Request("NR_SEQL_CBT_CTC")) <> "" Then
  	   objcon.pNR_SEQL_CBT_CTC = trim(request("NR_SEQL_CBT_CTC"))
    End If

	Set rsCobertura = objCon.mLerDadosCoberturaLmi()
	rsCobertura.ActiveConnection = Nothing
	If NOT rsCobertura.EOF Then
		
		valor_lmi_total = 0
		valor_premio_net_total = 0
%>
<tr><td>
<DIV style="OVERFLOW: scroll; WIDTH: 100%; HEIGHT: 254px">
<table cellpadding="3" cellspacing="0" border="1" width="832" class="escolha" bordercolor="#EEEEEE">
	<tr>
		<td class="td_label" width="20"></td>
		<td class="td_label" width="41"><b>C�d. Cob</b></td>
		<td class="td_label" width="280"><b>Cobertura</b></td>
		<td class="td_label" width="130"><b>Locais de Risco</b></td>
		<td class="td_label" width="78"><b>LMI</b></td>
		<td class="td_label" width="46"><b>Taxa NET</b></td>
		<td class="td_label" width="82"><b>Pr�mio NET</b></td>
	</tr>

	<% Do while not rsCobertura.EOF
		  pc_prem_cbt_ctc = testaDadoVazioFormataN(rsCobertura,"pc_prem_cbt_ctc",5)
		  vl_prem_cbt_ctc = testaDadoVazioFormataN(rsCobertura,"vl_prem_cbt_ctc",2)
          vl_iptc_cbt_ctc = testaDadoVazioFormataN(rsCobertura,"vl_iptc_cbt_ctc",2)
	  	  If vl_iptc_cbt_ctc <> "" and not isnull(rsCobertura("ACUMULA_IS")) Then
			 If ucase(trim(rsCobertura("ACUMULA_IS"))) = "S" then
				valor_lmi_total = cdbl(valor_lmi_total) + cdbl(vl_iptc_cbt_ctc)
			 End If
		  End If
		  If trim(vl_prem_cbt_ctc) <> "" Then
		 	 valor_premio_net_total = cdbl(valor_premio_net_total) + cdbl(vl_prem_cbt_ctc)
		  End If
		
		 'Indexador i, refere-se a cobertura
		  achou = "N"
	      cd_cbt = testaDadoVazioN(rsCobertura,"CD_CBT")
	      codObjeto   = cd_cbt & "|" & testaDadoVazioN(rsCobertura,"NR_SEQL_LIM_CTC")
	      nr_seql_cbt = testaDadoVazioN(rsCobertura,"NR_LIM_QSTN_CTC")

         'Franquias
		  objcon.pCD_CBT = cd_cbt
		  objcon.pNR_SEQL_CBT_CTC = nr_seql_cbt
		  Set rsFranquia = objcon.mLerDadosFranquia
		  rsFranquia.ActiveConnection = Nothing
		  tx_rgr_frquTotal = ""
		  If not rsFranquia.EOF Then
		      Do while not rsFranquia.EOF
			     tx_rgr_frquTotal = tx_rgr_frquTotal & Trim(rsFranquia("tx_rgr_frqu")) & "|"
			     If ucase(trim(rsFranquia("in_frqu_pdro"))) = "S" then
				 	tx_rgr_frquAux = Trim(rsFranquia("tx_rgr_frqu")) & tx_rgr_frquTotal & "|"
			     End If
			     rsFranquia.movenext
		      Loop
		  End If

		  For i = 0 To iContador - 1
		     'Acumula locais de risco
		      If aCobertASP(i,10) = codObjeto Then
		         achou = "S"
		         aCobertASP(i,1) = aCobertASP(i,1) & ", " & nr_seql_cbt
		         aCobertASP(i,7) = aCobertASP(i,7) & testaDadoVazioN(rsCobertura,"TX_FRQU_CTC") & "@"
		         aCobertASP(i,8) = aCobertASP(i,8) & tx_rgr_frquTotal & "@"
		         Exit For
		      End If
		  Next
		  If achou = "N" Then
		      aCobertASP(iContador,0)  = cd_cbt
		      aCobertASP(iContador,1)  = nr_seql_cbt
		      aCobertASP(iContador,2)  = testaDadoVazioN(rsCobertura,"nome")
		      aCobertASP(iContador,3)  = testaDadoVazioN(rsCobertura,"DT_INC_VCL")
		      aCobertASP(iContador,4)  = vl_iptc_cbt_ctc
		      aCobertASP(iContador,5)  = pc_prem_cbt_ctc
		      aCobertASP(iContador,6)  = vl_prem_cbt_ctc
		      aCobertASP(iContador,7)  = testaDadoVazioN(rsCobertura,"TX_FRQU_CTC") & "@"
		      aCobertASP(iContador,8)  = tx_rgr_frquTotal & "@"
		      aCobertASP(iContador,9)  = testaDadoVazioN(rsCobertura,"LIMITE_CAPITAL")
		      aCobertASP(iContador,10) = codObjeto
 		      iContador = iContador + 1
 		  End If
		  rsCobertura.movenext
	   Loop
 
       For i = 0 To iContador - 1
		   alterna_cor%>
		<tr id="tr_<%= i%>">
			<td width="20" class="td_dado" style="<%=cor_zebra%>">
				<input type="radio" name="R" onClick="selecionarcobertura(<%= i%>);" style="cursor:hand;">
			</td>
			<td width="41" class="td_dado" style="<%=cor_zebra%>">
 		  	    <input type="hidden" name="CD_CBT<%= i%>" value="<%= aCobertASP(i,0)%>">
 		  	    <input type="hidden" name="DT_INC_VCL<%= i%>" value="<%= aCobertASP(i,3)%>">
 		  	    <input type="hidden" name="TXT_FRANQUIA<%= i%>" value="<%= aCobertASP(i,7)%>">
 		  	    <input type="hidden" name="TXT_FRAN_COB<%= i%>" value="<%= aCobertASP(i,8)%>">
				&nbsp;<%= aCobertASP(i,0)%>
			</td>
			<td width="280" class="td_dado" style="<%=cor_zebra%>">
			    <input type="hidden" name="nome<%= i%>" value="<%= aCobertASP(i,2)%>">
				&nbsp;<%= aCobertASP(i,2)%>
			</td>
			<td width="130" class="td_dado" style="<%=cor_zebra%>">
				<input type="hidden" name="nr_sequencial_ctc<%= i%>" value="<%= aCobertASP(i,1)%>">
                &nbsp;<%= aCobertASP(i,1)%>
			</td>
			<td width="78" class="td_dado" style="<%=cor_zebra%>">
			    <input type="hidden" name="vl_iptc_cbt_ctc<%= i%>" value="<%= aCobertASP(i,4)%>">
				&nbsp;<%= Formatnumber(aCobertASP(i,4),2)%>
			</td>
			<td width="46" class="td_dado" style="<%=cor_zebra%>">
			    <input type="hidden" name="pc_prem_cbt_ctc<%= i%>" value="<%= aCobertASP(i,5)%>">
				<div align="right" id="lpc_prem_cbt_ctc<%= i%>"><%= formatnumber(aCobertASP(i,5),5)%></div>
			</td>
			<td width="82" class="td_dado" style="<%=cor_zebra%>">
			    <input type="hidden" name="vl_prem_cbt_ctc<%= i%>" value="<%= aCobertASP(i,6)%>">
			    <input type="hidden" name="limite_capital<%= i%>" value="<%= aCobertASP(i,9)%>">
				<div align="right" id="lvl_prem_cbt_ctc<%= i%>"><%= formatnumber(aCobertASP(i,6),2)%></div>
			</td>
		 </tr>
	 	 <input type="hidden" value="" name="exibe<%= i%>">
     <%Next
' Mostra os dados da cobertura selecionada, se selecionada
  If Request("numero_selecao") <> "" Then
     Locais = Split(Trim(aCobertASP(CInt(Request("numero_selecao")),1))&", ",", ")
  End If
%>
</table>
	<br>
	<table cellpadding="3" cellspacing="0" border="1" width="831" class="escolha" bordercolor="#EEEEEE">
		<tr>
			<td class="td_label_negrito" width="229">Limite M�ximo de Garantia - 
            LMG</td>
			<td class="td_dado" width="584"><%=formatnumber(valor_lmi_total,2)%>&nbsp;</td>
		</tr>
		<tr>
			<td class="td_label_negrito" width="229">Pr�mio NET Total:</td>
			<td class="td_dado" width="584"><%=formatnumber(valor_premio_net_total,2)%>&nbsp;</td>
		</tr>
	</table>
	</div>
</td></tr><tr><td>
	<br>
	<input type="hidden" name="altval" value="0">
	<input type="hidden" name="contador" value="<% = iContador %>">
	<table cellpadding="1" cellspacing="4" border="0" width="831" class="escolha">
		<tr>
			<td class="td_label" width="225">&nbsp;Cobertura
			<input type="hidden" name="CD_CBT_ALT">
			<input type="hidden" name="DT_INC_VCL_ALT">
			<!-- Campos usados na grava��o -->
			<%If Request("numero_selecao") <> "" Then
			     For i = 0 To Ubound(Locais)-1
			         Response.Write "<input type=""hidden"" name=""FRANQUIAS_" & i & """>"
	             Next
	          End If%>
			</td>
			<td class="td_dado" width="640" >&nbsp;<input type="text" name="nome" readonly style="background-color:#EEEEEE;" size="80"></td>
		</tr>
		<tr>
			<td class="td_label" width="225">&nbsp;Valor LMI</td>
			<td class="td_dado" width="640">&nbsp;<input type="text" name="vl_iptc_cbt_ctc" readonly style="background-color:#EEEEEE;text-align:right;" size="19">
			&nbsp;&nbsp;&nbsp;<font color="red"><span id="aviso"></span></font>
			</td>
		</tr>
		<tr>
			<td class="td_label" width="225">&nbsp;Locais de Risco</td>
			<td class="td_dado" width="640" >&nbsp;<input type="text" name="nr_seql_cbt_ctc_lst" readonly style="background-color:#EEEEEE;text-align:right;" size="19"></td>
		</tr>
		<tr>
			<td class="td_label" width="225">&nbsp;Taxa NET
			<input type="hidden" name="pc_prem_cbt_ctc_comp"></td>
			<td class="td_dado" width="640" >&nbsp;<input type="text" name="pc_prem_cbt_ctc" readonly style="background-color:#EEEEEE;text-align:right;" size="9" maxlength="9"></td>
		</tr>
		<tr>
			<td class="td_label" width="225">&nbsp;Pr�mio NET
			<input type="hidden" name="vl_prem_cbt_ctc_comp"></td>
			<td class="td_dado" width="640">&nbsp;<input type="text" name="vl_prem_cbt_ctc" readonly style="background-color:#EEEEEE;text-align:right;" size="19" maxlength="19"></td>
		</tr>
	</table>
	<br>
	<div id="div_franq">&nbsp;</div>
	<br>
	<!-- #include virtual = "/internet/serv/siscot3/funcoes_dados_qst.asp"-->
<%
	If Request.QueryString("cod_param")<>"" And Request("numero_selecao") <> "" And UBound(Locais) > 0 Then
	      htmlExibir = ""
      
	      objCon.pCD_CBT = Request("cod_param")
	      objCon.pCD_TIP_QSTN = "3"
	   
	      Set rsQST = objcon.mLerQuestionarioId()
	      rsQST.ActiveConnection = Nothing
	      %><table border="0" id="questionario<% = contador %>"><tr><td><%
	      If not rsQST.EOF Then
	 	     questionario_id = rsQST("NR_QSTN")
		     objCon.pNR_QSTN = questionario_id

             For indX = 0 To UBound(Locais)-1
		         objCon.pNR_SEQL_QSTN_CTC = Locais(indX)
  
                 Set rsNomeQSTN = objcon.mLerLocalRisco()
                 rsNomeQSTN.ActiveConnection = Nothing
		         If NOT rsNomeQSTN.EOF Then
			        If Trim(rsNomeQSTN("NR_CD_TIP_QSTN_CTC")) <> "0" and Trim(rsNomeQSTN("NR_CD_TIP_QSTN_CTC")) <> "" Then
                       htmlExibir = "<font face=""arial"" size=""2""><b>Local de Risco: " & objCon.pNR_SEQL_QSTN_CTC & "</b></font>"%>
				    <!-- #include virtual = "/internet/serv/siscot3/dados_qst.asp"-->
				<%  End If
				 End If
			 Next
		  End If%>
		  &nbsp;
		 </td></tr></table>
<%	End If

	If Trim(Request("numero_selecao")) <> "" and Trim(strError) = "" Then%>
	<script>preencheCampos(<%=Trim(Request("numero_selecao"))%>);</script>
  <%ElseIf Trim(strError) <> "" then%>
	<script>preencheCampos(<%=Trim(Request("numero_atual"))%>);</script>
  <%End If

  End If

End If%>
	</table>