<!--#include virtual = "/padrao.asp"-->
<script>
function mostra_div(a) 
{
	esconde_div();
	if (a.style.display =='')
		a.style.display = 'none';
	else
		a.style.display='';
}

function processasubgrupo()
{
	try { document.formProc.NR_SEQL_CBT_CTC.selectedIndex = 0; } catch (ex) { }
	submitclausula();
}

function alterarDados()
{
	document.formProc.salvarcla.value = 'S';
	submitclausula();
}

function submitclausula()
{
	document.formProc.action = 'liberar_cotacao_cla.asp';
	document.formProc.submit();
}
</script>
<input type="hidden" name="salvarcla" value="N">
<br>
<table cellspacing="0" border="0" width="100%" class="escolha" height="10">
<%
set rs = objcon.mLerSubgrupoCotacao

if trim(request("salvarcla")) = "S" and trim(request("CD_CLSL")) <> "" then

	set rsCotacao = objCon.mLerCotacao

	if ucase(rsCotacao("CD_ULT_EVT_CTC")) <> "T" then
		objcon.pCD_ULT_EVT_CTC = "T"
		bAtualizaCotacao = true
	else
		bAtualizaCotacao = false
	end if
	
	if trim(request("NM_PRPN_CTC")) <> "" then
		arrSubGrupoCotacao = split(trim(request("NM_PRPN_CTC")),"|")
		NR_CTC_SGRO_subgrupo = arrSubGrupoCotacao(0)
		NR_VRS_CTC_subgrupo = arrSubGrupoCotacao(1)
	else
		NR_CTC_SGRO_subgrupo = NR_CTC_SGRO
		NR_VRS_CTC_subgrupo = NR_VRS_CTC
	end if
	
	chkcd_clsl = "|" & trim(replace(trim(request("chkcd_clsl")),", ","|")) & "|"
	
	arrCD_CLSL = split(trim(request("CD_CLSL")), ", ")
	arrNR_SEQL_CLSL_CTC = split(trim(request("NR_SEQL_CLSL_CTC")), ", ")
	arrCD_CLSC_CLSL_CTC = split(trim(request("CD_CLSC_CLSL_CTC")), ", ")
	arrCD_OGM_CLSL_CTC = split(trim(request("CD_OGM_CLSL_CTC")), ", ")

	for iC = 0 to ubound(arrCD_CLSL)
		if instr(chkcd_clsl,"|" & trim(arrNR_SEQL_CLSL_CTC(iC)) & "|") <> 0 then
			if trim(CD_CLSL) <> "" then
				CD_CLSL = CD_CLSL & "|" & trim(arrCD_CLSL(iC))
				NR_SEQL_CLSL_CTC = NR_SEQL_CLSL_CTC & "|" & trim(arrNR_SEQL_CLSL_CTC(iC))
				CD_CLSC_CLSL_CTC = CD_CLSC_CLSL_CTC & "|" & trim(arrCD_CLSC_CLSL_CTC(iC))
				CD_OGM_CLSL_CTC = CD_OGM_CLSL_CTC & "|" & trim(arrCD_OGM_CLSL_CTC(iC))
			else
				CD_CLSL = trim(arrCD_CLSL(iC))
				NR_SEQL_CLSL_CTC = trim(arrNR_SEQL_CLSL_CTC(iC))
				CD_CLSC_CLSL_CTC = trim(arrCD_CLSC_CLSL_CTC(iC))
				CD_OGM_CLSL_CTC = trim(arrCD_OGM_CLSL_CTC(iC))
			end if
		end if
	next
	
    objCon.pCD_CLSL = CD_CLSL
    objCon.pNR_SEQL_CLSL_CTC = NR_SEQL_CLSL_CTC
    objCon.pCD_CLSC_CLSL_CTC = CD_CLSC_CLSL_CTC
    objCon.pCD_OGM_CLSL_CTC = CD_OGM_CLSL_CTC

	objCon.pWF_ID = WF_ID

	retorno = objCon.mAtualizarClausula(NR_CTC_SGRO_subgrupo, NR_VRS_CTC_subgrupo, "2", bAtualizaCotacao) '2 = Cl�usulas Facultativas

	if trim(retorno) <> "" then
		Response.redirect("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
		Response.End 
	end if
	
	chave
end if

if rs.eof then
	bSubgrupo = false
else
	bSubgrupo = true
%>
	<br>
	<tr>
		<td class="td_label" width="20%">
			Subgrupo
		</td>
		<td class="td_dado">&nbsp;
			<select name="NM_PRPN_CTC" onchange="processasubgrupo();">
			<option value="">--Escolha abaixo--</option>
			<%
			do while not rs.eof
				if trim(request("NM_PRPN_CTC")) = trim(rs("NR_CTC_SGRO")) & "|" & trim(rs("NR_VRS_CTC")) then
					sSelecionado = " selected"
				else
					sSelecionado = ""
				end if
				%>
				<option value="<%=trim(rs("NR_CTC_SGRO")) & "|" & trim(rs("NR_VRS_CTC"))%>"<%=sSelecionado%>><%=right(00 & rs("subgrupo_id"),2) & " (" & trim(rs("NR_CTC_SGRO")) & ")"%> - <%=trim(rs("NM_PRPN_CTC"))%></option>
				<%
				rs.movenext
			loop
			%>
			</select>
		</td>
	</tr>
<%
	if trim(request("NM_PRPN_CTC")) <> "" then 
		arrSubGrupoCotacao = split(trim(request("NM_PRPN_CTC")),"|")

		objCon.pNR_CTC_SGRO = arrSubGrupoCotacao(0)
		objCon.pNR_VRS_CTC = arrSubGrupoCotacao(1)
		NR_CTC_SGRO = objCon.pNR_CTC_SGRO
		NR_VRS_CTC = objCon.pNR_VRS_CTC
	end if
end if

if not bSubgrupo or trim(request("NM_PRPN_CTC")) <> "" then 
%>
	<tr>
		<td colspan="2">
			<br>
			<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#DDDDDD">
				<tr>
					<td class="td_label_negrito" style="text-align:center">
						Cl�usulas Gerais
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
						<%
						objcon.pTipo = "1"
						set rsCobertura = objcon.mLerDadosClausula()
						if not rsCobertura.eof then
							%>
							<tr>
								<td class="td_label_negrito" width="80">C�d. C�usula</td>
								<td class="td_label_negrito">T�tulo</td>
							</tr>
							<%vClsl = 1
							do while not rsCobertura.eof
								alterna_cor
								%>
								<tr>
									<td class="td_dado" style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazioN(rsCobertura,"cd_clsl")%></td>
									<td class="td_dado" style="<%=cor_zebra%>">&nbsp;<a onclick="mostra_div(tr_<%=rsCobertura("CD_CLSL")%>_<%=vClsl%>)" style="text-decoration:none;color:#0000FF;cursor:hand"><%= testaDadoVazioN(rsCobertura,"descr_clausula")%></td>
								</tr>
								<tr id="tr_<%=rsCobertura("CD_CLSL")%>_<%=vClsl%>" style="display:none">
									<td class="td_dado" style="<%=cor_zebra%>">&nbsp;</td>
									<td class="td_dado" style="<%=cor_zebra%>"><%= Replace(testaDadoVazio(rsCobertura,"texto"), Chr(13) + Chr(10), "<br>") %></td>
								</tr>
								<%vClsl = vClsl + 1
								rsCobertura.movenext
							loop%>
							</table>
						<%else%>
							<tr>
								<td class="td_label" style="text-align:center">&nbsp;Nenhuma cl�usula relacionada.</td>
							</tr>
							</table>
						<%end if%>
					</td>
				</tr>
				<tr>
					<td class="td_label_negrito" style="text-align:center">
						Cl�usulas Especiais
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
						<%
						objcon.pTipo = "2"
						set rsCobertura = objcon.mLerDadosClausula()
						if not rsCobertura.eof then
						%>
							<tr>
								<td class="td_label_negrito" width="80">C�d. C�usula</td>
								<td class="td_label_negrito">T�tulo</td>
							</tr>
							<%
							do while not rsCobertura.eof
								alterna_cor
								%>
								<tr>
									<td class="td_dado" style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazioN(rsCobertura,"cd_clsl")%></td>
									<td class="td_dado" style="<%=cor_zebra%>">&nbsp;<a onclick="mostra_div(tr_<%=rsCobertura("CD_CLSL")%>_<%=vClsl%>)" style="text-decoration:none;color:#0000FF;cursor:hand"><%= testaDadoVazioN(rsCobertura,"descr_clausula")%></td>
								</tr>
								<tr id="tr_<%=rsCobertura("CD_CLSL")%>_<%=vClsl%>" style="display:none">
									<td class="td_dado" style="<%=cor_zebra%>">&nbsp;</td>
									<td class="td_dado" style="<%=cor_zebra%>">
									<%
									objCon.pCD_CLSL = rsCobertura("CD_CLSL")
									set rsTexto = objCon.mLerTextoClausula()
									if not rsTexto.eof then
										Response.Write( Replace( testaDadoVazio(rsTexto,"TEXTO"), Chr(13) + Chr(10), "<br>") )
									end if
									set rsTexto = nothing
									%></td>
								</tr>
								<%vClsl = vClsl + 1
								rsCobertura.movenext
							loop%>
							</table>
						<%else%>
							<tr>
								<td class="td_label" style="text-align:center">&nbsp;Nenhuma cl�usula relacionada.</td>
							</tr>
							</table>
						<%end if%>
					</td>
				</tr>
				<tr>
					<td class="td_label_negrito" style="text-align:center">
						Cl�usulas Facultativas
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
						<%
						objcon.pTipo = "3"
						set rsCobertura = objcon.mLerDadosClausula()
						if not rsCobertura.eof then
						%>
							<tr>
								<td class="td_label_negrito" width="20">&nbsp;</td>
								<td class="td_label_negrito" width="80">C�d. C�usula</td>
								<td class="td_label_negrito">T�tulo</td>
							</tr>
							<%
							chkcd_clsl = request("chkcd_clsl")
							if chkcd_clsl <> "" then
								Achkcd_clsl = split(chkcd_clsl,",")
								chkcd_clsl = "|"
								for f = 0 to ubound(Achkcd_clsl)
									chkcd_clsl = chkcd_clsl & trim(Achkcd_clsl(f)) & "|"
								next
							end if
							NR_SEQL_CLSL_CTC = 1
							do while not rsCobertura.eof
								alterna_cor
								%>
								<tr>
									<td class="td_dado" style="<%=cor_zebra%>" width="20">
										<input disabled type="checkbox" name="chkcd_clsl" value="<%= NR_SEQL_CLSL_CTC %>" <%
											if trim(testaDadoVazioN(rsCobertura,"NR_SEQL_CLSL_CTC")) <> "" then
												response.Write "checked"
											end if						
										%>>
										<input type="hidden" name="NR_SEQL_CLSL_CTC" value="<% = NR_SEQL_CLSL_CTC %>">
										<input type="hidden" name="CD_CLSC_CLSL_CTC" value="2">
										<input type="hidden" name="CD_OGM_CLSL_CTC" value="1">
										<input type="hidden" name="CD_CLSL" value="<%= testaDadoVazioN(rsCobertura,"CD_CLSL")%>">
									</td>
									<td class="td_dado" style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazioN(rsCobertura,"cd_clsl")%></td>
									<td class="td_dado" style="<%=cor_zebra%>">&nbsp;<a onclick="mostra_div(tr_<%=rsCobertura("CD_CLSL")%>_<%=vClsl%>)" style="text-decoration:none;color:#0000FF;cursor:hand"><%= testaDadoVazioN(rsCobertura,"descr_clausula")%></td>
								</tr>
								<tr id="tr_<%=rsCobertura("CD_CLSL")%>_<%=vClsl%>" style="display:none">
									<td class="td_dado" style="<%=cor_zebra%>">&nbsp;</td>
									<td class="td_dado" style="<%=cor_zebra%>">&nbsp;</td>
									<td class="td_dado" style="<%=cor_zebra%>"><%= Replace( testaDadoVazio(rsCobertura,"texto"), Chr(13) + Chr(10), "<br>") %></td>
								</tr>
								<%vClsl = vClsl + 1
								rsCobertura.movenext
								NR_SEQL_CLSL_CTC = NR_SEQL_CLSL_CTC + 1
							loop%>
							</table>
						<%else%>
							<tr>
								<td class="td_label" style="text-align:center">&nbsp;Nenhuma cl�usula relacionada.</td>
							</tr>
							</table>
						<%end if%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
<%end if%>
</table>
<script>
function esconde_div()
{
	var allDivs = new Array(<%=DivIds%>);
	for(i=0;i<allDivs.length;i++)
	{
		var tr = eval("tr_"+allDivs[i]);
		tr.style.display = 'none';
	}
}
</script>

