<!--#include virtual = "/padrao.asp"-->
<%
set rs = objcon.mLerDadosFinanceiros(1) '1 = RE

if request("primeiro") = "false" then
	VL_IPTC_MOEN_CTC = request("VL_IPTC_MOEN_CTC")
	VL_PREM_MOEN_CTC = request("VL_PREM_MOEN_CTC")
	VL_PRMO_MOEN_CTC = request("VL_PRMO_MOEN_CTC")
	VL_SGRA_MOEN_CTC = request("VL_SGRA_MOEN_CTC")
	VL_PCL_MOEN_CTC = request("VL_PCL_MOEN_CTC")
	VL_LQDO_MOEN_CTC = request("VL_LQDO_MOEN_CTC")
	VL_IPTC_MOEE_CTC = request("VL_IPTC_MOEE_CTC")
	VL_PREM_MOEE_CTC = request("VL_PREM_MOEE_CTC")
	VL_PRMO_MOEE_CTC = request("VL_PRMO_MOEE_CTC")
	VL_SGRA_MOEE_CTC = request("VL_SGRA_MOEE_CTC")
	VL_PCL_MOEE_CTC = request("VL_PCL_MOEE_CTC")
	VL_LQDO_MOEE_CTC = request("VL_LQDO_MOEE_CTC")
	Margem_Comercial = request("Margem_Comercial")
	QT_PCL_PGTO_CTC = request("QT_PCL_PGTO_CTC")
	VL_CST_APLC_CTC = request("VL_CST_APLC_CTC")
	PC_DSC_TCN_CTC = request("PC_DSC_TCN_CTC")
	VL_DSC_TCN_CTC = request("VL_DSC_TCN_CTC")
	PC_DSC_CML_CTC = request("PC_DSC_CML_CTC")
	VL_DSC_CML_CTC = request("VL_DSC_CML_CTC")
	VL_IOF_CTC = request("VL_IOF_CTC")
	CD_MVTC_FNCR_PREM = request("CD_MVTC_FNCR_PREM")
	VL_MVTC_FNCR_PREM = request("VL_MVTC_FNCR_PREM")
else

	CD_TIP_OPR_CTC = testaDadoVazioN(rs,"CD_TIP_OPR_CTC")
	VL_IPTC_MOEN_CTC = testaDadoVazioN(rs,"VL_IPTC_MOEN_CTC")
	VL_PREM_MOEN_CTC = testaDadoVazioN(rs,"VL_PREM_MOEN_CTC")
	
	VL_PRMO_MOEN_CTC = testaDadoVazioN(rs,"VL_PRMO_MOEN_CTC")
	VL_SGRA_MOEN_CTC = testaDadoVazioN(rs,"VL_SGRA_MOEN_CTC")
	VL_PCL_MOEN_CTC = testaDadoVazioN(rs,"VL_PCL_MOEN_CTC")
	VL_LQDO_MOEN_CTC = testaDadoVazioN(rs,"VL_LQDO_MOEN_CTC")
	VL_IPTC_MOEE_CTC = testaDadoVazioN(rs,"VL_IPTC_MOEE_CTC")
	VL_PREM_MOEE_CTC = testaDadoVazioN(rs,"VL_PREM_MOEE_CTC")
	VL_PRMO_MOEE_CTC = testaDadoVazioN(rs,"VL_PRMO_MOEE_CTC")
	VL_SGRA_MOEE_CTC = testaDadoVazioN(rs,"VL_SGRA_MOEE_CTC")
	VL_PCL_MOEE_CTC = testaDadoVazioN(rs,"VL_PCL_MOEE_CTC")
	VL_LQDO_MOEE_CTC = testaDadoVazioN(rs,"VL_LQDO_MOEE_CTC")
	VL_CST_APLC_CTC = testaDadoVazioN(rs,"VL_CST_APLC_CTC")
	VL_IOF_CTC = testaDadoVazioN(rs,"VL_IOF_CTC")

	PC_DSC_CML_CTC = testaDadoVazioN(rs,"PC_DSC_CML_CTC")
	VL_DSC_CML_CTC = testaDadoVazioN(rs,"VL_DSC_CML_CTC")

	PC_DSC_TCN_CTC = testaDadoVazioN(rs,"PC_DSC_TCN_CTC")
	Margem_Comercial = testaDadoVazioN(rs,"Margem_Comercial")
	QT_PCL_PGTO_CTC = testaDadoVazioN(rs,"QT_PCL_PGTO_CTC")
	VL_DSC_TCN_CTC = testaDadoVazioN(rs,"VL_DSC_TCN_CTC")

	CD_MVTC_FNCR_PREM = testaDadoVazioN(rs,"CD_MVTC_FNCR_PREM")
	VL_MVTC_FNCR_PREM = testaDadoVazioN(rs,"VL_MVTC_FNCR_PREM")
end if


if isnull(rs("VL_MVTC_FNCR_PREM")) or isnull(rs("CD_MVTC_FNCR_PREM")) then
	bCalculaLoad = true
else
	bCalculaLoad = false
end if

set rsCotacao = objcon.mLerCotacao()
data_fim_vigencia = rsCotacao("DT_INC_VGC_CTC")
data_inicio_vigencia = rsCotacao("DT_FIM_VGC_CTC")
set rsCotacao = nothing
if not isnull(data_fim_vigencia) and not isnull(data_inicio_vigencia) then
	dias = datediff("d",data_fim_vigencia,data_inicio_vigencia)
	parcelas = int(dias/30)
else
	dias = 0
	parcelas = QT_PCL_PGTO_CTC
end if

if trim(VL_IPTC_MOEN_CTC) = "" then
	VL_IPTC_MOEN_CTC = "0,00"
end if
if trim(VL_PREM_MOEN_CTC) = "" then
	VL_PREM_MOEN_CTC = "0,00"
end if
if trim(Margem_Comercial) = "" then
	Margem_Comercial = "0,00"
end if
%>
<script language="JavaScript">
<!--
function FormatNumber(vr,tamdec,truncate){
	//A fun��o espera receber casas decimais com V�RGULA
	// truncate = 1 - n�o altera o valor de casas decimais, caso seja maior do que o tamanho decidido
	// truncate = 2 - remove os valores superiores as casa decimais
	// truncate = 3 - remove os valores superiores as casa decimais e arredonda caso haja necessidade
	vp = '';
	for(f=0;f<vr.length;f++){
		if(((vr.charCodeAt(f)>=48)&&(vr.charCodeAt(f)<=57))||(vr.charCodeAt(f)==44)){
			vp=vp+vr.charAt(f);
		}
	}
	a=1;
	vr = vp;
	if(vr.length>0){
		vr = vr.split(',');
		vrIni = vr[0];
		vrIniTemp = '';
		for(f=0;f<vrIni.length;f++){
			vrIniTemp = vrIni.charAt(vrIni.length-f-1) + vrIniTemp;
			if((a==3)&&(vrIni.length!=f+1)){
				vrIniTemp = '.' + vrIniTemp;
				a=0;
			}
			a++;
		}
		vrIni = vrIniTemp;
		vrFim = "";
		if(vr.length>1){
			vrFim = vr[1];
			if(vrFim.length>tamdec){
				if(truncate==3){
					valorFinal = vrFim.substring(tamdec,tamdec + 1);
					vrFim = vrFim.substring(0,tamdec);
					if(valorFinal>4){
						vrFim = vrFim/1 + 1;
					}
				} else if(truncate==2){
					vrFim = vrFim.substring(0,tamdec);
				}
			} else {
				for(f=vrFim.length;f<tamdec;f++){
					vrFim = vrFim + '0';
				}
			}
		} else {
			for(f=0;f<tamdec;f++){
				vrFim = vrFim + '0';
			}
		}
		vr = vrIni + ',' + vrFim;
		return vr;
	}
}

function converteNumeroJavascript(valor){
	valor = valor.split(',');
	valorNovo = '';
	for(f=0;f<valor[0].length;f++){
		if((valor[0].charCodeAt(f)>=48)&&(valor[0].charCodeAt(f)<=57)){
			valorNovo=valorNovo+valor[0].charAt(f);
		}
	}
	if(valor.length>1){
		valorNovo = valorNovo + '.' + valor[1];
	}
	return valorNovo;
}

function ValidaMargemComercial()
{
	var margem_comercial = parseFloat('0' + converteNumeroJavascript(document.all.Margem_Comercial.value));

	if (margem_comercial > 100)
	{
		alert('A Margem Comercial n�o pode ser superior a 100,00');
		document.all.Margem_Comercial.value = '';
	}
	
}

function calculaValores()
{
	var percentual_desconto_tecnico = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_DSC_TCN_CTC.value));
	var percentual_iof = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_IOF_CTC.value));
	var percentual_corretagem = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_CRE_CTC.value));
	var valor_is = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_IPTC_MOEN_CTC.value));

	var premio_net = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_PREM_MOEN_CTC.value));
	var taxa_net = ((premio_net / valor_is) * 1000);

	if ((premio_net > 0) && (percentual_corretagem > 0))
		var premio_liquido = (premio_net / (1 - (percentual_corretagem / 100)));
	else
		var premio_liquido = 0;

	if (premio_liquido > 0)
		var custo_apolice_temp = (premio_liquido / 10);
	else
		var custo_apolice_temp = 0;

	if (custo_apolice_temp > 60)
		var custo_apolice = 60;
	else
		var custo_apolice = custo_apolice_temp;	

	if (percentual_iof > 0)
		var valor_iof = ((premio_liquido + custo_apolice) * (percentual_iof / 100));
	else
		var valor_iof = 0;

	var valor_premio_bruto = (premio_liquido + custo_apolice + valor_iof);
	
	document.formProc.VL_CST_APLC_CTC.value = FormatNumber(custo_apolice.toString().replace(".", ","), 2, 2);
	document.formProc.VL_IOF_CTC.value = FormatNumber(valor_iof.toString().replace(".", ","), 2, 2);
	document.formProc.VL_PREM_MOEN_CTC.value = FormatNumber(premio_net.toString().replace(".", ","), 2, 2);
	document.formProc.VL_LQDO_MOEN_CTC.value = FormatNumber(premio_liquido.toString().replace(".", ","), 2, 2);
	document.formProc.valor_premio_bruto.value = FormatNumber(valor_premio_bruto.toString().replace(".", ","), 2, 2);
}

function calculaPremioNetDescontoTecnico()
{
	var premio_net = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_PREM_MOEN_CTC.value));
	var percentual_desconto_tecnico = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_DSC_TCN_CTC.value));

	if (premio_net > 0)
		var premio_net_desconto_tecnico = (premio_net - ((premio_net * percentual_desconto_tecnico) / 100));
	else
		var premio_net_desconto_tecnico = premio_net;

	if (premio_net_desconto_tecnico > 0)
		var desconto_tecnico = (premio_net - premio_net_desconto_tecnico);
	else
		var desconto_tecnico = 0;

	document.formProc.premio_net_desconto_tecnico.value = FormatNumber(premio_net_desconto_tecnico.toString().replace(".", ","), 2, 2);
	document.formProc.VL_DSC_TCN_CTC.value = FormatNumber(desconto_tecnico.toString().replace(".", ","), 2, 2);
}

function calculaDiferencaMovimentacao()
{
	var premio_net = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_PREM_MOEN_CTC.value));
	var valor_premio = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_premio.value));

	var valor_diferenca = (premio_net - valor_premio);
	
	document.formProc.valor_diferenca.value = FormatNumber(valor_diferenca.toString().replace(".", ","), 2, 2);
}

function calculaMovimentacao()
{
	var valor_diferenca = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_diferenca.value));
	var qtd_dias_endosso = parseInt('0' + converteNumeroJavascript(document.formProc.qtd_dias_endosso.value));
	var qtd_dias_apolice = parseInt('0' + converteNumeroJavascript(document.formProc.qtd_dias_apolice.value));

	if (qtd_dias_apolice == 0)
		var valor_pro_rata = 0;
	else
		var valor_pro_rata = valor_diferenca * (qtd_dias_endosso / qtd_dias_apolice);

	if (valor_diferenca < 0)
		var mov_endosso = 2;
	else if (valor_diferenca > 0)
		var mov_endosso = 1;
	else
		var mov_endosso = 0;
		
	document.formProc.VL_MVTC_FNCR_PREM.value = FormatNumber(valor_pro_rata.toString().replace(".", ","), 2, 2);
	document.formProc.CD_MVTC_FNCR_PREM.selectedIndex = mov_endosso;
	
	processaMovimentacao();
}

function processaMovimentacao()
{
	var iCodMov = parseInt('0' + document.formProc.CD_MVTC_FNCR_PREM.value);

	if (iCodMov == 1)
		document.formProc.VL_MVTC_FNCR_PREM.value = '0,00';

	if (iCodMov == 2)
		var sDisplay = 'none';
	else
		var sDisplay = '';

	try
	{
		document.all.mov.style.display = sDisplay;
	}
	catch(ex)
	{
		var iMov = document.all.mov.length;
		for (var i = 0; i < iMov; i++)
			document.all.mov[i].style.display = sDisplay;
	}
}

function submitdadosfinanceiros()
{
	document.formProc.action = 'liberar_cotacao_fin.asp';
	document.formProc.submit();
}

function alterarDados()
{
	document.formProc.salvarfin.value = 'S';
	submitdadosfinanceiros();
}
-->
</script>
<%

if trim(request("salvarfin")) = "S" then
	set rsCotacao = objCon.mLerCotacao

	if ucase(rsCotacao("CD_ULT_EVT_CTC")) <> "T" then
		objcon.pCD_ULT_EVT_CTC = "T"
		bAtualizaCotacao = true
	else
		bAtualizaCotacao = false
	end if
	
	objCon.pVL_IPTC_MOEN_CTC = VL_IPTC_MOEN_CTC
	objCon.pVL_PREM_MOEN_CTC = VL_PREM_MOEN_CTC
	objCon.pVL_PRMO_MOEN_CTC = VL_PRMO_MOEN_CTC
	objCon.pVL_SGRA_MOEN_CTC = VL_SGRA_MOEN_CTC
	objCon.pVL_PCL_MOEN_CTC = VL_PCL_MOEN_CTC
	objCon.pVL_LQDO_MOEN_CTC = VL_LQDO_MOEN_CTC
	objCon.pVL_IPTC_MOEE_CTC = VL_IPTC_MOEE_CTC
	objCon.pVL_PREM_MOEE_CTC = VL_PREM_MOEE_CTC
	objCon.pVL_PRMO_MOEE_CTC = VL_PRMO_MOEE_CTC
	objCon.pVL_SGRA_MOEE_CTC = VL_SGRA_MOEE_CTC
	objCon.pVL_PCL_MOEE_CTC = VL_PCL_MOEE_CTC
	objCon.pVL_LQDO_MOEE_CTC = VL_LQDO_MOEE_CTC
	objCon.pMargem_Comercial = Margem_Comercial
	objCon.pQT_PCL_PGTO_CTC = QT_PCL_PGTO_CTC
	objCon.pVL_CST_APLC_CTC = VL_CST_APLC_CTC
	objCon.pPC_DSC_TCN_CTC = PC_DSC_TCN_CTC
	objCon.pVL_DSC_TCN_CTC = VL_DSC_TCN_CTC
	objCon.pPC_DSC_CML_CTC = PC_DSC_CML_CTC
	objCon.pVL_DSC_CML_CTC = VL_DSC_CML_CTC
	objCon.pVL_IOF_CTC = VL_IOF_CTC
	objCon.pCD_MVTC_FNCR_PREM = CD_MVTC_FNCR_PREM
	objCon.pVL_MVTC_FNCR_PREM = VL_MVTC_FNCR_PREM

	chave
	objCon.pWF_ID = WF_ID

	retorno = objCon.mAtualizarDadosFinanceiros(bAtualizaCotacao)
	if trim(retorno) <> "" then
		Response.redirect("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
		Response.End 
	end if
	
	chave
end if

if isnull(rs("VL_IPTC_MOEN_CTC")) then 
	VL_IPTC_MOEN_CTC = 0
else
	VL_IPTC_MOEN_CTC = rs("VL_IPTC_MOEN_CTC")
end if

if isnull(rs("VL_PREM_MOEN_CTC")) then 
	VL_PREM_MOEN_CTC = 0
else
	VL_PREM_MOEN_CTC = rs("VL_PREM_MOEN_CTC")
end if

set rsSubgrupo = objcon.mLerSubgrupoCotacao

if not rsSubgrupo.eof then
	do while not rsSubgrupo.eof

		objCon.pNR_CTC_SGRO = rsSubgrupo("NR_CTC_SGRO")
		objCon.pNR_VRS_CTC =  rsSubgrupo("NR_VRS_CTC")

		set rs = objcon.mLerDadosFinanceiros(1) '1 = RE

		if not rs.eof then
			if not isnull(rs("VL_IPTC_MOEN_CTC")) then
				VL_IPTC_MOEN_CTC = cdbl(VL_IPTC_MOEN_CTC) + cdbl(rs("VL_IPTC_MOEN_CTC"))
			end if

			if not isnull(rs("VL_PREM_MOEN_CTC")) then
				VL_PREM_MOEN_CTC = cdbl(VL_PREM_MOEN_CTC) + cdbl(rs("VL_PREM_MOEN_CTC"))
			end if
		end if

		rsSubgrupo.movenext
	loop
end if

VL_IPTC_MOEN_CTC = formatnumber(VL_IPTC_MOEN_CTC,2)
VL_PREM_MOEN_CTC = formatnumber(VL_PREM_MOEN_CTC,2)

valor_premio_minimo = trim(request("valor_premio_minimo"))
if valor_premio_minimo = "" then
	set rsEndossoCotacao = objCon.mLerDadosEndossoCotacao()
	if rsEndossoCotacao.eof then
		valor_premio_minimo = 0
	else
		valor_premio_minimo = testaDadoVazioN(rsEndossoCotacao,"VL_PREM_MIN_EDS")
	end if
end if

set rsEndosso = objCon.mLerDadosEndosso()
if not rsEndosso.eof then
	qtd_dias_endosso = datediff("d", rsEndosso("DT_INC_VGC_CTC"), rsEndosso("DT_FIM_VGC_CTC"))
	qtd_dias_apolice = datediff("d", rsEndosso("DT_INC_VGC_CTR"), rsEndosso("DT_FIM_VGC_CTR"))
end if
%>
	<input type="hidden" name="primeiro" value="false">
	<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
		<tr>
			<td colspan="2" width="50%">
				<br>
				&nbsp;&nbsp;&nbsp;<input type="button" name="alterar" value="Gravar" style="width:80px;font-size:10px;margin: 2px 2px 2px 2px;" onclick="alterarDados()">
				<input type="hidden" name="salvarfin" value="N">
				<br><br>
			</td>
			<td width="25%"></td>
			<td width="25%">
				&nbsp;&nbsp;&nbsp;<input type="button" name="calcular" value="Calcular" style="width:80px;font-size:10px;margin: 2px 2px 2px 2px;" onclick="calculaMovimentacao()">
			</td>
		</tr>
		<tr>
			<td width="25%" nowrap class="td_label">&nbsp;Moeda do seguro da cota��o</td>
			<td width="25%" class="td_dado" width="800">&nbsp;
				<%=testaDadoVazio(rs,"CD_MOE_SGRO_CTC")%> - <%=testaDadoVazio(rs,"NOME_MOE_SGRO")%>
			</td>
			<td nowrap class="td_label">&nbsp;Pr�mio m�nimo para endosso</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="valor_premio_minimo" size="16" value="<%=formatnumber(valor_premio_minimo,2)%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>												
		<tr>
			<td nowrap class="td_label">&nbsp;Moeda de origem da cota��o</td>
			<td class="td_dado">&nbsp;
				<%=testaDadoVazio(rs,"CD_MOE_OGM_CTC")%> - <%=testaDadoVazio(rs,"NOME_MOE_OGM")%>
			</td>
			<td nowrap class="td_label">&nbsp;Valor IS atual</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_IPTC_MOEN_CTC" size="16" value="<%=VL_IPTC_MOEN_CTC%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual do custo de ap�lice</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_CST_APLC_CTC" size="16" value="<%=testaDadoVazioN(rs,"PC_CST_APLC_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
			<td nowrap class="td_label">&nbsp;Movimenta��o do endosso</td>
			<td class="td_dado">&nbsp;
				<select name="CD_MVTC_FNCR_PREM" onchange="processaMovimentacao();">
					<option value="1"<%if trim(CD_MVTC_FNCR_PREM) = "1" then Response.Write " selected"%>>Sem movimenta��o</option>
					<option value="2"<%if trim(CD_MVTC_FNCR_PREM) = "2" then Response.Write " selected"%>>Com cobran�a</option>
					<option value="3"<%if trim(CD_MVTC_FNCR_PREM) = "3" then Response.Write " selected"%>>Com devolu��o</option>
				</select>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual IOF</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_IOF_CTC" size="16" value="<%=testaDadoVazioN(rs,"PC_IOF_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
			<td nowrap class="td_label">&nbsp;Valor pr�mio NET devido</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_PREM_MOEN_CTC" size="16" value="<%=VL_PREM_MOEN_CTC%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual de corretagem</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_CRE_CTC" size="16" value="<%=testaDadoVazioN(rs,"PC_CRE_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
			<td nowrap class="td_label">&nbsp;Valor pr�mio NET pago</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="valor_premio" size="16" value="<%=testaDadoVazioN(rsEndosso,"VL_PREMIO")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual desconto t�cnico</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_DSC_TCN_CTC" size="16" value="<%=PC_DSC_TCN_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
			<td nowrap class="td_label">&nbsp;Valor da diferen�a</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="valor_diferenca" size="16" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Margem Comercial&nbsp;</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="Margem_Comercial" size="16" value="<%=Margem_Comercial%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
			<td nowrap class="td_label">&nbsp;Valor desconto t�cnico</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_DSC_TCN_CTC" size="16" value="<%=VL_DSC_TCN_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Qtd m�xima parcelas pgto do pr�mio</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="QT_PCL_PGTO_CTC" size="2" value="<%=QT_PCL_PGTO_CTC%>" maxlength="2" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
			<td nowrap class="td_label">&nbsp;Valor comiss�o</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="XXXXXXXXX" size="16" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td nowrap class="td_label">&nbsp;Valor do pr�mio l�quido</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_LQDO_MOEN_CTC" size="16" value="<%=VL_LQDO_MOEN_CTC%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr id="mov">
			<td></td>
			<td></td>
			<td nowrap class="td_label">&nbsp;Valor do custo da ap�lice</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_CST_APLC_CTC" size="16" value="<%=VL_CST_APLC_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td nowrap class="td_label">&nbsp;Valor pro-rata</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_MVTC_FNCR_PREM" size="16" value="<%=replace(VL_MVTC_FNCR_PREM,"-","")%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right;">
			</td>
		</tr>
		<tr>
			<td colspan="4" class="td_dado">
				&nbsp;<font color="red"><span id="aviso">
				<%
				retorno = objCon.mValidarCapitalSegurado(VL_IPTC_MOEN_CTC)
				if isnumeric(retorno) then
					if cint(retorno) = -1 then
						Response.Write "O Valor IS � menor que o m�nimo cadastrado"
					elseif cint(retorno) = 1 then
						Response.Write "O Valor IS � maior que o m�ximo cadastrado"
					end if
				elseif trim(retorno) <> "" then
					Response.redirect("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
					Response.End 
				end if
				%>
				</span></font>
			</td>
		</tr>
	</table>
	<input type="hidden" value="<%=IN_PG_ATO_CTC %>">
	<input type="hidden" name="VL_PRMO_MOEN_CTC" value="<%=VL_PRMO_MOEN_CTC%>">
	<input type="hidden" name="VL_SGRA_MOEN_CTC" value="<%=VL_PREM_MOEN_CTC%>">
	<input type="hidden" name="VL_PCL_MOEN_CTC" value="<%=VL_PCL_MOEN_CTC%>">
	<input type="hidden" name="PC_DSC_CML_CTC" value="<%=PC_DSC_CML_CTC%>">
	<input type="hidden" name="VL_DSC_CML_CTC" value="<%=VL_DSC_CML_CTC%>">
	<input type="hidden" name="qtd_dias_endosso" value="<%=qtd_dias_endosso%>">
	<input type="hidden" name="qtd_dias_apolice" value="<%=qtd_dias_apolice%>">
	<input type="hidden" name="VL_IOF_CTC" value="<%=VL_IOF_CTC%>">
	<input type="hidden" name="premio_net_desconto_tecnico" value="">
	<input type="hidden" name="valor_premio_bruto" value="">
	
	<script language="JavaScript">
	calculaValores();
	calculaPremioNetDescontoTecnico();
	calculaDiferencaMovimentacao();
	
	<%if bCalculaLoad then%>
		calculaMovimentacao();
	<%else%>
		processaMovimentacao();
	<%end if%>
	</script>