<!--#include virtual = "/padrao.asp"-->
<%
Set rs = objCon.mLerDadosFinanceiros(1) '1 = RE
rs.ActiveConnection = Nothing
If NOT rs.EOF Then
    If Request("primeiro") = "false" Then
 	   VL_IPTC_MOEN_CTC = Request("VL_IPTC_MOEN_CTC")
	   VL_PREM_MOEN_CTC = request("VL_PREM_MOEN_CTC")
	   VL_PRMO_MOEN_CTC = request("VL_PRMO_MOEN_CTC")
	   VL_SGRA_MOEN_CTC = request("VL_SGRA_MOEN_CTC")
	   VL_PCL_MOEN_CTC  = request("VL_PCL_MOEN_CTC")
	   VL_LQDO_MOEN_CTC = request("VL_LQDO_MOEN_CTC")
	   VL_IPTC_MOEE_CTC = request("VL_IPTC_MOEE_CTC")
	   VL_PREM_MOEE_CTC = request("VL_PREM_MOEE_CTC")
	   VL_PRMO_MOEE_CTC = request("VL_PRMO_MOEE_CTC")
	   VL_SGRA_MOEE_CTC = request("VL_SGRA_MOEE_CTC")
	   VL_PCL_MOEE_CTC  = request("VL_PCL_MOEE_CTC")
	   VL_LQDO_MOEE_CTC = request("VL_LQDO_MOEE_CTC")
	   Margem_Comercial = request("Margem_Comercial")
	   QT_PCL_PGTO_CTC  = request("QT_PCL_PGTO_CTC")
	   VL_CST_APLC_CTC  = request("VL_CST_APLC_CTC")
	   PC_DSC_TCN_CTC   = request("PC_DSC_TCN_CTC")
	   VL_DSC_TCN_CTC   = request("VL_DSC_TCN_CTC")
	   PC_DSC_CML_CTC   = request("PC_DSC_CML_CTC")
	   VL_DSC_CML_CTC   = request("VL_DSC_CML_CTC")
	   VL_IOF_CTC       = request("VL_IOF_CTC")
	Else

	   CD_TIP_OPR_CTC   = testaDadoVazioN(rs,"CD_TIP_OPR_CTC")
	   VL_IPTC_MOEN_CTC = testaDadoVazioN(rs,"VL_IPTC_MOEN_CTC")
	   VL_PREM_MOEN_CTC = testaDadoVazioN(rs,"VL_PREM_MOEN_CTC")
	   VL_PRMO_MOEN_CTC = testaDadoVazioN(rs,"VL_PRMO_MOEN_CTC")
	   VL_SGRA_MOEN_CTC = testaDadoVazioN(rs,"VL_SGRA_MOEN_CTC")
	   VL_PCL_MOEN_CTC  = testaDadoVazioN(rs,"VL_PCL_MOEN_CTC")
	   VL_LQDO_MOEN_CTC = testaDadoVazioN(rs,"VL_LQDO_MOEN_CTC")
	   If VL_LQDO_MOEN_CTC = "" Then VL_LQDO_MOEN_CTC = 0 End If
	   VL_IPTC_MOEE_CTC = testaDadoVazioN(rs,"VL_IPTC_MOEE_CTC")
	   VL_PREM_MOEE_CTC = testaDadoVazioN(rs,"VL_PREM_MOEE_CTC")
	   VL_PRMO_MOEE_CTC = testaDadoVazioN(rs,"VL_PRMO_MOEE_CTC")
	   VL_SGRA_MOEE_CTC = testaDadoVazioN(rs,"VL_SGRA_MOEE_CTC")
	   VL_PCL_MOEE_CTC  = testaDadoVazioN(rs,"VL_PCL_MOEE_CTC")
	   VL_LQDO_MOEE_CTC = testaDadoVazioN(rs,"VL_LQDO_MOEE_CTC")
	   VL_CST_APLC_CTC  = testaDadoVazioN(rs,"VL_CST_APLC_CTC")
	   VL_IOF_CTC       = testaDadoVazioN(rs,"VL_IOF_CTC")
	   PC_DSC_CML_CTC   = testaDadoVazioN(rs,"PC_DSC_CML_CTC")
	   VL_DSC_CML_CTC   = testaDadoVazioN(rs,"VL_DSC_CML_CTC")
	   PC_DSC_TCN_CTC   = testaDadoVazioN(rs,"PC_DSC_TCN_CTC")
 	   Margem_Comercial = testaDadoVazioN(rs,"Margem_Comercial")
	   QT_PCL_PGTO_CTC  = testaDadoVazioN(rs,"QT_PCL_PGTO_CTC")
	   VL_DSC_TCN_CTC   = testaDadoVazioN(rs,"VL_DSC_TCN_CTC")
	   PC_CRE_PDRO      = testaDadoVazioN(rs,"PC_CRE_CTC")
    End If

    Set rsCotacao = objcon.mLerCotacao()
    rsCotacao.ActiveConnection = Nothing
    data_fim_vigencia     = rsCotacao("DT_INC_VGC_CTC")
    data_inicio_vigencia  = rsCotacao("DT_FIM_VGC_CTC")
    val_prem_min_prpt     = CDbl(rsCotacao("VL_PREM_MIN_PRPT"))
    num_parc_max_pmt      = rsCotacao("NR_MAX_PCL_PMT")
    perc_custo_apolice    = rsCotacao("PC_CST_APLC")
    val_max_custo_apolice = rsCotacao("VL_MAX_CST_APLC")
    Set rsCotacao = Nothing

    If NOT isnull(data_fim_vigencia) And NOT isnull(data_inicio_vigencia) Then
	   dias = datediff("d",data_fim_vigencia,data_inicio_vigencia)
 	   parcelas = int(dias/30)
	   QT_PCL_PGTO_CTC = parcelas
    Else
	   dias = 0
	   parcelas = QT_PCL_PGTO_CTC
    End If
    If num_parc_max_pmt < parcelas Then
       parcelas = num_parc_max_pmt
       QT_PCL_PGTO_CTC = parcelas
    End If

    If NOT IsNull(val_prem_min_prpt) And val_prem_min_prpt > 0 And Len(parcelas) > 0 Then
       parcelasx = VL_LQDO_MOEN_CTC / val_prem_min_prpt
       If parcelasx < parcelas Then
          QT_PCL_PGTO_CTC = parcelasx
       End If
    End If
    If Trim(VL_IPTC_MOEN_CTC) = "" Then
	   VL_IPTC_MOEN_CTC = "0,00"
    End If
    If Trim(VL_PREM_MOEN_CTC) = "" Then
	   VL_PREM_MOEN_CTC = "0,00"
    End If
    If Trim(Margem_Comercial) = "" Then
	   Margem_Comercial = "0,00"
    End If
    If Trim(PC_DSC_TCN_CTC) = "" Then
       PC_DSC_TCN_CTC = "0,00"
    End If
%>
<script language="JavaScript">
<!--
function FormatNumber(vr,tamdec,truncate){
	//A fun��o espera receber casas decimais com V�RGULA
	// truncate = 1 - n�o altera o valor de casas decimais, caso seja maior do que o tamanho decidido
	// truncate = 2 - remove os valores superiores as casa decimais
	// truncate = 3 - remove os valores superiores as casa decimais e arredonda caso haja necessidade
	vp = '';
	for(f=0;f<vr.length;f++){
		if(((vr.charCodeAt(f)>=48)&&(vr.charCodeAt(f)<=57))||(vr.charCodeAt(f)==44)){
			vp=vp+vr.charAt(f);
		}
	}
	a=1;
	vr = vp;
	if(vr.length>0){
		vr = vr.split(',');
		vrIni = vr[0];
		vrIniTemp = '';
		for(f=0;f<vrIni.length;f++){
			vrIniTemp = vrIni.charAt(vrIni.length-f-1) + vrIniTemp;
			if((a==3)&&(vrIni.length!=f+1)){
				vrIniTemp = '.' + vrIniTemp;
				a=0;
			}
			a++;
		}
		vrIni = vrIniTemp;
		vrFim = "";
		if(vr.length>1){
			vrFim = vr[1];
			if(vrFim.length>tamdec){
				if(truncate==3){
					valorFinal = vrFim.substring(tamdec,tamdec + 1);
					vrFim = vrFim.substring(0,tamdec);
					if(valorFinal>4){
						vrFim = vrFim/1 + 1;
					}
				} else if(truncate==2){
					vrFim = vrFim.substring(0,tamdec);
				}
			} else {
				for(f=vrFim.length;f<tamdec;f++){
					vrFim = vrFim + '0';
				}
			}
		} else {
			for(f=0;f<tamdec;f++){
				vrFim = vrFim + '0';
			}
		}
		vr = vrIni + ',' + vrFim;
		return vr;
	}
}

function converteNumeroJavascript(valor){
	valor = valor.split(',');
	valorNovo = '';
	for(f=0;f<valor[0].length;f++){
		if((valor[0].charCodeAt(f)>=48)&&(valor[0].charCodeAt(f)<=57)){
			valorNovo=valorNovo+valor[0].charAt(f);
		}
	}
	if(valor.length>1){
		valorNovo = valorNovo + '.' + valor[1];
	}
	return valorNovo;
}

function calculaMovimentacao()
{
	var desconto_tecnico = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_DSC_TCN_CTC.value));
    var desconto_comercial = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_DSC_CML_CTC.value));
	var percentual_iof = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_IOF_CTC.value));
	var percentual_corretagem = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_CRE_CTC.value));
	var perc_custo_apolice = parseFloat('0' + converteNumeroJavascript(document.formProc.perc_custo_apolice.value));
	var val_max_custo_apolice = parseFloat('0' + converteNumeroJavascript(document.formProc.val_max_custo_apolice.value));

	var premio_net = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_PREM_MOEN_CTC.value));
    var premio_net_desc = premio_net - desconto_tecnico;

	if ((premio_net > 0) && (percentual_corretagem > 0))
		var premio_liquido = (premio_net_desc / (1 - (percentual_corretagem / 100))) - desconto_comercial;
	else
		var premio_liquido = 0;

    var custo_apolice = 0;
	if (parseFloat(premio_liquido) > 0) custo_apolice = premio_liquido * perc_custo_apolice / 100;
    if (parseFloat(custo_apolice) < 0)  custo_apolice = 0;
    if (parseFloat(custo_apolice) > parseFloat(val_max_custo_apolice)) custo_apolice = val_max_custo_apolice;
 
	var valor_iof = 0;
	if (percentual_iof > 0)          valor_iof = ((premio_liquido + custo_apolice) * (percentual_iof / 100));
    if (parseFloat(valor_iof)<0)     valor_iof = 0;

	var valor_premio_bruto = (premio_liquido + custo_apolice + valor_iof);
	
	var val_min      = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_prem_min_prpt.value));
    var par_por_vig  = parseInt(converteNumeroJavascript(document.formProc.qtd_parc_por_vigencia.value));
    val_min = premio_liquido / val_min;
    val_min = parseInt(val_min);
    if(val_min<par_por_vig) par_por_vig=val_min;
    document.formProc.QT_PCL_PGTO_CTC.value = par_por_vig.toString();

    document.formProc.premio_net_desconto_tecnico.value = FormatNumber(premio_net_desc.toString().replace(".", ","), 2, 2);
	document.formProc.VL_CST_APLC_CTC.value = FormatNumber(custo_apolice.toString().replace(".", ","), 2, 2);
	document.formProc.VL_IOF_CTC.value = FormatNumber(valor_iof.toString().replace(".", ","), 2, 2);
	document.formProc.VL_PREM_MOEN_CTC.value = FormatNumber(premio_net.toString().replace(".", ","), 2, 2);
	document.formProc.VL_LQDO_MOEN_CTC.value = FormatNumber(premio_liquido.toString().replace(".", ","), 2, 2);
	document.formProc.valor_premio_bruto.value = FormatNumber(valor_premio_bruto.toString().replace(".", ","), 2, 2);
}

function submitdadosfinanceiros()
{
	document.formProc.action = 'concluir_negocio_fin.asp';
	document.formProc.submit();
}

function alterarDados()
{
	var margem_comercial = parseFloat('0' + converteNumeroJavascript(document.all.Margem_Comercial.value));

	if (margem_comercial == 0)
	{
		if (!confirm('A Margem Comercial deveria ser maior que 0,00, o valor est� correto?'))
			return;
	}
	
	document.formProc.salvarfin.value = 'S';
	submitdadosfinanceiros();
}
-->
</script>
<%
  If Trim(request("salvarfin")) = "S" Then
 	 Set rsCotacao = objCon.mLerCotacao
     rsCotacao.ActiveConnection = Nothing
	 If UCase(rsCotacao("CD_ULT_EVT_CTC")) = "T" Then
		bAtualizaCotacao = False
	 Else
		objcon.pCD_ULT_EVT_CTC = "T"
		bAtualizaCotacao = True
	 End If
	
	 objCon.pVL_IPTC_MOEN_CTC = VL_IPTC_MOEN_CTC
	 objCon.pVL_PREM_MOEN_CTC = VL_PREM_MOEN_CTC
	 objCon.pVL_PRMO_MOEN_CTC = VL_PRMO_MOEN_CTC
	 objCon.pVL_SGRA_MOEN_CTC = VL_SGRA_MOEN_CTC
	 objCon.pVL_PCL_MOEN_CTC  = VL_PCL_MOEN_CTC
	 objCon.pVL_LQDO_MOEN_CTC = VL_LQDO_MOEN_CTC
	 objCon.pVL_IPTC_MOEE_CTC = VL_IPTC_MOEE_CTC
	 objCon.pVL_PREM_MOEE_CTC = VL_PREM_MOEE_CTC
	 objCon.pVL_PRMO_MOEE_CTC = VL_PRMO_MOEE_CTC
	 objCon.pVL_SGRA_MOEE_CTC = VL_SGRA_MOEE_CTC
	 objCon.pVL_PCL_MOEE_CTC  = VL_PCL_MOEE_CTC
	 objCon.pVL_LQDO_MOEE_CTC = VL_LQDO_MOEE_CTC
	 objCon.pMargem_Comercial = Margem_Comercial
	 objCon.pQT_PCL_PGTO_CTC  = QT_PCL_PGTO_CTC
	 objCon.pVL_CST_APLC_CTC  = VL_CST_APLC_CTC
	 objCon.pPC_DSC_TCN_CTC   = PC_DSC_TCN_CTC
	 objCon.pVL_DSC_TCN_CTC   = VL_DSC_TCN_CTC
	 objCon.pPC_DSC_CML_CTC   = PC_DSC_CML_CTC
	 objCon.pVL_DSC_CML_CTC   = VL_DSC_CML_CTC
	 objCon.pVL_IOF_CTC       = VL_IOF_CTC

	 chave
	 objCon.pWF_ID = WF_ID
 	 retorno = objCon.mAtualizarDadosFinanceiros(bAtualizaCotacao)
	 if trim(retorno) <> "" then
		Response.redirect("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
		Response.End 
	 End if
	 
	 chave
 	 objCon.pWF_ID = WF_ID
	 PC_CRE_PDRO = Trim(Request("PC_CRE_CTC"))

	 retorno = objCon.mAlterarPercentualCorretagem(PC_CRE_PDRO)
	 if trim(retorno) <> "" then
		Response.redirect("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
		Response.End 
	 end if
  End If

  If isnull(rs("VL_IPTC_MOEN_CTC")) Then 
	 VL_IPTC_MOEN_CTC = 0
  Else
	 VL_IPTC_MOEN_CTC = rs("VL_IPTC_MOEN_CTC")
  End If

  If isnull(rs("VL_PREM_MOEN_CTC")) Then 
	 VL_PREM_MOEN_CTC = 0
  Else
	 VL_PREM_MOEN_CTC = rs("VL_PREM_MOEN_CTC")
  End If
  
End If%>
	<br>
	&nbsp;&nbsp;&nbsp;<input type="button" name="alterar" value="Gravar" style="width:80px;font-size:10px;margin: 2px 2px 2px 2px;" onclick="alterarDados()">
	&nbsp;&nbsp;&nbsp;<input type="button" name="calcular" value="Calcular" style="width:80px;font-size:10px;margin: 2px 2px 2px 2px;" onclick="calculaMovimentacao()">
	<input type="hidden" name="salvarfin" value="N">
	<br><br>
	<input type="hidden" name="primeiro" value="false">
	<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
		<tr>
			<td nowrap class="td_label">&nbsp;Moeda do seguro da cota��o</td>
			<td class="td_dado" width="800">&nbsp;
				<%=testaDadoVazio(rs,"CD_MOE_SGRO_CTC")%> - <%=testaDadoVazio(rs,"NOME_MOE_SGRO")%>
			</td>
		</tr>												
		<tr>
			<td nowrap class="td_label">&nbsp;Moeda de origem da cota��o</td>
			<td class="td_dado">&nbsp;
				<%=testaDadoVazio(rs,"CD_MOE_OGM_CTC")%> - <%=testaDadoVazio(rs,"NOME_MOE_OGM")%>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual do custo de ap�lice</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_CST_APLC_CTC" size="16" value="<%=testaDadoVazioN(rs,"PC_CST_APLC_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual IOF</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_IOF_CTC" size="16" value="<%=testaDadoVazioN(rs,"PC_IOF_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual de corretagem</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_CRE_CTC" size="16" value="<%=PC_CRE_PDRO%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right;">
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual desconto t�cnico</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_DSC_TCN_CTC" size="16" value="<%=PC_DSC_TCN_CTC%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Margem Comercial&nbsp;</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="Margem_Comercial" size="16" value="<%=Margem_Comercial%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Qtd m�xima parcelas pgto do pr�mio</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="QT_PCL_PGTO_CTC" size="2" value="<%=QT_PCL_PGTO_CTC%>" maxlength="2" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="qtd_parc_por_vigencia" value="<%=parcelas%>">
				<input type="hidden" name="valor_prem_min_prpt" value="<%=valor_prem_min_prpt%>">
				<input type="hidden" name="perc_custo_apolice" value="<%=perc_custo_apolice%>">
				<input type="hidden" name="val_max_custo_apolice" value="<%=val_max_custo_apolice%>">
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
<%
Set rsSubgrupo = objcon.mLerSubgrupoCotacao
rsSubGrupo.ActiveConnection = Nothing

If not rsSubgrupo.EOF Then
	Do while not rsSubgrupo.EOF

		objCon.pNR_CTC_SGRO = rsSubgrupo("NR_CTC_SGRO")
		objCon.pNR_VRS_CTC =  rsSubgrupo("NR_VRS_CTC")

		Set rs = objcon.mLerDadosFinanceiros(1) '1 = RE
		rs.ActiveConnection = Nothing

		if not rs.EOF Then
			if not isnull(rs("VL_IPTC_MOEN_CTC")) then
				VL_IPTC_MOEN_CTC = cdbl(VL_IPTC_MOEN_CTC) + cdbl(rs("VL_IPTC_MOEN_CTC"))
			end if

			if not isnull(rs("VL_PREM_MOEN_CTC")) then
				VL_PREM_MOEN_CTC = cdbl(VL_PREM_MOEN_CTC) + cdbl(rs("VL_PREM_MOEN_CTC"))
			end if
		end if

		rsSubgrupo.MoveNext
	Loop
End If

VL_IPTC_MOEN_CTC = formatnumber(VL_IPTC_MOEN_CTC,2)
VL_PREM_MOEN_CTC = formatnumber(VL_PREM_MOEN_CTC,2)%>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor IS</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_IPTC_MOEN_CTC" size="16" value="<%=VL_IPTC_MOEN_CTC%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
				&nbsp;&nbsp;&nbsp;<font color="red"><span id="aviso">
				<%
				retorno = objCon.mValidarCapitalSegurado(CStr(VL_IPTC_MOEN_CTC))
				if isnumeric(retorno) then
					if cint(retorno) = -1 then
						Response.Write "O Valor IS � menor que o m�nimo cadastrado"
					elseif cint(retorno) = 1 then
						Response.Write "O Valor IS � maior que o m�ximo cadastrado"
					end if
				elseif trim(retorno) <> "" then
					Response.Write "<p>OCORREU UM ERRO.<BR>" & retorno
					'Response.redirect("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
					Response.End 
				end if
				%>
				</span></font>	
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor pr�mio NET</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_PREM_MOEN_CTC" size="16" value="<%=VL_PREM_MOEN_CTC%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly></td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor desconto t�cnico</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_DSC_TCN_CTC" size="16" value="<%=VL_DSC_TCN_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor pr�mio NET com desconto t�cnico</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="premio_net_desconto_tecnico" size="16" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<!--tr>
			<td nowrap class="td_label">&nbsp;Valor de corretagem</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="valor_corretagem" size="16" value="" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr-->
		<tr>
			<td nowrap class="td_label">&nbsp;Valor do pr�mio l�quido</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_LQDO_MOEN_CTC" size="16" value="<%=VL_LQDO_MOEN_CTC%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly></td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor do custo da ap�lice</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_CST_APLC_CTC" size="16" value="<%=VL_CST_APLC_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor IOF</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_IOF_CTC" size="16" value="<%=VL_IOF_CTC%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor do pr�mio bruto</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="valor_premio_bruto" size="16" value="" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
	</table>
	<input type="hidden" value="<%=IN_PG_ATO_CTC%>">
	<input type="hidden" name="VL_PRMO_MOEN_CTC" value="<%=VL_PRMO_MOEN_CTC%>">
	<input type="hidden" name="VL_SGRA_MOEN_CTC" value="<%=VL_PREM_MOEN_CTC%>">
	<input type="hidden" name="VL_PCL_MOEN_CTC" value="<%=VL_PCL_MOEN_CTC%>">
	<input type="hidden" name="PC_DSC_CML_CTC" value="<%=PC_DSC_CML_CTC%>">
	<input type="hidden" name="VL_DSC_CML_CTC" value="<%=VL_DSC_CML_CTC%>">
	
	<script language="JavaScript">
	calculaMovimentacao();
	</script>