<!--#include virtual = "/padrao.asp"-->
<script language="JavaScript">
<!--
function FormatNumber(vr,tamdec,truncate){
	//A fun��o espera receber casas decimais com V�RGULA
	// truncate = 1 - n�o altera o valor de casas decimais, caso seja maior do que o tamanho decidido
	// truncate = 2 - remove os valores superiores as casa decimais
	// truncate = 3 - remove os valores superiores as casa decimais e arredonda caso haja necessidade
	vp = '';
	for(f=0;f<vr.length;f++){
		if(((vr.charCodeAt(f)>=48)&&(vr.charCodeAt(f)<=57))||(vr.charCodeAt(f)==44)){
			vp=vp+vr.charAt(f);
		}
	}
	a=1;
	vr = vp;
	if(vr.length>0){
		vr = vr.split(',');
		vrIni = vr[0];
		vrIniTemp = '';
		for(f=0;f<vrIni.length;f++){
			vrIniTemp = vrIni.charAt(vrIni.length-f-1) + vrIniTemp;
			if((a==3)&&(vrIni.length!=f+1)){
				vrIniTemp = '.' + vrIniTemp;
				a=0;
			}
			a++;
		}
		vrIni = vrIniTemp;
		vrFim = "";
		if(vr.length>1){
			vrFim = vr[1];
			if(vrFim.length>tamdec){
				if(truncate==3){
					valorFinal = vrFim.substring(tamdec,tamdec + 1);
					vrFim = vrFim.substring(0,tamdec);
					if(valorFinal>4){
						vrFim = vrFim/1 + 1;
					}
				} else if(truncate==2){
					vrFim = vrFim.substring(0,tamdec);
				}
			} else {
				for(f=vrFim.length;f<tamdec;f++){
					vrFim = vrFim + '0';
				}
			}
		} else {
			for(f=0;f<tamdec;f++){
				vrFim = vrFim + '0';
			}
		}
		vr = vrIni + ',' + vrFim;
		return vr;
	}
}

function processasubgrupo()
{
	try { document.formProc.nr_seql_cnd_ctc.selectedIndex = 0; } catch (ex) { }
	submitdadostecnicos();
}

function submitdadostecnicos()
{
	document.formProc.action = 'concluir_negocio_tec.asp';
	document.formProc.submit();
}

function alterarDados()
{
	document.formProc.salvartec.value = 'S';
	submitdadostecnicos();
}

-->
</script>
	<br>
	&nbsp;&nbsp;&nbsp;<input type="button" name="alterar" value="Alterar" style="width:80px;font-size:10px;margin: 2px 2px 2px 2px;" onclick="alterarDados();">
	<input type="hidden" name="salvartec" value="N">
	<input type="hidden" name="primeiro" value="false">
	<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
<%
NR_CTC_SGRO_aux = NR_CTC_SGRO
NR_VRS_CTC_aux = NR_VRS_CTC

set rs = objcon.mLerSubgrupoCotacao
	
if rs.eof then
	bSubgrupo = false
else
	bSubgrupo = true
%>
	<tr><br>
		<td class="td_label" width="30%">&nbsp;Subgrupo</td>
		<td class="td_dado" colspan="3">&nbsp;
			<select name="NM_PRPN_CTC" onchange="processasubgrupo();">
			<option value="">--Escolha abaixo--</option>
			<%
			do while not rs.eof
				if trim(request("NM_PRPN_CTC")) = trim(rs("NR_CTC_SGRO")) & "|" & trim(rs("NR_VRS_CTC")) then
					sSelecionado = " selected"
				else
					sSelecionado = ""
				end if
				%>
				<option value="<%=trim(rs("NR_CTC_SGRO")) & "|" & trim(rs("NR_VRS_CTC"))%>"<%=sSelecionado%>><%=right(00 & rs("subgrupo_id"),2) & " (" & trim(rs("NR_CTC_SGRO")) & ")"%> - <%=trim(rs("NM_PRPN_CTC"))%></option>
				<%
				rs.movenext
			loop
			%>
			</select>
		</td>
	</tr>
<%
	if trim(request("NM_PRPN_CTC")) <> "" then 
		arrSubGrupoCotacao = split(trim(request("NM_PRPN_CTC")),"|")

		objCon.pNR_CTC_SGRO = arrSubGrupoCotacao(0)
		objCon.pNR_VRS_CTC = arrSubGrupoCotacao(1)
		NR_CTC_SGRO = objCon.pNR_CTC_SGRO
		NR_VRS_CTC = objCon.pNR_VRS_CTC
	end if
end if

if not bSubgrupo or trim(request("NM_PRPN_CTC")) <> "" then

	if trim(request("salvartec")) = "S" then
		set rsCotacao = objCon.mLerCotacao

		if ucase(rsCotacao("CD_ULT_EVT_CTC")) <> "T" then
			objcon.pCD_ULT_EVT_CTC = "T"
			bAtualizaCotacao = true
		else
			bAtualizaCotacao = false
		end if
			
		if trim(request("NM_PRPN_CTC")) <> "" then
			arrSubGrupoCotacao = split(trim(request("NM_PRPN_CTC")),"|")
			NR_CTC_SGRO_subgrupo = arrSubGrupoCotacao(0)
			NR_VRS_CTC_subgrupo = arrSubGrupoCotacao(1)
		else
			NR_CTC_SGRO_subgrupo = NR_CTC_SGRO
			NR_VRS_CTC_subgrupo = NR_VRS_CTC
		end if

		objCon.pvl_lim_cap_seg_cob_filhos = trim(request("vl_lim_cap_seg_cob_filhos"))
		objCon.pvl_lim_min_cap_seg = trim(request("vl_lim_min_cap_seg"))
		objCon.pvl_lim_max_cap_seg = trim(request("vl_lim_max_cap_seg"))
		objCon.pper_fat_seguro = trim(request("per_fat_seguro"))
		objCon.pper_revisao_tx_sinistro = trim(request("per_revisao_tx_sinistro"))
		objCon.pperc_ind_rev_tax_sin = trim(request("perc_ind_rev_tax_sin"))
		objCon.pqt_vidas_cotadas = trim(request("qt_vidas_cotadas"))
		objCon.pperc_min_adesao = trim(request("perc_min_adesao"))
		objCon.plim_idade_emi_aplc = trim(request("lim_idade_emi_aplc"))
		objCon.plim_idade_nova_adesao  = trim(request("lim_idade_nova_adesao"))
		objCon.pperc_exc_tnco  = trim(request("perc_exc_tnco"))
		objCon.pperc_da_exc_tnco  = trim(request("perc_da_exc_tnco"))

		objCon.pNR_CTC_SGRO = NR_CTC_SGRO_aux
		objCon.pNR_VRS_CTC = NR_VRS_CTC_aux

		objCon.pWF_ID = WF_ID
			
		retorno = objCon.mAtualizarDadosTecnicos(NR_CTC_SGRO_subgrupo, NR_VRS_CTC_subgrupo, bAtualizaCotacao)

		if trim(retorno) <> "" then
			Response.redirect("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
			Response.End 
		end if
			
		chave
	end if

	set rsDadosTecnicos = objcon.mLerDadosTecnicos()
		
	if not rsDadosTecnicos.eof then
		vl_lim_cap_seg_cob_filhos = testaDadoVazioFormataN(rsDadosTecnicos,"vl_lim_cap_seg_cob_filhos",2)
		vl_lim_min_cap_seg = testaDadoVazioFormataN(rsDadosTecnicos,"vl_lim_min_cap_seg",2)
		vl_lim_max_cap_seg = testaDadoVazioFormataN(rsDadosTecnicos,"vl_lim_max_cap_seg",2)
		per_fat_seguro = testaDadoVazioFormataN(rsDadosTecnicos,"per_fat_seguro",0)
		per_revisao_tx_sinistro = testaDadoVazioFormataN(rsDadosTecnicos,"per_revisao_tx_sinistro",0)
		perc_ind_rev_tax_sin = testaDadoVazioFormataN(rsDadosTecnicos,"perc_ind_rev_tax_sin",2)
		qt_vidas_cotadas = testaDadoVazioFormataN(rsDadosTecnicos,"qt_vidas_cotadas",0)
		perc_min_adesao = testaDadoVazioFormataN(rsDadosTecnicos,"perc_min_adesao",2)
		lim_idade_emi_aplc = testaDadoVazioFormataN(rsDadosTecnicos,"lim_idade_emi_aplc",0)
		lim_idade_nova_adesao  = testaDadoVazioFormataN(rsDadosTecnicos,"lim_idade_nova_adesao",0)
		perc_exc_tnco  = testaDadoVazioFormataN(rsDadosTecnicos,"perc_exc_tnco",2)
		perc_da_exc_tnco  = testaDadoVazioFormataN(rsDadosTecnicos,"perc_da_exc_tnco",2)
	end if
%>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td nowrap class="td_label" width="30%">&nbsp;Limite de capital segurado<br>&nbsp;para cobertura de filhos</td>
		<td class="td_dado" width="20%">&nbsp;
			<input type="text" name="vl_lim_cap_seg_cob_filhos" size="16" value="<%=vl_lim_cap_seg_cob_filhos%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right;">
		</td>
		<td nowrap class="td_label">&nbsp;Percentual m�nimo para ades�o<br>&nbsp;das vidas cotadas</td>
		<td class="td_dado">&nbsp;
			<input type="text" name="perc_min_adesao" size="6" value="<%=perc_min_adesao%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="6" style="text-align:right;">
		</td>
	</tr>				
	<tr>
		<td nowrap class="td_label">&nbsp;Limite m�ximo de capital segurado</td>
		<td class="td_dado">&nbsp;
			<input type="text" name="vl_lim_max_cap_seg" size="16" value="<%=vl_lim_max_cap_seg%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right;">
		</td>
		<td nowrap class="td_label">&nbsp;Limite de idade para emiss�o da ap�lice</td>
		<td class="td_dado">&nbsp;
			<input type="text" name="lim_idade_emi_aplc" size="2" value="<%=lim_idade_emi_aplc%>" onKeyUp="FormatValorContinuo(this,0);" maxlength="2" style="text-align:right;">
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Limite m�nimo de capital segurado</td>
		<td class="td_dado">&nbsp;
			<input type="text" name="vl_lim_min_cap_seg" size="16" value="<%=vl_lim_min_cap_seg%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right;">
		</td>
		<td nowrap class="td_label">&nbsp;Limite de idade para novas ades�es<br>&nbsp;na ap�lice</td>
		<td class="td_dado">&nbsp;
			<input type="text" name="lim_idade_nova_adesao" size="2" value="<%=lim_idade_nova_adesao%>" onKeyUp="FormatValorContinuo(this,0);" maxlength="2" style="text-align:right;">
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Periodicidade de faturamento do seguro</td>
		<td class="td_dado">&nbsp;
			<select name="per_fat_seguro">
				<option value="">--Escolha abaixo--</option>
				<option value="1" <%if per_fat_seguro = "1" then Response.Write "selected"%>>Mensal</option>
				<option value="3" <%if per_fat_seguro = "3" then Response.Write "selected"%>>Trimestral</option>
				<option value="6" <%if per_fat_seguro = "6" then Response.Write "selected"%>>Semestral</option>
				<option value="12" <%if per_fat_seguro = "12" then Response.Write "selected"%>>Anual</option>
			</select>
		</td>
		<td nowrap class="td_label">&nbsp;Periodicidade para revis�o<br>&nbsp;de taxa por sinistralidade</td>
		<td class="td_dado">&nbsp;
			<select name="per_revisao_tx_sinistro">
				<option value="">--Escolha abaixo--</option>
				<option value="1" <%if per_revisao_tx_sinistro = "1" then Response.Write "selected"%>>Mensal</option>
				<option value="3" <%if per_revisao_tx_sinistro = "3" then Response.Write "selected"%>>Trimestral</option>
				<option value="6" <%if per_revisao_tx_sinistro = "6" then Response.Write "selected"%>>Semestral</option>
				<option value="12" <%if per_revisao_tx_sinistro = "12" then Response.Write "selected"%>>Anual</option>
			</select>
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Vidas efetivamente cotadas para<br>&nbsp;cobertura do seguro</td>
		<td class="td_dado">&nbsp;
			<input type="text" name="qt_vidas_cotadas" size="4" value="<%=qt_vidas_cotadas%>" onKeyUp="FormatValorContinuo(this,0);" maxlength="4" style="text-align:right;">
		</td>
		<td nowrap class="td_label" width="30%">&nbsp;�ndice para revis�o da taxa<br>&nbsp;por sinistralidade</td>
		<td class="td_dado" width="20%">&nbsp;
			<input type="text" name="perc_ind_rev_tax_sin" size="6" value="<%=perc_ind_rev_tax_sin%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="6" style="text-align:right;">
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Percentual de excedente t�cnico</td>
		<td class="td_dado">&nbsp;
			<input type="text" name="perc_exc_tnco" size="6" value="<%=perc_exc_tnco%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="6" style="text-align:right;">
		</td>
		<td nowrap class="td_label">&nbsp;Percentual de DA do excedente t�cnico</td>
		<td class="td_dado">&nbsp;
			<input type="text" name="perc_da_exc_tnco" size="6" value="<%=perc_da_exc_tnco%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="6" style="text-align:right;">
		</td>
	</tr>
<%end if%>
</table>


