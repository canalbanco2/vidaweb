?<%
Set objPerguntas = Server.CreateObject("PRDL0001.cls00169")
Set objRespostas = server.CreateObject("PRDL0001.cls00167")

'Set objPerguntas = Server.CreateObject("sctl0003.ccotacao")
'Set objRespostas = server.CreateObject("sctl0003.ccotacao")




Function retornaIndice(valor)
	if isnull(valor) then
		valor= ""
	end if
	for iI = 0 to (ubound(oArvore) - 1)
		if trim(oArvore(iI).pergunta_id) = trim(valor) then
			retornaIndice = iI
			exit function
		end if
	next
	retornaIndice = ""
End Function

Function retornaIndiceResp(arr,valor)
	if isnull(valor) then
		valor= ""
	end if
	for iI = 1 to  UBound(arr)
		if trim(arr(iI).dominio_resposta_id) = trim(valor) then
			retornaIndiceResp = iI
			exit function
		end if
	next
	retornaIndiceResp = ""
End Function

function retornaIdResposta(arr, valor)
	resposta_pergunta_precedente = RespostasDasPerguntas(arr)
	for iR = 1 to ubound(resposta_pergunta_precedente)
		if trim(resposta_pergunta_precedente(iR).resposta_determinada) = trim(valor) then
			retornaIdResposta = resposta_pergunta_precedente(iR).dominio_resposta_id
			exit function
		end if
	next
	retornaIdResposta = ""
end function

Function testarRespostasObrigatorias(aba,ByRef perg)
	erro = ""
    abaT = ""
    If InStr(1,aba,"|") = 0 Then
       Dim arAbaCobert(1)
       arAbaCobert(0) = aba
       abaT = aba
    Else
       arAbaCobert = Split(aba,"|")
    End If

    For contV = 0 To UBound(arAbaCobert)-1
        cd_questionario = Request(arAbaCobert(contV) & "questionarioID")
        codigos_per = Split(Request(arAbaCobert(contV) & "pergQuestionario"),"|")

        For contB = 0 To UBound(codigos_per)
 	       If Trim(Request(abaT & cd_questionario & "indice" & contB)) <> "" Then
  		      pergunta_id = Trim(Request(abaT & cd_questionario & "indice" & contB))
  		      If Trim(Request(abaT & cd_questionario & "obrigatorio" & pergunta_id)) = "S" Then
  		         perg = pergunta_id
  		         If Trim(Request(abaT & cd_questionario & "rsp" & pergunta_id)) = "" Then
		            erro = erro &  "*A pergunta '" & Trim(Request(abaT & cd_questionario & "pergunta" & contB)) & "' precisa ser respondida.<br>"
		         End If
		      End If
		   End If
		Next
	Next
         
	If erro <> "" Then
		erro = "<font color=""red"">" & erro & "</font>"
	End If
	testarRespostasObrigatorias = erro
End Function

Function gravarRespostas(aba,CD_TIP_QSTN,NR_CD_TIP_QSTN_CTC,NR_SEQL_QSTN_CTC)
    If CD_TIP_QSTN = 9 Then
        CD_TIP_QSTN = 2
    Else
        preencheChaves
    End If
    If InStr(1,aba,"|") = 0 Then
        Redim arAbaCobert(1)
        arAbaCobert(0) = aba
    Else
        arAbaCobert = Split(aba,"|")
    End If
 
	erro = ""
	objCon.pCD_TIP_QSTN = CD_TIP_QSTN
    objCon.pNR_CD_TIP_QSTN_CTC = NR_CD_TIP_QSTN_CTC
    For contV = 0 To UBound(arAbaCobert)-1
        cd_questionario = Request(arAbaCobert(contV) &"questionarioID")
        seq_qstn = NR_SEQL_QSTN_CTC

        codigos_per = Split(Request(arAbaCobert(contV) & "pergQuestionario"),"|")
        For contB = 0 To UBound(codigos_per)
	        If Trim(Request(cd_questionario & "indice" & contB)) <> "" Then
  		       pergunta_id = Trim(Request(cd_questionario & "indice" & contB))
		       resposta = Trim(Request(cd_questionario & "rsp" & pergunta_id))
	
               objCon.pNR_QSTN = objCon.pNR_QSTN & cd_questionario & "|"
               objCon.pNR_SEQL_QSTN_CTC = objCon.pNR_SEQL_QSTN_CTC & seq_qstn & "|"
               objCon.pNR_QST = objCon.pNR_QST & pergunta_id & "|"
		       objCon.pNR_ORD_QSTN_CTC = objCon.pNR_ORD_QSTN_CTC & contB + 1 & "|"
		       objCon.pTX_DCR_RPST_CTC = objCon.pTX_DCR_RPST_CTC & resposta & "|"
	        End If
	    Next
    Next
   	erro = objCon.mIncluirQuestionario()
	If erro <> "" Then
		erro = "<font color=""red"">" & erro & "</font>"
	End If
	preencheChaves
	gravarRespostas = erro
End Function

Function excluirQuestionario(aba, CD_TIP_QSTN, NR_CD_TIP_QSTN_CTC, NR_SEQL_QSTN_CTC)
    If CD_TIP_QSTN = 9 Then
       CD_TIP_QSTN = 2
    Else
       preencheChaves 
    End If

	contador = 0
	erro = ""
	objCon.pNR_QSTN = Request(aba&"questionarioID")
	objCon.pCD_TIP_QSTN = CD_TIP_QSTN
	objCon.pNR_CD_TIP_QSTN_CTC = NR_CD_TIP_QSTN_CTC
	objCon.pNR_SEQL_QSTN_CTC = NR_SEQL_QSTN_CTC

	erro = objCon.mExcluirQuestionario()
	If erro <> "" Then
		erro = "<font color=""red"">" & erro & "</font>"
	End If
	preencheChaves
	excluirQuestionario = erro
End Function

Function RespostasDasPerguntas(pergunta_id)
	Set objRetornoRespostas = objRespostas.mObtem_Resposta_Pergunta(CD_PRD,CD_MDLD,CD_ITEM_MDLD,NR_CTC_SGRO,NR_VRS_CTC,NR_SEQL_QSTN_CTC,questionario_id,pergunta_id)
	Redim aRespostas(0)
	a = 0
	If NOT objRetornoRespostas.EOF Then
		Do while not objRetornoRespostas.EOF
			a=a+1
			redim preserve aRespostas(a)
			Set aRespostas(a) = new cRespostas
			aRespostas(a).dominio_resposta_id = objRetornoRespostas("dominio_resposta_id")
			aRespostas(a).resposta_determinada = objRetornoRespostas("resposta_determinada")
			aRespostas(a).STATUS = objRetornoRespostas("STATUS")
			objRetornoRespostas.movenext
		Loop
	End If
	Set objRetornoRespostas = Nothing
	RespostasDasPerguntas = aRespostas
End Function

Function retornaValor(cod,Arr,idc,postB)
	valor1 = Trim(Request(cod & "rsp" & Arr(idc).pergunta_id))
	valor2 = Trim(Arr(idc).resposta)
   'Se for PostBack ou tiver retornado de uma exclus�o
	If postB Then
		retornaValor = valor1
	Else
		retornaValor = valor2
	End If
End Function

Function mGeraArrayQuestionario(recset)
	Dim objItemTreeView
	Dim oArrayQuestionario()
	intloop = 0

	While not recset.EOF
		Redim preserve oArrayQuestionario(intloop+1)
		Set oArrayQuestionario(intloop) = New cItemTreeView
		Set objItemTreeView = New cItemTreeView
        If intOrdemMax < recset("num_ordem_pergunta") Then
           intOrdemMax = recset("num_ordem_pergunta")
        End If
        objItemTreeView.ordem_resposta = recset("num_ordem_pergunta") 
        objItemTreeView.pergunta_id = recset("pergunta_id")
		objItemTreeView.nome_pergunta = recset("nome_pergunta")
		If IsNull(recset("grupo_dominio_resposta_id")) Then
			objItemTreeView.grupo_dominio_resposta_id = "null"
		Else
			objItemTreeView.grupo_dominio_resposta_id = recset("grupo_dominio_resposta_id")
		End If
		objItemTreeView.num_pergunta_precedente = recset("num_pergunta_precedente")
		objItemTreeView.resposta_pergunta_precedente_id = recset("resposta_pergunta_precedente_id")
		objItemTreeView.nome_formato = recset("nome_formato")
		objItemTreeView.tamanho = recset("tamanho_resposta")
		objItemTreeView.tipo_formato = recset("tipo_formato")
		objItemTreeView.ind_obrigatoriedade = recset("ind_obrigatoriedade")
		'if tp_ativ_id =1 then    -- retirado para utiliza��o na pagina dados_qst
		If isnull(recset("resposta")) Then
			objItemTreeView.resposta = ""
		Else
			objItemTreeView.resposta = recset("resposta")
		End If
		'##############################33
		objItemTreeView.tabs = ""
        If objItemTreeView.num_pergunta_precedente <> "0" Then
			indiceID = AchaIndiceId(oArrayQuestionario,objItemTreeView.num_pergunta_precedente)
			If indiceID <> "" Then
				objItemTreeView.tabs = oArrayQuestionario(indiceID).tabs & "|"
			End If
        End If
		If isnull(recset("formato")) Then
			objItemTreeView.formato = ""
		Else
			objItemTreeView.formato = recset("formato")
		End If

		Set oArrayQuestionario(intloop) = objItemTreeView
		intloop = intloop + 1
		recset.MoveNext
	Wend
	mGeraArrayQuestionario = oArrayQuestionario
End Function

Function CalcularOrdem(ByRef arvore)
	If UBound(arvore) = 1 Then
		Redim ordem(UBound(arvore))
		ordem(0) = "00"
		arvore(0).ordem_resposta = 1
		CalcularOrdem = arvore
	ElseIf UBound(arvore) = 0 Then
		arvore(0).ordem_resposta = 1
	Else
		Dim iO, grupoBase, cod_aux, cod_pre, pos_aux, i2
		grupoBase = "00|@"

		For iO = 0 To UBound(arvore) - 1
			If Trim(arvore(iO).num_pergunta_precedente) = "0" Then

			    cod_aux = AchaProximo(arvore, iO)
			    If cod_aux <> "" Then
			       If cod_aux < 10 Then
			          cod_aux = "0" & cod_aux
			       End If
				    grupoBase = grupoBase & cod_aux & "|@"
				End If
			Else
			    cod_pre = AchaIndiceId(arvore,arvore(iO).num_pergunta_precedente)   'Indice na arvore da pergunta precedente
			    If cod_pre < 10 Then
			       cod_pre = "0" & cod_pre
			    End If
			    pos_aux = InStr(1, grupoBase, cod_pre & "|")

				If pos_aux > 0 Then
					pos_aux = InStr(pos_aux+1, grupoBase, "@")
					If iO < 10 Then
						cod_aux = "0" & iO
					Else
						cod_aux = iO
					End If
					grupoBase = Left(grupoBase,pos_aux-1) & cod_aux & "|@" & Mid(grupoBase,pos_aux+1)

					'Atualizando perguntas precedentes na arvore
					While Trim(arvore(cod_pre).num_pergunta_precedente) <> "0"
						cod_pre = AchaIndiceId(arvore,arvore(cod_pre).num_pergunta_precedente)
						arvore(iO).colecao_perguntas_precedentes = arvore(iO).colecao_perguntas_precedentes & arvore(cod_pre).pergunta_id & "|"
					WEnd
				End If
			End If

		Next

		ordem = Split(Replace(grupoBase,"@",""),"|")
    End If
    
    CalcularOrdem = ordem
End Function

Function AchaProximo(arvore, index)'##
    AchaProximo = ""
	 If UBound(arvore) <> 1 Then
	  	 For iP = (index + 1) To UBound(arvore)-1
	 	  	  If Trim(arvore(iP).num_pergunta_precedente) = "0" Then
			  	  AchaProximo = iP
				  iP = UBound(arvore)
			  End If
	 	 Next
	 End If
End Function

Function AchaPai(ByRef arvore, pai, indiceAtual) '#
	Dim i
	iIrmaoAnterior = 0
	For i = 0 To indiceAtual - 1
		If arvore(i).num_pergunta_precedente = arvore(indiceAtual).num_pergunta_precedente Then
			iIrmaoAnterior = i
		End If
	Next
	If iIrmaoAnterior <> 0 Then
		AchaPai =  iIrmaoAnterior
		Exit Function
	End If
	For i = 0 To UBound(arvore) - 1
		If arvore(i).pergunta_id = pai Then
			AchaPai = i
			Exit Function
		End If
	Next
End Function

Function AchaIndiceId(arvore,ID)
	AchaIndiceId = ""
	If Trim(ID) <> "" Then
   		For F=0 To Ubound(arvore)-1
	 	    If Trim(arvore(F).pergunta_id) = Trim(ID) Then
		 	    AchaIndiceId = F
			    F = Ubound(arvore)
		    End If
	   Next
	End If
End Function

function Respostas(pergunta_id)
	if tp_ativ_id =1 then
		set objRetornoRespostas = objRespostas.mObtem_Resposta_Pergunta(CD_PRD,CD_MDLD,CD_ITEM_MDLD,NR_CTC_SGRO,NR_VRS_CTC,NR_SEQL_QSTN_CTC,questionario_id,pergunta_id)
		if not objRetornoRespostas.eof then
			do while not objRetornoRespostas.eof
				if trim(ucase(objRetornoRespostas("STATUS"))) = "S" then
					Respostas = objRetornoRespostas("resposta_determinada")
					exit function
				end if
				objRetornoRespostas.movenext
			loop
			Respostas = ""
		else
			Respostas = ""
		end if
	else
		Respostas = ""
	end if
	set objRetornoRespostas = nothing
End Function
%>
<script>
<!--
	function pressionarQuestionario(qst,indice,valor){
 		trocouDado();
		for(ii=0;ii<document.forms[0].elements.length;ii++){
		    if(eval('document.forms[0].elements[ii].name.indexOf("'+qst+'pgpre'+indice+'rspq")')>-1){
   		       if((eval('document.forms[0].elements[ii].name.indexOf("'+qst+'pgpre'+indice+'rspq'+valor+'")')==-1)||(eval('document.forms[0].elements[ii].name.indexOf("'+qst+'pgpre'+indice+'rspq##")')>-1)){
		          eval('document.all.linTR_'+document.forms[0].elements[ii].value+'.style.display="none"');
		          var ind_a = document.forms[0].elements[ii].value;
		          
		          for(iii=0;iii<document.forms[0].elements.length;iii++){
                      if(eval('document.forms[0].elements[iii].name=="'+qst+'indice'+ind_a+'"')){
                         document.forms[0].elements[iii].value="";
                         document.forms[0].elements[iii-2].value="";
                         document.forms[0].elements[iii-2].checked=false;
                      }
                  }       
               }
               else{
		          eval('document.all.linTR_'+document.forms[0].elements[ii].value+'.style.display=""');
		          var perg_id = eval('document.forms[0].elements[ii].name.replace("'+qst+'pgpre'+indice+'rspq'+valor+'cd","")');
		          var ind_a = document.forms[0].elements[ii].value;
		          
		          for(iii=0;iii<document.forms[0].elements.length;iii++){
                      if(eval('document.forms[0].elements[iii].name=="'+qst+'indice'+ind_a+'"')){
                         document.forms[0].elements[iii].value=perg_id;
                         break;
                      }
                  }       
               }
		    }
		}
	}

    function ConcatenaData(campo,frm){
        arr_prefixo=campo.name.split("_");
        prefixo=arr_prefixo[0];
        frm[prefixo].value=frm[prefixo+'_ano'].value + '' + frm[prefixo+'_mes'].value + '' + frm[prefixo+'_dia'].value;
    }
//-->
</script>
<!--#include virtual = "/funcao/jscript.asp"-->