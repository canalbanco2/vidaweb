<!--#include virtual = "/padrao.asp"-->
<!--#include virtual = "/funcao/jscript.asp"-->
<!--#include virtual = "/scripts/wbra_internet.asp"-->
<style>
.bgout { 
background-color: #FFFAFA; 
} 
.bgover { 
background-color: #FFFAF0; 
} 
</style>

<script>
function camposData(op){
	if(op=='T')
	{
		document.FrmGrid.F_DiaIni.disabled = true;
		document.FrmGrid.F_MesIni.disabled = true;
		document.FrmGrid.F_AnoIni.disabled = true;

		document.FrmGrid.F_DiaFim.disabled = true;
		document.FrmGrid.F_MesFim.disabled = true;
		document.FrmGrid.F_AnoFim.disabled = true;
	}
	if(op=='P')
	{
		document.FrmGrid.F_DiaIni.disabled = false;
		document.FrmGrid.F_MesIni.disabled = false;
		document.FrmGrid.F_AnoIni.disabled = false;

		document.FrmGrid.F_DiaFim.disabled = false;
		document.FrmGrid.F_MesFim.disabled = false;
		document.FrmGrid.F_AnoFim.disabled = false;
	}
}

function submitform()
{
	<%
		arrParametro(9) = ""
		tp_wf_id = arrParametro(9)
		session("parametro") = arrParametro
	%>
	document.FrmGrid.postback.value = '1';
	document.FrmGrid.action = 'corpo2.asp';
	document.FrmGrid.submit();
}

function limparform()
{
	location.href='corpo2.asp';
}

function localizaragencia()
{
	var agencia_id = document.FrmGrid.F_agencia_id.value;

	if (agencia_id == '') return;

	for (x = 0; x < document.FrmGrid.F_agencia.length; x++)
	{
		if (document.FrmGrid.F_agencia[x].value == agencia_id)
		{
			document.FrmGrid.F_agencia.selectedIndex = x;
			return;
		}
	}
	
	//alert('Ag�ncia inexistente');	
	document.FrmGrid.F_agencia.selectedIndex = 0;
}

function alterarmunicipio()
{
	var municipio_nome = document.FrmGrid.F_municipio_nome.value;

	if (municipio_nome == '')
		return;
	else
		document.FrmGrid.F_municipio.selectedIndex = 0;
}

function mudaFluxo()
{
	
}

function VerificaPesquisa()
{
	if ( document.forms[0].F_cotacao.value == '' ){
		if ( document.forms[0].F_instancia.value == '' ){
			if ( window.document.forms[0].tp_ramo_id.selectedIndex == 0 )
			{
				alert('Deve-se escolher uma Cota��o, ou uma Inst�ncia ou um Tipo de Ramo');
				return false;
			}
		}
	}
}

</script>
<form name="FrmGrid" action="default2.asp" method="get" onsubmit="javascript:return VerificaPesquisa();">
<input type="hidden" name="postback" value="">
<table class='escolha' width="700px" align="center">
	<tr>
		<td colspan="2" class="td_dado">Selecione as caracter�sticas desejadas:</td>
	</tr>
	<tr>
		<td class='td_label'>Tipo de Ramo:</td>
		<td class='td_dado'>
		<%
		Set rsTipoRamo = objcon.mLerTipoRamo()
		
		TipoRamo = Trim(Request("tp_ramo_id"))
		If TipoRamo <> "" Then  TipoRamo = cint(TipoRamo)
		%>
		<Select name="tp_ramo_id" onchange="submitform()">
			<option value="" <%if TipoRamo = "" then response.write("Selected")%>>-- Escolha abaixo --</option>
			<%
			if not RsTipoRamo.eof then
				while not RsTipoRamo.eof
			%>
			<option value="<%=RsTipoRamo("tp_ramo_id")%>" <%if trim(TipoRamo) = trim(RsTipoRamo("tp_ramo_id")) then response.write("Selected")%>><%= RsTipoRamo("tp_ramo_id") %> - <%= NomeTipoRamo(RsTipoRamo("tp_ramo_id")) %></option>
			<%
					RsTipoRamo.movenext
				wend
			end if
			%>
		</select>
		</td>
	</tr>	
	<tr>
		<td class='td_label'>Ramo:</td>
		<td class='td_dado'>
		<%
		Ramo = request("F_ramo")
		if ramo <> "" then  ramo = cint(ramo)
		%>
		<Select name="F_ramo" onchange="submitform()">
			<option value="" <%if ramo = "" then response.write("Selected")%>> Todos </option>
			<%
			if trim(TipoRamo) <> "" then
				objcon.pTp_Ramo_id = request("tp_ramo_id")
				set RsRamo = objcon.mLerRamo()

				while not RsRamo.eof
			%>
			<option value="<%=RsRamo("ramo_id")%>" <%if trim(ramo) = trim(RsRamo("ramo_id")) then response.write("Selected")%>><%= RsRamo("ramo_id") %> - <%= RsRamo("nome") %></option>
			<%
					RsRamo.movenext
				wend
			end if
			%>
		</select>
		</td>
	</tr>
	<tr>
		<td class='td_label'>Subramo:</td>
		<td class='td_dado'>
		<%
		subramo = request("F_subramo")
		if subramo <> "" then subramo = Trim(subramo)
		%>
		<Select name="F_subramo">
			<option value="" <%if subramo = "" then response.write("Selected")%>> Todos </option>
			<%
			if trim(Ramo) <> "" then
				objcon.pRamo_id = request("F_ramo")
				set rsSubRamo = objcon.mLerSubRamo()
				
				while not RsSubRamo.eof
			%>
			<option value="<%=RsSubRamo("subramo_id")%>" <%if trim(subramo) = trim(RsSubRamo("subramo_id")) then response.write("Selected")%>><%= RsSubRamo("subramo_id") %> - <%= RsSubRamo("nome") %></option>
			<%
					RsSubRamo.movenext
				wend
			end if
			%>
		</select>
		</td>
	</tr>
	<tr>
		<td class='td_label'>Opera��o:</td>
		<td class='td_dado'>
		<%
		operacao = trim(request("F_operacao"))
		if operacao <> "" then
			tp_wf_id = RetornaTipoWorkflow(TipoRamo, operacao, "2")
		else
			tp_wf_id = "null"
		end if
		%>
		<Select name="F_operacao" onchange="submitform()">
			<option value=""> Todas </option>
			<option value="1"<%if operacao = "1" then%> selected<%end if%>>Seguro novo</option>
			<option value="2"<%if operacao = "2" then%> selected<%end if%>>Renova��o</option>
			<option value="3"<%if operacao = "3" then%> selected<%end if%>>Endosso</option>
			<option value="4"<%if operacao = "4" then%> selected<%end if%>>Endosso de cancelamento</option>
		</select>
		</td>
	</tr>
	<tr>
		<td class='td_label'>Iniciados em:</td>
		<td class='td_dado'>
			<INPUT TYPE="radio" NAME="F_tp_ini" value="T" OnClick="camposData('T');"<%if request("F_tp_ini") = "T" or F_tp_ini = "" then%> checked<%end if%>> 
			Todos&nbsp;&nbsp;&nbsp; 
			<INPUT TYPE="radio" NAME="F_tp_ini" value="P" OnClick="camposData('P');"<%if request("F_tp_ini") = "P" then%> checked<%end if%>> 
			De&nbsp;&nbsp;
			<INPUT TYPE="text" NAME="F_DiaIni" size="2" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=request("F_DiaIni")%>">
			<INPUT TYPE="text" NAME="F_MesIni" size="2" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=request("F_MesIni")%>">
			<INPUT TYPE="text" NAME="F_AnoIni" size="4" maxlength="4" onKeyUp="return autoTab(this, 4, event);" value="<%=request("F_AnoIni")%>"> 
			&nbsp;at�&nbsp;
			<INPUT TYPE="text" NAME="F_DiaFim" size="2" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=request("F_DiaFim")%>">
			<INPUT TYPE="text" NAME="F_MesFim" size="2" maxlength="2" onKeyUp="return autoTab(this, 2, event);" value="<%=request("F_MesFim")%>">
			<INPUT TYPE="text" NAME="F_AnoFim" size="4" maxlength="4" onKeyUp="return autoTab(this, 4, event);" value="<%=request("F_AnoFim")%>">
		</td>
	</tr>
	<tr>
		<td class='td_label'>Situa��o da Inst�ncia:</td>
		<td class='td_dado'>
			<SELECT NAME="F_wf_situacao">
				<OPTION value="" selected> Todos </OPTION>
				<OPTION value="1"<%if request("F_wf_situacao") = "1" OR request("F_wf_situacao") = "" then%> selected<%end if%>> 
				Ativa </OPTION>
				<OPTION value="2"<%if request("F_wf_situacao") = "2" then%> selected<%end if%>> 
				Encerrada </OPTION>
				<OPTION value="3"<%if request("F_wf_situacao") = "3" then%> selected<%end if%>> 
				Suspensa </OPTION>
				<OPTION value="9"<%if request("F_wf_situacao") = "9" then%> selected<%end if%>> 
				Cancelada </OPTION>
			</SELECT>
		</td>
	</tr>
	<tr>
		<td class='td_label'>Inst�ncia:</td>
		<td class='td_dado'>
			<INPUT TYPE="text" NAME="F_instancia" size="7" maxlength="9" value="<%=F_instancia%>">&nbsp;
		</td>
	</tr>
	<tr>
		<td class='td_label'>Cota��o:</td><%'Pesquisar pela Cota��o BB, mas considerar o label apenas como "Cota��o"%>
		<td class='td_dado'>
			<INPUT TYPE="text" NAME="F_cotacao_BB" size="7" maxlength="9" value="<%=F_cotacao%>">&nbsp;
		</td>
	</tr>
	<!--tr>
		<td class='td_label'>Cota��o BB:</td>
		<td class='td_dado'>
			<INPUT TYPE="text" NAME="F_cotacao_bb" size="5" maxlength="5" value="<%'F_cotacao_bb%>">&nbsp;
		</td>
	</tr-->
	<tr>
		<td class='td_label'>Estado:</td>
		<td class='td_dado'>
		<%
		estado = Cstr(request("F_estado"))
		set rsEstado = objcon.mLerUF
		%>
		<Select name="F_estado" onchange="submitform();">
			<option value="" <%if estado = "" then response.write("Selected")%>> Todos </option>
			<%
			While NOT rsEstado.EOF
			%>
			<option value="<%=rsEstado("estado")%>" <%if estado = Trim(Cstr(rsEstado("estado"))) then response.write("Selected")%>><%= rsEstado("estado") %></option>
			<%
				rsEstado.movenext
			WEnd
			%>
		</select>
		</td>
	</tr>
	<tr>
		<td class='td_label'>Munic�pio:</td>
		<td class='td_dado'>
		<%
		municipio = Cstr(request("F_municipio"))%>
		<Select name="F_municipio" onchange="submitform();">
			<option value="" <%if municipio = "" then response.write("Selected")%>> Todos </option>
			<%
			If Trim(estado) <> "" Then
				objcon.pUF = estado
				Set rsMunicipio = objcon.mLerMunicipio()
	            Do While NOT rsMunicipio.EOF
			%>
			<option value="<%=rsMunicipio("municipio_id")%>" <%if municipio = Trim(Cstr(rsMunicipio("municipio_id"))) then response.write("Selected")%>><%= rsMunicipio("nome") %></option>
			<%
					rsMunicipio.MoveNext
				Loop
			End If
			%>
		</select>
			<%if trim(estado) <> "" then%>
			<br>ou parte do nome&nbsp;&nbsp;&nbsp;<input type="text" name="F_municipio_nome" size="20" maxlength="60" value="<%=request("F_municipio_nome")%>" onBlur="alterarmunicipio();">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="F_municipio_pesquisa" value="Busca Ag�ncias" onClick="javascript:submitform();">
			<%end if%>
		</td>
	</tr>
	<tr>
		<td class='td_label'>Ag�ncia:</td>
		<td class='td_dado'>
		<%
		agencia = Cstr(request("F_agencia"))
		%>
		<Select name="F_agencia">
			<option value="" <%If agencia = "" Then Response.Write("selected")%>> Todas </option>
			<%
			If Trim(estado) <> "" And Trim(municipio) <> "" Then
				objcon.pMUNICIPIO_ID = trim(request("F_municipio"))
				objcon.pMUNICIPIO = trim(request("F_municipio_nome"))
				set rsAgencia = objcon.mLerAgencia
			    Do While not rsAgencia.EOF
			%>
			<option value="<%=rsAgencia("agencia_id")%>" <%if agencia = Trim(Cstr(rsAgencia("agencia_id"))) then response.write("Selected")%>><%= rsAgencia("agencia_id") %> - <%= rsAgencia("nome") %></option>
			<%
					rsAgencia.MoveNext
				Loop
			End If
			%>
		</select> <!--a style="color: white;" href="javascript:abre_janela('pesqAgencia.asp','agencias','no','yes',750,300,55,55);">pesquisar</a-->
			<br>ou c�digo da ag�ncia&nbsp;&nbsp;&nbsp;<input type="text" name="F_agencia_id" size="4" maxlength="4" value="<%=request("F_agencia_id")%>" onBlur="javascript:localizaragencia();">
		</td>
	</tr>
	<tr>
		<td class='td_label'>Atividade:</td>
		<td class='td_dado'>
			<select name="F_tp_ativ_id_informado">
				<OPTION value="" selected> Todas </OPTION>
				<%
				If Trim(tp_wf_id) <> "" Then
					sql = "exec w_ler_atividades_sps " & tp_wf_id
					Set rsAtividade = conex.execute(sql)
				    Do While NOT rsAtividade.EOF
					   Response.Write("<option value=""" & rsAtividade("tp_ativ_id") & """>" & rsAtividade("tp_ativ_id") & " - " & rsAtividade("nome") & "</option>")
					   rsAtividade.movenext
					Loop
				End If
				%>
			</select>&nbsp;
		</td>
	</tr>
	<tr>
		<td class='td_label'>Situa��o da Atividade:</td>
		<td class='td_dado'>
			<SELECT NAME="F_ativ_situacao">
				<OPTION value="" selected> Todos </OPTION>
				<OPTION value="1"<%If Request("F_ativ_situacao") = "1" Then%> selected<%End If%>> 
				Habilitada </OPTION>
				<OPTION value="5"<%If Request("F_ativ_situacao") = "5" Then%> selected<%End If%>> 
				Executada </OPTION>
			</SELECT>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td colspan="2" align="center">
			<input type="hidden" name="filtro" value="1">
			<INPUT type="submit" Value="<%=btContinua%>">
			<INPUT TYPE="button" Value="<%=btLimpa%> Dados" onClick="javascript:limparform();">
			<INPUT TYPE="Button" Value="<%=btVolta%>" OnClick="top.close();">
		</td>
	</tr>
</table>
<script>
<%if trim(request("F_tp_ini")) <> "" then%>
	camposData('<%=request("F_tp_ini")%>');
<%else%>
	camposData('T');
<%end if%>
</script>
</form>
</html>
<!--#include virtual = "/funcao/close_conex.asp"-->