<script>
function processasubgrupo()
{
	try { document.formProc.NR_SEQL_CBT_CTC.selectedIndex = 0; } catch (ex) { }
	document.formProc.submit();
}
</script>
<tr>
	<td colspan="2"><br>
		<script>
		function mostra_div(a) 
		{
			esconde_div();
			if (a.style.display =='')
				a.style.display = 'none';
			else
				a.style.display='';
		}
		</script>
		<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#DDDDDD">
			<tr>
				<td class="td_label_negrito" style="text-align:center">
					Cl�usulas Gerais
				</td>
			</tr>
			<tr>
				<td>
					<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
					<%
					objcon.pTipo = "1"
					vContaClsl = 1
					set rsCobertura = objcon.mLerDadosClausula()
					rsCobertura.ActiveConnection = Nothing
					If NOT rsCobertura.EOF Then
						%>
						<tr>
							<td class="td_label_negrito" width="80">C�d. Cl�usula</td>
							<td class="td_label_negrito">T�tulo</td>
						</tr>
						<%
						do while not rsCobertura.eof
							alterna_cor
							%>
							<tr>
								<td class="td_dado" style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazioN(rsCobertura,"cd_clsl")%></td>
								<td class="td_dado" style="<%=cor_zebra%>">&nbsp;<a onclick="mostra_div(tr_<%=rsCobertura("CD_CLSL")%>_<%=vContaClsl%>)" style="text-decoration:none;color:#0000FF;cursor:hand"><%= testaDadoVazioN(rsCobertura,"descr_clausula")%></td>
							</tr>
							<tr id="tr_<%=rsCobertura("CD_CLSL")%>_<%=vContaClsl%>" style="display:none">
								<td class="td_dado" style="<%=cor_zebra%>">&nbsp;</td>
								<td class="td_dado" style="<%=cor_zebra%>"><%= Replace(testaDadoVazio(rsCobertura,"texto"), Chr(13) + chr(10), "<br>")%>&nbsp;</td>
							</tr>
							<%
							vContaClsl = vContaClsl + 1
							rsCobertura.movenext
						loop%>
					<%else%>
						<tr>
							<td class="td_label" style="text-align:center">&nbsp;Nenhuma cl�usula relacionada.</td>
						</tr>
					<%end if%>
					</table>
				</td>
			</tr>
			<tr>
				<td class="td_label_negrito" style="text-align:center">
					Cl�usulas Especiais
				</td>
			</tr>
			<tr>
				<td>
					<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
					<%
					objcon.pTipo = "2"
					set rsCobertura = objcon.mLerDadosClausula()
					rsCobertura.ActiveConnection = Nothing
					if not rsCobertura.eof then
					%>
						<tr>
							<td class="td_label_negrito" width="80">C�d. Cl�usula</td>
							<td class="td_label_negrito">T�tulo</td>
						</tr>
						<%
						do while not rsCobertura.eof
							alterna_cor
							%>
							<tr>
								<td class="td_dado" style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazioN(rsCobertura,"cd_clsl")%></td>
								<td class="td_dado" style="<%=cor_zebra%>">&nbsp;<a onclick="mostra_div(tr_<%=rsCobertura("CD_CLSL")%>_<%=vContaClsl%>)" style="text-decoration:none;color:#0000FF;cursor:hand"><%= testaDadoVazioN(rsCobertura,"descr_clausula")%></td>
							</tr>
							<tr id="tr_<%=rsCobertura("CD_CLSL")%>_<%=vContaClsl%>" style="display:none">
								<td class="td_dado" style="<%=cor_zebra%>">&nbsp;</td>
								<td class="td_dado" style="<%=cor_zebra%>">
								<%
								objCon.pCD_CLSL = rsCobertura("CD_CLSL")
								set rsTexto = objCon.mLerTextoClausula()
                                rsTexto.ActiveConnection = Nothing
								if not rsTexto.eof then
									Response.Write( Replace(testaDadoVazio(rsTexto,"TEXTO"), Chr(13) + chr(10), "<br>") )
								end if
								set rsTexto = nothing
								%>&nbsp;</td>
							</tr>
							<%
							vContaClsl = vContaClsl + 1
							rsCobertura.movenext
						loop
					  else%>
						<tr>
							<td class="td_label" style="text-align:center">&nbsp;Nenhuma cl�usula relacionada.</td>
						</tr>
					<%end if%>
					</table>
				</td>
			</tr>
			<tr>
				<td class="td_label_negrito" style="text-align:center">
					Cl�usulas Facultativas
				</td>
			</tr>
			<tr>
				<td>
					<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
					<%
					objcon.pTipo = "5"
					set rsCobertura = objcon.mLerDadosClausula()
					rsCobertura.ActiveConnection = Nothing
					if not rsCobertura.eof then
					%>
						<tr>
							<td class="td_label_negrito" width="80">C�d. Cl�usula</td>
							<td class="td_label_negrito">T�tulo</td>
						</tr>
						<%
						NR_SEQL_CLSL_CTC = 1
						do while not rsCobertura.EOF
							alterna_cor
							%>
							<tr>
								<td class="td_dado" style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazioN(rsCobertura,"cd_clsl")%></td>
								<td class="td_dado" style="<%=cor_zebra%>">&nbsp;<a onclick="mostra_div(tr_<%=rsCobertura("CD_CLSL")%>_<%=vContaClsl%>)" style="text-decoration:none;color:#0000FF;cursor:hand"><%= testaDadoVazioN(rsCobertura,"descr_clausula")%></td>
							</tr>
							<tr id="tr_<%=rsCobertura("CD_CLSL")%>_<%=vContaClsl%>" style="display:none">
								<td class="td_dado" style="<%=cor_zebra%>">&nbsp;</td>
								<td class="td_dado" style="<%=cor_zebra%>"><%= Replace(testaDadoVazio(rsCobertura,"texto"), Chr(13) + chr(10), "<br>")%>&nbsp;</td>
							</tr>
							<%
							vContaClsl = vContaClsl + 1
							rsCobertura.movenext
							NR_SEQL_CLSL_CTC = NR_SEQL_CLSL_CTC + 1
						loop%>
					<%else%>
						<tr>
							<td class="td_dado" style="text-align:center">&nbsp;A ser selecionada na cota��o.</td>
						</tr>
					<%end if%>
					</table>
				</td>
			</tr>
		</table>
		<script>
		<!--
		function esconde_div()
		{
			var allDivs = new Array(<%=DivIds%>);
			for(i=0;i<allDivs.length;i++)
			{
				var tr = eval("tr_"+allDivs[i]);
				tr.style.display = 'none';
			}
		}
		//-->
		</script>
	</td>
</tr>