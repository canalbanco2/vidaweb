<!--#include virtual = "/internet/serv/siscot3/classe/citemtreeview.asp"-->
<br>
<input type="hidden" name="gravarDados" value="N">
<input type="hidden" name="excluirDados" value="N">
&nbsp;&nbsp;&nbsp;
<input type="button" name="gravar" value="Gravar <%If CBool(ValidaTipoWorkflowGeral(tp_wf_id, GERAL_WF_VG)) Then Response.Write "Subgrupo" Else Response.Write "Objeto de Risco"%>" style="width:120px;font-size:10px;margin: 2px 2px 2px 2px;" onclick="Fgravar()">
<input type="button" name="excluir" value="Excluir <%If CBool(ValidaTipoWorkflowGeral(tp_wf_id, GERAL_WF_VG)) Then Response.Write "Subgrupo" Else Response.Write "Objeto de Risco"%>" style="width:120px;font-size:10px;margin: 2px 2px 2px 2px;" onclick="Fexcluir()">
<br><br>
<input type="hidden" value="N" name="gravar">
<input type="hidden" value="N" name="coletarDadosBanco">
<script>
<!--
	function Fgravar(){
		document.all.dadosABAS.innerHTML = "";
		gravouDado();
		document.formProc.action = "";
		document.formProc.gravarDados.value = "S";
		document.formProc.submit();
	}
	function Fexcluir(){
		gravouDado();
		document.formProc.action = "";
		document.formProc.excluirDados.value = "S";
		document.formProc.coletarDadosBanco.value = "S";
		document.formProc.submit();
	}
	function trocaObjetoRisco(obj){
		if(aviso()){
			document.formProc.action = "";
			gravouDado();
			document.formProc.submit();
		} else {
			obj.selectedIndex = valorComboAnterior;
		}
	}
	var valorComboAnterior;
//-->
</script>
<!--#include virtual = "/internet/serv/siscot3/rpc/dados_obj_gravar.asp" -->
<%
var_NR_CTC_ATUAL = Request("NM_PRPN_CTC")
If var_NR_CTC_ATUAL = "" Then
    var_NR_CTC_ATUAL = "NOVO"
End If
var_NR_SEQL_QSTN_CTC = Request("seq_risc")

If Request("gravardados") = "S" And Request("reenvia") = "" Then
    perg_id = ""
	erro = testarRespostasObrigatorias("",perg_id)

	If erro = "" Then
       erro = mGravaLocalRisco("")
	End If
	If erro <> "" Then
	   Response.Write("<br>" & erro)
	End If
End If

If Request("excluirDados") = "S" Then
    If CBool(ValidaTipoWorkflowGeral(tp_wf_id, GERAL_WF_VG)) Then
       preencheChaves()
       NR_CTC_NOVO_array   = Split(var_NR_CTC_ATUAL&"|","|")
       objCon.pNR_CTC_SGRO = NR_CTC_NOVO_array(0)
       objCon.pNR_VRS_CTC  = NR_CTC_NOVO_array(1)
       erro = objCon.mExcluirCadastroCotacao()
    Else
       erro = excluirQuestionario("", 2, 0, var_NR_SEQL_QSTN_CTC)
    End If
End If

preencheChaves()
If CBool(ValidaTipoWorkflowGeral(tp_wf_id, GERAL_WF_VG)) Then
    Set rsSubgrupo = objcon.mLerSubgrupoCotacao
    rsSubGrupo.ActiveConnection = Nothing
    numProxSubgrupo = 0
%>
   <table cellpadding="2" cellspacing="1" border="0" class="escolha" width="343">
	 <tr>
		<td class="td_label" width="91">Subgrupo</td>
		<td class="td_dado" width="241">&nbsp;
			<select name="NM_PRPN_CTC" onchange="trocaObjetoRisco(this)" onclick="valorComboAnterior = this.selectedIndex;">
			<option value="">--Escolha abaixo--</option>
			<%
			Do While NOT rsSubgrupo.EOF
			    numProxSubgrupo = rsSubgrupo("SUBGRUPO_ID")
				If var_NR_CTC_ATUAL = Trim(rsSubgrupo("NR_CTC_SGRO")) & "|" & Trim(rsSubgrupo("NR_VRS_CTC")) Then
					sSelecionado = " selected"
					objCon.pNR_CTC_SGRO = rsSubgrupo("NR_CTC_SGRO")
                    objCon.pNR_VRS_CTC  = rsSubgrupo("NR_VRS_CTC")
                    cdCategoria  = rsSubgrupo("CD_CTGR_PSS_CTC")
                    cdProponente = rsSubgrupo("NM_PRPN_CTC")
                    cdDocumento  = rsSubgrupo("NR_SRF_PRPN_CTC")
				Else
					sSelecionado = ""
				End If
				%>
				<option value="<%=Trim(rsSubgrupo("NR_CTC_SGRO")) & "|" & Trim(rsSubgrupo("NR_VRS_CTC"))%>"<%=sSelecionado%>><%=Right(00 & rsSubgrupo("subgrupo_id"),2) & " (" & Trim(rsSubgrupo("NR_CTC_SGRO")) & ")"%> - <%=Trim(rsSubgrupo("NM_PRPN_CTC"))%></option>
				<%
				rsSubgrupo.MoveNext
			Loop%>
			<option value="NOVO" <%If var_NR_CTC_ATUAL="NOVO" Then Response.Write "selected"%>>Novo Subgrupo</option>
			</select>
			<input type="hidden" name="numProxSubgrupo" value=<%=numProxSubgrupo+1%>>
            <input type="hidden" name="seq_risc" value="1">
		</td>
	 </tr>  
   </table>

   <table cellpadding="2" cellspacing="1" border="0" class="escolha" width="648">
	 <tr>
		<td colspan="4" class="td_label">Proponente</td></tr>
	 <tr>
		<td class="td_label" width="91">Categoria:</td>
		<td class="td_dado" width="242">
			&nbsp;
			<select name="vd_categoria" onchange="document.formProc.gravarDados.value='S';">
				<option value="">-- Escolha Abaixo --</option>
				<option value="1" <%If cdCategoria = 1 Then Response.Write "selected"%>>F�sica</option>
			    <option value="2" <%If cdCategoria = 2 Then Response.Write "selected"%>>Jur�dica</option>
				<option value="3" <%If cdCategoria = 3 Then Response.Write "selected"%>>Funcion�rio do Banco do Brasil</option>
				<option value="4" <%If cdCategoria = 4 Then Response.Write "selected"%>>Funcion�rio do Banco Central</option>
				<option value="5" <%If cdCategoria = 5 Then Response.Write "selected"%>>Funcion�rio P�blico Federal</option>
				<option value="6" <%If cdCategoria = 6 Then Response.Write "selected"%>>Funcion�rio da Alian�a do Brasil</option>
				<option value="7" <%If cdCategoria = 7 Then Response.Write "selected"%>>Mutu�rios PREVI</option>
			</select>
		</td>
		<td class="td_label" width="75" id="vd_cnpj_cpf" name="vd_cnpj_cpf">CPF/CNPJ:</td>
		<td class="td_dado" width="219">
            <input type="text" name="vd_cnpj" maxlength="18" size="20" value="<%=cdDocumento%>"></td>
		</tr>
	 <tr>
		<td class="td_label" width="91">Nome:</td>
		<td class="td_dado" width="539" colspan="3">
            &nbsp;
            <input type="text" name="vd_nome" maxlength="60" size="74" value="<%=cdProponente%>"></td>
		</tr>
   </table>
<% 
   preencheChaves
   If var_NR_CTC_ATUAL <> "NOVO" Then
      NR_CTC_NOVO_array = Split(var_NR_CTC_ATUAL&"|","|")
      objCon.pNR_CTC_SGRO = NR_CTC_NOVO_array(0)
      objCon.pNR_VRS_CTC  = NR_CTC_NOVO_array(1)
   End If
   var_NR_SEQL_QSTN_CTC = 1
Else
   Set rsOBJRisc = objCon.mLerSequencialRiscoQuestionario()
   rsOBJRisc.ActiveConnection = Nothing
   campo = 0
%>
   <table cellpadding="2" cellspacing="1" border="0" class="escolha">
	<tr>
		<td class="td_label">Risco</td>
		<td class="td_dado">
			<select name="seq_risc" onchange="trocaObjetoRisco(this)" onclick="valorComboAnterior = this.selectedIndex;"><%
				Do while NOT rsOBJRisc.EOF %>
				<option value="<%=rsOBJRisc("NR_SEQL_QSTN_CTC")%>" <%If Trim(var_NR_SEQL_QSTN_CTC) = Trim(rsOBJRisc("NR_SEQL_QSTN_CTC")) Then Response.Write " selected"%>><%=rsOBJRisc("NR_SEQL_QSTN_CTC")%></option>
				<%
				    campo = rsOBJRisc("NR_SEQL_QSTN_CTC")
				    rsOBJRisc.MoveNext
				Loop

                campo = campo + 1
				If var_NR_SEQL_QSTN_CTC = "" Then
			       var_NR_SEQL_QSTN_CTC = campo
			    End If
				%>
				<option value="<%=campo%>" <%If Trim(var_NR_SEQL_QSTN_CTC) = Trim(campo) Then Response.Write " selected"%>><%=campo%></option>
			</select>
		</td>
	</tr>
</table>
<%
End If

   preencheChaves
   objCon.pCD_TIP_QSTN = 2
   Set rsQuestionario = objCon.mLerQuestionarioId()
   rsQuestionario.ActiveConnection = Nothing
   combo = True
   If NOT rsQuestionario.EOF Then
	   var_CD_TIP_QSTN = 2
	   questionario_id = rsQuestionario("NR_QSTN")
%>
	<!--#include virtual = "/internet/serv/siscot3/funcoes_dados_qst.asp"-->
	<!--#include virtual = "/internet/serv/siscot3/dados_qst_preenchimento.asp"-->
<% 
   End If%>
<center><a href="#inicio">Voltar ao inicio da p�gina</a></center>