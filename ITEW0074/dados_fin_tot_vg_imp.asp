	<%
	If valor_is_total_t > 0 Then
		taxa_media_mensal = CortaCasasDecimais(((valor_premio_liquido_total_t / valor_is_total_t) * 1000), 4)
	Else
		taxa_media_mensal = 0
	End If
	%>
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
				<tr>
					<td colspan="2" class="td_label_negrito">Dados Financeiros Totalizados</td>
				</tr>
				<tr>
					<td width="180" nowrap class="td_label">&nbsp;Valor total de IS</td>
					<td class="td_dado">&nbsp;<%=FormatNumber(valor_is_total_t, 2)%></td>
				</tr>
				<tr>
					<td nowrap class="td_label">&nbsp;Valor total do pr�mio puro</td>
					<td class="td_dado">&nbsp;<%=FormatNumber(valor_premio_puro_total_t, 2)%></td>
				</tr>
				<tr>
					<td nowrap class="td_label">&nbsp;Valor total do pr�mio l�quido</td>
					<td class="td_dado">&nbsp;<%=FormatNumber(valor_premio_liquido_total_t, 2)%></td>
				</tr>
				<tr>
					<td nowrap class="td_label">&nbsp;Taxa m�dia mensal</td>
					<td class="td_dado">&nbsp;<%=FormatNumber(taxa_media_mensal, 4)%></td>
				</tr>
			</table>
		</td>
	</tr>