<%
set rs = objcon.mLerDadosFinanceiros()

if trim(request("salvarfin")) = "S" then
	IN_FAT_UNCO_SGR = trim(request("IN_FAT_UNCO_SGR"))
	PC_CRE_CTC = trim(request("PC_CRE_CTC"))
	PC_IOF_CTC = trim(request("PC_IOF_CTC"))
	VL_IOF_CTC = trim(request("VL_IOF_CTC"))
	QT_PCL_PGTO_CTC = trim(request("QT_PCL_PGTO_CTC"))
	PC_ETLE_CTC = trim(request("PC_ETLE_CTC"))
	VL_LQDO_MOEN_CTC = trim(request("VL_LQDO_MOEN_CTC"))
	PC_DA_CTC = trim(request("PC_DA_CTC"))
	VL_PREM_PURO = trim(request("VL_PREM_PURO"))
	VL_SGRA_MOEN_CTC = trim(request("VL_SGRA_MOEN_CTC"))
	VL_IPTC_MOEN_CTC = trim(request("VL_IPTC_MOEN_CTC"))
	VL_IOF_CTC = trim(request("VL_IOF_CTC"))
	PC_MED_SGR = trim(request("PC_MED_SGR"))
else
	IN_FAT_UNCO_SGR = trata_sim(testaDadoVazio(rs,"IN_FAT_UNCO_SGR"))
	PC_CRE_CTC = testaDadoVazioN(rs,"PC_CRE_CTC")
	PC_IOF_CTC = testaDadoVazioN(rs,"PC_IOF_CTC")
	VL_IOF_CTC = testaDadoVazioN(rs,"VL_IOF_CTC")
	QT_PCL_PGTO_CTC = testaDadoVazioN(rs,"QT_PCL_PGTO_CTC")
	PC_ETLE_CTC = testaDadoVazioN(rs,"PC_ETLE_CTC")
	VL_LQDO_MOEN_CTC = testaDadoVazioN(rs,"VL_LQDO_MOEN_CTC")
	PC_DA_CTC = testaDadoVazioN(rs,"PC_DA_CTC")
	VL_PREM_PURO = testaDadoVazioN(rs,"VL_PREM_PURO")
	VL_SGRA_MOEN_CTC = testaDadoVazioN(rs,"VL_SGRA_MOEN_CTC")
	VL_IPTC_MOEN_CTC = testaDadoVazioN(rs,"VL_IPTC_MOEN_CTC")
	VL_IOF_CTC = testaDadoVazioN(rs,"VL_IOF_CTC")
	PC_MED_SGR = testaDadoVazioN(rs,"PC_MED_SGR")
end if

set rsCotacao = objcon.mLerCotacao()
data_fim_vigencia = rsCotacao("DT_INC_VGC_CTC")
data_inicio_vigencia = rsCotacao("DT_FIM_VGC_CTC")
set rsCotacao = nothing
if not isnull(data_fim_vigencia) and not isnull(data_inicio_vigencia) then
	dias = datediff("d",data_fim_vigencia,data_inicio_vigencia)
	parcelas = int(dias/30)
else
	dias = 0
	parcelas = QT_PCL_PGTO_CTC
end if

if trim(VL_IPTC_MOEN_CTC) = "" then
	VL_IPTC_MOEN_CTC = "0,00"
end if
if trim(VL_PREM_MOEN_CTC) = "" then
	VL_PREM_MOEN_CTC = "0,00"
end if

if trim(IN_FAT_UNCO_SGR) = "S" then
	estilo_subgrupo_edicao_a = "style=""text-align:right; background-color:#EEEEEE;"" readonly"
	estilo_subgrupo_edicao_b = "style=""text-align:right"""
else
	estilo_subgrupo_edicao_a = "style=""text-align:right"""
	estilo_subgrupo_edicao_b = "style=""text-align:right; background-color:#EEEEEE;"" readonly"
end if

'Verifica se o produto aceita averba��o
'objCon.pTP_PESQ = "BB"
'vAceitaAverbacao = UCase(objCon.mLerIndAceitaAverbacao())
'vAceitaAverbacao = "N" 'TESTE

%>
<script language="JavaScript">
<!--
function FormatNumber(vr,tamdec,truncate){
	//A fun��o espera receber casas decimais com V�RGULA
	// truncate = 1 - n�o altera o valor de casas decimais, caso seja maior do que o tamanho decidido
	// truncate = 2 - remove os valores superiores as casa decimais
	// truncate = 3 - remove os valores superiores as casa decimais e arredonda caso haja necessidade
	vp = '';
	for(f=0;f<vr.length;f++){
		if(((vr.charCodeAt(f)>=48)&&(vr.charCodeAt(f)<=57))||(vr.charCodeAt(f)==44)){
			vp=vp+vr.charAt(f);
		}
	}
	a=1;
	vr = vp;
	if(vr.length>0){
		vr = vr.split(',');
		vrIni = vr[0];
		vrIniTemp = '';
		for(f=0;f<vrIni.length;f++){
			vrIniTemp = vrIni.charAt(vrIni.length-f-1) + vrIniTemp;
			if((a==3)&&(vrIni.length!=f+1)){
				vrIniTemp = '.' + vrIniTemp;
				a=0;
			}
			a++;
		}
		vrIni = vrIniTemp;
		vrFim = "";
		if(vr.length>1){
			vrFim = vr[1];
			if(vrFim.length>tamdec){
				if(truncate==3){
					valorFinal = vrFim.substring(tamdec,tamdec + 1);
					vrFim = vrFim.substring(0,tamdec);
					if(valorFinal>4){
						vrFim = vrFim/1 + 1;
					}
				} else if(truncate==2){
					vrFim = vrFim.substring(0,tamdec);
				}
			} else {
				for(f=vrFim.length;f<tamdec;f++){
					vrFim = vrFim + '0';
				}
			}
		} else {
			for(f=0;f<tamdec;f++){
				vrFim = vrFim + '0';
			}
		}
		vr = vrIni + ',' + vrFim;
		return vr;
	}
}

function AtribuirPagtoParcelaHidden()
{
	document.formProc.QT_PCL_PGTO_CTC.value = document.formProc.rQT_PCL_PGTO_CTC.value;
}

function VerificarParcelamento(qtde_dias)
{
	AtribuirPagtoParcelaHidden();
	
	var qtde_meses = parseFloat((qtde_dias / 30));
	
	if (qtde_meses.toString().indexOf('.') >= 0)
		qtde_meses = parseInt(qtde_meses.toString().substring(0,qtde_meses.toString().indexOf('.')));
	
	if (document.formProc.QT_PCL_PGTO_CTC.value > qtde_meses)
		alert('A quantidade de parcelas � maior que os meses de vig�ncia do seguro');
}

function preencheCampos(iElemento)
{
	habilitaCampos(true);

	document.formProc.elemento.value = iElemento;

	document.formProc.VL_IOF_CTC.value = document.getElementById('valor_iof' + iElemento).innerHTML;
	document.formProc.per_fat_seguro.value = document.getElementById('per_fat_seguro' + iElemento).innerHTML;
	document.formProc.VL_SGRA_MOEN_CTC.value = document.getElementById('valor_premio_net' + iElemento).innerHTML;
	document.formProc.PC_MED_SGR.value = document.getElementById('taxa_comercial' + iElemento).innerHTML;
	
	document.formProc.VL_IPTC_MOEN_CTC.value = document.getElementById('VL_IPTC_MOEN_CTC' + iElemento).innerHTML;
	document.formProc.PC_DA_CTC.value = document.getElementById('PC_DA_CTC' + iElemento).innerHTML;
	document.formProc.PC_ETLE_CTC.value = document.getElementById('PC_ETLE_CTC' + iElemento).innerHTML;
	document.formProc.VL_PREM_PURO.value = document.getElementById('VL_PREM_PURO' + iElemento).innerHTML;
	document.formProc.VL_LQDO_MOEN_CTC.value = document.getElementById('VL_LQDO_MOEN_CTC' + iElemento).innerHTML;
	document.formProc.valor_pro_labore.value = document.getElementById('valor_pro_labore' + iElemento).innerHTML;

	eval('document.formProc.NR_CTC_SGRO_subgrupo.value = document.formProc.NR_CTC_SGRO_subgrupo' + iElemento + '.value');
	eval('document.formProc.NR_VRS_CTC_subgrupo.value = document.formProc.NR_VRS_CTC_subgrupo' + iElemento + '.value');

	calculaTotais('C');
	
	escreveFaturamento(document.formProc.per_fat_seguro.value);
}


var valor_is_total = 0;
var valor_premio_puro_total = 0;
var valor_premio_liquido_total = 0;
var valor_premio_net_total = 0;

function calculaTotais(tipo)
{

	if (tipo == 'C') //carregar dados
	{
		document.formProc.valor_is_total.value = document.formProc.valor_is_total_or.value;
		document.formProc.valor_premio_puro_total.value = document.formProc.valor_premio_puro_total_or.value;
		document.formProc.valor_premio_liquido_total.value = document.formProc.valor_premio_liquido_total_or.value
		document.formProc.taxa_media_mensal.value = document.formProc.taxa_media_mensal_or.value
		
		valor_is_total = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_is_total.value)) -
						 parseFloat('0' + converteNumeroJavascript(document.formProc.VL_IPTC_MOEN_CTC.value));

		valor_premio_puro_total = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_premio_puro_total.value)) -
								  parseFloat('0' + converteNumeroJavascript(document.formProc.VL_PREM_PURO.value));

		valor_premio_liquido_total = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_premio_liquido_total.value)) -
									 parseFloat('0' + converteNumeroJavascript(document.formProc.VL_LQDO_MOEN_CTC.value));

		valor_premio_net_total = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_premio_net_total.value)) -
								 parseFloat('0' + converteNumeroJavascript(document.formProc.VL_SGRA_MOEN_CTC.value));

	} 
	else if (tipo == 'S') //salvar dados
	{
		var valor_is_total_temp = parseFloat(valor_is_total) +
								  parseFloat('0' + converteNumeroJavascript(document.formProc.VL_IPTC_MOEN_CTC.value));
		
		document.formProc.valor_is_total.value = FormatNumber(valor_is_total_temp.toString().replace(".", ","), 2, 2);


		var valor_premio_puro_total_temp = parseFloat(valor_premio_puro_total) +
										   parseFloat('0' + converteNumeroJavascript(document.formProc.VL_PREM_PURO.value));

		document.formProc.valor_premio_puro_total.value = FormatNumber(valor_premio_puro_total_temp.toString().replace(".", ","), 2, 2);

										  
		var valor_premio_liquido_total_temp = parseFloat(valor_premio_liquido_total) +
											  parseFloat('0' + converteNumeroJavascript(document.formProc.VL_LQDO_MOEN_CTC.value));

		document.formProc.valor_premio_liquido_total.value = FormatNumber(valor_premio_liquido_total_temp.toString().replace(".", ","), 2, 2);

		var valor_premio_net_total_temp = parseFloat(valor_premio_net_total) +
										  parseFloat('0' + converteNumeroJavascript(document.formProc.VL_SGRA_MOEN_CTC.value));

		document.formProc.valor_premio_net_total.value = FormatNumber(valor_premio_net_total_temp.toString().replace(".", ","), 2, 2);

		if (valor_is_total_temp > 0)
			var taxa_media_mensal_temp = parseFloat((valor_premio_liquido_total_temp / valor_is_total_temp) * 1000);
		else
			var taxa_media_mensal_temp = 0;

		document.formProc.taxa_media_mensal.value = FormatNumber(taxa_media_mensal_temp.toString().replace(".", ","), 4, 3);
	}
}

function calculaValores()
{
	var per_fat_seguro = parseFloat('0' + converteNumeroJavascript(document.formProc.per_fat_seguro.value));

	var percentual_corretagem = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_CRE_CTC.value));
	var percentual_iof = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_IOF_CTC.value));
	var percentual_da = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_DA_CTC.value));
	var percentual_pro_labore = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_ETLE_CTC.value));
	var valor_premio_puro = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_PREM_PURO.value));
	var valor_is = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_IPTC_MOEN_CTC.value));

	if ((valor_premio_puro > 0) && (percentual_da > 0))
		var valor_premio_net = (valor_premio_puro / (1 - (percentual_da/100)));
	else
		var valor_premio_net = 0;
		
	if ((valor_premio_puro > 0) && (percentual_da > 0) && (percentual_corretagem > 0) && (percentual_iof > 0))
		var valor_premio_liquido = ((valor_premio_puro / (1 - (percentual_da/100))) / (1 - ((percentual_pro_labore/100) + (percentual_corretagem/100)))) * (1 + (percentual_iof/100));
	else
		var valor_premio_liquido = 0

	if ((percentual_pro_labore > 0) && (valor_premio_puro > 0))
		var valor_pro_labore = ((valor_premio_puro / (1 - (percentual_da/100))) * percentual_pro_labore) / 100;
	else
		var valor_pro_labore = 0;
	
	if ((valor_is > 0) && (valor_premio_liquido > 0) && (per_fat_seguro > 0))
		var taxa_comercial = ((valor_premio_liquido * per_fat_seguro) / valor_is) * 1000;
	else
		var taxa_comercial = 0;
		
	if ((valor_premio_puro > 0) && (percentual_iof > 0))
		var valor_iof = ((valor_premio_puro / (1 - (percentual_da/100))) * percentual_iof) / 100;
	else
		var valor_iof = 0;

	document.formProc.VL_LQDO_MOEN_CTC.value = FormatNumber(valor_premio_liquido.toString().replace(".", ","), 2, 2);
	document.formProc.PC_MED_SGR.value = FormatNumber(taxa_comercial.toString().replace(".", ","), 4, 2);
	document.formProc.valor_pro_labore.value = FormatNumber(valor_pro_labore.toString().replace(".", ","), 4, 2);
	document.formProc.VL_SGRA_MOEN_CTC.value = FormatNumber(valor_premio_net.toString().replace(".", ","), 4, 2);
	document.formProc.VL_IOF_CTC.value = FormatNumber(valor_iof.toString().replace(".", ","), 2, 2);

	calculaTotais('S');
}

function escreveFaturamento(faturamento)
{
	var faturamento_label = ''
	switch (parseInt(faturamento))
	{
		case 1: faturamento_label = 'mensal'; break;
		case 3: faturamento_label = 'trimestral'; break;
		case 6: faturamento_label = 'semestral'; break;
		case 12: faturamento_label = 'anual'; break;
	}
	
	document.all.faturamento.innerHTML = faturamento_label;
	document.formProc.periodicidade_faturamento.value = faturamento_label;
}

function converteNumeroJavascript(valor){
	valor = valor.split(',');
	valorNovo = '';
	for(f=0;f<valor[0].length;f++){
		if((valor[0].charCodeAt(f)>=48)&&(valor[0].charCodeAt(f)<=57)){
			valorNovo=valorNovo+valor[0].charAt(f);
		}
	}
	if(valor.length>1){
		valorNovo = valorNovo + '.' + valor[1];
	}
	return valorNovo;
}

function habilitaCampos(bHabilita)
{
	if (document.formProc.PC_DA_CTC.disabled == !bHabilita)
		return;
	
	document.formProc.PC_DA_CTC.disabled = !bHabilita;
	document.formProc.PC_ETLE_CTC.disabled = !bHabilita;
	document.formProc.VL_PREM_PURO.disabled = !bHabilita;
	document.formProc.VL_IPTC_MOEN_CTC.disabled = !bHabilita;
	document.formProc.PC_MED_SGR.disabled = !bHabilita;
	document.formProc.valor_pro_labore.disabled = !bHabilita;
}

function submitdadosfinanceiros()
{
	document.formProc.action = 'liberar_cotacao_fin.asp';
	document.formProc.submit();
}

function alterarDados()
{
	if (validaform())
	{
		document.formProc.salvarfin.value = 'S';
		submitdadosfinanceiros();
	}
}

function validaform()
{
	if (parseFloat('0' + converteNumeroJavascript(document.formProc.PC_DA_CTC.value)) <= 0)
	{
		document.all.msgerro.innerHTML = '<br>O Percentual DA deve ser maior que 0<br><br>';
		return false;
	}
	
	return true;
}
-->
</script>
	<input type="hidden" name="salvarfin" value="">
<%
strMensagem = ""

set rsSubgrupo = objcon.mLerSubgrupoCotacao

if rsSubgrupo.eof then
	bSubgrupo = false
else
	bSubgrupo = true
end if

if trim(request("salvarfin")) = "S" and ((trim(request("NR_CTC_SGRO_subgrupo")) <> "" and trim(request("NR_VRS_CTC_subgrupo")) <> "") or not bSubgrupo) then
	set rsCotacao = objCon.mLerCotacao

	if ucase(rsCotacao("CD_ULT_EVT_CTC")) <> "T" then
		objcon.pCD_ULT_EVT_CTC = "T"
		bAtualizaCotacao = true
	else
		bAtualizaCotacao = false
	end if

	chave

	objCon.pVL_IOF_CTC = VL_IOF_CTC
	objCon.pQT_PCL_PGTO_CTC = QT_PCL_PGTO_CTC
	objCon.pPC_ETLE_CTC = PC_ETLE_CTC
	objCon.pPC_DA_CTC = PC_DA_CTC
	objCon.pVL_PREM_PURO = VL_PREM_PURO
	objCon.pVL_SGRA_MOEN_CTC = VL_SGRA_MOEN_CTC
	objCon.pVL_IPTC_MOEN_CTC = VL_IPTC_MOEN_CTC
	objCon.pVL_LQDO_MOEN_CTC = VL_LQDO_MOEN_CTC
	objCon.pPC_MED_SGR = PC_MED_SGR

	if bSubgrupo then
		NR_CTC_SGRO_subgrupo = request("NR_CTC_SGRO_subgrupo")
		NR_VRS_CTC_subgrupo = request("NR_VRS_CTC_subgrupo")
	else
		NR_CTC_SGRO_subgrupo = ""
		NR_VRS_CTC_subgrupo = ""
	end if

	objCon.pWF_ID = WF_ID
	retorno = objCon.mAtualizarDadosFinanceiros(NR_CTC_SGRO_subgrupo, _
												NR_VRS_CTC_subgrupo, _
												trim(request("valor_is_total")), _
												trim(request("valor_premio_puro_total")), _
												trim(request("valor_premio_liquido_total")), _
												trim(request("valor_premio_net_total")), _
												trim(request("taxa_media_mensal")), _
												bAtualizaCotacao)

	if trim(retorno) <> "" then
		Response.redirect("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
		Response.End 
	end if

	chave

	if not bSubgrupo then
		set rs = objcon.mLerDadosFinanceiros(2) '2 = VIDA
	end if
end if
%>
	<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual de corretagem</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_CRE_CTC" size="16" value="<%=testaDadoVazioN(rs,"PC_CRE_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
			<td nowrap class="td_label">&nbsp;Qtd m�xima de parcelas pgto</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="QT_PCL_PGTO_CTC" size="2" value="<%=QT_PCL_PGTO_CTC%>" maxlength="2" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual IOF</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_IOF_CTC" size="16" value="<%=testaDadoVazioN(rs,"PC_IOF_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
			<%if bSubgrupo then%>
			<td nowrap class="td_label">&nbsp;Taxa Personalizada por subgrupo</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="IN_FAT_UNCO_SGR" size="3" value="<%=IN_FAT_UNCO_SGR%>" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
			<%else%>
			<td colspan="2"></td>
			<%end if%>
		</tr>
		<%
		if not bSubgrupo then
			percentual_corretagem = cdbl("0" & testaDadoVazioN(rs,"PC_CRE_CTC"))
			percentual_iof = cdbl("0" & testaDadoVazioN(rs,"PC_IOF_CTC"))
			percentual_da = cdbl("0" & testaDadoVazioN(rs,"PC_DA_CTC"))
			percentual_pro_labore = cdbl("0" & testaDadoVazioN(rs,"PC_ETLE_CTC"))
			valor_premio_puro = cdbl("0" & testaDadoVazioN(rs,"VL_PREM_PURO"))
			valor_is = cdbl("0" & testaDadoVazioN(rs,"VL_IPTC_MOEN_CTC"))
			valor_premio_liquido = cdbl("0" & testaDadoVazioN(rs,"VL_LQDO_MOEN_CTC"))
			valor_premio_net = cdbl("0" & testaDadoVazioN(rs,"VL_SGRA_MOEN_CTC"))
			per_fat_seguro =  cdbl("0" & testaDadoVazioN(rs,"PER_FAT_SEGURO"))
								
			valor_iof = CortaCasasDecimais(((valor_premio_puro * percentual_iof) / 100), 2)
								
			if percentual_pro_labore > 0 and valor_premio_liquido > 0 then
				valor_pro_labore = (valor_premio_puro * percentual_pro_labore) /100
				valor_pro_labore = CortaCasasDecimais(valor_pro_labore,5)
			else
				valor_pro_labore = 0
			end if
			
			if valor_premio_liquido > 0 and valor_is > 0 and per_fat_seguro > 0 then
				taxa_comercial = CortaCasasDecimais(((valor_premio_liquido * per_fat_seguro) / valor_is) * 1000, 4)
			else
				taxa_comercial = 0
			end if

			valor_premio_puro_total = valor_premio_puro
			valor_premio_liquido_total = valor_premio_liquido
			valor_premio_net_total = valor_premio_net
			valor_is_total = valor_is
			
			if valor_is_total > 0 then
				taxa_media_mensal = CortaCasasDecimais(((valor_premio_liquido_total / valor_is_total) * 1000), 4)
			else
				taxa_media_mensal = 0
			end if
		end if
		%>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor IOF</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_IOF_CTC" size="16" value="<%=testaDadoVazioN(rs,"VL_IOF_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
			<%if trim(IN_FAT_UNCO_SGR) <> "S" then%>
			<td nowrap class="td_label">&nbsp;Percentual DA</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_DA_CTC" size="16" value="<%=formatnumber(percentual_da,2)%>" onKeyUp="FormatValorContinuo(this,2);" onBlur="calculaValores();" maxlength="16" style="text-align:right;">
			</td>
			<%else%>
			<td></td>
			<td></td>
			<%end if%>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual pro-labore</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_ETLE_CTC" size="16" value="<%=formatnumber(percentual_pro_labore,2)%>" onKeyUp="FormatValorContinuo(this,2);" onBlur="calculaValores();" maxlength="16" style="text-align:right;">
			</td>
			<td nowrap class="td_label">&nbsp;Tipo de Capital Segurado</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="XXX" size="16" value="" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor pro-labore</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="valor_pro_labore" size="16" value="<%=formatnumber(valor_pro_labore, 4)%>" onKeyUp="FormatValorContinuo(this,2);" onBlur="calculaValores();" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
			<td nowrap class="td_label">&nbsp;Valor IS</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_IPTC_MOEN_CTC" size="16" value="<%=formatnumber(valor_is,2)%>" onKeyUp="FormatValorContinuo(this,2);" onBlur="calculaValores();" maxlength="16" style="text-align:right;">
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor pr�mio puro</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_PREM_PURO" size="16" value="<%=formatnumber(valor_premio_puro,2)%>" onKeyUp="FormatValorContinuo(this,2);" onBlur="calculaValores();" maxlength="16" style="text-align:right;">
			</td>
			<td nowrap class="td_label">&nbsp;Taxa comercial
				<span id="faturamento">
				<%
				if not bSubgrupo then
					Response.Write RetornaPeriodicidade(per_fat_seguro, vAceitaAverbacao)
				end if
				%>
				</span>
			</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_MED_SGR" size="16" value="<%=formatnumber(taxa_comercial, 4)%>" onBlur="calculaValores();" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="VL_LQDO_MOEN_CTC" value="<%=formatnumber(valor_premio_liquido, 2)%>">
				<input type="hidden" name="per_fat_seguro" value="<%=formatnumber(per_fat_seguro, 0)%>">
				<input type="hidden" name="VL_SGRA_MOEN_CTC" value="<%=formatnumber(valor_premio_net, 2)%>">
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Periodicidade de Faturamento</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="periodicidade_faturamento" size="16" value="<%=RetornaPeriodicidade(per_fat_seguro, vAceitaAverbacao)%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
			<td></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" name="alterar" value="Alterar" style="width:80px;font-size:10px;margin: 2px 2px 2px 2px;" onclick="alterarDados()">
			</td>
		</tr>		
		<%if trim(IN_FAT_UNCO_SGR) = "S" then%>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual DA</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_DA_CTC" size="16" value="<%=PC_DA_CTC%>" onKeyUp="FormatValorContinuo(this,2);" onBlur="calculaValores();" maxlength="16" style="text-align:right;">
			</td>
			<td></td>
			<td></td>
		</tr>
		<%end if%>
		<tr>
			<td colspan="4" height="10"></td>
		</tr>
		<%if bSubgrupo then%>
		<tr>
			<td colspan="4">
				<div style="position:relative; overflow:scroll; width:91%; height:110px;">
					<input type="hidden" name="subgrupo" value="">
					<table cellpadding="1" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
						<tr>
							<td class="td_label" width="2%">&nbsp;</td>
							<td class="td_label" width="8%"><div align="center"><b>Subgrupo</b></div></td>
							<td class="td_label" width="16%"><div align="center"><b>Valor IS</b></div></td>
							<td class="td_label" width="16%"><div align="center"><b>Pr�mio Puro</b></div></td>
							<td class="td_label" width="16%"><div align="center"><b>Pr�mio L�quido</b></div></td>
							<td class="td_label" width="14%"><div align="center"><b>DA (%)</b></div></td>
							<td class="td_label" width="14%"><div align="center"><b>Periodicidade</b></div></td>
							<td class="td_label" width="14%"><div align="center"><b>Pro-labore (%)</b></div></td>
						</tr>
						<%
						if not rs.eof then
							iContador = 0
			
							percentual_corretagem = cdbl("0" & testaDadoVazioN(rs,"PC_CRE_CTC"))
							percentual_iof = cdbl("0" & testaDadoVazioN(rs,"PC_IOF_CTC"))

							valor_premio_puro_total = 0
							valor_premio_liquido_total = 0
							valor_is_total = 0
							valor_premio_net_total = 0
							taxa_media_mensal = 0

							do while not rsSubgrupo.eof
								alterna_cor
								iContador = iContador + 1
								
								objCon.pNR_CTC_SGRO = rsSubgrupo("NR_CTC_SGRO")
								objCon.pNR_VRS_CTC =  rsSubgrupo("NR_VRS_CTC")
								
								set rsDadosFin = objCon.mLerDadosFinanceiros

								percentual_da = cdbl("0" & testaDadoVazioN(rsDadosFin,"PC_DA_CTC"))
								percentual_pro_labore = cdbl("0" & testaDadoVazioN(rsDadosFin,"PC_ETLE_CTC"))
								valor_premio_puro = cdbl("0" & testaDadoVazioN(rsDadosFin,"VL_PREM_PURO"))
								valor_premio_net = cdbl("0" & testaDadoVazioN(rsDadosFin,"VL_SGRA_MOEN_CTC"))
								valor_is = cdbl("0" & testaDadoVazioN(rsDadosFin,"VL_IPTC_MOEN_CTC"))
								valor_premio_liquido = cdbl("0" & testaDadoVazioN(rsDadosFin,"VL_LQDO_MOEN_CTC"))
								per_fat_seguro =  cdbl("0" & testaDadoVazioN(rsDadosFin,"PER_FAT_SEGURO"))
								
								valor_iof = CortaCasasDecimais(((valor_premio_puro * percentual_iof) / 100), 2)

								'valor_premio_puro = CortaCasasDecimais((valor_premio_puro * per_fat_seguro), 2)

								if percentual_pro_labore > 0 and valor_premio_liquido > 0 then
									valor_pro_labore = (valor_premio_puro * percentual_pro_labore) /100
									valor_pro_labore = CortaCasasDecimais(valor_pro_labore,5)
								else
									valor_pro_labore = 0
								end if
								
								if valor_premio_liquido > 0 and valor_is > 0 and per_fat_seguro > 0 then
									taxa_comercial = CortaCasasDecimais(((valor_premio_liquido * per_fat_seguro) / valor_is) * 1000, 4)
								else
									taxa_comercial = 0
								end if
								
								if taxa_comercial > 0 then
									if per_fat_seguro <> 1 then
										taxa_media_mensal = CortaCasasDecimais(cdbl(taxa_media_mensal) + ((valor_premio_liquido / valor_is) * 1000), 4)
									else
										taxa_media_mensal = CortaCasasDecimais(cdbl(taxa_media_mensal) + cdbl(taxa_comercial), 4)
									end if
								end if

								valor_premio_puro_total = valor_premio_puro_total + valor_premio_puro
								valor_premio_liquido_total = valor_premio_liquido_total + valor_premio_liquido
								valor_premio_net_total = valor_premio_net_total + valor_premio_net
								valor_is_total = valor_is_total + valor_is
						%>
						<tr style="<%=cor_zebra%>">
							<td class="td_dado"><div align="center"><input type="radio" name="elemento" value="" onClick="javascript:preencheCampos(<%=iContador%>)"></div></td>
							<td class="td_dado"><div align="center" id="SUBGRUPO_ID<%=iContador%>"><%=formatnumber(rsSubgrupo("SUBGRUPO_ID"), 0)%></div></td>
							<td class="td_dado"><div align="right" id="VL_IPTC_MOEN_CTC<%=iContador%>"><%=formatnumber(valor_is, 2)%></div></td>
							<td class="td_dado"><div align="center"><%=formatnumber(CortaCasasDecimais((valor_premio_puro * per_fat_seguro), 2), 2)%></div></td>
							<td class="td_dado"><div align="right"><%=formatnumber(CortaCasasDecimais((valor_premio_liquido * per_fat_seguro), 2), 2)%></div></td>
							<td class="td_dado"><div align="right" id="PC_DA_CTC<%=iContador%>"><%=formatnumber(percentual_da, 2)%></div></td>
							<td class="td_dado"><div align="center"><%=RetornaPeriodicidade(per_fat_seguro, vAceitaAverbacao)%></div></td>
							<td class="td_dado"><div align="right" id="PC_ETLE_CTC<%=iContador%>"><%=formatnumber(percentual_pro_labore, 2)%></div>
							
							<div style="display:none" id="VL_LQDO_MOEN_CTC<%=iContador%>"><%=formatnumber(valor_premio_liquido, 2)%></div>
							<div style="display:none" id="VL_PREM_PURO<%=iContador%>"><%=formatnumber(valor_premio_puro, 2)%></div>
							<div style="display:none" id="valor_iof<%=iContador%>"><%=formatnumber(valor_iof, 2)%></div>
							<div style="display:none" id="taxa_comercial<%=iContador%>"><%=formatnumber(taxa_comercial, 4)%></div>
							<div style="display:none" id="valor_pro_labore<%=iContador%>"><%=formatnumber(valor_pro_labore, 4)%></div>
							<div style="display:none" id="per_fat_seguro<%=iContador%>"><%=formatnumber(per_fat_seguro, 0)%></div>
							<div style="display:none" id="valor_premio_net<%=iContador%>"><%=formatnumber(valor_premio_net, 2)%></div>
							<input type="hidden" name="NR_CTC_SGRO_subgrupo<%=iContador%>" value="<%=formatnumber(rsSubgrupo("NR_CTC_SGRO"), 0)%>">
							<input type="hidden" name="NR_VRS_CTC_subgrupo<%=iContador%>" value="<%=formatnumber(rsSubgrupo("NR_VRS_CTC"), 0)%>">
							</td>
						</tr>
						<%
								rsSubgrupo.movenext
							loop
							
							if valor_is_total > 0 then
								taxa_media_mensal = CortaCasasDecimais(((valor_premio_liquido_total / valor_is_total) * 1000), 4)
							else
								taxa_media_mensal = 0
							end if
			
						end if
						%>
					</table>
					<input type="hidden" name="NR_CTC_SGRO_subgrupo" value="">
					<input type="hidden" name="NR_VRS_CTC_subgrupo" value="">
							
				</div>
			</td>
		</tr>
		<%end if%>
		<tr>
			<td colspan="4" height="10"></td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor total de IS</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="valor_is_total" size="16" value="<%=formatnumber(valor_is_total, 2)%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="valor_is_total_or" value="<%=formatnumber(valor_is_total, 2)%>">
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor total do pr�mio puro</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="valor_premio_puro_total" size="16" value="<%=formatnumber(valor_premio_puro_total, 2)%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="valor_premio_puro_total_or" value="<%=formatnumber(valor_premio_puro_total, 2)%>">
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor total do pr�mio l�quido</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="valor_premio_liquido_total" size="16" value="<%=formatnumber(valor_premio_liquido_total, 2)%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="valor_premio_liquido_total_or" value="<%=formatnumber(valor_premio_liquido_total, 2)%>">
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Taxa m�dia mensal</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="taxa_media_mensal" size="16" value="<%=formatnumber(taxa_media_mensal, 4)%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="taxa_media_mensal_or" value="<%=formatnumber(taxa_media_mensal, 4)%>">
				
				<input type="hidden" name="valor_premio_net_total" size="16" value="<%=formatnumber(valor_premio_net_total, 2)%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="valor_premio_net_total_or" value="<%=formatnumber(valor_premio_net_total, 2)%>">
			</td>
			<td></td>
			<td></td>
		</tr>
	</table>	
<script language="JavaScript">
	<%if bSubgrupo then%>
	habilitaCampos(false);
	<%end if%>
</script>