<script>
function processasubgrupo()
{
	try { document.formTec.NR_SEQL_CBT_CTC.selectedIndex = 0; } catch (ex) { }

	document.formTec.submit();
}
</script>
<%
set rs = objcon.mLerSubgrupoCotacao

if rs.eof then
	bSubgrupo = false
else
	bSubgrupo = true

set rs = objcon.mLerSubgrupoCotacao

'Verifica se o produto aceita averba��o
objCon.pTP_PESQ = "BB"
vAceitaAverbacao = UCase(objCon.mLerIndAceitaAverbacao())
%>
<tr>
	<td nowrap class="td_label">&nbsp;Subgrupo&nbsp;</td>
	<td class="td_dado" width="800">&nbsp;
		<select name="NM_PRPN_CTC" onchange="processasubgrupo();">
		<option value="">--Escolha abaixo--</option>
		<%
		do while not rs.eof
			if trim(request("NM_PRPN_CTC")) = trim(rs("NR_CTC_SGRO")) & "|" & trim(rs("NR_VRS_CTC")) then
				sSelecionado = " selected"
			else
				sSelecionado = ""
			end if
			%>
			<option value="<%=trim(rs("NR_CTC_SGRO")) & "|" & trim(rs("NR_VRS_CTC"))%>"<%=sSelecionado%>><%=right(00 & rs("subgrupo_id"),2) & " (" & trim(rs("NR_CTC_SGRO_BB")) & ")"%> - <%=trim(rs("NM_PRPN_CTC"))%></option>
			<%
			rs.movenext
		loop
		%>
		</select>
	</td>
</tr>
<%
	if trim(request("NM_PRPN_CTC")) <> "" then 
		arrSubGrupoCotacao = split(trim(request("NM_PRPN_CTC")),"|")
								
		objCon.pNR_CTC_SGRO = arrSubGrupoCotacao(0)
		objCon.pNR_VRS_CTC = arrSubGrupoCotacao(1)
		NR_CTC_SGRO = objCon.pNR_CTC_SGRO
		NR_VRS_CTC = objCon.pNR_VRS_CTC
	end if
end if
						
if not bSubgrupo or trim(request("NM_PRPN_CTC")) <> "" then 
	set rsDados = Objcon.mLerDadosTecnicos()
%>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td nowrap class="td_label">&nbsp;Limite de capital segurado para cobertura de filhos</td>
	<td class="td_dado" width="800">&nbsp;<%=testaDadoVazioFormataN2(rsDados,"vl_lim_cap_seg_cob_filhos", 0)%></td>
</tr>												
<tr>
	<td nowrap class="td_label">&nbsp;Percentual m�nimo para ades�o�das vidas cotadas</td>
	<td class="td_dado">&nbsp;<%=testaDadoVazioFormataN2(rsDados,"perc_min_adesao", 2)%></td>
</tr>												
<tr>
	<td nowrap class="td_label">&nbsp;Limite m�ximo de capital segurado</td>
	<td class="td_dado">&nbsp;<%=testaDadoVazioFormataN2(rsDados,"vl_lim_max_cap_seg", 2)%></td>
</tr>				
<tr>
	<td nowrap class="td_label">&nbsp;Limite de idade para emiss�o da ap�lice</td>
	<td class="td_dado">&nbsp;<%=testaDadoVazioFormataN2(rsDados,"lim_idade_emi_aplc", 0)%></td>
</tr>
<tr>
	<td nowrap class="td_label">&nbsp;Limite m�nimo de capital segurado</td>
	<td class="td_dado">&nbsp;<%=testaDadoVazioFormataN2(rsDados,"vl_lim_min_cap_seg", 2)%></td>
</tr>
<tr>
	<td nowrap class="td_label">&nbsp;Limite de idade para novas ades�es�na ap�lice</td>
	<td class="td_dado">&nbsp;<%=testaDadoVazioFormataN2(rsDados,"lim_idade_nova_adesao", 0)%></td>
</tr>
<tr>
	<td nowrap class="td_label">&nbsp;Periodicidade de faturamento do seguro</td>
	<td class="td_dado">&nbsp;<%if not isnull(rsDados("per_fat_seguro")) then Response.Write RetornaPeriodicidade(rsDados("per_fat_seguro"), vAceitaAverbacao) else Response.Write testaDadoVazio(rsDados,"per_fat_seguro") end if%></td>
</tr>
<tr>
	<td nowrap class="td_label">&nbsp;Periodicidade para revis�o�de taxa por sinistralidade</td>
	<td class="td_dado">&nbsp;<%if not isnull(rsDados("per_revisao_tx_sinistro")) then Response.Write RetornaPeriodicidade(rsDados("per_revisao_tx_sinistro")) else Response.Write testaDadoVazio(rsDados,"per_revisao_tx_sinistro") end if%></td>
</tr>
<tr>
	<td nowrap class="td_label">&nbsp;Vidas efetivamente cotadas para�cobertura do seguro</td>
	<td class="td_dado">&nbsp;<%=testaDadoVazioFormataN2(rsDados,"qt_vidas_cotadas", 0)%></td>
</tr>
<tr>
	<td nowrap class="td_label">&nbsp;�ndice para revis�o da taxa por sinistralidade</td>
	<td class="td_dado">&nbsp;<%=testaDadoVazioFormataN2(rsDados,"perc_ind_rev_tax_sin", 2)%></td>
</tr>
<tr>
	<td nowrap class="td_label">&nbsp;Percentual de excedente t�cnico</td>
	<td class="td_dado">&nbsp;<%=testaDadoVazioFormataN2(rsDados,"perc_exc_tnco", 2)%></td>
</tr>
<tr>
	<td nowrap class="td_label">&nbsp;Percentual de DA do excedente t�cnico</td>
	<td class="td_dado">&nbsp;<%=testaDadoVazioFormataN2(rsDados,"perc_da_exc_tnco", 2)%></td></tr><%end if%>