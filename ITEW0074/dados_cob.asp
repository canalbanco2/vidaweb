<input type="hidden" name="trocou_seql_cob" value="">
<script>
function processasubgrupo()
{
	try { document.formProc.NR_SEQL_CBT_CTC.selectedIndex = 0; } catch (ex) { }

	document.formProc.submit();
}
</script>
<%
Set rs = objcon.mLerSubgrupoCotacao
If rs.EOF Then
	bSubgrupo = False
Else
	bSubgrupo = True
%>
<tr>
	<td nowrap class="td_label">&nbsp;Subgrupo&nbsp;</td>
	<td class="td_dado" width="800">&nbsp;
		<select name="NM_PRPN_CTC" onchange="processasubgrupo();">
		<option value="">--Escolha abaixo--</option>
		<%
		do while not rs.eof
			if trim(request("NM_PRPN_CTC")) = trim(rs("NR_CTC_SGRO")) & "|" & trim(rs("NR_VRS_CTC")) then
				sSelecionado = " selected"
			else
				sSelecionado = ""
			end if
			%>
			<option value="<%=trim(rs("NR_CTC_SGRO")) & "|" & trim(rs("NR_VRS_CTC"))%>"<%=sSelecionado%>><%=right(00 & rs("subgrupo_id"),2) & " (" & trim(rs("NR_CTC_SGRO")) & ")"%> - <%=trim(rs("NM_PRPN_CTC"))%></option>
			<%
			rs.movenext
		loop
		%>
		</select>
	</td>
</tr>
<%
	If Trim(Request("NM_PRPN_CTC")) <> "" Then 
		arrSubGrupoCotacao = split(trim(request("NM_PRPN_CTC")),"|")
								
		objCon.pNR_CTC_SGRO = arrSubGrupoCotacao(0)
		objCon.pNR_VRS_CTC = arrSubGrupoCotacao(1)
		NR_CTC_SGRO = objCon.pNR_CTC_SGRO
		NR_VRS_CTC = objCon.pNR_VRS_CTC
	End If
End If

Set rsCoberturaComLMI = objCon.mLerIndCobLmi()
vLMI = False
If NOT rsCoberturaComLMI.EOF Then
   If rsCoberturaComLMI("IN_CBT_CTC_LMI") = "S" Then
      vLMI = True
   End If
End If
						
If NOT bSubgrupo or Trim(Request("NM_PRPN_CTC")) <> "" Then
   NR_SEQL_CBT_CTC = Trim(Request("NR_SEQL_CBT_CTC"))
%>
<tr>
	<td nowrap class="td_label">&nbsp;Objeto de Risco&nbsp;</td>
	<td class="td_dado" width="800">&nbsp;
		<Select name="NR_SEQL_CBT_CTC" onchange="document.formProc.trocou_seql_cob.value='1';document.formProc.submit();">
			<option value="">--Escolha Abaixo--</option>
			<%
			Set rsSeqlCobertura = objcon.mLerSeqlCobertura()
			Do while not rsSeqlCobertura.EOF
				Response.Write "<option value=""" & rsSeqlCobertura("NR_SEQL_CBT_CTC") & """" 
				If NR_SEQL_CBT_CTC = Trim(rsSeqlCobertura("NR_SEQL_CBT_CTC")) Then
				   Response.Write " selected "
				End If
				If CInt(rsSeqlCobertura("NR_SEQL_CBT_CTC")) = 9999 Then
				   Response.Write ">LMI</option>"
				Else
				   Response.Write ">" & rsSeqlCobertura("NR_SEQL_CBT_CTC") & "</option>"
				End If
				rsSeqlCobertura.MoveNext
			Loop%>
		</select>
		<script>
<!--
   if(document.formProc.NR_SEQL_CBT_CTC.length==2&&document.formProc.NR_SEQL_CBT_CTC.selectedIndex!=1){document.formProc.NR_SEQL_CBT_CTC.selectedIndex=1;document.formProc.submit();}
//-->
            </script>
	</td>
</tr>
<%
End If
%>
</tr>
	<td colspan="2">&nbsp;</td>
</tr>
<%
If NR_SEQL_CBT_CTC <> "" Then
	cd_cbt = Request("cd_cbt")
	If request("trocou_seql_cob") <> "" then
		cd_cbt = ""
	End If
	objCon.pNR_SEQL_CBT_CTC = NR_SEQL_CBT_CTC
	If vLMI Then
	    Set rsCobertura = objcon.mLerDadosCoberturaLmi()
    Else
  	    Set rsCobertura = objcon.mLerDadosCobertura()
    End If
	If rsCobertura.EOF Then%>
</tr>
	<td colspan="2" class="td_label">&nbsp;Nenhuma cobertura relacionada.</td>
</tr>
  <%Else
       If vLMI = True Then
          num_sequencial = rsCobertura("NR_LIM_QSTN_CTC")
       Else
          num_sequencial = rsCobertura("NR_SEQL_CBT_CTC")
       End If%>
</tr>
	<td colspan="2">
		<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
			<tr>
				<td class="td_label_negrito" style="text-align:center">Quest.</td>
				<td class="td_label_negrito" style="text-align:center">C�d. Cob</td>
				<td class="td_label_negrito" style="text-align:center">Cobertura</td>
				<td class="td_label_negrito" style="text-align:center">Valor IS</td>
				<td class="td_label_negrito" style="text-align:center">Franquia</td>
				<%if arrParametro(9) <> Application("WF_HAB") then%>
					<td class="td_label_negrito" style="text-align:center">Taxa NET</td>
					<td class="td_label_negrito" style="text-align:center">Pr�mio NET</td>
				<%END IF%>
			</tr>
			<%
			iContador = 0
			
			valor_is_total = 0
			valor_premio_net_total = 0
			Do while not rsCobertura.EOF
	           If testaDadoVazioFormataN(rsCobertura,"vl_iptc_cbt_ctc",2) <> "" and not isnull(rsCobertura("ACUMULA_IS")) Then
				  If UCase(trim(rsCobertura("ACUMULA_IS"))) = "S" Then
					 valor_is_total = cdbl(valor_is_total) + cdbl(testaDadoVazioFormataN(rsCobertura,"vl_iptc_cbt_ctc",2))
				  End If
			   End If
			   If testaDadoVazioFormataN(rsCobertura,"vl_prem_cbt_ctc",2) <> "" Then
				  valor_premio_net_total = cdbl(valor_premio_net_total) + cdbl(testaDadoVazioFormataN(rsCobertura,"vl_prem_cbt_ctc",2))
			   End If
				
			   alterna_cor %>
				<tr>
					<td class="td_dado" style="text-align:center<%=cor_zebra%>">
					<%
						objcon.pCD_CBT = rsCobertura("cd_cbt")
						objcon.pCD_TIP_QSTN = "3"
						Set rsTemp = objcon.mLerQuestionarioId()
						If not rsTemp.EOF Then
							objcon.pNR_QSTN = rsTemp("NR_QSTN")
							Set rsTemp1 = objcon.mLerLocalRisco()
							If NOT rsTemp1.EOF Then
								If trim(rsTemp1("NR_CD_TIP_QSTN_CTC")) <> "0" and trim(rsTemp1("NR_CD_TIP_QSTN_CTC")) <> "" Then%>
									<input type="radio" name="cd_cbt" onClick="document.formProc.trocou_seql_cob.value='';document.formProc.submit()" style="cursor:hand;" value="<%=rsCobertura("cd_cbt")%>"<%if trim(cd_cbt) = trim(rsCobertura("cd_cbt")) then%> checked<%end if%>>
							<%  Else
									Response.Write("&nbsp;")
								End If
							End If
							Set rsTemp1 = Nothing
						End If
						Set rsTemp = Nothing%>
						</td>
						<td class="td_dado" style="text-align:center<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rsCobertura,"cd_cbt")%></td>
						<td class="td_dado" style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazio(rsCobertura,"nome")%></td>
						<td class="td_dado" style="text-align:right<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rsCobertura,"vl_iptc_cbt_ctc")%></td>
						<td class="td_dado" style="text-align:center<%=cor_zebra%>">&nbsp;
						<%
						objcon.pCD_CBT = rsCobertura("CD_CBT")
						set rsFranquia = objcon.mLerDadosFranquia
						tx_rgr_frquFim = ""
						tx_rgr_frquTotal = ""
						if not rsFranquia.eof then
							Response.Write testaDadoVazio(rsCobertura,"TX_FRQU_CTC")
						else
							Response.Write("-- N�o h� franquias --")
						end if
						%>
						</td>
						<%if arrParametro(9) <> Application("WF_HAB") then%>
							<td class="td_dado" style="text-align:right<%=cor_zebra%>">&nbsp;<%= testaDadoVazioFormataN(rsCobertura,"pc_prem_cbt_ctc",5) %></td>
							<td class="td_dado" style="text-align:right<%=cor_zebra%>">&nbsp;<%= testaDadoVazioFormataN(rsCobertura,"vl_prem_cbt_ctc",2) %></td>
						<%END IF%>
					</tr>
					<%
				iContador = iContador + 1
				rsCobertura.movenext
			loop
			%>
		</table>
		<br>
		<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
			<tr>
				<td class="td_label_negrito" width="150">Valor IS Total:</td>
				<td class="td_dado"><%=formatnumber(valor_is_total,2)%>&nbsp;</td>
			</tr>
			<%if arrParametro(9) <> Application("WF_HAB") then%>
				<tr>
					<td class="td_label_negrito" width="150">Pr�mio NET Total:</td>
					<td class="td_dado"><%=formatnumber(valor_premio_net_total,2)%>&nbsp;</td>
				</tr>
			<%END IF%>
		</table>
	<%
	end if

	if trim(cd_cbt) <> "" then
	%>
		<br>
		<!-- #include file = "funcoes_dados_qst.asp"-->
		<%
		htmlExibir = "<table border=""0""><tr><td>" & vbcrlf
		objcon.pCD_CBT = cd_cbt
		objcon.pCD_TIP_QSTN = "3"
		set rsQST = objcon.mLerQuestionarioId()
		if not rsQST.eof then
			questionario_id = rsQST("NR_QSTN")
			objcon.pNR_QSTN = questionario_id
			set rsNomeQSTN = objcon.mLerLocalRisco()
			if not rsNomeQSTN.eof then
				NR_SEQL_QSTN_CTC = rsNomeQSTN("NR_SEQL_QSTN_CTC")
				objCon.pNR_SEQL_QSTN_CTC = NR_SEQL_QSTN_CTC
				htmlExibir = htmlExibir & "<font face=""arial"" size=""2""><b>" & rsNomeQSTN("nome") & "</b></font>" & vbcrlf
				%><!-- #include file = "dados_qst.asp"--><%
			end if
		end if
		htmlExibir2 = "</td></tr></table>" & vbcrlf
	end if
	%></td></tr><%end if%>