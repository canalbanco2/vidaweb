<!--#include virtual = "/internet/serv/siscot3/classe/citemtreeview.asp"-->
<!--#include virtual = "/internet/serv/siscot3/funcoes_dados_qst.asp"-->
<!--#include virtual = "/internet/serv/siscot3/request_geral.asp"-->
<!--#include virtual = "/internet/serv/siscot3/objWorkflowNegocio.asp"-->
<LINK href="/estilo_internet.css" rel="STYLESHEET" title="style" type="text/css">
<FORM name="frmQuestionario">
<%
codCobertura     = Request.QueryString("codCobertura")
NR_SEQL_QSTN_CTC = Request.QueryString("codLocalRisco")
var_CD_TIP_QSTN  = Request.QueryString("codTipQstn")        ' 1- Produto, 2- Objeto de Risco, 3- Coberturas
combo            = Request.QueryString("combo")             ' Se sim, as respostas s�o feitas em combos, sen�o por radio buttons
cabec            = Request.QueryString("cabec")
varPostBack      = False
booMostraCabec   = False
codigo_questionario= ""
If cabec = 1 Then
    booMostraCabec = True
End If
If InStr(1,codCobertura,"|") = 0 Then
    Redim arrQuestionarios(1)
    arrQuestionarios(0) = codCobertura
Else
    booMostraCabec = True
    arrQuestionarios = Split(codCobertura,"|")
    codigo_questionario = "$"
End If

strComandos = ""
For loopDADOS=0 To UBound(arrQuestionarios)-1
	If Trim(arrQuestionarios(loopDADOS)) <> "" Then
		arrCoberturas  = Split(arrQuestionarios(loopDADOS),"@")
		codCobertura   = arrCoberturas(0)
           
        objCon.pCD_CBT = codCobertura
        objCon.pCD_TIP_QSTN = var_CD_TIP_QSTN

	    If booMostraCabec Then
	       Response.Write "<table width=""100%""><tr><td class=""td_dado""><b>Question�rio da cobertura " & codCobertura & " - " & arrCoberturas(1) & "</b></td></tr></table>"
	       If codigo_questionario <> "" Then
	          codigo_questionario = codCobertura & "$"
	       End If
        End If

        questionario_id = arrCoberturas(2)
        indice_cob      = arrCoberturas(3)

        preencheChaves
        If var_NR_CTC_ATUAL <> "" Then
           NR_CTC_NOVO_array   = Split(var_NR_CTC_ATUAL&"|","|")
           objCon.pNR_CTC_SGRO = NR_CTC_NOVO_array(0)
           objCon.pNR_VRS_CTC  = NR_CTC_NOVO_array(1)
        End If
        objCon.pNR_QSTN = Trim(questionario_id)
        objCon.pCD_TIP_QSTN = var_CD_TIP_QSTN
        objCon.pNR_SEQL_QSTN_CTC = NR_SEQL_QSTN_CTC

        Set objDataSet = objCon.mLerPergunasQuestionarioGravado()
        objDataSet.ActiveConnection = Nothing
        Set oArvore = new cItemTreeView
        oArvore = mGeraArrayQuestionario(objDataSet)

        ordem = CalcularOrdem(oArvore)

        If UBound(oArvore) > 0 Then
	       Response.Write(htmlExibir)%>
	       <br>
 	       <table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
	       <input type="hidden" name="<%=codigo_questionario%>LocalRisco" value="<% = NR_SEQL_QSTN_CTC%>">
	       <input type="hidden" name="<%=codigo_questionario%>questionarioID" value="<% = questionario_id %>">

	    <% aCumiC = ""

           For iC = 0 to UBound(ordem)-1
               intContador = ordem(iC)
               aCumiC = aCumiC & iC & "|"
               identacao = Replace(oArvore(intContador).tabs,"|","&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")
               exibir = true
               pergunta_atual = Trim(oArvore(intContador).pergunta_id)                    'Cod.pergunta que esta sendo preenchida
		       pergunta_precedente = Trim(oArvore(intContador).num_pergunta_precedente)   'Cod.pergunta precedente

		       If pergunta_precedente <> "0" Then
                  If var_PostBack Then
                     If Trim(Request(questionario_id & "rsp" & oArvore(intContador).num_pergunta_precedente)) = "" Then
                        exibir = false
                     ElseIf Trim(oArvore(intContador).resposta_pergunta_precedente_id) <> Trim(Request(questionario_id & "rsp" & oArvore(intContador).num_pergunta_precedente)) Then
                        exibir = false
                     End If
                  Else
                     indice = AchaIndiceId(oArvore,pergunta_precedente)                  'Acha posi��o da pergunta precedente em oArvore
                     If indice = "" Then
                        exibir = false
                     Else
                        If Trim(oArvore(indice).resposta) = "" Then
                           exibir = false
                        ElseIf Trim(oArvore(indice).resposta) <> Trim(oArvore(intContador).resposta_pergunta_precedente_id) Then
                           exibir = false
                        End If
                     End If
                  End If
               End If
        %>

                 <tr id="linTR_<%= iC%>" style="display:<%If NOT exibir Then Response.Write("none") End If%>">
			     <td>
                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="escolha">
                    <tr>
					   <td class="td_dado"><%=identacao & (iC + 1) & ") "&oArvore(intContador).nome_pergunta%></td>
                   </tr>
				    <tr>
					   <td class="td_dado">
						<%								
						 size = oArvore(intContador).tamanho
						 If size > 70 Then
							size  =  70
						 End If
						 Response.Write identacao & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"

					     resposta = retornaValor(codigo_questionario&questionario_id,oArvore,intContador,var_PostBack)
                         strComandos = strComandos & "perguntas[" & intContador & "]='" & pergunta_atual & "@" & resposta & "@" & oArvore(intContador).ind_obrigatoriedade & "@';"
		
						 If oArvore(intContador).grupo_dominio_resposta_id = "null" Then
						     Select case oArvore(intContador).nome_formato
							 	    case "TEXTO" %>
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this,<%=indice_cob%>)" type="text" size="<%=size%>" name="<%=codigo_questionario&questionario_id%>rsp<%=pergunta_atual%>" maxlength="<%=oArvore(intContador).tamanho%>" value="<%=resposta%>">
								  <%case "NUMERO" %>
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this,<%=indice_cob%>)" type="text" onKeyUp="FormatValorContinuo(this,0);" name="<%=codigo_questionario&questionario_id%>rsp<%=pergunta_atual%>" size="<%=size%>" maxlength="<%=oArvore(intContador).tamanho%>" value="<%=resposta%>">
								  <%case "VALOR" %>
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this,<%=indice_cob%>)" type="text" onKeyUp="FormatValorContinuo(this,2);" name="<%=codigo_questionario&questionario_id%>rsp<%=pergunta_atual%>" size="19" maxlength="19" value="<%=resposta%>">
								  <%case "DATA (DD/MM/AAAA)"%>
										<input type="hidden" name="<%=codigo_questionario&questionario_id%>rsp<%=pergunta_atual%>" value="<%=resposta%>">
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this,<%=indice_cob%>)" type="text" onKeyUp="ConcatenaData(this,this.form);FormatValorContinuo(this,0);" name="<%=codigo_questionario&questionario_id%>rsp<%=pergunta_atual%>_dia" size="2" maxlength="2" value="<%=Right(resposta,2)%>">
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this,<%=indice_cob%>)" type="text" onKeyUp="ConcatenaData(this,this.form);FormatValorContinuo(this,0);" name="<%=codigo_questionario&questionario_id%>rsp<%=pergunta_atual%>_mes" size="2" maxlength="2" value="<%=Mid(resposta,5,2)%>">
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this,<%=indice_cob%>)" type="text" onKeyUp="ConcatenaData(this,this.form);FormatValorContinuo(this,0);" name="<%=codigo_questionario&questionario_id%>rsp<%=pergunta_atual%>_ano" size="4" maxlength="4" value="<%=Left(resposta,4)%>">
								  <%case "HORA (HH:MM:SS)"%>
										<input type="hidden" name="<%=codigo_questionario&questionario_id%>rsp<%=pergunta_atual%>" value="<%=resposta%>">
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this,<%=indice_cob%>)" type="text" onKeyUp="FormatValorContinuo(this,0);" name="<%=codigo_questionario&questionario_id%>rsp<%=pergunta_atual%>_H" size="2" maxlength="2" value="<%=Right(resposta,2)%>">
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this,<%=indice_cob%>)" type="text" onKeyUp="FormatValorContinuo(this,0);" name="<%=codigo_questionario&questionario_id%>rsp<%=pergunta_atual%>_M" size="2" maxlength="2" value="<%=Mid(resposta,3,2)%>">
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this,<%=indice_cob%>)" type="text" onKeyUp="FormatValorContinuo(this,0);" name="<%=codigo_questionario&questionario_id%>rsp<%=pergunta_atual%>_S" size="2" maxlength="2" value="<%=Left(resposta,2)%>">
								  <%case "PERCENTUAL" %>
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this,<%=indice_cob%>)" type="text" onKeyUp="FormatValorContinuo(this,2);" name="<%=codigo_questionario&questionario_id%>rsp<%=pergunta_atual%>" size="19" maxlength="19" value="<%=resposta%>">
								  <%case "TIMESTAMP" %>
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this,<%=indice_cob%>)" type="text" name="<%=codigo_questionario&questionario_id%>rsp<%=pergunta_atual%>" maxlength="<%=oArvore(intContador).tamanho%>" size="<%=size%>" value="<%=resposta%>">
							<%End Select
						 Else
							  If combo = 1 Then %>
									<select name="<%=codigo_questionario&questionario_id%>rsp<%=pergunta_atual%>" onChange="onblurTeste(this,<%=indice_cob%>);pressionarQuestionario('<%=codigo_questionario&questionario_id%>',<%=pergunta_atual%>,this.value);">
										<option value="">--Escolha abaixo--</option>
						   <% End If

                              respArray = RespostasDasPerguntas(pergunta_atual)        'Array com as respostas das perguntas(contem tambem a resposta ja gravada)
							  For f=1 to UBound(respArray)
								  If combo = 1 Then
								     If resposta = Trim(respArray(f).dominio_resposta_id) Then
										 selecionado = "selected"
									 Else
										 selecionado = ""
									 End If%>
 									    <option <%=selecionado%> value="<% = respArray(f).dominio_resposta_id %>"><% = respArray(f).resposta_determinada %></option>
								<%Else
									 If resposta = Trim(respArray(f).dominio_resposta_id) Then
										 selecionado = "checked"
									 Else
										 selecionado = ""
									 End If%>
										<input type="radio" <%=selecionado%> name="<%=codigo_questionario&questionario_id%>rsp<%=pergunta_atual%>" value="<%=respArray(f).dominio_resposta_id%>" onclick="onblurTeste(this,<%=indice_cob%>);pressionarQuestionario('<%=codigo_questionario&questionario_id%>',<%=pergunta_atual%>,this.value);"><%=respArray(f).resposta_determinada%>
								<%End If
							  Next

							  If combo = 1 Then%>
									</select>
				            <%End If

				         End If%>
								<input type="hidden" name="<%=codigo_questionario&questionario_id%>obrigatorio<%=pergunta_atual%>" value="<%=oArvore(intContador).ind_obrigatoriedade%>">
								<input type="hidden" name="<%=codigo_questionario&questionario_id%>indice<%=iC%>" value="<%If exibir Then Response.Write(pergunta_atual)%>">
								<input type="hidden" name="<%=codigo_questionario&questionario_id%>pergunta<%=iC%>" value="<%=oArvore(intContador).nome_pergunta%>">
                       <%If pergunta_precedente <> "0" Then%>
 		   			   	        <input type="hidden" name="<%=codigo_questionario&questionario_id%>pgpre<%=pergunta_precedente%>rspq<%=oArvore(intContador).resposta_pergunta_precedente_id%>cd<%=pergunta_atual%>" value="<%=iC%>">
                       <%   perguntasAux = Split(oArvore(intContador).colecao_perguntas_precedentes,"|")

                            For iAuxilia = 0 To Ubound(perguntasAux)-1%>
 		   			   	        <input type="hidden" name="<%=codigo_questionario&questionario_id%>pgpre<%=perguntasAux(iAuxilia)%>rspq##cd<%=pergunta_atual%>" value="<%=iC%>">
                       <%   Next

                         End If%>
							</td>
						</tr>
					</table>
				</td>
			</tr>
<%         Next %>
 	<input type="hidden" name="<%=codigo_questionario%>pergQuestionario" value="<%=aCumiC%>">
   	</table>
<%
	       Response.Write(htmlExibir2)
		End If

		strComandos = strComandos & "window.parent.document.formProc.cpoGrav_QSTN_"&indice_cob&".value=perguntas;perguntas=new Array();"
	End If
Next%>

<script>var perguntas=new Array();<%=strComandos%></script>
</form>
<script>
<!--
	var dadoTesteCampo;
	function onfocusTeste(campo){
		dadoTesteCampo = campo.value;
	}
	function onblurTeste(campo,indice){
		if(campo.value!=dadoTesteCampo){
  	       var perg_id = campo.name.substring(campo.name.indexOf('rsp')+3);
           var ps      = eval("window.parent.document.formProc.cpoGrav_QSTN_"+indice+".value");
           var arPerg  = ps.split(',');
           var a = 0;
           if(ps!=''){
              for(a=0;a<arPerg.length;a++){
                  var arResp = arPerg[a].split('@');
                  if(arResp[0]==perg_id){
                     var obrig = arResp[2];
                     break;
                  }
              }
           }
           arPerg[a] = perg_id + '@' + campo.value + '@' + obrig + '@';
           eval("window.parent.document.formProc.cpoGrav_QSTN_"+indice+".value=arPerg");
		}
	}
//-->
</script>