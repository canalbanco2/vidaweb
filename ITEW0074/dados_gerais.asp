<!--#include virtual = "/padrao.asp"-->
<!--#include virtual = "/funcao/formatar_data.asp"-->
<!--#include file = "dados_gerais_tarefa.asp"-->
<%
set rs = objCon.mLerDetalhecotacao()
rs.ActiveConnection = Nothing
if not rs.eof then
dim cpf 
cpf = rs("NR_SRF_PRPN_CTC")

%>
<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
	<tr><td colspan="2"></td></tr>
	<tr><td colspan="2" class="td_label"><span class="sub_titulo" style="font-size:12px" style="font-size:12px"><b>:: Dados do Proponente</b></span></td></tr>
	<tr><td colspan="2">
			<DIV style="OVERFLOW: auto;  HEIGHT: 100px; WIDTH:100%;">
				<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
					<tr>
						<td class="td_label" nowrap>&nbsp;Proponente</td>
						<td class="td_label" nowrap>&nbsp;Categoria</td>
						<td class="td_label" nowrap>&nbsp;Nome</td>
						<td class="td_label" nowrap>&nbsp;E-Mail</td>
						<td class="td_label" nowrap>&nbsp;CPF/CNPJ</td>
						<td class="td_label" nowrap>&nbsp;Data Nascimento</td>
					</tr>	
				<%while not rs.eof
				alterna_cor
				%>
					<tr>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazio(rs,"NR_SEQL_PRPN")%></td>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%= trata_funcionario(rs,"CD_CTGR_PSS_CTC")%></td>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazio(rs,"NM_PRPN_CTC")%></td>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazio(rs,"TX_EMAI_PRPN_CTC")%></td>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%= trata_cpf_cnpj(rs,"NR_SRF_PRPN_CTC")%></td>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazioData(rs,"DT_NSC")%></td>
					</tr>	
				<%
				rs.movenext
				wend
				%>
				</table>
			</div>
		</td>
	</tr>
</table>

<!-- INICIO Demanda 17916607 � Grid Acumulo de Risco - Habitacional-->
<%
set rs = objCon.mLerAvaliacaoAcumuloDeRiscoCotacao()
rs.ActiveConnection = Nothing
if rs("existe_acumulo_de_risco") > 0 then

dim total_MIP
total_MIP = 0
dim total_DFI
total_DFI = 0
%>

<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
    <tr>
        <td colspan="6" class="td_label">
            <span class="sub_titulo" style="font-size: 12px"><b>:: Ac�mulo de Risco</b></span>
        </td>
    </tr>
</TABLE>

	<%    
	set rs = objCon.mLerAcumuloDeRiscoCotacoes(cpf)
	rs.ActiveConnection = Nothing
	%>

	<%if not rs.eof then%>

		<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
			<tr><td colspan="2"></td></tr>
			<tr><td colspan="2" class="td_label"><span class="sub_titulo" style="font-size:12px" style="font-size:12px"><b>:: ::Cota��o</b></span></td></tr>
			<tr><td colspan="2">
					<DIV style="OVERFLOW: auto;  HEIGHT: 100px; WIDTH:100%;">
						<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
							<tr>
								<td class="td_label" nowrap>&nbsp;CPF/CNPJ Proponente</td>
								<td class="td_label" nowrap>&nbsp;Produto</td>
								<td class="td_label" nowrap>&nbsp;N� de Cota��o BB</td>
								<td class="td_label" nowrap>&nbsp;N� de Cota��o AB</td>
								<td class="td_label" nowrap>&nbsp;Inst�ncia do Workflow</td>
								<td class="td_label" nowrap>&nbsp;Nome do Proponente</td>
								<td class="td_label" nowrap>&nbsp;Valor IS MIP</td>
								<td class="td_label" nowrap>&nbsp;Valor IS DFI</td>
								<td class="td_label" nowrap>&nbsp;Data da Cota��o</td>
								<td class="td_label" nowrap>&nbsp;Valor da IS Total</td>
							</tr>	
						<%
						dim soma_MIP_cotacao 
						soma_MIP_cotacao = 0
						dim soma_DFI_cotacao 
						soma_DFI_cotacao = 0
						%>
						
						<%while not rs.eof
						alterna_cor
						%>
							<tr>
								<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rs,"cpf_cnpj")%></td>
								<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rs,"produto_id")%></td>
								<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rs,"cotacao_bb")%></td>
								<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rs,"cotacao_ab")%></td>
								<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rs,"instancia")%></td>
								<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rs,"nome")%></td>
								<td class="td_dado" nowrap style="<%=cor_zebra%>;text-align: right">&nbsp;<%=testaDadoVazioNumerico(rs,"vl_is_mip",2)%></td>
								<td class="td_dado" nowrap style="<%=cor_zebra%>;text-align: right">&nbsp;<%=testaDadoVazioNumerico(rs,"vl_is_dfi",2)%></td>
								<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%=testaDadoVazioData(rs,"data_cotacao")%></td>
								<td class="td_dado" nowrap style="<%=cor_zebra%>;text-align: right">&nbsp;<%=testaDadoVazioNumerico(rs,"val_is_total",2)%></td>
							</tr>	
						<%
						soma_MIP_cotacao = soma_MIP_cotacao + testaDadoVazioNumericoSoma(rs,"vl_is_mip")
						soma_DFI_cotacao = soma_DFI_cotacao + testaDadoVazioNumericoSoma(rs,"vl_is_dfi")
						rs.movenext
						wend
						total_MIP = total_MIP + soma_MIP_cotacao 
						total_DFI = total_DFI + soma_DFI_cotacao 
						%>
						</table>
					</div>
					
					</br>

					<DIV style=" float: right; HEIGHT: 100px; WIDTH:35%;">
						<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE" >
							<tr style="text-align: right">
								<td class="td_dado" width="50%">&nbsp;Total MIP:</td>
								<td class="td_dado" width="50%">&nbsp;<%=FormatNumber(soma_MIP_cotacao,2)%></td>
							</tr>
							
							<tr style="text-align: right">
								<td class="td_dado" width="50%">&nbsp;Total DFI:</td>
								<td class="td_dado" width="50%">&nbsp;<%=FormatNumber(soma_DFI_cotacao,2)%></td>
							</tr>
							
						</table>
					</div>
				</td>
			</tr>
		</table>

	<%else%>

		<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
			<tr><td colspan="2"></td></tr>
			<tr><td colspan="2" class="td_label"><span class="sub_titulo" style="font-size:12px" style="font-size:12px"><b>:: ::Cota��o</b></span></td></tr>
			<tr><td colspan="2">
				</td>
			</tr>
		</table>
				
		<span class="sub_titulo" style="font-size:12px" style="font-size:12px">:: ::Nenhuma cota��o ativa encontrada</span>

	<%end if%>

	<%
	set rs = objCon.mLerAcumuloDeRiscoPropostas(cpf)
	rs.ActiveConnection = Nothing
	%>
	
	<%if not rs.eof then%>

		<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha" ID="Table4">
			<tr><td colspan="2"></td></tr>
			<tr><td colspan="2" class="td_label"><span class="sub_titulo" style="font-size:12px" style="font-size:12px"><b>:: ::Proposta Contratada</b></span></td></tr>
			<tr><td colspan="2">
					<DIV style="OVERFLOW: auto;  HEIGHT: 100px; WIDTH:100%;">
						<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE" ID="Table5">
							<tr>
								<td class="td_label" nowrap>&nbsp;CPF do proponente</td>
								<td class="td_label" nowrap>&nbsp;C�d. Produto</td>
								<td class="td_label" nowrap>&nbsp;N� da proposta BB</td>
								<td class="td_label" nowrap>&nbsp;N� da proposta AB</td>
								<td class="td_label" nowrap>&nbsp;Nome do Proponente</td>
								<td class="td_label" nowrap>&nbsp;Valor IS MIP</td>
								<td class="td_label" nowrap>&nbsp;Valor IS DFI</td>
								<td class="td_label" nowrap>&nbsp;In�cio da Vig�ncia</td>
								<td class="td_label" nowrap>&nbsp;Valor da IS Total</td>
							</tr>	
						
						<%
						dim soma_MIP_proposta 
						soma_MIP_cotacao = 0
						dim soma_DFI_proposta 
						soma_DFI_cotacao = 0
						%>
						
						<%while not rs.eof
						alterna_cor
						%>
							<tr>
								<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rs,"cpf_cnpj")%></td>
								<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rs,"produto_id")%></td>
								<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rs,"proposta_bb")%></td>
								<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rs,"proposta_id")%></td>
								<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rs,"nome")%></td>
								<td class="td_dado" nowrap style="<%=cor_zebra%>;text-align: right">&nbsp;<%=testaDadoVazioNumerico(rs,"val_is_mip",2)%></td>
								<td class="td_dado" nowrap style="<%=cor_zebra%>;text-align: right">&nbsp;<%=testaDadoVazioNumerico(rs,"val_is_dfi",2)%></td>
								<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%=testaDadoVazioData(rs,"dt_inicio_vigencia")%></td>
								<td class="td_dado" nowrap style="<%=cor_zebra%>;text-align: right">&nbsp;<%=testaDadoVazioNumerico(rs,"val_is_total",2)%></td>
							</tr>	
						<%
						soma_MIP_proposta = soma_MIP_proposta + testaDadoVazioNumericoSoma(rs,"val_is_mip")
						soma_DFI_proposta = soma_DFI_proposta + testaDadoVazioNumericoSoma(rs,"val_is_dfi")
						rs.movenext
						wend
						total_MIP = total_MIP + soma_MIP_proposta
						total_DFI = total_DFI + soma_DFI_proposta
						%>
						</table>
					</div>
					
					</br>
					
					<DIV style=" float: right; HEIGHT: 100px; WIDTH:35%;">
						<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE" ID="Table5">
							<tr style="text-align: right">
								<td class="td_dado" width="50%">&nbsp;Total MIP:</td>
								<td class="td_dado" width="50%">&nbsp;<%= FormatNumber(soma_MIP_proposta,2)%></td>
							</tr>
							<tr style="text-align: right">
								<td class="td_dado" width="50%">&nbsp;Total DFI:</td>
								<td class="td_dado" width="50%">&nbsp;<%= FormatNumber(soma_DFI_proposta,2)%></td>
							</tr>
						</table>
					</div>
					
				</td>
			</tr>
		</table>

	<%else%>

		<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
			<tr><td colspan="2"></td></tr>
			<tr><td colspan="2" class="td_label"><span class="sub_titulo" style="font-size:12px" style="font-size:12px"><b>:: ::Proposta Contratada</b></span></td></tr>
			<tr><td colspan="2">
				</td>
			</tr>
		</table>
				
		<span class="sub_titulo" style="font-size:12px" style="font-size:12px">:: ::Nenhuma proposta encontrada</span>

	<%end if%>
	
	<DIV style=" float: right; HEIGHT: 100px; WIDTH:35%;">
		<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE" >
			<tr style="text-align: right">
				<td class="td_dado" width="50%">&nbsp;Total Ac�mulo de Risco MIP:</td>
				<td class="td_dado" width="50%">&nbsp;<%=FormatNumber(total_MIP,2)%></td>
			</tr>
			
			<tr style="text-align: right">
				<td class="td_dado" width="50%">&nbsp;Total Ac�mulo de Risco DFI:</td>
				<td class="td_dado" width="50%">&nbsp;<%=FormatNumber(total_DFI,2)%></td>
			</tr>
		</table>
	</div>
	
<%end if%>
<!-- FIM Demanda 17916607 � Grid Acumulo de Risco - Habitacional-->

<%
end if
'ROMULO.BARBOSA INICIO - 22/02/2012
'DEMANDA 17780569 - Cr�ticas de an�lise t�cnica para cota��es recusadas

sql = ""
sql = sql & " select recusa_cotacao_tb.cpf,"
sql = sql & "		recusa_cotacao_tb.NR_CTC_SGRO_BB,"
sql = sql & "		recusa_cotacao_tb.NR_CTC_SGRO,"
sql = sql & "		recusa_cotacao_tb.wf_id,"
sql = sql & "		recusa_cotacao_tb.nome,"
sql = sql & "		recusa_cotacao_tb.motivo_recusa,"
sql = sql & "		recusa_cotacao_tb.dt_recusa"
sql = sql & "   FROM als_operacao_db.dbo.recusa_cotacao_tb recusa_cotacao_tb"
sql = sql & "   LEFT JOIN als_operacao_db..prpn_ctc prpn_ctc"
sql = sql & "	 ON prpn_ctc.cd_prd = recusa_cotacao_tb.cd_prd  "
sql = sql & "	AND prpn_ctc.cd_mdld = recusa_cotacao_tb.cd_mdld  "
sql = sql & "	AND prpn_ctc.cd_item_mdld = recusa_cotacao_tb.cd_item_mdld  "
sql = sql & "	AND prpn_ctc.nr_ctc_sgro = recusa_cotacao_tb.nr_ctc_sgro  "
sql = sql & "	AND prpn_ctc.nr_vrs_ctc = recusa_cotacao_tb.nr_vrs_ctc"
sql = sql & "   LEFT JOIN als_operacao_db..CLIENTE_CTC CLIENTE_CTC"
sql = sql & "	 ON prpn_ctc.cd_prd = CLIENTE_CTC.cd_prd  "
sql = sql & "	AND prpn_ctc.cd_mdld = CLIENTE_CTC.cd_mdld " 
sql = sql & "	AND prpn_ctc.cd_item_mdld = CLIENTE_CTC.cd_item_mdld  "
sql = sql & "	AND prpn_ctc.nr_ctc_sgro = CLIENTE_CTC.nr_ctc_sgro  "
sql = sql & "	AND prpn_ctc.nr_vrs_ctc = CLIENTE_CTC.nr_vrs_ctc "
sql = sql & "	AND prpn_ctc.cd_pss_ctc = CLIENTE_CTC.cd_cli "
sql = sql & "  WHERE    cpf         = '" & cpf & "'"
sql = sql & "  or       nr_cpf_cnpj = '" & cpf & "'"
sql = sql & "  GROUP BY recusa_cotacao_tb.cpf,"
sql = sql & "		recusa_cotacao_tb.NR_CTC_SGRO_BB,"
sql = sql & "		recusa_cotacao_tb.NR_CTC_SGRO,"
sql = sql & "		recusa_cotacao_tb.wf_id,"
sql = sql & "		recusa_cotacao_tb.nome,"
sql = sql & "		recusa_cotacao_tb.motivo_recusa,"
sql = sql & "		recusa_cotacao_tb.dt_recusa"

%>
<!--#include virtual = "/scripts/wbra_internet.asp"-->
<%
set rs = conex.execute(sql)

%>
<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
	<tr><td colspan="2"></td></tr>
	<tr><td colspan="2" class="td_label"><span class="sub_titulo" style="font-size:12px" style="font-size:12px"><b>:: Dados de Recusa</b></span></td></tr>
	<tr><td colspan="2">
			<DIV style="OVERFLOW: auto;  HEIGHT: 100px; WIDTH:100%;">
				<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
					<tr>
						<td class="td_label" nowrap>&nbsp;CPF do(s) proponente(s)</td>
						<td class="td_label" nowrap>&nbsp;N� de Cota��o BB</td>
						<td class="td_label" nowrap>&nbsp;N� de Cota��o AB</td>
						<td class="td_label" nowrap>&nbsp;Inst�ncia do Workflow</td>
						<td class="td_label" nowrap>&nbsp;Nome(s) do(s) Proponente(s)</td>
						<td class="td_label" nowrap>&nbsp;Motivo da Recusa</td>
						<td class="td_label" nowrap>&nbsp;Data da Recusa</td>
					</tr>	
				<%while not rs.eof
				alterna_cor
				%>
					<tr>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazio(rs,"cpf")%></td>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazio(rs,"NR_CTC_SGRO_BB")%></td>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazio(rs,"NR_CTC_SGRO")%></td>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazio(rs,"wf_id")%></td>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazio(rs,"nome")%></td>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazio(rs,"motivo_recusa")%></td>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazioData(rs,"dt_recusa")%></td>
					</tr>	
				<%
				rs.movenext
				wend
				%>
				</table>
			</div>
		</td>
	</tr>
</table>

<%
set rs = objCon.mLerConsultaCotacaoMapfre()
rs.ActiveConnection = Nothing
if not rs.eof then
%>
<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
	<tr><td colspan="2"></td></tr>
	<tr><td colspan="2" class="td_label"><span class="sub_titulo" style="font-size:12px" style="font-size:12px"><b>:: Dados da Cota��o Mapfre</b></span></td></tr>
	<tr><td colspan="2">
			<DIV style="OVERFLOW: auto;  HEIGHT: 100px; WIDTH:100%;">
				<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
					<tr>
						<td class="td_label" nowrap>&nbsp;CPF / CNPJ</td>
						<td class="td_label" nowrap>&nbsp;Ramo</td>
						<td class="td_label" nowrap>&nbsp;Data da Entrada da Cota��o</td>
						<td class="td_label" nowrap>&nbsp;Status da Cota��o</td>
						<td class="td_label" nowrap>&nbsp;Nome</td>
						<td class="td_label" nowrap>&nbsp;Nome do Corretor</td>
						<td class="td_label" nowrap>&nbsp;Consulta de Cota��es Mapfre</td>
					</tr>	
				<%while not rs.eof
				alterna_cor
				%>
					<tr>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%=trata_cpf_cnpj(rs,"cpf_cnpj")%></td>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rs,"ramo_id")%></td>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%=testaDadoVazioData(rs,"dt_cotacao")%></td>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rs,"status_cotacao")%></td>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rs,"nome_segurado")%></td>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rs,"nome_corretor")%></td>
						<td class="td_dado" nowrap style="<%=cor_zebra%>">&nbsp;<a href="#" onclick="window.open('https://www.aliancadobrasil.com.br/GCO/GCOW0037/Default.aspx?login=producao2&wf_id=<%=testaDadoVazio(rs,"wf_id")%>');">Consultar novamente</a>
						</td>
					</tr>	
				<%
				rs.movenext
				wend
				%>
				</table>
			</div>
		</td>
	</tr>
</table>
<%else%>
<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
	<tr>
		<td class="td_label"><span class="sub_titulo" style="font-size:12px" style="font-size:12px"><b>:: Nenhuma cota��o Mapfre encontrada</b></span></td>
		<td class="td_label"><span class="sub_titulo" style="font-size:12px" style="font-size:12px"><b><a href="#" onclick="window.open('https://www.aliancadobrasil.com.br/GCO/GCOW0037/Default.aspx?login=producao2&wf_id=<%=request("wf_id")%>');">Consulta de Cota��es Mapfre</a></b></span></td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
</table>
<%
end if
set rs = nothing
%>
