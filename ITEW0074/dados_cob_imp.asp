</tr>
	<td colspan="2">
		<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
		<%
		If opcaoPorContrato = "T" Then
		   opcao = True
		Else
		   opcao = False
        End If

		If bLMI = True Or bLMI2 = True Then
            Set rsCobertura = objCon.mLerDadosCoberturaLmi(CBool(opcao))
		Else
			objCon.pNR_SEQL_CBT_CTC = NR_SEQL_QSTN_CTC
		    Set rsCobertura = objCon.mLerDadosCobertura(CBool(opcao))
		End If
		rsCobertura.ActiveConnection = Nothing
			
		If rsCobertura.EOF Then	%>
			<tr>
				<td class="td_label">&nbsp;Nenhuma cobertura relacionada.</td>
			</tr>
	 <% Else%>
			<tr>
				<td class="td_label_negrito" style="text-align:center">C�d. Cob</td>
				<td class="td_label_negrito" style="text-align:center">Cobertura</td>
				<%If bLMI Then%>
				<td class="td_label_negrito" style="text-align:center">Locais de Risco</td>
				<%End If%>
				<td class="td_label_negrito" style="text-align:center">Valor IS</td>
				<td class="td_label_negrito" style="text-align:center">Franquia</td>
				<td class="td_label_negrito" style="text-align:center">Taxa NET</td>
				<td class="td_label_negrito" style="text-align:center">Pr�mio NET</td>
			</tr>
			<%
			iContador = 0
			
			valor_is_total = 0
			valor_premio_net_total = 0
			strmonta_locais = ""
			numPrimeiroLocal= 9999
            If bLMI Then
 		        Do While NOT rsCobertura.EOF
 		           If numPrimeiroLocal > rsCobertura("NR_LIM_QSTN_CTC") Then
 		              numPrimeiroLocal = rsCobertura("NR_LIM_QSTN_CTC")
 		           End If
 		           pos_idx = InStr(1,strmonta_locais,rsCobertura("cd_cbt")&"COB"&rsCobertura("NR_SEQL_LIM_CTC")&"SEQ")
 		           If pos_idx = 0 Then
 		              strmonta_locais = strmonta_locais & rsCobertura("cd_cbt") & "COB" & rsCobertura("NR_SEQL_LIM_CTC") & "SEQ" & rsCobertura("NR_LIM_QSTN_CTC") & "@"
 		           Else
 		              pos_idx2 = InStr(pos_idx+1, strmonta_locais, "@")
                      strmonta_locais = Left(strmonta_locais,pos_idx2-1) & ", " & rsCobertura("NR_LIM_QSTN_CTC") & Mid(strmonta_locais,pos_idx2)
 		           End If
				   rsCobertura.MoveNext
			    Loop
             
		        rsCobertura.MoveFirst
		    End If

			Do While NOT rsCobertura.EOF
			    pos_idx = 1
			    If bLMI Then
			        pos_idx = InStr(1,strmonta_locais,rsCobertura("cd_cbt")&"COB"&rsCobertura("NR_SEQL_LIM_CTC")&"SEQ")
			    End If

				If testaDadoVazioFormataN(rsCobertura,"vl_iptc_cbt_ctc",2) <> "" and not isnull(rsCobertura("ACUMULA_IS")) then
					If ucase(trim(rsCobertura("ACUMULA_IS"))) = "S" then
						valor_is_total = cdbl(valor_is_total) + cdbl(testaDadoVazioFormataN(rsCobertura,"vl_iptc_cbt_ctc",2))
					End If
				End If

				If bLMI Then 
				   If rsCobertura("NR_LIM_QSTN_CTC") = numPrimeiroLocal Then
				      If testaDadoVazioFormataN(rsCobertura,"vl_prem_cbt_ctc",2) <> "" then
					     valor_premio_net_total = cdbl(valor_premio_net_total) + cdbl(testaDadoVazioFormataN(rsCobertura,"vl_prem_cbt_ctc",2))
				      End If
				   End If
                Else
          			  If testaDadoVazioFormataN(rsCobertura,"vl_prem_cbt_ctc",2) <> "" then
					     valor_premio_net_total = cdbl(valor_premio_net_total) + cdbl(testaDadoVazioFormataN(rsCobertura,"vl_prem_cbt_ctc",2))
				      End If
                End If
                If pos_idx > 0 Then			
				    alterna_cor
				%>
				<tr>
					<td class="td_dado" style="text-align:center<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rsCobertura,"cd_cbt")%></td>
					<td class="td_dado" style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazio(rsCobertura,"nome")%></td>
					<%If bLMI Then
					     pos_idx  = InStr(pos_idx+1, strmonta_locais, "SEQ") + 3
					     pos_idx2 = InStr(pos_idx+1, strmonta_locais, "@")
					     pos_idx2 = pos_idx2 - pos_idx
					     NR_LIM_QSTN_CTC = Mid(strmonta_locais,pos_idx,pos_idx2)
					     strmonta_locais = Replace(strmonta_locais, rsCobertura("cd_cbt")&"COB"&rsCobertura("NR_SEQL_LIM_CTC")&"SEQ"&NR_LIM_QSTN_CTC&"@", "")%>
					<td class="td_dado" style="text-align:center<%=cor_zebra%>">&nbsp;<%=NR_LIM_QSTN_CTC%></td>
					<%End If%>
					<td class="td_dado" style="text-align:right<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rsCobertura,"vl_iptc_cbt_ctc")%></td>
					<td class="td_dado" style="text-align:center<%=cor_zebra%>">&nbsp;
					<%
					objcon.pCD_CBT = rsCobertura("CD_CBT")
					Set rsFranquia = objcon.mLerDadosFranquia
					tx_rgr_frquFim = ""
					tx_rgr_frquTotal = ""
					If not rsFranquia.EOF Then
					    If opcaoPorContrato = "T" And (bLMI = True Or bLMI2 = True) Then
						   Response.Write testaDadoVazio(rsCobertura,"TX_FRQU_APLD_CTR")
						Else
						   Response.Write testaDadoVazio(rsCobertura,"TX_FRQU_CTC")
						End If
					Else
						Response.Write("-- N�o h� franquias --")
					End If
					%>
					</td>
					<td class="td_dado" style="text-align:right<%=cor_zebra%>">&nbsp;<%= testaDadoVazioFormataN(rsCobertura,"pc_prem_cbt_ctc",5) %></td>
					<td class="td_dado" style="text-align:right<%=cor_zebra%>">&nbsp;<%= testaDadoVazioFormataN(rsCobertura,"vl_prem_cbt_ctc",2) %></td>
				</tr>
				   <%
				    iContador = iContador + 1
                End If
				rsCobertura.MoveNext
			Loop
			%>
		</table>
		<br>
		<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
			<tr>
				<td class="td_label_negrito" width="150">Valor IS Total:</td>
				<td class="td_dado"><%=FormatNumber(valor_is_total,2)%>&nbsp;</td>
			</tr>
			<tr>
				<td class="td_label_negrito" width="150">Pr�mio NET Total:</td>
				<td class="td_dado"><%=FormatNumber(valor_premio_net_total,2)%>&nbsp;</td>
			</tr>
		</table>
		<br>
		<table cellpadding="0" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
			<tr>
				<td class="td_label_negrito">Question�rios das Coberturas</td>
			</tr>
		<%
		rsCobertura.MoveFirst
		
		bExisteQuestionario = False
		
		Do While NOT rsCobertura.EOF
			objCon.pCD_CBT = rsCobertura("cd_cbt")
			objCon.pCD_TIP_QSTN = "3"
			Set rsQST = objCon.mLerQuestionarioId(CBool(opcao))
			If NOT rsQST.EOF Then
				questionario_id = rsQST("NR_QSTN")
				objcon.pNR_QSTN = questionario_id
				objcon.pNR_SEQL_QSTN_CTC = ""
									
				Set rsNomeQSTN = objcon.mLerLocalRisco(CBool(opcao))
				If NOT rsNomeQSTN.EOF Then
					bExisteQuestionario = true
					NR_SEQL_QSTN_CTC = rsNomeQSTN("NR_SEQL_QSTN_CTC")
					objcon.pNR_SEQL_QSTN_CTC = NR_SEQL_QSTN_CTC
					htmlExibir = "<tr><td><font face=""arial"" size=""2""><b>" & rsNomeQSTN("nome") & "</b></font></td></tr><tr><td>" & vbcrlf
			        htmlExibir2 = "</td></tr>" & vbCrLf
					%>
					<!-- #include virtual = "/internet/serv/siscot3/dados_qst.asp"-->
					<%
				End If
			End If
		
			rsCobertura.Movenext
		Loop
		
		if not bExisteQuestionario then
		%>
		<tr><td class="td_dado"><br>Nenhum question�rio de cobertura relacionado.<br><br></td></tr>
		<%end if%>
		<%end if%>
		</table>
	</td>
</tr>