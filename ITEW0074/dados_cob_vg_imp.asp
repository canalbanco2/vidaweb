</tr>
	<td colspan="2">
		<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
			<tr>
				<td class="td_label_negrito" style="text-align:center">C�d. Cob</td>
				<td class="td_label_negrito" style="text-align:center">Cobertura</td>
				<td class="td_label_negrito" style="text-align:center">% da cob</td>
				<td class="td_label_negrito" style="text-align:center">Taxa NET</td>
				<td class="td_label_negrito" style="text-align:center">Pr�mio NET</td>
			</tr>
			<%
			If opcaoPorContrato = "T" Then
			   opcao = True
			Else
			   opcao = False
			End If

	        Set rsCobertura = objCon.mLerDadosCobertura(CBool(opcao))
		    rsCobertura.ActiveConnection = Nothing

			iQtdeCoberturas = 0
			If NOT rsCobertura.EOF Then
				Do while not rsCobertura.EOF
					iQtdeCoberturas = iQtdeCoberturas + 1	
					rsCobertura.MoveNext
				Loop

				rsCobertura.MoveFirst
			End If
			
			iContador = 0
			Do While NOT rsCobertura.EOF
				alterna_cor
				%>
				<tr>
					<td class="td_dado" style="text-align:center<%=cor_zebra%>">&nbsp;<%=testaDadoVazio(rsCobertura,"cd_cbt")%></td>
					<td class="td_dado" style="<%=cor_zebra%>">&nbsp;<%= testaDadoVazio(rsCobertura,"nome")%>
					</td>
					<td class="td_dado" style="text-align:center<%=cor_zebra%>">&nbsp;<%= testaDadoVazioN(rsCobertura,"PC_IPTC_CBT_CTC") %></td>
					<%
					If iContador = 0 Then
						If opcaoPorContrato = "T" Then    
                           Set rsDadosFin = objCon.mLerDadosFinanceirosContrato(2)
				        Else
                           Set rsDadosFin = objCon.mLerDadosFinanceiros(2)
				        End If
				        rsDadosFin.ActiveConnection = Nothing
						percentual_da = CDbl("0" & testaDadoVazioN(rsDadosFin,"PC_DA_CTC"))
						valor_premio_puro = CDbl("0" & testaDadoVazioN(rsDadosFin,"VL_PREM_PURO"))
						valor_is = CDbl("0" & testaDadoVazioN(rsDadosFin,"VL_IPTC_MOEN_CTC"))
						valor_premio_net = cdbl("0" & testaDadoVazioN(rsDadosFin,"VL_SGRA_MOEN_CTC"))
				
						If valor_premio_net > 0 and valor_is > 0 Then
							taxa_net = CortaCasasDecimais(((valor_premio_net / valor_is) * 100), 4)
						Else
							taxa_net = 0
						End If
					%>
					<td class="td_dado" style="text-align:right" rowspan="<%=iQtdeCoberturas%>">&nbsp;<%=formatnumber(taxa_net, 4)%></td>
					<td class="td_dado" style="text-align:right" rowspan="<%=iQtdeCoberturas%>">&nbsp;<%=formatnumber(valor_premio_net,2)%></td>
					<%end if%>
				</tr>
					<%
				iContador = iContador + 1

				rsCobertura.MoveNext
			Loop
			%>
		</table>
		<br>
		<table cellpadding="0" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
			<tr>
				<td class="td_label_negrito">Question�rios das Coberturas</td>
			</tr>
		<%
		If iQtdeCoberturas > 0 Then
 		   rsCobertura.MoveFirst
		
		   bExisteQuestionario = False
		
		   Do while NOT rsCobertura.EOF
		
			  htmlExibir = "<table border=""0""><tr><td>" & vbcrlf
			  objcon.pCD_CBT = rsCobertura("cd_cbt")
			  objcon.pCD_TIP_QSTN = "3"

 			  Set rsQST = objCon.mLerQuestionarioId(CBool(opcao))
 			  rsQST.ActiveConnection = Nothing
			  If not rsQST.EOF Then
			 	 questionario_id = rsQST("NR_QSTN")
				 objcon.pNR_QSTN = questionario_id
				 objcon.pNR_SEQL_QSTN_CTC = ""
									
                 Set rsNomeQSTN = objcon.mLerLocalRisco(CBool(opcao))
                 rsNomeQSTN.ActiveConnection = Nothing

				 If NOT rsNomeQSTN.EOF Then
					bExisteQuestionario = True

					NR_SEQL_QSTN_CTC = rsNomeQSTN("NR_SEQL_QSTN_CTC")
					objCon.pNR_SEQL_QSTN_CTC = NR_SEQL_QSTN_CTC
					htmlExibir = htmlExibir & "<font face=""arial"" size=""2""><b>" & rsNomeQSTN("nome") & "</b></font>" & vbcrlf
					%>
			<tr>
				<td>
					<!-- #include virtual = "/internet/serv/siscot3/dados_qst.asp"-->
				&nbsp;</td>
			</tr>
					<%
				 End If
			  End If
			  htmlExibir2 = "</td></tr></table>" & vbcrlf
		
			  rsCobertura.MoveNext
		  Loop
		End If
		
		if not bExisteQuestionario then
		%>
		<tr><td class="td_dado"><br>Nenhum question�rio de cobertura relacionado.<br><br></td></tr>
		<%end if%>
		</table>
	</td>
</tr>