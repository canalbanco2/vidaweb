<%
function print_dados_gerais_pdf(x,y, wf_id)
data_sistema = objCon.ObtemDtOperacional

sql = "set nocount on exec siscot_db..dados_pedido_sps " & wf_id
set rsInst = conex.execute(sql)'

if isnull(rsinst("cod_produto")) then cod_produto = "" else cod_produto = rsinst("cod_produto") end if
if isnull(rsinst("nome_produto")) then nome_produto = "" else nome_produto =rsinst("nome_produto")end if
if isnull(rsinst("num_proc_susep")) then codigo_susep ="" else codigo_susep =rsinst("num_proc_susep")end if
if isnull(rsinst("agencia")) then ag_contratante = "" else ag_contratante = rsinst("agencia") end if
tp_operacao = "PROPOSTA"

'mn_proposta = rsinst(5)
if isnull(rsinst("num_cotacao")) then nm_pedido = "" else nm_pedido = rsinst("num_cotacao")end if
''versao = rsinst(7)
if isnull(rsinst("dt_cotacao")) then dt_cotacao = "" else dt_cotacao = rsinst("dt_cotacao")end if

if isnull(rsinst("num_apolice")) then apolice_atual ="" else apolice_atual =rsinst("num_apolice")end if

if isnull(rsinst("inicio_vigencia_seguro")) then ini_vigi = "" else ini_vigi =  rsinst("inicio_vigencia_seguro")end if
if isnull(rsinst("fim_vigencia_seguro")) then fim_vig = "" else fim_vig =  rsinst("fim_vigencia_seguro")end if
if isnull(rsinst("prazo_dias")) then prazo_dias = "" else prazo_dias =  rsinst("prazo_dias")end if
''codnome_moeda = rsinst(13)
''num_endosso = rsinst(14)
''versao_endosso = rsinst(15)
''dt_endosso = rsinst(16)
''motiv_endosso = rsinst(17)
if isnull(rsinst("nome_proponente")) then nome = "" else nome =  rsinst("nome_proponente")end if
if isnull(rsinst("cpf_cnpj")) then cpf_cnpj = "" else cpf_cnpj = formatacnpj(rsinst("cpf_cnpj"))end if
if isnull(rsinst("tp_pessoa")) then tp_pessoa = "" else tp_pessoa =  rsinst("tp_pessoa")end if
if isnull(rsinst("endereco_cobranca")) then endereco = "" else endereco =  rsinst("endereco_cobranca")end if
''complemento = rsinst(17)
if isnull(rsinst("bairro_cobranca")) then bairro = "" else bairro =  rsinst("bairro_cobranca")end if
if isnull(rsinst("municipio_cobranca")) then cidade = "" else cidade =  rsinst("municipio_cobranca")end if
if isnull(rsinst("uf_cobranca")) then uf = "" else uf =  rsinst("uf_cobranca")end if
if isnull(rsinst("cep_cobranca")) then cod_cep = "" else cod_cep =  formata_cep(rsinst("cep_cobranca"))end if
''end_elet = rsinst(27)
''telefone = "(" & rsinst("ddd1") & " " & mid(rsinst("telefone1"),1,len(rsinst("telefone1")) - 4) & "-" right(rsinst("telefone1"),4)
if isnull(rsinst("atividade")) then ramo_ativ = "" else ramo_ativ =  rsinst("atividade")end if
''nome_contato = rsinst("contato")
if isnull(rsinst("ddd1")) then tel_contato = "" else tel_contato =  "(" & rsinst("ddd1") & ") " & mid(rsinst("telefone1"),1,len(rsinst("telefone1")) - 4) & "-" & right(rsinst("telefone1"),4)end if
''end_elet_contato = rsinst(32)


if y < 540 then
y = quebrar_pagina()
end if
xpdf = x - 11
ypdf = y - 459
pdf.StitchPDF  end_file & "pdf_dados_do_pedido.pdf", 1,xpdf, ypdf ,520,479,0
print_dados_gerais_pdf = ypdf + 93


'codigo do produto
xcod_produto = x + 11
ycod_produto = y - 33
cod_produto =  tira_acento(cod_produto)


pdf.PrintText xcod_produto, ycod_produto, cod_produto

'nome do produto
xnome_produto = x+72
ynome_produto = y-33
pdf.SetFont "Arial", 8

if len(nome_produto) > 26 then
pdf.SetFont "Arial", 5
ynome_produto = y - 28
nome_dividido = mid(nome_produto,1,26)
pdf.PrintText xnome_produto, ynome_produto, tira_acento(ucase(nome_dividido))
nome_dividido = mid(nome_produto,27)
ynome_produto=ynome_produto - 6
pdf.PrintText xnome_produto, ynome_produto, tira_acento(ucase(nome_dividido))
pdf.SetFont "Arial", 8
else
pdf.PrintText xnome_produto, ynome_produto,tira_acento(ucase(nome_produto))
end if

'Codigo SUSEP - PRODUTO
xcod_susep = x+236
ycod_susep = y-33
pdf.PrintText xcod_susep, ycod_susep, tira_acento(codigo_susep)

'Ag�ncia Contratante
xag_contratante = x + 362
yag_contratante = y-33
pdf.SetFont "Arial", 8

if len(ag_contratante) > 28 then
pdf.SetFont "Arial", 5
yag_contratante = y - 28
ag_contratante_dividido = mid(ag_contratante,1,26)
pdf.PrintText xag_contratante, yag_contratante, tira_acento(ucase(ag_contratante_dividido))
ag_contratante_dividido = mid(ag_contratante,27)
yag_contratante=yag_contratante - 6
pdf.PrintText xag_contratante, yag_contratante, tira_acento(ucase(ag_contratante_dividido))
pdf.SetFont "Arial", 8
else
pdf.PrintText xag_contratante, yag_contratante,tira_acento(ucase(ag_contratante))
end if 

'Tipo de Opera��o
xtp_operacao = x + 11
ytp_operacao = y - 61
pdf.PrintText xtp_operacao, ytp_operacao, tira_acento(ucase(tp_operacao))

'N� da Proposta
xmn_proposta = x + 102
ymn_proposta = y - 61
pdf.PrintText xmn_proposta, ymn_proposta, tira_acento(mn_proposta)

'n� do Pedido de Cota��o
xnm_pedido = x + 185
ynm_pedido = y - 61
pdf.PrintText xnm_pedido, ynm_pedido, tira_acento(cnm_pedido)

'vers�o do Pedido
xversao = x + 316
yversao = y - 61
pdf.PrintText xversao, yversao, tira_acento(versao)

'Data da Cota��o
xdt_cotacao = x + 418
ydt_cotacao = y - 61
pdf.PrintText xdt_cotacao, ydt_cotacao, tira_acento(dt_cotacao)

'N� da ap�lice atual
xapolice_atual = x + 11
yapolice_atual = y - 88
pdf.PrintText xapolice_atual, yapolice_atual, tira_acento(apolice_atual)

'Inicio de Vigencia
xini_vigi = x + 112
yini_vigi = y - 88
pdf.PrintText xini_vigi, yini_vigi, tira_acento(ini_vigi)

'Fim de Vigencia
xfim_vig = x + 215
yfim_vig = y - 88
pdf.PrintText xfim_vig, yfim_vig, tira_acento(fim_vig)

'Prazo em dias
xprazo_dias = x + 308
yprazo_dias = y - 88
pdf.PrintText xprazo_dias, yprazo_dias, prazo_dias

'C�digo / nome da moeda
xcodnome_moeda = x + 11
ycodnome_moeda = y - 115
pdf.PrintText xcodnome_moeda, ycodnome_moeda, tira_acento(codnome_moeda)

'n�mero do endosso
xnum_endosso = x + 215
ynum_endosso = y - 115
pdf.PrintText xnum_endosso, ynum_endosso, num_endosso

'Vers�o do Endosso
xversao_endosso = x + 319
yversao_endosso = y - 115
pdf.PrintText xversao_endosso, yversao_endosso, versao_endosso

'Data do Endosso
xdt_endosso = x + 418
ydt_endosso = y - 115
pdf.PrintText xdt_endosso, ydt_endosso, dt_endosso

'Motivo Endosso
vallinha = 0
ymotiv_endosso = y - 175 - vallinha
xmotiv_endosso = x + 11
motiv_endosso_parte = motiv_endosso
do while len(cod_produto_parte) > 100 and vallinha <=10
vallinha = vallinha + 10
pdf.PrintText xmotiv_endosso, ymotiv_endosso, tira_acento(mid(motiv_endosso_parte, 1, 100))
motiv_endosso_parte = mid(motiv_endosso_parte, 101)
ymotiv_endosso = y - 175 - vallinha
xmotiv_endosso = x + 11
loop
pdf.PrintText xmotiv_endosso, ymotiv_endosso, tira_acento(mid(motiv_endosso_parte, 1, 100))

'Nome
xnome = x + 11
ynome = y - 252
pdf.SetFont "Arial", 8

if len(nome) > 50 then
xnome = xnome - 5
pdf.SetFont "Arial", 7
end if
pdf.PrintText xnome, ynome,tira_acento(nome)
pdf.SetFont "Arial", 8

'CPF/CNPJ
xcpf_cnpj = x + 332
ycpf_cnpj = y - 252
pdf.PrintText xcpf_cnpj, ycpf_cnpj, tira_acento(cpf_cnpj)

'TIPO PESSOA
xtp_pessoa = x + 422
ytp_pessoa = y - 252
pdf.PrintText xtp_pessoa, ytp_pessoa, tira_acento(ucase(tp_pessoa))

'Endereco
xendereco = x + 11
yendereco = y - 280
pdf.PrintText xendereco, yendereco, tira_acento(ucase(endereco))

'Complemento
xcomplemento = x+233
ycomplemento = y - 280
pdf.PrintText xcomplemento, ycomplemento, tira_acento(ucase(complemento))

'Bairro
xbairro = x + 318
ybairro = y - 280
pdf.PrintText xbairro, ybairro, tira_acento(ucase(bairro))


'Cidade
xcidade = x + 11
ycidade = y - 308
pdf.PrintText xcidade, ycidade, tira_acento(ucase(cidade))

'UF
xuf = x+332
yuf = y - 308
pdf.PrintText xuf, yuf, uf

'CEP
xcod_cep = x + 422
ycod_cep = y - 308
pdf.PrintText xcod_cep, ycod_cep, cod_cep

'Endere�o Eletronico
xend_elet = x + 11
yend_elet = y - 336
pdf.PrintText xend_elet, yend_elet, ucase(end_elet)

'Telefone
xtelefone = x + 420
ytelefone = y - 336
pdf.PrintText xtelefone, ytelefone, telefone

'Ramo de Atividade
xramo_ativ = x + 11
yramo_ativ = y - 364
pdf.PrintText xramo_ativ, yramo_ativ, tira_acento(ucase(ramo_ativ))

'Nome do Contato para Inspe��o
'xnome_contato = x + 11
'ynome_contato = y - 422
'pdf.PrintText xnome_contato, ynome_contato, nome_contato

'Telefone
'xtel_contato = x+260
'ytel_contato = y - 422
'pdf.PrintText xtel_contato, ytel_contato, tel_contato

'Endere�o Eletr�nico
'xend_elet_contato = x + 343
'yend_elet_contato = y - 422
'pdf.PrintText xend_elet_contato, yend_elet_contato, end_elet_contato
end function

function detalhamento_subramo(x,y, wf_id)

if y < 540 then
y = quebrar_pagina()
end if
xpdf = x - 7
ypdf = y - 681
pdf.StitchPDF  end_file & "pdf_detalhamento.pdf", 1,xpdf, ypdf ,520,681,0
detalhamento_subramo = ypdf + 58


set rs = objNegocio.VerificarExistenciaLMI(agendamento_id)
if rs.EOF then
	MostraRadio = True
	if cLMI = "" then
		cLMI = "N"
	end if
else
	MostraRadio = False
	if CInt(rs("lmi")) = 1 Then
		cLMI = "S"
		cLMI2 = "S"
	elseif CInt(rs("lmi")) = 2 Then
		cLMI = "S"
		cLMI2 = "N"
	else
		cLMI = "N"
	end if
	rs.MoveFirst
end if

set rs_cotacao = objNegocio.LerCotacaoRe(agendamento_id,CStr(cLMI))


meu_num_parcelas = RetornaCampoObj(rs_cotacao,"num_maximo_parcelas","num_maximo_parcelas")
meu_corretagem = RetornaCampoObj(rs_cotacao,"perc_corretagem","perc_corretagem")
meu_custo_apolice = RetornaCampoObj(rs_cotacao,"custo_apolice","custo_apolice")
meu_valor_premio = RetornaCampoObj(rs_cotacao,"vl_premio_net_total","vl_premio_net_total")
vl_parcelamento = RetornaCampoObj(rs_cotacao,"vl_coef_parcelamento","vl_coef_parcelamento")
vl_primeira_parcela =  RetornaCampoObj(rs_cotacao,"vl_primeira_parcela","vl_primeira_parcela")
vl_demais_parcelas =  RetornaCampoObj(rs_cotacao,"vl_demais_parcelas","vl_demais_parcelas")
vl_pago_no_ato =  RetornaCampoObj(rs_cotacao,"vl_pgto_ato","vl_pgto_ato")
numero_de_parcelas = RetornaCampoObj(rs_cotacao,"num_parcelas","num_parcelas")
numero_de_parcelas = RetornaCampoObj(rs_cotacao,"num_parcelas","num_parcelas")
forma_pgto_id = RetornaCampoObj(rs_cotacao,"forma_pgto_id","forma_pgto_id")
deb_agencia_id = RetornaCampoObj(rs_cotacao,"deb_agencia_id","deb_agencia_id")
deb_agencia_dv_id = RetornaCampoObj(rs_cotacao,"deb_agencia_dv_id","deb_agencia_dv_id")
deb_conta_corrente_id = RetornaCampoObj(rs_cotacao,"deb_conta_corrente_id","deb_conta_corrente_id")
deb_conta_corrente_dv_id = RetornaCampoObj(rs_cotacao,"deb_conta_corrente_dv_id","deb_conta_corrente_dv_id")

'Solu��o de Contorno
if forma_pgto_id = "1" then
forma_pgto_id = "D�BITO EM CONTA - BANCO DO BRASIL"
else
forma_pgto_id = "FICHA DE COMPENSA��O BANC�RIA"
end if


if trim(deb_conta_corrente_id) <> "" then
deb_conta_corrente_id = deb_conta_corrente_id & "-" & deb_conta_corrente_dv_id
end if

if trim (deb_agencia_id) <> "" then
deb_agencia_id = deb_agencia_id & "-" & deb_agencia_dv_id
end if



vl_premio_net_total = RetornaCampoObj(rs_cotacao,"vl_premio_net_total","vl_premio_net_total")
vl_custo_apolice = RetornaCampoObj(rs_cotacao,"custo_apolice","custo_apolice")
'AMARCO - 21/05/2008 Nova regra para o percentual de IOF
if len(dt_cotacao)>=8 then

	dia_a = day(dt_cotacao)
	mes_a = month(dt_cotacao)
	ano_a = year(dt_cotacao)
		
	if len(dia_a) < 2 then dia_a = "0" & dia_a end if
	if len(mes_a) < 2 then mes_a = "0" & mes_a end if
	
	dt_cotacao1 = ano_a & mes_a & dia_a
else
	dt_cotacao1 = dt_sistema	
end if

IF CDbl(dt_cotacao1) > CDbl("20080103") then
	iof = "7,38"
else
	iof = "7,00"
END IF
vl_premio_liquido = vl_premio_net_total / (1 - (replace(meu_corretagem,".",",")/100))
vl_custo_apolice = vl_premio_liquido/10
if vl_custo_apolice > 60 then
vl_custo_apolice = 60
end if
vl_iof = (vl_premio_liquido + vl_custo_apolice) * (iof/100)
vl_premio_avista = vl_premio_liquido + vl_custo_apolice + vl_iof

pdf.printtext xpdf+210 , y - 85,  formatnumber(vl_premio_liquido,2)
pdf.printtext xpdf+210 , y - 101, formatnumber(vl_custo_apolice,2)
pdf.printtext xpdf+210 , y - 116, formatnumber(vl_iof,2)
pdf.printtext xpdf+210 , y - 132, formatnumber(vl_parcelamento,5)
pdf.printtext xpdf+210 , y - 148, formatnumber(vl_premio_avista,2)


				pdf.printtext xpdf+38 , y - 213,  tira_acento(forma_pgto_id)
				pdf.printtext xpdf+225 , y - 213,  deb_agencia_id
			 pdf.printtext xpdf+360 , y - 213,  deb_conta_corrente_id


				pdf.printtext xpdf+45 , y - 241,  formatnumber(replace(vl_pago_no_ato,".",","),2)
				pdf.printtext xpdf+135 , y - 241,  numero_de_parcelas
 	  pdf.printtext xpdf+225 , y - 241,  formatnumber(replace(vl_primeira_parcela,".",","),2)
			 pdf.printtext xpdf+325 , y - 241,  formatnumber(replace(vl_demais_parcelas,".",","),2)
				
			
end function

function imprime_coberturas_lmi(x, y, agendamento_id)

if y < 130 then
y = quebrar_pagina()
end if
xcob = x-97
imprime_coberturas_lmi = y

'pdf.StitchPDF  end_file & "pdf_coberturas.pdf", 1,xcob, ycob ,700,157,0
'pdf.PrintText x + 57 , y - 2 , "LMI"
'imprime_coberturas_lmi = ycob + 108

set result_cob = objNegocio.VerificarExistenciaLMI(agendamento_id)
primeiro = true
DO WHILE NOT RESULT_COB.EOF	

if imprime_coberturas_lmi < 130 then
imprime_coberturas_lmi = quebrar_pagina()
end if
if primeiro then
ycob = imprime_coberturas_lmi - 137
pdf.StitchPDF  end_file & "pdf_coberturas.pdf", 1,xcob, ycob ,700,157,0
pdf.PrintText x + 57 , y - 2 , "LMI"
imprime_coberturas_lmi = ycob + 108
else
ycob = imprime_coberturas_lmi - 147
pdf.StitchPDF  end_file & "pdf_coberturas2.pdf", 1,xcob, ycob ,700,157,0
imprime_coberturas_lmi = ycob + 108
end if

ylinha = imprime_coberturas_lmi - 151

pdf.StitchPDF  end_file & "pdf_linha_coberturas.pdf", 1,xcob, ylinha ,700,157,0

pdf.PrintText x + 7, ylinha + 139, RetornaDado(result_cob,"tp_cobertura_id")
pdf.PrintText x + 40, ylinha + 139, tira_acento(ucase(mid(RetornaDado(result_cob,"nome"),1,36)))
pdf.PrintText x + 360, ylinha + 139, "------"

if isnull(RetornaDado(result_cob,"vl_is")) then
vvl_is = "------"
else
vvl_is = RetornaDado(result_cob,"vl_is")
end if

pdf.PrintText x + 435, ylinha + 139,  vl_is

if ylinha < 1 then
ylinha = quebrar_pagina() - 125
end if

pdf.StitchPDF  end_file & "pdf_franquia.pdf", 1,xcob + 85, ylinha + 59 ,524,104,0
ylinha = ylinha - 60

vlfranquia = ucase(RetornaDado(result_cob,"franquia"))

vallinha = 0
yfranquia = ylinha + 170 - vallinha
xfranquia = x + 11
do while len(vlfranquia) > 65 and vallinha <=20
vallinha = vallinha + 10
pdf.PrintText xfranquia, yfranquia, tira_acento(ucase(mid(vlfranquia, 1, 65)))
vlfranquia = mid(vlfranquia,66)
yfranquia = ylinha + 170 - vallinha
xfranquia = x + 11
loop

pdf.PrintText xfranquia, yfranquia, tira_acento(ucase(mid(vlfranquia, 1, 65)))



imprime_coberturas_lmi = ylinha + 137
result_cob.movenext
primeiro = false
loop
if subramo_id = 1110 then
produto_id = 810
elseif subramo_id = 1111 then
produto_id = 811
elseif subramo_id = 1112 then
produto_id = 112
elseif subramo_id = 1119 then
produto_id = 112
elseif subramo_id = 1113 then
produto_id = 113
elseif subramo_id = 1114 then
produto_id = 113
elseif subramo_id = 1811 then
produto_id = 120
elseif subramo_id = 1117 then
produto_id = 10
end if
produto_id = 103


'Aguardar ALS criar spp e metodo para buscar clausulas para Cobertura
set result_clau = objNegocio.VerificarExistenciaLMI(0)
if not result_clau.eof then
xclau =  x-97

if imprime_coberturas_lmi < 130 then
imprime_coberturas_lmi = quebrar_pagina()
end if

yclau = imprime_coberturas_lmi - 157

pdf.StitchPDF  end_file & "pdf_clausulas_especificas.pdf", 1,xclau, yclau ,700,157,0

imprime_coberturas_lmi = yclau + 103



DO WHILE NOT result_clau.EOF	

if imprime_coberturas_lmi < 76 then
imprime_coberturas_lmi = quebrar_pagina()
yclau = imprime_coberturas_lmi - 157
pdf.StitchPDF  end_file & "pdf_clausulas_especificas.pdf", 1,xclau, yclau ,700,157,0
imprime_coberturas_lmi = yclau + 103
end if

ylinha = imprime_coberturas_lmi - 143

pdf.StitchPDF  end_file & "pdf_linha_clausulas_especificas.pdf", 1,xcob, ylinha ,700,157,0
pdf.PrintText x + 7, ylinha + 131, result_clau("cod_clausula")
pdf.PrintText x + 40, ylinha + 131, tira_acento(ucase(mid(result_clau("descr_clausula"),1,85)))

imprime_coberturas_lmi = ylinha + 129
result_clau.movenext
loop
end if

end function


function endereco_risco(x, y, agendamento_id)

set result_cob = objNegocio.VerificarExistenciaLMI(agendamento_id)

if RetornaDado(result_cob,"lmi") <> 0 then
y = imprime_coberturas_lmi(x,y,agendamento_id)
y = y - 20
cob_lmi = true
end if

xpdf = x - 13
set result_lori = objNegocio.LerLocalRisco(agendamento_id)
do while not result_lori.eof
if y < 190then
y = quebrar_pagina()
end if

ypdf = y - 135

pdf.StitchPDF  end_file & "pdf_end_risco.pdf", 1,xpdf, ypdf ,528,157,0


pdf.printtext x + 122, y+1, " -  " & RetornaDado(result_lori,"item_local_id")

pdf.PrintText x + 7,y - 30, tira_acento(ucase(testaDadoVazio(result_lori,"endereco")))
pdf.PrintText x + 275,y - 30, ""
pdf.PrintText x + 385,y - 30, tira_acento(ucase(testaDadoVazio(result_lori,"bairro")))
pdf.PrintText x + 7,y - 55, tira_acento(ucase(testaDadoVazio(result_lori,"municipio")))
pdf.PrintText x + 275,y - 55, tira_acento(ucase(testaDadoVazio(result_lori,"uf")))
pdf.PrintText x + 385,y - 55, tira_acento(ucase(testaDadoVazioCEP(result_lori,"cep")))

pdf.PrintText x+ 172, y - 85 , tira_acento(testaDadoVazioNumerico(result_lori,"vl_risco_predio_conteudo",2))
pdf.PrintText x+ 172, y - 100 , testaDadoVazioNumerico(result_lori,"vl_risco_predio",2)
pdf.PrintText x+ 172, y - 115 , testaDadoVazioNumerico(result_lori,"vl_risco_maquina",2)
pdf.PrintText x+ 450, y - 85 , testaDadoVazioNumerico(result_lori,"vl_risco_mercadoria",2)
pdf.PrintText x+ 450, y - 100 , testaDadoVazioNumerico(result_lori,"vl_risco_isolado_local",2)
pdf.PrintText x+ 450, y - 115 , testaDadoVazioNumerico(result_lori,"vl_risco_lucros",2)

endereco_risco = ypdf + 16

if endereco_risco < 130 then
endereco_risco = quebrar_pagina()
end if

xinspec = xpdf -85
yinspec = endereco_risco - 160
pdf.StitchPDF  end_file & "dados_inspecao.pdf", 1,xinspec, yinspec ,707,157,0
pdf.PrintText x + 7,yinspec +111 , tira_acento(ucase(testaDadoVazio(result_lori,"contato")))
pdf.PrintText x + 270,yinspec +111 ,"(" &  ucase(testaDadoVazio(result_lori,"ddd_telefone_contato")) & ") " & mid(ucase(testaDadoVazio(result_lori,"telefone_contato")),1,len(ucase(testaDadoVazio(result_lori,"telefone_contato"))) - 4) & "-" & right(ucase(testaDadoVazio(result_lori,"telefone_contato")),4)
pdf.PrintText x + 360,yinspec +111, "-----"

y = yinspec + 90
endereco_risco = yinspec + 109
if not cob_lmi then
local_risco_id =  RetornaDado(result_lori,"item_local_id")
endereco_risco = imprime_cob_sem_lmi(x,y, agendamento_id,local_risco_id)
 y = endereco_risco - 20 'y - 100

end if
result_lori.movenext
loop

end function

function imprime_cob_sem_lmi(x,y, agendamento_id,local_risco_id)

if y < 130 then
y = quebrar_pagina()
end if
set result_cob = objNegocio.VerificarExistenciaLMI(agendamento_id)
primeiro = true
do while  not result_cob.eof 
if result_cob("item_local_id") = local_risco_id  then

xcob = x-97
ycob = y - 137

pdf.StitchPDF  end_file & "pdf_coberturas.pdf", 1,xcob, ycob ,700,157,0
pdf.PrintText x + 57 , y - 2 , "Local - " & local_risco_id
imprime_cob_sem_lmi = ycob + 108

DO WHILE NOT RESULT_COB.EOF	

if result_cob("item_local_id") = local_risco_id then

if imprime_cob_sem_lmi < 90 then
imprime_cob_sem_lmi = quebrar_pagina()
end if
if not primeiro then
ycob = imprime_cob_sem_lmi - 137
pdf.StitchPDF  end_file & "pdf_coberturas2.pdf", 1,xcob, ycob ,700,157,0
imprime_cob_sem_lmi = ycob + 108
end if

ylinha = imprime_cob_sem_lmi - 151

pdf.StitchPDF  end_file & "pdf_linha_coberturas.pdf", 1,xcob, ylinha ,700,157,0

pdf.PrintText x + 7, ylinha + 139, RetornaDado(result_cob,"tp_cobertura_id")
if len(RetornaDado(result_cob,"nome")) > 40 then
pdf.SetFont "arial", 8
end if
pdf.PrintText x + 40, ylinha + 139, tira_acento(ucase(mid(RetornaDado(result_cob,"nome"),1,48)))
pdf.SetFont "arial", 9
pdf.PrintText x + 360, ylinha + 139, "------"
if isnull(RetornaDado(result_cob,"vl_is")) then
vvl_is = "------"
else
vvl_is = RetornaDado(result_cob,"vl_is")
end if

pdf.PrintText x + 435, ylinha + 139,  vvl_is


if ylinha < 1 then
ylinha = quebrar_pagina() - 125
end if

pdf.StitchPDF  end_file & "pdf_franquia.pdf", 1,xcob + 85, ylinha + 59 ,524,104,0
ylinha = ylinha - 60

vlfranquia = ucase(RetornaDado(result_cob,"franquia"))

vallinha = 0
yfranquia = ylinha + 170 - vallinha
xfranquia = x + 11
do while len(vlfranquia) > 65 and vallinha <=20
vallinha = vallinha + 10
pdf.PrintText xfranquia, yfranquia, tira_acento(ucase(mid(vlfranquia, 1, 65)))
vlfranquia = mid(vlfranquia,66)
yfranquia = ylinha + 170 - vallinha
xfranquia = x + 11
loop

pdf.PrintText xfranquia, yfranquia,tira_acento(ucase( mid(vlfranquia, 1, 65)))

imprime_cob_sem_lmi = ylinha + 137
end if
result_cob.movenext
primeiro = false
loop
end if

if not result_cob.eof then
result_cob.movenext
end if

loop


set result_cob = objNegocio.VerificarExistenciaLMI(0)
if not result_cob.eof then
xclau =  x-97

if imprime_cob_sem_lmi < 144 then
imprime_cob_sem_lmi = quebrar_pagina()
end if

yclau = imprime_cob_sem_lmi - 157
pdf.StitchPDF  end_file & "pdf_clausulas_especificas.pdf", 1,xclau, yclau ,700,157,0
imprime_cob_sem_lmi = yclau + 103



DO WHILE NOT RESULT_COB.EOF	

if imprime_cob_sem_lmi < 90 then
imprime_cob_sem_lmi = quebrar_pagina()
yclau = imprime_cob_sem_lmi - 157
pdf.StitchPDF  end_file & "pdf_clausulas_especificas.pdf", 1,xclau, yclau ,700,157,0
imprime_cob_sem_lmi = yclau + 103
end if

ylinha = imprime_cob_sem_lmi - 143

pdf.StitchPDF  end_file & "pdf_linha_clausulas_especificas.pdf", 1,xcob, ylinha ,700,157,0
pdf.PrintText x + 7, ylinha + 131, "c�digo"
pdf.PrintText x + 40, ylinha + 131, "c�digo"

imprime_cob_sem_lmi = ylinha + 129
result_cob.movenext
loop
end if

end function

function tira_acento (variavel)
Variavel = Replace(Variavel, "�", "a",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "e",1 ,len(variavel), 0)
Variavel = Replace(Variavel, "�", "i",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "o",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "u",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "a",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "o",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "a",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "e",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "o",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "n",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "A",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "E",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "I",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "O",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "U",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "A",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "O",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "A",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "E",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "O",1 ,len(variavel) , 0)
Variavel = Replace(Variavel, "�", "N",1 ,len(variavel) , 0)
tira_acento = Variavel

end function

%>