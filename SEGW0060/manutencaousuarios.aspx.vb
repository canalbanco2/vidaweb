Imports System.Data
Imports System.Web.UI.WebControls

Public Class ManutencaoUsuarios
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblVigencia As System.Web.UI.WebControls.Label
    Protected WithEvents lblNavegacao As System.Web.UI.WebControls.Label
    Protected WithEvents Panel1 As System.Web.UI.WebControls.Panel
    Protected WithEvents btnIncluir As System.Web.UI.WebControls.Button
    Protected WithEvents lblTituloTabela As System.Web.UI.WebControls.Label
    Protected WithEvents btnIncluirUsuario As System.Web.UI.WebControls.Button
    Protected WithEvents grdPesquisa As System.Web.UI.WebControls.DataGrid
    Protected WithEvents ucPaginacao As segw0060.paginacaogrid


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public RequestInclusao As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        btnIncluir.Attributes.Add("onclick", "return GB_show('Manuten��o de usu�rios', '../../ManutencaoUsuario_Form.aspx', 210, 450)")

        lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Subgrupo: " & Session("nomeSubgrupo") & " -> " & "Manuten��o de Usu�rios"
        lblVigencia.Text = "<br>" & Session("nomeSubgrupo") & " - " & cUtilitarios.getPeriodoCompetencia(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
        lblVigencia.Visible = True
        'pnlCadastroUsuario.Visible = False


        If Not IsPostBack Then
            mConsultaUsuarios(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
        End If

        Dim alterar As String = Request.QueryString("alterar1")
        Dim incluir As String = Request.QueryString("incluir")

        ''Flag de inclus�o do cadastro de um usu�rio
        'If incluir <> "" Then
        '    pnlCadastroUsuario.Visible = True
        'End If

    End Sub

    'Private Sub btnIncluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIncluir.Click
    '    pnlCadastroUsuario.Visible = True
    '    txtConfirmacaoEmail.Text = ""
    '    txtCPF.Text = ""
    '    txtEmail.Text = ""
    '    txtLogin.Text = ""
    '    btnExcluir.Visible = False
    '    btnIncluirUsuario.Text = "Incluir"
    '    txtUsuario.Text = ""
    '    ddlAcesso.SelectedValue = "Master"
    '    ddlSituacao.SelectedValue = "Ativo"

    '    If Request.QueryString("alterar") <> "" Then
    '        Response.Redirect("ManutencaoUsuarios.aspx?incluir=ok")
    '    End If
    'End Sub

    'Private Sub btnIncluirUsuario_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIncluirUsuario.Click
    '    If Request.QueryString("alterar") = "" Then
    '        Response.Write("<script>alert('Foi enviada uma senha para o e-mail " & txtEmail.Text & ".')</script>")
    '    End If
    '    pnlCadastroUsuario.Visible = False

    'End Sub

    'Private Sub btnFechar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFechar.Click
    '    pnlCadastroUsuario.Visible = False
    'End Sub

    '''Consulta os usu�rios e preenche os campos
    

    '''Consulta os usu�rios
    Private Sub mConsultaUsuarios(ByVal apolice_id As Integer, ByVal ramo_id As Int16, ByVal subgrupo_id As Int16)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        bd.consultar_usuarios_SPS(apolice_id, ramo_id, subgrupo_id)

        Try

            Dim dt As Data.DataTable = bd.ExecutaSQL_DT

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then

                Me.grdPesquisa.CurrentPageIndex = 0
                Me.grdPesquisa.SelectedIndex = -1

                Me.ucPaginacao._valor = "2"
                Me.ucPaginacao.GridDataBind(Me.grdPesquisa, dt)
                Session("grdDescarte") = dt
            Else
                'Me.Label1.Text = "Nenhum registro encontrado."
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message + ex.StackTrace)

        End Try

    End Sub



#Region "Eventos"
    Private Sub mGridDataBind(ByVal dt As DataTable)
        Me.ucPaginacao.GridDataBind(grdPesquisa, dt)
    End Sub

    Private Sub mSelecionaGrid(ByVal ItemIndex As Integer)
        Me.grdPesquisa.SelectedIndex = ItemIndex
        Me.mGridDataBind(Session("grdDescarte"))
        CType(Me.grdPesquisa.SelectedItem.Cells(0).Controls(1), RadioButton).Checked = True
    End Sub

    Private Sub mPaginacaoClick(ByVal Argumento As String) Handles ucPaginacao.ePaginaAlterada
        Me.grdPesquisa.SelectedIndex = -1
        Select Case Argumento
            Case "pro"
                Me.grdPesquisa.CurrentPageIndex += 1
            Case "ant"                
                Me.grdPesquisa.CurrentPageIndex -= 1
            Case Else
                Me.grdPesquisa.CurrentPageIndex = Argumento
        End Select
        Me.ucPaginacao.GridDataBind(grdPesquisa, Session("grdDescarte"))
    End Sub

    Private Sub grdPesquisa_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdPesquisa.ItemDataBound
        'If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
        'CType(e.Item.Cells(0).FindControl("linkCodDoc"), HyperLink).NavigateUrl = "javascript:busca_registro('" & e.Item.Cells(1).Text.Trim & "','" & e.Item.Cells(2).Text.Trim & "','" & e.Item.Cells(4).Text.Trim & "','" & e.Item.Cells(5).Text.Trim & "');"
        'End If
        'If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
        'CType(e.Item.Cells(0).FindControl("linkCodDocDI"), HyperLink).NavigateUrl = "javascript:excluir_DI('" & e.Item.Cells(1).Text.Trim & "','" & e.Item.Cells(2).Text.Trim & "','" & e.Item.Cells(6).Text.Trim & "');"
        'End If
    End Sub

#End Region

End Class
