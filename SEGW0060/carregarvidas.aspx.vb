Public Class CarregarVidas
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblVigencia As System.Web.UI.WebControls.Label
    Protected WithEvents lblNavegacao As System.Web.UI.WebControls.Label
    Protected WithEvents btnFatura As System.Web.UI.WebControls.Button
    Protected WithEvents btnRelacaoVidas As System.Web.UI.WebControls.Button
    Protected WithEvents btnResumo As System.Web.UI.WebControls.Button
    Protected WithEvents pnlConfirma As System.Web.UI.WebControls.Panel
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button
    Protected WithEvents pnlAlerta As System.Web.UI.WebControls.Panel
    Protected WithEvents lblListagemEnviada As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Subgrupo: " & Session("subGrupo") & " -> " & "Carregar rela��o anterior"
        lblVigencia.Visible = True
        'btnImprimeCertificado.Attributes.Add("onclick", "abre_janela('Impressao.aspx?tipo=certificado','janela_msg','no','yes','500','400','','')")
        
    End Sub

    
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        pnlAlerta.Visible = True
        pnlConfirma.Visible = False
    End Sub
End Class
