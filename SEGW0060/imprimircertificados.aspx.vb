Public Class ImprimirCertificado
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblVigencia As System.Web.UI.WebControls.Label
    Protected WithEvents lblNavegacao As System.Web.UI.WebControls.Label
    Protected WithEvents btnFatura As System.Web.UI.WebControls.Button
    Protected WithEvents btnRelacaoVidas As System.Web.UI.WebControls.Button
    Protected WithEvents btnResumo As System.Web.UI.WebControls.Button
    Protected WithEvents pnlFaturaDetalhe As System.Web.UI.WebControls.Panel
    Protected WithEvents Label8 As System.Web.UI.WebControls.Label
    Protected WithEvents btnFechaPagina As System.Web.UI.WebControls.Button
    Protected WithEvents btnImprimeCertificado As System.Web.UI.WebControls.Button
    Protected WithEvents Image2 As System.Web.UI.WebControls.Image
    Protected WithEvents txtAte As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    Protected WithEvents txtDe As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label6 As System.Web.UI.WebControls.Label
    Protected WithEvents btnMarcaTodos As System.Web.UI.WebControls.Button
    Protected WithEvents CheckBox1 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents CheckBox2 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents CheckBox3 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents CheckBox4 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents CheckBox5 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents CheckBox6 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents CheckBox7 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents pnlTodasFaturas As System.Web.UI.WebControls.Panel
    Protected WithEvents lblTituloFatura As System.Web.UI.WebControls.Label
    Protected WithEvents pnlFaturas As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlFiltro As System.Web.UI.WebControls.Panel
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents txtNome As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents ddlFiltros As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents btnPesquisarPeriodo As System.Web.UI.WebControls.Button
    Protected WithEvents btnPesquisarCertificados As System.Web.UI.WebControls.Button
    Protected WithEvents txtCPF As System.Web.UI.WebControls.TextBox
    Protected WithEvents imgPlus As System.Web.UI.WebControls.ImageButton
    Protected WithEvents pnlDependente As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Subgrupo: " & Session("subGrupo") & " -> " & "Imprimir certificado individual"
        lblVigencia.Visible = True
        btnImprimeCertificado.Attributes.Add("onclick", "abre_janela('Impressao.aspx?tipo=certificado','janela_msg','no','yes','500','400','','')")
        imgPlus.Attributes.Add("onclick", "abreTreeView()")

        Dim showTree As String = Request.Form("showTree")

        Dim dependentes As String = Request.QueryString("dependentes")
        If dependentes = 1 Then
            pnlFiltro.Visible = True
            pnlFaturas.Visible = True
            pnlTodasFaturas.Visible = True            
        End If

        If showTree = "1" Then
            pnlFiltro.Visible = False
            pnlFaturas.Visible = False
            pnlTodasFaturas.Visible = False
            pnlFaturaDetalhe.Visible = True

            If pnlDependente.Visible = True Then
                pnlDependente.Visible = False
                imgPlus.ImageUrl = "images/imgmais.gif"
            Else
                pnlDependente.Visible = True
                imgPlus.ImageUrl = "Images/imgMenos.gif"
            End If
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pnlFaturaDetalhe.Visible = True
        pnlFiltro.Visible = False
        pnlFaturas.Visible = False
        pnlTodasFaturas.Visible = False

        Dim status As Boolean = True

        If txtNome.Text.Length < 3 And txtNome.Text <> "" Then
            status = False
            Response.Write("<script>alert('A pesquisa deve ser realizada com no m�nimo 3 caracteres.')</script>")
        End If
    End Sub



    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        CheckBox1.Checked = True
        CheckBox2.Checked = True

    End Sub

    Private Sub btnFechaPagina_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFechaPagina.Click
        Response.Redirect("Centro.aspx")
    End Sub

    Private Sub btnMarcaTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMarcaTodos.Click
        Dim controle As Web.UI.WebControls.Panel
        Dim chkbox As Web.UI.WebControls.CheckBox
        pnlFaturaDetalhe.Visible = True
        pnlFiltro.Visible = False
        pnlFaturas.Visible = False
        pnlTodasFaturas.Visible = False
        controle = Page.FindControl("pnlFaturaDetalhe")


        For Each c As Web.UI.Control In controle.Controls
            If c.GetType.ToString = "System.Web.UI.WebControls.CheckBox" Then

                If CType(c, Web.UI.WebControls.CheckBox).Checked = True Then
                    CType(c, Web.UI.WebControls.CheckBox).Checked = False
                Else
                    CType(c, Web.UI.WebControls.CheckBox).Checked = True
                End If
            End If
        Next
        
        'For i As Integer = 0 To controle.Controls.Count - 1
        'If controle.Controls.Item(i).GetType.ToString = "HtmlInputCheckbox" Then
        '        controle.Controls.Item(i).
        'End If

        'Next

    End Sub

    Private Sub btnImprimeCertificado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimeCertificado.Click
        Response.Write("<script>alert('Imprimindo os certificados dos titulares selecionados.')</script>")
    End Sub

    Private Sub btnPesquisarPeriodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisarPeriodo.Click
        pnlFaturaDetalhe.Visible = False
        pnlFiltro.Visible = False
        pnlFaturas.Visible = True
        pnlTodasFaturas.Visible = True
    End Sub

    Private Sub btnPesquisarCertificados_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisarCertificados.Click
        Dim status As Boolean
        status = True

        If txtNome.Text <> "" Then
            If txtNome.Text.Length < 3 Then
                Response.Write("<script>alert('A pesquisa deve ser realizada com no m�nimo 3 caracteres.')</script>")
                status = False
            End If
        End If
        If txtCPF.Text <> "" Then
            If txtCPF.Text.Length < 3 Then
                Response.Write("<script>alert('A pesquisa deve ser realizada com no m�nimo 3 caracteres.')</script>")
                status = False
            End If
        End If
        If status = True Then
            pnlFaturaDetalhe.Visible = True
            pnlFiltro.Visible = False
            pnlFaturas.Visible = False
            pnlTodasFaturas.Visible = False
        End If
    End Sub
End Class
