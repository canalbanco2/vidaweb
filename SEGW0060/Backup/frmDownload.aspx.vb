Imports ExpertPdf
Imports ExpertPdf.HtmlToPdf
Imports System.IO
Imports System.Drawing
Imports System.Web
Imports org.pdfbox.pdmodel
Imports org.pdfbox.util
Imports org.pdfbox.pdmodel.interactive.documentnavigation.destination
Imports org.pdfbox.pdmodel.interactive.documentnavigation.outline
Imports org.pdfbox.encryption
Imports org.pdfbox.pdmodel.encryption

Partial Public Class frmDownload
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GerarPDFPropostaFromHtml(Session("pdf").ToString())


    End Sub

    Private m_oPdfConverter As PdfConverter
    Public Property PdfConverter() As PdfConverter
        Get
            If IsNothing(m_oPdfConverter) Then
                m_oPdfConverter = New PdfConverter
                m_oPdfConverter.LicenseKey = "u5CJm4qKm46Cm42Vi5uIipWKiZWCgoKC"
            End If

            Return m_oPdfConverter
        End Get
        Set(ByVal value As PdfConverter)
            m_oPdfConverter = value
        End Set
    End Property

    Public ReadOnly Property AmbienteID() As Integer
        Get
            Return Alianca.Seguranca.BancoDados.cCon.Ambiente
        End Get
    End Property



    Public Sub GerarPDFPropostaFromHtml(ByVal p_sFileHtml As String) ' As String
        Try
            Dim NomeArquivoPDF As String = "Sinistros.pdf"

            PdfConverter.AddPadding = True
            PdfConverter.PdfDocumentOptions.ShowFooter = True
            PdfConverter.PdfFooterOptions.ShowPageNumber = True
            PdfConverter.PdfFooterOptions.PageNumberTextFontSize = 8

            PdfConverter.PdfFooterOptions.FooterTextFontName = "Arial"

            PdfConverter.PdfFooterOptions.FooterText = ""
            PdfConverter.PdfFooterOptions.FooterTextFontSize = 1
            PdfConverter.PdfFooterOptions.DrawFooterLine = False

            PdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4
            PdfConverter.PdfDocumentOptions.LeftMargin = 2
            PdfConverter.PdfDocumentOptions.RightMargin = 45
            PdfConverter.PdfDocumentOptions.BottomMargin = 10
            PdfConverter.PdfDocumentOptions.TopMargin = 25
            PdfConverter.PdfDocumentInfo.AuthorName = "COMPANHIA DE SEGUROS ALIANCA DO BRASIL"
            PdfConverter.PdfDocumentInfo.Title = "Resumo de Produto"
            PdfConverter.PdfDocumentInfo.Subject = "Resumo de Produto"
            PdfConverter.PdfDocumentInfo.CreatedDate = DateTime.Now
            PdfConverter.PdfFooterOptions.PageNumberingFormatString = "P�gina &p; de &P;"

            Dim CSSpath As String = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf("/")) & "/img"

            'Gerar c�digo bin�rio do PDF com CSS da p�gina
            Dim binario As Array
            binario = PdfConverter.GetPdfBytesFromHtmlString(p_sFileHtml, CSSpath)

            Try
                DownloadFileBinario(binario, Me.Response, NomeArquivoPDF)
            Catch ex As Exception
                Throw New Exception("Download: " & " " & ex.Message)
            End Try
            Return
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    Public Shared Sub DownloadFileBinario(ByVal vetor As Array, ByVal HTTPResponse As HttpResponse, ByVal NomeArquivoPDF As String)
        Dim MyPdfMemoryStream As MemoryStream = Nothing
        MyPdfMemoryStream = New MemoryStream(vetor)

        HTTPResponse.ClearHeaders()
        HTTPResponse.AddHeader("Accept-Header", MyPdfMemoryStream.Length.ToString())
        HTTPResponse.AddHeader("Content-Length", MyPdfMemoryStream.Length.ToString())
        HTTPResponse.AddHeader("content-disposition", "attachment; filename=" & NomeArquivoPDF)
        HTTPResponse.AddHeader("Expires", 0)
        HTTPResponse.AddHeader("Pragma", "cache")
        HTTPResponse.AddHeader("Cache-Control", "no-cache")
        HTTPResponse.CacheControl = "no-cache"
        HTTPResponse.AddHeader("Refresh", 0.1)
        HTTPResponse.ContentType = "text/plain"
        HTTPResponse.AddHeader("Accept-Ranges", "bytes")
        HTTPResponse.BinaryWrite(MyPdfMemoryStream.ToArray())
        HTTPResponse.Flush()
    End Sub

End Class