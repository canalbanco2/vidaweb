Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine

Partial Public Class ImprimirEndosso
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CR As New ReportDocument
        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim objDataTable As Data.DataTable = Nothing
        Dim strSql As String = ""

        'Implementação da SABL0101
        Dim linkseguro As Alianca.Seguranca.Web.LinkSeguro = New Alianca.Seguranca.Web.LinkSeguro
        linkseguro.LerUsuario("ImprimirEndosso.aspx", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))
        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, linkseguro.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
        Else
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, linkseguro.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
        End If

        strSql = "SELECT " & vbCrLf
        strSql &= "	A.PROPOSTA_ID, A.ENDOSSO_ID, F.APOLICE_ID, D.NOME, D.CPF_CNPJ, E.ENDERECO, E.MUNICIPIO, E.ESTADO, E.BAIRRO, E.CEP, B.DESCRICAO_ENDOSSO " & vbCrLf
        strSql &= "FROM " & vbCrLf
        strSql &= "	[SISAB003].SEGUROS_DB.DBO.PARAMETROS_ENDOSSO_TB A WITH (NOLOCK) " & vbCrLf
        strSql &= "	JOIN [SISAB003].SEGUROS_DB.DBO.ENDOSSO_TB B WITH (NOLOCK) " & vbCrLf
        strSql &= "		ON A.ENDOSSO_ID = B.ENDOSSO_ID " & vbCrLf
        strSql &= "		AND A.PROPOSTA_ID = B.PROPOSTA_Id " & vbCrLf
        strSql &= "		AND A.ENDOSSO_WEB = 1 " & vbCrLf
        strSql &= "	JOIN [SISAB003].SEGUROS_DB.DBO.PROPOSTA_TB C WITH (NOLOCK) " & vbCrLf
        strSql &= "		ON C.PROPOSTA_ID = A.PROPOSTA_ID " & vbCrLf
        strSql &= "	JOIN [SISAB003].SEGUROS_DB.DBO.CLIENTE_TB D WITH (NOLOCK) " & vbCrLf
        strSql &= "		ON D.CLIENTE_ID = C.PROP_CLIENTE_ID " & vbCrLf
        strSql &= "	JOIN [SISAB003].SEGUROS_DB.DBO.ENDERECO_CORRESP_TB E WITH (NOLOCK) " & vbCrLf
        strSql &= "		ON E.PROPOSTA_ID = A.PROPOSTA_ID " & vbCrLf
        strSql &= "	JOIN [SISAB003].SEGUROS_DB.DBO.APOLICE_TB F WITH (NOLOCK) " & vbCrLf
        strSql &= "		ON F.PROPOSTA_ID = A.PROPOSTA_ID " & vbCrLf
        strSql &= "WHERE " & vbCrLf
        strSql &= "	A.PROPOSTA_ID = " & Request.QueryString("proposta_id").ToString & vbCrLf
        strSql &= "	AND A.ENDOSSO_ID = " & Request.QueryString("endosso_id").ToString & vbCrLf

        BD.SQL = strSql
        objDataTable = BD.ExecutaSQL_DT

        CR.Load(Server.MapPath("reports\Endosso.rpt"))

        Dim crParameterDiscreteValue As ParameterDiscreteValue
        Dim crParameterFieldDefinitions As ParameterFieldDefinitions
        Dim crParameterFieldLocation As ParameterFieldDefinition
        Dim crParameterValues As ParameterValues

        crParameterFieldDefinitions = CR.DataDefinition.ParameterFields
        crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue

        crParameterFieldLocation = crParameterFieldDefinitions.Item("endosso_id")
        crParameterValues = crParameterFieldLocation.CurrentValues
        crParameterDiscreteValue.Value = Request.QueryString("endosso_id")
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)

        crParameterFieldLocation = crParameterFieldDefinitions.Item("apolice_id")
        crParameterValues = crParameterFieldLocation.CurrentValues
        crParameterDiscreteValue.Value = objDataTable.Rows(0).Item("APOLICE_ID").ToString
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)

        crParameterFieldLocation = crParameterFieldDefinitions.Item("proposta_id")
        crParameterValues = crParameterFieldLocation.CurrentValues
        crParameterDiscreteValue.Value = Request.QueryString("proposta_id")
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)

        crParameterFieldLocation = crParameterFieldDefinitions.Item("segurado")
        crParameterValues = crParameterFieldLocation.CurrentValues
        crParameterDiscreteValue.Value = objDataTable.Rows(0).Item("NOME").ToString
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)

        crParameterFieldLocation = crParameterFieldDefinitions.Item("cnpj")
        crParameterValues = crParameterFieldLocation.CurrentValues
        crParameterDiscreteValue.Value = objDataTable.Rows(0).Item("CPF_CNPJ").ToString
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)

        crParameterFieldLocation = crParameterFieldDefinitions.Item("endereco")
        crParameterValues = crParameterFieldLocation.CurrentValues
        crParameterDiscreteValue.Value = objDataTable.Rows(0).Item("ENDERECO").ToString
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)

        crParameterFieldLocation = crParameterFieldDefinitions.Item("cidade")
        crParameterValues = crParameterFieldLocation.CurrentValues
        crParameterDiscreteValue.Value = objDataTable.Rows(0).Item("municipio").ToString
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)

        crParameterFieldLocation = crParameterFieldDefinitions.Item("bairro")
        crParameterValues = crParameterFieldLocation.CurrentValues
        crParameterDiscreteValue.Value = objDataTable.Rows(0).Item("BAIRRO").ToString
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)

        crParameterFieldLocation = crParameterFieldDefinitions.Item("cep")
        crParameterValues = crParameterFieldLocation.CurrentValues
        crParameterDiscreteValue.Value = objDataTable.Rows(0).Item("CEP").ToString
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)

        crParameterFieldLocation = crParameterFieldDefinitions.Item("texto_endosso")
        crParameterValues = crParameterFieldLocation.CurrentValues
        crParameterDiscreteValue.Value = objDataTable.Rows(0).Item("DESCRICAO_ENDOSSO").ToString
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)

        CrystalReportViewer1.ReportSource = CR
    End Sub
End Class