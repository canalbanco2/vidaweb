﻿Public Partial Class ImprimeClausula
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Carrega_Clausulas(Request.QueryString("nSeq"))
    End Sub

    Private Sub Carrega_Clausulas(ByVal nIndex As Integer)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim cTexto As String = ""

        'Me.lblTextoClausula.Text = ""
        bd.Clausulas(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), nIndex)

        Try
            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    ' pablo.dias (Nova Consultoria) - 01/08/2011
                    ' 9420650 - Melhorias no Sistema Vida Web 
                    ' Melhorias na geração de relatório de cláusulas
                    cTexto = dr.Item("texto_clausula")
                    'Substituição das quebras de linha
                    cTexto = Replace(cTexto, Chr(13), Chr(60) & "BR" & Chr(62))
                    'Substituição dos espaços 
                    Me.lblTextoClausula.Text = cTexto
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim v_sCodigoErro As String = "1 - Carrega_Clausulas"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nCódigo: " & v_sCodigoErro, ex)
        End Try

    End Sub

End Class