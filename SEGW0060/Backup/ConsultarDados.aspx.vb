Partial Class ConsultarDados
    Inherits System.Web.UI.Page

    Dim data_inicio, data_fim, retorno As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblLimiteMinimo As System.Web.UI.WebControls.Label
    Protected WithEvents lblLimiteDiaria As System.Web.UI.WebControls.Label


    ' Protected WithEvents ucPaginacao As segw0060.paginacaogrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & " -> Subgrupo: " & Session("nomeSubgrupo") & " -> Fun��o: " & getTpAcesso()
        lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomesubGrupo") & "<br>Fun��o: Consultar Dados "

        lblVigencia.Text = "<br>" & getPeriodoCompetenciaSession()

        Carrega_Dados_do_Subgrupo()
        getCriticas()
        If Not Page.IsPostBack Then
            Me.getCoberturas(1)
        End If

        Try
            If Session("apolice") = "" Then
                cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
                Response.End()
            End If
        Catch ex As Exception
            cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
            Response.End()
        End Try

    End Sub

    Private Function getTpAcesso() As String
        Dim ind_acesso As String = Nothing
        Select Case Session("acesso").ToString
            Case "1"
                ind_acesso = "Administrador da Alian�a do Brasil"
            Case "2"
                ind_acesso = "Administrador de Ap�lice"
            Case "3"
                ind_acesso = "Administrador de Subgrupo"
            Case "4"
                ind_acesso = "Operador"
            Case "5" ' Projeto 539202 - Jo�o Ribeiro - 28/04/2009
                ind_acesso = "Consulta"
            Case "6" ' Projeto 539202 - Jo�o Ribeiro - 28/04/2009
                ind_acesso = "Central de Atendimento"
            Case "7"    ' Demanda 4370246
                ind_acesso = "Adm. Master"
        End Select

        Return ind_acesso
    End Function

    Private Sub mPreenchePrazoRestante(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)
        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        BD.dia_venc(apolice_id, ramo_id, subgrupo_id, getWF_ID)
        'Response.Write(BD.SQL)

        Try

            dr = BD.ExecutaSQL_DR()

            If dr.Read() Then
                'Me.lblPrazo.Text = "Faltam " + dr.GetValue(0).ToString() + " dias para atingir o prazo limite para informar as vidas neste sistema."
                'Me.lblPrazo.Text = "Dia " + CType(dr.Item("dt_fim_faturamento"), DateTime).ToString("dd/MM/yyyy") + " � o prazo limite para informar as vidas neste sistema."
                If IsDBNull(dr.Item("dia_cobranca")) Then
                    Me.lblDiaVenc.Text = "---"
                Else
                    Me.lblDiaVenc.Text = dr.Item("dia_cobranca")
                End If
            Else
                'Me.lblPrazo.Text = "Prazo n�o encontrado"
                Me.lblDiaVenc.Text = "---"
            End If

        Catch ex As Exception
            Dim v_sCodigoErro As String = "5 - mPreenchePrazoRestante"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        Finally
            If Not dr Is Nothing Then
                dr.Close()
            End If

            dr = Nothing
        End Try

        'Me.lblPrazo.Visible = False


    End Sub

    Private Function getWF_ID() As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim cRetorno As String = ""

        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

        Try
            dr = bd.ExecutaSQL_DR()

            If dr.Read() Then
                cRetorno = dr.GetValue(0).ToString
            End If
            dr.Close()
            dr = Nothing

        Catch ex As Exception
            Dim v_sCodigoErro As String = "5 - getWF_ID"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        Finally
            If Not dr Is Nothing Then
                dr.Close()
            End If

            dr = Nothing
        End Try
        Return cRetorno

    End Function

    Private Function getPeriodoCompetenciaSession() As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)
        'Dim data_inicio, data_fim, retorno As String
        Try

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()


            If dr.HasRows Then
                While dr.Read()

                    data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                    data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)
                    cUtilitarios.escreveScript("var dt_ini_comp = '" & data_inicio & "';")
                    cUtilitarios.escreveScript("var dt_fim_comp = '" & data_fim & "';")

                End While

                retorno = "Per�odo de Compet�ncia: " & data_inicio & " a " & data_fim
            Else
                Session.Abandon()
                'cUtilitarios.br("N�o existe nenhuma Compet�ncia de Fatura em aberto")
                'System.Web.HttpContext.Current.Response.Write("<script>parent.window.location=parent.window.location;</script>")
                Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
            End If
            dr.Close()
            dr = Nothing
        Catch ex As Exception
            Dim v_sCodigoErro As String = "1 - getPeriodoCompetenciaSession"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
            retorno = ""
        End Try
        Return retorno
    End Function

    Public Sub Carrega_Dados_do_Subgrupo()

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        'Pega Capital Maximo dos Dados do Subgrupo
        bd.GetCapitalMaximo(Session("apolice"), Session("subgrupo_id"), Session("ramo"), 6785, 0, Session("usuario"), Session("acesso"), Session("cpf"))

        'Response.Write(bd.SQL)

        Try
            dr = bd.ExecutaSQL_DR()
            If dr.HasRows Then
                dr.Read()
                Me.lblCapMax.Text = CType(dr.GetValue(0), Double).ToString("N")
            End If

            Me.lblCapMax.Text = cUtilitarios.ValidaLabels(lblCapMax.Text) '[new code 13.07 - Validacao]

        Catch ex As Exception

            Dim v_sCodigoErro As String = "2 - Carrega_Dados_do_Subgrupo"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        Finally
            If Not dr Is Nothing Then
                dr.Close()
            End If
        End Try

        'Pega primeira parte dos dados
        bd.GetDadosSubgrupoE1(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

        Try
            dr = bd.ExecutaSQL_DR()
            If dr.Read Then
                'Linha 2
                If Not dr.IsDBNull(3) Then
                    Me.lblDtIniSubg.Text = CType(dr.GetValue(3), DateTime).ToShortDateString()
                Else
                    Me.lblDtIniSubg.Text = " --- "
                End If

                Me.lblDiaMovimento.Text = dr.GetValue(4).ToString
                Me.lblDiaVenc.Text = dr.GetValue(5).ToString
                Me.lblPerPgto.Text = dr.GetValue(2).ToString
                'Linha 3
                Me.lblFormaPgto.Text = dr.GetValue(1).ToString
                Me.lblAgDebito.Text = dr.GetValue(6).ToString
                Me.lblContaCorrente.Text = dr.GetValue(7).ToString
                'Linha 4
                Me.lblCusteio.Text = dr.GetValue(0).ToString

                'Formata os campos acima se foram vazios
                If Trim(Me.lblAgDebito.Text) = "" Then
                    Me.lblAgDebito.Text = " --- "
                End If
                If Trim(Me.lblContaCorrente.Text) = "" Then
                    Me.lblContaCorrente.Text = " --- "
                End If

            Else
                'Linha 2
                Me.lblDtIniSubg.Text = " --- "
                Me.lblDiaMovimento.Text = " --- "
                Me.lblDiaVenc.Text = " --- "
                Me.lblPerPgto.Text = " --- "
                'Linha 3
                Me.lblFormaPgto.Text = " --- "
                Me.lblAgDebito.Text = " --- "
                Me.lblContaCorrente.Text = " --- "
                'Linha 4
                Me.lblCusteio.Text = " --- "
            End If

            '[new code 13.07 - Validacao]
            Me.lblDtIniSubg.Text = cUtilitarios.ValidaLabels(lblDtIniSubg.Text)
            Me.lblDiaMovimento.Text = cUtilitarios.ValidaLabels(lblDiaMovimento.Text)
            Me.lblDiaVenc.Text = cUtilitarios.ValidaLabels(lblDiaVenc.Text)
            Me.lblPerPgto.Text = cUtilitarios.ValidaLabels(lblPerPgto.Text)
            Me.lblFormaPgto.Text = cUtilitarios.ValidaLabels(lblFormaPgto.Text)
            Me.lblAgDebito.Text = cUtilitarios.ValidaLabels(lblAgDebito.Text)
            Me.lblContaCorrente.Text = cUtilitarios.ValidaLabels(lblContaCorrente.Text)
            Me.lblCusteio.Text = cUtilitarios.ValidaLabels(lblCusteio.Text)
            '[end code]

        Catch ex As Exception
            Dim v_sCodigoErro As String = "3 - Carrega_Dados_do_Subgrupo"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        Finally
            If Not dr Is Nothing Then
                dr.Close()
            End If

            dr = Nothing
        End Try

        'Pega segunda parte dos campos
        bd.GetDadosSubgrupoE2(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

        Try
            dr = bd.ExecutaSQL_DR()
            If dr.Read Then
                'Linha 4
                Me.lblIdadeMin.Text = dr.GetValue(0).ToString
                Me.lblIdadeMax.Text = dr.GetValue(1).ToString
                'Me.lblLimiteMinimo.Text = dr.GetValue(4).ToString
                'Me.lblLimiteMaximo.Text = dr.GetValue(5).ToString
                'Me.lblLimiteDiariaAs.Text = dr.GetValue(8).ToString
                'Me.lblCarencia.Text = dr.GetValue(7).ToString
                'Me.lblPercCobert.Text = dr.GetValue(6).ToString
            End If

            Me.lblIdadeMin.Text = cUtilitarios.ValidaLabels(lblIdadeMin.Text) '[new code 13.07 - Validacao]
            Me.lblIdadeMax.Text = cUtilitarios.ValidaLabels(lblIdadeMax.Text) '[new code 13.07 - Validacao]

        Catch ex As Exception
            Dim v_sCodigoErro As String = "5 - Carrega_Dados_do_Subgrupo"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        Finally
            If Not dr Is Nothing Then
                dr.Close()
            End If

            dr = Nothing
        End Try

        'Pega restante dos dados
        bd.GetDadosSubGrupoE3(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

        Try
            dr = bd.ExecutaSQL_DR
            If dr.Read Then
                'Linha 1
                Me.lblCodigo.Text = Session("subgrupo_id")
                Me.lblNome.Text = Session("nomesubgrupo")
                Me.lblCapSeg.Text = dr.GetValue(1).ToString
                Me.lblMSalarial.Text = dr.GetValue(2).ToString
                'Linha 4
                Me.lblConj.Text = dr.GetValue(0).ToString
            End If

            '[new code 13.07 - Validacao]
            Me.lblIdadeMin.Text = cUtilitarios.ValidaLabels(lblIdadeMin.Text)
            Me.lblCodigo.Text = cUtilitarios.ValidaLabels(lblCodigo.Text)
            Me.lblNome.Text = cUtilitarios.ValidaLabels(lblNome.Text)
            Me.lblCapSeg.Text = cUtilitarios.ValidaLabels(lblCapSeg.Text)
            Me.lblMSalarial.Text = cUtilitarios.ValidaLabels(lblMSalarial.Text)
            Me.lblConj.Text = cUtilitarios.ValidaLabels(lblConj.Text)
            '[end code]

            '[New code: Cobertura N�o Contratada]
            If lblConj.Text.Trim = "C�njuge inexistente" Or lblConj.Text.Trim = "Cobertura n�o contratada" Then
                DropdownSelComponente.Items.RemoveAt(1)
                DropdownSelComponente.Items.RemoveAt(1)
            End If
            '[end new code]

            Dim a As Integer = 0
            If Not checkCobertura(3) Then 'FLOW 1296063 - Marcelo Ferreira - Confitec Sistemas - 2009-10-02: Tipo Componente ID de Filho � 6, e n�o 2. - Antes: checkCobertura(2)
                For m As Integer = 0 To (Convert.ToInt32(DropdownSelComponente.Items.Count()) - 1)
                    Try
                        If DropdownSelComponente.Items(m - a).Text.IndexOf("C�njuge") <> -1 Then 'FLOW 1296063 - Marcelo Ferreira - Confitec Sistemas - 2009-10-02: Tipo Componente ID de Filho � 6, e n�o 2. - Antes: DropdownSelComponente.Items(m - a).Text.IndexOf("Filho")
                            DropdownSelComponente.Items.RemoveAt(m - a)
                            a = a + 1
                        End If
                    Catch ex As Exception

                    End Try
                Next
            End If
            a = 0
            If Not checkCobertura(6) Then 'FLOW 1296063 - Marcelo Ferreira - Confitec Sistemas - 2009-10-02: Tipo Componente ID de Filho � 6, e n�o 2. - Antes: checkCobertura(3)
                For m As Integer = 0 To DropdownSelComponente.Items.Count - 1
                    Try
                        If DropdownSelComponente.Items(m - a).Text.IndexOf("Filho") <> -1 Then 'FLOW 1296063 - Marcelo Ferreira - Confitec Sistemas - 2009-10-02: Tipo Componente ID de Filho � 6, e n�o 2. - Antes: DropdownSelComponente.Items(m - a).Text.IndexOf("C�njuge")
                            DropdownSelComponente.Items.RemoveAt(m - a)
                            a = a + 1
                        End If
                    Catch ex As Exception

                    End Try
                Next
            End If

            Me.mPreenchePrazoRestante(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

        Catch ex As Exception
            Dim v_sCodigoErro As String = "4 - Carrega_Dados_do_Subgrupo"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        Finally
            dr = Nothing
        End Try

    End Sub

    Private Sub DropdownSelComponente_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DropdownSelComponente.SelectedIndexChanged
        Me.getCoberturas(Me.DropdownSelComponente.SelectedValue)
    End Sub

    Private Sub getCoberturas(ByVal componente As String)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim bd1 As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        'Para buscar o lim.min igual a segw0065
        Dim dr1 As Data.SqlClient.SqlDataReader = Nothing

        bd.coberturas(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), componente)
        'Dim sql As New Text.StringBuilder

        'sql.Append("SELECT distinct es.class_tp_cobertura, cc.tp_cobertura_id, cb.nome,")
        'sql.Append("	case es.perc_basica WHEN 0 THEN 100 ELSE es.perc_basica END as perc_basica, es.carencia_dias, ")
        'sql.Append("	case  when isnull(VAL_LIM_MAX_IS,0) = 0 then  isnull(es.val_is,0) else isnull(VAL_LIM_MAX_IS,0) end  val_lim_max_is,")
        'sql.Append("	es.val_min_franquia as val_min_franquia ,")
        'sql.Append("	case cb.nome ")
        'sql.Append("    when (select top 1 nome from [sisab003].seguros_db.dbo.tp_cobertura_tb where nome like 'MORTE%' and nome = cb.nome) then 1")
        'sql.Append("    when (select top 1 nome from [sisab003].seguros_db.dbo.tp_cobertura_tb where nome like '%IEA%' and nome = cb.nome) then 2")
        'sql.Append("    when (select top 1 nome from [sisab003].seguros_db.dbo.tp_cobertura_tb where nome like '%IPA%' and nome = cb.nome) then 3")
        'sql.Append("    when (select top 1 nome from [sisab003].seguros_db.dbo.tp_cobertura_tb where nome like '%IPD%' and nome = cb.nome) then 4")
        'sql.Append("    else 5")
        'sql.Append("    end ordem_cobertura ,cb.tp_cobertura_id, es.lim_diaria_dias")
        'sql.Append("    FROM    [sisab003].seguros_db.dbo.escolha_sub_grp_tp_cob_comp_tb es  (NOLOCK)")
        'sql.Append("    JOIN [sisab003].seguros_db.dbo.tp_cob_comp_tb cc  (NOLOCK)")
        'sql.Append("    ON  es.tp_cob_comp_id = cc.tp_cob_comp_id ")
        'sql.Append("    JOIN [sisab003].seguros_db.dbo.tp_cobertura_tb cb  (NOLOCK) ")
        'sql.Append("    ON cc.tp_cobertura_id = cb.tp_cobertura_id  ")
        'sql.Append("    WHERE(es.apolice_id = 13776)")
        'sql.Append("    AND es.sucursal_seguradora_id = 0 ")
        'sql.Append("    AND es.seguradora_cod_susep = 6785")
        'sql.Append("    AND es.ramo_id = 93")
        'sql.Append("    AND es.sub_grupo_id =1")
        'sql.Append("    AND cc.tp_componente_id = 1")
        'sql.Append("    ORDER BY ordem_cobertura")
        'bd.SQL = sql.ToString

        'Response.Write(bd.SQL)



        Try

            dr = bd.ExecutaSQL_DR()

            ''Temporario para s� exibir na 1� vez...
            Dim vezes As Int16 = 0

            While dr.Read
                Dim tr As New Web.UI.HtmlControls.HtmlTableRow

                tr.Cells.Add(cUtilitarios.addCell(cUtilitarios.ValidaLabels(dr.Item("nome"))))
                'Regra de valid.apolice. Agora, o lim.min ser� igual ao m�ximo (16/04/07)
                If Me.lblCapSeg.Text = "Capital fixo" Then
                    tr.Cells.Add(cUtilitarios.addCell(cUtilitarios.ValidaLabels(cUtilitarios.trataMoeda(dr.Item("val_lim_max_is").ToString.Trim))))
                Else
                    'Busca lim.min igual � segw0065
                    bd1.BuscaLimMin(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
                    dr1 = bd1.ExecutaSQL_DR()
                    If vezes = 0 Then
                        If dr1.Read Then
                            'OLDCOD:->tr.Cells.Add(addCell(IIf(dr1.Item("val_min_franquia").ToString.Trim = "", " --- ", cUtilitarios.trataMoeda(dr1.Item("val_min_franquia").ToString.Trim))))
                            tr.Cells.Add(cUtilitarios.addCell(cUtilitarios.ValidaLabels(cUtilitarios.trataMoeda(cUtilitarios.getLimMinCapital()))))
                            'cUtilitarios.br(cUtilitarios.getLimMinCapital())
                        End If
                    Else
                        tr.Cells.Add(cUtilitarios.addCell(cUtilitarios.ValidaLabels(cUtilitarios.trataMoeda(dr.Item("val_min_franquia").ToString.Trim))))
                    End If
                End If
                '[new code: valida��o 13.07.07]
                tr.Cells.Add(cUtilitarios.addCell(cUtilitarios.ValidaLabels(cUtilitarios.trataMoeda(dr.Item("val_lim_max_is").ToString.Trim))))
                tr.Cells.Add(cUtilitarios.addCell(cUtilitarios.ValidaLabels(cUtilitarios.trataMoeda(dr.Item("perc_basica").ToString.Trim) & "%")))
                tr.Cells.Add(cUtilitarios.addCell(cUtilitarios.ValidaLabels(dr.Item("carencia_dias").ToString.Trim)))
                tr.Cells.Add(cUtilitarios.addCell(cUtilitarios.ValidaLabels(dr.Item("lim_diaria_dias").ToString.Trim)))

                tr.Cells.Item(1).Align = "center"
                tr.Cells.Item(2).Align = "center"
                tr.Cells.Item(3).Align = "center"
                tr.Cells.Item(4).Align = "center"
                tr.Cells.Item(5).Align = "center"

                Me.tabelaMovimentacao.Rows.Add(tr)

                vezes = 1

            End While

            If Not dr.HasRows Then
                Dim tr As New Web.UI.HtmlControls.HtmlTableRow

                tr.Cells.Add(cUtilitarios.addCell(" --- "))
                tr.Cells.Add(cUtilitarios.addCell(" --- "))
                tr.Cells.Add(cUtilitarios.addCell(" --- "))

                tr.Cells.Item(1).Align = "center"
                tr.Cells.Item(2).Align = "center"

                Me.tabelaMovimentacao.Rows.Add(tr)
            End If
        Catch ex As Exception
            Dim v_sCodigoErro As String = "4 - getCoberturas"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)

        Finally
            If Not dr Is Nothing Then
                dr.Close()
            End If
            dr = Nothing
        End Try

    End Sub

    Private Function checkCobertura(ByVal componente As String) As Boolean
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        bd.coberturas(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), componente)

        'Response.Write(bd.SQL & "<br>")

        Try
            dr = bd.ExecutaSQL_DR()

            If dr.HasRows Then
                Return True
            End If

        Catch ex As Exception
            Dim v_sCodigoErro As String = "5 - checkCobertura"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
            Return False
        Finally
            If Not dr Is Nothing Then
                dr.Close()
            End If

            dr = Nothing
        End Try

        Return False

    End Function

    Private Sub getCriticas()

        Me.lblCritica.Text = ""

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        bd.criticas(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), data_inicio)

        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Try

            dr = bd.ExecutaSQL_DR()

            If dr.Read Then

                If dr.Item("critica_todos").ToString.ToUpper <> "N" And dr.Item("critica_todos").ToString <> "" Then
                    Me.lblCritica.Text &= "Todos / "
                End If

                If dr.Item("critica_idade").ToString.ToUpper <> "N" And dr.Item("critica_idade").ToString <> "" Then
                    Me.lblCritica.Text &= "Idade maior que " & dr.Item("lim_max_idade") & " /"
                End If

                If dr.Item("critica_capital").ToString.ToUpper <> "N" And dr.Item("critica_capital").ToString <> "" Then
                    Me.lblCritica.Text &= "Capital maior que " & CType(dr.Item("lim_max_capital"), Double).ToString("N") & " /"
                End If

                If Me.lblCritica.Text.TrimEnd.EndsWith("/") Then
                    Me.lblCritica.Text = Me.lblCritica.Text.Trim.Substring(0, Me.lblCritica.Text.Trim.Length - 2)
                End If

                If Me.lblCritica.Text.Trim = "" Then
                    Me.lblCritica.Text = "---"
                End If

            Else
                Me.lblCritica.Text = "---"
            End If

            Me.lblCritica.Text = cUtilitarios.ValidaLabels(Me.lblCritica.Text) '[new code validation 13.07.07]


        Catch ex As Exception
            Dim v_sCodigoErro As String = "6 - getCriticas"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        Finally
            If Not dr Is Nothing Then
                dr.Close()
            End If

            dr = Nothing
        End Try


    End Sub

End Class


