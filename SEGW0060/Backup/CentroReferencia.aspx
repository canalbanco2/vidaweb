<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CentroReferencia.aspx.vb" Inherits="segw0060.CentroReferencia"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CentroReferencia</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<P>
				<asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela" Visible="False">Label</asp:label><BR>
				<asp:label id="lblVigencia" runat="server" CssClass="Caminhotela"><br>COTEMINAS S.A - Per�odo de compet�ncia: 01/12/2006 at� 31/12/2006</asp:label></P>
			<BR>
			<asp:Panel id="pnlFaturaAtual" runat="server" Visible="False" BorderStyle="Solid" BorderWidth="1px"
				Width="300px" BorderColor="#CCCCCC" style="PADDING-RIGHT:4px; PADDING-LEFT:10px; PADDING-BOTTOM:4px; MARGIN-LEFT:20px; PADDING-TOP:4px">
				<P></P>
				<P>
					<asp:Label id="Label1" runat="server" CssClass="Caminhotela">Fatura Atual</asp:Label></P>
				<P>&nbsp;&nbsp;&nbsp;
					<asp:HyperLink id="HyperLink1" runat="server" CssClass="Caminhotela2" Height="16px" NavigateUrl="../segw0065/MovimentacaoUmaUm.aspx">Movimenta��o on-line</asp:HyperLink><BR>
					&nbsp;&nbsp;&nbsp;
					<asp:HyperLink id="HyperLink2" runat="server" CssClass="Caminhotela2" Height="16px" NavigateUrl="../segw0066/MovimentacaoMassa.aspx">Movimenta��o por arquivo</asp:HyperLink><BR>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:HyperLink id="HyperLink3" runat="server" CssClass="Caminhotela2" Height="16px" NavigateUrl="../segw0068/VidasCriticadas.aspx">Resultado processamento arquivo</asp:HyperLink><BR>
					&nbsp;&nbsp;&nbsp;
					<asp:HyperLink id="HyperLink4" runat="server" CssClass="Caminhotela2" Height="16px" NavigateUrl="../segw0078/CarregarVidas.aspx">Carregar rela��o anterior</asp:HyperLink><BR>
					&nbsp;&nbsp;&nbsp;
					<asp:HyperLink id="HyperLink5" runat="server" CssClass="Caminhotela2" NavigateUrl="../segw0069/FechamentoFatura.aspx">Encerrar movimenta��o</asp:HyperLink></P>
			</asp:Panel>
			<asp:Panel id="pnlFaturasAnteriores" runat="server" Visible="False" BorderStyle="Solid" BorderWidth="1px"
				Width="300px" BorderColor="#CCCCCC" style=" PADDING-RIGHT:4px; PADDING-LEFT:10px; PADDING-BOTTOM:4px; MARGIN-LEFT:20px; PADDING-TOP:4px">
<P></P>
<P></P>
<P>
					<asp:Label id="Label2" runat="server" CssClass="Caminhotela">Faturas Anteriores</asp:Label></P>
<P>&nbsp;&nbsp;&nbsp;
					<asp:HyperLink id="HyperLink10" runat="server" CssClass="Caminhotela2" NavigateUrl="ConsultarFaturas.aspx">Consultar / Imprimir fatura</asp:HyperLink><BR>
					&nbsp;&nbsp;&nbsp;
			</asp:Panel>
			<asp:Panel id="pnlAdministracao" runat="server" Visible="False" BorderStyle="Solid" BorderWidth="1px"
				Width="300px" BorderColor="#CCCCCC" style=" PADDING-RIGHT:4px; PADDING-LEFT:10px; PADDING-BOTTOM:4px; MARGIN-LEFT:20px; PADDING-TOP:4px">
				<P></P>
				<P></P>
				<P>
					<asp:Label id="Label4" runat="server" CssClass="Caminhotela">Administra��o</asp:Label></P>
				<P>&nbsp;&nbsp;&nbsp;
					<asp:HyperLink id="HyperLink13" runat="server" CssClass="Caminhotela2" Height="16px" NavigateUrl="../segw0076/ManutencaoUsuarios.aspx">Manuten��o de usu�rios</asp:HyperLink><BR>
					&nbsp;&nbsp;&nbsp;
					<asp:HyperLink id="HyperLink6" runat="server" CssClass="Caminhotela2" NavigateUrl="../segw0075/listaSubstituto.aspx">Atribuir usu�rio ao subgrupo</asp:HyperLink></P>
			</asp:Panel>
			<asp:Panel id="pnlApoliceSubgrupo" runat="server" Visible="False" BorderStyle="Solid" BorderWidth="1px"
				Width="300px" BorderColor="#CCCCCC" style=" PADDING-RIGHT:4px; PADDING-LEFT:10px; PADDING-BOTTOM:4px; MARGIN-LEFT:20px; PADDING-TOP:4px">
				<P></P>
				<P></P>
				<P>
					<asp:Label id="Label3" runat="server" CssClass="Caminhotela">Ap�lice / Subgrupo</asp:Label></P>
				<P>&nbsp;&nbsp;&nbsp;
					<asp:HyperLink id="HyperLink12" runat="server" CssClass="Caminhotela2" Height="16px" NavigateUrl="#">Selecionar ap�lice/subgrupo</asp:HyperLink><BR>
					&nbsp;&nbsp;&nbsp;
					<asp:HyperLink id="HyperLink11" runat="server" CssClass="Caminhotela2" Height="16px" NavigateUrl="ConsultarDados.aspx">Consultar dados subgrupo</asp:HyperLink><BR>
					&nbsp;&nbsp;&nbsp;
					<asp:HyperLink id="HyperLink8" runat="server" CssClass="Caminhotela2" Height="16px" NavigateUrl="../segw0081/LayoutArquivo.aspx">Configurar layout do arquivo</asp:HyperLink><BR>
					&nbsp;&nbsp;&nbsp;
					<asp:HyperLink id="HyperLink7" runat="server" CssClass="Caminhotela2" NavigateUrl="Centro.aspx">Checklist</asp:HyperLink></P>
			</asp:Panel>
		</form>
	</body>
</HTML>
