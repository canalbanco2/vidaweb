<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ajudaF.aspx.vb" Inherits="segw0060.ajudaF" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Ajuda</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<div id="cabecAjuda"><IMG src="Images/ajuda-1.gif">
			</div>
			<P>&nbsp;</P>
			<TABLE id="Table1" height="61" cellSpacing="0" cellPadding="0" width="94%" align="center"
				border="0">
				<TR>
					<TD><asp:label id="Label7" runat="server" CssClass="ajudaTituloItem">Resumo das movimenta��es da rela��o de vidas atual detalhada e o comparativo com os dados consolidados da �ltima fatura</asp:label>
						<br>
						<br>
					</TD>
				</TR>
				<TR>
					<TD><asp:label id="Label14" runat="server" CssClass="ajudaTituloItem">Vidas</asp:label>
						<span class="ajudaItem">- Quantidade de vidas seguradas (rela��o de vidas encaminhadas pela empresa)</span>
					</TD>
				</TR>
				<TR>
					<TD><asp:label id="Label15" runat="server" CssClass="ajudaTituloItem">Capital (R$)</asp:label>
						<span class="ajudaItem">- Capital segurado total de todas as vidas da rela��o encaminhada pela empresa.</span>
					</TD>
				</TR>
				<TR>
					<TD><asp:label id="Label16" runat="server" CssClass="ajudaTituloItem">Pr�mio</asp:label>
						<span class="ajudaItem">- Valor estimado do pr�mio a ser pago <br><b>OBS.: O valor do 
								pr�mio pode sofrer altera��es ap�s an�lise da Companhia</b>.</span>
						<br>
						<br>
					</TD>
				</TR>
				<TR>
					<TD><asp:label id="Label17" runat="server" CssClass="ajudaTituloItem">Fatura Anterior</asp:label>
						<span class="ajudaItem">- Dados consolidados da �ltima fatura emitida.</span>
					</TD>
				</TR>
				<TR>
					<TD><asp:label id="Label1" runat="server" CssClass="ajudaTituloItem">Inclus�es</asp:label>
						<span class="ajudaItem">- Quantidade de pessoas inclu�das na rela��o de vidas no per�odo de compet�ncia atual.</span>
					</TD>
				</TR>
				<TR>
					<TD><asp:label id="Label2" runat="server" CssClass="ajudaTituloItem">Exclus�es</asp:label>
						<span class="ajudaItem">- Quantidade de pessoas exclu�das na rela��o de vidas no per�odo de compet�ncia atual.</span>
					</TD>
				</TR>
				<TR>
					<TD><asp:label id="Label3" runat="server" CssClass="ajudaTituloItem">Altera��es</asp:label>
						<span class="ajudaItem">- Quantidade de pessoas que tiveram algum dado alterado na rela��o de vidas no per�odo de compet�ncia atual.</span>
					</TD>
				</TR>
				<TR>
					<TD><asp:label id="Label4" runat="server" CssClass="ajudaTituloItem">Acertos</asp:label>
						<span class="ajudaItem">- Quantidade de opera��es retroativas � Ex.: Inclus�o de um segurado que deveria constar na fatura do per�odo anterior.</span>
					</TD>
				</TR>
				<TR>
					<TD><asp:label id="Label5" runat="server" CssClass="ajudaTituloItem">DPS Pendente</asp:label>
						<span class="ajudaItem">- Quantidade de pessoas que devem enviar a Declara��o Pessoal de Sa�de (DPS), para que, ap�s an�lise da Companhia, sejam inclusas na rela��o de vidas.</span>
					</TD>
				</TR>
				<TR>F
					<TD><asp:label id="Label6" runat="server" CssClass="ajudaTituloItem">Fatura Atual</asp:label>
						<span class="ajudaItem">- Dados consolidados da rela��o de vidas atual (estimativa sujeita a altera��es).</span>
					</TD>
				</TR>
				<!--TR>
					<TD><br>
						<asp:label id="Label8" runat="server" CssClass="ajudaTituloItem">1</asp:label>
						<span class="ajudaItem">- As quantidades de vida e os valores de capital podem englobar mais de um per�odo.<br></span>
					</TD>
				</TR>
				<TR>
					<TD><asp:label id="Label9" runat="server" CssClass="ajudaTituloItem">2</asp:label>
						<span class="ajudaItem">- O Pr�mio indicado � uma estimativa.<br></span>
					</TD>
				</TR-->
			</TABLE>
			<BR>
			<P></P>
		</form>
	</body>
</HTML>
