Partial Class CentroReferencia
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents HyperLink9 As System.Web.UI.WebControls.HyperLink

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim secao As String = Request.QueryString("secao")
        Dim mov_online As String = ""

        Dim linkseguro As Alianca.Seguranca.Web.LinkSeguro

        'Implementa��o da SABL0101
        linkseguro = New Alianca.Seguranca.Web.LinkSeguro
        Dim usuario As String = linkseguro.LerUsuario("", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))
        If Request.QueryString("fatura") = "" Then
            If usuario = "" Then
                Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
            End If

            Session("usuario_id") = linkseguro.Usuario_ID
            Session("usuario") = linkseguro.Login_REDE
            Session("cpf") = linkseguro.CPF
        End If


        Dim cAmbiente As Alianca.Seguranca.Web.ControleAmbiente
        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
        cAmbiente.ObterAmbiente(url)
        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
        Else
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
        End If


        Dim str As String = "&"

        For Each m As String In Session.Keys
            If Not IsNothing(Session(m)) Then
                If Session(m).GetType.ToString = "System.String" Then
                    str &= System.Web.HttpUtility.UrlEncode(m) & "=" & System.Web.HttpUtility.UrlEncode(Session(m)) & "&"
                End If
            End If
        Next


        'Links do Menu
        mov_online = linkseguro.GerarParametro("", Alianca.Seguranca.Web.LinkSeguro.TempoExpiracao.Grande, Request.ServerVariables("REMOTE_ADDR"), Session("usuario_id"))


        lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomesubGrupo") & "<br>Fun��o: Hist�rico e pr�ximos passos"
        lblVigencia.Text = "<br>" & cUtilitarios.getPeriodoCompetencia(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
        lblNavegacao.Visible = True
        lblVigencia.Visible = True

        HyperlinkPorArquivo.NavigateUrl = HyperlinkPorArquivo.NavigateUrl & "?SLinkSeguro=" & mov_online & "&expandir=1" & str
        HyperLink13.NavigateUrl = HyperLink13.NavigateUrl & "?SLinkSeguro=" & mov_online & "&expandir=1" & str
        HyperLink6.NavigateUrl = HyperLink6.NavigateUrl & "?SLinkSeguro=" & mov_online & "&expandir=1" & str        
        HyperLink11.NavigateUrl = HyperLink11.NavigateUrl & "?SLinkSeguro=" & mov_online & "&expandir=1" & str
        HyperLink8.NavigateUrl = HyperLink8.NavigateUrl & "?SLinkSeguro=" & mov_online & "&expandir=1" & str
        HyperLink7.NavigateUrl = HyperLink7.NavigateUrl & "?SLinkSeguro=" & mov_online & "&expandir=1" & str
        HyperLink10.NavigateUrl = HyperLink10.NavigateUrl & "?SLinkSeguro=" & mov_online & "&expandir=1" & str
        HyperLink5.NavigateUrl = HyperLink5.NavigateUrl & "?SLinkSeguro=" & mov_online & "&expandir=1" & str
        HyperLink4.NavigateUrl = HyperLink4.NavigateUrl & "?SLinkSeguro=" & mov_online & "&expandir=1" & str
        HyperLink3.NavigateUrl = HyperLink3.NavigateUrl & "?SLinkSeguro=" & mov_online & "&expandir=1" & str
        HyperLink2.NavigateUrl = HyperLink2.NavigateUrl & "?SLinkSeguro=" & mov_online & "&expandir=1" & str
        HyperLink1.NavigateUrl = HyperLink1.NavigateUrl & "?SLinkSeguro=" & mov_online & "&expandir=1" & str


        HyperLink12.Attributes.Add("onclick", "top.__doPostBack('lnkTrocar','')")

        If secao = "Administra��o" Then
            pnlAdministracao.Visible = True
        ElseIf secao = "Faturas Atuais" Then
            pnlFaturaAtual.Visible = True
        ElseIf secao = "Faturas Anteriores" Then
            pnlFaturasAnteriores.Visible = True
        ElseIf secao = "Ap�lice / Subgrupo" Then
            pnlApoliceSubgrupo.Visible = True
        End If

        Try
            If Session("apolice") = "" Then
                cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
                Response.End()
            End If
        Catch ex As Exception
            cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
            Response.End()
        End Try
    End Sub

End Class
