<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ConsultarDados.aspx.vb" Inherits="segw0060.ConsultarDados"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ConsultarDados</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela">Label</asp:label><asp:label id="lblVigencia" runat="server" CssClass="Caminhotela"><br>COTEMINAS S.A - Per�odo de compet�ncia: 01/12/2006 at� 31/12/2006</asp:label><BR>
			<BR>
			<br>
			<asp:panel id="pnlApresentacao" runat="server" HorizontalAlign="Center">
				<DIV class="CaminhoTela" style="TEXT-ALIGN: left">Dados de Subgrupo</DIV>
				<TABLE id="Table2" style="BORDER-RIGHT: #003399 1px solid; BORDER-TOP: #003399 1px solid; BORDER-LEFT: #003399 1px solid; BORDER-BOTTOM: #003399 1px solid"
					height="171" cellSpacing="3" cellPadding="1" width="100%" align="center" border="0"> <!-- PRIMEIRA LINHA -->
					<TR class="titulo-tabela sem-sublinhado">
						<TD height="18">C�digo</TD>
						<TD colSpan="2" height="18">Nome</TD>
						<TD height="18">Capital Segurado</TD>
						<TD height="18">Capital M�ximo</TD>
						<TD height="18">M�lt. Salarial</TD>
					</TR>
					<TR>
						<TD>
							<asp:Label id="lblCodigo" runat="server"></asp:Label></TD>
						<TD colSpan="2">
							<asp:Label id="lblNome" runat="server"></asp:Label></TD>
						<TD>
							<asp:Label id="lblCapSeg" runat="server"></asp:Label></TD>
						<TD>
							<asp:Label id="lblCapMax" runat="server"></asp:Label></TD>
						<TD align="center">
							<asp:Label id="lblMSalarial" runat="server"></asp:Label></TD>
					</TR> <!-- SEGUNDA LINHA -->
					<TR class="titulo-tabela sem-sublinhado">
						<TD>Dt. In�cio Subgrupo</TD>
						<TD>Dia de Movimento</TD>
						<TD>Dia de Vencimento</TD>
						<TD>Per�odo de Pgto</TD>
						<TD colSpan="2">Cr�tica DPS</TD>
					</TR>
					<TR>
						<TD>
							<asp:Label id="lblDtIniSubg" runat="server"></asp:Label></TD>
						<TD>
							<asp:Label id="lblDiaMovimento" runat="server"></asp:Label></TD>
						<TD>
							<asp:Label id="lblDiaVenc" runat="server"></asp:Label></TD>
						<TD>
							<asp:Label id="lblPerPgto" runat="server"></asp:Label></TD>
						<TD colSpan="2">
							<asp:Label id="lblCritica" runat="server"></asp:Label></TD>
					</TR> <!-- TERCEIRA LINHA -->
					<TR class="titulo-tabela sem-sublinhado">
						<TD colSpan="2">Forma de Pagamento</TD>
						<TD colSpan="2">Ag�ncia D�bito</TD>
						<TD colSpan="2">Conta corrente</TD>
					</TR>
					<TR>
						<TD colSpan="2">
							<asp:Label id="lblFormaPgto" runat="server"></asp:Label></TD>
						<TD colSpan="2">
							<asp:Label id="lblAgDebito" runat="server"></asp:Label></TD>
						<TD colSpan="2">
							<asp:Label id="lblContaCorrente" runat="server"></asp:Label></TD>
					</TR> <!-- QUARTA LINHA -->
					<TR class="titulo-tabela sem-sublinhado">
						<TD>Idade M�n. Mov.</TD>
						<TD>Idade M�x. Mov</TD>
						<TD colSpan="2">C�njuge</TD>
						<TD colSpan="2">Custeio</TD>
					</TR>
					<TR>
						<TD>
							<asp:Label id="lblIdadeMin" runat="server"></asp:Label></TD>
						<TD>
							<asp:Label id="lblIdadeMax" runat="server"></asp:Label></TD>
						<TD colSpan="2">
							<asp:Label id="lblConj" runat="server"></asp:Label></TD>
						<TD colSpan="2">
							<asp:Label id="lblCusteio" runat="server"></asp:Label></TD>
					</TR>
				</TABLE>
				<BR>
				<BR>
				<asp:panel id="Panel2" runat="server" HorizontalAlign="Left">
					<TABLE id="tabelaMovimentacao2" style="MARGIN-TOP: 2px; MARGIN-BOTTOM: 0px; MARGIN-LEFT: 0px; BORDER-COLLAPSE: collapse"
						borderColor="#cccccc" height="177" cellSpacing="0" cellPadding="0" align="center" border="0">
						<TR>
							<TD vAlign="bottom">
								<asp:Label id="Label1" runat="server" CssClass="Caminhotela">&nbsp;</asp:Label></TD>
						</TR>
						<TR>
							<TD colSpan="4">
								<asp:panel id="Panel1" runat="server" HorizontalAlign="Left">
									<TABLE id="tabelaMovimentacao2" style="MARGIN-TOP: 10px; MARGIN-BOTTOM: 10px; MARGIN-LEFT: 0px; BORDER-COLLAPSE: collapse"
										borderColor="#cccccc" cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<TD vAlign="bottom">
												<asp:Label id="Label2" runat="server" CssClass="Caminhotela" Width="88px">Coberturas</asp:Label></TD>
										</TR>
										<TR>
											<TD vAlign="middle" height="20">Selecione o Componente:
												<asp:DropDownList id="DropdownSelComponente" runat="server" Width="113px" AutoPostBack="True">
													<asp:ListItem Value="1">001 - Titular</asp:ListItem>													
													<asp:ListItem Value="3">003 - C�njuge</asp:ListItem>
													<asp:ListItem Value="6">006 - Filho</asp:ListItem>
												</asp:DropDownList></TD>
										</TR>
										<TR>
											<TD align="center">
												<DIV style="OVERFLOW: auto" DESIGNTIMEDRAGDROP="740">
													<DIV style="OVERFLOW: hidden" DESIGNTIMEDRAGDROP="900">
														<TABLE id="tabelaMovimentacao" style="MARGIN-TOP: 0px; MARGIN-BOTTOM: 0px; MARGIN-LEFT: 0px; BORDER-COLLAPSE: collapse"
															borderColor="#cccccc" cellSpacing="0" cellPadding="0" width="100%" border="1" runat="server">
															<TR class="titulo-tabela sem-sublinhado">
																<TD align="center" width="500">Nome Cobertura</TD>
																<TD align="center" width="90">Limite M�nimo</TD>
																<TD align="center" width="120">Limite M�ximo</TD>
																<TD align="center" width="150">Perc.de Cobertura B�sica do Titular</TD>
																<TD align="center" width="120">Car�ncia</TD>
																<TD align="center" width="120">Limite de Di�rias</TD>
															</TR>
															<TR>
																<TD align="center">
																	<asp:Label id="Label3" runat="server"></asp:Label></TD>
																<TD align="center">
																	<asp:Label id="lblLimiteMaximo" Runat="server"></asp:Label></TD>
																<TD align="center">
																	<asp:Label id="lblLimiteM�nimo" Runat="server"></asp:Label></TD>
																<TD align="center">
																	<asp:Label id="lblPercCobert" Runat="server"></asp:Label></TD>
																<TD align="center">
																	<asp:Label id="lblCarencia" Runat="server"></asp:Label></TD>
																<TD align="center">
																	<asp:Label id="lblLimiteDiariaAs" Runat="server"></asp:Label></TD>
															</TR>
														</TABLE>
													</DIV>
												</DIV>
											</TD>
										</TR>
									</TABLE>
								</asp:panel></TD>
						</TR>
					</TABLE>
				</asp:panel>
				<DIV class="centro-mensagem2" style="MARGIN-LEFT: -15px">Se alguma das informa��es 
					acima apresentadas necessitarem de altera��o, entre em contato com a <b>Central de 
					Atendimento aos Clientes (0800 729 7000 - Op��o 3 - 1 - 1)</b>, sua Ag�ncia de Relacionamento BB 
					ou seu Consultor de Seguros.</DIV>
			</asp:panel></form>
		<script>
		top.escondeaguarde();
		</script>
	</body>
</HTML>
