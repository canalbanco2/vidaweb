﻿Option Explicit On
Option Strict On

Imports System
Imports System.Runtime.InteropServices
Imports SHDocVw

Namespace InternetExplorer

    Public Class ieTeste
        Public Shared Sub Main()
            Dim explorer As New ieTeste()
            explorer.Run()
        End Sub

        Public Sub Run()
            Dim o As Object = Nothing
            Dim s As String

            Try
                ' Starts the browser.
                m_IExplorer = New SHDocVw.InternetExplorer()
            Catch e As Exception
                Console.WriteLine("Exception when creating Internet Explorer object {0}", e)
                Return
            End Try

            ' Wires your event handlers to m_IExplorer.
            SetAllEvents()

            Try
                ' Goes to the home page.
                m_WebBrowser = CType(m_IExplorer, IWebBrowserApp)
                m_WebBrowser.Visible = True
                m_WebBrowser.GoHome()

                ' Starts navigating to different URLs.
                Console.Write("Enter URL (or enter to quit): ")
                s = Console.ReadLine()
                While s <> "" And Not (m_IExplorer Is Nothing) _
                And Not (m_WebBrowser Is Nothing)
                    m_WebBrowser.Navigate(s, o, o, o, o)
                    Console.Write("Enter URL (or enter to quit): ")
                    s = Console.ReadLine()
                End While
                m_WebBrowser.Quit()
            Catch sE As Exception
                If m_IExplorer Is Nothing And m_WebBrowser Is Nothing Then
                    Console.WriteLine("Internet Explorer has gone away")
                Else
                    Console.WriteLine("Exception happens {0}", sE)
                End If
            End Try
        End Sub

        ' Uses the AddHandler for adding delegates to events.
        Sub SetAllEvents()
            If Not (m_IExplorer Is Nothing) Then
                ' Title Change event
                ' DWebBrowserEvents2 is the name of the sink event interface.
                ' TitleChange is the name of the event.
                ' DWebBrowserEvents2_TitleChangeEventHandler is the delegate 
                ' name assigned by TlbImp.exe.
                'Dim DTitleChangeE As New _DWebBrowserEvents2_TitleChangeEventHandler(AddressOf OnTitleChange)
                'AddHandler m_IExplorer.TitleChange, DTitleChangeE
            End If
        End Sub

        '----------------------------------------------------------------
        ' Defines event handlers.
        ' Document title changed
        Shared Sub OnTitleChange(ByVal sText As String)
            Console.WriteLine("Title changes to {0}", sText)
        End Sub


        'End Sub
        '----------------------------------------------------------------
        ' The following are class fields.
        Private Shared m_IExplorer As SHDocVw.InternetExplorer = Nothing
        Private Shared m_WebBrowser As IWebBrowserApp = Nothing
    End Class


End Namespace