﻿Imports System.Collections.Generic

'Matheus Souza
'21/06/2010
'Responsável por consultar os dados de assistencias.
Public Class cAssistenciaBusiness
    Public v_sCodigoErro As String

    'Matheus Souza
    '21/06/2010
    'obtem os planos de assistência cadastrados no subgrupo. 
    Public Function ConsultaPlanos(ByVal apolice_id As Long, _
                            ByVal Ramo_id As Integer, ByVal Sucursal_id As Integer, _
                            ByVal Seguradora_id As Integer, _
                            ByVal DataInicioVigenciaSubgrupo As Date, _
                            ByVal SubGrupoid As Integer, _
                            ByVal Data_Sistema As Date, _
                            ByVal glambiente_id As Alianca.Seguranca.BancoDados.cCon.Ambientes, _
                            ByVal SiglaSistema As String, _
                            ByVal SiglaRecurso As String, _
                            ByVal DescricaoRecurso As String) As List(Of cAssistencia_EO)
        Try

            Dim assistencias As List(Of cAssistencia_EO) = New List(Of cAssistencia_EO)

            v_sCodigoErro = "1 - ConsultaPlanos/cAssistenciaBusiness"

            Dim oSubGrupo As Object = CreateObject("SEGL0308.Cls00492")

            v_sCodigoErro = "2 - ConsultaPlanos/cAssistenciaBusiness"

            Dim dataSistema As String = FormatDateTime(Data_Sistema, DateFormat.ShortDate)
            Dim dataInicioVigencia As String = FormatDateTime(DataInicioVigenciaSubgrupo, DateFormat.ShortDate)

            v_sCodigoErro = "3 - ConsultaPlanos/cAssistenciaBusiness"

            If (oSubGrupo.ConsultarPlanos(SiglaSistema, _
                                          SiglaRecurso, _
                                          DescricaoRecurso, _
                                          glambiente_id, _
                                          dataSistema, _
                                          CLng(apolice_id), _
                                          CShort(Ramo_id), _
                                          CShort(Sucursal_id), _
                                          CShort(Seguradora_id), _
                                          dataInicioVigencia, _
                                          CShort(SubGrupoid))) Then

                For Each assistencia491 As Object In oSubGrupo.PlanosAssistencia
                    Dim assistencia As cAssistencia_EO = New cAssistencia_EO

                    v_sCodigoErro = "4 - ConsultaPlanos/cAssistenciaBusiness"

                    assistencia.PlanoId = assistencia491.iPlanoId
                    assistencia.InicioVigencia = assistencia491.sInicioVigencia
                    assistencia.CustoAssistencia = assistencia491.dCustoAssistencia
                    assistencia.TipoAssistencia = assistencia491.sTipoAssistencia
                    assistencia.Status = assistencia491.sStatus
                    assistencia.Sequencial = assistencia491.iSequencial
                    assistencia.CustoAssistenciaNumVidas = assistencia491.dCustoAssistenciaNumVidas
                    assistencia.InicioAssistenciaSubGrupo = assistencia491.sInicioAssistenciaSubGrupo
                    assistencia.FimAssistenciaSubGrupo = assistencia491.sFimAssistenciaSubGrupo
                    assistencia.TipoAssistenciaId = assistencia491.iTipoAssistenciaId

                    v_sCodigoErro = "5 - ConsultaPlanos/cAssistenciaBusiness"

                    For Each servico490 As Object In assistencia491.ServicoAssistencia
                        Dim servico As cServicoAssistencia_EO = New cServicoAssistencia_EO
                        servico.ServicoId = servico490.iServicoId
                        servico.NomeServico = servico490.sNomeServico
                        servico.Limite = servico490.sLimite
                        servico.Abrangencia = servico490.sAbrangencia
                        assistencia.ServicoAssistencia.Add(servico)
                    Next

                    v_sCodigoErro = "6 - ConsultaPlanos/cAssistenciaBusiness"

                    assistencias.Add(assistencia)

                    v_sCodigoErro = "7 - ConsultaPlanos/cAssistenciaBusiness"

                Next

            End If

            Return assistencias
            v_sCodigoErro = "8 - ConsultaPlanos/cAssistenciaBusiness"

        Catch ex As Exception
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nCódigo: " & v_sCodigoErro, ex)
        End Try


    End Function

End Class
