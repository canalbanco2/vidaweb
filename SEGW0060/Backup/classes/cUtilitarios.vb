Public Class cUtilitarios

    Public mensagem As String = ""

    'Trata a data para o seuginte formato dd/mm/aaaa hh:mm
    Public Shared Function trataDataHora(ByVal data As String) As String

        Try
            Dim dt_final As String

            Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim ano As String = CType(data, DateTime).Year

            Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = dia + "/" + mes + "/" + ano


            Return dt_final + " " + hora + ":" + minuto

        Catch ex As Exception

            Return ""

        End Try

    End Function

    'Exibe o valor em um alert na tela
    Public Shared Sub br(ByVal valor As String)

        Dim texto As String

        texto = "<script>" + vbNewLine
        texto &= "alert(""" + valor + """);"
        texto &= "</script>"

        System.Web.HttpContext.Current.Response.Write(texto)
    End Sub


    Public Shared Function trataMoeda(ByVal valor As String) As String
        'br(valor)
        If valor = "&nbsp;" Or valor = "" Then
            Return ""
        Else
            'Return String.Format(String.Format("{0:c}", CDbl(valor))).Substring(3)
            Dim minus As String = ""

            If String.Format(String.Format("{0:c}", CDbl(valor))).IndexOf(")") <> -1 Then
                minus = "-"
            End If

            Return minus & String.Format(String.Format("{0:c}", CDbl(valor))).Substring(3).Replace("(", "").Replace(")", "").Replace(" ", "")
        End If

    End Function


    Public Shared Function trataCPF(ByVal cpf As String) As String

        If cpf.Length = 11 Then
            Return cpf.Substring(0, 3) + "." + cpf.Substring(3, 3) + "." + cpf.Substring(6, 3) + "-" + cpf.Substring(9, 2)
        Else
            Return cpf
        End If

    End Function

    Public Shared Function destrataCPF(ByVal cpf As String) As String

        If cpf.Length = 14 Then
            Return cpf.Replace(".", "").Replace("-", "")
        Else
            Return cpf
        End If

    End Function

    Public Shared Sub escreveScript(ByVal valor As String)
        Web.HttpContext.Current.Response.Write("<script>" + valor + "</script>")
    End Sub

    Public Shared Sub escreve(ByVal valor As String)
        Web.HttpContext.Current.Response.Write(valor)
    End Sub

    'Fun��o n�o utilit�ria, mas repetitiva... com isso n�o fica sendo necess�rio reescreve-la
    'Fun��o para validar o periodo de competencia
    Public Shared Function getPeriodoCompetencia(ByVal apolice As Integer, ByVal ramo As Integer, ByVal subgrupo_id As Integer) As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5696_SPS(apolice, ramo, subgrupo_id, cConstantes.CWORKFLOW, cConstantes.CVERSAOID)
        'cUtilitarios.br(bd.SQL)
        Dim data_inicio As String = ""
        Dim data_fim As String = ""
        Dim retorno As String = ""

        Try
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.HasRows Then
                While dr.Read()

                    data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                    data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)

                End While

                retorno = "Per�odo de Compet�ncia: " & data_inicio & " a " & data_fim
            Else
                cUtilitarios.br("N�o existe nenhuma Compet�ncia de Fatura em aberto")
                'System.Web.HttpContext.Current.Session("abandonar") = 1
                'System.Web.HttpContext.Current.Response.Write("<script>parent.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp'</script>")
                System.Web.HttpContext.Current.Response.End()
                retorno = ""
            End If
            dr.Close()
            dr = Nothing
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return retorno

    End Function

    'Trata a data para o seguinte formato dd/mm/aaaa 
    Public Shared Function trataData(ByVal data As String) As String

        Try

            Dim dt_arr() As String = data.Split("/")
            Dim dt_final As String

            'Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim dia As String = dt_arr(0).PadLeft(2, "0")
            'Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim mes As String = dt_arr(1).PadLeft(2, "0")
            'Dim ano As String = CType(data, DateTime).Year
            Dim ano As String = dt_arr(2)

            'Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            'Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = dia + "/" + mes + "/" + ano


            Return dt_final

        Catch ex As Exception

            Return ""

        End Try

    End Function
    'Trata a data para o seguinte formato dd/mm/aaaa 
    Public Shared Function trataDataDB(ByVal data As String) As String

        Try

            Dim dt_arr() As String = data.Split("/")
            Dim dt_final As String

            'Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim dia As String = dt_arr(0).PadLeft(2, "0")
            'Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim mes As String = dt_arr(1).PadLeft(2, "0")
            'Dim ano As String = CType(data, DateTime).Year
            Dim ano As String = dt_arr(2)

            'Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            'Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = ano + mes + dia


            Return dt_final

        Catch ex As Exception

            Return ""

        End Try

    End Function

    Public Shared Function getLimMinCapital() As Double
        Dim limMin As Double = 0

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        Try
            Dim dr As Data.SqlClient.SqlDataReader

            bd.SEGS5744_SPS(Web.HttpContext.Current.Session("apolice"), Web.HttpContext.Current.Session("ramo"), Web.HttpContext.Current.Session("subgrupo_id"))

            dr = bd.ExecutaSQL_DR()

            Dim valor_capital_global As String = "0"

            If dr.Read Then
                limMin = IIf(dr.GetValue(5).ToString.Trim = "", "0", CType(dr.GetValue(5).ToString.Trim, Double))
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return limMin

    End Function

    Public Shared Function GetDataEncerramento() As String

        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader

        If Web.HttpContext.Current.Session("ramo") = "" Or Web.HttpContext.Current.Session("apolice") = "" Or CStr(Web.HttpContext.Current.Session("subgrupo_id")) = "" Then
            Return ""
        End If

        BD.SEGS6843_SPS(Web.HttpContext.Current.Session("ramo"), Web.HttpContext.Current.Session("apolice"), Web.HttpContext.Current.Session("subgrupo_id"))

        Try
            dr = BD.ExecutaSQL_DR()

            If dr.Read() Then
                Return "Faturamento encerrado na data e hora " & cUtilitarios.trataDataHora(dr.GetValue(0).ToString) & "."
            Else
                Return "N�o h� data e hora de encerramento de faturamento dispon�veis."
            End If

            dr.Close()
            dr = Nothing


        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return ""


    End Function

    Public Shared Function VerificaAcesso(ByVal cpf As String) As String

        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader

        BD.SEGS6581_SPS(cpf)

        Try
            dr = BD.ExecutaSQL_DR()

            If dr.Read() Then
                Return dr.GetValue(0).ToString
            End If

            dr.Close()
            dr = Nothing

            Return "0"

        Catch ex As Exception
            Dim excp As New clsException(ex)
            Return "1"
        End Try

    End Function

    Public Shared Function addCell(ByVal valor As String) As Web.UI.HtmlControls.HtmlTableCell
        Dim td As New Web.UI.HtmlControls.HtmlTableCell
        td.InnerText = valor
        Return td
    End Function

    Public Shared Function ValidaLabels(ByVal valor_conteudo As String) As String
        '[New code: function created by Me 13.07.07]
        Dim backup As String = ""

        Try
            backup = valor_conteudo

            If valor_conteudo.Trim = "" Then
                valor_conteudo = " --- "
            End If

            If valor_conteudo.ToString = "0" Or valor_conteudo.ToString = "0,00" Or valor_conteudo = "0.00" Then
                valor_conteudo = " --- "
            End If

            Return valor_conteudo

        Catch ex As Exception
            Return backup
            'Dim excp As New clsException(ex)

        End Try

    End Function

    Public Shared Function Formata_Clausula(ByVal stRClau As String) As String
        '* Formata texto para que n�o existam quebras no meio de palavras,
        '* considerando-se 90 chars por linha

        Dim Texto As String = ""
        Dim ult_quebra As Object = 1
        Dim Encontrou As Boolean = False
        Dim FRASE As String = ""
        Dim cont_clau As Object = Nothing
        Dim CONT_FRASE As Integer = 0
        Dim ACHA_ESPACO As Object = Nothing

        '
        For cont_clau = 1 To Len(stRClau)
            CONT_FRASE = CONT_FRASE + 1
            '' Se caracter 'cont' � return, 'frase' recebe peda�o
            '' de ultima quebra at� cont_clau
            If Mid(stRClau, cont_clau, 1) = Chr(13) Then
                FRASE = Mid(stRClau, ult_quebra, cont_clau - ult_quebra) & vbNewLine
                ult_quebra = cont_clau + 2
                'Pula o char de line feed - Chr(10)
                cont_clau = cont_clau + 2
                CONT_FRASE = 0
                '' Se char n�o � return, se frase j� tem 90 chars
                '' isto e, ja chegou ao final da linha de impressao
            ElseIf CONT_FRASE = 90 Then
                Encontrou = False
                '' Se o prox. char � <> de espa�o e return
                If Mid(stRClau, cont_clau + 1, 1) <> " " And Mid(stRClau, cont_clau + 1, 1) <> Chr(13) Then
                    '' Procura o 1� espaco anterior ao char atual dentro da frase
                    For ACHA_ESPACO = cont_clau To ult_quebra Step -1
                        '' Se achou o espaco
                        If Mid(stRClau, ACHA_ESPACO, 1) = " " Then
                            FRASE = Mid(stRClau, ult_quebra, ACHA_ESPACO - ult_quebra) & vbNewLine
                            CONT_FRASE = cont_clau - ACHA_ESPACO
                            ult_quebra = ACHA_ESPACO + 1
                            Encontrou = True
                            Exit For
                        End If
                    Next ACHA_ESPACO
                End If
                '' Se n�o precisou procurar por espaco
                If Not Encontrou Then
                    FRASE = RTrim(Mid(stRClau, ult_quebra, 90)) & vbNewLine
                    CONT_FRASE = 0
                    ult_quebra = ult_quebra + 90
                    '' Se ultimo char restante � espa�o ou return, pula
                    If Mid(stRClau, ult_quebra, 1) = Chr(13) Then
                        ult_quebra = ult_quebra + 2
                        cont_clau = cont_clau + 2
                    ElseIf Mid(stRClau, ult_quebra, 1) = " " Then
                        ult_quebra = ult_quebra + 1
                        cont_clau = cont_clau + 1
                        If Mid(stRClau, ult_quebra, 1) = Chr(13) Then
                            ult_quebra = ult_quebra + 2
                            cont_clau = cont_clau + 2
                        End If
                    End If
                End If 'not encontrou
            End If
            '' Acumula as frases na var. 'texto'
            If FRASE <> "" Then
                Texto = Texto & FRASE
                FRASE = ""
            End If
        Next cont_clau
        '
        If ult_quebra < Len(stRClau) Then
            Texto = Texto & Mid(stRClau, ult_quebra, cont_clau - ult_quebra + 1)
        End If

        Return Texto

    End Function

    Public Shared Function getParametros(ByVal p_sSiglaSistema As String, ByVal p_sSecao As String, ByVal p_sCampo As String, ByVal p_sAmbiente As String) As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.obter_parametros_sps(p_sSiglaSistema, p_sSecao, p_sCampo, p_sAmbiente)

        Try
            Dim v_oDtResultado As Data.DataTable = bd.ExecutaSQL_DT()

            If v_oDtResultado.Rows.Count > 0 Then
                Return v_oDtResultado.Rows(0)("Valor")
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Function

    ' pablo.dias (Nova Consultoria) - 01/08/2011
    ' 9420650 - Melhorias no sistema Vida Web
    ' Fun��o auxiliar para configura��o de hyperlink
    Public Shared Sub SetOnClickEvent(ByVal link As Web.UI.WebControls.HyperLink, ByVal onclick As String)
        link.Attributes.Add("onclick", onclick)
        link.NavigateUrl = "#"
        link.Target = "_self"
    End Sub
End Class

