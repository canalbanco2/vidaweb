
Public Class cAcompanhamento

#Region "Heran�a"
    Inherits cBanco
#End Region

    Public Sub SEGS7471_SPS(ByVal p_iApoliceId As Integer, _
                            ByVal p_iRamoId As Integer, _
                            ByVal p_iSubGrupo As Integer, _
                            ByVal p_sIndSituacao As String, _
                            Optional ByVal p_iIndAcesso As Integer = Nothing, _
                            Optional ByVal p_sCPF As String = Nothing)

        SQL = "exec vida_web_db..SEGS7471_SPS"
        SQL &= " @apolice_id = " & p_iApoliceId
        SQL &= ", @ramo_id = " & p_iRamoId
        If Not IsNothing(p_sCPF) Then SQL &= ", @CPF = " & p_sCPF
        SQL &= ", @ind_situacao = '" & p_sIndSituacao & "'"
        If Not p_iIndAcesso.Equals(0) Then SQL &= ", @ind_acesso = " & p_iIndAcesso

    End Sub

    Public Sub SEGS7561_SPS(ByVal p_iApoliceId As Integer, _
                        ByVal p_iRamoId As Integer, _
                        ByVal p_iSubGrupo As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS7561_SPS "
        SQL &= " @apolice_id = " & p_iApoliceId
        SQL &= ", @ramo_id = " & p_iRamoId
        SQL &= ", @subgrupo_id = " & p_iSubGrupo

    End Sub

    Public Sub SEGS7599_SPS(Optional ByVal p_iTipoDocVidaWebId As Integer = -1)
        SQL = "exec VIDA_WEB_DB..SEGS7599_SPS "
        If p_iTipoDocVidaWebId <> -1 Then SQL &= " @tipo_doc_vida_web_id = " & p_iTipoDocVidaWebId
    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String, ByVal id As String, ByVal tipo As String)

        SQL = "exec VIDA_WEB_DB..SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        'SQL &= ", @cpf = " & cpf
        SQL &= ", @cpf = '" & cpf & "' "
        SQL &= ", @nome = " & nome
        SQL &= ", @id = " & id
        SQL &= ", @tipo = " & tipo

    End Sub

    'Consulta as condi��es do subgrupo (valor lim�te m�ximo)
    Public Sub SEGS5744_SPS(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As String)
        SQL = "exec VIDA_WEB_DB..SEGS5744_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @cod_susep = 6785"
        SQL &= ", @seguradora_id = 0"
        SQL &= ", @subgrupo_id = " & subgrupo_id
        SQL &= ", @usuario = ''"
        SQL &= ", @ind_acesso = ''"
        SQL &= ", @cpf_id = ''"
    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String)

        SQL = "exec VIDA_WEB_DB..SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        'SQL &= ", @cpf = " & cpf
        SQL &= ", @cpf = '" & cpf & "' "
        SQL &= ", @nome = " & nome

    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String, ByVal id As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        'SQL &= ", @cpf = " & cpf
        SQL &= ", @cpf = '" & cpf & "' "
        SQL &= ", @nome = " & nome
        SQL &= ", @id = " & id
    End Sub

    Public Sub SEGS6581_SPS(ByVal cpf As String, ByVal estipulante As String)
        SQL = "exec VIDA_WEB_DB..SEGS6581_SPS "
        SQL &= "  @estipulante = '" & estipulante
        SQL &= "', @cpf = '" & cpf & "'"
    End Sub

    Public Sub SEGS6581_SPS(ByVal cpf As String)
        SQL = "exec VIDA_WEB_DB..SEGS6581_SPS @verificaAcesso = 1, @cpf = '" & cpf & "' "
    End Sub

    Public Sub SEGS6581_SPS(ByVal cpf As String, ByVal estipulante As String, ByVal apolice_id As String, ByVal ramo_id As String, ByVal cnpj As String)
        SQL = "exec VIDA_WEB_DB..SEGS6581_SPS "
        SQL &= "  @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @cpf = '" & cpf & "' "
        SQL &= ", @estipulante = " & estipulante
        If UCase(Trim(cnpj)) <> "NULL" Then
            SQL &= ", @cnpj = '" & cnpj & "' "
        End If
    End Sub

    Public Sub SEGS5653_SPI(ByVal wf_id As Integer)
        SQL = "exec [sisab003].seguros_db.dbo.SEGS5653_SPI @wf_id = " & wf_id
    End Sub


    Public Sub SEGS5393_SPI(ByVal wf_id As Integer, ByVal tp_wf_id As Integer, ByVal tp_tarefa_id As Integer, ByVal tp_ativ_id As Integer)

        SQL = "exec [sisab003].seguros_db.dbo.SEGS5393_SPI "
        SQL &= " @wf_id = " & wf_id
        SQL &= ", @tp_wf_id = " & tp_wf_id
        SQL &= ", @tp_tarefa_id = " & tp_tarefa_id
        SQL &= ", @tp_ativ_id = " & tp_ativ_id

    End Sub

    Public Sub SEGS5699_SPI(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal wf_id As String, ByVal dt_inicio_competencia As String, ByVal dt_fim_competencia As String, ByVal usuario As String)

        SQL = "exec VIDA_WEB_DB..SEGS5699_SPI "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @seguradora_cod_susep = 6785"
        SQL &= ", @sucursal_seguradora_id = 0"
        SQL &= ", @wf_id = " & wf_id
        SQL &= ", @dt_inicio_competencia = '" & dt_inicio_competencia & "'"
        SQL &= ", @dt_fim_competencia = '" & dt_fim_competencia & "'"
        SQL &= ", @desc_historico = 'Realizado o Encerramento da Fatura'"
        SQL &= ", @aplicacao = 'SEGW0069'"
        SQL &= ", @usuario = " & usuario

    End Sub

    Public Sub executar_atividade_lote_spu()
        SQL = " exec [sisab003].workflow_db.dbo.executar_atividade_lote_spu "
    End Sub

    'Consulta as condi��es do subgrupo
    Public Sub SEGS5705_SPS(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5705_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub

    'Consulta as condi��es do subgrupo
    Public Sub SEGS5706_SPS(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5706_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub

    'Consulta os dados b�sicos da ap�lice
    Public Sub dados_basico_apolice_sps(ByVal apolice_id As Long, ByVal ramo_id As Int16, ByVal cod_susep As Int16, ByVal seguradora_id As Int16, ByVal cpf As String)
        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5660_SPS "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @seguradora_cod_susep=" & trInt(cod_susep)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @sucursal_seguradora_id=" & seguradora_id
        SQL &= ", @cpf='" & cpf & "'"

    End Sub

    Public Sub SEGS6929_SPS(ByVal cod_susep As Int16, ByVal seguradora_id As Int16, ByVal cpf As String, ByVal data As String, Optional ByVal somenteCNPJ As Int16 = 0)
        SQL = "set nocount on exec VIDA_WEB_DB..SEGS6929_SPS "
        SQL &= " @seguradora_cod_susep=" & trInt(cod_susep)
        SQL &= ", @sucursal_seguradora_id=" & seguradora_id
        SQL &= ", @cpf='" & cpf & "'"
        SQL &= ", @data = '" & data & "%'"
        SQL &= ", @somenteCNPJ = " & somenteCNPJ

    End Sub


    'Consulta os log dos �ltimos 10 acessos
    Public Sub segs5644_sps(ByVal usuario As String)

        SQL = "exec VIDA_WEB_DB..segs5644_sps '" & usuario & "'"

    End Sub

    Public Sub SGSS0782_SPI(ByVal sigla_recurso As String, ByVal sigla_sistema As String, ByVal chave As String, ByVal de As String, ByVal para As String, ByVal assunto As String, ByVal mensagem As String, ByVal cc As String, ByVal anexo As String, ByVal formato As String, ByVal id As String)

        SQL = " exec [sisab003].email_db.dbo.SGSS0782_SPI "
        SQL &= " @sigla_recurso ='" & sigla_recurso
        SQL &= "', @sigla_sistema ='" & sigla_sistema
        SQL &= "', @chave ='" & chave
        SQL &= "', @de ='" & de
        SQL &= "', @para ='" & para
        SQL &= "', @assunto ='" & assunto
        SQL &= "', @mensagem ='" & mensagem
        SQL &= "', @cc ='" & cc
        SQL &= "', @anexo ='" & anexo
        SQL &= "', @formato ='" & formato
        SQL &= "', @usuario ='" & id & "'"

    End Sub

    ' pablo.dias (Nova Consultoria) - 01/08/2011
    ' 9420650 - Melhorias no sistema Vida Web
    ' Consultar faturas pendentes da ap�lice
    Public Sub SEGS9678_SPS(ByVal apolice As Long, ByVal ramo As Long)
        SQL = " EXEC vida_web_db..SEGS9678_SPS "
        SQL &= "     @ramo_id =" & ramo
        SQL &= "   , @apolice_id =" & apolice

    End Sub

    ' breno.nery (Nova Consultoria) - 01/03/2012
    ' 12784329 - Cria��o de Painel de Monitoramento Vida Web 
    ' Retornar permiss�es de acesso ao painel de monitoramento
    Public Sub SEGS10093_SPS(ByVal login As String)
        SQL = " EXEC vida_web_db..SEGS10093_SPS "
        SQL &= "     @login =" & login
    End Sub

    'Busca o Resumo da Fatura atual (anterior) da tela de abertura do Vida
    Public Sub ResumoFaturaAtual_Anterior(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal usuario As String, ByVal wf_id As Int64)

        'Busca Resumo da Fatura Atual Anterior
        SQL = "exec VIDA_WEB_DB..SEGS5732_SPS "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @subgrupo_id=" & subgrupo_id
        SQL &= ", @usuario=" & trStr(usuario)
        SQL &= ", @wf_id = " & wf_id

    End Sub

    'Busca o Resumo da Fatura atual (Inclusoes, Altera��es e Exclusoes) da tela de abertura do Vida
    Public Sub ResumoFaturaAtual_IAE(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal usuario As String, ByVal wf_id As Int64)

        'Busca Resumo da Fatura Atual: IAE
        SQL = "exec VIDA_WEB_DB..SEGS5733_SPS "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @subgrupo_id=" & subgrupo_id
        SQL &= ", @usuario=" & trStr(usuario)
        SQL &= ", @wf_id = " & wf_id

    End Sub

    'Insere um novo registro no log de acesso
    Public Sub TEMPinsereUltimoAcesso(ByVal apolice_id As Long, ByVal subgrupo_id As Integer, ByVal ramo_id As Integer, ByVal usuario As String)

        SQL = "exec VIDA_WEB_DB..SEGS5665_SPI "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @subgrupo_id=" & trInt(subgrupo_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @usuario=" & trStr(usuario)
        SQL &= ", @seguradora_cod_susep=6785"
        SQL &= ", @sucursal_seguradora_id=0"

    End Sub

    'Consulta as ap�lices e ramos dispon�veis para o usu�rio logado
    Public Sub consultar_apolice_ramos_sps(ByVal id_usuario As String)

        SQL = "exec VIDA_WEB_DB..segs5659_sps "
        SQL &= "@cpf='" & id_usuario & "'"

    End Sub

    Public Sub consultar_apolice_ramos_sps(ByVal id_usuario As String, ByVal acesso As Int16)

        SQL = "exec VIDA_WEB_DB..segs5659_sps "
        SQL &= "@cpf='" & id_usuario & "'"
        'SQL &= ",@ind_acesso=" & acesso

    End Sub

    'Consulta os subgrupos da ap�lice de acordo com um CPF
    Public Sub consulta_subgrupo_sps(ByVal apolice_id As Long, ByVal ramo_id As Int16, ByVal cpf As String)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5661_SPS "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @cpf='" & cpf & "'"

    End Sub

    'Consulta os subgrupos da ap�lice de acordo com um CPF
    Public Sub consulta_subgrupo_sps(ByVal apolice_id As Long, ByVal ramo_id As Int16, ByVal cpf As String, ByVal ind_acesso As Int16)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5661_SPS "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @cpf='" & cpf & "'"
        SQL &= ",@acesso = " & ind_acesso
    End Sub

    'Consulta todos os subgrupos da ap�lice
    Public Sub consulta_subgrupo_sps(ByVal apolice_id As Long, ByVal ramo_id As Int16)

        SQL = "set nocount on exec web_intranet_db..SEGS5645_SPS "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
    End Sub


    'Consulta os usu�rio v�lidos para uma ap�lice-ramo-subgrupo
    Public Sub consultar_usuarios_SPS(ByVal apolice_id As Long, ByVal ramo_id As Int16, ByVal subgrupo_id As Int16)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5662_SPS "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @subgrupo_id=" & trInt(subgrupo_id)
        SQL &= ", @seguradora_id=0"
        SQL &= ", @cod_susep=6785"

    End Sub

    'Consulta os usu�rio v�lidos para uma ap�lice-ramo-subgrupo
    Public Sub consultar_usuarios_web_seguros_SPS()

        SQL = "exec segs5712_sps "

    End Sub

    Public Sub consultar_usuarios_web_seguros_SPS(ByVal apolice_id As String, ByVal ramo_id As String)

        SQL = "exec segs5712_sps "
        SQL &= " @apolice = " & apolice_id
        SQL &= ", @ramo = " & ramo_id

    End Sub

    'Consulta os dados de um usu�rio
    Public Sub consultar_usuarios_SPS(ByVal apolice_id As Long, ByVal ramo_id As Int16, ByVal subgrupo_id As Int16, ByVal cpf As String)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5662_SPS "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @subgrupo_id=" & trInt(subgrupo_id)
        SQL &= ", @seguradora_id=0"
        SQL &= ", @cod_susep=6785"
        SQL &= ", @cpf='" & cpf & "'"

    End Sub

    'Realiza a atualiza��o dos dados do usu�rio de uma ap�lice, ramo e subgrupo
    Public Sub atualizarUsuario(ByVal apolice_id As Long, ByVal ramo_id As Int16, ByVal subgrupo_id As Int16 _
                                , ByVal cpf As String, ByVal nome As String, ByVal login As String _
                                , ByVal acesso As String, ByVal situacao As String, ByVal email As String)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5674_SPU "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @subgrupo_id=" & trInt(subgrupo_id)
        SQL &= ", @seguradora_id=0"
        SQL &= ", @cod_susep=6785"
        SQL &= ", @cpf='" & cpf & "'"
        SQL &= ", @nome=" & trStr(nome)
        SQL &= ", @login=" & login
        SQL &= ", @ind_acesso=" & acesso
        SQL &= ", @ind_situacao=" & situacao
        SQL &= ", @email=" & trStr(email)
        SQL &= ", @usuario=" & Web.HttpContext.Current.Session("usuario")

    End Sub

    'Realiza a exclus�o l�gica do usu�rio
    Public Sub excluiUsuario_spd(ByVal apolice_id As Long, ByVal ramo_id As Int16, ByVal subgrupo_id As Int16, ByVal cpf As String)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5673_SPD "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @subgrupo_id=" & trInt(subgrupo_id)
        SQL &= ", @seguradora_id=0"
        SQL &= ", @cod_susep=6785"
        SQL &= ", @cpf='" & cpf & "'"
        SQL &= ", @usuario=" & Web.HttpContext.Current.Session("usuario")

    End Sub

    'Verifica se o usu�rio j� existe atrav�s de seu CPF
    Public Sub verificaUsuario(ByVal cpf As String)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5663_SPS "
        SQL &= "@cpf='" & cpf & "'"

    End Sub

    'Realiza a inclus�o de um novo usu�rio
    Public Sub incluiUsuario(ByVal apolice_id As Long, ByVal ramo_id As Int16, ByVal subgrupo_id As Int16 _
                                , ByVal cpf As String, ByVal nome As String, ByVal login As String _
                                , ByVal acesso As String, ByVal situacao As String, ByVal email As String)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5666_SPI "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @subgrupo_id=" & trInt(subgrupo_id)
        SQL &= ", @seguradora_id=0"
        SQL &= ", @cod_susep=6785"
        SQL &= ", @cpf='" & cpf & "'"
        SQL &= ", @nome=" & trStr(nome)
        SQL &= ", @login=" & trStr(login)
        SQL &= ", @ind_acesso=" & acesso
        SQL &= ", @ind_situacao=" & situacao
        SQL &= ", @email=" & trStr(email)
        SQL &= ", @usuario=" & Web.HttpContext.Current.Session("usuario")

    End Sub


    'Consulta o prazo restante
    Public Sub prazo_limite(ByVal apolice_id As Long, ByVal ramo_id As Int16, ByVal subgrupo_id As Int16, ByVal wf_id As Integer)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5722_SPS "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @sub_grupo_id=" & trInt(subgrupo_id)
        SQL &= ", @tp_wf_id=" & cConstantes.CWORKFLOW
        SQL &= ", @tp_versao_id=" & cConstantes.CVERSAOID
        SQL &= ", @wf_id=" & wf_id

    End Sub

    'Consulta o prazo restante
    Public Sub dia_venc(ByVal apolice_id As Long, ByVal ramo_id As Int16, ByVal subgrupo_id As Int16, ByVal wf_id As Integer)

        SQL = "SELECT * FROM [sisab003].seguros_db.dbo.sub_grupo_apolice_tb WHERE "
        SQL = SQL & " ramo_id = " & trInt(ramo_id)
        SQL = SQL & " and apolice_id = " & trInt(apolice_id)
        SQL = SQL & " and sub_grupo_id = " & trInt(subgrupo_id)

    End Sub

    Public Sub New(Optional ByVal banco As cDadosBasicos.eBanco = cDadosBasicos.eBanco.web_intranet_db)
        MyBase.New(banco)
        Alianca.Seguranca.BancoDados.cCon.BancodeDados = banco.ToString
    End Sub

    Function trInt(ByVal valor As Object) As String

        If valor = 0 Then
            Return "null"
        Else
            Return valor
        End If

    End Function

    Function trStr(ByVal valor As Object) As String

        If valor = "" Then
            Return "null"
        Else
            Return "'" & valor.ToString.Replace("'", "''") & "'"
        End If

    End Function

    Public Sub Recalcula_Resumo_Operacoes(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal usuario As String)
        SQL = "exec VIDA_WEB_DB..SEGS7048_SPI "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @usuario = '" & usuario & "'"
    End Sub

    Public Sub VerificaMovimentacaoEncerrada(ByVal apolice_id As Long, ByVal ramo_id As Int16, ByVal subgrupo_id As Int16)

        SQL = "SELECT   SITUACAO "
        SQL = SQL & " FROM   [sisab003].seguros_db.dbo.CONTROLE_FATURAMENTO_TB A"
        SQL = SQL & " WHERE A.APOLICE_ID = " & trInt(apolice_id)
        SQL = SQL & " AND    A.RAMO_ID = " & trInt(ramo_id)
        SQL = SQL & " AND    A.SUB_GRUPO_ID = " & trInt(subgrupo_id)
        SQL = SQL & " AND    A.SEQ_FATURAMENTO = (SELECT MAX(SEQ_FATURAMENTO) FROM [sisab003].seguros_db.dbo.CONTROLE_FATURAMENTO_TB B"
        SQL = SQL & "                    WHERE       B.APOLICE_ID = A.APOLICE_ID"
        SQL = SQL & "                    AND   B.RAMO_ID = A.RAMO_ID"
        SQL = SQL & "                    AND   B.SUB_GRUPO_ID = A.SUB_GRUPO_ID)"

    End Sub

    Public Sub SEGS5696_SPS(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal workflow As Integer, ByVal valor_id As Integer)
        SQL = " exec VIDA_WEB_DB..segs5696_sps @apolice_id = " & apolice_id.ToString
        SQL &= ", @subgrupo_id = " & subgrupo.ToString
        SQL &= ", @ramo_id = " & ramo_id.ToString
        SQL &= ", @tp_wf_id = " & workflow.ToString
        SQL &= ", @tp_versao_id = " & valor_id.ToString
    End Sub

    Public Sub SEGS5722_SPS(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal workflow As Integer, ByVal valor_id As Integer, ByVal wf_id As Long)
        SQL = "exec segs5722_sps  @apolice_id = " & apolice_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @tp_wf_id = " & workflow
        SQL &= ", @tp_versao_id = " & valor_id
        SQL &= ", @wf_id = " & wf_id
    End Sub

    Public Sub delete_usuario_apolice_spu(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal usuario As String)

        SQL = " exec VIDA_WEB_DB..SEGS5715_SPU @apolice_id = " & apolice_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @cpf = '" & cpf & "'"
        SQL &= ", @usuario = " & usuario
        SQL &= ", @ind_situacao = I"

    End Sub

    Public Sub insere_usuario_apolice_spi(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal usuario As String)

        SQL = " exec VIDA_WEB_DB..SEGS5713_SPI @apolice_id = " & apolice_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @cpf = '" & cpf & "'"
        SQL &= ", @usuario = " & usuario

    End Sub

    Public Sub getHistorico_sps(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal usuario As String)

        SQL = " exec VIDA_WEB_DB..segs5711_sps @apolice_id = " & apolice_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @usuario = " & usuario

    End Sub

    Public Sub getEstadoAtividade(ByVal wf_id As String, ByVal tp_ativ As String)

        SQL = " exec VIDA_WEB_DB..SEGS5771_SPS "
        SQL &= "@wf_id = " & wf_id
        SQL &= ", @tp_ativ_id = " & tp_ativ

    End Sub

    Public Sub GetDadosSubgrupoE1(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5751_SPS "
        SQL &= "  @apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @subgrupo_id=" & trInt(subgrupo_id)


    End Sub
    'M�todo para retornar todos os tipos de Sinistros: Abertos - A, Pendentes - P, Pagos - G e Todos - T
    Public Sub GetSinistros(ByVal apolice_id As Long, ByVal ramo_id As Long, ByVal sub_grupo_id As Long, ByVal status As String)
        SQL = "set nocount on exec VIDA_WEB_DB..SEGS11906_SPS"
        SQL &= " @apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @sub_grupo_id=" & trInt(sub_grupo_id)
        SQL &= ", @status='" & Trim(status) & "'"

    End Sub
    Public Sub GetCapitalMaximo(ByVal apolice_id As Long, ByVal subgrupo_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal usuario As String, ByVal ind_acesso As Integer, ByVal cpf_id As String)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5744_SPS "
        SQL &= "  @apolice_id=" & trInt(apolice_id)
        SQL &= ", @subgrupo_id=" & trInt(subgrupo_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @cod_susep=" & cod_susep
        SQL &= ", @seguradora_id=" & seguradora_id
        SQL &= ", @usuario=" & usuario
        SQL &= ", @ind_acesso=" & ind_acesso
        SQL &= ", @cpf_id='" & cpf_id & "'"

    End Sub

    'Consulta as condi��es do subgrupo
    Public Sub GetDadosSubgrupoE2(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5705_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub

    'Consulta as condi��es do subgrupo
    Public Sub GetDadosSubGrupoE3(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5706_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub


    'Consulta as condi��es do subgrupo SEGS5750_SPS
    Public Sub coberturas(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer, ByVal componente As String)

        SQL = "exec VIDA_WEB_DB..SEGS5750_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @componente = " & componente
        SQL &= ", @ind_acesso = 0"
        SQL &= ", @cpf_id = '0'"
        SQL &= ", @usuario = 0"
        SQL &= ", @seguradora_cod_susep = 6785"
        SQL &= ", @sucursal_seguradora_id = 0 "

    End Sub

    Public Sub Clausulas(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer, Optional ByVal nSeqClausula As Integer = 0)
        Dim dt_inicio_query As String

        dt_inicio_query = Format(Date.Now.Year, "0000") & Format(Date.Now.Month, "00") & Format(Date.Now.Day, "00")

        SQL = "SELECT cp.seq_clausula, c.descr_clausula, cp.texto_clausula "
        SQL &= "FROM [sisab003].seguros_db.dbo.clausula_personalizada_tb cp with (nolock), [sisab003].seguros_db.dbo.clausula_tb c with (nolock), [sisab003].seguros_db.dbo.apolice_tb a with (nolock) "
        SQL &= "WHERE cp.dt_inicio_vigencia <= '" & dt_inicio_query & "'"
        SQL &= " AND (cp.dt_fim_vigencia is null"
        SQL &= "      OR cp.dt_fim_vigencia >= '" & dt_inicio_query & "')"
        SQL &= " AND c.cod_clausula = cp.cod_clausula_original "
        SQL &= " AND cp.proposta_id = a.proposta_id "
        SQL &= " AND a.apolice_id = " & apolice_id
        SQL &= " AND a.ramo_id = " & ramo_id
        If nSeqClausula <> 0 Then
            SQL &= " AND cp.seq_clausula = " & nSeqClausula
        End If
        SQL &= " AND a.seguradora_cod_susep = 6785"
        SQL &= " AND a.sucursal_seguradora_id = 0 "
        SQL &= " AND c.dt_inicio_vigencia = cp.dt_inicio_vigencia_cl_original"
        SQL &= " ORDER BY seq_clausula"

    End Sub

    'Consulta as cr�ticas existentes para a ap�lice.
    Public Sub criticas(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer, ByVal dt_ini_fatura As String)

        SQL = "exec [sisab003].seguros_db.dbo.sel_crit_cartao_sub_grupo_sps "
        SQL &= " @apolice = " & apolice_id
        SQL &= ", @ramo = " & ramo_id
        SQL &= ", @subgrupo = " & subgrupo
        SQL &= ", @dt_ini_fat = '" & cUtilitarios.trataDataDB(dt_ini_fatura) & "'"
        SQL &= ", @seguradora = 6785"
        SQL &= ", @sucursal = 0 "

    End Sub

    Public Sub BuscaLimMin(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As String)
        SQL = "exec VIDA_WEB_DB..SEGS5744_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @cod_susep = 6785"
        SQL &= ", @seguradora_id = 0"
        SQL &= ", @subgrupo_id = " & subgrupo_id
        SQL &= ", @usuario = ''"
        SQL &= ", @ind_acesso = ''"
        SQL &= ", @cpf_id = ''"
    End Sub

    Public Sub VerificaMovimentacao(ByVal apolice_id As Int32, ByVal ramo_id As Int16, ByVal subgrupo_id As Int16, Optional ByVal tipo As Integer = 0)
        'SQL = "exec VIDA_WEB_DB..SEGS6670_SPS "
        SQL = "exec VIDA_WEB_DB..SEGS6670_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @subgrupo_id = " & subgrupo_id
        SQL &= ", @tipo = " & tipo
    End Sub

    Public Sub SEGS6843_SPS(ByVal ramo_id As Integer, ByVal apolice_id As Long, ByVal subgrupo As Int16)
        SQL = "exec VIDA_WEB_DB..segs6843_sps  @apolice_id = " & apolice_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @ramo_id = " & ramo_id
    End Sub

    Public Sub getLayoutArquivo(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)
        SQL = "exec VIDA_WEB_DB..SEGS5763_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id  = " & ramo_id
        SQL &= ", @subgrupo_id = " & subgrupo_id
    End Sub

    Public Sub SEGS7705_SPS(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)
        'SQL = "exec VIDA_WEB_DB..SEGS7705_SPS "
        SQL = "exec [sisab003].seguros_db.dbo.SEGS7705_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id  = " & ramo_id
        SQL &= ", @sub_grupo_id = " & subgrupo_id
        SQL &= ", @seguradora_cod_susep = 6785"
        SQL &= ", @sucursal_seguradora_id = 0"
        SQL &= ", @tipo = 1"
    End Sub

    'Matheus Souza
    '21/06/2010
    'Consulta a data de in�cio da vigencia.
    Public Sub getDataInicioVigencia(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)
        SQL = " Select dt_inicio_vigencia_sbg "
        SQL &= " from [sisab003].seguros_db.dbo.sub_grupo_apolice_tb "
        SQL &= " where apolice_id = " & apolice_id
        SQL &= " and ramo_id = " & ramo_id
        SQL &= " and sucursal_seguradora_id = 0 "
        SQL &= " and seguradora_cod_susep = 6785 "
        SQL &= " and sub_grupo_id = " & subgrupo_id
        SQL &= " and dt_fim_vigencia_sbg is null  "
    End Sub

    Public Sub obter_parametros_sps(ByVal p_sSiglaSistema As String, ByVal p_sSecao As String, ByVal p_sCampo As String, ByVal p_sAmbiente As String)
        SQL = "exec controle_sistema_db..obter_parametros_sps @SIGLA_SISTEMA = '" & p_sSiglaSistema & "'"
        SQL &= ", @SECAO = '" & p_sSecao & "'"
        SQL &= ", @CAMPO = '" & p_sCampo & "'"
        SQL &= ", @AMBIENTE = '" & p_sAmbiente & "'"
    End Sub

    Public Sub getAliquotaIOF(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)
        SQL = "exec VIDA_WEB_DB..SEGS7717_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id  = " & ramo_id
        SQL &= ", @subgrupo_id = " & subgrupo_id
    End Sub

    'Autor: breno.nery (Nova Consultoria) - Data da Altera��o: 12/01/2012
    'Demanda: 12784293 - Item: 17 - Alterar a mensagem de bloqueio de faturamento.
    Public Sub ConsultarAgencia(ByVal ApoliceId As String, ByVal RamoId As String)
        SQL = "SELECT agencia_tb.nome "
        SQL &= " FROM [sisab003].seguros_db.dbo.apolice_tb AS apolice_tb WITH (NOLOCK)"
        SQL &= " LEFT JOIN [sisab003].seguros_db.dbo.proposta_fechada_tb AS proposta_fechada_tb WITH (NOLOCK)"
        SQL &= "   ON apolice_tb.proposta_id = proposta_fechada_tb.proposta_id "
        SQL &= " LEFT JOIN [sisab003].seguros_db.dbo.proposta_adesao_tb AS proposta_adesao_tb WITH (NOLOCK)"
        SQL &= "   ON apolice_tb.proposta_id = proposta_adesao_tb.proposta_id "
        SQL &= "INNER JOIN [sisab003].seguros_db.dbo.agencia_tb AS agencia_tb WITH (NOLOCK)"
        SQL &= "   ON agencia_tb.agencia_id = ISNULL(proposta_adesao_tb.cont_agencia_id, proposta_fechada_tb.cont_agencia_id) "
        SQL &= "  AND agencia_tb.banco_id = ISNULL(proposta_adesao_tb.cont_banco_id, proposta_fechada_tb.cont_banco_id) "
        SQL &= "WHERE apolice_tb.apolice_id = " & ApoliceId & " "
        SQL &= "  AND apolice_tb.ramo_id = " & RamoId & " "
    End Sub
    ''Autor: Sergio.Pires (Nova Consultoria) - Data da Altera��o: 12/01/2012
    ''Demanda: 17893306 - Upload de DPS.Valida��o de acesso tecnico
    'Public Sub ConsultarNivelTecnico(ByVal ApoliceId As String, ByVal RamoId As String, ByVal cpf As String)
    '    SQL = "EXEC VIDA_WEB_DB..SEGS7471_SPS "
    '    SQL &= "@apolice_id= " & ApoliceId & ", "
    '    SQL &= "@ramo_id = " & RamoId & ", "
    '    SQL &= "@cpf = '" & cpf & "', "
    '    SQL &= "@ind_acesso = 4, "
    '    SQL &= "@ind_situacao='A' "
    'End Sub

    ''Autor: Victor da Fonseca (Nova Tend�ncia) - Data da Altera��o: 24/06/2019
    ''Demanda: C00157271 - Controle de Libera��es e/ou Recusas de DPS
    Public Sub BuscarHistoricoDps(ByVal RamoId As String, ByVal ApoliceId As String, Optional ByVal subgrupo_id As String = Nothing, Optional ByVal cpf As String = Nothing, Optional ByVal dt_inicio As String = "", Optional ByVal dt_fim As String = "")
        SQL = "EXEC VIDA_WEB_DB.dbo.SEGS14302_SPS "
        'SQL = "[SISAB003].desenv_db.dbo.SEGS14302_SPS "
        If RamoId = "" Then
            SQL &= "@ramo_id = NULL, "
        Else
            SQL &= "@ramo_id = " & RamoId & ", "
        End If

        If ApoliceId = "" Then
            SQL &= "@apolice_id = NULL, "
        Else
            SQL &= "@apolice_id = " & ApoliceId & ", "
        End If

        If subgrupo_id = "" Then
            SQL &= "@sub_grupo_id = NULL, "
        Else
            SQL &= "@sub_grupo_id = '" & subgrupo_id & "', "
        End If

        If cpf = "" Then
            SQL &= "@cpf = NULL, "
        Else
            SQL &= "@cpf = '" & cpf & "', "
        End If

        If dt_inicio = "" Then
            SQL &= "@dt_recebimento_inicio = NULL, "
        Else
            SQL &= "@dt_recebimento_inicio = '" & dt_inicio & "', "
        End If

        If dt_fim = "" Then
            SQL &= "@dt_recebimento_fim = NULL "
        Else
            SQL &= "@dt_recebimento_fim = '" & dt_fim & "'"
        End If

    End Sub
End Class
