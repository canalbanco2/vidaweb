﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Collections
Imports System.Data
Imports System.Drawing 'Biblioteca de desenhos e cores
Imports System.Text

Partial Public Class RelatorioSinistro
    Inherits System.Web.UI.Page
    Dim sinistrosAbertos As DataTable
    Dim sinistros_pendentes As DataTable
    Dim sinistros_pagos As DataTable
    Dim todos_sinistros As DataTable
    Shared tipoRelatorio As String
    Protected WithEvents allContent As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim scriptManagerExcel As ScriptManager = ScriptManager.GetCurrent(Page)
        Dim scriptManagerPDF As ScriptManager = ScriptManager.GetCurrent(Page)

        scriptManagerExcel.ID = "ScriptManagerExcel"
        scriptManagerExcel.RegisterPostBackControl(LinkGerarExcel)
        scriptManagerPDF.ID = "ScriptManagerPDF"
        scriptManagerPDF.RegisterPostBackControl(LinkImprimir)

    End Sub

    Protected Sub btnAberto_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAberto.Click

        sinistrosAbertos = obterSinistros("A")
        Session("grdSinistros") = sinistrosAbertos

        If sinistrosAbertos.Rows.Count > 0 Then
            Call esconderBotoes("btnAberto")
            Call preencherTableSinistrosAbertos(sinistrosAbertos)
            vlTelaAberta.Value = "aberto"
        Else
            Dim label As New Label

            label.Text = "Não existem sinistros abertos para exibição"
            label.CssClass = "lblMensagemAlerta"

            Call esconderBotoes("btnAberto", False)
            Call removePainel()
            PlaceHolder1.Controls.Add(label)
        End If

    End Sub

    Protected Sub btnPendentes_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPendentes.Click

        sinistros_pendentes = obterSinistros("P")
        Session("grdSinistros") = sinistros_pendentes

        If sinistros_pendentes.Rows.Count > 0 Then
            Call esconderBotoes("btnPendentes")
            Call preencherTableSinistrosPendentes(sinistros_pendentes)
            vlTelaAberta.Value = "pendente"
        Else
            Dim label As New Label

            label.Text = "Não existem sinistros pendentes para exibição"
            label.CssClass = "lblMensagemAlerta"

            Call esconderBotoes("btnPendentes", False)
            Call removePainel()
            PlaceHolder1.Controls.Add(label)
        End If
    End Sub

    Protected Sub btnPagos_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPagos.Click

        sinistros_pagos = obterSinistros("G")
        Session("grdSinistros") = sinistros_pagos

        If sinistros_pagos.Rows.Count > 0 Then
            Call esconderBotoes("btnPagos")
            Call preencherTableSinistrosPagos(sinistros_pagos)
            vlTelaAberta.Value = "pagos"
        Else
            Dim label As New Label

            label.Text = "Não existem sinistros pagos para exibição"
            label.CssClass = "lblMensagemAlerta"

            Call esconderBotoes("btnPagos", False)
            Call removePainel()
            PlaceHolder1.Controls.Add(label)
        End If

    End Sub

    Protected Sub btnTodos_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTodos.Click

        todos_sinistros = obterSinistros("T")
        Session("grdSinistros") = todos_sinistros

        If todos_sinistros.Rows.Count > 0 Then
            Call esconderBotoes("btnTodos")
            Call preencherTableSinistrosTodos(todos_sinistros)
            vlTelaAberta.Value = "todos"
        Else
            Dim label As New Label

            label.Text = "Não existem sinistros para exibição"
            label.CssClass = "lblMensagemAlerta"

            Call esconderBotoes("btnTodos", False)
            Call removePainel()
            PlaceHolder1.Controls.Add(label)
        End If

    End Sub

    Private Function obterSinistros(ByVal tipo As String) As DataTable
        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dtSinistros As New DataTable

        Try
            BD.GetSinistros(CInt(Session("apolice")), CInt(Session("ramo")), CInt(Session("subgrupo_id")), tipo)
            'BD.GetSinistros(31123, 77, 2, tipo)

            dtSinistros = BD.ExecutaSQL_DT
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return dtSinistros

    End Function

    Private Sub preencherTableSinistrosAbertos(ByVal dt As DataTable)

        Call removePainel()

        Call exibirTabelaSinistros(dt)

    End Sub

    Private Sub preencherTableSinistrosPendentes(ByVal dt As DataTable)

        Call removePainel()

        Call exibirTabelaSinistros(dt, True)

    End Sub

    Private Sub preencherTableSinistrosPagos(ByVal dt As DataTable)

        Call removePainel()

        Call exibirTabelaSinistros(dt)

    End Sub

    Private Sub preencherTableSinistrosTodos(ByVal dt As DataTable)

        Call removePainel()

        Call exibirTabelaSinistros(dt)

    End Sub

    Private Sub exibirTabelaSinistros(ByVal dt As DataTable, ByVal optional exibirLink As Boolean = false)

        Dim novoGrid As New Web.UI.WebControls.GridView()

        novoGrid.ID = "grd_Sinstros"

        novoGrid.HeaderStyle.BackColor = Drawing.Color.Navy
        novoGrid.HeaderStyle.HorizontalAlign = Web.UI.WebControls.HorizontalAlign.Center
        novoGrid.HeaderStyle.ForeColor = Drawing.Color.White
        novoGrid.CssClass = "tabela_sinistros"

        If exibirLink Then
            dt.Columns.Add("Digitalização", GetType(String))
        End If

        novoGrid.DataSource = dt
        novoGrid.DataBind()
        novoGrid.AllowPaging = True
        novoGrid.PageSize = 5

        If exibirLink Then
            For Each row As GridViewRow In novoGrid.Rows
                Dim link As New LinkButton
                link.ID = "linkRedirect"
                link.Text = "Up-load de Documentos"

                Dim sinistro_id As String
                Dim cpf As String

                sinistro_id = row.Cells(0).Text.ToString
                cpf = row.Cells(2).Text.ToString

                link.OnClientClick = "JavaScript:return linkRedirect_click('" & sinistro_id & "','" & cpf & "','" & Alianca.Seguranca.BancoDados.cCon.Ambiente & "');"
                row.Cells(row.Cells.Count - 1).Controls.Add(link)
            Next
        End If

        PlaceHolder1.Controls.Add(novoGrid)

    End Sub

    Private Sub esconderBotoes(ByVal btnSelecionado As String, Optional ByVal mostrarLinks As Boolean = True)

        LinkGerarExcel.Visible = mostrarLinks
        LinkImprimir.Visible = mostrarLinks
        LinkVoltar.Visible = True

        Select Case btnSelecionado
            Case "btnAberto" : btnAberto.Visible = True : btnPendentes.Visible = False : btnPagos.Visible = False : btnTodos.Visible = False
            Case "btnPendentes" : btnPendentes.Visible = True : btnAberto.Visible = False : btnPagos.Visible = False : btnTodos.Visible = False
            Case "btnPagos" : btnPagos.Visible = True : btnAberto.Visible = False : btnPendentes.Visible = False : btnTodos.Visible = False
            Case "btnTodos" : btnTodos.Visible = True : btnAberto.Visible = False : btnPendentes.Visible = False : btnPagos.Visible = False
        End Select

    End Sub

    Private Sub removePainel()

        PlaceHolder1.Controls.Clear()

    End Sub

    Protected Sub lnkGerarExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LinkGerarExcel.Click
        tipoRelatorio = vlTelaAberta.Value

        Select Case (vlTelaAberta.Value)
            Case "aberto"
                sinistrosAbertos = Session("grdSinistros")
                Gera_Arquivo_Excel(sinistrosAbertos, "a")
            Case "pendente"
                sinistros_pendentes = Session("grdSinistros")
                Gera_Arquivo_Excel(sinistros_pendentes, "p")
            Case "pagos"
                sinistros_pagos = New DataTable
                sinistros_pagos = Session("grdSinistros")
                Gera_Arquivo_Excel(sinistros_pagos, "g")
            Case "todos"
                todos_sinistros = Session("grdSinistros")
                Gera_Arquivo_Excel(todos_sinistros, "t")
        End Select
    End Sub

    Protected Sub lnkVoltar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkVoltar.Click
        Response.Redirect("RelatorioSinistro.aspx")
    End Sub

    Protected Sub lnkImprimir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkImprimir.Click
        Select Case (vlTelaAberta.Value)
            Case "aberto"
                sinistrosAbertos = Session("grdSinistros")
                Gera_Arquivo_pdf(sinistrosAbertos, "a")
            Case "pendente"
                sinistros_pendentes = Session("grdSinistros")
                Gera_Arquivo_pdf(sinistros_pendentes, "p")
            Case "pagos"
                sinistros_pagos = Session("grdSinistros")
                Gera_Arquivo_pdf(sinistros_pagos, "g")
            Case "todos"
                todos_sinistros = Session("grdSinistros")
                Gera_Arquivo_pdf(todos_sinistros, "t")
        End Select

    End Sub

    Private Sub Gera_Arquivo_Excel(ByVal dt As DataTable, ByVal tipoSinistro As String)
        Dim grd As New GridView

        grd.DataSource = dt
        grd.DataBind()
        grd.AutoGenerateColumns = False
        Export("sinistros" & tipoSinistro & ".xls", grd)
    End Sub

    Private Sub Gera_Arquivo_pdf(ByVal dt As DataTable, ByVal tipoSinistro As String)
        Dim grd As New GridView

        grd.DataSource = dt
        grd.DataBind()
        grd.AutoGenerateColumns = False
        ExportPDFNew("sinistros" & tipoSinistro & ".pdf", grd)
    End Sub

    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)

        Dim sw As IO.StringWriter = New IO.StringWriter
        sw = HtmlRelatorio(gv, True)

        Dim imagem As String = HttpContext.Current.Request.Url.ToString().Replace("RelatorioSinistro.aspx", "images/bb_mapfre_seguros.gif")

        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        HttpContext.Current.Response.Write("<table rules=""all"" border=""0""><tr></tr></table>" + sw.ToString().Replace("%IMAGEM%", "<img src=""" + imagem + """ />"))
        HttpContext.Current.Response.End()

    End Sub

    Public Sub ExportPDFNew(ByVal fileName As String, ByVal gv As GridView)

        Dim sb As IO.StringWriter = New IO.StringWriter
        sb = HtmlRelatorio(gv)

        Dim caminhoImagem As String = System.IO.Path.Combine(Server.MapPath("~"), "images")
        Dim htmlImagem As String = "<table rules=""all"" border=""0""><tr><td><img src=""" + caminhoImagem + "\bb_mapfre_seguros.gif""/></td></tr><tr></tr></table>"

        Session.Add("pdf", htmlImagem + sb.ToString())
        Response.Redirect("frmDownload.aspx")

    End Sub

    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))

            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

    Private Shared Function LinhaEmBranco(ByRef numeroCelulas As Integer) As TableRow

        Dim linha As New TableRow

        For index As Integer = 1 To numeroCelulas
            linha.Cells.Add(New TableCell())
        Next

        Return linha

    End Function


    Private Shared Function HtmlRelatorio(ByVal gv As GridView, Optional ByVal imagemCelula As Boolean = False) As IO.StringWriter

        Dim numeroColunas As Integer
        Select Case (tipoRelatorio)
            Case "aberto", "pagos"
                numeroColunas = 4
            Case Else
                numeroColunas = 5
        End Select

        Dim table As Table = New Table
        table.GridLines = gv.GridLines

        If imagemCelula Then

            Dim linhaCabecalho As TableRow = LinhaEmBranco(1)
            linhaCabecalho.Cells(0).Text = "%IMAGEM%"

            table.Rows.Add(LinhaEmBranco(0))
            table.Rows.Add(linhaCabecalho)
            table.Rows.Add(LinhaEmBranco(0))
            table.Rows.Add(LinhaEmBranco(0))

        End If

        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If

        Dim totalEstimativa As Double

        For Each row As GridViewRow In gv.Rows

            PrepareControlForExport(row)
            table.Rows.Add(row)

            Dim valorParse As Double
            If Double.TryParse(row.Cells(numeroColunas - 1).Text, valorParse) Then
                totalEstimativa += valorParse
            End If

        Next

        Dim r As TableRow
        r = LinhaEmBranco(numeroColunas)
        r.Cells(numeroColunas - 2).Text = "Total:"
        r.Cells(numeroColunas - 1).Text = totalEstimativa.ToString()

        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If

        table.Rows.Add(r)

        Dim sw As IO.StringWriter = New IO.StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)

        table.RenderControl(htw)

        Return sw

    End Function

End Class