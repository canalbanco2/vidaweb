<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Ajuda.aspx.vb" Inherits="segw0060.Ajuda"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Ajuda</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<div id="cabecAjuda"><IMG src="Images/ajuda-1.gif">
			</div>
			<P>&nbsp;</P>
			<P class="ajuda"><asp:label id="Label2" runat="server" Font-Bold="True" Font-Underline="True">Administra��o</asp:label>&nbsp;- 
				Todas as op��es desta �rea s�o de acesso exclusivo do administrador da ap�lice.<BR>
				<TABLE id="Table1" height="61" cellSpacing="0" cellPadding="0" width="94%" align="center"
					border="0">
					<TR>
						<TD><asp:label id="Label4" runat="server" CssClass="ajudaTituloItem">- Manuten��o de usu�rios</asp:label>
							<span class="ajudaItem">- Op��o de inser��o/habilita��o de usu�rios do sistema.</span>
						</TD>
					</TR>
					<TR>
						<TD><asp:label id="Label5" runat="server" CssClass="ajudaTituloItem">- Definir Usu�rio Substituto</asp:label>
							<span class="ajudaItem">- Op��o para que o administrador conceda acesso aos subgrupos&nbsp;da ap�lice e 
							aos usu�rios do sistema, permitindo definir inclusive aquele que ser� o administrador do subgrupo.</span>
						</TD>
					</TR>
				</TABLE>
				<asp:label id="Label3" runat="server" Font-Bold="True" Font-Underline="True">Fatura Atual</asp:label>&nbsp; 
				- Refere-se a movimenta��o de vidas e consultas ao subgrupo selecionado.<BR>
				<TABLE id="Table2" height="61" cellSpacing="0" cellPadding="0" width="94%" align="center"
					border="0">
					<TR>
						<TD><asp:label id="Label7" runat="server" CssClass="ajudaTituloItem">- Movimenta��o on-line</asp:label>
							<span class="ajudaItem">- Op��o para realizar a movimenta��o de vidas individualmente, ou seja, item por item.</span>
						</TD>
					</TR>
					<TR>
						<TD>
							<P><asp:label id="Label8" runat="server" CssClass="ajudaTituloItem">- Movimenta��o por arquivo</asp:label>
								<span class="ajudaItem">- Op��o para realizar a movimenta��o em massa, isto �, por meio de envio de arquivo contendo toda a rela��o de 
								vida para a Seguradora. Para isso, existem duas formas de envio:<BR></span>
								<span class="ajudaItem">�<u>Incremental</u>: � enviado um arquivo apenas com as altera��es ocorridas na lista de 
								funcion�rios em rela��o ao per�odo de compet�ncia anterior. </span><br>
								<span class="ajudaItem">�<u>Substitui��o</u>: � enviado um arquivo descartando toda movimenta��o do per�odo de compet�ncia 
								anterior, adotando-se a nova listagem que dever� contemplar todos funcion�rios 
								de uma s� vez. Para tanto, � necess�rio definir o modelo do arquivo a ser 
								enviado, no menu lateral, em <b>Ap�lice/Subgrupo</b>, clique em <b>Configurar Layout do 
										Arquivo</b>.</span>
							</P>
						</TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label9" runat="server" CssClass="ajudaTituloItem">- Resultado do Processamento do Arquivo</asp:label>
							<span class="ajudaItem">� Op��o utilizada ap�s o envio de arquivo para Seguradora (�Movimenta��o por Arquivo�), para verificar se o arquivo foi processado corretamente e efetuar os devidos ajustes, se houver necessidade.</span>
						</TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label10" runat="server" CssClass="ajudaTituloItem">- Carregar a Rela��o Anterior</asp:label>
							<span class="ajudaItem">� Op��o para limpar todas as movimenta��es de vidas do subgrupo atual. Ap�s a utiliza��o desta op��o, a rela��o de vidas ser� a mesma que foi considerada para emiss�o da �ltima fatura.</span>
						</TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label1" runat="server" CssClass="ajudaTituloItem">- Encerrar Movimenta��o</asp:label>
							<span class="ajudaItem">(Acesso Restrito ao Administrador do Subgrupo) - Op��o para autorizar a Seguradora a emitir a fatura. Esta op��o deve ser utilizada somente depois que todas as movimenta��es do per�odo forem conclu�das. Vale ressaltar que, ap�s a utiliza��o desta op��o, n�o ser� mais poss�vel qualquer movimenta��o dentro do per�odo atual.</span>
						</TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label19" runat="server" CssClass="ajudaTituloItem">- Resumo da Fatura Atual</asp:label>
							<span class="ajudaItem"> - Resumo das movimenta��es da rela��o de vidas atual detalhada e o comparativo com os dados consolidados da �ltima fatura.</span>
						</TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label6" runat="server" CssClass="ajudaTituloItem">- Imprimir/Consultar Fatura Anterior</asp:label>
							<span class="ajudaItem"> - Op��o utilizada para consultar informa��es das faturas j� emitidas, inclusive a impress�o.</span>
						</TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label11" runat="server" CssClass="ajudaTituloItem">- Imprimir Certificado Individual</asp:label>
							<span class="ajudaItem"> - Op��o utilizada para imprimir o certificado do seguro para cada segurado separadamente.</span>
						</TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label12" runat="server" CssClass="ajudaTituloItem">- Download do Modelo de DPS (Declara��o Pessoal de Sa�de)</asp:label>
							<span class="ajudaItem"> - Permite a impress�o do modelo de DPS, exigida para capitais segurados superiores a R$ 100.000,00.</span>
						</TD>
					</TR>
				</TABLE>
				<asp:label id="Label13" runat="server" Font-Bold="True" Font-Underline="True">Ap�lice/Subgrupo</asp:label>&nbsp; 
				- Refere-se ao grupo de fun��es para administra��o do subgrupo.<br>
				<TABLE id="Table3" height="61" cellSpacing="0" cellPadding="0" width="94%" align="center"
					border="0">
					<TR>
						<TD><asp:label id="Label14" runat="server" CssClass="ajudaTituloItem">- Selecionar Outra Ap�lice/Subgrupo</asp:label>
							<span class="ajudaItem">- Permite que o usu�rio acesse uma outra ap�lice/subgrupo durante a navega��o no sistema.</span>
						</TD>
					</TR>
					<TR>
						<TD><asp:label id="Label15" runat="server" CssClass="ajudaTituloItem">- Consultar Dados do Subgrupo</asp:label>
							<span class="ajudaItem">- Permite a consulta de informa��es referentes ao subgrupo selecionado.</span>
						</TD>
					</TR>
					<TR>
						<TD><asp:label id="Label16" runat="server" CssClass="ajudaTituloItem">- Configurar Layout do Arquivo</asp:label>
							<span class="ajudaItem">- A fun��o de configura��o do layout do arquivo permite que o usu�rio estabele�a o modelo de arquivo a ser enviado � Seguradora. Nessa fun��o, define-se, por exemplo, quais colunas do arquivo constam as informa��es referentes � movimenta��o de vida solicitadas pela Companhia.</span>
						</TD>
					</TR>
					<TR>
						<TD><asp:label id="Label17" runat="server" CssClass="ajudaTituloItem">- Hist�rico e Pr�ximo Passo</asp:label>
							<span class="ajudaItem">- Op��o para consulta dos �ltimos acessos do subgrupo e quais os pr�ximos passos do processo de faturamento.</span>
						</TD>
					</TR>
				</TABLE>
				<asp:label id="Label20" runat="server" Font-Bold="True" Font-Underline="True">Sair</asp:label>&nbsp; 
				- Sair do sistema.<BR>
				<BR>
			</P>
			<P></P>
		</form>
	</body>
</HTML>
