Imports System.Drawing
Imports System.Web.UI.WebControls
Imports System.Data

Partial Class paginacaogrid
    Inherits System.Web.UI.UserControl


    Public _valor As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub

    Public Event ePaginaAlterada(ByVal Argumento As String)
    Public Event ePaginaAlteradaA(ByVal Argumento As String)

    Private Sub mDisparaEvento(ByVal Argumento As String)
        RaiseEvent ePaginaAlterada(Argumento)
    End Sub
    Private Sub mDisparaEventoA(ByVal Argumento As String)
        RaiseEvent ePaginaAlteradaA(Argumento)
    End Sub

    Private Sub mPaginacaoClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles a1.Command, a2.Command, a3.Command, a4.Command, a5.Command, a6.Command, a7.Command
        mDisparaEvento(e.CommandArgument)
    End Sub

    Private Sub aPaginacaoClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles a1.Command, a2.Command, a3.Command, a4.Command, a5.Command, a6.Command, a7.Command
        mDisparaEventoA(e.CommandArgument)
    End Sub

    Private Sub mMontaPaginacao(ByVal GridPaginaAtual As Integer, ByVal GridPaginasTotal As Integer)
        Dim paginainicial As Integer
        Dim paginafinal As Integer
        Dim paginaatual As Integer = GridPaginaAtual + 1

        If (paginaatual / 5) > 0 Then
            If (paginaatual Mod 5) <> 0 Then
                paginainicial = (Int(paginaatual / 5) * 5) + 1
            Else
                paginainicial = (Int((paginaatual - 1) / 5) * 5) + 1
            End If
            paginafinal = paginainicial + 4
            If paginafinal > GridPaginasTotal Then paginafinal = GridPaginasTotal
        Else
            paginainicial = 1
            paginafinal = 5
            If paginafinal > GridPaginasTotal Then paginafinal = GridPaginasTotal
        End If

        Me.a1.Visible = False
        Me.a2.Visible = False
        Me.a3.Visible = False
        Me.a4.Visible = False
        Me.a5.Visible = False
        Me.a6.Visible = False
        Me.a7.Visible = False
        If paginaatual > 1 Then
            a1.Text = "Anterior"
            a1.ForeColor = Color.White
            a1.Style.Add("font-family", "verdana")
            a1.Style.Add("font-size", "10px")
            a1.Style.Add("font-weight", "bold")
            a1.Style.Add("text-decoration", "underline")
            a1.Visible = True
        End If
        Dim a As Integer = 2
        For i As Integer = paginainicial To paginafinal
            CType(Me.FindControl("a" + a.ToString), LinkButton).CommandArgument = (i - 1).ToString
            CType(Me.FindControl("a" + a.ToString), LinkButton).Text = i.ToString
            CType(Me.FindControl("a" + a.ToString), LinkButton).Visible = True
            If i = paginaatual Then
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("color", "white")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("text-decoration", "none")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-family", "verdana")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-size", "10px")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-weight", "bold")

            Else
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("color", "white;")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("text-decoration", "underline")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-family", "verdana")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-size", "10px")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-weight", "bold")

            End If
            a += 1
        Next
        If paginaatual <> GridPaginasTotal Then
            a7.Text = "Pr�xima"
            a7.ForeColor = Color.White
            a7.Style.Add("font-family", "verdana")
            a7.Style.Add("font-size", "10px")
            a7.Style.Add("font-weight", "bold")
            a7.Style.Add("text-decoration", "underline")
            a7.Visible = True
        End If
    End Sub

    Public Sub GridDataBind(ByVal Grid As DataGrid, ByVal dt As DataTable)
        Grid.DataSource = dt
        Grid.DataBind()

        For Each x As Web.UI.WebControls.DataGridItem In Grid.Items

            x.Attributes.Add("onmouseover", "this.style.background='#E8F1FF'")
            x.Attributes.Add("onmouseout", "this.style.background=''")
            x.Cells.Item(Me._valor).Text = cUtilitarios.trataCPF(x.Cells.Item(Me._valor).Text)
            x.Attributes.Add("onclick", "GB_show('Manuten��o de usu�rios', '../../ManutencaoUsuario_Form.aspx?acao=alterar&cpf=" + cUtilitarios.destrataCPF(x.Cells.Item(Me._valor).Text) + "', 210, 450)")

            'onclick="location.href='?alterar=ok'"

        Next

        Me.mMontaPaginacao(Grid.CurrentPageIndex, Grid.PageCount)
    End Sub

    Public Property CssClass() As String
        Get
            Return Me.lblPaginacao.Attributes.Item("class")
        End Get
        Set(ByVal Value As String)
            Me.lblPaginacao.Attributes.Add("class", Value)
        End Set
    End Property
End Class
