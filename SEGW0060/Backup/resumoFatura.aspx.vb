Partial Class resumoFatura
    Inherits System.Web.UI.Page

    Private mov_online As String = ""


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub




    Protected WithEvents lblTopUsuario As System.Web.UI.WebControls.Label
    Protected WithEvents lblTopCPF As System.Web.UI.WebControls.Label


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim mov_online As String
        Dim linkseguro As New Alianca.Seguranca.Web.LinkSeguro

        Dim str As String = "&"

        For Each m As String In Session.Keys
            If Not IsNothing(Session(m)) Then
                If Session(m).GetType.ToString = "System.String" Then
                    str &= System.Web.HttpUtility.UrlEncode(m) & "=" & System.Web.HttpUtility.UrlEncode(Session(m)) & "&"
                End If
            End If
        Next

        btnVoltar.Attributes.Add("onclick", "top.exibeaguarde();history.back();")
        btnVoltar.Attributes.Add("onmouseover", "this.style.cursor='hand'")
        btnVoltar.Attributes.Add("onmouseout", "this.style.cursor='default'")
        'Imagem azul do help
        Imagebutton2.Attributes.Add("onclick", "return popup('')")
        Imagebutton2.Attributes.Add("style", "cursor:pointer")

        'Links do Menu
        mov_online = linkseguro.GerarParametro("", Alianca.Seguranca.Web.LinkSeguro.TempoExpiracao.Grande, Request.ServerVariables("REMOTE_ADDR"), Session("usuario_id"))

        lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomesubGrupo") & "<br>Fun��o: Resumo Fatura "
        lblVigencia.Text = "<br>" & cUtilitarios.getPeriodoCompetencia(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
        lblNavegacao.Visible = True
        lblVigencia.Visible = True

        Try
            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

            bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
            Dim wf_id As String = ""

            If dr.HasRows Then
                While dr.Read()
                    wf_id = dr.GetValue(0).ToString()
                End While
            End If
            dr.Close()
            'Mostra Resumo da Fatura Atual

            bd.ResumoFaturaAtual_Anterior(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Session("usuario"), wf_id)
            dr = bd.ExecutaSQL_DR

            If dr.Read() Then
                lblVidasFA.Text = dr.GetValue(8).ToString
                lblCapitalFA.Text = cUtilitarios.trataMoeda(dr.GetValue(6).ToString)
                lblPremioFA.Text = cUtilitarios.trataMoeda(dr.GetValue(7).ToString)
                Me.lblIOFA.Text = cUtilitarios.trataMoeda(dr.GetValue(14).ToString)
                Me.lblVlPremioBrutoA.Text = cUtilitarios.trataMoeda(dr.GetValue(13).ToString)
            Else
                lblVidasFA.Text = "0"
                lblCapitalFA.Text = "0,00"
                lblPremioFA.Text = "0,00"
                Me.lblIOFA.Text = "0,00"
                Me.lblVlPremioBrutoA.Text = "0,00"
            End If
            dr.Close()

            Dim bMovimentacaoAberta As Boolean = True
            bd.VerificaMovimentacaoEncerrada(Session("apolice"), _
                                            Session("ramo"), _
                                            Session("subgrupo_id"))
            Try
                Dim dr1 As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

                If dr1.HasRows Then
                    While dr1.Read()
                        If UCase(dr1.GetValue(0).ToString()) <> "A" Then
                            bMovimentacaoAberta = False
                        End If
                    End While
                End If
                dr1.Close()
            Catch ex As Exception
                Dim excp As New clsException(ex)
            End Try

            If bMovimentacaoAberta Then
                'Recalcular o resumo das opera��es - VIDA_WEB_DB..SEGS7048_SPI
                bd.Recalcula_Resumo_Operacoes(Session("apolice"), _
                                Session("ramo"), _
                                Session("subgrupo_id"), _
                                Session("usuario"))

                Try
                    bd.ExecutaSQL()
                Catch ex As Exception
                    Dim v_sCodigoErro As String = "1 - Page_Load"
                    Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
                End Try
            End If

            Try
                bd.ExecutaSQL()
            Catch ex As Exception
                Dim v_sCodigoErro As String = "1 - Page_Load"
                Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
            End Try

            'Resumo de Inclusoes, Altera��es e Exclus�es
            bd.ResumoFaturaAtual_IAE(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Session("usuario"), wf_id)
            'Response.Write(bd.SQL)
            Try
                dr = bd.ExecutaSQL_DR

                'variavel que atualiza na segw0060, o Resumo da Fatura Atual
                Dim script As String = "<script>"

                While dr.Read()
                    If dr.Item("operacao") = "I" Then
                        lblInclusoes_vidas.Text = dr.GetValue(0).ToString
                        lblInclusoes_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        lblInclusoes_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                        Me.lblVlPremioBrutoI.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)

                        'Soma
                        script &= "top.document.getElementById('divInclusoes_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                        script &= "top.document.getElementById('divInclusoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                        script &= "top.document.getElementById('divInclusoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf

                    ElseIf dr.Item("operacao") = "E" Then
                        lblExclusoes_vidas.Text = dr.GetValue(0).ToString
                        lblExclusoes_capital.Text = "-" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        lblExclusoes_premio.Text = "-" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                        Me.lblVlPremioBrutoE.Text = "-" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString)

                        'Subtrai
                        script &= "top.document.getElementById('divExclusoes_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                        script &= "top.document.getElementById('divExclusoes_capital').innerHTML= '-" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                        script &= "top.document.getElementById('divExclusoes_premio').innerHTML= '-" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf

                    ElseIf dr.Item("operacao") = "A" Then
                        lblAlteracoes_vidas.Text = dr.GetValue(0).ToString
                        lblAlteracoes_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        lblAlteracoes_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                        Me.lblVlPremioBrutoAlt.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)

                        script &= "top.document.getElementById('divAlteracoes_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                        script &= "top.document.getElementById('divAlteracoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                        script &= "top.document.getElementById('divAlteracoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf

                    ElseIf dr.Item("operacao") = "P" Then
                        lblDPS_vidas.Text = dr.GetValue(0).ToString
                        lblDPS_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        lblDPS_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                        Me.lblVlPremioBrutoPend.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)

                        script &= "top.document.getElementById('divDPS_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                        script &= "top.document.getElementById('divDPS_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                        script &= "top.document.getElementById('divDPS_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf

                    ElseIf dr.Item("operacao") = "R" Then
                        lblAcertos_vidas.Text = dr.GetValue(0).ToString
                        lblAcertos_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        lblAcertos_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                        Me.lblVlPremioBrutoAc.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)

                        script &= "top.document.getElementById('divAcertos_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                        script &= "top.document.getElementById('divAcertos_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                        script &= "top.document.getElementById('divAcertos_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf

                    ElseIf dr.Item("operacao") = "B" Then
                        lblExcAgendada_vidas.Text = dr.GetValue(0).ToString
                        lblExcAgendada_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        lblExcAgendada_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                        Me.lblVlPremioBrutoEAg.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)

                        script &= "top.document.getElementById('divExcAgen_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                        script &= "top.document.getElementById('divExcAgen_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                        script &= "top.document.getElementById('divExcAgen_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf

                    ElseIf dr.Item("operacao") = "F" Then
                        Me.lblPremioBrutoFT.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)

                    ElseIf dr.Item("operacao") = "O" Then
                        Me.lblIOFFT.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)

                    ElseIf dr.Item("operacao") = "T" Then
                        lblVidasFT.Text = dr.GetValue(0).ToString
                        lblCapitalFT.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                        lblPremioFT.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)

                        script &= "top.document.getElementById('divVidasFT').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                        script &= "top.document.getElementById('divCapitalFT').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                        script &= "top.document.getElementById('divPremioFT').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf
                    End If

                End While

                Response.Write(script & "</script>")
            Catch ex As Exception
                Dim excp As New clsException(ex)
            End Try

            'Trata valores (label) vazios
            If lblVidasFA.Text = "" Then
                lblVidasFA.Text = "0"
            End If
            If lblCapitalFA.Text = "" Then
                lblCapitalFA.Text = "0,00"
            End If
            If lblPremioFA.Text = "" Then
                lblPremioFA.Text = "0,00"
            End If
            If lblVlPremioBrutoA.Text = "" Then
                lblVlPremioBrutoA.Text = "0,00"
            End If

            If lblInclusoes_vidas.Text = "" Then
                lblInclusoes_vidas.Text = "0"
            End If
            If lblInclusoes_capital.Text = "" Then
                lblInclusoes_capital.Text = "0,00"
            End If
            If lblInclusoes_premio.Text = "" Then
                lblInclusoes_premio.Text = "0,00"
            End If
            If lblVlPremioBrutoI.Text = "" Then
                lblVlPremioBrutoI.Text = "0,00"
            End If

            If lblExcAgendada_vidas.Text = "" Then
                lblExcAgendada_vidas.Text = "0"
            End If
            If lblExcAgendada_capital.Text = "" Then
                lblExcAgendada_capital.Text = "0,00"
            End If
            If lblExcAgendada_premio.Text = "" Then
                lblExcAgendada_premio.Text = "0,00"
            End If


            If lblExclusoes_vidas.Text = "" Then
                lblExclusoes_vidas.Text = "0"
            End If
            If lblExclusoes_capital.Text = "" Then
                lblExclusoes_capital.Text = "0,00"
            End If
            If lblExclusoes_premio.Text = "" Then
                lblExclusoes_premio.Text = "0,00"
            End If
            If lblVlPremioBrutoE.Text = "" Then
                lblVlPremioBrutoE.Text = "0,00"
            End If

            If lblAlteracoes_vidas.Text = "" Then
                lblAlteracoes_vidas.Text = "0"
            End If
            If lblAlteracoes_capital.Text = "" Then
                lblAlteracoes_capital.Text = "0,00"
            End If
            If lblAlteracoes_premio.Text = "" Then
                lblAlteracoes_premio.Text = "0,00"
            End If

            If lblAcertos_vidas.Text = "" Then
                lblAcertos_vidas.Text = "0"
            End If
            If lblAcertos_capital.Text = "" Then
                lblAcertos_capital.Text = "0,00"
            End If
            If lblAcertos_premio.Text = "" Then
                lblAcertos_premio.Text = "0,00"
            End If

            If lblDPS_vidas.Text = "" Then
                lblDPS_vidas.Text = "0"
            End If
            If lblDPS_capital.Text = "" Then
                lblDPS_capital.Text = "0,00"
            End If
            If lblDPS_premio.Text = "" Then
                lblDPS_premio.Text = "0,00"
            End If

            If lblVidasFT.Text = "" Then
                lblVidasFT.Text = lblVidasFA.Text
            End If
            If lblCapitalFT.Text = "" Then
                lblCapitalFT.Text = lblCapitalFA.Text
            End If
            If lblPremioFT.Text = "" Then
                lblPremioFT.Text = lblPremioFA.Text
            End If

            '-------------------------------------------
            Me.lblSubTotal_vidas.Text = Int32.Parse(lblVidasFA.Text) + Int32.Parse(lblInclusoes_vidas.Text) - Int32.Parse(lblExclusoes_vidas.Text)
            Me.lblSubTotal_capital.Text = cUtilitarios.trataMoeda(Decimal.Parse(lblCapitalFA.Text) + Decimal.Parse(lblInclusoes_capital.Text) + Decimal.Parse(lblExclusoes_capital.Text))
            Me.lblSubTotal_premio.Text = cUtilitarios.trataMoeda(Decimal.Parse(lblPremioFA.Text) + Decimal.Parse(lblInclusoes_premio.Text) + Decimal.Parse(lblExclusoes_premio.Text))
            Me.lblSubTotalPremio.Text = cUtilitarios.trataMoeda(Decimal.Parse(lblVlPremioBrutoA.Text) + Decimal.Parse(lblVlPremioBrutoI.Text) + Decimal.Parse(lblVlPremioBrutoE.Text))
            '-------------------------------------------
            dr.Close()
            dr = Nothing
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        MostraColunaPremio()
    End Sub

    Private Function getTpAcesso() As String
        Dim ind_acesso As String = ""
        Select Case Session("acesso").ToString
            Case "1"
                ind_acesso = "Administrador da Alian�a do Brasil"
            Case "2"
                ind_acesso = "Administrador de Ap�lice"
            Case "3"
                ind_acesso = "Administrador de Subgrupo"
            Case "4"
                ind_acesso = "Operador"
            Case "5" ' Projeto 539202 - Jo�o Ribeiro - 28/04/2009
                ind_acesso = "Consulta"
            Case "6" ' Projeto 539202 - Jo�o Ribeiro - 28/04/2009
                ind_acesso = "Central de Atendimento"
            Case "7"    ' Demanda 4370246
                ind_acesso = "Adm. Master"
        End Select

        Return ind_acesso
    End Function

    Public Shared Function getCapitalSeguradoSession() As String

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5706_SPS(Web.HttpContext.Current.Session("apolice"), Web.HttpContext.Current.Session("ramo"), 6785, 0, Web.HttpContext.Current.Session("subgrupo_id"))

        Dim capitalSegurado As String = ""

        Try
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR

            Dim count As Int16 = 1

            dr.Read()

            capitalSegurado = dr.GetValue(1).ToString()

            dr.Close()
            dr = Nothing

        Catch ex As Exception
            Dim v_sCodigoErro As String = "2 - getCapitalSeguradoSession"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        End Try

        Return capitalSegurado

    End Function

    Public Shared Function MostraColunaPremio(Optional ByVal printjs As Boolean = True) As String

        Dim mostraColPremio As Boolean = False

        Dim capitalSegurado As String = getCapitalSeguradoSession()

        Select Case capitalSegurado
            Case "Capital Global"
                Web.HttpContext.Current.Response.Write("<input type='hidden' value='1' name='ocultaPremio' id='ocultaPremio'>")
        End Select

        Return mostraColPremio

    End Function

End Class



