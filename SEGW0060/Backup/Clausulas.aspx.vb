﻿Public Partial Class Clausulas
    Inherits System.Web.UI.Page
    Dim data_inicio, data_fim, retorno As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblNavegacao.Text = "Ramo/Apólice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomesubGrupo") & "<br>Função: Consulta de Cláusulas "

        lblVigencia.Text = "<br>" & getPeriodoCompetenciaSession()

        If Not Me.IsPostBack Then
            Le_Clausulas()
            DropdownSelClausula.SelectedIndex = 0
            Carrega_Clausulas(Me.DropdownSelClausula.SelectedValue)
        End If

        Try
            If Session("apolice") = "" Then
                cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
                Response.End()
            End If
        Catch ex As Exception
            cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
            Response.End()
        End Try
    End Sub

    Private Sub Le_Clausulas()
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim oListItem As System.Web.UI.WebControls.ListItem = Nothing

        bd.Clausulas(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

        Try
            dr = bd.ExecutaSQL_DR()

            Me.DropdownSelClausula.Items.Clear()
            If Not dr Is Nothing Then
                While dr.Read

                    oListItem = New System.Web.UI.WebControls.ListItem
                    oListItem.Value = dr.Item("seq_clausula").ToString.Trim
                    oListItem.Text = dr.Item("seq_clausula").ToString.Trim & " - " & dr.Item("descr_clausula").ToString.Trim

                    Me.DropdownSelClausula.Items.Add(oListItem)
                End While
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim v_sCodigoErro As String = "1 - Le_Clausulas"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nCódigo: " & v_sCodigoErro, ex)
        End Try

    End Sub

    Private Sub Carrega_Clausulas(ByVal nIndex As Integer)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim cTexto As String = ""

        Me.lblClausula.Text = ""
        bd.Clausulas(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), nIndex)

        btnImprimir.NavigateUrl = "ImprimeClausula.aspx?nSeq=" & nIndex
        txtSeq.Text = nIndex

        Try
            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    ' pablo.dias (Nova Consultoria) - 01/08/2011
                    ' 9420650 - Melhorias no Sistema Vida Web 
                    ' Melhorias no relatório de cláusulas
                    cTexto = dr.Item("texto_clausula")
                    'Substituição das quebras de linha
                    cTexto = Replace(cTexto, Chr(13), Chr(60) & "BR" & Chr(62))
                    Me.lblClausula.Text = cTexto
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim v_sCodigoErro As String = "2 - Carrega_Clausulas"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nCódigo: " & v_sCodigoErro, ex)
        End Try

    End Sub

    Private Function getPeriodoCompetenciaSession() As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)
        Try

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.HasRows Then
                While dr.Read()

                    data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                    data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)
                    cUtilitarios.escreveScript("var dt_ini_comp = '" & data_inicio & "';")
                    cUtilitarios.escreveScript("var dt_fim_comp = '" & data_fim & "';")

                End While

                retorno = "Período de Competência: " & data_inicio & " a " & data_fim
            Else
                Session.Abandon()
                Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
            End If
            dr.Close()
            dr = Nothing
        Catch ex As Exception
            Dim v_sCodigoErro As String = "3 - getPeriodoCompetenciaSession"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nCódigo: " & v_sCodigoErro, ex)
            retorno = ""
        End Try
        Return retorno
    End Function

    Protected Sub DropdownSelClausula_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DropdownSelClausula.SelectedIndexChanged
        Carrega_Clausulas(Me.DropdownSelClausula.SelectedValue)
    End Sub
End Class