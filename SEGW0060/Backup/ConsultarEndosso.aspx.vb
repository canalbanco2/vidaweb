Imports System.Web.UI.WebControls

Partial Public Class ConsultarEndosso
    Inherits System.Web.UI.Page

    Protected Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim objDataTable As Data.DataTable = Nothing
        Dim strSql As String = ""

        strSql = "SELECT A.* "
        strSql &= " FROM [SISAB003].SEGUROS_DB.DBO.PARAMETROS_ENDOSSO_TB A WITH (NOLOCK) "
        strSql &= "	JOIN [SISAB003].SEGUROS_DB.DBO.ENDOSSO_TB B WITH (NOLOCK) ON A.ENDOSSO_ID = B.ENDOSSO_ID "
        strSql &= "		AND A.PROPOSTA_ID = B.PROPOSTA_ID "
        strSql &= "		AND A.ENDOSSO_WEB = 1 "
        strSql &= "WHERE A.PROPOSTA_ID = " & txtPropostaId.Text

        BD.SQL = strSql
        objDataTable = BD.ExecutaSQL_DT

        If objDataTable.Rows.Count > 0 Then
            grdLista.DataSource = objDataTable
            grdLista.DataBind()
        Else
            lblRegistroInexistente.Visible = True
            grdLista.Visible = False
        End If
    End Sub

    Private Sub ConsultarEndosso_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim objDataTable As Data.DataTable = Nothing

        Dim secao As String = Request.QueryString("secao")
        Dim linkseguro As Alianca.Seguranca.Web.LinkSeguro

        'Implementação da SABL0101
        linkseguro = New Alianca.Seguranca.Web.LinkSeguro
        linkseguro.LerUsuario("ConsultarEndosso.aspx", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))

        Session("usuario_id") = linkseguro.Usuario_ID
        Session("usuario") = linkseguro.Login_REDE
        Session("cpf") = linkseguro.CPF

        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, linkseguro.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
        Else
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, linkseguro.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
        End If

        BD.SQL = "SELECT A.PROPOSTA_ID FROM [SISAB003].SEGUROS_DB.DBO.APOLICE_TB A WITH (NOLOCK) WHERE A.RAMO_ID = " & Session("ramo") & " AND A.APOLICE_ID = " & Session("apolice")
        objDataTable = BD.ExecutaSQL_DT

        If Not Page.IsPostBack Then
            If objDataTable.Rows.Count > 0 Then
                txtPropostaId.Text = objDataTable.Rows(0).Item("proposta_id").ToString
                btnPesquisar_Click(Nothing, Nothing)
            Else
                
            End If
        End If
    End Sub

    Private Sub grdLista_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdLista.ItemDataBound

        If e.Item.ItemType <> ListItemType.Pager And e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            e.Item.Attributes("onclick") = "ExibirRelatorioEndosso(" & e.Item.Cells(0).Text & ", " & e.Item.Cells(1).Text & ")"
        End If

    End Sub
End Class