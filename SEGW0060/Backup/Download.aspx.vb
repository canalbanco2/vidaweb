Partial Class Downloads
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents HyperLink1 As System.Web.UI.WebControls.HyperLink

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomesubGrupo") & "<br>Fun��o: Download modelo de DPS"
        lblNavegacao.Visible = True
        lblVigencia.Text = "<br>" & getPeriodoCompetenciaSession()

        Dim v_oDtDocumento As Data.DataTable = Nothing
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS7599_SPS()
        v_oDtDocumento = bd.ExecutaSQL_DT

        '1
        Dim v_oDvDocumento1 As New Data.DataView(v_oDtDocumento)
        v_oDvDocumento1.RowFilter = "tipo_doc_vida_web_id = 1"
        Dim v_oDtDocumento1 As Data.DataTable = v_oDvDocumento1.ToTable()
        If v_oDtDocumento1.Rows.Count > 0 Then
            Me.hplCliqueAqui.Enabled = True
            Me.hplCliqueAqui02.Enabled = True
            Me.hplCliqueAqui03.Enabled = True
        End If

        '2
        Dim v_oDvDocumento2 As New Data.DataView(v_oDtDocumento)
        v_oDvDocumento1.RowFilter = "tipo_doc_vida_web_id = 2"
        Dim v_oDtDocumento2 As Data.DataTable = v_oDvDocumento1.ToTable()
        If v_oDtDocumento2.Rows.Count > 0 Then Me.hplDPS.Enabled = True

        '3
        Dim v_oDvDocumento3 As New Data.DataView(v_oDtDocumento)
        v_oDvDocumento1.RowFilter = "tipo_doc_vida_web_id = 3"
        Dim v_oDtDocumento3 As Data.DataTable = v_oDvDocumento1.ToTable()
        If v_oDtDocumento2.Rows.Count > 0 Then Me.hplAte100.Enabled = True

        '4
        Dim v_oDvDocumento4 As New Data.DataView(v_oDtDocumento)
        v_oDvDocumento1.RowFilter = "tipo_doc_vida_web_id = 4"
        Dim v_oDtDocumento4 As Data.DataTable = v_oDvDocumento1.ToTable()
        If v_oDtDocumento2.Rows.Count > 0 Then Me.hplEntre100.Enabled = True

        '5
        Dim v_oDvDocumento5 As New Data.DataView(v_oDtDocumento)
        v_oDvDocumento1.RowFilter = "tipo_doc_vida_web_id = 5"
        Dim v_oDtDocumento5 As Data.DataTable = v_oDvDocumento1.ToTable()
        If v_oDtDocumento2.Rows.Count > 0 Then Me.hplMaior500.Enabled = True

        '6
        Dim v_oDvDocumento6 As New Data.DataView(v_oDtDocumento)
        v_oDvDocumento1.RowFilter = "tipo_doc_vida_web_id = 6"
        Dim v_oDtDocumento6 As Data.DataTable = v_oDvDocumento1.ToTable()
        If v_oDtDocumento2.Rows.Count > 0 Then Me.hplFichaFinanceira.Enabled = True

        Try
            If Session("apolice") = "" Then
                cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
                Response.End()
            End If
        Catch ex As Exception
            cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
            Response.End()
        End Try
    End Sub

    Private Function getTpAcesso() As String
        Dim ind_acesso As String = ""
        Select Case Session("acesso").ToString
            Case "1"
                ind_acesso = "Administrador da Alian�a do Brasil"
            Case "2"
                ind_acesso = "Administrador de Ap�lice"
            Case "3"
                ind_acesso = "Administrador de Subgrupo"
            Case "4"
                ind_acesso = "Operador"
            Case "5" ' Projeto 539202 - Jo�o Ribeiro - 28/04/2009
                ind_acesso = "Consulta"
            Case "6" ' Projeto 539202 - Jo�o Ribeiro - 28/04/2009
                ind_acesso = "Central de Atendimento"
            Case "7"    ' Demanda 4370246
                ind_acesso = "Adm. Master"
        End Select

        Return ind_acesso
    End Function

    Private Function getPeriodoCompetenciaSession() As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)
        Dim data_inicio As String = ""
        Dim data_fim As String = ""
        Dim retorno As String = ""

        Try

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()


            If dr.HasRows Then
                While dr.Read()

                    data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                    data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)
                    cUtilitarios.escreveScript("var dt_ini_comp = '" & data_inicio & "';")
                    cUtilitarios.escreveScript("var dt_fim_comp = '" & data_fim & "';")

                End While

                retorno = "Per�odo de Compet�ncia: " & data_inicio & " a " & data_fim
            Else
                Session.Abandon()
                System.Web.HttpContext.Current.Response.Write("<script>parent.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';</script>")
            End If
            dr.Close()
            dr = Nothing
        Catch ex As Exception
            Dim v_sCodigoErro As String = "1 - getPeriodoCompetenciaSession"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
            retorno = ""
        End Try
        Return retorno
    End Function

    Private Sub btnCartao_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Response.Write("<script>window.open('downloads/Cartao_Proposta.doc');</script>")
    End Sub

    'Ficha Financeira
    Private Sub lnkFinanceira_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Response.Write("<script>window.open('Downloads/Ficha_Financeira.doc'); </script>")
    End Sub


End Class
