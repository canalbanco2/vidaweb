Partial Class popMsg
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()        

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        If (Request.Form("msg") = Nothing) Then
            lblMsg.Text = ""
            lblTitulo.Text = ""
            '| johnny.barros(Confitec) Demanda:16035497 - 25/02/2013 [inicio] |
            lblVigencia.Text = "00/00/0000 at� 00/00/0000"
            '| johnny.barros(Confitec) Demanda:16035497 - 25/02/2013 [fim] |
        Else
            lblMsg.Text = System.Web.HttpUtility.UrlDecode(Request.Form("msg")).Replace("||", "<")
            lblTitulo.Text = System.Web.HttpUtility.UrlDecode(Request.Form("tit").ToString())
            '| johnny.barros(Confitec) Demanda:16035497 - 25/02/2013 [inicio] |
            Dim vVigIni As String
            Dim vVigFim As String
            'vVigIni = CType(System.Web.HttpUtility.UrlDecode(Request.Form("vigIni")), DateTime).ToString("dd/MM/yyyy")
            'vVigFim = CType(System.Web.HttpUtility.UrlDecode(Request.Form("vigFim")), DateTime).ToString("dd/MM/yyyy")
            vVigIni = System.Web.HttpUtility.UrlDecode(Request.Form("vigIni").ToString())
            vVigFim = System.Web.HttpUtility.UrlDecode(Request.Form("vigFim").ToString())
            lblVigencia.Text = vVigIni + " at� " + vVigFim
            '| johnny.barros(Confitec) Demanda:16035497 - 25/02/2013 [fim] |
        End If
    End Sub

End Class
