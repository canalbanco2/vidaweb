Imports System.Web
Imports System.Web.UI.WebControls

Partial Class _Default
    Inherits cPage
    Private mov_online As String = ""

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents pnlLiberar As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlCheckList As System.Web.UI.WebControls.Panel


    Protected WithEvents Panel1 As System.Web.UI.WebControls.Panel

    Protected WithEvents Link11 As System.Web.UI.WebControls.HyperLink

    Protected WithEvents HyperLink7 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents HyperLink9 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents session_interface_text As System.Web.UI.WebControls.TextBox
    'novos - Perfil Consulta (nao remover coment�rio)
    Protected WithEvents ExtratoArquivo As System.Web.UI.WebControls.HyperLink
    Protected WithEvents lblDPS_vidas1 As System.Web.UI.WebControls.Label
    Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    Protected WithEvents Label14 As System.Web.UI.WebControls.Label
    Protected WithEvents Label15 As System.Web.UI.WebControls.Label
    Protected WithEvents Label16 As System.Web.UI.WebControls.Label
    Protected WithEvents Label17 As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents Label5 As System.Web.UI.WebControls.Label
    Protected WithEvents Label6 As System.Web.UI.WebControls.Label
    Protected WithEvents lblNavega As System.Web.UI.WebControls.Label
    Protected WithEvents lblVigencia As System.Web.UI.WebControls.Label
    Protected WithEvents lblSaudacao As System.Web.UI.WebControls.Label
    Protected WithEvents lblDados As System.Web.UI.WebControls.Label
    Protected WithEvents lblCNPJ As System.Web.UI.WebControls.Label
    Protected WithEvents lblEmpresa As System.Web.UI.WebControls.Label
    Protected WithEvents lblStatus As System.Web.UI.WebControls.Label
    Protected WithEvents lblEndereco As System.Web.UI.WebControls.Label
    Protected WithEvents lblComplemento As System.Web.UI.WebControls.Label
    Protected WithEvents lblBairro As System.Web.UI.WebControls.Label
    Protected WithEvents lblCEP As System.Web.UI.WebControls.Label
    Protected WithEvents pnlApresentacao As System.Web.UI.WebControls.Panel
    Protected WithEvents lblCidade As System.Web.UI.WebControls.Label
    Protected WithEvents lblUF As System.Web.UI.WebControls.Label
    Protected WithEvents lblTextEmail As System.Web.UI.WebControls.Label
    Protected WithEvents lblDDD As System.Web.UI.WebControls.Label
    Protected WithEvents lblTelefone As System.Web.UI.WebControls.Label
    Protected WithEvents lblEmail As System.Web.UI.WebControls.Label
    Protected WithEvents lblSubgrupos As System.Web.UI.WebControls.Label
    Protected WithEvents pnlLogAcesso As System.Web.UI.WebControls.Panel
    Protected WithEvents tbUltimosAcessos As System.Web.UI.WebControls.Table
    Protected WithEvents pnInfoComplementar As System.Web.UI.WebControls.Panel
    Protected WithEvents tbInfo As System.Web.UI.WebControls.Table
    Protected WithEvents linkPorArquivo As System.Web.UI.WebControls.HyperLink
    Protected WithEvents linkMovOnline As System.Web.UI.WebControls.HyperLink
    Protected WithEvents contentEmail As System.Web.UI.WebControls.Label
    Protected WithEvents lblPrazo As System.Web.UI.WebControls.Label
    Protected WithEvents pnlBtnVoltar As System.Web.UI.WebControls.Panel
    Protected WithEvents btnVoltar As System.Web.UI.WebControls.Button
    Protected WithEvents lblAviso As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        ' FELIPE MARINHO - CONFITEC
        HyperLink7.NavigateUrl = "ConsultarEndosso.aspx?SLinkSeguro=" & Request.QueryString("sLinkSeguro")
    End Sub

#End Region

    Function diaSemana(ByVal dia As String) As String
        Dim nomedia As String = ""
        If dia = "1" Then
            nomedia = "Segunda-Feira, "
        ElseIf dia = "2" Then
            nomedia = "Ter�a-Feira, "
        ElseIf dia = "3" Then
            nomedia = "Quarta-Feira, "
        ElseIf dia = "4" Then
            nomedia = "Quinta-Feira, "
        ElseIf dia = "5" Then
            nomedia = "Sexta-Feira, "
        ElseIf dia = "6" Then
            nomedia = "S�bado, "
        ElseIf dia = "7" Then
            nomedia = "Domingo,  "
        End If

        Return nomedia
    End Function

    Function nomeMes(ByVal mes As String) As String
        If mes = "1" Then
            mes = "Janeiro"
        ElseIf mes = "2" Then
            mes = "Fevereiro"
        ElseIf mes = "3" Then
            mes = "Mar�o"
        ElseIf mes = "4" Then
            mes = "Abril"
        ElseIf mes = "5" Then
            mes = "Maio"
        ElseIf mes = "6" Then
            mes = "Junho"
        ElseIf mes = "7" Then
            mes = "Julho"
        ElseIf mes = "8" Then
            mes = "Agosto"
        ElseIf mes = "9" Then
            mes = "Setembro"
        ElseIf mes = "10" Then
            mes = "Outubro"
        ElseIf mes = "11" Then
            mes = "Novembro"
        ElseIf mes = "12" Then
            mes = "Dezembro"
        End If
        Return mes
    End Function

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim reloaded As String = ""

        Try
            reloaded = Request("reloaded")
            If reloaded Is Nothing Then
                reloaded = ""
            End If
        Catch ex As Exception

            Dim v_sCodigoErro As String = "1 - Page_Load"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)


        End Try

        If Not Page.IsPostBack And reloaded.Length = 0 Then

            Session.Remove("apolice")
            Session.Remove("ramo")
            Session.Remove("subgrupo_id")

            Dim temp As String = "&"

            If Request.QueryString.Count = 0 Then
                temp = "?"
            End If

            Dim v_sUrl As String = HttpContext.Current.Request.Url.AbsoluteUri
            Dim vUrl As String = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
            If vUrl = "http://w1ab.app.bbseguros.com.br/" Then
                v_sUrl = v_sUrl.Replace("w1ab.app.bbseguros.com.br/", "www.aliancadobrasil.com.br/") & temp & "reloaded=1"
                Response.Redirect(v_sUrl)
                'Response.Write("<script>parent.window.location='" & v_sUrl & "';</script>")
            Else
                Response.Write("<script>parent.window.location=parent.window.location + '" & temp & "reloaded=1';</script>")
            End If

        End If

        Me.txtApolice.Attributes.Add("onkeydown", "return soNum(this, event)")
        Me.txtCNPJ.Attributes.Add("onkeydown", "return soNum(this, event)")

        If Session("abandonar") = 1 Or Request.QueryString("abandonar") = 1 Then
            Session.Abandon()
        End If

        Dim linkseguro As Alianca.Seguranca.Web.LinkSeguro
        Dim cAmbiente As Alianca.Seguranca.Web.ControleAmbiente

        'Implementa��o da SABL0101
        linkseguro = New Alianca.Seguranca.Web.LinkSeguro
        Dim usuario As String = linkseguro.LerUsuario("Default.aspx", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))
        If usuario = "" Then
            'Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
        End If

        Session("usuario_id") = linkseguro.Usuario_ID
        Session("usuario") = linkseguro.Login_WEB
        Session("nomeUsuario") = linkseguro.Nome
        Session("cpf") = linkseguro.CPF

        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"

        cAmbiente.ObterAmbiente(url)

        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
        Else
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
        End If

        Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)

        Dim requestSubGrupo As String = Request.QueryString("subgrupo")
        If requestSubGrupo <> "" Then

            Session("nomeSubgrupo") = Request.QueryString("nomeSubgrupo")
            Session("subgrupo_id") = requestSubGrupo
            Session("acesso") = Request.QueryString("acesso")
        End If

        'Imagem azul do help
        Imagebutton2.Attributes.Add("onclick", "return popAjuda('AjudaResumo.aspx')")
        Imagebutton2.Attributes.Add("style", "cursor:pointer")

        Dim acesso_user As String = ""

        acesso_user = cUtilitarios.VerificaAcesso(Session("cpf"))

        If acesso_user.Equals("") Then
            acesso_user = -1
        End If
        Session("acesso") = acesso_user

        Me.ddlRamo.Attributes.Add("onchange", "return validaRamo();")
        Me.ddlEstipulante.Attributes.Add("onchange", "return validaEstipulante();")

        If Session("apolice") <> "" And Session("ramo") <> "" Then
            pnlResumo.Visible = False
            pnlHelp.Visible = True
        Else
            pnlResumo.Visible = False
            pnlHelp.Visible = False
        End If


        If Not Page.IsPostBack Then
            Me.lblDataCorrente.Text = diaSemana(Date.Today.DayOfWeek) & Date.Today.Day & " de " & nomeMes(Date.Today.Month) & " de " & Date.Today.Year

            'Implementa��o do Cabe�alho/Rodap�
            Me.lblUsuario.Text = ""
            Me.lblCPF.Text = ""
            Me.lblAmbiente.Text = IIf(cAmbiente.Ambiente = Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade, "Qualidade", "Produ��o")  'Convert.ToString("Produ��o")
            ''Me.lblAmbiente.Text = Convert.ToString("PRODU��O")

            Me.lblTopCPF.Text = Session("cpf")
            Me.lblTopUsuario.Text = Session("usuario")
            Me.lblTopAcessoUsuario.Text = Me.getTpAcesso()

            Response.Write("<input type='hidden' value='" & Session("acesso") & "' id='ind_acesso' name='ind_acesso'>")

            Me.pnlSubgruposOpcoes.Visible = False
            Me.pnlLegendaRamos.Visible = True

            If CStr(Session("subgrupo_id")) <> "" Then       'Ao selecionar uma Ap�lice/Subgrupo
                'Dim v_oDt As Data.DataTable = ListarPermissoesSituacao("A", 7)
                'If v_oDt.Rows.Count >= 1 Then
                '    Session("ExisteMaster") = 1
                'Else
                '    Session("ExisteMaster") = 0
                'End If
                pnlSubgrupoApolice.Visible = False
                pnlResumo.Visible = True
                pnlSubgruposOpcoes.Visible = True

                ConsultaAcesso(Session("apolice"), Session("ramo"), Session("cpf"))

                Call ExibeInformacoesResumo()
                '----------------------------------------------

                'Retira o menu de Administra��o caso o tipo de acesso for oper�rio
                If Session("acesso") = "4" Then
                    Me.pnlManutencaoUsuario.Visible = False
                End If

                Try
                    If Not Me.isAtividadeAberta(Me.getWfID(), 2) Then
                        'linkCarregarExtratoVidas.Attributes.Add("onclick", "alert('" & cUtilitarios.GetDataEncerramento() & "');return false;")
                        Link5.Attributes.Add("onclick", "verificaFaturaAberta(1,'Link5');return false;")
                        Link6.Attributes.Add("onclick", "verificaFaturaAberta(1,'Link6');return false;")
                        Hyperlink1.Attributes.Add("onclick", "alert('" & cUtilitarios.GetDataEncerramento() & "');return false;")
                        Hyperlink4.Attributes.Add("onclick", "verificaFaturaAberta(1,'Hyperlink4');return false;")
                    End If
                Catch ex As Exception
                    Dim v_sCodigoErro As String = "1 - page_load"
                    Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
                End Try
                '-----------------------------------------------

                Me.pnlLegendaRamos.Visible = False

            Else                                    'Ao clicar em "Selecionar Ap�lice/Subgrupo"
                pnlSubgrupoApolice.Visible = True
                pnlResumo.Visible = False
                'Mostra o menu de busca de ap�lice se for Consulta (acesso = 5) [27.09.07]

                Dim ind_acesso As String = cUtilitarios.VerificaAcesso(Session("cpf"))

                If ind_acesso.Equals("1") Then
                    ind_acesso = -1
                End If

                If ind_acesso.Equals("5") Or ind_acesso.Equals("-1") Then
                    Session("acesso") = ind_acesso
                    painelPerfilConsulta.Visible = True
                    mMontaDdlApolices(Session("cpf"), "", 5)
                Else
                    '| johnny.barros(Confitec) Demanda:16035497 - 01/03/2013 [inicio] |
                    painelPerfilConsulta.Visible = True
                    '| johnny.barros(Confitec) Demanda:16035497 - 01/03/2013 [fim] |
                    mMontaDdlApolices(Session("cpf"), "")
                End If



            End If
        End If


        Dim str As String = "&"

        Try
            For Each m As String In Session.Keys
                If Not IsNothing(Session(m)) Then
                    If Session(m).GetType.ToString = "System.String" Then
                        str &= System.Web.HttpUtility.UrlEncode(m) & "=" & System.Web.HttpUtility.UrlEncode(Session(m)) & "&"
                    End If
                End If
            Next
        Catch ex As Exception
            Dim v_sCodigoErro As String = "6 - page_load"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        End Try

        'Links do Menu
        mov_online = linkseguro.GerarParametro("", Alianca.Seguranca.Web.LinkSeguro.TempoExpiracao.Grande, Request.ServerVariables("REMOTE_ADDR"), Session("usuario_id"))

        'Bloqueia o item Encerrar Movimenta��o caso o tipo de acesso for diferente de adm. subgrupo.
        Try
            If Me.isAtividadeAberta(Me.getWfID(), 2) Then
                If Session("acesso") = "1" Or Session("acesso") = "3" Or Session("acesso") = "4" Or Session("acesso") = "7" Then
                    ' 1|
                    Hyperlink4.Attributes.Add("onclick", "$.post('ajaxLayout.aspx',{op:'relAnterior'},function(data){	" _
                                                           & "      if(data.split('|')[0] == '0'){" _
                                                           & "		    alert('" & cUtilitarios.GetDataEncerramento() & "');return false;" _
                                                           & "	    }else{" _
                                                           & "          if(data.split('|')[0] == '1'){" _
                                                           & "      " & Hyperlink4.Attributes.Item("onclick") & ";" _
                                                           & "              window.IFrameConteudoPrincipal.location = '../segw0078/CarregarVidas.aspx?SLinkSeguro=" + mov_online + "&expandir=1" & str & "';" _
                                                           & "	        }else{" _
                                                           & "              alert('Voc� n�o tem permiss�o para Carregar Rela��o Anterior');" _
                                                           & "          }}" _
                                                           & "      });")

                    Hyperlink4.NavigateUrl = "#"
                    Hyperlink4.Target = "_self"
                Else
                    '' 2|
                    Hyperlink4.Attributes.Add("onclick", "$.post('ajaxLayout.aspx',{op:'relAnterior'},function(data){	" _
                                                           & "      if(data.split('|')[0] == '0'){" _
                                                           & "		    alert('" & cUtilitarios.GetDataEncerramento() & "');return false;" _
                                                           & "	    }else{" _
                                                           & "          if(data.split('|')[0] == '1'){" _
                                                           & " " & Hyperlink4.Attributes.Item("onclick") & ";" _
                                                           & "              window.IFrameConteudoPrincipal.location = '../segw0078/CarregarVidas.aspx?SLinkSeguro=" + mov_online + "&expandir=1" & str & "';" _
                                                           & "	        }else{" _
                                                           & "              alert('Voc� n�o tem permiss�o para Carregar Rela��o Anterior');" _
                                                           & "          }}" _
                                                           & "      });")
                    Hyperlink4.NavigateUrl = "#"
                    Hyperlink4.Target = "_self"
                End If

                ' Projeto 539202 - Jo�o Ribeiro - 27/04/2009
                ' If Session("acesso") = "3" Or Session("acesso") = "1" Then
                If Session("acesso") = "3" Or Session("acesso") = "1" Or Session("acesso") = "6" Or Session("acesso") = "7" Then

                    'Autor: breno.nery (Nova Consultoria) - Data da Altera��o: 12/01/2012
                    'Demanda: 12784293 - Item: 17 - Alterar a mensagem de bloqueio de faturamento.
                    Dim MsgBloqueioFaturamento As String

                    If Me.VerificaAgenciaPilarAtacado(Session("apolice").ToString, Session("ramo").ToString) Then

                        MsgBloqueioFaturamento = _
                            "O encerramento da movimenta��o est� bloqueado por ordem da    \n" & _
                            "seguradora. Favor entrar em contato pelo telefone (11)3149-2656    "
                    Else
                        'Renato Vasconcelos
                        'Flow 14735947 
                        'Inicio
                        MsgBloqueioFaturamento = _
                            "O encerramento da movimenta��o est� bloqueado por ordem da  \n" & _
                            "Seguradora. Favor entrar em contato com o seu corretor ou com a ASA. \n" & _
                            "Telefone 0800 729 7000"
                        'MsgBloqueioFaturamento = _
                        '    "O encerramento da movimenta��o est� bloqueado por ordem da  \n" & _
                        '    "seguradora. Favor entrar em contato com a central de atendimento.  \n" & _
                        '    "Telefone 0800 729 7000"
                        'Fim
                    End If

                    Link4.Attributes.Add("onclick", "$.post('ajaxLayout.aspx',{op:'encerramento'},function(data){	" _
                                                           & "      if(data.split('|')[0] == '0'){ " _
                                                           & " " & Link4.Attributes.Item("onclick") & ";" _
                                                           & "          if( " & lblDPS_vidas.Text & "  > 0){" _
                                                           & "              if (confirm('Prezado Cliente, o subgrupo possui " & lblDPS_vidas.Text & " vidas(s) pendente(s), ap�s o encerramento da movimenta��o as inclus�es ou altera��es de capitais n�o ser�o acatadas e as pend�ncias ser�o eliminadas na atualiza��o da compet�ncia para a pr�xima movimenta��o. Para o(s) proponente(s) que n�o houve altera��o do capital, este permanecer� no faturamento considerando o �ltimo capital faturado.')) {" _
                                                           & "                  window.IFrameConteudoPrincipal.location = '../segw0069/FechamentoFatura.aspx?SLinkSeguro=" + mov_online + "&expandir=1" & str & "';" _
                                                           & "	            }" _
                                                           & "	        }else{" _
                                                           & "	            window.IFrameConteudoPrincipal.location = '../segw0069/FechamentoFatura.aspx?SLinkSeguro=" + mov_online + "&expandir=1" & str & "';" _
                                                           & "	        }" _
                                                           & "	    }else{" _
                                                           & "      if(data.split('|')[0] == '2'){" _
                                                           & "          alert('N�o � poss�vel encerrar a movimenta��o. \nValor total para faturamento inv�lido.');" _
                                                           & "	     }else{" _
                                                           & "      if(data.split('|')[0] == '3'){ " _
                                                           & "              alert('" & MsgBloqueioFaturamento & "');" _
                                                           & "      }else{" _
                                                           & "          alert('N�o � poss�vel encerrar a movimenta��o. \n" & cUtilitarios.GetDataEncerramento() & "');" _
                                                           & "      }}}" _
                                                           & "      });")
                    Link4.NavigateUrl = "#"
                    Link4.Target = "_self"

                    ' Projeto 539202 - Jo�o Ribeiro - 27/04/2009
                    ' If Session("acesso") = "1" Then
                    If Session("acesso") = "1" Or Session("acesso") = "6" Or Session("acesso") = "7" Then
                        Try
                            If (Link5.Attributes.Item("onclick") = Nothing) Then
                                Link5.Attributes.Add("onclick", "verificaFaturaAberta(1,'Link5');return false;")
                            End If
                        Catch ex As Exception
                            Link5.Attributes.Add("onclick", "verificaFaturaAberta(1,'Link5');return false;")
                        End Try
                        Link5.NavigateUrl = "../segw0065/MovimentacaoUmaUm.aspx?SLinkSeguro=" + mov_online + "&expandir=1" & str
                        'Link5.Attributes.Add("onclick", Link5.Attributes.Item("onclick") + "top.exibeaguarde('Link5');")
                        Link5.Attributes.Add("onclick", "top.exibeaguarde('Link5');")
                    Else
                        'Mov on-line
                        Link5.Attributes.Add("onclick", "alert('Voc� n�o tem permiss�o para realizar a Movimenta��o on-line');")
                        Link5.NavigateUrl = "#"
                        Link5.Target = "_self"

                    End If

                ElseIf Session("acesso") = "2" Or Session("acesso") = "5" Then
                    'Encerramento mov.
                    Link4.Attributes.Add("onclick", "alert('Voc� n�o tem permiss�o para Encerrar Movimenta��o');")
                    Link4.NavigateUrl = "#"
                    Link4.Target = "_self"

                    'Mov on-line
                    Link5.Attributes.Add("onclick", "alert('Voc� n�o tem permiss�o para realizar a Movimenta��o on-line');")
                    Link5.NavigateUrl = "#"
                    Link5.Target = "_self"

                Else
                    If Session("acesso") <> "3" And Session("acesso") <> "1" And Session("acesso") <> "7" Then    'acesso diferente de Adm. AB e subgrupo
                        Link4.Attributes.Add("onclick", "window.open('/seg/segw0069/ASP/teclado.asp?SLinkSeguro=" & mov_online & " &expandir=1" & str & "','win','toolbar=0,location=0,directories=0,status=no,statusbar=no,menubar=0,scrollbars=0,resizable=no,width=290,height=175,left=80,top=70');")
                        Link4.NavigateUrl = "#"
                        Link4.Target = "_self"
                    End If

                    ' Projeto 539202 - Jo�o Ribeiro - 27/04/2009
                    ' If Session("acesso") <> "4" And Session("acesso") <> "1" Then
                    If Session("acesso") <> "4" And Session("acesso") <> "1" And Session("acesso") <> "6" Then
                        'Mov on-line
                        Link5.Attributes.Add("onclick", "alert('Voc� n�o tem permiss�o para realizar a Movimenta��o on-line');")
                        Link5.NavigateUrl = "#"
                        Link5.Target = "_self"
                    Else
                        Try
                            If (Link5.Attributes.Item("onclick").Trim.Length = 0) Then
                                Link5.Attributes.Add("onclick", "verificaFaturaAberta(1,'Link5');return false;")
                            End If
                        Catch ex As Exception
                            Link5.Attributes.Add("onclick", "verificaFaturaAberta(1,'Link5');return false;")
                        End Try
                        Link5.NavigateUrl = "../segw0065/MovimentacaoUmaUm.aspx?SLinkSeguro=" + mov_online + "&expandir=1" & str
                        'Link5.Attributes.Add("onclick", Link5.Attributes.Item("onclick") + "top.exibeaguarde('Link5');")
                        Link5.Attributes.Add("onclick", "top.exibeaguarde('Link5');")
                    End If
                End If
            Else
                Hyperlink4.NavigateUrl = "../segw0078/CarregarVidas.aspx?SLinkSeguro=" + mov_online + "&expandir=1" & str
                Try
                    If (Hyperlink4.Attributes.Item("onclick") = Nothing) Then
                        Hyperlink4.Attributes.Add("onclick", "verificaFaturaAberta(1,'hyperlink4');return false;")
                    End If
                Catch ex As Exception
                    Hyperlink4.Attributes.Add("onclick", "verificaFaturaAberta(1,'hyperlink4');return false;")
                End Try
                Hyperlink4.Attributes.Add("onclick", Hyperlink4.Attributes.Item("onclick") + "top.exibeaguarde('hyperlink4');")
            End If
        Catch ex As Exception
            Dim v_sCodigoErro As String = "1 - page_load"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        End Try

        If Session("acesso") = "5" Then
            Link6.Attributes.Add("onclick", "alert('Voc� n�o tem permiss�o para realizar o Envio de Arquivo para An�lise.');return false;")
            Link6.NavigateUrl = "#"
            Link6.Target = "_self"
        Else
            'Link6.Attributes.Add("onclick", "$.post('ajaxLayout.aspx',{op:'layout'},function(data){	" _
            '                                           & "      if(data.split('|')[0] == '0'){" _
            '                                           & "		    alert('Para movimenta��o por arquivo � necess�rio configurar o layout anteriormente.');" _
            '                                           & "	    }else{" _
            '                                           & "          if(data.split('|')[0] == '1'){" _
            '                                           & " " & Link6.Attributes.Item("onclick") & "; top.exibeaguarde('Link6');" _
            '                                           & "              window.IFrameConteudoPrincipal.location = '../segw0066/MovimentacaoMassa.aspx?SLinkSeguro=" & mov_online & "&expandir=1" & str & "';" _
            '                                           & "	        }else{" _
            '                                           & "              alert('N�o � poss�vel executar movimenta��o por arquivo ap�s executar movimenta��es nas vidas');" _
            '                                           & "          }}" _
            '                                           & "      });")


            'Renato Vasconcelos
            'Flow 14735947 
            Link6.Attributes.Add("onclick", "$.post('ajaxLayout.aspx',{op:'layout'},function(data){	" _
                                                     & "      if(data.split('|')[0] == '0'){" _
                                                     & "		    alert('Para movimenta��o por arquivo � necess�rio configurar o layout anteriormente.');" _
                                                     & "	    }else{" _
                                                     & "          if(data.split('|')[0] == '1'){" _
                                                     & " " & Link6.Attributes.Item("onclick") & "; top.exibeaguarde('Link6');" _
                                                     & "              window.IFrameConteudoPrincipal.location = '../segw0066/MovimentacaoMassa.aspx?SLinkSeguro=" & mov_online & "&expandir=1" & str & "';" _
                                                     & "	        }else{" _
                                                     & "              alert('Se voc� solicitou endosso de inclus�o de vidas acima do limite de idade \n ou realizou movimenta��o no modo on-line, n�o ser� poss�vel importar o \n arquivo nesta compet�ncia. Favor prosseguir na movimenta��o on-line.');" _
                                                     & "          }}" _
                                                     & "      });")
            Link6.NavigateUrl = "#"
            Link6.Target = "_self"
        End If
        '----------------------------------------------------------

        Link3.NavigateUrl = "../segw0076/manutencaoUsuarios.aspx?SLinkSeguro=" + mov_online + "&expandir=1" & str
        Link3.Attributes.Add("onclick", "top.exibeaguarde('Link3');")

        '[08/10/2007] - Adicionada nova regra de permiss�o para o perfil de Consulta
        If Session("acesso") = "5" Then
            Hyperlink3.Attributes.Add("onclick", "alert('Voc� n�o tem permiss�o para Configurar Layout.');")
            Hyperlink3.NavigateUrl = "#"
            Hyperlink3.Target = "_self"

            ' Projeto 539202 - Jo�o Ribeiro - 27/04/2009
            ' ElseIf Session("acesso") = "1" Or Session("acesso") = "4" Then
        ElseIf Session("acesso") = "1" Or Session("acesso") = "4" Or Session("acesso") = "6" Or Session("acesso") = "7" Then
            Hyperlink3.NavigateUrl = "../segw0081/LayoutArquivo.aspx?SLinkSeguro=" + mov_online + "&expandir=1" & str
            Hyperlink3.Attributes.Add("onclick", "top.exibeaguarde('Hyperlink3');")
        Else
            Hyperlink3.Attributes.Add("onclick", "alert('Voc� n�o tem permiss�o para Configurar Layout.');")
            Hyperlink3.NavigateUrl = "#"
            Hyperlink3.Target = "_self"
        End If

        linkCarregarExtratoVidas.NavigateUrl = "../segw0080/CarregaExtratoVidas.aspx?SLinkSeguro=" + mov_online + "&expandir=1" & str
        linkCarregarExtratoVidas.Attributes.Add("onclick", linkCarregarExtratoVidas.Attributes.Item("onclick") + " top.exibeaguarde('linkCarregarExtratoVidas');")

        '[08/10/2007] - Adicionada nova regra de permiss�o para o perfil de Consulta
        If Session("acesso") = "5" Then
            Hyperlink1.Attributes.Add("onclick", "alert('Voc� n�o tem permiss�o para Resultado Processamento.');")
            Hyperlink1.NavigateUrl = "#"
            Hyperlink1.Target = "_self"

            ' Projeto 539202 - Jo�o Ribeiro - 27/04/2009
            ' ElseIf Session("acesso") = "1" Or Session("acesso") = "4" Then
        ElseIf Session("acesso") = "1" Or Session("acesso") = "4" Or Session("acesso") = "6" Or Session("acesso") = "7" Then
            Hyperlink1.NavigateUrl = "../segw0068/VidasCriticadas.aspx?SLinkSeguro=" + mov_online + "&expandir=1" & str
            Hyperlink1.Attributes.Add("onclick", "top.exibeaguarde('Hyperlink1');")
            Hyperlink1.Attributes.Add("onclick", "verificaFaturaAberta(1,'Hyperlink1');return false;")
        Else
            Hyperlink1.Attributes.Add("onclick", "alert('Voc� n�o tem permiss�o para Resultado Processamento.');")
            Hyperlink1.NavigateUrl = "#"
            Hyperlink1.Target = "_self"
        End If


        'If Session("apolice") <> "" And Session("ramo") <> "" And Session("cpf") <> "" Then
        '    'If ConsultaAcessoTecnico(Session("apolice"), Session("ramo"), Session("cpf")) Then
        '    linkRelatorioArquivosUpload.NavigateUrl = "../segw0202/relacao_pendentes.aspx?SLinkSeguro=" + mov_online + "&apolice=" & Session("apolice") & "&ramo=" & Session("ramo") & "&subgrupo_id=" & Session("subgrupo_id") & "&SoMedico=N"
        '    linkRelatorioArquivosUpload.Attributes.Add("onclick", "top.exibeaguarde('linkRelatorioArquivosUpload');")
        '    'Else
        '    '    linkRelatorioArquivosUpload.Attributes.Add("onclick", "alert('Voc� n�o tem permiss�o para visualiza��o desse relat�rio.');")
        '    '    linkRelatorioArquivosUpload.NavigateUrl = "#"
        '    '    linkRelatorioArquivosUpload.Target = "_self"
        '    'End If
        'Else
        '    linkRelatorioArquivosUpload.Attributes.Add("onclick", "alert('Selecionar um ramo/ap�lice para consulta.');")
        '    linkRelatorioArquivosUpload.NavigateUrl = "#"
        '    linkRelatorioArquivosUpload.Target = "_self"
        'End If

        HyperLink8.NavigateUrl = "../segw0072/consultarfaturas.aspx?SLinkSeguro=" + mov_online + "&expandir=1" & str
        HyperLink8.Attributes.Add("onclick", "top.exibeaguarde('HyperLink8');")

        link99.NavigateUrl = "../segw0123/Default.aspx?SLinkSeguro=" + mov_online + "&expandir=1" & str
        link99.Attributes.Add("onclick", "top.exibeaguarde('link99');")

        ' breno.nery (Nova Consultoria) - 01/03/2012
        ' 12784329 - Cria��o de Painel de Monitoramento Vida Web 
        ' Retornar permiss�es de acesso ao painel de monitoramento -----------------------------------------------

        LinkConsultaMonitoramento.NavigateUrl = "../SEGW0188/MonitoramentoConsulta.aspx?SLinkSeguro=" + mov_online + "&expandir=1" & str
        LinkConsultaMonitoramento.Attributes.Add("onclick", "top.exibeaguarde('LinkConsultaMonitoramento');")

        LinkAdministracaoMonitoramento.NavigateUrl = "../SEGW0187/MonitoramentoAdministracao.aspx?SLinkSeguro=" + mov_online + "&expandir=1" & str
        LinkAdministracaoMonitoramento.Attributes.Add("onclick", "top.exibeaguarde('LinkAdministracaoMonitoramento');")

        linkRelatorioArquivosUploadMonitoramento.NavigateUrl = "../segw0202/relacao_pendentes.aspx?SLinkSeguro=" + mov_online + "&expandir=1" + "&apolice=0&ramo=0&subgrupo_id=0&SoMedico=N"
        linkRelatorioArquivosUploadMonitoramento.Attributes.Add("onclick", "top.exibeaguarde('linkRelatorioArquivosUpload');")

        Call MostrarPainelMonitoramento()
        ' --------------------------------------------------------------------------------------------------------

        Link8.NavigateUrl = "../segw0071/imprimircertificados.aspx?SLinkSeguro=" + mov_online + "&expandir=1" & str
        Link8.Attributes.Add("onclick", "top.exibeaguarde('Link8');")

        Hyperlink2.Attributes.Add("onclick", "top.exibeaguarde('Hyperlink2');")

        'pcarvalho - 20080904 - Bloqueia quando movimenta��o estiver encerrada
        '-----------------
        link80.NavigateUrl = "../segw0102/Pendencias.aspx?SLinkSeguro=" + mov_online + "&expandir=1" & str
        link80.Attributes.Add("onclick", "top.exibeaguarde('Link80');")
        '''''''

        ' Autor: pablo.cardoso (Nova Consultoria)
        ' Data da Altera��o: 12/07/2011
        ' Demanda: 9420650
        ' Item: 9 - Minutas de Endosso
        linkMinutasEndosso.NavigateUrl = "../SEGW0183/MinutasEndosso.aspx?SLinkSeguro=" + mov_online + "&expandir=1" & str
        linkMinutasEndosso.Attributes.Add("onclick", "top.exibeaguarde('linkMinutasEndosso');")
        ''''''''
        ' Autor: thiago.mello (Nova Consultoria)
        ' Data da Altera��o: 23/04/2014
        ' Demanda: 18151764
        ' Item: Criar Menu para Visualiza��o-Impress�o de Sinistro
        'linkRelatorioSinistro.NavigateUrl = "../SEGW0183/MinutasEndosso.aspx?SLinkSeguro=" + mov_online + "&expandir=1" & str
        'linkRelatorioSinistro.Attributes.Add("onclick", "top.exibeaguarde('LinkRelatorioSinistro');")
        ''''''''

        If Session("acesso") = "2" Or Session("acesso") = "5" Then
            Link4.Attributes.Add("onclick", "alert('Voc� n�o tem permiss�o para Encerrar Movimenta��o');")
            Link4.NavigateUrl = "#"
            Link4.Target = "_self"

        Else
            If Not Me.isAtividadeAberta(Me.getWfID(), 2) Then
                Link4.Attributes.Add("onclick", "alert('N�o h� atividade aberta - " & cUtilitarios.GetDataEncerramento() & "');return false;")
                Link4.NavigateUrl = "#"
                Link4.Target = "_self"
            Else
                ' Projeto 539202 - Jo�o Ribeiro - 27/04/2009
                ' If Session("acesso") <> "3" And Session("acesso") <> "1" Then 'acesso diferente de Adm. AB e subgrupo
                If Session("acesso") <> "3" And Session("acesso") <> "1" And Session("acesso") <> "6" And Session("acesso") <> "7" Then 'acesso diferente de Adm. AB e subgrupo
                    Link4.Attributes.Add("onclick", "window.open('/seg/segw0069/ASP/teclado.asp?SLinkSeguro=" & mov_online & " &expandir=1" & str & "','win','toolbar=0,location=0,directories=0,status=no,statusbar=no,menubar=0,scrollbars=0,resizable=no,width=290,height=175,left=80,top=70');")
                    Link4.NavigateUrl = "#"
                    Link4.Target = "_self"
                End If
            End If
        End If

        Hyperlink5.Attributes.Add("onclick", "top.exibeaguarde('HyperLink5');")
        Me.linkResumoFatura.Visible = True

        linkResumoFatura.Attributes.Add("onclick", "top.exibeaguarde('linkResumoFatura');")
        Link9.Attributes.Add("onclick", "top.exibeaguarde('Link9');")

        MostraColunaPremio()

        Me.btnRamoApolice.Attributes.Add("onclick", "return ValidaFiltro();")
        Me.btnEstipulante.Attributes.Add("onclick", "return ValidaFiltro();")
        Me.radioRamo.Attributes.Add("onclick", "LimpaCampos();")
        Me.radioEstipulante.Attributes.Add("onclick", "LimpaCampos();")

        ' pablo.dias (Nova Consultoria) - 01/08/2011
        ' 9420650 - Melhorias no Sistema Vida Web 
        ' Bloquear links quando ap�lice estiver cancelada
        If Session("situacao_apolice") = "Cancelado" Then
            cUtilitarios.SetOnClickEvent(Link5, "alert('N�o � poss�vel executar movimenta��o on-line em ap�lices canceladas.');")
            cUtilitarios.SetOnClickEvent(Link4, "alert('N�o � poss�vel encerrar movimenta��o de ap�lices canceladas.');")
            cUtilitarios.SetOnClickEvent(Hyperlink3, "alert('N�o � poss�vel configurar layout em ap�lices canceladas.');")
            cUtilitarios.SetOnClickEvent(Link6, "alert('N�o � poss�vel enviar arquivo para an�lise em ap�lices canceladas.');")
            cUtilitarios.SetOnClickEvent(Hyperlink1, "alert('N�o � poss�vel ver o resultado do processamento em ap�lices canceladas.');")
        End If

        'gabriel.faria (Nova Consultoria) - 12/07/2013
        '
        HyperLink9.NavigateUrl = String.Concat("../SEGW0199/CartaAssistencia.aspx?SLinkSeguro=", mov_online, "&expandir=1", str, "&CNPNCartao=", Session("CNPJCartao"))
        HyperLink9.Attributes.Add("onclick", "top.exibeaguarde('HyperLink9');")

    End Sub

    ' pablo.dias (Nova Consultoria) - 01/08/2011
    ' 9420650 - Melhorias no Sistema Vida Web 
    ' Exibir tela informando as faturas inadimplentes de uma ap�lice
    Private Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("apolice") <> "" And Session("subgrupo_id") = "" Then
            If PodeMostrarJanelaApolice() Then
                Dim url As String = HyperLink8.NavigateUrl
                url = url.Replace("consultarfaturas.aspx", "ConsultarFaturasPendentes.aspx")
                url = ReplaceQueryString(url, "apolice", Session("apolice"))
                url = ReplaceQueryString(url, "ramo", Session("ramo"))
                url = ReplaceQueryString(url, "estipulante", Session("estipulante"))
                Response.Write("<script>window.open('" & url & "', 'winfaturaspendentes', 'toolbar=0,location=0,directories=0,status=no,statusbar=no,menubar=0,scrollbars=1,resizable=no,width=750,height=400,left=80,top=70');</script>")
            End If
        End If
    End Sub

    ' pablo.dias (Nova Consultoria) - 01/08/2011
    ' 9420650 - Melhorias no Sistema Vida Web 
    ' Verificar se pode mostrar a janela da ap�lice - ela s� deve aparecer uma vez
    Private Function PodeMostrarJanelaApolice() As Boolean
        Dim PodeMostrar As Boolean = (Session("apolice") <> Session("apolice_anterior")) And (Session("ramo") <> Session("ramo_anterior"))
        If PodeMostrar Then
            Session("apolice_anterior") = Session("apolice")
            Session("ramo_anterior") = Session("ramo")
        End If
        Return PodeMostrar AndAlso ApolicePendente()
    End Function

    ' pablo.dias (Nova Consultoria) - 01/08/2011
    ' 9420650 - Melhorias no Sistema Vida Web 
    ' Verificar se a ap�lice possui faturas pendentes
    Private Function ApolicePendente() As Boolean
        Dim BancoDados As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim ExecSQL As Data.SqlClient.SqlDataReader

        BancoDados.SEGS9678_SPS(Session("apolice"), Session("ramo"))

        ExecSQL = BancoDados.ExecutaSQL_DR()

        Return ExecSQL.HasRows

    End Function

    ' pablo.dias (Nova Consultoria) - 01/08/2011
    ' 9420650 - Melhorias no Sistema Vida Web 
    ' Fun��o auxiliar para substituir valores no hyperlink de visualiza��o de faturas
    Private Function ReplaceQueryString(ByVal strURL As String, ByVal QuerystringKey As String, ByVal NewValue As String) As String

        Dim re As New Text.RegularExpressions.Regex("(.*)(\?|&)" + QuerystringKey + "=[^&]+.(&)(.*)", Text.RegularExpressions.RegexOptions.IgnoreCase)
        strURL = (strURL + "&").Replace(QuerystringKey + "=&", QuerystringKey + "=" + NewValue + "&")
        strURL = re.Replace(strURL + "&", "$1$2$4")
        strURL = Mid(strURL, 1, (strURL.Length - 1))

        If InStr(strURL, "?") = 0 Then
            Return strURL + "?" + QuerystringKey + "=" + NewValue
        Else
            Return strURL + "&" + QuerystringKey + "=" + NewValue
        End If

    End Function

    ' breno.nery (Nova Consultoria) - 01/03/2012
    ' 12784329 - Cria��o de Painel de Monitoramento Vida Web 
    ' Retornar permiss�es de acesso ao painel de monitoramento
    Private Sub MostrarPainelMonitoramento()
        Dim BancoDados As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim ExecSQL As Data.SqlClient.SqlDataReader

        Try
            If CStr(Session("subgrupo_id")) = "" Then

                BancoDados.SEGS10093_SPS(Session("usuario"))
                ExecSQL = BancoDados.ExecutaSQL_DR

                Dim verConsulta As Boolean = False
                Dim verAdministracao As Boolean = False

                While ExecSQL.Read()
                    verConsulta = (Convert.ToString(ExecSQL("permissao_consulta")) = "s")
                    verAdministracao = (Convert.ToString(ExecSQL("permissao_administracao")) = "s")
                End While

                pnlMonitoramentoVidaWeb.Visible = verConsulta OrElse verAdministracao
                LinkConsultaMonitoramento.Visible = verConsulta
                linkRelatorioArquivosUploadMonitoramento.Visible = verConsulta
                LinkAdministracaoMonitoramento.Visible = verAdministracao

            Else

                Me.pnlMonitoramentoVidaWeb.Visible = False

            End If

        Catch ex As Exception

            Throw New Exception(ex.Message)

        End Try

    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        If Session("ramo") = "" Then
            Me.ddlRamo.SelectedIndex = 0
        End If
    End Sub

#Region " Metodos Botao "
    Private Sub lnkTrocar1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkTrocar1.Click
        pnlSubgrupoApolice.Visible = True

        Session("subgrupo_id") = ""
        'Session("acesso") = ""
        'Session.Remove("acesso")

        ddlRamo.SelectedIndex = 0
        pnlSubgruposOpcoes.Visible = False
        Me.pnlLegendaRamos.Visible = True
        pnlResumo.Visible = False

        ' breno.nery (Nova Consultoria) - 01/03/2012
        ' 12784329 - Cria��o de Painel de Monitoramento Vida Web 
        ' Retornar permiss�es de acesso ao painel de monitoramento
        MostrarPainelMonitoramento()

        'Mostra o menu de busca de ap�lice se for Consulta (acesso = 5) [27.09.07]
        Try
            Dim ind_acesso As String = cUtilitarios.VerificaAcesso(Session("cpf"))
            If ind_acesso = "1" Then
                ind_acesso = -1
            End If

            If ind_acesso = "5" Or ind_acesso = "-1" Then
                Session("acesso") = ind_acesso
                painelPerfilConsulta.Visible = True
                mMontaDdlApolices(Session("cpf"), "", 5)
            Else
                mMontaDdlApolices(Session("cpf"), "")
            End If

        Catch ex As Exception
            Dim v_sCodigoErro As String = "3 - lnkTrocar1_Click"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato com oadministrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        End Try
    End Sub

    Private Sub lnkTrocar2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkTrocar2.Click
        pnlSubgrupoApolice.Visible = True

        Session("ramo") = ""
        Session("apolice") = ""
        Session("subgrupo_id") = ""
        Session("acesso") = ""
        Session.Remove("acesso")

        ddlRamo.SelectedIndex = 0
        pnlSubgruposOpcoes.Visible = False
        Me.pnlLegendaRamos.Visible = True
        pnlResumo.Visible = False

        ' breno.nery (Nova Consultoria) - 01/03/2012
        ' 12784329 - Cria��o de Painel de Monitoramento Vida Web 
        ' Retornar permiss�es de acesso ao painel de monitoramento
        MostrarPainelMonitoramento()

        'Mostra o menu de busca de ap�lice se for Consulta (acesso = 5) [27.09.07]
        Try
            Dim ind_acesso As String = cUtilitarios.VerificaAcesso(Session("cpf"))
            If ind_acesso = "1" Then
                ind_acesso = -1
            End If

            If ind_acesso = "5" Or ind_acesso = "-1" Then
                Session("acesso") = ind_acesso
                painelPerfilConsulta.Visible = True
                mMontaDdlApolices(Session("cpf"), "", 5)
            Else
                mMontaDdlApolices(Session("cpf"), "")
            End If

        Catch ex As Exception
            Dim v_sCodigoErro As String = "2 - lnkTrocar2_Click"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato com oadministrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        End Try
    End Sub

    Private Sub btnRamoApolice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRamoApolice.Click

        If Me.txtRamo.SelectedValue <> "" And Me.txtApolice.Text.Trim <> "" Then
            Me.lblSemApolice.Visible = False
            Me.BuscaApolice(1)
        Else
            Me.txtRamo.SelectedIndex() = 0
            Me.txtApolice.Text = ""
        End If

    End Sub

    Private Sub btnEstipulante_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEstipulante.Click

        If Me.txtEstipulante.Text.Trim <> "" Then
            Me.lblSemApolice.Visible = False
            Me.BuscaApolice(2)
        End If

    End Sub

    Private Sub btnCNPJ_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCNPJ.Click
        If Me.txtCNPJ.Text.Trim <> "" And Me.txtCNPJ.Text.Length >= 3 Then
            Me.lblSemApolice.Visible = False
            Me.BuscaApolice(3)
        End If
    End Sub

    Private Sub btnPesqTodos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPesqTodos.Click
        Me.lblSemApolice.Visible = False
        If txtApolice.Text.Trim.Length = 0 And txtCNPJ.Text.Trim.Length = 0 And txtEstipulante.Text.Trim.Length = 0 And txtRamo.SelectedIndex = 0 Then
            recarregaDdlRamo()
            Session("subgrupo_id") = ""
            Session("ramo") = ""
            Session("apolice") = ""
            Session("estipulante") = ""
            pnlSubgrupoApolice.Visible = True
            pnlSubgruposOpcoes.Visible = False
        Else
            Me.BuscaApolice(4)
        End If
    End Sub
#End Region

#Region " Metodos DropDownlist "
    'Carrega dropdown com as ap�lices do usu�rio
    Private Sub mMontaDdlApolices(ByVal usuario As String, ByVal selected As String, Optional ByVal acesso As Int16 = 0)

        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader
        Dim count As Int16 = 1
        Dim text As String
        Dim value As String

        If acesso = 0 Then
            BD.consultar_apolice_ramos_sps(usuario, 1)
        Else
            BD.consultar_apolice_ramos_sps(usuario, acesso)
        End If

        Try
            dr = BD.ExecutaSQL_DR()

            If Not dr Is Nothing Then
                While dr.Read()

                    value = dr.GetValue(1).ToString() & "-" & dr.GetValue(0).ToString() & " - " & dr.GetValue(4).ToString()
                    text = dr.GetValue(1).ToString() & "-" & dr.GetValue(0).ToString() & " - " & dr.GetValue(4).ToString()

                    ddlRamo.Items.Add(text)
                    ddlRamo.Items(count).Value = value

                    If value = selected Then
                        ddlRamo.SelectedIndex = count
                    End If

                    count = count + 1
                End While
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
            Me.ddlRamo.SelectedIndex = 0
        Catch ex As Exception
            Dim v_sCodigoErro As String = "1 - MontaddlApolices"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato com oadministrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        End Try

    End Sub

    Public Sub populaDropDown(ByVal dropdown As Web.UI.WebControls.DropDownList, ByVal dr As Data.SqlClient.SqlDataReader, Optional ByVal selected As String = "")
        Dim count As Int16 = 1
        Dim text As String
        Dim value As String

        If Not dr Is Nothing Then
            If dr.HasRows Then

                While dr.Read()
                    value = dr.GetValue(0).ToString()
                    text = dr.GetValue(1).ToString()

                    dropdown.Items.Add(text)
                    dropdown.Items(count).Value = value

                    If value = selected Then
                        ddlRamo.SelectedIndex = count
                    End If

                    count = count + 1
                End While
            End If
        End If
    End Sub

    Private Sub ddlRamo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlRamo.SelectedIndexChanged

        If ddlRamo.SelectedValue <> "" Then

            Session("subgrupo_id") = ""
            Dim ramoApolice As String = ddlRamo.SelectedValue
            Dim ramoApoliceSplit As Array = ramoApolice.Split("-")

            ' pablo.dias (Nova Consultoria) - 01/08/2011
            ' 9420650 - Melhorias no Sistema Vida Web 
            ' Retirando espa�os dos valores jogados na sess�o
            If UBound(ramoApoliceSplit) > 0 Then
                Session("ramo") = Trim(ramoApoliceSplit(0))
                Session("apolice") = Trim(ramoApoliceSplit(1))

                Try
                    Session("estipulante") = Trim(ramoApoliceSplit(2))
                Catch ex As Exception
                    Session("estipulante") = "---"
                End Try
            End If
            pnlSubgrupoApolice.Visible = True
            pnlSubgruposOpcoes.Visible = False
        Else

            Session("apolice") = ""

            Session("subgrupo_id") = ""
            Session("estipulante") = ""
            Session("ramo") = ""
            pnlSubgrupoApolice.Visible = True
            pnlSubgruposOpcoes.Visible = False
        End If
    End Sub

    Private Sub ddlEstipulante_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEstipulante.SelectedIndexChanged
        If ddlEstipulante.SelectedValue <> "" Then

            Session("subgrupo_id") = ""
            Dim ramoApolice As String = ddlEstipulante.SelectedValue
            Dim ramoApoliceSplit As Array = ramoApolice.Split("-")

            If UBound(ramoApoliceSplit) > 0 Then
                Session("ramo") = ramoApoliceSplit(0)
                Session("apolice") = ramoApoliceSplit(1)

                Try
                    Session("estipulante") = ramoApoliceSplit(2)
                Catch ex As Exception
                    Session("estipulante") = "---"
                End Try
            End If
            pnlSubgrupoApolice.Visible = True
            pnlSubgruposOpcoes.Visible = False
        Else

            Session("apolice") = ""

            Session("subgrupo_id") = ""
            Session("estipulante") = ""
            Session("ramo") = ""
            pnlSubgrupoApolice.Visible = True
            pnlSubgruposOpcoes.Visible = False
        End If
    End Sub

    Private Sub recarregaDdlRamo()
        Try
            Dim ind_acesso As String = cUtilitarios.VerificaAcesso(Session("cpf"))
            If ind_acesso = "1" Then
                ind_acesso = -1
            End If
            If ind_acesso = "5" Or ind_acesso = "-1" Then
                Session("acesso") = ind_acesso
                painelPerfilConsulta.Visible = True
                Me.ddlRamo.Items.Clear()
                ddlRamo.Items.Add("Selecione o Ramo - Ap�lice")
                mMontaDdlApolices(Session("cpf"), "", 5)
            Else
                Me.ddlRamo.Items.Clear()
                ddlRamo.Items.Add("Selecione o Ramo - Ap�lice")
                mMontaDdlApolices(Session("cpf"), "")
            End If

        Catch ex As Exception
            Dim v_sCodigoErro As String = "11 - BuscaApolice"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato com oadministrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        End Try
    End Sub
#End Region

#Region " Outros Metodos "
    Private Sub TrataCamposVazios()
        If lblVidasFA.Text = "" Then lblVidasFA.Text = "0"
        If lblCapitalFA.Text = "" Then lblCapitalFA.Text = "0,00"
        If lblPremioFA.Text = "" Then lblPremioFA.Text = "0,00"

        If lblInclusoes_vidas.Text = "" Then lblInclusoes_vidas.Text = "0"
        If lblInclusoes_capital.Text = "" Then lblInclusoes_capital.Text = "0,00"
        If lblInclusoes_premio.Text = "" Then lblInclusoes_premio.Text = "0,00"

        If lblExclusoes_vidas.Text = "" Then lblExclusoes_vidas.Text = "0"
        If lblExclusoes_capital.Text = "" Then lblExclusoes_capital.Text = "0,00"
        If lblExclusoes_premio.Text = "" Then lblExclusoes_premio.Text = "0,00"

        If lblAlteracoes_vidas.Text = "" Then lblAlteracoes_vidas.Text = "0"
        If lblAlteracoes_capital.Text = "" Then lblAlteracoes_capital.Text = "0,00"
        If lblAlteracoes_premio.Text = "" Then lblAlteracoes_premio.Text = "0,00"

        If lblAcertos_vidas.Text = "" Then lblAcertos_vidas.Text = "0"
        If lblAcertos_capital.Text = "" Then lblAcertos_capital.Text = "0,00"
        If lblAcertos_premio.Text = "" Then lblAcertos_premio.Text = "0,00"

        If lblExcAgen_vidas.Text = "" Then lblExcAgen_vidas.Text = "0"
        If lblExcAgen_capital.Text = "" Then lblExcAgen_capital.Text = "0,00"
        If lblExcAgen_premio.Text = "" Then lblExcAgen_premio.Text = "0,00"

        If lblDPS_vidas.Text = "" Then lblDPS_vidas.Text = "0"
        If lblDPS_capital.Text = "" Then lblDPS_capital.Text = "0,00"
        If lblDPS_premio.Text = "" Then lblDPS_premio.Text = "0,00"

        If lblVidasFT.Text = "" Then lblVidasFT.Text = lblVidasFA.Text
        If lblCapitalFT.Text = "" Then lblCapitalFT.Text = lblCapitalFA.Text
        If lblPremioFT.Text = "" Then lblPremioFT.Text = lblPremioFA.Text
    End Sub

    Private Sub ExibeInformacoesResumo()
        'Pega wf_id para Fatura Atual
        Dim wf_id As String = ""
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Try
            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read() Then
                    wf_id = dr.GetValue(0).ToString()
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If

            If wf_id <> "" Then
                'Mostra Resumo da Fatura Atual
                Dim bMovimentacaoAberta As Boolean = True
                bd.VerificaMovimentacaoEncerrada(Session("apolice"), _
                                                Session("ramo"), _
                                                Session("subgrupo_id"))
                Try
                    Dim dr1 As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

                    If Not dr1 Is Nothing Then
                        If dr1.Read() Then
                            If UCase(dr1.GetValue(0).ToString()) <> "A" Then
                                bMovimentacaoAberta = False
                            End If
                        End If
                        If Not dr1.IsClosed Then dr1.Close()
                        dr1 = Nothing
                    End If
                Catch ex As Exception
                    Dim excp As New clsException(ex)
                End Try

                If bMovimentacaoAberta Then
                    'Recalcular o resumo das opera��es - VIDA_WEB_DB..SEGS7048_SPI
                    bd.Recalcula_Resumo_Operacoes(Session("apolice"), _
                                    Session("ramo"), _
                                    Session("subgrupo_id"), _
                                    Session("usuario"))

                    Try
                        bd.ExecutaSQL()
                    Catch ex As Exception
                        Dim excp As New clsException(ex)
                    End Try
                End If

                bd.ResumoFaturaAtual_Anterior(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Session("usuario"), wf_id)
                dr = bd.ExecutaSQL_DR

                If Not dr Is Nothing Then
                    If dr.Read() Then
                        lblVidasFA.Text = dr.GetValue(8).ToString
                        lblCapitalFA.Text = cUtilitarios.trataMoeda(dr.GetValue(6).ToString)
                        '539202 - confitec 27/07/09
                        'lblPremioFA.Text = cUtilitarios.trataMoeda(dr.GetValue(7).ToString)
                        lblPremioFA.Text = cUtilitarios.trataMoeda(dr("val_bruto"))
                    Else
                        lblVidasFA.Text = "0"
                        lblCapitalFA.Text = "0,00"
                        lblPremioFA.Text = "0,00"
                    End If
                    If Not dr.IsClosed Then dr.Close()
                    dr = Nothing
                End If

                'Resumo de Inclusoes, Altera��es e Exclus�es
                bd.ResumoFaturaAtual_IAE(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Session("usuario"), wf_id)
                dr = bd.ExecutaSQL_DR

                If Not dr Is Nothing Then
                    While dr.Read()
                        If dr.Item("operacao") = "I" Then
                            lblInclusoes_vidas.Text = dr.GetValue(0).ToString
                            lblInclusoes_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                            lblInclusoes_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                        ElseIf dr.Item("operacao") = "E" Then
                            lblExclusoes_vidas.Text = dr.GetValue(0).ToString
                            lblExclusoes_capital.Text = "-" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                            lblExclusoes_premio.Text = "-" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                        ElseIf dr.Item("operacao") = "A" Then
                            lblAlteracoes_vidas.Text = dr.GetValue(0).ToString
                            lblAlteracoes_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                            lblAlteracoes_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                        ElseIf dr.Item("operacao") = "R" Then
                            lblAcertos_vidas.Text = dr.GetValue(0).ToString
                            lblAcertos_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                            lblAcertos_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                        ElseIf dr.Item("operacao") = "P" Then
                            Dim varPendencia As Integer
                            lblDPS_vidas.Text = dr.GetValue(0).ToString
                            varPendencia = CInt(lblDPS_vidas.Text)
                            If varPendencia > 0 Then
                                link80.Style.Add("COLOR", "RED")
                            Else
                                link80.Style.Add("COLOR", "#555555")
                            End If
                            lblDPS_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                            lblDPS_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                        ElseIf dr.Item("operacao") = "B" Then
                            lblExcAgen_vidas.Text = dr.GetValue(0).ToString
                            lblExcAgen_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                            lblExcAgen_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                            '539202 - confitec 27/07/09
                            'ElseIf dr.Item("operacao") = "T" Then
                        ElseIf dr.Item("operacao") = "F" Then
                            lblVidasFT.Text = dr.GetValue(0).ToString
                            lblCapitalFT.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                            lblPremioFT.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                        End If

                    End While
                    If Not dr.IsClosed Then dr.Close()
                    dr = Nothing
                End If

                'Trata valores (label) vazios
                '-------------------------------------------
                Call TrataCamposVazios()
                '-------------------------------------------

                Dim totalFA As Int32 = 0
                Dim totalIncVidas As Int32 = 0
                Dim totalExcVidas As Int32

                Try
                    totalFA = Int32.Parse(lblVidasFA.Text)
                    totalIncVidas = Int32.Parse(lblInclusoes_vidas.Text)
                    totalExcVidas = Int32.Parse(lblExclusoes_vidas.Text)
                Catch ex As Exception

                End Try

                Me.lblSubTotal_vidas.Text = totalFA + totalIncVidas - totalExcVidas
                Me.lblSubTotal_capital.Text = cUtilitarios.trataMoeda(Decimal.Parse(lblCapitalFA.Text) + Decimal.Parse(lblInclusoes_capital.Text) + Decimal.Parse(lblExclusoes_capital.Text))
                Me.lblSubTotal_premio.Text = cUtilitarios.trataMoeda(Decimal.Parse(lblPremioFA.Text) + Decimal.Parse(lblInclusoes_premio.Text) + Decimal.Parse(lblExclusoes_premio.Text))

                'If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Private Function ExisteMovimentacaoVidas(ByVal apolice As Int32, ByVal ramo As Int16, ByVal subgrupo As Int16) As Boolean

        'Quando existe movimenta��o nas vidas, retorna TRUE, quando n�o existe, retorna FALSE
        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader
        Dim retorno As Boolean = False

        BD.VerificaMovimentacao(apolice, ramo, subgrupo, 1)

        Try
            dr = BD.ExecutaSQL_DR

            If Not dr Is Nothing Then
                If dr.Read() Then
                    retorno = True
                Else
                    retorno = False
                End If

                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If

            Return retorno

        Catch ex As Exception
            Dim excp As New clsException(ex)
            Return retorno
        End Try

    End Function

    Private Function ExisteLayout(ByVal apolice As Int32, ByVal ramo As Int16, ByVal subgrupo As Int16) As Boolean

        'Quando existe movimenta��o nas vidas, retorna TRUE, quando n�o existe, retorna FALSE
        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader
        Dim retorno As Boolean = False

        BD.getLayoutArquivo(apolice, ramo, subgrupo)

        Try
            dr = BD.ExecutaSQL_DR

            If Not dr Is Nothing Then
                If dr.Read() Then
                    retorno = True
                Else
                    retorno = False
                End If

                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If

            Return retorno

        Catch ex As Exception
            Dim excp As New clsException(ex)
            Return retorno
        End Try

    End Function

    Private Function ExisteMovimentacaoArquivo(ByVal apolice As Int32, ByVal ramo As Int16, ByVal subgrupo As Int16) As Boolean
        'Quando existe movimenta��o de arquivo, retorna TRUE, quando n�o existe, retorna FALSE
        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader
        Dim retorno As Boolean = False

        BD.VerificaMovimentacao(apolice, ramo, subgrupo)

        Try
            dr = BD.ExecutaSQL_DR

            If Not dr Is Nothing Then
                If dr.Read() Then
                    retorno = True
                Else
                    retorno = False
                End If

                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If

            Return retorno

        Catch ex As Exception
            Dim excp As New clsException(ex)
            MsgBox(excp.Message)
            Return False
        End Try

    End Function

    Public Function getWfID() As String
        If CStr(Session("subgrupo_id")) <> "" Then       'Ao selecionar uma Ap�lice/Subgrupo

            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
            bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)
            Dim dr As Data.SqlClient.SqlDataReader = Nothing
            Try
                dr = bd.ExecutaSQL_DR()

                If Not dr Is Nothing Then
                    If dr.Read() Then
                        Return dr.GetValue(0).ToString()
                    End If
                    If Not dr.IsClosed Then dr.Close()
                    dr = Nothing
                End If
                Return "0"
            Catch ex As Exception
                Dim v_sCodigoErro As String = "4 - getWfID"
                Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato com oadministrador do sistema \nC�digo: " & v_sCodigoErro, ex)
                Return ""
            End Try

        Else
            Return "0"
        End If
    End Function

    Private Function getWF_ID() As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim cRetorno As String = ""

        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

        Try
            dr = bd.ExecutaSQL_DR()

            If Not dr Is Nothing Then
                If dr.Read() Then
                    cRetorno = dr.GetValue(0).ToString
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If

        Catch ex As Exception
            Dim v_sCodigoErro As String = "9 - getWF_ID"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato com oadministrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        End Try
        Return cRetorno

    End Function

    Public Function isAtividadeAberta(ByVal wf_id As String, ByVal atividade As String) As Boolean
        Try

            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
            bd.getEstadoAtividade(wf_id, atividade)
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If Not dr Is Nothing Then
                If dr.Read Then
                    If dr.GetValue(0).ToString = "1" Then
                        Return True
                    End If
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If

        Catch ex As Exception
            Dim v_sCodigoErro As String = "5 - IsAtividadeAberta"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato com oadministrador do sistema \nC�digo: " & v_sCodigoErro, ex)
            Return False
        End Try

        Return False

    End Function

    Public Function VerificaAgenciaPilarAtacado(ByVal ApoliceId As String, ByVal RamoId As String) As Boolean
        Try

            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
            bd.ConsultarAgencia(ApoliceId, RamoId)
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If Not dr Is Nothing Then
                If dr.Read Then
                    If dr.GetValue(0).ToString.Trim.ToUpper Like "*CORPORATE*" Or _
                       dr.GetValue(0).ToString.Trim.ToUpper Like "*EMPRES*" Or _
                       dr.GetValue(0).ToString.Trim = "" Then
                        Return True
                    End If
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If

        Catch ex As Exception
            Dim v_sCodigoErro As String = "5 - VerificaAgenciaPilarAtacado"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato com o administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
            Return False
        End Try

        Return False

    End Function

    'Insere os dados no banco para logar os �ltimos Acessos
    Private Sub mLogaAcesso(ByVal apolice_id As Long, ByVal subgrupo_id As Integer, ByVal ramo_id As Integer, ByVal usuario As String)

        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        BD.TEMPinsereUltimoAcesso(apolice_id, subgrupo_id, ramo_id, usuario)

        Try
            BD.ExecutaSQL()
        Catch ex As Exception
            Dim v_sCodigoErro As String = "6 - mlogaAcesso"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato com oadministrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        End Try
    End Sub

    Private Function getTpAcesso() As String
        Dim ind_acesso As String = ""
        Select Case Session("acesso").ToString
            Case "1"
                ind_acesso = "Adm. Alian�a do Brasil"
            Case "2"
                ind_acesso = "Adm. Ap�lice"
            Case "3"
                ind_acesso = "Adm. Subgrupo"
            Case "4"
                ind_acesso = "Operador"
            Case "5"
                ind_acesso = "Consulta"
            Case "6"    ' Projeto 539202 - Jo�o Ribeiro - 30/04/2009
                ind_acesso = "Central de Atendimento"
            Case "7"    ' Demanda 4370246
                ind_acesso = "Adm. Master"
        End Select

        Return ind_acesso
    End Function

    Private Function ValidaEncerramento() As Boolean
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

        Dim retorno As Boolean = False
        Dim data_fim As DateTime
        'Dim prazo_limite As DateTime
        Dim hoje As DateTime = DateTime.Today

        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

        Try

            If Not dr Is Nothing Then
                If dr.Read() Then
                    data_fim = dr.GetValue(2).ToString.Substring(0, 10)
                Else
                    Session.Abandon()
                    cUtilitarios.br("N�o existe nenhuma Compet�ncia de Fatura em aberto")
                    System.Web.HttpContext.Current.Response.Write("<script>parent.window.location=parent.window.location;</script>")
                End If

                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim v_sCodigoErro As String = "7 - ValidaEncerramento"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato com oadministrador do sistema \nC�digo: " & v_sCodigoErro, ex)
            retorno = False
        End Try

        'C�lculo de valida��o
        If data_fim.AddDays(-30) <= hoje Then
            retorno = True
        Else
            retorno = False
        End If
        Return retorno

    End Function

    Public Function GetDataLimiteFaturamento() As Integer

        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader
        Dim wf_id As Long

        BD.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

        Try
            dr = BD.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read() Then
                    wf_id = dr.GetValue(0).ToString()
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim v_sCodigoErro As String = "8 - GetDatalimiteFaturamento"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato com oadministrador do sistema \nC�digo: " & v_sCodigoErro, ex)
            Return ""
        End Try


        BD.SEGS5722_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID, wf_id)

        Try
            dr = BD.ExecutaSQL_DR()

            If Not dr Is Nothing Then
                If dr.Read() Then
                    Return CInt(dr.GetValue(0))
                End If

                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Function

    Public Shared Function getCapitalSeguradoSession() As String
        Dim capitalSegurado As String = ""
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim count As Int16 = 1

        Try

            If Web.HttpContext.Current.Session("apolice") Is Nothing Then 'Or Web.HttpContext.Current.Session("subgrupo_id") Is Nothing Then
                Return ""
            End If

            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
            If (Web.HttpContext.Current.Session("apolice") = "" Or Web.HttpContext.Current.Session("subgrupo_id") = "" Or Web.HttpContext.Current.Session("ramo") = "") Then
                Return ""
            End If

            bd.SEGS5706_SPS(Web.HttpContext.Current.Session("apolice"), Web.HttpContext.Current.Session("ramo"), 6785, 0, Web.HttpContext.Current.Session("subgrupo_id"))

            Try
                dr = bd.ExecutaSQL_DR()

                If Not dr Is Nothing Then
                    If dr.Read() Then
                        capitalSegurado = dr.GetValue(1).ToString()
                    End If
                    If Not dr.IsClosed Then dr.Close()
                    dr = Nothing
                End If

            Catch ex As Exception
                Dim v_sCodigoErro As String = "9- getCapitalSeguradoSession"
                Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato com oadministrador do sistema \nC�digo: " & v_sCodigoErro, ex)
            End Try

        Catch ex As Exception
            Dim v_sCodigoErro As String = "9 - getCapitalSeguradoSession"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato com oadministrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        End Try

        If capitalSegurado <> "" Then
            Return capitalSegurado
        Else
            Return ""
        End If
    End Function

    Public Shared Function MostraColunaPremio(Optional ByVal printjs As Boolean = True) As String

        Dim mostraColPremio As Boolean = False

        Dim capitalSegurado As String = getCapitalSeguradoSession()

        Select Case capitalSegurado
            Case "Capital Global"
                Web.HttpContext.Current.Response.Write("<input type='hidden' value='1' name='ocultaPremio' id='ocultaPremio'>")
        End Select

        Return mostraColPremio

    End Function

    Private Sub BuscaApolice(ByVal RamoApolice1_Estipulante2 As Short)

        'FLOW 11543918 - CONFITEC - EDUARDO DE OLIVEIRA - INICIO
        If RamoApolice1_Estipulante2 = 4 Then

            Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
            Dim dr As Data.SqlClient.SqlDataReader = Nothing

            Me.painelEstipulante.Visible = False

            Dim apol As String = Me.txtApolice.Text.Trim
            Try
                If apol.Length = 0 Then
                    apol = "null"
                Else
                    apol = Int32.Parse(apol)
                End If
            Catch ex As Exception
                apol = "null"
            End Try

            Dim ram As String = Me.txtRamo.SelectedValue
            Try
                If ram.Length = 0 Then
                    ram = "null"
                Else
                    ram = Int32.Parse(ram)
                End If
            Catch ex As Exception
                ram = "null"
            End Try

            Dim cnpj As String
            If txtCNPJ.Text.Trim.Length = 0 Then
                cnpj = "null"
            Else
                cnpj = txtCNPJ.Text.Trim
            End If

            Dim estipulante As String
            If txtEstipulante.Text.Trim.Length = 0 Then
                estipulante = "null"
            Else
                estipulante = "'" & txtEstipulante.Text.Trim & "'"
            End If

            BD.SEGS6581_SPS(Session("cpf"), estipulante, apol, ram, cnpj)

            Try
                dr = BD.ExecutaSQL_DR()

                If Not dr Is Nothing Then
                    If dr.HasRows Then
                        If dr.Read Then
                            Session("ramo") = dr.Item("ramo_id").ToString
                            Session("apolice") = dr.Item("apolice_id").ToString
                            Session("subgrupo_id") = ""
                            Session("estipulante") = "---"
                            pnlSubgrupoApolice.Visible = True
                            pnlSubgruposOpcoes.Visible = False
                        End If

                        Dim count As Int16 = 1
                        Dim retornou As Boolean = False
                        Try
                            'Me.ddlRamo.Items.Clear()

                            Dim text As String = ""
                            Dim value As String = ""

                            If dr.HasRows Then

                                'ddlRamo.Items.Add("Selecione o Ramo - Ap�lice")

                                While dr.Read

                                    If Not IsDBNull(dr.Item("nome")) Then
                                        text = dr.Item("nome").ToString & " (" & dr.Item("ramo_id").ToString & "-" & dr.Item("apolice_id").ToString & ")"
                                        value = dr.Item("ramo_id").ToString & "-" & dr.Item("apolice_id").ToString & "-" & dr.Item("nome").ToString

                                        If value = "" Then
                                            ddlRamo.SelectedIndex = count
                                        End If

                                        retornou = True
                                        count = count + 1
                                    Else
                                        retornou = False
                                    End If
                                End While

                                If retornou = True Then
                                    painelEstipulante.Visible = False
                                Else
                                    lblSemApolice.Text = "Nenhuma ap�lice encontrada."
                                    lblSemApolice.Visible = True
                                    painelEstipulante.Visible = False
                                    ddlRamo.Items.Add("Selecione o Ramo - Ap�lice")
                                End If

                            End If

                        Catch ex As Exception
                            Dim v_sCodigoErro As String = "10 - BuscaApolice"
                            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato com oadministrador do sistema \nC�digo: " & v_sCodigoErro, ex)
                        End Try

                    Else
                        Session("subgrupo_id") = ""
                        Session("ramo") = ""
                        Session("apolice") = ""
                        Session("estipulante") = ""
                        pnlSubgrupoApolice.Visible = True
                        pnlSubgruposOpcoes.Visible = False

                        lblSemApolice.Text = "Nenhuma ap�lice encontrada."
                        lblSemApolice.Style.Add("color", "red")
                        lblSemApolice.Visible = True
                        ddlRamo.Items.Add("Selecione o Ramo - Ap�lice")
                    End If

                    If Not dr.IsClosed Then dr.Close()
                    dr = Nothing

                End If
            Catch ex As Exception
                Dim excp As New clsException(ex)
            End Try

            'FLOW 11543918 - CONFITEC - EDUARDO DE OLIVEIRA - FIM

            '**********************************************************************

        ElseIf RamoApolice1_Estipulante2 = 1 Then

            Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
            Dim dr As Data.SqlClient.SqlDataReader = Nothing

            Me.painelEstipulante.Visible = False

            Dim apol As String = Me.txtApolice.Text.Trim
            Try
                If apol.Length = 0 Then
                    apol = "null"
                Else
                    apol = Int32.Parse(apol)
                End If
            Catch ex As Exception
                apol = "null"
            End Try

            Dim ram As String = Me.txtRamo.SelectedValue
            Try
                If ram.Length = 0 Then
                    ram = "null"
                Else
                    ram = Int32.Parse(ram)
                End If
            Catch ex As Exception
                ram = "null"
            End Try

            BD.SEGS6581_SPS(Session("cpf"), "null", apol, ram, "null")

            Try
                dr = BD.ExecutaSQL_DR()

                If Not dr Is Nothing Then
                    If dr.HasRows Then
                        If dr.Read Then
                            Session("ramo") = dr.Item("ramo_id").ToString
                            Session("apolice") = dr.Item("apolice_id").ToString
                            Session("subgrupo_id") = ""
                            Session("estipulante") = "---"
                            pnlSubgrupoApolice.Visible = True
                            pnlSubgruposOpcoes.Visible = False
                        End If

                        Dim count As Int16 = 1
                        Dim retornou As Boolean = False

                        Try
                            Me.ddlEstipulante.Items.Clear()

                            Dim text As String = ""
                            Dim value As String = ""

                            dr = BD.ExecutaSQL_DR()

                            If dr.HasRows Then

                                ddlEstipulante.Items.Add("Selecione Estipulante (Ramo - Ap�lice)")

                                While dr.Read

                                    If Not IsDBNull(dr.Item("nome")) Then
                                        text = dr.Item("nome").ToString & " (" & dr.Item("ramo_id").ToString & "-" & dr.Item("apolice_id").ToString & ")"
                                        value = dr.Item("ramo_id").ToString & "-" & dr.Item("apolice_id").ToString & "-" & dr.Item("nome").ToString

                                        ddlEstipulante.Items.Add(text)
                                        ddlEstipulante.Items(count).Value = value

                                        If value = "" Then
                                            ddlEstipulante.SelectedIndex = count
                                        End If

                                        retornou = True
                                        count = count + 1
                                    Else
                                        retornou = False
                                    End If
                                End While

                                If retornou = True Then
                                    painelEstipulante.Visible = True
                                Else
                                    lblSemApolice.Text = "Nenhum estipulante encontrado."
                                    lblSemApolice.Visible = True
                                    painelEstipulante.Visible = False
                                End If
                            Else
                                lblSemApolice.Text = "Nenhum estipulante encontrado."
                                lblSemApolice.Visible = True
                            End If

                        Catch ex As Exception
                            Dim v_sCodigoErro As String = "10 - BuscaApolice"
                            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato com oadministrador do sistema \nC�digo: " & v_sCodigoErro, ex)
                        End Try

                    Else
                        Session("subgrupo_id") = ""
                        Session("ramo") = ""
                        Session("apolice") = ""
                        Session("estipulante") = ""
                        pnlSubgrupoApolice.Visible = True
                        pnlSubgruposOpcoes.Visible = False

                        lblSemApolice.Text = "Nenhuma ap�lice encontrada."
                        lblSemApolice.Visible = True
                    End If

                    If Not dr.IsClosed Then dr.Close()
                    dr = Nothing
                End If
            Catch ex As Exception
                Dim v_sCodigoErro As String = "10 - BuscaApolice"
                Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato com oadministrador do sistema \nC�digo: " & v_sCodigoErro, ex)
            End Try

            '********************************************************

        ElseIf RamoApolice1_Estipulante2 = 2 Then

            'Busca estipulante e preenche dropdowlist
            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
            Dim dr As Data.SqlClient.SqlDataReader = Nothing

            bd.SEGS6581_SPS(Session("cpf"), Me.txtEstipulante.Text.Trim)

            Try
                Me.ddlEstipulante.Items.Clear()

                Dim text As String = ""
                Dim value As String = ""
                Dim count As Int16 = 1
                Dim retornou As Boolean = False

                dr = bd.ExecutaSQL_DR()

                If Not dr Is Nothing Then

                    If dr.HasRows Then

                        ddlEstipulante.Items.Add("Selecione o Ramo - Ap�lice")

                        While dr.Read

                            If Not IsDBNull(dr.Item("nome")) Then
                                text = dr.Item("nome").ToString & " (" & dr.Item("ramo_id").ToString & "-" & dr.Item("apolice_id").ToString & ")"
                                value = dr.Item("ramo_id").ToString & "-" & dr.Item("apolice_id").ToString & "-" & dr.Item("nome").ToString

                                ddlEstipulante.Items.Add(text)
                                ddlEstipulante.Items(count).Value = value

                                If value = "" Then
                                    ddlEstipulante.SelectedIndex = count
                                End If

                                retornou = True
                                count = count + 1
                            Else
                                retornou = False
                            End If
                        End While

                        If retornou = True Then
                            painelEstipulante.Visible = True
                        Else
                            lblSemApolice.Text = "Nenhum estipulante encontrado."
                            lblSemApolice.Visible = True
                            painelEstipulante.Visible = False
                        End If
                    Else
                        lblSemApolice.Text = "Nenhum estipulante encontrado."
                        lblSemApolice.Visible = True
                    End If
                    If Not dr.IsClosed Then dr.Close()
                    dr = Nothing
                End If
            Catch ex As Exception
                Dim v_sCodigoErro As String = "10 - BuscaApolice"
                Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato com oadministrador do sistema \nC�digo: " & v_sCodigoErro, ex)
            End Try

            Session("subgrupo_id") = ""
            Session("ramo") = ""
            Session("apolice") = ""
            Session("estipulante") = "" '?

            pnlSubgrupoApolice.Visible = True
            pnlSubgruposOpcoes.Visible = False

        ElseIf RamoApolice1_Estipulante2 = 3 Then

            'Busca estipulante e preenche dropdowlist
            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
            Dim dr As Data.SqlClient.SqlDataReader = Nothing

            bd.SEGS6929_SPS(6785, 0, Session("cpf"), Me.txtCNPJ.Text.Trim, 0)

            Try

                Me.ddlEstipulante.Items.Clear()

                Dim text As String = ""
                Dim value As String = ""
                Dim count As Int16 = 1
                Dim retornou As Boolean = False

                dr = bd.ExecutaSQL_DR()

                If Not dr Is Nothing Then

                    If dr.HasRows Then

                        ddlEstipulante.Items.Add("Selecione Estipulante (Ramo - Ap�lice)")

                        While dr.Read

                            If Not IsDBNull(dr.Item("nome")) Then
                                text = dr.Item("nome").ToString & " (" & dr.Item("ramo_id").ToString & "-" & dr.Item("apolice_id").ToString & ")"
                                value = dr.Item("ramo_id").ToString & "-" & dr.Item("apolice_id").ToString & "-" & dr.Item("nome").ToString

                                ddlEstipulante.Items.Add(text)
                                ddlEstipulante.Items(count).Value = value

                                If value = "" Then
                                    ddlEstipulante.SelectedIndex = count
                                End If

                                retornou = True
                                count = count + 1
                            Else
                                retornou = False
                            End If
                        End While

                        If retornou = True Then
                            painelEstipulante.Visible = True
                        Else
                            lblSemApolice.Text = "Nenhum estipulante encontrado."
                            lblSemApolice.Visible = True
                            painelEstipulante.Visible = False
                        End If
                    Else
                        lblSemApolice.Text = "Nenhum estipulante encontrado."
                        lblSemApolice.Visible = True
                    End If
                    If Not dr.IsClosed Then dr.Close()
                    dr = Nothing
                End If
            Catch ex As Exception
                Dim v_sCodigoErro As String = "10 - BuscaApolice"
                Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato com oadministrador do sistema \nC�digo: " & v_sCodigoErro, ex)
            End Try

            Session("subgrupo_id") = ""
            Session("ramo") = ""
            Session("apolice") = ""
            Session("estipulante") = "" '?

            pnlSubgrupoApolice.Visible = True
            pnlSubgruposOpcoes.Visible = False

        End If

    End Sub

    Sub ConsultaAcesso(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal cpf As String)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        bd.consulta_subgrupo_sps(apolice_id, ramo_id, cpf)

        Try
            dr = bd.ExecutaSQL_DR()

            If Not dr Is Nothing Then
                If dr.Read() Then
                    Session("Acesso") = dr.GetValue(2).ToString()
                End If
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If
        Catch ex As Exception
            Dim v_sCodigoErro As String = "12 - BuscaApolice"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato com oadministrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        End Try
    End Sub

    Public Function ListarPermissoesSituacao(ByVal p_iIndSituacao As String, ByVal p_iIndAcesso As Integer) As Data.DataTable
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS7471_SPS(CInt(Session("apolice")), CInt(Session("ramo")), CInt(Session("subgrupo_id")), "A", 7, Session("cpf"))
        Return bd.ExecutaSQL_DT()
    End Function

    'Private Function ConsultaAcessoTecnico(ByVal apolice_id As String, ByVal ramo_id As String, ByVal cpf As String) As Boolean
    '    Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
    '    Dim dr As Data.SqlClient.SqlDataReader = Nothing
    '    Dim bResultado As Boolean

    '    bd.ConsultarNivelTecnico(apolice_id, ramo_id, cpf)

    '    bResultado = False
    '    Try
    '        dr = bd.ExecutaSQL_DR()
    '        If Not dr Is Nothing Then
    '            If dr.Read() Then
    '                bResultado = True
    '            End If
    '            If Not dr.IsClosed Then dr.Close()
    '            dr = Nothing
    '        End If
    '    Catch ex As Exception
    '        If Not dr.IsClosed Then dr.Close()
    '        dr = Nothing
    '    End Try

    '    Return bResultado
    'End Function

#End Region

End Class
