<%@ Page Language="vb" AutoEventWireup="false" Codebehind="popMsg.aspx.vb" Inherits="segw0060.popMsg"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Leitura de Mensagem</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0">
		<style>
			BODY { FONT-SIZE: 9pt; MARGIN: 2px; FONT-FAMILY: arial }
			TD { FONT-SIZE: 9pt; MARGIN: 2px; FONT-FAMILY: arial }
			</style>
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<table cellspacing="0" cellpadding="0" width="100%" style="BORDER-RIGHT:#666 1px solid; PADDING-RIGHT:3px; BORDER-TOP:#666 1px solid; PADDING-LEFT:3px; PADDING-BOTTOM:3px; BORDER-LEFT:#666 1px solid; PADDING-TOP:3px; BORDER-BOTTOM:#666 1px solid"
				height="100%">
				<tr>
					<td bgcolor="#003399" height="20" align="center"><b>
							<asp:Label id="lblTitulo" runat="server" BackColor="Transparent" ForeColor="White"></asp:Label></b></td>
				</tr>
				<tr>
					<td valign="top">
						<asp:Label ID="lblMsg" Runat="server"></asp:Label>&nbsp;<br />
                        <br />
                        &nbsp;
                        <!--| johnny.barros(Confitec) Demanda:16035497 - 25/02/2013 [inicio] |-->
                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="X-Small" Text="Vig�ncia:"></asp:Label>&nbsp;
                        <asp:Label ID="lblVigencia" runat="server" Font-Bold="True" Font-Size="X-Small" Text="00/00/0000 at� 00/00/0000"
                            Width="170px"></asp:Label>
                        <!--| johnny.barros(Confitec) Demanda:16035497 - 25/02/2013 [fim] |-->
					</td>
				</tr>
                <tr>
                    <td valign="top">
                    </td>
                </tr>
			</table>
		</form>
	</body>
</HTML>
