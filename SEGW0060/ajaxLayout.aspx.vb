Partial Class ajaxLayout
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        Dim op As String = Undefned(Request.Form("op"))

        If op = "layout" Then
            RetornaLayout()
        ElseIf op = "relAnterior" Then
            RelAnterior()
        ElseIf op = "encerramento" Then
            Encerramento()
        End If

    End Sub

    Function Undefned(ByVal value As String) As String
        If value = "undefined" Then
            Return ""
        End If
        Return value
    End Function

    'Sub Encerramento()
    '    If ValidaEncerramento() Then
    '        If MovimentacaoValida() Then
    '            Response.Write("0|")
    '        Else
    '            Response.Write("2|")
    '        End If
    '    Else
    '        Response.Write("1|")
    '    End If
    'End Sub
    Sub Encerramento()
        If ValidaEncerramento() Then
            If MovimentacaoValida() Then
                If EncerramentoInibido() Then    'incluir esta linha
                    Response.Write("3|")     'incluir esta linha
                Else                 'incluir esta linha
                    Response.Write("0|")
                End If               'incluir esta linha
            Else
                Response.Write("2|")
            End If
        Else
            Response.Write("1|")
        End If
    End Sub

    Private Function EncerramentoInibido(Optional ByVal cOperacao As String = "T") As Boolean
        Dim cValor As Boolean = False

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS7705_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        Try
            dr = bd.ExecutaSQL_DR()
            If dr.Read() Then
                cValor = True
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return cValor
    End Function

    Sub RetornaLayout()
        'Autor: breno.nery (Nova Consultoria) - Data da Altera��o: 12/01/2012
        'Demanda: 12784293 - Item: 10 - Permitir movimenta��o por arquivo.

        If Not ExisteLayout(Session("apolice"), Session("ramo"), Session("subgrupo_id")) Then
            Response.Write("0|")
        ElseIf ExisteMovimentacaoVidas(Session("apolice"), Session("ramo"), Session("subgrupo_id")) Then
            Response.Write("2|")
        Else
            Response.Write("1|")
        End If
    End Sub

    Private Function ExisteLayout(ByVal apolice As Int32, ByVal ramo As Int16, ByVal subgrupo As Int16) As Boolean

        'Quando existe movimenta��o nas vidas, retorna TRUE, quando n�o existe, retorna FALSE

        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader
        Dim retorno As Boolean = False


        BD.getLayoutArquivo(apolice, ramo, subgrupo)

        Try
            dr = BD.ExecutaSQL_DR

            If dr.Read() Then
                retorno = True
                'cUtilitarios.br("existe")
            Else
                retorno = False
                'cUtilitarios.br("n�o existe")
            End If

            dr.Close()
            dr = Nothing

            Return retorno

        Catch ex As Exception
            'cUtilitarios.br(ex.Message)
            Dim excp As New clsException(ex)
            Return retorno
        End Try

    End Function

    Private Function ExisteMovimentacaoVidas(ByVal apolice As Int32, ByVal ramo As Int16, ByVal subgrupo As Int16) As Boolean

        'Quando existe movimenta��o nas vidas, retorna TRUE, quando n�o existe, retorna FALSE

        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader
        Dim retorno As Boolean = False


        BD.VerificaMovimentacao(apolice, ramo, subgrupo, 1)

        Try
            dr = BD.ExecutaSQL_DR

            If dr.Read() Then
                retorno = True
                'cUtilitarios.br("existe")
            Else
                retorno = False
                'cUtilitarios.br("n�o existe")
            End If

            dr.Close()
            dr = Nothing

            Return retorno

        Catch ex As Exception
            'cUtilitarios.br(ex.Message)
            Dim excp As New clsException(ex)
            Return retorno
        End Try

    End Function

    Sub RelAnterior()
        If Not Me.isAtividadeAberta(Me.getWfID(), 2) Then
            Response.Write("0|")
        Else

            ' Projeto 539202 - Jo�o Ribeiro - 27/04/2009
            ' If Session("acesso") = "4" Or Session("acesso") = "1" Then
            If Session("acesso") = "4" Or Session("acesso") = "1" Or Session("acesso") = "6" Or Session("acesso") = "7" Then
                Response.Write("1|")
            Else
                Response.Write("2|")
            End If
        End If
    End Sub

    Public Function isAtividadeAberta(ByVal wf_id As String, ByVal atividade As String) As Boolean
        Try
            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
            bd.getEstadoAtividade(wf_id, atividade)
            'Response.Write(bd.SQL)
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read Then
                If dr.GetValue(0).ToString = "1" Then
                    Return True
                End If
            End If
            dr.Close()
            dr = Nothing

        Catch ex As Exception
            Dim excp As New clsException(ex)
            Return False
        End Try

        Return False
    End Function

    Public Function getWfID() As String
        If CStr(Session("subgrupo_id")) <> "" Then       'Ao selecionar uma Ap�lice/Subgrupo

            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
            bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

            Try
                Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

                If dr.Read() Then
                    Return dr.GetValue(0).ToString()
                End If
                dr.Close()
                dr = Nothing
                Return "0"
            Catch ex As Exception
                Dim excp As New clsException(ex)
                Return ""
            End Try

        Else
            Return "0"
        End If
    End Function

    Private Function ValidaEncerramento() As Boolean
        If Not Me.isAtividadeAberta(Me.getWfID(), 2) Then
            Return True
        Else

            ' Projeto 539202 - Jo�o Ribeiro - 27/04/2009
            'If Session("acesso") = "3" Or Session("acesso") = "3" Or Session("acesso") = "1" Then
            If Session("acesso") = "3" Or Session("acesso") = "6" Or Session("acesso") = "1" Or Session("acesso") = "7" Then
                Return True
            Else
                Return False
            End If
        End If
    End Function

    Private Function MovimentacaoValida() As Boolean
        Dim bValido As Boolean = False

        If ValorFatura() > 0 Then
            bValido = True
        End If

        Return bValido

    End Function

    Private Function ValorFatura(Optional ByVal cOperacao As String = "T") As Double
        Dim cValor As Double = 0

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.ResumoFaturaAtual_IAE(Session("apolice"), Session("ramo"), Session("subgrupo_id"), "", 0)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        Try
            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                While dr.Read()
                    If Trim(dr.GetValue(3).ToString()) = Trim(cOperacao) Then
                        cValor = CDbl(dr.GetValue(2).ToString())
                    End If
                End While
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return cValor
    End Function
End Class
