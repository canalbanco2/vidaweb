<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AcessoSubGrupos_Form.aspx.vb" Inherits="segw0060.AcessoSubGrupos_Form"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AcessoSubGrupos_Form</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
	</HEAD>
	<script>
	function insereUsuario() {
		document.Form1.getAcao.value="1";
		document.Form1.submit();
	}
	function excluirUsuario() {
		document.Form1.getAcao.value="2";
		document.Form1.submit();
	}
	</script>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
		<input type="hidden" value="" name="getAcao">
			<P>
				<asp:Panel id="pnlSubGrupos" runat="server" DESIGNTIMEDRAGDROP="44" Width="99%">
					<TABLE id="Table1" cellSpacing="0" cellPadding="2" width="100%" border="0">
						<TR>
							<TD align="left" colSpan="2" height="19">
								<asp:Label id="lblTituloSuBgrupo" runat="server" CssClass="Caminhotela">:: Associar usu�rio ao subgrupo</asp:Label></TD>
						</TR>
						<TR>
							<TD align="right" width="160" height="30">Subgrupo:</TD>
							<TD height="30">
								<asp:DropDownList id="ddlSubGrupo" runat="server">
								</asp:DropDownList></TD>
						</TR>
						<TR>
							<TD vAlign="top" align="right" width="160">Usu�rio:</TD>
							<TD>
								<asp:DropDownList id="ddlUsuarios" runat="server">
								</asp:DropDownList>
								<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" Width="8px" ErrorMessage="*" ControlToValidate="ddlUsuarios"
									Height="16px"></asp:RequiredFieldValidator></TD>
						</TR>
						<TR>
							<TD vAlign="top" align="center" colSpan="2"><BR>
								<asp:Button id="btnIncluirSubgrupo" runat="server" Width="56px" CssClass="Botao" Text="Incluir"></asp:Button>
								<asp:Button id="btnExcluir" runat="server" Width="56px" CssClass="Botao" Text="Excluir"></asp:Button>
								<asp:Button id="btnCancelar" runat="server" Width="67px" CssClass="Botao" Text="Cancelar"></asp:Button></TD>
						</TR>
					</TABLE>
				</asp:Panel></P>
		</form>
	</body>
</HTML>
