﻿Imports System.Collections.Generic

Partial Public Class ConsultarAssistencia
    Inherits System.Web.UI.Page

    Dim data_inicio, data_fim, retorno As String
    Public v_sCodigoErro As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblNavegacao.Text = "Ramo/Apólice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomesubGrupo") & "<br>Função: Consulta de Assistência "
        lblVigencia.Text = "<br>" & GetPeriodoCompetenciaSession()

        Dim cAmbiente As Alianca.Seguranca.Web.ControleAmbiente = GetAmbiente()
        Dim controller As cAssistenciaController = GetAssistenciaController()

        If Not Me.IsPostBack Then
            GetDataInicioVigencia(controller)
            ObterListaPlanos(controller, cAmbiente)
        End If

        If Not ContemItensNaCombo() Then
            Me.panelConteudoAssistencia.Visible = False
            Me.panelNaoExistemAssistencias.Visible = True
        Else
            btnImprimir.NavigateUrl = "ImprimeAssitencia.aspx?PlanoId=" & GetPlanoSelecionado()
            controller.ObterTextoAssistencia(GetPlanoSelecionado(), Me.txtAssistencia)
        End If

    End Sub

    Private Function ContemItensNaCombo() As Boolean
        Return Me.ddlAssistencia.Items.Count <> 0
    End Function

    Private Function GetAmbiente() As Alianca.Seguranca.Web.ControleAmbiente
        Dim cAmbiente As Alianca.Seguranca.Web.ControleAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        Dim url As String = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
        cAmbiente.ObterAmbiente(url)
        Return cAmbiente
    End Function

    Private Function GetPlanoSelecionado() As Object
        Return Me.ddlAssistencia.SelectedValue
    End Function

    Private Function GetAssistenciaController() As cAssistenciaController
        Dim controller As cAssistenciaController = Session("cAssistenciaController")
        If controller Is Nothing Then
            controller = New cAssistenciaController
            Session("cAssistenciaController") = controller
        End If
        Return controller
    End Function

    Private Sub GetDataInicioVigencia(ByVal oAssistenciaController As cAssistenciaController)
        If Session("data_inicio_vigencia") Is Nothing Then
            Session("data_inicio_vigencia") = oAssistenciaController.GetDataInicioVigencia(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
        End If
    End Sub

    Private Sub ObterListaPlanos(ByVal oAssistenciaController As cAssistenciaController, ByVal cAmbiente As Alianca.Seguranca.Web.ControleAmbiente)
        Try

            v_sCodigoErro = "1 - ObterListaPlanos/ConsultarAssistencia"
            oAssistenciaController.ObterListaPlanos(Convert.ToInt32(Session("apolice")), Convert.ToInt32(Session("ramo")), 0, 6785, Convert.ToDateTime(Session("data_inicio_vigencia")), Convert.ToInt32(Session("subgrupo_id")), Date.Now, cAmbiente.Ambiente, Me.ddlAssistencia)
            v_sCodigoErro = "2 - ObterListaPlanos/ConsultarAssistencia"
        Catch ex As Exception
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nCódigo: " & v_sCodigoErro, ex)
        End Try
    End Sub


    Private Function GetPeriodoCompetenciaSession() As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)
        Try

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.HasRows Then
                While dr.Read()

                    data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                    data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)
                    cUtilitarios.escreveScript("var dt_ini_comp = '" & data_inicio & "';")
                    cUtilitarios.escreveScript("var dt_fim_comp = '" & data_fim & "';")

                End While

                retorno = "Período de Competência: " & data_inicio & " a " & data_fim
            Else
                Session.Abandon()
                Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
            End If
            dr.Close()
            dr = Nothing
        Catch ex As Exception
            Dim v_sCodigoErro As String = "3 - GetPeriodoCompetenciaSession"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nCódigo: " & v_sCodigoErro, ex)
            retorno = ""
        End Try
        Return retorno
    End Function


End Class