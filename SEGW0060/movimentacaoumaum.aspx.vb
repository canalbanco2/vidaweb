Public Class MovimentacaoUmaUm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents pnlDependentes As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlAcoes As System.Web.UI.WebControls.Panel
    Protected WithEvents Panel1 As System.Web.UI.WebControls.Panel
    Protected WithEvents lblNavegacao As System.Web.UI.WebControls.Label
    Protected WithEvents lblVigencia As System.Web.UI.WebControls.Label
    Protected WithEvents lblListagem As System.Web.UI.WebControls.Label
    Protected WithEvents DropDownList2 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtFiltro As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnFiltrar As System.Web.UI.WebControls.Button
    Protected WithEvents lblFiltro As System.Web.UI.WebControls.Label
    Protected WithEvents btnIncluir As System.Web.UI.WebControls.Button
    Protected WithEvents btnAlterar As System.Web.UI.WebControls.Button
    Protected WithEvents btnDesfazAlteracoes As System.Web.UI.WebControls.Button
    Protected WithEvents btnFechaPagina As System.Web.UI.WebControls.Button
    Protected WithEvents btnIncluiDependente As System.Web.UI.WebControls.Button
    Protected WithEvents btnExcluir As System.Web.UI.WebControls.Button
    Protected WithEvents Label17 As System.Web.UI.WebControls.Label
    Protected WithEvents pnlDependente As System.Web.UI.WebControls.Panel
    Protected WithEvents btnLuis As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lblAcao As System.Web.UI.WebControls.Label
    Protected WithEvents imgPlus As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblInclusao2 As System.Web.UI.WebControls.Label
    Protected WithEvents Panel2 As System.Web.UI.WebControls.Panel
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents DropdownSelComponente As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnIncluir.Attributes.Add("onclick", "return GB_show('Movimentação on-line', '../../MovimentacaoUmaUm_Form.aspx', 250, 450)")
        btnAlterar.Attributes.Add("onclick", "return GB_show('Movimentação on-line', '../../MovimentacaoUmaUm_Form.aspx?acao=alterar', 250, 450)")
        btnIncluiDependente.Attributes.Add("onclick", "return GB_show('Movimentação on-line', '../../MovimentacaoUmaUm_Form.aspx?acao=incluirdependente', 250, 450)")
        btnExcluir.Attributes.Add("onclick", "return GB_show('Movimentação on-line', '../../MovimentacaoUmaUm_Form.aspx?acao=excluir', 250, 450)")
        'Antiga opção do msgbox -> btnExcluir.Attributes.Add("onclick", "return confirmaExclusao('true')")

        Dim filtraPesquisa As String = Request.QueryString("acao")

        'Response.Write(filtraPesquisa)

        If filtraPesquisa = "incl" Or filtraPesquisa = "excl" Or filtraPesquisa = "alter" Or filtraPesquisa = "acer" Or filtraPesquisa = "dps" Then
            Dim valor As String
            If filtraPesquisa = "incl" Then
                valor = "Inclusões"
                DropDownList2.SelectedValue = "Inclusoes"
            ElseIf filtraPesquisa = "excl" Then
                valor = "Exclusões"
                DropDownList2.SelectedValue = "Exclusoes"
            ElseIf filtraPesquisa = "alter" Then
                valor = "Alterações"
                DropDownList2.SelectedValue = "Alteracoes"
            ElseIf filtraPesquisa = "acer" Then
                valor = "Acertos"
                DropDownList2.SelectedValue = "Acertos"
            Else
                valor = "DPS Pendente"
                DropDownList2.SelectedValue = "DPS"
            End If

            lblFiltro.Text = "<div align='left'>Pesquisado por: " & valor & "</div>"
            lblFiltro.Visible = True
        End If

        btnDesfazAlteracoes.Attributes.Add("onclick", "return confirmaDesfazer()")

        lblAcao.Text = "&nbsp;"
        lblNavegacao.Text = "Ramo/Apólice: (" & Session("ramo") & ") " & Session("apolice") & " -> Subgrupo: " & Session("subGrupo") & " -> " & "Movimentação on-line"
        Dim excluido = Request.QueryString("excluido")

        If Not Page.IsPostBack Then
            Dim excluir As String = Request.QueryString("excluir")

            Dim expandir As String = Request.QueryString("expandir")

            If expandir = 1 Then
                imgPlus.ImageUrl = "Images/imgMenos.gif"
                pnlDependente.Visible = True
            Else
                imgPlus.ImageUrl = "images/imgmais.gif"
                pnlDependente.Visible = False
            End If

        End If
    End Sub


    Private Sub btnIncluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIncluir.Click



    End Sub

    Private Sub btnDesfazAlteracoes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("MovimentacaoUmaUm.aspx")
    End Sub

    Private Sub txtFiltro_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub DropDownList2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DropDownList2.SelectedIndexChanged
        If DropDownList2.SelectedValue = "Todos" Then
            txtFiltro.Visible = False
            lblFiltro.Visible = False
        ElseIf DropDownList2.SelectedValue = "Inclusoes" Or DropDownList2.SelectedValue = "Exclusoes" Or DropDownList2.SelectedValue = "Alteracoes" Then
            lblFiltro.Visible = False
            txtFiltro.Visible = False
        Else
            lblFiltro.Visible = False
            txtFiltro.Text = ""
            txtFiltro.Visible = True
        End If
    End Sub

    Private Sub btnFiltrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFiltrar.Click

        Dim complemento As String
        Dim situacao As Boolean = True

        If DropDownList2.SelectedValue <> "Todos" And DropDownList2.SelectedValue <> "Inclusoes" And DropDownList2.SelectedValue <> "Exclusoes" And DropDownList2.SelectedValue <> "Alteracoes" Then
            complemento = " (" & txtFiltro.Text & ")"
            If txtFiltro.Text.Length() < 3 Then
                Response.Write("<script>alert('A pesquisa deve ser realizada com no mínimo 3 caracteres.');</script>")
                situacao = False
            End If
        End If
        If situacao = True Then
            lblFiltro.Text = "<div align='left'>Pesquisado por: " & DropDownList2.SelectedValue & complemento & "</div>"
            lblFiltro.Visible = True
        End If
    End Sub

    Private Sub btnFechaPagina_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("Centro.aspx")
    End Sub

    Private Sub btnAlterar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAlterar.Click

    End Sub

    Private Sub btnExcluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcluir.Click

    End Sub


    Private Sub btnLuis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLuis.Click

        btnIncluiDependente.Visible = True

        btnAlterar.Visible = True
        btnIncluir.Visible = True
        btnExcluir.Visible = True
        'btnDesfazerAlteracao.Visible = False
        'pnlCadastro.Visible = True
        'pnlCadastroDependente.Visible = True
        'ddlCadastro.SelectedValue = "Dependente"
        'btnIncluir.Visible = False
        'pnlCadastroTitular.Visible = False

        'txtDepNome.Text = "Gustavo Modena"
        'txtDepNascimento.Text = "xx/xx/xxxx"
        'txtDepCPF.Text = "xxx.xxx.xxx-xx"
        'ddlDepSexo.SelectedValue = "masc."
        'ddlDepTipo.SelectedValue = "Filho"


    End Sub

    Private Sub imgPlus_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPlus.Click
        If pnlDependente.Visible = True Then

            imgPlus.ImageUrl = "Images/imgMais.gif"
            pnlDependente.Visible = False
        Else
            imgPlus.ImageUrl = "Images/imgMenos.gif"
            pnlDependente.Visible = True
        End If
    End Sub
End Class
