Imports System.Web.Services
Imports System.Web.HttpContext

<System.Web.Services.WebService(Namespace:="http://tempuri.org/segw0060/listCNPJ")> _
Public Class wsSEGW0060
    Inherits System.Web.Services.WebService

#Region " Web Services Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Web Services Designer.
        InitializeComponent()

        'Add your own initialization code after the InitializeComponent() call

    End Sub

    'Required by the Web Services Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Web Services Designer
    'It can be modified using the Web Services Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        'CODEGEN: This procedure is required by the Web Services Designer
        'Do not modify it using the code editor.
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region

    <WebMethod()> _
    Public Function listCNPJ() As String

        If Not Me.checkLogin() Then
            Return ""
        End If
        
        Current.Response.Write("<ul><li>teste</li></ul>")
        Return ""

    End Function

    <WebMethod()> _
    Public Function checkLogin() As Boolean

        Try
            If Session("usuario_id").ToString = "" Then                
                Return False
            End If
        Catch ex As Exception            
            Return False
        End Try

        Return True

    End Function

End Class
