<%@ Page Language="vb" AutoEventWireup="false" Codebehind="resumoFatura.aspx.vb" Inherits="segw0060.resumoFatura" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>resumoFatura</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0">
    <link href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
    <link href="/seg/segw0069/ASP/estilo_internet.css" type="text/css" rel="stylesheet">

    <script src="scripts/box/AJS.js" type="text/javascript"></script>

    <script src="scripts/box/box.js" type="text/javascript"></script>

    <script type="text/javascript" src="scripts/overlib421/overlib.js"></script>

    <script>
				function popup(numero){
				var tamW = (screen.width/2)-200;
				var tamH = (screen.height/2)-200;
				//1
				window.open('ajudaF.aspx','',"resizable=1,scrollbars=1,width=400,height=450,top="+tamH+",left="+tamW+",toolbar=0,location=0,directories=0,status=0,menubar=0");
				return false
			}
    </script>

</head>
<body ms_positioning="FlowLayout">
    <form id="Form1" method="post" runat="server" onsubmit="return false;">
        <asp:Panel ID="pnlResumo" Height="200px" runat="server">
            <table style="margin-top: 10px" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblNavegacao" runat="server" Visible="False" CssClass="Caminhotela">Label</asp:Label><br>
                        <asp:Label ID="lblVigencia" runat="server" CssClass="Caminhotela"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <table style="margin-top: 5px" height="20" cellspacing="0" cellpadding="0" width="455"
                            border="0">
                            <tr>
                                <td width="7" background="Images/imgTopoMenuMeio.gif" height="20">
                                    <img ismap src="Images/imgTopoMenuCanto.gif"></td>
                                <td class="font_verdana_10_preto_bold" valign="middle" align="left" width="80%" background="Images/imgTopoMenuMeio.gif"


                                    height="20">
                                    <strong>:: Resumo da fatura atual ::</strong></td>
                                <td class="font_verdana_10_preto_bold" valign="middle" align="right" width="17%"
                                    background="Images/imgTopoMenuMeio.gif" height="20">
                                    <asp:ImageButton ID="Imagebutton2" onmouseover="return overlib('Clique aqui para obter ajuda deste menu.');"
                                        onmouseout="return nd();" runat="server" ImageUrl="Images/help_azul.gif" ImageAlign="right">
                                    </asp:ImageButton></td>
                                <td nowrap align="right" width="20" background="Images/imgTopoMenuMeio.gif" height="20">
                                    <img src="Images/imgTopoMenuCurva.gif" border="0"></td>
                            </tr>
                        </table>
                        <div class="divPopup" id="relatorio">
                            <table class="CamposResumo" id="Table5" style="border-right: #d4c601 1px solid; border-bottom: #d4c601 1px solid"
                                cellspacing="2" cellpadding="0" width="450" bgcolor="#e4e4e4" border="0">
                                <!--240-560-->
                                <tr>
                                    <td width="25%">
                                        &nbsp;</td>
                                    <td align="center" width="10%">
                                        Vidas</td>
                                    <td nowrap align="center" width="40%">
                                        Capital
                                        <asp:Label ID="Label1" runat="server" Font-Size="Larger">(R$)</asp:Label></td>
                                    <td style="display:none;" id="premio1" nowrap align="center" width="25%">
                                        Pr�mio
                                        <asp:Label ID="Label2" runat="server" Font-Size="Larger">(R$)</asp:Label></td>
                                    <td style="display:none;" align="center" nowrap="nowrap" width="25%">
                                        IOF
                                        <asp:Label ID="Label3" runat="server" Font-Size="Larger">(R$)</asp:Label></td>
                                    <td align="center" nowrap="nowrap" width="25%">
                                        Pr�mio Bruto
                                        <asp:Label ID="Label4" runat="server" Font-Size="Larger">(R$)</asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="quadro-resumo-borda" nowrap align="left" width="25%">
                                        Fatura Anterior</td>
                                    <td class="quadro-resumo-borda" align="center" width="10%">
                                        <div id="divVidasFA">
                                            <asp:Label ID="lblVidasFA" runat="server"></asp:Label></div>
                                    </td>
                                    <td class="quadro-resumo-borda" align="right" width="40%">
                                        <div id="divCapitalFA">
                                            <asp:Label ID="lblCapitalFA" runat="server"></asp:Label></div>
                                    </td>
                                    <td style="display:none;" class="quadro-resumo-borda" id="premio2" align="right" width="25%">
                                        <div id="divPremioFA">
                                            <asp:Label ID="lblPremioFA" runat="server"></asp:Label></div>
                                    </td>
                                    <td style="display:none;" align="right" class="quadro-resumo-borda" width="25%">
                                        <asp:Label ID="lblIOFA" runat="server"></asp:Label></td>
                                    <td align="right" class="quadro-resumo-borda" width="25%">
                                        <asp:Label ID="lblVlPremioBrutoA" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" width="25%">
                                        <asp:HyperLink ID="btnInclusoes" runat="server" CssClass="hand2" Target="IFrameConteudoPrincipal">Inclus�es</asp:HyperLink></td>
                                    <td align="center" width="10%">
                                        <div id="divInclusoes_vidas">
                                            <asp:Label ID="lblInclusoes_vidas" runat="server"></asp:Label></div>
                                    </td>
                                    <td align="right" width="40%">
                                        <div id="divInclusoes_capital">
                                            <asp:Label ID="lblInclusoes_capital" runat="server"></asp:Label></div>
                                    </td>
                                    <td style="display:none;" id="premio3" align="right" width="25%">
                                        <div id="divInclusoes_premio">
                                            <asp:Label ID="lblInclusoes_premio" runat="server"></asp:Label></div>
                                    </td>
                                    <td style="display:none;" align="right" width="25%">
                                    </td>
                                    <td align="right" width="25%">
                                        <asp:Label ID="lblVlPremioBrutoI" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" width="25%" style="height: 13px">
                                        <asp:HyperLink ID="btnExclusoes" runat="server" CssClass="hand2" Target="IFrameConteudoPrincipal">Exclus�es</asp:HyperLink></td>
                                    <td align="center" width="10%" bgcolor="#ffffff" style="height: 13px">
                                        <div id="divExclusoes_vidas">
                                            <asp:Label ID="lblExclusoes_vidas" runat="server"></asp:Label></div>
                                    </td>
                                    <td align="right" bgcolor="#ffffff" width="40%" style="height: 13px">
                                        <div id="divExclusoes_capital">
                                            <asp:Label ID="lblExclusoes_capital" runat="server"></asp:Label></div>
                                    </td>
                                    <td style="display:none; height: 13px;" id="premio4" align="right" bgcolor="#ffffff" width="25%">
                                        <div id="divExclusoes_premio">
                                            <asp:Label ID="lblExclusoes_premio" runat="server"></asp:Label></div>
                                    </td>
                                    <td style="display:none; height: 13px;" align="right" bgcolor="#ffffff" width="25%">
                                    </td>
                                    <td align="right" bgcolor="#ffffff" width="25%" style="height: 13px">
                                        <asp:Label ID="lblVlPremioBrutoE" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="quadro-resumo-borda" align="left" width="25%" style="height: 20px">
                                        <asp:HyperLink ID="btnSubTotal" runat="server" Target="IFrameConteudoPrincipal">Subtotal</asp:HyperLink></td>
                                    <td class="quadro-resumo-borda" align="center" width="10%" style="height: 20px">
                                        <div id="divSubTotal_vidas">
                                            <asp:Label ID="lblSubTotal_vidas" runat="server">0,00</asp:Label></div>
                                    </td>
                                    <td class="quadro-resumo-borda" align="right" width="40%" style="height: 20px">
                                        <div id="divSubTotal_capital">
                                            <asp:Label ID="lblSubTotal_capital" runat="server">0,00</asp:Label></div>
                                    </td>
                                    <td style="display:none; height: 20px;" class="quadro-resumo-borda" id="premio4_1" align="right" width="25%">
                                        <div id="divSubTotal_premio">
                                            <asp:Label ID="lblSubTotal_premio" runat="server">0,00</asp:Label></div>
                                    </td>
                                    <td style="display:none; height: 20px;" align="right" class="quadro-resumo-borda" width="25%">
                                    </td>
                                    <td align="right" class="quadro-resumo-borda" width="25%" style="height: 20px">
                                        <asp:Label ID="lblSubTotalPremio" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td nowrap align="left" width="25%">
                                        <asp:HyperLink ID="btnAlteracoes" runat="server" CssClass="hand2" Target="IFrameConteudoPrincipal">Altera&ccedil;�es de Capital</asp:HyperLink></td>
                                    <td align="center" width="10%">
                                        <div id="divAlteracoes_vidas">
                                            <asp:Label ID="lblAlteracoes_vidas" runat="server"></asp:Label></div>
                                    </td>
                                    <td align="right" width="40%">
                                        <div id="divAlteracoes_capital">
                                            <asp:Label ID="lblAlteracoes_capital" runat="server"></asp:Label></div>
                                    </td>
                                    <td style="display:none;" id="premio5" align="right" width="25%">
                                        <div id="divAlteracoes_premio">
                                            <asp:Label ID="lblAlteracoes_premio" runat="server"></asp:Label></div>
                                    </td>
                                    <td style="display:none;" align="right" width="25%">
                                    </td>
                                    <td align="right" width="25%">
                                        <asp:Label ID="lblVlPremioBrutoAlt" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td nowrap align="left" width="25%">
                                        <asp:HyperLink ID="btnAcertos" runat="server" CssClass="hand2" Target="IFrameConteudoPrincipal">Acertos</asp:HyperLink></td>
                                    <td nowrap align="center" width="10%" bgcolor="#ffffff">
                                        <div id="divAcertos_vidas">
                                            <asp:Label ID="lblAcertos_vidas" runat="server"></asp:Label></div>
                                    </td>
                                    <td nowrap align="right" bgcolor="#ffffff" width="40%">
                                        <div id="divAcertos_capital">
                                            <asp:Label ID="lblAcertos_capital" runat="server"></asp:Label></div>
                                    </td>
                                    <td style="display:none;" id="premio6" nowrap align="right" bgcolor="#ffffff" width="25%">
                                        <div id="divAcertos_premio">
                                            <asp:Label ID="lblAcertos_premio" runat="server"></asp:Label></div>
                                    </td>
                                    <td style="display:none;" align="right" bgcolor="#ffffff" nowrap="nowrap" width="25%">
                                    </td>
                                    <td align="right" bgcolor="#ffffff" nowrap="nowrap" width="25%">
                                        <asp:Label ID="lblVlPremioBrutoAc" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="quadro-resumo-borda" align="left" width="25%">
                                        Fatura Atual<sup>*</sup></td>
                                    <td class="quadro-resumo-borda" align="center" width="10%">
                                        <div id="divVidasFT">
                                            <asp:Label ID="lblVidasFT" runat="server"></asp:Label></div>
                                    </td>
                                    <td class="quadro-resumo-borda" align="right" width="40%">
                                        <div id="divCapitalFT">
                                            <asp:Label ID="lblCapitalFT" runat="server"></asp:Label></div>
                                    </td>
                                    <td style="display:none;" class="quadro-resumo-borda" id="premio7" align="right" width="25%">
                                        <div id="divPremioFT">
                                            <asp:Label ID="lblPremioFT" runat="server"></asp:Label></div>
                                    </td>
                                    <td style="display:none;" align="right" class="quadro-resumo-borda" width="25%">
                                        <asp:Label ID="lblIOFFT" runat="server"></asp:Label></td>
                                    <td align="right" class="quadro-resumo-borda" width="25%">
                                        <asp:Label ID="lblPremioBrutoFT" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td nowrap align="left" width="25%">
                                        <asp:HyperLink ID="btnDpsPendente" runat="server" CssClass="hand2" Target="IFrameConteudoPrincipal">Pendente</asp:HyperLink></td>
                                    <td nowrap align="center" width="10%">
                                        <div id="divDPS_vidas">
                                            <asp:Label ID="lblDPS_vidas" runat="server"></asp:Label></div>
                                    </td>
                                    <td nowrap align="right" width="40%">
                                        <div id="divDPS_capital">
                                            <asp:Label ID="lblDPS_capital" runat="server"></asp:Label></div>
                                    </td>
                                    <td style="display:none;" id="premio8" nowrap align="right" width="25%">
                                        <div id="divDPS_premio">
                                            <asp:Label ID="lblDPS_premio" runat="server"></asp:Label></div>
                                    </td>
                                    <td style="display:none;" align="right" nowrap="nowrap" width="25%">
                                    </td>
                                    <td align="right" nowrap="nowrap" width="25%">
                                        <asp:Label ID="lblVlPremioBrutoPend" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td nowrap align="left" width="25%">
                                        <span>
                                            <asp:HyperLink ID="btnExcAgendada" runat="server" CssClass="hand2" Target="IFrameConteudoPrincipal">Exclus�o Agendada</asp:HyperLink>
                                        </span>
                                    </td>
                                    <td nowrap align="center" width="10%">
                                        <div id="divExcAgendada_vidas">
                                            <asp:Label ID="lblExcAgendada_vidas" runat="server" Width="96px"></asp:Label></div>
                                    </td>
                                    <td nowrap align="right" width="40%">
                                        <div id="divExcAgendada_capital">
                                            <asp:Label ID="lblExcAgendada_capital" runat="server"></asp:Label></div>
                                    </td>
                                    <td style="display:none;" id="premio9" nowrap align="right" width="25%">
                                        <div id="divExcAgendada_premio">
                                            <asp:Label ID="lblExcAgendada_premio" runat="server"></asp:Label></div>
                                    </td>
                                    <td style="display:none;" align="right" nowrap="nowrap" width="25%">
                                    </td>
                                    <td align="right" nowrap="nowrap" width="25%">
                                        <asp:Label ID="lblVlPremioBrutoEAg" runat="server"></asp:Label></td>
                                </tr>
                            </table>
                        </div>
                        <div class="divPopup">
                            * Esses valores s�o estimados
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <br>
                        <br>
                        <asp:Button ID="btnVoltar" runat="server" CssClass="Botao" Text="Voltar"></asp:Button></td>
                </tr>
            </table>
            <div>
            </div>
        </asp:Panel>
    </form>

    <script>
			top.escondeaguarde();
					
			if(document.getElementById("ocultaPremio")) {
				i = 1;
				document.getElementById("premio4_1").style.display = 'none';
				while(document.getElementById("premio" + i)) {
					document.getElementById("premio" + i).style.display = 'none';
					i++;
				}
			}	
    </script>

</body>
</html>