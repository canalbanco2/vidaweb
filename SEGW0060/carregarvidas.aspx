<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CarregarVidas.aspx.vb" Inherits="segw0060.CarregarVidas"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ConsultarFaturas</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<P>
				<asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela">Label</asp:label><BR>
				<asp:Label id="lblVigencia" runat="server" CssClass="Caminhotela"><br>COTEMINAS S.A - Per�odo de compet�ncia: 01/12/2006 at� 31/12/2006</asp:Label></P>
			<P><BR>
				<asp:Panel id="pnlAlerta" runat="server" CssClass="fundo-azul-claro" Width="60%" HorizontalAlign="Center"
					Height="70px" Visible="False">
					<BR>
					<asp:Label id="lblListagemEnviada" runat="server" CssClass="Caminhotela"><br>Rela��o de vidas anteriores carregadas com sucesso.</asp:Label>
				</asp:Panel>
				<asp:Panel id="pnlConfirma" runat="server">
					<TABLE class="carregaVida fundo-azul-claro" id="Table2" style="FONT-SIZE: x-small" height="21"
						cellSpacing="1" cellPadding="15" width="496" border="0">
						<TBODY>
							<TR>
								<TD align="center" colSpan="2"><FONT color="navy">
										<P align="left"><BR>
											Esta funcionalidade permite carregar a rela��o de vidas da fatura anterior.</P>
										<P align="left">
										Ao clicar no bot�o "Confirmar" todas as movimenta��es que foram realizadas 
										nesta compet�ncia (01/12/2006 at� 31/12/2006) ser�o descondideradas.</FONT>
			</P>
			<P>
				<asp:Button id="Button1" runat="server" CssClass="Botao" Width="81px" Text="Confirmar"></asp:Button><BR>
				&nbsp;</P>
			</TD></TR></TBODY></TABLE></asp:Panel></P>
		</form>
	</body>
</HTML>
