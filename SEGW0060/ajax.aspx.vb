Partial Class ajax
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'cUtilitarios.escreveScript("top.IFrameConteudoPrincipal.location.href=top.document.getElementById('" & Request("nlink") & "').href;")
        'Response.End()
        Try
            If Session("apolice") = "" Then
                cUtilitarios.escreveScript("parent.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
            Else

                If Request.QueryString.Item("tipo") = 1 Then
                    If Me.isAtividadeAberta(Me.getWfID(), 2) Then
                        'como existem 3 itens do menu que chamam essa funcao de verificar atividade, foi necessario pegar o link para posteriormente
                        'pegar o caminho correto de cada item
                        'cUtilitarios.escreveScript("top.IFrameConteudoPrincipal.location.href=top.document.getElementById('Link5').href;")
                        If Request("nlink") <> "Link6" Then
                            cUtilitarios.escreveScript("top.IFrameConteudoPrincipal.location.href=top.document.getElementById('" & Request("nlink") & "').href;")
                        End If
                        cUtilitarios.escreveScript("top.escondeaguarde();")
                    Else
                        cUtilitarios.br(cUtilitarios.GetDataEncerramento())
                        'cUtilitarios.escreveScript("parent.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
                        cUtilitarios.escreveScript("top.escondeaguarde();")
                    End If

                Else

                    If Me.isAtividadeAberta(Me.getWfID(), 2) Then
                        If Me.ValidaEncerramento = False Then
                            cUtilitarios.br(cUtilitarios.GetDataEncerramento())
                            'cUtilitarios.escreveScript("parent.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
                            cUtilitarios.escreveScript("top.escondeaguarde();")
                        Else
                            cUtilitarios.escreveScript("parent.document.getElementById('Link4').onclick = '';parent.document.getElementById('Link4').click();")
                        End If

                    Else
                        cUtilitarios.br(cUtilitarios.GetDataEncerramento())
                        'cUtilitarios.escreveScript("parent.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
                        cUtilitarios.escreveScript("top.escondeaguarde();")
                    End If
                End If
            End If

        Catch ex As Exception
            cUtilitarios.br("N�o existe nada aberto")
            cUtilitarios.escreveScript("top.escondeaguarde();")
        End Try



    End Sub

    Public Function isAtividadeAberta(ByVal wf_id As String, ByVal atividade As String) As Boolean

        Try

            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
            bd.getEstadoAtividade(wf_id, atividade)
            'Response.Write(bd.SQL)
            'cUtilitarios.br(bd.SQL)
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read Then
                If dr.GetValue(0).ToString = "1" Then
                    Return True
                End If
            End If
            dr.Close()
            dr = Nothing

        Catch ex As Exception
            Dim excp As New clsException(ex)
            Return False
        End Try

        Return False


    End Function

    Public Function getWfID() As String
        If CStr(Session("subgrupo_id")) <> "" Then       'Ao selecionar uma Ap�lice/Subgrupo

            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
            bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

            Try
                Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

                If dr.Read() Then
                    Return dr.GetValue(0).ToString()
                End If
                dr.Close()
                dr = Nothing
                Return "0"
            Catch ex As Exception
                Dim excp As New clsException(ex)
                Return ""
            End Try

        Else
            Return "0"
        End If
    End Function

    Private Function ValidaEncerramento() As Boolean
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

        Dim retorno As Boolean = False
        Dim data_fim As DateTime
        'Dim prazo_limite As DateTime
        Dim hoje As DateTime = DateTime.Today

        Try

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.HasRows Then
                While dr.Read()

                    data_fim = dr.GetValue(2).ToString.Substring(0, 10)

                End While
            Else
                Session.Abandon()
                'cUtilitarios.br("N�o existe nenhuma Compet�ncia de Fatura em aberto")
                System.Web.HttpContext.Current.Response.Write("<script>parent.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp'</script>")
                'System.Web.HttpContext.Current.Response.Write("<script>parent.window.location=parent.window.location;</script>")                
                'Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
            End If

            dr.Close()
            dr = Nothing
        Catch ex As Exception
            Dim excp As New clsException(ex)
            retorno = False
        End Try

        'Chamando outro m�todo para pegar o Prazo Limite...
        'Try
        '    bd.prazo_limite(Session("apolice"), Session("ramo"), Session("subgrupo_id"), getWF_ID)

        '    dr = bd.ExecutaSQL_DR()

        '    If dr.Read() Then
        '        prazo_limite = dr.Item("dt_fim_faturamento").ToString.Substring(0, 10)

        '    Else
        '        retorno = False
        '    End If

        'Catch ex As Exception
        '    Dim excp As New clsException(ex)
        'Finally
        '    dr.Close()
        '    dr = Nothing
        'End Try

        'C�lculo de valida��o
        If data_fim.AddDays(-30) <= hoje Then
            retorno = True
        Else
            retorno = False
        End If
        'cUtilitarios.br(data_fim)
        'cUtilitarios.br(hoje)
        Return retorno

    End Function

End Class
