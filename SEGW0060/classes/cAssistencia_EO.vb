﻿Imports System.Collections.Generic

'Matheus Souza
'21/06/2010
'Encapsula as informações de assistências e é 
'utilizada para trocar essas informações entre as classes
Public Class cAssistencia_EO

    Private nPlanoId As Integer
    Private dInicioVigencia As Date
    Private nCustoAssistencia As Double
    Private sTipoAssistencia As String
    Private sStatus As String
    Private nSequencial As Integer
    Private nCustoAssistenciaNumVidas As Double
    Private sInicioAssistenciaSubGrupo As String
    Private sFimAssistenciaSubGrupo As String
    Private nTipoAssistenciaId As Integer
    Private oServicoAssistencia As List(Of cServicoAssistencia_EO)

    Public Property PlanoId() As Integer
        Get
            Return nPlanoId
        End Get
        Set(ByVal Value As Integer)
            nPlanoId = Value
        End Set
    End Property

    Public Property InicioVigencia() As Date
        Get
            Return dInicioVigencia
        End Get
        Set(ByVal Value As Date)
            dInicioVigencia = Value
        End Set
    End Property

    Public Property CustoAssistencia() As Double
        Get
            Return nCustoAssistencia
        End Get
        Set(ByVal Value As Double)
            nCustoAssistencia = Value
        End Set
    End Property

    Public Property TipoAssistencia() As String
        Get
            Return sTipoAssistencia
        End Get
        Set(ByVal Value As String)
            sTipoAssistencia = Value
        End Set
    End Property

    Public Property Status() As String
        Get
            Return sStatus
        End Get
        Set(ByVal Value As String)
            sStatus = Value
        End Set
    End Property

    Public Property Sequencial() As Integer
        Get
            Return nSequencial
        End Get
        Set(ByVal Value As Integer)
            nSequencial = Value
        End Set
    End Property

    Public Property CustoAssistenciaNumVidas() As Double
        Get
            Return nCustoAssistenciaNumVidas
        End Get
        Set(ByVal Value As Double)
            nCustoAssistenciaNumVidas = Value
        End Set
    End Property

    Public Property InicioAssistenciaSubGrupo() As String
        Get
            Return sInicioAssistenciaSubGrupo
        End Get
        Set(ByVal Value As String)
            sInicioAssistenciaSubGrupo = Value
        End Set
    End Property

    Public Property FimAssistenciaSubGrupo() As String
        Get
            Return sFimAssistenciaSubGrupo
        End Get
        Set(ByVal Value As String)
            sFimAssistenciaSubGrupo = Value
        End Set
    End Property

    Public Property TipoAssistenciaId() As Integer
        Get
            Return nTipoAssistenciaId
        End Get
        Set(ByVal Value As Integer)
            nTipoAssistenciaId = Value
        End Set
    End Property

    Public Property ServicoAssistencia() As List(Of cServicoAssistencia_EO)
        Get
            If oServicoAssistencia Is Nothing Then
                oServicoAssistencia = New List(Of cServicoAssistencia_EO)
            End If
            Return oServicoAssistencia
        End Get
        Set(ByVal Value As List(Of cServicoAssistencia_EO))
            oServicoAssistencia = Value
        End Set
    End Property

End Class
