
Namespace Model


Public Class cUsuario

#Region "Atributos"
    Private _usuarioID As Integer
    Private _loginWeb As String
    Private _nome As String
    Private _cpf As String
#End Region

#Region "Propriedades"
    Public Property UsuarioID() As Integer
        Get
            Return Me._usuarioID
        End Get
        Set(ByVal Value As Integer)
            Me._usuarioID = Value
        End Set
    End Property

    Public Property LoginWEB() As String
        Get
            Return Me._loginWeb
        End Get
        Set(ByVal Value As String)
            Me._loginWeb = Value
        End Set
    End Property

    Public Property Nome() As String
        Get
            Return Me._nome
        End Get
        Set(ByVal Value As String)
            Me._nome = Value
        End Set
    End Property

    Public Property CPF() As String
        Get
            Return Me._cpf
        End Get
        Set(ByVal Value As String)
            Me._cpf = Value
        End Set
    End Property

#End Region

#Region "Construtores"
    Sub New()

    End Sub

    Sub New(ByVal pUsuarioID As Integer)
        Me.UsuarioID = pUsuarioID
    End Sub

    Sub New(ByVal pUsuarioID As Integer, ByVal pLoginWEB As String, ByVal pNome As String, ByVal pCPF As String)
        Me.UsuarioID = pUsuarioID
        Me.LoginWEB = pLoginWEB
        Me.Nome = pNome
        Me.CPF = pCPF
    End Sub
#End Region

    End Class

End Namespace
