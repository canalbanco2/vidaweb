Namespace Controle

    Public Class cBasePage
        Inherits System.Web.UI.Page

        Protected linkseguro As Alianca.Seguranca.Web.LinkSeguro
        Protected cAmbiente As Alianca.Seguranca.Web.ControleAmbiente

        Protected usuario As Model.cUsuario



        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Me.linkseguro = New Alianca.Seguranca.Web.LinkSeguro

            Dim _usuario As String = Me.linkseguro.LerUsuario("Default.aspx", Request.ServerVariables("REMOTE_ADDR"), Request.QueryString("SLinkSeguro"))

            If _usuario = "" Then
                Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
            End If

            Me.usuario = New Model.cUsuario(Me.linkseguro.Usuario_ID, Me.linkseguro.Login_WEB, Me.linkseguro.Nome, Me.linkseguro.CPF)


            Dim url As String

            Me.cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
            url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
            Me.cAmbiente.ObterAmbiente(url)

            If Alianca.Seguranca.BancoDados.cCon.configurado Then
                Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
                'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
                'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
                'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produ��o)
            Else
                Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
                'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
                'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
                'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produ��o)
            End If

            Session.Add("GLAMBIENTE_ID", Me.cAmbiente.Ambiente)

        End Sub
    End Class
End Namespace