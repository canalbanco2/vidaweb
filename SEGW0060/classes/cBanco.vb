Imports Alianca.Seguranca.BancoDados
Imports System.Data

Public MustInherit Class cBanco

    Private oDS As DataSet
    Private sSQL As String
    Private _banco As String
    Dim Conn As SqlClient.SqlConnection


    Sub New(ByVal Bd As eBanco)

        _banco = Bd

    End Sub
    Public Enum eBanco As Byte

        Imagem_db = 1

    End Enum
    Public Property DS() As DataSet
        Get
            Return oDS
        End Get
        Set(ByVal Value As DataSet)
            oDS = Value
        End Set
    End Property
    Public Property SQL() As String
        Get
            Return sSQL
        End Get
        Set(ByVal Value As String)
            sSQL = Value
        End Set
    End Property
    Public Function IniciaTransacao() As Integer
        Return cCon.BeginTransaction()
    End Function
    Public Sub CommitTransacao(ByVal _numTran As Integer)
        cCon.CommitTransaction(_numTran)
    End Sub
    Public Sub RollBackTransacao(ByVal _numTran As Integer)
        cCon.RollBackTransaction(_numTran)
    End Sub
    Public Property Banco() As eBanco
        Get
            Return _banco
        End Get
        Set(ByVal Value As eBanco)
            _banco = Value
        End Set
    End Property
    Public Overridable Sub ExecutaSQL()
        Try

            Call ConfigurarConecao()

            'Descomentar para SABL0101
            oDS = cCon.ExecuteDataset(CommandType.Text, SQL)

            'Comentar para SABL0101
            'Dim Comm As New Data.SqlClient.SqlCommand(SQL, Conn)
            'Comm.ExecuteNonQuery()
            'Comm.Dispose()
            'Comm = Nothing
            'Fim Comentar para SABL0101

        Catch ex As Exception

            Dim errMessage As String = ""
            Dim tempException As Exception = ex

            While (Not tempException Is Nothing)
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine
                tempException = tempException.InnerException
            End While

            Throw New Exception(errMessage)
            'Comentar para SABL0101
            'Finally
            'Me.Conn.Close()
            'Me.Conn.Dispose()
            'Fim Comentar para SABL0101

        End Try

    End Sub
    Public Function ExecutaSQL_DR() As Data.SqlClient.SqlDataReader
        Try
            'If Not cCon.configurado Then
            Call ConfigurarConecao()
            'End If

            'Descomentar para SABL0101
            Return cCon.ExecuteReader(CommandType.Text, SQL, Nothing)

            'Comentar para SABL0101
            'Dim Comm As New Data.SqlClient.SqlCommand(SQL, Conn)
            'Dim _dr As Data.SqlClient.SqlDataReader
            '_dr = Comm.ExecuteReader()
            'Comm.Dispose()
            'Comm = Nothing
            'Return _dr
            'Fim Comentar para SABL0101


        Catch ex As Exception

            Dim errMessage As String = ""
            Dim tempException As Exception = ex

            While (Not tempException Is Nothing)
                errMessage += tempException.Message + Environment.NewLine + tempException.StackTrace + Environment.NewLine
                tempException = tempException.InnerException
            End While

            Throw New Exception(errMessage)

        End Try

    End Function
    Public Function ExecutaSQL_DT() As Data.DataTable
        Try
            'If Not cCon.configurado Then                
            Call ConfigurarConecao()
            'End If

            'Descomentar para SABL0101
            Dim ds As Data.DataSet = cCon.ExecuteDataset(CommandType.Text, SQL)
            If ds.Tables.Count = 0 Then
                Return CType(Nothing, DataTable)
            Else
                Return ds.Tables(0)
            End If


            'Comentar para SABL0101
            'Dim _Ds As New DataSet
            'Dim DA As New Data.SqlClient.SqlDataAdapter(SQL, Conn)
            'DA.Fill(_Ds)
            'DA.Dispose()
            'Conn.Dispose()
            'Conn = Nothing
            'DA = Nothing
            'Return _Ds.Tables(0)
            'Fim Comentar para SABL0101


        Catch ex As Exception

            Dim errMessage As String = ""
            Dim tempException As Exception = ex

            While (Not tempException Is Nothing)
                errMessage += tempException.Message + Environment.NewLine + tempException.StackTrace + Environment.NewLine
                tempException = tempException.InnerException
            End While

            Throw New Exception(errMessage)
        End Try

    End Function
    Private Sub ConfigurarConecao()
        'Dim a As Integer
        Try
            'If cCon.configurado = False Then
            'a = cCon.ConfiguraConexao(Reflection.Assembly.GetExecutingAssembly(), cCon.Ambiente)
            'Else
            '   a = cCon.ReConfiguraConexao(Reflection.Assembly.GetExecutingAssembly(), cCon.Ambiente)
            'End If
            'cUtilitarios.br(a)
            'cCon.ConnectionTimeout = cCon.TimeTimeout.Infinito
            'cCon.BancodeDados = Banco

        Catch ex As Exception

            Dim errMessage As String = ""
            Dim tempException As Exception = ex

            While (Not tempException Is Nothing)
                errMessage += tempException.Message + Environment.NewLine + tempException.StackTrace + Environment.NewLine
                tempException = tempException.InnerException
            End While

            'Throw New Exception(errMessage)

        End Try
    End Sub

    Public Overridable Function ExecutaSQL_Non_Query(Optional ByVal trann As Int16 = 0) As Integer
        Try


            'Comentar para SABL0101
            Return cCon.ExecuteNonQuery(CommandType.Text, SQL)



        Catch ex As Exception

            Dim errMessage As String = ""
            Dim tempException As Exception = ex

            While (Not tempException Is Nothing)
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine
                tempException = tempException.InnerException
            End While

            Throw New Exception(errMessage)


        End Try

    End Function


#Region "M�todos Utilitarios"
    Public Sub fPrint(ByVal msg As String)
        Web.HttpContext.Current.Response.Write(msg & "**<br>" & vbNewLine)
        Web.HttpContext.Current.Response.Flush()
    End Sub
    Public Function TrataString(ByVal Str As String) As String
        If Str Is Nothing Then
            Return "null"
        Else
            If Str.ToString.Trim = String.Empty Then
                Return "null"
            Else
                Return "'" & Str.Replace("'", "`") & "'"
            End If
        End If
    End Function
    Public Function TrataInteger(ByVal Str As Object) As String
        If Str Is Nothing Then
            Return "null"
        Else
            If Str.ToString.Trim = String.Empty Then
                Return "null"
            Else
                Return Str.ToString.Replace(".", "").Replace(",", ".")
            End If
        End If
    End Function
    Public Function TrataData(ByVal Dia As String, ByVal Mes As String, ByVal Ano As String) As String
        If Dia = "" And Mes = "" And Ano = "" Then
            Return "null"
        Else
            Dim Data As Double
            Data = Integer.Parse(Dia)
            Data += Integer.Parse(Mes) * 100
            Data += Integer.Parse(Ano) * 10000

            Return "'" & Data & "'"

        End If
    End Function
    Public Function TrataData(ByVal data As String) As String
        Dim dia, mes, ano, Adata() As String
        dia = ""
        mes = ""
        ano = ""
        If data.Trim <> "" Then
            Adata = data.Split(" ")(0).Split("/")
            If Adata.Length >= 3 Then
                dia = Adata(0)
                mes = Adata(1)
                ano = Adata(2)
            End If
        End If

        Return TrataData(dia, mes, ano)
    End Function
#End Region


End Class