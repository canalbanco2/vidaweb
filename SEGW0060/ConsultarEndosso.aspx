<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ConsultarEndosso.aspx.vb"
    Inherits="segw0060.ConsultarEndosso" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Consulta de Endosso</title>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
    <meta content="Microsoft Visual Studio .NET 8" name="GENERATOR" />
    <meta content="Visual Basic .NET 8" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema" />
    <link media="all" href="scripts/box/box.css" type="text/css" rel="stylesheet" />
    <link href="CSS/EstiloBase.css" type="text/css" rel="stylesheet" />

    <script src="scripts/box/AJS.js" type="text/javascript"></script>

    <script src="scripts/box/box.js" type="text/javascript"></script>

    <script type="text/javascript" src="scripts/overlib421/mini/overlibY.js"></script>

    <script type="text/javascript">
		var GB_IMG_DIR = "scripts/box/";
		GreyBox.preloadGreyBoxImages();

        function ExibirRelatorioEndosso(proposta_id, endosso_id){
            window.open('ImprimirEndosso.aspx?SLinkSeguro=<%=Request("SLinkSeguro")%>&proposta_id=' + proposta_id + '&endosso_id=' + endosso_id, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1024,height=480');
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="form-consulta-endosso">
            <p style="display: none;">
                Proposta:
                <asp:TextBox ID="txtPropostaId" runat="server" /><asp:Button ID="btnPesquisar" runat="server"
                    Text="Pesquisar" /></p>
            <br />
            <asp:Label ID="lblRegistroInexistente" runat="server" Text="N�o h� endossos a serem exibidos!" Visible="false" />
            <asp:DataGrid ID="grdLista" runat="server" CellPadding="0" CellSpacing="0" AutoGenerateColumns="False"
                ShowHeader="True" PageSize="50" AllowPaging="True" BorderColor="#DDDDDD" HeaderStyle-CssClass="th"
                Width="100%">
                <SelectedItemStyle CssClass="ponteiro"></SelectedItemStyle>
                <AlternatingItemStyle CssClass="ponteiro"></AlternatingItemStyle>
                <ItemStyle CssClass="ponteiro"></ItemStyle>
                <FooterStyle CssClass="0019GridFooter"></FooterStyle>
                <Columns>
                    <asp:BoundColumn DataField="proposta_id" HeaderText="Proposta">
                        <ItemStyle HorizontalAlign="left" Width="70px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="endosso_id" HeaderText="Endosso">
                        <ItemStyle HorizontalAlign="left" Width="70px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="descricao" HeaderText="Descri��o">
                        <ItemStyle HorizontalAlign="left"></ItemStyle>
                    </asp:BoundColumn>
                </Columns>
                <PagerStyle Visible="False"></PagerStyle>
            </asp:DataGrid>
        </div>
    </form>
</body>
</html>
