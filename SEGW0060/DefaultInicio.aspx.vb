﻿Public Partial Class DefaultInicio
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim v_sUsuario As String = IIf(IsNothing(Request("Usuario")), "ALGRIGORIO", Request("Usuario"))
        'Dim v_sUsuario As String = IIf(IsNothing(Request("Usuario")), "daisouza", Request("Usuario"))

        Dim v_sValorParametro As String

        Dim v_oParametro As New Alianca.Seguranca.Web.LinkSeguro()
        'Dim v_oParametro As New Alianca.Seguranca.Web.LinkSeguro(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
        v_sValorParametro = v_oParametro.GerarParametro("Default.aspx", Alianca.Seguranca.Web.LinkSeguro.TempoExpiracao.Grande, Request.UserHostAddress, v_sUsuario, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)

        Dim teste As String = v_oParametro.LerUsuario(Nothing, Request.UserHostAddress, v_sValorParametro)

        v_oParametro.LerUsuario("Default.aspx", Request.UserHostAddress, v_sValorParametro)

        v_oParametro = Nothing

        Response.Redirect("Default.aspx?SLinkSeguro=" & v_sValorParametro & "&pqsId=" & Request.QueryString("pqsId"))
        
    End Sub

End Class