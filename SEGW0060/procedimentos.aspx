<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Procedimentos.aspx.vb" Inherits="segw0060.Procedimentos"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Procedimentos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<P>
				<asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela" Visible="False">Label</asp:label><BR>
				<asp:label id="lblVigencia" runat="server" CssClass="Caminhotela"><br>COTEMINAS S.A - Per�odo de compet�ncia: 01/12/2006 at� 31/12/2006</asp:label></P>
			<P><BR>
				<asp:Label id="lblTitulo" runat="server" CssClass="Caminhotela">:: Procedimentos</asp:Label></P>
			<P>
				<asp:Label id="Label1" runat="server" CssClass="Caminhotela">Movimenta��o on-line</asp:Label><BR>
				<div class="estiloProcedimento">
					<STRONG>Primeiro passo:</STRONG>
					<BR>
					Primeiro passo a ser executado para cumprir a rotina do procedimento;
					<BR>
					<STRONG>Segundo passo:
						<BR>
					</STRONG>Segundo passo a ser executado para que a rotina do procedimento possa 
					ser cumprida;<BR>
					<STRONG>Terceiro passo:
						<BR>
					</STRONG>Terceiro passo a ser executado apra que possa ser cumprida a rotina do 
					procedimento;
				</div>
			<P>
				<asp:Label id="Label2" runat="server" CssClass="Caminhotela">Movimenta��o por arquivo</asp:Label><BR>
				<div class="estiloProcedimento">
					<b>Primeiro passo:</b>
					<BR>
					</STRONG>Quarto passo a ser executado para cumprir a rotina do procedimento;
					<BR>
					<STRONG><STRONG>Segundo </STRONG>passo:
						<BR>
					</STRONG>Quinto passo a ser executado para que a rotina do procedimento possa 
					ser cumprida;<BR>
					<STRONG><STRONG>Terceiro </STRONG>passo:
						<BR>
					</STRONG>Sexto passo a ser executado para que possa ser cumprida a rotina do 
					procedimento;
				</div>
			<P>
				<asp:Label id="Label3" runat="server" CssClass="Caminhotela">Encerrar movimenta��o</asp:Label><BR>
				<div class="estiloProcedimento">
					<b>Primeiro passo:</b>
					<BR>
					</STRONG>Quarto passo a ser executado para cumprir a rotina do procedimento;
					<BR>
					<STRONG><STRONG>Segundo </STRONG>passo:
						<BR>
					</STRONG>Quinto passo a ser executado para que a rotina do procedimento possa 
					ser cumprida;<BR>
					<STRONG><STRONG>Terceiro </STRONG>passo:
						<BR>
					</STRONG>Sexto passo a ser executado para que possa ser cumprida a rotina do 
					procedimento;
					<P>
						<asp:HyperLink id="HyperLink1" runat="server" Font-Size="X-Small" NavigateUrl="Download.aspx" Font-Underline="True">Clique aqui para Retornar "Declara��o Pessoal de Sa�de (DPS)"</asp:HyperLink><BR>
						&nbsp;</P>
				</div>
		</form>
	</body>
</HTML>
