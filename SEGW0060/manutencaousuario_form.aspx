<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManutencaoUsuario_Form.aspx.vb" Inherits="segw0060.ManutencaoUsuario_Form" Transaction="Required" debug="True"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>ManutencaoUsuario_Form</TITLE>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY onload="onload();">
		<SCRIPT language="javascript">
		
		function onload() {
			try {
				if (msg != '') {
					alert(msg);
				}
			}
			catch (ex) {
				
			}
		}
		
		
	
function validar() {					
						
	var usuario = document.getElementById('txtUsuario');
	var cpf = document.getElementById('txtCpf');
	var login = document.getElementById('txtLogin');
	var email = document.getElementById('txtEmail');			
	var confirmEmail = document.getElementById('txtConfirmacaoEmail');			
	
	if(usuario.value == "") {
		alert('Informe o Usu�rio.');
		usuario.focus();
		return false;
	}
						
	if(!verifica_cpf(cpf.value)) {	
		alert('Informe um CPF v�lido.');
		cpf.focus();
		return false;
	}			
						
	if (login.value.length < 6 || login.value.length > 8) {								
		alert('Informe um Login v�lido.');
		login.focus();
		return false;
	} 
		
	if(email.value != confirmEmail.value || email.value == "" ) {
		
		if(email.value != confirmEmail.value && email.value != "") {
			alert('Confirme seu email.');	
			confirmEmail.focus();
		} else {
			alert('Informe um email v�lido.');	
			email.focus();
		}
		
		return false;
	}			
			
	return true;			
}
		
		
function isEmpty(str) 
{ 
	if (str==null) return true
	
	for (var intLoop = 0; intLoop < str.length; intLoop++)
		if (" " != str.charAt(intLoop))
			return false;            
			
	return true; 
}		
		
function verifica_cpf(valor) 
{
    if (isEmpty(valor))
      return false;

	valor = valor.toString()
	
	valor = valor.replace( "-", "" );
	valor = valor.replace( "-", "" );
	valor = valor.replace( ".", "" );
	valor = valor.replace( ".", "" );
	valor = valor.replace( ".", "" );		

	if ((isNaN(valor)) && (valor.length != 11))
      return false
	
	Mult1 = 10   
	Mult2 = 11
	dig1 = 0
	dig2 = 0
	
	for(var i=0;i<=8;i++)
	{
	    ind = valor.charAt(i)
		dig1 += ((parseFloat(ind))* Mult1)
		Mult1--
	}
	
	for(var i=0;i<=9;i++)
	{
	    ind = valor.charAt(i)
		dig2 += ((parseFloat(ind))* Mult2)
		Mult2--
	}

	dig1 = (dig1 * 10) % 11   
	dig2 = (dig2 * 10) % 11   
	
	if (dig1 == 10)
      dig1 = 0
      
	if(dig2 == 10)
      dig2 = 0
	 
	if (parseFloat(valor.charAt(9)) != dig1)
		return false   
	if (parseFloat(valor.charAt(10)) != dig2)
		return false   
	
	//Verificar a digita��o de CPF com todos os d�gitos iguais
	igual = new Array();
	for (var i=0;i<=10;i++)
	{
		if (i == 0) {
			anterior = valor.substring(i,i+1);
		}
		else {
			if (anterior == valor.substring(i,i+1)) {
				igual[i] = "sim";
			}
			anterior = valor.substring(i,i+1);
		}
	}
	var cont = 1;
	for (var i=0;i<=10;i++)
	{
		if (igual[i] == "sim") {
			cont++;
		}
	}
	if (cont == 11)
		return false
	
	return true
}
		
		
function FormataCPF(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres){
 var wTecla, wVr, wTam;
 
       //alert(pForm);
  
 wTecla = pTeclaPres.keyCode;
 wVr = pForm[pCampo].value;
 wTam = wVr.length ;	
 ult	= wVr.substring(wTam-1,wTam);
 vl = ult.charCodeAt(0);
 //trava caracteres n�o num�ricos
 if (vl < 48 || vl > 57){ 
	 wVr = wVr.replace( ult.valueOf() , "" );
	 pForm[pCampo].value = wVr;
 }
 
 wVr = wVr.toString().replace( "-", "" );
 wVr = wVr.toString().replace( ".", "" );
 wVr = wVr.toString().replace( ".", "" );
 wVr = wVr.toString().replace( "/", "" );
 wTam = wVr.length ;

 if (wTam < pTamMax && wTecla != 8) { 
    wTam = wVr.length + 1 ; 
 }

 if (wTecla == 8 ) { 
    wTam = wTam - 1 ; 
 }
   
 if ( wTecla == 8 || wTecla == 88 || wTecla >= 48 && wTecla <= 57 || wTecla >= 96 && wTecla <= 105 ){
  if ( wTam <= 2 ){
    pForm[pCampo].value = wVr ;
  }
  if (wTam > pPosTraco && wTam <= pTamMax) {
        wVr = wVr.substr(0, wTam - pPosTraco) + '-' + wVr.substr(wTam - pPosTraco, wTam);
  }
  if ( wTam == pTamMax){
        wVr = wVr.substr( 0, wTam - pPos1 ) + '.' + wVr.substr(wTam - pPos1, 3) + '.' + wVr.substr(wTam - pPos2, wTam);
  }
  pForm[pCampo].value = wVr;
 
 }

}


		</SCRIPT>
		<form id="Form1" name="FormCpf" method="post" runat="server">
			<asp:panel id="pnlCadastroUsuario" runat="server" Width="100%" Visible="True">
				<TABLE class="DescricaoCampo" cellSpacing="0" cellPadding="2" width="100%" border="0">
					<TR>
						<TD align="left" width="25%" colSpan="2">
							<asp:Label id="lblCadastroUsuario" runat="server" CssClass="Caminhotela">:: Cadastro de usu�rio</asp:Label></TD>
					</TR>
					<TR>
						<TD align="right" width="25%">Usu�rio:</TD>
						<TD>
							<asp:TextBox id="txtUsuario" runat="server" Width="216px"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD align="right">CPF:</TD>
						<TD>
							<asp:TextBox onkeyup="FormataCPF(document.getElementById('Form1'),'txtCPF',11,8,5,2,event);"
								id="txtCPF" runat="server" Width="96px" MaxLength="14"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD align="right">Login:</TD>
						<TD>
							<asp:TextBox id="txtLogin" runat="server" Width="64px" MaxLength="8"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD align="right" height="9">Acesso:</TD>
						<TD height="9">
							<asp:DropDownList id="ddlAcesso" runat="server">
								<asp:ListItem Value="Master">Master</asp:ListItem>
								<asp:ListItem Value="Operacional">Operacional</asp:ListItem>
							</asp:DropDownList></TD>
					</TR>
					<TR>
						<TD align="right" height="15">Situa��o:</TD>
						<TD height="15">
							<asp:DropDownList id="ddlSituacao" runat="server">
								<asp:ListItem Value="Ativo">Ativo</asp:ListItem>
								<asp:ListItem Value="Inativo">Inativo</asp:ListItem>
							</asp:DropDownList></TD>
					</TR>
					<TR>
						<TD align="right" height="24">E-Mail:</TD>
						<TD vAlign="top" height="24">
							<asp:TextBox id="txtEmail" runat="server" Width="200px"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD noWrap align="right" height="24">Confirma��o E-mail:</TD>
						<TD vAlign="top" height="24">
							<asp:TextBox id="txtConfirmacaoEmail" runat="server" Width="200px"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2">
							<asp:Button id="btnIncluirUsuario" runat="server" CssClass="Botao" Text="Incluir"></asp:Button>
							<asp:Button id="btnExcluir" runat="server" CssClass="Botao" Text="Excluir"></asp:Button>
							<asp:Button id="btnCancelar" runat="server" CssClass="Botao" Text="Cancelar" CausesValidation="False"></asp:Button></TD>
					</TR>
				</TABLE>
			</asp:panel></form>
		</SCRIPT>
	</BODY>
</HTML>
