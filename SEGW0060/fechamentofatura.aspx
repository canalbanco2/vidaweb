<%@ Page Language="vb" AutoEventWireup="false" Codebehind="FechamentoFatura.aspx.vb" Inherits="segw0060.FechamentoFatura"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Downloads</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
			<script>
			function confirmaFechamento(){
				var confirma = confirm("Toda a movimenta��o de vidas ser� finalizada, n�o sendo poss�vel incluir / alterar / excluir\nmais nenhuma vida referente � fatura de 01/12/2006 at� 31/12/2006.\n\nConfirma a finaliza��o da movimenta��o de vidas?");
				if (confirma==true){
					//alert('Fatura fechada com sucesso.');
					location.href='?fatura=ok';
					return false;
				}else{
					return false;
				}
			}
			</script>
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<P>
				<asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela" Visible="False">Label</asp:label><BR>
				<asp:label id="lblVigencia" runat="server" CssClass="Caminhotela"><br>COTEMINAS S.A - Per�odo de compet�ncia: 01/12/2006 at� 31/12/2006</asp:label></P>
			<P>
				<asp:Label id="Label17" runat="server" CssClass="Caminhotela">Condi��es do subgrupo</asp:Label>
				<TABLE id="Table5" style="BORDER-COLLAPSE: collapse" borderColor="#cccccc" cellSpacing="0"
					cellPadding="1" width="99%" border="1"> <!-- PRIMEIRA LINHA -->
					<TR class="titulo-tabela sem-sublinhado">
						<TD>Capital Segurado</TD>
						<TD>Limite&nbsp;M�ximo</TD>
						<TD>M�lt. Salarial</TD>
						<TD>Idade M�n. Mov.</TD>
						<TD>Idade M�x. Mov</TD>
						<TD>C�njuge</TD>
					</TR>
					<TR>
						<TD align="center">M�ltiplo de Sal�rio</TD>
						<TD align="right">R$500.000,00</TD>
						<TD align="center">24</TD>
						<TD align="center">14</TD>
						<TD align="center">60</TD>
						<TD align="center">sim</TD>
					</TR> <!-- SEGUNDA LINHA --> <!-- TERCEIRA LINHA --> <!-- QUARTA LINHA --></TABLE>
				<BR>
				<asp:Panel id="pnlAlerta" runat="server" Visible="False" CssClass="fundo-azul-claro" Width="60%"
					HorizontalAlign="Center" Height="70px">
					<BR>
					<asp:Label id="lblListagemEnviada" runat="server" CssClass="Caminhotela"><br>Fatura encerrada com sucesso!</asp:Label>
				</asp:Panel>
				<BR>
				<asp:Label id="lblListagemVidas" runat="server" CssClass="Caminhotela">Lista das vidas</asp:Label>
				<TABLE id="tabelaMovimentacao" style="MARGIN-BOTTOM: 10px; BORDER-COLLAPSE: collapse" borderColor="#cccccc"
					cellSpacing="0" cellPadding="0" width="100%" border="1">
					<TR class="titulo-tabela sem-sublinhado">
						<TD width="70">Opera��o</TD>
						<TD width="266">Segurado</TD>
						<TD width="66">Tipo</TD>
						<TD width="134">CPF</TD>
						<TD width="55">Nascimento</TD>
						<TD width="28">Sexo</TD>
						<TD width="85" nowrap>Sal�rio (R$)</TD>
						<TD width="83" nowrap>Capital (R$)</TD>
					</TR>
					<TR class="paddingzero">
						<TD colSpan="8">
							<DIV style="OVERFLOW: auto; HEIGHT: 240px" DESIGNTIMEDRAGDROP="807">
								<TABLE class="paddingzero" id="Table2" style="BORDER-COLLAPSE: collapse" borderColor="#cccccc"
									cellSpacing="0" cellPadding="0" width="100%" border="1">
									<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#ffffff'">
										<TD width="70" height="15"></TD>
										<TD>Rodrigo Augusto
										</TD>
										<TD width="53" height="15">Titular</TD>
										<TD width="92" height="15">xxx.xxx.xxx-xx</TD>
										<TD width="82" height="15">xx/xx/xxxx</TD>
										<TD width="35" height="15">masc.</TD>
										<TD align="right" width="89" height="15">1.000,00</TD>
										<TD align="right" width="69">25.000,00</TD>
									</TR> <!--<% if Request.QUeryString("excluir")=1 then %>#AACCFF<% end if %>-->
									<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background=''">
										<TD height="15">
											<asp:Label id="lblAcao" runat="server" ForeColor="DarkRed"></asp:Label></TD>
										<TD height="15">
											<asp:ImageButton id="imgPlus" runat="server" ImageUrl="Images/imgMenos.gif" ImageAlign="AbsMiddle"></asp:ImageButton>
											&nbsp;Jos� Luis de Souza</TD>
										<TD height="15">Titular</TD>
										<TD height="15">xxx.xxx.xxx-xx</TD>
										<TD height="15">xx/xx/xxxx</TD>
										<TD height="15">masc.</TD>
										<TD align="right" height="15">1.000,00</TD>
										<TD align="right" height="15">9.000,00</TD>
									</TR>
									<asp:Panel id="pnlDependente" Visible="False" Runat="server">
										<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #f5f5f5"
											onmouseout="this.style.background='#f5f5f5'">
											<TD>&nbsp;</TD>
											<TD><SPAN class="grid-sub">Ricardo Vaccani</SPAN></TD>
											<TD>Filho</TD>
											<TD>xxx.xxx.xxx-xx</TD>
											<TD>xx/xx/xxxx</TD>
											<TD>masc.</TD>
											<TD align="right">1.000,00</TD>
											<TD align="right">12.000,00</TD>
										</TR>
										<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #f5f5f5"
											onclick="vidaExcluir(3)" onmouseout="this.style.background='#f5f5f5'">
											<TD>
												<asp:Label id="Label15" runat="server" ForeColor="DarkRed"></asp:Label></TD>
											<TD><SPAN class="grid-sub">Gustavo Modena</SPAN></TD>
											<TD>Filho</TD>
											<TD>xxx.xxx.xxx-xx</TD>
											<TD>xx/xx/xxxx</TD>
											<TD>masc.</TD>
											<TD align="right">1.000,00</TD>
											<TD align="right">9.000,00</TD>
										</TR>
										<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #f5f5f5"
											onmouseout="this.style.background='#f5f5f5'">
											<TD>&nbsp;</TD>
											<TD><SPAN class="grid-sub">Maria da Silva</SPAN></TD>
											<TD>C�njuge</TD>
											<TD>xxx.xxx.xxx-xx</TD>
											<TD>xx/xx/xxxx</TD>
											<TD>fem.</TD>
											<TD align="right"></TD>
											<TD align="right">17.000,00</TD>
										</TR>
										<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #f5f5f5"
											onmouseout="this.style.background='#f5f5f5'">
											<TD>&nbsp;</TD>
											<TD><SPAN class="grid-sub">Marcio de Almeida</SPAN></TD>
											<TD>Filho</TD>
											<TD>xxx.xxx.xxx-xx</TD>
											<TD>xx/xx/xxxx</TD>
											<TD>masc.</TD>
											<TD align="right">1.000,00</TD>
											<TD align="right">21.000,00</TD>
										</TR>
									</asp:Panel><% for i as integer = 0  to 100 %>
									<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#ffffff'">
										<TD height="15"></TD>
										<TD height="15">Rodrigo Augusto</TD>
										<TD height="15">Titular</TD>
										<TD height="15">xxx.xxx.xxx-xx</TD>
										<TD height="15">xx/xx/xxxx</TD>
										<TD height="15">masc.</TD>
										<TD align="right" height="15">1.000,00</TD>
										<TD align="right">25.000,00</TD>
									</TR>
									<% next %>
								</TABLE>
							</DIV>
						</TD>
					</TR>
					<TR style="COLOR: #fff; TEXT-ALIGN: center" bgColor="#003399">
						<TD colSpan="9">
							<TABLE id="Table1" style="COLOR: #ffffff" cellSpacing="1" cellPadding="1" width="100%"
								border="0">
								<TR>
									<TD align="center" width="837">P�gina -&nbsp;&lt;&lt;&nbsp;1 &gt;&gt;</TD>
									<TD noWrap align="right">Total registros: 100</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
				<asp:Panel id="pnlBtnEncerrar" runat="server" Visible="true">
				<DIV align="center">					
					<div style="TEXT-ALIGN:center">Ao clicar no bot�o						
						"Encerrar movimenta��o" ser� encerrada a movimenta��o das vidas que far�o parte 
						da fatura<br>
						de 01/12/2006 at� 31/12/2006.<br>
						&nbsp;
						<br>
						<asp:Button id="btnFecharMovimentacao" runat="server" CssClass="Botao" Width="160px" Text="Encerrrar movimenta��o"></asp:Button></div>
					<BR>
					&nbsp;
					<P></P>
				</DIV>
				</asp:Panel>
		</form>
	</body>
</HTML>
