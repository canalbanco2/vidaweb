<%@ Page Language="vb" AutoEventWireup="false" Codebehind="MovimentacaoUmaUm.aspx.vb" Inherits="segw0060.MovimentacaoUmaUm"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>	
		<title>MovimentacaoUmaUm</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<LINK media="all" href="scripts/box/box.css" type="text/css" rel="stylesheet">
			<script src="scripts/box/AJS.js" type="text/javascript"></script>
			<script src="scripts/box/box.js" type="text/javascript"></script>
			<script type="text/javascript">
			var GB_IMG_DIR = "scripts/box/";
			GreyBox.preloadGreyBoxImages();
			</script>
			<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
				<script>
			function escondeDiv(div){
				var idDiv = document.getElementById(div);
				if (idDiv.style.display==''){
					idDiv.style.display='none';
				}else{
					idDiv.style.display='';
				}
			}
			
			function vidaExcluir(numero){
				if (numero)
					location.href="MovimentacaoUmaUm.aspx?excluir=1&expandir=1";
				else
					location.href="MovimentacaoUmaUm.aspx&expandir=1";
			}
			
			function confirmaExclusao(valor){
				var divExcluir = document.getElementById("lblAcao");
				var confirmacao = confirm('Confirma a exclus�o do segurado "Luiz Jos� de Souza"?\nSe existirem c�njuges para este segurado todos ser�o exclu�dos.')
				if (valor=="true"){
					
					if (confirmacao==true){
						divExcluir.innerHTML='Exclu�do';
					}else{
						return false;
					}
				}else{
					divExcluir.innerHTML='';
				}
				return false;
			}
			
			function confirmaDesfazer(){
				var confirmacao = confirm('Ser�o desfeitas todas as altera��es realizadas para o Subgrupo "<%= Session("subgrupo")%>".\n\nConfirma a opera��o?');
			}
				</script>
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<br>
			<asp:panel id="Panel1" runat="server" HorizontalAlign="Left">
				<TABLE cellSpacing="0" cellPadding="0" width="99%" border="0">
					<TR>
						<TD align="left" width="1021" colSpan="4" height="24"><!-- Painel de Cadastro geral -->
							<TABLE id="Table4" cellSpacing="0" cellPadding="0" border="0">
								<TR>
									<TD>
										<asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela">Label</asp:label><BR>
										<asp:Label id="lblVigencia" runat="server" CssClass="Caminhotela"><br>COTEMINAS S.A - Per�odo de compet�ncia: 01/12/2006 at� 31/12/2006</asp:Label></TD>
									<TD align="right"></TD>
								</TR>
							</TABLE>
							<BR>
							<asp:Label id="Label17" runat="server" CssClass="Caminhotela">Condi��es do subgrupo</asp:Label>
							<TABLE id="Table5" style="BORDER-COLLAPSE: collapse" borderColor="#cccccc" cellSpacing="0"
								cellPadding="1" width="100%" border="1"> <!-- PRIMEIRA LINHA -->
								<TR class="titulo-tabela sem-sublinhado">
									<TD>Capital Segurado</TD>
									<TD>Limite&nbsp;M�ximo</TD>
									<TD>M�lt. Salarial</TD>
									<TD>Idade M�n. Mov.</TD>
									<TD>Idade M�x. Mov</TD>
									<TD>C�njuge</TD>
								</TR>
								<TR>
									<TD align="center">M�ltiplo de Sal�rio</TD>
									<TD align="right">R$500.000,00</TD>
									<TD align="center">24</TD>
									<TD align="center">14</TD>
									<TD align="center">60</TD>
									<TD align="center"><%=Session("infoConjuge")%></TD>
								</TR> <!-- SEGUNDA LINHA --> <!-- TERCEIRA LINHA --> <!-- QUARTA LINHA --></TABLE>
						</TD>
					</TR>
					<TR>
						<TD colSpan="4" height="5"></TD>
					</TR>
					<TR>
						<TD colSpan="4">
						<asp:panel id="Panel2" runat="server" HorizontalAlign="Left">
						<TABLE id="tabelaMovimentacao2" style="MARGIN-LEFT: 0px;MARGIN-TOP: 10px; MARGIN-BOTTOM: 10px; BORDER-COLLAPSE: collapse" borderColor="#cccccc"
										cellSpacing="0" cellPadding="0" border="0">
						<tr>
							<TD vAlign="bottom">
								<asp:Label id="Label1" runat="server" CssClass="Caminhotela">Componente</asp:Label>
							</TD>
						</tr>
						<tr>
							<TD vAlign="middle" height="20">
								Selecione o Componente: 
								<asp:DropDownList id="DropdownSelComponente" runat="server" AutoPostBack="True" Width="113px">																		
									<asp:ListItem Value="1">001 - Titular</asp:ListItem>
									<asp:ListItem Value="2">002 - Filhos</asp:ListItem>
									<asp:ListItem Value="3">003 - C�njuge</asp:ListItem>
								</asp:DropDownList>								
							</TD>
						</tr>
						<tr>
							<td>
							<DIV style="OVERFLOW: auto; WIDTH: 550px; Height: 109px" DESIGNTIMEDRAGDROP="750">
								<DIV style="OVERFLOW: hidden;" DESIGNTIMEDRAGDROP="900">
								<TABLE id="tabelaMovimentacao" style="MARGIN-LEFT: 0px;MARGIN-TOP: 0px; MARGIN-BOTTOM: 0px; BORDER-COLLAPSE: collapse" borderColor="#cccccc"
											cellSpacing="0" cellPadding="0" width="540" border="1">
																				
								<TR class="titulo-tabela sem-sublinhado">
									<!-- TD width="50">C�digo</TD>
									<TD>Descri��o Cobertura </TD>
									<TD width="100">Imp. Segurada </TD>
									<TD width="90">% Perc. B�s.</TD>
									<TD width="80">%Taxa Net</TD>
									<TD width="90">Taxa Tarifa</TD>
									<TD width="50">Pr�mio</TD>
									<TD width="120">% Desc. T�cnico</TD>
									<TD width="130">Dt. In�cio Vig�ncia</TD>
									<TD width="130">Dt. Fim Vig�ncia</TD>
									<TD width="130">M�nimo Franquia</TD>
									<TD width="90">% Franquia</TD>
									<TD width="120">Descr. Franquia</TD>
									<TD width="90">Lim. M�n. IS</TD>
									<TD width="90">Lim. M�x. IS</TD>
									<TD width="50">Car�ncia</TD>
									<TD width="90">Lim. Di�rias</TD -->
									<TD>Nome Cobertura </TD>
									<TD width="90">Limite M�nimo</td>
									<TD width="120">Limite M�ximo</td>
								</TR>
								<% for i as integer = 1  to 5 %>
								<TR>
									<!-- TD align="center"><%=i%></TD>
									<TD>Descri��o <%=i%></TD>
									<TD align="center"><%=i*1000%>,00 </TD>
									<TD align="center"><%=i*10%></TD>
									<TD align="center"><%=i*5%></TD>
									<TD align="center"><%=i*100%>,00</TD>
									<TD align="center"><%=i*50%>,00</TD>
									<TD align="center"><%=i*2%></TD>
									<TD align="center">xx/0<%=i%>/xxxxx</TD>
									<TD align="center">xx/0<%=i + 1%>/xxxxx</TD>
									<TD align="center"><%=i*500%>,00</TD>
									<TD align="center"><%=i*15%></TD>
									<TD align="center">Descr Franq <%=i*50%></TD>
									<TD align="center"><%=i*30%>,00</TD>
									<TD align="center"><%=i*20%>,00</TD>
									<TD align="center"><%=i*5%></TD>
									<TD align="center"><%=i*60%>,00</TD -->
									<TD>Cobertura <%=i%> </TD>
									<TD align="center"><%=i*1000%>,00 </td>
									<TD align="center"><%=i*2500%>,00 </td>
								</TR>
								<% next %>
								</TABLE>
								</div>
							</div>
							</td>
						</tr>
						</table>
						</asp:panel>
						</TD>
					</TR>
					<TR>
						<TD vAlign="bottom" width="70%">
							<asp:Label id="lblListagem" runat="server" CssClass="Caminhotela">Lista das vidas</asp:Label></TD>
						<TD vAlign="middle" noWrap align="right" width="30%" colSpan="2" height="20">							
							<asp:DropDownList id="DropDownList2" runat="server" AutoPostBack="True" Width="113px">
								<asp:ListItem Value="Todos" Selected="True">Todos</asp:ListItem>
								<asp:ListItem Value="CPF">CPF</asp:ListItem>								
								<asp:ListItem Value="Nome">Nome</asp:ListItem>
								<asp:ListItem Value="Inclusoes">Inclus�es</asp:ListItem>
								<asp:ListItem Value="Exclusoes">Exclus�es</asp:ListItem>
								<asp:ListItem Value="Alteracoes">Altera��es</asp:ListItem>
								<asp:ListItem Value="Acertos">Acertos</asp:ListItem>
								<asp:ListItem Value="DPS">DPS Pendente</asp:ListItem>
							</asp:DropDownList>
							<asp:TextBox id="txtFiltro" runat="server" Visible="False"></asp:TextBox>
							<asp:Button id="btnFiltrar" runat="server" CssClass="Botao" Text="Pesquisar"></asp:Button>
							<asp:Label id="lblFiltro" runat="server" Visible="False" ForeColor="Red"></asp:Label>							
							</TD>
					</TR>
					<TR>
						<TD colSpan="4">
						<DIV style="OVERFLOW: auto; WIDTH: 750px; Height: 176px" DESIGNTIMEDRAGDROP="750">
							<DIV style="OVERFLOW: hidden;" DESIGNTIMEDRAGDROP="750">
								<TABLE id="tabelaMovimentacao" style="MARGIN-BOTTOM: 0px; BORDER-COLLAPSE: collapse" borderColor="#cccccc"
									cellSpacing="0" cellPadding="0" width="975" border="1">
									<TR class="titulo-tabela sem-sublinhado">
										<TD width="8%">Opera��o</TD>
										<TD width="14%">Data da Opera��o</TD>
										<TD width="29%">Segurado</TD>
										<TD width="8%">Tipo</TD>
										<TD width="11%">CPF</TD>
										<TD width="8%">Nascimento</TD>
										<TD width="4%">Sexo</TD>
										<TD noWrap width="9%">Sal�rio (R$)</TD>
										<TD noWrap width="9%">Capital (R$)</TD>
									</TR>
									<TR class="paddingzero">
										<TD colSpan="9">
											<DIV style="OVERFLOW: auto; HEIGHT: 120px" DESIGNTIMEDRAGDROP="750">
												<TABLE class="paddingzero" id="Table2" style="BORDER-COLLAPSE: collapse" borderColor="#cccccc"
													cellSpacing="0" cellPadding="0" border="1">
													<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#ffffff'">
														<TD width="8%" height="15"></TD>
														<TD width="14%" align="center">xx/xx/xxxx</td>
														<TD width="30%">Rodrigo Augusto
														</TD>
														<TD width="8%" height="15">Titular</TD>
														<TD width="11%" height="15">xxx.xxx.xxx-xx</TD>
														<TD width="9%" height="15">xx/xx/xxxx</TD>
														<TD width="4%" height="15">masc.</TD>
														<TD align="right" width="9%" height="15">1.000,00</TD>
														<TD align="right" width="7%">25.000,00</TD>
													</TR> <!--<% if Request.QUeryString("excluir")=1 then %>#AACCFF<% end if %>-->
													<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background=''">
														<TD height="15">
															<asp:Label id="lblAcao" runat="server" ForeColor="Blue">Inclus�o</asp:Label></TD>
														<TD align="center">xx/xx/xxxx</td>
														<TD height="15">
															<asp:ImageButton id="imgPlus" runat="server" ImageUrl="Images/imgMenos.gif" ImageAlign="AbsMiddle"></asp:ImageButton>
															<asp:LinkButton id="btnLuis" runat="server" ForeColor="Black" Font-Underline="True">Jos� Luis de Souza</asp:LinkButton></TD>
														<TD height="15">Titular</TD>
														<TD height="15">xxx.xxx.xxx-xx</TD>
														<TD height="15">xx/xx/xxxx</TD>
														<TD height="15">masc.</TD>
														<TD align="right" height="15">1.000,00</TD>
														<TD align="right" height="15">9.000,00</TD>
													</TR>
													<asp:Panel id="pnlDependente" Visible="False" Runat="server">
														<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #f5f5f5"
															onmouseout="this.style.background='#f5f5f5'">
															<TD>&nbsp;</TD>
															<TD align="center">xx/xx/xxxx</td>
															<TD><SPAN class="grid-sub">Ricardo Vaccani</SPAN></TD>
															<TD>Filho</TD>
															<TD>xxx.xxx.xxx-xx</TD>
															<TD>xx/xx/xxxx</TD>
															<TD>masc.</TD>
															<TD align="right">1.000,00</TD>
															<TD align="right">12.000,00</TD>
														</TR>
														<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #f5f5f5"
															onclick="vidaExcluir(3)" onmouseout="this.style.background='#f5f5f5'">
															<TD>
																<asp:Label id="lblInclusao2" runat="server" Visible="True" ForeColor="Blue">Inclus�o</asp:Label></TD>
															<TD align="center">xx/xx/xxxx</td>
															<TD><SPAN class="grid-sub">Gustavo Modena</SPAN></TD>
															<TD>Filho</TD>
															<TD>xxx.xxx.xxx-xx</TD>
															<TD>xx/xx/xxxx</TD>
															<TD>masc.</TD>
															<TD align="right">1.000,00</TD>
															<TD align="right">9.000,00</TD>
														</TR>
														<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #f5f5f5"
															onmouseout="this.style.background='#f5f5f5'">
															<TD>&nbsp;</TD>
															<TD align="center">xx/xx/xxxx</td>
															<TD><SPAN class="grid-sub">Maria da Silva</SPAN></TD>
															<TD>C�njuge</TD>
															<TD>xxx.xxx.xxx-xx</TD>
															<TD>xx/xx/xxxx</TD>
															<TD>fem.</TD>
															<TD align="right"></TD>
															<TD align="right">17.000,00</TD>
														</TR>
														<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #f5f5f5"
															onmouseout="this.style.background='#f5f5f5'">
															<TD>&nbsp;</TD>
															<TD align="center">xx/xx/xxxx</td>
															<TD><SPAN class="grid-sub">Marcio de Almeida</SPAN></TD>
															<TD>Filho</TD>
															<TD>xxx.xxx.xxx-xx</TD>
															<TD>xx/xx/xxxx</TD>
															<TD>masc.</TD>
															<TD align="right">1.000,00</TD>
															<TD align="right">21.000,00</TD>
														</TR>
													</asp:Panel><% for i as integer = 0  to 100 %>
													<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#ffffff'">
														<TD height="15"></TD>
														<TD align="center">xx/xx/xxxx</td>
														<TD height="15">Rodrigo Augusto</TD>
														<TD height="15">Titular</TD>
														<TD height="15">xxx.xxx.xxx-xx</TD>
														<TD height="15">xx/xx/xxxx</TD>
														<TD height="15">masc.</TD>
														<TD align="right" height="15">1.000,00</TD>
														<TD align="right">25.000,00</TD>
													</TR>
													<% next %>
												</TABLE>
											</DIV>
										</TD>
									</TR>
									<TR style="COLOR: #fff; TEXT-ALIGN: center" bgColor="#003399">
										<TD colSpan="9">
											<TABLE id="Table1" style="COLOR: #ffffff" cellSpacing="1" cellPadding="1" width="100%"
												border="0">
												<TR>
													<TD align="center" width="837">P�gina -&nbsp;&lt;&lt;&nbsp;1 &gt;&gt;</TD>
													<TD noWrap align="right">Total registros: 100</TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
								</TABLE>
							</div>
						</div>
							<asp:Panel id="pnlAcoes" runat="server" HorizontalAlign="Center">
								<asp:Button id="btnIncluir" runat="server" CssClass="Botao" Width="60px" Text="Incluir"></asp:Button>
								<asp:Button id="btnIncluiDependente" runat="server" CssClass="Botao" Width="125px" Visible="False"
									Text="Incluir c�njuge"></asp:Button>
								<asp:Button id="btnAlterar" runat="server" CssClass="Botao" Width="60px" Visible="False" Text="Alterar"></asp:Button>
								<asp:Button id="btnExcluir" runat="server" CssClass="Botao" Width="60px" Visible="False" Text="Excluir"></asp:Button>
								<asp:Button id="btnDesfazAlteracoes" runat="server" CssClass="Botao" Width="258px" Visible="False"
									Text="Desfazer todas altera��es deste subgrupo"></asp:Button>
								<asp:Button id="btnFechaPagina" runat="server" CssClass="Botao" Width="56px" Visible="False"
									Text="Fechar"></asp:Button>
							</asp:Panel><BR>
						</TD>
					</TR>
				</TABLE>
			</asp:panel><BR> <!-- Fim do painel de cadastro --></form>
	</body>
</HTML>
