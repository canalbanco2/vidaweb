Public Class MovimentacaoUmaUm_Form
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnFecharDependente As System.Web.UI.WebControls.Button
    Protected WithEvents Button15 As System.Web.UI.WebControls.Button
    Protected WithEvents btnDepGravar As System.Web.UI.WebControls.Button
    Protected WithEvents ddlDepTipo As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Label25 As System.Web.UI.WebControls.Label
    Protected WithEvents ddlDepSexo As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Label23 As System.Web.UI.WebControls.Label
    Protected WithEvents txtDepNascimento As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label5 As System.Web.UI.WebControls.Label
    Protected WithEvents txtDepCPF As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents txtDepNome As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    Protected WithEvents Label14 As System.Web.UI.WebControls.Label
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents Label6 As System.Web.UI.WebControls.Label
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents pnlCadastroDependente As System.Web.UI.WebControls.Panel
    Protected WithEvents btnDesfazerAlteracao As System.Web.UI.WebControls.Button
    Protected WithEvents btnTitGravar As System.Web.UI.WebControls.Button
    Protected WithEvents txtTitCapital As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label12 As System.Web.UI.WebControls.Label
    Protected WithEvents txtTitSalario As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label13 As System.Web.UI.WebControls.Label
    Protected WithEvents txtTitAdmissao As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label11 As System.Web.UI.WebControls.Label
    Protected WithEvents ddlTitSexo As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Label10 As System.Web.UI.WebControls.Label
    Protected WithEvents txtTitNascimento As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label9 As System.Web.UI.WebControls.Label
    Protected WithEvents txtTitCPF As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label8 As System.Web.UI.WebControls.Label
    Protected WithEvents txtTitNome As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents pnlCadastroTitular As System.Web.UI.WebControls.Panel
    Protected WithEvents lblAcaoIndividual As System.Web.UI.WebControls.Label
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents pnlCadastro As System.Web.UI.WebControls.Panel
    Protected WithEvents btnCancelaTitular As System.Web.UI.WebControls.Button
    Protected WithEvents Label15 As System.Web.UI.WebControls.Label
    Protected WithEvents txtTitDesligamento As System.Web.UI.WebControls.TextBox
    Protected WithEvents img_help_desligamento As System.Web.UI.WebControls.Image
    Protected WithEvents btnTitDesligar As System.Web.UI.WebControls.Button
    Protected WithEvents btnTitApagar As System.Web.UI.WebControls.Button

    

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim requestAcao As String = Request.QueryString("acao")
        btnFecharDependente.Attributes.Add("onclick", "parent.parent.GB_hide();")
        btnCancelaTitular.Attributes.Add("onclick", "parent.parent.GB_hide();")

        If requestAcao = "alterar" Then
            lblAcaoIndividual.Text = "- Alterar"
            pnlCadastroDependente.Visible = False
            pnlCadastro.Visible = True
            pnlCadastroTitular.Visible = True
            'btnIncluiDependente.Visible = True
            txtTitAdmissao.Text = "xx/xx/xxxx"
            txtTitCapital.Text = "9.000,00"
            txtTitCPF.Text = "xxx.xxx.xxx-xx"
            txtTitNascimento.Text = "xx/xx/xxxx"
            txtTitNome.Text = "Luiz Jos� de Souza"
            txtTitSalario.Text = "1.000,00"
            ddlTitSexo.SelectedValue = "masc."
            'campo desligamento s� pode aparecer quando op��o = exlcuir
            txtTitDesligamento.Visible = False
            Label15.Visible = False
            img_help_desligamento.Visible = False
            btnTitDesligar.Visible = False
            btnTitApagar.Visible = False
        ElseIf requestAcao = "incluirdependente" Then
            lblAcaoIndividual.Text = "- Incluir c�njuge"
            pnlCadastro.Visible = True
            'remove tipo de dependente porque sempre ser� conjuge
            Label25.Visible = False
            ddlDepTipo.Visible = False
            '-----------------------------------------------
            pnlCadastroDependente.Visible = True
            pnlCadastroTitular.Visible = False
            txtTitAdmissao.Text = ""
            txtTitCapital.Text = ""
            txtTitCPF.Text = ""
            txtTitNascimento.Text = ""
            txtTitNome.Text = ""
            txtTitSalario.Text = ""
            ddlTitSexo.SelectedValue = "Selecione"

            'Adicionado op��o excluir - 17/01/07
        ElseIf requestAcao = "excluir" Then
            lblAcaoIndividual.Text = "- Excluir"
            pnlCadastroDependente.Visible = False
            pnlCadastro.Visible = True
            pnlCadastroTitular.Visible = True
            txtTitAdmissao.Text = "xx/xx/xxxx"
            txtTitAdmissao.Enabled = False
            txtTitCapital.Text = "9.000,00"
            txtTitCapital.Enabled = False
            txtTitCPF.Text = "xxx.xxx.xxx-xx"
            txtTitCPF.Enabled = False
            txtTitNascimento.Text = "xx/xx/xxxx"
            txtTitNascimento.Enabled = False
            txtTitNome.Text = "Luiz Jos� de Souza"
            txtTitNome.Enabled = False
            txtTitSalario.Text = "1.000,00"
            txtTitSalario.Enabled = False
            ddlTitSexo.SelectedValue = "masc."
            ddlTitSexo.Enabled = False
            '----Exibe campo Data Desligamento e botoes
            txtTitDesligamento.Visible = True
            txtTitDesligamento.Text = "xx/xx/xxxx"
            Label15.Visible = True
            img_help_desligamento.Visible = True
            btnTitDesligar.Visible = True
            btnTitApagar.Visible = True
            btnTitGravar.Visible = False
        Else
            lblAcaoIndividual.Text = "- Incluir"
            pnlCadastro.Visible = True
            'btnIncluiDependente.Visible = False
            pnlCadastroDependente.Visible = False
            pnlCadastroTitular.Visible = True
            txtTitAdmissao.Text = ""
            txtTitCapital.Text = ""
            txtTitCPF.Text = ""
            txtTitNascimento.Text = ""
            txtTitNome.Text = ""
            txtTitSalario.Text = ""
            ddlTitSexo.SelectedValue = "Selecione"
            'campo desligamento s� pode aparecer quando op��o = exlcuir
            txtTitDesligamento.Visible = False
            Label15.Visible = False
            img_help_desligamento.Visible = False
            btnTitDesligar.Visible = False
            btnTitApagar.Visible = False
        End If
    End Sub

End Class
