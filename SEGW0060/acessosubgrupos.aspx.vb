Imports System.Data
Imports System.Web.UI.WebControls

Public Class AcessoSubGrupos
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblVigencia As System.Web.UI.WebControls.Label
    Protected WithEvents lblNavegacao As System.Web.UI.WebControls.Label
    Protected WithEvents btnIncluir As System.Web.UI.WebControls.Button
    Protected WithEvents lblListagem As System.Web.UI.WebControls.Label
    Protected WithEvents grdPesquisa As System.Web.UI.WebControls.DataGrid
    Protected WithEvents ucPaginacao As segw0060.paginacaogrid_sub_grupo

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnIncluir.Attributes.Add("onclick", "return GB_show('Atribuir usu�rio ao subgrupo', '../../AcessoSubGrupos_Form.aspx', 140, 350)")

        If Request.QueryString("alterar") <> "" Then
            Response.Write("<script>window.onload=function(){GB_show('Atribuir usu�rio ao subgrupo', '../../AcessoSubGrupos_Form.aspx?acao=alterar&id=1', 140, 350)}</script>")
        End If

        lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Subgrupo: " & Session("nomeSubgrupo") & " -> " & "Atribuir Usu�rio ao Subgrupo"

        lblVigencia.Text = "<br>" & Session("nomeSubgrupo") & " - " & cUtilitarios.getPeriodoCompetencia(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
        lblVigencia.Visible = True

        Dim alterar As String = Request.QueryString("alterar")
        mConsultaUsuarios(Session("apolice"), Session("ramo"))

    End Sub

    '''Consulta os usu�rios
    Private Sub mConsultaUsuarios(ByVal apolice_id As Integer, ByVal ramo_id As Int16)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        bd.consultar_usuarios_web_seguros_SPS(apolice_id, ramo_id)

        Try

            Dim dt As Data.DataTable = bd.ExecutaSQL_DT

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then

                Me.grdPesquisa.CurrentPageIndex = 0
                Me.grdPesquisa.SelectedIndex = -1

                Me.ucPaginacao._valor = "2"
                Me.ucPaginacao.GridDataBind(Me.grdPesquisa, dt)
                Session("grdDescarte") = dt
            Else
                'Me.Label1.Text = "Nenhum registro encontrado."
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message + ex.StackTrace)

        End Try

    End Sub

#Region "Eventos"
    Private Sub mGridDataBind(ByVal dt As DataTable)
        Me.ucPaginacao.GridDataBind(grdPesquisa, dt)
    End Sub

    Private Sub mSelecionaGrid(ByVal ItemIndex As Integer)
        Me.grdPesquisa.SelectedIndex = ItemIndex
        Me.mGridDataBind(Session("grdDescarte"))
        CType(Me.grdPesquisa.SelectedItem.Cells(0).Controls(1), RadioButton).Checked = True
    End Sub

    Private Sub mPaginacaoClick(ByVal Argumento As String) Handles ucPaginacao.ePaginaAlterada
        Me.grdPesquisa.SelectedIndex = -1
        Select Case Argumento
            Case "pro"
                Me.grdPesquisa.CurrentPageIndex += 1
            Case "ant"
                Me.grdPesquisa.CurrentPageIndex -= 1
            Case Else
                Me.grdPesquisa.CurrentPageIndex = Argumento
        End Select
        Me.ucPaginacao.GridDataBind(grdPesquisa, Session("grdDescarte"))
    End Sub

    Private Sub grdPesquisa_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdPesquisa.ItemDataBound
        'If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
        'CType(e.Item.Cells(0).FindControl("linkCodDoc"), HyperLink).NavigateUrl = "javascript:busca_registro('" & e.Item.Cells(1).Text.Trim & "','" & e.Item.Cells(2).Text.Trim & "','" & e.Item.Cells(4).Text.Trim & "','" & e.Item.Cells(5).Text.Trim & "');"
        'End If
        'If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
        'CType(e.Item.Cells(0).FindControl("linkCodDocDI"), HyperLink).NavigateUrl = "javascript:excluir_DI('" & e.Item.Cells(1).Text.Trim & "','" & e.Item.Cells(2).Text.Trim & "','" & e.Item.Cells(6).Text.Trim & "');"
        'End If
    End Sub

#End Region

End Class
