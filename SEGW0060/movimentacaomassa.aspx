<%@ Page Language="vb" AutoEventWireup="false" Codebehind="MovimentacaoMassa.aspx.vb" Inherits="segw0060.MovimentacaoMassa"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>MovimentacaoMassa</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
			<script>
			function escondeDiv(div){
				var idDiv = document.getElementById(div);
				if (idDiv.style.display==''){
					idDiv.style.display='none';
				}else{
					idDiv.style.display='';
				}
			}
			
			function confirmaModelo(){
					
					//alert('N�o existe layout definido para o envio do arquivo de vidas.\nClique em [ok] para definir o layout.');
					//location.href='Layoutarquivo.aspx';
					//return false;
			}
			</script>
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<P>
				<asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela"></asp:label><BR>
				<asp:Label id="lblVigencia" runat="server" CssClass="Caminhotela"><br>COTEMINAS S.A - Per�odo de compet�ncia: 01/12/2006 at� 31/12/2006</asp:Label></P>
			<P>
				<asp:Panel id="pnlTipodeMovimentacao" runat="server" HorizontalAlign="Left">
					<P class="fundo-azul-claro"><BR>
						<asp:Panel id="escolhaTpMovimentacao" runat="server" HorizontalAlign="Left" Visible="False">
<asp:Label id="lblTipoMovimentacao" runat="server" CssClass="fonte-destaque-azul">Selecione um tipo de movimenta��o:</asp:Label>
<asp:RadioButton id="rdIncremental" runat="server" Font-Size="X-Small" GroupName="TipoMovimentacao"
								Text="Incremental"></asp:RadioButton>
<asp:RadioButton id="rdSobreposicao" runat="server" Font-Size="X-Small" GroupName="TipoMovimentacao"
								Text="Sobreposi��o"></asp:RadioButton>&nbsp;&nbsp; 
<IMG alt="" src="Images/help.gif"> </asp:Panel>
						<asp:Panel id="pnlProcurar" runat="server" CssClass="fundo-azul-claro" Visible="False"><BR>
<asp:Label id="Label2" runat="server" CssClass="Caminhotela2">Clique em "Procurar" para selecionar o arquivo contendo a rela��o de vidas a ser enviada para a Alian�a do Brasil.</asp:Label><BR><BR><INPUT class="Botao2" id="fileProcurar" type="file" size="59" name="fileProcurar" runat="server">&nbsp;&nbsp;<BR><BR><BR>
<DIV style="TEXT-ALIGN: center">
								<asp:Button id="btnImportar" runat="server" CssClass="Botao" Text="Enviar Arquivo" Width="104px"></asp:Button>
								<asp:Button id="btnFechaPagina" runat="server" CssClass="Botao" Visible="False" Text="Fechar"
									Width="56px"></asp:Button></DIV><BR></asp:Panel>
					<P>&nbsp;</P>
				</asp:Panel>
				<asp:Panel id="pnlMovimentacao" runat="server" CssClass="fundo-azul-claro" HorizontalAlign="Center"
					Width="60%" Height="80px" Visible="False">
					<BR>
					<asp:Label id="lblListagemEnviada" runat="server" CssClass="Caminhotela"><br>Seu arquivo foi enviado para a Alian�a do Brasil.<br>&nbsp;<br>Voc� receber� um e-mail da Alian�a do Brasil, no qual ser�o informadas as vidas aceitas e as vidas recusadas.<br>&nbsp;<br>O n�mero do protocolo �: 1850&nbsp;&nbsp;<img src="Images/help.gif"><br>&nbsp;</asp:Label>
				</asp:Panel>
				<asp:Panel id="pnlModelodoArquivo" runat="server" HorizontalAlign="Left" Visible="False">
					<asp:Label id="lblModelo" runat="server" CssClass="Caminhotela">Lista das vidas do arquivo selecionado</asp:Label>
					<BR>
					<DIV align="center">
						<TABLE id="tabelaMovimentacao" style="MARGIN-BOTTOM: 10px; BORDER-COLLAPSE: collapse" borderColor="#cccccc"
							cellSpacing="0" cellPadding="0" width="100%" border="1">
							<TR class="titulo-tabela sem-sublinhado">
								<TD width="266">Segurado</TD>
								<TD width="66">Tipo</TD>
								<TD width="134">CPF</TD>
								<TD width="55">Nascimento</TD>
								<TD width="28">Sexo</TD>
								<TD noWrap width="85">Sal�rio (R$)</TD>
								<TD noWrap width="83">Capital (R$)</TD>
							</TR>
							<TR class="paddingzero">
								<TD colSpan="8">
									<DIV style="OVERFLOW: auto; HEIGHT: 240px" DESIGNTIMEDRAGDROP="807">
										<TABLE class="paddingzero" id="Table2" style="BORDER-COLLAPSE: collapse" borderColor="#cccccc"
											cellSpacing="0" cellPadding="0" width="100%" border="1">
											<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#ffffff'">
												<TD>Rodrigo Augusto
												</TD>
												<TD width="60" height="15">Titular</TD>
												<TD width="108" height="15">xxx.xxx.xxx-xx</TD>
												<TD width="86" height="15">xx/xx/xxxx</TD>
												<TD width="37" height="15">masc.</TD>
												<TD align="right" width="94" height="15">1.000,00</TD>
												<TD align="right" width="76">25.000,00</TD>
											</TR> <!--<% if Request.QUeryString("excluir")=1 then %>#AACCFF<% end if %>-->
											<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background=''">
												<TD height="15">
													<asp:ImageButton id="imgPlus" runat="server" ImageAlign="AbsMiddle" ImageUrl="Images/imgMenos.gif" Visible='false'></asp:ImageButton>Jos� 
													Luis de Souza</TD>
												<TD height="15">Titular</TD>
												<TD height="15">xxx.xxx.xxx-xx</TD>
												<TD height="15">xx/xx/xxxx</TD>
												<TD height="15">masc.</TD>
												<TD align="right" height="15">1.000,00</TD>
												<TD align="right" height="15">9.000,00</TD>
											</TR>
											<asp:Panel id="pnlDependente" Visible="True" Runat="server">
											</asp:Panel>
											<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #FFFFFF"
												onmouseout="this.style.background='#FFFFFF'">
												<TD>Ricardo Vaccani</TD>
												<TD>Filho</TD>
												<TD>xxx.xxx.xxx-xx</TD>
												<TD>xx/xx/xxxx</TD>
												<TD>masc.</TD>
												<TD align="right">1.000,00</TD>
												<TD align="right">12.000,00</TD>
											</TR>
											<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #FFFFFF"
												onclick="vidaExcluir(3)" onmouseout="this.style.background='#f5f5f5'">
												<TD>Gustavo Modena</TD>
												<TD>Filho</TD>
												<TD>xxx.xxx.xxx-xx</TD>
												<TD>xx/xx/xxxx</TD>
												<TD>masc.</TD>
												<TD align="right">1.000,00</TD>
												<TD align="right">9.000,00</TD>
											</TR>
											<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #FFFFFF"
												onmouseout="this.style.background='#FFFFFF'">
												<TD>Maria da Silva</TD>
												<TD>C�njuge</TD>
												<TD>xxx.xxx.xxx-xx</TD>
												<TD>xx/xx/xxxx</TD>
												<TD>fem.</TD>
												<TD align="right">1.000,00</TD>
												<TD align="right">17.000,00</TD>
											</TR>
											<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #FFFFFF"
												onmouseout="this.style.background='#FFFFFF'">
												<TD>Marcio de Almeida</TD>
												<TD>Filho</TD>
												<TD>xxx.xxx.xxx-xx</TD>
												<TD>xx/xx/xxxx</TD>
												<TD>masc.</TD>
												<TD align="right">1.000,00</TD>
												<TD align="right">21.000,00</TD>
											</TR>
											
											<% for i as integer = 0  to 100 %>
											<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#ffffff'">
												<TD height="15">Rodrigo Augusto</TD>
												<TD height="15">Titular</TD>
												<TD height="15">xxx.xxx.xxx-xx</TD>
												<TD height="15">xx/xx/xxxx</TD>
												<TD height="15">masc.</TD>
												<TD align="right" height="15">1.000,00</TD>
												<TD align="right">25.000,00</TD>
											</TR>
											<% next %>
										</TABLE>
									</DIV>
								</TD>
							</TR>
							<TR style="COLOR: #fff; TEXT-ALIGN: center" bgColor="#003399">
								<TD colSpan="9">
									<TABLE id="Table1" style="COLOR: #ffffff" cellSpacing="1" cellPadding="1" width="100%"
										border="0">
										<TR>
											<TD align="center" width="837">P�gina -&nbsp;&lt;&lt;&nbsp;1 &gt;&gt;</TD>
											<TD noWrap align="right">Total registros: 100</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
						<BR>
						<BR>
						Ao clicar no bot�o "Confirmar", voc� estar� confirmando que o layout do arquivo 
						est� correto, bem como as informa��es das vidas.<BR>
						<BR>
						<BR>
						<asp:Button id="btnConfirmar" runat="server" CssClass="Botao" Text="Confirmar"></asp:Button></DIV>
				</asp:Panel><BR>
				<asp:Panel id="pnlMensagemEnviada" runat="server" Visible="False" HorizontalAlign="Center"></asp:Panel>
		</form>
	</body>
</HTML>
