<%@ Register TagPrefix="uc1" TagName="paginacaogrid" Src="paginacaogrid.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManutencaoUsuarios.aspx.vb" Inherits="segw0060.ManutencaoUsuarios"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ManutencaoUsuarios</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
			<LINK media="all" href="scripts/box/box.css" type="text/css" rel="stylesheet">
				<script src="scripts/box/AJS.js" type="text/javascript"></script>
				<script src="scripts/box/box.js" type="text/javascript"></script>
				<script type="text/javascript">
			var GB_IMG_DIR = "scripts/box/";
			GreyBox.preloadGreyBoxImages();
				</script>
				<script>
			function confirmaEnvio(){
					alert("Foi enviada uma senha para o e-mail rsantos@aliancadobrasil.com.br.");
			}
				</script>
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<P><asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela">Label</asp:label><BR>
				<asp:label id="lblVigencia" runat="server" CssClass="Caminhotela"></asp:label></P>
			<P><asp:panel id="Panel1" runat="server">
					<asp:Label id="lblTituloTabela" runat="server" CssClass="Caminhotela">Lista de usu�rios</asp:Label>
					<BR> <!-- In�cio da grid de Usu�rios -->
					<asp:datagrid id="grdPesquisa" runat="server" AllowPaging="True" PageSize="10" AutoGenerateColumns="False"
						CellPadding="3" Width="100%">
						<SelectedItemStyle CssClass="ponteiro"></SelectedItemStyle>
						<AlternatingItemStyle CssClass="ponteiro"></AlternatingItemStyle>
						<ItemStyle CssClass="ponteiro"></ItemStyle>
						<HeaderStyle CssClass="titulo-tabela sem-sublinhado"></HeaderStyle>
						<FooterStyle CssClass="0019GridFooter"></FooterStyle>
						<Columns>
							<asp:BoundColumn DataField="nome" HeaderText="Usu�rio"></asp:BoundColumn>
							<asp:BoundColumn DataField="email" HeaderText="E-Mail"></asp:BoundColumn>
							<asp:BoundColumn DataField="cpf" HeaderText="CPF"></asp:BoundColumn>
							<asp:BoundColumn DataField="login_rede" HeaderText="Login"></asp:BoundColumn>
							<asp:BoundColumn DataField="Acesso" HeaderText="Acesso"></asp:BoundColumn>
							<asp:BoundColumn DataField="Situacao" HeaderText="Situa��o"></asp:BoundColumn>
						</Columns>
						<PagerStyle Visible="false"></PagerStyle>
					</asp:datagrid>
					<uc1:paginacaogrid id="ucPaginacao" runat="server"></uc1:paginacaogrid>
					<BR> <!-- Fim da grid de Usu�rios -->
					<DIV style="TEXT-ALIGN: center"><BR>
						<asp:Button id="btnIncluir" runat="server" CssClass="Botao" Text="Incluir"></asp:Button></DIV>
				</asp:panel>
		</form>
	</body>
</HTML>
