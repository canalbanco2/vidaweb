<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ImprimirCertificados.aspx.vb" Inherits="segw0060.ImprimirCertificado"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ConsultarFaturas</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
			<SCRIPT language="javascript">
		<!--
		function abre_janela(link,nome,resizable,scrollbars,width,height,top,left)
			{  
			nova=window.open(link,nome,
							"resizable="+resizable+",scrollbars="+scrollbars+",width="+width+",height="+height+",top="+top+",left="+left+",toolbar=0,location=0,directories=0,status=0,menubar=0");
			}
		function abreTreeView() {
			document.Form1.showTree.value='1';
		}
		-->
			</SCRIPT>
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
		<input type="hidden" value="" name="showTree" id="showTree">
			<P><asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela">Label</asp:label><BR>
				<asp:label id="lblVigencia" runat="server" CssClass="Caminhotela"><br>COTEMINAS S.A - Per�odo de compet�ncia: 01/12/2006 at� 31/12/2006</asp:label>
			<P><br>
			<TABLE id="Table4" style="BORDER-COLLAPSE: collapse; TEXT-ALIGN: center" borderColor="#cccccc"
							cellSpacing="0" cellPadding="2" width="600" border="0">
				<tr>
					<td align="right">
						<asp:label id="Label6" runat="server" CssClass="Caminhotela">
							Informe o per�odo de compet�ncia:</asp:label>&nbsp;
					</td>
					<td align="left">
						<asp:textbox id="txtDe" runat="server" Width="56px">06/2006</asp:textbox>&nbsp;
						<asp:label id="Label7" runat="server" CssClass="Caminhotela">at�</asp:label>&nbsp;
						<asp:textbox id="txtAte" runat="server" Width="56px">06/2007</asp:textbox>&nbsp;
						<asp:image id="Image2" runat="server" ImageUrl="Images/help.gif"></asp:image>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
				<tr>
					<td align="right">
						<asp:Label id="Label4" runat="server" CssClass="Caminhotela">&nbsp;</asp:Label>
						<asp:Label id="Label2" runat="server" CssClass="Caminhotela">Nome:</asp:Label>&nbsp;					
					</td>
					<td align="left">
						<asp:TextBox id="txtNome" runat="server" Width="173px"></asp:TextBox>						
					</td>
				</tr>
				<tr>
					<td align="right">
						<asp:Label id="Label3" runat="server" CssClass="Caminhotela">CPF:</asp:Label>&nbsp;
					</td>
					<td align="left">
						<asp:TextBox id="txtCPF" runat="server"></asp:TextBox>&nbsp;&nbsp;&nbsp;
						<asp:button id="btnPesquisarPeriodo" runat="server" CssClass="Botao" Width="80px" Text="Pesquisar"></asp:button>						
					</td>
				</tr>
				</table>	
				<br>								
					<asp:panel id="pnlFaturas" runat="server" Visible="False" HorizontalAlign="Justify">  					
					<asp:Label id="lblTituloFatura" CssClass="Caminhotela" Runat="server">Selecione a fatura a consultar</asp:Label>
					<asp:Panel id="pnlTodasFaturas" runat="server" Width="100%" Height="46px">
						<TABLE id="Table4" style="BORDER-COLLAPSE: collapse; TEXT-ALIGN: center" borderColor="#cccccc"
							cellSpacing="0" cellPadding="2" width="600" border="1">
							<TR class="titulo-tabela sem-sublinhado">
								<TD>N�mero&nbsp;Fatura</TD>
								<TD width="183">Compet�ncia da Fatura</TD>
								<TD>Vencimento</TD>
								<TD align="center" width="156">Data do Pagamento</TD>
								<TD align="center">Situa��o</TD>
							</TR> <!--<% if Request.QUeryString("visualizar")<>"" then %>#AACCFF<% end if %>-->
							<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onclick="location.href='?dependentes=1'"
								onmouseout="this.style.background=''">
								<TD height="1">12056</TD>
								<TD width="183" height="1">01/12/06 a 31/12/06</TD>
								<TD height="1">25/12/06</TD>
								<TD align="center" width="156" height="1">-</TD>
								<TD align="center" height="1">Emitida</TD>
							</TR>
							<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#f5f5f5'"
								bgColor="#f5f5f5">
								<TD>12345</TD>
								<TD width="183">01/11/06 a 30/11/06</TD>
								<TD>25/11/06</TD>
								<TD align="center" width="156">24/11/06</TD>
								<TD align="center">Emitida</TD>
							</TR>
							<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#ffffff'">
								<TD>15285</TD>
								<TD width="183">01/10/06 a 31/10/06</TD>
								<TD>25/10/06</TD>
								<TD align="center" width="156">24/10/06</TD>
								<TD align="center">Emitida</TD>
							</TR>
							<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#f5f5f5'"
								bgColor="#f5f5f5">
								<TD>17456</TD>
								<TD width="183">01/09/06 a 30/09/06</TD>
								<TD>25/09/06</TD>
								<TD align="center" width="156">24/09/06</TD>
								<TD align="center">Cancelada</TD>
							</TR>
							<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#ffffff'">
								<TD>5853</TD>
								<TD width="183">01/08/06 a 31/08/06</TD>
								<TD>25/08/06</TD>
								<TD align="center" width="156">24/08/06</TD>
								<TD align="center">Emitida</TD>
							</TR>
							<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#f5f5f5'"
								bgColor="#f5f5f5">
								<TD>11458</TD>
								<TD width="183">01/07/06 a 31/07/06</TD>
								<TD>25/07/06</TD>
								<TD align="center" width="156">24/07/06</TD>
								<TD align="center">Emitida</TD>
							</TR>
							<TR class="fonte-branca" bgColor="#003399">
								<TD align="center" colSpan="5">
									<TABLE id="Table5" style="COLOR: #ffffff" cellSpacing="1" cellPadding="1" width="100%"
										border="0">
										<TR>
											<TD align="center" width="837">P�gina -&nbsp;&lt;&lt;&nbsp;1 &gt;&gt;</TD>
											<TD noWrap align="right">Total registros: 100</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</asp:Panel>
				</asp:panel><asp:panel id="pnlFiltro" runat="server" Visible="False"><BR>
					<TABLE id="Table2" style="BORDER-RIGHT: #ccc 1px solid; BORDER-TOP: #ccc 1px solid; BORDER-LEFT: #ccc 1px solid; BORDER-BOTTOM: #ccc 1px solid"
						height="88" cellSpacing="1" cellPadding="1" width="600" border="0">
						<TR>
							<TD align="right" width="25%">
								<asp:Label id="Label1" runat="server" CssClass="Caminhotela">Opera��o:</asp:Label></TD>
							<TD width="25%">
								<asp:DropDownList id="ddlFiltros" runat="server" AutoPostBack="True">
									<asp:ListItem Value="Todos">Todos</asp:ListItem>
									<asp:ListItem Value="Inclus&#245;es">Inclus&#245;es</asp:ListItem>
									<asp:ListItem Value="Exclus&#245;es">Exclus&#245;es</asp:ListItem>
									<asp:ListItem Value="Altera&#231;&#227;o de Capital">Altera&#231;&#227;o de Capital</asp:ListItem>
									<asp:ListItem Value="Acertos">Acertos</asp:ListItem>
									<asp:ListItem Value="Cart&#227;o Proposta Pendente">DPS Pendente</asp:ListItem>
								</asp:DropDownList></TD>
							<TD align="center" width="25%">								
							<TD align="right" width="25%">								
							<TD width="25%">
							</TD>
						</TR>
						<TR>
							<TD align="right" width="80"></TD>
							<TD width="212"></TD>
							<TD align="right"></TD>
							<TD align="right"></td>							
							<TD>
							</TD>
						</TR>
						<TR>
							<TD align="right" width="80" colSpan="4"></TD>
							<TD align="right" width="80"></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="5">
								<asp:Button id="btnPesquisarCertificados" runat="server" CssClass="Botao" Width="81px" Text="Pesquisar"></asp:Button></TD>
						</TR>
					</TABLE>
				</asp:panel>
				<asp:panel id="pnlFaturaDetalhe" runat="server" Visible="False" HorizontalAlign="Left">
					<P>
						<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD width="60%" height="5">
									<asp:Label id="Label8" runat="server" CssClass="Caminhotela">Lista das vidas da fatura 12056 - Pesquisado por: Todos</asp:Label></TD>
								<TD align="right" height="5"></TD>
							</TR>
							<TR>
								<TD colSpan="2">
									<DIV align="center">
										<TABLE id="tabelaMovimentacao" style="MARGIN-BOTTOM: 10px; BORDER-COLLAPSE: collapse" borderColor="#cccccc"
											cellSpacing="0" cellPadding="0" width="100%" border="1">
											<TR class="titulo-tabela sem-sublinhado">
												<TD width="36"></TD>
												<TD width="266">Segurado</TD>
												<TD width="66">Tipo</TD>
												<TD width="142">CPF</TD>
												<TD width="55">Nascimento</TD>
												<TD width="28">Sexo</TD>
												<TD noWrap width="85">Sal�rio (R$)</TD>
												<TD noWrap width="83">Capital (R$)</TD>
											</TR>
											<TR class="paddingzero" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px">
												<TD colSpan="9" style="padding:0px">
													<DIV style="OVERFLOW: auto; HEIGHT: 240px" DESIGNTIMEDRAGDROP="807">
														<TABLE class="paddingzero" id="Table3" style="BORDER-COLLAPSE: collapse" borderColor="#cccccc"
															cellSpacing="0" cellPadding="0" width="100%" border="1">
															<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#ffffff'">
																<TD width="25" height="15">
																	<asp:CheckBox id="CheckBox1" runat="server"></asp:CheckBox></TD>
																<TD>Rodrigo Augusto
																</TD>
																<TD width="57" height="15">Titular</TD>
																<TD width="106" height="15">xxx.xxx.xxx-xx</TD>
																<TD width="83" height="15">xx/xx/xxxx</TD>
																<TD width="35" height="15">masc.</TD>
																<TD align="right" width="91" height="15">1.000,00</TD>
																<TD align="right" width="71">25.000,00</TD>
															</TR> <!--<% if Request.QUeryString("excluir")=1 then %>#AACCFF<% end if %>-->
															<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background=''">
																<TD width="10" height="15">
																	<asp:CheckBox id="CheckBox2" runat="server"></asp:CheckBox></TD>
																<TD height="15">
																	<asp:ImageButton id="imgPlus" runat="server" ImageUrl="Images/imgMenos.gif" ImageAlign="AbsMiddle"></asp:ImageButton>
																	Jos� Luis de Souza
																</TD>
																<TD height="15">Titular</TD>
																<TD height="15">xxx.xxx.xxx-xx</TD>
																<TD height="15">xx/xx/xxxx</TD>
																<TD height="15">masc.</TD>
																<TD align="right" height="15">1.000,00</TD>
																<TD align="right" height="15">9.000,00</TD>
															</TR>
															<asp:Panel id="pnlDependente" Visible="False" Runat="server">
															<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #f5f5f5"
																onmouseout="this.style.background='#f5f5f5'">
																<TD width="10">
																	<asp:CheckBox id="CheckBox5" runat="server"></asp:CheckBox></TD>
																<TD>&nbsp;&nbsp;&nbsp;Maria da Silva</TD>
																<TD>C�njuge</TD>
																<TD>xxx.xxx.xxx-xx</TD>
																<TD>xx/xx/xxxx</TD>
																<TD>fem.</TD>
																<TD align="right"></TD>
																<TD align="right">17.000,00</TD>
															</TR>
															</asp:Panel>
															<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #FFFFFF"
																onmouseout="this.style.background='#FFFFFF'">
																<TD width="10">
																	<asp:CheckBox id="CheckBox3" runat="server"></asp:CheckBox></TD>
																<TD>Ricardo Vaccani</TD>
																<TD>Filho</TD>
																<TD>xxx.xxx.xxx-xx</TD>
																<TD>xx/xx/xxxx</TD>
																<TD>masc.</TD>
																<TD align="right">1.000,00</TD>
																<TD align="right">12.000,00</TD>
															</TR>
															<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #FFFFFF"
																onclick="vidaExcluir(3)" onmouseout="this.style.background='#FFFFFF'">
																<TD width="10">
																	<asp:CheckBox id="CheckBox4" runat="server"></asp:CheckBox></TD>
																<TD>Gustavo Modena</TD>
																<TD>Filho</TD>
																<TD>xxx.xxx.xxx-xx</TD>
																<TD>xx/xx/xxxx</TD>
																<TD>masc.</TD>
																<TD align="right">1.000,00</TD>
																<TD align="right">9.000,00</TD>
															</TR>															
															<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #FFFFFF"
																onmouseout="this.style.background='#FFFFFF'">
																<TD width="10">
																	<asp:CheckBox id="CheckBox6" runat="server"></asp:CheckBox></TD>
																<TD>Marcio de Almeida</TD>
																<TD>Filho</TD>
																<TD>xxx.xxx.xxx-xx</TD>
																<TD>xx/xx/xxxx</TD>
																<TD>masc.</TD>
																<TD align="right">1.000,00</TD>
																<TD align="right">21.000,00</TD>
															</TR>															
															<% for i as integer = 0  to 100 %>
															<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#ffffff'">
																<TD width="10" height="15">
																	<asp:CheckBox id="CheckBox7" runat="server"></asp:CheckBox></TD>
																<TD height="15">Rodrigo Augusto</TD>
																<TD height="15">Titular</TD>
																<TD height="15">xxx.xxx.xxx-xx</TD>
																<TD height="15">xx/xx/xxxx</TD>
																<TD height="15">masc.</TD>
																<TD align="right" height="15">1.000,00</TD>
																<TD align="right">25.000,00</TD>
															</TR>
															<% next %>
														</TABLE>
													</DIV>
												</TD>
											</TR>
											<TR style="COLOR: #fff; TEXT-ALIGN: center" bgColor="#003399">
												<TD colSpan="10">
													<TABLE id="Table1" style="COLOR: #ffffff" cellSpacing="1" cellPadding="1" width="100%"
														border="0">
														<TR>
															<TD align="center" width="837">P�gina -&nbsp;&lt;&lt;&nbsp;1 &gt;&gt;</TD>
															<TD noWrap align="right">Total registros: 100</TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</DIV>
								</TD>
							</TR>
							<TR>
								<TD colSpan="2"></TD>
							</TR>
						</TABLE>
						<DIV align="center">
							<asp:Button id="btnImprimeCertificado" runat="server" CssClass="Botao" Width="133px" Text="Imprimir Certificado"></asp:Button>
							<asp:Button id="btnFechaPagina" runat="server" CssClass="Botao" Width="56px" Text="Fechar" Visible="False"></asp:Button>
							<asp:Button id="btnMarcaTodos" runat="server" CssClass="Botao" Width="159px" Text="Marcar/Desmarcar Todos"></asp:Button></DIV>
				</asp:panel>
			<P></P>
			<P></P>
		</form>
	</body>
</HTML>
