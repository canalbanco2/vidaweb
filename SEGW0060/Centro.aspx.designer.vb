'------------------------------------------------------------------------------
' <gerado automaticamente>
'     Este c�digo foi gerado por uma ferramenta.
'
'     As altera��es ao arquivo poder�o causar comportamento incorreto e ser�o perdidas se
'     o c�digo for recriado
' </gerado automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Centro

    '''<summary>
    '''Controle Form1.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''Controle lblNavega.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblNavega As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblVigencia.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblVigencia As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle pnlApresentacao.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents pnlApresentacao As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Controle lblSaudacao.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblSaudacao As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblUsuario.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblUsuario As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblDados.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblDados As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblCNPJ.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblCNPJ As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblEmpresa.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblEmpresa As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblStatus.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblStatus As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblEndereco.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblEndereco As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblComplemento.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblComplemento As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblBairro.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblBairro As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblCEP.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblCEP As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblCidade.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblCidade As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblUF.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblUF As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblTextEmail.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblTextEmail As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblDDD.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblDDD As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblTelefone.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblTelefone As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblEmail.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblEmail As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblSubgrupos.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblSubgrupos As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle ulSubGrupos.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents ulSubGrupos As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Controle pnlLogAcesso.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents pnlLogAcesso As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Controle tbUltimosAcessos.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents tbUltimosAcessos As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Controle pnInfoComplementar.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents pnInfoComplementar As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Controle tbInfo.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents tbInfo As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Controle pnlCheckList.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents pnlCheckList As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Controle linkPorArquivo.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents linkPorArquivo As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Controle linkMovOnline.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents linkMovOnline As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Controle contentEmail.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents contentEmail As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblPrazo.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblPrazo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle pnlBtnVoltar.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents pnlBtnVoltar As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Controle btnVoltar.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnVoltar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Controle lblAviso.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblAviso As Global.System.Web.UI.WebControls.Label
End Class
