Public Class AcessoSubGrupos_Form
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnExcluir As System.Web.UI.WebControls.Button
    Protected WithEvents btnIncluirSubgrupo As System.Web.UI.WebControls.Button
    Protected WithEvents ddlUsuarios As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlSubGrupo As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblTituloSuBgrupo As System.Web.UI.WebControls.Label
    Protected WithEvents pnlSubGrupos As System.Web.UI.WebControls.Panel
    Protected WithEvents btnCancelar As System.Web.UI.WebControls.Button
    Protected WithEvents RequiredFieldValidator1 As System.Web.UI.WebControls.RequiredFieldValidator

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        If Page.IsPostBack Then
            Dim cpf As String = Request.Form("ddlUsuarios")

            If Request.Form("getAcao") = 1 Then

                bd.insere_usuario_apolice_spi(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cpf, Session("usuario"))

            End If

            If Request.Form("getAcao") = 2 Then

                bd.delete_usuario_apolice_spu(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cpf, Session("usuario"))

            End If

            Try
                bd.ExecutaSQL_DR()

                If Request.Form("getAcao") = 1 Then
                    Response.Write("<script>alert('Foi cadastrado o usu�rio """ & ddlUsuarios.SelectedValue & """ para o subgrupo """ & ddlSubGrupo.SelectedValue & """')</script>")                    
                Else
                    Response.Write("<script>alert('Foi retirado o usu�rio """ & ddlUsuarios.SelectedValue & """ do subgrupo """ & ddlSubGrupo.SelectedValue & """')</script>")
                End If
                Response.Write("<script>parent.parent.window.location=parent.parent.window.location;</script>")
            Catch ex As Exception
                Throw New Exception(ex.Message + ex.StackTrace)

            End Try


        End If

        btnIncluirSubgrupo.Attributes.Add("onclick", "insereUsuario();")
        btnExcluir.Attributes.Add("onclick", "excluirUsuario();")

        btnCancelar.Attributes.Add("onclick", "parent.parent.GB_hide();")

        Dim requestAcao As String = Request.QueryString("acao")

        If Not IsPostBack Then
            If requestAcao = "alterar" Then
                Me.getSubgrupos(Session("apolice"), Session("ramo"), Request.QueryString("subgrupo"))
                Me.getUsuarios(Request.QueryString("cpf"))
                Me.ddlSubGrupo.Enabled = False
                Me.ddlUsuarios.Enabled = False
                Me.btnIncluirSubgrupo.Visible = False
            Else
                Me.getSubgrupos(Session("apolice"), Session("ramo"), "")
                Me.getUsuarios("")
                Me.btnExcluir.Visible = False
            End If

        End If

    End Sub

    '    Private Sub btnIncluirSubgrupo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Response.Write("<script>alert('Foi cadastrado o usu�rio """ & ddlUsuarios.SelectedValue & """ para o subgrupo """ & ddlSubGrupo.SelectedValue & """')</script>")
    '   End Sub
    Private Sub getSubgrupos(ByVal apolice_id As String, ByVal ramo_id As String, ByVal selected As String)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        bd.consulta_subgrupo_sps(apolice_id, ramo_id)

        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

        Dim count As Int16 = 0
        Dim text As String
        Dim value As String

        Try

            While dr.Read()

                value = dr.GetValue(0).ToString()
                text = dr.GetValue(1).ToString()

                Me.ddlSubGrupo.Items.Add(text)
                Me.ddlSubGrupo.Items(count).Value = value

                If value = selected Then
                    Me.ddlSubGrupo.SelectedIndex = count
                End If

                count = count + 1
            End While
            dr.Close()
            dr = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message + ex.StackTrace)

        End Try
    End Sub
    Private Sub getUsuarios(ByVal selected As String)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        bd.consultar_usuarios_web_seguros_SPS()

        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

        Dim count As Int16 = 0
        Dim text As String
        Dim value As String

        Try

            While dr.Read()

                value = dr.GetValue(0).ToString()
                text = dr.GetValue(1).ToString()

                Me.ddlUsuarios.Items.Add(text)
                Me.ddlUsuarios.Items(count).Value = value

                If value = selected Then
                    Me.ddlUsuarios.SelectedIndex = count
                End If

                count = count + 1
            End While
            dr.Close()
            dr = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message + ex.StackTrace)

        End Try
    End Sub
End Class
