<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AjudaOpcao.aspx.vb" Inherits="segw0060.AjudaOpcao"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Ajuda</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<div id="cabecAjuda"><IMG src="Images/ajuda-1.gif">
			</div>
			<P>&nbsp;</P>
			<TABLE id="Table1" height="61" cellSpacing="0" cellPadding="0" width="94%" align="center"
				border="0">
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Este menu apresenta as principais op��es dispon�veis no sistema, conforme descrito:</span>
						<br>
						<br>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Manuten��o de Usu�rio</span>
						<span class="ajudaItem">- Fun��o destinada � administra��o de usu�rios do sistema. (Link n�o dispon�vel para usu�rios do n�vel operador).</span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Movimenta��o on-line</span>
						<span class="ajudaItem">- Acessa a tela com as op��es de movimenta��o vida a vida, ou seja, cada segurado deve ser inclu�do/alterado/exclu�do individualmente. Normalmente esta op��o � utilizada por estipulantes com baixo volume movimenta��es de vidas em cada per�odo de compet�ncia da fatura.</span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Movimenta��o por Arquivo</span>
						<span class="ajudaItem">- Acessa a tela com as op��es de movimenta��es em massa, ou seja, o estipulante envia para a Alian�a do Brasil um arquivo eletronico contendo todas as pessoas que devem ser inclu�das, exclu�das ou alteradas. Normalmente esta op��o � utilizada por estipulantes com alto volume movimenta��es de vidas em cada per�odo de compet�ncia da fatura.</span>
					</TD>
				</TR>
				<TR>
					<td width="5%"> &nbsp </td>
					<TD>
						<span class="ajudaTituloItem">Configurar layout</span>
						<span class="ajudaItem">- Nesta tela � poss�vel informar qual ser� a ordem dos campos do arquivo eletr�nico que ser� enviado para a Alian�a do Brasil. Esta funcionalidade reduz a necessidade do usu�rio de realizar qualquer altera��o na vers�o de arquivo eletr�nico que ele j� possua. Neste local, tamb�m, o usu�rio definir� outros par�metros para que o sistema consiga capturar de forma correta as informa��es do arquivo eletr�nico a ser enviado.</span>
					</TD>
				</TR>
				<TR>
					<td width="5%"> &nbsp </td>
					<TD>
						<span class="ajudaTituloItem">Enviar arquivo para an�lise</span>
						<span class="ajudaItem">- Esta op��o � utilizada para que o sistema fa�a a leitura do arquivo a ser enviado, conforme estipulado na op��o de Configura��o de Layout. Ap�s envio para Alian�a do Brasil o arquivo ser� analisado a fim de identificar sua consist�ncia.</span>
					</TD>
				</TR>
				<TR>
					<td width="5%"> &nbsp </td>
					<TD>
						<span class="ajudaTituloItem">Resultado Processamento</span>
						<span class="ajudaItem">- Ap�s o envio do arquivo e an�lise da Alian�a do Brasil ser� apresentada nessa tela, caso haja, as inconsist�ncias encontradas durante o processamento do arquivo. O Operador ter� aqui op��es para ajustar as inconsist�ncias existentes manualmente, optar por um novo envio ou ainda utilizar as op��es de Movimenta��o on-line.</span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem"><br>Obs.: Enquanto n�o encerrar a movimenta��o do per�odo de compet�ncia, o Operador poder� utilizar ambas as formas de Movimenta��o de Vidas.<br><br></span>
					</TD>
				</TR>
				
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Consulta</span>
						<span class="ajudaItem">- Links para visualiza��o de informa��es pertinentes � ap�lice ou fatura.</span>
					</TD>
				</TR>
				<TR>
					<td width="5%"> &nbsp </td>
					<TD>
						<span class="ajudaTituloItem">Certificado individual</span>
						<span class="ajudaItem">- Fun��o de impress�o dos Certificados Individuais que podem ser entregues aos segurados (titular e dependentes).</span>
					</TD>
				</TR>
				<TR>
					<td width="5%"> &nbsp </td>
					<TD>
						<span class="ajudaTituloItem">Dados do subgrupo</span>
						<span class="ajudaItem">- Consulta das principais informa��es do subgrupo, como data de vencimento, limites de coberturas, dentre outras informa��es.</span>
					</TD>
				</TR>
				<TR>
					<td width="5%"> &nbsp </td>
					<TD>
						<span class="ajudaTituloItem">Extrato da Movimenta��o</span>
						<span class="ajudaItem">- Consulta das opera��es realizadas na ap�lice durante o per�odo de compet�ncia da fatura atual.</span>
					</TD>
				</TR>
				<TR>
					<td width="5%"> &nbsp </td>
					<TD>
						<span class="ajudaTituloItem">Fatura Anterior</span>
						<span class="ajudaItem">- Consulta dos dados das faturas anteriores, bem como fun��o de impress�o de boletos n�o pagos, de resumos de faturas e de rela��es de vidas anteriores � vig�ncia atual.</span>
					</TD>
				</TR>
				<TR>
					<td width="5%"> &nbsp </td>
					<TD>
						<span class="ajudaTituloItem">Hist�rico e pr�ximo passo</span>
						<span class="ajudaItem">- Tem por objetivo auxiliar o Operador mostrando qual o pr�ximo passo que deve ser seguido e quais etapas do faturamento j� foram cumpridas.</span>
					</TD>
				</TR>
				<TR>
					<td width="5%"> &nbsp </td>
					<TD>
						<span class="ajudaTituloItem">Download de Formul�rio</span>
						<span class="ajudaItem">- Atalhos para que o Estipulante imprima os modelos de documentos necess�rios durante o processo de faturamento.</span>
					</TD>
				</TR>
				<TR>
					<td width="5%"> &nbsp </td>
					<TD>
						<span class="ajudaTituloItem">Pend�ncias</span>
						<span class="ajudaItem">- Mostra para o usu�rio as pend�ncias junto � Alian�a do Brasil, referentes ao processo de faturamento, que devem ser sanadas pelo Estipulante. (quando existir pend�ncias a palavra �Pend�ncias� estar� em vermelho).</span>
					</TD>
				</TR>
				
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Carregar Rela��o Anterior</span>
						<span class="ajudaItem">- Desfaz todas as movimenta��es efetuadas pelo Operador dentro da vig�ncia atual, retornando a posi��o da rela��o de vidas ao �ltimo faturamento efetuado.</span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Encerrar movimenta��o</span>
						<span class="ajudaItem">- Fun��o destinada ao Administrador de Subgrupo para que conclua a movimenta��o do per�odo de vig�ncia da fatura atual e envie para an�lise da Alian�a do Brasil.</span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem"><br>Obs.: Ap�s encerramento da movimenta��o ser�o poss�veis movimenta��es apenas no pr�ximo per�odo de vig�ncia.<br><br></span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Trocar ap�lice/subgrupo</span>
						<span class="ajudaItem">- Trocar ap�lice/subgrupo � Fecha o subgrupo atual e volta � tela inicial, onde ser� poss�vel escolher uma outra ap�lice ou subgrupo a ser movimentado, se houver.</span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Sair</span>
						<span class="ajudaItem">- Fecha o sistema.</span>
					</TD>
				</TR>
			</TABLE>
			<BR>
			<P></P>
			<P></P>
		</form>
	</body>
</HTML>
