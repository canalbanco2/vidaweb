Public Class ConsultarFaturas
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblVigencia As System.Web.UI.WebControls.Label
    Protected WithEvents lblNavegacao As System.Web.UI.WebControls.Label
    Protected WithEvents btnFatura As System.Web.UI.WebControls.Button
    Protected WithEvents btnRelacaoVidas As System.Web.UI.WebControls.Button
    Protected WithEvents btnResumo As System.Web.UI.WebControls.Button
    Protected WithEvents Label6 As System.Web.UI.WebControls.Label
    Protected WithEvents txtDe As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    Protected WithEvents txtAte As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnListar As System.Web.UI.WebControls.Button
    Protected WithEvents Image2 As System.Web.UI.WebControls.Image
    Protected WithEvents Label5 As System.Web.UI.WebControls.Label
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents pnlFaturas As System.Web.UI.WebControls.Panel
    Protected WithEvents lblTituloFatura As System.Web.UI.WebControls.Label
    Protected WithEvents pnlTodasFaturas As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlDetalhesFatura As System.Web.UI.WebControls.Panel
    Protected WithEvents Label8 As System.Web.UI.WebControls.Label
    Protected WithEvents Label9 As System.Web.UI.WebControls.Label
    Protected WithEvents ddlFiltros As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblAcao As System.Web.UI.WebControls.Label
    Protected WithEvents imgPlus As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnLuis As System.Web.UI.WebControls.Label
    Protected WithEvents pnlDependente As System.Web.UI.WebControls.Panel
    Protected WithEvents Label15 As System.Web.UI.WebControls.Label
    Protected WithEvents chkLista As System.Web.UI.WebControls.CheckBoxList
    Protected WithEvents btnImprimir As System.Web.UI.WebControls.Button
    Protected WithEvents btnFechaPagina As System.Web.UI.WebControls.Button
    Protected WithEvents ImageButton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Label10 As System.Web.UI.WebControls.Label
    Protected WithEvents pnlFiltro As System.Web.UI.WebControls.Panel
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button
    Protected WithEvents Label11 As System.Web.UI.WebControls.Label
    Protected WithEvents Label12 As System.Web.UI.WebControls.Label
    Protected WithEvents Label13 As System.Web.UI.WebControls.Label
    Protected WithEvents DropDownList1 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents TextBox2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label14 As System.Web.UI.WebControls.Label
    Protected WithEvents DropDownList2 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Button2 As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim lista As String
        lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Subgrupo: " & Session("subGrupo") & " -> " & "Imprimir / Consultar Faturas Anteriores"
        lblVigencia.Visible = True


        For i As Integer = 0 To chkLista.Items.Count - 1

            If chkLista.Items(i).Selected = True Then
                If lista <> "" Then
                    lista &= ","
                End If
                lista &= chkLista.Items(i).Value
            End If
        Next

        btnImprimir.Attributes.Add("onclick", "abre_janela('Impressao.aspx?tipo=" & lista & "','janela_msg','no','yes','838','400','','')")
        Dim visualizar As String = Request.QueryString("visualizar")

        If visualizar <> "" Then

            pnlFaturas.Visible = True

            pnlTodasFaturas.Visible = True
            pnlDetalhesFatura.Visible = True

        End If

    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        pnlFaturas.Visible = True
        pnlTodasFaturas.Visible = True
        pnlDetalhesFatura.Visible = False
    End Sub

    Private Sub btnFechaPagina_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("Centro.aspx")
    End Sub



    Private Sub imgPlus_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPlus.Click
        If pnlDependente.Visible = True Then

            imgPlus.ImageUrl = "Images/imgMais.gif"
            pnlDependente.Visible = False
        Else
            imgPlus.ImageUrl = "Images/imgMenos.gif"
            pnlDependente.Visible = True
        End If
    End Sub
End Class
