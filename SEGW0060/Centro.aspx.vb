Imports Alianca.Seguranca.BancoDados
Imports System.web.UI.WebControls
Imports System.Data.SqlClient

Partial Class Centro
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents botaoVoltar As System.Web.UI.WebControls.Button
    Protected WithEvents lblNavegacao As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim str As String = "&"
        'Recupera todas as informa��es da session
        If (Session.Keys.Count = 0) Then
            Exit Sub
        End If
        For Each m As String In Session.Keys
            If Not IsNothing(Session(m)) Then
                If Session(m).GetType.ToString = "System.String" Then
                    str &= System.Web.HttpUtility.UrlEncode(m) & "=" & System.Web.HttpUtility.UrlEncode(Session(m)) & "&"
                End If
            End If
        Next

        Dim mov_online As String
        Dim linkseguro As New Alianca.Seguranca.Web.LinkSeguro

        mov_online = linkseguro.GerarParametro("", Alianca.Seguranca.Web.LinkSeguro.TempoExpiracao.Grande, Request.ServerVariables("REMOTE_ADDR"), Session("usuario_id"))
        'Implementa o Link para a movimenta��o on-line com o LinkSeguro
        linkMovOnline.NavigateUrl = "../segw0065/MovimentacaoUmaUm.aspx?SLinkSeguro=" & mov_online & "&expandir=1" & str
        'Implementa o Link para a movimenta��o por arquivo com o LinkSeguro (adicionado em 29/03/07)

        btnVoltar.Attributes.Add("onclick", "top.exibeaguarde();return telaInicial();")

        Dim trocar As String = Request.QueryString("trocar")
        Dim area As String = Request.QueryString("area")
        Dim requestSubGrupo As String = Request.QueryString("subgrupo")

        Dim requestVoltar As String = Request.Form("voltar")

        If requestVoltar = 1 Then
            'Session.Abandon()
            'Response.Write("<script>parent.document.Form1.submit();</script>")
            Response.Write("<script>parent.document.getElementById(""Form1"").submit();</script>")
        End If

        If trocar <> "" Then
            Session.Abandon()
            Response.Write("<script>parent.window.reload()</script>")
        End If

        If requestSubGrupo <> "" Then

            Session("nomeSubgrupo") = Request.QueryString("nomeSubgrupo")
            Session("subgrupo_id") = requestSubGrupo
            Session("acesso") = Request.QueryString("acesso")

            mLogaAcesso(Session("apolice"), Session("subgrupo_id"), Session("ramo"), Session("usuario"))

            Response.Write("<script>parent.window.location=parent.window.location;</script>")
        End If

        If Session("infoConjuge") = "" Then
            Session.Add("infoConjuge", "N�o")
        End If

        If (Session("apolice") = "303030") Then
            Session("infoConjuge") = "N�o"
        Else
            Session("infoConjuge") = "Facultativo"
        End If


        If CStr(Session("subgrupo_id")) <> "" And Session("apolice") <> "" And Session("ramo") <> "" Then


            lblNavega.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomesubGrupo") & "<br>Fun��o: Hist�rico e pr�ximos passos"
            lblVigencia.Text = "<br>" & cUtilitarios.getPeriodoCompetencia(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

            lblNavega.Visible = True
            pnlCheckList.Visible = True
            lblAviso.Visible = True
            pnlLogAcesso.Visible = False
            pnlBtnVoltar.Visible = False
            pnlApresentacao.Visible = False
            lblVigencia.Visible = True
            pnInfoComplementar.Visible = False
            'Exibe o prazo restante 
            mPreenchePrazoRestante(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

            'Exibe os email enviados e hist�ricos
            mPreencheHistoricoFatura()

            cUtilitarios.escreveScript("top.exibePerfil('" & Session("acesso") & "')")

        ElseIf CStr(Session("subgrupo_id")) = "" And Session("apolice") <> "" And Session("ramo") <> "" And IsNumeric(Session("apolice")) Then
            lblVigencia.Text = ""
            lblVigencia.Visible = False
            'lblNavegacao.Visible = True
            pnlCheckList.Visible = False
            lblAviso.Visible = False
            pnlLogAcesso.Visible = False
            pnlApresentacao.Visible = True
            pnInfoComplementar.Visible = False

        Else
            'lblNavegacao.Visible = True
            lblVigencia.Visible = False
            pnlCheckList.Visible = False
            lblAviso.Visible = False
            pnlApresentacao.Visible = False
            pnlLogAcesso.Visible = True
            pnInfoComplementar.Visible = True

            Me.mConfigLogAcesso()
            pnlBtnVoltar.Visible = False

        End If

        If pnlApresentacao.Visible = True Then

            If IsNumeric(Session("apolice")) Then
                mPreencheDadosBasicosApolice(Session("apolice"), Session("ramo"))
            End If

        End If

        Try
            If Session("usuario_id").ToString = "" Then
                cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
                Response.End()
            End If
        Catch ex As Exception
            cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
            Response.End()
        End Try

        ' Projeto 539202 - Jo�o Ribeiro - 27/04/2009
        'If Session("acesso") <> 1 And Session("acesso") <> 4 And Session("acesso") <> 6 Then
        If Session("acesso") = 2 Or Session("acesso") = 3 Or Session("acesso") = 5 Then

            linkMovOnline.Attributes.Add("onclick", "alert('Voc� n�o tem permiss�o para realizar a Movimenta��o on-line');")
            linkMovOnline.NavigateUrl = "#"
            linkMovOnline.Target = "_self"

            'linkPorArquivo.Attributes.Add("onclick", "alert('Voc� n�o tem permiss�o para realizar a Movimenta��o Por Arquivo');")
			linkPorArquivo.Attributes.Add("onclick", "$.post('ajaxLayout.aspx',{op:'layout'},function(data){	" _
                                                     & "      if(data.split('|')[0] == '0'){" _
                                                     & "		    alert('Para movimenta��o por arquivo � necess�rio configurar o layout anteriormente.');" _
                                                     & "	    }else{" _
                                                     & "          if(data.split('|')[0] == '1'){" _
                                                     & " " & linkPorArquivo.Attributes.Item("onclick") & "; top.exibeaguarde('linkPorArquivo');" _
                                                     & "              window.IFrameConteudoPrincipal.location = '../segw0066/MovimentacaoMassa.aspx?SLinkSeguro=" & mov_online & "&expandir=1" & str & "';" _
                                                     & "	        }else{" _
                                                     & "              alert('Se voc� solicitou endosso de inclus�o de vidas acima do limite de idade \n ou realizou movimenta��o no modo on-line, n�o ser� poss�vel importar o \n arquivo nesta compet�ncia. Favor prosseguir na movimenta��o on-line.');" _
                                                     & "          }}" _
                                                     & "      });")
            
            linkPorArquivo.NavigateUrl = "#"
            linkPorArquivo.Target = "_self"

        Else

            linkMovOnline.NavigateUrl = "../segw0065/MovimentacaoUmaUm.aspx?SLinkSeguro=" & mov_online & "&expandir=1" & str
            linkPorArquivo.NavigateUrl = "../segw0066/MovimentacaoMassa.aspx?SLinkSeguro=" & mov_online & "&expandir=1" & str

        End If

        ' pablo.dias (Nova Consultoria) - 01/08/2011
        ' 9420650 - Melhorias no Sistema Vida Web        
        ' Bloquear links quando ap�lice estiver cancelada
        If Session("situacao_apolice") = "Cancelado" Then
            cUtilitarios.SetOnClickEvent(linkMovOnline, "alert('N�o � poss�vel executar movimenta��o on-line em ap�lices canceladas.');")
            cUtilitarios.SetOnClickEvent(linkPorArquivo, "alert('N�o � poss�vel executar movimenta��o por arquivo em ap�lices canceladas.');")
        End If
        Dim teste As String = Session("apolice")
    End Sub

    Private Function ExisteMovimentacaoVidas(ByVal apolice As Int32, ByVal ramo As Int16, ByVal subgrupo As Int16) As Boolean
        ''Quando existe movimenta��o nas vidas, retorna TRUE, quando n�o existe, retorna FALSE
        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader
        Dim retorno As Boolean = False


        BD.VerificaMovimentacao(apolice, ramo, subgrupo, 1)

        Try
            dr = BD.ExecutaSQL_DR

            If dr.Read() Then
                retorno = True
            Else
                retorno = False
            End If

            dr.Close()
            dr = Nothing

            Return retorno

        Catch ex As Exception
            Dim v_sCodigoErro As String = "1 - ExisteMovimentacaoVidas"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
            Return retorno
        End Try

    End Function

    Private Function ExisteLayout(ByVal apolice As Int32, ByVal ramo As Int16, ByVal subgrupo As Int16) As Boolean

        'Quando existe movimenta��o nas vidas, retorna TRUE, quando n�o existe, retorna FALSE
        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader
        Dim retorno As Boolean = False

        BD.getLayoutArquivo(apolice, ramo, subgrupo)

        Try
            dr = BD.ExecutaSQL_DR

            If dr.Read() Then
                retorno = True
            Else
                retorno = False
            End If

            dr.Close()
            dr = Nothing

            Return retorno

        Catch ex As Exception
            Dim v_sCodigoErro As String = "1 - ExisteLayout"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
            Return retorno
        End Try

    End Function

    Private Function getTpAcesso() As String
        Dim ind_acesso As String = ""
        Select Case Session("acesso").ToString
            Case "1"
                ind_acesso = "Administrador da Alian�a do Brasil"
            Case "2"
                ind_acesso = "Administrador de Ap�lice"
            Case "3"
                ind_acesso = "Administrador de Subgrupo"
            Case "4"
                ind_acesso = "Operador"
            Case "5"
                ind_acesso = "Consulta"
            Case "6" ' Projeto 539202 - Jo�o Ribeiro - 27/04/2009
                ind_acesso = "Central de Atendimento"
            Case "7"    ' Demanda 4370246
                ind_acesso = "Adm. Master"
        End Select

        Return ind_acesso
    End Function

    Private Function getTpAcesso(ByVal acesso As Integer) As String
        Dim ind_acesso As String = ""
        Select Case acesso
            Case "1"
                ind_acesso = "Administrador da Alian�a do Brasil"
            Case "2"
                ind_acesso = "Administrador de Ap�lice"
            Case "3"
                ind_acesso = "Administrador de Subgrupo"
            Case "4"
                ind_acesso = "Operador"
            Case "5"
                ind_acesso = "Consulta"
            Case "6" ' Projeto 539202 - Jo�o Ribeiro - 27/04/2009
                ind_acesso = "Central de Atendimento"
            Case "7"    ' Demanda 4370246
                ind_acesso = "Adm. Master"
        End Select

        Return ind_acesso
    End Function

    'Popula a Grid dos �ltimos Acessos
    Private Sub mConfigLogAcesso()

        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim dtApolice As Data.DataTable

        BD.segs5644_sps(Session("usuario"))

        Try

            dr = BD.ExecutaSQL_DR()

            BD.consultar_apolice_ramos_sps(Session("cpf").ToString())
            dtApolice = BD.ExecutaSQL_DT()

            Dim tr As TableRow
            Dim count As Int16 = 1

            'Lista todos os acessos que o usu�rio teve.
            While dr.Read()

                tr = New TableRow
                tr.Attributes.Add("Height", "20px")
                If ((count Mod 2) = 0) Then
                    tr.Attributes.CssStyle.Add("BACKGROUND-COLOR", "#f2f2f2")
                End If

                tr.Cells.Add(addCell(cUtilitarios.trataDataHora(dr.GetValue(0).ToString)))
                tr.Cells.Add(addCell(dr.GetValue(1).ToString))

                Try
                    If (dtApolice.Select("apolice_id = " & CType(dr.GetValue(1), String).Split("-")(1).Trim).Length = 0) Then
                        tr.Cells.Add(addCell("---"))
                    Else
                        tr.Cells.Add(addCell(dtApolice.Select("apolice_id = " & CType(dr.GetValue(1), String).Split("-")(1).Trim)(0).Item("Estipulante")))
                    End If
                Catch

                End Try

                tr.Cells.Add(addCell(dr.GetValue(3).ToString))

                tr.Cells.Add(addCell(dr.GetValue(4).ToString))

                Me.tbUltimosAcessos.Rows.Add(tr)
                tr = Nothing
                count = count + 1

            End While

            'Caso seja a primeira vez, ser� exibida a mensagem
            If Not dr.HasRows Then

                tr = New TableRow
                tr.Cells.Add(addCell("Esta � a 1� vez que voc� acessa o Sistema Movimenta��o de Vida na WEB.<br>Aqui ser�o listados todos os seus acessos ao Sistema Movimenta��o de Vida na WEB."))

                tr.Cells(0).ColumnSpan = 5
                tr.Cells(0).Attributes.Add("align", "center")
                tr.Cells(0).Attributes.Add("style", "font-size:13px;")
                Me.tbUltimosAcessos.Rows.RemoveAt(0)
                Me.tbUltimosAcessos.Rows.Add(tr)

            End If

            'garante que o header esteja com a informa��o correta do acesso do usu�rio            
            Dim acesso As String
            Select Case Session("acesso").ToString
                Case 1
                    acesso = "Adm. Alian�a do Brasil"
                Case 2
                    acesso = "Adm. de Ap�lice"
                Case 3
                    acesso = "Adm. de Subgrupo"
                Case 4
                    acesso = "Operador"
                Case 5
                    acesso = "Consulta"
                Case 6 ' Projeto 539202 - Jo�o Ribeiro - 27/04/2009
                    acesso = "Central de Atendimento"
                Case "7"    ' Demanda 4370246
                    acesso = "Adm. Master"
            End Select

            'cUtilitarios.escreveScript("document.getElementById('lblTopAcessoUsuario').innerHTML = '" & acesso & "';")

        Catch ex As Exception
            Dim v_sCodigoErro As String = "2 - mConfigLogAcesso"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        Finally
            If Not dr Is Nothing Then
                dr.Close()
            End If

            dr = Nothing
        End Try
    End Sub

    'Insere os dados no banco para logar os �ltimos Acessos
    Private Sub mLogaAcesso(ByVal apolice_id As Long, ByVal subgrupo_id As Integer, ByVal ramo_id As Integer, ByVal usuario As String)

        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        BD.TEMPinsereUltimoAcesso(apolice_id, subgrupo_id, ramo_id, usuario)

        Try
            BD.ExecutaSQL()
        Catch ex As Exception
            Dim v_sCodigoErro As String = "3 - mLogaAcesso"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        End Try
    End Sub

    Private Function addCell(ByVal valor As String, Optional ByVal align As Web.UI.WebControls.HorizontalAlign = HorizontalAlign.Left) As TableCell
        Dim td As New TableCell
        td.Text = valor
        If valor.Trim = "---" Then
            td.HorizontalAlign = align
        End If

        Return td
    End Function

    'Exibe os valores b�sicos da ap�lice e exibe seus subgrupos
    Private Sub mPreencheDadosBasicosApolice(ByVal apolice_id As Long, ByVal ramo_id As Integer)

        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        BD.dados_basico_apolice_sps(apolice_id, ramo_id, 6785, 0, Session("cpf"))

        Try

            dr = BD.ExecutaSQL_DR()

            If dr.Read() Then

                lblEmpresa.Text = dr.GetValue(2).ToString
                '| johnny.barros(Confitec) Demanda:16035497 - 25/02/2013 [inicio] |
                If (dr.GetValue(3).ToString = "Vigente") Then
                    lblStatus.Text = "Ativa"
                Else
                    lblStatus.Text = dr.GetValue(3).ToString
                End If
                'lblStatus.Text = dr.GetValue(3).ToString
                '| johnny.barros(Confitec) Demanda:16035497 - 25/02/2013 [fim] |
                lblBairro.Text = IIf(dr.GetValue(6).ToString = "", "---", dr.GetValue(6).ToString)
                lblComplemento.Text = dr.GetValue(5).ToString
                lblCEP.Text = dr.GetValue(7).ToString
                lblCidade.Text = dr.GetValue(8).ToString
                lblUF.Text = dr.GetValue(9).ToString

                lblTextEmail.Text = "E-mail" & " " & getTpAcesso()

                lblEmail.Text = dr.GetValue(12).ToString
                lblDDD.Text = dr.GetValue(10).ToString
                lblTelefone.Text = dr.GetValue(11).ToString
                lblEndereco.Text = dr.GetValue(4).ToString
                lblCNPJ.Text = dr.GetValue(1).ToString
                Session("CNPJCartao") = dr.GetValue(1).ToString

            Else
                lblEmpresa.Text = "---"
                lblStatus.Text = "---"
                lblBairro.Text = "---"
                lblComplemento.Text = "---"
                lblCEP.Text = "---"
                lblCidade.Text = "---"
                lblUF.Text = "---"

                lblTextEmail.Text = "---"

                lblEmail.Text = "---"
                lblDDD.Text = "---"
                lblTelefone.Text = "---"
                lblEndereco.Text = "---"
                lblCNPJ.Text = "---"
            End If

            ' pablo.dias (Nova Consultoria) - 01/08/2011
            ' 9420650 - Melhorias no Sistema Vida Web 
            ' Bloquear links quando ap�lice estiver cancelada
            Session("situacao_apolice") = lblStatus.Text

            mPreencheSubGrupos(apolice_id, ramo_id, Session("cpf"))

        Catch ex As Exception
            Dim v_sCodigoErro As String = "4 - mPreencheDadosBasicosApolice"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        Finally
            If Not dr Is Nothing Then
                dr.Close()
            End If

            dr = Nothing
        End Try

    End Sub

    'Preenche os subgrupos dispon�veis para o usu�rio
    Private Sub mPreencheSubGrupos(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal cpf As String)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        If Session("acesso") <> 5 And Session("acesso") <> 1 Then
            bd.consulta_subgrupo_sps(apolice_id, ramo_id, cpf)
        Else
            bd.consulta_subgrupo_sps(apolice_id, ramo_id, cpf, Session("acesso"))
        End If
        Try
            dr = bd.ExecutaSQL_DR()

            Dim texto As String
            Dim primeiro As Boolean = True

            While dr.Read

                If primeiro Then
                    lblTextEmail.Text = "E-mail" & " " & getTpAcesso(CInt(dr.GetValue(2)))

                    cUtilitarios.escreve("<input type='hidden' name='ind_acesso' id='ind_acesso' value='" & dr.GetValue(2).ToString & "'>")
                    primeiro = False
                End If

                texto = "<LI><IMG src='Images/quadro_recados.jpg' align='absMiddle'><A href='Centro.aspx?subgrupo=" + dr.GetValue(1).ToString + "&nomeSubgrupo=" + dr.GetValue(0).ToString + "&acesso=" + dr.GetValue(2).ToString + "' onclick='top.exibeaguarde();'>" & dr.GetValue(1).ToString & " - " + Replace(dr.GetValue(0).ToString, "%26", "&") + "</A>"
                Me.ulSubGrupos.InnerHtml &= texto

            End While
        Catch ex As Exception
            Dim v_sCodigoErro As String = "5 - mPreencheSubGrupos"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        Finally
            If Not dr Is Nothing Then
                dr.Close()
            End If

            dr = Nothing
        End Try

    End Sub

    Private Sub mPreencheHistoricoFatura()

        Try
            Dim ret As Integer = Me.isAtividadeAberta(Me.getWfID(), 2)
            If ret = 0 Then
                Me.linkMovOnline.Attributes.Add("onclick", "parent.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';return false;")
            Else
                If ret > 1 Then
                    Me.linkMovOnline.Attributes.Add("onclick", "alert('" & cUtilitarios.GetDataEncerramento() & "');return false;")
                End If
            End If
        Catch ex As Exception
            Dim v_sCodigoErro As String = "5 - mPreencheHistoricoFatura"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        Finally
        End Try

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        bd.getHistorico_sps(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Session("usuario"))

        Dim content As String = ""

        Try
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
            Dim count As Int16
            count = 0
            Dim texto, titulopopup, vVigIni, vVigFim As String
            While dr.Read And count < 10
                count = count + 1
                'content &= "<LI style='line-height:22px;'> <a href='#' onClick=""openPopMail('" & System.Web.HttpUtility.UrlEncode(dr.GetValue(2).ToString.Replace("<", "||")).Replace("'", "") & "')"">" & dr.GetValue(0).ToString & "</a> - " & IIf(dr.GetValue(1).ToString.IndexOf("SEG") <> -1, dr.GetValue(1).ToString + " - " + dr.GetValue(2).ToString, dr.GetValue(1).ToString) & "</LI>"

                texto = System.Web.HttpUtility.UrlEncode(dr.GetValue(2).ToString.Replace("<", "||")).Replace("'", "")

                'titulopopup = dr.GetValue(0).ToString & " - " & IIf(dr.GetValue(1).ToString.IndexOf("SEG") <> -1, dr.GetValue(1).ToString + " - " + dr.GetValue(2).ToString, dr.GetValue(1).ToString)
                titulopopup = dr.GetValue(0).ToString & " - " & _
                                IIf(dr.GetValue(1).ToString.IndexOf("SEG") <> -1, dr.GetValue(2).ToString, dr.GetValue(1).ToString) & _
                                " - Por: " & UCase(dr.GetValue(3).ToString)

                '| johnny.barros(Confitec) Demanda:16035497 - 25/02/2013 [inicio] |
                vVigIni = System.Web.HttpUtility.UrlEncode(dr.GetValue(4).ToString)
                vVigFim = System.Web.HttpUtility.UrlEncode(dr.GetValue(5).ToString)
                '| johnny.barros(Confitec) Demanda:16035497 - 25/02/2013 [fim] |

                'content &= "<LI style='line-height:22px;cursor:hand;' onClick=""openPopMail('" & texto & "','" & titulopopup & "')""><u> <span style='cursor:hand;' onClick=""openPopMail('" & texto & "," & titulopopup & "')"">" & dr.GetValue(0).ToString & "</span> - " & IIf(dr.GetValue(1).ToString.IndexOf("SEG") <> -1, dr.GetValue(1).ToString + " - " + dr.GetValue(2).ToString, dr.GetValue(1).ToString) & "</u></LI>"
                'content &= "<LI style='line-height:22px;cursor:hand;' onClick=""openPopMail('" & texto & "','" & titulopopup & "')""><u>" & _
                content &= "<LI style='line-height:22px;cursor:hand;' onClick=""openPopMail('" & texto & "','" & titulopopup & "','" & vVigIni & "','" & vVigFim & "')""><u>" & _
                                "<span style='cursor:hand;' onClick=""openPopMail('" & texto & "," & titulopopup & "')"">" & _
                                dr.GetValue(0).ToString & "</span> - " & _
                                IIf(dr.GetValue(1).ToString.IndexOf("SEG") <> -1, dr.GetValue(2).ToString, dr.GetValue(1).ToString) & _
                                " - Por: " & UCase(dr.GetValue(3).ToString) & "</u></LI>"

            End While
            dr.Close()
            dr = Nothing
        Catch ex As Exception
            content = "N�o foi poss�vel obter o hist�rico desta fatura, por favor tente novamente."
            System.Web.HttpContext.Current.Response.Write("<!--" & ex.Message & vbNewLine & ex.StackTrace & "-->")
        End Try

        If content.Length > 0 Then
            Me.contentEmail.Text = content
        Else
            Me.contentEmail.Text = "<center>N�o h� hist�rico para esta fatura. </center>"
        End If

    End Sub

    Public Function isAtividadeAberta(ByVal wf_id As String, ByVal atividade As String) As Integer

        Try

            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
            bd.getEstadoAtividade(wf_id, atividade)
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read Then
                If dr.GetValue(0).ToString = "1" Then
                    Return 1
                Else
                    Return dr.GetValue(0).ToString
                End If
            End If
            dr.Close()
            dr = Nothing

        Catch ex As Exception
            Dim v_sCodigoErro As String = "6 - IsAtividadeAberta"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
            Return 0
        End Try

        Return 0

    End Function

    Public Function getWfID() As String
        If CStr(Session("subgrupo_id")) <> "" Then       'Ao selecionar uma Ap�lice/Subgrupo

            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
            bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

            Try
                Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

                If dr.Read() Then
                    Return dr.GetValue(0).ToString()
                End If
                dr.Close()
                dr = Nothing
                Return "0"
            Catch ex As Exception
                Dim v_sCodigoErro As String = "7 - getWfID"
                Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
                Return ""
            End Try

        Else
            Return "0"
        End If
    End Function

    Private Sub mPreenchePrazoRestante(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)
        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        BD.prazo_limite(apolice_id, ramo_id, subgrupo_id, getWF_ID)

        Try

            dr = BD.ExecutaSQL_DR()

            If dr.Read() Then
                'Me.lblPrazo.Text = "Faltam " + dr.GetValue(0).ToString() + " dias para atingir o prazo limite para informar as vidas neste sistema."
                Me.lblPrazo.Text = "Dia " + CType(dr.Item("dt_movimento"), DateTime).ToString("dd/MM/yyyy") + " � o prazo limite para informar as vidas neste sistema por meio ON LINE." + _
                "<BR>O processamento por meio de ARQUIVO � at� " + DateAdd(DateInterval.Day, -1, CType(dr.Item("dt_movimento"), DateTime)).ToString("dd/MM/yyyy") + "."
            Else
                Me.lblPrazo.Text = "Prazo n�o encontrado"
            End If

        Catch ex As Exception
            Dim v_sCodigoErro As String = "8 - mPreenchePrazoRestante"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)

        Finally
            If Not dr Is Nothing Then
                dr.Close()
            End If

            dr = Nothing
        End Try

    End Sub

    Private Function getWF_ID() As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim cRetorno As String = "0"

        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

        Try
            dr = bd.ExecutaSQL_DR()

            If dr.Read() Then
                cRetorno = dr.GetValue(0).ToString
            End If
            dr.Close()
            dr = Nothing

        Catch ex As Exception
            Dim v_sCodigoErro As String = "9 - getWF_ID"
            Dim excp As New clsException("Ocorreu um erro inesperado.Favor entrar em contato como administrador do sistema \nC�digo: " & v_sCodigoErro, ex)
        Finally
            If Not dr Is Nothing Then
                dr.Close()
            End If

            dr = Nothing
        End Try

        Return cRetorno
    End Function

End Class
