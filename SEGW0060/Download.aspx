<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Download.aspx.vb" Inherits="segw0060.Downloads" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Downloads</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
    <link href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">

    <script src="scripts/overlib421/overlib.js" type="text/javascript"></script>

    <style type="text/css">
         .header {background-color: rgb(232, 241, 255);cursor:hand}
         .content {border: solid;}
    </style>
</head>
<body ms_positioning="FlowLayout">
    <div id="overDiv" style="z-index: 1000; visibility: hidden; position: absolute">
    </div>
    <form id="Form1" method="post" runat="server">
        <asp:ScriptManager runat="server">
        </asp:ScriptManager>
        <p>
            <asp:Label ID="lblNavegacao" runat="server" CssClass="Caminhotela" Visible="False">Label</asp:Label><br>
            <asp:Label ID="lblVigencia" runat="server" CssClass="Caminhotela"><br>COTEMINAS S.A - Per�odo de compet�ncia: 01/12/2006 at� 31/12/2006</asp:Label></p>
        <p>
            <br>
        </p>
        <cc1:Accordion ID="MyAccordion" runat="Server" SelectedIndex="0" AutoSize="None"
            FadeTransitions="true" RequireOpenedPane="false" HeaderCssClass="header" SuppressHeaderPostbacks="true">
            <Panes>
                <cc1:AccordionPane ID="AccordionPane1" runat="server">
                    <Header>
                        <asp:Label ID="lblDownloadCartao" runat="server" CssClass="Caminhotela">:: Download de declara��o pessoal de sa�de (DPS)</asp:Label>
                    </Header>
                    <Content>
                        <br />
                        <asp:Label ID="Label1" runat="server" CssClass="DescricaoCampo" Font-Size="X-Small">Clique no link abaixo para fazer o download do documento de declara��o pessoal de sa�de (DPS).</asp:Label>&nbsp;
                        <asp:Image ID="Image1" onmouseover="return overlib('Clique no link abaixo para fazer o download do documento de declara��o pessoal de sa�de (DPS).');"
                            onmouseout="return nd();" runat="server" ImageUrl="Images/help.gif"></asp:Image>
                        <asp:Label ID="lblTexto" runat="server" Font-Size="X-Small">Qualquer d�vida sobre os procedimentos de preenchimento e envio da declara��o pessoal de sa�de (DPS), </asp:Label>
                        <asp:HyperLink ID="hplCliqueAqui" runat="server" Font-Underline="True" Target="iFrameDownload"
                            NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=1" Enabled="False">clique aqui</asp:HyperLink>
                        <asp:Label ID="Label10" runat="server" Font-Size="X-Small">para visualizar as instru��es de download  do mesmo.</asp:Label>
                        <br />
                        <br />
                        <ul>
                            <li>
                                <asp:HyperLink ID="hplDPS" runat="server" Font-Size="X-Small" Width="360px" Font-Underline="True"
                                    Target="iFrameDownload" NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=2" Enabled="False">- Modelo de declara��o pessoal de sa�de (DPS)</asp:HyperLink><br />
                                <br />
                                <br />
                            </li>
                        </ul>
                    </Content>
                </cc1:AccordionPane>
            </Panes>
            <Panes>
                <cc1:AccordionPane ID="AccordionPane2" runat="server">
                    <Header>
                        <asp:Label ID="Label2" runat="server" CssClass="Caminhotela">:: Download de proposta com declara��o pessoal de sa�de (DPS)</asp:Label>
                    </Header>
                    <Content>
                        <br />
                        <asp:Label ID="Label3" runat="server" CssClass="DescricaoCampo" Font-Size="X-Small">Clique no link abaixo para fazer o download do documento de proposta com declara��o pessoal de sa�de (DPS).</asp:Label>&nbsp;
                        <asp:Image ID="Image2" onmouseover="return overlib('Clique no link abaixo para fazer o download do documento de proposta com declara��o pessoal de sa�de (DPS).');"
                            onmouseout="return nd();" runat="server" ImageUrl="Images/help.gif"></asp:Image>
                        <br />
                        <asp:Label ID="Label4" runat="server" Font-Size="X-Small">Qualquer d�vida sobre os procedimentos de preenchimento e envio da proposta de declara��o pessoal de sa�de (DPS), </asp:Label>
                        <asp:HyperLink ID="hplCliqueAqui02" runat="server" Font-Underline="True" Target="iFrameDownload"
                            NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=1" Enabled="False">clique aqui</asp:HyperLink>
                        <asp:Label ID="Label8" runat="server" Font-Size="X-Small"> para visualizar as instru��es de download  do mesmo.</asp:Label>
                        <br />
                        <ul>
                            
                            <li>
                               <asp:HyperLink ID="hplSolicitacaoEndossoVida" runat="server" Width="419px" Font-Size="X-Small" Font-Underline="True"
                                    Target="iFrameDownload"  NavigateUrl="~/downloads/SOLICITACAO_COTACAO_ENDOSSO_VIDA.zip"  >- Formulario de Endosso</asp:HyperLink>
                                <br />
                                <br />
                            </li>                        
                        
                            <li>
                                <asp:HyperLink ID="hplAte100" runat="server" Width="419px" Font-Size="X-Small" Font-Underline="True"
                                    Target="iFrameDownload" NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=3" Enabled="False">- Proposta de ades�o sem DPS <br>Capital individual at� R$ 300 mil</asp:HyperLink>
                                <br />
                                <br />
                            </li>
                            
                            <li>
                                <asp:HyperLink ID="hplEntre100" runat="server" Width="419px" Font-Size="X-Small"
                                    Font-Underline="True" Target="iFrameDownload" NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=4"
                                    Enabled="False">- Proposta de ades�o com DPS <br>Capital individual entre R$ 300.000,01 e R$ 800 mil</asp:HyperLink>
                                <br />
                                <br />
                            </li>
                            <li>
                                <asp:HyperLink ID="hplMaior500" runat="server" Width="419px" Font-Size="X-Small"
                                    Font-Underline="True" Target="iFrameDownload" NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=5"
                                    Enabled="False">- Proposta de ades�o com DPS <br>Capital individual maior que R$ 800 mil</asp:HyperLink>
                                <br />
                                <br />
                            </li>
                        </ul>
                        <br />
                    </Content>
                </cc1:AccordionPane>
            </Panes>
            <Panes>
                <cc1:AccordionPane ID="AccordionPane3" runat="server">
                    <Header>
                        <asp:Label ID="Label5" runat="server" CssClass="Caminhotela">:: Download de ficha financeira</asp:Label>
                    </Header>
                    <Content>
                        <br />
                        <asp:Label ID="Label6" runat="server" CssClass="DescricaoCampo" Font-Size="X-Small">Clique no link abaixo para fazer o download do documento de ficha de informa��es financeiras.</asp:Label>&nbsp;
                        <asp:Image ID="Image3" onmouseover="return overlib('Clique no link abaixo para fazer o download do documento de ficha de informa��es financeiras.');"
                            onmouseout="return nd();" runat="server" ImageUrl="Images/help.gif"></asp:Image>
                        <br />
                        <asp:Label ID="Label7" runat="server" Font-Size="X-Small">Qualquer d�vida sobre os procedimentos de preenchimento e envio da ficha financeira, </asp:Label>
                        <asp:HyperLink ID="hplCliqueAqui03" runat="server" Font-Underline="True" Target="iFrameDownload"
                            NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=1" Enabled="False">clique aqui</asp:HyperLink>
                        <asp:Label ID="Label9" runat="server" Font-Size="X-Small">para visualizar as instru��es de download  do mesmo.</asp:Label>
                        <br />
                        <br />
                        <ul>
                            <li>
                                <asp:HyperLink ID="hplFichaFinanceira" runat="server" Width="419px" Font-Size="X-Small"
                                    Font-Underline="True" Target="iFrameDownload" NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=6"
                                    Enabled="False">- Modelo de ficha de informa��es financeiras</asp:HyperLink>
                                <br />
                                <br />
                            </li>
                        </ul>
                    </Content>
                </cc1:AccordionPane>
                <cc1:AccordionPane ID="AccordionPane4" runat="server">
                    <Header>
                        <asp:Label ID="Label15" runat="server" CssClass="Caminhotela">:: Check-lists e Declaracoes</asp:Label>
                    </Header>
                    <Content>
                        <br />
                        <cc1:Accordion ID="Accordion1" runat="Server" SelectedIndex="0" AutoSize="None" FadeTransitions="true"
                            RequireOpenedPane="false" HeaderCssClass="header" SuppressHeaderPostbacks="true">
                            <Panes>
                                <cc1:AccordionPane ID="AccordionPane5" runat="server">
                                    <Header>
                                        <strong>:: ::Check-List</strong></Header>
                                    <Content>
                                        <asp:Label ID="Label16" runat="server" CssClass="DescricaoCampo" Font-Size="X-Small">Clique no link abaixo para fazer o download do documento de ficha de informa��es financeiras.</asp:Label>&nbsp;
                                        <asp:Image ID="Image5" onmouseover="return overlib('Clique no link abaixo para fazer o download do documento de ficha de informa��es financeiras.');"
                                            onmouseout="return nd();" runat="server" ImageUrl="Images/help.gif"></asp:Image>
                                        <br />
                                        <asp:Label ID="Label17" runat="server" Font-Size="X-Small">Qualquer d�vida sobre os procedimentos de preenchimento e envio da ficha financeira, </asp:Label>
                                        <asp:HyperLink ID="HyperLink35" runat="server" Font-Underline="True" Target="iFrameDownload"
                                            NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=1" Enabled="False">clique aqui</asp:HyperLink>
                                        <asp:Label ID="Label18" runat="server" Font-Size="X-Small">para visualizar as instru��es de download  do mesmo.</asp:Label>
                                        <p>
                                            <br />
                                            <br />
                                            <asp:HyperLink ID="HyperLink15" runat="server" Width="419px" Font-Size="X-Small"
                                                Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/para-empresa/seguro-de-vida-em-grupo/kit-sinistro/ouro_vida_grupo_especial_lista_documentacao_basica-mn.pdf"
                                                Enabled="True">- Lista de documentos para Morte Natural</asp:HyperLink></p>
                                        <p>
                                            <br />
                                            <asp:HyperLink ID="HyperLink16" runat="server" Width="419px" Font-Size="X-Small"
                                                Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/para-empresa/seguro-de-vida-em-grupo/kit-sinistro/ouro_vida_grupo_especial_lista_documentacao_basica-ma.pdf"
                                                Enabled="True">- Lista de documentos para Morte Acidental</asp:HyperLink></p>
                                        <br />
                                        <asp:HyperLink ID="HyperLink17" runat="server" Width="419px" Font-Size="X-Small"
                                            Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/para-empresa/seguro-de-vida-em-grupo/kit-sinistro/ouro_vida_grupo_especial_lista_documentacao_basica-dt.pdf"
                                            Enabled="True">- Lista de documentos para Doen�a Terminal</asp:HyperLink></p>
                                        <br />
                                        <asp:HyperLink ID="HyperLink18" runat="server" Width="419px" Font-Size="X-Small"
                                            Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/para-empresa/seguro-de-vida-em-grupo/kit-sinistro/ouro_vida_grupo_especial_lista_documentacao_basica-ipa.pdf"
                                            Enabled="True">- Lista de documentos para Invalidez por Acidente</asp:HyperLink></p>
                                        <br />
                                        <asp:HyperLink ID="HyperLink19" runat="server" Width="419px" Font-Size="X-Small"
                                            Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/para-empresa/seguro-de-vida-em-grupo/kit-sinistro/ov_grupo_especial_mn_site.pdf"
                                            Enabled="True">- Formul�rios para Morte Natural</asp:HyperLink></p>
                                        <br />
                                        <asp:HyperLink ID="HyperLink20" runat="server" Width="419px" Font-Size="X-Small"
                                            Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/para-empresa/seguro-de-vida-em-grupo/kit-sinistro/ov_grupo_especial_ma_site.pdf"
                                            Enabled="True">- Formul�rios para Morte Acidental</asp:HyperLink></p>
                                        <br />
                                        <asp:HyperLink ID="HyperLink21" runat="server" Width="419px" Font-Size="X-Small"
                                            Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/para-empresa/seguro-de-vida-em-grupo/kit-sinistro/ov_grupo_especial_dt_site.pdf"
                                            Enabled="True">- Formul�rios para Doen�a Terminal</asp:HyperLink></p>
                                        <asp:HyperLink ID="HyperLink22" runat="server" Width="419px" Font-Size="X-Small"
                                            Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/pra-voce/bb-seguro-ouro-vida/kit-sinistro/ov_2000_ipa_site.pdf"
                                            Enabled="True">- Formul�rios para Invalidez por Acidente</asp:HyperLink></p>
                                    </Content>
                                </cc1:AccordionPane>
                            </Panes>
                            <Panes>
                                <cc1:AccordionPane ID="AccordionPane6" runat="server">
                                    <Header>
                                        <strong>:: ::Declaracoes</strong></Header>
                                    <Content>
                                        <br />
                                        <br />
                                        <asp:Label ID="Label19" runat="server" CssClass="DescricaoCampo" Font-Size="X-Small">Clique no link abaixo para fazer o download do documento de ficha de informa��es financeiras.</asp:Label>&nbsp;
                                        <asp:Image ID="Image6" onmouseover="return overlib('Clique no link abaixo para fazer o download do documento de ficha de informa��es financeiras.');"
                                            onmouseout="return nd();" runat="server" ImageUrl="Images/help.gif"></asp:Image>
                                        <br />
                                        <asp:Label ID="Label20" runat="server" Font-Size="X-Small">Qualquer d�vida sobre os procedimentos de preenchimento e envio da ficha financeira, </asp:Label>
                                        <asp:HyperLink ID="HyperLink36" runat="server" Font-Underline="True" Target="iFrameDownload"
                                            NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=1" Enabled="False">clique aqui</asp:HyperLink>
                                        <asp:Label ID="Label21" runat="server" Font-Size="X-Small">para visualizar as instru��es de download  do mesmo.</asp:Label>
                                        <br />
                                        <br />
                                        <asp:HyperLink ID="HyperLink23" runat="server" Width="419px" Font-Size="X-Small"
                                            Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/atendimento/kit-sinistro/declaracao-de-companheiro-a.pdf"
                                            Enabled="True">- Declara��o de companheira</asp:HyperLink></p>
                                        <br />
                                        <asp:HyperLink ID="HyperLink24" runat="server" Width="419px" Font-Size="X-Small"
                                            Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/atendimento/kit-sinistro/declaracao_herdeiros.pdf"
                                            Enabled="True">- Declara��o de n�o casado e sem filhos</asp:HyperLink></p>
                                        <br />
                                        <asp:HyperLink ID="HyperLink25" runat="server" Width="419px" Font-Size="X-Small"
                                            Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/atendimento/kit-sinistro/declaracao_herdeiros.pdf"
                                            Enabled="True">- Declara��o de n�o casado e com filhos</asp:HyperLink></p>
                                        <br />
                                        <asp:HyperLink ID="HyperLink26" runat="server" Width="419px" Font-Size="X-Small"
                                            Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/atendimento/kit-sinistro/declaracao_herdeiros.pdf"
                                            Enabled="True">- Declara��o de casado e sem filhos</asp:HyperLink></p>
                                        <br />
                                        <asp:HyperLink ID="HyperLink27" runat="server" Width="419px" Font-Size="X-Small"
                                            Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/atendimento/kit-sinistro/declaracao_herdeiros.pdf"
                                            Enabled="True">- Declara��o de casado e com filhos</asp:HyperLink></p>
                                        <br />
                                        <asp:HyperLink ID="HyperLink28" runat="server" Width="419px" Font-Size="X-Small"
                                            Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/atendimento/kit-sinistro/autorizacao_pagamento.pdf"
                                            Enabled="True">- Autoriza��o de Cr�dito em Conta</asp:HyperLink></p>
                                        <br />
                                        <asp:HyperLink ID="HyperLink29" runat="server" Width="419px" Font-Size="X-Small"
                                            Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/atendimento/kit-sinistro/declaracao_medica_doenca_terminal.pdf"
                                            Enabled="True">- Doen�a Terminal</asp:HyperLink></p>
                                        <br />
                                        <asp:HyperLink ID="HyperLink30" runat="server" Width="419px" Font-Size="X-Small"
                                            Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/atendimento/kit-sinistro/declaracao_medica_invalidez_por_doenca.pdf"
                                            Enabled="True">- Invalidez por Doen�a</asp:HyperLink></p>
                                        <br />
                                        <asp:HyperLink ID="HyperLink31" runat="server" Width="419px" Font-Size="X-Small"
                                            Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/atendimento/kit-sinistro/declaracao_medica_invalidez_por_acidente.pdf"
                                            Enabled="True">-Invalidez por Acidente</asp:HyperLink></p>
                                        <br />
                                        <asp:HyperLink ID="HyperLink32" runat="server" Width="419px" Font-Size="X-Small"
                                            Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/atendimento/kit-sinistro/declaracao_medica_invalidez_por_acidente.pdf"
                                            Enabled="True">- Morte Acidental</asp:HyperLink></p>
                                        <br />
                                        <asp:HyperLink ID="HyperLink33" runat="server" Width="419px" Font-Size="X-Small"
                                            Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/atendimento/kit-sinistro/declaracao_medica_morte_natural.pdf"
                                            Enabled="True">- Morte Natural</asp:HyperLink></p>
                                        <br />
                                        <asp:HyperLink ID="HyperLink34" runat="server" Width="419px" Font-Size="X-Small"
                                            Font-Underline="True" Target="iFrameDownload" NavigateUrl="http://www.bbseguros.com.br/documents/atendimento/kit-sinistro/declaracao-de-inexistencia-de-proposta-de-adesao.pdf"
                                            Enabled="True">- Declara��o de Inexist�ncia de Proposta de ades�o</asp:HyperLink></p>
                                        <br />
                                        <br />
                                    </Content>
                                </cc1:AccordionPane>
                            </Panes>
                        </cc1:Accordion>
                        </p>
                    </Content>
                </cc1:AccordionPane>
            </Panes>
        </cc1:Accordion>
        <asp:Panel ID="Panel4" runat="server" Visible="false" CssClass="fundo-azul-claro">
            <p>
            </p>
            <p>
                <asp:Label ID="Label11" runat="server" CssClass="Caminhotela">:: Check-list e declara��es</asp:Label></p>
            <p>
                <table id="Table4" cellspacing="0" cellpadding="2" width="100%" border="0">
                    <tr>
                        <td colspan="2" height="27">
                            <p>
                                <asp:Label ID="Label12" runat="server" CssClass="DescricaoCampo" Font-Size="X-Small">Clique no link abaixo para fazer o download do documento de proposta com declara��o pessoal de sa�de (DPS).</asp:Label>&nbsp;
                                <asp:Image ID="Image4" onmouseover="return overlib('Clique no link abaixo para fazer o download do documento de proposta com declara��o pessoal de sa�de (DPS).');"
                                    onmouseout="return nd();" runat="server" ImageUrl="Images/help.gif"></asp:Image></p>
                            <p>
                                <asp:Label ID="Label13" runat="server" Font-Size="X-Small">Qualquer d�vida sobre os procedimentos de preenchimento e envio da proposta de declara��o pessoal de sa�de (DPS), </asp:Label>
                                <asp:HyperLink ID="HyperLink1" runat="server" Font-Underline="True" Target="iFrameDownload"
                                    NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=1" Enabled="False">clique aqui</asp:HyperLink>
                                <asp:Label ID="Label14" runat="server" Font-Size="X-Small"> para visualizar as instru��es de download  do mesmo.</asp:Label></p>
                        </td>
                    </tr>
                    <tr>
                        <td width="2" colspan="2" height="59">
                            <p>
                                <br />
                                <asp:HyperLink ID="HyperLink2" runat="server" Width="419px" Font-Size="X-Small" Font-Underline="True"
                                    Target="iFrameDownload" NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=7" Enabled="True">- Declara��o de Inexist�ncia de PR</asp:HyperLink></p>
                            <p>
                                <asp:HyperLink ID="HyperLink3" runat="server" Width="419px" Font-Size="X-Small" Font-Underline="True"
                                    Target="iFrameDownload" NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=8" Enabled="False">- Declara��o de Companheira</asp:HyperLink></p>
                            <p>
                                <asp:HyperLink ID="HyperLink4" runat="server" Width="419px" Font-Size="X-Small" Font-Underline="True"
                                    Target="iFrameDownload" NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=9" Enabled="False">- Declara��o de N�o Casado e Sem Filhos</asp:HyperLink></p>
                            <p>
                                <asp:HyperLink ID="HyperLink5" runat="server" Width="419px" Font-Size="X-Small" Font-Underline="True"
                                    Target="iFrameDownload" NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=10" Enabled="False">- Declara��o de N�o Casado e Com Filhos</asp:HyperLink></p>
                            <p>
                                <asp:HyperLink ID="HyperLink6" runat="server" Width="419px" Font-Size="X-Small" Font-Underline="True"
                                    Target="iFrameDownload" NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=11" Enabled="False">- Declara��o de Casado e Sem Filhos</asp:HyperLink></p>
                            <p>
                                <asp:HyperLink ID="HyperLink7" runat="server" Width="419px" Font-Size="X-Small" Font-Underline="True"
                                    Target="iFrameDownload" NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=12" Enabled="False">- Declara��o de Casado e Com Filhos</asp:HyperLink></p>
                            <p>
                                <asp:HyperLink ID="HyperLink8" runat="server" Width="419px" Font-Size="X-Small" Font-Underline="True"
                                    Target="iFrameDownload" NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=13" Enabled="False">- Autoriza��o de Cr�dito em Conta 01/07 <br>Capital individual maior que R$ 800 mil</asp:HyperLink></p>
                            <p>
                                <asp:HyperLink ID="HyperLink9" runat="server" Width="419px" Font-Size="X-Small" Font-Underline="True"
                                    Target="iFrameDownload" NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=14" Enabled="False">- DT, IPD, IFPD V3</asp:HyperLink></p>
                            <p>
                                <asp:HyperLink ID="HyperLink10" runat="server" Width="419px" Font-Size="X-Small"
                                    Font-Underline="True" Target="iFrameDownload" NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=15"
                                    Enabled="False">- Autoriza��o de Cr�dito em Conta 24/08 </asp:HyperLink></p>
                            <p>
                                <asp:HyperLink ID="HyperLink11" runat="server" Width="419px" Font-Size="X-Small"
                                    Font-Underline="True" Target="iFrameDownload" NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=16"
                                    Enabled="False">- MA v5</asp:HyperLink></p>
                            <p>
                                <asp:HyperLink ID="HyperLink12" runat="server" Width="419px" Font-Size="X-Small"
                                    Font-Underline="True" Target="iFrameDownload" NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=17"
                                    Enabled="False">- IPA v2</asp:HyperLink></p>
                            <p>
                                <asp:HyperLink ID="HyperLink13" runat="server" Width="419px" Font-Size="X-Small"
                                    Font-Underline="True" Target="iFrameDownload" NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=18"
                                    Enabled="False">- MN Valores acima de 600 mil</asp:HyperLink></p>
                            <p>
                                <asp:HyperLink ID="HyperLink14" runat="server" Width="419px" Font-Size="X-Small"
                                    Font-Underline="True" Target="iFrameDownload" NavigateUrl="~/exportar.aspx?DocumentoVidaWebId=19"
                                    Enabled="False">- MN Valores abaixo de 600 mil</asp:HyperLink></p>
                        </td>
                    </tr>
                </table>
            </p>
        </asp:Panel>
    </form>

    <script>
		top.escondeaguarde();
    </script>

    <iframe id="iFrameDownload" name="iFrameDownload" style="display: none;"></iframe>
</body>
</html>
