﻿Public Partial Class ImprimeAssitencia
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim controller As cAssistenciaController = Session("cAssistenciaController")
        controller.ObterTextoAssistencia(Request.QueryString("PlanoId"), Me.lblTextoAssitencia)
        Me.lblTextoAssitencia.Text = Replace(Me.lblTextoAssitencia.Text, Chr(13), Chr(60) & "BR" & Chr(62))
    End Sub

End Class