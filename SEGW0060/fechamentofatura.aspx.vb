Public Class FechamentoFatura
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblVigencia As System.Web.UI.WebControls.Label
    Protected WithEvents lblNavegacao As System.Web.UI.WebControls.Label
    Protected WithEvents lblListagemVidas As System.Web.UI.WebControls.Label
    Protected WithEvents Label17 As System.Web.UI.WebControls.Label
    Protected WithEvents Label15 As System.Web.UI.WebControls.Label
    Protected WithEvents pnlDependente As System.Web.UI.WebControls.Panel
    Protected WithEvents imgPlus As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblAcao As System.Web.UI.WebControls.Label
    Protected WithEvents btnFecharMovimentacao As System.Web.UI.WebControls.Button
    Protected WithEvents lblListagemEnviada As System.Web.UI.WebControls.Label
    Protected WithEvents pnlAlerta As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlBtnEncerrar As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Request.QueryString("fatura") <> "" Then
            pnlAlerta.Visible = True
            pnlBtnEncerrar.Visible = False
        End If
        If Not Page.IsPostBack Then
            pnlDependente.Visible = True
        End If
        lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Subgrupo: " & Session("subGrupo") & " -> " & "Encerrar movimenta��o"
        lblNavegacao.Visible = True
        btnFecharMovimentacao.Attributes.Add("onclick", "return confirmaFechamento()")
    End Sub

    Private Sub btnFechaPagina_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("Centro.aspx")
    End Sub

    Private Sub btnLuis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        
    End Sub

    Private Sub imgPlus_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPlus.Click
        If pnlDependente.Visible = True Then

            imgPlus.ImageUrl = "Images/imgMais.gif"
            pnlDependente.Visible = False
        Else
            imgPlus.ImageUrl = "Images/imgMenos.gif"
            pnlDependente.Visible = True
        End If
    End Sub
End Class
