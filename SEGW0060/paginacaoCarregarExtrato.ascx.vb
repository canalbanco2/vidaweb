Imports System.Drawing
Imports System.Web.UI.WebControls
Imports System.Data

Public Class paginacaoCarregaExtrato
    Inherits System.Web.UI.UserControl


    Public _valor As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents a1 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents a2 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents a3 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents a4 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents a5 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents a6 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents a7 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lblPaginacao As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub

    Public Event ePaginaAlterada(ByVal Argumento As String)
    Public Event ePaginaAlteradaA(ByVal Argumento As String)

    Private Sub mDisparaEvento(ByVal Argumento As String)
        RaiseEvent ePaginaAlterada(Argumento)
    End Sub
    Private Sub mDisparaEventoA(ByVal Argumento As String)
        RaiseEvent ePaginaAlteradaA(Argumento)
    End Sub

    Private Sub mPaginacaoClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles a1.Command, a2.Command, a3.Command, a4.Command, a5.Command, a6.Command, a7.Command
        mDisparaEvento(e.CommandArgument)
    End Sub

    Private Sub aPaginacaoClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles a1.Command, a2.Command, a3.Command, a4.Command, a5.Command, a6.Command, a7.Command
        mDisparaEventoA(e.CommandArgument)
    End Sub

    Private Sub mMontaPaginacao(ByVal GridPaginaAtual As Integer, ByVal GridPaginasTotal As Integer)
        Dim paginainicial As Integer
        Dim paginafinal As Integer
        Dim paginaatual As Integer = GridPaginaAtual + 1

        If (paginaatual / 5) > 0 Then
            If (paginaatual Mod 5) <> 0 Then
                paginainicial = (Int(paginaatual / 5) * 5) + 1
            Else
                paginainicial = (Int((paginaatual - 1) / 5) * 5) + 1
            End If
            paginafinal = paginainicial + 4
            If paginafinal > GridPaginasTotal Then paginafinal = GridPaginasTotal
        Else
            paginainicial = 1
            paginafinal = 5
            If paginafinal > GridPaginasTotal Then paginafinal = GridPaginasTotal
        End If

        Me.a1.Visible = False
        Me.a2.Visible = False
        Me.a3.Visible = False
        Me.a4.Visible = False
        Me.a5.Visible = False
        Me.a6.Visible = False
        Me.a7.Visible = False
        If paginaatual > 1 Then
            a1.Text = "Anterior"
            a1.ForeColor = Color.White
            a1.Style.Add("font-family", "verdana")
            a1.Style.Add("font-size", "10px")
            a1.Style.Add("font-weight", "bold")
            a1.Style.Add("text-decoration", "underline")
            a1.Visible = True
        End If
        Dim a As Integer = 2
        For i As Integer = paginainicial To paginafinal
            CType(Me.FindControl("a" + a.ToString), LinkButton).CommandArgument = (i - 1).ToString
            CType(Me.FindControl("a" + a.ToString), LinkButton).Text = i.ToString
            CType(Me.FindControl("a" + a.ToString), LinkButton).Visible = True
            If i = paginaatual Then
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("color", "white")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("text-decoration", "none")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-family", "verdana")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-size", "10px")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-weight", "bold")

            Else
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("color", "white;")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("text-decoration", "underline")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-family", "verdana")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-size", "10px")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-weight", "bold")

            End If
            a += 1
        Next
        If paginaatual <> GridPaginasTotal Then
            a7.Text = "Pr�xima"
            a7.ForeColor = Color.White
            a7.Style.Add("font-family", "verdana")
            a7.Style.Add("font-size", "10px")
            a7.Style.Add("font-weight", "bold")
            a7.Style.Add("text-decoration", "underline")
            a7.Visible = True
        End If
    End Sub

    Public Sub GridDataBind(ByVal Grid As DataGrid, ByVal dt As DataTable)

        Grid.DataSource = dt
        Grid.DataBind()

        Dim tipo As String
        Dim cell As Web.UI.WebControls.DataGridItem
        Dim pai As Web.UI.WebControls.DataGridItem
        Dim count As Int16 = 0
        Dim pai_id As String = ""
        Dim count2 As Integer = 0
        Dim w As Integer = 1
        Dim t As Integer = 1
        Dim primeiro As Boolean = True
        Dim textoCell As String = ""
        Dim tamanho(13) As Integer
        Dim txtMax(13) As String

        tamanho(0) = 0
        tamanho(1) = 0
        tamanho(2) = 0
        tamanho(3) = 0
        tamanho(4) = 0
        tamanho(5) = 0
        tamanho(6) = 0
        tamanho(7) = 0
        tamanho(8) = 0
        tamanho(9) = 0
        tamanho(10) = 0
        tamanho(11) = 0
        tamanho(12) = 0
        tamanho(13) = 0

        txtMax(0) = ""
        txtMax(1) = ""
        txtMax(2) = ""
        txtMax(3) = ""
        txtMax(4) = ""
        txtMax(5) = ""
        txtMax(6) = ""
        txtMax(7) = ""
        txtMax(8) = ""
        txtMax(9) = ""
        txtMax(10) = ""
        txtMax(11) = ""
        txtMax(12) = ""
        txtMax(13) = ""

        If cUtilitarios.MostraColunaSalario() Then
            w = 1
        Else
            w = 0
        End If

        If cUtilitarios.MostraColunaCapital() Then
            t = 1
        Else
            t = 0
        End If

        If Grid.Items.Count > 0 Then

            For Each x As Web.UI.WebControls.DataGridItem In Grid.Items

                cell = x
                If x.Cells.Item(4).Text = "Titular" Then
                    pai = x
                    x.ID = count2
                    pai_id = x.ID

                    x.Attributes.Add("onmouseover", "mO('#E8F1FF', this)")
                    x.Attributes.Add("onmouseout", "mO('#FFFFFF', this)")

                    count = 0
                    count2 += 1
                Else
                    x.ID = pai_id + "_" + count.ToString
                    x.Attributes.Add("onmouseover", "mO('#E8F1FF', this)")
                    x.Attributes.Add("onmouseout", "mO('#FFFFFF', this)")

                    count = count + 1
                End If

                For i As Integer = 0 To x.Cells.Count - 1
                    x.Cells.Item(i).ID = "cell_" & i
                    x.Cells.Item(i).Wrap = False

                    If i = 2 Then
                        tamanho(i) = 25
                        txtMax(i) = "".PadLeft(25, "O")
                    End If

                Next

                'ITEM PARA O LINK DO DOCUMENTO
                If x.Cells.Item(3).Text.IndexOf("Permitido") <> -1 Then
                    x.Cells.Item(5).Text = "Modelo de declara��o pessoal de sa�de (DPS)"

                ElseIf x.Cells.Item(3).Text.IndexOf("300") <> -1 Then
                    x.Cells.Item(5).Text = "Modelo de declara��o pessoal de sa�de (DPS) Capital individual entre R$ 300.000,01 e 800.000,00 "

                ElseIf x.Cells.Item(3).Text.IndexOf("800") <> -1 Then
                    x.Cells.Item(5).Text = "Modelo de declara��o pessoal de sa�de (DPS) Capital individual maior que R$ 800.000,00"

                ElseIf x.Cells.Item(3).Text.IndexOf("Idade") <> -1 Then
                    x.Cells.Item(5).Text = "Modelo de declara��o pessoal de sa�de (DPS)" 'Removido -> Capital individual at� R$ 100.000,00

                ElseIf x.Cells.Item(3).Text.IndexOf("Contratual") <> -1 Then
                    x.Cells.Item(5).Text = "Modelo de ficha de informa��es financeiras"
                End If

                x.Cells.Item(4).Text = cUtilitarios.trataCPF(x.Cells.Item(4).Text)

                x.Cells.Item(2).Text = x.Cells.Item(2).Text.ToUpper
                x.Cells.Item(2).HorizontalAlign = HorizontalAlign.Left

                x.Cells.Item(1).Text = cUtilitarios.trataData(x.Cells.Item(1).Text.Split(" ")(0))
                x.Cells.Item(4).Text = cUtilitarios.trataData(x.Cells.Item(4).Text.Split(" ")(0))

                For i As Integer = 0 To x.Cells.Count - 1
                    textoCell = x.Cells.Item(i).Text.ToString.Replace("&atilde;", "a").Replace("&ccedil;", "c").Replace("(", "O").Replace(")", "O")
                    If tamanho(i) <= textoCell.Length Then
                        tamanho(i) = textoCell.Length
                        txtMax(i) = x.Cells.Item(i).Text
                    End If
                Next

                '''[new code] Mostra segurado nome completo 17.07.07           
                If (x.Cells.Item(2).Text.Length > 25) Then
                    x.Cells.Item(2).Text = "<span onmouseover=""return overlib('" & x.Cells.Item(2).Text & "');"" onmouseout='return nd();'>" & x.Cells.Item(2).Text.Substring(0, 25) & "...</span>"
                Else
                    x.Cells.Item(2).Text = "<span onmouseover=""return overlib('" & x.Cells.Item(2).Text & "');"" onmouseout='return nd();'>" & x.Cells.Item(2).Text & "</span>"
                End If
                '''[end  code]

                If primeiro Then
                    primeiro = False
                    Dim n As Integer
                    For i As Integer = 0 To x.Cells.Count - 1
                        x.Cells.Item(i).Text = "<div class='headerFakeDiv'id='headerFake" & i & "'></div>" & x.Cells.Item(i).Text
                    Next
                End If

                'ITEM PARA O LINK DO DOCUMENTO
                If x.Cells.Item(3).Text.IndexOf("Permitido") <> -1 Then
                    x.Cells.Item(5).Text = "<a href='../SEGW0060/downloads/Proposta_de_Adesao_com_DPS_-_BB.doc' target='_blank'>" & x.Cells.Item(5).Text & "</a>"
                    'http://qld.aliancadobrasil.com.br/SEG/SEGW0060/downloads/Proposta_de_Adesao_com_DPS_-_BB.doc

                ElseIf x.Cells.Item(3).Text.IndexOf("300") <> -1 Then
                    x.Cells.Item(5).Text = "<a href='../SEGW0060/downloads/Proposta_de_Adesao_com_DPS_-_BB.doc' target='_blank'>" & x.Cells.Item(5).Text & "</a>"
                    'http://qld.aliancadobrasil.com.br/SEG/SEGW0060/downloads/Proposta_de_Adesao_com_DPS_-_BB.doc

                ElseIf x.Cells.Item(3).Text.IndexOf("800") <> -1 Then
                    x.Cells.Item(5).Text = "<a href='../SEGW0060/downloads/Proposta_de_Adesao_com_DPS_-_BB_500.doc' target='_blank'>" & x.Cells.Item(5).Text & "</a>"
                    'http://qld.aliancadobrasil.com.br/SEG/SEGW0060/downloads/Proposta_de_Adesao_com_DPS_-_BB 500.doc

                ElseIf x.Cells.Item(3).Text.IndexOf("Idade") <> -1 Then
                    x.Cells.Item(5).Text = "<a href='../SEGW0060/downloads/Proposta_de_Adesao_com_DPS_-_BB.doc' target='_blank'>" & x.Cells.Item(5).Text & "</a>"
                    'http://qld.aliancadobrasil.com.br/SEG/SEGW0060/downloads/Proposta_de_Adesao_com_DPS_-_BB.doc

                ElseIf x.Cells.Item(3).Text.IndexOf("Contratual") <> -1 Then
                    x.Cells.Item(5).Text = "<a href='../SEGW0060/downloads/FICHA_DE_INFORMACOES_FINANCEIRAS_BB.doc' target='_blank'>" & x.Cells.Item(5).Text & "</a>"
                    'http://qld.aliancadobrasil.com.br/SEG/SEGW0060/downloads/FICHA_DE_INFORMACOES_FINANCEIRAS_BB.doc
                End If
                '-------------------------------

            Next

            Dim z As Integer = 0
            Dim texto As String

            Dim tamMin(13) As Integer
            tamMin(0) = 10
            tamMin(1) = 12
            tamMin(2) = 20 's
            tamMin(3) = 7
            tamMin(4) = 8
            tamMin(5) = 13
            tamMin(6) = 5
            tamMin(7) = 16
            tamMin(8) = 18
            tamMin(9) = 12
            tamMin(10) = 12
            tamMin(11) = 14
            tamMin(12) = 12
            tamMin(13) = 15

            Try


                For Each m As String In txtMax

                    texto = txtMax(z).ToString.Trim.Replace(" ", "O").Replace("-", "O").Replace("&atilde;", "a").Replace("&ccedil;", "c").PadRight(tamMin(z), "O").Replace("(", "O").Replace(")", "O")
                    Response.Write("<input type='hidden' value='" & texto & "' id='txtMax" & z & "'>" & vbCrLf)

                    z = z + 1
                Next
            Catch ex As Exception
                'Response.End()
            End Try
        End If

        Me.mMontaPaginacao(Grid.CurrentPageIndex, Grid.PageCount)
    End Sub

    Public Property CssClass() As String
        Get
            Return Me.lblPaginacao.Attributes.Item("class")
        End Get
        Set(ByVal Value As String)
            Me.lblPaginacao.Attributes.Add("class", Value)
        End Set
    End Property
End Class
