<%@ Page Language="vb" AutoEventWireup="false" Codebehind="MovimentacaoUmaUm_Form.aspx.vb" Inherits="segw0060.MovimentacaoUmaUm_Form"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>MovimentacaoUmaUm_Form</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<asp:panel id="pnlCadastro" runat="server" Visible="False" Width="100%" HorizontalAlign="Left">
				<TABLE id="Table2" cellSpacing="0" cellPadding="2" width="100%" border="0">
					<TR>
						<TD vAlign="middle" align="left" width="20%" colSpan="2" height="28">
							<asp:Label id="lblTitulo" runat="server" Width="200px" Font-Bold="True" CssClass="TituloUsuario">:: Cadastro de vida individual</asp:Label>&nbsp;</TD>
						<TD vAlign="top" align="left" width="80%" colSpan="5" height="28">
							<asp:Label id="lblAcaoIndividual" runat="server" CssClass="Caminhotela"></asp:Label></TD>
					</TR>
				</TABLE>
				<asp:Panel id="pnlCadastroTitular" runat="server" HorizontalAlign="Center" Wrap="False">
					<TABLE id="Table3" height="190" cellSpacing="1" cellPadding="1" width="100%" align="left"
						border="0">
						<TR>
							<TD align="right" width="20%">
								<asp:Label id="Label1" runat="server" CssClass="DescricaoCampo">Nome:</asp:Label></TD>
							<TD align="left" width="80%">
								<asp:TextBox id="txtTitNome" runat="server" Width="151px"></asp:TextBox>&nbsp;<IMG alt="" src="Images/help.gif"></TD>
						</TR>
						<TR>
							<TD align="right" width="20%" height="8">
								<asp:Label id="Label8" runat="server" CssClass="DescricaoCampo">CPF:</asp:Label></TD>
							<TD align="left" width="80%" height="8">
								<asp:TextBox id="txtTitCPF" runat="server"></asp:TextBox>&nbsp;<IMG alt="" src="Images/help.gif"></TD>
						</TR>
						<TR>
							<TD align="right" width="20%">
								<asp:Label id="Label9" runat="server" CssClass="DescricaoCampo">Nascimento:</asp:Label></TD>
							<TD align="left" width="80%">
								<asp:TextBox id="txtTitNascimento" runat="server" Width="72px"></asp:TextBox>&nbsp;<IMG alt="" src="Images/help.gif"></TD>
						</TR>
						<TR>
							<TD align="right" width="20%" height="4">
								<asp:Label id="Label10" runat="server" CssClass="DescricaoCampo">Sexo:</asp:Label></TD>
							<TD align="left" width="80%" height="5">
								<asp:DropDownList id="ddlTitSexo" runat="server">
									<asp:ListItem Value="Selecione">Selecione</asp:ListItem>
									<asp:ListItem Value="masc.">Masculino</asp:ListItem>
									<asp:ListItem Value="fem.">Feminino</asp:ListItem>
								</asp:DropDownList>&nbsp;<IMG alt="" src="Images/help.gif"></TD>
						</TR>
						<TR>
							<TD align="right" width="20%">
								<asp:Label id="Label11" runat="server" CssClass="DescricaoCampo">Admiss�o:</asp:Label></TD>
							<TD align="left" width="80%">
								<asp:TextBox id="txtTitAdmissao" runat="server" Width="72px"></asp:TextBox>&nbsp;<IMG alt="" src="Images/help.gif"></TD>
						</TR>
						<TR>
							<TD align="right" width="20%">
								<asp:Label id="Label13" runat="server" CssClass="DescricaoCampo">Sal�rio:</asp:Label></TD>
							<TD align="left" width="80%">
								<asp:TextBox id="txtTitSalario" runat="server"></asp:TextBox>&nbsp;<IMG alt="" src="Images/help.gif"></TD>
						</TR>
						<TR>
							<TD noWrap align="right" width="20%" height="2">
								<asp:Label id="Label12" runat="server" CssClass="DescricaoCampo">Capital Segurado:</asp:Label></TD>
							<TD align="left" width="80%" height="2">
								<asp:TextBox id="txtTitCapital" runat="server" ReadOnly="True"></asp:TextBox>&nbsp;<IMG alt="" src="Images/help.gif"></TD>
						</TR>
						<TR>
							<TD noWrap align="right" width="20%" height="2">
								<asp:Label id="Label15" runat="server" CssClass="DescricaoCampo">Desligamento:</asp:Label></TD>
							<TD align="left" width="80%" height="2">
								<asp:TextBox id="txtTitDesligamento" runat="server" Width="72px" MaxLength="10"></asp:TextBox>&nbsp;
								<asp:Image id="img_help_desligamento" runat="server" ImageUrl="Images/help.gif"></asp:Image></TD>
						</TR>
						<TR>
							<TD align="right" width="20%" colSpan="3" height="5"></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="3" height="18">
								<asp:Button id="btnTitGravar" runat="server" Width="65px" CssClass="Botao" Text="Gravar"></asp:Button>
								<asp:Button id="btnDesfazerAlteracao" runat="server" Width="229px" Visible="False" CssClass="Botao"
									Text="Desfazer altera��es deste segurado"></asp:Button>
								<asp:Button id="btnTitDesligar" runat="server" Width="66px" CssClass="Botao" Text="Desligar"></asp:Button>
								<asp:Button id="btnTitApagar" runat="server" Width="66px" CssClass="Botao" Text="Apagar"></asp:Button>
								<asp:Button id="btnCancelaTitular" runat="server" Width="66px" CssClass="Botao" Text="Cancelar"></asp:Button></TD>
						</TR>
						<TR>
							<TD align="center" colSpan="3"></TD>
						</TR>
					</TABLE>
				</asp:Panel> <!-- Fim do painel de cadastro do Titular --> <!-- Painel de cadastro do Dependente -->
				<asp:Panel id="pnlCadastroDependente" runat="server">
					<TABLE id="Table1" height="142" cellSpacing="1" cellPadding="1" width="100%" border="0">
						<TR>
							<TD noWrap align="right" width="20%">
								<asp:Label id="Label4" runat="server" CssClass="DescricaoCampo">Nome do titular:</asp:Label></TD>
							<TD width="80%">
								<asp:Label id="Label6" runat="server" CssClass="DescricaoCampo">Luiz Jos� de Souza</asp:Label></TD>
						</TR>
						<TR>
							<TD align="right" width="20%">
								<asp:Label id="Label2" runat="server" CssClass="DescricaoCampo">CPF:</asp:Label></TD>
							<TD width="80%">
								<asp:Label id="Label14" runat="server" CssClass="DescricaoCampo">xxx.xxx.xxx-xx</asp:Label></TD>
						</TR>
						<TR>
							<TD align="right" width="20%">
								<asp:Label id="Label7" runat="server" CssClass="DescricaoCampo">Nome:</asp:Label></TD>
							<TD width="80%">
								<asp:TextBox id="txtDepNome" runat="server" Width="320px"></asp:TextBox>&nbsp;<IMG alt="" src="Images/help.gif"></TD>
						</TR>
						<TR>
							<TD align="right" width="20%">
								<asp:Label id="Label3" runat="server" CssClass="DescricaoCampo">CPF:</asp:Label></TD>
							<TD width="80%">
								<asp:TextBox id="txtDepCPF" runat="server"></asp:TextBox>&nbsp;<IMG alt="" src="Images/help.gif"></TD>
						</TR>
						<TR>
							<TD align="right" width="20%">
								<asp:Label id="Label5" runat="server" CssClass="DescricaoCampo">Nascimento:</asp:Label></TD>
							<TD width="80%">
								<asp:TextBox id="txtDepNascimento" runat="server" Width="72px"></asp:TextBox>&nbsp;<IMG alt="" src="Images/help.gif"></TD>
						</TR>
						<TR>
							<TD align="right" width="20%" height="16">
								<asp:Label id="Label23" runat="server" CssClass="DescricaoCampo">Sexo:</asp:Label></TD>
							<TD width="80%" height="16">
								<asp:DropDownList id="ddlDepSexo" runat="server">
									<asp:ListItem Value="Selecione">Selecione</asp:ListItem>
									<asp:ListItem Value="masc.">Masculino</asp:ListItem>
									<asp:ListItem Value="fem.">Feminino</asp:ListItem>
								</asp:DropDownList>&nbsp;<IMG alt="" src="Images/help.gif"></TD>
						</TR>
						<TR>
							<TD align="right" width="20%" height="15">
								<asp:Label id="Label25" runat="server" CssClass="DescricaoCampo">Tipo:</asp:Label></TD>
							<TD width="80%" height="15">
								<asp:DropDownList id="ddlDepTipo" runat="server">
									<asp:ListItem Value="C&#244;njuge">C&#244;njuge</asp:ListItem>
									<asp:ListItem Value="Filho">Filho</asp:ListItem>
								</asp:DropDownList></TD>
						</TR>
						<TR>
							<TD align="right" width="20%" colSpan="2" height="13"></TD>
						</TR>
						<TR>
							<TD align="center" width="20%" colSpan="2">
								<asp:Button id="btnDepGravar" runat="server" Width="65px" CssClass="Botao" Text="Gravar"></asp:Button>
								<asp:Button id="Button15" runat="server" Width="229px" Visible="False" CssClass="Botao" Text="Desfazer altera��es deste segurado"></asp:Button>
								<asp:Button id="btnFecharDependente" runat="server" Width="62px" CssClass="Botao" Text="Cancelar"></asp:Button></TD>
						</TR>
					</TABLE>
				</asp:Panel> <!-- Fim do painel de cadastro do dependente --></asp:panel></form>
	</body>
</HTML>
