Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Partial Public Class Exportar
    Inherits System.Web.UI.Page

    Private Enum TipoDocumentoVidaWeb
        RegraParaSegurosDePessoas = 1
        DeclaracaoPessoalDeSaude = 2
        PropostaAdesaoSegurosPessoasCapitalIndividualAte300Mil = 3
        PropostaAdesaoSegurosPessoasCapitalIndividualDe300a800Mil = 4
        PropostaAdesaoSegurosPessoasCapitalIndividualAcima800Mil = 5
        FichaDeInformacoesFinanceiraConfidenciais = 6

        DeclaracaoDeInexistenciaPR = 7
        DeclaracaoDeCompanheira = 8
        DeclaracaoDeNaoCasadoeSemFilhos = 9
        DeclaracaoDeNaoCasadoeComFilhos = 10
        DeclaracaoDeCasadoeSemFilhos = 11
        DeclaracaoDeCasadoeComFilhos = 12
        AutorizacaoDeCreditoEmConta01_07 = 13
        DtIpdIfpd = 14
        AutorizacaoDeCreditoEmConta24_08 = 15
        MAv5 = 16
        IPAv2 = 17
        ValoresAcima600mil = 18
        ValoresAbaixo600mil = 19

    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("ramo")) Then
            Response.Write("<script>alert('N�o � possivel visualizar o arquivo.'); window.close();</script>")
            Exit Sub
        End If

        Dim cAmbiente As New Alianca.Seguranca.Web.ControleAmbiente
        cAmbiente.ObterAmbiente(String.Format("http://{0}/", Request.ServerVariables("SERVER_NAME")))

        If Not IsPostBack Then
            ProcessarDownload(Integer.Parse(Request("DocumentoVidaWebId")))
        End If

    End Sub

    ' italo.souza (Nova Consultoria) - 01/08/2011
    ' 539202 - Evolutiva do Sistema Vida Web
    ' Permitir salvar documentos somente em PDF
    Private Sub ProcessarDownload(ByVal documentoVidaWebId As Integer)

        Dim rptDoc As New ReportDocument
        Dim caminhoRelatorio As String = ObterCaminhoRelatorio(documentoVidaWebId)

        If File.Exists(caminhoRelatorio) Then
            rptDoc.Load(caminhoRelatorio)
            ConfigurarParametrosRelatorio(rptDoc)
            rptDoc.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, Path.GetFileNameWithoutExtension(caminhoRelatorio).Substring(3))
        End If

    End Sub

    Private Sub ConfigurarParametrosRelatorio(ByVal rptDoc As ReportDocument)

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS7561_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

        Dim dataTable As New Data.DataTable
        dataTable = bd.ExecutaSQL_DT()

        Dim parametrosRelatorio() As String = {"APOLICE", "RAMO", "SUBGRUPO", "SUBESTIPULANTE", _
            "NOMESUBGRUPO", "NOMESUBGRUPO", "NOMESUBESTIPULANTE", "CNPJESTIPULANTE", "ENDERECOSUBGRUPO", _
            "CNPJSUBGRUPO", "INICIOVIGENCIAAPOLICE", "FIMVIGENCIAAPOLICE", "AGENCIA"}

        For Each parametro As String In parametrosRelatorio
            rptDoc.SetParameterValue(parametro, dataTable.Rows(0)(parametro))
        Next parametro

    End Sub

    Private Function ObterCaminhoRelatorio(ByVal documentoVidaWebId As Integer) As String

        Dim caminhoRelatorio As String = Server.MapPath("~/reports/{0}.rpt")

        Select Case documentoVidaWebId

            Case TipoDocumentoVidaWeb.RegraParaSegurosDePessoas
                caminhoRelatorio = String.Format(caminhoRelatorio, "RelRegraParaSegurosDePessoas")

            Case TipoDocumentoVidaWeb.DeclaracaoPessoalDeSaude
                caminhoRelatorio = String.Format(caminhoRelatorio, "RelPropostaAdesaoSegurosPessoasCapitalIndividualAte300Mil")

            Case TipoDocumentoVidaWeb.PropostaAdesaoSegurosPessoasCapitalIndividualAte300Mil
                caminhoRelatorio = String.Format(caminhoRelatorio, "RelPropostaAdesaoSegurosPessoasCapitalIndividualAte300Mil")

            Case TipoDocumentoVidaWeb.PropostaAdesaoSegurosPessoasCapitalIndividualDe300a800Mil
                caminhoRelatorio = String.Format(caminhoRelatorio, "RelPropostaAdesaoSegurosPessoasCapitalIndividualDe300a800Mil")

            Case TipoDocumentoVidaWeb.PropostaAdesaoSegurosPessoasCapitalIndividualAcima800Mil
                caminhoRelatorio = String.Format(caminhoRelatorio, "RelPropostaAdesaoSegurosPessoasCapitalIndividualAcima800Mil")

            Case TipoDocumentoVidaWeb.FichaDeInformacoesFinanceiraConfidenciais
                caminhoRelatorio = String.Format(caminhoRelatorio, "RelFichaDeInformacoesFinanceiraConfidenciais")


                ' Check-list e Declaracoes
            Case TipoDocumentoVidaWeb.DeclaracaoDeInexistenciaPR
                caminhoRelatorio = String.Format(caminhoRelatorio, "RelFichaDeInformacoesFinanceiraConfidenciais - C�pia")

            Case TipoDocumentoVidaWeb.DeclaracaoDeCompanheira
                caminhoRelatorio = String.Format(caminhoRelatorio, "RelRegraParaSegurosDePessoas")

            Case TipoDocumentoVidaWeb.DeclaracaoDeNaoCasadoeSemFilhos
                caminhoRelatorio = String.Format(caminhoRelatorio, "RelRegraParaSegurosDePessoas")

            Case TipoDocumentoVidaWeb.DeclaracaoDeNaoCasadoeComFilhos
                caminhoRelatorio = String.Format(caminhoRelatorio, "RelRegraParaSegurosDePessoas")

            Case TipoDocumentoVidaWeb.DeclaracaoDeCasadoeSemFilhos
                caminhoRelatorio = String.Format(caminhoRelatorio, "RelRegraParaSegurosDePessoas")

            Case TipoDocumentoVidaWeb.DeclaracaoDeCasadoeComFilhos
                caminhoRelatorio = String.Format(caminhoRelatorio, "RelRegraParaSegurosDePessoas")

            Case TipoDocumentoVidaWeb.AutorizacaoDeCreditoEmConta01_07
                caminhoRelatorio = String.Format(caminhoRelatorio, "RelRegraParaSegurosDePessoas")

            Case TipoDocumentoVidaWeb.DtIpdIfpd
                caminhoRelatorio = String.Format(caminhoRelatorio, "RelRegraParaSegurosDePessoas")

            Case TipoDocumentoVidaWeb.AutorizacaoDeCreditoEmConta24_08
                caminhoRelatorio = String.Format(caminhoRelatorio, "RelRegraParaSegurosDePessoas")

            Case TipoDocumentoVidaWeb.MAv5
                caminhoRelatorio = String.Format(caminhoRelatorio, "RelRegraParaSegurosDePessoas")

            Case TipoDocumentoVidaWeb.IPAv2
                caminhoRelatorio = String.Format(caminhoRelatorio, "RelRegraParaSegurosDePessoas")

            Case TipoDocumentoVidaWeb.ValoresAcima600mil
                caminhoRelatorio = String.Format(caminhoRelatorio, "RelRegraParaSegurosDePessoas")

            Case TipoDocumentoVidaWeb.ValoresAbaixo600mil
                caminhoRelatorio = String.Format(caminhoRelatorio, "RelRegraParaSegurosDePessoas")


            Case Else
                Response.Write("Documento inexistente.")

        End Select

        Return caminhoRelatorio

    End Function

End Class