Partial Class ImprimirBoleto
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnFatura As System.Web.UI.WebControls.Button
    Protected WithEvents btnRelacaoVidas As System.Web.UI.WebControls.Button
    Protected WithEvents btnResumo As System.Web.UI.WebControls.Button
    Protected WithEvents imgPlus As System.Web.UI.WebControls.ImageButton
    Protected WithEvents pnlDependentes As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load        
        lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Subgrupo: " & Session("subGrupo") & " -> " & "Consultar / Imprimir Boleto"
        lblVigencia.Visible = True

        Dim imprimir As String = Request.QueryString("imprimir")
        Dim expandir As String = Request.QueryString("expandir")

        If expandir = 1 Then
            imgPlus.ImageUrl = "Images/imgMenos.gif"
            pnlDependentes.Visible = True
        Else
            imgPlus.ImageUrl = "images/imgmais.gif"
            pnlDependentes.Visible = False

        End If

        If imprimir <> "" Then
            pnlBotaoImprimir.Visible = True
        End If
        
        btnImprimir.Attributes.Add("onclick", "alert('Imprimindo boleto do per�odo de 20/11/06 a 19/12/06.')")
    End Sub

    Private Sub imgPlus_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPlus.Click
        If pnlDependentes.Visible = True Then

            imgPlus.ImageUrl = "Images/imgMais.gif"
            pnlDependentes.Visible = False
        Else
            imgPlus.ImageUrl = "Images/imgMenos.gif"
            pnlDependentes.Visible = True
        End If
    End Sub

    Private Sub chkMarcaTodos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMarcaTodos.CheckedChanged
        If chkMarcaTodos.Checked = True Then
            Checkbox1.Checked = True
        Else
            Checkbox1.Checked = False
        End If
    End Sub

    Private Sub btnFechaPagina_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFechaPagina.Click
        Response.Redirect("Centro.aspx")
    End Sub
End Class
