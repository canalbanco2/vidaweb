Public Class ManutencaoUsuario_Form
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblCadastroUsuario As System.Web.UI.WebControls.Label
    Protected WithEvents txtUsuario As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtLogin As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlAcesso As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlSituacao As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtEmail As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtConfirmacaoEmail As System.Web.UI.WebControls.TextBox
    Protected WithEvents pnlCadastroUsuario As System.Web.UI.WebControls.Panel
    Protected WithEvents btnExcluir As System.Web.UI.WebControls.Button
    Protected WithEvents btnCancelar As System.Web.UI.WebControls.Button
    Protected WithEvents btnIncluirUsuario As System.Web.UI.WebControls.Button
    Protected WithEvents txtCPF As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim requestAcao As String = Request.QueryString("acao")

        btnCancelar.Attributes.Add("onclick", "parent.parent.GB_hide();")
        Me.btnIncluirUsuario.Attributes.Add("onclick", "return validar();")


        If Not IsPostBack Then
            If Request.QueryString("acao") = "alterar" Then
                Me.txtCPF.Enabled = False
                mConsultaUsuario(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Request.QueryString("cpf"))
            Else
                Me.btnExcluir.Visible = False
            End If
        End If
    End Sub

    Private Sub mConsultaUsuario(ByVal apolice_id As Integer, ByVal ramo_id As Int16, ByVal subgrupo_id As Int16, ByVal cpf As String)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        bd.consultar_usuarios_SPS(apolice_id, ramo_id, subgrupo_id, cpf)
        'cUtilitarios.br(bd.SQL)
        'Response.End()
        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR

        If dr.Read() Then

            pnlCadastroUsuario.Visible = True
            btnExcluir.Visible = True
            btnIncluirUsuario.Text = "Gravar"

            txtUsuario.Text = dr.Item("nome")

            txtCPF.Text = cUtilitarios.trataCPF(dr.Item("cpf"))

            txtLogin.Text = dr.Item("login_rede")
            Me.ddlAcesso.SelectedValue = dr.Item("Acesso")
            Me.ddlSituacao.SelectedValue = dr.Item("Situacao")
            txtEmail.Text = dr.Item("email")
            txtConfirmacaoEmail.Text = dr.Item("email")
        End If
        dr.Close()
        dr = Nothing

    End Sub


    Private Sub btnIncluirUsuario_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIncluirUsuario.Click
        Dim acao As String = ""

        'Variaveis de configura��o do email
        Dim sigla_recurso, key, sigla_sistema, chave, de, para, assunto, mensagem, cc, anexo, formato, idUser As String

        sigla_recurso = "segw0060"
        sigla_sistema = "SEGBR"
        'chave = "vida_web-" & Session("apolice_id") & "-" & Session("subgrupo_id") & "-" & Session("ramo")
        chave = "vvwra600"
        de = "gestao@alwaysweb.com"
        para = Me.txtEmail.Text
        assunto = "Cadastro no Movimenta��o de Vida na Web"

        key = Session.SessionID.ToString

        mensagem = "<html><body>" & Me.txtUsuario.Text.Replace("'", "''") & ", <br> "
        mensagem &= "Foi criado um novo usu�rio para o senhor(a) no Movimenta��o de Vida na Web. <br>"
        mensagem &= "Para acess�-lo basta ir at� a <a href=''url''>url</a> e informar os dados: <br>"
        mensagem &= "<li>Login = " & Me.txtLogin.Text & "</li>"
        mensagem &= "<li>Senha = " & key.Substring(key.Length - 9, 8) & "</li>"
        mensagem &= "<br><br>"
        mensagem &= "Obrigado.<br>"
        mensagem &= "XXX da Silva.<br>"
        mensagem &= "</body></html>"

        cc = "null"
        anexo = "null"
        formato = "2"
        idUser = Session("usuario")

        Try

            '''Flag para saber se a a��o de altera��o
            If Me.txtCPF.Enabled = False Then
                Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

                bd.atualizarUsuario(Session("apolice"), Session("ramo"), Session("subgrupo_id"), _
                                    cUtilitarios.destrataCPF(txtCPF.Text), Me.txtUsuario.Text, _
                                    Me.txtLogin.Text, Me.ddlAcesso.SelectedValue, Me.ddlSituacao.SelectedValue _
                                    , Me.txtEmail.Text)
                bd.ExecutaSQL()

                enviaEmailSenha(sigla_recurso, sigla_sistema, chave, de, para, assunto, mensagem, cc, anexo, formato, idUser)


                acao = "alterado"
            End If

            '''Flag para saber se a a��o de inclus�o
            If Me.txtCPF.Enabled = True Then
                Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

                bd.verificaUsuario(cUtilitarios.destrataCPF(txtCPF.Text))

                Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR

                If dr.Read Then
                    Throw New FieldAccessException("CPF existente em nossa base de dados, por favor, tente novamente.")
                End If

                bd.incluiUsuario(Session("apolice"), Session("ramo"), Session("subgrupo_id"), _
                                    cUtilitarios.destrataCPF(txtCPF.Text), Me.txtUsuario.Text, _
                                    Me.txtLogin.Text, Me.ddlAcesso.SelectedValue, Me.ddlSituacao.SelectedValue _
                                    , Me.txtEmail.Text)

                bd.ExecutaSQL()

                enviaEmailSenha(sigla_recurso, sigla_sistema, chave, de, para, assunto, mensagem, cc, anexo, formato, idUser)

                acao = "inclu�do"
                dr.Close()
                dr = Nothing
            End If

            cUtilitarios.escreveScript("parent.parent.GB_reloadOnClose('true');")
            cUtilitarios.escreveScript("parent.parent.GB_hide();")
            cUtilitarios.br("Usu�rio " + acao + " com sucesso.")
        Catch ex As FieldAccessException
            'cUtilitarios.escreveScript("window.document.onload = alert('" + ex.Message + "');")
            cUtilitarios.escreveScript("var msg = '" + ex.Message + "';")
        Catch ex As Exception
            If ex.Message.IndexOf("PRIMARY KEY") <> -1 Then
                cUtilitarios.escreveScript("var msg = 'CPF existente em nossa base de dados, por favor, tente novamente.';")
            Else
                Throw ex
            End If

            'cUtilitarios.escreveScript("parent.parent.GB_hide();")
            'cUtilitarios.br("Ocorreu um erro ao tentar executar a opera��o, por favor, tente novamente.")
        End Try
    End Sub

    Private Sub enviaEmailSenha(ByVal sigla_recurso As String, ByVal sigla_sistema As String, ByVal chave As String, ByVal de As String, ByVal para As String, ByVal assunto As String, ByVal mensagem As String, ByVal cc As String, ByVal anexo As String, ByVal formato As String, ByVal id As String)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)
        bd.SGSS0782_SPI(sigla_recurso, sigla_sistema, chave, de, para, assunto, mensagem, cc, anexo, formato, id)

        Try
            bd.ExecutaSQL()
        Catch ex As Exception
            Throw ex
            'cUtilitarios.escreveScript("parent.parent.GB_hide();")
            'cUtilitarios.br("Ocorreu um erro ao tentar executar a opera��o, por favor, tente novamente.")
        End Try

    End Sub

    Private Sub btnExcluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcluir.Click
        Try

            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

            bd.excluiUsuario_spd(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cUtilitarios.destrataCPF(txtCPF.Text))

            bd.ExecutaSQL()

            cUtilitarios.escreveScript("parent.parent.GB_reloadOnClose('true');")
            cUtilitarios.escreveScript("parent.parent.GB_hide();")
            cUtilitarios.br("Usu�rio exclu�do com sucesso.")
        Catch ex As Exception
            cUtilitarios.escreveScript("parent.parent.GB_hide();")
            cUtilitarios.br("Ocorreu um erro ao tentar executar a exclus�o, por favor, tente novamente.")
        End Try
    End Sub

End Class

