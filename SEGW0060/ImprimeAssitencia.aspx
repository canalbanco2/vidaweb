﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ImprimeAssitencia.aspx.vb" Inherits="segw0060.ImprimeAssitencia" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head runat="server">
        <title>Consultar Assitências</title>
        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema" />
		<link href="CSS/EstiloBase.css" type="text/css" rel="stylesheet" />
    </head>
    <script>
	    function Imprimir()
	    {
		    print();
		    history.back()
	    }
	</script>
    <body MS_POSITIONING="FlowLayout" onload="Imprimir();">
		<form id="form1" runat="server" >
            <TABLE id="tabelaMovimentacao2" style="MARGIN-TOP: 10px; MARGIN-BOTTOM: 10px; MARGIN-LEFT: 0px; BORDER-COLLAPSE: collapse; width: 1100px;"
		        borderColor="#cccccc" cellSpacing="0" cellPadding="0" border="0" align="center">
		        <TR>
		            <TD style="width: 10px">
                       &nbsp;
                    </TD>
			        <TD vAlign="bottom" style="width: 890px; height: 34px;">
                        <asp:label
                            id=lblTextoAssitencia 
                            runat="server" 
                            Width="1048px" 
                            Height="520px" 
                            CssClass="Impressao" />
                    </TD>
                 </TR>
	        </TABLE>
        </form>
    </body>
</html>
