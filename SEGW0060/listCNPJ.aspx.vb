Partial Class listCNPJ
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        Dim data As String
        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader
        Dim str As String

        openResp()

        Try
            data = Request("txtCNPJ")
        
            BD.SEGS6929_SPS(6785, 0, Session("cpf"), data, 1)

            Try
                dr = BD.ExecutaSQL_DR()

                While dr.Read()
                    'str = dr.GetValue(0).ToString() + "-" + dr.GetValue(1).ToString() + "-" + dr.GetValue(2).ToString()
                    str = dr.GetValue(0).ToString()
                    writeLi(str)
                End While
                dr.Close()
                dr = Nothing

            Catch ex As Exception
                writeLi(ex.Message & "<br>" & BD.SQL)
            End Try

        Catch ex As Exception
            writeLi(ex.Message)
        End Try

        closeResp()
        Response.End()

    End Sub

    Public Sub openResp()
        Response.Write("<ul>")
    End Sub

    Public Sub closeResp()
        Response.Write("</ul>")
    End Sub

    Public Sub write(ByVal str As String)
        Response.Write(str)
    End Sub

    Public Sub writeLi(ByVal str As String)
        Response.Write("<li>" & str & "</li>")
    End Sub

    Public Function li(ByVal str As String) As String
        Return "<li>" & str & "</li>"
    End Function

End Class
