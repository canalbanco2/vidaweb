'------------------------------------------------------------------------------
' <gerado automaticamente>
'     Este código foi gerado por uma ferramenta.
'
'     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
'     o código for recriado
' </gerado automaticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class resumoFatura

    '''<summary>
    '''Controle Form1.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''Controle pnlResumo.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents pnlResumo As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Controle lblNavegacao.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblNavegacao As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblVigencia.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblVigencia As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle Imagebutton2.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Imagebutton2 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Controle Label1.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Label1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle Label2.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Label2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle Label3.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Label3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle Label4.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents Label4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblVidasFA.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblVidasFA As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblCapitalFA.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblCapitalFA As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblPremioFA.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblPremioFA As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblIOFA.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblIOFA As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblVlPremioBrutoA.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblVlPremioBrutoA As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle btnInclusoes.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnInclusoes As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Controle lblInclusoes_vidas.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblInclusoes_vidas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblInclusoes_capital.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblInclusoes_capital As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblInclusoes_premio.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblInclusoes_premio As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblVlPremioBrutoI.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblVlPremioBrutoI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle btnExclusoes.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnExclusoes As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Controle lblExclusoes_vidas.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblExclusoes_vidas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblExclusoes_capital.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblExclusoes_capital As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblExclusoes_premio.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblExclusoes_premio As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblVlPremioBrutoE.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblVlPremioBrutoE As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle btnSubTotal.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnSubTotal As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Controle lblSubTotal_vidas.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblSubTotal_vidas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblSubTotal_capital.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblSubTotal_capital As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblSubTotal_premio.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblSubTotal_premio As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblSubTotalPremio.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblSubTotalPremio As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle btnAlteracoes.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnAlteracoes As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Controle lblAlteracoes_vidas.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblAlteracoes_vidas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblAlteracoes_capital.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblAlteracoes_capital As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblAlteracoes_premio.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblAlteracoes_premio As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblVlPremioBrutoAlt.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblVlPremioBrutoAlt As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle btnAcertos.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnAcertos As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Controle lblAcertos_vidas.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblAcertos_vidas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblAcertos_capital.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblAcertos_capital As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblAcertos_premio.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblAcertos_premio As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblVlPremioBrutoAc.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblVlPremioBrutoAc As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblVidasFT.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblVidasFT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblCapitalFT.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblCapitalFT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblPremioFT.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblPremioFT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblIOFFT.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblIOFFT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblPremioBrutoFT.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblPremioBrutoFT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle btnDpsPendente.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnDpsPendente As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Controle lblDPS_vidas.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblDPS_vidas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblDPS_capital.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblDPS_capital As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblDPS_premio.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblDPS_premio As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblVlPremioBrutoPend.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblVlPremioBrutoPend As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle btnExcAgendada.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnExcAgendada As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''Controle lblExcAgendada_vidas.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblExcAgendada_vidas As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblExcAgendada_capital.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblExcAgendada_capital As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblExcAgendada_premio.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblExcAgendada_premio As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle lblVlPremioBrutoEAg.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents lblVlPremioBrutoEAg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Controle btnVoltar.
    '''</summary>
    '''<remarks>
    '''Campo gerado automaticamente.
    '''Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
    '''</remarks>
    Protected WithEvents btnVoltar As Global.System.Web.UI.WebControls.Button
End Class
