﻿Imports System.Data
Imports System.Drawing
Imports System.Web.UI.WebControls
Imports System.Web.HttpContext



Partial Public Class HistoricoDPS
    Inherits System.Web.UI.Page



    Dim dtDataTable As DataTable = New Data.DataTable()
    Dim cpf As String
    Dim subgrupo As String
    Dim dt_ini As String
    Dim dt_fim As String
    Dim opcao As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblNavegacao.Text = "Ramo/Apólice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomesubGrupo") & "<br>Função: Histórido de DPS Aprovada ou Recusada"
        Try
            If Not IsPostBack Then
         

                InicializaInterface()

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub InicializaInterface()
        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim count As Integer
        Dim text As String
        Dim value As String

        cboSubGrupo.Items.Clear()
        txtCPF.Text = "000.000.000.00"
        txtDtFim.Text = "00/00/0000"
        txtDtInicio.Text = "00/00/0000"


        cboSubGrupo.Items.Insert(0, New Web.UI.WebControls.ListItem("Todos os Subgrupos", "0"))
        cboSubGrupo.Items.Insert(0, New Web.UI.WebControls.ListItem("CPF", "CPF"))
        gridHistorico.DataBind()


        BD.consulta_subgrupo_sps(Session("apolice"), Session("ramo"), Session("cpf"))

        Try
            dr = BD.ExecutaSQL_DR()

            count = 2

            If Not dr Is Nothing Then
                While dr.Read()

                    value = dr.GetValue(1).ToString()
                    text = dr.GetValue(1).ToString() & " - " & dr.GetValue(0).ToString()
                    cboSubGrupo.Items.Insert(count, text)
                    cboSubGrupo.Items(count).Value = value

                    count = count + 1

                End While
            End If


        Catch ex As Exception
            Dim v_sCodigoErro As String = "13 - Inicializa Histórico DPS"
            Dim excp As New clsException("Ocorreu um erro inesperado. Favor entrar em contato com o administrador do sistema \nCódigo: " & v_sCodigoErro, ex)
        End Try

    End Sub

    Private Sub btnPesquisar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing


        If ValidaCampos() = True Then
            BD.BuscarHistoricoDps(Session("ramo"), Session("apolice"), subgrupo, cpf, dt_ini, dt_fim)
        Else
            Exit Sub
        End If

        Try
            dr = BD.ExecutaSQL_DR()

            If Not dr Is Nothing Then

                dtDataTable.Load(dr)
                gridHistorico.DataSource = dtDataTable
                gridHistorico.PageIndex = 0
                gridHistorico.DataBind()
                gridHistorico_IsEmptv()

            End If


        Catch ex As Exception
            Dim v_sCodigoErro As String = "13 - Pesquisar Histórico de DPS"
            Dim excp As New clsException("Ocorreu um erro inesperado. Favor entrar em contato com o administrador do sistema \nCódigo: " & v_sCodigoErro, ex)
        End Try
    End Sub

    Protected Sub gridHistorico_IsEmptv()

        If gridHistorico.Rows.Count.ToString = 0 Then
            MSG_ERRO("Não há Histórico de DPS aceita ou recusada para os dados pesquisados.")
        End If

    End Sub

    Protected Sub gridHistorico_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gridHistorico.PageIndexChanging

        atualizaPaginacao(e.NewPageIndex)

    End Sub


    Protected Sub atualizaPaginacao(ByVal indice As Integer)

        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        If ValidaCampos() = True Then
            BD.BuscarHistoricoDps(Session("ramo"), Session("apolice"), subgrupo, cpf, dt_ini, dt_fim)
        Else
            Exit Sub
        End If
        Try
            dr = BD.ExecutaSQL_DR()

            If Not dr Is Nothing Then

                dtDataTable.Load(dr)
                gridHistorico.DataSource = dtDataTable
                gridHistorico.PageIndex = indice
                gridHistorico.DataBind()
                gridHistorico_IsEmptv()
            End If

        Catch ex As Exception
            Dim v_sCodigoErro As String = "Pagina de DPS inexistente"
            Dim excp As New clsException("Ocorreu um erro inesperado. Favor entrar em contato com o administrador do sistema \nCódigo: " & v_sCodigoErro, ex)
        End Try


    End Sub
    Protected Sub btnLimpar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpar.Click

        dtDataTable = Nothing
        gridHistorico.DataBind()
        txtCPF.Text = "000.000.000.00"
        txtDtFim.Text = "00/00/0000"
        txtDtInicio.Text = "00/00/0000"


    End Sub


    Protected Function ValidaCampos() As Boolean

        Dim dDataIni As Date
        Dim dDataFim As Date
        Dim valida As Boolean

        valida = True
        cpf = ""
        subgrupo = ""
        dt_ini = ""
        dt_fim = ""

        opcao = cboSubGrupo.SelectedItem.Value


        If opcao = "CPF" Then
            cpf = txtCPF.Text.Replace(".", "")
        Else
            txtCPF.Text = "000.000.000.00"
            If opcao = "0" Then
                'TODOS OS GRUPOS  
                cpf = ""
                subgrupo = ""
            Else
                subgrupo = opcao
            End If
        End If

        'TRATAMENTO DAS DATAS  
        If (txtDtInicio.Text <> "00/00/0000" Or txtDtFim.Text <> "00/00/0000") Then
            If (DateTime.TryParse(txtDtInicio.Text, dDataIni)) Then
                dt_ini = Format(dDataIni, "yyyyMMdd")
            Else
                'insira o codigo do seu alerta informando que a data inicio não é valida.
                MSG_ERRO("Data inicial inválida")
                txtDtInicio.Text.Remove(0, 10)
                'txtDtInicio.Focus()
                valida = False

            End If

            If (DateTime.TryParse(txtDtFim.Text, dDataFim)) Then
                dt_fim = Format(dDataFim, "yyyyMMdd")
            Else
                'insira o codigo do seu alerta informando que a data fim não é valida.
                MSG_ERRO("Data final inválida")
                valida = False

            End If

            If (dDataIni > dDataFim) Then
                'insira o codigo do seu alerta informando que a data inicio não pode ser maior que a data fim
                MSG_ERRO("Data inicial maior que a final")
                valida = False
            End If
        End If


        Return valida

    End Function





    Private Sub btnGerarExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGerarExcel.Click
        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        If ValidaCampos() = True Then
            BD.BuscarHistoricoDps(Session("ramo"), Session("apolice"), subgrupo, cpf, dt_ini, dt_fim)
        Else
            Exit Sub
        End If

        Try
            dr = BD.ExecutaSQL_DR()

            If Not dr Is Nothing Then

                dtDataTable.Load(dr)
                gridHistorico.DataSource = dtDataTable
                gridHistorico.DataBind()
                gridHistorico_IsEmptv()
                If gridHistorico.Rows.Count.ToString + 1 < 65536 And gridHistorico.Rows.Count.ToString > 0 Then


                    gridHistorico.AllowPaging = "False"
                    gridHistorico.DataBind()

                    For Each cell As TableCell In gridHistorico.HeaderRow.Cells
                        cell.BackColor = Color.FromArgb(0, 51, 153)
                        cell.ForeColor = Color.White

                    Next

                    gridHistorico.BorderColor = Color.Black

                    Dim tw As New IO.StringWriter()
                    Dim hw As New System.Web.UI.HtmlTextWriter(tw)
                    Dim frm As Web.UI.HtmlControls.HtmlForm = New Web.UI.HtmlControls.HtmlForm()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("content-disposition", "attachment;filename=" & "HistoricoDPS" & ".xls")
                    Response.Charset = ""
                    EnableViewState = False
                    Controls.Add(frm)
                    frm.Controls.Add(gridHistorico)
                    frm.RenderControl(hw)

                    Response.Write(tw.ToString())
                    Response.End()
                    gridHistorico.AllowPaging = "True"
                    gridHistorico.DataBind()
                Else
                    MSG_ERRO("Não é possivel gerar uma planilha com " & gridHistorico.Rows.Count.ToString + 1 & " linhas")
                End If


            End If

        Catch ex As Exception
            Dim v_sCodigoErro As String = "Não existe dados para inserir na planilha"
            Dim excp As New clsException("Ocorreu um erro inesperado. Favor entrar em contato com o administrador do sistema \nCódigo: " & v_sCodigoErro, ex)
        End Try

    End Sub

    Protected Sub MSG_ERRO(ByVal msgDefault As String)

        Dim strMessage As String
        strMessage = msgDefault

        Dim strScript As String = "<script language=JavaScript>"
        strScript += "alert(""" & strMessage & """);"
        strScript += "</script>"

        If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "clientScript", strScript)
        End If


    End Sub

End Class