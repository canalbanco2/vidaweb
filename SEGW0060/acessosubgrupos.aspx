<%@ Register TagPrefix="uc1" TagName="paginacaogrid_sub_grupo" Src="paginacaogrid_sub_grupo.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AcessoSubGrupos.aspx.vb" Inherits="segw0060.AcessoSubGrupos"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AcessoSubGrupos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
			<link href="scripts/box/box.css" rel="stylesheet" type="text/css" media="all">
				<script type="text/javascript" src="scripts/box/AJS.js"></script>
				<script type="text/javascript" src="scripts/box/box.js"></script>
				<script type="text/javascript">
			var GB_IMG_DIR = "scripts/box/";
			GreyBox.preloadGreyBoxImages();
				</script>
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<P>
				<asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela">Label</asp:label><BR>
				<asp:Label id="lblVigencia" runat="server" CssClass="Caminhotela"></asp:Label></P>
			<P>
				<asp:Label id="lblListagem" runat="server" CssClass="Caminhotela">Lista de subgrupo e usu�rio</asp:Label>
				<br> <!-- In�cio da grid de Usu�rios -->
				<asp:datagrid id="grdPesquisa" runat="server" AllowPaging="True" PageSize="10" AutoGenerateColumns="False"
					CellPadding="3" Width="60%">
					<SelectedItemStyle CssClass="ponteiro"></SelectedItemStyle>
					<AlternatingItemStyle CssClass="ponteiro"></AlternatingItemStyle>
					<ItemStyle CssClass="ponteiro"></ItemStyle>
					<HeaderStyle CssClass="titulo-tabela sem-sublinhado"></HeaderStyle>
					<FooterStyle CssClass="0019GridFooter"></FooterStyle>
					<Columns>
						<asp:BoundColumn DataField="subgrupo_id" Visible="False"></asp:BoundColumn>
						<asp:BoundColumn DataField="cpf" Visible="False"></asp:BoundColumn>
						<asp:BoundColumn DataField="nome" HeaderText="Subgrupo"></asp:BoundColumn>
						<asp:BoundColumn DataField="usuario" HeaderText="Usu�rio"></asp:BoundColumn>
					</Columns>
					<PagerStyle Visible="false"></PagerStyle>
				</asp:datagrid>
				<uc1:paginacaogrid_sub_grupo width="60%" id="ucPaginacao" runat="server"></uc1:paginacaogrid_sub_grupo>
				<BR> <!-- Fim da grid de Usu�rios -->
			</P>
			<div align="center" style="WIDTH:500px"><br>
				&nbsp;
				<asp:Button id="btnIncluir" runat="server" CssClass="Botao" Text="Incluir" Width="56px"></asp:Button>
			</div>
			<P></P>
			<P>&nbsp;</P>
		</form>
	</body>
</HTML>
