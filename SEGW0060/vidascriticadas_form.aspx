<%@ Page Language="vb" AutoEventWireup="false" Codebehind="VidasCriticadas_Form.aspx.vb" Inherits="segw0060.VidasCriticadas_Form"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AcessoSubGrupos_Form</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<P>
				<asp:Panel id="pnlSubGrupos" runat="server" DESIGNTIMEDRAGDROP="44" Width="99%">
					<TABLE id="Table1" cellSpacing="0" cellPadding="2" width="100%" border="0">
						<TR>
							<TD align="left" colSpan="2" height="1">
								<asp:Label id="lblTituloSuBgrupo" runat="server" CssClass="Caminhotela">:: Vida criticada</asp:Label></TD>
						</TR>
						<TR>
							<TD vAlign="middle" align="right" width="160" height="29">Nome:
							</TD>
							<TD height="29">Rodrigo Augusto&nbsp;
								<asp:Image id="Image1" runat="server" ImageUrl="Images/help.gif"></asp:Image></TD>
						</TR>
						<TR>
							<TD vAlign="middle" align="right" width="160">CPF:
							</TD>
							<TD>
								<asp:TextBox id="TextBox1" runat="server" Width="96px">xxx.xxx.xxx-xx</asp:TextBox>&nbsp;
								<asp:Image id="Image2" runat="server" ImageUrl="Images/help.gif"></asp:Image>
								<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1" ErrorMessage="*"></asp:RequiredFieldValidator></TD>
						</TR>
						<TR>
							<TD vAlign="middle" noWrap align="right" width="160" height="15">Data de 
								Nascimento:
							</TD>
							<TD height="15">xx/xx/xxxx&nbsp;
								<asp:Image id="Image3" runat="server" ImageUrl="Images/help.gif"></asp:Image></TD>
						</TR>
						<TR>
							<TD vAlign="top" align="center" colSpan="2"><BR>
								<asp:Button id="btnExcluir" runat="server" Width="56px" CssClass="Botao" Text="Gravar"></asp:Button>
								<asp:Button id="btnCancelar" runat="server" Width="67px" CssClass="Botao" Text="Cancelar"></asp:Button></TD>
						</TR>
					</TABLE>
				</asp:Panel></P>
		</form>
	</body>
</HTML>
