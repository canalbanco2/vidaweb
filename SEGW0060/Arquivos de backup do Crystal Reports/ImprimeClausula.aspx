﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ImprimeClausula.aspx.vb" Inherits="segw0060.ImprimeClausula" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <title>Clausulas</title>

    <link href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">

    <style type="text/css">
        #reportHeader { position:fixed; top: 0;}
        #reportFooter { position:fixed; bottom: 0; width: 100%;}
        #reportContent { margin:0;padding:0;}
        #reportContent { margin:0;padding:0;}
        thead {display:table-header-group;}
        tfoot {display: table-footer-group;}
    </style>

    <script type="text/javascript">
        window.onload = function () {
            print();
		    history.back()
        }
    </script>

</head>
<body>

    <div id="reportHeader">
        &nbsp;<img src="images/logo.png" style="height: 71px" width="162" /></div>

    <div id="reportContent">

        <table>

            <thead>
                <tr>
                    <th style="height: 90px;">&nbsp;</th>
                </tr>
            </thead>

            <tfoot>
                <tr>
                    <th style="height: 110px;">&nbsp;</th>
                </tr>
            </tfoot>

            <tbody>
                <tr>
                    <td valign="top">
                        <div class="Impressao">
                            <form id="form1" runat="server" >
                                <asp:label id="lblTextoClausula" runat="server"></asp:label><br /><br />
                                <div align="right"><img alt="" src="images\Assinatura.png" width="219"></div><br />
                            </form>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div id="reportFooter">
        <p class="Impressao" style="text-align: center">COMPANHIA DE SEGUROS ALIANÇA DO BRASIL - CNPJ: 28.196.889/0001-43<br>
        Central de Atendimento aos Clientes: 0800 729 7000<br />
        Deficientes Auditivos ou de Fala: 0800 729 0088 / Ouvidoria: 0800 880 2930<br />
        Correio eletrônico: alianca@aliancadobrasil.com.br - www.aliancadobrasil.com.br<br />
        Endereço: Rua Manoel da Nóbrega, 1280, 9º andar, São Paulo, SP - CEP: 04001-004</p>
    </div>
    
</body>
</html>