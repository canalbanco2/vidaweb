﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RelatorioSinistro.aspx.vb" Inherits="segw0060.RelatorioSinistro" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
        <title>Relatório de Sinistro</title>
        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
	    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
	    <meta content="JavaScript" name="vs_defaultClientScript"/>
	    <meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema"/>
	    <link href="CSS/EstiloBase.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
	        .linkButton
            {
	            float : left;
	            padding : 5px 10px;
	            margin : 10px 5px 0 5px;
	            width : 70px;
	            background-color : #13024F;
	            color : White;
	            text-align : center;
            }
	        #tableBtn
            {	
	            padding: 10px 15px;
            }

            #tableBtn tr td 
            {
	            width: 200px;
	            padding-left: 25px;
            }

	    </style>
</head>
<script type="text/javascript">    
    function linkRedirect_click(sinistro_id, cpf, ambiente){   
        if (ambiente == "3") {
            window.open("https://qld.aliancadobrasil.com.br/ite/itew0126/Default.aspx?sinistro=" + sinistro_id + "&cpf=" + cpf);
        } else {
            window.open("http://www.aliancadobrasil.com.br/ite/itew0126/Default.aspx?sinistro=" + sinistro_id + "&cpf=" + cpf);
        }
        //window.open("https://qld.aliancadobrasil.com.br/ite/itew0126/Default.aspx?utm_source=home-site&utm_medium=destaques&utm_campaign=sinistros&sinistro=" + sinistro_id + "&cpf=" + cpf);
        //window.open("http://www.bbseguros.com.br/alianca/atendimentobb/sinistros.html?utm_source=home-site&utm_medium=destaques&utm_campaign=sinistros&sinistro=" + sinistro_id + "&cpf=" + cpf);
        //window.open("http://localhost:2152/Default.aspx?sinistro=" + sinistro_id + "&cpf=" + cpf);
        //window.open("http://localhost:2152/itew0126?utm_source=home-site&utm_medium=destaques&utm_campaign=sinistros&sinistro=" + sinistro_id + "&cpf=" + cpf);
    }
</script>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
        
    <div>           
        <asp:Label ID="lblCabecalho" runat="server" Text="Relatório de Sinistro" Width="350" CssClass = "lblCabecalho"></asp:Label>
            <div id="div_botoes">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table id="tableBtn" cellspacing="0" width="300" border="0">
                        <tr>
                            <td>
                                <asp:Button ID="btnAberto" runat="server" Height="42px" OnClick="btnAberto_click" Text="Aberto" Width="120px"/>
                            </td>
                            <td>
                                <asp:Button ID="btnPendentes" runat="server" Height="42px" OnClick="btnPendentes_click" Text="Pendentes" Width="120px"/>
                            </td>
                            <td>
                                <asp:Button ID="btnPagos" runat="server" Height="42px" OnClick="btnPagos_click" Text="Pagos" Width="120px"/>
                            </td>
                            <td>
                                <asp:Button ID="btnTodos" runat="server" Height="42px" OnClick="btnTodos_click" Text="Todos" Width="120px"/>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                </asp:UpdatePanel>
            </div>
             <div style="width:auto">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false"> 
                    <ContentTemplate>
                        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                        <asp:HiddenField  value="" id="vlTelaAberta" runat="Server"/> 
                    </ContentTemplate> 
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnAberto" EventName="click" />
                        <asp:AsyncPostBackTrigger ControlID="btnPendentes" EventName="click" />
                        <asp:AsyncPostBackTrigger ControlID="btnPagos" EventName="click" />
                        <asp:AsyncPostBackTrigger ControlID="btnTodos" EventName="click" />
                        <asp:AsyncPostBackTrigger ControlID="LinkVoltar" EventName="click" />
                    </Triggers>                                   
                </asp:UpdatePanel> 
             </div>
             <div class="opcao_botao" id="div_botoes_opcao">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:LinkButton ID="LinkGerarExcel" runat="server" onclick="lnkGerarExcel_Click" CssClass="linkButton" Visible = "false" >Gerar Excel</asp:LinkButton>
                        <asp:LinkButton ID="LinkImprimir" runat="server" OnClick="lnkImprimir_Click" CssClass="linkButton" Visible = "false" >Imprimir</asp:LinkButton> 
                        <asp:LinkButton ID="LinkVoltar" runat="server" OnClick="lnkVoltar_Click" CssClass="linkButton" Visible = "false" >Voltar</asp:LinkButton>
                    </ContentTemplate> 
                </asp:UpdatePanel>
             </div>      
        </div>          
    </form>
</body>
</html>
