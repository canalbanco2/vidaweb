﻿<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Clausulas.aspx.vb" Inherits="segw0060.Clausulas"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Clausulas</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="form1" runat="server">
		<asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela">Label</asp:label><asp:label id="lblVigencia" runat="server" CssClass="Caminhotela"><br>COTEMINAS S.A - Período de competência: 01/12/2006 até 31/12/2006</asp:label><BR>
		<BR>
    <div>
	    <TABLE id="tabelaMovimentacao2" style="MARGIN-TOP: 10px; MARGIN-BOTTOM: 10px; MARGIN-LEFT: 0px; BORDER-COLLAPSE: collapse; width: 648px;"
		    borderColor="#cccccc" cellSpacing="0" cellPadding="0" border="0">
		    <TR>
			    <TD vAlign="bottom" style="width: 790px; height: 34px;">
				    <asp:Label id="Label2" runat="server" CssClass="Caminhotela" Width="70px">Cláusulas</asp:Label>
					<asp:DropDownList id="DropdownSelClausula" runat="server" Width="650px" AutoPostBack="True">
					</asp:DropDownList></TD>
		    </TR>
		    <TR>
			    <TD align="left" style="width: 790px; height: 308px;">
				    <DIV style="OVERFLOW: auto" DESIGNTIMEDRAGDROP="1080">
					    <DIV style="OVERFLOW: hidden" DESIGNTIMEDRAGDROP="900">
						    <TABLE id="tbClausulas" style="MARGIN-TOP: 0px; MARGIN-BOTTOM: 0px; MARGIN-LEFT: 0px; BORDER-COLLAPSE: collapse"
							    borderColor="#cccccc" cellSpacing="0" cellPadding="0" width="100%" border="1" runat="server">
                            </TABLE>
					    </DIV>
				    </DIV>
                    <asp:Panel ID="pnlClausula" runat="server" Height="520px" Width="650px" ScrollBars="Vertical">&nbsp;<IMG style="HEIGHT: 71px" src="images/logo.png" width=162 /><BR /><BR /><asp:Label id="lblClausula" runat="server" CssClass="Impressao" Width="608px" Height="8px" Text="Label"></asp:Label><BR /><BR /><DIV align=right><IMG src="images/assinatura.png" width=219 /></DIV><BR /><P align=center>COMPANHIA DE SEGUROS ALIANÇA DO BRASIL - CNPJ: 28.196.889/0001-43<BR />Central de Atendimento aos Clientes: 0800 729 7000<BR />Deficientes Auditivos ou de Fala: 0800 729 0088 / Ouvidoria: 0800 880 2930<BR />Correio eletrônico: alianca@aliancadobrasil.com.br - www.aliancadobrasil.com.br<BR />Endereço: Avenida das Nações Unidas, 11.711 – Brooklin – CEP 04578-000 – São Paulo – SP </P></asp:Panel>
                  </TD>
		    </TR>
		    <TR>
                <asp:TextBox ID="txtSeq" runat="server" Visible=false></asp:TextBox></TR>
	    </TABLE>
    <center>
        <asp:HyperLink ID="btnImprimir" runat="server" Width='100px' style='border: #CCCCCC 1px solid; font-family: Verdana;font-size: 10px;color: #FFFFFF;font-weight: bold;background-color: #003399;'>Imprimir</asp:HyperLink>
        <br />&nbsp;
    </center>               
    </form>
</body>
</html>
