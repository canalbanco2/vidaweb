﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ConsultarAssistencia.aspx.vb" Inherits="segw0060.ConsultarAssistencia" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
	<head>
        <title>Consultar Assistências</title>
        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
	    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
	    <meta content="JavaScript" name="vs_defaultClientScript"/>
	    <meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema"/>
	    <link href="CSS/EstiloBase.css" type="text/css" rel="stylesheet"/>
    </head>
    
    <body MS_POSITIONING="FlowLayout">
        <form id="form1" runat="server">
            <asp:label 
                id="lblNavegacao" 
                runat="server" 
                CssClass="CaminhoTela">
                Label
            </asp:label>
            <asp:label 
                id="lblVigencia" 
                runat="server" 
                CssClass="CaminhoTela"><br/>
            </asp:label>
            <br/>
		    <br/>
		    
		    <asp:Panel ID="panelConteudoAssistencia" Visible="True" runat="server">
		
	            <table id="tabelaAssistencias" style="MARGIN-TOP: 10px; MARGIN-BOTTOM: 10px; MARGIN-LEFT: 0px; BORDER-COLLAPSE: collapse; width: 648px;"
		            borderColor="#cccccc" cellSpacing="0" cellPadding="0" border="0">
		            <tr>
			            <td vAlign="bottom" style="width: 790px; height: 34px;">
				            <asp:Label id="Label2" runat="server" CssClass="CaminhoTela" Width="70px">Assistências</asp:Label>
                            <asp:DropDownList id="ddlAssistencia" runat="server" Width="650px" AutoPostBack="true" />
                        </td>
		            </tr>
		            <tr>
			            <td align="left" style="width: 790px; height: 308px;">
				            <div style="OVERFLOW: auto" DESIGNTIMEDRAGDROP="1080">
					            <div style="OVERFLOW: hidden" DESIGNTIMEDRAGDROP="900">
						            <table id="tbAssistencias" style="MARGIN-TOP: 0px; MARGIN-BOTTOM: 0px; MARGIN-LEFT: 0px; BORDER-COLLAPSE: collapse"
							            borderColor="#cccccc" cellSpacing="0" cellPadding="0" width="100%" border="1" runat="server">
                                    </table>
					            </div>
				            </div>
                            <asp:TextBox id="txtAssistencia" runat="server" Width="650px" Height="520px" ReadOnly="True" TextMode="MultiLine" />
                        </td>
		            </tr>
	            </table>
        	    
                <center>
                    <asp:HyperLink ID="btnImprimir" runat="server" Width='100px' style='border: #CCCCCC 1px solid; font-family: Verdana;font-size: 10px;color: #FFFFFF;font-weight: bold;background-color: #003399;'>Imprimir</asp:HyperLink>
                </center>
            
            </asp:Panel>	
            <asp:Panel ID="panelNaoExistemAssistencias" runat="server" Visible="False">
                <center style="padding-top: 20%" >
                    <label class="CaminhoTela" id="lblMensagemNaoExisteAssistencia">
                        Não existem assistências contratadas para este subgrupo
                    </label>
                </center>
            </asp:Panel>
        </form>
    </body>
</html>
