﻿<%@ Page Language="vb" AutoEventWireup="false" Codebehind="HistoricoDPS.aspx.vb"
    Inherits="segw0060.HistoricoDPS" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Histórico de DPS Aceitas ou Recusadas</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../segw0060/CSS/EstiloBase.css" type="text/css" rel="stylesheet" />

    <script src="../segw0060/scripts/overlib421/overlib.js" type="text/javascript"></script>

    <script src="../segw0060/scripts/script.js" type="text/javascript"></script>



<style type="text/css"></style>
 
</head>
<body onload="AlteraLayout()">

    <form id="fmrPesquisa" runat="server">
        <div>
            <asp:Label ID="lblNavegacao" runat="server" CssClass="headerHistDPS">Label</asp:Label> <br/><br/> 
        </div>
        <div id="DivFormPesquisa" onload="AlteraLayout">
            <table style="width: 995px">
                <tbody  class="textHistDPS">
                    <tr>
                        <td> <asp:Label ID="lblSubGrupo" runat="server" CssClass="Caminhotela">Selecione a opção: </asp:Label></td>
                        <td><asp:Label ID="lblInput" runat="server" CssClass="Caminhotela">Informe o Período: </asp:Label></td>
                    </tr>
                    <tr>
                        <td >
                             <asp:DropDownList ID="cboSubGrupo" runat="server"  AutoPostBack="True" Width="230"></asp:DropDownList>
                        </td>
                        <td id="inputArea" >
                            De:<asp:TextBox ID="txtDtInicio"  onkeyup= "mskDate('txtDtInicio');" maxlength="10" runat="server" Width="180" Enabled="true"></asp:TextBox>
                            
                        </td>
                        <td >Até:<asp:TextBox ID="txtDtFim"  onkeyup= "mskDate('txtDtFim');" maxlength="10" runat="server" Width="180" Enabled="true"></asp:TextBox></td>
                         
                         <td >
                            <asp:Button ID="btnPesquisar" runat="server" CssClass="Botao" Width="90px" CausesValidation="False"
                                Text="Pesquisar"></asp:Button>
                            <asp:Button ID="btnLimpar" runat="server" CssClass="Botao" Width="90px" CausesValidation="False"
                                Text="Limpar"></asp:Button>
                            <asp:Button ID="btnGerarExcel"  runat="server" CssClass="Botao" Width="90px" CausesValidation="False"               
                                Text="Gerar Excel"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td id="lblCPF" style="visibility: hidden;" >Informe o CPF</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td> <asp:TextBox ID="txtCPF"  onkeyup= "mskCPF('txtCPF');" maxlength="14" Style="visibility: hidden;" runat="server" Width="180" Enabled="true" Visible="true"></asp:TextBox></td>
                        <td></td>
                    
                    </tr>
                    <tr> </tr>
                </tbody>
            </table>
        </div>
    <div>
        <asp:Label ID="Label1" runat="server" CssClass="textHistDPS">Histórico de Aceites e Recusadas</asp:Label> <br/> <br/>
        	
		  
		  </div>
        <div id="DivGridResultado">
            <asp:GridView ID="gridHistorico" runat="server" CellPadding="4" AutoGenerateColumns="False" PageSize="20" AllowPaging="True" BorderColor="#DDDDDD" AllowSorting="True" EmptyDataText="Não foram encontradas DPS aceitas ou recusadas para os dados pesquisados" >
                    <PagerStyle CssClass="gridStyle" HorizontalAlign="Center"/>
                    <HeaderStyle CssClass="gridHeader" />     
                <Columns>
                    <asp:BoundField DataField="ramo" HeaderText="Ramo" />
                    <asp:BoundField DataField="apolice" HeaderText="Ap&#243;lice" />
                    <asp:BoundField DataField="subgrupo" HeaderText="Subgrupo" />
                    <asp:BoundField DataField="operacao" HeaderText="Opera&#231;&#227;o" />
                    <asp:BoundField DataField="dt_pendencia" HeaderText="Dt. Solicita&#231;&#227;o" />
                    <asp:BoundField DataField="dt_upload" HeaderText="Dt. de Upload"  />
                    <asp:BoundField DataField="nomesegurado" HeaderText="Nome" >
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="cpf" HeaderText="CPF" />
                    <asp:BoundField DataField="dt_nascimento" HeaderText="Dt. Nascimento" />
                    <asp:BoundField DataField="sexo" HeaderText="Sexo" />
                    <asp:BoundField DataField="dt_inicio_vigencia" HeaderText="Inicio de Vig&#234;ncia" Visible="False" />
                    <asp:BoundField DataField="salario" HeaderText="S&#225;lario" />
                    <asp:BoundField DataField="capital_segurado" HeaderText="Capital anterior" />
                    <asp:BoundField DataField="capital_solicitado" HeaderText="Capital solicitado" />
                    <asp:BoundField DataField="situacao_dps" HeaderText="Status da pend&#234;ncia" />
                    <asp:HyperLinkField DataNavigateUrlFields="link_dps" Text="ARQUIVO" HeaderText="Visualizar DPS" Target="_blank" />         
                </Columns>
              
                <EmptyDataTemplate>       
                    Não há registros                    
                </EmptyDataTemplate>
                <RowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                
            </asp:GridView>
            
        </div>
    </form>
</body>

<script type="text/javascript">
    
    function AlteraLayout() {
	        var a = document.getElementById("cboSubGrupo");
	        
	        var element;
	      
	        if (a.options[a.selectedIndex].value == 'CPF'){
	         //alterar o layout para seleção do CPF
	          document.getElementById("txtCPF").style.visibility = "visible";
	          document.getElementById("lblCPF").style.visibility = "visible";      
	        }                  	      	              
	    }    
    
    function mskDate(campo){
  
          var aux = document.getElementById(campo).value;        
          var mydata = '';
          var valor = aux;

          mydata = mydata + valor;
          if (mydata.length == 2){
            mydata = mydata + '/';
            document.getElementById(campo).value = mydata;
          }
          if (mydata.length == 5){
            mydata = mydata + '/';
           document.getElementById(campo).value = mydata;
          }

    }

    function mskCPF(campo){
  
      var aux = document.getElementById(campo).value;  
      var mydata = '';
      var valor = aux;

      mydata = mydata + valor;
          if (mydata.length == 3){
            mydata = mydata + '.';
            document.getElementById(campo).value = mydata;
          }
          if (mydata.length == 7){
            mydata = mydata + '.';
           document.getElementById(campo).value = mydata;
          }
          
          if (mydata.length == 11){
            mydata = mydata + '.';
           document.getElementById(campo).value = mydata;
          }
}



</script><script language="VB" Runat="server"></script>


</html>
