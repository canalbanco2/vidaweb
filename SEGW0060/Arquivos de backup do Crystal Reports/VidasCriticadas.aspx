<%@ Page Language="vb" AutoEventWireup="false" Codebehind="VidasCriticadas.aspx.vb" Inherits="segw0060.VidasCriticadas"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Downloads</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
			<link href="scripts/box/box.css" rel="stylesheet" type="text/css" media="all">
				<script type="text/javascript" src="scripts/box/AJS.js"></script>
				<script type="text/javascript" src="scripts/box/box.js"></script>
				<script type="text/javascript">
			var GB_IMG_DIR = "scripts/box/";
			GreyBox.preloadGreyBoxImages();
				</script>
				<script>
			function confirmaDescarte(){
				var confirma = confirm("O arquivo enviado de protocolo 1850 ser� descartado.\nConfirma a opera��o?");
				if (confirma==true){
					//alert('Vidas criticadas descartadas com sucesso.');
					location.href='?fatura=ok';
					return false;
				}else{
					return false;
				}
			}
				</script>
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<P>
				<asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela" Visible="False">Label</asp:label><BR>
				<asp:label id="lblVigencia" runat="server" CssClass="Caminhotela"><br>COTEMINAS S.A - Per�odo de compet�ncia: 01/12/2006 at� 31/12/2006</asp:label></P>
			<P align="left"><BR>
				<asp:Panel id="pnlAlerta" runat="server" Visible="False" CssClass="fundo-azul-claro" Width="60%"
					Height="70px" HorizontalAlign="Center">
					<BR>
					<asp:Label id="lblListagemEnviada" runat="server" CssClass="Caminhotela"><br>Vidas criticadas descartadas com sucesso.</asp:Label>
				</asp:Panel>
				<asp:Panel id="pnlFormulario" runat="server">
					<asp:Label id="Label1" runat="server" CssClass="Caminhotela">Resultado do processamento do arquivo protocolo 1850</asp:Label>
					<TABLE id="Table1" cellSpacing="0" cellPadding="1" border="1" height="32" bordercolor="#cccccc"
						style="BORDER-COLLAPSE:collapse">
						<TR>
							<TD class="titulo-tabela sem-sublinhado" borderColor="#cccccc" align="center" colSpan="2"
								height="13">Registros aceitos</TD>
							<TD class="titulo-tabela sem-sublinhado" borderColor="#cccccc" align="center" height="13">Registros 
								recusados</TD>
						</TR>
						<TR>
							<TD borderColor="#cccccc" align="center" colSpan="2" height="13">50</TD>
							<TD borderColor="#cccccc" align="center" height="13">2</TD>
						</TR>
					</TABLE>
					<BR>
					<asp:Label id="lblListagemVidas" runat="server" CssClass="Caminhotela">Lista das vidas recusadas</asp:Label>
					<TABLE id="tabelaMovimentacao" style="MARGIN-BOTTOM: 10px; BORDER-COLLAPSE: collapse" borderColor="#cccccc"
						cellSpacing="0" cellPadding="0" width="99%" border="1">
						<TR class="titulo-tabela sem-sublinhado">
							<TD align="center" width="37"></TD>
							<TD>Nome</TD>
							<TD>CPF</TD>
							<TD>Nascimento</TD>
						</TR>
						<TR style="CURSOR: pointer" onclick="GB_show('Vida criticada', '../../VidasCriticadas_Form.aspx', 143, 350)">
							<TD align="center" width="37" height="15">140</TD>
							<TD height="15">Rodrigo Augusto</TD>
							<TD bgColor="#ffff00" height="15">xxx.xxx.xxx-xx</TD>
							<TD height="15">xx/xx/xxxx</TD>
						</TR> <!--<% if Request.QUeryString("excluir")=1 then %>#AACCFF<% end if %>-->
						<TR bgColor="#f5f5f5">
							<TD align="center" width="37">225</TD>
							<TD>Luiz Jos� de Souza</TD>
							<TD>xxx.xxx.xxx-xx</TD>
							<TD bgColor="#ffff00">xx/xx/xxxx</TD>
						</TR>
					</TABLE></P>
			(*) Os campos em cor amarela, apresentaram problemas no processamento do 
			arquivo.<BR>
			&nbsp;<BR>
			<asp:Label id="Label2" runat="server" CssClass="Caminhotela2">Enquanto os registros n�o forem corrigidos, n�o far�o parte da movimenta��o referente � compet�ncia 01/12/2006 at� 31/12/2006 </asp:Label>
			<BR>
			<div style="WIDTH:99%" align="center">
				<asp:Button id="Button1" runat="server" CssClass="Botao" Width="164px" Text="Descartar arquivo enviado"></asp:Button>&nbsp;
				<asp:Image id="Image1" runat="server" ImageUrl="Images/help.gif"></asp:Image></div>
			<P align="left"></asp:Panel><BR>
			<P>
			</P>
		</form>
	</body>
</HTML>
