<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AjudaResumo.aspx.vb" Inherits="segw0060.AjudaResumo"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Ajuda</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<div id="cabecAjuda"><img src="Images/ajuda-1.gif">
			</div>
			<P>&nbsp;</P>
			<TABLE id="Table1" height="61" cellSpacing="0" cellPadding="0" width="94%" align="center"
				border="0">
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Resumo das movimenta��es da rela��o de vidas atual e o comparativo com os dados de resumo da �ltima fatura</span><br><br>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Linhas</span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2" style="height: 12px">
						<span class="ajudaTituloItem">F. Anterior (Fatura Anterior)</span>
						<span class="ajudaItem">- Mostra a quantidade de vidas seguradas da �ltima fatura emitida.  </span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Inclus�o</span>
						<span class="ajudaItem">- Mostra a quantidade de inclus�es na rela��o de vidas no per�odo de compet�ncia atual.</span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Exclus�o</span>
						<span class="ajudaItem">- Mostra a quantidade de exclus�es na rela��o de vidas no per�odo de compet�ncia atual.</span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Subtotal</span>
						<span class="ajudaItem">- Mostra a quantidade de vidas seguradas previstas da fatura atual em movimenta��o, resultante da conta: Fatura Anterior + Inclus�o - Exclus�o.</span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Alt. de Capital (Altera��es de Capital)</span>
						<span class="ajudaItem">- Mostra a quantidade de altera��es de Capital Segurado na rela��o de vidas no per�odo de compet�ncia atual.</span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Acertos</span>
						<span class="ajudaItem">- Mostra a quantidade de opera��es retroativas realizadas � Ex.: Inclus�o de um segurado que deveria constar na fatura do per�odo anterior.</span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem"><br>Obs.: Por regras da seguradora algumas opera��es retroativas poder�o constar no campo Pend�ncias.<br><br></span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">F. Atual (Fatura Atual)</span>
						<span class="ajudaItem">- Mostra a quantidade de vidas seguradas previstas da fatura atual em movimenta��o, resultante da conta: Subtotal + Altera��es de capital + Acertos.</span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Pendente</span>
						<span class="ajudaItem">- Mostra a quantidade de pessoas na rela��o que est�o com pend�ncias de envio da Declara��o Pessoal de Sa�de (DPS), para que a Alian�a do Brasil analise a possibilidade de inclus�o na rela��o de vidas seguradas vigente.</span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">E. Agendada (Exclus�o Agendada)</span>
						<span class="ajudaItem">- Mostra a quantidade de exclus�es dentro do per�odo de vig�ncia da fatura atual, que ser�o realizadas no t�rmino da sua vig�ncia.</span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<br><span class="ajudaTituloItem">Colunas</span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Vidas</span>
						<span class="ajudaItem">- Quantidade de vidas seguradas na rela��o de vidas encaminhadas pela empresa </span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Pr�mio</span>
						<span class="ajudaItem">- Valor estimado do pr�mio a ser pago na fatura atual</span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem"><br>OBS.: O valor do pr�mio pode sofrer altera��es ap�s an�lise da Companhia.<br><br></span>
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<span class="ajudaTituloItem">Detalhamento da Fatura Atual</span>
						<span class="ajudaItem">- Mostra o quadro de resumo da fatura atual, com a informa��o adicional do capital segurado total (coluna �Capital (R$)�) da soma de capitais de todas as vidas seguradas da rela��o encaminhada pela empresa.</span>
					</TD>
				</TR>
			</TABLE>
			<br><br><br>
		</form>
	</body>
</HTML>
