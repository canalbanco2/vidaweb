Partial Class Impressao
    Inherits System.Web.UI.Page
    Public titulo As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Request.QueryString("tipo") = "boleto" Then
            titulo.InnerText = "Impress�o do Boleto"
            Imagem.ImageUrl = "Images/boleto.bmp"
            btnImprimir.Visible = False

        ElseIf Request.QueryString("tipo") = "vidas" Then
            titulo.InnerText = "Impress�o da Rela��o de Vidas"
            Imagem.ImageUrl = "Images/vidas.bmp"
            btnImprimir.Visible = True
        ElseIf Request.QueryString("tipo") = "fatura" Then
            titulo.InnerText = "Impress�o da Rela��o de Faturas"
            Imagem.ImageUrl = "Images/fatura.bmp"
            btnImprimir.Visible = True
        Else
            Imagem.Visible = False
            lblMensagem.Visible = True
            lblMensagem.Text = "(Certificado)"
        End If
    End Sub

End Class
