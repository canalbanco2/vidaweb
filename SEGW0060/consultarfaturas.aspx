<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ConsultarFaturas.aspx.vb" Inherits="segw0060.ConsultarFaturas"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ConsultarFaturas</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
			<SCRIPT language="javascript">
		<!--
		function abre_janela(link,nome,resizable,scrollbars,width,height,top,left)
			{  
			nova=window.open(link,nome,
							"resizable="+resizable+",scrollbars="+scrollbars+",width="+width+",height="+height+",top="+top+",left="+left+",toolbar=0,location=0,directories=0,status=0,menubar=0");
								}
		-->
			</SCRIPT>
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<P><asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela">Label</asp:label><BR>
				<asp:label id="lblVigencia" runat="server" CssClass="Caminhotela"><br>COTEMINAS S.A - Per�odo de compet�ncia: 01/12/2006 at� 31/12/2006</asp:label></P>
			<P><BR>
				<asp:label id="Label6" runat="server" CssClass="Caminhotela">Informe o per�odo da compet�ncia:</asp:label>&nbsp;
				<asp:textbox id="txtDe" runat="server" Width="56px">06/2006</asp:textbox>&nbsp;
				<asp:label id="Label7" runat="server" CssClass="Caminhotela">at�</asp:label>&nbsp;
				<asp:textbox id="txtAte" runat="server" Width="56px">06/2007</asp:textbox>&nbsp;
				<asp:image id="Image2" runat="server" ImageUrl="Images/help.gif"></asp:image>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<asp:button id="btnListar" runat="server" CssClass="Botao" Width="80px" Text="Pesquisar"></asp:button><BR>
			</P>
			<P align="center"><asp:panel id="pnlFaturas" runat="server" HorizontalAlign="Justify" Visible="False">
					<asp:Label id="lblTituloFatura" CssClass="Caminhotela" Runat="server">Selecione a fatura a consultar</asp:Label>
					<asp:Panel id="pnlTodasFaturas" runat="server" Width="100%" Height="46px">
						<TABLE id="Table1" style="BORDER-COLLAPSE: collapse; TEXT-ALIGN: center" borderColor="#cccccc"
							cellSpacing="0" cellPadding="2" width="700" border="1">
							<TR class="titulo-tabela sem-sublinhado">
								<TD>N�mero&nbsp;Fatura</TD>
								<TD width="171">Compet�ncia da Fatura</TD>
								<TD>Vencimento</TD>
								<TD align="center" width="148">Valor do Pr�mio</TD>
								<TD align="center" width="148">Data do Pagamento</TD>								
								<TD align="center">Situa��o</TD>
							</TR> <!--<% if Request.QUeryString("visualizar")<>"" then %>#AACCFF<% end if %>-->
							<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onclick="location.href='?visualizar=1&amp;tipo=todos'"
								onmouseout="this.style.background=''">
								<TD height="1">12056</TD>
								<TD width="171" height="1">01/12/06 a 31/12/06</TD>
								<TD height="1">25/12/06</TD>
								<TD>1.000,00</TD>
								<TD align="center" width="148" height="1">-</TD>								
								<TD align="center" height="1">Emitida</TD>
							</TR>
							<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#f5f5f5'"
								bgColor="#f5f5f5">
								<TD>12345</TD>
								<TD width="171">01/11/06 a 30/11/06</TD>
								<TD>25/11/06</TD>
								<TD>1.100,00</TD>
								<TD align="center" width="148">24/11/06</TD>								
								<TD align="center">Emitida</TD>
							</TR>
							<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#ffffff'">
								<TD>15285</TD>
								<TD width="171">01/10/06 a 31/10/06</TD>
								<TD>25/10/06</TD>
								<TD>1.200,00</TD>
								<TD align="center" width="148">24/10/06</TD>								
								<TD align="center">Emitida</TD>
							</TR>
							<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#f5f5f5'"
								bgColor="#f5f5f5">
								<TD>17456</TD>
								<TD width="171">01/09/06 a 30/09/06</TD>
								<TD>25/09/06</TD>
								<TD>3.000,00</TD>
								<TD align="center" width="148">24/09/06</TD>								
								<TD align="center">Cancelada</TD>
							</TR>
							<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#ffffff'">
								<TD>5853</TD>
								<TD width="171">01/08/06 a 31/08/06</TD>
								<TD>25/08/06</TD>
								<TD>2.000,00</TD>
								<TD align="center" width="148">24/08/06</TD>								
								<TD align="center">Emitida</TD>
							</TR>
							<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#f5f5f5'"
								bgColor="#f5f5f5">
								<TD>11458</TD>
								<TD width="171">01/07/06 a 31/07/06</TD>
								<TD>25/07/06</TD>
								<TD>1.500,00</TD>
								<TD align="center" width="148">24/07/06</TD>								
								<TD align="center">Pendente de Pagamento</TD>
							</TR>
							<TR class="fonte-branca" bgColor="#003399">
								<TD align="center" colSpan="6">
									<TABLE id="Table3" style="COLOR: #ffffff" cellSpacing="1" cellPadding="1" width="100%"
										border="0">
										<TR>
											<TD align="center" width="837">P�gina -&nbsp;&lt;&lt;&nbsp;1 &gt;&gt;</TD>
											<TD noWrap align="right">Total registros: 100</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</asp:Panel>
				</asp:panel></P>
			<asp:Panel ID="pnlFiltro" Visible="False" Runat="server">
				<TABLE id="Table4" height="54" cellSpacing="1" cellPadding="1" width="600" border="0">
					<TR>
						<TD align="right" width="5%">
							<asp:Label id="Label13" runat="server" CssClass="Caminhotela">Opera��o:</asp:Label></TD>
						<TD width="5%">
							<asp:DropDownList id="DropDownList1" runat="server">
								<asp:ListItem Value="Todos">Todos</asp:ListItem>
								<asp:ListItem Value="Inclus&#245;es">Inclus&#245;es</asp:ListItem>
								<asp:ListItem Value="Exclus&#245;es">Exclus&#245;es</asp:ListItem>
							</asp:DropDownList></TD>
						<TD align="right" width="5%">
							<asp:Label id="Label11" runat="server" CssClass="Caminhotela">Nome:</asp:Label></TD>
						<TD width="5%">
							<asp:TextBox id="TextBox1" runat="server" Width="173px"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD align="right" width="5%">
							<asp:Label id="Label12" runat="server" CssClass="Caminhotela">CPF:</asp:Label></TD>
						<TD width="5%">
							<asp:TextBox id="TextBox2" runat="server"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD align="center" width="5%" colSpan="4">
							<asp:Button id="Button1" runat="server" CssClass="Botao" Width="75px" Text="Pesquisar"></asp:Button></TD>
					</TR>
				</TABLE>
			</asp:Panel>
			<asp:panel id="pnlDetalhesFatura" runat="server" Visible="False" Width="99%">
				<TABLE id="Table5" cellSpacing="1" cellPadding="1" width="100%" border="0">
					<TR>
						<TD height="14">
							<asp:Label id="Label14" runat="server" CssClass="Caminhotela">Detalhamento da fatura 12056  -  01/12/06 a 31/12/06</asp:Label></TD>
						<TD noWrap align="right" height="14">
							<asp:DropDownList id="DropDownList2" runat="server">
								<asp:ListItem Value="Todos">Todos</asp:ListItem>
								<asp:ListItem Value="Inclus&#245;es">Inclus&#245;es</asp:ListItem>
								<asp:ListItem Value="Altera&#231;&#245;es">Altera&#231;&#245;es</asp:ListItem>
								<asp:ListItem Value="Exclus&#245;es">Exclus&#245;es</asp:ListItem>
								<asp:ListItem Value="Acertos">Acertos</asp:ListItem>
								<asp:ListItem Value="Cart&#227;o Pendente">Cart&#227;o Pendente</asp:ListItem>
							</asp:DropDownList>
							<asp:Button id="Button2" runat="server" CssClass="Botao" Width="73px" Text="Pesquisar"></asp:Button></TD>
					</TR>
				</TABLE>
				<TABLE id="tabelaMovimentacao" style="MARGIN-BOTTOM: 10px; BORDER-COLLAPSE: collapse" borderColor="#cccccc"
					cellSpacing="0" cellPadding="0" width="100%" border="1">
					<TR class="titulo-tabela sem-sublinhado">
						<TD width="70">Opera��o</TD>
						<TD width="266">Segurado</TD>
						<TD width="66">Tipo</TD>
						<TD width="134">CPF</TD>
						<TD width="55">Nascimento</TD>
						<TD width="28">Sexo</TD>
						<TD noWrap width="85">Sal�rio (R$)</TD>
						<TD noWrap width="83">Capital (R$)</TD>
					</TR>
					<TR class="paddingzero">
						<TD colSpan="8" style="padding:0px">
							<DIV style="OVERFLOW: auto; HEIGHT: 88px" DESIGNTIMEDRAGDROP="807">
								<TABLE class="paddingzero" id="Table2" style="BORDER-COLLAPSE: collapse" borderColor="#cccccc"
									cellSpacing="0" cellPadding="0" width="100%" border="1">
									<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#ffffff'">
										<TD width="70" height="15"></TD>
										<TD>Rodrigo Augusto
										</TD>
										<TD width="52" height="15">Titular</TD>
										<TD width="90" height="15">xxx.xxx.xxx-xx</TD>
										<TD width="82" height="15">xx/xx/xxxx</TD>
										<TD width="35" height="15">masc.</TD>
										<TD align="right" width="89" height="15">1.000,00</TD>
										<TD align="right" width="69">25.000,00</TD>
									</TR> <!--<% if Request.QUeryString("excluir")=1 then %>#AACCFF<% end if %>-->
									<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background=''">
										<TD height="15">
											<asp:Label id="lblAcao" runat="server" ForeColor="DarkRed"></asp:Label></TD>
										<TD height="15">
											<asp:ImageButton id="imgPlus" runat="server" ImageUrl="Images/imgMenos.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;Jos� 
											Luis de Souza</TD>
										<TD height="15">Titular</TD>
										<TD height="15">xxx.xxx.xxx-xx</TD>
										<TD height="15">xx/xx/xxxx</TD>
										<TD height="15">masc.</TD>
										<TD align="right" height="15">1.000,00</TD>
										<TD align="right" height="15">9.000,00</TD>
									</TR>
									<asp:Panel id="pnlDependente" Visible="True" Runat="server">
										<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #f5f5f5"
											onmouseout="this.style.background='#f5f5f5'">
											<TD>&nbsp;</TD>
											<TD><SPAN class="grid-sub">Ricardo Vaccani</SPAN></TD>
											<TD>Filho</TD>
											<TD>xxx.xxx.xxx-xx</TD>
											<TD>xx/xx/xxxx</TD>
											<TD>masc.</TD>
											<TD align="right">1.000,00</TD>
											<TD align="right">12.000,00</TD>
										</TR>
										<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #f5f5f5"
											onclick="vidaExcluir(3)" onmouseout="this.style.background='#f5f5f5'">
											<TD>
												<asp:Label id="Label15" runat="server" ForeColor="DarkRed"></asp:Label></TD>
											<TD><SPAN class="grid-sub">Gustavo Modena</SPAN></TD>
											<TD>Filho</TD>
											<TD>xxx.xxx.xxx-xx</TD>
											<TD>xx/xx/xxxx</TD>
											<TD>masc.</TD>
											<TD align="right">1.000,00</TD>
											<TD align="right">9.000,00</TD>
										</TR>
										<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #f5f5f5"
											onmouseout="this.style.background='#f5f5f5'">
											<TD>&nbsp;</TD>
											<TD><SPAN class="grid-sub">Maria da Silva</SPAN></TD>
											<TD>C�njuge</TD>
											<TD>xxx.xxx.xxx-xx</TD>
											<TD>xx/xx/xxxx</TD>
											<TD>fem.</TD>
											<TD align="right"></TD>
											<TD align="right">17.000,00</TD>
										</TR>
										<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer; BACKGROUND-COLOR: #f5f5f5"
											onmouseout="this.style.background='#f5f5f5'">
											<TD>&nbsp;</TD>
											<TD><SPAN class="grid-sub">Marcio de Almeida</SPAN></TD>
											<TD>Filho</TD>
											<TD>xxx.xxx.xxx-xx</TD>
											<TD>xx/xx/xxxx</TD>
											<TD>masc.</TD>
											<TD align="right">1.000,00</TD>
											<TD align="right">21.000,00</TD>
										</TR>
									</asp:Panel><% for i as integer = 0  to 100 %>
									<TR onmouseover="this.style.background='#E8F1FF'" style="CURSOR: pointer" onmouseout="this.style.background='#ffffff'">
										<TD height="15"></TD>
										<TD height="15">Rodrigo Augusto</TD>
										<TD height="15">Titular</TD>
										<TD height="15">xxx.xxx.xxx-xx</TD>
										<TD height="15">xx/xx/xxxx</TD>
										<TD height="15">masc.</TD>
										<TD align="right" height="15">1.000,00</TD>
										<TD align="right">25.000,00</TD>
									</TR>
									<% next %>
								</TABLE>
							</DIV>
						</TD>
					</TR>
					<TR style="COLOR: #fff; TEXT-ALIGN: center" bgColor="#003399">
						<TD colSpan="9">
							<TABLE id="Table1" style="COLOR: #ffffff" cellSpacing="1" cellPadding="1" width="100%"
								border="0">
								<TR>
									<TD align="center" width="837">P�gina -&nbsp;&lt;&lt;&nbsp;1 &gt;&gt;</TD>
									<TD noWrap align="right">Total registros: 100</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
				<P style="MARGIN-TOP: 0px; MARGIN-BOTTOM: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px"
					align="center"><BR>
					<asp:CheckBoxList id="chkLista" runat="server" Width="430px" RepeatDirection="Horizontal" AutoPostBack="True">
						<asp:ListItem Value="fatura" Selected="True">Imprimir Fatura</asp:ListItem>
						<asp:ListItem Value="boletos" Selected="True">Imprimir Boletos</asp:ListItem>
						<asp:ListItem Value="relacao">Imprimir Rela&#231;&#227;o de Vidas</asp:ListItem>
					</asp:CheckBoxList><BR>
					<BR>
					<asp:Button id="btnImprimir" runat="server" CssClass="Botao" Width="73px" Text="Imprimir"></asp:Button>
					<asp:Button id="btnFechaPagina" runat="server" CssClass="Botao" Width="56px" Text="Fechar" Visible="False"></asp:Button><BR>
					&nbsp;<BR>
					&nbsp;</P>
			</asp:panel></form>
	</body>
</HTML>
