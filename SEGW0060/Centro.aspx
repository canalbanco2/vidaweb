<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Centro.aspx.vb" Inherits="segw0060.Centro"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Centro</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
			<script src="scripts/overlib421/overlib.js" type="text/javascript"></script>
			<SCRIPT language="javascript">
			top.escondeaguarde();

			function exibePerfil(ind_acesso) {				
				var acesso;
								
				if (ind_acesso == '1')
					acesso = 'Adm. Alian&ccedil;a do Brasil';
					
				if (ind_acesso == '2')
					acesso = 'Adm. de Ap&oacute;lice';
				
				if (ind_acesso == '3')
					acesso = 'Adm. de Subgrupo';
				
				if (ind_acesso == '4')
					acesso = 'Operador';
					
				if (ind_acesso == '5')
					acesso = 'Consulta';	
					
				if (ind_acesso == '6') // Projeto 539202 - Jo�o Ribeiro - 28/04/2009
					acesso = 'Central de Atendimento';	

				if (ind_acesso == '7')
					acesso = 'Adm. Master';					
				
				if (ind_acesso != ''){
				    //alert(acesso);	
				    top.document.getElementById('lblTopAcessoUsuario').innerHTML = acesso;
				}
			}		
		<!--			
		function abre_janela(link,nome,resizable,scrollbars,width,height,top,left)
			{  
			nova=window.open(link,nome,
							"resizable="+resizable+",scrollbars="+scrollbars+",width="+width+",height="+height+",top="+top+",left="+left+",toolbar=0,location=0,directories=0,status=0,menubar=0");
								}
		function telaInicial() {
			//document.Form1.voltar.value=1;
			//document.Form1.submit();
            document.getElementById("voltar").value = 1;
            document.getElementById("Form1").submit();
			return true;
		}
		function openPopMail(msg,titulo,vigIni,vigFim) {
			//top.window.scroll(0,0);
			//top.document.body.scrollTop = 0;
			//?msg=' + msg + '&tit='+titulo
			
			document.getElementById("msg").value = msg;
			document.getElementById("tit").value = titulo;
			//| johnny.barros(Confitec) Demanda:16035497 - 25/02/2013 [inicio] |
            document.getElementById("vigIni").value = vigIni;
            document.getElementById("vigFim").value = vigFim;
            //| johnny.barros(Confitec) Demanda:16035497 - 25/02/2013 [fim] |
			
			abre_janela('popMsg.aspx','janela_msg','no','yes','400','500','','');	
						
			//document.form2.action = "popMsg.aspx";
			//document.form2.target = "janela_msg";
			//document.form2.submit();	
            document.getElementById("form2").action = "popMsg.aspx";
			document.getElementById("form2").target = "janela_msg";
			document.getElementById("form2").submit();
		}
		-->
		
	
            </SCRIPT>
	</HEAD>
	<!--<body MS_POSITIONING="FlowLayout"> -->
	<body >
		<div id="overDiv" style="Z-INDEX: 1000; VISIBILITY: hidden; POSITION: absolute"></div>
		<form name='form2' method="post">
			<input type="hidden" name="msg" id="msg"> 
			<input type="hidden" name="tit" id="tit">
			<!--| johnny.barros(Confitec) Demanda:16035497 - 25/02/2013 [inicio] |-->
            <input type="hidden" name="vigIni" id="vigIni">
            <input type="hidden" name="vigFim" id="vigFim">
            <!--| johnny.barros(Confitec) Demanda:16035497 - 25/02/2013 [fim] |-->
		</form>
		<form id="Form1" method="post" runat="server">
			<input type="hidden" name="voltar">
			<P>
				<asp:Label id="lblNavega" runat="server" CssClass="Caminhotela" Width="100%"></asp:Label><br>
				<asp:label id="lblVigencia" runat="server" CssClass="Caminhotela"></asp:label></P>
			<DIV style="MARGIN: 20px">
				<asp:panel id="pnlApresentacao" runat="server" Visible="false" HorizontalAlign="Center">
					<DIV class="centro-mensagem" style="MARGIN-BOTTOM: 0px; PADDING-BOTTOM: 0px">
						<asp:Label id="lblSaudacao" runat="server" CssClass="Caminhotela" Visible="False" ForeColor="Black"></asp:Label>
						<asp:Label id="lblUsuario" runat="server" CssClass="Caminhotela" Visible="False" ForeColor="Black"></asp:Label><BR>
						<asp:Label id="lblDados" runat="server" CssClass="Caminhotela">Dados b�sicos da ap�lice</asp:Label></DIV>
					<TABLE style="BORDER-RIGHT: #ccc 1px solid; BORDER-TOP: #ccc 1px solid; BORDER-LEFT: #ccc 1px solid; BORDER-BOTTOM: #ccc 1px solid"
						cellSpacing="3" cellPadding="1" width="95%" align="center" border="0">
						<TR class="titulo-tabela-amarelo sem-sublinhado destaque-titulo">
							<TD width="10%">Ramo</TD>
							<TD width="10%">Ap�lice</TD>
							<TD width="18%">CNPJ</TD>
							<TD width="42%" colSpan="2">Empresa</TD>
							<TD width="10%">Situa��o</TD>
						</TR>
						<TR class="destaque-titulo">
							<TD width="65"><%= Session("ramo")%></TD>
							<TD width="65"><%= Session("apolice")%></TD>
							<TD width="140">
								<asp:Label id="lblCNPJ" runat="server"></asp:Label></TD>
							<TD colSpan="2">
								<asp:Label id="lblEmpresa" runat="server"></asp:Label></TD>
							<TD width="10%">
								<asp:Label id="lblStatus" runat="server"></asp:Label></TD>
						</TR>
					</TABLE>
					<BR>
					<TABLE style="BORDER-RIGHT: #ccc 1px solid; BORDER-TOP: #ccc 1px solid; BORDER-LEFT: #ccc 1px solid; BORDER-BOTTOM: 0px"
						cellSpacing="3" cellPadding="1" width="95%" align="center" border="0">
						<TR class="titulo-tabela sem-sublinhado titulo-cinza">
							<TD width="70%" colSpan="3">Endere�o</TD>
							<TD width="30%">Complemento</TD>
						</TR>
						<TR>
							<TD colSpan="3">
								<asp:Label id="lblEndereco" runat="server" Width="288px"></asp:Label></TD>
							<TD>
								<asp:Label id="lblComplemento" runat="server"></asp:Label></TD>
						</TR>
					</TABLE>
					<TABLE style="BORDER-RIGHT: #ccc 1px solid; BORDER-TOP: 0px; BORDER-LEFT: #ccc 1px solid; BORDER-BOTTOM: #ccc 1px solid"
						cellSpacing="3" cellPadding="1" width="95%" align="center" border="0">
						<TR class="titulo-tabela sem-sublinhado titulo-cinza">
							<TD colSpan="2">Bairro</TD>
							<TD width="85">CEP</TD>
							<TD width="200">Cidade</TD>
							<TD width="30">UF</TD>
						</TR>
						<TR>
							<TD colSpan="2">
								<asp:Label id="lblBairro" runat="server"></asp:Label></TD>
							<TD>
								<asp:Label id="lblCEP" runat="server"></asp:Label></TD>
							<TD>
								<asp:Label id="lblCidade" runat="server"></asp:Label></TD>
							<TD>
								<asp:Label id="lblUF" runat="server"></asp:Label></TD>
						</TR>
						<TR class="titulo-tabela sem-sublinhado titulo-cinza">
							<TD width="35" colSpan="1">DDD</TD>
							<TD colSpan="3">Telefone</TD>
							<TD colSpan="1">
								<asp:Label id="lblTextEmail" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD colSpan="1">
								<asp:Label id="lblDDD" runat="server"></asp:Label></TD>
							<TD colSpan="3">
								<asp:Label id="lblTelefone" runat="server"></asp:Label></TD>
							<TD colSpan="1">
								<asp:Label id="lblEmail" runat="server"></asp:Label></TD>
						</TR>
					</TABLE>
					<DIV class="centro-mensagem"><FONT color="#333333">Para atualizar o endere�o ou 
							telefone desta ap�lice, ligue para nossa <b>Central de Atendimento aos 
								Clientes (0800 729 7000 - Op��o 3 - 1 - 1)</b>. Se preferir, procure sua ag�ncia de 
							relacionamento BB ou seu consultor de seguros. </FONT>
					</DIV>
					<DIV class="centro-mensagem">
						<asp:Label id="lblSubgrupos" runat="server" CssClass="Caminhotela" Visible="True">Selecione um subgrupo para iniciar suas consultas e movimenta��es:</asp:Label>
						<UL id="ulSubGrupos" runat="server"> <!--LI>
								<IMG src="Images/quadro_recados.jpg" align="absMiddle"> <A href="Centro.aspx?subgrupo=Filial Bahia">
									Filial Bahia</A>
							<LI>
								<IMG src="Images/quadro_recados.jpg" align="absMiddle"> <A href="Centro.aspx?subgrupo=Filial Rio de Janeiro">
									Filial Rio de Janeiro</A>
							<LI>
								<IMG src="Images/quadro_recados.jpg" align="absMiddle"> <A href="Centro.aspx?subgrupo=Matriz S�o Paulo - Diretores">
									Matriz S�o Paulo - Diretores</A>
							<LI>
								<IMG src="Images/quadro_recados.jpg" align="absMiddle"> <A href="Centro.aspx?subgrupo=Matriz S�o Paulo - Funcion�rios">
									Matriz S�o Paulo - Funcion�rios</A>
							</LI--></UL>
					</DIV>
				</asp:panel>
			</DIV>
			<asp:panel id="pnlLogAcesso" runat="server" Visible="False" HorizontalAlign="Left" Width="100%"
				BorderStyle="Solid" BorderWidth="0px">
				<!--FIELDSET id="Fieldset1" style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 0px"-->
				<FIELDSET id="Fieldset2" style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 0px">
					<LEGEND style="FONT-WEIGHT: bold; FONT-SIZE: 13px; COLOR: blue">&nbsp;�ltimos 
Acessos&nbsp;</LEGEND>
					<asp:Table id="tbUltimosAcessos" runat="server" cellSpacing="2" cellPadding="1" width="100%" border="0">
						<asp:TableRow class="titulo-tabela sem-sublinhado">
							<asp:TableCell width="120">
								<B>Data / Hora</B></asp:TableCell>
							<asp:TableCell width="105">Ramo - Ap�lice</asp:TableCell>
							<asp:TableCell width="105">Estipulante</asp:TableCell>
							<asp:TableCell>
								<B>Subgrupo</B></asp:TableCell>
							<asp:TableCell width="15%">
								<B>Usu�rio</B></asp:TableCell>
						</asp:TableRow>
					</asp:Table>
				</FIELDSET>
			</asp:panel><br>
			<asp:panel id="pnInfoComplementar" runat="server" HorizontalAlign="Left" Width="100%" BorderStyle="Solid"
				BorderWidth="0px">
				<FIELDSET id="Fieldset1" style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 0px">
					<LEGEND style="FONT-WEIGHT: bold; FONT-SIZE: 13px; COLOR: blue">&nbsp;Informa��es 
Importantes&nbsp;</LEGEND>
					<asp:Table id="tbInfo" style="FONT-SIZE: 13px" runat="server" cellSpacing="2" cellPadding="1"
						width="100%" border="0">
						<asp:TableRow Height="5px">
							<asp:TableCell width="120" ColumnSpan="2" Height="5px"></asp:TableCell>
						</asp:TableRow>
						<asp:TableRow>
							<asp:TableCell width="50%" style="padding-left:20px;">
								<b>Recomenda��es de configura��es M�NIMAS:</b><br>
								<span style="width:380px;"><b>Microcomputador:</b> Pentium III, 800 Mhz, 256 MB RAM</span><br>
								<span style="width:380px;"><b>Conex�o internet:</b> banda larga, 256 Kbps</span><br>
								<span style="width:380px;"><b>Navegador de internet:</b> Microsoft Internet Explorer 6</span><br>
								<span style="width:380px;"><b>Resolu��o da tela:</b> 1024 por 768 pixels</span><br>&nbsp;
							</asp:TableCell>
							<asp:TableCell width="25%" VerticalAlign="Bottom" HorizontalAlign="Center">
								&nbsp;
							</asp:TableCell>
						</asp:TableRow>
						<asp:TableRow Height="5px" Visible="False">
							<asp:TableCell width="25%" VerticalAlign="Bottom" HorizontalAlign="right">
								<a href="" class="info" style="padding-right:80px;">Acesso e Seguran�a</a>
							</asp:TableCell>
							<asp:TableCell width="25%" VerticalAlign="Bottom" HorizontalAlign="Center">
								<a href="" class="info">Pol�tica de Privacidade</a>
							</asp:TableCell>
						</asp:TableRow>
					</asp:Table>
				</FIELDSET>
			</asp:panel>
			<DIV style="MARGIN: 0px"><asp:panel id="pnlCheckList" runat="server" Visible="False" HorizontalAlign="Left" Width="100%"
					BorderStyle="Solid" BorderWidth="0px"><!--<FIELDSET id="fsChecklist" style="FONT-SIZE: 11px; FLOAT: left; WIDTH: 36%; HEIGHT: 160px">
						<LEGEND style="FONT-WEIGHT: bold; FONT-SIZE: 13px; MARGIN-BOTTOM: 6px; COLOR: blue">&nbsp;Checklist&nbsp;</LEGEND>
						<DIV class="divCentroPrincipal"><IMG src="Images/concluido.gif">&nbsp;Abertura para 
							movimenta��o - 07/01/07</DIV>
						<DIV class="divCentroPrincipal">
							<P><IMG src="Images/traco.gif">&nbsp;Movimenta��o&nbsp;<A style="font-decoration: underline" href="MovimentacaoMassa.aspx"><U>por 
										arquivo</U></A><BR>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="MovimentacaoUmaUm.aspx"><U>on-line</U></A>
								- 06/01/07</P>
						</DIV>
						<DIV class="divCentroPrincipal">
							<P><IMG src="Images/pendente.gif" align="middle">&nbsp;<A onclick="abre_janela('popCartaoProposta.aspx','janela_msg','no','yes','350','200','','')"
									href="javascript:;">Emiss�o</A> da Fatura</P>
						</DIV>
						<DIV style="PADDING-LEFT: 10px"><BR>
							<B>Legenda:&nbsp;&nbsp;&nbsp;</B>&nbsp;<BR>
							<UL style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 2px; PADDING-TOP: 0px">
								<IMG src="images/pendente.gif">&nbsp;Pendente<IMG src="Images/chkOk.png">Conclu�do
								<IMG src="Images/traco.gif" align="middle">&nbsp;Em execu��o</UL>
						</DIV>
					</FIELDSET>-->
					<FIELDSET id="fsCheckProximopasso" style="FONT-SIZE: 11px; WIDTH: 100%">
						<LEGEND style="FONT-WEIGHT: bold; FONT-SIZE: 13px; COLOR: blue">&nbsp;Pr�ximos 
passos&nbsp;<BR></LEGEND>
						<DIV class="Caminhotela2" align="center"><BR>
							Movimenta��o
							<asp:HyperLink id="linkPorArquivo" onclick="top.exibeaguarde();" runat="server">
								<U>por arquivo</U>&nbsp;</asp:HyperLink>ou
							<asp:HyperLink id="linkMovOnline" onclick="top.exibeaguarde();" runat="server">
								<U>on-line</U></asp:HyperLink>&nbsp;<IMG onmouseover="return overlib('Escolher movimenta��o desejada.');" onmouseout="return nd();"
								alt="" src="../segw0060/Images/help.gif"><BR>
							&nbsp;
						</DIV>
					</FIELDSET>
					<FIELDSET id="fsCheckMensagem" style="FONT-SIZE: 11px; WIDTH: 100%">
						<LEGEND style="FONT-WEIGHT: bold; FONT-SIZE: 13px; PADDING-BOTTOM: 15px; COLOR: blue">&nbsp;Hist�rico 
da Movimenta��o Atual&nbsp; </LEGEND>
						<UL class="Caminhotela2" style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; MARGIN: 5px; PADDING-TOP: 0px">
							<asp:Label id="contentEmail" runat="server"></asp:Label></UL>
						<BR>
					</FIELDSET>
					<DIV id="divPrazo" style="FONT-SIZE: 11px; WIDTH: 100%" align="center">
						<asp:Label id="lblPrazo" Runat="server">Faltam 17 dias para atingir o prazo limite para informar as vidas neste sistema.</asp:Label></DIV>
				</asp:panel></DIV>
			<asp:panel id="pnlBtnVoltar" runat="server" Visible="true" HorizontalAlign="Center">
				<asp:Button id="btnVoltar" runat="server" Width="120px" CssClass="Botao" Text="�ltimos Acessos"></asp:Button>
				<DIV class="centro-mensagem" style="MARGIN: 20px"><FONT color="#333333">Para mais 
						informa��es, consulte as <A href="#">Respostas �s D�vidas mais Freq�entes</A>.&nbsp;<BR>
						</FONT></DIV>
			</asp:panel>
		</form>
		
		<asp:Label ID="lblAviso" Visible="False" Runat="server" CssClass="Caminhotela">O n�o encerramento da movimenta��o implicar� na repeti��o de acordo com o movimento da fatura anterior.</asp:Label>
		<script>
		try {
			var campo;
			campo = document.getElementById('fsCheckMensagem');
			
			var prazo;
			prazo = document.getElementById('divPrazo');
			//alert(campo.scrollHeight);
			prazo.style.top = campo.scrollHeight + 150;		
		}
		catch(ex) {
		
		}		
		try {
			exibePerfil(document.getElementById('ind_acesso').value)
		} catch (ex) {
		
		}
		top.escondeaguarde();
		</script>
		<DIV></DIV>
		<input type="hidden" id="Testa">
	</body>
</HTML>
