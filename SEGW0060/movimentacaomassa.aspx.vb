Public Class MovimentacaoMassa
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblVigencia As System.Web.UI.WebControls.Label
    Protected WithEvents lblNavegacao As System.Web.UI.WebControls.Label
    Protected WithEvents pnlTipodeMovimentacao As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlModelodoArquivo As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlMensagemEnviada As System.Web.UI.WebControls.Panel
    Protected WithEvents lblTipoMovimentacao As System.Web.UI.WebControls.Label
    Protected WithEvents rdSobreposicao As System.Web.UI.WebControls.RadioButton
    Protected WithEvents pnlProcurar As System.Web.UI.WebControls.Panel
    Protected WithEvents fileProcurar As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents rdIncremental As System.Web.UI.WebControls.RadioButton
    Protected WithEvents btnImportar As System.Web.UI.WebControls.Button
    Protected WithEvents lblModelo As System.Web.UI.WebControls.Label
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents btnFechaPagina As System.Web.UI.WebControls.Button
    Protected WithEvents btnConfirmar As System.Web.UI.WebControls.Button
    Protected WithEvents lblListagemEnviada As System.Web.UI.WebControls.Label
    Protected WithEvents pnlMovimentacao As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlDependente As System.Web.UI.WebControls.Panel
    Protected WithEvents imgPlus As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents escolhaTpMovimentacao As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Subgrupo: " & Session("subGrupo") & " -> " & "Movimenta��o por arquivo"
        lblTipoMovimentacao.Attributes.Add("style", "cursor:default")
        pnlProcurar.Visible = True
        lblListagemEnviada.Visible = False
        btnImportar.Attributes.Add("onclick", "return confirmaModelo()")
    End Sub

    Private Sub btnEnviarListagem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pnlModelodoArquivo.Visible = False
        pnlProcurar.Visible = False
        pnlTipodeMovimentacao.Visible = False
        pnlMensagemEnviada.Visible = True
        rdIncremental.Checked = False
        rdSobreposicao.Checked = False
    End Sub

    Private Sub rdIncremental_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pnlProcurar.Visible = True
        pnlModelodoArquivo.Visible = False
        rdSobreposicao.Checked = False
    End Sub

    Private Sub rdSobreposicao_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pnlProcurar.Visible = True
        pnlModelodoArquivo.Visible = False
        rdIncremental.Checked = False
    End Sub

    Private Sub btnImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportar.Click
        Dim nomeArquivo As String = fileProcurar.PostedFile.FileName
        Dim nomeArquivo_split As Array = nomeArquivo.Split("\")
        pnlModelodoArquivo.Visible = True
        pnlProcurar.Visible = False
        pnlMovimentacao.Visible = False
        pnlTipodeMovimentacao.Visible = False
        pnlMensagemEnviada.Visible = True
        'lblListagemEnviada.Visible = True
    End Sub

    Private Sub btnFechaPagina_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("Centro.aspx")
    End Sub

    Private Sub btnConfirmar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmar.Click
        pnlMovimentacao.Visible = True
        pnlModelodoArquivo.Visible = False
        pnlProcurar.Visible = False
        pnlTipodeMovimentacao.Visible = False
        pnlMensagemEnviada.Visible = True
        lblListagemEnviada.Visible = True
    End Sub
End Class
