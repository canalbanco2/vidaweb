<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Default.aspx.vb" Inherits="segw0087.WebForm1"  %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WebForm1</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK title="style" href="/_styles/estilo.css" type="text/css" rel="STYLESHEET">
		<script>
			function Btn_Associar(orig, dest) {
				var origem = document.getElementById(orig);
				var destino = document.getElementById(dest);
				var passou = 1;
				while (passou == 1) {
					passou = 0;
					for(i=0;i<origem.options.length;i++) {
					
						if(origem.options[i].selected == true) {
							passou = 1
							var valor = origem.options[i].value;
							var text = origem.options[i].text;
						
							destino.options[destino.length] = new Option( text, valor, true, true);
							
							origem.options[i] = null;	
						}	
					}
				}
				
			}
			
			function Btn_Retirar(orig, dest) {
				var origem = document.getElementById(orig);
				var destino = document.getElementById(dest);
				var passou = 1;
				while (passou == 1) {
					passou = 0;
					for(i=0;i<origem.options.length;i++) {
				
						if(origem.options[i].selected == true) {
							passou = 1
							var valor = origem.options[i].value;
							var text = origem.options[i].text;
						
							if(text.indexOf("(") != - 1) 
								text = text.substring(0, text.indexOf("(") );

							destino.options[destino.length] = new Option( text, valor, true, true);
						
							origem.options[i] = null;	
						}	
					}
				}
			}
			
			function verifBtnPesquisar()
			{
				var campo = document.getElementById('txtUsuario');
				var ramo = document.getElementById('ddlRamo');
				
				if(ramo.selectedIndex < 1)
				{
					alert('� necess�rio escolher o ramo para realizar a pesquisa.');
					return false;
				}
				if (campo.value.length < 3)
				{
					alert('� necess�rio especificar no m�nimo 3 caracteres para pesquisar pelo nome.');
					return false;
				}
				return true;
			}
			
			function gravar()
			{
				var usuarios = document.getElementById('ddlUserApolice');
				var ramo = document.getElementById('ddlRamo');
				
				if(ramo.selectedIndex < 1)
				{
					alert('Para gravar � necess�rio escolher um ramo.');
					return false;
				}
				
				if(usuarios.options.length < 1 )
				{
					alert('Para gravar � necess�rio escolher no m�nimo um usu�rio.');
					return false;
				}
				
				for(i=0;i<usuarios.options.length;i++) {
					usuarios.options[i].selected = true;
				}
				
				return true;
				//return false;
			}
		
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table id="tbCadastro" runat="server" class="tabelaform" cellSpacing="0" cellPadding="2"
				width="90%" align="center" border="0">
				<tr>
					<td>&nbsp;</td>
					<td style="WIDTH: 172px">&nbsp;</td>
					<td style="WIDTH: 49px">&nbsp;</td>
					<td>&nbsp;</td>
					<td style="WIDTH: 4px">&nbsp;</td>
				</tr>
				<tr bgColor="#f5f5f5">
					<td align="right" colSpan="5"><font face="verdana" size="2">SEGW0087</font></td>
				</tr>
				<tr>
					<td class="tituloseguranca" align="right" colSpan="5">Associar usu�rio � 
						ap�lice-subgrupo</td>
				</tr>
				<tr>
					<td class="labelseguranca" colSpan="5">&nbsp;</td>
				</tr>
				<tr>
					<td class="labelseguranca" colSpan="5">&nbsp;</td>
				</tr>
				<tr>
					<td class="labelseguranca" colSpan="5">&nbsp;
						<asp:label id="Label1" runat="server" BorderColor="Red" BorderWidth="0px" Width="100px">Ramo</asp:label>&nbsp;
						<asp:button id="btnGravar" runat="server" Width="100px" Visible="False" Text="Gravar" CssClass="INSeguranca"></asp:button>
					</td>
				</tr>
				<tr>
					<td class="labelseguranca" colSpan="5" style="HEIGHT: 13px">&nbsp;
						<asp:dropdownlist id="ddlRamo" runat="server" Width="264px" AutoPostBack="True">
							<asp:ListItem Value="-- Escolha --">-- Escolha --</asp:ListItem>
							<asp:ListItem Value=" 77"> 77 - Prestamista </asp:ListItem>
							<asp:ListItem Value="82">82 - Acidentes Pessoais Coletivo </asp:ListItem>
							<asp:ListItem Value="93">93 - Vida em Grupo </asp:ListItem>
							<asp:ListItem Value="98">98 - Seguro de Vida do Produtor Rural </asp:ListItem>
						</asp:dropdownlist></td>
				</tr>
				<tr id="trApolice_pt4" runat="server">
					<td class="labelseguranca" style="HEIGHT: 12px" colSpan="5">&nbsp;
						<asp:label id="Label3" runat="server" BorderColor="Red" BorderWidth="0px" Width="100px" CssClass="labelseguranca">Ap�lice</asp:label>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
				<tr>
					<td class="labelseguranca" colSpan="5">&nbsp;
						<asp:dropdownlist id="ddlApolice" runat="server" Width="136px" AutoPostBack="False">
							<asp:ListItem Value="--Escolha--" Selected="True">--Escolha--</asp:ListItem>
						</asp:dropdownlist>
					</td>
				</tr>
				<tr>
					<td class="labelseguranca" colSpan="5">&nbsp;Usu�rio <small>(Digite no m�nimo 3 letras 
							para pesquisar)</small></td>
				</tr>
				<tr>
					<td class="labelseguranca">&nbsp;
						<asp:textbox id="txtUsuario" runat="server"></asp:textbox></td>
					<td class="labelseguranca" colSpan="4"><asp:button id="btnPesquisar" runat="server" Text="Pesquisar" CssClass="INSeguranca"></asp:button></td>
				</tr>
				<tr>
					<td class="labelseguranca" colSpan="5">&nbsp;</td>
				</tr>
				<tr>
					<td class="labelseguranca" colSpan="3">&nbsp;Usu�rios</td>
					<td class="labelseguranca" colSpan="2">&nbsp;Usu�rios Associados � ap�lice-subgrupo</td>
				</tr>
				<tr>
					<td class="labelseguranca" colSpan="2">&nbsp;
						<asp:listbox id="ddlUser" runat="server" Width="320px" SelectionMode="Multiple" Rows="10" EnableViewState="False">
							<asp:ListItem Value="--Escolha Abaixo--">--Escolha Abaixo--</asp:ListItem>
						</asp:listbox></td>
					<td class="labelseguranca"><asp:button id="btnAssociarUsuario" runat="server" Width="100px" Text="Associar"></asp:button><br>
						&nbsp;<br>
						<asp:button id="btnRetirarUsuario" runat="server" Width="100px" Text="Retirar"></asp:button></td>
					<td class="labelseguranca" colSpan="2">&nbsp;
						<asp:listbox id="ddlUserApolice" runat="server" Width="320px" SelectionMode="Multiple" Rows="10"
							EnableViewState="False"></asp:listbox></td>
				</tr>
				<tr>
					<td class="labelseguranca" colSpan="5">&nbsp;</td>
				</tr>
				<tr id="trApolice_pt" runat="server" visible="false">
					<td class="labelseguranca" colSpan="5">&nbsp;</td>
				</tr>
				<tr id="trApolice_pt0" runat="server" visible="false">
					<td class="labelseguranca" colSpan="5">&nbsp;</td>
				</tr>
				<tr id="trApolice_pt1" runat="server" visible="false">
					<td class="labelseguranca" colSpan="3">&nbsp;Lista de ap�lices</td>
					<td class="labelseguranca" colSpan="2">&nbsp;Ap�lices escolhidas</td>
				</tr>
				<tr id="trApolice_pt2" runat="server" visible="false">
					<td class="labelseguranca" colSpan="2">&nbsp;
						<asp:listbox id="ddlApolices" runat="server" Width="320px" SelectionMode="Multiple" Rows="10">
							<asp:ListItem Value="--Escolha Abaixo--">--Escolha Abaixo--</asp:ListItem>
						</asp:listbox></td>
					<td class="labelseguranca"><asp:button id="Button3" runat="server" Width="100px" Text="Associar"></asp:button><br>
						&nbsp;<br>
						<asp:button id="Button4" runat="server" Width="100px" Text="Retirar"></asp:button></td>
					<td class="labelseguranca" colSpan="2">&nbsp;
						<asp:listbox id="ddlApolicesEscolhidas" runat="server" Width="320px" SelectionMode="Multiple"
							Rows="10"></asp:listbox></td>
				</tr>
				<tr id="trApolice_pt3" runat="server" visible="false">
					<td class="labelseguranca" colSpan="5">&nbsp;</td>
				</tr>
				<tr>
					<td class="labelseguranca" colSpan="5">&nbsp;</td>
				</tr>
				<tr>
					<td class="labelseguranca" colSpan="2"></td>
					<td class="labelseguranca" align="center">&nbsp;
						<asp:button id="btnGravar1" runat="server" Width="100px" Text="Gravar" CssClass="INSeguranca"></asp:button></td>
					<td class="labelseguranca" colSpan="2"></td>
				</tr>
			</table>
			<table id="tbConfirmacao" runat="server" visible="false" class="tabelaform" cellSpacing="0"
				cellPadding="2" width="90%" align="center" border="0">
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td style="FONT-SIZE: 13px; FONT-FAMILY: Verdana; TEXT-ALIGN: center" colspan="3"><B>Dados 
							cadastrados com sucesso !</B></td>
				</tr>
				<tr>
					<td colspan="4">&nbsp;
						<asp:DataList id="DataList1" runat="server">
							<HeaderTemplate>
								<B style="FONT-SIZE: 13px; FONT-FAMILY: Verdana">Ap�lices Associadas</B>
							</HeaderTemplate>
							<ItemTemplate>
								<%# Container.DataItem("apolice_id")%>
							</ItemTemplate>
						</asp:DataList></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td colspan="3" style="TEXT-ALIGN: center">
						<asp:Button id="btnNovoCadastro" runat="server" Text="Novo Cadastro"></asp:Button>&nbsp;
					</td>
				</tr>
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
