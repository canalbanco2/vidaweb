Partial Class WebForm1
    Inherits Estrutura.cPageBase

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not Page.IsPostBack Then
            Me.btnAssociarUsuario.Attributes.Add("onclick", "Btn_Associar('ddlUser','ddlUserApolice');return false;")
            Me.btnRetirarUsuario.Attributes.Add("onclick", "Btn_Retirar('ddlUserApolice','ddlUser');return false;")
            Me.btnPesquisar.Attributes.Add("onclick", "return verifBtnPesquisar();")
            Me.btnGravar1.Attributes.Add("onclick", "return gravar();")
        End If
    End Sub

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        If Me.txtUsuario.Text.Length < 3 Then
            Throw New Exception("Para realizar a consulta deve ser informado no m�nimo 3 letras!")
        End If

        If Me.ddlRamo.SelectedIndex = 0 Then
            Throw New Exception("Para gravar � necess�rio escolher um ramo.")
        End If

        Me.ddlUser.DataSource = cUsuario.getUsers(Me.txtUsuario.Text, Me.ddlRamo.SelectedValue, Me.ddlApolice.SelectedValue)
        Me.ddlUser.DataTextField = "NOME"
        Me.ddlUser.DataValueField = "VALUE"
        Me.ddlUser.DataBind()

        Me.ddlUserApolice.DataSource = cUsuario.getUsersConsulta(Me.ddlRamo.SelectedValue, Me.ddlApolice.SelectedValue)
        Me.ddlUserApolice.DataTextField = "NOME"
        Me.ddlUserApolice.DataValueField = "VALUE"
        Me.ddlUserApolice.DataBind()
    End Sub

    Private Sub ddlApolice_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlApolice.SelectedIndexChanged
        'If Me.ddlApolice.SelectedIndex <> 0 Then
        '    If Me.ddlApolice.SelectedValue = 1 Then
        '        Me.btnGravar.Visible = True
        '        Me.trApolice_pt1.Visible = False
        '        Me.trApolice_pt2.Visible = False
        '        Me.trApolice_pt3.Visible = False
        '        Me.trApolice_pt0.Visible = False
        '        Me.trApolice_pt.Visible = False
        '        Me.btnGravar1.Visible = False
        '    Else
        '        If Me.ddlApolice.SelectedValue = 2 Then
        '            Me.btnGravar.Visible = False
        '            Me.trApolice_pt1.Visible = True
        '            Me.trApolice_pt2.Visible = True
        '            Me.trApolice_pt3.Visible = True
        '            Me.trApolice_pt0.Visible = True
        '            Me.btnGravar1.Visible = True
        '        End If
        '    End If
        'End If
    End Sub

    Private Sub btnGravar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGravar.Click
        If Me.ddlUserApolice.Items.Count() = 0 Then
            Throw New Exception("Para gravar � necess�rio associar no m�nimo um usu�rio.")
        End If

        If Me.ddlRamo.SelectedIndex = 0 Then
            Throw New Exception("Para gravar � necess�rio escolher um ramo.")
        End If

    End Sub

    Private Sub btnGravar1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGravar1.Click
        If Me.ddlRamo.SelectedIndex = 0 Then
            Throw New Exception("Para gravar � necess�rio escolher um ramo.")
        End If

        If Request.Form.Item("ddlUserApolice").Trim = "" Then
            Throw New Exception("Para gravar � necess�rio escolher no m�nimo um usu�rio.")
        End If

        cUsuario.Inativa(Me.ddlRamo.SelectedValue, Me.ddlApolice.SelectedValue)

        For Each usuario As String In Request.Form.Item("ddlUserApolice").Split(",")
            cUsuario.save(Me.ddlRamo.SelectedValue, usuario.Split("|")(0), Session("usuario"), usuario.Split("|")(1), Me.ddlApolice.SelectedValue)
        Next

        Me.DataList1.DataSource = cApolice.getApolicesDR(Me.ddlRamo.SelectedValue)
        Me.DataList1.DataBind()
        Me.DataList1.Visible = False

        Me.txtUsuario.Text = ""
        Me.ddlUser.Items.Clear()
        Me.ddlUserApolice.Items.Clear()
        Me.ddlRamo.SelectedIndex = 0

        Me.tbCadastro.Visible = False
        Me.tbConfirmacao.Visible = True


    End Sub

    Private Sub btnNovoCadastro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNovoCadastro.Click
        Me.tbCadastro.Visible = True
        Me.tbConfirmacao.Visible = False
        Me.ddlApolice.Items.Clear()
    End Sub

    Private Sub ddlRamo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlRamo.SelectedIndexChanged
        Dim listApolices As ArrayList = DAO.cApoliceDAO.getAllByRamo(Integer.Parse(Me.ddlRamo.SelectedValue.Trim))

        Me.ddlApolice.DataSource = listApolices
        Me.ddlApolice.DataTextField = "ApoliceID"
        Me.ddlApolice.DataValueField = "ApoliceID"
        Me.ddlApolice.DataBind()
    End Sub
End Class
