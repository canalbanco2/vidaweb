Namespace Domain

    Public Class cApolice
#Region "Atributos"
        Private _apoliceID As Integer
        Private _ramoID As Integer
#End Region

#Region "Propriedades"

        Public Property ApoliceID()
            Get
                Return Me._apoliceID
            End Get
            Set(ByVal Value)
                Me._apoliceID = Value
            End Set
        End Property

        Public Property RamoID()
            Get
                Return Me._ramoID
            End Get
            Set(ByVal Value)
                Me._ramoID = Value
            End Set
        End Property

#End Region

        Sub New(ByVal pRamoID As Integer, ByVal pApoliceID As Integer)
            Me.ApoliceID = pApoliceID
            Me.RamoID = pRamoID
        End Sub

        Sub New()

        End Sub

    End Class

End Namespace
