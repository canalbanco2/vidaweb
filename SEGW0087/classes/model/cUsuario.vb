Imports Alianca.Seguranca.BancoDados
Imports System.Data


Public Class cUsuario

    Private _nome As String
    Private _id As Int64
    Private _cpf As String
    Private _login As String


    Sub New(ByVal nomeUsuario As String, ByVal idUsuario As Int64, ByVal cpfUsuario As String)
        Me.NOME = nomeUsuario
        Me.ID = idUsuario
        Me.CPF = cpfUsuario
    End Sub

    Sub New(ByVal nomeUsuario As String, ByVal idUsuario As Int64, ByVal cpfUsuario As String, ByVal loginUsuario As String)
        Me.NOME = nomeUsuario
        Me.ID = idUsuario
        Me.CPF = cpfUsuario
        Me.LOGIN = loginUsuario
    End Sub

#Region "Propriedades"


    Public Property NOME()
        Get
            Return Me._nome
        End Get
        Set(ByVal Value)
            Me._nome = Value
        End Set
    End Property

    Public Property LOGIN()
        Get
            Return Me._login
        End Get
        Set(ByVal Value)
            Me._login = Value
        End Set
    End Property

    Public Property ID()
        Get
            Return Me._id
        End Get
        Set(ByVal Value)
            Me._id = Value
        End Set
    End Property

    Public Property CPF()
        Get
            Return Me._cpf
        End Get
        Set(ByVal Value)
            Me._cpf = Value
        End Set
    End Property

    Public ReadOnly Property VALUE()
        Get
            Return Me._cpf & "|" & Me.NOME
        End Get
    End Property

#End Region

    '''Retorna um array de usu�rios de acordo com o nome do usu�rio informado.
    '''O parametro nome ser� utilizado usando o LIKE '%nomeUsuario%'
    '''Estes usu�rios n�o pertencem ao perfil Adm. Ap�lice, adm.subgrupo, operador e adm alian�a
    Public Shared Function getUsers(ByVal nomeUsuario As String, ByVal ramo As Int16, ByVal apolice As Integer) As Collections.ArrayList
        Dim arr As New ArrayList
        'Dim sql As String = "select nome, usuario_id, cpf, login_web from segab_db..usuario_tb where nome like '" & nomeUsuario & "%'  and login_web is not null and cpf not in (select distinct ua.cpf from web_seguros_db..usuario_apolice_tb ua where ua.ramo_id = " & ramo & " and ind_acesso <> 5 and ind_situacao = 'A') order by nome"
        Dim sql As String = "exec web_seguros_db..SEGS6574_SPS 1, '" & nomeUsuario & "', " & ramo & ",null," & apolice
        Dim user As cUsuario

        Dim dr As Data.SqlClient.SqlDataReader = cCon.ExecuteReader(CommandType.Text, sql, Nothing)

        If dr.HasRows Then
            While dr.Read()
                user = New cUsuario(dr.Item("nome").ToString(), dr.Item("usuario_id"), dr.Item("cpf").ToString(), dr.Item("login_web").ToString())
                arr.Add(user)
                user = Nothing
            End While
        End If

        Return arr
    End Function

    '''Retorna um array de usu�rios que pertencem ao perfil consulta do ramo selecionado
    Public Shared Function getUsersConsulta(ByVal ramo As Int16, ByVal apolice As Integer) As Collections.ArrayList
        Dim arr As New ArrayList
        'Dim sql As String = "select distinct u.nome, u.usuario_id, u.cpf, u.login_web from web_seguros_db..usuario_apolice_tb ua join segab_db..usuario_tb u on u.cpf = ua.cpf where ua.ramo_id = " & ramo & " and ua.ind_acesso = 5 and ua.ind_situacao = 'A' order by u.nome"
        Dim sql As String = "exec web_seguros_db..SEGS6574_SPS 2, '', " & ramo & ",null," & apolice
        Dim user As cUsuario

        Dim dr As Data.SqlClient.SqlDataReader = cCon.ExecuteReader(CommandType.Text, sql, Nothing)

        If dr.HasRows Then
            While dr.Read()
                user = New cUsuario(dr.Item("nome").ToString(), dr.Item("usuario_id"), dr.Item("cpf").ToString(), dr.Item("login_web").ToString())
                arr.Add(user)
                user = Nothing
            End While
        End If

        Return arr
    End Function

    Public Shared Sub save(ByVal ramoUser As Int16, ByVal cpfUser As String, ByVal usuario As String, ByVal nome As String, ByVal apolice As Integer)
        cCon.ExecuteNonQuery(CommandType.Text, "exec web_seguros_db..SEGS6575_SPI 2,''," & ramoUser & ",'" & cpfUser & "','" & usuario & "'," & HttpContext.Current.Session("GLAMBIENTE_ID") & "," & apolice)

        enviaEmail("", ramoUser, nome, cpfUser, apolice)
    End Sub

    Public Shared Sub Inativa(ByVal ramoUser As Int16, ByVal apolice As Integer)
        cCon.ExecuteNonQuery(CommandType.Text, "exec web_seguros_db..SEGS6575_SPI 1,''," & ramoUser & ",null,null,null," & apolice)
    End Sub

    Private Shared Function msg(ByVal senha As String, ByVal ramoUser As Int16, ByVal nome As String, ByVal apolice As Integer) As String

        Dim mensagem As New System.Text.StringBuilder

        Dim apolices As New System.Text.StringBuilder


        'For Each apolice As cApolice In cApolice.getApolices(ramoUser)
        '    'apolices += apolices & "," & apolice.ID
        '    apolices.Append("," & apolice.ID)
        'Next

        apolices.Append(apolice)

        Dim mes(12) As String
        mes(1) = "janeiro"
        mes(2) = "fevereiro"
        mes(3) = "mar�o"
        mes(4) = "abril"
        mes(5) = "maio"
        mes(6) = "junho"
        mes(7) = "julho"
        mes(8) = "agosto"
        mes(9) = "setembro"
        mes(10) = "outubro"
        mes(11) = "novembro"
        mes(12) = "dezembro"

        Dim ind_acesso As String
        ind_acesso = "Perfil de Consulta"

        mensagem.Append("<html><body>")
        mensagem.Append("<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""font-face:Arial;font-size:16px;"">")

        mensagem.Append("<tr>")
        mensagem.Append("   <td align=""right""><IMG src=""http://qld.aliancadobrasil.com.br/SEG/segw0071/files_print/bnb_alianca_apolice.gif"" border=""0""><br><br></td>")
        mensagem.Append("</tr>")
        mensagem.Append("<tr>")
        mensagem.Append("   <td align=""right"">S�o Paulo, " & Date.Now.Day.ToString.PadLeft(2, "0") & " de " & mes(Date.Now.Month) & " de " & Date.Now.Year & "<br><br></td>")
        mensagem.Append("</tr>")
        mensagem.Append("<tr>")
        mensagem.Append("   <td>Ramo: " & ramoUser & "<br><br></td>")
        mensagem.Append("</tr>")

        mensagem.Append("<tr>")
        mensagem.Append("   <td>Sr(a)." & nome.Replace("'", "''") & ", foi concedido acesso a aplica��o Movimenta��o vida a vida na web com o perfil de consulta<br><br></td>")
        mensagem.Append("</tr>")
        mensagem.Append("<tr>")
        mensagem.Append("   <td>As ap�lices liberadas para o seu acesso s�o: " & apolices.ToString().Substring(1) & ".<br><br></td>")
        mensagem.Append("</tr>")

        mensagem.Append("<tr>")
        mensagem.Append("   <td>Para fazer a altera��o da senha, acesse o site www.aliancadobrasil.com.br, no menu extranet e digite o login e a senha.<br><br></td>")
        mensagem.Append("</tr>")
        mensagem.Append("<tr>")
        mensagem.Append("   <td>Em caso de d�vida, ligue para nossa <b>Central de Atendimento aos Clientes � 0800 729 7000.</b><br><br></td>")
        mensagem.Append("</tr>")
        mensagem.Append("<tr>")
        mensagem.Append("   <td>Atenciosamente,<br><br></td>")
        mensagem.Append("</tr>")
        mensagem.Append("<tr>")
        mensagem.Append("   <td><b>Companhia de Seguros Alian�a do Brasil</b><br><br></td>")
        mensagem.Append("</tr>")
        mensagem.Append("</table>")
        mensagem.Append("</body></html>")

        Return mensagem.ToString()

    End Function

    Private Shared Sub enviaEmail(ByVal senha As String, ByVal ramoUser As Int16, ByVal nome As String, ByVal cpf As String, ByVal apolice As Integer)


        'Variaveis de configura��o do email
        Dim sigla_recurso, key, sigla_sistema, chave, de, para, assunto, mensagem, cc, anexo, formato, idUser As String

        sigla_recurso = "SEGW0087"
        sigla_sistema = "SEGBR"
        chave = "7800wges"
        de = "emissao.vida@aliancadobrasil.com.br"
        'para = Me.txtEmail.Text
        assunto = "Confirma��o de Cadastro na Web"
        idUser = Web.HttpContext.Current.Session("usuario")

        mensagem = msg(senha, ramoUser, nome, apolice)

        Dim sql As String = "exec web_seguros_db..SEGS6574_SPS 4, '', 93,'" & cpf & "'"

        Dim dr As Data.SqlClient.SqlDataReader = cCon.ExecuteReader(CommandType.Text, sql, Nothing)

        If dr.HasRows Then
            If dr.Read() Then
                para = dr.Item("email")
            End If
        End If

        anexo = "null"
        formato = "2"

        enviaEmailSenha(sigla_recurso, sigla_sistema, chave, de, para, assunto, mensagem, cc, anexo, formato, idUser)



    End Sub

    Private Shared Sub enviaEmailSenha(ByVal sigla_recurso As String, ByVal sigla_sistema As String, ByVal chave As String, ByVal de As String, ByVal para As String, ByVal assunto As String, ByVal mensagem As String, ByVal cc As String, ByVal anexo As String, ByVal formato As String, ByVal id As String)

        Dim sql As String
        sql = " exec email_db..SGSS0782_SPI "
        sql &= " @sigla_recurso ='" & sigla_recurso
        sql &= "', @sigla_sistema ='" & sigla_sistema
        sql &= "', @chave ='" & chave
        sql &= "', @de ='" & de
        sql &= "', @para ='" & para
        sql &= "', @assunto ='" & assunto
        sql &= "', @mensagem ='" & mensagem
        sql &= "', @cc ='" & cc
        sql &= "', @anexo ='" & anexo
        sql &= "', @formato ='" & formato
        sql &= "', @usuario ='" & id & "'"

        'Try

        cCon.ExecuteNonQuery(CommandType.Text, sql)


        'Dim dr As Data.SqlClient.SqlDataReader = cCon.ExecuteReader(CommandType.Text, sql) 'bd.ExecutaSQL_DR
        'Dim id_email As String = ""

        'If dr.Read Then
        'id_email = dr.GetValue(0).ToString
        'End If
        'dr.Close()
        'dr = Nothing

        'bd.SEGS5745_SPI(id_email, Session("apolice"), Session("ramo"), Session("subgrupo_id"), Session("usuario"))

        'bd.ExecutaSQL()

        'Catch ex As Exception
        'Dim excp As New clsException(ex)
        'End Try

    End Sub

End Class
