Imports Alianca.Seguranca.BancoDados
Imports System.Data

Public Class cApolice
    Private _ramo As Int64
    Private _id As Int64
    'Public _subgrupo As New Collections.SortedList


    Sub New(ByVal ramoApolice As Int64, ByVal idApolice As Int64)
        Me.ID = idApolice
        Me.RAMO = ramoApolice
    End Sub

#Region "Propriedades"


    Public Property ID()
        Get
            Return Me._id
        End Get
        Set(ByVal Value)
            Me._id = Value
        End Set
    End Property

    Public Property RAMO()
        Get
            Return Me._ramo
        End Get
        Set(ByVal Value)
            Me._ramo = Value
        End Set
    End Property

#End Region

    '''Retorna um array de usu�rios de acordo com o nome do usu�rio informado.
    '''O parametro nome ser� utilizado usando o LIKE '%nomeUsuario%'
    Public Shared Function getApolices(ByVal ramo As Int64) As Collections.ArrayList
        Dim arr As New ArrayList
        Dim sql As String = "exec web_seguros_db..SEGS6574_SPS 3, '', " & ramo
        Dim apolice As cApolice

        Dim dr As Data.SqlClient.SqlDataReader = cCon.ExecuteReader(CommandType.Text, sql, Nothing)

        If dr.HasRows Then
            While dr.Read()

                apolice = New cApolice(ramo, dr.Item("apolice_id"))
                arr.Add(apolice)
                apolice = Nothing

            End While
        End If

        Return arr
    End Function

    '''Retorna um array de usu�rios de acordo com o nome do usu�rio informado.
    '''O parametro nome ser� utilizado usando o LIKE '%nomeUsuario%'
    Public Shared Function getApolicesDR(ByVal ramo As Int64) As SqlClient.SqlDataReader
        'Dim arr As New ArrayList
        Dim sql As String = "exec web_seguros_db..SEGS6574_SPS 3, '', " & ramo
        'Dim apolice As cApolice

        Return cCon.ExecuteReader(CommandType.Text, sql, Nothing)

        'If dr.HasRows Then
        'While dr.Read()

        'apolice = New cApolice(ramo, dr.Item("apolice_id"))
        'arr.Add(apolice)
        'apolice = Nothing

        'End While
        'End If

        'Return arr


    End Function
End Class



