Namespace Estrutura

    Public Class cPageBase
        Inherits System.Web.UI.Page



        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

            Dim linkseguro As Alianca.Seguranca.Web.LinkSeguro

            'Implementação da SABL0101
            linkseguro = New Alianca.Seguranca.Web.LinkSeguro
            Dim usuario As String = linkseguro.LerUsuario("Default.aspx", Request.ServerVariables("REMOTE_ADDR"), Request("pqsSeguranca"))
            If usuario = "" Then
                Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
            End If

            Session("usuario_id") = linkseguro.Usuario_ID
            Session("usuario") = linkseguro.Login_REDE
            Session("cpf") = linkseguro.CPF

            Dim cAmbiente As Alianca.Seguranca.Web.ControleAmbiente
            Dim url As String

            cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
            url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
            cAmbiente.ObterAmbiente(url)
            If Alianca.Seguranca.BancoDados.cCon.configurado Then
                Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            Else
                Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            End If

            Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)
        End Sub
    End Class

End Namespace
