
Public Class cAcompanhamento

#Region "Heran�a"
    Inherits cBanco
#End Region


    'Busca o Resumo da Fatura atual (anterior) da tela de abertura do Vida
    Public Sub ResumoFaturaAtual_Anterior(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal usuario As String, ByVal wf_id As Int64)

        'Busca Resumo da Fatura Atual Anterior
        SQL = "exec VIDA_WEB_DB..SEGS5732_SPS "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @subgrupo_id=" & subgrupo_id
        SQL &= ", @usuario=" & trStr(usuario)
        SQL &= ", @wf_id = " & wf_id

    End Sub
    'retorna a data de in�cio e fim de vig�ncia do subgrupo

    Public Sub GetCapitalMaximo(ByVal apolice_id As Integer, ByVal subgrupo_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal usuario As String, ByVal ind_acesso As Integer, ByVal cpf_id As String)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5744_SPS "
        SQL &= "  @apolice_id=" & trInt(apolice_id)
        SQL &= ", @subgrupo_id=" & trInt(subgrupo_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @cod_susep=" & cod_susep
        SQL &= ", @seguradora_id=" & seguradora_id
        SQL &= ", @usuario=" & usuario
        SQL &= ", @ind_acesso=" & ind_acesso
        SQL &= ", @cpf_id=" & cpf_id
    End Sub


    'Busca o Resumo da Fatura atual (Inclusoes, Altera��es e Exclusoes) da tela de abertura do Vida
    Public Sub ResumoFaturaAtual_IAE(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal usuario As String, ByVal wf_id As Int64)

        'Busca Resumo da Fatura Atual: IAE
        SQL = "exec VIDA_WEB_DB..SEGS5733_SPS "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @subgrupo_id=" & subgrupo_id
        SQL &= ", @usuario=" & trStr(usuario)
        SQL &= ", @wf_id = " & wf_id

    End Sub



    Public Sub SEGS5655_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String)

        SQL = "exec VIDA_WEB_DB..SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @cpf = '" & cpf & "'"
        SQL &= ", @nome = " & nome

    End Sub
    Public Sub SEGS5696_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal workflow As Integer, ByVal valor_id As Integer)
        SQL = "exec VIDA_WEB_DB..segs5696_sps  @apolice_id = " & apolice_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @tp_wf_id = " & workflow
        SQL &= ", @tp_versao_id = " & valor_id
    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String, ByVal id As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @cpf = " & cpf
        SQL &= ", @nome = " & nome
        SQL &= ", @id = " & id
    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String, ByVal id As String, ByVal tipo As String)

        SQL = "exec VIDA_WEB_DB..SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @cpf = " & cpf
        SQL &= ", @nome = " & nome
        SQL &= ", @id = " & id
        SQL &= ", @tipo = " & tipo

    End Sub

    Public Sub SEGS5740_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal fatura As String, ByVal operacao As String)

        SQL = "exec VIDA_WEB_DB..SEGS5740_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @fatura_id = " & fatura
        SQL &= ", @operacao = " & operacao


    End Sub
    Public Sub SEGS5740_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal fatura As String, ByVal operacao As String, ByVal content As String)

        SQL = "exec VIDA_WEB_DB..SEGS5740_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @fatura_id = " & fatura
        SQL &= ", @operacao = " & operacao
        SQL &= ", @strPesq = " & content


    End Sub

    Public Sub SEGS5658_SPD(ByVal id As Integer, ByVal dt As String)
        SQL = "exec VIDA_WEB_DB..SEGS5658_SPD "
        SQL &= " @id = " & id
        SQL &= ", @dtDesligamento  = " & "'" & cUtilitarios.trataDataDB(dt) & "'"
    End Sub

    Public Sub SEGS5657_SPU(ByVal id As Integer, ByVal seq_cliente_id As Integer, ByVal seq_faturamento As Integer, ByVal apolice_id As Integer, ByVal sucursal_seguradora_id As Integer, ByVal seguradora_cod_susep As Integer, ByVal ramo_id As Integer, ByVal sub_grupo_id As Integer, ByVal dt_inicio_vigencia_sbg As String, ByVal nome As String, ByVal tp_componente_id As Integer, ByVal cpf As String, ByVal dt_nascimento As String, ByVal sexo As String, ByVal val_capital_segurado As String, ByVal val_salario As String, ByVal dt_inclusao As String, ByVal usuario As String, ByVal pendente As String, ByVal permanente As String)
        apolice_id = Int(apolice_id)
        ramo_id = Int(ramo_id)
        seguradora_cod_susep = Int(seguradora_cod_susep)
        sucursal_seguradora_id = Int(sucursal_seguradora_id)
        sub_grupo_id = Int(sub_grupo_id)
        nome = "'" & nome & "'"
        cpf = "'" & cUtilitarios.destrataCPF(cpf) & "'"
        dt_nascimento = "'" & cUtilitarios.trataDataDB(dt_nascimento) & "'"
        sexo = "'" & sexo.Substring(0, 1) & "'"
        seq_cliente_id = Int(seq_cliente_id)
        tp_componente_id = Int(tp_componente_id)
        dt_inicio_vigencia_sbg = "'" & cUtilitarios.trataDataDB(dt_inicio_vigencia_sbg) & "'"
        usuario = "'" & usuario & "'"
        val_capital_segurado = val_capital_segurado.Replace(",", ".")
        val_salario = val_salario.Replace(",", ".")
        val_capital_segurado = val_capital_segurado.Replace(",", ".")

        SQL = "exec VIDA_WEB_DB..SEGS5657_SPU @id=" & id
        SQL &= ",  @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seguradora_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & sucursal_seguradora_id
        SQL &= ", @sub_grupo_id = " & sub_grupo_id
        SQL &= ", @nome = " & nome
        SQL &= ", @cpf = " & cpf
        SQL &= ", @dt_nascimento = " & dt_nascimento
        SQL &= ", @sexo = " & sexo
        SQL &= ", @val_capital_segurado = " & val_capital_segurado
        SQL &= ", @val_salario = " & val_salario
        SQL &= ", @seq_faturamento = " & seq_faturamento
        SQL &= ", @seq_cliente_id = " & seq_cliente_id
        SQL &= ", @tp_componente_id = " & tp_componente_id
        SQL &= ", @dt_inicio_vigencia_sbg = " & dt_inicio_vigencia_sbg
        SQL &= ", @pendente = " & pendente
        SQL &= ", @permanente = " & permanente
        SQL &= ", @usuario = " & usuario & " "


    End Sub

    Public Sub SEGS5656_SPI(ByVal seq_cliente_id As Integer, ByVal seq_faturamento As Integer, ByVal apolice_id As Integer, ByVal sucursal_seguradora_id As Integer, ByVal seguradora_cod_susep As Integer, ByVal ramo_id As Integer, ByVal sub_grupo_id As Integer, ByVal dt_inicio_vigencia_sbg As String, ByVal nome As String, ByVal tp_componente_id As Integer, ByVal cpf As String, ByVal dt_nascimento As String, ByVal sexo As String, ByVal val_capital_segurado As String, ByVal val_salario As String, ByVal dt_inclusao As String, ByVal usuario As String, ByVal pendente As String, ByVal permanente As String, ByVal id As Integer)

        apolice_id = Int(apolice_id)
        ramo_id = Int(ramo_id)
        seguradora_cod_susep = Int(seguradora_cod_susep)
        sucursal_seguradora_id = Int(sucursal_seguradora_id)
        sub_grupo_id = Int(sub_grupo_id)
        nome = "'" & nome & "'"
        cpf = "'" & cUtilitarios.destrataCPF(cpf) & "'"
        dt_nascimento = "'" & cUtilitarios.trataDataDB(dt_nascimento) & "'"
        sexo = "'" & sexo.Substring(0, 1) & "'"
        seq_cliente_id = Int(seq_cliente_id)
        tp_componente_id = Int(tp_componente_id)
        dt_inicio_vigencia_sbg = "'" & cUtilitarios.trataDataDB(dt_inicio_vigencia_sbg) & "'"
        usuario = "'" & usuario & "'"
        val_salario = val_salario.Replace(",", ".")
        val_capital_segurado = val_capital_segurado.Replace(",", ".")

        SQL = "exec VIDA_WEB_DB..SEGS5656_SPI "
        SQL &= "  @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seguradora_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & sucursal_seguradora_id
        SQL &= ", @sub_grupo_id = " & sub_grupo_id
        SQL &= ", @nome = " & nome
        SQL &= ", @cpf = " & cUtilitarios.destrataCPF(cpf)
        SQL &= ", @dt_nascimento = " & dt_nascimento
        SQL &= ", @sexo = " & sexo
        SQL &= ", @val_capital_segurado = " & val_capital_segurado
        SQL &= ", @val_salario = " & val_salario
        SQL &= ", @seq_faturamento = " & seq_faturamento
        SQL &= ", @seq_cliente_id = " & seq_cliente_id
        SQL &= ", @tp_componente_id = " & tp_componente_id
        SQL &= ", @dt_inicio_vigencia_sbg = " & dt_inicio_vigencia_sbg
        SQL &= ", @pendente = " & pendente
        SQL &= ", @permanente = " & permanente
        SQL &= ", @usuario = " & usuario & " "
        SQL &= ", @idAnterior = " & id & " "
    End Sub

    '''Consulta as condi��es do subgrupo
    Public Sub SEGS5705_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5705_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub


    '''Consulta as condi��es do subgrupo
    Public Sub SEGS5706_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5706_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub

    '''Consulta as condi��es do subgrupo (valor lim�te m�ximo)
    Public Sub SEGS5744_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5744_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub

    '''Consulta as condi��es do subgrupo SEGS5750_SPS
    Public Sub coberturas(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer, ByVal componente As String)

        SQL = "exec VIDA_WEB_DB..SEGS5750_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @componente = " & componente
        SQL &= ", @ind_acesso = 0"
        SQL &= ", @cpf_id = '0'"
        SQL &= ", @usuario = 0"
        SQL &= ", @seguradora_cod_susep = 6785"
        SQL &= ", @sucursal_seguradora_id = 0 "

    End Sub

    Public Sub segs5876_sps(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal cliente_id As Integer, ByVal dt_inicio_vigencia_sbr As String, ByVal dt_fim_vigencia_sbr As String, ByVal cpf As String)

        SQL = "set nocount on exec VIDA_WEB_DB..segs5876_sps "
        SQL &= "  @apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @sub_grupo_id =" & trInt(subgrupo_id)
        SQL &= ", @cliente_id =" & trInt(cliente_id)
        SQL &= ", @dt_inicio_vigencia_sbr =" & dt_inicio_vigencia_sbr
        SQL &= ", @dt_fim_vigencia_sbr =" & dt_fim_vigencia_sbr
        SQL &= ", @cpf_segurado =" & cpf

    End Sub

    Public Sub GetDadosSubgrupoE1(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5751_SPS "
        SQL &= "  @apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @subgrupo_id=" & trInt(subgrupo_id)



    End Sub

    Public Sub GetDadosSubgrupoE1(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal cpf As String, ByVal fatura_id As Integer)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5751_SPS "
        SQL &= "  @apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @subgrupo_id=" & trInt(subgrupo_id)
        SQL &= ", @cpf=" & trStr(cpf)
        SQL &= ", @fatura_id=" & trInt(fatura_id)

    End Sub

    '''Consulta as condi��es do subgrupo SEGS5750_SPS ("tecnicamente", foi colocado assim pois havia duvidas quanto a essa altera��o)
    Public Sub coberturasNew(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer, ByVal componente As String)

        SQL = "SELECT distinct case es.class_tp_cobertura WHEN 'B' Then 1 WHEN 'A' THEN 2 WHEN 'D' THEN 3 WHEN 'C' THEN 4 WHEN 'F' THEN 5 ELSE 6 END as class_tp_cobertura,"
        SQL &= " cc.tp_cobertura_id, cb.nome, "
        SQL &= " case es.perc_basica WHEN 0 THEN 100"
        SQL &= " ELSE es.perc_basica"
        SQL &= " END as perc_basica, "
        SQL &= " es.carencia_dias,"
        SQL &= " isnull(es.val_lim_max_is, 0) val_lim_max_is,"
        SQL &= " es.val_min_franquia,"
        SQL &= " CASE cb.nome WHEN 'MORTE NATURAL OU ACIDENTAL' THEN ' MORTE NATURAL OU ACIDENTAL' ELSE cb.nome END as nome, cb.tp_cobertura_id "
        SQL &= " FROM    [sisab003].seguros_db.dbo.escolha_sub_grp_tp_cob_comp_tb es  (NOLOCK) "
        SQL &= " JOIN [sisab003].seguros_db.dbo.tp_cob_comp_tb cc  (NOLOCK) "
        SQL &= " ON  es.tp_cob_comp_id = cc.tp_cob_comp_id "
        SQL &= " JOIN [sisab003].seguros_db.dbo.tp_cobertura_tb cb  (NOLOCK) "
        SQL &= " ON cc.tp_cobertura_id = cb.tp_cobertura_id  "
        SQL &= " WHERE   es.apolice_id = " & apolice_id
        SQL &= " AND es.sucursal_seguradora_id = " & seguradora_id
        SQL &= " AND es.seguradora_cod_susep = " & cod_susep
        SQL &= " AND es.ramo_id = " & ramo_id
        SQL &= " AND es.sub_grupo_id = " & subgrupo
        SQL &= " AND cc.tp_componente_id = " & componente
        SQL &= " ORDER BY class_tp_cobertura ASC, nome ASC, cb.tp_cobertura_id DESC "

    End Sub

    '''Consulta as condi��es do subgrupo SEGS5750_SPS ("tecnicamente", foi colocado assim pois havia duvidas quanto a essa altera��o)
    Public Sub coberturasNew(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer, ByVal componente As String, ByVal class_tp_cobertura As String)

        SQL = "SELECT distinct es.class_tp_cobertura,"
        SQL &= " cc.tp_cobertura_id, cb.nome, "
        SQL &= " case es.perc_basica WHEN 0 THEN 100"
        SQL &= " ELSE es.perc_basica"
        SQL &= " END as perc_basica, "
        SQL &= " es.carencia_dias,"
        SQL &= " isnull(es.val_lim_max_is, 0) val_lim_max_is,"
        SQL &= " es.val_min_franquia,"
        SQL &= " cb.nome, cb.tp_cobertura_id "
        SQL &= " FROM    [sisab003].seguros_db.dbo.escolha_sub_grp_tp_cob_comp_tb es  (NOLOCK) "
        SQL &= " JOIN [sisab003].seguros_db.dbo.tp_cob_comp_tb cc  (NOLOCK) "
        SQL &= " ON  es.tp_cob_comp_id = cc.tp_cob_comp_id "
        SQL &= " JOIN [sisab003].seguros_db.dbo.tp_cobertura_tb cb  (NOLOCK) "
        SQL &= " ON cc.tp_cobertura_id = cb.tp_cobertura_id  "
        SQL &= " WHERE   es.apolice_id = " & apolice_id
        SQL &= " AND es.sucursal_seguradora_id = " & seguradora_id
        SQL &= " AND es.seguradora_cod_susep = " & cod_susep
        SQL &= " AND es.ramo_id = " & ramo_id
        SQL &= " AND es.sub_grupo_id = " & subgrupo
        SQL &= " AND cc.tp_componente_id = " & componente
        SQL &= " ORDER BY es.class_tp_cobertura DESC, cb.tp_cobertura_id DESC "

    End Sub


    Public Sub getTpClausulaConjuge(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer)

        SQL = " Select tp_clausula_conjuge"
        SQL &= " from [sisab003].seguros_db.dbo.sub_grupo_apolice_tb "
        SQL &= " where  apolice_id =" & apolice_id
        SQL &= " and ramo_id = " & ramo_id
        SQL &= " and  sub_grupo_id = " & subgrupo

    End Sub


    Public Sub getTpApoliceConjuge(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As String)
        SQL = "select isnull(tp_clausula_conjuge,'N')"
        SQL &= " from [sisab003].seguros_db.dbo.sub_grupo_apolice_tb"
        SQL &= " where apolice_id = " & apolice_id & " and"
        SQL &= " sucursal_seguradora_id = 0 and"
        SQL &= " seguradora_cod_susep = 6785 and"
        SQL &= " ramo_id = " & ramo_id & " and"
        SQL &= " sub_grupo_id = " & subgrupo_id
    End Sub

    Public Sub SEGS5744_SPS(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As String)
        SQL = "exec VIDA_WEB_DB..SEGS5744_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @cod_susep = 6785"
        SQL &= ", @seguradora_id = 0"
        SQL &= ", @subgrupo_id = " & subgrupo_id
        SQL &= ", @usuario = ''"
        SQL &= ", @ind_acesso = ''"
        SQL &= ", @cpf_id = ''"
    End Sub

    Public Sub SEGS5828(ByVal cpf As String)
        SQL = "exec segs5828_sps '%" & cpf & "%'"
    End Sub
    Public Sub New(Optional ByVal banco As cDadosBasicos.eBanco = cDadosBasicos.eBanco.VIDA_WEB_DB)
        MyBase.New(banco)
        Alianca.Seguranca.BancoDados.cCon.BancodeDados = banco.ToString

    End Sub

    Function trInt(ByVal valor As Object) As String

        If valor = 0 Then
            Return "null"
        Else
            Return valor
        End If

    End Function

    Function trStr(ByVal valor As Object) As String

        If valor = "" Then
            Return "null"
        Else
            Return "'" & valor.ToString.Replace("'", "''") & "'"
        End If

    End Function

    '************************************************************************
    '''ABAIXO, fun��es para Impress�o do Certificado (impressao.aspx)

    Public Sub BuscarDadosSubEstipulante(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal dt_ini_fatura As String)

        SQL = "exec [sisab003].seguros_db.dbo.sel_estip_fatura_sps "
        SQL &= "@seguradora= 6785"
        SQL &= ",@sucursal= 0"
        SQL &= ",@apolice=" & apolice_id
        SQL &= ", @ramo=" & ramo_id
        SQL &= ", @subgrupo=" & subgrupo_id
        SQL &= ", @dt_ini_fatura = '" & dt_ini_fatura & "'"

        'OBS:Tipo @dt_ini_fatura � SmallDateTime

    End Sub

    Public Sub BuscarDadosCliente(ByVal cliente_id As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5769_SPS "
        SQL &= "@cliente_id=" & cliente_id

    End Sub

    Public Sub BuscaDadosCorretor(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal dt_ini_fatura As String)

        SQL = "exec VIDA_WEB_DB..SEGS5768_SPS "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @subgrupo_id=" & subgrupo_id
        SQL &= ", @dt_ini_fatura = '" & dt_ini_fatura & "'"


    End Sub

    Public Sub BuscaPremio(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal nr_fatura As Integer, ByVal cliente_id As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5784_SPS "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @numero_fatura=" & nr_fatura
        SQL &= ", @cliente_id=" & cliente_id

    End Sub

    '''Consulta os dados do estipulante
    Public Sub dados_basico_apolice_sps(ByVal apolice_id As Int16, ByVal ramo_id As Int16, ByVal cod_susep As Int16, ByVal seguradora_id As Int16, ByVal cpf As String)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5660_SPS "
        SQL &= "@apolice_id=" & trInt(apolice_id)
        SQL &= ", @seguradora_cod_susep=" & trInt(cod_susep)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @sucursal_seguradora_id=" & seguradora_id
        SQL &= ", @cpf=" & cpf

    End Sub


    Public Sub getDataIniVigenciaApolice(ByVal apolice_id As String, ByVal ramo_id As String)
        SQL = "select convert(varchar,dt_inicio_vigencia,103) as data, convert(varchar,dt_fim_vigencia,103) as data_fim "
        SQL &= " from [sisab003].seguros_db.dbo.apolice_tb"
        SQL &= " where apolice_id = " + apolice_id + " and"
        SQL &= " ramo_id = " + ramo_id + " and "
        SQL &= " sucursal_seguradora_id = 0 and"
        SQL &= " seguradora_cod_susep = 6785"

    End Sub

    Public Sub buscaCertificadoID(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As Integer, ByVal cliente_id As Integer, Optional ByVal seguradora_cod_susepi As Int16 = 6785)
        'Sucursal = 0
        SQL = "exec VIDA_WEB_DB..SEGS5778_SPS "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @sucursal_id= 0"
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @subgrupo_id=" & subgrupo_id
        SQL &= ", @cliente_id=" & cliente_id
        SQL &= ", @seguradora_cod_susep = 6785"

    End Sub

    Public Sub geraCertificadoID(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As Integer, ByVal cliente_id As Integer, ByVal Usuario As String, Optional ByVal seguradora_cod_susepi As Int16 = 6785)
        'Sucursal = 0
        SQL = "exec SISAB003.SEGUROS_DB.dbo.rel_certif_sps "
        SQL &= "@apolice=" & apolice_id
        SQL &= ", @sucursal= 0"
        SQL &= ", @ramo=" & ramo_id
        SQL &= ", @subgrupo=" & subgrupo_id
        SQL &= ", @cliente_id=" & cliente_id
        SQL &= ", @usuario=" & Usuario
        SQL &= ", @seguradora = 6785"

    End Sub

    Public Sub buscaDataEmissao(ByVal apolice_id As String, ByVal ramo_id As String, ByVal certificado_id As Integer, Optional ByVal seguradora_cod_susepi As Int16 = 6785)
        'Sucursal = 0
        SQL = "exec VIDA_WEB_DB..SEGS5779_SPS "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @sucursal_id= 0"
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @certificado_id=" & certificado_id
        SQL &= ", @seguradora_cod_susep = 6785"

    End Sub

    Public Sub buscaObservacoes(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As Integer, Optional ByVal seguradora_cod_susepi As Int16 = 6785)
        'Sucursal = 0
        SQL = "exec VIDA_WEB_DB..SEGS5780_SPS "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @sucursal_id= 0"
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @subgrupo_id=" & subgrupo_id
        SQL &= ", @seguradora_cod_susep = 6785"

    End Sub

    Public Sub dados_segurado_sps(ByVal prop_cliente As Integer, ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As Integer)

        SQL = "exec [sisab003].seguros_db.dbo.dados_segurado_sps  "
        SQL &= " @prop_cliente=" & prop_cliente
        SQL &= ", @apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @sub_grupo_id=" & subgrupo_id

    End Sub

    Public Sub getCodSusep(ByVal apolice_id As Integer, ByVal ramo_id As Integer)
        SQL = " exec [sisab003].seguros_db.dbo.num_proc_susep_apolice_sps "
        SQL &= " @apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
    End Sub


    '''Busca N� Certificado
    Public Sub SEGS0000_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal usuario As String)
        SQL = "exec VIDA_WEB_DB..SEGS0000_SPS "
        SQL &= "  @apolice = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @cpf = " & cpf
        SQL &= ", @usuario = '" & usuario
        SQL &= "', @sub_grupo_id = " & subgrupo
    End Sub

    Public Sub SEGS7032_SPS(ByVal apolice_id As Integer, _
                            ByVal sucursal As Integer, _
                            ByVal cod_susep As Integer, _
                            ByVal ramo_id As String, _
                            ByVal subgrupo_id As String, _
                            ByVal prop_cliente As String)

        SQL = "exec VIDA_WEB_DB..SEGS7032_SPS "
        SQL &= "  @apolice_id = " & apolice_id
        SQL &= ", @sucursal = " & sucursal
        SQL &= ", @cod_susep = " & cod_susep
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @subgrupo_id = " & subgrupo_id
        SQL &= ", @prop_cliente = " & prop_cliente
    End Sub

    Public Sub SEGS6980_SPS(ByVal cliente_id As Integer, _
                            ByVal apolice_id As Integer, _
                            ByVal ramo_id As Integer, _
                            ByVal subgrupo_id As Integer, _
                            ByVal fatura_id As Integer, _
                            ByVal dt_ini_fatura As String, _
                            ByVal certificado_id As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS6980_SPS "
        SQL &= "  @cliente_id = " & cliente_id
        SQL &= ", @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @subgrupo_id = " & subgrupo_id
        SQL &= ", @fatura_id = " & fatura_id
        SQL &= ", @dt_ini_fatura = '" & dt_ini_fatura
        SQL &= "', @certificado_id = " & certificado_id
    End Sub

End Class


Public Class cDadosImpressao
    Private vTitulo As String
    Private vProcessoSusep As String
    Private vRamo As String
    Private vNumApolice As Integer
    Private vIni_vg_apo As String
    Private vIni_vg_subgrp As String
    Private vFim_vg_subgrp As String
    Private vFim_vg_apo As String
    Private vNomeEstipulante As String
    Private vCNPJe As String
    Private vNomeSubEstipulante As String
    Private vCNPJsub_e As String
    Private vSeg_Nome As String
    Private vSeg_DataNasc As String
    Private vSeg_cpf As String
    Private vPremio As String
    Private vCorretor As String
    Private vSUSEP As String
    Private vCertificadoID As String
    Private vDataEmissao As String
    Private vObservacoes As String
    '''referentes da tabela de coberturas
    Private vIEA_min As String
    Private vIEA_max As String
    Private vIPA_min As String
    Private vIPA_max As String
    Private vIPD_min As String
    Private vIPD_max As String
    Private vMNA_min As String
    Private vMNA_max As String

    Public Property ProcessoSusep() As String
        Get
            Return vProcessoSusep
        End Get
        Set(ByVal Value As String)
            vProcessoSusep = Value
        End Set
    End Property
    Public Property Titulo() As String
        Get
            Return vTitulo
        End Get
        Set(ByVal Value As String)
            vTitulo = Value
        End Set
    End Property

    Public Property Ramo() As String
        Get
            Return vRamo
        End Get
        Set(ByVal Value As String)
            vRamo = Value
        End Set
    End Property

    Public Property NumApolice() As Integer
        Get
            Return vNumApolice
        End Get
        Set(ByVal Value As Integer)
            vNumApolice = Value
        End Set
    End Property

    Public Property Ini_vg_apo() As String
        Get
            Return vIni_vg_apo
        End Get
        Set(ByVal Value As String)
            vIni_vg_apo = Value
        End Set
    End Property

    Public Property Fim_vg_apo() As String
        Get
            Return vFim_vg_apo
        End Get
        Set(ByVal Value As String)
            vFim_vg_apo = Value
        End Set
    End Property

    Public Property NomeEstipulante() As String
        Get
            Return vNomeEstipulante
        End Get
        Set(ByVal Value As String)
            vNomeEstipulante = Value
        End Set
    End Property

    Public Property CNPJe() As String
        Get
            Return vCNPJe
        End Get
        Set(ByVal Value As String)
            vCNPJe = Value
        End Set
    End Property

    Public Property NomeSubEstipulante() As String
        Get
            Return vNomeSubEstipulante
        End Get
        Set(ByVal Value As String)
            vNomeSubEstipulante = Value
        End Set
    End Property

    Public Property CNPJsub_e() As String
        Get
            Return vCNPJsub_e
        End Get
        Set(ByVal Value As String)
            vCNPJsub_e = Value
        End Set
    End Property

    Public Property Seg_Nome() As String
        Get
            Return vSeg_Nome
        End Get
        Set(ByVal Value As String)
            vSeg_Nome = Value
        End Set
    End Property

    Public Property Seg_DataNasc() As String
        Get
            Return vSeg_DataNasc
        End Get
        Set(ByVal Value As String)
            vSeg_DataNasc = Value
        End Set
    End Property

    Public Property Seg_cpf() As String
        Get
            Return vSeg_cpf
        End Get
        Set(ByVal Value As String)
            vSeg_cpf = Value
        End Set
    End Property

    Public Property Premio() As String
        Get
            Return vPremio
        End Get
        Set(ByVal Value As String)
            vPremio = Value
        End Set
    End Property

    Public Property Corretor() As String
        Get
            Return vCorretor
        End Get
        Set(ByVal Value As String)
            vCorretor = Value
        End Set
    End Property

    Public Property SUSEP() As String
        Get
            Return vSUSEP
        End Get
        Set(ByVal Value As String)
            vSUSEP = Value
        End Set
    End Property

    Public Property Ini_vg_subgrp() As String
        Get
            Return vIni_vg_subgrp
        End Get
        Set(ByVal Value As String)
            vIni_vg_subgrp = Value
        End Set
    End Property
    Public Property Fim_vg_subgrp() As String
        Get
            Return vFim_vg_subgrp
        End Get
        Set(ByVal Value As String)
            vFim_vg_subgrp = Value
        End Set
    End Property
    Public Property CertificadoID() As String
        Get
            Return vCertificadoID
        End Get
        Set(ByVal Value As String)
            vCertificadoID = Value
        End Set
    End Property

    Public Property DataEmissao() As String
        Get
            Return vDataEmissao
        End Get
        Set(ByVal Value As String)
            vDataEmissao = Value
        End Set
    End Property

    Public Property IEA_min() As String
        Get
            Return vIEA_min
        End Get
        Set(ByVal Value As String)
            vIEA_min = Value
        End Set
    End Property

    Public Property IEA_max() As String
        Get
            Return vIEA_max
        End Get
        Set(ByVal Value As String)
            vIEA_max = Value
        End Set
    End Property

    Public Property IPA_min() As String
        Get
            Return vIPA_min
        End Get
        Set(ByVal Value As String)
            vIPA_min = Value
        End Set
    End Property

    Public Property IPA_max() As String
        Get
            Return vIPA_max
        End Get
        Set(ByVal Value As String)
            vIPA_max = Value
        End Set
    End Property

    Public Property IPD_min() As String
        Get
            Return vIPD_min
        End Get
        Set(ByVal Value As String)
            vIPD_min = Value
        End Set
    End Property

    Public Property IPD_max() As String
        Get
            Return vIPD_max
        End Get
        Set(ByVal Value As String)
            vIPD_max = Value
        End Set
    End Property

    Public Property MNA_min() As String
        Get
            Return vMNA_min
        End Get
        Set(ByVal Value As String)
            vMNA_min = Value
        End Set
    End Property

    Public Property MNA_max() As String
        Get
            Return vMNA_max
        End Get
        Set(ByVal Value As String)
            vMNA_max = Value
        End Set
    End Property

    Public Property Observacoes() As String
        Get
            Return vObservacoes
        End Get
        Set(ByVal Value As String)
            vObservacoes = Value
        End Set
    End Property



End Class







