<%@ Page Language="vb" AutoEventWireup="false" Codebehind="impressao.aspx.vb" Inherits="segw0071.certificado"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<!--<title>Impress�o de Certificado</title>-->
		<style type="text/css">#LabelsAzuis { FONT-SIZE: 1px; COLOR: #333399; FONT-FAMILY: Arial }
		</style>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/impressao.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body onload="top.loaded();">
		&nbsp;
		<TABLE style="WIDTH: 800px; HEIGHT: 1150px" cellSpacing="0" cellPadding="0" align="center"
			border="0">
			<TBODY>
				<TR id="imgPrint">
					<TD style="WIDTH: 682px; HEIGHT: 50px" align="right" colSpan="3">
						<!-- A style="CURSOR: hand"><IMG src="files_print/bnb_printer_apolice.gif" border="0" onclick="JavaScript:imprimir();"></A --><INPUT style="BORDER-RIGHT: #cccccc 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #cccccc 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: bold; FONT-SIZE: 10px; BORDER-LEFT: #cccccc 1px solid; COLOR: #ffffff; BORDER-BOTTOM: #cccccc 1px solid; FONT-FAMILY: Verdana; HEIGHT: 17px; BACKGROUND-COLOR: #003399"
							onclick="JavaScript:imprimir();" type="button" value="Imprimir" Width="165px">
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 682px">
						<TABLE style="WIDTH: 100%; HEIGHT: 58px" cellSpacing="0" cellPadding="1">
							<TBODY>
								<TR>
									<TD style="WIDTH: 193px" align="left"><IMG style="BORDER-RIGHT: white 4px solid; BORDER-TOP: white 3px solid" src="files_print/bnb_alianca_apolice.gif"
											border="0"></TD>
									<TD style="WIDTH: 704px" align="right"><FONT style="FONT-SIZE: 9pt" size="2"><FONT face="Arial, sans-serif"><FONT face="Arial, sans-serif">
													<P class="western" style="MARGIN-BOTTOM: 0cm" align="center"><FONT size="4"><FONT face="Verdana">Certificado 
																Individual de Seguro de Vida em Grupo</FONT>
															<BR>
														</FONT><FONT size="1">
															<BR>
														</FONT>
												</FONT>Processo Susep: VG � 10005463/99-80 </FONT></FONT></P></TD>
								</TR>
							</TBODY>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 100%; HEIGHT: 42px">
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 46px" cellSpacing="0" cellPadding="2">
							<TBODY>
								<TR>
									<TD class="0034td_label" style="WIDTH: 607px; HEIGHT: 36px">&nbsp;<FONT face="Arial" color="#333399" size="1">N�mero 
											do Certificado Individual</FONT></TD>
									<TD class="0034td_label" style="WIDTH: 540px; HEIGHT: 16px"><FONT face="Arial" color="#333399" size="1">In�cio 
											de Vig�ncia do Segurado</FONT></TD> <!-- In�cio 
											de Vig�ncia da Ap�lice --> </FONT>
									<TD class="0034td_label" style="WIDTH: 463px; HEIGHT: 16px"><FONT face="Arial" color="#333399" size="1">Fim&nbsp;de 
											Vig�ncia do Segurado</FONT></TD> <!-- In�cio 
											de Vig�ncia da Ap�lice --> </FONT></TD>
				</TR>
				<TR>
					<TD class="0034td_dado" style="WIDTH: 607px; HEIGHT: 36px">&nbsp;
						<asp:label id="lblNumeroCertificado" Width="272px" CssClass="DataLabels" runat="server"></asp:label></TD>
					<TD class="0034td_dado" style="WIDTH: 400px; HEIGHT: 16px">
						<asp:label id="lblIni_vg_sbg" Width="272px" CssClass="DataLabels" runat="server"></asp:label></TD>
					</TD>
					<TD class="0034td_dado" style="WIDTH: 400px; HEIGHT: 16px">
						<asp:label id="lblFim_vg_sbg" Width="272px" CssClass="DataLabels" runat="server"></asp:label></TD>
					</TD>
				</TR>
			</TBODY>
		</TABLE>
		<TABLE style="WIDTH: 100%; HEIGHT: 144px" cellSpacing="0" cellPadding="1" border="0">
			<TBODY>
				<TR>
					<TD class="0034td_label" style="WIDTH: 192px; HEIGHT: 16px"><FONT face="Arial" color="#333399" size="1">&nbsp;Ramo</FONT></TD>
					<TD class="0034td_label" style="WIDTH: 600px; HEIGHT: 16px">&nbsp;<FONT face="Arial" color="#333399" size="1">N�mero 
							da Ap�lice</FONT></TD>
					<TD class="0034td_label" style="HEIGHT: 16px" colSpan="2"><FONT face="Arial" color="#333399" size="1">In�cio 
							de Vig�ncia da Ap�lice<!-- In�cio 
											de Vig�ncia da Ap�lice --> </FONT>
					</TD>
					<TD class="0034td_label" style="HEIGHT: 16px" width="380"><FONT face="Arial" color="#333399" size="1">Fim&nbsp;de 
							Vig�ncia da Ap�lice</FONT>
					</TD>
				</TR>
				<TR>
					<TD class="0034td_dado" style="WIDTH: 192px">&nbsp;
						<asp:label id="lblRamo" Width="152px" CssClass="DataLabels" runat="server"></asp:label></TD>
					<TD class="0034td_dado" style="WIDTH: 600px">&nbsp;
						<asp:label id="lblNumApolice" Width="175px" CssClass="DataLabels" runat="server"></asp:label></TD>
					<TD class="0034td_dado" style="WIDTH: 163px" colSpan="2"><asp:label id="lblIni_vg_apo" Width="200px" CssClass="DataLabels" runat="server" Visible="False"></asp:label></TD>
					<TD class="0034td_dado" style="WIDTH: 163px"><asp:label id="lblFim_vg_apo" Width="152px" CssClass="DataLabels" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD class="0034td_dado" style="WIDTH: 192px"><FONT face="Arial" size="1"></FONT></TD>
					<TD class="0034td_dado" style="WIDTH: 380px" colSpan="2"></TD>
					<TD class="0034td_dado" style="WIDTH: 380px"></TD>
					<TD class="0034td_dado" width="700"></TD>
				</TR>
				<TR>
					<TD class="0034td_label" style="WIDTH: 500px" colSpan="4"><FONT face="Arial" color="#333399" size="1">Nome 
							do Estipulante</FONT>
					</TD>
					<TD class="0034td_label" width="100"><FONT face="Arial" color="#333399" size="1">CNPJ&nbsp;</FONT>
					</TD>
				</TR>
				<TR>
					<TD class="0034td_dado" style="WIDTH: 800px" colSpan="4">
						<P align="left"><asp:label id="lblNomeEstipulante" Width="360px" CssClass="DataLabels" runat="server"></asp:label></P>
					</TD>
					<TD class="0034td_dado" style="WIDTH: 263px">
						<P align="left"><asp:label id="lblCNPJe" Width="152px" CssClass="DataLabels" runat="server"></asp:label></P>
					</TD>
				</TR>
				<TR>
					<TD class="0034td_label" style="WIDTH: 858px" colSpan="4">
						<P align="left"><FONT face="Arial" color="#333399" size="1">Nome do Sub-Estipulante</FONT></P>
					</TD>
					<TD class="0034td_label" style="WIDTH: 263px"><FONT face="Arial" color="#333399" size="1">CNPJ</FONT>
					</TD>
				</TR>
				<TR>
					<TD class="0034td_dado" style="WIDTH: 858px" colSpan="4">
						<P align="left"><asp:label id="lblNomeSubEstipulante" Width="360px" CssClass="DataLabels" runat="server"></asp:label></P>
					</TD>
					<TD class="0034td_dado" style="WIDTH: 263px">
						<P align="left"><asp:label id="lblCNPJsub_e" Width="152px" CssClass="DataLabels" runat="server"></asp:label></P>
					</TD>
				</TR>
			</TBODY>
		</TABLE>
		</TD></TR>
		<TR>
			<TD style="WIDTH: 100%">
				<TABLE style="WIDTH: 100%; HEIGHT: 64px" cellSpacing="0" cellPadding="1" border="0">
					<TBODY>
						<TR>
							<TD class="0034td_label" style="WIDTH: 903px; HEIGHT: 12px" colSpan="2"><FONT face="Arial" size="1"><FONT face="Arial" color="#333399" size="1">Dados 
										do Segurado</FONT> </FONT>
							</TD>
						</TR>
						<TR>
							<TD class="0034td_dado" style="WIDTH: 903px" vAlign="top"><FONT face="Arial" color="#333399" size="1">Nome:</FONT>
								<asp:label id="lblSeg_Nome" Width="336px" CssClass="DataLabels" runat="server"></asp:label><BR>
								</FONT><FONT face="Arial" color="#333399" size="1"><BR>
									Data de Nascimento:</FONT>
								<asp:label id="lblSeg_DataNasc" Width="152px" CssClass="DataLabels" runat="server"></asp:label></TD>
							<TD class="0034td_dado" vAlign="top" width="970">
								<P align="left"><FONT face="Arial" color="#333399" size="1">CPF:</FONT><asp:label id="lblSeg_cpf" Width="196px" CssClass="DataLabels" runat="server"></asp:label></P>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
				<TABLE style="WIDTH: 100%; HEIGHT: 34px" cellSpacing="0" cellPadding="2">
					<TBODY>
						<!--TR>
									<TD class="0034espaco">&nbsp;</TD>
								</TR-->
						<TR>
							<TD class="0034td_label" style="WIDTH: 903px"><FONT face="Arial" color="#333399" size="1">Dados 
									do Seguro</FONT></TD>
						</TR>
						<TR>
							<TD class="0034td_dado" style="WIDTH: 903px" width="903"><asp:label id="lblCobertura1" Width="152px" CssClass="DataLabels" runat="server"></asp:label><asp:panel id="pnlCoberturaTitular" Width="500px" CssClass="DataLabels" runat="server" BorderStyle="none">
									<TABLE id="tblSeguros" style="WIDTH: 568px; HEIGHT: 52px" runat="server">
										<TR class="0034td_dado">
											<TD colSpan="3">Titular</TD>
										</TR>
										<TR class="0034td_dado">
											<TD style="WIDTH: 387px">Nome Cobertura</TD>
											<TD width="15%">Capital Individual</TD>
										</TR>
									</TABLE>
								</asp:panel><asp:panel id="pnlCoberturaConjuge" Width="500px" CssClass="DataLabels" runat="server" BorderStyle="none">
									<TABLE id="tblSegurosConjuge" style="WIDTH: 568px; HEIGHT: 52px" runat="server">
										<TR class="0034td_dado">
											<TD colSpan="2">C�njuge</TD>
										</TR>
										<TR class="0034td_dado">
											<TD>Nome Cobertura</TD>
											<TD width="15%"></TD>
										</TR>
									</TABLE>
								</asp:panel><asp:panel id="pnlCoberturaFilho" Width="500px" CssClass="DataLabels" runat="server" BorderStyle="none">
									<TABLE id="tblSegurosFilho" style="WIDTH: 568px; HEIGHT: 52px" runat="server">
										<TR class="0034td_dado">
											<TD colSpan="2">Filho</TD>
										</TR>
										<TR class="0034td_dado">
											<TD>Nome Cobertura</TD>
											<TD width="15%"></TD>
										</TR>
									</TABLE>
								</asp:panel><br>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
		</TR>
		<TR>
			<TD style="WIDTH: 100%">
				<TABLE style="WIDTH: 100%; HEIGHT: 38px" cellSpacing="0" cellPadding="2">
					<TBODY>
						<TR>
							<TD class="0034td_label" style="WIDTH: 903px; HEIGHT: 19px"><FONT face="Arial" color="#333399" size="1">Pr�mio 
									Individual: R$</FONT></TD>
							<TD class="0034td_label" style="WIDTH: 680px; HEIGHT: 19px"><FONT face="Arial" color="#333399" size="1">&nbsp;Data 
									de Emiss�o</FONT></TD>
						</TR>
						<TR>
							<TD class="0034td_dado"><asp:label id="lblPremio" Width="192px" CssClass="DataLabels" runat="server"></asp:label></TD>
							<TD class="0034td_dado"><FONT face="Arial" size="1">&nbsp;</FONT>
								<asp:label id="lblDataEmissao" Width="152px" CssClass="DataLabels" runat="server"></asp:label></TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
		</TR>
		<TR>
			<TD style="WIDTH: 100%; HEIGHT: 73px">
				<TABLE style="WIDTH: 100%; HEIGHT: 37px" cellSpacing="0" cellPadding="2">
					<TBODY>
						<TR>
							<TD class="0034td_label" style="WIDTH: 655px; HEIGHT: 19px"><FONT face="Arial" color="#333399" size="1">Corretor</FONT></TD>
							<TD class="0034td_label" style="WIDTH: 1045px; HEIGHT: 19px"><FONT face="Arial" color="#333399" size="1">C�digo 
									SUSEP</FONT></TD>
						</TR>
						<TR>
							<TD class="0034td_dado"><asp:label id="lblCorretor" Width="384px" CssClass="DataLabels" runat="server"></asp:label></TD>
							<TD class="0034td_dado"><asp:label id="lblSUSEP" Width="152px" CssClass="DataLabels" runat="server"></asp:label></TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
		</TR>
		<TR>
			<TD style="WIDTH: 100%; HEIGHT: 127px">
				<TABLE style="WIDTH: 100%; HEIGHT: 126px" cellSpacing="0" cellPadding="2">
					<TBODY>
						<TR>
							<TD class="0034td_label" style="WIDTH: 848px; HEIGHT: 17px"><FONT face="Arial" color="#333399" size="1"><BR>
									Observa��es<BR>
								</FONT>
							</TD>
							<TD class="0034td_label" style="HEIGHT: 17px"><FONT face="Arial" color="#333399" size="1">Compahia 
									de Seguros Alian�a do Brasil</FONT></TD>
						</TR>
						<TR>
							<TD class="0034td_dado" style="WIDTH: 848px"><asp:label id="lblOBS" Width="152px" CssClass="DataLabels" runat="server"></asp:label><BR>
							</TD>
							<TD class="0034td_dado" style="WIDTH: 640px; HEIGHT: 18px"><IMG src="files_print/assinatura.gif"></TD>
						</TR>
			</TD>
		</TR>
		</TBODY></TABLE></TD></TR>
		<TR>
			<TD style="WIDTH: 682px">
				<TABLE style="WIDTH: 790px; HEIGHT: 301px" cellSpacing="0" cellPadding="2" border="0">
					<TBODY>
						<TR>
							<TD>
								<P class="western" style="MARGIN-BOTTOM: 0cm" align="left"><FONT style="FONT-SIZE: 8pt" color="#333399" size="1"><B>
											<center>Resumo das Condi��es Gerais</center>
										</B></FONT><FONT face="Arial, sans-serif"><FONT color="#333399" size="1">1. O 
											presente seguro � regido pelas Condi��es Gerais e Especiais que fazem parte 
											integrante da ap�lice em poder do Estipulante/Sub-Estipulante.<BR>
										</FONT></FONT><FONT face="Arial, sans-serif"><FONT color="#333399" size="1">2. O 
											presente Certificado Individual substitui e anula os anteriores e foi emitido 
											conforme as Condi��es Gerais e Particulares da ap�lice.<BR>
										</FONT></FONT><FONT face="Arial, sans-serif"><FONT color="#333399" size="1">3. O 
											seguro a que se refere este Certificado Individual ser� renovado conforme 
											disposto nas Condi��es Gerais e Particulares da ap�lice.<BR>
										</FONT></FONT><FONT face="Arial, sans-serif"><FONT color="#333399" size="1">4. 
											Todas as comunica��es relativas ao seguro, inclusive altera��es e cancelamento, 
											ser�o feitas diretamente ao Estipulante/Sub-Estipulante, como representante </FONT>
									</FONT><FONT color="#333399"><FONT face="Arial, sans-serif" size="1">legal do Segurado, 
											conforme autoriza��o deste expressa na respectiva Proposta de Ades�o.<BR>
										</FONT></FONT><FONT face="Arial, sans-serif"><FONT color="#333399" size="1">5. A 
											vig�ncia da cobertura individual ter� in�cio a partir das 24 (vinte e quatro) 
											horas da data de ingresso do Segurado no grupo, atrav�s de formaliza��o do </FONT>
									</FONT><FONT color="#333399"><FONT face="Arial, sans-serif" size="1">Estipulante, desde 
											que recebida e aceita a Proposta de Ades�o pela Seguradora. No caso de 
											propostas recepcionadas com o pagamento do pr�mio total ou </FONT></FONT>
									<FONT color="#333399"><FONT face="Arial, sans-serif" size="1">parcial, o in�cio da 
											cobertura ocorrer� �s 24 (vinte e quatro) horas da data de recep��o das 
											Propostas de Ades�o pela sociedade Seguradora.<BR>
										</FONT></FONT><FONT face="Arial, sans-serif"><FONT color="#333399" size="1">6. A 
											Proposta de Ades�o, preenchida de pr�prio punho pelo Segurado, datada e 
											assinada, dever� obrigatoriamente ser entregue � Seguradora para an�lise de </FONT>
									</FONT><FONT color="#333399"><FONT face="Arial, sans-serif" size="1">aceita��o em at� 
											45 dias da inclus�o do Segurado pelo Estipulante.<BR>
										</FONT></FONT><FONT color="#333399"><FONT face="Arial, sans-serif" size="1">7. O 
											final de vig�ncia da cobertura individual ser� a data de t�rmino de vig�ncia da 
											ap�lice, respeitando o disposto no item 10 deste Certificado Individual.<BR>
										</FONT></FONT><FONT color="#333399"><FONT face="Arial, sans-serif" size="1">8. Os 
											benefici�rios do seguro s�o aqueles indicados pelo Segurado na proposta de 
											ades�o no momento da contrata��o, podendo ser alterados mediante solicita��o </FONT>
									</FONT><FONT color="#333399"><FONT face="Arial, sans-serif" size="1">por escrito do 
											segurado, ou na falta de indica��o conforme o artigo 792 do C�digo Civil 
											Brasileiro de 11/01/2002 (metade ao c�njuge n�o separado judicialmente e </FONT>
									</FONT><FONT color="#333399"><FONT face="Arial, sans-serif" size="1">o restante aos 
											herdeiros do segurado, obedecida a ordem de voca��o heredit�ria).
											<BR>
										</FONT></FONT><FONT face="Arial, sans-serif"><FONT color="#333399" size="1">9. O 
											pr�mio e os capitais segurados ser�o reajustados conforme estabelecido nas 
											Condi��es Gerais da ap�lice.<BR>
										</FONT></FONT><FONT color="#333399"><FONT face="Arial, sans-serif" size="1">10. O 
											seguro representado por este Certificado Individual cessar� automaticamente:<BR>
										</FONT></FONT><FONT color="#333399"><FONT face="Arial, sans-serif" size="1">- Com a 
											n�o renova��o ou o cancelamento da ap�lice;<BR>
										</FONT></FONT><FONT color="#333399"><FONT face="Arial, sans-serif" size="1">- Com o 
											desaparecimento do v�nculo entre o Segurado e Estipulante ou Sub-estipulantes, 
											com exce��o dos segurados aposentados que tenham optado por </FONT></FONT>
									<FONT color="#333399"><FONT face="Arial, sans-serif" size="1">permanecer no referido 
											seguro;<BR>
										</FONT></FONT><FONT color="#333399"><FONT face="Arial, sans-serif" size="1">- 
											Quando o Segurado solicitar sua exclus�o da ap�lice ou quando o mesmo deixar de 
											contribuir com sua parte do pr�mio; e
											<BR>
										</FONT></FONT><FONT color="#333399"><FONT face="Arial, sans-serif" size="1">- 
											Conforme demais defini��es estabelecidas nas Condi��es Gerais da ap�lice.<BR>
											<BR>
										</FONT></FONT><FONT color="#333399"><FONT face="Arial, sans-serif" size="1">11. 
											Este seguro � por tempo determinado, tendo a seguradora a faculdade de n�o 
											renovar a ap�lice na data de vencimento, sem devolu��o dos pr�mios pagos nos </FONT>
									</FONT><FONT color="#333399"><FONT face="Arial, sans-serif" size="1">termos da ap�lice.</FONT></FONT>
								<P></P>
								<P style="MARGIN-BOTTOM: 0cm" align="center"><FONT color="#333399"><FONT face="Arial" size="1"><B>Companhia 
												de Seguros Alian�a do Brasil - Rua Manuel da N�brega, 1280 - 9� andar - 
												04001-004 - S�o Paulo - SP - C.N.P.J.: 28.196.889/0001-43</B></FONT></FONT></P>
								<P style="MARGIN-BOTTOM: 0cm" align="center"><FONT color="#333399"><FONT style="FONT-SIZE: 5pt" face="Arial"><B><FONT size="1">Central 
													de Atendimento 0800 729 7000 - </FONT><A href="http://www.aliancadobrasil.com.br">
													<FONT size="1">www.aliancadobrasil.com.br</FONT></A></B></FONT></FONT></P>
							</TD>
						</TR>
						</FORM></TBODY></TABLE>
			</TD>
		</TR>
		</TBODY></TABLE>

		<SCRIPT>
	function imprimir()
	{
		document.getElementById("imgPrint").style.display = 'none';
		print();
		document.getElementById("imgPrint").style.display = 'block';
	}
		</SCRIPT>
	</body>
</HTML>
