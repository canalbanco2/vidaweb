<%@ Register TagPrefix="uc1" TagName="paginacaogrid" Src="paginacaogrid.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ImprimirCertificados.aspx.vb"
    Inherits="segw0071.ImprimirCertificado" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>ConsultarFaturas</title>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
    <link href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">

    <script src="scripts/overlib421/overlib.js" type="text/javascript"></script>
    <style>
    .headerFixDiv {
	FONT-WEIGHT: bold; WIDTH: 100%; COLOR: #ffffff; POSITION: absolute; TOP: 2px; BACKGROUND-COLOR: #003399
}
.tdStyle {
	FONT-SIZE: 9px; COLOR: #000000; FONT-FAMILY: Verdana; BACKGROUND-COLOR: #ffffff
}
.headerFake {
	DISPLAY: none; POSITION: absolute; TOP: 2px
}
.headerFixDiv2 {
	VISIBILITY: hidden
}
.headerFakeDiv {
	VISIBILITY: hidden; LINE-HEIGHT: 0
}

input.btnInvisivel 
{
    border-width:0 !important;
 background-color:Transparent !important;
}
</style>

    <script language="javascript">
		<!--
			function getPesquisaLoad(valor) {				
												
				if(valor == "CPF" || valor == "Nome") {
					document.getElementById("divCampoPesquisa").style.display = "block";
				} else {
					document.getElementById("divCampoPesquisa").style.display = "none";
				}				
			}
			function getPesquisa(valor) {				
				
				document.Form1.campoPesquisa.value = "";
				
				if(valor == "CPF" || valor == "Nome") {
					document.getElementById("divCampoPesquisa").style.display = "block";
				} else {
					document.getElementById("divCampoPesquisa").style.display = "none";
				}				
			}
			
			function VerificaSelecaoPesquisa()
			{
				if(document.getElementById("rTipoNomeCPF").checked)
				{
					document.getElementById("tblTipoNomeCPF").style.display = "block";
                    document.getElementById("Table4").style.display = "none";
				}
				else if (document.getElementById("rFatura").checked)
				{
					document.getElementById("tblTipoNomeCPF").style.display = "none";
                    document.getElementById("Table4").style.display = "block";
				}
				else
				{
                    document.getElementById("Table4").style.display = "none";
					document.getElementById("tblTipoNomeCPF").style.display = "none";
				}
			}
			function exibeDependentes(id,src) {
					
				if (src.src.indexOf('imgMenos') != -1)
					src.src = '../segw0060/Images/imgMais.gif';
				else
					src.src = '../segw0060/Images/imgMenos.gif';
				
				for(i=0;i<=5;i++) {					
					var tr = eval("document.getElementById('grdPesquisa_"+id+"_"+i+"')");
					if (tr == null)
						break;
					if (tr.style.display == 'none') 
						tr.style.display = '';
					else
						tr.style.display = 'none';
				}		
			}	
		
		function abre_janela(link,nome,resizable,scrollbars,width,height,top,left)
		{  
			nova=window.open(link,nome,	"resizable="+resizable+",scrollbars="+scrollbars+",width="+width+",height="+height+",top="+top+",left="+left+",toolbar=0,location=0,directories=0,status=0,menubar=0");
		}
		
		function teste(valor)
		{
		  alert(valor);
		  
		}
		
		function abreCertificado() {
			todos ='<%=SESSION("clientes")%>'
			todosPaginaAtual = '<%=Session("marcarPaginaAtual")%>' 
		
			var checked;
			var cliente_id;
			var totalVidas;
			checked = 0 
			totalVidas = 0
			var clientes = "";
			
			for(var i=0; i<Form1.elements.length; i++) {
			
				if(Form1.elements[i].type == 'checkbox') { 
				//alert(Form1.elements[i].value);
					if (Form1.elements[i].checked == true) {
						checked++;
						var cliente_id = Form1.elements[i].value;
						clientes += "," + Form1.elements[i].value;
						totalVidas++;
					}
				}
			}			
			
			
			
			//if (checked == 10) {
			//	var nr_fatura;
			//	var dt_ini_fatura;
				
			//	nr_fatura = document.getElementById("lblFatura").innerText;
				 
				
			//	try {						
			//			dt_ini_fatura = document.getElementById("perCompetencia").value;
			//			
			//	} catch(ex) {						
			//			dt_ini_fatura = window.location.search.split('&')[2];
			//			dt_ini_fatura = dt_ini_fatura.substring(12, 26);							
			//			dt_ini_fatura = dt_ini_fatura.split('/')[2] + dt_ini_fatura.split('/')[1] + dt_ini_fatura.split('/')[0]							
			//			document.Form1.clientes_id.value=clientes.substring(1);	
						
						
						
			//		    dia_fatura = dt_ini_fatura.substr(3, 2);
			//		    mes_fatura = dt_ini_fatura.substr(8, 2);
			//		    ano_fatura = dt_ini_fatura.substr(13, 4);
			//		    dt_ini_fatura = ano_fatura + mes_fatura + dia_fatura			
			//	}		
				
			//		abre_janela('loader.aspx?alvo=Impressao.aspx&tipo=certificado&nr_fatura=' + nr_fatura + '&dt_ini_fatura=' + dt_ini_fatura + '&cliente_id=' + cliente_id,
			//		'janela_msg','no','yes','820','700','','');	
					
			//}
			//else
				//if (checked == 0 && todos!=1 && todosPaginaAtual!=1)
				
				if (todos==0)
					alert('Por favor selecione um Segurado');
					
				else
				{
					//if (checked > 0 || todos==1 || todosPaginaAtual==1) {
				  		
				  		var nr_fatura;
						var dt_ini_fatura;
						var dia_fatura;
						var mes_fatura;
						var ano_fatura;
						var tam_str_fatura;
						var dt_ini_fatura2;
									
						nr_fatura = document.getElementById("lblFatura").innerText;
                        
                        //-----------------'
                        // INC000004424278 '
                        //-----------------'
						try 
						{
							dt_ini_fatura = document.getElementById("perCompetencia").value;
							
						    if (dt_ini_fatura == '')
						    {
					            dt_ini_fatura = document.Form1.dt_inicio_fatura.value;
					            
					            dt_ini_fatura = dt_ini_fatura.substring(0, 10);
					            
					            dia_fatura = dt_ini_fatura.substr(0, 2);
				                mes_fatura = dt_ini_fatura.substr(3, 2);
				                ano_fatura = dt_ini_fatura.substr(6, 4);
				                
				                dt_ini_fatura = ano_fatura + mes_fatura + dia_fatura;
					            
					            document.Form1.clientes_id.value=clientes.substring(1);							
						    }
						} 
						catch(ex) 
						{
					        dt_ini_fatura = document.Form1.dt_inicio_fatura.value;
					        dt_ini_fatura = dt_ini_fatura.substring(0, 10);
					        dia_fatura = dt_ini_fatura.substr(0, 2);
				            mes_fatura = dt_ini_fatura.substr(3, 2);
				            ano_fatura = dt_ini_fatura.substr(6, 4);
				            dt_ini_fatura = ano_fatura + mes_fatura + dia_fatura;
					        document.Form1.clientes_id.value=clientes.substring(1);							
						}							
                        //-----------------'
                        // INC000004424278 '
                        //-----------------'
                        
						abre_janela('blank.aspx','janela_msg','no','yes','820','700','','');
							
						//document.Form1.action = 'multi_prints.aspx?tipo=certificado&nr_fatura=' + nr_fatura + '&dt_ini_fatura=' + dt_ini_fatura + '&totalVidas=' + totalVidas;
						var acao = document.Form1.action;
						var tar = document.Form1.target;
						
						//document.write('loader.aspx?alvo=multi_prints.aspx&tipo=certificado&nr_fatura=' + nr_fatura + '&dt_ini_fatura=' + dt_ini_fatura + '&totalVidas=' + totalVidas + '&clientes_id=' + clientes);
						
						//'-------------------Brauner----------------------------
						//Altera��o: Marcar e imprimir todos
		                //Brauner Rangel
		                //09/10/2013
		                
		                
						//if (todos==1){ 
						
						totalVidas ='<%=Session("totalVidasImpressao")%>'   
						clientes= '<%=Session("clientes")%>'
						
//						//}
//						//if(todosPaginaAtual==1){
//						totalVidas ='<%=Session("totalVidasPaginaAtual")%>'   
//						clientes= '<%=Session("idsPaginaAtual")%>'
//								//				}
						
						//alert(nr_fatura);
						//alert(dt_ini_fatura);
						//alert(totalVidas);
						//alert(clientes);
						
						contador = '<%=SESSION("linhasGridFaturas")%>';
					       for(var i = 0; i <= contador; i++){						
				            if(document.getElementById("chkLista" + i)) {
					            document.getElementById("chkLista" + i).checked = false;										
					            document.getElementById("chckYes" + i).style.display = "none"; 
					            document.getElementById("chckNo" + i).style.display = "block"; 
				            }	
						}
						
						document.Form1.action = 'loader.aspx?alvo=multi_prints.aspx&tipo=certificado&nr_fatura=' + nr_fatura + '&dt_ini_fatura=' + dt_ini_fatura + '&totalVidas=' + totalVidas + '&clientes_id=' + clientes;
						
						//'-------------------------------------------------------
						
						document.Form1.target = 'janela_msg';						
						
						document.Form1.submit();
						
						
						document.Form1.action = acao;
						document.Form1.target = tar;
					}
		}
				
				
		function alteracoes(valor, tipo, nome, idTitular, alvo, operacao) {}
		
		
		
		//__________________________________________________________________________
		//Altera��o: Marcar e imprimir todos
		//Brauner Rangel
		//09/10/2013
//		
		function MarcaDesmarcaTodos(){	

			
	   contador = '<%=SESSION("linhasGridFaturas")%>';

       todos ='<%=SESSION("todosmarcados")%>'

	  
			
			var marcados = document.getElementById("marcados").value;
			
			
			if (todos == 0){
		//alert(marcados);
			    for(var i = 0; i <= contador; i++){						
					    if(document.getElementById("chkLista" + i)) {
						    document.getElementById("chkLista" + i).checked = true;			
						    document.getElementById("chckNo" + i).style.display = "none"; 
						    document.getElementById("chckYes" + i).style.display = "block"; 
					    }														
				    }
				    document.Form1.marcs.value = '1';
		    }else{
		        
		        for(var i = 0; i <= contador; i++){						
				    if(document.getElementById("chkLista" + i)) {
					    document.getElementById("chkLista" + i).checked = false;										
					    document.getElementById("chckYes" + i).style.display = "none"; 
					    document.getElementById("chckNo" + i).style.display = "block"; 
				    }														
			    }
			    document.Form1.marcs.value = '0';
	        }
			
		}
		
		
		//________________________________________________________________________________________________________________
		
		
		function MarcaDesmarca(paginaAtual){	
		    
		    //alert(document.getElementById("marcados").value);		    
		    /*if (document.getElementById("marcados").value == 0 || document.getElementById("marcados").value == ""){
		        document.getElementById("marcados").value = 1;
		    }else{
		        document.getElementById("marcados").value = 0;
		    }*/		    
			
		    var contador = 0;
		    
			if(paginaAtual){
			    contador = '<%=SESSION("linhasGridFaturas")%>';
			}else{
			    contador = '<%=SESSION("numeroSegurados")%>';	
			    
			    /*
			    var checked;			 
			    checked = 0 
			    
			    for(var i=0; i<Form1.elements.length; i++) {
				    if(Form1.elements[i].type == 'checkbox') { 
					    Form1.elements[i].checked == true;
						checked++;						   					    
				    }
			    }		
			    
			    alert(checked);
			    */
			}			
			//alert(contador);
			var marcados = document.getElementById("marcados").value;
			
			if (document.getElementById("marcados").value == '0'){
			   //alert(marcados);
			    for(var i = 0; i <= contador; i++){						
					    if(document.getElementById("chkLista" + i)) {
						    document.getElementById("chkLista" + i).checked = true;			
						    document.getElementById("chckNo" + i).style.display = "none"; 
						    document.getElementById("chckYes" + i).style.display = "block"; 
					    }														
				    }
				    alert(todos)
				    document.Form1.marcs.value = '1';
		    }else{
		        //alert(marcados);
		        for(var i = 0; i <= contador; i++){						
				    if(document.getElementById("chkLista" + i)) {
					    document.getElementById("chkLista" + i).checked = false;										
					    document.getElementById("chckYes" + i).style.display = "none"; 
					    document.getElementById("chckNo" + i).style.display = "block"; 
				    }														
			    }
			    document.Form1.marcs.value = '0';
	        }
	        
	    }

		function validaMouseOut(src,color) {
				src.style.background=color;			
					
				teste = 0;
			}
			function mO(cor, alvo) {												
				if(alvo.style.background != '#f8ef00')
					alvo.style.background=cor;							
			}
				
		function validaData(valor, tipo) {
				
		valor = valor.toString();
		
		var mes = parseInt(valor.substr(0, 2));
		var ano = parseInt(valor.substr(3, 6));	
		
				
		if (valor.substr(0, 2) == '08')
			mes = 8
			
		if (valor.substr(0, 2) == '09')
			mes = 8
			
		if (mes == '00')
		{
		return false;
		}
			
		//verificac�o b�sica
		if(mes > 12 || ano > 2050 || ano < 1800 || mes < 0) {
			return false;
		}
			
		return true;		
	}
	
	function validaCampos()
	{
	var dt_de = document.Form1.txtDe.value;
	var dt_ate = document.Form1.txtAte.value;
					
				if(dt_de == "") {	
					alert('Informe o in�cio do per�odo de compet�ncia.');
					return false;
				}
					
				if(dt_ate == "") {	
					alert('Informe o fim do per�odo de compet�ncia.');
					return false;
				}
				
				if(!validaData(dt_de,2)) {		
					alert('A data informada � inv�lida.');
					top.escondeaguarde();
					return false;
				}
				else
				{
					if(!validaData(dt_ate,2)) {		
						alert('A data informada � inv�lida');
						top.escondeaguarde();
						return false;
					}
					else
					{
						return true;
					}
				}
				
				
				
	}
	function changeChck(id,id2) {
		
	    var display = document.getElementById("chckYes" + id).style.display;
        
	
		//if(document.getElementById("chkLista" + id) && document.getElementById("chkLista" + id).checked) {
		if(display == "" ) {
			document.getElementById("chkLista" + id).checked = false;						
			document.getElementById("chckYes" + id).style.display = "none"; 
			document.getElementById("chckNo" + id).style.display = "block"; 
			document.getElementById('<%= labelTipoOperacao.ClientID %>').value="adiciona";
		//	alert("teste");
		} else {
			document.getElementById("chkLista" + id).checked = true;	
			document.getElementById("chckNo" + id).style.display = "none"; 
			document.getElementById("chckYes" + id).style.display = "block"; 	
			document.getElementById('<%= labelTipoOperacao.ClientID %>').value="remove";	
			//alert("teste2");
		}
		
		document.getElementById('<%= labelId.ClientID %>').value= id2;
		
		document.getElementById('<%= btnInvisivel.ClientID %>').click();
		
	}
	
	function VerificaPreenchNomeCpf()
	{
		if (document.getElementById("txtTipoNome").value == "" && document.getElementById("txtTipoCPF").value == "")
		{
			alert("Preencha o(s) campo(s) Nome e/ou CPF.");
			document.getElementById("txtTipoNome").setfocus;
			return false;
		}
		top.exibeaguarde();
		return true;
	}
	
	function FormataCPF(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres){
		var wTecla, wVr, wTam;
		 
			//alert(pForm);
		  
		wTecla = pTeclaPres.keyCode;
		wVr = pForm[pCampo].value;
		
		wTam = wVr.length ;	
		ult	= wVr.substring(wTam-1,wTam);
		vl = ult.charCodeAt(0);
		//trava caracteres n�o num�ricos
		if (vl < 48 || vl > 57){
			wVr = wVr.replace( ult.valueOf() , "" );
			pForm[pCampo].value = wVr;
		}
		
		wVr = wVr.toString().replace( "-", "" );		
		wVr = wVr.toString().replace( "-", "" );
		wVr = wVr.toString().replace( "-", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( "/", "" );
		wTam = wVr.length ;			

		if(wTam > pTamMax) {
			wTam = pTamMax;
			wVr = wVr.substr(0, pTamMax)
		}
			
		if (wTam < pTamMax && wTecla != 8) { 
			wTam = wVr.length + 1 ; 
		}

		if (wTecla == 8 ) { 
			wTam = wTam - 1 ; 
		}
		   
		if ( wTecla == 8 || wTecla == 88 || wTecla >= 48 && wTecla <= 57 || wTecla >= 96 && wTecla <= 105 ){
		if ( wTam <= 2 ){
			pForm[pCampo].value = wVr ;
		}
		if (wTam > pPosTraco && wTam <= pTamMax) {
				wVr = wVr.substr(0, wTam - pPosTraco) + '-' + wVr.substr(wTam - pPosTraco, wTam);
		}
		if ( wTam == pTamMax){
				wVr = wVr.substr( 0, wTam - pPos1 ) + '.' + wVr.substr(wTam - pPos1, 3) + '.' + wVr.substr(wTam - pPos2, wTam);
		}
		pForm[pCampo].value = wVr;
		 
		}

		}
		
		function verificaComTry(){
			try{			
				VerificaSelecaoPesquisa();
			}			
			catch(ex){
				//Nothing	
			}
			
		}
	
	
	-->
    </script>

</head>
<body onload="verificaComTry();setWidths();">
    <div id="overDiv" style="z-index: 1000; visibility: hidden; position: absolute">
    </div>
    <form id="Form1" onsubmit="return validaCampos()" method="post" runat="server">
        <!-- 
            '-----------------'
            ' INC000004424278 '
            '-----------------'        
        -->
        <input id='perCompetencia' name='perCompetencia' runat="server" type='hidden' value=''>
        <!-- 
            '-----------------'
            ' INC000004424278 '
            '-----------------'        
        -->
        <input id="showTree" type="hidden" name="showTree">
        <input id="clientes_id" type="hidden" name="clientes_id">
        <p>
            <asp:Label ID="lblNavegacao" runat="server" CssClass="Caminhotela">Label</asp:Label><br>
            <asp:Label ID="lblVigencia" runat="server" CssClass="Caminhotela"><br>Per�odo de compet�ncia: 01/12/2006 at� 31/12/2006</asp:Label>
        <p>
            <br>
            <!-- Adicionado radio 16/04/07-->
            <!------------------------------>
            <asp:Panel ID="pnlTipoPesquisa" runat="server">
                <p>
                    <asp:RadioButton ID="rTipoNomeCPF" runat="server" CssClass="Caminhotela" GroupName="Pesquisa"
                        Text="Nome ou CPF"></asp:RadioButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:RadioButton ID="rFatura" runat="server" CssClass="Caminhotela" GroupName="Pesquisa"
                        Text="Fatura"></asp:RadioButton><br>
                    <br>
                    <br>
                    <br>
                </p>
            </asp:Panel>
            <table id="tblTipoNomeCPF" style="display: none" cellspacing="4" cellpadding="0">
                <tr>
                    <td>
                        <asp:Label ID="lblTipoNome" runat="server" CssClass="Caminhotela">Nome</asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtTipoNome" runat="server"></asp:TextBox>&nbsp;
                        <asp:Image ID="Image1" onmouseover="return overlib('Digitar o nome completo.');"
                            onmouseout="return nd();" runat="server" ImageUrl="Images/help.gif"></asp:Image></td>
                    <td valign="middle" rowspan="2">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblTipoCPF" runat="server" CssClass="Caminhotela">CPF</asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtTipoCPF" onkeyup="FormataCPF(document.getElementById('Form1'),'txtTipoCPF',11,8,5,2,event);"
                            runat="server"></asp:TextBox>&nbsp;
                        <asp:Image ID="Image3" onmouseover="return overlib('Digitar o CPF sem pontua��o e d�gito.');"
                            onmouseout="return nd();" runat="server" ImageUrl="Images/help.gif"></asp:Image></td>
                    <td valign="middle">
                        &nbsp;<asp:Button ID="btnTipoNomeCPF" runat="server" CssClass="Botao" Text="Pesquisar"
                            Width="80px"></asp:Button></td>
                </tr>
            </table>
            <table id="Table4" style="display: none; border-collapse: collapse; text-align: center"
                bordercolor="#cccccc" cellspacing="0" cellpadding="2" width="100%" border="0">
                <tr>
                    <td align="left" width="237">
                        <asp:Label ID="Label6" runat="server" CssClass="Caminhotela">
							Informe o per�odo de compet�ncia:</asp:Label></td>
                    <td align="left">
                        <asp:TextBox ID="txtDe" runat="server" Width="56px" MaxLength="7">06/2006</asp:TextBox>&nbsp;
                        <asp:Label ID="Label7" runat="server" CssClass="Caminhotela">at�</asp:Label>&nbsp;
                        <asp:TextBox ID="txtAte" runat="server" Width="56px" MaxLength="7">06/2007</asp:TextBox>&nbsp;
                        <asp:Image ID="Image2" onmouseover="return overlib('Digitar o m�s e ano que deseja consultar, com barra (mm/aaaa) at� (mm/aaaa).');"
                            onmouseout="return nd();" runat="server" ImageUrl="Images/help.gif"></asp:Image>&nbsp;&nbsp;<asp:Button
                                ID="btnPesquisarPeriodo" runat="server" CssClass="Botao" Text="Pesquisar" Width="80px">
                            </asp:Button>&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
            </table>
            <br>
            <asp:Panel ID="pnlFaturas" runat="server" HorizontalAlign="Justify" Visible="False">
                <asp:Label ID="lblTituloFatura" CssClass="Caminhotela" runat="server">Selecione a fatura a consultar</asp:Label>
                <asp:DataGrid ID="grdFaturas" runat="server" Width="100%" AutoGenerateColumns="False"
                    BorderColor="#999999" BorderStyle="None" BorderWidth="1px" BackColor="White"
                    CellPadding="3" GridLines="Vertical" AllowPaging="True" PageSize="6">
                    <FooterStyle Wrap="False"></FooterStyle>
                    <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
                    <EditItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></EditItemStyle>
                    <AlternatingItemStyle HorizontalAlign="Center" VerticalAlign="Middle" BackColor="WhiteSmoke">
                    </AlternatingItemStyle>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                    <HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="titulo-tabela sem-sublinhado">
                    </HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="fatura_id" HeaderText="N&#250;mero Fatura">
                            <HeaderStyle Wrap="False"></HeaderStyle>
                            <ItemStyle Wrap="False"></ItemStyle>
                            <FooterStyle Wrap="False"></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="competencia" HeaderText="Compet&#234;ncia da Fatura">
                            <HeaderStyle Wrap="False"></HeaderStyle>
                            <ItemStyle Wrap="False"></ItemStyle>
                            <FooterStyle Wrap="False"></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="dt_recebimento" HeaderText="Vencimento">
                            <HeaderStyle Wrap="False"></HeaderStyle>
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="dt_baixa" HeaderText="Dt. do Pagamento">
                            <HeaderStyle Wrap="False"></HeaderStyle>
                            <ItemStyle Wrap="False"></ItemStyle>
                            <FooterStyle Wrap="False"></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_situacao" HeaderText="Situa&#231;&#227;o">
                            <HeaderStyle Wrap="False"></HeaderStyle>
                            <ItemStyle Wrap="False"></ItemStyle>
                            <FooterStyle Wrap="False"></FooterStyle>
                        </asp:BoundColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Center" CssClass="fonte-branca" Wrap="False" Mode="NumericPages">
                    </PagerStyle>
                </asp:DataGrid>
                <br>
                <asp:Label ID="lblInfo" runat="server" CssClass="Caminhotela">N�o foi encontrada nenhuma fatura nesse per�odo.</asp:Label>
            </asp:Panel>
            <p>
                <asp:Panel ID="pnlFiltro" runat="server" Width="100%" Visible="False" BorderColor="Gainsboro"
                    BorderWidth="1px">
                    <table id="Table1" height="88" cellspacing="1" cellpadding="1" width="600" border="0">
                        <tr>
                            <td align="right" width="5%">
                                <p>
                                    <br>
                                    <asp:Label ID="Label13" runat="server" CssClass="Caminhotela">Opera��o:</asp:Label></p>
                            </td>
                            <td width="5%">
                                <br>
                            </td>
                            <td align="right" width="5%">
                                <asp:Label ID="Label11" runat="server" CssClass="Caminhotela">Nome:</asp:Label></td>
                            <td width="5%">
                                <asp:TextBox ID="txtNome" runat="server" Width="173px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td width="5%">
                            </td>
                            <td width="5%">
                            </td>
                            <td align="right" width="5%">
                                <asp:Label ID="Label12" runat="server" CssClass="Caminhotela">CPF:</asp:Label></td>
                            <td width="5%">
                                <asp:TextBox ID="txtCPf" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="center" width="5%" colspan="4">
                                <p>
                                    <br>
                                    <asp:Button ID="btnFiltro" runat="server" CssClass="Botao" Text="Pesquisar" Width="75px">
                                    </asp:Button><br>
                                    <br>
                                </p>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </p>
        <asp:Panel ID="pnlDetalhesFatura" runat="server" Width="100%" Visible="False" Height="250px">
            <table id="Table5" height="22" cellspacing="1" cellpadding="1" width="100%" border="0">
                <tr>
                    <td nowrap height="14">
                        <table id="TableCab" height="22" cellspacing="1" cellpadding="1" width="100%" border="0">
                            <tr>
                                <td width="200">
                                    <asp:Label ID="Label14" runat="server" CssClass="Caminhotela">Listas da vida da fatura</asp:Label>
                                    <asp:Label ID="lblFatura" runat="server" CssClass="Caminhotela"></asp:Label>
                                    <asp:Label ID="Label17" runat="server" CssClass="Caminhotela"></asp:Label></td>
                                <td align="left" width="220">
                                    <asp:Label ID="Label1" runat="server" CssClass="Caminhotela">Pesquisado por:</asp:Label>&nbsp;
                                    <asp:Label ID="lblPesquisado" runat="server" CssClass="Caminhotela"></asp:Label></td>
                                <td>
                                    <asp:DropDownList ID="ddlFiltroOperacao" runat="server" Width="88px">
                                    </asp:DropDownList></td>
                                <td width="120">
                                    <div id="divCampoPesquisa" style="display: none">
                                        <input id="campoPesquisa" type="text" name="campoPesquisa" value="<%=Request("campoPesquisa")%>"
                                            onkeyup="formataCampo(document.getElementById('Form1'),'campoPesquisa',11,8,5,2,event)"
                                            maxlength="14"></div>
                                    
                                </td>
                                <td>
                                    <asp:Button ID="btnFiltrar" runat="server" CssClass="Botao" Text="Pesquisar" OnClick="desmarcaTodos"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    </td>
                    <td nowrap align="right" height="14">
                    </td>
                </tr>
            </table>
            
            <div id="divGridMovimentacao" style="overflow: auto; width: 735px; height: 298px">
            
                <div style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px; overflow: hidden;
                    padding-top: 0px">
                    <table id="gridMovimentacao" style="border-collapse: collapse" bordercolor="#dddddd" cellspacing="0" cellpadding="0" border="1" runat="server">
                        <tr class="paddingzero-sembordalateral">
                            <td>
                                <div style="width: 100%">
                                    <table style="border-collapse: collapse" cellspacing="0" cellpadding="0" border="1"
                                        cssclass="escondeTituloGridTable">
                                                         <tr>
                                            <td id="tdHeader0">
                                                <div class="headerFixDiv2" id="header0">
                                                </div>
                                                <div class="headerFixDiv">
                                                </div>
                                            </td>
                                            <td id="tdHeader1" wrap="false">
                                                <div class="headerFixDiv2" id="header1">
                                                </div>
                                                <div class="headerFixDiv">
                                                    Segurado</div>
                                            </td>
                                            <td id="tdHeader2" wrap="false">
                                                <div class="headerFixDiv2" id="header2">
                                                </div>
                                                <div class="headerFixDiv">
                                                    Tipo</div>
                                            </td>
                                            <td id="tdHeader3" wrap="false">
                                                <div class="headerFixDiv2" id="header3">
                                                </div>
                                                <div class="headerFixDiv">
                                                    CPF</div>
                                            </td>
                                            <td id="tdHeader4" wrap="false">
                                                <div class="headerFixDiv2" id="header4">
                                                </div>
                                                <div class="headerFixDiv">
                                                    Nascimento</div>
                                            </td>
                                            <td id="tdHeader5" wrap="false">
                                                <div class="headerFixDiv2" id="header5">
                                                </div>
                                                <div class="headerFixDiv">
                                                    Sexo</div>
                                            </td>
                                            <td id="tdHeader6" wrap="false">
                                                <div class="headerFixDiv2" id="header6">
                                                </div>
                                                <div class="headerFixDiv">
                                                    In�cio Vig�ncia</div>
                                            </td>
                                            <td id="tdHeader7" wrap="false">
                                                <div class="headerFixDiv2" id="header7">
                                                </div>
                                                <div class="headerFixDiv">
                                                    Sal�rio (R$)</div>
                                            </td>
                                            <td id="tdHeader8" wrap="false">
                                                <div class="headerFixDiv2" id="header8">
                                                </div>
                                                <div class="headerFixDiv">
                                                    Capital (R$)</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="9">
                                                <div id="divScrollGrid" style="overflow: auto; height: 262px">
                                                    <!--- In�cio da grid de Usu�rios -->
                                                    <asp:DataGrid ID="grdPesquisa" runat="server" CssClass="escondeTituloGridTable" AutoGenerateColumns="false" 
                                                        BorderColor="#DDDDDD" CellPadding="0" AllowPaging="true" PageSize="20" CellSpacing="0"
                                                        ShowHeader="false" ShowFooter="false">
                                                        
                                                        <SelectedItemStyle CssClass="ponteiro"></SelectedItemStyle>
                                                        <AlternatingItemStyle CssClass="ponteiro"></AlternatingItemStyle>
                                                        <ItemStyle CssClass="ponteiro"></ItemStyle>
                                                        <FooterStyle CssClass="0019GridFooter"></FooterStyle>
                                                        
                                                        <Columns>
                                                            <asp:BoundColumn DataField="prop_cliente_id" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="tdStyle"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Left" headertext="Segurado" DataField="nome" ItemStyle-CssClass="tdStyle"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Left" headertext="Tipo" DataField="TP_COMPONENTE_ID" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="tdStyle"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Left" headertext="CPF" DataField="cpf" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="tdStyle"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Left" headertext="Nascimento" DataField="dt_nascimento" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-CssClass="tdStyle"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Left" headertext="Sexo" DataField="sexo" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="tdStyle"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Left" headertext="In�cio vig�ncia" DataField="DT_INI_VIG_SEG" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="tdStyle"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Left" headertext="Sal�rio R$" DataField="salario_seg" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="tdStyle"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Left" headertext="Capital R$" DataField="capital_seg" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="tdStyle"></asp:BoundColumn>
                                                            <asp:TemplateColumn >
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTeste1" runat="server" Visible="false"  Text='<%# Bind("prop_cliente_id") %>'> ></asp:Label>
                                                                </ItemTemplate>                                                            
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                        <PagerStyle Visible="false"></PagerStyle>
                                                    </asp:DataGrid></div>
                                                    <input type="hidden" id="dt_inicio_fatura" name="dt_inicio_fatura" value="<%=Request("competencia")%>"/>
                                            </td>
                                        </tr>
                                   </table>
                                     <asp:HiddenField ID="labelId" runat="server"></asp:HiddenField> 
                                        <asp:HiddenField ID="labelTipoOperacao" runat="server"></asp:HiddenField>
                                     <asp:Button id="btnInvisivel"  OnClick="btnInvisivel_Click" runat="server" text="" CssClass="btnInvisivel" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <center>
                    <asp:Label ID="lblSemUsuario" CssClass="Caminhotela" runat="server" Visible="False"></asp:Label></center>
            </div>
            <div>
            </div>
            <uc1:paginacaogrid ID="ucPaginacao" runat="server"></uc1:paginacaogrid>
            <p align="center">
                <div id="div_botoes" align="center">
                    <asp:Button class="botao" id="btnMarcarDesmarcar" runat="server" OnClientClick="MarcaDesmarca(true)" OnClick="marcaDesmarcaPaginaAtual" Text="Marcar/Desmarcar todos desta p�gina"/>
                    <asp:Button id="btnImprimir" class="Botao" OnClientClick="abreCertificado();" OnClick="imprimeTodos" Text="Imprimir/Consultar" width="165px"  runat="server" />
                   <%-- <input class="botao" id="btnMarcarDesmarcarTodos" onclick="MarcaDesmarca(false)" type="button"
                        value="Marcar/Desmarcar Todos">     --%>
                  <asp:Button id="btnMarcarDesmarcarTodos" runat="server" Text="Marcar/Desmarcar Todos" class ="botao" OnClientClick="MarcaDesmarcaTodos()" OnClick="marcaDesmarcaTodos"/>
            </p>
            
            <br>
            <input id="alvo" type="hidden" name="alvo"><input id="alvoTemp" type="hidden" value="0"
                name="alvo"><input id="nomeAlvo" type="hidden" name="nomeAlvo">
            <input id="marcados" type="hidden" name="marcs" value=0>
            <input id="idTitular" type="hidden" name="idTitular">
        </asp:Panel>
        <br>
         
    </form>

    <script>
    			try { 	
				for(i=0; i<=8; i++) {					
					if(document.getElementById("grdPesquisa__0_cell_" + i)) {
					
						if(document.getElementById("grdPesquisa__0_cell_" + i).style.display == "none") {
							document.getElementById("tdHeader" + i).style.display = "none";
						}
					}					
				}
				
				for(i=0; i<=8; i++) {					
							
					if(i == 8 && document.getElementById("grdPesquisa__0_cell_8").style.display != "none" ) {
						add = "OO";
					} else if(i == 7 && document.getElementById("grdPesquisa__0_cell_7").style.display != "none" && document.getElementById("grdPesquisa__0_cell_7").style.display == "none") {
						add = "OO";
					} else {
						add = "";
					} 
										
					document.getElementById("header" + i).innerHTML = document.getElementById("txtMax" + i).value + add;
					document.getElementById("headerFake" + i).innerHTML = document.getElementById("txtMax" + i).value;												
					
				}																							
			} catch(e) {  }
				
			try {
				getPesquisaLoad(document.Form1.ddlFiltroOperacao.value);
			} catch(e) { }
			
			
			top.escondeaguarde();
			
			function hideAndSeekHandler(op,w){
			dv = document.getElementById("divGridMovimentacao");			
			pag = document.getElementById("ucPaginacao_lblPaginacao");
			//alert(pag);
			if (op == 'esconder'){
				dv.style.width = w ;								
				pag.style.width = dv.clientWidth-150;
			}
			else{
				dv.style.width = "730px";	
				pag.style.width = "100%";			
			}
		}
		
		function setWidths(){
			top.iniciarMenu();
		}					
			
    </script>

    <div id="div_oculta_botoes" runat="server">
    </div>
    
</body>
</html>
