Partial Class certificado
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub




    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Dim linkseguro As Alianca.Seguranca.Web.LinkSeguro

        'Implementa��o do LinkSeguro
        'linkseguro = New Alianca.Seguranca.Web.LinkSeguro
        'Dim usuario As String = linkseguro.LerUsuario("", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))
        'If usuario = "" Then
        'Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
        'End If

        'Session("usuario_id") = linkseguro.Usuario_ID
        'Session("usuario") = linkseguro.Login_REDE
        'Session("cpf") = linkseguro.CPF

        'Implementa��o do controle de ambiente
        Dim cAmbiente As Alianca.Seguranca.Web.ControleAmbiente
        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & Request.ServerVariables("URL") & "/"
        cAmbiente.ObterAmbiente(url)

        'configura��o da end com o banco de dados
        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
        Else
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
        End If

        Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)


        If Not Page.IsPostBack Then

            For Each m As String In Request.QueryString.Keys
                If m <> "" Then
                    Session(m) = Request.QueryString(m)
                End If
            Next

            '****************************************************
            'Inicia processo de busca de dados: Etapas
            '****************************************************

            PreencheRamoApolice()

            Carrega_Dados_do_Subgrupo()

            getdataApolice()

            mPreencheDadosBasicosApolice()

            BuscaDadosSubEstipulante()

            BuscaDadosCliente()

            BuscaDadosCorretor()

            BuscaPremio()

            getCoberturas(1) '1 Titular

            'Inicio-Autor: Rodrigo.Moura-Confitec Data:16/05/2011
            'Esta altera��o foi necess�ria porque TpComponente para filhos � 6 e n�o 2 como estava
            'getCoberturas(2)
            getCoberturas(6) '6 Filhos
            'Fim-Autor: Rodrigo.Moura-Confitec Data:16/05/2011
            getCoberturas(3) '3 Conjuge
            BuscaCertificadoID()

            buscaDataEmissao(Me.lblNumeroCertificado.Text)

            BuscaObservacoes()

        End If

        Try
            If Session("apolice") = "" Then
                cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
                Response.End()
            End If
        Catch ex as System.Exception
            cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
            Response.End()
        End Try


    End Sub

    Sub PreencheRamoApolice()
        Try
            Me.lblRamo.Text = Session("ramo")
            Me.lblNumApolice.Text = Session("apolice")
        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Public Sub Carrega_Dados_do_Subgrupo()

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        'Pega Data de in�cio e fim de vig�ncia do subgrupo

        bd.GetCapitalMaximo(Session("apolice"), Session("subgrupo_id"), Session("ramo"), 6785, 0, Session("usuario"), Session("acesso"), Session("cpf"))

        Try
            dr = bd.ExecutaSQL_DR()
            If dr.Read Then
                'Linha 2
                Me.lblIni_vg_sbg.Text = dr.GetValue(3).ToString
                Me.lblFim_vg_sbg.Text = dr.GetValue(4).ToString




            Else

                Me.lblIni_vg_sbg.Text = " --- "

                Me.lblFim_vg_sbg.Text = " --- "


            End If

        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        Finally
            dr.Close()
        End Try

    End Sub

    Sub BuscaDadosSubEstipulante()

        'Busca: Dados sub-estipulante
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dt_ini_fatura As String = Request.QueryString("dt_ini_fatura")
        Dim tipo_pessoa As Integer

        Try
            bd.BuscarDadosSubEstipulante(Session("apolice"), Session("ramo"), Session("subgrupo_id"), dt_ini_fatura)

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read() Then

                If Not IsDBNull(dr.Item("estipulante")) Then
                    Me.lblNomeSubEstipulante.Text = dr.Item("estipulante").ToString
                End If

                If Not IsDBNull(dr.Item("tp_pessoa")) Then
                    tipo_pessoa = dr.Item("tp_pessoa").ToString

                    If tipo_pessoa = "1" Then
                        If Not IsDBNull(dr.Item("cgc_estipulante")) Then
                            Me.lblCNPJsub_e.Text = dr.Item("cgc_estipulante").ToString
                        End If
                    Else
                        If Not IsDBNull(dr.Item("cpf_estipulante")) Then
                            Me.lblCNPJsub_e.Text = dr.Item("cpf_estipulante").ToString
                        End If
                    End If


                End If

            Else
                Me.lblNomeSubEstipulante.Text = "-------------------"
            End If

            dr.Close()
            dr = Nothing

        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Sub BuscaDadosCorretor()

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dt_ini_fatura As String = Request.QueryString("dt_ini_fatura")

        Try
            bd.BuscaDadosCorretor(Session("apolice"), Session("ramo"), Session("subgrupo_id"), dt_ini_fatura)

            'cUtilitarios.br(bd.SQL)

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read() Then

                If Not IsDBNull(dr.Item("corretor")) Then
                    Me.lblCorretor.Text = dr.Item("corretor").ToString
                End If

                If Not IsDBNull(dr.Item("corretor_susep")) Then
                    Me.lblSUSEP.Text = cUtilitarios.getCodSusep() 'dr.Item("corretor_susep").ToString
                End If


            End If

            dr.Close()
            dr = Nothing

        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        End Try


    End Sub

    Sub BuscaDadosCliente()

        'Busca: Dados dos cliente
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim cliente_id As String = Request.QueryString("cliente_id")

        Try
            bd.BuscarDadosCliente(cliente_id)

            'cUtilitarios.br(bd.SQL)

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read() Then

                If Not IsDBNull(dr.Item("nome")) Then
                    Me.lblSeg_Nome.Text = dr.Item("nome").ToString
                End If

                If Not IsDBNull(dr.Item("dt_nascimento")) Then
                    'Me.lblSeg_DataNasc.Text = Left(dr.Item("dt_nascimento").ToString, 10)
                    Me.lblSeg_DataNasc.Text = Format(dr.Item("dt_nascimento"), "dd/MM/yyyy".ToString).Substring(0, 10)
                End If

                If Not IsDBNull(dr.Item("cpf")) Then
                    Me.lblSeg_cpf.Text = cUtilitarios.trataCPF(dr.Item("cpf").ToString)
                End If

            End If

            dr.Close()
            dr = Nothing

        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        End Try


    End Sub

    Sub BuscaPremio()

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim nr_fatura As String = Request.QueryString("nr_fatura")
        Dim cliente_id As String = Request.QueryString("cliente_id")

        Try
            bd.BuscaPremio(Session("apolice"), Session("ramo"), nr_fatura, cliente_id)

            'cUtilitarios.br(bd.SQL)

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read() Then

                If Not IsDBNull(dr.Item("premio_bruto_seg")) Then
                    Me.lblPremio.Text = dr.Item("premio_bruto_seg").ToString
                End If

            End If

            dr.Close()
            dr = Nothing

        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        End Try


    End Sub

    Private Sub getdataApolice()
        Try

            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

            bd.getDataIniVigenciaApolice(Session("apolice"), Session("ramo"))

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read Then
                Me.lblIni_vg_apo.Text = IIf(dr.GetValue(0).ToString = "", "---", dr.GetValue(0).ToString)
                Me.lblFim_vg_apo.Text = IIf(dr.GetValue(1).ToString = "", "---", dr.GetValue(1).ToString)


            Else
                Me.lblIni_vg_apo.Text = "---"
                Me.lblFim_vg_apo.Text = "---"
            End If
            dr.Close()
            dr = Nothing

        Catch ex As System.Exception
            Dim excp As New clsException(ex)

        End Try

    End Sub

    Private Sub mPreencheDadosBasicosApolice()

        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        BD.dados_basico_apolice_sps(Session("apolice"), Session("ramo"), 6785, 0, "'" & Session("cpf") & "'")

        Try

            dr = BD.ExecutaSQL_DR()

            If dr.Read() Then

                Me.lblNomeEstipulante.Text = IIf(dr.GetValue(2).ToString = "", "---", dr.GetValue(2).ToString)
                Me.lblCNPJe.Text = IIf(dr.GetValue(1).ToString = "", "---", dr.GetValue(1).ToString)
            Else
                Me.lblNomeEstipulante.Text = "---"
                Me.lblCNPJe.Text = "---"
            End If

        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        Finally
            dr.Close()
            dr = Nothing
        End Try

    End Sub

    Private Sub getCoberturas(ByVal componente As String)

        Dim cliente_id As String = Request.QueryString("cliente_id")

        Try
            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

            bd.coberturasNew(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), componente)

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            While dr.Read
                Dim tr As New Web.UI.HtmlControls.HtmlTableRow
                tr.Attributes.Add("class", "0034td_dado")
                tr.Cells.Add(addCell(IIf(dr.Item("nome").ToString.Trim = "", " --- ", dr.Item("nome").ToString.Trim)))
                tr.Cells.Add(addCell(cUtilitarios.getCapByTpCobertura(cliente_id, componente, dr.Item("class_tp_cobertura"))))

                tr.Cells.Item(1).Align = "right"

                If componente = 1 Then
                    Me.tblSeguros.Rows.Add(tr)
                ElseIf componente = 3 Then
                    Me.tblSegurosConjuge.Rows.Add(tr)
                Else
                    Me.tblSegurosFilho.Rows.Add(tr)
                End If

            End While

            If Not dr.HasRows Then
                Dim tr As New Web.UI.HtmlControls.HtmlTableRow
                tr.Attributes.Add("class", "0034td_dado")

                tr.Cells.Add(addCell(" --- "))
                'tr.Cells.Add(addCell("  "))
                tr.Cells.Add(addCell(" --- "))

                tr.Cells.Item(0).Align = "left"
                tr.Cells.Item(1).Align = "center"

                If componente = 1 Then
                    Me.tblSeguros.Rows.Add(tr)
                ElseIf componente = 3 Then
                    Me.tblSegurosConjuge.Rows.Add(tr)
                Else
                    Me.tblSegurosFilho.Rows.Add(tr)
                End If
            End If

            dr.Close()
            dr = Nothing

        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Sub BuscaCertificadoID()
        Dim cliente_id As String = Request.QueryString("cliente_id")

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        Try
            bd.buscaCertificadoID(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cliente_id)

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read() Then

                If Not IsDBNull(dr.Item("cert_id")) Then
                    Me.lblNumeroCertificado.Text = dr.Item("cert_id").ToString
                End If
            Else
                Me.lblNumeroCertificado.Text = "---"
            End If

            dr.Close()
            dr = Nothing

        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Sub buscaDataEmissao(ByVal certificado_id As String)

        If certificado_id = "---" Then
            Me.lblDataEmissao.Text = "---"
            Exit Sub
        End If

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        Try
            bd.buscaDataEmissao(Session("apolice"), Session("ramo"), Integer.Parse(certificado_id))

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read() Then

                If Not IsDBNull(dr.Item("dt_Emissao")) Then
                    Me.lblDataEmissao.Text = dr.Item("dt_Emissao").ToString
                End If
            Else
                Me.lblDataEmissao.Text = "---"
            End If

            dr.Close()
            dr = Nothing

        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Sub BuscaObservacoes()

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        Try
            bd.buscaObservacoes(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read() Then

                If Not IsDBNull(dr.Item("sub_grupo_id")) Then
                    'Me.lblOBS.Text = dr.Item("sub_grupo_id").ToString & " - "
                End If

                If Not IsDBNull(dr.Item("nome")) Then
                    'Me.lblOBS.Text &= dr.Item("nome").ToString
                End If

            Else
                Me.lblOBS.Text = "---"
            End If

            dr.Close()
            dr = Nothing

        Catch ex As System.Exception
            Response.Write(ex.ToString)
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Private Function addCell(ByVal valor As String) As Web.UI.HtmlControls.HtmlTableCell
        Dim td As New Web.UI.HtmlControls.HtmlTableCell
        td.Attributes.Add("class", "DataLabelsMini")
        td.InnerText = valor
        Return td
    End Function

End Class
