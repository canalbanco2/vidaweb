Partial Class ImprimirCertificado
    Inherits System.Web.UI.Page





#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnFatura As System.Web.UI.WebControls.Button
    Protected WithEvents btnRelacaoVidas As System.Web.UI.WebControls.Button
    Protected WithEvents btnResumo As System.Web.UI.WebControls.Button
    Protected WithEvents btnMarcarDesmarcar As System.Web.UI.WebControls.Button
    Protected WithEvents btnMarcarDesmarcarTodos As System.Web.UI.WebControls.Button

    Protected WithEvents VerificaData1 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents VerificaData2 As System.Web.UI.WebControls.RegularExpressionValidator

    Protected WithEvents ucPaginacao As paginacaogrid
    Protected WithEvents chkLista As System.Web.UI.WebControls.CheckBoxList
    'Protected WithEvents btnImprimir As System.Web.UI.WebControls.Button
    Protected WithEvents pnlPesquisa As System.Web.UI.WebControls.Panel
    Protected WithEvents chkNomeCpf As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkFatura As System.Web.UI.WebControls.CheckBox


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Property TipoCPF() As String
        Get
            If ViewState("TipoCPF") Is Nothing Then
                Return String.Empty
            Else
                Return ViewState("TipoCPF").ToString()
            End If
        End Get
        Set(ByVal Value As String)
            ViewState("TipoCPF") = Value
        End Set
    End Property
    Private Property TipoNome() As String
        Get
            If ViewState("TipoNome") Is Nothing Then
                Return String.Empty
            Else
                Return ViewState("TipoNome").ToString()
            End If
        End Get
        Set(ByVal Value As String)
            ViewState("TipoNome") = Value
        End Set
    End Property

    Public Structure Impressao
        Dim dt_ini_fatura As String
        Dim nr_fatura As Integer
        Dim cliente_id As Integer
        Dim ramo_id As Integer
        Dim apolice_id As Integer
        Dim subgrupo_id As Integer
        Dim codigo_susep As Integer
        Dim sucursal_seguradora_id As Integer

    End Structure
    Private Enum EnumTipoOperacao
        Todos
        Nome
        CPF
        NomeCPF
    End Enum

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim cAmbiente As Alianca.Seguranca.Web.ControleAmbiente
        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
        cAmbiente.ObterAmbiente(url)

        'cAmbiente.ObterAmbiente("http://intranet.aliancadobrasil.com.br/")

        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
1:          'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
        Else
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
        End If

        Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)
        'Session.Add("GLAMBIENTE_ID", 3)

        Try

            For Each m As String In Request.QueryString.Keys
                Session(m) = Request.QueryString(m)
            Next

        Catch ex As System.Exception
            Response.Write(ex.Message + ex.StackTrace)
            Response.End()
        End Try


        lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomeSubgrupo") & "<br>Fun��o: Imprimir certificado individual"
        lblVigencia.Visible = True
        lblVigencia.Text = "<br>" & getPeriodoCompetenciaSession()

        'Nova regra do per�odo da Fatura 26/04/07
		'Inicio:INC000004283469 Autor:Rodrigo Moura Data:04/06/2014
        Session.Remove("clientes")
			'Fim:INC000004283469 Autor:Rodrigo Moura Data:04/06/2014
        If Not Page.IsPostBack Then
            Me.getRegraFatura()
			
            rTipoNomeCPF.Checked = False
            rFatura.Checked = False

            If IsNothing(Session("marcados")) Then
                Dim marcados As New ArrayList
                Session("marcados") = marcados
            End If
        End If
        '-----------------------------

        Dim showTree As String = Request.Form("showTree")

        'Configura o filtro
        Dim filtraPesquisa As String = Request.QueryString("acao")

        Dim excluido As String = ""
        excluido = Request.QueryString("excluido")

        Dim visualizar As String = Request.QueryString("visualizar")

        If visualizar <> "" Then
            If ddlFiltroOperacao.Items.Count = 0 Then
                PopulaComboFiltroOperacao(EnumTipoOperacao.Todos)
            End If
        End If

        Me.configFiltro(filtraPesquisa)

        If visualizar <> "" Then
            Me.pnlFaturas.Visible = True
            grdFaturas.Visible = True
            pnlFiltro.Visible = True
            Me.txtNome.Visible = False
            Me.txtCPf.Visible = False
            Me.Label11.Visible = False
            Me.Label12.Visible = False

            txtDe.Text = IIf(Not IsNothing(Request.Form("txtDe")), Request.Form("txtDe"), Request.QueryString("txtDe"))
            txtAte.Text = IIf(Not IsNothing(Request.Form("txtAte")), Request.Form("txtAte"), Request.QueryString("txtAte"))
            lblFatura.Text = Request.QueryString("fatura")
            Session("fatura") = Request.QueryString("fatura")


            lblPesquisado.Text = ddlFiltroOperacao.SelectedItem.Text

            If Not IsPostBack Then

                grdFaturaDataBind(Request.QueryString("page"))
                grdFaturas.Visible = True
            End If

            'Me.pnlFaturas.Visible = False
            Me.pnlFiltro.Visible = False
            pnlTipoPesquisa.Visible = False
            Me.pnlDetalhesFatura.Visible = True

        End If

        Me.btnPesquisarPeriodo.Attributes.Add("onclick", "top.exibeaguarde();")
        Me.btnFiltro.Attributes.Add("onclick", "top.exibeaguarde();")
        Me.btnTipoNomeCPF.Attributes.Add("onclick", "return VerificaPreenchNomeCpf();")

        rTipoNomeCPF.Attributes.Add("onclick", "VerificaSelecaoPesquisa()")
        rFatura.Attributes.Add("onclick", "VerificaSelecaoPesquisa()")

        ddlFiltroOperacao.Attributes.Add("onChange", "getPesquisa(this.value)")

        btnFiltrar.EnableViewState = False
        btnFiltrar.CausesValidation = False

        Try
            'If Session("apolice") = "" Then
            If IsNothing(Session("apolice")) Then
                cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
                Response.End()
            End If
        Catch ex As System.Exception
            cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
            Response.End()
        End Try

    End Sub

    Public Sub marcaDesmarcaTodos(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Altera��o: Marcar e imprimir todos
        'Brauner Rangel
        '09/10/2013
        Dim vetor As New ArrayList
        Dim totalvidas As Integer
        totalvidas = Session("totalVidas")
        vetor = TryCast(Session("marcados"), ArrayList)

        If vetor.Count <> 0 Then
            If vetor.Count < totalvidas Then
                vetor.Clear()
                Session("marcados") = vetor
            End If
        End If

        If IsNothing(Session("todosmarcados")) Or Session("todosmarcados") = 0 Then
            Session("todosmarcados") = 1
        Else
            Session("todosmarcados") = 0
        End If


        ''If grdPesquisa.Items.Count > 0 Then
        ''    For Each x As Web.UI.WebControls.DataGridItem In grdPesquisa.Items
        ''        If x.Cells.Item(0).Text = "<div class='headerFakeDiv'id='headerFake0'></div><div id='chckNo0'><img src='images/checkboxNo.jpg' onclick='changeChck(0);return false;'></div><div id='chckYes0' style='display:none'><img src='images/checkboxYes.jpg' onclick='changeChck(0);return false;'></div><div style='display:none'><input type='checkbox' id='chkLista0' value='9848183' style='padding:0;'></div>" Then
        ''            Session("todosmarcados") = 1
        ''        Else
        ''            Session("todosmarcados") = 0
        ''        End If
        ''        Exit Sub
        ''    Next
        ''End If




    End Sub

    Public Sub imprimeTodos(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click

        Exit Sub

        If Session("todosMarcados") = 1 Then
            If Not IsNothing(Session("todosIds")) Then
                Dim somaVidas As Integer = Session("TotalVidas")
                Response.Redirect("loader.aspx?alvo=multi_prints.aspx&tipo=certificado&nr_fatura=' " & lblFatura.Text & "'&dt_ini_fatura='" & txtDe.Text & "'&totalVidas='" & somaVidas & "'&clientes_id='" & Session("todosIds") & "'")
            End If
        End If


    End Sub


    Private Sub PopulaComboFiltroOperacao(Optional ByVal filtro As EnumTipoOperacao = EnumTipoOperacao.Todos)


        ddlFiltroOperacao.Items.Clear()
        ddlFiltroOperacao.Items.Add(New ListItem("Todos", "Todos"))
        If filtro <> EnumTipoOperacao.CPF And filtro <> EnumTipoOperacao.NomeCPF Then ddlFiltroOperacao.Items.Add(New ListItem("CPF", "CPF"))
        If filtro <> EnumTipoOperacao.Nome And filtro <> EnumTipoOperacao.NomeCPF Then ddlFiltroOperacao.Items.Add(New ListItem("Nome", "Nome"))
        ddlFiltroOperacao.Items.Add(New ListItem("Inclus�es", "Inclusoes"))
        ddlFiltroOperacao.Items.Add(New ListItem("Altera��es", "Alteracoes"))
        ddlFiltroOperacao.Items.Add(New ListItem("Exclus�es", "Exclusoes"))
        ddlFiltroOperacao.Items.Add(New ListItem("Pendente", "Pendente"))
        ddlFiltroOperacao.Items.Add(New ListItem("Hist�rico", "Historico"))

    End Sub

    Private Function getTpAcesso() As String
        Dim ind_acesso As String = ""
        Select Case Session("acesso").ToString
            Case "1"
                ind_acesso = "Administrador da Alian�a do Brasil"
            Case "2"
                ind_acesso = "Administrador de Ap�lice"
            Case "3"
                ind_acesso = "Administrador de Subgrupo"
            Case "4"
                ind_acesso = "Operador"
        End Select

        Return ind_acesso
    End Function

    Private Function getPeriodoCompetenciaSession() As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)
        Dim data_inicio As String = ""
        Dim data_fim As String = ""
        Dim retorno As String = ""

        Try

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()


            If dr.HasRows Then
                While dr.Read()

                    data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                    data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)
                    cUtilitarios.escreveScript("var dt_ini_comp = '" & data_inicio & "';")
                    cUtilitarios.escreveScript("var dt_fim_comp = '" & data_fim & "';")

                End While

                retorno = "Per�odo de Compet�ncia: " & data_inicio & " a " & data_fim
            Else
                Session.Abandon()
                System.Web.HttpContext.Current.Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
                'cUtilitarios.br("N�o existe nenhuma Compet�ncia de Fatura em aberto")
                'System.Web.HttpContext.Current.Response.Write("<script>parent.window.location=parent.window.location;</script>")
            End If
            dr.Close()
            dr = Nothing
        Catch ex As System.Exception
            Dim excp As New clsException(ex)
            retorno = ""
        End Try
        Return retorno
    End Function

    Private Sub getRegraFatura()

        Dim data_inicio_comp, data_fim_comp As String
        Dim data_de, data_ate As DateTime

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

        Try

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.HasRows Then
                While dr.Read()

                    data_inicio_comp = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                    data_fim_comp = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)

                    data_ate = DateTime.Parse(data_inicio_comp).AddDays(-1).ToString("dd/MM/yyyy")
                    Me.txtAte.Text = data_ate.AddMonths(-12).ToString("dd/MM/yyyy").Substring(3)

                    data_de = DateTime.Parse(Me.txtAte.Text).AddMonths(-12)
                    Me.txtDe.Text = data_de.ToString("dd/MM/yyyy").Substring(3)

                End While

            Else
                Session.Abandon()
                'cUtilitarios.br("N�o existe nenhuma Compet�ncia de Fatura em aberto")
                System.Web.HttpContext.Current.Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
                'System.Web.HttpContext.Current.Response.Write("<script>parent.window.location=parent.window.location;</script>")
            End If

            dr.Close()
            dr = Nothing
        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Sub btnFechaPagina_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("Centro.aspx")
    End Sub

    Private Sub btnImprimeCertificado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Write("<script>alert('Imprimindo os certificados dos titulares selecionados.')</script>")
    End Sub

    Private Sub btnPesquisarPeriodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisarPeriodo.Click

        If dataIsValid(txtDe.Text) And dataIsValid(txtAte.Text) Then
            'Elimino os viewstate apenas por seguranca
            TipoCPF = String.Empty
            TipoNome = String.Empty

            pnlFaturas.Visible = True
            grdFaturaDataBind(0)
            pnlFiltro.Visible = False
            pnlDetalhesFatura.Visible = False
            pnlTipoPesquisa.Visible = False
        Else
            cUtilitarios.br("Favor verificar se a data inserida � v�lida e est� no formato mm/aaaa.")
            cUtilitarios.escreveScript("setTimeout('top.escondeaguarde()',500)")
        End If
    End Sub

    Public Function dataIsValid(ByVal data As String) As Boolean

        Try
            Convert.ToDateTime("01/" & data)
            Return True
        Catch ex As System.Exception
            Return False
        End Try

    End Function

    Private Sub grdFaturaDataBind(Optional ByVal page As Integer = 0)





        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dt_de As Date
        Dim dt_ate As Date

        dt_de = Convert.ToDateTime("01/" & txtDe.Text)

        dt_ate = Convert.ToDateTime("01/" & txtAte.Text)
        dt_ate = DateAdd(DateInterval.Month, 1, dt_ate)
        dt_ate = DateAdd(DateInterval.Day, -1, dt_ate)

        Dim dt As New DataTable

        Try
            bd.ConsultaFaturasPorPeriodoComp(Session("apolice"), Session("ramo"), Session("subgrupo_id"), dt_de.ToString("yyyyMMdd"), dt_ate.ToString("yyyyMMdd"))

            dt = bd.ExecutaSQL_DT()
            Session("dtFatura") = dt
            grdFaturas.DataSource = dt
            grdFaturas.DataBind()
            pnlDetalhesFatura.Visible = False

            'Impede que aparece o grid vazio
            If grdFaturas.Items.Count = 0 Then
                If Request.QueryString("visualizar") = "" Then
                    grdFaturas.Visible = False
                    lblTituloFatura.Visible = False
                    lblInfo.Visible = True
                End If
            Else
                grdFaturas.Visible = True
                pnlDetalhesFatura.Visible = False
                lblInfo.Visible = False
                lblTituloFatura.Visible = True
            End If
            '---------------------------

            If page < grdFaturas.PageCount And page > -1 Then
                grdFaturas.CurrentPageIndex = page
                grdFaturas.DataBind()
            End If

        Catch ex As System.Exception
            Response.Write(bd.SQL & vbNewLine)
            Response.Write(ex.Message & vbNewLine)
        End Try

    End Sub

    Private Sub ListarUltimaFatura()
        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.VIDA_WEB_DB)

        Dim dr As SqlClient.SqlDataReader
        Try

            bd.ConsultaUltimaFatura(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

            dr = bd.ExecutaSQL_DR


            If dr.HasRows Then
                dr.Read()
                lblFatura.Text = dr.GetValue(0).ToString()
                'A gera��o do certificado precisa da competenci

                Response.RedirectLocation = "?visualizar=&amp;fatura=" & lblFatura.Text & "&amp;competencia=" & dr.GetValue(2).ToString()

                'Response.Write("<input type='hidden' value='" & CType(dr.GetValue(2).ToString.Split(" a ")(0), DateTime).ToString("yyyyMMdd") & "' name='perCompetencia' id='perCompetencia'>")
                '-----------------'
                ' INC000004424278 '
                '-----------------'
                Me.perCompetencia.Value = CType(dr.GetValue(2).ToString.Split(" a ")(0), DateTime).ToString("yyyyMMdd")
                '-----------------'
                ' INC000004424278 '
                '-----------------'

                'e.Item.Attributes.Add("onclick", "location.href='?visualizar=1&amp;fatura=" & e.Item.Cells(0).Text & "&amp;competencia=" & e.Item.Cells(1).Text & "&amp;Page=" & grdFaturas.CurrentPageIndex & "&amp;" & txtDe.ID & "=" & txtDe.Text & "&amp;" & txtAte.ID & "=" & txtAte.Text & "'")

                dr.Close()

                If TipoCPF <> String.Empty And TipoNome <> String.Empty Then
                    PopulaComboFiltroOperacao(EnumTipoOperacao.NomeCPF)
                ElseIf TipoCPF <> String.Empty Then
                    PopulaComboFiltroOperacao(EnumTipoOperacao.CPF)
                Else
                    PopulaComboFiltroOperacao(EnumTipoOperacao.Nome)
                End If

                pnlTipoPesquisa.Visible = False

                mConsultaUsuarios("Todos", lblFatura.Text)

                pnlDetalhesFatura.Visible = True
            Else
                lblTituloFatura.Visible = False
                lblInfo.Visible = True
                'ucPaginacao.Visible = False
                'grdPesquisa.Visible = False
                'gridMovimentacao.Visible = False
            End If


        Catch ex As System.Exception
            Response.Write(bd.SQL & vbNewLine)
            Response.Write(ex.Message & vbNewLine)
        End Try




    End Sub
    Private Sub grdFaturas_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grdFaturas.PageIndexChanged
        grdFaturaDataBind(e.NewPageIndex)
    End Sub

    Private Sub grdFaturas_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdFaturas.ItemDataBound


        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            If e.Item.ItemType = ListItemType.Item Then
                e.Item.Attributes.Add("onmouseout", "this.style.background='#ffffff'")
            Else
                e.Item.Attributes.Add("onmouseout", "this.style.background='#f5f5f5'")
            End If

            e.Item.Attributes.Add("onclick", "location.href='?visualizar=1&amp;fatura=" & e.Item.Cells(0).Text & "&amp;competencia=" & e.Item.Cells(1).Text & "&amp;Page=" & grdFaturas.CurrentPageIndex & "&amp;" & txtDe.ID & "=" & txtDe.Text & "&amp;" & txtAte.ID & "=" & txtAte.Text & "'")
            e.Item.Attributes.Add("onmouseover", "this.style.background='#E8F1FF'")

            e.Item.Attributes.Add("Style", "CURSOR: pointer")

        End If

    End Sub

    Private Sub grdFaturas_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdFaturas.ItemCreated
        If e.Item.ItemType = ListItemType.Pager Then
            Dim cl As New TableCell
            cl.Text = "Total&nbsp;de&nbsp;Registros:&nbsp;" & Session("dtFatura").Rows.Count.ToString
            cl.HorizontalAlign = HorizontalAlign.Right
            cl.BorderWidth = Unit.Pixel(0)
            cl.ColumnSpan = 2
            e.Item.Cells(0).ColumnSpan -= 2
            e.Item.Cells(0).BorderWidth = Unit.Pixel(0)
            e.Item.Cells.Add(cl)
        End If
    End Sub

    Private Sub btnFiltro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFiltro.Click

        Me.pnlFaturas.Visible = False
        Me.pnlFiltro.Visible = False

        mConsultaUsuarios(Me.ddlFiltroOperacao.SelectedValue, lblFatura.Text)
        Me.pnlDetalhesFatura.Visible = True

    End Sub

    Private Sub mConsultaUsuarios(ByVal tipo As String, ByVal fatura As String)




        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim filtro As String = ""

        If tipo = "Todos" Then
            tipo = "null"
        Else
            tipo = tipo.Substring(0, 1)
        End If


        If tipo = "N" Or tipo = "C" Then
            Dim content As String = Request("campoPesquisa")
            content = "'%" & content & "%'"
            bd.SEGS5740_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), fatura, tipo, content)
        Else
            bd.SEGS5740_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), fatura, tipo)
        End If

        If TipoCPF <> String.Empty And TipoNome <> String.Empty Then
            filtro = "Nome like '%" & TipoNome & "%' AND CPF like '%" & TipoCPF & "%'"
        ElseIf TipoCPF <> String.Empty Then
            filtro = "CPF like '%" & TipoCPF & "%'"
        ElseIf TipoNome <> String.Empty Then
            filtro = "Nome like '%" & TipoNome & "%'"
        End If

        Try
            Dim dt As Data.DataTable = bd.ExecutaSQL_DT

            'Adaptacao tecnica para filtrar o resultado do datatable
            'evitando alteracao na procedure e no metodo abaixo
            If filtro <> String.Empty Then
                'Foi necessario copiar o dt pois o metodo select retorna a instancia por referencia
                'entao, quando eu apagava o datatable a referencia anterior era apagada
                Dim dtFiltrado As DataTable = dt.Copy()
                dt.Clear()
                Dim dr As DataRow() = dtFiltrado.Select(filtro)
                For Each linha As DataRow In dr
                    dt.ImportRow(linha)
                Next
            End If


            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                Session("numeroSegurados") = dt.Rows.Count

                If Session("indice") <= Me.grdPesquisa.PageCount Then
                    Me.grdPesquisa.CurrentPageIndex = Session("indice")
                Else
                    Me.grdPesquisa.CurrentPageIndex = 0
                    Session("indice") = 0
                End If

                Me.grdPesquisa.SelectedIndex = -1

                Me.ucPaginacao._valor = "2"

                Me.ucPaginacao.GridDataBind(Me.grdPesquisa, dt)
                Session("grdDescarte") = dt

                Me.lblSemUsuario.Visible = False
                ucPaginacao.Visible = True
                grdPesquisa.Visible = True
                gridMovimentacao.Visible = True

                div_oculta_botoes.InnerHtml = "<script>document.getElementById('div_botoes').style.display = 'block';</script>"


            Else
                Session.Remove("indice")
                Session.Remove("grdDescarte")
                Me.lblSemUsuario.Text = "<br><br><br>Nenhuma Vida encontrada.<br><br>"
                Me.lblSemUsuario.Visible = True
                ucPaginacao.Visible = False
                grdPesquisa.Visible = False
                gridMovimentacao.Visible = False

                div_oculta_botoes.InnerHtml = "<script>document.getElementById('div_botoes').style.display = 'none';</script>"


            End If


        Catch ex As System.Exception
            Throw New System.Exception(ex.Message + ex.StackTrace)

        End Try


    End Sub


    '''Configura o filtro de pesquisa
    Private Sub configFiltro(ByVal filtraPesquisa As String)

        If filtraPesquisa = "incl" Or filtraPesquisa = "excl" Or filtraPesquisa = "alter" Or filtraPesquisa = "acer" Or filtraPesquisa = "dps" Then
            Dim valor As String
            If filtraPesquisa = "incl" Then
                valor = "Inclus�es"
                ddlFiltroOperacao.SelectedValue = "Inclusoes"
            ElseIf filtraPesquisa = "excl" Then
                valor = "Exclus�es"
                ddlFiltroOperacao.SelectedValue = "Exclusoes"
            ElseIf filtraPesquisa = "alter" Then
                valor = "Altera��es"
                ddlFiltroOperacao.SelectedValue = "Alteracoes"
            ElseIf filtraPesquisa = "acer" Then
                valor = "Acertos"
                ddlFiltroOperacao.SelectedValue = "Acertos"
            Else
                valor = "DPS Pendente"
                ddlFiltroOperacao.SelectedValue = "DPS"
            End If
        End If

    End Sub

    Private Sub mGridDataBind(ByVal dt As DataTable)
        Me.ucPaginacao.GridDataBind(grdPesquisa, dt)
    End Sub

    Private Sub mSelecionaGrid(ByVal ItemIndex As Integer)
        Me.grdPesquisa.SelectedIndex = ItemIndex
        Me.mGridDataBind(Session("grdDescarte"))
        CType(Me.grdPesquisa.SelectedItem.Cells(0).Controls(1), RadioButton).Checked = True
    End Sub

    Private Sub mPaginacaoClick(ByVal Argumento As String) Handles ucPaginacao.ePaginaAlterada
        Me.grdPesquisa.SelectedIndex = -1
        Select Case Argumento
            Case "pro"
                Me.grdPesquisa.CurrentPageIndex += 1
            Case "ant"
                Me.grdPesquisa.CurrentPageIndex -= 1
            Case Else
                Me.grdPesquisa.CurrentPageIndex = Argumento
        End Select
        Session("indice") = Me.grdPesquisa.CurrentPageIndex
        Me.ucPaginacao.GridDataBind(grdPesquisa, Session("grdDescarte"))
    End Sub

    Private Sub btnMarcarDesmarcar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMarcarDesmarcar.Click
        ''INUTILizADO 17/09/2013
        If Session("checkboxMarcados") = 0 Then
            Session("checkboxMarcados") = 1
        Else
            Session("checkboxMarcados") = 0
        End If
    End Sub
    'Private Sub btnMarcarDesmarcarTodos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMarcarDesmarcarTodos.Click

    '    ''INUTILizADO 17/09/2013
    '    If Session("checkboxMarcados") = 0 Then
    '        Session("checkboxMarcados") = 1
    '    Else
    '        Session("checkboxMarcados") = 0
    '    End If
    'End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        If Me.pnlDetalhesFatura.Visible = True Then
            mConsultaUsuarios(ddlFiltroOperacao.SelectedValue, lblFatura.Text)
            Me.pnlFaturas.Visible = False
        End If
    End Sub
    Public Sub desmarcaTodos(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Altera��o: Marcar e imprimir todos
        'Brauner(Rangel)
        '09/10/2013

        If Session("todosmarcados") = 1 Then
            Session("todosmarcados") = 0
        End If

    End Sub

    Public Sub marcaDesmarcaPaginaAtual(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'Altera��o: Marcar e imprimir todos
        'Brauner(Rangel)
        '09/10/2013
        Dim vetor As New ArrayList
        Dim vetor2 As New ArrayList
        Dim totalvidas As Integer
        totalvidas = Session("totalVidas")
        vetor = TryCast(Session("marcados"), ArrayList)
        If IsNothing(TryCast(Session("idsPaginaAtual"), ArrayList)) Then
            vetor2.Add("")
        Else
            vetor2 = TryCast(Session("idsPaginaAtual"), ArrayList)
        End If


        Dim imprimiu As Integer = Session("imprimiu")
        Dim conferi As Integer = Session("marcarPaginaAtual")


        Session("marcados") = vetor

        'If vetor.Count <> 0 Then
        '    If Not vetor.Count <> vetor2.Count Then
        '        If vetor.Count <= totalvidas Or imprimiu = 0 Then
        '            vetor.Clear()
        '            Session("marcados") = vetor
        '        End If
        '    End If
        'End If
        Session("imprimiu") = 0
        Session("todosmarcados") = 0
        Session("marcarPaginaAtual") = 1
        'If (Session("marcarPaginaAtual")) = 1 Then
        '    Session("marcarPaginaAtual") = 0
        'Else
        '    Session("marcarPaginaAtual") = 1
        'End If

    End Sub

    Public Sub btnInvisivel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInvisivel.Click

        Dim existe As Boolean = False
        Dim marcados As New ArrayList()
        marcados = TryCast(Session("marcados"), ArrayList)


        'id = labelId.Value
        'operacao = labelTipoOperacao.Value

        For Each a As String In marcados
            If a = labelId.Value Then
                existe = True
                Exit For
            End If
        Next


        If existe Then
            marcados.Remove(labelId.Value)
        Else
            marcados.Add(labelId.Value)
        End If
        Session("marcados") = marcados

    End Sub

    Private Sub btnFiltrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFiltrar.Click
        Session("indice") = 0
    End Sub

    Private Sub btnTipoNomeCPF_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTipoNomeCPF.Click
        'grdFaturaDataBind(0)
        TipoCPF = txtTipoCPF.Text
        TipoNome = txtTipoNome.Text
        ListarUltimaFatura()
    End Sub
End Class

