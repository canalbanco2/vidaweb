Partial Class loader
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object




    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim str As String = "?temp=1"
        Dim alvoStr As String = ""
        Dim postStr As String = "?temp=1"

        For Each z As String In Request.QueryString.Keys
            If z <> "__VIEWSTATE" Then
                If z = "alvo" Then
                    alvoStr = Request.QueryString(z)
                Else
                    str &= "&" & z & "=" & Request.QueryString(z)
                End If
            End If
        Next

        For Each k As String In Request.Form.Keys
            If k <> "__VIEWSTATE" Then                
                postStr &= "<input type='hidden' name='" & k & "' value='" & Request.Form(k) & "'> "
            End If
        Next

        _get.Text = str
        alvo.Text = alvoStr
        post.Text = postStr

        Try
            If Trim(Session("apolice")) = "" Then
                cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
                Response.End()
            End If
        Catch ex as System.Exception
            cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
            Response.End()
        End Try

    End Sub


End Class
