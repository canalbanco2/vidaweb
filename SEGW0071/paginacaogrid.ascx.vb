Imports System.Drawing
Imports System.Web.UI.WebControls
Imports System.Data

Partial Class paginacaogrid
    Inherits System.Web.UI.UserControl


    Public _valor As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub

    Public Event ePaginaAlterada(ByVal Argumento As String)
    Public Event ePaginaAlteradaA(ByVal Argumento As String)

    Private Sub mDisparaEvento(ByVal Argumento As String)
        RaiseEvent ePaginaAlterada(Argumento)
    End Sub
    Private Sub mDisparaEventoA(ByVal Argumento As String)
        RaiseEvent ePaginaAlteradaA(Argumento)
    End Sub

    Private Sub mPaginacaoClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles a1.Command, a2.Command, a3.Command, a4.Command, a5.Command, a6.Command, a7.Command
        mDisparaEvento(e.CommandArgument)
    End Sub

    Private Sub aPaginacaoClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles a1.Command, a2.Command, a3.Command, a4.Command, a5.Command, a6.Command, a7.Command
        mDisparaEventoA(e.CommandArgument)
    End Sub

    Private Sub mMontaPaginacao(ByVal GridPaginaAtual As Integer, ByVal GridPaginasTotal As Integer)
        Dim paginainicial As Integer
        Dim paginafinal As Integer
        Dim paginaatual As Integer = GridPaginaAtual + 1


        If (paginaatual / 5) > 0 Then
            If (paginaatual Mod 5) <> 0 Then
                paginainicial = (Int(paginaatual / 5) * 5) + 1
            Else
                paginainicial = (Int((paginaatual - 1) / 5) * 5) + 1
            End If
            paginafinal = paginainicial + 4
            If paginafinal > GridPaginasTotal Then paginafinal = GridPaginasTotal
        Else
            paginainicial = 1
            paginafinal = 5
            If paginafinal > GridPaginasTotal Then paginafinal = GridPaginasTotal
        End If

        Me.a1.Visible = False
        Me.a2.Visible = False
        Me.a3.Visible = False
        Me.a4.Visible = False
        Me.a5.Visible = False
        Me.a6.Visible = False
        Me.a7.Visible = False
        If paginaatual > 1 Then
            a1.Text = "Anterior"
            a1.ForeColor = Color.White
            a1.Style.Add("font-family", "verdana")
            a1.Style.Add("font-size", "10px")
            a1.Style.Add("font-weight", "bold")
            a1.Style.Add("text-decoration", "underline")
            a1.Visible = True
        End If
        Dim a As Integer = 2
        For i As Integer = paginainicial To paginafinal
            CType(Me.FindControl("a" + a.ToString), LinkButton).CommandArgument = (i - 1).ToString
            CType(Me.FindControl("a" + a.ToString), LinkButton).Text = i.ToString
            CType(Me.FindControl("a" + a.ToString), LinkButton).Visible = True
            If i = paginaatual Then
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("color", "white")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("text-decoration", "none")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-family", "verdana")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-size", "10px")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-weight", "bold")

            Else
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("color", "white;")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("text-decoration", "underline")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-family", "verdana")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-size", "10px")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-weight", "bold")

            End If
            a += 1
        Next
        If paginaatual <> GridPaginasTotal Then
            a7.Text = "Pr�xima"
            a7.ForeColor = Color.White
            a7.Style.Add("font-family", "verdana")
            a7.Style.Add("font-size", "10px")
            a7.Style.Add("font-weight", "bold")
            a7.Style.Add("text-decoration", "underline")
            a7.Visible = True
        End If
    End Sub

    Dim todosIdsSemVirgula As New ArrayList
    Dim todosPaginaAtualSemVirgula As New ArrayList

    Public Sub GridDataBind(ByVal Grid As DataGrid, ByVal dt As DataTable)


        Grid.DataSource = dt

        '---------brauner----
        Dim todosIds As String = ""

        Dim totalVidas As Integer = dt.Rows.Count
        For Each x As DataRow In dt.Rows
            If todosIds = "" Then
                todosIds = x.Item(11).ToString

            Else
                todosIds &= "," & x.Item(11).ToString

            End If
            todosIdsSemVirgula.Add(x.Item(11).ToString)
        Next
        '-----------------------------

        Grid.DataBind()

        Dim cell As Web.UI.WebControls.DataGridItem
        Dim pai As Web.UI.WebControls.DataGridItem
        Dim count As Int16 = 0
        Dim tamMax(10) As Integer
        Dim tamMaxPos(10) As String
        Dim pai_id As String = ""
        Dim count2 As Integer = 0
        Dim k As Integer = 0
        Dim w As Integer = 1
        Dim t As Integer = 1
        Dim primeiro As Boolean = True
        Dim tamanho(9) As Integer
        Dim textoCell As String = ""
        Dim contadorImg As Integer = 0
        tamanho(0) = 10
        tamanho(1) = 11
        tamanho(2) = 40
        tamanho(3) = 14
        tamanho(4) = 12
        tamanho(5) = 11
        tamanho(6) = 11
        tamanho(7) = 10
        tamanho(8) = 10


        Dim txtMax(9) As String
        txtMax(0) = ""
        txtMax(1) = ""
        txtMax(2) = ""
        txtMax(3) = ""
        txtMax(4) = ""
        txtMax(5) = ""
        txtMax(6) = ""
        txtMax(7) = ""
        txtMax(8) = ""


        If cUtilitarios.MostraColunaSalario() Then
            w = 1
        Else
            w = 0
        End If

        If cUtilitarios.MostraColunaCapital() Then
            t = 1
        Else
            t = 0
        End If

        Dim temp As String = ""

        '---------brauner----


        Dim todosPaginaAtual As String = ""
        Dim totalVidasPaginaAtual As Integer = Grid.Items.Count
        For Each x As DataGridItem In Grid.Items
            If todosPaginaAtual = "" Then
                todosPaginaAtual = x.Cells(0).Text

            Else
                todosPaginaAtual &= "," & x.Cells(0).Text

            End If
            todosPaginaAtualSemVirgula.Add(x.Cells(0).Text)
        Next

        '-----------------------------
        Dim impressao As String = ""
        Dim totalImpressao As Integer = 0
        Dim marcados As New ArrayList

        If Session("todosmarcados") = 1 Then
            Dim vetor As New ArrayList
            vetor = TryCast(Session("marcados"), ArrayList)
            Dim vetor2 As New ArrayList
            For i As Integer = 0 To todosIdsSemVirgula.Count - 1

                If Not vetor2.Contains(todosIdsSemVirgula.Item(i)) Then
                    vetor2.Add(todosIdsSemVirgula.Item(i))
                End If
            Next

            If vetor.Count < vetor2.Count And vetor.Count <> 0 Then
                For i As Integer = 0 To todosIdsSemVirgula.Count - 1

                    If Not vetor.Contains(todosIdsSemVirgula.Item(i)) Then
                        vetor.Add(todosIdsSemVirgula.Item(i))
                    End If
                Next
            End If



            For i As Integer = 0 To todosIdsSemVirgula.Count - 1

                If Not vetor.Contains(todosIdsSemVirgula.Item(i)) Then
                    vetor.Add(todosIdsSemVirgula.Item(i))
                Else
                    vetor.Remove(todosIdsSemVirgula.Item(i))
                End If
            Next

            Session("marcados") = vetor


        ElseIf Session("marcarPaginaAtual") = 1 Then

            Dim naoConsta As Boolean = False
            Dim vetor As New ArrayList
            Dim vetor2 As New ArrayList

            'vetor2 = TryCast(Session("marcados"), ArrayList)
            vetor = TryCast(Session("marcados"), ArrayList)

            For i As Integer = 0 To todosPaginaAtualSemVirgula.Count - 1

                If Not vetor2.Contains(todosPaginaAtualSemVirgula.Item(i)) Then
                    vetor2.Add(todosPaginaAtualSemVirgula.Item(i))


                End If
            Next

            ' If vetor.Count < vetor2.Count And vetor.Count <> 0 Then

            Dim marcacaoSimples As Integer

            For i As Integer = 0 To todosPaginaAtualSemVirgula.Count - 1

                If vetor.Contains(todosPaginaAtualSemVirgula.Item(i)) Then
                    marcacaoSimples += 1
                End If
            Next

            For i As Integer = 0 To todosPaginaAtualSemVirgula.Count - 1

                If Not vetor.Contains(todosPaginaAtualSemVirgula.Item(i)) Then
                    vetor.Add(todosPaginaAtualSemVirgula.Item(i))
                Else
                    If marcacaoSimples >= todosPaginaAtualSemVirgula.Count Then
                        vetor.Remove(todosPaginaAtualSemVirgula.Item(i))
                    End If
                End If
            Next

            If Not IsNothing(Session("idsPaginaAtual")) Then

                Session("idsPaginaAtual") = vetor
            Else
                Session.Add("idsPaginaAtual", vetor)

            End If

            Session("marcados") = vetor


            ' End If

            '    For i As Integer = 0 To todosPaginaAtualSemVirgula.Count - 1

            '        If Not vetor.Contains(todosPaginaAtualSemVirgula.Item(i)) Then
            '            ' If Not vetor2.Contains(todosPaginaAtualSemVirgula.Item(i)) Then

            '            vetor.Add(todosPaginaAtualSemVirgula.Item(i))
            '            'End If
            '        Else
            '            vetor.Remove(todosPaginaAtualSemVirgula.Item(i))
            '        End If

            '    Next

            '    Session("marcados") = vetor

        End If



        If Grid.Items.Count > 0 Then




            Session("linhasGridFaturas") = Grid.Items.Count
            For Each x As Web.UI.WebControls.DataGridItem In Grid.Items
                contadorImg += 1
                'Verifica titular
                Dim tp_componente_id As String = x.Cells.Item(2).Text.Trim
                Dim prop_cliente_id As String = x.Cells.Item(0).Text.Trim

                If tp_componente_id = "1" Then
                    x.Cells.Item(2).Text = "Titular"
                ElseIf tp_componente_id = "2" Then
                    x.Cells.Item(2).Text = "C�njuge"
                Else
                    x.Cells.Item(2).Text = "Filho"
                End If

                cell = x

                If x.Cells.Item(3).Text = "Titular" Then
                    pai = x
                    x.ID = count2
                    pai_id = x.ID

                    x.Attributes.Add("onmouseover", "mO('#E8F1FF', this)")
                    x.Attributes.Add("onmouseout", "mO('#FFFFFF', this)")

                    count = 0
                    count2 += 1
                Else
                    x.ID = pai_id + "_" + count.ToString
                    x.Attributes.Add("style", "background-color:#f5f5f5;")
                    x.Attributes.Add("onmouseover", "mO('#E8F1FF', this)")
                    x.Attributes.Add("onmouseout", "mO('#f5f5f5', this)")

                    count = count + 1
                End If




                For i As Integer = 0 To x.Cells.Count - 1
                    x.Cells.Item(i).ID = "cell_" & i
                    x.Cells.Item(i).Wrap = False
                Next


                x.Cells.Item(4).Text = cUtilitarios.trataCPF(x.Cells.Item(4).Text)
                x.Cells.Item(7).Text = cUtilitarios.trataMoeda(x.Cells.Item(7).Text).Trim

                'x.Cells.Item(7).Text = getCapitalByComponente(prop_cliente_id, tp_componente_id)
                x.Cells.Item(8).Text = cUtilitarios.trataMoeda(x.Cells.Item(8).Text).Trim

                x.Cells.Item(8).HorizontalAlign = HorizontalAlign.Right
                x.Cells.Item(7).HorizontalAlign = HorizontalAlign.Right


                x.Attributes.Add("onclick", "alteracoes('" & x.Cells.Item(0).Text & "', '" & x.Cells.Item(3).Text & "', '" & x.Cells.Item(2).Text & "','" + x.Cells.Item(0).Text + "', '" & x.ID & "', '" + x.Cells.Item(1).Text & "')")

                Dim id2 As String = CType(x.FindControl("lblTeste1"), Label).Text



                '---------------Brauner---------------------------------




                marcados = TryCast(Session("marcados"), ArrayList)
                Dim estaMarcado As Boolean = False

                'id = x.Cells.Item(9).Text

                If marcados.Contains(id2) Then
                    estaMarcado = True
                End If





                If estaMarcado Then
                    'x.Cells.Item(0).Text = "<div id='chckYes" & k & "'><img src='images/checkboxYes.jpg' onclick='changeChck(" & k & "," & id2 & ");return false;'></div><div id='chckYes" & k & "' style='display:none'><img src='images/checkboxYes.jpg' onclick='changeChck(" & k & "," & id2 & ");return false;'></div><div style='display:none'><input type='checkbox' id='chkLista" & k & "' value='" & x.Cells.Item(0).Text & "' style='padding:0;'></div>"
                    x.Cells.Item(0).Text = "<div id='chckYes" & k & "'><img src='images/checkboxYes.jpg' onclick='changeChck(" & k & "," & id2 & ");' /></div><div style='display:none' id='chckNo" & k & "'><img src='images/checkboxNo.jpg' onclick='changeChck(" & k & "," & id2 & ");' /></div><div style='display:none'><input type='checkbox' id='chkLista" & k & "' value='" & x.Cells.Item(0).Text & "' style='padding:0;'/></div>"
                Else
                    'If Session("todosmarcados") = 1 Then
                    '    ' "<asp:image id="checkboxYes.jpg" runat="server" imageurl="images/checkboxYes.jpg" onclick="verificaMarcacao" OnClientClick="changeChck(" & k & ")";return false;/>"
                    '    'Dim text As String
                    '    'text = "<div id='chckYes" & k & "'><img src='images/checkboxYes.jpg' onclick='changeChck(" & k & "," & id2 & ");return false;'></div><div id='chckYes" & k & "' style='display:none'><img src='images/checkboxYes.jpg' onclick='changeChck(" & k & "," & id2 & ");return false;'></div><div style='display:none'><input type='checkbox' id='chkLista" & k & "' value='" & x.Cells.Item(0).Text & "' style='padding:0;'></div>"
                    '    'text = text & "<div id='chckNo" & k & "'><img src='images/checkboxNo.jpg' onclick='changeChck(" & k & "," & id2 & ");return false;'></div><div id='chckYes" & k & "' style='display:none'><img src='images/checkboxYes.jpg' onclick='changeChck(" & k & "," & id2 & ");return false;'></div><div style='display:none'><input type='checkbox' id='chkLista" & k & "' value='" & x.Cells.Item(0).Text & "' style='padding:0;'></div>"
                    '    x.Cells.Item(0).Text = x.Cells.Item(0).Text = "<div id='chckYes" & k & "'><img src='images/checkboxYes.jpg' onclick='changeChck(" & k & "," & id2 & ");' /></div><div style='display:none' id='chckNo" & k & "'><img src='images/checkboxNo.jpg' onclick='changeChck(" & k & "," & id2 & ");' /></div><div style='display:none'><input type='checkbox' id='chkLista" & k & "' value='" & x.Cells.Item(0).Text & "' style='padding:0;'/></div>"

                    'ElseIf Session("marcarPaginaAtual") = 1 Then

                    '    'x.Cells.Item(0).Text = "<div id='chckYes" & k & "'><img src='images/checkboxYes.jpg' onclick='changeChck(" & k & "," & id2 & ");return false;'></div><div id='chckYes" & k & "' style='display:none'><img src='images/checkboxYes.jpg' onclick='changeChck(" & k & "," & id2 & ");return false;'></div><div style='display:none'><input type='checkbox' id='chkLista" & k & "' value='" & x.Cells.Item(0).Text & "' style='padding:0;'></div>"
                    '    x.Cells.Item(0).Text = x.Cells.Item(0).Text = "<div id='chckYes" & k & "'><img src='images/checkboxYes.jpg' onclick='changeChck(" & k & "," & id2 & ");' /></div><div style='display:none' id='chckNo" & k & "'><img src='images/checkboxNo.jpg' onclick='changeChck(" & k & "," & id2 & ");' /></div><div style='display:none'><input type='checkbox' id='chkLista" & k & "' value='" & x.Cells.Item(0).Text & "' style='padding:0;'/></div>"

                    'Else
                    x.Cells.Item(0).Text = "<div style='display:none' id='chckYes" & k & "'><img src='images/checkboxYes.jpg' onclick='changeChck(" & k & "," & id2 & ");' /></div><div id='chckNo" & k & "'><img src='images/checkboxNo.jpg' onclick='changeChck(" & k & "," & id2 & ");' /></div><div style='display:none'><input type='checkbox' id='chkLista" & k & "' value='" & x.Cells.Item(0).Text & "' style='padding:0;'/></div>"
                    ' "<div id='chckNo" & k & "'><img src='images/checkboxNo.jpg' onclick='changeChck(" & k & "," & id2 & ");return false;'></div><div id='chckYes" & k & "' style='display:none'><img src='images/checkboxYes.jpg' onclick='changeChck(" & k & "," & id2 & ");return false;'></div><div style='display:none'><input type='checkbox' id='chkLista" & k & "' value='" & x.Cells.Item(0).Text & "' style='padding:0;'></div>"
                    'x.Cells.Item(0).Text = "<asp:ImageButton ID=""checkboxYes" & contadorImg & """ ImageUrl=""~/images/checkboxYes.JPG"" runat=""server"" />" '</div><div id='chckYes" & k & "' style='display:none'><img src='images/checkboxYes.jpg' onclick='changeChck(" & k & ");return false;'></div><div style='display:none'><input type='checkbox' id='chkLista" & k & "' value='" & x.Cells.Item(0).Text & "' style='padding:0;'></div>"
                    '<asp:image id= checkboxYes.jpg runat= server imageurl=images/checkboxYes.jpg onclick= verificaMarcacao OnClientClick= changeChck(" & k & ");return false;/>
                    'End If
                End If


                'If Session("todosmarcadosPaginaAtual") = 1 Then


                '    x.Cells.Item(0).Text = "<div id='chckYes" & k & "'><img src='images/checkboxYes.jpg' onclick='changeChck(" & k & ");return false;'></div><div id='chckYes" & k & "' style='display:none'><img src='images/checkboxYes.jpg' onclick='changeChck(" & k & ");return false;'></div><div style='display:none'><input type='checkbox' id='chkLista" & k & "' value='" & x.Cells.Item(0).Text & "' style='padding:0;'></div>"

                'ElseIf Session("todosmarcadosPaginaAtual") = 0 Then

                '    x.Cells.Item(0).Text = "<div id='chckNo" & k & "'><img src='images/checkboxNo.jpg' onclick='changeChck(" & k & ");return false;'></div><div id='chckYes" & k & "' style='display:none'><img src='images/checkboxYes.jpg' onclick='changeChck(" & k & ");return false;'></div><div style='display:none'><input type='checkbox' id='chkLista" & k & "' value='" & x.Cells.Item(0).Text & "' style='padding:0;'></div>"

                'End If


                '-----------------------------------------------------

                x.Cells.Item(2).Text = x.Cells.Item(2).Text.ToUpper

                If x.Cells.Item(1).Text = "Hist�rico" Then
                    x.Cells.Item(1).Text = "&nbsp;"
                End If

                For i As Integer = 0 To x.Cells.Count - 1
                    x.Cells.Item(i).Attributes.Add("class", "contentGridTable")
                Next

                For i As Integer = 0 To x.Cells.Count - 1
                    textoCell = x.Cells.Item(i).Text.ToString.Replace("&atilde;", "a").Replace("&ccedil;", "c")
                    If tamanho(i) <= textoCell.Length Then
                        If i = 0 Then
                            tamanho(i) = 3
                            txtMax(i) = "OO"
                        Else
                            tamanho(i) = textoCell.Length
                            txtMax(i) = x.Cells.Item(i).Text
                        End If


                    End If
                Next

                If w = 0 Then
                    x.Cells.Item(7).Style.Add("display", "none")
                Else
                    x.Cells.Item(7).Text = cUtilitarios.trataMoeda(x.Cells.Item(7).Text).Trim
                    x.Cells.Item(7).HorizontalAlign = HorizontalAlign.Right
                End If

                If t = 0 Then
                    x.Cells.Item(8).Style.Add("display", "none")
                Else
                    x.Cells.Item(8).Text = cUtilitarios.trataMoeda(x.Cells.Item(8).Text).Trim
                    x.Cells.Item(8).HorizontalAlign = HorizontalAlign.Right
                End If


                If primeiro Then
                    primeiro = False
                    For i As Integer = 0 To x.Cells.Count - 1
                        x.Cells.Item(i).Text = "<div class='headerFakeDiv'id='headerFake" & i & "'></div>" & x.Cells.Item(i).Text
                    Next
                End If

                k = k + 1
            Next

            For Each a As String In marcados
                totalImpressao += 1

                impressao &= "," & a

            Next



            Session("todosmarcados") = 0
            Session("marcarPaginaAtual") = 0
			'Inicio:INC000004283469 Autor:Rodrigo Moura Data:04/06/2014
            Session.Remove("clientes")
			'Fim:INC000004283469 Autor:Rodrigo Moura Data:04/06/2014

            Dim z As Integer = 0
            Dim texto As String
            Dim tamMin(9) As Integer
            tamMin(0) = 3
            tamMin(1) = 30
            tamMin(2) = 7
            tamMin(3) = 15
            tamMin(4) = 11
            tamMin(5) = 5
            tamMin(6) = 11
            tamMin(7) = 10
            tamMin(8) = 10


            Try

                For Each m As String In txtMax

                    texto = txtMax(z).ToString.Trim.Replace(" ", "O").Replace("&atilde;", "a").Replace("-", "O").Replace("&ccedil;", "c").PadRight(tamMin(z), "O")
                    Response.Write("<input type='hidden' value='" & texto & "' id='txtMax" & z & "'>" & vbCrLf)

                    z = z + 1

                    '-- FLOW 982821 - PMARQUES - CONFITEC - 18/05/2009
                    If z.Equals(9) Then
                        Exit For
                    End If
                Next
            Catch ex As System.Exception
                'Response.End()
            End Try


        End If
        '---------------------------Brauner----------------------------------




        If Not IsNothing(Session("totalVidasImpressao")) Then

            Session("totalVidasImpressao") = totalImpressao
        Else
            Session.Add("totalVidasImpressao", totalImpressao)
        End If


        If Not IsNothing(Session("clientes")) Then

            Session("clientes") = impressao
        Else
            Session.Add("clientes", impressao)
        End If



        If Not IsNothing(Session("totalVidas")) Then

            Session("totalVidas") = totalVidas
        Else
            Session.Add("totalVidas", totalVidas)
        End If



        '--------------------------------------------------------------------
        Me.mMontaPaginacao(Grid.CurrentPageIndex, Grid.PageCount)

    End Sub

    Public Sub enviaSession()

    End Sub




    Public Property CssClass() As String
        Get
            Return Me.lblPaginacao.Attributes.Item("class")
        End Get
        Set(ByVal Value As String)
            Me.lblPaginacao.Attributes.Add("class", Value)
        End Set
    End Property
End Class