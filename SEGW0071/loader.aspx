<%@ Page Language="vb" AutoEventWireup="false" Codebehind="loader.aspx.vb" Inherits="segw0071.loader"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>loader</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
    <style>
    body {
      padding: 0;
      margin: 0;
      overflow: hidden;
      font-family: helvetica, verdana, sans-serif;
    }

    #GB_frame {
      visibility: hidden;
      width: 100%;
      height: 100%;
    }

    #loading {
      padding-top: 10px;
      position: absolute;
      width: 100%;
      top: 0;
      font-size: 18px;
      text-align: center;
      color: #616161;
    }
  </style>
  <script>
  function loader() {
	var content = document.getElementById("_get").value;
	var alvo = document.getElementById("alvo").value;
	
	document.Form1.action = alvo + content;
	document.Form1.submit();
  }
  function loaded() {
	document.getElementById("loading").style.display = "none";
	document.getElementById("content").style.display = "block";
  }
  </script>
  <body MS_POSITIONING="GridLayout" onload="loader()">
	<div id="loading">
	<img src="scripts/box/indicator.gif" style="padding-bottom: 10px"><br />
	Aguarde...	
	</div>
	<div id="content" style="display:none;">    
		<iframe id="load" name="load" frameborder="0" width="100%" height="100%" src="blank.html"></iframe>
	</div>
	<div id="form" style="display:none;">    
		<form id="Form1" method="post" runat="server" action="" target="load">		
			<asp:TextBox ID="_get" Runat="server"></asp:TextBox>
			<asp:TextBox ID="alvo" Runat="server"></asp:TextBox>
			<asp:label ID="post" Runat="server"></asp:label>		
		</form>
	</div>
  </body>
</html>
