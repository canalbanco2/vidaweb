Imports System.Globalization

Partial Class multi_prints
    Inherits System.Web.UI.Page
    Public campos As New cDadosImpressao
    Public tb_coberturas As String = ""
    Public _error As Int16 = 0


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Dim linkseguro As Alianca.Seguranca.Web.LinkSeguro

        'Implementa��o do LinkSeguro
        'linkseguro = New Alianca.Seguranca.Web.LinkSeguro
        'Dim usuario As String = linkseguro.LerUsuario("", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))
        'If usuario = "" Then
        'Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
        'End If

        'Session("usuario_id") = linkseguro.Usuario_ID
        'Session("usuario") = linkseguro.Login_REDE
        'Session("cpf") = linkseguro.CPF

        'Implementa��o do controle de ambiente


        Dim cAmbiente As Alianca.Seguranca.Web.ControleAmbiente
        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & Request.ServerVariables("URL") & "/"
        cAmbiente.ObterAmbiente(url)

        'cAmbiente.ObterAmbiente("http://intranet.aliancadobrasil.com.br/")

        'configura��o da conex�o com o banco de dados
        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
        Else
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
        End If

        Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)


        If Not Page.IsPostBack Then

            For Each m As String In Request.QueryString.Keys
                If m <> "" Then
                    Session(m) = Request.QueryString(m)
                End If
            Next


            '****************************************************
            'Inicia processo de busca de dados: Etapas
            '****************************************************
         
            MontaTelaImpressao()
            'Inicio:INC000004283469 Autor:Rodrigo Moura Data:04/06/2014

            Dim marcados As New ArrayList
            Session("marcados") = marcados

            'Fim:INC000004283469 Autor:Rodrigo Moura Data:04/06/2014
        End If

        Try
            If Trim(Session("apolice")) = "" Then
                cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
                Response.End()
            End If
        Catch ex As System.Exception
            cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
            Response.End()
        End Try

    End Sub

    Sub CarregaFormulario(ByVal cliente_id As Integer, _
                            ByVal apolice_id As Integer, _
                            ByVal ramo_id As Integer, _
                            ByVal subgrupo_id As Integer, _
                            ByVal fatura_id As Integer, _
                            ByVal dt_ini_fatura As String, _
                            ByVal usuario As String) ', _                            ByVal certificado_id As Integer)

        Dim tipo_pessoa As Integer
        Dim certificado_id As Integer
        Dim dr As Data.SqlClient.SqlDataReader
        Dim dr2 As Data.SqlClient.SqlDataReader

        PreencheRamoApolice()


        Try

            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

            'Flow 1224728 - Altera��o para verificar se existe o n�mero de Certificado e se n�o houver gera um novo
            'Douglas - Confitec 15.09.2009

            Dim bd2 As New cAcompanhamento(cDadosBasicos.eBanco.segab_db)

            'bd.buscaCertificadoID(apolice_id, ramo_id, subgrupo_id, cliente_id, usuario)
            bd2.geraCertificadoID(apolice_id, ramo_id, subgrupo_id, cliente_id, Session("usuario"))
            'campos.CertificadoID = "---"  'valor default se houver preenche sen�o permanece assim
            dr2 = bd2.ExecutaSQL_DR()


            If dr2.Read() Then
                If Not IsDBNull(dr2.GetValue(0).ToString) Then
                    If dr2.GetValue(0).ToString <> 0 Then
                        campos.CertificadoID = dr2.GetValue(0).ToString
                        certificado_id = campos.CertificadoID
                    End If
                End If

            End If

            dr2.Close()
            dr2 = Nothing



            bd.SEGS6980_SPS(cliente_id, apolice_id, ramo_id, subgrupo_id, fatura_id, dt_ini_fatura, certificado_id)

            'Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
            dr = bd.ExecutaSQL_DR()

            If dr.Read Then

                campos.Ini_vg_apo = IIf(dr.GetValue(0).ToString = "", "---", dr.GetValue(0).ToString)
                campos.Fim_vg_apo = IIf(dr.GetValue(1).ToString = "", "---", dr.GetValue(1).ToString)

                If Not IsDBNull(dr.Item(2)) Then
                    campos.Seg_Nome = dr.Item(2).ToString
                End If

                If Not IsDBNull(dr.Item(3)) Then
                    campos.Seg_DataNasc = Format(dr.Item(3), "dd/MM/yyyy".ToString).Substring(0, 10)
                End If

                If Not IsDBNull(dr.Item(4)) Then
                    campos.Seg_cpf = cUtilitarios.trataCPF(dr.Item(4).ToString)
                End If

                If Not dr.IsDBNull(5) Then
                    campos.Ini_vg_subgrp = IIf(dr.IsDBNull(5), " --- ", CType(dr.GetValue(5), DateTime).ToShortDateString())
                Else
                    campos.Ini_vg_subgrp = " --- "
                End If

                Try
                    If Not dr.IsDBNull(6) Then
                        campos.Fim_vg_subgrp = IIf(dr.IsDBNull(6), " --- ", CType(dr.GetValue(6), DateTime).ToShortDateString())
                    Else
                        campos.Fim_vg_subgrp = " --- "
                    End If

                Catch ex As System.Exception
                    campos.Fim_vg_subgrp = " --- "
                End Try

                campos.NomeEstipulante = IIf(dr.GetValue(7).ToString = "", "---", dr.GetValue(7).ToString)
                campos.CNPJe = IIf(dr.GetValue(8).ToString = "", "---", dr.GetValue(8).ToString)

                If Not IsDBNull(dr.Item(9)) Then
                    campos.NomeSubEstipulante = dr.Item(9).ToString
                End If

                If Not IsDBNull(dr.Item(10)) Then
                    tipo_pessoa = dr.Item(10).ToString

                    If tipo_pessoa = "1" Then
                        If Not IsDBNull(dr.Item(11)) Then
                            campos.CNPJsub_e = dr.Item(11).ToString
                        End If
                    Else
                        If Not IsDBNull(dr.Item(12)) Then
                            campos.CNPJsub_e = dr.Item(12).ToString
                        End If
                    End If

                End If

                If Not IsDBNull(dr.Item(13)) Then
                    campos.Corretor = dr.Item(13).ToString
                End If

                'AKIO OKUNO - INICIO - 09/12/2011 - FLOWBR 12083065 
                '                If Not IsDBNull(dr.Item(14)) Then
                '                    campos.SUSEP = dr.Item(14).ToString
                '                End If
                If Not IsDBNull(dr.Item(17)) Then
                    campos.SUSEP = dr.Item(17).ToString
                End If
                'AKIO OKUNO - FIM - 09/12/2011 - FLOWBR 12083065 

                If Not IsDBNull(dr.Item(15)) Then
                    campos.Premio = dr.Item(15).ToString
                End If

                If Not IsDBNull(dr.Item(16)) Then
                    campos.DataEmissao = dr.Item(16).ToString
                Else
                    campos.DataEmissao = "---"
                End If

            Else
                campos.Ini_vg_apo = "---"
                campos.Fim_vg_apo = "---"
                campos.Ini_vg_subgrp = " --- "
                campos.Fim_vg_subgrp = " --- "
                campos.NomeEstipulante = "---"
                campos.CNPJe = "---"
                campos.NomeSubEstipulante = "-------------------"
                campos.DataEmissao = "---"
                campos.Observacoes = "---"
            End If
            '''''

            '''''
            dr.Close()
            dr = Nothing

        Catch ex As System.Exception
            If _error = 0 Then
                Dim excp As New clsException(ex)
            End If

            _error = _error + 1

        End Try

        getCoberturas(cliente_id)

        MontaHeaderFooterCobertura()

    End Sub

    Sub IniciaEtapas(ByVal cliente_id As String)
        '****************************************************
        'Inicia processo de busca de dados: Etapas
        '****************************************************

        CarregaFormulario(cliente_id, Session("apolice"), Session("ramo"), Session("subgrupo_id"), Request.QueryString("nr_fatura"), Request.QueryString("dt_ini_fatura"), Request.QueryString("cpf"))

    End Sub

    Sub PreencheRamoApolice()
        Try
            campos.Ramo = Session("ramo")
            campos.NumApolice = Session("apolice")

            Select Case campos.Ramo
                Case "77"
                    campos.Titulo = "Certificado Individual de Seguro de Prestamista"
                    campos.ProcessoSusep = "Processo Susep: 15414.003297/2004-11"
                Case "82"
                    campos.Titulo = "Certificado Individual de Seguro de Acidentes Pessoais Coletivo"
                    campos.ProcessoSusep = "Processo Susep: 10.005462/99-17"
                Case Else
                    campos.Titulo = "Certificado Individual de Seguro de Vida em Grupo"
                    campos.ProcessoSusep = "Processo Susep: 10005463/99-80"
            End Select

        Catch ex As System.Exception
            If _error = 0 Then
                Dim excp As New clsException(ex)
            End If

            _error = _error + 1
        End Try

    End Sub

    Private Sub getdataApolice()
        Try

            Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

            bd.getDataIniVigenciaApolice(Session("apolice"), Session("ramo"))

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read Then
                campos.Ini_vg_apo = IIf(dr.GetValue(0).ToString = "", "---", dr.GetValue(0).ToString)
                campos.Fim_vg_apo = IIf(dr.GetValue(1).ToString = "", "---", dr.GetValue(1).ToString)
            Else
                campos.Ini_vg_apo = "---"
                campos.Fim_vg_apo = "---"
            End If
            dr.Close()
            dr = Nothing

        Catch ex As System.Exception
            If _error = 0 Then
                Dim excp As New clsException(ex)
            End If

            _error = _error + 1

        End Try

    End Sub

    Public Sub Carrega_Dados_do_Subgrupo()

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Dim nr_fatura As String = Request.QueryString("nr_fatura")

        'Pega Data de in�cio e fim de vig�ncia do subgrupo

        'bd.GetCapitalMaximo(Session("apolice"), Session("subgrupo_id"), Session("ramo"), 6785, 0, Session("usuario"), Session("acesso"), Session("cpf"))
        bd.GetDadosSubgrupoE1(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cUtilitarios.destrataCPF(campos.Seg_cpf), nr_fatura)

        'Response.Write(bd.SQL)

        Try
            dr = bd.ExecutaSQL_DR()

            If dr.Read Then

                'For m As Integer = 0 To dr.FieldCount() - 1
                '    If Not dr.IsDBNull(m) Then
                '        cUtilitarios.br(m & "=>" & dr.GetValue(m))
                '    Else
                '        cUtilitarios.br(m & "=> '' ")
                '    End If
                'Next

                'Linha 2
                If Not dr.IsDBNull(0) Then
                    campos.Ini_vg_subgrp = IIf(dr.IsDBNull(0), " --- ", CType(dr.GetValue(0), DateTime).ToShortDateString())
                Else
                    campos.Ini_vg_subgrp = " --- "
                End If

                Try
                    If Not dr.IsDBNull(1) Then
                        campos.Fim_vg_subgrp = IIf(dr.IsDBNull(1), " --- ", CType(dr.GetValue(1), DateTime).ToShortDateString())
                    Else
                        campos.Fim_vg_subgrp = " --- "
                    End If

                Catch ex As System.Exception
                    campos.Fim_vg_subgrp = " --- "
                End Try

            Else
                campos.Ini_vg_subgrp = " --- "
                campos.Fim_vg_subgrp = " --- "
            End If

        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        Finally
            dr.Close()
        End Try

    End Sub

    Private Sub mPreencheDadosBasicosApolice()

        Dim BD As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        BD.dados_basico_apolice_sps(Session("apolice"), Session("ramo"), 6785, 0, "'" & Session("cpf") & "'")

        Try

            dr = BD.ExecutaSQL_DR()

            If dr.Read() Then

                campos.NomeEstipulante = IIf(dr.GetValue(2).ToString = "", "---", dr.GetValue(2).ToString)
                campos.CNPJe = IIf(dr.GetValue(1).ToString = "", "---", dr.GetValue(1).ToString)
            Else
                campos.NomeEstipulante = "---"
                campos.CNPJe = "---"
            End If

        Catch ex As System.Exception
            If _error = 0 Then
                Dim excp As New clsException(ex)
            End If

            _error = _error + 1

        Finally
            dr.Close()
            dr = Nothing
        End Try

    End Sub

    Private Sub MontaHeaderFooterCobertura()
        tb_coberturas = "<table id='tblSeguros'>" & tb_coberturas & "</table>"
    End Sub

    Private Sub getCoberturas(ByVal cliente_id As String)

        Dim tb_head_temp_T As String = "	<tr class='0034td_dado'><td colspan='3'>Titular</td>	</tr>	<tr class='0034td_dado'>		<td width='80%'>Nome Cobertura</td>		<td>&nbsp&nbsp&nbsp</td>		<td>Capital Individual</td>	</tr>"
        Dim tb_head_temp_F As String = "<tr class='0034td_dado'>		<td colspan='3'>Filhos</td>	    </tr>	<tr class='0034td_dado'>		<td width='80%'>Nome Cobertura</td>		<td>&nbsp&nbsp&nbsp</td>		<td></td>	</tr>"
        Dim tb_foot_temp_F As String = "</table>"
        Dim tb_head_temp_C As String = "<tr class='0034td_dado'>		<td colspan='3'>C�njuge</td>	    </tr>	<tr class='0034td_dado'>		<td width='80%'>Nome Cobertura</td>		<td>&nbsp&nbsp&nbsp</td>		<td></td>	</tr>"
        Dim tb_foot_temp_C As String = "</table>"

        Dim bAtendidoCaso_1 As Boolean = False
        Dim bAtendidoCaso_2 As Boolean = False
        Dim bAtendidoCaso_3 As Boolean = False

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        bd.SEGS7032_SPS(Session("apolice"), 0, 6785, Session("ramo"), Session("subgrupo_id"), cliente_id)

        Try
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            While dr.Read
                Select Case CInt(dr.Item("tp_componente_id"))
                    Case 1 'Titular

                        Dim tb_meio_temp_T As String = "	<tr class='0034td_dado'>		<td class='DataLabelsMini'>bd_nome_cob</td>		<td class='DataLabelsMini' align='right'>bd_iea_min</td>		<td class='DataLabelsMini' align='right'>bd_iea_max</td>	</tr>"

                        tb_meio_temp_T = tb_meio_temp_T.Replace("bd_nome_cob", IIf(dr.Item(8).ToString.Trim = "", " --- ", dr.Item(8).ToString.Trim))
                        tb_meio_temp_T = tb_meio_temp_T.Replace("bd_iea_min", "&nbsp&nbsp&nbsp")
                        tb_meio_temp_T = tb_meio_temp_T.Replace("bd_iea_max", IIf(cUtilitarios.trataMoeda(Convert.ToString(dr.Item(10))) = "", " --- ", cUtilitarios.trataMoeda(Convert.ToString(dr.Item(10)))))

                        tb_head_temp_T &= tb_meio_temp_T

                        bAtendidoCaso_1 = True
                        'Inicio- Autor: Rodrigo.Moura Confitec Data:16/05/2011
                        'Comentado porque n�o funcionava para filhos � igual a 6,
                        'Case 2 
                    Case 6 'Filhos
                        'FIM- Autor: Rodrigo.Moura Confitec Data:16/05/2011
                        Dim tb_meio_temp_F As String = "	<tr class='0034td_dado'>		<td class='DataLabelsMini'>bd_nome_cob</td>		<td class='DataLabelsMini' align='right'>bd_iea_min</td>		<td class='DataLabelsMini' align='right'>bd_iea_max</td>	</tr>"

                        tb_meio_temp_F = tb_meio_temp_F.Replace("bd_nome_cob", IIf(dr.Item(8).ToString.Trim = "", " --- ", dr.Item(8).ToString.Trim))
                        tb_meio_temp_F = tb_meio_temp_F.Replace("bd_iea_min", "&nbsp&nbsp&nbsp")
                        tb_meio_temp_F = tb_meio_temp_F.Replace("bd_iea_max", IIf(cUtilitarios.trataMoeda(Convert.ToString(dr.Item(10))) = "", " --- ", cUtilitarios.trataMoeda(Convert.ToString(dr.Item(10)))))

                        tb_head_temp_F &= tb_meio_temp_F

                        bAtendidoCaso_2 = True

                    Case 3 'Conjuge

                        Dim tb_meio_temp_C As String = "	<tr class='0034td_dado'>		<td class='DataLabelsMini'>bd_nome_cob</td>		<td class='DataLabelsMini' align='right'>bd_iea_min</td>		<td class='DataLabelsMini' align='right'>bd_iea_max</td>	</tr>"

                        tb_meio_temp_C = tb_meio_temp_C.Replace("bd_nome_cob", IIf(dr.Item(8).ToString.Trim = "", " --- ", dr.Item(8).ToString.Trim))
                        tb_meio_temp_C = tb_meio_temp_C.Replace("bd_iea_min", "&nbsp&nbsp&nbsp")
                        tb_meio_temp_C = tb_meio_temp_C.Replace("bd_iea_max", IIf(cUtilitarios.trataMoeda(Convert.ToString(dr.Item(10))) = "", " --- ", cUtilitarios.trataMoeda(Convert.ToString(dr.Item(10)))))

                        tb_head_temp_C &= tb_meio_temp_C

                        bAtendidoCaso_3 = True

                End Select
            End While

            If Not bAtendidoCaso_1 Then
                Dim tb_meio_temp_T As String = "	<tr class='0034td_dado'>		<td class='DataLabelsMini'>&nbsp&nbsp&nbsp</td>		<td class='DataLabelsMini' align='right'>&nbsp&nbsp</td>		<td class='DataLabelsMini' align='right'>---</td>	</tr>"
                tb_head_temp_T &= tb_meio_temp_T
            End If
            If Not bAtendidoCaso_2 Then
                'Dim tb_meio_temp_F As String = "	<tr class='0034td_dado'>		<td class='DataLabelsMini'>&nbsp&nbsp&nbsp</td>		<td class='DataLabelsMini' align='right'>&nbsp&nbsp</td>		<td class='DataLabelsMini' align='right'>---</td>	</tr>"
                'tb_head_temp_F &= tb_meio_temp_F
            End If
            If Not bAtendidoCaso_3 Then
                'Dim tb_meio_temp_C As String = "	<tr class='0034td_dado'>		<td class='DataLabelsMini'>&nbsp&nbsp&nbsp</td>		<td class='DataLabelsMini' align='right'>&nbsp&nbsp</td>		<td class='DataLabelsMini' align='right'>---</td>	</tr>"
                'tb_head_temp_C &= tb_meio_temp_C
            End If

            tb_coberturas &= tb_head_temp_T & ""
            tb_coberturas &= tb_head_temp_C & ""
            tb_coberturas &= tb_head_temp_F & ""

        Catch ex As Exception
            If _error = 0 Then
                Dim excp As New clsException(ex)
            End If
            _error = _error + 1

        End Try

    End Sub

    Sub BuscaDadosSubEstipulante()

        'Busca: Dados sub-estipulante
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dt_ini_fatura As String = Request.QueryString("dt_ini_fatura")
        Dim tipo_pessoa As Integer

        Try

            bd.BuscarDadosSubEstipulante(Session("apolice"), Session("ramo"), Session("subgrupo_id"), dt_ini_fatura)

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read() Then

                If Not IsDBNull(dr.Item("estipulante")) Then
                    campos.NomeSubEstipulante = dr.Item("estipulante").ToString
                End If

                If Not IsDBNull(dr.Item("tp_pessoa")) Then
                    tipo_pessoa = dr.Item("tp_pessoa").ToString

                    If tipo_pessoa = "1" Then
                        If Not IsDBNull(dr.Item("cgc_estipulante")) Then
                            campos.CNPJsub_e = dr.Item("cgc_estipulante").ToString
                        End If
                    Else
                        If Not IsDBNull(dr.Item("cpf_estipulante")) Then
                            campos.CNPJsub_e = dr.Item("cpf_estipulante").ToString
                        End If
                    End If


                End If

            Else
                campos.NomeSubEstipulante = "-------------------"
            End If

            dr.Close()
            dr = Nothing

        Catch ex As System.Exception
            If _error = 0 Then
                Dim excp As New clsException(ex)
            End If

            _error = _error + 1
        End Try

    End Sub

    Sub BuscaDadosCliente(ByVal cliente_id As String)

        'Busca: Dados dos cliente
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        'Dim cliente_id As String = Request.QueryString("cliente_id")

        Try
            bd.BuscarDadosCliente(cliente_id)

            'cUtilitarios.br(bd.SQL)

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read() Then

                If Not IsDBNull(dr.Item("nome")) Then
                    campos.Seg_Nome = dr.Item("nome").ToString
                End If

                If Not IsDBNull(dr.Item("dt_nascimento")) Then
                    'campos.Seg_DataNasc = Left(dr.Item("dt_nascimento").ToString, 10)
                    campos.Seg_DataNasc = Format(dr.Item("dt_nascimento"), "dd/MM/yyyy".ToString).Substring(0, 10)
                End If

                If Not IsDBNull(dr.Item("cpf")) Then
                    campos.Seg_cpf = cUtilitarios.trataCPF(dr.Item("cpf").ToString)
                End If

            End If

            dr.Close()
            dr = Nothing

        Catch ex As System.Exception
            If _error = 0 Then
                Dim excp As New clsException(ex)
            End If

            _error = _error + 1
        End Try

    End Sub

    Sub BuscaDadosCorretor()

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dt_ini_fatura As String = Request.QueryString("dt_ini_fatura")

        Try
            bd.BuscaDadosCorretor(Session("apolice"), Session("ramo"), Session("subgrupo_id"), dt_ini_fatura)

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read() Then

                If Not IsDBNull(dr.Item("corretor")) Then
                    campos.Corretor = dr.Item("corretor").ToString
                End If

                If Not IsDBNull(dr.Item("corretor_susep")) Then
                    campos.SUSEP = cUtilitarios.getCodSusep() 'dr.Item("corretor_susep").ToString
                End If


            End If

            dr.Close()
            dr = Nothing

        Catch ex As System.Exception
            If _error = 0 Then
                Dim excp As New clsException(ex)
            End If

            _error = _error + 1
        End Try


    End Sub

    Sub BuscaPremio(ByVal cliente_id As String)

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim nr_fatura As String = Request.QueryString("nr_fatura")
        'Dim cliente_id As String = Request.QueryString("cliente_id")

        Try
            bd.BuscaPremio(Session("apolice"), Session("ramo"), nr_fatura, cliente_id)

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read() Then

                If Not IsDBNull(dr.Item("premio_bruto_seg")) Then
                    campos.Premio = dr.Item("premio_bruto_seg").ToString
                End If

            End If

            dr.Close()
            dr = Nothing

        Catch ex As System.Exception
            If _error = 0 Then
                Dim excp As New clsException(ex)
            End If

            _error = _error + 1
        End Try

    End Sub

    'Function BuscaCertificadoID(ByVal cliente_id As String)

    '    Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

    '    Try
    '        bd.buscaCertificadoID(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cliente_id)
    '        Response.Write(bd.SQL)
    '        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

    '        If dr.Read() Then

    '            If Not IsDBNull(dr.Item("cert_id")) Then
    '                campos.CertificadoID = dr.Item("cert_id").ToString
    '            End If
    '        Else
    '            campos.CertificadoID = "---"
    '        End If

    '        dr.Close()
    '        dr = Nothing

    '    Catch ex as System.Exception
    '        If _error = 0 Then
    '            Dim excp As New clsException(ex)
    '        End If

    '        _error = _error + 1
    '    End Try

    'End Function

    Sub buscaDataEmissao(ByVal certificado_id As String)

        If certificado_id = "---" Then
            campos.DataEmissao = "---"
            Exit Sub
        End If

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        Try
            ' bd.buscaDataEmissao(Session("apolice"), Session("ramo"), Integer.Parse(certificado_id))
            bd.buscaDataEmissao(Session("apolice"), Session("ramo"), 0)

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read() Then

                If Not IsDBNull(dr.Item("dt_Emissao")) Then
                    campos.DataEmissao = dr.Item("dt_Emissao").ToString
                End If
            Else
                campos.DataEmissao = "---"
            End If

            dr.Close()
            dr = Nothing

        Catch ex As System.Exception
            If _error = 0 Then
                Dim excp As New clsException(ex)
            End If

            _error = _error + 1
        End Try

    End Sub

    Sub BuscaObservacoes()

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        Try
            bd.buscaObservacoes(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.Read() Then

                If Not IsDBNull(dr.Item("sub_grupo_id")) Then
                    'campos.Observacoes = dr.Item("sub_grupo_id").ToString & " - "
                End If

                If Not IsDBNull(dr.Item("nome")) Then
                    'campos.Observacoes &= dr.Item("nome").ToString
                End If

            Else
                campos.Observacoes = "---"
            End If

            dr.Close()
            dr = Nothing

        Catch ex As System.Exception
            If _error = 0 Then
                Dim excp As New clsException(ex)
            End If

            _error = _error + 1
        End Try
    End Sub

    Private Function addCell(ByVal valor As String) As Web.UI.HtmlControls.HtmlTableCell
        Dim td As New Web.UI.HtmlControls.HtmlTableCell
        td.Attributes.Add("class", "DataLabelsMini")
        td.InnerText = valor
        Return td
    End Function
    'Monta o HTML das p�ginas da tela de impressao do certificado individual
    Sub MontaTelaImpressao()
        Session("imprimiu") = 1
        Try

            Dim totalVidas As Integer = Request.QueryString("totalVidas")
            'CONFITEC - LUCIANO - IM00730587 -  29/03/2019 - INICIO
			Dim clientes As String = Session("clientes")
			'CONFITEC - LUCIANO - IM00730587 -  29/03/2019 - FIM
			
            'Remove a primeira v�rgula da variavel array
            clientes = Right(clientes, clientes.Length - 1)
            'Separa os clientes_id
            Dim clientes_id() As String = clientes.Split(",")

            Dim x As Integer = 0
            Dim firstHTML As String = ""
            Dim repeatHTML As String = ""
            Dim tabelaHTML As String = ""

            'Abaixo, ser� utilizado posteriormente
            'BD_tabela_coberturas = "<table id='tblSeguros'>	<tr class='0034td_dado'>		<td colspan='3'>Titular</td>	</tr>	<tr class='0034td_dado'>		<td>Nome Cobertura</td>		<td>Limite M�nimo</td>		<td>Limite M�ximo</td>	</tr>	<tr class='0034td_dado'>		<td class='DataLabelsMini'>INDENIZA&#199;AO ESPECIAL POR ACIDENTE - IEA</td>		<td class='DataLabelsMini' align='right'>0,00</td>		<td class='DataLabelsMini' align='right'>0,00</td>	</tr>	<tr class='0034td_dado'>		<td class='DataLabelsMini'>INVALIDEZ PERMANENTE TOTAL OU PARCIAL POR ACIDENTE - IPA</td>		<td class='DataLabelsMini' align='right'>0,00</td>		<td class='DataLabelsMini' align='right'>0,00</td>	</tr>	<tr class='0034td_dado'>		<td class='DataLabelsMini'>INVALIDEZ PERMANENTE TOTAL POR DOEN&#199;A - IPD</td>		<td class='DataLabelsMini' align='right'>0,00</td>		<td class='DataLabelsMini' align='right'>0,00</td>	</tr>	<tr class='0034td_dado'>		<td class='DataLabelsMini'>MORTE NATURAL OU ACIDENTAL</td>		<td class='DataLabelsMini' align='right'>10.000,00</td>		<td class='DataLabelsMini' align='right'>900.000,00</td>	</tr></table>"
            tabelaHTML = "<table id='tblSeguros'>	<tr class='0034td_dado'>		<td colspan='3'>Titular</td>	</tr>	<tr class='0034td_dado'>		<td>Nome Cobertura</td>		<td>Limite M�nimo</td>		<td>Limite M�ximo</td>	</tr>	<tr class='0034td_dado'>		<td class='DataLabelsMini'>INDENIZA&#199;AO ESPECIAL POR ACIDENTE - IEA</td>		<td class='DataLabelsMini' align='right'>BD_IEA_min</td>		<td class='DataLabelsMini' align='right'>BD_IEA_max</td>	</tr>	<tr class='0034td_dado'>		<td class='DataLabelsMini'>INVALIDEZ PERMANENTE TOTAL OU PARCIAL POR ACIDENTE - IPA</td>		<td class='DataLabelsMini' align='right'>BD_IPA_min</td>		<td class='DataLabelsMini' align='right'>BD_IPA_max</td>	</tr>	<tr class='0034td_dado'>		<td class='DataLabelsMini'>INVALIDEZ PERMANENTE TOTAL POR DOEN&#199;A - IPD</td>		<td class='DataLabelsMini' align='right'>BD_IPD_min</td>		<td class='DataLabelsMini' align='right'>BD_IPD_max</td>	</tr>	<tr class='0034td_dado'>		<td class='DataLabelsMini'>MORTE NATURAL OU ACIDENTAL</td>		<td class='DataLabelsMini' align='right'>BD_MNA_min</td>		<td class='DataLabelsMini' align='right'>BD_MNA_max</td>	</tr></table>"

            '<div style='height:1100px;border:1px solid black;position:relative;'></div>
            firstHTML = "<TABLE cellSpacing='0' cellPadding='0' align='center' border='0' style='WIDTH: 800px;'><TBODY><TR id='imgPrint'><TD style='WIDTH: 782px;' align='right' colSpan='3'><INPUT type='button' value='Imprimir/Consultar' Width='165px' onclick='JavaScript:imprimir();' style='border: #CCCCCC 1px solid; font-family: Verdana;font-size: 10px;color: #FFFFFF;font-weight: bold;background-color: #003399;padding-left:5px;padding-right:5px;'></TD></TR><TR><TD style='WIDTH: 582px'>"
            'repeatHTML = "<TABLE cellSpacing='0' cellPadding='0' align='center' border='0' style='WIDTH: 700px; HEIGHT: 1150px'><TBODY><TR><TD style='WIDTH: 682px'><TABLE cellSpacing='0' cellPadding='2' style='WIDTH: 100%;'><TBODY><TR><TD style='WIDTH: 123px' align='left'><IMG style='BORDER-RIGHT: white 4px solid; BORDER-TOP: white 3px solid' src='files_print/bnb_alianca_apolice.gif' border='0'></TD><TD align='right' style='WIDTH: 704px' valign='bottom'><FONT style='FONT-SIZE: 9pt' size='2'><FONT face='Arial, sans-serif'><P class='western' style='MARGIN-BOTTOM: 0cm' align='center'><FONT face='Verdana'><p style='font-family:Verdana;font-size:14px;font-weight:bold;'><br><br>Certificado Individual de Seguro de Vida em Grupo</FONT></p>Processo Susep: 10005463/99-80 </FONT></FONT></P></TD>								</TR>							</TBODY>						</TABLE></TD></TR><TR><TD style='WIDTH: 90%;'><TABLE id='Table1' style='WIDTH: 90%;' cellSpacing='0' cellPadding='1'><TBODY><TR><TD class='0034td_label' style='WIDTH: 90%'>&nbsp;<FONT face='Arial' color='#333399' size='1'>N�mero do Certificado Individual</FONT></TD> </TR><TR><TD class='0034td_dado' style='WIDTH: 90%'>&nbsp;<span id='lblNumeroCertificado' class='DataLabels' style='width:172px;'>BD_lblNumeroCertificado</span></TD></TR></TBODY></TABLE><TABLE cellSpacing='0' cellPadding='1' style='WIDTH: 90%; HEIGHT: 104px' border='0'><TBODY><TR><TD class='0034td_label' style='WIDTH: 102px;'><FONT face='Arial' color='#333399' size='1'>&nbsp;Ramo</FONT></TD><TD class='0034td_label' style='WIDTH: 500px;'>&nbsp;<FONT face='Arial' color='#333399' size='1'>N�mero da Ap�lice</FONT></TD><TD class='0034td_label'><FONT face='Arial' color='#333399' size='1'>In�cio<font color='#ffffff'>_</font>de<font color='#ffffff'>_</font>Vig�ncia<font color='#ffffff'>_</font>da<font color='#ffffff'>_</font>Ap�lice</FONT></TD><TD class='0034td_label' width='180'><P align='left'><FONT face='Arial' color='#333399' size='1'>Fim<font color='#ffffff'>_</font>de<font color='#ffffff'>_</font>Vig�ncia<font color='#ffffff'>_</font>da<font color='#ffffff'>_</font>Ap�lice</FONT></P></TD></TR><TR><TD class='0034td_dado' style='WIDTH: 102px'>&nbsp;<span id='lblRamo' class='DataLabels' style='width:102px;'>BD_lblRamo</span></TD><TD class='0034td_dado' style='WIDTH: 500px'>&nbsp;<span id='lblNumApolice' class='DataLabels' style='width:105px;'>BD_lblNumApolice</span></TD><TD class='0034td_dado' style='WIDTH: 103px'><span id='lblIni_vg_apo' class='DataLabels' style='width:100px;'>BD_lblIni_vg_apo</span></TD><TD class='0034td_dado' width='700'><P align='left'><span id='lblFim_vg_apo' class='DataLabels' style='width:102px;'>BD_lblFim_vg_apo</span></P></TD></TR><TR>	<TD class='0034td_label' style='WIDTH: 300px' colSpan='2'><P align='left'><FONT face='Arial' color='#333399' size='1'>Nome do Estipulante</FONT></P></TD><TD class='0034td_label' width='100'><P align='left'><FONT face='Arial' color='#333399' size='1'>CNPJ&nbsp;</FONT></P></TD></TR><TR><TD class='0034td_dado' style='WIDTH: 600px' colSpan='2'><P align='left'><span id='lblNomeEstipulante' class='DataLabels' style='width:260px;'>BD_lblNomeEstipulante</span></P></TD><TD class='0034td_dado' style='WIDTH: 163px'><P align='left'><span id='lblCNPJe' class='DataLabels' style='width:102px;'>BD_lblCNPJe</span></P></TD></TR><TR><TD class='0034td_label' style='WIDTH: 758px' colSpan='2'><P align='left'><FONT face='Arial' color='#333399' size='1'>Nome do Sub-Estipulante</FONT></P></TD>	<TD class='0034td_label' style='WIDTH: 163px'><FONT face='Arial' color='#333399' size='1'>CNPJ</FONT></TD></TR><TR>	<TD class='0034td_dado' style='WIDTH: 758px' colSpan='2'><P align='left'><span id='lblNomeSubEstipulante' class='DataLabels' style='width:260px;'>BD_lblNomeSubEstipulante</span></P></TD><TD class='0034td_dado' style='WIDTH: 163px'><P align='left'><span id='lblCNPJsub_e' class='DataLabels' style='width:102px;'>BD_lblCNPJsub_e</span></P></TD></TR></TBODY></TABLE></TD></TR><TR><TD style='WIDTH: 90%'><TABLE cellSpacing='0' cellPadding='1' style='WIDTH: 90%;'><TBODY><TR><TD class='0034td_label' style='WIDTH: 703px;' colSpan='2'><FONT face='Arial' size='1'>	<P align='left'><FONT face='Arial' color='#333399' size='1'>Dados do Segurado</FONT></P></FONT>	</TD></TR><TR><TD class='0034td_dado' style='WIDTH: 703px;' width='789' valign='top'><FONT face='Arial' color='#333399' size='1'>Nome:</FONT><span id='lblSeg_Nome' class='DataLabels' style='width:336px;'>BD_lblSeg_Nome</span><BR></FONT><FONT face='Arial' color='#333399' size='1'><BR>Data de Nascimento:</FONT><span id='lblSeg_DataNasc' class='DataLabels' style='width:102px;'>BD_lblSeg_DataNasc</span></TD><TD class='0034td_dado' width='470' valign='top'><P align='left'><FONT face='Arial' color='#333399' size='1'>CPF:</FONT><span id='lblSeg_cpf' class='DataLabels' style='width:106px;'>BD_lblSeg_cpf</span></P></TD></TR></TBODY></TABLE><TABLE style='WIDTH: 90%; ' cellSpacing='0' cellPadding='1'>	<TBODY><TR><TD class='0034td_label' style='WIDTH: 703px'><FONT face='Arial' color='#333399' size='1'>Dados do Seguro</FONT></TD></TR><TR><TD class='0034td_dado' width='903' style='WIDTH: 703px'><span id='lblCobertura1' class='DataLabels' style='width:102px;'></span>BD_tabela_coberturas</TD></TR></TBODY></TABLE></TD></TR><TR><TD style='WIDTH: 90%'><TABLE cellSpacing='0' cellPadding='2' style='WIDTH: 100%;'><TBODY><TR><TD class='0034td_label' style='WIDTH: 703px;'><FONT face='Arial' color='#333399' size='1'>Pr�mio: R$</FONT></TD><TD class='0034td_label' style='WIDTH: 580px;'><FONT face='Arial' color='#333399' size='1'>&nbsp;Data de Emiss�o</FONT></TD></TR><TR><TD class='0034td_dado'><span id='lblPremio' class='DataLabels' style='width:102px;'>BD_lblPremio</span></TD><TD class='0034td_dado'><FONT face='Arial' size='1'>&nbsp;</FONT><span id='lblDataEmissao' class='DataLabels' style='width:102px;'>BD_lblDataEmissao</span></TD></TR></TBODY></TABLE></TD></TR><TR><TD style='WIDTH: 90%;'><TABLE cellSpacing='0' cellPadding='1' style='WIDTH: 100%; HEIGHT: 37px'>							<TBODY>								<TR>									<TD class='0034td_label' style='WIDTH: 655px; HEIGHT: 19px'><FONT face='Arial' color='#333399' size='1'>Corretor</FONT></TD>									<TD class='0034td_label' style='WIDTH: 1045px;HEIGHT: 19px'><FONT face='Arial' color='#333399' size='1'>C�digo 											SUSEP</FONT></TD>								</TR>								<TR>									<TD class='0034td_dado'><span id='lblCorretor' class='DataLabels' style='width:384px;'>BD_lblCorretor</span></TD>									<TD class='0034td_dado'><span id='lblSUSEP' class='DataLabels' style='width:152px;'>BD_lblSUSEP</span></TD>								</TR>							</TBODY>						</TABLE>					</TD>				</TR>				<TR>					<TD style='WIDTH: 100%; HEIGHT: 127px'>						<TABLE style='WIDTH: 100%; HEIGHT: 126px' cellSpacing='0' cellPadding='2'>							<TBODY>								<TR>									<TD class='0034td_label' style='WIDTH: 848px;'><FONT face='Arial' color='#333399' size='1'><BR>											Observa��es<BR>										</FONT>									</TD>									<TD class='0034td_label' style='HEIGHT: 17px'><FONT face='Arial' color='#333399' size='1'>Compahia 											de Seguros Alian�a do Brasil</FONT></TD>								</TR>								<TR>									<TD class='0034td_dado' style='WIDTH: 848px'><span id='lblOBS' class='DataLabels' style='width:152px;'>BD_lblOBS</span><BR>									</TD>									<TD class='0034td_dado' style='WIDTH: 640px; HEIGHT: 18px'><IMG src='files_print/assinatura.gif'></TD>								</TR>					</TD>				</TR>			</TBODY>		</TABLE>		</TD></TR>		<TR>			<TD style='WIDTH: 682px'>				<TABLE cellSpacing='0' cellPadding='2' border='0' style='WIDTH: 790px; HEIGHT: 301px'>					<TBODY>						<TR>							<TD>								<P class='western' style='MARGIN-BOTTOM: 0cm' align='left'><FONT style='FONT-SIZE: 8pt' size='1' color='#333399'><B><center>Resumo 												das Condi��es Gerais</center><BR></B></FONT></FONT><FONT face='Arial, sans-serif' color='#333399' size='1'>1. O presente seguro � regido pelas Condi��es Gerais e Especiais que fazem parte integrante da ap�lice em poder do Estipulante/Sub-Estipulante.<BR>										</FONT></FONT><FONT face='Arial, sans-serif'><FONT color='#333399' size='1'>2. O 											presente Certificado Individual substitui e anula os anteriores e foi emitido 											conforme as Condi��es Gerais e Particulares da ap�lice.<BR>										</FONT></FONT><FONT face='Arial, sans-serif'><FONT color='#333399' size='1'>3. O 											seguro a que se refere este Certificado Individual ser&aacute; renovado conforme 											disposto nas Condi��es Gerais e Particulares da ap�lice.<BR>										</FONT></FONT><FONT face='Arial, sans-serif'><FONT color='#333399' size='1'>4. 											Todas as comunica��es relativas ao seguro, inclusive altera��es e cancelamento, 											ser�o feitas diretamente ao Estipulante/Sub-Estipulante, como representante </FONT>									</FONT><FONT color='#333399'><FONT face='Arial, sans-serif' size='1'>legal do Segurado, 											conforme autoriza��o deste expressa na respectiva Proposta de Ades�o.<BR>										</FONT></FONT><FONT face='Arial, sans-serif'><FONT color='#333399' size='1'>5. A 											vig�ncia da cobertura individual ter&aacute; in�cio a partir das 24 (vinte e quatro) 											horas da data de ingresso do Segurado no grupo, atrav�s de formaliza��o do </FONT>									</FONT><FONT color='#333399'><FONT face='Arial, sans-serif' size='1'>Estipulante, desde 											que recebida e aceita a Proposta de Ades�o pela Seguradora. No caso de 											propostas recepcionadas com o pagamento do pr�mio total ou </FONT></FONT>									<FONT color='#333399'><FONT face='Arial, sans-serif' size='1'>parcial, o in�cio da 											cobertura ocorrer&aacute; �s 24 (vinte e quatro) horas da data de recep��o das 											Propostas de Ades�o pela sociedade Seguradora.<BR>										</FONT></FONT><FONT face='Arial, sans-serif'><FONT color='#333399' size='1'>6. A 											Proposta de Ades�o, preenchida de pr�prio punho pelo Segurado, datada e 											assinada, dever&aacute; obrigatoriamente ser entregue � Seguradora para an&aacute;lise de </FONT>									</FONT><FONT color='#333399'><FONT face='Arial, sans-serif' size='1'>aceita��o em at� 											45 dias da inclus�o do Segurado pelo Estipulante.<BR>										</FONT></FONT><FONT color='#333399'><FONT face='Arial, sans-serif' size='1'>7. O 											final de vig�ncia da cobertura individual ser&aacute; a data de t�rmino de vig�ncia da 											ap�lice, respeitando o disposto no item 10 deste Certificado Individual.<BR>										</FONT></FONT><FONT color='#333399'><FONT face='Arial, sans-serif' size='1'>8. Os 											benefici&aacute;rios do seguro s�o aqueles indicados pelo Segurado na proposta de 											ades�o no momento da contrata��o, podendo ser alterados mediante solicita��o </FONT>									</FONT><FONT color='#333399'><FONT face='Arial, sans-serif' size='1'>por escrito do 											segurado, ou na falta de indica��o conforme o artigo 792 do C�digo Civil 											Brasileiro de 11/01/2002 (metade ao c�njuge n�o separado judicialmente e </FONT>									</FONT><FONT color='#333399'><FONT face='Arial, sans-serif' size='1'>o restante aos 											herdeiros do segurado, obedecida a ordem de voca��o heredit&aacute;ria).											<BR>										</FONT></FONT><FONT face='Arial, sans-serif'><FONT color='#333399' size='1'>9. O 											pr�mio e os capitais segurados ser�o reajustados conforme estabelecido nas 											Condi��es Gerais da ap�lice.<BR>										</FONT></FONT><FONT color='#333399'><FONT face='Arial, sans-serif' size='1'>10. O 											seguro representado por este Certificado Individual cessar&aacute; automaticamente:<BR>										</FONT></FONT><FONT color='#333399'><FONT face='Arial, sans-serif' size='1'>- Com a 											n�o renova��o ou o cancelamento da ap�lice;<BR>										</FONT></FONT><FONT color='#333399'><FONT face='Arial, sans-serif' size='1'>- Com o 											desaparecimento do v�nculo entre o Segurado e Estipulante ou Sub-estipulantes, 											com exce��o dos segurados aposentados que tenham optado por </FONT></FONT>									<FONT color='#333399'><FONT face='Arial, sans-serif' size='1'>permanecer no referido 											seguro;<BR>										</FONT></FONT><FONT color='#333399'><FONT face='Arial, sans-serif' size='1'>- 											Quando o Segurado solicitar sua exclus�o da ap�lice ou quando o mesmo deixar de 											contribuir com sua parte do pr�mio; e											<BR>										</FONT></FONT><FONT color='#333399'><FONT face='Arial, sans-serif' size='1'>- 											Conforme demais defini��es estabelecidas nas Condi��es Gerais da ap�lice.<BR>											<BR>										</FONT></FONT><FONT color='#333399'><FONT face='Arial, sans-serif' size='1'>11. 											Este seguro � por tempo determinado, tendo a seguradora a faculdade de n�o 											renovar a ap�lice na data de vencimento, sem devolu��o dos pr�mios pagos nos </FONT>									</FONT><FONT color='#333399'><FONT face='Arial, sans-serif' size='1'>termos da ap�lice.</FONT></FONT>								<P></P>								<P style='MARGIN-BOTTOM: 0cm' align='center'>									<FONT color='#333399'><FONT face='Arial' size='1'><B>Companhia de Seguros Alian�a do Brasil 												- Rua Manuel da N�brega, 1280 - 9� andar - 04001-004 - S�o Paulo - SP - 												C.N.P.J.: 28.196.889/0001-43</B></FONT></FONT></P>								<P style='MARGIN-BOTTOM: 0cm' align='center'><FONT color='#333399'> <FONT style='FONT-SIZE: 5pt' face='Arial'><B><FONT size='1'>Central de Atendimento 0800 729 7000 - </FONT><A href='http://www.aliancadobrasil.com.br' style='page-break-after: always'><FONT size='1'>www.aliancadobrasil.com.br</FONT></A></B></FONT></FONT></P></TD></TR></FORM></TBODY></TABLE></TD></TR></TBODY></TABLE>"

            Try
                Dim temp As IO.FileStream = IO.File.OpenRead(Request.PhysicalApplicationPath() & "template.htm")
                Dim content As IO.StreamReader = New IO.StreamReader(temp)
                Dim str As String

                str = content.ReadToEnd

                temp.Close()
                content.Close()
                temp = Nothing
                content = Nothing

                repeatHTML = repeatHTML & str

            Catch ex As System.Exception
                Dim excp As New clsException(ex)
            End Try


            firstHTML = firstHTML & repeatHTML
            If totalVidas = 1 Then
                firstHTML = firstHTML.Replace("page-break-after:always", "page-break-after:avoid")
            End If
            'firstHTML = firstHTML.Replace("style='PAGE-BREAK-BEFORE:always'", "")




            'Primeira amostragem do relat�rio
            IniciaEtapas(clientes_id(0))
            Me.div.InnerHtml = SubstituiFirstHTML(firstHTML, tb_coberturas) 'firstHTML
            'Me.div.InnerHtml &= QuebraLinha(54 - 8)
            'Limpa Tabela
            tb_coberturas = ""

            'Demais amonstragens do relat�rio
            For x = 1 To totalVidas - 1
                IniciaEtapas(clientes_id(x))

                'If x = totalVidas - 1 Then
                '    repeatHTML = repeatHTML.Replace("id=""tabelaPrincipal"" style=""PAGE-BREAK-AFTER:always""", "id=""tabelaPrincipal""")
                'End If
                repeatHTML = repeatHTML.Replace("style='page-break-before:always'", "style='page-break-before:always'")
                repeatHTML = repeatHTML.Replace("style='PAGE-BREAK-BEFORE:always'", "style='page-break-before:always'")
                If x = totalVidas - 1 Then
                    repeatHTML = repeatHTML.Replace("class=""folha""", "")
                    'repeatHTML = repeatHTML & "<div style=""PAGE-BREAK-AFTER:avoid""/>"
                End If
                ' Style = "page-break-after:always"
                'Me.div.InnerHtml &= "<br><br><br><br><br><br>"
                Me.div.InnerHtml &= SubstituiRepeatHTML(repeatHTML, tb_coberturas)

                tb_coberturas = ""
            Next

        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Sub BuscaCertificadoID(ByVal cliente_id As Integer)

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader
        Dim cpf As String = ""

        bd.BuscarDadosCliente(cliente_id)

        Try
            dr = bd.ExecutaSQL_DR()

            If dr.Read() Then
                If Not IsDBNull(dr.Item("cpf")) Then
                    cpf = "'" & dr.Item("cpf").ToString & "'"
                End If
            End If

        Catch ex As System.Exception

        End Try

        Try
            campos.CertificadoID = cUtilitarios.getNumCertificado(cpf, Session("usuario"))

        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Function SubstituiFirstHTML(ByVal var As String, ByVal tblCoberturas As String) As String
        Try
            var = var.Replace("BD_lblNumeroCertificado", campos.CertificadoID)
            var = var.Replace("BD_lblIni_vg_subgrp", campos.Ini_vg_subgrp)
            var = var.Replace("BD_lblFim_vg_subgrp", campos.Fim_vg_subgrp)
            var = var.Replace("BD_lblFim_vg_apo", campos.Fim_vg_apo)
            var = var.Replace("BD_lblFim_vg_apo", campos.Fim_vg_apo)
            var = var.Replace("BD_lblTitulo", campos.Titulo)
            var = var.Replace("BD_lblProcessoSusep", campos.ProcessoSusep)
            var = var.Replace("BD_lblRamo", campos.Ramo)
            var = var.Replace("BD_lblNumApolice", campos.NumApolice)
            var = var.Replace("BD_lblIni_vg_apo", campos.Ini_vg_apo)
            var = var.Replace("BD_lblFim_vg_apo", campos.Fim_vg_apo)
            var = var.Replace("BD_lblNomeEstipulante", campos.NomeEstipulante)
            var = var.Replace("BD_lblCNPJe", campos.CNPJe)
            var = var.Replace("BD_lblNomeSubEstipulante", campos.NomeSubEstipulante)
            var = var.Replace("BD_lblCNPJsub_e", campos.CNPJsub_e)
            var = var.Replace("BD_lblSeg_Nome", campos.Seg_Nome)
            var = var.Replace("BD_lblSeg_DataNasc", campos.Seg_DataNasc)
            var = var.Replace("BD_lblSeg_cpf", campos.Seg_cpf)
            'Substitui��o das coberturas
            var = var.Replace("BD_tabela_coberturas", tblCoberturas)
            'var = var.Replace("BD_IEA_min", campos.IEA_min)
            'var = var.Replace("BD_IEA_max", campos.IEA_max)
            'var = var.Replace("BD_IPA_min", campos.IPA_min)
            'var = var.Replace("BD_IPA_max", campos.IPA_max)
            'var = var.Replace("BD_IPD_min", campos.IPD_min)
            'var = var.Replace("BD_IPD_max", campos.IPA_max)
            'var = var.Replace("BD_MNA_min", campos.MNA_min)
            'var = var.Replace("BD_MNA_max", campos.MNA_max)
            '----------------------------------------------------------
            var = var.Replace("BD_lblPremio", campos.Premio)
            var = var.Replace("BD_lblDataEmissao", campos.DataEmissao)
            var = var.Replace("BD_lblCorretor", campos.Corretor)
            var = var.Replace("BD_lblSUSEP", campos.SUSEP)
            'In�cio - 04/05/2020 - Demanda SD02165895 - Cleiton Queiroz - Confitec SP
            Dim culture As New CultureInfo("pt-BR")
            Dim formataData As DateTimeFormatInfo = culture.DateTimeFormat
            Dim dia As String = Now.Day.ToString()
            Dim mes_ext As String = culture.TextInfo.ToTitleCase(formataData.GetMonthName(Now.Month))
            Dim ano As String = Now.Year.ToString()

            var = var.Replace("BD_lblOBS", "S�o Paulo, " & dia & " de " & mes_ext & " de " & ano & "<br>" & campos.Observacoes)
            'Fim - 04/05/2020 - Demanda SD02165895 - Cleiton Queiroz - Confitec SP
            Return var
        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        End Try

    End Function

    Private Function SubstituiRepeatHTML(ByVal var As String, ByVal tblCoberturas As String) As String
        Try
            var = var.Replace("BD_lblNumeroCertificado", campos.CertificadoID)
            var = var.Replace("BD_lblTitulo", campos.Titulo)
            var = var.Replace("BD_lblProcessoSusep", campos.ProcessoSusep)
            var = var.Replace("BD_lblRamo", campos.Ramo)
            var = var.Replace("BD_lblNumApolice", campos.NumApolice)
            var = var.Replace("BD_lblIni_vg_subgrp", campos.Ini_vg_subgrp)
            var = var.Replace("BD_lblFim_vg_subgrp", campos.Fim_vg_subgrp)
            var = var.Replace("BD_lblIni_vg_apo", campos.Ini_vg_apo)
            var = var.Replace("BD_lblFim_vg_apo", campos.Fim_vg_apo)
            var = var.Replace("BD_lblNomeEstipulante", campos.NomeEstipulante)
            var = var.Replace("BD_lblCNPJe", campos.CNPJe)
            var = var.Replace("BD_lblNomeSubEstipulante", campos.NomeSubEstipulante)
            var = var.Replace("BD_lblCNPJsub_e", campos.CNPJsub_e)
            var = var.Replace("BD_lblSeg_Nome", campos.Seg_Nome)
            var = var.Replace("BD_lblSeg_DataNasc", campos.Seg_DataNasc)
            var = var.Replace("BD_lblSeg_cpf", campos.Seg_cpf)
            'Substitui��o das coberturas
            var = var.Replace("BD_tabela_coberturas", tblCoberturas)
            'var = var.Replace("BD_IEA_min", campos.IEA_min)
            'var = var.Replace("BD_IEA_max", campos.IEA_max)
            'var = var.Replace("BD_IPA_min", campos.IPA_min)
            'var = var.Replace("BD_IPA_max", campos.IPA_max)
            'var = var.Replace("BD_IPD_min", campos.IPD_min)
            'var = var.Replace("BD_IPD_max", campos.IPA_max)
            'var = var.Replace("BD_MNA_min", campos.MNA_min)
            'var = var.Replace("BD_MNA_max", campos.MNA_max)
            '----------------------------------------------------------
            var = var.Replace("BD_lblPremio", campos.Premio)
            var = var.Replace("BD_lblDataEmissao", campos.DataEmissao)
            var = var.Replace("BD_lblCorretor", campos.Corretor)
            var = var.Replace("BD_lblSUSEP", campos.SUSEP)
            'In�cio - 04/05/2020 - Demanda SD02165895 - Cleiton Queiroz - Confitec SP
            Dim culture As New CultureInfo("pt-BR")
            Dim formataData As DateTimeFormatInfo = culture.DateTimeFormat
            Dim dia As String = Now.Day.ToString()
            Dim mes_ext As String = culture.TextInfo.ToTitleCase(formataData.GetMonthName(Now.Month))
            Dim ano As String = Now.Year.ToString()

            var = var.Replace("BD_lblOBS", "S�o Paulo, " & dia & " de " & mes_ext & " de " & ano & "<br>" & campos.Observacoes)
            'Fim - 04/05/2020 - Demanda SD02165895 - Cleiton Queiroz - Confitec SP
            Return var
        Catch ex As System.Exception
            Dim excp As New clsException(ex)
        End Try


    End Function

    Private Function QuebraLinha(ByVal total As Integer) As String
        Dim linhas As String = ""

        For total = 0 To total
            linhas &= "<br>" ' & total & ""
        Next
        Return linhas

    End Function

End Class
