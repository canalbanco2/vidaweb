Public Class CarregarVidas
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblVigencia As System.Web.UI.WebControls.Label
    Protected WithEvents lblNavegacao As System.Web.UI.WebControls.Label
    Protected WithEvents btnFatura As System.Web.UI.WebControls.Button
    Protected WithEvents btnRelacaoVidas As System.Web.UI.WebControls.Button
    Protected WithEvents btnResumo As System.Web.UI.WebControls.Button
    Protected WithEvents pnlConfirma As System.Web.UI.WebControls.Panel
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button
    Protected WithEvents pnlAlerta As System.Web.UI.WebControls.Panel
    Protected WithEvents lblListagemEnviada As System.Web.UI.WebControls.Label
    Protected WithEvents lblPeriodo As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim linkseguro As Alianca.Seguranca.Web.LinkSeguro

        'Implementa��o da SABL0101
        linkseguro = New Alianca.Seguranca.Web.LinkSeguro
        Dim usuario As String = linkseguro.LerUsuario("", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))
        If usuario = "" Then
            Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
        End If

        Session("usuario_id") = linkseguro.Usuario_ID
        Session("usuario") = linkseguro.Login_REDE
        Session("cpf") = linkseguro.CPF

        Dim cAmbiente As Alianca.Seguranca.Web.ControleAmbiente
        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"

        cAmbiente.ObterAmbiente(url)
        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
        Else
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
        End If

        Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)

        For Each m As String In Request.QueryString.Keys
            Session(m) = Request.QueryString(m)
        Next

        Dim periodo As String = getPeriodoCompetenciaSession()

        'lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Subgrupo: " & Session("nomeSubgrupo") & " -> " & "Fun��o: Carregar rela��o anterior"
        lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomesubGrupo") & "<br>Fun��o: Carregar rela��o anterior"

        lblVigencia.Text = "<br>" & Session("nomeSubgrupo") & " - Per�odo de Compet�ncia: " & periodo
        Me.lblPeriodo.Text = periodo
        lblVigencia.Visible = True
        Me.Button1.Attributes.Add("onclick", "top.escondeaguarde();")
        'btnImprimeCertificado.Attributes.Add("onclick", "abre_janela('Impressao.aspx?tipo=certificado','janela_msg','no','yes','500','400','','')")


        ' Gabriel Vieira (23/02/2011) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        ' Remo��o de restri��o para o perfil de acesso 6.

        ' Projeto 539202 - Jo�o Ribeiro - 29/04/2009
        'If Session("acesso") = "6" Then
        '    Button1.Enabled = False
        'End If
        ' Projeto 539202 - Fim
        ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        pnlAlerta.Visible = True
        pnlConfirma.Visible = False

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)
        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)
        Dim data_inicio, data_fim, wf_id As String

        Try

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            If dr.HasRows Then
                While dr.Read()
                    wf_id = dr.GetValue(0).ToString()
                    data_inicio = cUtilitarios.trataDataDB(dr.GetValue(1).ToString().Split(" ")(0))
                    data_fim = cUtilitarios.trataDataDB(dr.GetValue(2).ToString().Split(" ")(0))
                End While
            End If
            dr.Close()
            dr = Nothing

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try


        Try

            bd.SEGS5699_SPI(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"), wf_id, data_inicio, data_fim, "'Carregada a rela��o de vida anterior'", "'SEGW0078'", Session("usuario"))

            bd.ExecutaSQL()

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Try

            bd.SEGS5720_SPD(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

            bd.ExecutaSQL()

            Me.montaFaturaAtual(wf_id)

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Function getPeriodoCompetenciaSession() As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)
        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

        Dim data_inicio, data_fim, retorno As String

        Try

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()


            If dr.HasRows Then
                While dr.Read()

                    data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                    data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)

                End While
            End If
            dr.Close()
            dr = Nothing
            retorno = data_inicio & " a " & data_fim

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return retorno
    End Function

    Private Sub montaFaturaAtual(ByVal wf_id As Integer)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)
        Dim dr As Data.SqlClient.SqlDataReader
        'Mostra Resumo da Fatura Atual
        Dim totalVidaFA As Integer, totalCapitalFA As Double, totalPremioFA As Double
        Try
            bd.ResumoFaturaAtual_Anterior(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Session("usuario"), wf_id)

            dr = bd.ExecutaSQL_DR

            If dr.Read() Then

                totalVidaFA = dr.GetValue(8).ToString
                totalCapitalFA = cUtilitarios.trataMoeda(dr.GetValue(6).ToString)
                totalPremioFA = cUtilitarios.trataMoeda(dr.GetValue(7).ToString)

            End If
            dr.Close()

            'variavel que atualiza na segw0060, o Resumo da Fatura Atual
            Dim script As String = "<script>"


            bd.ResumoFaturaAtual_IAE(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Session("usuario"), wf_id)
            dr = bd.ExecutaSQL_DR            

            Dim totalVida As Integer, totalCapital As Double, totalPremio As Double

            script &= "top.document.getElementById('divInclusoes_vidas').innerHTML= '0';" & vbCrLf
            script &= "top.document.getElementById('divInclusoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
            script &= "top.document.getElementById('divInclusoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf

            script &= "top.document.getElementById('divExclusoes_vidas').innerHTML= '0';" & vbCrLf
            script &= "top.document.getElementById('divExclusoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
            script &= "top.document.getElementById('divExclusoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf

            script &= "top.document.getElementById('divAlteracoes_vidas').innerHTML= '0';" & vbCrLf
            script &= "top.document.getElementById('divAlteracoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
            script &= "top.document.getElementById('divAlteracoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf

            script &= "top.document.getElementById('divAcertos_vidas').innerHTML= '0';" & vbCrLf
            script &= "top.document.getElementById('divAcertos_capital').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
            script &= "top.document.getElementById('divAcertos_premio').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf

            script &= "top.document.getElementById('divDPS_vidas').innerHTML= '0';" & vbCrLf
            script &= "top.document.getElementById('divDPS_capital').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
            script &= "top.document.getElementById('divDPS_premio').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf

            script &= "top.document.getElementById('divExcAgen_vidas').innerHTML= '0';" & vbCrLf
            script &= "top.document.getElementById('divExcAgen_capital').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
            script &= "top.document.getElementById('divExcAgen_premio').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf

            script &= "top.document.getElementById('divVidasFT').innerHTML= '" & totalVida + totalVidaFA & "';" & vbCrLf
            script &= "top.document.getElementById('divCapitalFT').innerHTML= '" & cUtilitarios.trataMoeda(totalCapital + totalCapitalFA) & "';" & vbCrLf
            script &= "top.document.getElementById('divPremioFT').innerHTML= '" & cUtilitarios.trataMoeda(totalPremio + totalPremioFA) & "';" & vbCrLf

            script &= "top.document.getElementById('divSubTotal_vidas').innerHTML= '0';" & vbCrLf
            script &= "top.document.getElementById('divSubTotal_capital').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
            script &= "top.document.getElementById('divSubTotal_premio').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf

            Dim qtdVidasSub As Integer = Int32.Parse(totalVidaFA)
            Dim valCapSubTotal As Double = Double.Parse(totalCapitalFA)
            Dim valPremioSubTotal As Double = Double.Parse(totalPremioFA)

            While dr.Read()
                If dr.Item("operacao") = "I" Then

                    Try
                        qtdVidasSub = qtdVidasSub + Int32.Parse(dr.GetValue(0))
                        valCapSubTotal = valCapSubTotal + Double.Parse(dr.GetValue(1))
                        valPremioSubTotal = valPremioSubTotal + Double.Parse(dr.GetValue(2))
                    Catch ex As Exception

                    End Try

                    script &= "top.document.getElementById('divInclusoes_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                    script &= "top.document.getElementById('divInclusoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                    script &= "top.document.getElementById('divInclusoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf
                    'Soma
                    totalVida = totalVida + dr.GetValue(0).ToString
                    totalCapital = totalCapital + cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                    totalPremio = totalPremio + cUtilitarios.trataMoeda(dr.GetValue(2).ToString)

                ElseIf dr.Item("operacao") = "E" Then

                    Try
                        qtdVidasSub = qtdVidasSub - Int32.Parse(dr.GetValue(0))
                        valCapSubTotal = valCapSubTotal - Double.Parse(dr.GetValue(1))
                        valPremioSubTotal = valPremioSubTotal - Double.Parse(dr.GetValue(2))
                    Catch ex As Exception

                    End Try

                    script &= "top.document.getElementById('divExclusoes_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                    If cUtilitarios.trataMoeda(dr.GetValue(2).ToString) = "0,00" Then
                        script &= "top.document.getElementById('divExclusoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
                        script &= "top.document.getElementById('divExclusoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(0) & "';" & vbCrLf
                    Else
                        script &= "top.document.getElementById('divExclusoes_capital').innerHTML= '-" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                        script &= "top.document.getElementById('divExclusoes_premio').innerHTML= '-" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf
                    End If
                    'Subtrai
                    totalVida = totalVida - dr.GetValue(0).ToString
                    totalCapital = totalCapital - cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                    totalPremio = totalPremio - cUtilitarios.trataMoeda(dr.GetValue(2).ToString)

                ElseIf dr.Item("operacao") = "A" Then
                    script &= "top.document.getElementById('divAlteracoes_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                    script &= "top.document.getElementById('divAlteracoes_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                    script &= "top.document.getElementById('divAlteracoes_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf
                ElseIf dr.Item("operacao") = "P" Then
                    script &= "top.document.getElementById('divDPS_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                    script &= "top.document.getElementById('divDPS_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                    script &= "top.document.getElementById('divDPS_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf
                ElseIf dr.Item("operacao") = "B" Then
                    'script &= "top.document.getElementById('divExcAgendada_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                    'script &= "top.document.getElementById('divExcAgendada_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                    'script &= "top.document.getElementById('divExcAgendada_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf
                    script &= "top.document.getElementById('divExcAgen_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                    script &= "top.document.getElementById('divExcAgen_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                    script &= "top.document.getElementById('divExcAgen_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf
                ElseIf dr.Item("operacao") = "R" Then
                    script &= "top.document.getElementById('divAcertos_vidas').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                    script &= "top.document.getElementById('divAcertos_capital').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                    script &= "top.document.getElementById('divAcertos_premio').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf
                ElseIf dr.Item("operacao") = "T" Then
                    script &= "top.document.getElementById('divVidasFT').innerHTML= '" & dr.GetValue(0).ToString & "';" & vbCrLf
                    script &= "top.document.getElementById('divCapitalFT').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "';" & vbCrLf
                    script &= "top.document.getElementById('divPremioFT').innerHTML= '" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "';" & vbCrLf
                End If

                'totalVida = totalVida + dr.GetValue(0).ToString
                'totalCapital = totalCapital + cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                'totalPremio = totalPremio + cUtilitarios.trataMoeda(dr.GetValue(3).ToString)

                'script &= "top.document.getElementById('divVidasFT').innerHTML= '" & totalVida + totalVidaFA & "';" & vbCrLf
                'script &= "top.document.getElementById('divCapitalFT').innerHTML= '" & cUtilitarios.trataMoeda(totalCapital + totalCapitalFA) & "';" & vbCrLf
                'script &= "top.document.getElementById('divPremioFT').innerHTML= '" & cUtilitarios.trataMoeda(totalPremio + totalPremioFA) & "';" & vbCrLf

            End While

            script &= "top.document.getElementById('divSubTotal_vidas').innerHTML= '" & qtdVidasSub & "';" & vbCrLf
            script &= "top.document.getElementById('divSubTotal_capital').innerHTML= '" & cUtilitarios.trataMoeda(valCapSubTotal) & "';" & vbCrLf
            script &= "top.document.getElementById('divSubTotal_premio').innerHTML= '" & cUtilitarios.trataMoeda(valPremioSubTotal) & "';" & vbCrLf

            Response.Write(script & "var teste = 2; </script>")

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
        '----------------------------------------------
        dr.Close()
        dr = Nothing
    End Sub

End Class
