Public Class cUtilitarios

    'Trata a data para o seguinte formato dd/mm/aaaa hh:mm
    Public Shared Function trataDataHora(ByVal data As String) As String

        Try
            Dim dt_final As String

            Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim ano As String = CType(data, DateTime).Year

            Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = dia + "/" + mes + "/" + ano


            Return dt_final + " " + hora + ":" + minuto

        Catch ex As Exception

            Return ""

        End Try

    End Function

    'Trata a data para o seguinte formato dd/mm/aaaa 
    Public Shared Function trataData(ByVal data As String) As String

        Try

            Dim dt_arr() As String = data.Split("/")
            Dim dt_final As String

            'Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim dia As String = dt_arr(0).PadLeft(2, "0")
            'Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim mes As String = dt_arr(1).PadLeft(2, "0")
            'Dim ano As String = CType(data, DateTime).Year
            Dim ano As String = dt_arr(2)

            'Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            'Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = dia + "/" + mes + "/" + ano


            Return dt_final

        Catch ex As Exception

            Return ""

        End Try

    End Function

    'Trata a data para o seguinte formato dd/mm/aaaa 
    Public Shared Function trataDataDB(ByVal data As String) As String

        Try

            Dim dt_arr() As String = data.Split("/")
            Dim dt_final As String

            'Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim dia As String = dt_arr(0).PadLeft(2, "0")
            'Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim mes As String = dt_arr(1).PadLeft(2, "0")
            'Dim ano As String = CType(data, DateTime).Year
            Dim ano As String = dt_arr(2)

            'Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            'Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = ano + mes + dia


            Return dt_final

        Catch ex As Exception

            Return ""

        End Try

    End Function

    Public Shared Function trataSmalldate(ByVal data As String) As String

        Try
            Dim dt_final As String

            Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim ano As String = CType(data, DateTime).Year

            Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = ano + mes + dia


            Return dt_final + " " + hora + ":" + minuto

        Catch ex As Exception

            Return ""

        End Try

    End Function

    '''Exibe o valor em um alert na tela
    Public Shared Sub br(ByVal valor As String)

        Dim texto As String

        texto = "<script>" + vbNewLine
        texto &= "alert(""" + valor + """);"
        texto &= "</script>"

        System.Web.HttpContext.Current.Response.Write(texto)
    End Sub

    Public Shared Function destrataCPF(ByVal cpf As String) As String

        If cpf.Length = 14 Then
            Return cpf.Replace(".", "").Replace("-", "")
        Else
            Return cpf
        End If

    End Function

    Public Shared Function trataCPF(ByVal cpf As String) As String

        If cpf.Length = 11 Then
            Return cpf.Substring(0, 3) + "." + cpf.Substring(3, 3) + "." + cpf.Substring(6, 3) + "-" + cpf.Substring(9, 2)
        Else
            Return cpf
        End If

    End Function

    Public Shared Function trataMoeda(ByVal valor As String) As String

        If valor = "&nbsp;" Or valor = "" Then
            Return ""
        Else
            Return String.Format(String.Format("{0:c}", CDbl(valor))).Substring(3)
        End If

    End Function

End Class
