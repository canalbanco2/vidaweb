
Public Class cAcompanhamento

#Region "Heran�a"
    Inherits cBanco
#End Region


    'Busca o Resumo da Fatura atual (anterior) da tela de abertura do Vida
    Public Sub ResumoFaturaAtual_Anterior(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal usuario As String, ByVal wf_id As Int64)

        'Busca Resumo da Fatura Atual Anterior
        SQL = "exec vida_web_db.dbo.SEGS5732_SPS "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @subgrupo_id=" & subgrupo_id
        SQL &= ", @usuario=" & trStr(usuario)
        SQL &= ", @wf_id = " & wf_id

    End Sub

    'Busca o Resumo da Fatura atual (Inclusoes, Altera��es e Exclusoes) da tela de abertura do Vida
    Public Sub ResumoFaturaAtual_IAE(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal usuario As String, ByVal wf_id As Int64)

        'Busca Resumo da Fatura Atual: IAE
        SQL = "exec vida_web_db.dbo.SEGS5733_SPS "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @subgrupo_id=" & subgrupo_id
        SQL &= ", @usuario=" & trStr(usuario)
        SQL &= ", @wf_id = " & wf_id

    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String)

        SQL = "exec vida_web_db.dbo.SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @cpf = " & cpf
        SQL &= ", @nome = " & nome

    End Sub
    Public Sub SEGS5696_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal workflow As Integer, ByVal valor_id As Integer)
        ' Gabriel Vieira (23/02/2011) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        ' Corre��o do comando SQL.
        SQL = "exec vida_web_db.dbo.SEGS5696_SPS @apolice_id = " & apolice_id
        ' SQL = "exec segs5696_sps @apolice_id = " & apolice_id
        ' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @tp_wf_id = " & workflow
        SQL &= ", @tp_versao_id = " & valor_id
    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String, ByVal id As Integer)

        SQL = "exec vida_web_db.dbo.SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @cpf = " & cpf
        SQL &= ", @nome = " & nome
        SQL &= ", @id = " & id
    End Sub

    Public Sub SEGS5653_SPI(ByVal wf_id As Integer)

        SQL = "exec [sisab003].seguros_db.dbo.SEGS5653_SPI @wf_id = " & wf_id

    End Sub


    Public Sub SEGS5393_SPI(ByVal wf_id As Integer, ByVal tp_wf_id As Integer, ByVal tp_tarefa_id As Integer, ByVal tp_ativ_id As Integer)

        SQL = "exec [sisab003].seguros_db.dbo.SEGS5393_SPI "
        SQL &= " @wf_id = " & wf_id
        SQL &= ", @tp_wf_id = " & tp_wf_id
        SQL &= ", @tp_tarefa_id = " & tp_tarefa_id
        SQL &= ", @tp_ativ_id = " & tp_ativ_id

    End Sub

    Public Sub executar_atividade_lote_spu()
        SQL = " exec [sisab003].workflow_db.dbo.executar_atividade_lote_spu "
    End Sub

    '''Consulta as condi��es do subgrupo
    Public Sub SEGS5705_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)

        SQL = "exec vida_web_db.dbo.SEGS5705_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub

    '''Consulta as condi��es do subgrupo
    Public Sub SEGS5706_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)

        SQL = "exec vida_web_db.dbo.SEGS5706_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub

    Public Sub SEGS5699_SPI(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer, ByVal wf_id As Integer, ByVal dt_inicio_competencia As String, ByVal dt_fim_competencia As String, ByVal desc_historico As String, ByVal aplicacao As String, ByVal usuario As String)

        SQL = "exec vida_web_db.dbo.SEGS5699_SPI "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @wf_id = " & subgrupo
        SQL &= ", @dt_inicio_competencia = '" & dt_inicio_competencia & "'"
        SQL &= ", @dt_fim_competencia = '" & dt_fim_competencia & "'"
        SQL &= ", @desc_historico = " & desc_historico
        SQL &= ", @aplicacao = " & aplicacao
        SQL &= ", @usuario = " & usuario

    End Sub

    Public Sub SEGS5720_SPD(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)

        SQL = "exec vida_web_db.dbo.SEGS5720_SPD "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo

    End Sub



    Public Sub New(Optional ByVal banco As cDadosBasicos.eBanco = cDadosBasicos.eBanco.web_seguros_db)
        MyBase.New(banco)

        Alianca.Seguranca.BancoDados.cCon.BancodeDados = banco.ToString

    End Sub

    Function trInt(ByVal valor As Object) As String

        If valor = 0 Then
            Return "null"
        Else
            Return valor
        End If

    End Function

    Function trStr(ByVal valor As Object) As String

        If valor = "" Then
            Return "null"
        Else
            Return "'" & valor.ToString.Replace("'", "''") & "'"
        End If

    End Function

End Class
