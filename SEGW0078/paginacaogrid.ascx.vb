Imports System.Drawing
Imports System.Web.UI.WebControls
Imports System.Data

Public Class paginacaogrid
    Inherits System.Web.UI.UserControl


    Public _valor As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents a1 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents a2 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents a3 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents a4 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents a5 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents a6 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents a7 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lblPaginacao As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub

    Public Event ePaginaAlterada(ByVal Argumento As String)
    Public Event ePaginaAlteradaA(ByVal Argumento As String)

    Private Sub mDisparaEvento(ByVal Argumento As String)
        RaiseEvent ePaginaAlterada(Argumento)
    End Sub
    Private Sub mDisparaEventoA(ByVal Argumento As String)
        RaiseEvent ePaginaAlteradaA(Argumento)
    End Sub

    Private Sub mPaginacaoClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles a1.Command, a2.Command, a3.Command, a4.Command, a5.Command, a6.Command, a7.Command
        mDisparaEvento(e.CommandArgument)
    End Sub

    Private Sub aPaginacaoClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles a1.Command, a2.Command, a3.Command, a4.Command, a5.Command, a6.Command, a7.Command
        mDisparaEventoA(e.CommandArgument)
    End Sub

    Private Sub mMontaPaginacao(ByVal GridPaginaAtual As Integer, ByVal GridPaginasTotal As Integer)
        Dim paginainicial As Integer
        Dim paginafinal As Integer
        Dim paginaatual As Integer = GridPaginaAtual + 1

        If (paginaatual / 5) > 0 Then
            If (paginaatual Mod 5) <> 0 Then
                paginainicial = (Int(paginaatual / 5) * 5) + 1
            Else
                paginainicial = (Int((paginaatual - 1) / 5) * 5) + 1
            End If
            paginafinal = paginainicial + 4
            If paginafinal > GridPaginasTotal Then paginafinal = GridPaginasTotal
        Else
            paginainicial = 1
            paginafinal = 5
            If paginafinal > GridPaginasTotal Then paginafinal = GridPaginasTotal
        End If

        Me.a1.Visible = False
        Me.a2.Visible = False
        Me.a3.Visible = False
        Me.a4.Visible = False
        Me.a5.Visible = False
        Me.a6.Visible = False
        Me.a7.Visible = False
        If paginaatual > 1 Then
            a1.Text = "Anterior"
            a1.ForeColor = Color.White
            a1.Style.Add("font-family", "verdana")
            a1.Style.Add("font-size", "10px")
            a1.Style.Add("font-weight", "bold")
            a1.Style.Add("text-decoration", "underline")
            a1.Visible = True
        End If
        Dim a As Integer = 2
        For i As Integer = paginainicial To paginafinal
            CType(Me.FindControl("a" + a.ToString), LinkButton).CommandArgument = (i - 1).ToString
            CType(Me.FindControl("a" + a.ToString), LinkButton).Text = i.ToString
            CType(Me.FindControl("a" + a.ToString), LinkButton).Visible = True
            If i = paginaatual Then
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("color", "white")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("text-decoration", "none")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-family", "verdana")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-size", "10px")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-weight", "bold")

            Else
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("color", "white;")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("text-decoration", "underline")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-family", "verdana")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-size", "10px")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-weight", "bold")

            End If
            a += 1
        Next
        If paginaatual <> GridPaginasTotal Then
            a7.Text = "Pr�xima"
            a7.ForeColor = Color.White
            a7.Style.Add("font-family", "verdana")
            a7.Style.Add("font-size", "10px")
            a7.Style.Add("font-weight", "bold")
            a7.Style.Add("text-decoration", "underline")
            a7.Visible = True
        End If
    End Sub

    Public Sub GridDataBind(ByVal Grid As DataGrid, ByVal dt As DataTable)
        Grid.DataSource = dt
        Grid.DataBind()

        Dim tipo As String
        Dim cell As Web.UI.WebControls.DataGridItem
        Dim pai As Web.UI.WebControls.DataGridItem
        Dim temp As Int16 = 0
        Dim count As Int16 = 0



        If Grid.Items.Count > 0 Then
            For Each x As Web.UI.WebControls.DataGridItem In Grid.Items

                If Not cell Is Nothing Then
                    If x.Cells.Item(4).Text <> "Titular" And cell.Cells.Item(4).Text = "Titular" Then
                        cell.Cells.Item(3).Text = "<img src='../segw0060/Images/imgMenos.gif' onclick=exibeDependentes('" + cell.Cells.Item(0).Text + "',this);>" & cell.Cells.Item(3).Text
                    End If
                End If

                cell = x
                If x.Cells.Item(4).Text = "Titular" Then
                    pai = x
                    x.ID = x.Cells.Item(0).Text

                    x.Attributes.Add("onmouseover", "this.style.background='#E8F1FF'")
                    x.Attributes.Add("onmouseout", "this.style.background=''")

                    count = 0
                Else
                    x.Cells.Item(3).Attributes.Add("style", "padding-left:20px;")
                    x.ID = pai.Cells.Item(0).Text + "_" + count.ToString
                    x.Attributes.Add("style", "background-color:#f5f5f5;")
                    x.Attributes.Add("onmouseover", "this.style.background='#E8F1FF'")
                    x.Attributes.Add("onmouseout", "this.style.background='#f5f5f5'")
                    count = count + 1
                End If

                x.Cells.Item(5).Text = cUtilitarios.trataCPF(x.Cells.Item(5).Text)
                x.Cells.Item(8).Text = cUtilitarios.trataMoeda(x.Cells.Item(8).Text).Trim
                x.Cells.Item(9).Text = cUtilitarios.trataMoeda(x.Cells.Item(9).Text).Trim
                x.Cells.Item(8).HorizontalAlign = HorizontalAlign.Right


                'x.Attributes.Add("onclick", "alteracoes('" & x.Cells.Item(0).Text & "', '" & x.Cells.Item(4).Text & "', '" & x.Cells.Item(3).Text & "','" + x.ID.Split("_")(0) + "')")
            Next
        End If
        Me.mMontaPaginacao(Grid.CurrentPageIndex, Grid.PageCount)
    End Sub

    Public Property CssClass() As String
        Get
            Return Me.lblPaginacao.Attributes.Item("class")
        End Get
        Set(ByVal Value As String)
            Me.lblPaginacao.Attributes.Add("class", Value)
        End Set
    End Property
End Class
