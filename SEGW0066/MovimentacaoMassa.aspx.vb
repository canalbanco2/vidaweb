Imports System.Reflection
Imports System.Data
Imports System.Web.UI

Partial Class MovimentacaoMassa
    Inherits System.Web.UI.Page

    Public dominio_sexo As New Collections.Specialized.StringDictionary
    Public dominio_operacao As New Collections.Specialized.StringDictionary
    Public dominio_vida As New Collections.Specialized.StringDictionary
    Protected WithEvents col10 As System.Web.UI.HtmlControls.HtmlGenericControl
    Public campos As New Collections.Specialized.ListDictionary
    Public DTIMPORT As DataTable

    ' pablo.dias (Nova Consultoria) - 12784293 - 11/09/2012
    Private valOperacaoExclusao As String = ""

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents pnlDependente As System.Web.UI.WebControls.Panel
    Protected WithEvents imgPlus As System.Web.UI.WebControls.ImageButton



    Protected WithEvents pnlConstrucao As System.Web.UI.WebControls.Panel

    Protected WithEvents ucPaginacao As paginacaogrid
    Dim sbuffer As New System.Text.StringBuilder



    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub

#End Region

    Dim bErro As Boolean
    Dim sMensagemErro As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '' '' '' ''Douglas desenvolvimento ------------------------------------
        '' '' ''If Not Page.IsPostBack Then
        '' '' ''    Session.Add("apolice", "17244")
        '' '' ''    Session.Add("ramo", "93")
        '' '' ''    Session.Add("subgrupo_id", "10")
        '' '' ''End If
        ' '' '' ''-------------------------------------------------------

        If bErro Then
            bErro = False
            sMensagemErro = ""
        End If

        Me.btnAcao.Attributes.Add("onclick", "try{top.exibeaguarde();}catch(err){}")

        txtLinhasDesconsiderar.Attributes.Add("onkeypress", "return Somente_Numero();")
        Trace.Write("Início do Page Load")

        'acintra 20080501 - início
        If hdf.Value = "1" Then
            hdf.Value = "2"
            Literal1.Text = ""

            If Session("Imp_Subgrupo") = "Todos" Then
                ApagarRegistroImportadoNaoProcessadoTodosSubgrupos(Session("apolice"), Session("ramo"))
            End If

            If Session("Imp_Subgrupo") = "Atual" Then
                ApagarRegistroImportadoNaoProcessado(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
            End If

            If bErro Then
                cUtilitarios.br(sMensagemErro)
            End If

            Me.Verifica_Sheets_Excel()

        ElseIf hdf.Value = "0" Then
            Literal1.Text = ""
        End If
        'acintra 20080501 - fim

        'Implementação do LinkSeguro
        Dim linkseguro As New Alianca.Seguranca.Web.LinkSeguro

        Dim usuario As String = linkseguro.LerUsuario("", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))


        '-------alterar ao terminar
        'If usuario = "" Then
        'Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
        'End If
        '--------------------------------------------------

        Session("usuario_id") = linkseguro.Usuario_ID
        '[New code 05.06.07 - Alterar login de rede p/ login_web]
        Session("usuario") = linkseguro.Login_WEB
        '[end code]
        Session("cpf") = linkseguro.CPF

        'Implementação do controle de ambiente
        Dim cAmbiente As New Alianca.Seguranca.Web.ControleAmbiente

        If Not Page.IsPostBack Then

            Session("indice") = 0
            For Each m As String In Request.QueryString.Keys
                If m <> "" Then
                    Session(m) = Request.QueryString(m)
                End If
            Next
        End If

        Try
            If Session("apolice") = "" Then
                cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
            End If
        Catch ex As Exception
            cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
            Response.End()
        End Try

        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
        cAmbiente.ObterAmbiente(url)
        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção)
        Else '
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção)
        End If

        pnlQualidade.Visible = True

        Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)

        If Not Page.IsPostBack Then

            'Inicio do desenvolvimento da página
            lblNavegacao.Text = "Ramo/Apólice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomeSubgrupo") & "<br>Função: Enviar arquivo para análise"

            lblVigencia.Text = getPeriodoCompetenciaSession()

            lblTipoMovimentacao.Attributes.Add("style", "cursor:default")
            pnlProcurar.Visible = True
            lblListagemEnviada.Visible = False
            btnImportar.Attributes.Add("onclick", "return confirmaModelo()")
            btnConfirmar.Attributes.Add("onclick", "try{top.exibeaguarde();}catch(err){}")

            txtLinhasDesconsiderar.Text = 1

        End If

        Trace.Write("Fim do Page Load")
    End Sub

    Private Sub btnEnviarListagem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pnlModelodoArquivo.Visible = False
        pnlProcurar.Visible = False
        pnlTipodeMovimentacao.Visible = False
        pnlMensagemEnviada.Visible = True
        rdIncremental.Checked = False
        rdSobreposicao.Checked = False
        cUtilitarios.escreveScript("bloqueiaLinks();")
    End Sub

    Private Sub rdIncremental_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pnlProcurar.Visible = True
        pnlModelodoArquivo.Visible = False
        rdSobreposicao.Checked = False
    End Sub

    ' pablo.dias (Nova Consultoria) - 11/09/2012 - Novo parâmetro: linhasInvalidas
    Private Sub mConsultaRegistros(ByVal ds As DataSet, ByVal linhasInvalidas As Integer)

        Try
            Dim dt As DataTable
            dt = ds.Tables(0)

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then

                Me.grdPesquisa.CurrentPageIndex = Session("indice")
                Me.grdPesquisa.SelectedIndex = -1
                Me.ucPaginacao._valor = "2"
                Session("grdDescarte") = dt
                Me.ucPaginacao.GridDataBind(Me.grdPesquisa, dt, Me)


                Me.lblSemUsuario.Visible = False
                ucPaginacao.Visible = True
                grdPesquisa.Visible = True
                gridMovimentacao.Visible = True

            Else
                Session.Remove("indice")
                Session.Remove("grdDescarte")
                Me.lblSemUsuario.Text = "<br><br><br>Nenhum Registro encontrado.<br><br>"
                ' Autor: pablo.dias (Nova Consultoria) - Data da Alteração: 11/09/2012
                ' Demanda: 12784293 - Item: 16 - Processar as movimentações do arquivo e apenas recusar a linha que estiver invalida
                ' Exibir nº de linhas inválidas quando não houver nenhum registro encontrado
                If linhasInvalidas > 0 Then
                    Me.lblSemUsuario.Text = Me.lblSemUsuario.Text + Me.lblQtdLinhas.Text
                End If
                ' FIM - pablo.dias - 11/09/2012
                Me.btnConfirmar.Visible = False
                Me.lblSemUsuario.Visible = True
                Me.pnlConfirmar.Visible = False
                ucPaginacao.Visible = False
                grdPesquisa.Visible = False
                gridMovimentacao.Visible = False
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Sub rdSobreposicao_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pnlProcurar.Visible = True
        pnlModelodoArquivo.Visible = False
        rdIncremental.Checked = False
    End Sub

    Private Sub mCarregaDominios(ByVal layout_id As Integer)

        Dim bd As New cAcompanhamento
        'Pega os domínios para o tipo do Sexo
        bd.getDominiosLayout(layout_id, "S")

        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Try
            dr = bd.ExecutaSQL_DR()
            While dr.Read
                dominio_sexo.Add(dr.GetString(1), dr.GetString(2))
            End While
        Catch ex As Exception
            'Dim excp As New clsException(ex)
        Finally
            dr.Close()
            dr = Nothing
        End Try

        'Pega os domínios para o tipo de Operação
        bd.getDominiosLayout(layout_id, "O")

        Try
            dr = bd.ExecutaSQL_DR()
            While dr.Read
                dominio_operacao.Add(dr.GetString(1), dr.GetString(2))

                ' Autor: pablo.dias (Nova Consultoria) - Data da Alteração: 11/09/2012
                ' 12784293 - Identificar operação de exclusão para tratamento na importação
                If dr.GetString(2).Equals("Exclusão") Then
                    valOperacaoExclusao = dr.GetString(1)
                End If
                ' Fim - pablo.dias - 11/09/2012

            End While
        Catch ex As Exception
            'Dim excp As New clsException(ex)
        Finally
            dr.Close()
            dr = Nothing
        End Try

        'Pega os domínios para o tipo de Vida
        bd.getDominiosLayout(layout_id, "D")

        Try
            dr = bd.ExecutaSQL_DR()
            While dr.Read

                dominio_vida.Add(dr.GetString(1), dr.GetString(2))
            End While
        Catch ex As Exception
            'Dim excp As New clsException(ex)
        Finally
            dr.Close()
            dr = Nothing
        End Try

    End Sub

    Private Sub btnImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportar.Click
        Label1.Text = ""
        Me.CaminhoArquivo = fileProcurar.PostedFile.FileName.ToString
        Session.Add("arquivo_cliente", fileProcurar.PostedFile.FileName.ToString)
        Verifica_Sheets_Excel()

        If bErro Then
            cUtilitarios.br(sMensagemErro)
            btnImportar.Enabled = True
            fileProcurar.Disabled = False
        End If

    End Sub

    Private Sub AbortarImportacao(ByVal motivo As String)

        Select Case motivo
            Case "excesso de abas"
        End Select

    End Sub

    Public Property CaminhoArquivo() As String
        Get
            Return ViewState("caminhoarquivo")
        End Get
        Set(ByVal value As String)
            ViewState("caminhoarquivo") = value
        End Set
    End Property

    Private Sub Verifica_Sheets_Excel()
        Dim ambiente As New Alianca.Seguranca.Web.ControleAmbiente

        'PMARQUES - CONFITEC - 18/06/2010
        'Comentada essa parte Sergio
        'Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        'Dim dtTable As DataTable
        'bd.ListaEnderecoAmbiente(Request.ServerVariables("SERVER_NAME"))
        'dtTable = bd.ExecutaSQL_DT()
        'Dim post As String = dtTable.Rows(0).Item(0) & "\vida_web\"

        'ambiente.ObterAmbiente("http://" & Request.ServerVariables("SERVER_NAME") & "/")
        'Dim post As String = ambiente.Post & "vida_web\"

        '' '' ''douglas 
        Dim post As String = "E:\sgvpost\" 'Descomentada essa linha Sergio

        Dim strCaminho As String = ""

        Session("quantidade_linhas") = Nothing

        Dim nome_arquivo As String = CType(Session("ramo"), String).Trim & CType(Session("apolice"), String).Trim & CType(Session("subgrupo_id"), String).Trim & "_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & "_" & Now.Hour.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0")

        CaminhoArquivo = fileProcurar.PostedFile.FileName.ToString

        '----
        Dim tipo_arquivo As String = ""
        Select Case IO.Path.GetExtension(CaminhoArquivo).ToLower
            Case ".xls"
                tipo_arquivo = "E"
                strCaminho = post & nome_arquivo & ".xls"
            Case ".csv"
                tipo_arquivo = "C"
                strCaminho = post & nome_arquivo & ".csv"
            Case ".txt"
                tipo_arquivo = "T"
                strCaminho = post & nome_arquivo & ".txt"
            Case ".xlsx"
                tipo_arquivo = "E"
                strCaminho = post & nome_arquivo & ".xlsx"
        End Select

        '----

        If Not fileProcurar.PostedFile.FileName.ToString = "" Then

            Session.Add("Arquivo", fileProcurar.PostedFile.FileName.ToString)
            fileProcurar.PostedFile.SaveAs(strCaminho)

            fileProcurar.Attributes.Add("value", CaminhoArquivo)

            Session.Add("arquivo_cliente", Session("arquivo"))
            Session.Add("arquivogeral", strCaminho)
            Session.Add("strCaminho", strCaminho)
        Else
            Session("strCaminho") = Session("arquivogeral")
            strCaminho = Session("strCaminho")
        End If

        Session("strCaminho") = strCaminho

        If tipo_arquivo = "E" Then

            ' Autor: italo.souza (Nova Consultoria) - Data da Alteração: 19/01/2012
            ' Demanda: 12784293 - Item: Permitir a importação de arquivo Excel 2007 e superior
            Dim xls As New OleDb.OleDbConnection()
            'xls = ObterConexaoExcel(strCaminho, True)
            xls = f_ConexaoExcel(strCaminho, True)

            Try
                xls.Open()
            Catch ex As Exception
                sMensagemErro = "A extensão deste arquivo não é permitido, favor salvar o arquivo com o formato XLS 97-2003"
                bErro = True
                Exit Sub
            End Try

            Dim schema As Data.DataTable = xls.GetOleDbSchemaTable(OleDb.OleDbSchemaGuid.Tables, Nothing)

            Dim nomesheet As String

            Dim i As Integer
            nomesheet = ""
            Dim bd2 As New cAcompanhamento(cDadosBasicos.eBanco.segab_db)

            Dim dt2 As DataTable
            Dim mensagem, mensagem2 As String
            Dim importacao_abortada As Boolean = False
            Dim iQtdAbas As Integer

            mensagem = ""
            mensagem2 = ""
            Session.Add("qtd_Subgrupos", 0)

            iQtdAbas = schema.Rows.Count

            Dim DSFILE As DataSet = DsFromFile()

            If DSFILE.Tables.Count > 0 Then

                iQtdAbas = DSFILE.Tables.Count

                bd2.consulta_todos_subgrupo_sps(Session("apolice"), Session("ramo"))
                dt2 = bd2.ExecutaSQL_DT()

                Dim ht As New Hashtable

                For Each r As DataRow In dt2.Rows
                    ht.Add(r(0).ToString(), r(1))
                Next

                '---------------------novo

                For i = 0 To iQtdAbas - 1

                    nomesheet = DSFILE.Tables(i).TableName 'Replace(Trim(Replace((schema.Rows(i)("TABLE_NAME").ToString), "$", "")), "'", "")

                    'Se existir somente uma aba, altera para o subgrupo atual
                    If iQtdAbas = 1 And InStr(nomesheet, "_") = 0 Then

                        nomesheet = Session("subgrupo_id")
                        mensagem = "Foram encontradas vidas para os seguintes subgrupos:"
                        mensagem = mensagem & " " & nomesheet.ToString
                        Session("qtd_Subgrupos") = Session("qtd_Subgrupos") + 1
                        Exit For

                    Else

                        If Len(nomesheet) < 4 And InStr(nomesheet, "_") = 0 Then

                            If Not ht.Contains(nomesheet) Then
                                mensagem = "A importação será abortada. A aba " & nomesheet & "não representa um Subgrupo cadastrado."
                                importacao_abortada = True
                            Else

                                If mensagem = "" Then
                                    mensagem = "Foram encontradas vidas para os seguintes subgrupos:<br>"
                                End If

                                mensagem = mensagem & " " & nomesheet & " - " & ht(nomesheet) & ",<br>"
                                Session("qtd_Subgrupos") = Session("qtd_Subgrupos") + 1

                            End If

                        End If
                    End If
                Next

                Dim importado As Boolean = False

                If importacao_abortada Then
                    Label1.Visible = True
                    If mensagem <> "" Then
                        Label1.Text = Mid(mensagem, 1, (mensagem.Length))
                    End If
                    xls.Close()
                    xls = Nothing

                    IO.File.Delete(strCaminho)

                    Exit Sub

                Else

                    Session("Imp_Subgrupo") = ""

                    If Session("qtd_Subgrupos") = 1 Then
                        Session("Imp_Subgrupo") = "Atual"
                        If ExisteRegistroImportadoNaoProcessado(Session("apolice"), Session("ramo"), Session("subgrupo_id")) Then
                            Literal1.Text = "<script language='javascript'> if(confirm( ""Existem registros importados não processados. Deseja prosseguir e eliminá-los?"" )) { document.forms[0].hdf.value='1';document.forms[0].submit(); } else { document.forms[0].hdf.value='0';document.forms[0].submit();}</script>"
                            Exit Sub
                        End If

                        pnlSubgrupo.Visible = False
                        Label1.Visible = False
                        Label3.Visible = False
                        Label3.Text = ""
                        Importar_Atual()
                        importado = True

                    End If


                    If Session("qtd_Subgrupos") = 0 Then
                        If schema.Rows.Count = 1 Then
                            Session("qtd_Subgrupos") = 1
                        End If

                        If Session("qtd_Subgrupos") = 1 And importado = False Then
                            Session("Imp_Subgrupo") = "Atual"
                            If ExisteRegistroImportadoNaoProcessado(Session("apolice"), Session("ramo"), Session("subgrupo_id")) Then
                                Literal1.Text = "<script language='javascript'> if(confirm( ""Existem registros importados não processados. Deseja prosseguir e eliminá-los?"" )) { document.forms[0].hdf.value='1';document.forms[0].submit(); } else { document.forms[0].hdf.value='0';document.forms[0].submit();}</script>"
                                Exit Sub
                            End If

                            pnlSubgrupo.Visible = False
                            Label1.Visible = False
                            Label3.Visible = False
                            Label3.Text = ""

                            Importar_Atual()

                        End If
                    End If

                    If Session("qtd_Subgrupos") > 1 Then
                        If ExisteRegistroImportadoNaoProcessadoTodosSubgrupos(Session("apolice"), Session("ramo")) Then
                            Session("Imp_Subgrupo") = "Todos"
                            Literal1.Text = "<script language='javascript'> if(confirm( ""Existem registros importados não processados. Deseja prosseguir e eliminá-los?"" )) { document.forms[0].hdf.value='1';document.forms[0].submit(); } else { document.forms[0].hdf.value='0';document.forms[0].submit();}</script>"
                            Exit Sub

                        End If

                        Label1.Visible = True
                        Label3.Visible = True
                        pnlSubgrupo.Visible = True

                        btnImportar.Enabled = False
                        If mensagem <> "" Then
                            Label1.Text = Mid(mensagem, 1, (mensagem.Length - 2))
                        End If

                        Label3.Text = " Deseja importar vidas somente do subgrupo ATUAL ou de TODOS os subgrupos? "

                    End If

                    fileProcurar.Disabled = True
                    btnImportar.Enabled = False

                End If
            Else
                If String.IsNullOrEmpty(sMensagemErro) Then
                    sMensagemErro = "O arquivo está diferente do layout definido!"
                    bErro = True
                End If
            End If

            schema.Clear()
            schema = Nothing
            xls.Close()
            xls = Nothing

        Else
            Importar()
        End If
    End Sub

    Private Function ObterConexaoExcel(ByVal caminhoArquivo As String, ByVal possuiCabecalho As Boolean) As OleDb.OleDbConnection

        ' Autor: italo.souza (Nova Consultoria) - Data da Alteração: 19/01/2012
        ' Demanda: 12784293 - Item: Permitir a importação de arquivo Excel 2007 e superior

        ObterConexaoExcel = New OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & caminhoArquivo & _
            ";Extended Properties=""Excel 8.0;HDR=" & IIf(possuiCabecalho, "YES", "NO") & ";IMEX=1""")

    End Function

    Public Function f_ConexaoExcel(ByVal p_Path As String, ByVal possuiCabecalho As Boolean) As OleDb.OleDbConnection

        Dim v_Path As String
        v_Path = p_Path
        p_Path = p_Path.Substring(p_Path.Length - 4, 4)
        p_Path = p_Path.Replace(".", "")
        Dim v_Cnn As OleDb.OleDbConnection = New OleDb.OleDbConnection()

        If p_Path = "xls" Then
            'Conexão Planilhas Excel formato .xls
            v_Cnn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & v_Path & ";Extended Properties=""Excel 8.0;HDR=" & IIf(possuiCabecalho, "YES", "NO") & ";IMEX=0"""
        Else
            'Conexão Planilhas Excel formato .xlsx
            v_Cnn.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & v_Path & ";Extended Properties=""Excel 12.0;HDR=" & IIf(possuiCabecalho, "YES", "NO") & ";IMEX=0"""
        End If

        Return v_Cnn

    End Function

    Private Sub Importar(Optional ByVal tp_imp_Excel As String = "ATUAL")

        'verifica se existe layout de arquivo
        If Not verificaLayOutAtivo(Session("apolice"), Session("ramo"), Session("subgrupo_id")) Then
            pnlMsgSemLayout.Visible = True
            'Adicionado alert quando nao tem layout (04/03/2007)
            cUtilitarios.br("2 - Seu arquivo não foi carregado. Por favor, definir layout.")
            cUtilitarios.escreveScript("top.document.getElementById('Link6').click()")
            pnlSimples.Visible = False
            Exit Sub
        End If

        Trace.Write("Verificou se tem layout ativo")

        'Dim ambiente As New Alianca.Seguranca.Web.ControleAmbiente
        'ambiente.ObterAmbiente("http://" & Request.ServerVariables("SERVER_NAME") & "/")
        'Dim post As String = ambiente.Post & "vida_web\"

        'PMARQUES - CONFITEC - 18/06/2010
        'Retirada essa pare Sergio
        'Dim bd2 As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        'Dim dtTable As DataTable
        'bd2.ListaEnderecoAmbiente(Request.ServerVariables("SERVER_NAME"))
        'dtTable = bd2.ExecutaSQL_DT()
        'Dim post As String = dtTable.Rows(0).Item(0) & "\vida_web\"

        '' '' ''Douglas
        Dim post As String = "E:\sgvpost\" ' Descomentada essa linha

        Dim strCaminho As String = ""

        If txtLinhasDesconsiderar.Text = "" Then
            txtLinhasDesconsiderar.Text = 0
        End If

        'acintra 20080506
        If CType(txtLinhasDesconsiderar.Text, Integer) > 0 Then
            Session("chkDescartaCabecalho") = "S"
        Else
            Session("chkDescartaCabecalho") = "N"
        End If

        strCaminho = Session("Caminho")

        If hdf.Value = "" Or hdf.Value = "2" Then

            strCaminho = Session("Caminho")

            If Session("Caminho") <> "" Then
				Session("Caminho") = Session("strcaminho") '**** ALTERADO ODAIR.SANTOS - CONFITEC - INCIDENTE - INC000003780448 - 31-10-2012 ********
                strCaminho = Session("Caminho")
            Else
                Session("Arquivo") = Session("arquivo_cliente") 'fileProcurar.PostedFile.FileName.ToString
                Session("Caminho") = Session("strcaminho") 'post & nome_arquivo
                strCaminho = Session("Caminho")
            End If

        End If

        Dim nomeArquivo As String = Session("Caminho")

        'Obtem o tipo do arquivo a ser feito upload
        Dim tipo_arquivo As String = ""
        Select Case IO.Path.GetExtension(nomeArquivo).ToLower
            Case ".xls"
                tipo_arquivo = "E"
            Case ".csv"
                tipo_arquivo = "C"
            Case ".txt"
                tipo_arquivo = "T"
            Case ".xlsx"
                tipo_arquivo = "E"
        End Select

        Session.Add("tpArquivo", tipo_arquivo)

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        'Pega dados do Layout ativo
        bd.SEGS5763_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), tipo_arquivo)
        Dim dt As DataTable = bd.ExecutaSQL_DT()
        Dim layout_id As Integer

        'novo: evitando o erro
        If dt.Rows.Count > 0 Then
            layout_id = dt.Rows(0).Item(1)
        Else
            pnlMsgSemLayout.Visible = True
            'Adicionado alert quando nao tem layout (04/03/2007)
            cUtilitarios.br("1 - Seu arquivo não foi carregado. Por favor, definir layout.")
            cUtilitarios.escreveScript("top.document.getElementById('Link6').click()")
            pnlSimples.Visible = False
            Exit Sub
        End If
        '-------------------------------------------------

        Session("sIdLayout") = layout_id
        Dim separador As Char = CType(dt.Rows(0).Item(2), String).Chars(0)

        Me.mCarregaDominios(layout_id)

        'Preenche as colunas
        Dim ds As DataSet
        bd.SEGS5849_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), 1)
        bd.ExecutaSQL()
        ds = bd.DS

        For Each d As Data.DataColumn In ds.Tables(0).Columns
            'cUtilitarios.br(d.DataType.ToString)
            d.DataType = Type.GetType("System.String")
            Type.GetType("System.String")
        Next

        ''Mapeamento dos campos do Layout com a ordem dos parâmetros da SP
        Dim ordem_correta As Collection = New Collection
        Dim tamanhos As Collection = New Collection
        Dim final As Integer = 0
        Dim iColunas As Integer = 0
        Dim iColunasExcel As Integer = 0
        Dim iColunasBranco As Integer = 0
        Dim iTamanhoTotalLinha As Integer = 0

        Me.campos.Clear()
        Dim alterarindice As Boolean = False
        For j As Integer = 0 To dt.Rows.Count - 1

            iColunas = iColunas + 1

            'Parada do for se mudar o layout id
            Dim verificador_layout_id As Integer = dt.Rows(j).Item(1)
            If verificador_layout_id <> layout_id Then
                iColunas = iColunas - 1
                Exit For
            End If

            Dim valor As String = dt.Rows(j).ItemArray(4)

            Select Case valor
                Case "Data de Inclusão no Seguro", "Operação", "Nome do Segurado", "CPF do Titular", "Data de Nascimento", "Sexo", "sexo", "Valor do Salário", "Capital Segurado", "CPFConjuge", "DataDesligamento"
                    Me.campos.Add(dt.Rows(j).ItemArray(4), New cCampo(dt.Rows(j).ItemArray(4), dt.Rows(j).ItemArray(5), dt.Rows(j).ItemArray(6), IIf(alterarindice, dt.Rows(j).ItemArray(7), dt.Rows(j).ItemArray(7) + 1)))
                    Me.grdPesquisa.Columns.Item(final).HeaderText = Me.deParaHeader(dt.Rows(j).ItemArray(4))
                    CType(Me.FindControl("col" & final), System.Web.UI.HtmlControls.HtmlGenericControl).InnerHtml = Me.deParaHeader(dt.Rows(j).ItemArray(4))
                    final = final + 1
                Case "TipoComponente"
                Case "--Descartar--"
                Case "Subgrupo"
                    iColunas = iColunas - 1
                    alterarindice = True
                Case Else
            End Select
            'Popula os tamanhos
            iTamanhoTotalLinha = iTamanhoTotalLinha + dt.Rows(j).ItemArray(6)
            tamanhos.Add(dt.Rows(j).ItemArray(6), "" & j)

        Next

        dt.Clear()
        dt = Nothing

        Session.Add("iColunas", iColunas)

        ' pablo.dias (Nova Consultoria) - 11/09/2012 - Declaração única da linhasInvalidas 
        Dim linhasInvalidas As Integer = 0

        Select Case tipo_arquivo
            Case "E"

                Try

                    If IsNothing(DTIMPORT) Then
                        'Alterado o indice da tabela pelo indice do subgrupo estava dando erro Sergio
                        'DTIMPORT = DsFromFile.Tables(Session("subgrupo_id"))
                        DTIMPORT = DsFromFile.Tables(0)
                    End If

                    Dim xlsContent As New DataTable
                    Dim contador As Integer
                    Dim iLinhasBranco As Integer = 0
                    Dim iContaColuna As Integer
                    Dim tableName As String = DTIMPORT.TableName '''''schema.Rows(0)(2).ToString

                    xlsContent = DTIMPORT

                    Dim m As DataRow

                    'acintra 20050526 - vefiricando colunas do arquivo
                    iContaColuna = 0 '''''''''CType(txtLinhasDesconsiderar.Text, Integer)

                    If iContaColuna >= xlsContent.Rows.Count Then
                        bErro = True
                        sMensagemErro = "O arquivo possui " & xlsContent.Rows.Count & " linhas, o processo não pode ignorar " & iContaColuna & " linhas!"
                        Exit Sub
                    End If

                    If Not iContaColuna = 0 Then
                        iContaColuna = iContaColuna - 1
                    End If

                    For iIndex As Integer = 0 To xlsContent.Rows(iContaColuna).ItemArray.GetLength(0) - 1
                        If Not IsDBNull(xlsContent.Rows(iContaColuna).ItemArray(iIndex)) Then
                            iColunasExcel = iColunasExcel + 1
                            iColunasBranco = 0
                        Else
                            iColunasExcel = iColunasExcel + 1
                            'iColunasBranco = iColunasBranco + 1
                        End If
                    Next

                    iColunasExcel = iColunasExcel - iColunasBranco

                    Dim total As Integer = CType(iColunas, Integer)

                    'pablo.cardoso 27/01/2012
                    'Validação não ocorrerá mais aqui
                    'If iColunasExcel <> CType(iColunas, Integer) Then
                    '    bErro = True
                    '    sMensagemErro = "Quantidade de colunas do arquivo diferente do layout definido!"
                    '    Exit Sub
                    'End If

                    'Insere as Colunas sobressalentes caso não possua
                    If xlsContent.Rows(0).ItemArray.GetLength(0) < 11 Then

                        For x As Integer = xlsContent.Rows(0).ItemArray.GetLength(0) - 1 To 10
                            Dim dc As DataColumn = New DataColumn("c" & x)
                            xlsContent.Columns.Add(dc)
                        Next

                    End If

                    Dim ContadorLinhas As Integer = 0
                    contador = 0

                    Dim tamanho As Integer = sbuffer.Length
                    sbuffer.Remove(0, tamanho)

                    For z As Integer = 0 To xlsContent.Rows.Count - 1
                        Try

                            Dim rowString As String = String.Empty
                            Dim separadoreStr As String = String.Empty


                            'Autor: pablo.cardoso (Nova Consultoria) 
                            'Data da Alteração: 27/01/2012
                            'Demanda AB: 12784293 
                            'Item: 16 - Processar as movimentações do arquivo e apenas recusar a linha que estiver invalida.

                            ' Autor: pablo.dias (Nova Consultoria) - Data da Alteração: 11/09/2012
                            ' Permitir a importação quando a data de desligamento não estiver preenchida para outras operações sem ser exclusão
                            ' A existência do campo deve ser validada para não causar erro em layouts de sobreposição
                            Dim indiceOperacao As Integer = -1
                            If Not IsNothing(Me.campos("Operação")) Then
                                indiceOperacao = Me.campos("Operação").Ordem - 1
                            End If

                            Dim indiceDataDesligamento As Integer = -1
                            If Not IsNothing(Me.campos("DataDesligamento")) Then
                                indiceDataDesligamento = Me.campos("DataDesligamento").Ordem - 1
                            End If

                            Dim operacaoExclusao As Boolean = False

                            Dim operacao As String = ""
                            Dim dataDesligamento As String = ""

                            For y As Integer = 0 To xlsContent.Columns.Count - 1
                                Dim colunaConteudoStr As String = ""
                                If Not String.IsNullOrEmpty(xlsContent.Rows(z)(y).ToString()) Then
                                    colunaConteudoStr = xlsContent.Rows(z)(y).ToString().Replace(";", " ")
                                    rowString += String.Concat(colunaConteudoStr.Trim(), ";")
                                End If

                                ' Identificar operação e data de desligamento
                                If y = indiceOperacao Then
                                    operacao = colunaConteudoStr
                                    operacaoExclusao = operacao.Equals(valOperacaoExclusao)
                                End If

                                If y = indiceDataDesligamento Then
                                    dataDesligamento = colunaConteudoStr
                                End If

                            Next

                            Dim arrSplitCount As String() = rowString.Split(";"c)

                            ' A data de desligamento pode ficar vazia caso a operação não seja de exclusão
                            Dim permiteDataDesligamentoVazia As Boolean = _
                                Not (String.IsNullOrEmpty(dataDesligamento) And operacaoExclusao)

                            'Colunas do Layout padrão
                            If (arrSplitCount.Length - 1) <> CType(iColunas, Integer) _
                                And Not String.IsNullOrEmpty(rowString) _
                                And Not permiteDataDesligamentoVazia Then ' pablo.dias - 11/09/2012
                                'CONTAR LINHA INVÁLIDA
                                linhasInvalidas = linhasInvalidas + 1
                                ' FIM - pablo.dias - 11/09/2012
                            Else
                                'IMPORTA LINHA
                                If VerificarLinha(xlsContent.Rows(z)) Then
                                    iLinhasBranco = 0
                                    'contador = contador + 1
                                    m = xlsContent.Rows(z)
                                    '''''''If ContadorLinhas >= CType(txtLinhasDesconsiderar.Text, Integer) Then
                                    If Not insereLinhaArquivo(m, ds) Then
                                        Exit For
                                    End If
                                    contador = contador + 1
                                    '''''''End If
                                Else
                                    iLinhasBranco = iLinhasBranco + 1
                                End If
                                ContadorLinhas = ContadorLinhas + 1
                            End If
                            'Fim pablo.cardoso

                            

                            'If iLinhasBranco = 49 Then
                            'Exit For
                            'End If

                        Catch ex As Exception
                            Dim excp1 As New clsException(ex)
                        End Try
                    Next

                    Session("arq_quantidade_linhas") = contador

                    Session("quantidade_linhas") = Session("quantidade_linhas") + contador
                    lblQtdLinhas.Text = "Serão importadas " & Session("quantidade_linhas") & " linhas "

                    'pablo.cardoso 27/01/2012
                    If linhasInvalidas > 0 Then
                        lblQtdLinhas.Text += "<br><font color='red'><strong> Existe(m) " & linhasInvalidas & " linha(s) que não pode(m) ser importada(s), ajuste pelo método online ou acerte o arquivo de importação</strong></font>"
                    End If

                    xlsContent.Clear()
                    xlsContent = Nothing

                    m = Nothing

                Catch ex As Exception
                    Dim excp As New clsException(ex)
                End Try

            Case "C"
                Try

                    'If Session("tp_arquivo") = "C" Then   

                    'Extrai os registros do CSV

                    Dim arquivo As IO.StreamReader = IO.File.OpenText(strCaminho)
                    Dim linha As String = ""
                    Dim aux As String = ""
                    Dim cont As Integer
                    Dim contador As Integer = 0
                    Dim pos As Integer = 0
                    Dim separadores() As Char = {separador} '{separador, vbCrLf}
                    'Dim iColunasCSV As Integer
                    'Dim sLinhaAux As String
                    Dim iContaLinha As Integer = 0
                    Dim bVerificaColuna As Boolean = False
                    Dim lnInvalida As Boolean = False


                    While arquivo.Peek() > -1

                        lnInvalida = False

                        Dim dados() As String = {"", "", "", "", "", "", "", "", "", "", ""}
                        cont = 0
                        linha = arquivo.ReadLine()
                        linha = linha.Trim() & vbCrLf

                        'If iContaLinha = CType(txtLinhasDesconsiderar.Text, Integer) Then
                        '    bVerificaColuna = True
                        '    sLinhaAux = linha.Trim()
                        '    While sLinhaAux <> "1"
                        '        If InStr(sLinhaAux, ";", CompareMethod.Text) <> 0 Then
                        '            iColunasCSV = iColunasCSV + 1
                        '            sLinhaAux = sLinhaAux.Substring(InStr(sLinhaAux, ";", CompareMethod.Text))
                        '        Else
                        '            sLinhaAux = "1"
                        '        End If
                        '    End While
                        'End If

                        'pablo.cardoso
                        Dim arrSplitCount As String() = linha.Split(separador)

                        For j As Integer = 0 To arrSplitCount.Length - 1 'Validando para Coluna a menos(coluna nula ou vazia)
                            If String.IsNullOrEmpty(arrSplitCount(j)) Then
                                linhasInvalidas = linhasInvalidas + 1
                                lnInvalida = True
                                Exit For
                            End If
                        Next

                        If (arrSplitCount.Length - 1) <> iColunas And Not lnInvalida Then 'Validando para coluna a mais
                            linhasInvalidas = linhasInvalidas + 1
                            lnInvalida = True
                        End If


                        If linha.Trim <> "" And Not lnInvalida Then
                            contador = contador + 1
                            'Separa as Colunas de acordo com o delimitador

                            cont = 1

                            While linha <> ""

                                pos = linha.IndexOfAny(separadores)
                                'Verifica se não foi enviado o ; final e captura o registro final da linha
                                If pos < 0 Then
                                    Exit While
                                End If

                                ' New code
                                aux = linha.Substring(0, pos)

                                If cont >= dados.Length Then
                                    Exit While
                                End If

                                dados(cont) = aux

                                ' end code
                                Try
                                    linha = linha.Substring(pos + 1, linha.Length - (pos + 1))
                                Catch ex As Exception
                                    linha = ""
                                End Try
                                cont = cont + 1
                            End While

                            'Insere no BD
                            If Not insereLinhaArquivo(dados, ds) Then
                                Exit While
                            End If
                        End If 'linha <> "" And Not lnInvalida

                        iContaLinha = iContaLinha + 1

                    End While

                    If CType(txtLinhasDesconsiderar.Text, Integer) > 0 Then
                        contador = contador - CType(txtLinhasDesconsiderar.Text, Integer)
                    End If

                    lblQtdLinhas.Text = contador & " linhas no arquivo"
                    Session("quantidade_linhas") = contador

                    'pablo.cardoso 28/01/2012
                    If linhasInvalidas > 0 Then
                        lblQtdLinhas.Text += "<br><font color='red'><strong> Existe(m) " & linhasInvalidas & " linha(s) que não pode(m) ser importada(s), ajuste pelo método online ou acerte o arquivo de importação</strong></font>"
                    End If

                    arquivo.Close()
                    'GRAVA TXT

                Catch ex As Exception
                    Dim excp As New clsException(ex, "O arquivo enviado não corresponde ao formato configurado.")
                End Try
            Case "T" 'Else
                Try

					'Extrai os registros do Txt
                    Dim arquivo As IO.StreamReader = IO.File.OpenText(strCaminho)
                    Dim linha As String = ""
                    Dim aux As String = ""
                    Dim cont As Integer
                    Dim contador As Integer = 0
                    Dim pos As Integer = 0
                    Dim iContaLinha As Integer = 0
                    Dim lnInvalida As Boolean = False

                    While arquivo.Peek() > -1
                        lnInvalida = False

                        Dim dados() As String = {"", "", "", "", "", "", "", "", "", "", ""}
                        cont = 0

                        linha = arquivo.ReadLine()
                        linha = linha.TrimEnd(" ")

                        'If iContaLinha = CType(txtLinhasDesconsiderar.Text, Integer) Then
                        If linha.Length <> iTamanhoTotalLinha Then
                            'bErro = True
                            'sMensagemErro = "Quantidade de colunas do arquivo diferente do layout definido!"
                            'Exit Sub
                            lnInvalida = True
                            linhasInvalidas = linhasInvalidas + 1
                        End If
                        'End If


                        If linha.Trim <> "" And Not lnInvalida Then
                            contador = contador + 1
                            'Separa as Colunas de acordo com o tamanho
                            While linha <> ""
                                Try
                                    aux = linha.Substring(0, tamanhos.Item("" & cont))
                                Catch ex As Exception
                                    aux = linha.Substring(0)
                                End Try
                                dados(cont + 1) = aux 'pablo.cardoso -> Erro encontrado em teste da importação de TXT, não importava a 1ª clna
                                Try
                                    linha = linha.Substring(tamanhos.Item("" & cont), linha.Length - tamanhos.Item("" & cont))
                                Catch ex As Exception
                                    linha = ""
                                End Try
                                cont = cont + 1
                            End While

                            'Insere no BD
                            insereLinhaArquivo(dados, ds)
                        End If

                        iContaLinha = iContaLinha + 1

                    End While

					If CType(txtLinhasDesconsiderar.Text, Integer) > 0 Then
                        contador = contador - CType(txtLinhasDesconsiderar.Text, Integer)
                    End If

                    lblQtdLinhas.Text = contador & " linhas no arquivo"
                    Session("quantidade_linhas") = contador

                    'pablo.cardoso(28 / 1 / 2012)
                    If linhasInvalidas > 0 Then
                        lblQtdLinhas.Text += "<br><font color='red'><strong> Existe(m) " & linhasInvalidas & " linha(s) que não pode(m) ser importada(s), ajuste pelo método online ou acerte o arquivo de importação</strong></font>"
                    End If

                    arquivo.Close()


                Catch ex As Exception
                    Dim excp As New clsException(ex, "O arquivo enviado não corresponde ao formato configurado.")
                End Try

        End Select


        Dim valorsub As String = Session("subgrupo_id")

        If tp_imp_Excel = "TODOS" Then

            Session("sStrCaminho") = Session("Caminho") 'strCaminho
            Session("ssbuffer") = sbuffer

            ' pablo.dias (Nova Consultoria) - 11/09/2012 - Tratar retorno de linhasInvalidas
            mConsultaRegistros(ds, linhasInvalidas)

            Insere_registros()

        Else

            pnlModelodoArquivo.Visible = True
            pnlProcurar.Visible = False
            pnlMovimentacao.Visible = False
            pnlTipodeMovimentacao.Visible = False
            pnlMensagemEnviada.Visible = True

            For i As Int16 = Me.campos.Count To 9
                'cUtilitarios.escreve(Me.campos.Count)
                'Me.grdPesquisa.Columns.RemoveAt(Me.campos.Count)
                Me.grdPesquisa.Columns.Item(i).Visible = False
                Me.FindControl("col" & i).Visible = False
            Next

            'Me.grdPesquisa.Columns.RemoveAt(4)

            'Coloca na Session o DataSet e o arquivo enviado
            Session("sStrCaminho") = strCaminho
            Session("ssbuffer") = sbuffer

            ' pablo.dias (Nova Consultoria) - 11/09/2012 - Tratar retorno de linhasInvalidas
            mConsultaRegistros(ds, linhasInvalidas)

            Session("Linha_1") = ""
            Session("ContadorLinhas") = Nothing

        End If

        'Me.campos.Clear()

        'Session("ssbuffer") = Nothing
        ds = Nothing
        dt = Nothing


    End Sub

    'acintra
    Private Function VerificarLinha(ByVal dtrLinha As DataRow) As Boolean

        If IsDBNull(dtrLinha.Item(0)) And _
           IsDBNull(dtrLinha.Item(1)) And _
           IsDBNull(dtrLinha.Item(2)) And _
           IsDBNull(dtrLinha.Item(3)) And _
           IsDBNull(dtrLinha.Item(4)) And _
           IsDBNull(dtrLinha.Item(5)) And _
           IsDBNull(dtrLinha.Item(6)) And _
           IsDBNull(dtrLinha.Item(7)) And _
           IsDBNull(dtrLinha.Item(8)) And _
           IsDBNull(dtrLinha.Item(9)) And _
           IsDBNull(dtrLinha.Item(10)) Then
            Return False
        Else
            Return True
        End If

    End Function

    Private Sub inserirNoBanco(ByVal arquivo As String)

        Trace.Write("Bulk Insert", "Começou")
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        bd.SEGS5787_SPI(Session("controla_processo_id"), _
                        Session("ramo"), _
                        Session("apolice"), _
                        Session("subgrupo_id"), _
                        arquivo, _
                        IO.Path.GetFileName(Session("Arquivo")), _
                        Session("usuario") & "'", _
                        Session("chkDescartaCabecalho"))

        'Try
        bd.ExecutaSQL()

        'Catch ex As Exception

        '    Dim excp As New clsException(ex)

        'End Try

        Trace.Write("Bulk Insert", "Terminou")

    End Sub

    Private Function Trata_Nome_Arquivo(ByVal File As String) As String
        File = File.Replace(":", ":\").Replace("vida_web", "\vida_web\")
        Return File
    End Function

    Private Sub btnFechaPagina_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("Centro.aspx")
    End Sub

    Private Sub btnConfirmar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmar.Click

        Dim valor As String = Session("subgrupo_id")

        Insere_registros()

        Session("Caminho") = ""
        Session("Arquivo") = ""

        If Session("subgrupo_id_original") <> "" Then
            Session("subgrupo_id") = Session("subgrupo_id_original")
        End If

    End Sub

    Private Sub Insere_registros()

        pnlMovimentacao.Visible = True
        pnlModelodoArquivo.Visible = False
        pnlProcurar.Visible = False
        pnlTipodeMovimentacao.Visible = False
        pnlMensagemEnviada.Visible = True
        lblListagemEnviada.Visible = True

        Dim strCaminho As String = Session("sStrCaminho")
        sbuffer = Session("ssbuffer")

        Try

            'Grava o arquivo csv com marcador ';'
            'pcarvalho 20080917 - para gravar com acentuação utiliza codepage ANSI


            ' Autor: italo.souza (Nova Consultoria) - Data da Alteração: 19/01/2012
            ' Demanda: 12784293 - Item: Permitir a importação de arquivo Excel 2007 e superior

            Dim arquivo As String = strCaminho

            Dim extensao As String = System.IO.Path.GetExtension(strCaminho).ToLower

            If extensao = ".xls" Or extensao = ".xlsx" Then
                arquivo = Mid(strCaminho, 1, Len(strCaminho) - extensao.Length)
                Session("Caminho") = arquivo
            End If


            Dim writer As New IO.StreamWriter(arquivo, False, System.Text.Encoding.GetEncoding(1252))
            writer.Write(sbuffer.ToString)
            writer.Close()
            writer = Nothing
            
            insereControleArquivo(Session("ramo"), _
                                  Session("apolice"), _
                                  Session("subgrupo_id"), _
                                  arquivo, _
                                  IO.Path.GetFileName(Session("Arquivo")), _
                                  Session("estipulante"), _
                                  Session("arq_quantidade_linhas"))

            Session("controla_processo_id") = ObterControleArquivo()
            inserirNoBanco(arquivo)
            cUtilitarios.MoveArquivo(strCaminho, Session("GLAMBIENTE_ID"))
            

            'Atualiza menu, bloqueando acesso à SEGW0078
            'cUtilitarios.br("Bloqueando...")
            cUtilitarios.escreveScript("top.document.getElementById('Hyperlink4').onclick = function() { alert('Neste momento, não é possível Carregar Relação Anterior.');return false;}")
            cUtilitarios.escreveScript("top.document.getElementById('Link6').onclick = function() { alert('Neste momento, não é possível realizar a movimentação por arquivo.');return false;}")
        Catch ex As Exception
            'Dim excp As New clsException(ex)
        End Try

    End Sub

    ''' Verififca se existe layout de arquivo definido.
    Private Function verificaLayOutAtivo(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer) As Boolean

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)
        bd.getLayoutArquivoAtivos(Session("apolice"), Session("ramo"), Session("subgrupo_id"), 1)

        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Try

            dr = bd.ExecutaSQL_DR()

            If dr.HasRows Then
                Return True
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        Finally
            dr.Close()
            dr = Nothing
        End Try

        Return False

    End Function

    Private Function ExisteRegistroImportadoNaoProcessado(ByVal apolice_id As Long, _
                                                          ByVal ramo_id As Integer, _
                                                          ByVal subgrupo_id As Integer) As Boolean

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)
        bd.VerificarImportacaoAnterior(apolice_id, ramo_id, subgrupo_id)

        Dim dr As Data.SqlClient.SqlDataReader
        Try

            dr = bd.ExecutaSQL_DR()

            If dr.HasRows Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        Finally
            dr.Close()
            dr = Nothing
        End Try

    End Function

    Private Function ExisteRegistroImportadoNaoProcessadoTodosSubgrupos(ByVal apolice_id As Long, _
                                                          ByVal ramo_id As Integer) As Boolean

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)
        bd.VerificarImportacaoAnteriorTodosSubgrupos(apolice_id, ramo_id)

        Dim dr As Data.SqlClient.SqlDataReader = Nothing
        Try

            dr = bd.ExecutaSQL_DR()

            If dr.HasRows Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        Finally
            dr.Close()
            dr = Nothing
        End Try

    End Function

    Private Function ApagarRegistroImportadoNaoProcessado(ByVal apolice_id As Long, _
                                                          ByVal ramo_id As Integer, _
                                                          ByVal subgrupo_id As Integer) As Boolean

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        bd.ApagarImportacaoAnterior(Session("apolice"), _
                                    Session("ramo"), _
                                    Session("subgrupo_id"))

        Try
            bd.ExecutaSQL()
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Function

    Private Function ApagarRegistroImportadoNaoProcessadoTodosSubgrupos(ByVal apolice_id As Long, _
                                                              ByVal ramo_id As Integer) As Boolean

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        bd.ApagarImportacaoAnteriorTodosSubgrupos(Session("apolice"), _
                                    Session("ramo"))

        Try
            bd.ExecutaSQL()
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Function

    'acintra 20080502
    Private Sub insereControleArquivo(ByVal iRamoId As Integer, _
                                      ByVal lApoliceId As Long, _
                                      ByVal iSubGrupoId As Integer, _
                                      ByVal sPathArquivo As String, _
                                      ByVal sNomeArquivo As String, _
                                      ByVal sNomeEstipulante As String, _
                                      ByVal iQtRegistros As Integer)

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        bd.insereProcessoUpload(iRamoId, _
                                lApoliceId, _
                                iSubGrupoId, _
                                sPathArquivo, _
                                sNomeArquivo, _
                                "P", _
                                Session("usuario"), _
                                CType(Now, DateTime).ToString("yyyMMdd"), _
                                Session("usuario"), _
                                sNomeEstipulante, _
                                iQtRegistros)

        Try
            bd.ExecutaSQL()
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Private Function ObterControleArquivo() As Integer

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        Dim dr As Data.SqlClient.SqlDataReader

        Try

            bd.ObterUltimoControleArquivo(Session("apolice"), _
                                          Session("ramo"), _
                                          Session("subgrupo_id"))

            dr = bd.ExecutaSQL_DR()

            If dr.HasRows Then
                While dr.Read()
                    ObterControleArquivo = dr.GetValue(0)
                End While
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Function

    Private Function insereLinhaArquivo(ByVal m As DataRow, ByRef ds As DataSet) As Boolean

        Try
            Dim operacao As String = ""
            Dim dt_inicio_vigencia As String = ""
            Dim nome As String = ""
            Dim cpf As String = ""
            Dim dt_nascimento As String = ""
            Dim sexo As String = ""
            Dim val_salario As String = ""
            Dim val_capital_segurado As String = ""
            Dim cpf_conjuge As String = ""
            Dim dt_fim_vigencia As String = ""
            Dim iIndice As Integer = 0

            Try
                If Not IsNothing(campos("Data de Inclusão no Seguro")) Then
                    dt_inicio_vigencia = ouvazio(m.ItemArray, CType(campos("Data de Inclusão no Seguro"), cCampo).ordem - 1).Replace("'", "")

                    If dt_inicio_vigencia.Length > 10 Then
                        dt_inicio_vigencia = dt_inicio_vigencia.Substring(0, 10)
                    End If
                End If
            Catch ex As Exception

            End Try

            Try
                If Not IsNothing(campos("Operação")) Then
                    operacao = ouvazio(m.ItemArray, CType(campos("Operação"), cCampo).ordem - 1).Replace("'", "")
                End If
            Catch ex As Exception

            End Try

            Try
                If Not IsNothing(campos("Nome do Segurado")) Then
                    nome = ouvazio(m.ItemArray, CType(campos("Nome do Segurado"), cCampo).ordem - 1).Replace("'", "")

                    If nome.Length > 60 Then
                        nome = nome.Substring(0, 60)
                    End If
                End If
            Catch ex As Exception

            End Try

            Try
                If Not IsNothing(campos("CPF do Titular")) Then
                    cpf = ouvazio(m.ItemArray, CType(campos("CPF do Titular"), cCampo).ordem - 1).Replace("'", "").Replace("-", "").Replace(".", "")

                    If cpf.Length > 14 Then
                        cpf = cpf.Substring(0, 14)
                    End If
                End If
            Catch ex As Exception

            End Try

            Try
                If Not IsNothing(campos("Data de Nascimento")) Then
                    dt_nascimento = ouvazio(m.ItemArray, CType(campos("Data de Nascimento"), cCampo).ordem - 1).Replace("'", "")

                    If dt_nascimento.Length > 10 Then
                        dt_nascimento = dt_nascimento.Substring(0, 10)
                    End If
                End If
            Catch ex As Exception

            End Try

            Try
                If Not IsNothing(campos("Sexo")) Then
                    sexo = ouvazio(m.ItemArray, CType(campos("Sexo"), cCampo).ordem - 1).Replace("'", "")
                End If
            Catch ex As Exception

            End Try

            Try
                If Not IsNothing(campos("Valor do Salário")) Then
                    val_salario = ouvazio(m.ItemArray, CType(campos("Valor do Salário"), cCampo).ordem - 1).Replace("'", "")
                    val_salario = Trim(Replace(UCase(val_salario), "R$", ""))
                    If IsNumeric(val_salario) Then
                        val_salario = Format(CDbl(val_salario), "############0.00")
                    End If

                    If val_salario.Length > 16 Then
                        val_salario = val_salario.Substring(0, 16)
                    End If
                End If
            Catch ex As Exception

            End Try

            Try
                If Not IsNothing(campos("Capital Segurado")) Then
                    val_capital_segurado = ouvazio(m.ItemArray, CType(campos("Capital Segurado"), cCampo).ordem - 1).Replace("'", "")

                    If val_capital_segurado.Length > 16 Then
                        val_capital_segurado = val_capital_segurado.Substring(0, 16)
                    End If
                End If
            Catch ex As Exception

            End Try

            Try
                If Not IsNothing(campos("CPFConjuge")) Then
                    cpf_conjuge = ouvazio(m.ItemArray, CType(campos("CPFConjuge"), cCampo).ordem - 1).Replace("'", "").Replace("-", "").Replace(".", "")
                    If cpf_conjuge.Length > 14 Then
                        cpf_conjuge = cpf_conjuge.Substring(0, 14)
                    End If
                End If
            Catch ex As Exception

            End Try

            Try
                If Not IsNothing(campos("DataDesligamento")) Then
                    dt_fim_vigencia = ouvazio(m.ItemArray, CType(campos("DataDesligamento"), cCampo).ordem - 1).Replace("'", "")

                    If dt_fim_vigencia.Length > 10 Then
                        dt_fim_vigencia = dt_fim_vigencia.Substring(0, 10)
                    End If
                End If
            Catch ex As Exception

            End Try

            Dim row As DataRow = ds.Tables(0).NewRow
            row.Item(0) = 6785                      'Seguradora_cod_susep
            row.Item(1) = 0                         'sucursal_seguradora_id
            row.Item(2) = Session("nomeSubgrupo")   'nome_estip
            row.Item(3) = Session("ramo")           'ramo_id
            row.Item(4) = Session("apolice")        'apolice_id
            row.Item(5) = Session("subgrupo_id")    'sub_grupo_id
            row.Item(6) = Session("usuario")        'Usuário
            row.Item(7) = Now                       'dt_inclusao

            Dim t As Integer = 8

            For Each campo As cCampo In campos.Values
                'cUtilitarios.escreve(campo.nome & "<br>")
                Try
                    row.Item(t) = ouvazio(m.ItemArray, campo).Replace("'", "")
                Catch ex As Exception
                    row.Item(t) = ""
                End Try
                t = t + 1
            Next

            ds.Tables(0).Rows.Add(row)

            'tratando as datas
            If Not dt_inicio_vigencia = "" Then
                If IsDate(dt_inicio_vigencia) Then
                    dt_inicio_vigencia = CType(dt_inicio_vigencia, DateTime).ToString("dd/MM/yyyy")
                End If
            End If

            If Not dt_fim_vigencia = "" Then
                If IsDate(dt_fim_vigencia) Then
                    dt_fim_vigencia = CType(dt_fim_vigencia, DateTime).ToString("dd/MM/yyyy")
                End If
            End If

            If Not dt_nascimento = "" Then
                If IsDate(dt_nascimento) Then
                    dt_nascimento = CType(dt_nascimento, DateTime).ToString("dd/MM/yyyy")
                End If
            End If

            'Monta a linha no sbuffer
            sbuffer.Append(dt_inicio_vigencia & ";" & _
                           dt_fim_vigencia & ";" & _
                           operacao & ";" & _
                           nome & ";" & _
                           cpf & ";" & _
                           dt_nascimento & ";" & _
                           sexo & ";" & _
                           val_salario & ";" & _
                           val_capital_segurado & ";" & _
                           cpf_conjuge & ";" & vbCrLf)

        Catch ex As Exception
            'Dim excp As New clsException(ex)
            Response.Write("<br><pre>")
            Response.Write(ex.Message)
            Response.Write(ex.StackTrace)
            Response.Write("</pre>")
            Return False
        End Try

        Return True

    End Function

    Private Function ouvazio(ByVal matriz As Array, ByVal idx As Integer) As String

        Dim retorno As String = ""
        Try
            retorno = matriz(idx)
        Catch ex As Exception
            retorno = ""
        End Try

        Return retorno

    End Function

    Private Function ouvazio(ByVal matriz As Array, ByVal campo As cCampo) As String

        Dim retorno As String = ""
        Try
            Select Case campo.nome
                Case "DataDesligamento", "Data de Nascimento", "Data de Inclusão no Seguro"
                    If IsDate(matriz(campo.ordem)) Then
                        retorno = CType(matriz(campo.ordem - 1), DateTime).ToString("dd/MM/yyyy")
                    Else
                        retorno = matriz(campo.ordem - 1)
                    End If
                Case "Sexo", "sexo"
                    retorno = Me.dominio_sexo(matriz(campo.ordem - 1).ToString.Trim)
                    If retorno Is Nothing Then
                        retorno = matriz(campo.ordem - 1).ToString.Trim
                    End If
                Case "Operação"
                    retorno = Me.dominio_operacao(matriz(campo.ordem - 1).ToString.Trim)
                Case Else
                    retorno = matriz(campo.ordem - 1)
            End Select
        Catch ex As Exception
            retorno = ""
        End Try

        If retorno Is Nothing Then
            retorno = matriz(campo.ordem)
        End If

        Return retorno

    End Function

    Private Function insereLinhaArquivo(ByVal m As Array, ByVal ds As DataSet) As Boolean

        Try

            Dim dt_inicio_vigencia As String = ""
            Dim operacao As String = ""
            Dim nome As String = ""
            Dim cpf As String = ""
            Dim dt_nascimento As String = ""
            Dim sexo As String = ""
            Dim val_salario As String = ""
            Dim val_capital_segurado As String = ""
            Dim cpf_conjuge As String = ""
            Dim dt_fim_vigencia As String = ""

            Try
                If CType(Session("ContadorLinhas"), Integer) < CType(txtLinhasDesconsiderar.Text, Integer) Then
                    Session("ContadorLinhas") = Session("ContadorLinhas") + 1
                    If Session("Linha_1") = "" Then
                        If CType(Session("ContadorLinhas"), Integer) < CType(txtLinhasDesconsiderar.Text, Integer) Then
                            Session("Linha_1") = ""
                        Else
                            Session("Linha_1") = "1"
                        End If
                        Return True
                    End If
                End If
            Catch ex As Exception

            End Try

            Try
                dt_inicio_vigencia = ouvazio(m, CType(campos("Data de Inclusão no Seguro"), cCampo).ordem).Replace("'", "")

                If dt_inicio_vigencia.Trim.Length > 10 Then
                    dt_inicio_vigencia = dt_inicio_vigencia.Trim.Substring(0, 10)
                End If

            Catch ex As Exception

            End Try

            Try
                operacao = ouvazio(m, CType(campos("Operação"), cCampo).ordem).Replace("'", "")
            Catch ex As Exception

            End Try

            Try
                nome = ouvazio(m, CType(campos("Nome do Segurado"), cCampo).ordem).Replace("'", "''")

                If nome.Trim.Length > 60 Then
                    nome = nome.Trim.Substring(0, 60)
                End If

            Catch ex As Exception

            End Try


            Try
                cpf = ouvazio(m, CType(campos("CPF do Titular"), cCampo).ordem).Replace("'", "").Replace("-", "").Replace(".", "")

                If cpf.Trim.Length > 14 Then
                    cpf = cpf.Trim.Substring(0, 14)
                End If
            Catch ex As Exception

            End Try

            Try
                dt_nascimento = ouvazio(m, CType(campos("Data de Nascimento"), cCampo).ordem).Replace("'", "")

                If dt_nascimento.Trim.Length > 10 Then
                    dt_nascimento = dt_nascimento.Trim.Substring(0, 10)
                End If
            Catch ex As Exception

            End Try

            Try
                sexo = ouvazio(m, CType(campos("Sexo"), cCampo).ordem).Replace("'", "")
            Catch ex As Exception

            End Try

            Try
                val_salario = ouvazio(m, CType(campos("Valor do Salário"), cCampo).ordem).Replace("'", "")

                If val_salario.Trim.Length > 16 Then
                    val_salario = val_salario.Trim.Substring(0, 16)
                End If
            Catch ex As Exception

            End Try

            Try
                val_capital_segurado = ouvazio(m, CType(campos("Capital Segurado"), cCampo).ordem).Replace("'", "")

                If val_capital_segurado.Trim.Length > 16 Then
                    val_capital_segurado = val_capital_segurado.Trim.Substring(0, 16)
                End If
            Catch ex As Exception

            End Try

            Try
                cpf_conjuge = ouvazio(m, CType(campos("CPFConjuge"), cCampo).ordem).Replace("'", "").Replace("-", "").Replace(".", "")

                If cpf_conjuge.Trim.Length > 14 Then
                    cpf_conjuge = cpf_conjuge.Trim.Substring(0, 14)
                End If
            Catch ex As Exception

            End Try

            Try
                dt_fim_vigencia = ouvazio(m, CType(campos("DataDesligamento"), cCampo).ordem).Replace("'", "")

                If dt_fim_vigencia.Trim.Length > 10 Then
                    dt_fim_vigencia = dt_fim_vigencia.Trim.Substring(0, 10)
                End If
            Catch ex As Exception

            End Try

            'Tratamento dos valores
            Dim row As DataRow = ds.Tables(0).NewRow

            row.Item(0) = 6785                      'Seguradora_cod_susep
            row.Item(1) = 0                         'sucursal_seguradora_id
            row.Item(2) = Session("nomeSubgrupo")   'nome_estip
            row.Item(3) = Session("ramo")           'ramo_id
            row.Item(4) = Session("apolice")        'apolice_id
            row.Item(5) = Session("subgrupo_id")    'sub_grupo_id
            row.Item(6) = Session("usuario")        'Usuário
            row.Item(7) = Now                       'dt_inclusao

            Dim t As Integer = 8

            ' '' ''If dt_inicio_vigencia <> "" Or dt_fim_vigencia <> "" Or operacao <> "" Then
            ' '' ''    t = 8
            ' '' ''Else
            ' '' ''    t = 11
            ' '' ''End If


            For Each campo As cCampo In campos.Values
                Try
                    row.Item(t) = ouvazio(m, campo.ordem).Replace("'", "")
                Catch ex As Exception
                    row.Item(t) = "---"
                End Try
                t = t + 1
            Next

            ds.Tables(0).Rows.Add(row)

            'Monta a linha no sbuffer
            sbuffer.Append(dt_inicio_vigencia & ";" & _
                           dt_fim_vigencia & ";" & _
                           operacao & ";" & _
                           nome & ";" & _
                           cpf & ";" & _
                           dt_nascimento & ";" & _
                           sexo & ";" & _
                           val_salario & ";" & _
                           val_capital_segurado & ";" & _
                           cpf_conjuge & ";" & vbCrLf)

        Catch ex As Exception
            Dim excp As New clsException(ex, "O arquivo enviado não corresponde ao formato configurado.")
            Response.Write("<!-- ")
            Response.Write(ex.Message)
            Response.Write(ex.StackTrace)
            Response.Write("--> ")
            Return False
        End Try

        Return True

    End Function

    Private Sub mPaginacaoClick(ByVal Argumento As String) Handles ucPaginacao.ePaginaAlterada
        Me.grdPesquisa.SelectedIndex = -1
        Select Case Argumento
            Case "pro"
                Me.grdPesquisa.CurrentPageIndex += 1
            Case "ant"
                Me.grdPesquisa.CurrentPageIndex -= 1
            Case Else
                Me.grdPesquisa.CurrentPageIndex = Argumento
        End Select
        Session("indice") = Me.grdPesquisa.CurrentPageIndex
        Me.ucPaginacao.GridDataBind(grdPesquisa, Session("grdDescarte"))
    End Sub

    Private Function getPeriodoCompetenciaSession() As String
        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        Try

            bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

            dr = bd.ExecutaSQL_DR()
            Dim data_inicio As String = ""
            Dim data_fim As String = ""
            Dim retorno As String = ""

            If dr.HasRows Then
                While dr.Read()

                    data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                    data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)

                End While

                retorno = "Período de Competência: " & data_inicio & " a " & data_fim
            Else
                Try
                    If Session("sessionValidate") Is Nothing Then
                        If Request.QueryString.Keys.Count > 0 Then
                            For Each m As String In Request.QueryString.Keys
                                If m <> "" Then
                                    Session(m) = Request.QueryString(m)
                                End If
                            Next

                            Session("sessionValidate") = 1

                            If (Not Session("apolice") Is Nothing) And (Not Session("ramo") Is Nothing) And (Not Session("subgrupo_id") Is Nothing) Then
                                getPeriodoCompetenciaSession()
                                Response.End()
                            End If
                        End If
                    End If
                Catch ex As Exception

                End Try

            End If

            Return retorno

        Catch ex As Exception
            Dim excp As New clsException(ex)
            System.Web.HttpContext.Current.Response.Write("<script>try{top.escondeaguarde();}catch(err){}</script>")

        Finally
            dr.Close()
            dr = Nothing
        End Try

        Return Nothing
    End Function

    Public Function addBoundColumn(ByVal datafield As String) As BoundColumn

        Dim coluna As New BoundColumn

        coluna.DataField = datafield
        coluna.ItemStyle.HorizontalAlign = HorizontalAlign.Center

        Return coluna
    End Function

    Private Function deParaHeader(ByVal header As String) As String
        Dim valor As String = ""

        Select Case header.Trim
            Case "CPF do Titular"
                valor = "CPF do Titular"

            Case "Nome do Segurado"
                valor = "Nome do Segurado"

            Case "Data de Inclusão no Seguro"
                valor = "Data Início de Vigência"

            Case "Operação"
                valor = "Operação"

            Case "Valor do Salário"
                valor = "Salário do Segurado"

            Case "Capital Segurado"
                valor = "Capital do Segurado"

            Case "Sexo"
                valor = "Sexo"

            Case "Data de Nascimento"
                valor = "Data de Nascimento"

            Case "TipoComponente"
                valor = "Tipo de Componente"

            Case "CPFConjuge"
                valor = "CPF do Cônjuge"

            Case "DataDesligamento"
                valor = "Data de Desligamento"

        End Select

        Return valor
    End Function

    Private Sub Importar_Atual()
        Importar()
    End Sub

    Private Sub Importar_Todos()

        Dim cAmbiente As New Alianca.Seguranca.Web.ControleAmbiente
        'cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        'cAmbiente.ObterAmbiente("http://" & Request.ServerVariables("SERVER_NAME") & "/")
        'Dim post As String = cAmbiente.Post & "vida_web\"

        'PMARQUES - CONFITEC - 18/06/2010
        'Retirado Sergio
        'Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        'Dim dtTable As DataTable
        'bd.ListaEnderecoAmbiente(Request.ServerVariables("SERVER_NAME"))
        'dtTable = bd.ExecutaSQL_DT()
        'Dim post As String = dtTable.Rows(0).Item(0) & "\vida_web\"


        '' '' ''douglas
        Dim post As String = "E:\sgvpost\" 'Descomentada Sergio

        Dim DSFILE As DataSet = DsFromFile()
        Dim xlsContent As New DataTable '<----

        Dim nomesheet As String

        Session.Add("subgrupo_id_original", Session("subgrupo_id"))

        Dim cont_subgrupos As Integer = 0

        nomesheet = ""

        Dim i As Integer
        For i = 0 To DSFILE.Tables.Count - 1

            nomesheet = ""
            nomesheet = DSFILE.Tables(i).TableName '''''Trim(Replace(Replace((schema.Rows(i)("TABLE_NAME").ToString), "$", ""), "'", ""))

            If Len(nomesheet) < 4 Then
                Session("subgrupo_id") = DSFILE.Tables(i).TableName '''''dr2.GetInt32(0).ToString()

                DTIMPORT = DSFILE.Tables(i)

                cont_subgrupos = cont_subgrupos + 1

                If cont_subgrupos = Session("qtd_Subgrupos") Then
                    Importar("ATUAL")
                Else
                    Importar("TODOS")
                End If


            Else

            End If

        Next

    End Sub

    Protected Sub btnAcao_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAcao.Click
        If Me.rblSelecao.SelectedValue = "Atual" Then
            Importar_Atual()
        Else
            Importar_Todos()
        End If
    End Sub

    Protected Function DsFromFile() As DataSet
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)
        Dim xlsContent As New DataTable
        Dim nomesheet As String = ""
        Dim DSFILE As New DataSet
        Dim schema As Data.DataTable = Nothing
        Dim possui_subgrupo As Boolean = False
        Dim coluna_subgrupo As Integer = 0
        Dim subgrupo As String

        ' Autor: italo.souza (Nova Consultoria) - Data da Alteração: 19/01/2012
        ' Demanda: 12784293 - Item: Permitir a importação de arquivo Excel 2007 e superior
        Dim dbConnection As New OleDb.OleDbConnection()
        'dbConnection = ObterConexaoExcel(Session("strCaminho"), txtLinhasDesconsiderar.Text <> 0)
        dbConnection = f_ConexaoExcel(Session("strCaminho"), txtLinhasDesconsiderar.Text <> 0)

        Try
            dbConnection.Open()
        Catch ex As Exception
            sMensagemErro = "A extensão deste arquivo não é permitido, favor salvar o arquivo com o formato XLS 97-2003"
            bErro = True
            Return New DataSet
        End Try

        schema = dbConnection.GetOleDbSchemaTable(OleDb.OleDbSchemaGuid.Tables, Nothing)

        For i As Integer = 0 To schema.Rows.Count - 1
            nomesheet = ""
            nomesheet = Replace(Replace((schema.Rows(i)("TABLE_NAME").ToString), "$", ""), "'", "")
            If IsNumeric(nomesheet) Then


                'FLOW 5791981 - FATURAMENTO WEB - ERRO IMPORTAÇÃO
                'Douglas Oliveira - Confitec - 08/10/2010
                'Tratamento para não receber subgrupos que não estejam cadastrados no SEGBR

                bd.consulta_subgrupo_sps(Session("apolice"), Session("ramo"), nomesheet)
                Dim dt2 As DataTable = bd.ExecutaSQL_DT()

                If dt2.Rows.Count < 1 Then
                    possui_subgrupo = False
                    Return DSFILE
                End If
                '--------------------------------------------------------------------------



                bd.SEGS8428_SPS(Session("apolice"), Session("ramo"), nomesheet)
                Dim dt As DataTable = bd.ExecutaSQL_DT()

                'novo: evitando o erro
                If dt.Rows.Count < 1 Then
                    possui_subgrupo = False
                    'coluna_subgrupo = (dt.Rows(0).Item(0))
                Else
                    possui_subgrupo = True
                    coluna_subgrupo = (dt.Rows(0).Item(0))
                End If

                Dim DTNEW As New DataTable(nomesheet)

                Dim SQL As String = "select * from [" & nomesheet & "$]"
                Dim adapter As New OleDb.OleDbDataAdapter(SQL, dbConnection)
                adapter.Fill(DTNEW)

                If CType(txtLinhasDesconsiderar.Text, Integer) > 1 Then
                    Dim linhas_eliminar As Integer = CType(txtLinhasDesconsiderar.Text, Integer)
                    Dim ExcluirLinha As Integer = 0
                    While linhas_eliminar > 1
                        For c As Integer = 0 To DTNEW.Columns.Count - 1
                            If Not IsDBNull(DTNEW.Rows(ExcluirLinha)(c)) Then
                                DTNEW.DefaultView.Table.Columns(c).ColumnName = DTNEW.Rows(ExcluirLinha)(c)
                            End If
                        Next
                        DTNEW.Rows(ExcluirLinha).Delete()
                        ExcluirLinha = ExcluirLinha + 1
                        linhas_eliminar = linhas_eliminar - 1
                    End While

                End If
                DTNEW.DefaultView.Sort = DTNEW.DefaultView.Table.Columns(coluna_subgrupo).Caption
                If possui_subgrupo Then
                    DTNEW = DTNEW.DefaultView.ToTable
                    DTNEW.Columns.Remove(DTNEW.DefaultView.Table.Columns(coluna_subgrupo).Caption)
                End If

                DSFILE.Tables.Add(DTNEW.DefaultView.ToTable)
            Else
                Dim DTNEW As New DataTable(nomesheet)

                Dim SQL As String = "select * from [" & nomesheet & "$]"
                Dim adapter As New OleDb.OleDbDataAdapter(SQL, dbConnection)
                adapter.Fill(DTNEW)

                If CType(txtLinhasDesconsiderar.Text, Integer) > 1 Then
                    Dim linhas_eliminar As Integer = CType(txtLinhasDesconsiderar.Text, Integer)
                    Dim ExcluirLinha As Integer = 0
                    While linhas_eliminar > 1
                        For c As Integer = 0 To DTNEW.Columns.Count - 1
                            If Not IsDBNull(DTNEW.Rows(ExcluirLinha)(c)) Then
                                DTNEW.DefaultView.Table.Columns(c).ColumnName = DTNEW.Rows(ExcluirLinha)(c)
                            End If
                        Next
                        DTNEW.Rows(ExcluirLinha).Delete()
                        ExcluirLinha = ExcluirLinha + 1
                        linhas_eliminar = linhas_eliminar - 1
                    End While

                End If
                DTNEW = DTNEW.DefaultView.ToTable


                bd.SEGS8428_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
                Dim dt As DataTable = bd.ExecutaSQL_DT()

                '                Dim possui_subgrupo As Boolean = False
                '                Dim coluna_subgrupo As Integer = 0
                If DTNEW.Rows.Count > 0 Then
                    If dt.Rows.Count >= 1 Then
                        coluna_subgrupo = (dt.Rows(0).Item(0))

                        If coluna_subgrupo <= (DTNEW.Columns.Count - 1) Then

                            Dim distinctTable As DataTable = DTNEW.DefaultView.ToTable(True, DTNEW.Columns(coluna_subgrupo).ToString)


                            'FLOW 5791981 - FATURAMENTO WEB - ERRO IMPORTAÇÃO
                            'Douglas Oliveira - Confitec - 08/10/2010
                            'Tratamento para não receber subgrupos que não estejam cadastrados no SEGBR
                            'e não permitir importação de linha vazia

                            For cont As Integer = 0 To distinctTable.Rows.Count - 1
                                subgrupo = distinctTable.Rows(cont).Item(0).ToString
                                If subgrupo <> "" Then
                                    bd.consulta_subgrupo_sps(Session("apolice"), Session("ramo"), subgrupo)
                                    Dim dt2 As DataTable = bd.ExecutaSQL_DT()

                                    If dt2.Rows.Count < 1 Then
                                        possui_subgrupo = False
                                        Return DSFILE
                                    End If
                                Else
                                    possui_subgrupo = False
                                    Return DSFILE
                                End If
                            Next
                            '--------------------------------------------------------------------------



                            For Each row As DataRow In distinctTable.Rows

                                bd.SEGS8428_SPS(Session("apolice"), Session("ramo"), row.Item(0).ToString)
                                dt = bd.ExecutaSQL_DT()

                                If dt.Rows.Count > 0 Then
                                    possui_subgrupo = True
                                    coluna_subgrupo = dt.Rows(0).Item(0) '0
                                End If

                                Dim dts As DataTable = DTNEW.Clone()
                                dts.TableName = row(0)


                                Dim coluna As String = "[" & DTNEW.Columns(coluna_subgrupo).Caption & "]" & " = " & row(0)

                                For Each r As DataRow In DTNEW.Select(coluna)
                                    dts.ImportRow(r)
                                Next

                                If possui_subgrupo Then
                                    dts = dts.DefaultView.ToTable
                                    dts.Columns.Remove(DTNEW.DefaultView.Table.Columns(coluna_subgrupo).Caption)
                                End If

                                DSFILE.Tables.Add(dts.DefaultView.ToTable)

                            Next
                        End If
                    Else
                        DTNEW.TableName = Session("subgrupo_id")
                        DSFILE.Tables.Add(DTNEW.DefaultView.ToTable)
                    End If

                    Exit For
                End If
            End If
        Next

        Return DSFILE
    End Function
End Class