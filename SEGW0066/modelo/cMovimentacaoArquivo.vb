Public Class cMovimentacaoArquivo

#Region "Atributos"
    Private _apolice As Int64
    Private _ramo As Int16
    Private _subgrupo As Int16
    Private _nomeArquivo As String
    Private _novoNomeArquivo As String
    Private _strCaminho As String
    Private _layout As cLayout
#End Region

#Region "Propriedades"
    Public Property APOLICE()
        Get
            Return Me._apolice
        End Get
        Set(ByVal Value)
            Me._apolice = Value
        End Set
    End Property
    Public Property RAMO()
        Get
            Return Me._ramo
        End Get
        Set(ByVal Value)
            Me._ramo = Value
        End Set
    End Property
    Public Property SUBGRUPO()
        Get
            Return Me._subgrupo
        End Get
        Set(ByVal Value)
            Me._subgrupo = Value
        End Set
    End Property
    Public Property NOMEARQUIVO()
        Get
            Return Me._nomeArquivo
        End Get
        Set(ByVal Value)
            Me._nomeArquivo = Value
        End Set
    End Property
    Public Property NOVONOMEARQUIVO()
        Get
            Return Me._novoNomeArquivo
        End Get
        Set(ByVal Value)
            Me._novoNomeArquivo = Value
        End Set
    End Property
    Public Property CAMINHO()
        Get
            Return Me._strCaminho
        End Get
        Set(ByVal Value)
            Me._strCaminho = Value
        End Set
    End Property
    Public Property LAYOUT()
        Get
            Return Me._layout
        End Get
        Set(ByVal Value)
            Me._layout = Value
        End Set
    End Property
#End Region


    Public Sub New()

    End Sub

    Public Sub New(ByVal apolice As Int64, ByVal ramo As Int16, ByVal subgrupo As Int16)
        Me.APOLICE = apolice
        Me.RAMO = ramo
        Me.SUBGRUPO = subgrupo

        Me.NOVONOMEARQUIVO = CType(Me.RAMO, String).Trim & CType(Me.APOLICE, String).Trim & CType(Me.SUBGRUPO, String).Trim & "_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & "_" & Now.Hour.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0")
    End Sub

#Region "M�todos"
    ''' Verifica se existe algum layout ativo. Caso n�o exista, 
    ''' o usu�rio n�o poder� fazer o upload de arquivo.
    Public Function isLayoutAtivo() As Boolean
        Dim retorno As Boolean = False

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)
        bd.getLayoutArquivoAtivos(Me.APOLICE, Me.RAMO, Me.SUBGRUPO, 1)

        Dim dr As Data.SqlClient.SqlDataReader
        Try

            dr = bd.ExecutaSQL_DR()

            If dr.HasRows Then
                retorno = True
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        Finally
            dr.Close()
            dr = Nothing
            bd = Nothing
        End Try

        Return retorno

    End Function
    '''Define os diret�rio onde ser� gravado o arquivo
    Public Sub controleArquivo()
        Dim ambiente As New Alianca.Seguranca.Web.ControleAmbiente
        ambiente.ObterAmbiente(System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToUpper)

        Dim post As String = ambiente.Post
        post = post & "vida_web\"

        If Not IO.Directory.Exists(post) Then
            IO.Directory.CreateDirectory(post)
        End If

        Me.CAMINHO = post & Me.NOVONOMEARQUIVO

        'Insere o controle de postagem.
        Me.insereControleArquivo(post, Me.NOVONOMEARQUIVO)
    End Sub
    '''Insere o controle do processo de upload
    Private Sub insereControleArquivo(ByVal path As String, ByVal arquivo As String)

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        bd.insereProcessoUpload("'" & path & "'", "'" & arquivo & "'", System.Web.HttpContext.Current.Session("usuario"))

        Try
            bd.ExecutaSQL()
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
        bd = Nothing
    End Sub
    '''Busca o layout definido para aquela ap�lice ramo e subgrupo
    Public Sub getLayoutApolice()
        Me.LAYOUT = cLayout.getLayout(Me.APOLICE, Me.RAMO, Me.SUBGRUPO)
    End Sub
#End Region

End Class
