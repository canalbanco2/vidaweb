Imports System.Reflection
Imports System.Data
Imports System.Web.UI

Public Class MovimentacaoMassa
    Inherits System.Web.UI.Page

    Public dominio_sexo As New Collections.Specialized.StringDictionary
    Public dominio_operacao As New Collections.Specialized.StringDictionary
    Public dominio_vida As New Collections.Specialized.StringDictionary
    Protected WithEvents col0 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents col1 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents col2 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents col3 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents col4 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents col5 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents col6 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents col7 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents col8 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents col10 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents col9 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents chkDescCabecalho As System.Web.UI.WebControls.CheckBox
    Protected WithEvents Literal1 As System.Web.UI.WebControls.Literal
    Protected WithEvents hdf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblQtdLinhas As System.Web.UI.WebControls.Label
    Public campos As New Collections.Specialized.ListDictionary

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblVigencia As System.Web.UI.WebControls.Label
    Protected WithEvents lblNavegacao As System.Web.UI.WebControls.Label
    Protected WithEvents pnlTipodeMovimentacao As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlModelodoArquivo As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlMensagemEnviada As System.Web.UI.WebControls.Panel
    Protected WithEvents lblTipoMovimentacao As System.Web.UI.WebControls.Label
    Protected WithEvents rdSobreposicao As System.Web.UI.WebControls.RadioButton
    Protected WithEvents pnlProcurar As System.Web.UI.WebControls.Panel
    Protected WithEvents fileProcurar As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents rdIncremental As System.Web.UI.WebControls.RadioButton
    Protected WithEvents btnImportar As System.Web.UI.WebControls.Button
    Protected WithEvents lblModelo As System.Web.UI.WebControls.Label
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents btnFechaPagina As System.Web.UI.WebControls.Button
    Protected WithEvents btnConfirmar As System.Web.UI.WebControls.Button
    Protected WithEvents lblListagemEnviada As System.Web.UI.WebControls.Label
    Protected WithEvents pnlMovimentacao As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlDependente As System.Web.UI.WebControls.Panel
    Protected WithEvents imgPlus As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents escolhaTpMovimentacao As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlQualidade As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlProducao As System.Web.UI.WebControls.Panel



    Protected WithEvents pnlSimples As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlConstrucao As System.Web.UI.WebControls.Panel
    Protected WithEvents Form1 As System.Web.UI.HtmlControls.HtmlForm
    Protected WithEvents pnlMsgSemLayout As System.Web.UI.WebControls.Panel

    Protected WithEvents grdPesquisa As System.Web.UI.WebControls.DataGrid
    Protected WithEvents gridMovimentacao As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents ucPaginacao As paginacaogrid
    Protected WithEvents lblSemUsuario As System.Web.UI.WebControls.Label
    Dim sbuffer As System.Text.StringBuilder = New System.Text.StringBuilder
    Protected WithEvents pnlConfirmar As System.Web.UI.HtmlControls.HtmlGenericControl



    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Trace.Write("In�cio do Page Load")

        'acintra 20080501 - in�cio
        If hdf.Value = "1" Then
            hdf.Value = "2"
            Literal1.Text = ""
            ApagarRegistroImportadoNaoProcessado(Session("apolice"), _
                                                 Session("ramo"), _
                                                 Session("subgrupo_id"))

            Importar()

        ElseIf hdf.Value = "0" Then
            Literal1.Text = ""
        End If
        'acintra 20080501 - fim

        'Implementa��o do LinkSeguro
        Dim linkseguro As New Alianca.Seguranca.Web.LinkSeguro

        Dim usuario As String = linkseguro.LerUsuario("", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))
        If usuario = "" Then
            Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
        End If

        Session("usuario_id") = linkseguro.Usuario_ID
        '''[New code 05.06.07 - Alterar login de rede p/ login_web]
        Session("usuario") = linkseguro.Login_WEB
        '''[end code]
        Session("cpf") = linkseguro.CPF

        'Implementa��o do controle de ambiente
        Dim cAmbiente As New Alianca.Seguranca.Web.ControleAmbiente

        If Not Page.IsPostBack Then
            Session("indice") = 0
            For Each m As String In Request.QueryString.Keys
                If m <> "" Then
                    Session(m) = Request.QueryString(m)
                End If
            Next
        End If

        Try
            If Session("apolice") = "" Then
                cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
            End If
        Catch ex As Exception
            cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
            Response.End()
        End Try

        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
        cAmbiente.ObterAmbiente(url)
        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produ��o)
        Else '
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produ��o)
        End If

        If cAmbiente.Ambiente = Alianca.Seguranca.BancoDados.cCon.Ambientes.Produ��o Then
            pnlProducao.Visible = True
            pnlQualidade.Visible = False
        Else
            pnlProducao.Visible = False
            pnlQualidade.Visible = True
        End If

        Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)

        If Not Page.IsPostBack Then

            'Inicio do desenvolvimento da p�gina
            lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomeSubgrupo") & "<br>Fun��o: Movimenta��o por arquivo"

            lblVigencia.Text = getPeriodoCompetenciaSession()

            lblTipoMovimentacao.Attributes.Add("style", "cursor:default")
            pnlProcurar.Visible = True
            lblListagemEnviada.Visible = False
            btnImportar.Attributes.Add("onclick", "return confirmaModelo()")
            btnConfirmar.Attributes.Add("onclick", "top.exibeaguarde()")
        End If
        Trace.Write("Fim do Page Load")
    End Sub

    Private Sub btnEnviarListagem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pnlModelodoArquivo.Visible = False
        pnlProcurar.Visible = False
        pnlTipodeMovimentacao.Visible = False
        pnlMensagemEnviada.Visible = True
        rdIncremental.Checked = False
        rdSobreposicao.Checked = False
        cUtilitarios.escreveScript("bloqueiaLinks();")
    End Sub

    Private Sub rdIncremental_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pnlProcurar.Visible = True
        pnlModelodoArquivo.Visible = False
        rdSobreposicao.Checked = False
    End Sub

    Private Sub mConsultaRegistros(ByVal ds As DataSet)

        Try
            Dim dt As DataTable
            dt = ds.Tables(0)

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then

                Me.grdPesquisa.CurrentPageIndex = Session("indice")
                Me.grdPesquisa.SelectedIndex = -1
                Me.ucPaginacao._valor = "2"
                Session("grdDescarte") = dt
                Me.ucPaginacao.GridDataBind(Me.grdPesquisa, dt, Me)


                Me.lblSemUsuario.Visible = False
                ucPaginacao.Visible = True
                grdPesquisa.Visible = True
                gridMovimentacao.Visible = True

            Else
                Session.Remove("indice")
                Session.Remove("grdDescarte")
                Me.lblSemUsuario.Text = "<br><br><br>Nenhum Registro encontrado.<br><br>"
                Me.btnConfirmar.Visible = False
                Me.lblSemUsuario.Visible = True
                Me.pnlConfirmar.Visible = False
                ucPaginacao.Visible = False
                grdPesquisa.Visible = False
                gridMovimentacao.Visible = False
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Sub rdSobreposicao_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pnlProcurar.Visible = True
        pnlModelodoArquivo.Visible = False
        rdIncremental.Checked = False
    End Sub

    Private Sub mCarregaDominios(ByVal layout_id As Integer)

        Dim bd As New cAcompanhamento
        '''Pega os dom�nios para o tipo do Sexo
        bd.getDominiosLayout(layout_id, "S")

        Dim dr As Data.SqlClient.SqlDataReader
        Try
            dr = bd.ExecutaSQL_DR()
            While dr.Read
                dominio_sexo.Add(dr.GetString(1), dr.GetString(2))
            End While
        Catch ex As Exception
            'Dim excp As New clsException(ex)
        Finally
            dr.Close()
            dr = Nothing
        End Try

        '''Pega os dom�nios para o tipo de Opera��o
        bd.getDominiosLayout(layout_id, "O")
        
        Try
            dr = bd.ExecutaSQL_DR()
            While dr.Read
                dominio_operacao.Add(dr.GetString(1), dr.GetString(2))
            End While
        Catch ex As Exception
            'Dim excp As New clsException(ex)
        Finally
            dr.Close()
            dr = Nothing
        End Try

        '''Pega os dom�nios para o tipo de Vida
        bd.getDominiosLayout(layout_id, "D")

        Try
            dr = bd.ExecutaSQL_DR()
            While dr.Read

                dominio_vida.Add(dr.GetString(1), dr.GetString(2))
            End While
        Catch ex As Exception
            'Dim excp As New clsException(ex)
        Finally
            dr.Close()
            dr = Nothing
        End Try

    End Sub

    Private Sub btnImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportar.Click

        Importar()

    End Sub

    Private Sub Importar()

        'verifica se existe layout de arquivo
        If Not verificaLayOutAtivo(Session("apolice"), Session("ramo"), Session("subgrupo_id")) Then
            pnlMsgSemLayout.Visible = True
            '''Adicionado alert quando nao tem layout (04/03/2007)
            cUtilitarios.br("Seu arquivo n�o foi carregado. Por favor, definir layout.")
            cUtilitarios.escreveScript("top.document.getElementById('Link6').click()")
            pnlSimples.Visible = False
            Exit Sub
        End If
        Trace.Write("Verificou se tem layout ativo")

        Dim ambiente As New Alianca.Seguranca.Web.ControleAmbiente

        ambiente.ObterAmbiente(Request.Url.AbsoluteUri.ToUpper)

        Dim post As String = ambiente.Post & "vida_web\"
        Dim strCaminho As String

        'acintra 20080506
        If chkDescCabecalho.Checked Then
            Session("chkDescartaCabecalho") = "S"
        Else
            Session("chkDescartaCabecalho") = "N"
        End If

        If hdf.Value = "" Then

            Session("Arquivo") = fileProcurar.PostedFile.FileName.ToString

            Dim nome_arquivo As String = CType(Session("ramo"), String).Trim & CType(Session("apolice"), String).Trim & CType(Session("subgrupo_id"), String).Trim & "_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & "_" & Now.Hour.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0")

            If Not IO.Directory.Exists(post) Then
                IO.Directory.CreateDirectory(post)
            End If

            Session("Caminho") = post & nome_arquivo

            fileProcurar.PostedFile.SaveAs(Session("Caminho"))

        End If

        strCaminho = Session("Caminho")

        Dim nomeArquivo As String = Session("Arquivo")

        '''Obtem o tipo do arquivo a ser feito upload
        Dim tipo_arquivo As String
        Select Case IO.Path.GetExtension(nomeArquivo).ToLower
            Case ".xls"
                tipo_arquivo = "E"
            Case ".csv"
                tipo_arquivo = "C"
            Case ".txt"
                tipo_arquivo = "T"
        End Select

        'acintra 20080501 - fn�cio
        If ExisteRegistroImportadoNaoProcessado(Session("apolice"), _
                                                Session("ramo"), _
                                                Session("subgrupo_id")) Then

            Literal1.Text = "<script language='javascript'> if(confirm( ""Existem registros importados n�o processados. Deseja prosseguir e elimin�-los?"" )) { document.forms[0].hdf.value='1';document.forms[0].submit(); } else { document.forms[0].hdf.value='0';document.forms[0].submit();}</script>"
            Exit Sub

        End If
        'acintra 20080502 - fim

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        'Pega dados do Layout ativo
        bd.SEGS5763_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), tipo_arquivo)
        Dim dt As DataTable = bd.ExecutaSQL_DT()
        Dim layout_id As Integer

        '''novo: evitando o erro
        If dt.Rows.Count > 0 Then
            layout_id = dt.Rows(0).Item(1)
        Else
            pnlMsgSemLayout.Visible = True
            '''Adicionado alert quando nao tem layout (04/03/2007)
            cUtilitarios.br("Seu arquivo n�o foi carregado. Por favor, definir layout.")
            cUtilitarios.escreveScript("top.document.getElementById('Link6').click()")
            pnlSimples.Visible = False
            Exit Sub
        End If
        '-------------------------------------------------

        Session("sIdLayout") = layout_id
        Dim separador As Char = CType(dt.Rows(0).Item(2), String).Chars(0)


        Me.mCarregaDominios(layout_id)

        'Preenche as colunas
        Dim ds As DataSet
        bd.SEGS5849_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), 1)
        bd.ExecutaSQL()
        ds = bd.DS

        For Each d As Data.DataColumn In ds.Tables(0).Columns
            'cUtilitarios.br(d.DataType.ToString)
            d.DataType = Type.GetType("System.String")
            Type.GetType("System.String")

        Next
        'ds.Tables(0).Columns.iColumns(0).DataType = Type.GetType("String")

        ''Mapeamento dos campos do Layout com a ordem dos par�metros da SP
        Dim ordem_correta As Collection = New Collection
        Dim tamanhos As Collection = New Collection
        Dim final As Integer = 0

        For j As Integer = 0 To dt.Rows.Count - 1

            'Parada do for se mudar o layout id
            Dim verificador_layout_id As Integer = dt.Rows(j).Item(1)
            If verificador_layout_id <> layout_id Then
                Exit For
            End If

            Select Case dt.Rows(j).ItemArray(4)
                Case "DataRisco", "Opera��o", "NomeSegurado", "CPFTitular", "DataNascimento", "Sexo", "sexo", "ValorSalario", "ValorCapital", "CPFConjuge", "DataDesligamento"
                    Me.campos.Add(dt.Rows(j).ItemArray(4), New cCampo(dt.Rows(j).ItemArray(4), dt.Rows(j).ItemArray(5), dt.Rows(j).ItemArray(6), dt.Rows(j).ItemArray(7)))
                    Me.grdPesquisa.Columns.Item(final).HeaderText = Me.deParaHeader(dt.Rows(j).ItemArray(4))
                    CType(Me.FindControl("col" & final), System.Web.UI.HtmlControls.HtmlGenericControl).InnerHtml = Me.deParaHeader(dt.Rows(j).ItemArray(4))
                    final = final + 1
                Case "TipoComponente"
                Case "--Descartar--"
                Case Else
            End Select
            'Popula os tamanhos
            tamanhos.Add(dt.Rows(j).ItemArray(6), "" & j)

        Next

        Try

            'gravaExcel
            If tipo_arquivo = "E" Then

                'Abre o arquivo e o salva como csv
                Try
                    Dim xls As New OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & strCaminho & ";Extended Properties=""Excel 8.0;HDR=NO;IMEX=1""")
                    xls.Open()
                    Dim schema As Data.DataTable = xls.GetOleDbSchemaTable(OleDb.OleDbSchemaGuid.Tables, Nothing)

                    Dim tableName As String = schema.Rows(0)(2).ToString
                    Dim xlsContent As New DataTable

                    Try

                        Dim xlsRead As New OleDb.OleDbDataAdapter("select * from [plan1$]", xls)

                        xlsRead.Fill(xlsContent)
                        xlsRead.Dispose()
                    Catch ex As Exception
                        Dim xlsRead As New OleDb.OleDbDataAdapter("select * from [" & tableName & "]", xls)

                        xlsRead.Fill(xlsContent)
                        xlsRead.Dispose()

                    End Try

                    xls.Close()

                    Dim m As DataRow

                    'Insere as Colunas sobressalentes caso n�o possua
                    If xlsContent.Rows(0).ItemArray.GetLength(0) < 11 Then

                        For x As Integer = xlsContent.Rows(0).ItemArray.GetLength(0) - 1 To 10
                            Dim dc As DataColumn = New DataColumn("c" & x)
                            xlsContent.Columns.Add(dc)
                        Next

                    End If

                    lblQtdLinhas.Text = xlsContent.Rows.Count & " linhas no arquivo"

                    For z As Integer = 0 To xlsContent.Rows.Count - 1
                        Try
                            m = xlsContent.Rows(z)
                            'm.
                            If Not insereLinhaArquivo(m, ds) Then
                                Exit For
                            End If
                        Catch ex As Exception
                            Dim excp1 As New clsException(ex)
                        End Try
                    Next

                    xlsContent.Clear()

                Catch ex As Exception
                    Dim excp As New clsException(ex)
                End Try
                'Grava CSV
            ElseIf tipo_arquivo = "C" Then

                'Extrai os registros do CSV
                Dim arquivo As IO.StreamReader = IO.File.OpenText(strCaminho)
                Dim linha As String = ""
                Dim aux As String = ""
                Dim dados() As String = {"", "", "", "", "", "", "", "", "", "", ""}
                Dim cont As Integer
                Dim contador As Integer = 0
                Dim pos As Integer = 0
                Dim separadores() As Char = {separador, vbCrLf}

                While arquivo.Peek() > -1
                    cont = 0
                    contador = contador + 1
                    linha = arquivo.ReadLine()
                    linha = linha.Trim() & vbCrLf
                    'Separa as Colunas de acordo com o delimitador

                    While linha <> ""
                        pos = linha.IndexOfAny(separadores)
                        'Verifica se n�o foi enviado o ; final e captura o registro final da linha
                        If pos < 0 Then
                            Exit While
                        End If

                        ''' New code
                        aux = linha.Substring(0, pos)

                        If cont >= dados.Length Then
                            Exit While
                        End If

                        dados(cont) = aux

                        ''' end code
                        Try
                            linha = linha.Substring(pos + 1, linha.Length - (pos + 1))
                        Catch ex As Exception
                            linha = ""
                        End Try
                        cont = cont + 1
                    End While
                    'Insere no BD
                    If Not insereLinhaArquivo(dados, ds) Then
                        Exit While
                    End If

                    lblQtdLinhas.Text = contador & " linhas no arquivo"

                End While
                arquivo.Close()
                'GRAVA TXT
            Else
                'Extrai os registros do Txt
                Dim arquivo As IO.StreamReader = IO.File.OpenText(strCaminho)
                Dim linha As String = ""
                Dim aux As String = ""
                Dim dados() As String = {"", "", "", "", "", "", "", "", "", "", ""}
                Dim cont As Integer
                Dim contador As Integer = 0
                Dim pos As Integer = 0

                While arquivo.Peek() > -1
                    cont = 0
                    contador = contador + 1
                    linha = arquivo.ReadLine()

                    linha = linha.TrimEnd(" ")
                    'Separa as Colunas de acordo com o tamanho
                    While linha <> ""
                        Try
                            aux = linha.Substring(0, tamanhos.Item("" & cont))
                        Catch ex As Exception
                            aux = linha.Substring(0)
                        End Try
                        dados(cont) = aux
                        Try
                            linha = linha.Substring(tamanhos.Item("" & cont), linha.Length - tamanhos.Item("" & cont))
                        Catch ex As Exception
                            linha = ""
                        End Try
                        cont = cont + 1
                    End While
                    'Insere no BD
                    insereLinhaArquivo(dados, ds)

                    lblQtdLinhas.Text = contador & " linhas no arquivo"

                End While
                arquivo.Close()
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex, "O arquivo enviado n�o corresponde ao formato configurado.")
        End Try

        pnlModelodoArquivo.Visible = True
        pnlProcurar.Visible = False
        pnlMovimentacao.Visible = False
        pnlTipodeMovimentacao.Visible = False
        pnlMensagemEnviada.Visible = True

        For i As Int16 = Me.campos.Count To 9
            'cUtilitarios.escreve(Me.campos.Count)
            'Me.grdPesquisa.Columns.RemoveAt(Me.campos.Count)
            Me.grdPesquisa.Columns.Item(i).Visible = False
        Next

        'Me.grdPesquisa.Columns.RemoveAt(4)

        'Coloca na Session o DataSet e o arquivo enviado
        Session("sStrCaminho") = strCaminho
        Session("ssbuffer") = sbuffer

        'Exibe a Grid
        mConsultaRegistros(ds)

        Session("Linha_1") = ""

    End Sub
    'acintra
    Private Sub inserirNoBanco(ByVal arquivo As String)

        Trace.Write("Bulk Insert", "Come�ou")
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        bd.SEGS5787_SPI(Session("controla_processo_id"), _
                        Session("ramo"), _
                        Session("apolice"), _
                        Session("subgrupo_id"), _
                        Session("Caminho"), _
                        IO.Path.GetFileName(Session("Arquivo")), _
                        Session("usuario") & "'", _
                        Session("chkDescartaCabecalho"))

        Try
            bd.ExecutaSQL()

        Catch ex As Exception

            Dim excp As New clsException(ex)

        End Try

        Trace.Write("Bulk Insert", "Terminou")

    End Sub

    Private Function Trata_Nome_Arquivo(ByVal File As String) As String
        File = File.Replace(":", ":\").Replace("vida_web", "\vida_web\")
        Return File
    End Function

    Private Sub btnFechaPagina_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("Centro.aspx")
    End Sub

    Private Sub btnConfirmar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmar.Click
        pnlMovimentacao.Visible = True
        pnlModelodoArquivo.Visible = False
        pnlProcurar.Visible = False
        pnlTipodeMovimentacao.Visible = False
        pnlMensagemEnviada.Visible = True
        lblListagemEnviada.Visible = True

        Dim strCaminho As String = Session("sStrCaminho")
        sbuffer = Session("ssbuffer")

        Try

            'Grava o arquivo csv com marcador ';'
            Dim writer As IO.StreamWriter = IO.File.CreateText(strCaminho)
            writer.Write(sbuffer.ToString)
            writer.Close()

            'Insere no banco o arquivo
            insereControleArquivo(Session("ramo"), _
                                  Session("apolice"), _
                                  Session("subgrupo_id"), _
                                  Session("Caminho"), _
                                  IO.Path.GetFileName(Session("Arquivo")))

            Session("controla_processo_id") = ObterControleArquivo()

            inserirNoBanco(strCaminho)
            cUtilitarios.MoveArquivo(strCaminho, Session("GLAMBIENTE_ID"))

            'Atualiza menu, bloqueando acesso � SEGW0078
            'cUtilitarios.br("Bloqueando...")
            cUtilitarios.escreveScript("top.document.getElementById('Hyperlink4').onclick = function() { alert('Neste momento, n�o � poss�vel Carregar Rela��o Anterior.');return false;}")
            cUtilitarios.escreveScript("top.document.getElementById('Link6').onclick = function() { alert('Neste momento, n�o � poss�vel realizar a movimenta��o por arquivo.');return false;}")
        Catch ex As Exception
            'Dim excp As New clsException(ex)
        End Try

    End Sub

    ''' Verififca se existe layout de arquivo definido.
    Private Function verificaLayOutAtivo(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer) As Boolean

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)
        bd.getLayoutArquivoAtivos(Session("apolice"), Session("ramo"), Session("subgrupo_id"), 1)

        Dim dr As Data.SqlClient.SqlDataReader
        Try

            dr = bd.ExecutaSQL_DR()

            If dr.HasRows Then
                Return True
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        Finally
            dr.Close()
            dr = Nothing
        End Try

        Return False

    End Function

    Private Function ExisteRegistroImportadoNaoProcessado(ByVal apolice_id As Integer, _
                                                          ByVal ramo_id As Integer, _
                                                          ByVal subgrupo_id As Integer) As Boolean

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)
        bd.VerificarImportacaoAnterior(Session("apolice"), _
                                       Session("ramo"), _
                                       Session("subgrupo_id"))

        Dim dr As Data.SqlClient.SqlDataReader
        Try

            dr = bd.ExecutaSQL_DR()

            If dr.HasRows Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        Finally
            dr.Close()
            dr = Nothing
        End Try

    End Function

    Private Function ApagarRegistroImportadoNaoProcessado(ByVal apolice_id As Integer, _
                                                          ByVal ramo_id As Integer, _
                                                          ByVal subgrupo_id As Integer) As Boolean

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        bd.ApagarImportacaoAnterior(Session("apolice"), _
                                    Session("ramo"), _
                                    Session("subgrupo_id"))

        Try
            bd.ExecutaSQL()
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Function
    'acintra 20080502
    Private Sub insereControleArquivo(ByVal iRamoId As Integer, _
                                      ByVal lApoliceId As Long, _
                                      ByVal iSubGrupoId As Integer, _
                                      ByVal sPathArquivo As String, _
                                      ByVal sNomeArquivo As String)

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        bd.insereProcessoUpload(iRamoId, _
                                lApoliceId, _
                                iSubGrupoId, _
                                sPathArquivo, _
                                sNomeArquivo, _
                                "P", _
                                Session("usuario"), _
                                CType(Now, DateTime).ToString("yyyMMdd"), _
                                Session("usuario"))

        Try
            bd.ExecutaSQL()
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Private Function ObterControleArquivo() As Integer

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        Dim dr As Data.SqlClient.SqlDataReader

        Try

            bd.ObterUltimoControleArquivo(Session("apolice"), _
                                          Session("ramo"), _
                                          Session("subgrupo_id"))

            dr = bd.ExecutaSQL_DR()

            If dr.HasRows Then
                While dr.Read()
                    ObterControleArquivo = dr.GetValue(0)
                End While
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Function


    Private Function insereLinhaArquivo(ByVal m As DataRow, ByRef ds As DataSet) As Boolean

        Try
            Dim operacao As String = ""
            Dim dt_inicio_vigencia As String = ""
            Dim nome As String = ""
            Dim cpf As String = ""
            Dim dt_nascimento As String = ""
            Dim sexo As String = ""
            Dim val_salario As String = ""
            Dim val_capital_segurado As String = ""
            Dim cpf_conjuge As String = ""
            Dim dt_fim_vigencia As String = ""
            Dim iIndice As Integer = 0

            Try
                If chkDescCabecalho.Checked Then
                    If Session("Linha_1") = "" Then
                        Session("Linha_1") = "1"
                        Return True
                    End If
                End If
            Catch ex As Exception

            End Try

            Try
                dt_inicio_vigencia = ouvazio(m.ItemArray, CType(campos("DataRisco"), cCampo).ordem).Replace("'", "''")

                If dt_inicio_vigencia.Length > 10 Then
                    dt_inicio_vigencia = dt_inicio_vigencia.Substring(0, 10)
                End If

            Catch ex As Exception

            End Try

            Try
                operacao = ouvazio(m.ItemArray, CType(campos("Opera��o"), cCampo).ordem).Replace("'", "''")

                ''If operacao.Length > 2 Then
                ''    operacao = operacao.Substring(0, 2)
                ''End If
            Catch ex As Exception

            End Try

            Try
                nome = ouvazio(m.ItemArray, CType(campos("NomeSegurado"), cCampo).ordem).Replace("'", "''")

                If nome.Length > 60 Then
                    nome = nome.Substring(0, 60)
                End If
            Catch ex As Exception

            End Try

            Try
                cpf = ouvazio(m.ItemArray, CType(campos("CPFTitular"), cCampo).ordem).Replace("'", "''").Replace("-", "").Replace(".", "")

                If cpf.Length > 11 Then
                    cpf = cpf.Substring(0, 11)
                End If
            Catch ex As Exception

            End Try

            Try
                dt_nascimento = ouvazio(m.ItemArray, CType(campos("DataNascimento"), cCampo).ordem).Replace("'", "''")


                If dt_nascimento.Length > 10 Then
                    dt_nascimento = dt_nascimento.Substring(0, 10)
                End If
            Catch ex As Exception

            End Try

            Try
                sexo = ouvazio(m.ItemArray, CType(campos("Sexo"), cCampo).ordem).Replace("'", "''")

                ''If sexo.Length > 2 Then
                ''    sexo = sexo.Substring(0, 2)
                ''End If
            Catch ex As Exception

            End Try

            Try
                val_salario = ouvazio(m.ItemArray, CType(campos("ValorSalario"), cCampo).ordem).Replace("'", "''")

                If val_salario.Length > 16 Then
                    val_salario = val_salario.Substring(0, 16)
                End If
            Catch ex As Exception

            End Try

            Try
                val_capital_segurado = ouvazio(m.ItemArray, CType(campos("ValorCapital"), cCampo).ordem).Replace("'", "''")

                If val_capital_segurado.Length > 16 Then
                    val_capital_segurado = val_capital_segurado.Substring(0, 16)
                End If
            Catch ex As Exception

            End Try

            Try
                cpf_conjuge = ouvazio(m.ItemArray, CType(campos("CPFConjuge"), cCampo).ordem).Replace("'", "''")

                If cpf_conjuge.Length > 14 Then
                    cpf_conjuge = cpf_conjuge.Substring(0, 14)
                End If
            Catch ex As Exception

            End Try

            Try
                dt_fim_vigencia = ouvazio(m.ItemArray, CType(campos("DataDesligamento"), cCampo).ordem).Replace("'", "''")

                If dt_fim_vigencia.Length > 10 Then
                    dt_fim_vigencia = dt_fim_vigencia.Substring(0, 10)
                End If
            Catch ex As Exception

            End Try

            Dim row As DataRow = ds.Tables(0).NewRow
            row.Item(0) = 6785                      'Seguradora_cod_susep
            row.Item(1) = 0                         'sucursal_seguradora_id
            row.Item(2) = Session("nomeSubgrupo")   'nome_estip
            row.Item(3) = Session("ramo")           'ramo_id
            row.Item(4) = Session("apolice")        'apolice_id
            row.Item(5) = Session("subgrupo_id")    'sub_grupo_id
            row.Item(6) = Session("usuario")        'Usu�rio
            row.Item(7) = Now                       'dt_inclusao


            Dim t As Integer = 8

            For Each campo As cCampo In campos.Values
                'cUtilitarios.escreve(campo.nome & "<br>")
                Try
                    row.Item(t) = ouvazio(m.ItemArray, campo).Replace("'", "''")
                Catch ex As Exception
                    row.Item(t) = ""
                End Try
                t = t + 1
            Next

            ds.Tables(0).Rows.Add(row)


            'Monta a linha no sbuffer
            sbuffer.Append(dt_inicio_vigencia & ";" & _
                     dt_fim_vigencia & ";" & _
                     operacao & ";" & _
                     nome & ";" & _
                     cpf & ";" & _
                     dt_nascimento & ";" & _
                     sexo & ";" & _
                     val_salario & ";" & _
                     val_capital_segurado & ";" & _
                     cpf_conjuge & ";" & vbCrLf)



        Catch ex As Exception
            'Dim excp As New clsException(ex)
            Response.Write("<br><pre>")
            Response.Write(ex.Message)
            Response.Write(ex.StackTrace)
            Response.Write("</pre>")
            Return False
        End Try

        Return True

    End Function

    Private Function ouvazio(ByVal matriz As Array, ByVal idx As Integer) As String

        Dim retorno As String = ""
        Try
            retorno = matriz(idx)
        Catch ex As Exception
            retorno = ""
        End Try

        Return retorno

    End Function

    Private Function ouvazio(ByVal matriz As Array, ByVal campo As cCampo) As String

        Dim retorno As String = ""
        Try
            Select Case campo.nome
                Case "DataDesligamento", "DataNascimento", "DataRisco"
                    retorno = CType(matriz(campo.ordem), Date).ToShortDateString
                Case "Sexo", "sexo"
                    retorno = Me.dominio_sexo(matriz(campo.ordem).ToString.Trim)
                    If retorno Is Nothing Then
                        retorno = matriz(campo.ordem).ToString.Trim
                    End If
                Case "Opera��o"
                    retorno = Me.dominio_operacao(matriz(campo.ordem).ToString.Trim)
                Case Else
                    retorno = matriz(campo.ordem)
            End Select
        Catch ex As Exception
            retorno = ""
        End Try

        'If retorno Is Nothing Then
        '    Return ""
        'End If

        If retorno Is Nothing Then
            retorno = matriz(campo.ordem)
        End If

        Return retorno

    End Function

    Private Function insereLinhaArquivo(ByVal m As Array, ByVal ds As DataSet) As Boolean

        Try

            Dim dt_inicio_vigencia As String = ""
            Dim operacao As String = ""
            Dim nome As String = ""
            Dim cpf As String = ""
            Dim dt_nascimento As String = ""
            Dim sexo As String = ""
            Dim val_salario As String = ""
            Dim val_capital_segurado As String = ""
            Dim cpf_conjuge As String = ""
            Dim dt_fim_vigencia As String = ""

            Try
                If chkDescCabecalho.Checked Then
                    If Session("Linha_1") = "" Then
                        Session("Linha_1") = "1"
                        Return True
                    End If
                End If
            Catch ex As Exception

            End Try

            Try
                dt_inicio_vigencia = ouvazio(m, CType(campos("DataRisco"), cCampo).ordem).Replace("'", "''")

                If dt_inicio_vigencia.Trim.Length > 10 Then
                    dt_inicio_vigencia = dt_inicio_vigencia.Trim.Substring(0, 10)
                End If

            Catch ex As Exception

            End Try

            Try
                operacao = ouvazio(m, CType(campos("Opera��o"), cCampo).ordem).Replace("'", "''")
                ''If operacao.Trim.Length > 2 Then
                ''    operacao = operacao.Trim.Substring(0, 2)
                ''End If
            Catch ex As Exception

            End Try

            Try
                nome = ouvazio(m, CType(campos("NomeSegurado"), cCampo).ordem).Replace("'", "''")

                If nome.Trim.Length > 60 Then
                    nome = nome.Trim.Substring(0, 60)
                End If

            Catch ex As Exception

            End Try


            Try
                'cpf = ouvazio(m, CType(campos("CPFTitular"), cCampo).ordem).Replace("'", "''")
                cpf = ouvazio(m, CType(campos("cpf_titular"), cCampo).ordem).Replace("'", "''").Replace("-", "").Replace(".", "")

                If cpf.Trim.Length > 11 Then
                    cpf = cpf.Trim.Substring(0, 11)
                End If
            Catch ex As Exception

            End Try

            Try
                dt_nascimento = ouvazio(m, CType(campos("DataNascimento"), cCampo).ordem).Replace("'", "''")

                If dt_nascimento.Trim.Length > 10 Then
                    dt_nascimento = dt_nascimento.Trim.Substring(0, 10)
                End If
            Catch ex As Exception

            End Try

            Try
                sexo = ouvazio(m, CType(campos("Sexo"), cCampo).ordem).Replace("'", "''")
                ''If sexo.Trim.Length > 2 Then
                ''    sexo = sexo.Trim.Substring(0, 2)
                ''End If
            Catch ex As Exception

            End Try

            Try
                val_salario = ouvazio(m, CType(campos("ValorSalario"), cCampo).ordem).Replace("'", "''")

                If val_salario.Trim.Length > 16 Then
                    val_salario = val_salario.Trim.Substring(0, 16)
                End If
            Catch ex As Exception

            End Try

            Try
                val_capital_segurado = ouvazio(m, CType(campos("ValorCapital"), cCampo).ordem).Replace("'", "''")

                If val_capital_segurado.Trim.Length > 16 Then
                    val_capital_segurado = val_capital_segurado.Trim.Substring(0, 16)
                End If
            Catch ex As Exception

            End Try

            Try
                cpf_conjuge = ouvazio(m, CType(campos("CPFConjuge"), cCampo).ordem).Replace("'", "''")

                If cpf_conjuge.Trim.Length > 14 Then
                    cpf_conjuge = cpf_conjuge.Trim.Substring(0, 14)
                End If
            Catch ex As Exception

            End Try

            Try
                dt_fim_vigencia = ouvazio(m, CType(campos("DataDesligamento"), cCampo).ordem).Replace("'", "''")

                If dt_fim_vigencia.Trim.Length > 10 Then
                    dt_fim_vigencia = dt_fim_vigencia.Trim.Substring(0, 10)
                End If
            Catch ex As Exception

            End Try

            'Tratamento dos valores
            Dim row As DataRow = ds.Tables(0).NewRow

            row.Item(0) = 6785                      'Seguradora_cod_susep
            row.Item(1) = 0                         'sucursal_seguradora_id
            row.Item(2) = Session("nomeSubgrupo")   'nome_estip
            row.Item(3) = Session("ramo")           'ramo_id
            row.Item(4) = Session("apolice")        'apolice_id
            row.Item(5) = Session("subgrupo_id")    'sub_grupo_id
            row.Item(6) = Session("usuario")        'Usu�rio
            row.Item(7) = Now                       'dt_inclusao

            Dim t As Integer = 8

            For Each campo As cCampo In campos.Values
                Try
                    row.Item(t) = ouvazio(m, campo).Replace("'", "''")
                Catch ex As Exception
                    row.Item(t) = "---"
                End Try
                t = t + 1
            Next

            ds.Tables(0).Rows.Add(row)

            'Monta a linha no sbuffer
            sbuffer.Append(dt_inicio_vigencia & ";" & _
                    dt_fim_vigencia & ";" & _
                    operacao & ";" & _
                    nome & ";" & _
                    cpf & ";" & _
                    dt_nascimento & ";" & _
                    sexo & ";" & _
                    val_salario & ";" & _
                    val_capital_segurado & ";" & _
                    cpf_conjuge & ";" & vbCrLf)

        Catch ex As Exception
            Dim excp As New clsException(ex, "O arquivo enviado n�o corresponde ao formato configurado.")
            Response.Write("<!-- ")
            Response.Write(ex.Message)
            Response.Write(ex.StackTrace)
            Response.Write("--> ")
            Return False
        End Try

        Return True

    End Function

    Private Sub mPaginacaoClick(ByVal Argumento As String) Handles ucPaginacao.ePaginaAlterada
        Me.grdPesquisa.SelectedIndex = -1
        Select Case Argumento
            Case "pro"
                Me.grdPesquisa.CurrentPageIndex += 1
            Case "ant"
                Me.grdPesquisa.CurrentPageIndex -= 1
            Case Else
                Me.grdPesquisa.CurrentPageIndex = Argumento
        End Select
        Session("indice") = Me.grdPesquisa.CurrentPageIndex
        Me.ucPaginacao.GridDataBind(grdPesquisa, Session("grdDescarte"))
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender

    End Sub

    Private Function getPeriodoCompetenciaSession()

        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)

        Dim dr As Data.SqlClient.SqlDataReader
        Try

            bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

            dr = bd.ExecutaSQL_DR()
            Dim data_inicio, data_fim, retorno As String

            If dr.HasRows Then
                While dr.Read()

                    data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                    data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)

                End While

                retorno = "Per�odo de Compet�ncia: " & data_inicio & " a " & data_fim
            Else
                Try
                    If Session("sessionValidate") Is Nothing Then
                        If Request.QueryString.Keys.Count > 0 Then
                            For Each m As String In Request.QueryString.Keys
                                If m <> "" Then
                                    Session(m) = Request.QueryString(m)
                                End If
                            Next

                            Session("sessionValidate") = 1

                            If (Not Session("apolice") Is Nothing) And (Not Session("ramo") Is Nothing) And (Not Session("subgrupo_id") Is Nothing) Then
                                getPeriodoCompetenciaSession()
                                Response.End()
                            End If
                        End If
                    End If
                Catch ex As Exception

                End Try

            End If

            Return retorno

        Catch ex As Exception
            Dim excp As New clsException(ex)
            System.Web.HttpContext.Current.Response.Write("<script>top.escondeaguarde();</script>")

        Finally
            dr.Close()
            dr = Nothing
        End Try


    End Function

    Public Function addBoundColumn(ByVal datafield As String) As BoundColumn

        Dim coluna As New BoundColumn

        coluna.DataField = datafield
        coluna.ItemStyle.HorizontalAlign = HorizontalAlign.Center

        Return coluna
    End Function

    Private Function deParaHeader(ByVal header As String) As String
        Dim valor As String = ""

        Select Case header.Trim
            Case "CPFTitular"
                valor = "CPF do Titular"

            Case "NomeSegurado"
                valor = "Nome do Segurado"

            Case "DataRisco"
                valor = "Data In�cio de Vig�ncia"

            Case "Opera��o"
                valor = "Opera��o"

            Case "ValorSalario"
                valor = "Sal�rio do Segurado"

            Case "ValorCapital"
                valor = "Capital do Segurado"

            Case "Sexo"
                valor = "Sexo"

            Case "DataNascimento"
                valor = "Data de Nascimento"

            Case "TipoComponente"
                valor = "Tipo de Componente"

            Case "CPFConjuge"
                valor = "CPF do C�njuge"

            Case "DataDesligamento"
                valor = "Data de Desligamento"


        End Select

        Return valor
    End Function

    Private Sub btnConfirmar_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles btnConfirmar.Command

    End Sub
End Class
