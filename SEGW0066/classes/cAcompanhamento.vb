
Public Class cAcompanhamento

#Region "Heran�a"
    Inherits cBanco
#End Region

    Public newSQL As New System.Text.StringBuilder


    Public Sub SEGS5696_SPS(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal workflow As Integer, ByVal valor_id As Integer)
        SQL = "exec vida_web_db.dbo.segs5696_sps  @apolice_id = " & apolice_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @tp_wf_id = " & workflow
        SQL &= ", @tp_versao_id = " & valor_id

    End Sub


    Public Sub getDadosLayout(ByVal layout_arquivo_id As Integer, ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)

        SQL = " EXEC vida_web_db.dbo.SEGS5763_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id  = " & ramo_id
        SQL &= ", @subgrupo_id = " & subgrupo_id
        SQL &= ", @layout_arquivo_id = " & layout_arquivo_id


    End Sub

    Public Sub getDominiosLayout(ByVal layout_id As Integer, ByVal tipo As String)

        SQL = " EXEC vida_web_db.dbo.SEGS6767_SPS "
        SQL &= "@layout_id = " & layout_id
        SQL &= ", @tipo = '" & tipo & "'"

    End Sub

    Public Sub SEGS5763_SPS(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)

        SQL = "exec vida_web_db.dbo.SEGS5763_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id  = " & ramo_id
        SQL &= ", @subgrupo_id = " & subgrupo_id

    End Sub

    Public Sub SEGS5763_SPS(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal tipo_arquivo As String)

        'SQL = "exec vida_web_db.dbo.SEGS5763_SPS "
        'SQL &= " @apolice_id = " & apolice_id
        'SQL &= ", @ramo_id  = " & ramo_id
        'SQL &= ", @subgrupo_id = " & subgrupo_id
        'SQL &= ", @tipo = '" & tipo_arquivo & "'"
        newSQL.Remove(0, newSQL.Length)
        newSQL.Append("exec vida_web_db.dbo.SEGS5763_SPS ")
        newSQL.Append(" @apolice_id = " & apolice_id.ToString())
        newSQL.Append(", @ramo_id  = " & ramo_id.ToString())
        newSQL.Append(", @subgrupo_id = " & subgrupo_id.ToString())
        newSQL.Append(", @tipo = '" & tipo_arquivo & "'")

        SQL = newSQL.ToString()

    End Sub
    Public Sub SEGS5849_SPS(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal vazio As Integer)

        'SQL = "exec SEGS5849_SPS "
        'SQL &= " @apolice_id = " & apolice_id
        'SQL &= ", @ramo_id  = " & ramo_id
        'SQL &= ", @subgrupo_id = " & subgrupo_id
        'SQL &= ", @vazio = " & vazio
        newSQL.Remove(0, newSQL.Length)
        newSQL.Append("exec SEGS5849_SPS ")
        newSQL.Append(" @apolice_id = " & apolice_id.ToString())
        newSQL.Append(", @ramo_id  = " & ramo_id.ToString())
        newSQL.Append(", @subgrupo_id = " & subgrupo_id.ToString())
        newSQL.Append(", @vazio = " & vazio.ToString())

        SQL = newSQL.ToString()


    End Sub

    Public Sub SEGS8428_SPS(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)

        newSQL.Remove(0, newSQL.Length)
        newSQL.Append("exec vida_web_db..SEGS8428_SPS ")
        newSQL.Append(" @apolice_id = " & apolice_id.ToString())
        newSQL.Append(", @ramo_id  = " & ramo_id.ToString())
        newSQL.Append(", @subgrupo_id = " & subgrupo_id.ToString())
        SQL = newSQL.ToString()

        'SQL = " Select layout_arquivo_vida_campos_tb.ordem " & _
        '      " from VIDA_WEB_DB..layout_arquivo_vida_campos_tb layout_arquivo_vida_campos_tb " & _
        '      " join VIDA_WEB_DB..layout_arquivo_vida_tb layout_arquivo_vida_tb " & _
        '      " on layout_arquivo_vida_campos_tb.layout_id = layout_arquivo_vida_tb.layout_id " & _
        '      " where(layout_arquivo_vida_tb.apolice_id = 13760) " & _
        '      " and layout_arquivo_vida_tb.ramo_id = 93 " & _
        '      " and layout_arquivo_vida_tb.subgrupo_id = 2 " & _
        '      " and layout_arquivo_vida_tb.situacao = 'A' " & _
        '      " and layout_arquivo_vida_tb.dt_fim_vigencia is null " & _
        '      " and layout_arquivo_vida_campos_tb.nome = 'Subgrupo'"



    End Sub

    'Consulta todos os subgrupos da ap�lice
    Public Sub consulta_subgrupo_sps(ByVal apolice_id As Long, ByVal ramo_id As Int16, ByVal subgrupo_id As String)


        'SQL = " select distinct sub_grupo_id, nome " & _
        '      " from [SISAB051\qualid].seguros_db.dbo.sub_grupo_apolice_tb   " & _
        '      " where ramo_id = " & trInt(ramo_id) & _
        '      " and apolice_id = " & trInt(apolice_id) & _
        '      " and sub_grupo_id = " & subgrupo_id & _
        '      " and dt_fim_vigencia_sbg Is null " & _
        '      " ORDER BY sub_grupo_id "

        SQL = " select distinct sub_grupo_id, nome " & _
              " from [SISAB003].seguros_db.dbo.sub_grupo_apolice_tb   " & _
              " where ramo_id = " & trInt(ramo_id) & _
              " and apolice_id = " & trInt(apolice_id) & _
              " and sub_grupo_id = " & subgrupo_id & _
              " and dt_fim_vigencia_sbg Is null " & _
              " ORDER BY sub_grupo_id "
    End Sub



    Public Sub consulta_todos_subgrupo_sps(ByVal apolice_id As Long, ByVal ramo_id As Int16)


        'SQL = " select distinct sub_grupo_id, nome " & _
        '      " from [SISAB051\qualid].seguros_db.dbo.sub_grupo_apolice_tb   " & _
        '      " where ramo_id = " & trInt(ramo_id) & _
        '      " and apolice_id = " & trInt(apolice_id) & _
        '      " and sub_grupo_id = " & subgrupo_id & _
        '      " and dt_fim_vigencia_sbg Is null " & _
        '      " ORDER BY sub_grupo_id "

        SQL = " select distinct sub_grupo_id, nome " & _
              " from [SISAB003].seguros_db.dbo.sub_grupo_apolice_tb   " & _
              " where ramo_id = " & trInt(ramo_id) & _
              " and apolice_id = " & trInt(apolice_id) & _
              " and dt_fim_vigencia_sbg Is null " & _
              " ORDER BY sub_grupo_id "
    End Sub
    Public Sub getLayoutArquivoAtivos(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal ativos As Integer)

        'SQL = "exec desenv_db..SEGS5763_SPS "
        'SQL &= " @apolice_id = " & apolice_id
        'SQL &= ", @ramo_id  = " & ramo_id
        'SQL &= ", @subgrupo_id = " & subgrupo_id
        'SQL &= ", @ativos = " & ativos
        newSQL.Remove(0, newSQL.Length)
        newSQL.Append("exec vida_web_db.dbo.SEGS5763_SPS ")
        newSQL.Append(" @apolice_id = " & apolice_id.ToString())
        newSQL.Append(", @ramo_id  = " & ramo_id.ToString())
        newSQL.Append(", @subgrupo_id = " & subgrupo_id.ToString())
        newSQL.Append(", @ativos = " & ativos.ToString())

        SQL = newSQL.ToString()

    End Sub


    Public Sub New(Optional ByVal banco As cDadosBasicos.eBanco = cDadosBasicos.eBanco.web_seguros_db)
        MyBase.New(banco)
        Alianca.Seguranca.BancoDados.cCon.BancodeDados = banco.ToString

    End Sub

    Function trInt(ByVal valor As Object) As String

        If valor = 0 Then
            Return "null"
        Else
            Return valor
        End If

    End Function

    Function trStr(ByVal valor As Object) As String

        If valor = "" Then
            Return "null"
        Else
            Return "'" & valor.ToString.Replace("'", "''") & "'"
        End If

    End Function

    Public Sub SEGS5765_SPI(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal layout_arquivo_id As Integer, ByVal cod_susep As Integer, ByVal sucursal_id As Integer, ByVal separador As String, ByVal tipo_arquivo As String, ByVal sobreposicao As String, ByVal nome_layout As String, ByVal str_nome_campo As String, ByVal dominio_titular As String, ByVal dominio_conjuge As String, ByVal dominio_masc As String, ByVal dominio_fem As String, ByVal dom_inclusao As String, ByVal dom_alteracao As String, ByVal dom_exclusao As String, ByVal usuario As String, ByVal nome_arquivo As String)

        SQL = " EXEC SEGS5765_SPI "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id  = " & ramo_id
        SQL &= ", @subgrupo_id = " & subgrupo_id
        SQL &= ", @layout_arquivo_id = " & layout_arquivo_id
        SQL &= ", @separador = " & separador
        SQL &= ", @tipo_arquivo = " & tipo_arquivo
        SQL &= ", @nome_layout = " & nome_layout
        SQL &= ", @str_nome_campo = " & str_nome_campo
        SQL &= ", @dominio_titular = " & dominio_titular
        SQL &= ", @dominio_conjuge = " & dominio_conjuge
        SQL &= ", @dominio_masc = " & dominio_masc
        SQL &= ", @dominio_fem = " & dominio_fem
        SQL &= ", @dom_inclusao = " & dom_inclusao
        SQL &= ", @dom_alteracao = " & dom_alteracao
        SQL &= ", @dom_exclusao = " & dom_exclusao
        SQL &= ", @usuario = " & usuario
        SQL &= ", @nm_arquivo = " & nome_arquivo


    End Sub

    Public Sub SEGS5787_SPI(ByVal iControlaProcessoId As Integer, _
                            ByVal iRamoId As Integer, _
                            ByVal lApoliceId As Long, _
                            ByVal iSubGrupoId As Integer, _
                            ByVal sCaminhoArquivo As String, _
                            ByVal sNomeArquivo As String, _
                            ByVal sUsuario As String, _
                            ByVal sDescartaCabecalho As String)

        SQL = "EXEC vida_web_db.dbo.SEGS5787_SPI "
        SQL &= iControlaProcessoId
        SQL &= ", " & iRamoId
        SQL &= ", " & lApoliceId
        SQL &= ", " & iSubGrupoId
        SQL &= ", '" & sCaminhoArquivo & "'"
        SQL &= ", '" & sNomeArquivo & "'"
        SQL &= ", '" & sDescartaCabecalho & "'"
        SQL &= ", '" & sUsuario

    End Sub

    Public Sub insereProcessoUpload(ByVal iRamoId As Integer, _
                                    ByVal lApoliceId As Long, _
                                    ByVal iSubGrupoId As Integer, _
                                    ByVal sPathArquivo As String, _
                                    ByVal sNomeArquivo As String, _
                                    ByVal sStatus As String, _
                                    ByVal sUsuarioImportacao As String, _
                                    ByVal sDtImportacao As String, _
                                    ByVal sUsuario As String, _
                                    ByVal sNomeEstipulante As String, _
                                    ByVal iQtRegistros As Integer)

        SQL = "EXEC vida_web_db.dbo.SEGS5790_SPI "
        SQL &= iRamoId
        SQL &= ", " & lApoliceId
        SQL &= ", " & iSubGrupoId
        SQL &= ", '" & sPathArquivo & "'"
        SQL &= ", '" & sNomeArquivo & "'"
        SQL &= ", '" & sStatus & "'"
        SQL &= ", '" & sUsuarioImportacao & "'"
        SQL &= ", '" & sDtImportacao & "'"
        SQL &= ", '" & sUsuario & "'"
        SQL &= ", '" & sNomeEstipulante & "'"
        SQL &= ", " & iQtRegistros

    End Sub

    Public Sub VerificarImportacaoAnterior(ByVal apolice_id As Long, _
                                           ByVal ramo_id As Integer, _
                                           ByVal subgrupo_id As Integer)

        SQL = "  SELECT 1" & vbNewLine
        SQL &= "   FROM vida_web_db.dbo.controla_processo_upload_tb" & vbNewLine
        SQL &= "  WHERE status = 'p'" & vbNewLine
        SQL &= "    AND apolice_id = " & apolice_id & vbNewLine
        SQL &= "    AND ramo_id = " & ramo_id & vbNewLine
        SQL &= "    AND sub_grupo_id = " & subgrupo_id

    End Sub

    Public Sub VerificarImportacaoAnteriorTodosSubgrupos(ByVal apolice_id As Long, _
                                           ByVal ramo_id As Integer)

        SQL = "  SELECT 1" & vbNewLine
        SQL &= "   FROM vida_web_db.dbo.controla_processo_upload_tb" & vbNewLine
        SQL &= "  WHERE status = 'p'" & vbNewLine
        SQL &= "    AND apolice_id = " & apolice_id & vbNewLine
        SQL &= "    AND ramo_id = " & ramo_id

    End Sub

    Public Sub ObterUltimoControleArquivo(ByVal apolice_id As Long, _
                                          ByVal ramo_id As Integer, _
                                          ByVal subgrupo As Integer)

        SQL = "  SELECT MAX(controla_processo_id)" & vbNewLine
        SQL &= "   FROM vida_web_db.dbo.controla_processo_upload_tb" & vbNewLine
        SQL &= "  WHERE status = 'p'" & vbNewLine
        SQL &= "    AND apolice_id = " & apolice_id & vbNewLine
        SQL &= "    AND ramo_id = " & ramo_id & vbNewLine
        SQL &= "    AND sub_grupo_id = " & subgrupo & vbNewLine

    End Sub


    Public Sub ApagarImportacaoAnterior(ByVal apolice_id As Long, _
                                        ByVal ramo_id As Integer, _
                                        ByVal subgrupo_id As Integer)

        SQL = " exec vida_web_db.dbo.SEGS7031_SPD  "
        SQL &= apolice_id
        SQL &= ", " & ramo_id
        SQL &= ", " & subgrupo_id


    End Sub

    Public Sub ApagarImportacaoAnteriorTodosSubgrupos(ByVal apolice_id As Long, _
                                        ByVal ramo_id As Integer)

        SQL = " exec vida_web_db.dbo.SEGS8156_SPD  "
        SQL &= apolice_id
        SQL &= ", " & ramo_id


    End Sub

End Class
