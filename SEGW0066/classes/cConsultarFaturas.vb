
Public Class cConsultarFaturas

#Region "Heran�a"
    Inherits cBanco
#End Region

    Public newSQL As New System.Text.StringBuilder

    Public Sub ConsultaFaturasPorPeriodoComp(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subGrupo_id As String, ByVal dt_ini As String, ByVal dt_fim As String)

        SQL = " EXEC dbo.SEGS5739_SPS "
        SQL &= "     @ramo_id = " & ramo_id
        SQL &= "   , @apolice_id = " & apolice_id
        SQL &= "   , @subgrupo_id = " & subGrupo_id
        SQL &= "   , @dt_ini = '" & dt_ini & "'"
        SQL &= "   , @dt_fim = '" & dt_fim & "'"

    End Sub

    Public Sub ConsultarSegurados(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subGrupo_id As String, ByVal fatura_id As Integer, Optional ByVal Operacao As String = "X")

        SQL = " EXEC dbo.SEGS5740_SPS "
        SQL &= "     @ramo_id = " & ramo_id
        SQL &= "   , @apolice_id = " & apolice_id
        SQL &= "   , @subgrupo_id = " & subGrupo_id
        SQL &= "   , @fatura_id = " & fatura_id
        If Operacao <> "X" Then
            SQL &= "   , @operacao = '" & Operacao & "'"
        End If

    End Sub

    Public Sub New(Optional ByVal banco As cDadosBasicos.eBanco = cDadosBasicos.eBanco.web_seguros_db)
        MyBase.New(banco)
        Alianca.Seguranca.BancoDados.cCon.BancodeDados = banco.ToString
    End Sub

    Function trInt(ByVal valor As Object) As String

        If valor = 0 Then
            Return "null"
        Else
            Return valor
        End If

    End Function

    Public Sub ImprimeBoleto(ByVal apolice_id As String, ByVal ramo_id As String)

        SQL = "EXEC [sisab003].seguros_db.dbo.SEGS5746_SPS @apolice_id = " & apolice_id
        SQL &= "                           , @ramo_id = " & ramo_id

    End Sub

    Public Sub SEGS5696_SPS(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal workflow As Integer, ByVal valor_id As Integer)
        'SQL = "exec web_seguros_db.dbo.segs5696_sps  @apolice_id = " & apolice_id
        'SQL &= ", @subgrupo_id = " & subgrupo
        'SQL &= ", @ramo_id = " & ramo_id
        'SQL &= ", @tp_wf_id = " & workflow
        'SQL &= ", @tp_versao_id = " & valor_id
        newSQL.Remove(0, newSQL.Length)
        newSQL.Append("exec web_seguros_db.dbo.segs5696_sps  @apolice_id = " & apolice_id.ToString())
        newSQL.Append(", @subgrupo_id = " & subgrupo.ToString())
        newSQL.Append(", @ramo_id = " & ramo_id.ToString())
        newSQL.Append(", @tp_wf_id = " & workflow.ToString())
        newSQL.Append(", @tp_versao_id = " & valor_id.ToString())

        SQL = newSQL.ToString()
    End Sub

    Public Sub ImprimeFatura(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal fatura_id As Integer)

        SQL = "[sisab003].seguros_db.dbo.rel_fatura_sub_grupo_sps " & apolice_id & ", 0, 6785, " & ramo_id & ", " & fatura_id

    End Sub

    Public Sub ImprimeRelacaoVidas(ByVal apolice_id As Long, ByVal ramo_id As Integer, ByVal fatura_id As Integer, ByVal Operacao As String)

        SQL = "[sisab003].seguros_db.dbo.SEGS5747_SPS " & apolice_id & ", 0, 6785, " & ramo_id & ", " & fatura_id & ", '" & Operacao & "' "

    End Sub

    Public Sub ListaEnderecoAmbiente(ByVal Url As String)

        SQL = "SELECT valor FROM [sisab003].controle_sistema_db.dbo.parametro_tb where sigla_sistema = 'WEB' and secao = 'POST2' AND CAMPO = '" & Url & "'"

    End Sub

End Class
