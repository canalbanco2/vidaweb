Imports System.Drawing
Imports System.Web.UI.WebControls
Imports System.Data

Partial Class paginacaogrid
    Inherits System.Web.UI.UserControl


    Public _valor As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub

    Public Event ePaginaAlterada(ByVal Argumento As String)
    Public Event ePaginaAlteradaA(ByVal Argumento As String)

    Private Sub mDisparaEvento(ByVal Argumento As String)
        RaiseEvent ePaginaAlterada(Argumento)
    End Sub
    Private Sub mDisparaEventoA(ByVal Argumento As String)
        RaiseEvent ePaginaAlteradaA(Argumento)
    End Sub

    Private Sub mPaginacaoClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles a1.Command, a2.Command, a3.Command, a4.Command, a5.Command, a6.Command, a7.Command
        mDisparaEvento(e.CommandArgument)
    End Sub

    Private Sub aPaginacaoClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles a1.Command, a2.Command, a3.Command, a4.Command, a5.Command, a6.Command, a7.Command
        mDisparaEventoA(e.CommandArgument)
    End Sub

    Private Sub mMontaPaginacao(ByVal GridPaginaAtual As Integer, ByVal GridPaginasTotal As Integer)
        Dim paginainicial As Integer
        Dim paginafinal As Integer
        Dim paginaatual As Integer = GridPaginaAtual + 1

        If (paginaatual / 5) > 0 Then
            If (paginaatual Mod 5) <> 0 Then
                paginainicial = (Int(paginaatual / 5) * 5) + 1
            Else
                paginainicial = (Int((paginaatual - 1) / 5) * 5) + 1
            End If
            paginafinal = paginainicial + 4
            If paginafinal > GridPaginasTotal Then paginafinal = GridPaginasTotal
        Else
            paginainicial = 1
            paginafinal = 5
            If paginafinal > GridPaginasTotal Then paginafinal = GridPaginasTotal
        End If

        Me.a1.Visible = False
        Me.a2.Visible = False
        Me.a3.Visible = False
        Me.a4.Visible = False
        Me.a5.Visible = False
        Me.a6.Visible = False
        Me.a7.Visible = False
        If paginaatual > 1 Then
            a1.Text = "Anterior"
            a1.ForeColor = Color.White
            a1.Style.Add("font-family", "verdana")
            a1.Style.Add("font-size", "10px")
            a1.Style.Add("font-weight", "bold")
            a1.Style.Add("text-decoration", "underline")
            a1.Visible = True
        End If
        Dim a As Integer = 2
        For i As Integer = paginainicial To paginafinal
            CType(Me.FindControl("a" + a.ToString), LinkButton).CommandArgument = (i - 1).ToString
            CType(Me.FindControl("a" + a.ToString), LinkButton).Text = i.ToString
            CType(Me.FindControl("a" + a.ToString), LinkButton).Visible = True
            If i = paginaatual Then
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("color", "white")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("text-decoration", "none")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-family", "verdana")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-size", "10px")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-weight", "bold")

            Else
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("color", "white;")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("text-decoration", "underline")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-family", "verdana")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-size", "10px")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-weight", "bold")

            End If
            a += 1
        Next
        If paginaatual <> GridPaginasTotal Then
            a7.Text = "Pr�xima"
            a7.ForeColor = Color.White
            a7.Style.Add("font-family", "verdana")
            a7.Style.Add("font-size", "10px")
            a7.Style.Add("font-weight", "bold")
            a7.Style.Add("text-decoration", "underline")
            a7.Visible = True
        End If
    End Sub

    Public Sub GridDataBind(ByVal Grid As DataGrid, ByVal dt As DataTable)

        Dim ndt As DataTable = New DataTable
        With ndt.Columns
            .Add(New DataColumn("dt_inicio_vigencia"))
            .Add(New DataColumn("dt_fim_vigencia"))
            .Add(New DataColumn("operacao"))
            .Add(New DataColumn("nome"))
            .Add(New DataColumn("cpf"))
            .Add(New DataColumn("dt_nascimento"))
            .Add(New DataColumn("sexo"))
            .Add(New DataColumn("val_salario"))
            .Add(New DataColumn("val_capital_segurado"))
            .Add(New DataColumn("cpf_conjuge"))
        End With

        Dim n As Integer = dt.Rows.Count
        If n > 10 Then n = 10
        For i As Integer = 0 To n - 1
            Dim row As DataRow = ndt.NewRow
            row.Item(0) = dt.Rows(i).Item(8)
            row.Item(1) = dt.Rows(i).Item(9)
            row.Item(2) = dt.Rows(i).Item(10)
            row.Item(3) = dt.Rows(i).Item(11)
            row.Item(4) = dt.Rows(i).Item(12)
            row.Item(5) = dt.Rows(i).Item(13)
            row.Item(6) = dt.Rows(i).Item(14)
            row.Item(7) = dt.Rows(i).Item(15)
            row.Item(8) = dt.Rows(i).Item(16)
            row.Item(9) = dt.Rows(i).Item(17)
            ndt.Rows.Add(row)
        Next

        Grid.DataSource = ndt
        Grid.DataBind()

        Me.mMontaPaginacao(Grid.CurrentPageIndex, Grid.PageCount)

        'Dim tipo As String
        'Dim cell As Web.UI.WebControls.DataGridItem
        Dim pai As Web.UI.WebControls.DataGridItem
        Dim count As Int16 = 0
        Dim pai_id As String = ""
        Dim count2 As Integer = 0
        Dim w As Integer = 1
        Dim t As Integer = 1
        Dim primeiro As Boolean = True
        Dim tamanho(9) As Integer
        Dim textoCell As String = ""

        tamanho(0) = 0
        tamanho(1) = 0
        tamanho(2) = 0
        tamanho(3) = 0
        tamanho(4) = 0
        tamanho(5) = 0
        tamanho(6) = 0
        tamanho(7) = 0
        tamanho(8) = 0
        tamanho(9) = 0




        Dim txtMax(9) As String

        If Grid.Items.Count > 0 Then

            For Each x As Web.UI.WebControls.DataGridItem In Grid.Items

                For i As Integer = 0 To x.Cells.Count - 1
                    cUtilitarios.escreve(x.Cells.Item(i).Text & " - " & x.Cells.Item(i).Width.ToString & "<br>")
                    x.Cells.Item(i).ID = "cell_" & i
                    x.Cells.Item(i).Wrap = False
                    x.Cells.Item(i).Width = New Web.UI.WebControls.Unit(50)
                    textoCell = Server.HtmlEncode(x.Cells.Item(i).Text.ToString)

                    If tamanho(i) <= textoCell.Length Then
                        tamanho(i) = textoCell.Length
                        txtMax(i) = x.Cells.Item(i).Text
                    End If
                Next

                pai = x
                x.ID = count2
                pai_id = x.ID
                count2 += 1

                If primeiro Then
                    primeiro = False
                    For i As Integer = 0 To x.Cells.Count - 1
                        x.Cells.Item(i).Text = "<div class='headerFakeDiv'id='headerFake" & i & "'></div>" & x.Cells.Item(i).Text
                    Next
                End If


            Next

            Dim z As Integer = 0
            Dim texto As String = ""
            Dim tamMin(9) As Integer
            tamMin(0) = 13
            tamMin(1) = 14
            tamMin(2) = 10
            tamMin(3) = 20
            tamMin(4) = 10
            tamMin(5) = 10
            tamMin(6) = 12
            tamMin(7) = 12
            tamMin(8) = 12
            tamMin(9) = 15



            For Each m As String In txtMax
                Try
                    texto = txtMax(z).ToString.Trim.Replace(" ", "O").Replace("&atilde;", "O")
                    texto = texto.Replace("&atilde;", "a").Replace("&ccedil;", "c")
                    texto = texto.Replace("-", "O").PadRight(tamMin(z), "O")
                    texto = texto.Replace("i", "O").Replace("1", "O").Replace("1", "O")
                    texto = texto.Replace("y", "O").Replace("I", "O").Replace("l", "O")
                Catch ex As Exception

                End Try

                Response.Write("<input type='hidden' value='" & texto & "' id='txtMax" & z & "'>" & vbCrLf)
                z = z + 1
            Next

            'For m As Int16 = 0 To 9
            '    Try
            '        CType(Me.FindControl("col" & m), System.Web.UI.HtmlControls.HtmlGenericControl)()
            '    Catch ex As Exception

            '    End Try

            '    Response.Write("<input type='hidden' value='" & texto & "' id='txtMax" & z & "'>" & vbCrLf)
            '    z = z + 1
            'Next



        End If

    End Sub

    Public Sub GridDataBind(ByVal Grid As DataGrid, ByVal dt As DataTable, ByVal form As Web.UI.Page)

        Dim ndt As DataTable = New DataTable
        With ndt.Columns
            .Add(New DataColumn("dt_inicio_vigencia"))
            .Add(New DataColumn("dt_fim_vigencia"))
            .Add(New DataColumn("operacao"))
            .Add(New DataColumn("nome"))
            .Add(New DataColumn("cpf"))
            .Add(New DataColumn("dt_nascimento"))
            .Add(New DataColumn("sexo"))
            .Add(New DataColumn("val_salario"))
            .Add(New DataColumn("val_capital_segurado"))
            .Add(New DataColumn("cpf_conjuge"))
        End With

        Dim n As Integer = dt.Rows.Count
        If n > 10 Then n = 10
        For i As Integer = 0 To n - 1
            Dim row As DataRow = ndt.NewRow
            row.Item(0) = dt.Rows(i).Item(8)
            row.Item(1) = dt.Rows(i).Item(9)
            row.Item(2) = dt.Rows(i).Item(10)
            row.Item(3) = dt.Rows(i).Item(11)
            row.Item(4) = dt.Rows(i).Item(12)
            row.Item(5) = dt.Rows(i).Item(13)
            row.Item(6) = dt.Rows(i).Item(14)
            row.Item(7) = dt.Rows(i).Item(15)
            row.Item(8) = dt.Rows(i).Item(16)
            row.Item(9) = dt.Rows(i).Item(17)
            ndt.Rows.Add(row)
        Next




        Grid.DataSource = ndt
        Grid.DataBind()

        Me.mMontaPaginacao(Grid.CurrentPageIndex, Grid.PageCount)



        '' '' ''Dim tipo As String
        '' '' ''Dim cell As Web.UI.WebControls.DataGridItem
        ' '' ''Dim pai As Web.UI.WebControls.DataGridItem
        ' '' ''Dim count As Int16 = 0
        ' '' ''Dim pai_id As String = ""
        ' '' ''Dim count2 As Integer = 0
        ' '' ''Dim w As Integer = 1
        ' '' ''Dim t As Integer = 1
        ' '' ''Dim primeiro As Boolean = True
        ' '' ''Dim tamanho(9) As Integer
        ' '' ''Dim textoCell As String = ""

        ' '' ''tamanho(0) = 0
        ' '' ''tamanho(1) = 0
        ' '' ''tamanho(2) = 0
        ' '' ''tamanho(3) = 0
        ' '' ''tamanho(4) = 0
        ' '' ''tamanho(5) = 0
        ' '' ''tamanho(6) = 0
        ' '' ''tamanho(7) = 0
        ' '' ''tamanho(8) = 0
        ' '' ''tamanho(9) = 0




        ' '' ''Dim txtMax(9) As String

        ' '' ''If Grid.Items.Count > 0 Then

        ' '' ''    For Each x As Web.UI.WebControls.DataGridItem In Grid.Items

        ' '' ''        For i As Integer = 0 To x.Cells.Count - 1
        ' '' ''            'cUtilitarios.escreve(x.Cells.Item(i).Text & " - " & x.Cells.Item(i).Width.ToString & "<br>")
        ' '' ''            x.Cells.Item(i).ID = "cell_" & i
        ' '' ''            x.Cells.Item(i).Wrap = False
        ' '' ''            'CType(form.FindControl("col" & i), System.Web.UI.HtmlControls.HtmlGenericControl).InnerHtml.Length * 13
        ' '' ''            'x.Cells.Item(i).Width = New Web.UI.WebControls.Unit(CType(form.FindControl("col" & i), System.Web.UI.HtmlControls.HtmlGenericControl).InnerHtml.Length * 13)

        ' '' ''            textoCell = Server.HtmlEncode(x.Cells.Item(i).Text.ToString)

        ' '' ''            If tamanho(i) <= textoCell.Length Then
        ' '' ''                tamanho(i) = textoCell.Length
        ' '' ''                txtMax(i) = x.Cells.Item(i).Text
        ' '' ''            End If
        ' '' ''        Next

        ' '' ''        pai = x
        ' '' ''        x.ID = count2
        ' '' ''        pai_id = x.ID
        ' '' ''        count2 += 1

        ' '' ''        If primeiro Then
        ' '' ''            primeiro = False
        ' '' ''            For i As Integer = 0 To x.Cells.Count - 1
        ' '' ''                x.Cells.Item(i).Text = "<div class='headerFakeDiv'id='headerFake" & i & "'></div>" & x.Cells.Item(i).Text
        ' '' ''            Next
        ' '' ''        End If


        ' '' ''    Next

        ' '' ''    Dim z As Integer = 0
        ' '' ''    Dim texto As String = ""
        ' '' ''    Dim tamMin(9) As Integer
        ' '' ''    tamMin(0) = 13
        ' '' ''    tamMin(1) = 14
        ' '' ''    tamMin(2) = 10
        ' '' ''    tamMin(3) = 20
        ' '' ''    tamMin(4) = 10
        ' '' ''    tamMin(5) = 10
        ' '' ''    tamMin(6) = 12
        ' '' ''    tamMin(7) = 12
        ' '' ''    tamMin(8) = 12
        ' '' ''    tamMin(9) = 15



        ' '' ''    For Each m As String In txtMax
        ' '' ''        Try
        ' '' ''            texto = txtMax(z).ToString.Trim.Replace(" ", "O").Replace("&atilde;", "O")
        ' '' ''            texto = texto.Replace("&atilde;", "a").Replace("&ccedil;", "c")
        ' '' ''            texto = texto.Replace("-", "O").PadRight(tamMin(z), "O")
        ' '' ''            texto = texto.Replace("i", "O").Replace("1", "O").Replace("1", "O")
        ' '' ''            texto = texto.Replace("y", "O").Replace("I", "O").Replace("l", "O")
        ' '' ''        Catch ex As Exception

        ' '' ''        End Try

        ' '' ''        Response.Write("<input type='hidden' value='" & texto & "' id='txtMax" & z & "'>" & vbCrLf)
        ' '' ''        z = z + 1
        ' '' ''    Next

        'For m As Int16 = 0 To 9
        '    Try
        '        CType(Me.FindControl("col" & m), System.Web.UI.HtmlControls.HtmlGenericControl)()
        '    Catch ex As Exception

        '    End Try

        '    Response.Write("<input type='hidden' value='" & texto & "' id='txtMax" & z & "'>" & vbCrLf)
        '    z = z + 1
        'Next



        ' '' ''End If

    End Sub

    Public Property CssClass() As String
        Get
            Return Me.lblPaginacao.Attributes.Item("class")
        End Get
        Set(ByVal Value As String)
            Me.lblPaginacao.Attributes.Add("class", Value)
        End Set
    End Property
End Class
