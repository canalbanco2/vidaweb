<%@ Page Language="vb" AutoEventWireup="false" Codebehind="MovimentacaoMassa.aspx.vb" Inherits="segw0066.MovimentacaoMassa" trace = "false" %>
<%@ Register TagPrefix="uc1" TagName="paginacaogrid" Src="paginacaogrid.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>MovimentacaoMassa</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<LINK href="../segw0060/CSS/EstiloBase.css" type="text/css" rel="stylesheet">
			<script>
				function escondeDiv(div){
					var idDiv = document.getElementById(div);
					if (idDiv.style.display==''){
						idDiv.style.display='none';
					}else{
						idDiv.style.display='';
					}
				}
				
				function confirmaModelo(){
						
						var arquivo = document.getElementById('fileProcurar').value;
						arquivo = arquivo.toLowerCase();
						arquivo = arquivo.split('.');
						
						arquivo = arquivo[arquivo.length-1];						
						
						if ((arquivo != 'csv') && (arquivo != 'txt') && (arquivo != 'xls')&& (arquivo != 'xlsx')) 
						{
							alert('Para enviar um arquivo � necess�rio que a extens�o do arquivo seja .xls, xlsx, .csv ou .txt.');
							return false;
						}			
						try{top.exibeaguarde();}catch(err){}
			
						return true;
				}
				
				function bloqueiaLinks()
				{
					var link = top.document.getElementById('Link6');
					
					link.onClick = 'return false;'
				}
				
				function confirmaExclusao()
				{
					var confirmacao = confirm('Existem registros importados n�o processados. Deseja prosseguir e elimin�-los?')
					return confirmacao
				}
				
				function Somente_Numero()
				{
					if (!(event.keyCode >= 48 && event.keyCode <= 57))
					{
						return false;
					}
					return true;
				}
				
			</script>
			
			<STYLE>.headerFixDiv {
	FONT-WEIGHT: bold; WIDTH: 100%; COLOR: #ffffff; TOP: 2px; BACKGROUND-COLOR: #003399
}
.headerFake {
	POSITION: absolute; TOP: 2px
}
.headerFixDiv2 {
	VISIBILITY: hidden; LINE-HEIGHT: 0; TOP: -20px
}
.hFake {
	VISIBILITY: hidden; LINE-HEIGHT: 0; TOP: -2px
}
.headerFakeDiv {
	VISIBILITY: hidden; LINE-HEIGHT: 0
}
.headerGrid {
	FONT-WEIGHT: bold; WIDTH: 100%; COLOR: #ffffff; BACKGROUND-COLOR: #003399
}
</STYLE><STYLE>.headerFixDiv {
	FONT-WEIGHT: bold; WIDTH: 100%; COLOR: #ffffff; TOP: 2px; BACKGROUND-COLOR: #003399
}
.headerFake {
	POSITION: absolute; TOP: 2px
}
.headerFixDiv2 {
	VISIBILITY: hidden; LINE-HEIGHT: 0; TOP: -20px
}
.hFake {
	VISIBILITY: hidden; LINE-HEIGHT: 0; TOP: -2px
}
.headerFakeDiv {
	VISIBILITY: hidden; LINE-HEIGHT: 0
}
.headerGrid {
	FONT-WEIGHT: bold; WIDTH: 100%; COLOR: #ffffff; BACKGROUND-COLOR: #003399
}
</STYLE>
        <link href="SEGW0019_style.css" rel="STYLESHEET" type="text/css" />
        <link href="SEGW0019_style.css" rel="STYLESHEET" type="text/css" />
        <link href="SEGW0019_style.css" rel="STYLESHEET" type="text/css" />
	</HEAD>
	<body onload="setWidths();" MS_POSITIONING="FlowLayout">
		<FORM id="Form1" method="post" encType="multipart/form-data" runat="server">
			<asp:panel id="pnlMsgSemLayout" Visible="false" Runat="server"></asp:panel><asp:panel id="pnlSimples" Runat="server">
				<P>
					<asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela"></asp:label><BR>
					<BR>
					<asp:Label id="lblVigencia" runat="server" CssClass="Caminhotela"></asp:Label></P>
				<P>
					<asp:panel id="pnlQualidade" runat="server" visible="false">
						<asp:Panel id="pnlTipodeMovimentacao" runat="server" HorizontalAlign="Left">
							<P class="fundo-azul-claro"><BR>
								<asp:Panel id="escolhaTpMovimentacao" runat="server" Visible="False" HorizontalAlign="Left">
<asp:Label id="lblTipoMovimentacao" runat="server" CssClass="fonte-destaque-azul">
										Selecione um tipo de movimenta��o:
									</asp:Label>
<asp:RadioButton id="rdIncremental" runat="server" Font-Size="X-Small" GroupName="TipoMovimentacao"
										Text="Incremental"></asp:RadioButton>
<asp:RadioButton id="rdSobreposicao" runat="server" Font-Size="X-Small" GroupName="TipoMovimentacao"
										Text="Sobreposi��o"></asp:RadioButton>&nbsp;&nbsp; 
<IMG alt="" src="../segw0060/Images/help.gif"> </asp:Panel>
								<asp:Panel HorizontalAlign="Left" id="pnlProcurar" runat="server" Visible="False" CssClass="fundo-azul-claro"><BR>
<asp:Label id="Label2" runat="server" CssClass="Caminhotela2">Clique em "Procurar" para selecionar o arquivo contendo a rela��o de vidas a ser enviada para a Alian�a do Brasil.</asp:Label><BR><BR><INPUT class="Botao2" id="fileProcurar" type="file" size="59" name="fileProcurar" runat="server" enableviewstate="true">&nbsp;&nbsp;<BR>
                                    <font color="red"><b>Para ap�lices com mais de um subgrupo, � poss�vel importar a rela��o de vidas com todo o grupo segurado na ap�lice, em uma �nica guia. <br>Para identifica��o, � preciso mencionar o subgrupo que o segurado pertence ou pertencer�.</b></font>
                                    <table style="width: 420px">
                                        <tr>
                                            <td style="height: 38px">
<asp:TextBox id="txtLinhasDesconsiderar" runat="server" MaxLength="2" Width="24px"></asp:TextBox>
                                            </td>
                                            <!--| johnny.barros(Confitec) Demanda:16035497 - 25/02/2013 [inicio] |-->
                                            <td style="height: 38px">
<asp:Label id="lblLnhADesconsiderar" Runat="server">Quantidade de linhas/cabe�alho a desconsiderar</asp:Label></td>
                                        </tr>
                                        <!--| johnny.barros(Confitec) Demanda:16035497 - 25/02/2013 [fim] |-->
                                        <tr>
                                            <td colspan="3" style="text-align: center">
										        <asp:Button id="btnImportar" runat="server" CssClass="Botao" Text="Enviar Arquivo" Width="104px"></asp:Button>
										    </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="text-align: center">
										<asp:Button id="btnFechaPagina" runat="server" Visible="False" CssClass="Botao" Text="Fechar"
											Width="56px"></asp:Button></td>
                                        </tr>
                                    </table>
<DIV>
    &nbsp;<br />
    <br />
    <br />
    <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="Red" Text=" "
        Width="344px" Visible="False"></asp:Label><br />
    <asp:Label ID="Label3" runat="server" ForeColor="Red" Text=" " Visible="False" Width="344px"></asp:Label><br />
    &nbsp;<asp:Panel ID="pnlSubgrupo" runat="server" Height="50px" Visible="False" Width="125px">
    
    <table style="width: 200px; text-align: left">
        <tr>
            <td colspan="1" style="width: 167px; height: 2px" class="fonte-destaque-azul" valign="top" >
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td colspan="2" style="height: 2px; width: 200px;">
                <asp:RadioButtonList ID="rblSelecao" runat="server" Height="20px" RepeatDirection="Horizontal" Width="295px">
                    <asp:ListItem Selected="True" Value="Atual">Somente o subgrupo selecionado</asp:ListItem>
                    <asp:ListItem Value="Todos">Todos os subgrupos</asp:ListItem>
                </asp:RadioButtonList></td>
            <td style="width: 17px; height: 2px" valign="top">
                <asp:Button ID="btnAcao" runat="server" Text="Confirmar" CssClass="Botao" /></td>
        </tr>
    </table>
    </asp:Panel>
    <br />
    &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
    &nbsp;&nbsp;<br />
    &nbsp;<br />
</DIV></asp:Panel>
							<P>&nbsp;</P>
						</asp:Panel>
						<asp:Panel id="pnlMovimentacao" runat="server" Visible="False" CssClass="fundo-azul-claro"
							HorizontalAlign="Center" Width="60%" Height="80px">
							<BR>
							<asp:Label id="lblListagemEnviada" runat="server" CssClass="Caminhotela" Width="508px"><br>Seu arquivo foi enviado para a Alian�a do Brasil.<br>&nbsp;<br>Voc� receber� um e-mail da Alian�a do Brasil, no qual ser�o informadas as vidas aceitas e as vidas recusadas.<br>&nbsp;</asp:Label>
						</asp:Panel>
						<asp:Panel id="pnlModelodoArquivo" runat="server" Visible="False" HorizontalAlign="Left">
							<asp:Label id="lblModelo" runat="server" CssClass="Caminhotela">
							Lista das vidas do arquivo selecionado
						</asp:Label>
							<BR>
							<DIV align="center">
								
								<TABLE id="gridMovimentacao" width="100%" border="0" runat="server">
									<TR>
										<TD>
											<DIV id="divGridMovimentacao">
												<DIV id="divScrollGrid" style="OVERFLOW: auto"><!-- In�cio da grid de Usu�rios -->
													<TABLE style="BORDER-COLLAPSE: collapse" height="230" cellSpacing="0" cellPadding="0" border="0"> <!--TR id="tr1" style="BACKGROUND-COLOR: #003399">
															<TD id="tdHeader0">
																<DIV class="headerFixDiv2" id="header0"></DIV>
																<DIV class="headerFixDiv" id="col0" runat="server">In�cio de Risco</DIV>
															</TD>
															<TD id="tdHeader1">
																<DIV class="headerFixDiv2" id="header1"></DIV>
																<DIV class="headerFixDiv" id="col1" runat="server">Fim de Vig�ncia</DIV>
															</TD>
															<TD id="tdHeader2">
																<DIV class="headerFixDiv2" id="header2"></DIV>
																<DIV class="headerFixDiv" id="col2" runat="server">Opera��o</DIV>
															</TD>
															<TD id="tdHeader3">
																<DIV class="headerFixDiv2" id="header3"></DIV>
																<DIV class="headerFixDiv" id="col3" runat="server">Nome</DIV>
															</TD>
															<TD id="tdHeader4">
																<DIV class="headerFixDiv2" id="header4"></DIV>
																<DIV class="headerFixDiv" id="col4" runat="server">CPF</DIV>
															</TD>
															<TD id="tdHeader5">
																<DIV class="headerFixDiv2" id="header5"></DIV>
																<DIV class="headerFixDiv" id="col5" runat="server">Data Nasc.</DIV>
															</TD>
															<TD id="tdHeader6">
																<DIV class="headerFixDiv2" id="header6"></DIV>
																<DIV class="headerFixDiv" id="col6" runat="server">Sexo</DIV>
															</TD>
															<TD id="tdHeader7">
																<DIV class="headerFixDiv2" id="header7"></DIV>
																<DIV class="headerFixDiv" id="col7" runat="server">Sal�rio (R$)</DIV>
															</TD>
															<TD id="tdHeader8">
																<DIV class="headerFixDiv2" id="header8"></DIV>
																<DIV class="headerFixDiv" id="col8" runat="server">Capital (R$)
																</DIV>
															</TD>
															<TD id="tdHeader9">
																<DIV class="headerFixDiv2" id="header9"></DIV>
																<DIV class="headerFixDiv" id="col9" runat="server">CPF C�njuge</DIV>
															</TD>
														</TR-->
														<TR>
															<TD><!--TD colSpan="10"-->
																<DIV id="divScrollGrid2" style="OVERFLOW: auto; HEIGHT: 230px"><!-- In�cio da grid de Usu�rios -->
																	<asp:datagrid id="grdPesquisa" runat="server" CssClass="escondeTituloGridTable" Width="90%" BorderColor="#DDDDDD"
																		AllowPaging="True" AutoGenerateColumns="False" CellPadding="3">
																		<FooterStyle CssClass="0019GridFooter"></FooterStyle>
																		<SelectedItemStyle CssClass="ponteiro"></SelectedItemStyle>
																		<AlternatingItemStyle CssClass="ponteiro"></AlternatingItemStyle>
																		<ItemStyle CssClass="ponteiro"></ItemStyle>
																		<HeaderStyle CssClass="headerGrid"></HeaderStyle>
																		<Columns>
																			<asp:BoundColumn DataField="dt_inicio_vigencia" HeaderText="In&#237;cio de Risco">
																				<ItemStyle HorizontalAlign="Center"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="dt_fim_vigencia" HeaderText="Fim de Vig&#234;ncia">
																				<ItemStyle HorizontalAlign="Center"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="operacao" HeaderText="Opera&#231;&#227;o">
																				<ItemStyle HorizontalAlign="Center"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="nome" HeaderText="Nome">
																				<ItemStyle HorizontalAlign="Center"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="cpf" HeaderText="CPF">
																				<ItemStyle HorizontalAlign="Center"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="dt_nascimento" HeaderText="Data Nasc.">
																				<ItemStyle HorizontalAlign="Center"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="sexo" HeaderText="Sexo">
																				<ItemStyle HorizontalAlign="Center"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="val_salario" HeaderText="Sal&#225;rio (R$)">
																				<ItemStyle HorizontalAlign="Center"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="val_capital_segurado" HeaderText="Capital (R$)">
																				<ItemStyle HorizontalAlign="Center"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="cpf_conjuge" HeaderText="CPF C&#244;njuge">
																				<ItemStyle HorizontalAlign="Center"></ItemStyle>
																			</asp:BoundColumn>
																		</Columns>
																		<PagerStyle Visible="False"></PagerStyle>
																	</asp:datagrid></DIV>
															</TD>
														</TR>
													</TABLE>
												</DIV>
											</DIV>
											<CENTER>
												<asp:Label id="lblQtdLinhas" runat="server"></asp:Label></CENTER>
										</TD>
									</TR>
									<BR>
								</TABLE>
								<CENTER>
									<asp:Label id="lblSemUsuario" Runat="server" Visible="False" CssClass="Caminhotela"></asp:Label></CENTER>
								<uc1:paginacaogrid id="ucPaginacao" runat="server"></uc1:paginacaogrid><BR>
								<BR>
								<DIV id="pnlConfirmar" runat="server">Ao clicar no bot�o "Confirmar", voc� estar� 
									confirmando que o layout do arquivo est� correto, bem como as informa��es das 
									vidas.<BR>
								</DIV>
								<BR>
								<BR>
								<asp:Button id="btnConfirmar" runat="server" CssClass="Botao" Text="Confirmar"></asp:Button></DIV>
						</asp:Panel>
						<BR>
						<asp:Panel id="pnlMensagemEnviada" runat="server" Visible="False" HorizontalAlign="Center"></asp:Panel>
					</asp:panel>
					<SCRIPT>
		for(i=0; i<=9; i++) {
			try {
				tam = 0;
				if(document.getElementById("grdPesquisa"))	
					tam = document.getElementById("grdPesquisa").clientHeight;		
					
				if(document.getElementById("grdPesquisa_0_cell_" + i)) {										
					add = "";					
					if(i == 9) {												
						if(tam >= 124)
							add = "OO";
					}
					
					document.getElementById("header" + i).innerHTML = document.getElementById("txtMax" + i).value + "OO" + add;
					document.getElementById("headerFake" + i).innerHTML = document.getElementById("txtMax" + i).value + "OO";								
					
					
					/*
					txtCol = "";					
					txtCol = txtCol + document.getElementById("col" + i).innerHTML;
					
					txtHead = "";					
					txtHead = txtHead + document.getElementById("headerFake" + i).innerHTML;
					
										
					if(txtHead.toString().length <= txtCol.toString().length) {
					    var txtColTemp = txtCol;
						txtCol = "";
						
						for(i=0;i<txtColTemp.toString().length;i++) {													
							txtCol = txtCol + "O";							
						}
						
						document.getElementById("header" + i).innerHTML = txtCol;
						document.getElementById("headerFake" + i).innerHTML = txtCol;						
					}*/
					
					if(document.getElementById("hFake" + i))
						document.getElementById("hFake" + i).innerHTML = document.getElementById("txtMax" + i).value + add + "OO";
				}
			} catch (e) { }
		}
		
		try{top.escondeaguarde();}catch(err){}
		
		function hideAndSeekHandler(op,w){
					dv = document.getElementById("divGridMovimentacao");
					//pag = document.getElementById("ucPaginacao_lblPaginacao");					
					
					if (op == 'esconder'){
						dv.style.width = w - 10;
						//pag.style.width = dv.clientWidth;
						//tp.style.width = "100%";
					}
					else{
						dv.style.width = "735px";
						//pag.style.width = "730px";						
					}
				}
				
				function setWidths(){
					
					try{top.iniciarMenu();}catch(err){}
				}
				
				try {
					for(i=0; i<=9; i++) {
						var tamanho = document.getElementById("col" + i).clientWidth;
						//alert(document.getElementById("col" + i).clientHeight);
						//alert(document.getElementById("col" + i).innerHTML);
						if (document.getElementById("grdPesquisa_1_cell_" + i)) {
							if (document.getElementById("col" + i).clientHeight >= 24) {
								var multiplicador;
								multiplicador = (document.getElementById("col" + i).clientHeight / 12);
								multiplicador = multiplicador - (0.01 * (multiplicador-1));
								//alert(multiplicador);
								document.getElementById("col" + i).style.width = tamanho * multiplicador;
								document.getElementById("col" + i).style.height = 12;
								
								document.getElementById("grdPesquisa_1_cell_" + i).style.width = tamanho * multiplicador;
							}
							else {
								if (i != 9) {
									document.getElementById("col" + i).style.width = tamanho;
									document.getElementById("grdPesquisa_1_cell_" + i).style.width = tamanho;
									document.getElementById("col" + i).style.height = 12;
								}
							}
							document.getElementById("tdHeader0").style.height = 12;
						}else{
							//document.getElementById("col" + i).style.display = 'none';
						}
					}
				} catch (e) { }
		
					</SCRIPT>
			</asp:panel>
			<INPUT id="hdf" style="Z-INDEX: 101; LEFT: 256px; POSITION: absolute; TOP: 40px" type="hidden"
				name="hdf" runat="server"><asp:literal id="Literal1" runat="server"></asp:literal>
		</FORM>
	</body>
</HTML>
