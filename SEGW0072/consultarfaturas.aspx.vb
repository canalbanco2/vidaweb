Public Class ConsultarFaturas
    Inherits ImplementarSABL0101LinkSeguro

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblVigencia As System.Web.UI.WebControls.Label
    Protected WithEvents lblNavegacao As System.Web.UI.WebControls.Label
    Protected WithEvents btnFatura As System.Web.UI.WebControls.Button
    Protected WithEvents btnRelacaoVidas As System.Web.UI.WebControls.Button
    Protected WithEvents btnResumo As System.Web.UI.WebControls.Button
    Protected WithEvents Label6 As System.Web.UI.WebControls.Label
    Protected WithEvents txtDe As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    Protected WithEvents txtAte As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnListar As System.Web.UI.WebControls.Button
    Protected WithEvents Image2 As System.Web.UI.WebControls.Image
    Protected WithEvents Label5 As System.Web.UI.WebControls.Label
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents pnlDetalhesFatura As System.Web.UI.WebControls.Panel
    Protected WithEvents Label8 As System.Web.UI.WebControls.Label
    Protected WithEvents Label9 As System.Web.UI.WebControls.Label
    Protected WithEvents ddlFiltros As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnLuis As System.Web.UI.WebControls.Label
    Protected WithEvents btnImprimir As System.Web.UI.WebControls.Button
    Protected WithEvents ImageButton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Label10 As System.Web.UI.WebControls.Label
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button
    Protected WithEvents Label11 As System.Web.UI.WebControls.Label
    Protected WithEvents Label12 As System.Web.UI.WebControls.Label
    Protected WithEvents Label13 As System.Web.UI.WebControls.Label
    Protected WithEvents DropDownList1 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents TextBox2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label14 As System.Web.UI.WebControls.Label
    Protected WithEvents Label17 As System.Web.UI.WebControls.Label
    Protected WithEvents lblFatura As System.Web.UI.WebControls.Label
    Protected WithEvents lblCompetencia As System.Web.UI.WebControls.Label
    Protected WithEvents grdSegurados As System.Web.UI.WebControls.DataGrid
    Protected WithEvents pnlFiltro As System.Web.UI.WebControls.Panel
    Protected WithEvents lblTituloFatura As System.Web.UI.WebControls.Label
    Protected WithEvents ddlFiltroSegurados As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnFiltroSegurados As System.Web.UI.WebControls.Button
    Protected WithEvents grdFaturas As System.Web.UI.WebControls.DataGrid
    Protected WithEvents chkImpFatura As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkImpBoletos As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkImpRelacao As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblFiltro As System.Web.UI.WebControls.Label
    Protected WithEvents lblPesquisar As System.Web.UI.WebControls.Label
    'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - in�cio
    Protected WithEvents pnlProrrogacao As System.Web.UI.WebControls.Panel
    Protected WithEvents grdProrrogacao As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnConfirmarProrrogacao As System.Web.UI.WebControls.Button
    'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - fim

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim dependentesfalta As Integer
    Dim dependentesinicio As Integer
    Dim TotalSegurados As Integer
    Dim dt_original As String
    Dim juros As String


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            dependentesfalta = 0
            dependentesinicio = 0

            EscreverEscondeLinhas()
            Alianca.Seguranca.BancoDados.cCon.ConnectionTimeout = Alianca.Seguranca.BancoDados.cCon.TimeTimeout.Infinito

            Session("dtFatura") = Nothing

            'lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & " -> Subgrupo: " & Session("nomeSubgrupo") & " -> Fun��o: " & getTpAcesso()
            lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("nomeSubgrupo") & "<br>Fun��o: Imprimir/Consultar Faturas Anteriores"
            lblVigencia.Text = getPeriodoCompetenciaSession()

            If Not Page.IsPostBack Then

                txtDe.Text = getSugestaoCompetencia("txtde")
                txtAte.Text = getSugestaoCompetencia("txtate")
            End If

            Dim visualizar As String = Request.QueryString("visualizar")

            Me.btnListar.Attributes.Add("onclick", "top.exibeaguarde();")
            Me.btnFiltroSegurados.Attributes.Add("onclick", "top.exibeaguarde();")

            'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - in�cio
            Dim prorrogar As String = Request.QueryString("prorrogar")

            If prorrogar <> "" Then
                Session("pagina") = Request.QueryString("page")
                Session("fatura_prorrogacao") = IIf(IsNothing(Session("fatura_prorrogacao")), Request.QueryString("fatura"), Session("fatura_prorrogacao"))
                Call grdFaturaDataBind(Session("pagina"))
                Call grdProrrogacaoDataBind()

                grdFaturas.Visible = True

                'Call ExibirPainelProrrogacao() '06/02/2019 - Demanda MU-2018-070495 - Cleiton Queiroz - Confitec SP

            ElseIf visualizar <> "" Then

                If Not Page.IsPostBack Then
                    If Not IsNothing(Request.Form("txtDe")) Then txtDe.Text = Request.Form("txtDe") Else txtDe.Text = Request.QueryString("txtDe")
                    If Not IsNothing(Request.Form("txtAte")) Then txtAte.Text = Request("txtAte") Else txtAte.Text = Request.QueryString("txtAte")
                End If

                Me.lblFiltro.Text = ddlFiltroSegurados.SelectedItem.Text

                lblFatura.Text = Request.QueryString("fatura")
                Session("fatura") = Request.QueryString("fatura")
                'lblCompetencia.Text = Request.QueryString("competencia")
                grdFaturaDataBind(Request.QueryString("page"))
                grdSeguradosDataBind()
                grdFaturas.Visible = True
                lblTituloFatura.Visible = True
                grdSegurados.Visible = True
                pnlDetalhesFatura.Visible = True

                'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - in�cio                
                Call EscondePainelProrrogacao()
                'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - fim

                'btnImprimir.Visible = False
                FormaPgto()
                EscreverImprimir()

            End If

            Call ExibirBotaoProrrogar(grdFaturas)
            'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - fim

        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message)
            'End If
        End Try

    End Sub

    Private Function getTpAcesso() As String
        Dim ind_acesso As String = ""

        Dim valor As String = ""
        valor = Session("acesso")

        Select Case valor
            Case "1"
                ind_acesso = "Administrador da Alian�a do Brasil"
            Case "2"
                ind_acesso = "Administrador de Ap�lice"
            Case "3"
                ind_acesso = "Administrador de Subgrupo"
            Case "4"
                ind_acesso = "Operador"
        End Select

        Return ind_acesso
    End Function

    Private Function VaidaData(ByVal sValor As String) As Boolean
        Dim mes As String
        Dim ano As String
        Dim today As DateTime = Now()

        VaidaData = False

        mes = Left(sValor, 2)
        ano = Right(sValor, 4)

        If mes <> "" And ano <> "" Then
            If mes >= 1 And mes <= 12 Then
                If ano > 1899 Then
                    VaidaData = True
                End If
            End If
        End If

    End Function

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click

        Try
            If VaidaData(txtDe.Text) And VaidaData(txtAte.Text) Then
                If Not IsDate(txtDe.Text) Or Not IsDate(txtAte.Text) Then
                    cUtilitarios.escreveScript("alert('Per�odo de Compet�ncia Inv�lido');")
                    Exit Sub
                End If

                grdFaturaDataBind()

                grdFaturas.Visible = True
                pnlDetalhesFatura.Visible = False

                'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - in�cio
                Call ExibirBotaoProrrogar(grdFaturas)
                Call EscondePainelProrrogacao()
                'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - fim
            Else
                cUtilitarios.escreveScript("alert('A data informada n�o � valida');")
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message)
            'End If
        End Try

    End Sub

    'bnery - Nova Consultoria - 17/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - in�cio
    Private Sub grdFaturas_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdFaturas.ItemCommand

        If e.CommandName = "prorrogar" Then

            'Inicio 27/12/2018 - Demanda MU-2018-070495 - Cleiton Queiroz - Confitec SP

            Dim fatura_id As Integer = CInt(Session("fatura"))
            Redirecionar(fatura_id)

            'Fim 27/12/2018 - Demanda MU-2018-070495 - Cleiton Queiroz - Confitec SP

        End If

    End Sub
    'bnery - Nova Consultoria - 17/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - fim

    Private Sub grdFaturas_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdFaturas.ItemDataBound

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
		
			'Inicio 06/02/2019 - Demanda MU-2018-070495 - Cleiton Queiroz - Confitec SP
            If Not e.Item.Cells(5).Text = "Pendente" Then

                Dim btn As LinkButton
                btn = e.Item.Cells(6).Controls(0)
                btn.Enabled = False
            End If
            'Fim 06/02/2019 - Demanda MU-2018-070495 - Cleiton Queiroz - Confitec SP

            If e.Item.ItemType = ListItemType.Item Then
                e.Item.Attributes.Add("onmouseout", "this.style.background='#ffffff'")
            Else
                e.Item.Attributes.Add("onmouseout", "this.style.background='#f5f5f5'")
            End If

            'bnery - Nova Consultoria - 17/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - in�cio
            For I As Integer = 0 To 5
                'e.Item.Attributes.Add("onclick", "location.href='?visualizar=1&amp;fatura=" & e.Item.Cells(0).Text & "&amp;competencia=" & e.Item.Cells(1).Text & "&amp;Page=" & grdFaturas.CurrentPageIndex & "&amp;" & txtDe.ID & "=" & txtDe.Text & "&amp;" & txtAte.ID & "=" & txtAte.Text & "&amp;SF=" & e.Item.Cells(5).Text & "&amp;SLinkSeguro=" & Request("SLinkSeguro") & "'")
                e.Item.Cells(I).Attributes.Add("onclick", "location.href='?visualizar=1&amp;fatura=" & e.Item.Cells(0).Text & "&amp;competencia=" & e.Item.Cells(1).Text & "&amp;Page=" & grdFaturas.CurrentPageIndex & "&amp;" & txtDe.ID & "=' + document.getElementById('txtDe').value + '&amp;" & txtAte.ID & "=' + document.getElementById('txtAte').value + '&amp;SF=" & e.Item.Cells(5).Text & "&amp;SLinkSeguro=" & Request("SLinkSeguro") & "'")
            Next I

            'e.Item.Attributes.Add("onclick", "location.href='?visualizar=1&amp;fatura=" & e.Item.Cells(0).Text & "&amp;competencia=" & e.Item.Cells(1).Text & "&amp;Page=" & grdFaturas.CurrentPageIndex & "&amp;" & txtDe.ID & "=' + document.getElementById('txtDe').value + '&amp;" & txtAte.ID & "=' + document.getElementById('txtAte').value + '&amp;SF=" & e.Item.Cells(5).Text & "&amp;SLinkSeguro=" & Request("SLinkSeguro") & "'")

            e.Item.Cells(6).Attributes.Add("onclick", "location.href='?prorrogar=1&amp;fatura=" & e.Item.Cells(0).Text & "&amp;competencia=" & e.Item.Cells(1).Text & "&amp;Page=" & grdFaturas.CurrentPageIndex & "&amp;" & txtDe.ID & "=' + document.getElementById('txtDe').value + '&amp;" & txtAte.ID & "=' + document.getElementById('txtAte').value + '&amp;SF=" & e.Item.Cells(5).Text & "&amp;SLinkSeguro=" & Request("SLinkSeguro") & "'")
            'bnery - Nova Consultoria - 17/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - fim

            e.Item.Attributes.Add("onmouseover", "this.style.background='#E8F1FF'")

            Response.Write("<script>top.exibeaguarde();</script>")

            e.Item.Attributes.Add("Style", "CURSOR: pointer")

        End If

    End Sub

    Private Sub grdFaturas_PageIndexChanged(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grdFaturas.PageIndexChanged

        Try
            grdFaturaDataBind(e.NewPageIndex)

            'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - in�cio
            Call ExibirBotaoProrrogar(grdFaturas)
            Call EscondePainelProrrogacao()

            grdSegurados.Visible = False
            pnlDetalhesFatura.Visible = False

            Session("pagina") = e.NewPageIndex
            'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - fim

        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message)
            'End If
        End Try

    End Sub

    Private Sub grdFaturaDataBind(Optional ByVal page As Integer = 0)
        Dim dtFaturas As DataTable

        Try
            dtFaturas = CarregarDataTableFaturas()

            Session("dtFatura") = dtFaturas
            grdFaturas.DataSource = dtFaturas
            grdFaturas.DataBind()
            pnlDetalhesFatura.Visible = False

            'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - in�cio
            Call EscondePainelProrrogacao()
            'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - fim

            If page < grdFaturas.PageCount And page > -1 Then
                grdFaturas.CurrentPageIndex = page
                grdFaturas.DataBind()
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message + bd.SQL)
            'End If
        End Try

    End Sub

    'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - in�cio
    Private Function CarregarDataTableFaturas() As DataTable
        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        Dim dt_de As Date
        Dim dt_ate As Date
        Dim ano As String
        Dim mes As String

        mes = txtDe.Text.Substring(0, txtDe.Text.IndexOf("/"))
        ano = txtDe.Text.Substring(txtDe.Text.IndexOf("/") + 1, 4)

        dt_de = Convert.ToDateTime(ano & "/" & mes & "/" & 1)

        mes = txtAte.Text.Substring(0, txtAte.Text.IndexOf("/"))
        ano = txtAte.Text.Substring(txtAte.Text.IndexOf("/") + 1, 4)

        dt_ate = Convert.ToDateTime(ano & "/" & mes & "/" & 1)
        dt_ate = DateAdd(DateInterval.Month, 1, dt_ate)
        dt_ate = DateAdd(DateInterval.Day, -1, dt_ate)

        bd.ConsultaFaturasPorPeriodoComp(Session("apolice"), Session("ramo"), Session("subgrupo_id"), dt_de.Year.ToString("0000") & dt_de.Month.ToString("00") & dt_de.Day.ToString("00"), dt_ate.Year.ToString("0000") & dt_ate.Month.ToString("00") & dt_ate.Day.ToString("00"))
        CarregarDataTableFaturas = bd.ExecutaSQL_DT()

        Return CarregarDataTableFaturas

    End Function
    'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - fim

    Private Sub grdSeguradosDataBind(Optional ByVal paginando As Boolean = False)

        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        Dim dt As New DataTable

        Dim i As Integer
        Dim j As Integer
        Dim paginaAnterior As Integer

        Try
            grdSegurados.PageSize = 10

            bd.ConsultarSegurados(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Convert.ToInt32(lblFatura.Text), ddlFiltroSegurados.SelectedItem.Value)
            dt = bd.ExecutaSQL_DT()
            grdSegurados.DataSource = dt
            TotalSegurados = dt.Rows.Count

            paginaAnterior = grdSegurados.CurrentPageIndex
            grdSegurados.CurrentPageIndex = 0
            grdSegurados.DataBind()
            If paginaAnterior < grdSegurados.PageCount And paginaAnterior > -1 Then
                grdSegurados.CurrentPageIndex = paginaAnterior
                grdSegurados.DataBind()
                If dependentesinicio > 0 Then
                    dt.Rows.Remove(dt.Rows(0))
                    dt.AcceptChanges()
                    grdSegurados.DataSource = dt
                    dependentesfalta = 0
                    dependentesinicio = 0
                    grdSegurados.DataBind()
                End If
                Do While dependentesfalta > 0
                    If paginaAnterior > 0 Then
                        For i = 1 To paginaAnterior * dependentesfalta
                            Dim dtr As DataRow = dt.NewRow
                            For j = 0 To dt.Rows(0).ItemArray.Length - 1
                                dtr(j) = dt.Rows(0).Item(j)
                            Next
                            dt.Rows.InsertAt(dtr, 0)
                        Next
                        dt.AcceptChanges()
                    End If
                    grdSegurados.PageSize = grdSegurados.PageSize + dependentesfalta
                    grdSegurados.DataSource = dt
                    dependentesfalta = 0
                    dependentesinicio = 0
                    grdSegurados.DataBind()
                Loop
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message + bd.SQL)
            'End If
        End Try

    End Sub

    Private Sub grdSegurados_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grdSegurados.PageIndexChanged

        Try
            grdSegurados.CurrentPageIndex = e.NewPageIndex
            grdSeguradosDataBind(True)
        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message)
            'End If
        End Try

    End Sub

    Private Sub grdSegurados_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdSegurados.ItemDataBound

        Dim i As Integer
        Dim AbreFecha As New HtmlImage
        Dim lbl As New Label

        AbreFecha.Src = "images\imgmenos.gif"

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            'IM00172008 - INICIO
            ' e.Item.Cells(6).Text = CLng(e.Item.Cells(6).Text).ToString("#,###,###,###,##0.00") '.Replace(",", "#").Replace(".", ",").Replace("#", ".")
            ' e.Item.Cells(7).Text = CLng(e.Item.Cells(7).Text).ToString("#,###,###,###,##0.00")

            e.Item.Cells(6).Text = Format(Convert.ToDouble(e.Item.Cells(6).Text), "#,###,###,##0.00")
            e.Item.Cells(7).Text = Format(Convert.ToDouble(e.Item.Cells(7).Text), "#,###,###,##0.00")
            'IM00172008 - FIM

            e.Item.ID = "Linha" & e.Item.ItemIndex

            If e.Item.ItemIndex = 0 And e.Item.Cells(2).Text.ToUpper <> "TITULAR" Then
                dependentesinicio += 1
            End If

            If e.Item.Cells(2).Text.ToUpper <> "TITULAR" Then
                e.Item.BackColor = Color.WhiteSmoke
                e.Item.Attributes.Add("onmouseout", "this.style.background='F5F5F5'")
                e.Item.Cells(1).Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & e.Item.Cells(1).Text
                dependentesfalta -= 1
            Else

                dependentesfalta = CInt(e.Item.Cells(8).Text)

                If e.Item.ItemIndex > 0 AndAlso grdSegurados.Items(e.Item.ItemIndex - 1).Cells(2).Text.ToUpper <> "TITULAR" Then

                    For i = e.Item.ItemIndex - 1 To 0 Step -1
                        If grdSegurados.Items(i).Cells(2).Text.ToUpper = "TITULAR" Then
                            AbreFecha.ID = "AbreFecha" & i.ToString
                            grdSegurados.Items(i).Attributes.Add("OnClick", "escondeLinhas(" & i + 1 & "," & e.Item.ItemIndex - 1 & ", grdSegurados_Linha" & i.ToString & "_AbreFecha" & i.ToString & ");")
                            lbl.Text = "&nbsp;&nbsp;" & grdSegurados.Items(i).Cells(1).Text
                            grdSegurados.Items(i).Cells(1).Text = String.Empty
                            grdSegurados.Items(i).Cells(1).Controls.Add(AbreFecha)
                            grdSegurados.Items(i).Cells(1).Controls.Add(lbl)
                            Exit For
                        End If
                    Next

                End If

                e.Item.BackColor = Color.White
                e.Item.Attributes.Add("onmouseout", "this.style.background='FFFFFF'")

            End If

            If e.Item.ItemIndex > 1 AndAlso (e.Item.ItemIndex = grdSegurados.PageSize - 1 Or e.Item.ItemIndex = (DirectCast(grdSegurados.DataSource, DataTable).Rows.Count Mod grdSegurados.PageSize) - 1) And e.Item.Cells(2).Text.ToUpper <> "TITULAR" Then

                For i = e.Item.ItemIndex - 1 To 0 Step -1
                    If grdSegurados.Items(i).Cells(2).Text.ToUpper = "TITULAR" Then
                        AbreFecha.ID = "AbreFecha" & i.ToString
                        grdSegurados.Items(i).Attributes.Add("OnClick", "escondeLinhas(" & i + 1 & "," & e.Item.ItemIndex & ", grdSegurados_Linha" & i.ToString & "_AbreFecha" & i.ToString & ");")
                        lbl.Text = "&nbsp;&nbsp;" & grdSegurados.Items(i).Cells(1).Text
                        grdSegurados.Items(i).Cells(1).Text = String.Empty
                        grdSegurados.Items(i).Cells(1).Controls.Add(AbreFecha)
                        grdSegurados.Items(i).Cells(1).Controls.Add(lbl)
                        Exit For
                    End If
                Next

            End If

            e.Item.Attributes.Add("onmouseover", "this.style.background='#E8F1FF'")
            e.Item.Attributes.Add("Style", "CURSOR: pointer")
            e.Item.Cells(e.Item.Cells.Count - 1).HorizontalAlign = HorizontalAlign.Right

        End If

    End Sub

    Private Sub grdFaturas_ItemCreated(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdFaturas.ItemCreated

        Try
            If e.Item.ItemType = ListItemType.Pager Then
                Dim cl As New TableCell

                cl.Text = "Total&nbsp;de&nbsp;Registros:&nbsp;"

                If Not IsNothing(Session("dtFatura")) Then
                    cl.Text &= Session("dtFatura").Rows.Count.ToString()
                End If

                cl.HorizontalAlign = HorizontalAlign.Right
                cl.BorderWidth = Unit.Pixel(0)
                cl.ColumnSpan = 2
                e.Item.Cells(0).ColumnSpan -= 2
                e.Item.Cells(0).BorderWidth = Unit.Pixel(0)
                e.Item.Cells.Add(cl)
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message)
            'End If
        End Try

    End Sub

    Private Sub grdSegurados_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdSegurados.ItemCreated

        Try
            If e.Item.ItemType = ListItemType.Pager Then
                Dim cl As New TableCell
                cl.Text = "Total&nbsp;de&nbsp;Registros:&nbsp;" & TotalSegurados.ToString
                cl.HorizontalAlign = HorizontalAlign.Right
                cl.BorderWidth = Unit.Pixel(0)
                cl.ColumnSpan = 3
                e.Item.Cells(0).ColumnSpan -= 3
                e.Item.Cells(0).BorderWidth = Unit.Pixel(0)
                e.Item.Cells.Add(cl)
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message)
            'End If
        End Try

    End Sub

    Private Function getPeriodoCompetenciaSession() As String

        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)



        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
        Dim data_inicio, data_fim, retorno As String

        data_inicio = ""
        data_fim = ""
        retorno = ""

        If dr.HasRows Then
            While dr.Read()

                data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)

            End While

            retorno = "Per�odo de Compet�ncia: " & data_inicio & " a " & data_fim
        Else
            Session.Abandon()
            cUtilitarios.br("N�o existe nenhuma Compet�ncia de Fatura em aberto")
            System.Web.HttpContext.Current.Response.Write("<script>parent.window.location=parent.window.location;</script>")
        End If

        Return retorno



    End Function

    Private Sub FormaPgto()
        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        Dim dt As New DataTable

        Try

            'bd.ObtemFormaPagamento(Session("apolice"), Session("ramo"))
            bd.ObtemFormaPagamento(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
            dt = bd.ExecutaSQL_DT()

            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item("TP_PGTO") = 1 Then
                    'Caso o forma de pagamento seja D�BITO EM CONTA n�o ir� imprimir o boleto
                    chkImpBoletos.Enabled = False
                End If
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Function EscreverEscondeLinhas() As Boolean

        Dim js As String
        Try
            js = "function escondeLinhas(primeiraLinha, ultimaLinha, imagem)" & vbNewLine
            js &= "{" & vbNewLine
            js &= "     if (eval('grdSegurados_Linha'+primeiraLinha).style.display == 'none')" & vbNewLine
            js &= "     {" & vbNewLine
            js &= "         imagem.src='images\\imgmenos.gif';" & vbNewLine
            js &= "         for (i=primeiraLinha;i<=ultimaLinha;i++)" & vbNewLine
            js &= "         {" & vbNewLine
            js &= "             eval('grdSegurados_Linha'+i).style.display = '';" & vbNewLine
            js &= "         }" & vbNewLine
            js &= "     }" & vbNewLine
            js &= "     else" & vbNewLine
            js &= "     {" & vbNewLine
            js &= "         imagem.src='images\\imgmais.gif';" & vbNewLine
            js &= "         for (i=primeiraLinha;i<=ultimaLinha;i++)" & vbNewLine
            js &= "         {" & vbNewLine
            js &= "             eval('grdSegurados_Linha'+i).style.display = 'none';" & vbNewLine
            js &= "         }" & vbNewLine
            js &= "     }" & vbNewLine
            js &= "}" & vbNewLine
            cUtilitarios.escreveScript(js)
        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message)
            'End If
        End Try

    End Function

    Private Function EscreverImprimir() As Boolean

        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        Dim Dt As New DataTable
        Dim Caminho As String

        Dim js As String

        Try


            '---Rotina incluida para buscar o num_cobranca para gera��o do boleto
            '---Flow 3717553 - 
            '---Douglas - Confitec 13.05.2010
            Dim Cobranca As Integer

            bd.Obtem_NumCobranca(Session("apolice"), Session("ramo"), Me.lblFatura.Text)
            Dt = bd.ExecutaSQL_DT()

            If Dt.Rows.Count > 0 Then
                Cobranca = Dt.Rows(0).Item("Num_Cobranca")
            End If
            '---


            js = "function imprimir()" & vbNewLine
            js &= "{" & vbNewLine

            bd.ImprimeBoleto(Session("apolice"), Session("ramo"))
            Dt = bd.ExecutaSQL_DT()

            js &= "     if(document.getElementById('chkImpFatura').checked)" & vbNewLine

            Caminho = "ImprimirFatura.aspx?SLinkSeguro=" & Request("SLinkSeguro")

            js &= "         abre_janela('" & Caminho & "','janela_Fatura','no','yes','750','500','10','10');" & vbNewLine
            js &= "     if(document.getElementById('chkImpBoletos').checked)" & vbNewLine

            If Not IsNothing(Request("SF")) AndAlso Request("SF").ToUpper = "PENDENTE" Then

                ' Tratamento Https 
                Dim Https As String = ""
                If Request.ServerVariables("HTTPS") = "off" Then
                    Https = "http://"
                Else
                    Https = "https://"
                End If

                Caminho = Https & Request.ServerVariables("SERVER_NAME") & "/internet/serv/boleto/impressao/boleto.asp?proposta="
                Caminho &= Dt.Rows(0).Item(0).ToString()
                Caminho &= "&cobranca="
                '---
                'Caminho &= Me.grdFaturas.DataKeys.Item(0)     'lblFatura.Text
                Caminho &= Cobranca
                '---
                Caminho &= "&SLinkSeguro=" & Request("SLinkSeguro") 'Vulnerabilidade 2020 - inclus�o LinkSeguro

                js &= "         abre_janela('" & Caminho & "','janela_boleto','no','yes','750','500','20','20');" & vbNewLine

            Else

                js &= "         alert('Esta Fatura n�o est� pendente.');" & vbNewLine

            End If

            js &= "     if(document.getElementById('chkImpRelacao').checked)" & vbNewLine

            Caminho = "ImprimirRelacaoVidas.aspx?SLinkSeguro=" & Request("SLinkSeguro")

            js &= "         abre_janela('" & Caminho & "','janela_Vidas','no','yes','750','500','30','30');" & vbNewLine
            js &= "}" & vbNewLine
            cUtilitarios.escreveScript(js)
        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message + bd.SQL)
            'End If
        End Try

    End Function

    Private Function getSugestaoCompetencia(ByVal alvo As String) As String
        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)

        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
        Dim data_inicio, data_fim, retorno As String

        data_inicio = ""
        data_fim = ""
        retorno = ""

        If dr.HasRows Then

            While dr.Read()
                data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)
            End While

            Dim data_de As DateTime = CDate(data_inicio), data_ate As DateTime = CDate(data_fim)

            If alvo = "txtde" Then
                data_de = data_de.AddMonths(-12)
                retorno = Right(cUtilitarios.trataData(data_de).ToString, 7)
            Else
                data_ate = data_ate.AddMonths(-1)
                retorno = Right(cUtilitarios.trataData(data_ate), 7)
            End If

        End If

        Return retorno

    End Function

    'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - in�cio
#Region "Prorroga��o"

    Public Const SITUACAO_PENDENTE As String = "Pendente"

    Private Sub ExibirPainelProrrogacao()
        pnlDetalhesFatura.Visible = False
        pnlProrrogacao.Visible = True
        grdProrrogacao.Visible = True
    End Sub

    ''' <summary>
    '''  27/12/2018 - Demanda MU-2018-070495 - Cleiton Queiroz - Confitec SP
    ''' </summary>
    ''' <param name="fatura_id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function Redirecionar(ByVal fatura_id As Integer) As Boolean

        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        Dim Dt As New DataTable
        Dim Caminho As String

        Dim js As String

        Try

            Dim Cobranca As Integer

            bd.Obtem_NumCobranca(Session("apolice"), Session("ramo"), fatura_id)
            Dt = bd.ExecutaSQL_DT()

            If Dt.Rows.Count > 0 Then
                Cobranca = Dt.Rows(0).Item("Num_Cobranca")
            End If

            bd.ImprimeBoleto(Session("apolice"), Session("ramo"))
            Dt = bd.ExecutaSQL_DT()

            'Caminho = "http://localhost/impressao/linhaDigitavel.asp?proposta="
            Caminho = "http://" & Request.ServerVariables("SERVER_NAME") & "/internet/serv/boleto/impressao/linhaDigitavel.asp?proposta="
            Caminho &= Dt.Rows(0).Item(0).ToString()
            Caminho &= "&cobranca="

            Caminho &= Cobranca

            js = "         abre_janela('" & Caminho & "','janela_boleto','no','no','675','225','200','200');" & vbNewLine


            Me.ClientScript.RegisterClientScriptBlock(Me.GetType(), "AbreJanela", js, True)
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Function

    Private Sub EscondePainelProrrogacao()
        grdProrrogacao.Visible = False
        pnlProrrogacao.Visible = False
    End Sub

    Private Sub ExibirBotaoProrrogar(ByVal DataGrid As DataGrid)

        For Each item As DataGridItem In DataGrid.Items
            If item.Cells(5).Text = SITUACAO_PENDENTE Then
                DataGrid.Items(item.ItemIndex).Cells(6).Enabled = True
            Else
                DataGrid.Items(item.ItemIndex).Cells(6).Enabled = False
            End If
        Next item

    End Sub

    Private Sub grdProrrogacaoDataBind()
        Dim dtDadosFatura As New DataTable

        dtDadosFatura = CarregarDataTableFaturas()
        dtDadosFatura = ExcluirColunasFatura(dtDadosFatura)
        dtDadosFatura = CarregarDadosProrrogacao(dtDadosFatura)

        If Not IsNothing(dtDadosFatura) Then

            Session("TotalProrrogacao") = dtDadosFatura.Rows.Count

            grdProrrogacao.DataSource = dtDadosFatura
            grdProrrogacao.DataBind()

        End If

        Call ExibirBotaoProrrogar(grdFaturas)

    End Sub

    Private Function CarregarDadosProrrogacao(ByVal dtDadosFatura As DataTable) As DataTable
        Dim count As Integer
        Dim linha As Integer

        If Not IsNothing(Session("fatura_prorrogacao")) Then
            dtDadosFatura.Columns.Add("subgrupo", Type.GetType("System.Int64"))

            linha = 0

            For count = 0 To dtDadosFatura.Rows.Count - 1
                If dtDadosFatura.Rows(linha).ItemArray(0) <> Session("fatura_prorrogacao") Then
                    dtDadosFatura.Rows(linha).Delete()
                    dtDadosFatura.AcceptChanges()
                Else
                    linha = linha + 1
                End If
            Next count

            dtDadosFatura.Rows(0)("subgrupo") = Session("subgrupo_id")

            Return dtDadosFatura

        Else : Return Nothing : End If

    End Function

    Private Function ExcluirColunasFatura(ByVal dtDadosFatura As DataTable) As DataTable
        dtDadosFatura.Columns.Remove("desc_situacao")
        dtDadosFatura.Columns.Remove("dt_baixa")
        dtDadosFatura.Columns.Remove("val_liquid")
        dtDadosFatura.Columns.Remove("origem_baixa")
        dtDadosFatura.Columns.Remove("wf_id")
        dtDadosFatura.Columns.Remove("val_iof")
        dtDadosFatura.Columns.Remove("num_cobranca")

        Return dtDadosFatura

    End Function

    Private Function CalculaJuros(ByVal val_cobranca As String, ByVal dt_original As String, ByVal dt_agendamento As String) As String
        Dim vencimento As Date
        Dim novo_vencimento As Date
        Dim diferenca As Integer
        Dim cont As Integer
        Dim total As Double



        vencimento = Convert.ToDateTime(dt_original)
        novo_vencimento = Convert.ToDateTime(dt_agendamento)

        diferenca = Convert.ToInt32(DateDiff(DateInterval.Day, vencimento, novo_vencimento))

        cont = 0
        total = Convert.ToDouble(val_cobranca)

        'CALCULO COM JUROS SIMPLES
        total = total + ((total * 0.000748) * diferenca)

        'CALCULO COM JUROS COMPOSTO
        'While (cont < diferenca)
        '    total = total + (total * 0.000748)
        '    cont = cont + 1
        'End While

        total = System.Math.Round(total, 2)

        juros = "s"

        Return total.ToString


    End Function



    Private Sub ImprimirFaturaProrrogacao(ByVal novoVencimento As String, ByVal faturaProrrogacao As String)
        Dim bancoDados As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        Dim dtDadosBoleto As New DataTable
        Dim dt As New DataTable
        Dim QueryString As String
        Dim Caminho As String = ""
        Dim Https As String

        bancoDados.ImprimeBoleto(Session("apolice"), Session("ramo"))
        dtDadosBoleto = bancoDados.ExecutaSQL_DT()


        bancoDados.Obtem_NumCobranca(Session("apolice"), Session("ramo"), faturaProrrogacao)
        Dt = bancoDados.ExecutaSQL_DT()

        If Dt.Rows.Count > 0 Then
            faturaProrrogacao = dt.Rows(0).Item("Num_Cobranca")
        End If


        QueryString = "?proposta=" & dtDadosBoleto.Rows(0).Item(0).ToString & "&cobranca=" & faturaProrrogacao & "&dtfim=" & novoVencimento

        If juros = "s" Then
            QueryString = QueryString & "&juros=" & juros & "&dtorig=" & dt_original
        End If

        'bnery - Nova Consultoria - original --
        If Request.ServerVariables("HTTPS") = "off" Then
            Https = "http://"
        Else
            Https = "https://"
        End If

        Caminho = Https & Request.ServerVariables("SERVER_NAME") & "/internet/serv/boleto/impressao/"
        'bnery - Nova Consultoria - teste para qualidade --
        'Caminho = "https://qld.aliancadobrasil.com.br/internet/serv/boleto/impressao/"

        Caminho = Caminho & "boleto.asp"
        Caminho = Caminho & QueryString & "&SLinkSeguro=" & Request("SLinkSeguro") 'Vulnerabilidade 2020 - inclus�o LinkSeguro

        cUtilitarios.escreveScript("window.open('" & Caminho & "'," & _
                                                 "'BoletoComVencimentoProrrogado'," & _
                                                 "'resizable=no," & _
                                                 "scrollbars=yes," & _
                                                 "width=1024," & _
                                                 "height=500," & _
                                                 "top=20," & _
                                                 "left=20," & _
                                                 "toolbar=no," & _
                                                 "location=no," & _
                                                 "directories=no," & _
                                                 "status=no," & _
                                                 "menubar=no')")
    End Sub

    Public Sub ShowMessageBox(ByVal mensagem As String)
        Dim pagina As Page = TryCast(HttpContext.Current.Handler, Page)

        If pagina IsNot Nothing Then

            mensagem = mensagem.Replace("'", "'")
            Dim script As String = String.Format("alert('{0}');", mensagem)
            pagina.ClientScript.RegisterStartupScript(pagina.[GetType](), "Alerta", script, True)

        End If

    End Sub

    Private Sub grdProrrogacao_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdProrrogacao.ItemCreated

        Try
            If e.Item.ItemType = ListItemType.Pager Then
                Dim cl As New TableCell
                cl.Text = "Total&nbsp;de&nbsp;Registros:&nbsp;" & Session("TotalProrrogacao")
                cl.HorizontalAlign = HorizontalAlign.Right
                cl.BorderWidth = Unit.Pixel(0)
                cl.ColumnSpan = 3
                e.Item.Cells(0).ColumnSpan -= 3
                e.Item.Cells(0).BorderWidth = Unit.Pixel(0)
                e.Item.Cells.Add(cl)
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Protected Sub btnConfirmarProrrogacao_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirmarProrrogacao.Click
        Dim bancoDados As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        Dim dtDadosProrrogacao As DataTable
        Dim txtVencimentoEscolhido As TextBox
        Dim proposta_id As String
        Dim dt_agendamento As String
        Dim val_cobranca As String
        Dim fatura_id As String
        Dim endosso_id As String

        Try

            For Each item As DataGridItem In grdProrrogacao.Items
                txtVencimentoEscolhido = grdProrrogacao.Items(item.ItemIndex).FindControl("txtVencimentoEscolhido")

                If txtVencimentoEscolhido.Text <> "" Then

                    'Data agendamento
                    dt_agendamento = txtVencimentoEscolhido.Text

                    'falmeida - Nova Consultoria in�cio ===================================
                    'Data original - (Apenas para o calculo dos juros)
                    dt_original = item.Cells(4).Text

                    'Valor cobran�a
                    'val_cobranca = Replace(item.Cells(3).Text, ",", ".")
                    val_cobranca = Replace(CalculaJuros(item.Cells(3).Text, dt_original, dt_agendamento), ",", ".")

                    'falmeida - Nova Consultoria fim ======================================

                    'Fatura
                    fatura_id = item.Cells(0).Text

                    'Endosso
                    bancoDados.ImprimeFatura(Session("apolice"), Session("ramo"), fatura_id)
                    dtDadosProrrogacao = bancoDados.ExecutaSQL_DT
                    endosso_id = dtDadosProrrogacao.Rows(0).ItemArray(7).ToString
                    dtDadosProrrogacao = Nothing

                    'Proposta
                    bancoDados.ImprimeBoleto(Session("apolice"), Session("ramo"))
                    dtDadosProrrogacao = bancoDados.ExecutaSQL_DT
                    proposta_id = dtDadosProrrogacao.Rows(0).ItemArray(0).ToString
                    dtDadosProrrogacao = Nothing

                    'Gravar hist�rico

                    bancoDados.GravarHistoricoProrrogacao(proposta_id, dt_agendamento, val_cobranca, fatura_id, endosso_id)
                    bancoDados.ExecutaSQL()

                    Call ImprimirFaturaProrrogacao(dt_agendamento, fatura_id)
                Else
                    Call ShowMessageBox("N�o foi escolhida uma data v�lida para o novo vencimento.")
                End If
            Next item

        Catch ex As Exception
            Dim mensagemErro As String = Replace(Replace(ex.Message, vbCrLf, ""), "5 - ExecuteDataset - Erro", "")
            Call ShowMessageBox(mensagemErro)
        End Try

    End Sub

#End Region
    'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - fim

End Class
