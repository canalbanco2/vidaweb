
Public Class cConsultarFaturas

#Region "Heran�a"
    Inherits cBanco
#End Region


    Public Sub ConsultaFaturasPorPeriodoComp(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subGrupo_id As String, ByVal dt_ini As String, ByVal dt_fim As String)

        SQL = " EXEC dbo.SEGS5739_SPS "
        SQL &= "     @ramo_id = " & ramo_id
        SQL &= "   , @apolice_id = " & apolice_id
        SQL &= "   , @subgrupo_id = " & subGrupo_id
        SQL &= "   , @dt_ini = '" & dt_ini & "'"
        SQL &= "   , @dt_fim = '" & dt_fim & "'"

    End Sub

    Public Sub ConsultaFaturasPendentes(ByVal apolice_id As String, ByVal ramo_id As String)
        SQL = " EXEC vida_web_db..SEGS9678_SPS "
        SQL &= "     @ramo_id = " & ramo_id
        SQL &= "   , @apolice_id = " & apolice_id

    End Sub

    Public Sub ConsultarSegurados(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subGrupo_id As String, ByVal fatura_id As Integer, Optional ByVal Operacao As String = "X")

        SQL = " EXEC dbo.SEGS5740_SPS "
        SQL &= "     @ramo_id = " & ramo_id
        SQL &= "   , @apolice_id = " & apolice_id
        SQL &= "   , @subgrupo_id = " & subGrupo_id
        SQL &= "   , @fatura_id = " & fatura_id
        If Operacao <> "X" Then
            SQL &= "   , @operacao = '" & Operacao & "'"
        End If

    End Sub

    Public Sub New(Optional ByVal banco As cDadosBasicos.eBanco = cDadosBasicos.eBanco.web_seguros_db)
        MyBase.New(banco)
        Alianca.Seguranca.BancoDados.cCon.BancodeDados = banco.ToString
    End Sub

    Function trInt(ByVal valor As Object) As String

        If valor = 0 Then
            Return "null"
        Else
            Return valor
        End If

    End Function

    Public Sub ImprimeBoleto(ByVal apolice_id As String, ByVal ramo_id As String)

        SQL = "EXEC SISAB003.SEGUROS_DB.dbo.SEGS5746_SPS @apolice_id = " & apolice_id
        SQL &= "                           , @ramo_id = " & ramo_id

    End Sub

    Public Sub SEGS5696_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal workflow As Integer, ByVal valor_id As Integer)
        SQL = "exec segs5696_sps  @apolice_id = " & apolice_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @tp_wf_id = " & workflow
        SQL &= ", @tp_versao_id = " & valor_id
    End Sub

    Public Sub ImprimeFatura(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal fatura_id As Integer)

        SQL = "SISAB003.seguros_db.dbo.rel_fatura_sub_grupo_sps " & apolice_id & ", 0, 6785, " & ramo_id & ", " & fatura_id

    End Sub

    Public Sub ImprimeRelacaoVidas(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal fatura_id As Integer, ByVal Operacao As String)

        SQL = "SISAB003.seguros_db.dbo.SEGS5747_SPS " & apolice_id & ", 0 , 6785, " & ramo_id & ", " & fatura_id & ", '" & Operacao & "' "

    End Sub

    Public Sub ObtemFormaPagamento(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)

        'SQL = "[SISAB003].SEGUROS_DB.dbo.SEGS5809_SPS " & apolice_id & ", " & ramo_id & ", 0, 6785"
        SQL = " SELECT tp_pgto = forma_pgto_id " & _
              "   FROM [SISAB003].seguros_db.dbo.sub_grupo_apolice_tb " & _
              "  WHERE apolice_id = " & apolice_id & _
              "    AND sucursal_seguradora_id = 0" & _
              "    AND seguradora_cod_susep = 6785" & _
              "    AND ramo_id = " & ramo_id & _
              "    AND sub_grupo_id = " & subgrupo_id

    End Sub

    '---Rotina incluida para buscar o num_cobranca para gera��o do boleto
    '---Flow 3717553 - 
    '---Douglas - Confitec 13.05.2010

    Public Sub Obtem_NumCobranca(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal fatura_id As Integer)

        SQL = " Select agendamento_cobranca_tb.num_cobranca " & _
              " FROM SISAB003.seguros_db.dbo.agendamento_cobranca_tb agendamento_cobranca_tb (nolock)" & _
              " WHERE agendamento_cobranca_tb.apolice_id = " & apolice_id & _
              " AND agendamento_cobranca_tb.ramo_id  = " & ramo_id & _
              " AND agendamento_cobranca_tb.fatura_id = " & fatura_id
    End Sub

    Public Sub GravarHistoricoProrrogacao(ByVal proposta_id As String, ByVal data_agendamento As String, _
                                          ByVal val_cobranca As String, ByVal fatura_id As String, ByVal endosso_id As String)
        SQL = "EXEC SISAB003.SEGUROS_DB.dbo.SEGS10482_SPI "
        SQL &= proposta_id & ",'"
        SQL &= data_agendamento & "',"
        SQL &= val_cobranca & ","
        SQL &= fatura_id & ","
        SQL &= endosso_id
    End Sub

End Class
