<%@ Page Language="vb" AutoEventWireup="false" Codebehind="consultarfaturas.aspx.vb" EnableViewStateMac="true" Inherits="SEGW0072.ConsultarFaturas" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ConsultarFaturas</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
			<script src="scripts/overlib421/overlib.js" type="text/javascript"></script>
			<script src="scripts/script.js" type="text/javascript"></script>
			<SCRIPT language="javascript">
			function abre_janela(link,nome,resizable,scrollbars,width,height,top,left)
				{  
					nova=window.open(link,nome,
					"resizable="+resizable+",scrollbars="+scrollbars+",width="+width+",height="+height+",top="+top+",left="+left+",toolbar=0,location=0,directories=0,status=0,menubar=0");
				}
			function formataData(i, delKey)	{
				if (i.value.length < 8){
					if (delKey!=9) { // se for tab
						if(delKey!=8 && delKey!=46 && delKey!=16 &&  !(delKey>36 && delKey<41))
						{ //teclas delete, backspace, shift, nao disparam o evento
							var fieldLen = i.value.length
							if ((delKey >= 48 && delKey <= 57) || (delKey >= 96 && delKey <=105)){
								if ((fieldLen == 2))	{
									i.value = i.value + "/";
								}
							} 
							i.focus();
						}
					} 
				}
			}
								
			</SCRIPT>
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<div id="overDiv" style="Z-INDEX: 1000; VISIBILITY: hidden; POSITION: absolute"></div>
		<form id="Form1" method="post" runat="server">
			<P><asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela">Label</asp:label><BR>
				<BR>
				<asp:label id="lblVigencia" runat="server" CssClass="Caminhotela"></asp:label><BR>
				<BR>
				<asp:label id="Label6" runat="server" CssClass="Caminhotela">Informe o per�odo de compet�ncia m�s e ano (mm/aaaa):</asp:label>&nbsp;
				<asp:textbox id="txtDe" onkeydown="javascript:formataData(this, window.event.keyCode);" runat="server"
					Width="56px" MaxLength="7"></asp:textbox>&nbsp;
				<asp:label id="Label7" runat="server" CssClass="Caminhotela">at�</asp:label>&nbsp;
				<asp:textbox id="txtAte" onkeydown="javascript:formataData(this, window.event.keyCode);" runat="server"
					Width="56px" MaxLength="7"></asp:textbox>&nbsp;
				<asp:image id="Image2" onmouseover="return overlib('[Ajuda]');" onmouseout="return nd();" runat="server"
					ImageUrl="Images/help.gif"></asp:image>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<asp:button id="btnListar" runat="server" CssClass="Botao" Width="80px" Text="Pesquisar"></asp:button><br>
				<BR>
				<asp:label id="lblTituloFatura" CssClass="Caminhotela" Visible="False" Runat="server">Selecione a fatura a consultar</asp:label>
				<asp:datagrid id="grdFaturas" runat="server" PageSize="6" AllowPaging="True" GridLines="Vertical"
					CellPadding="3" BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#999999" 
					AutoGenerateColumns="False" DataKeyField="num_cobranca" Width="680px" >
					<FooterStyle Wrap="False"></FooterStyle>
					<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
					<EditItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></EditItemStyle>
					<AlternatingItemStyle HorizontalAlign="Center" VerticalAlign="Middle" BackColor="WhiteSmoke"></AlternatingItemStyle>
					<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
					<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="titulo-tabela sem-sublinhado"></HeaderStyle>
					<Columns>
						<asp:BoundColumn DataField="fatura_id" HeaderText="N&#250;mero Fatura">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False"></ItemStyle>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="competencia" HeaderText="Compet&#234;ncia da Fatura">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False"></ItemStyle>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="dt_recebimento" HeaderText="Vencimento">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="val_bruto" HeaderText="Valor do Pr&#234;mio">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False"></ItemStyle>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="dt_baixa" HeaderText="Dt. do Pagamento">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False"></ItemStyle>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="desc_situacao" HeaderText="Situa&#231;&#227;o">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False"></ItemStyle>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:BoundColumn>
						<%--  Inicio 27/12/2018 - Demanda MU-2018-070495 - Cleiton Queiroz - Confitec SP --%>    
						<asp:ButtonColumn HeaderText="Atualiza��o do boleto" ButtonType="linkbutton" Text="Atualizar"
						                  CommandName="prorrogar" ItemStyle-CssClass="Botao" />
                        <%--  Fim 27/12/2018 - Demanda MU-2018-070495 - Cleiton Queiroz - Confitec SP --%>    						                  
					</Columns>
					<PagerStyle HorizontalAlign="Center" CssClass="fonte-branca" Wrap="False" Mode="NumericPages"></PagerStyle>
				</asp:datagrid></P>
			<asp:panel id="pnlFiltro" Width="100%" Visible="False" Runat="server">
				<TABLE id="Table4" height="54" cellSpacing="1" cellPadding="1" width="600" border="0">
					<TR>
						<TD align="right" width="5%">
							<asp:Label id="Label13" runat="server" CssClass="Caminhotela">Opera��o:</asp:Label></TD>
						<TD width="5%">
							<asp:DropDownList id="DropDownList1" runat="server">
								<asp:ListItem Value="Todos">Todos</asp:ListItem>
								<asp:ListItem Value="Inclus&#245;es">Inclus&#245;es</asp:ListItem>
								<asp:ListItem Value="Exclus&#245;es">Exclus&#245;es</asp:ListItem>
							</asp:DropDownList></TD>
						<TD align="right" width="5%">
							<asp:Label id="Label11" runat="server" CssClass="Caminhotela">Nome:</asp:Label></TD>
						<TD width="5%">
							<asp:TextBox id="TextBox1" runat="server" Width="173px"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD align="right" width="5%">
							<asp:Label id="Label12" runat="server" CssClass="Caminhotela">CPF:</asp:Label></TD>
						<TD width="5%">
							<asp:TextBox id="TextBox2" runat="server"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD align="center" width="5%" colSpan="4">
							<asp:Button id="Button1" runat="server" CssClass="Botao" Width="75px" Text="Pesquisar"></asp:Button></TD>
					</TR>
				</TABLE>
			</asp:panel><asp:panel id="pnlDetalhesFatura" runat="server" Width="700px" Visible="False" HorizontalAlign="Center">
				<TABLE id="Table5" height="22" cellSpacing="1" cellPadding="1" width="100%" border="0">
					<TR>
						<TD noWrap height="14">
							<asp:Label id="Label14" runat="server" CssClass="Caminhotela" Width="160px">Detalhamento da fatura</asp:Label>
							<asp:Label id="lblFatura" runat="server" CssClass="Caminhotela"></asp:Label>
						</TD>
						<td>
							<asp:Label id="lblPesquisar" runat="server" CssClass="Caminhotela">Pesquisado por:</asp:Label>
							<asp:Label id="lblFiltro" runat="server" CssClass="Caminhotela"></asp:Label>
						</td>
						
						<TD noWrap align="right" height="14">
							<asp:DropDownList id="ddlFiltroSegurados" runat="server">
								<asp:ListItem Value="X">Todos</asp:ListItem>
								<asp:ListItem Value="I">Inclus&#245;es</asp:ListItem>
								<asp:ListItem Value="A">Altera&#231;&#245;es</asp:ListItem>
								<asp:ListItem Value="E">Exclus&#245;es</asp:ListItem>
								<asp:ListItem Value="C">Cobertura Provis�ria</asp:ListItem>
								<asp:ListItem Value="N">Hist�rico</asp:ListItem>
							</asp:DropDownList>
							<asp:Button id="btnFiltroSegurados" runat="server" CssClass="Botao" Width="73px" Text="Pesquisar"></asp:Button></TD>
					</TR>
				</TABLE>
				<asp:DataGrid id="grdSegurados" runat="server" Width="700px" Visible="False" AutoGenerateColumns="False"
					AllowPaging="True">
					<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
					<EditItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></EditItemStyle>
					<AlternatingItemStyle VerticalAlign="Middle"></AlternatingItemStyle>
					<ItemStyle VerticalAlign="Middle"></ItemStyle>
					<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="titulo-tabela sem-sublinhado"></HeaderStyle>
					<Columns>
						<asp:BoundColumn DataField="operacao" HeaderText="Opera&#231;&#227;o">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Left" Width="1px"></ItemStyle>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="nome" HeaderText="Segurado">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle HorizontalAlign="Left"></ItemStyle>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="componente" HeaderText="Tipo">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Left" Width="1px"></ItemStyle>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="cpf" HeaderText="CPF">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Left" Width="90px"></ItemStyle>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="dt_nascimento" HeaderText="Nascimento">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Left" Width="1px"></ItemStyle>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="sexo" HeaderText="Sexo">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Left" Width="1px"></ItemStyle>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="salario_seg" HeaderText="Sal&#225;rio&amp;nbsp;(R$)">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Right" Width="80px"></ItemStyle>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="capital_seg" HeaderText="Capital&amp;nbsp;(R$)">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Right" Width="80px"></ItemStyle>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn Visible="False" DataField="qtd_depen" HeaderText="qtd_depen"></asp:BoundColumn>
					</Columns>
					<PagerStyle HorizontalAlign="Center" CssClass="fonte-branca" Mode="NumericPages"></PagerStyle>
				</asp:DataGrid>
				<BR>
				<asp:CheckBox id="chkImpFatura" runat="server" Text="Imprimir Fatura"></asp:CheckBox>
				<asp:CheckBox id="chkImpBoletos" runat="server" Text="Imprimir Boletos"></asp:CheckBox>
				<asp:CheckBox id="chkImpRelacao" runat="server" Text="Imprimir Rela��o de Vidas"></asp:CheckBox>
				<BR>
				<BR>
				<INPUT class="Botao" id="btnImprimir" onclick="imprimir();" type="button" value="Imprimir/Consultar"
					Width="73px">
			</asp:panel>
			<asp:Panel id="pnlProrrogacao" runat="server" Width="700px" Visible="False" HorizontalAlign="Center">
			    <p align =left>
			        <asp:Label id="lblProrrogarFatura" runat="server" CssClass="Caminhotela" Width="100%">
			            Prorrogar vencimento da fatura:
			        </asp:Label>
			    </p>
				<asp:DataGrid id="grdProrrogacao" runat="server" Width="700px" Visible="False" AutoGenerateColumns="False"
					AllowPaging="True">
					<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
					<EditItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></EditItemStyle>
					<AlternatingItemStyle VerticalAlign="Middle"></AlternatingItemStyle>
					<ItemStyle VerticalAlign="Middle"></ItemStyle>
					<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="titulo-tabela sem-sublinhado"></HeaderStyle>
					<Columns>
					    <asp:BoundColumn DataField="fatura_id" HeaderText="N&#250;mero Fatura">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False"></ItemStyle>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="subgrupo" HeaderText="Subgrupo">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False"></ItemStyle>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="competencia" HeaderText="Compet&#234;ncia da Fatura">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False"></ItemStyle>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="val_bruto" HeaderText="Valor do Pr&#234;mio">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False"></ItemStyle>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="dt_recebimento" HeaderText="Vencimento Real">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>
						<asp:TemplateColumn HeaderText="Vencimento Escolhido">						    
	                        <ItemTemplate>
		                        <asp:TextBox ID="txtVencimentoEscolhido" runat="server" BorderWidth="0px" Width="100%" Text="" 
		                                     OnKeyPress="formatar(this, '##/##/####')" OnBlur="validarData(this,this.value)"
		                                     MaxLength="10"/>                                
	                        </ItemTemplate>
                        	
	                        <HeaderStyle HorizontalAlign="Left" Width="250px" />
	                        <ItemStyle HorizontalAlign="Left" Width="250px" />                            
                        </asp:TemplateColumn>
					</Columns>
					<PagerStyle HorizontalAlign="Center" CssClass="fonte-branca" Mode="NumericPages"></PagerStyle>
				</asp:DataGrid>
				<br />				
				<asp:button ID="btnConfirmarProrrogacao" runat="server" CssClass="Botao"
				            OnClick="btnConfirmarProrrogacao_Click" Text="Confirmar" Width="150px" />
			</asp:Panel>
			</form>
		<script>
			top.escondeaguarde();
		</script>
	</body>
</HTML>
