<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ImprimirRelacaoVidas.aspx.vb" EnableViewStateMac="true" Inherits="SEGW0072.ImprimirRelacaoVidas" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML xmlns:o>
	<HEAD>
		<title>ImprimirFatura</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
			<SCRIPT language="javascript">
		function imprimir(x)
		{
			x.style.display = 'none';
			window.print();
			x.style.display = '';
		}

		<!--
		function abre_janela(link,nome,resizable,scrollbars,width,height,top,left)
			{  
			nova=window.open(link,nome,
							"resizable="+resizable+",scrollbars="+scrollbars+",width="+width+",height="+height+",top="+top+",left="+left+",toolbar=0,location=0,directories=0,status=0,menubar=0");
								}
		-->
			</SCRIPT>
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<asp:Panel id="pnlGeral" runat="server" HorizontalAlign="Center"></asp:Panel>
			<br>
			<center>
				<P>
					<asp:datagrid id="grdVidas" runat="server" Width="650px" PageSize="1" AllowPaging="True" GridLines="None"
						CellPadding="3" BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#999999"
						AutoGenerateColumns="False">
						<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
						<EditItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></EditItemStyle>
						<AlternatingItemStyle HorizontalAlign="Center" VerticalAlign="Middle" BackColor="WhiteSmoke"></AlternatingItemStyle>
						<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
						<HeaderStyle CssClass="titulo-tabela sem-sublinhado"></HeaderStyle>
						<Columns>
							<asp:BoundColumn DataField="operacao" HeaderText="Opera&#231;&#227;o">
								<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="SeqId" HeaderText="Seq">
								<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="segurado" HeaderText="Nome Segurado">
								<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="CPF" HeaderText="CPF">
								<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="DtNascimento" HeaderText="Dt. Nascimento" DataFormatString="{0:d}">
								<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="capital" HeaderText="Capital">
								<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="premio" HeaderText="Pr&#234;mio L&#237;quido">
								<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="PremioBruto" HeaderText="Pr&#234;mio Bruto">
								<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left"></ItemStyle>
							</asp:BoundColumn>
						</Columns>
						<PagerStyle Visible="False" HorizontalAlign="Center" CssClass="fonte-branca" Mode="NumericPages"></PagerStyle>
					</asp:datagrid>
                    <asp:Button ID="btnExportarExcel" runat="server" Text="Exportar Excel" CssClass="Botao" Width="99px" /></P>
			</center>
		</form>
		<script>
	removeEspacador();
	function imprimir()
	{
		document.getElementById("btnImprimir").style.display = 'none';
		print();
		document.getElementById("btnImprimir").style.display = '';
	}
		</script>
	</body>
</HTML>
