Public Class ImprimiFatura
    Inherits ImplementarSABL0101LinkSeguro

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnFatura As System.Web.UI.WebControls.Button
    Protected WithEvents btnRelacaoVidas As System.Web.UI.WebControls.Button
    Protected WithEvents btnResumo As System.Web.UI.WebControls.Button
    Protected WithEvents Label8 As System.Web.UI.WebControls.Label
    Protected WithEvents Label9 As System.Web.UI.WebControls.Label
    Protected WithEvents ddlFiltros As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnLuis As System.Web.UI.WebControls.Label
    Protected WithEvents ImageButton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Label10 As System.Web.UI.WebControls.Label
    Protected WithEvents lblEstipulante As System.Web.UI.WebControls.Label
    Protected WithEvents lblSubGrupo As System.Web.UI.WebControls.Label
    Protected WithEvents lblSubEstip As System.Web.UI.WebControls.Label
    Protected WithEvents lblSucursal As System.Web.UI.WebControls.Label
    Protected WithEvents lblApolice As System.Web.UI.WebControls.Label
    Protected WithEvents lblEndosso As System.Web.UI.WebControls.Label
    Protected WithEvents lblMoeda As System.Web.UI.WebControls.Label
    Protected WithEvents lblDtEmissao As System.Web.UI.WebControls.Label
    Protected WithEvents lblTipoPgto As System.Web.UI.WebControls.Label
    Protected WithEvents lblFatura As System.Web.UI.WebControls.Label
    Protected WithEvents lblCnpj2 As System.Web.UI.WebControls.Label
    Protected WithEvents lblCnpj1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblCPF2 As System.Web.UI.WebControls.Label
    Protected WithEvents lblCPF1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblDtVencto As System.Web.UI.WebControls.Label
    Protected WithEvents lblVigencia As System.Web.UI.WebControls.Label
    Protected WithEvents lblCorretor As System.Web.UI.WebControls.Label
    Protected WithEvents lblCapitalSaldoAnt As System.Web.UI.WebControls.Label
    Protected WithEvents lblPremioSaldoAnt As System.Web.UI.WebControls.Label
    Protected WithEvents lblVidasSaldoAnt As System.Web.UI.WebControls.Label
    Protected WithEvents lblVidasInc As System.Web.UI.WebControls.Label
    Protected WithEvents lblCapitalInc As System.Web.UI.WebControls.Label
    Protected WithEvents lblPremioInc As System.Web.UI.WebControls.Label
    Protected WithEvents lblVidasExc As System.Web.UI.WebControls.Label
    Protected WithEvents lblCapitalExc As System.Web.UI.WebControls.Label
    Protected WithEvents lblPremioExc As System.Web.UI.WebControls.Label
    Protected WithEvents lblVidasAlt As System.Web.UI.WebControls.Label
    Protected WithEvents lblCapitalAlt As System.Web.UI.WebControls.Label
    Protected WithEvents lblPremioAlt As System.Web.UI.WebControls.Label
    Protected WithEvents lblVidasAcerto As System.Web.UI.WebControls.Label
    Protected WithEvents lblCapitalAcerto As System.Web.UI.WebControls.Label
    Protected WithEvents lblPremioAcerto As System.Web.UI.WebControls.Label
    Protected WithEvents lblVidasCob As System.Web.UI.WebControls.Label
    Protected WithEvents lblCapitalCob As System.Web.UI.WebControls.Label
    Protected WithEvents lblPremioCob As System.Web.UI.WebControls.Label
    Protected WithEvents lblVidasSaldoAtual As System.Web.UI.WebControls.Label
    Protected WithEvents lblCapitalSaldoAtual As System.Web.UI.WebControls.Label
    Protected WithEvents lblPremioSaldoAtual As System.Web.UI.WebControls.Label
    Protected WithEvents lblVidasPendencia As System.Web.UI.WebControls.Label
    Protected WithEvents lblCapitalPendencia As System.Web.UI.WebControls.Label
    Protected WithEvents lblPremioPendencia As System.Web.UI.WebControls.Label
    Protected WithEvents lblVidasRecusas As System.Web.UI.WebControls.Label
    Protected WithEvents lblCapitalRecusas As System.Web.UI.WebControls.Label
    Protected WithEvents lblPremioRecusas As System.Web.UI.WebControls.Label
    Protected WithEvents lblObs As System.Web.UI.WebControls.Label
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button
    Protected WithEvents Form1 As System.Web.UI.HtmlControls.HtmlForm
    Protected WithEvents lblRamo As System.Web.UI.WebControls.Label
    Protected WithEvents lblPremioLiq As System.Web.UI.WebControls.Label
    Protected WithEvents lblIof As System.Web.UI.WebControls.Label
    Protected WithEvents lblPremioTotal As System.Web.UI.WebControls.Label
    'C00157284 - Eliminar pend�ncias a cada movimenta��o e criar hist�rico no Vida Web
    Protected WithEvents lblVidasInclPendente As System.Web.UI.WebControls.Label
    Protected WithEvents lblCapitalInclPendente As System.Web.UI.WebControls.Label
    Protected WithEvents lblPremioInclPendente As System.Web.UI.WebControls.Label
    Protected WithEvents lblVidasAltPendente As System.Web.UI.WebControls.Label
    Protected WithEvents lblCapitalAltPendente As System.Web.UI.WebControls.Label
    Protected WithEvents lblPremioAltPendente As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        Dim dt As New DataTable
        Dim valor As Integer = 0

        Try
            bd.ImprimeFatura(Session("apolice"), Session("ramo"), Session("fatura"))

            dt = bd.ExecutaSQL_DT()

            lblEstipulante.Text = dt.Rows(0).Item("prop_cliente_id") & " - " & dt.Rows(0).Item("nome")
            lblSubGrupo.Text = dt.Rows(0).Item("sub_grupo_id") & " - " & dt.Rows(0).Item("nome_sub")
            lblSubEstip.Text = dt.Rows(0).Item("est_cliente_id") & " - " & dt.Rows(0).Item("nome_est")
            lblSucursal.Text = dt.Rows(0).Item("sucursal_seguradora_id") & " - " & dt.Rows(0).Item("nome_suc")
            lblApolice.Text = dt.Rows(0).Item("apolice_id")
            lblEndosso.Text = dt.Rows(0).Item("endosso_id")
            lblMoeda.Text = dt.Rows(0).Item("premio_moeda_id") & " - " & dt.Rows(0).Item("nome_moeda")
            lblDtEmissao.Text = cUtilitarios.trataData(dt.Rows(0).Item("dt_emissao"))
            lblTipoPgto.Text = dt.Rows(0).Item("forma_pgto")

            valor = dt.Rows(0).Item("tp_est")

            Select Case valor
                Case 2
                    lblCnpj1.Text = CLng(DirectCast(dt.Rows(0).Item("cgc"), String)).ToString("00"".""000"".""000""/""0000""-""00")
                    lblCnpj2.Text = CLng(DirectCast(dt.Rows(0).Item("cgc_est"), String)).ToString("00"".""000"".""000""/""0000""-""00")
                    Me.lblCPF1.Visible = False
                    Me.lblCPF2.Visible = False
                    Me.lblCnpj1.Visible = True
                    Me.lblCnpj2.Visible = True

                Case 1
                    Me.lblCPF1.Text = CLng(DirectCast(dt.Rows(0).Item("CPF"), String)).ToString("000"".""000"".""000""-""00")
                    Me.lblCPF2.Text = CLng(DirectCast(dt.Rows(0).Item("CPF_est"), String)).ToString("000"".""000"".""000""-""00")
                    Me.lblCPF1.Visible = True
                    Me.lblCPF2.Visible = True
                    Me.lblCnpj1.Visible = False
                    Me.lblCnpj2.Visible = False

            End Select


            lblRamo.Text = dt.Rows(0).Item("ramo_id") & " - " & dt.Rows(0).Item("nome_ramo")
            lblFatura.Text = dt.Rows(0).Item("fatura_id")
            lblVigencia.Text = "Das 24:00 horas do dia " & cUtilitarios.trataData(dt.Rows(0).Item("dt_inicio_vigencia")) & " �s 24:00 horas do dia " & cUtilitarios.trataData(dt.Rows(0).Item("dt_fim_vigencia"))
            lblDtVencto.Text = cUtilitarios.trataData(dt.Rows(0).Item("dt_recebimento"))

            lblCorretor.Text = DirectCast(dt.Rows(0).Item("corr_pj"), Decimal).ToString("000000-000") & " - " & dt.Rows(0).Item("nome_corr_pj")
            lblVidasSaldoAnt.Text = dt.Rows(0).Item("num_vidas")
            lblCapitalSaldoAnt.Text = DirectCast(dt.Rows(0).Item("val_is"), Decimal).ToString("#,###,###,###,##0.00")
            lblPremioSaldoAnt.Text = DirectCast(dt.Rows(0).Item("val_premio"), Decimal).ToString("#,###,###,###,##0.00")

            lblVidasInc.Text = dt.Rows(0).Item("num_vidas_inc")
            lblCapitalInc.Text = DirectCast(dt.Rows(0).Item("val_is_inc"), Decimal).ToString("#,###,###,###,##0.00")
            lblPremioInc.Text = DirectCast(dt.Rows(0).Item("val_premio_inc"), Decimal).ToString("#,###,###,###,##0.00")

            lblVidasExc.Text = dt.Rows(0).Item("num_vidas_exc")
            lblCapitalExc.Text = DirectCast(dt.Rows(0).Item("val_is_exc"), Decimal).ToString("#,###,###,###,##0.00")
            lblPremioExc.Text = DirectCast(dt.Rows(0).Item("val_premio_exc"), Decimal).ToString("#,###,###,###,##0.00")

            lblVidasAlt.Text = dt.Rows(0).Item("num_vidas_aumento")
            lblCapitalAlt.Text = DirectCast(dt.Rows(0).Item("val_is_aumento"), Decimal).ToString("#,###,###,###,##0.00")
            lblPremioAlt.Text = DirectCast(dt.Rows(0).Item("val_premio_aumento"), Decimal).ToString("#,###,###,###,##0.00")

            lblVidasAcerto.Text = dt.Rows(0).Item("num_vidas_acerto")
            lblCapitalAcerto.Text = DirectCast(dt.Rows(0).Item("val_is_acerto"), Decimal).ToString("#,###,###,###,##0.00")
            lblPremioAcerto.Text = DirectCast(dt.Rows(0).Item("val_premio_acerto"), Decimal).ToString("#,###,###,###,##0.00")

            lblVidasCob.Text = dt.Rows(0).Item("num_vidas_cob_provisoria")
            lblCapitalCob.Text = DirectCast(dt.Rows(0).Item("val_is_cob_provisoria"), Decimal).ToString("#,###,###,###,##0.00")
            lblPremioCob.Text = DirectCast(dt.Rows(0).Item("val_premio_cob_provisoria"), Decimal).ToString("#,###,###,###,##0.00")

            lblVidasSaldoAtual.Text = dt.Rows(0).Item("num_vidas") + dt.Rows(0).Item("num_vidas_inc") + dt.Rows(0).Item("num_vidas_cob_provisoria") - dt.Rows(0).Item("num_vidas_exc")
            lblCapitalSaldoAtual.Text = DirectCast(dt.Rows(0).Item("val_is") + dt.Rows(0).Item("val_is_inc") + dt.Rows(0).Item("val_is_aumento") + dt.Rows(0).Item("val_is_acerto") + dt.Rows(0).Item("val_is_cob_provisoria") - dt.Rows(0).Item("val_is_exc"), Decimal).ToString("#,###,###,###,##0.00")
            lblPremioSaldoAtual.Text = DirectCast(dt.Rows(0).Item("val_premio") + dt.Rows(0).Item("val_premio_inc") + dt.Rows(0).Item("val_premio_aumento") + dt.Rows(0).Item("val_premio_acerto") + dt.Rows(0).Item("val_premio_cob_provisoria") - dt.Rows(0).Item("val_premio_exc"), Decimal).ToString("#,###,###,###,##0.00")

            lblPremioLiq.Text = dt.Rows(0).Item("val_liq")
            lblIof.Text = DirectCast(dt.Rows(0).Item("val_iof"), Decimal).ToString("#,###,###,###,##0.00")
            lblPremioTotal.Text = DirectCast(dt.Rows(0).Item("val_bruto"), Decimal).ToString("#,###,###,###,##0.00")

            lblVidasInclPendente.Text = dt.Rows(0).Item("vidas_pendentes_incl")
            lblCapitalInclPendente.Text = DirectCast(dt.Rows(0).Item("val_capital_incl_pendente"), Decimal).ToString("#,###,###,###,##0.00")
            lblPremioInclPendente.Text = DirectCast(dt.Rows(0).Item("premio_incl_pendente"), Decimal).ToString("#,###,###,###,##0.00")

            lblVidasAltPendente.Text = dt.Rows(0).Item("vidas_pendentes_alt")
            lblCapitalAltPendente.Text = DirectCast(dt.Rows(0).Item("val_capital_alt_pendente"), Decimal).ToString("#,###,###,###,##0.00")
            lblPremioAltPendente.Text = DirectCast(dt.Rows(0).Item("premio_alt_pendente"), Decimal).ToString("#,###,###,###,##0.00")

            lblObs.Text = dt.Rows(0).Item("observacao")

            'lblVidasPendencia.Text = dt.Rows(0).Item("num_vidas_pend")
            lblVidasRecusas.Text = dt.Rows(0).Item("num_vidas_recusa")

        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message)
            'End If
        End Try

    End Sub

End Class
