Public Class ConsultarFaturasPendentes
    Inherits ImplementarSABL0101LinkSeguro

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblNavegacao As System.Web.UI.WebControls.Label
    Protected WithEvents btnFatura As System.Web.UI.WebControls.Button
    Protected WithEvents btnRelacaoVidas As System.Web.UI.WebControls.Button
    Protected WithEvents btnResumo As System.Web.UI.WebControls.Button
    Protected WithEvents Label6 As System.Web.UI.WebControls.Label
    Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    Protected WithEvents Image2 As System.Web.UI.WebControls.Image
    Protected WithEvents Label5 As System.Web.UI.WebControls.Label
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents pnlDetalhesFatura As System.Web.UI.WebControls.Panel
    Protected WithEvents Label8 As System.Web.UI.WebControls.Label
    Protected WithEvents Label9 As System.Web.UI.WebControls.Label
    Protected WithEvents ddlFiltros As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnImprimir As System.Web.UI.WebControls.Button
    Protected WithEvents ImageButton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Label10 As System.Web.UI.WebControls.Label
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button
    Protected WithEvents Label11 As System.Web.UI.WebControls.Label
    Protected WithEvents Label12 As System.Web.UI.WebControls.Label
    Protected WithEvents Label13 As System.Web.UI.WebControls.Label
    Protected WithEvents DropDownList1 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents TextBox2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label14 As System.Web.UI.WebControls.Label
    Protected WithEvents Label17 As System.Web.UI.WebControls.Label
    Protected WithEvents lblFatura As System.Web.UI.WebControls.Label
    Protected WithEvents lblCompetencia As System.Web.UI.WebControls.Label
    Protected WithEvents grdSegurados As System.Web.UI.WebControls.DataGrid
    Protected WithEvents pnlFiltro As System.Web.UI.WebControls.Panel
    Protected WithEvents lblTituloFatura As System.Web.UI.WebControls.Label
    Protected WithEvents ddlFiltroSegurados As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnFiltroSegurados As System.Web.UI.WebControls.Button
    Protected WithEvents grdFaturas As System.Web.UI.WebControls.DataGrid
    Protected WithEvents chkImpFatura As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkImpBoletos As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkImpRelacao As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblFiltro As System.Web.UI.WebControls.Label
    Protected WithEvents lblPesquisar As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim dependentesfalta As Integer
    Dim dependentesinicio As Integer
    Dim TotalSegurados As Integer

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            dependentesfalta = 0
            dependentesinicio = 0

            EscreverEscondeLinhas()
            Alianca.Seguranca.BancoDados.cCon.ConnectionTimeout = Alianca.Seguranca.BancoDados.cCon.TimeTimeout.Infinito

            Session("dtFatura") = Nothing

            lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Fun��o: Imprimir/Consultar Faturas Pendentes"

            Dim visualizar As String = Request.QueryString("visualizar")

            'Me.btnFiltroSegurados.Attributes.Add("onclick", "top.exibeaguarde();")

            If visualizar <> "" Then
                Me.lblFiltro.Text = ddlFiltroSegurados.SelectedItem.Text

                lblFatura.Text = Request.QueryString("fatura")
                Session("fatura") = Request.QueryString("fatura")
                Session("fatura_subgrupo") = Request.QueryString("subgrupo")
                'lblCompetencia.Text = Request.QueryString("competencia")
                grdFaturaDataBind(Request.QueryString("page"))
                grdSeguradosDataBind()
                grdFaturas.Visible = True
                grdSegurados.Visible = True
                pnlDetalhesFatura.Visible = True

                'btnImprimir.Visible = False
                FormaPgto()
                EscreverImprimir()

            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message)
            'End If
        End Try

    End Sub

    Private Function getTpAcesso() As String
        Dim ind_acesso As String = ""

        Dim valor As String = ""
        valor = Session("acesso")

        Select Case valor
            Case "1"
                ind_acesso = "Administrador da Alian�a do Brasil"
            Case "2"
                ind_acesso = "Administrador de Ap�lice"
            Case "3"
                ind_acesso = "Administrador de Subgrupo"
            Case "4"
                ind_acesso = "Operador"
        End Select

        Return ind_acesso
    End Function

    Private Function VaidaData(ByVal sValor As String) As Boolean
        Dim mes As String
        Dim ano As String
        Dim today As DateTime = Now()

        VaidaData = False

        mes = Left(sValor, 2)
        ano = Right(sValor, 4)

        If mes <> "" And ano <> "" Then
            If mes >= 1 And mes <= 12 Then
                If ano > 1899 Then
                    VaidaData = True
                End If
            End If
        End If

    End Function

    Private Sub grdFaturas_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdFaturas.ItemDataBound

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            If e.Item.ItemType = ListItemType.Item Then
                e.Item.Attributes.Add("onmouseout", "this.style.background='#ffffff'")
            Else
                e.Item.Attributes.Add("onmouseout", "this.style.background='#f5f5f5'")
            End If

            e.Item.Attributes.Add("onclick", "location.href='?visualizar=1&amp;fatura=" & e.Item.Cells(0).Text & _
                "&amp;competencia=" & e.Item.Cells(1).Text & "&amp;Page=" & grdFaturas.CurrentPageIndex & _
                "&amp;SF=" & e.Item.Cells(5).Text & "&amp;subgrupo=" & e.Item.Cells(6).Text & _
                "&amp;SLinkSeguro=" & Request("SLinkSeguro") & "'")
            e.Item.Attributes.Add("onmouseover", "this.style.background='#E8F1FF'")

            'Response.Write("<script>top.exibeaguarde();</script>")

            e.Item.Attributes.Add("Style", "CURSOR: pointer")

        End If

    End Sub

    Private Sub grdFaturas_PageIndexChanged(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grdFaturas.PageIndexChanged

        Try
            grdFaturaDataBind(e.NewPageIndex)
        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message)
            'End If
        End Try

    End Sub

    Private Sub grdFaturaDataBind(Optional ByVal page As Integer = 0)

        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        Dim dt As New DataTable

        Try
            bd.ConsultaFaturasPendentes(Session("apolice"), Session("ramo"))
            dt = bd.ExecutaSQL_DT()
            Session("dtFatura") = dt
            grdFaturas.DataSource = dt
            grdFaturas.DataBind()
            pnlDetalhesFatura.Visible = False
            If page < grdFaturas.PageCount And page > -1 Then
                grdFaturas.CurrentPageIndex = page
                grdFaturas.DataBind()
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message + bd.SQL)
            'End If
        End Try

    End Sub

    Private Sub grdSeguradosDataBind(Optional ByVal paginando As Boolean = False)

        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        Dim dt As New DataTable

        Dim i As Integer
        Dim j As Integer
        Dim paginaAnterior As Integer
        Dim subgrupo As String

        Try
            grdSegurados.PageSize = 10

            subgrupo = IIf(IsNothing(Session("fatura_subgrupo")), "", Session("fatura_subgrupo"))
            bd.ConsultarSegurados(Session("apolice"), Session("ramo"), Convert.ToInt32(subgrupo), Convert.ToInt32(lblFatura.Text), ddlFiltroSegurados.SelectedItem.Value)
            dt = bd.ExecutaSQL_DT()
            grdSegurados.DataSource = dt
            TotalSegurados = dt.Rows.Count

            paginaAnterior = grdSegurados.CurrentPageIndex
            grdSegurados.CurrentPageIndex = 0
            grdSegurados.DataBind()
            If paginaAnterior < grdSegurados.PageCount And paginaAnterior > -1 Then
                grdSegurados.CurrentPageIndex = paginaAnterior
                grdSegurados.DataBind()
                If dependentesinicio > 0 Then
                    dt.Rows.Remove(dt.Rows(0))
                    dt.AcceptChanges()
                    grdSegurados.DataSource = dt
                    dependentesfalta = 0
                    dependentesinicio = 0
                    grdSegurados.DataBind()
                End If
                Do While dependentesfalta > 0
                    If paginaAnterior > 0 Then
                        For i = 1 To paginaAnterior * dependentesfalta
                            Dim dtr As DataRow = dt.NewRow
                            For j = 0 To dt.Rows(0).ItemArray.Length - 1
                                dtr(j) = dt.Rows(0).Item(j)
                            Next
                            dt.Rows.InsertAt(dtr, 0)
                        Next
                        dt.AcceptChanges()
                    End If
                    grdSegurados.PageSize = grdSegurados.PageSize + dependentesfalta
                    grdSegurados.DataSource = dt
                    dependentesfalta = 0
                    dependentesinicio = 0
                    grdSegurados.DataBind()
                Loop
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message + bd.SQL)
            'End If
        End Try

    End Sub

    Private Sub grdSegurados_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grdSegurados.PageIndexChanged

        Try
            grdSegurados.CurrentPageIndex = e.NewPageIndex
            grdSeguradosDataBind(True)
        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message)
            'End If
        End Try

    End Sub

    Private Sub grdSegurados_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdSegurados.ItemDataBound

        Dim i As Integer
        Dim AbreFecha As New HtmlImage
        Dim lbl As New Label

        AbreFecha.Src = "images\imgmenos.gif"

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            e.Item.Cells(6).Text = CLng(e.Item.Cells(6).Text).ToString("#,###,###,###,##0.00") '.Replace(",", "#").Replace(".", ",").Replace("#", ".")
            e.Item.Cells(7).Text = CLng(e.Item.Cells(7).Text).ToString("#,###,###,###,##0.00")

            e.Item.ID = "Linha" & e.Item.ItemIndex

            If e.Item.ItemIndex = 0 And e.Item.Cells(2).Text.ToUpper <> "TITULAR" Then
                dependentesinicio += 1
            End If

            If e.Item.Cells(2).Text.ToUpper <> "TITULAR" Then
                e.Item.BackColor = Color.WhiteSmoke
                e.Item.Attributes.Add("onmouseout", "this.style.background='F5F5F5'")
                e.Item.Cells(1).Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & e.Item.Cells(1).Text
                dependentesfalta -= 1
            Else

                dependentesfalta = CInt(e.Item.Cells(8).Text)

                If e.Item.ItemIndex > 0 AndAlso grdSegurados.Items(e.Item.ItemIndex - 1).Cells(2).Text.ToUpper <> "TITULAR" Then

                    For i = e.Item.ItemIndex - 1 To 0 Step -1
                        If grdSegurados.Items(i).Cells(2).Text.ToUpper = "TITULAR" Then
                            AbreFecha.ID = "AbreFecha" & i.ToString
                            grdSegurados.Items(i).Attributes.Add("OnClick", "escondeLinhas(" & i + 1 & "," & e.Item.ItemIndex - 1 & ", grdSegurados_Linha" & i.ToString & "_AbreFecha" & i.ToString & ");")
                            lbl.Text = "&nbsp;&nbsp;" & grdSegurados.Items(i).Cells(1).Text
                            grdSegurados.Items(i).Cells(1).Text = String.Empty
                            grdSegurados.Items(i).Cells(1).Controls.Add(AbreFecha)
                            grdSegurados.Items(i).Cells(1).Controls.Add(lbl)
                            Exit For
                        End If
                    Next

                End If

                e.Item.BackColor = Color.White
                e.Item.Attributes.Add("onmouseout", "this.style.background='FFFFFF'")

            End If

            If e.Item.ItemIndex > 1 AndAlso (e.Item.ItemIndex = grdSegurados.PageSize - 1 Or e.Item.ItemIndex = (DirectCast(grdSegurados.DataSource, DataTable).Rows.Count Mod grdSegurados.PageSize) - 1) And e.Item.Cells(2).Text.ToUpper <> "TITULAR" Then

                For i = e.Item.ItemIndex - 1 To 0 Step -1
                    If grdSegurados.Items(i).Cells(2).Text.ToUpper = "TITULAR" Then
                        AbreFecha.ID = "AbreFecha" & i.ToString
                        grdSegurados.Items(i).Attributes.Add("OnClick", "escondeLinhas(" & i + 1 & "," & e.Item.ItemIndex & ", grdSegurados_Linha" & i.ToString & "_AbreFecha" & i.ToString & ");")
                        lbl.Text = "&nbsp;&nbsp;" & grdSegurados.Items(i).Cells(1).Text
                        grdSegurados.Items(i).Cells(1).Text = String.Empty
                        grdSegurados.Items(i).Cells(1).Controls.Add(AbreFecha)
                        grdSegurados.Items(i).Cells(1).Controls.Add(lbl)
                        Exit For
                    End If
                Next

            End If

            e.Item.Attributes.Add("onmouseover", "this.style.background='#E8F1FF'")
            e.Item.Attributes.Add("Style", "CURSOR: pointer")
            e.Item.Cells(e.Item.Cells.Count - 1).HorizontalAlign = HorizontalAlign.Right

        End If

    End Sub

    Private Sub grdFaturas_ItemCreated(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdFaturas.ItemCreated

        Try
            If e.Item.ItemType = ListItemType.Pager Then
                Dim cl As New TableCell

                cl.Text = "Total&nbsp;de&nbsp;Registros:&nbsp;"

                If Not IsNothing(Session("dtFatura")) Then
                    cl.Text &= Session("dtFatura").Rows.Count.ToString()
                End If

                cl.HorizontalAlign = HorizontalAlign.Right
                cl.BorderWidth = Unit.Pixel(0)
                cl.ColumnSpan = 2
                e.Item.Cells(0).ColumnSpan -= 2
                e.Item.Cells(0).BorderWidth = Unit.Pixel(0)
                e.Item.Cells.Add(cl)
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message)
            'End If
        End Try

    End Sub

    Private Sub grdSegurados_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdSegurados.ItemCreated

        Try
            If e.Item.ItemType = ListItemType.Pager Then
                Dim cl As New TableCell
                cl.Text = "Total&nbsp;de&nbsp;Registros:&nbsp;" & TotalSegurados.ToString
                cl.HorizontalAlign = HorizontalAlign.Right
                cl.BorderWidth = Unit.Pixel(0)
                cl.ColumnSpan = 3
                e.Item.Cells(0).ColumnSpan -= 3
                e.Item.Cells(0).BorderWidth = Unit.Pixel(0)
                e.Item.Cells.Add(cl)
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message)
            'End If
        End Try

    End Sub

    Private Sub FormaPgto()
        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        Dim dt As New DataTable

        Try

            'bd.ObtemFormaPagamento(Session("apolice"), Session("ramo"))
            bd.ObtemFormaPagamento(Session("apolice"), Session("ramo"), Session("fatura_subgrupo"))
            dt = bd.ExecutaSQL_DT()

            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item("TP_PGTO") = 1 Then
                    'Caso o forma de pagamento seja D�BITO EM CONTA n�o ir� imprimir o boleto
                    chkImpBoletos.Enabled = False
                End If
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Function EscreverEscondeLinhas() As Boolean

        Dim js As String
        Try
            js = "function escondeLinhas(primeiraLinha, ultimaLinha, imagem)" & vbNewLine
            js &= "{" & vbNewLine
            js &= "     if (eval('grdSegurados_Linha'+primeiraLinha).style.display == 'none')" & vbNewLine
            js &= "     {" & vbNewLine
            js &= "         imagem.src='images\\imgmenos.gif';" & vbNewLine
            js &= "         for (i=primeiraLinha;i<=ultimaLinha;i++)" & vbNewLine
            js &= "         {" & vbNewLine
            js &= "             eval('grdSegurados_Linha'+i).style.display = '';" & vbNewLine
            js &= "         }" & vbNewLine
            js &= "     }" & vbNewLine
            js &= "     else" & vbNewLine
            js &= "     {" & vbNewLine
            js &= "         imagem.src='images\\imgmais.gif';" & vbNewLine
            js &= "         for (i=primeiraLinha;i<=ultimaLinha;i++)" & vbNewLine
            js &= "         {" & vbNewLine
            js &= "             eval('grdSegurados_Linha'+i).style.display = 'none';" & vbNewLine
            js &= "         }" & vbNewLine
            js &= "     }" & vbNewLine
            js &= "}" & vbNewLine
            cUtilitarios.escreveScript(js)
        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message)
            'End If
        End Try

    End Function

    Private Function EscreverImprimir() As Boolean

        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        Dim Dt As New DataTable
        Dim Caminho As String

        Dim js As String

        Try


            '---Rotina incluida para buscar o num_cobranca para gera��o do boleto
            '---Flow 3717553 - 
            '---Douglas - Confitec 13.05.2010
            Dim Cobranca As Integer

            bd.Obtem_NumCobranca(Session("apolice"), Session("ramo"), Me.lblFatura.Text)
            Dt = bd.ExecutaSQL_DT()

            If Dt.Rows.Count > 0 Then
                Cobranca = Dt.Rows(0).Item("Num_Cobranca")
            End If
            '---


            js = "function imprimir()" & vbNewLine
            js &= "{" & vbNewLine

            bd.ImprimeBoleto(Session("apolice"), Session("ramo"))
            Dt = bd.ExecutaSQL_DT()

            js &= "     if(document.getElementById('chkImpFatura').checked)" & vbNewLine

            Caminho = "ImprimirFatura.aspx?SLinkSeguro=" & Request("SLinkSeguro")

            js &= "         abre_janela('" & Caminho & "','janela_Fatura','no','yes','750','500','10','10');" & vbNewLine
            js &= "     if(document.getElementById('chkImpBoletos').checked)" & vbNewLine

            If Not IsNothing(Request("SF")) AndAlso Request("SF").ToUpper = "PENDENTE" Then

                ' Tratamento Https 
                Dim Https As String = ""
                If Request.ServerVariables("HTTPS") = "off" Then
                    Https = "http://"
                Else
                    Https = "https://"
                End If

                Caminho = Https & Request.ServerVariables("SERVER_NAME") & "/internet/serv/boleto/impressao/boleto.asp?proposta="
                Caminho &= Dt.Rows(0).Item(0).ToString()
                Caminho &= "&cobranca="
                '---
                'Caminho &= Me.grdFaturas.DataKeys.Item(0)     'lblFatura.Text
                Caminho &= Cobranca
                '---
                Caminho &= "&SLinkSeguro=" & Request("SLinkSeguro") 'Vulnerabilidade 2020 - inclus�o LinkSeguro

                js &= "         abre_janela('" & Caminho & "','janela_boleto','no','yes','750','500','20','20');" & vbNewLine

            Else

                js &= "         alert('Esta Fatura n�o est� pendente.');" & vbNewLine

            End If
            js &= "     if(document.getElementById('chkImpRelacao').checked)" & vbNewLine

            Caminho = "ImprimirRelacaoVidas.aspx?SLinkSeguro=" & Request("SLinkSeguro")

            js &= "         abre_janela('" & Caminho & "','janela_Vidas','no','yes','750','500','30','30');" & vbNewLine
            js &= "}" & vbNewLine
            cUtilitarios.escreveScript(js)
        Catch ex As Exception
            Dim excp As New clsException(ex)
            'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
            'cUtilitarios.MensagemErro(ex.Message + bd.SQL)
            'End If
        End Try

    End Function

    Private Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Not pnlDetalhesFatura.Visible Then
            grdFaturaDataBind(grdFaturas.CurrentPageIndex)
            grdFaturas.Visible = True
            pnlDetalhesFatura.Visible = False
        End If
    End Sub
End Class
