<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ImprimirFatura.aspx.vb" EnableViewStateMac="true" Inherits="SEGW0072.ImprimiFatura" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML xmlns:o>
	<HEAD>
		<title>ImprimirFatura</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
			<SCRIPT language="javascript">
		<!--
		function abre_janela(link,nome,resizable,scrollbars,width,height,top,left)
			{  
			nova=window.open(link,nome,
							"resizable="+resizable+",scrollbars="+scrollbars+",width="+width+",height="+height+",top="+top+",left="+left+",toolbar=0,location=0,directories=0,status=0,menubar=0");
								}
		-->
			</SCRIPT>
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<table id="Table1" style="BORDER-COLLAPSE: collapse" borderColor="#cccccc" cellSpacing="0"
			cellPadding="2" width="650" align="center" border="0">
			<tr>
				<td>
					<form id="Form1" method="post" runat="server">
						<P>
							<table class="sem-sublinhado" cellSpacing="0" cellPadding="0" width="650" border="0">
								<tr>
									<td><font size="3"><b>Brasilseg Companhia De Seguros</b></font></td>
									<td align="right" CssClass="Caminhotela"><font size="1"><b>Fatura de Pr�mios</b></font></td>
								</tr>
								<tr>
									<td><font size="2"><b>Sistema de Seguros - SEGBR</b></font></td>
								</tr>
							</table>
							<BR>
							<BR>
							<table id="Table2" style="BORDER-COLLAPSE: collapse; TEXT-ALIGN: center" borderColor="#cccccc"
								cellSpacing="0" cellPadding="2" width="650" border="0">
								<tr>
									<td align="left" width="74" height="16">Estipulante</td>
									<td align="left" width="313" height="16"><asp:label class="table" id="lblEstipulante" runat="server">Estipulante</asp:label></td>
									<td align="left" width="58" height="16">CNPJ</td>
									<td align="left" width="205" height="16">
									<asp:label class="table" id="lblCnpj1" runat="server">CNPJ</asp:label>
									<asp:label class="table" id="lblCPF1" runat="server" Visible="False">CPF</asp:label></td>
								</tr>
								<tr>
									<td align="left" width="74">SubGrupo</td>
									<td align="left" width="313"><asp:label class="table" id="lblSubGrupo" runat="server">SubGrupo</asp:label></td>
									<td align="left" width="58">CNPJ</td>
									<td align="left" width="205">
									<asp:label class="table" id="lblCnpj2" runat="server">CNPJ</asp:label>
									<asp:label class="table" id="lblCPF2" runat="server" Visible="False">CPF</asp:label>
									</td>
								</tr>
								<tr>
									<td align="left" width="74">SubEstip</td>
									<td align="left" width="313"><asp:label class="table" id="lblSubEstip" runat="server">SubEstip</asp:label></td>
									<td align="left" width="58">Ramo</td>
									<td align="left" width="205"><asp:label class="table" id="lblRamo" runat="server">Ramo</asp:label></td>
								</tr>
								<tr>
									<td align="left" width="74">Sucursal</td>
									<td align="left" width="313"><asp:label class="table" id="lblSucursal" runat="server">Sucursal</asp:label></td>
									<td align="left" width="58">Fatura</td>
									<td align="left" width="205"><asp:label class="table" id="lblFatura" runat="server">Fatura</asp:label></td>
								</tr>
								<tr>
									<td align="left" width="74">Ap�lice</td>
									<td align="left" width="313"><asp:label class="table" id="lblApolice" runat="server">Apolice</asp:label></td>
									<td align="left" width="58">Vig�ncia</td>
									<td align="left" width="205"><asp:label class="table" id="lblVigencia" runat="server">Vigencia</asp:label></td>
								</tr>
								<tr>
								    <td align="left" width="74">Endosso</td>
									<td align="left" width="313"><asp:label class="table" id="lblEndosso" runat="server">Endosso</asp:label></td>
								</tr>
								<tr>
									<td align="left" width="74">Moeda</td>
									<td align="left" width="313"><asp:label class="table" id="lblMoeda" runat="server">Moeda</asp:label></td>
									<td align="left" width="58">Dt. Vencto.</td>
									<td align="left" width="205"><asp:label class="table" id="lblDtVencto" runat="server">DtVencto</asp:label></td>
								</tr>
								<tr>
									<td align="left" width="74">Dt. Emiss�o</td>
									<td align="left" width="313"><asp:label class="table" id="lblDtEmissao" runat="server">DtEmissao</asp:label></td>
									<td align="left" width="58">&nbsp;</td>
									<td align="left" width="205">&nbsp;</td>
								</tr>
								<tr>
									<td align="left" width="74">Tipo Pgto.</td>
									<td align="left" width="313"><asp:label class="table" id="lblTipoPgto" runat="server">TipoPgto</asp:label></td>
									<td align="left" width="58">&nbsp;</td>
									<td align="left" width="205">
										<P align="right">&nbsp;<INPUT class="Botao" id="btnImprimir" style="WIDTH: 70px" onclick="JavaScript:imprimir();"
												type="button" value="Imprimir" name="btnImprimir"></P>
									</td>
								</tr>
								<tr>
									<td colSpan="4">
										<hr>
									</td>
								</tr>
								<tr>
									<td align="left" width="74">Corretor(es)</td>
									<td align="left" width="465" colSpan="3"><asp:label class="table" id="lblCorretor" runat="server">Corretor</asp:label></td>
								</tr>
							</table>
							<hr>
							<table id="Table2" style="BORDER-COLLAPSE: collapse; TEXT-ALIGN: center" borderColor="#cccccc"
								cellSpacing="0" cellPadding="2" width="650" border="0">
								<tr class="titulo-tabela sem-sublinhado">
									<td align="left" width="133" height="17">Hist�rico</td>
									<td align="right" width="179" height="17">Vidas</td>
									<td align="right" width="205" height="17">Capital Segurado</td>
									<td align="right" height="17">Pr�mio L�quido</td>
								</tr>
								<tr bgColor="#e8f1ff">
									<td align="left" width="133">Saldo Anterior</td>
									<td align="right" width="179"><asp:label class="table" id="lblVidasSaldoAnt" runat="server"></asp:label></td>
									<td align="right" width="205"><asp:label class="table" id="lblCapitalSaldoAnt" runat="server"></asp:label></td>
									<td align="right"><asp:label class="table" id="lblPremioSaldoAnt" runat="server"></asp:label></td>
								</tr>
								<tr>
									<td align="left" width="133">Inclus�es</td>
									<td align="right" width="179"><asp:label class="table" id="lblVidasInc" runat="server"></asp:label></td>
									<td align="right" width="205"><asp:label class="table" id="lblCapitalInc" runat="server"></asp:label></td>
									<td align="right"><asp:label class="table" id="lblPremioInc" runat="server"></asp:label></td>
								</tr>
								<tr bgColor="#e8f1ff">
									<td align="left" width="133">Exclus�es</td>
									<td align="right" width="179"><asp:label class="table" id="lblVidasExc" runat="server"></asp:label></td>
									<td align="right" width="205"><asp:label class="table" id="lblCapitalExc" runat="server"></asp:label></td>
									<td align="right"><asp:label class="table" id="lblPremioExc" runat="server"></asp:label></td>
								</tr>
								<tr>
									<td align="left" width="133">Altera��es de Capital</td>
									<td align="right" width="179"><asp:label class="table" id="lblVidasAlt" runat="server"></asp:label></td>
									<td align="right" width="205"><asp:label class="table" id="lblCapitalAlt" runat="server"></asp:label></td>
									<td align="right"><asp:label class="table" id="lblPremioAlt" runat="server"></asp:label></td>
								</tr>
								<tr bgColor="#e8f1ff">
									<td align="left" width="133">Acertos</td>
									<td align="right" width="179"><asp:label class="table" id="lblVidasAcerto" runat="server"></asp:label></td>
									<td align="right" width="205"><asp:label class="table" id="lblCapitalAcerto" runat="server"></asp:label></td>
									<td align="right"><asp:label class="table" id="lblPremioAcerto" runat="server"></asp:label></td>
								</tr>
								<tr>
									<td align="left" width="133">Cobertura Provis�ria</td>
									<td align="right" width="179"><asp:label class="table" id="lblVidasCob" runat="server"></asp:label></td>
									<td align="right" width="205"><asp:label class="table" id="lblCapitalCob" runat="server"></asp:label></td>
									<td align="right"><asp:label class="table" id="lblPremioCob" runat="server"></asp:label></td>
								</tr>
								<tr bgColor="#e8f1ff">
									<td align="left" width="133">Saldo Atual</td>
									<td align="right" width="179"><asp:label class="table" id="lblVidasSaldoAtual" runat="server"></asp:label></td>
									<td align="right" width="205"><asp:label class="table" id="lblCapitalSaldoAtual" runat="server"></asp:label></td>
									<td align="right"><asp:label class="table" id="lblPremioSaldoAtual" runat="server"></asp:label></td>
								</tr>
								<tr>
									<td align="left" width="133">Inclus�es n�o Processadas</td>
									<td align="right" width="179"><asp:label class="table" id="lblVidasInclPendente" runat="server"></asp:label></td>
									<td align="right" width="205"><asp:label class="table" id="lblCapitalInclPendente" runat="server"></asp:label></td>
									<td align="right"><asp:label class="table" id="lblPremioInclPendente" runat="server"></asp:label></td>
								</tr>
								<tr bgColor="#e8f1ff">
									<td align="left" width="133">Altera��es n�o Processadas</td>
									<td align="right" width="179"><asp:label class="table" id="lblVidasAltPendente" runat="server"></asp:label></td>
									<td align="right" width="205"><asp:label class="table" id="lblCapitalAltPendente" runat="server"></asp:label></td>
									<td align="right"><asp:label class="table" id="lblPremioAltPendente" runat="server"></asp:label></td>
								</tr>
								<tr>
									<td align="left" width="133">Recusas</td>
									<td align="right" width="179"><asp:label class="table" id="lblVidasRecusas" runat="server"></asp:label></td>
									<td align="right" width="205"><asp:label class="table" id="lblCapitalRecusas" runat="server"></asp:label></td>
									<td align="right"><asp:label class="table" id="lblPremioRecusas" runat="server"></asp:label></td>
								</tr>
								<tr>
									<td colSpan="4">
										<hr>
									</td>
								</tr>
								<tr>
									<td align="left" width="133">Pr�mio L�quido</td>
									<td align="right" width="179"><asp:label class="table" id="lblPremioLiq" runat="server"></asp:label></td>
									<td width="205">&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr bgColor="#e8f1ff">
									<td align="left" width="133" height="16">IOF</td>
									<td align="right" width="179" height="16"><asp:label class="table" id="lblIof" runat="server"></asp:label></td>
									<td width="205" height="16">&nbsp;</td>
									<td height="16">&nbsp;</td>
								</tr>
								<tr>
									<td align="left" width="133" height="15">Pr�mio Total</td>
									<td align="right" width="179" height="15"><asp:label class="table" id="lblPremioTotal" runat="server"></asp:label></td>
									<td width="205" height="15">&nbsp;</td>
									<td height="15">&nbsp;</td>
								</tr>
								<tr>
									<td colSpan="4">
										<hr>
									</td>
								</tr>
								<tr bgColor="#e8f1ff">
									<td align="left" width="133">Observa��es</td>
									<td align="left" colSpan="3"><asp:label class="table" id="lblObs" runat="server"></asp:label></td>
								</tr>
							</table>
							<hr>
							<table width="650" border="0">
								<tr>
									<td align="right"><b>Valores expressos em R$</b></td>
								</tr>
								<tr>
									<td align=center>
									        Brasilseg Companhia de Seguros - C�digo SUSEP N� 06785 - CNPJ 28.196.889/0001-43 <br />
                                            SAC � Servi�o de Atendimento ao Cliente (24 horas, 7 dias por semana) <br /> 
											0800 729 7000 - https://www.bbseguros.com.br/  <br />
                                            Av. das Na��es Unidas, 14.261, Ala A�Vila Gertrudes �S�o Paulo�SP�CEP 04794-000  <br />
										
									</td>
								</tr>
							</table>
					</form>
				</td>
			</tr>
		</table>
		<Script>
	function imprimir()
	{
		document.getElementById("btnImprimir").style.display = 'none';
		print();
		document.getElementById("btnImprimir").style.display = '';
	}
		</Script>
	</body>
</HTML>
