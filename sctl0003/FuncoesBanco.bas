Attribute VB_Name = "funcoesBanco"
Option Explicit

'local variable(s) to hold property value(s)
Private msg_erro As String
Private IND_CARGA As String
Private Erro As String
Private NumeroErro As String
Private MetodoErro As String
Private USUARIO As String
Private SQLERRO As String
Public WF_ID As String
Public ambiente As String

Const eChave = "5267"

Public Property Get p_msg_erro() As String
    p_msg_erro = msg_erro
End Property

Public Property Let p_msg_erro(ByVal valor As String)
    msg_erro = valor
End Property

Public Property Let pIND_CARGA(ByVal vData As String)
    IND_CARGA = vData
End Property
Public Property Get pIND_CARGA() As String
    pIND_CARGA = IND_CARGA
End Property

Public Property Get pMetodoErro() As String
    pMetodoErro = MetodoErro
End Property
Public Property Let pMetodoErro(ByVal vData As String)
    MetodoErro = vData
End Property

Public Property Get pNumeroErro() As String
    pNumeroErro = NumeroErro
End Property
Public Property Let pNumeroErro(ByVal vData As String)
    NumeroErro = vData
End Property

Public Property Let pUSUARIO(ByVal vData As String)
    USUARIO = vData
End Property
Public Property Get pUSUARIO() As String
    pUSUARIO = USUARIO
End Property

Public Function mDevolveZeros(ByVal sValor As String, ByVal iMaximo As Integer) As String
    Dim sZeros As String
    sZeros = ""
    Dim intLoop As Integer
    For intLoop = Len(sValor) To iMaximo - 1
        sZeros = sZeros & "0"
    Next
    mDevolveZeros = sZeros & sValor
End Function

Public Function mTrataNullNumero(ByVal strValor As String) As String
    If Trim(strValor) = "" Then
        mTrataNullNumero = "null"
    Else
        mTrataNullNumero = Replace(Replace(strValor, ".", ""), ",", ".")
    End If
End Function

Public Function mTrataNullTexto(ByVal strValor As String) As String
    If Trim(strValor) = "" Then
        mTrataNullTexto = "null"
    Else
        mTrataNullTexto = "'" & Replace(strValor, "'", "''") & "'"
    End If
End Function

Public Sub GuardaErro(ByVal sErro As String, ByVal StrSQL As String)
    If sErro <> "" Then
        If Erro = "" Then
            Erro = sErro
        Else
            Erro = Erro & "<br>(" & vbCrLf & sErro & ") -- (" & StrSQL & ")" & "<br>" & vbCrLf
        End If
        Dim guardaValores() As String
        Dim intLoop As Integer
        guardaValores = Split(Erro, "<br>" & vbCrLf)
        If UBound(guardaValores) > 80 Then
            Erro = ""
            For intLoop = 2 To UBound(guardaValores)
            Erro = Erro & "<br>" & vbCrLf & guardaValores(intLoop)
            Next
        End If
    End If
End Sub

Public Function RetornaErro()
        RetornaErro = Erro
End Function

Public Sub ApagaErro()
        Erro = ""
End Sub

Public Sub GuardaSQLErro(ByVal sSQLErro As String)
    If SQLERRO = "" Then
        SQLERRO = sSQLErro
    Else
        SQLERRO = SQLERRO & "<br>" & vbCrLf & sSQLErro
    End If
    Dim guardaValores() As String
    Dim intLoop As Integer
    guardaValores = Split(SQLERRO, "<br>" & vbCrLf)
    If UBound(guardaValores) > 80 Then
        SQLERRO = ""
        For intLoop = 2 To UBound(guardaValores)
        SQLERRO = SQLERRO & "<br>" & vbCrLf & guardaValores(intLoop)
        Next
    End If
End Sub

Public Function RetornaSQLErro()
        RetornaSQLErro = SQLERRO
End Function

Public Sub ApagaSQLErro()
        SQLERRO = ""
End Sub

Public Function mTrataNullData(ByVal strValor As String) As String
    If Trim(strValor) = "" Or Trim(strValor) = "//" Then
        mTrataNullData = "null"
    Else
        Dim dataParte() As String
        Dim hora As String
        hora = ""
        dataParte = Split(strValor, " ")
        If UBound(dataParte) > 0 Then
            hora = " " & dataParte(1)
        End If
        dataParte = Split(dataParte(0), "/")
        strValor = dataParte(2) & "/" & dataParte(1) & "/" & dataParte(0) & hora
        mTrataNullData = "'" & strValor & "'"
    End If
End Function
Function OpenCx() As ADODB.Connection
On Error GoTo ErrOpenCx
    
    Dim StrConex As String

    Set OpenCx = New ADODB.Connection
    

    StrConex = StringBancoAB_ABS(p_ambiente)
    
    OpenCx.ConnectionString = StrConex
    OpenCx.CommandTimeout = 0
    OpenCx.CursorLocation = adUseClient
    OpenCx.Open

Exit Function

ErrOpenCx:
    If p_msg_erro = "" Then
        p_msg_erro = "<br>ERRO: OpenCx - " & Err.Description
    End If
End Function

Function StringBancoAB_ABS(ByVal ambiente As Integer) As String
On Error GoTo ErrStringBancoAB_ABS

      Dim conex As ADODB.Connection
      Dim posicao As String
      
      posicao = "criando objeto conex�o"
      Set conex = New ADODB.Connection
      
      posicao = "definindo timeout"
      conex.CommandTimeout = 0
      Dim StrConex As String
      Dim SQL As String
      Dim rs_siscot As Recordset
      Dim TrataABS As String
      Dim BD_banco As String
      Dim BD_Senha As String
      Dim BD_Servidor As String
      Dim BD_Usuario As String
      
      posicao = "atribuindo string de conex�o"
      StrConex = "Provider=SQLOLEDB.1;User ID=SISBR;Password=SISBRY2K;Initial Catalog=web_seguros_db;Data Source=SISAB003;Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Workstation ID=WEB;Use Encryption for Data=False;Tag with column collation when possible=False"
      conex.ConnectionString = StrConex
      conex.CursorLocation = adUseClient
      
      posicao = "abrindo conex�o"
      conex.Open
      
      posicao = "montando Select"
      SQL = "select valor from controle_sistema_db..parametro_tb where sigla_sistema = 'SISCOT' and secao = 'INTERNET' and ambiente_id = " & ambiente & " and campo in('banco', 'servidor', 'usuario', 'senha') order by campo"
      
      posicao = "executando Select"
      Set rs_siscot = conex.Execute(SQL)
              
      posicao = "lendo variaveis"
      BD_banco = rs_siscot(0)
      rs_siscot.MoveNext
      BD_Senha = rs_siscot(0)
      rs_siscot.MoveNext
      BD_Servidor = rs_siscot(0)
      rs_siscot.MoveNext
      BD_Usuario = rs_siscot(0)
        
      posicao = "montando retorno"
      StrConex = "Provider=SQLOLEDB.1;Password=" & BD_Senha & ";User ID=" & BD_Usuario & ";Initial Catalog=" & BD_banco & ";Data Source=" & BD_Servidor & ";Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Workstation ID=WEB;Use Encryption for Data=False;Tag with column collation when possible=False"
            
      posicao = "retornando"
      StringBancoAB_ABS = StrConex
    Exit Function

ErrStringBancoAB_ABS:

    p_msg_erro = "<br>ERRO: StringBancoAB_ABS - " & posicao & " - " & Err.Description

End Function


Public Property Get p_ambiente() As Integer
    p_ambiente = ambiente
End Property
Public Property Let p_ambiente(ByVal vData As Integer)
    ambiente = vData
End Property

Public Function ExecutarSQL(ByVal StrSQL As String) As ADODB.Recordset
    Dim Rs As New ADODB.Recordset
    Dim Cx As New ADODB.Connection
    Set Cx = OpenCx()
    GuardaSQLErro StrSQL
    Rs.Open StrSQL, Cx
    Rs.ActiveConnection = Nothing
    Set ExecutarSQL = Rs
    Set Cx = Nothing
    Set Rs = Nothing
End Function

Public Function Verifica_acesso() As Boolean
    'Dim seguranca As Object
    'Set seguranca = CreateObject("WFL0004.cls00227")
    'seguranca.p_ambiente = p_ambiente
    'seguranca.verifica_ambiente
    'Verifica_acesso = seguranca.p_valido
    'p_msg_erro = seguranca.p_mensagem
    'Set seguranca = Nothing
    Verifica_acesso = True
End Function

Public Sub ExecutarSQLTrans(ByVal StrSQL As String, ByRef objConection As Object) 'objConection = SCTL0002.cls00141
    GuardaSQLErro StrSQL
    objConection.ExecutaSQL_ALS StrSQL
End Sub
