Public Class ImplementarSABL0101LinkSeguro

    Inherits System.Web.UI.Page

    Protected linkseguro As New Alianca.Seguranca.Web.LinkSeguro

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then

            Try

                For Each m As String In Request.QueryString.Keys
                    Session(m) = Request.QueryString(m)
                Next

            Catch ex As Exception

                Dim excp As New clsException(ex)
                'If Not (Alianca.Seguranca.Erro.TrataErro(ex) = String.Empty) Then
                'cUtilitarios.MensagemErro(ex.Message)
                'End If
            End Try
        End If

        Implementar_LinkSeguro_SABL0101(linkseguro)

        Session("usuario_id") = linkseguro.Usuario_ID
        Session("usuario") = linkseguro.Login_REDE
        Session("cpf") = linkseguro.CPF

    End Sub

    Private Sub Implementar_LinkSeguro_SABL0101(ByRef ls As Alianca.Seguranca.Web.LinkSeguro, Optional ByVal pagina As String = "")

        Dim bd As New cConsultarFaturas

        If Not bd.ValidarLinkSeguro(ls, Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"), pagina) Then
            'Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
        End If

        bd.ConfiguraConexaoSABL0101(Request.ServerVariables("SERVER_NAME"))

    End Sub

End Class
