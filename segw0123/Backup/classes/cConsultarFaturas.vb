
Public Class cConsultarFaturas

#Region "Heran�a"
    Inherits cBanco
#End Region

    Public Sub New(Optional ByVal banco As cDadosBasicos.eBanco = cDadosBasicos.eBanco.web_seguros_db)
        MyBase.New(banco)
        Alianca.Seguranca.BancoDados.cCon.BancodeDados = banco.ToString
    End Sub

    Function trInt(ByVal valor As Object) As String

        If valor = 0 Then
            Return "null"
        Else
            Return valor
        End If

    End Function

    ''' <summary>
    ''' 27/12/2018 - Demanda MU-2018-070495 - Cleiton Queiroz - Confitec SP 
    ''' </summary>
    ''' <param name="apolice_id"></param>
    ''' <param name="ramo_id"></param>
    ''' <param name="fatura_id"></param>
    ''' <remarks></remarks>
    Public Sub Obtem_NumCobranca(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal fatura_id As Integer)

        SQL = " Select agendamento_cobranca_tb.num_cobranca " & _
              " FROM SISAB003.seguros_db.dbo.agendamento_cobranca_tb agendamento_cobranca_tb (nolock)" & _
              " WHERE agendamento_cobranca_tb.apolice_id = " & apolice_id & _
              " AND agendamento_cobranca_tb.ramo_id  = " & ramo_id & _
              " AND agendamento_cobranca_tb.fatura_id = " & fatura_id
    End Sub

    ''' <summary>
    '''  27/12/2018 - Demanda MU-2018-070495 - Cleiton Queiroz - Confitec SP 
    ''' </summary>
    ''' <param name="apolice_id"></param>
    ''' <param name="ramo_id"></param>
    ''' <remarks></remarks>
    Public Sub ImprimeBoleto(ByVal apolice_id As String, ByVal ramo_id As String)

        SQL = "EXEC SISAB003.SEGUROS_DB.dbo.SEGS5746_SPS @apolice_id = " & apolice_id
        SQL &= "                           , @ramo_id = " & ramo_id

    End Sub


    Public Sub ConsultaCompetencia(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal workflow As Integer, ByVal valor_id As Integer)
        SQL = "EXEC SEGS5696_sps  @apolice_id = " & apolice_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @tp_wf_id = " & workflow
        SQL &= ", @tp_versao_id = " & valor_id
    End Sub

    Public Sub ListaFaturasPendentes(ByVal ramo_id As Integer, ByVal apolice_id As Integer)
        SQL = "EXEC VIDA_WEB_DB..SEGS8093_SPS  @ramo_id = " & ramo_id
        SQL &= ", @apolice_id = " & apolice_id
    End Sub

    Public Sub InsereTemporariaFaturaId(ByVal fatura_id As Integer, ByVal proposta_id As Integer, _
                                        ByVal num_cobranca As Integer, ByVal exibe_fatura As Boolean, _
                                        ByVal exibe_boleto As Boolean, ByVal exibe_rel_vidas As Boolean, ByVal usuario As String)
        SQL = "EXEC SISAB003.seguros_db.dbo.SEGS8096_SPI  @fatura_id = " & fatura_id
        SQL &= ", @proposta_id = " & proposta_id
        SQL &= ", @num_cobranca = " & num_cobranca
        SQL &= ", @exibe_fatura = " & exibe_fatura
        SQL &= ", @exibe_boleto = " & exibe_boleto
        SQL &= ", @exibe_rel_vidas = " & exibe_rel_vidas
        SQL &= ", @usuario = " & usuario
    End Sub

    Public Sub ListaImpressaoFaturas(ByVal apolice_id As Integer, ByVal sucursal_id As Integer, ByVal seguradora_id As Integer, ByVal ramo_id As Integer, ByVal usuario As String)
        SQL = "EXEC SISAB003.seguros_db.dbo.SEGS8097_SPS @apolice_id = " & apolice_id
        SQL &= ", @sucursal_id = " & sucursal_id
        SQL &= ", @seguradora_id = " & seguradora_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @usuario = " & usuario
    End Sub

    Public Sub ListaImpressaoBoletos(ByVal usuario As String, Optional ByVal novoVencimento As String = Nothing)
        SQL = "EXEC SISAB003.seguros_db.dbo.SEGS8098_SPS " & usuario
        If Not IsNothing(novoVencimento) Then
            SQL &= ",'" & novoVencimento & "'"
        End If
    End Sub

    Public Sub ListaImpressaoRelacaoDeVidas(ByVal usuario As String)
        SQL = "EXEC SISAB003.seguros_db.dbo.SEGS8099_SPS " & usuario
    End Sub

    Public Sub ObterCaminhoServidor(ByVal sigla_sistema As String, ByVal secao As String, ByVal campo As String, ByVal ambiente As String)
        SQL = "EXEC SISAB003.controle_sistema_db.dbo.OBTER_PARAMETROS_SPS @SIGLA_SISTEMA = " & sigla_sistema
        SQL &= ", @SECAO = " & secao
        SQL &= ", @CAMPO = " & campo
        SQL &= ", @AMBIENTE = " & ambiente
    End Sub

    Public Sub ListaSelecao(ByVal usuario As String)
        SQL = "EXEC SISAB003.seguros_db.dbo.SEGS8155_SPS " & usuario
    End Sub

    Public Sub DeletaTemporaria(ByVal usuario As String)
        SQL = "EXEC SISAB003.seguros_db.dbo.SEGS8147_SPD " & usuario
    End Sub
        
    Public Sub GravarHistoricoProrrogacao(ByVal proposta_id As String, ByVal data_agendamento As String, _
                                          ByVal val_cobranca As String, ByVal fatura_id As String, ByVal endosso_id As String)
        SQL = "EXEC SISAB003.SEGUROS_DB.dbo.SEGS10482_SPI "
        SQL &= proposta_id & ",'"
        SQL &= data_agendamento & "',"
        SQL &= val_cobranca & ","
        SQL &= fatura_id & ","
        SQL &= endosso_id
    End Sub
        
End Class
