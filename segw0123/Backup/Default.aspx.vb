Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Imports System.Web.Configuration.WebConfigurationManager

Imports System.IO
Imports System.IO.Path
Imports System.Drawing


Partial Public Class _Default
    Inherits System.Web.UI.Page

    'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - in�cio
    Protected WithEvents btnProrrogar As System.Web.UI.WebControls.Button
    'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - fim

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim url As String

        'Try

        '------------------------------------------------------------------------------------------------------
        Dim linkseguro As Alianca.Seguranca.Web.LinkSeguro
        Dim cAmbiente As Alianca.Seguranca.Web.ControleAmbiente

        'Implementa��o da SABL0101
        linkseguro = New Alianca.Seguranca.Web.LinkSeguro
        Dim usuario As String = linkseguro.LerUsuario("Default.aspx", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))
        'If usuario = "" Then
        '    Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
        'End If

        If Not Page.IsPostBack Then
            Session("indice") = 0
            For Each m As String In Request.QueryString.Keys
                If m <> "" Then
                    Session(m) = Request.QueryString(m)
                End If
            Next
        End If

        'VERIFICANDO A SEGURAN�A COM O PAR�METRO GERADO COM OU SEM O NOME DA P�GINA DE DESTINO
        'If linkseguro.LerUsuario(Nothing, Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro")) <> String.Empty OrElse linkseguro.LerUsuario("Default.aspx", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro")) <> String.Empty Then
        'Else
        '    'Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
        '    'Exit Sub
        'End If

        Session("usuario_id") = linkseguro.Usuario_ID
        Session("usuario") = linkseguro.Login_WEB
        Session("nomeUsuario") = linkseguro.Nome
        Session("cpf") = linkseguro.CPF

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
        'url = "http://www.aliancadobrasil.com.br/"
        cAmbiente.ObterAmbiente(url)
        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
        Else
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
        End If

        Session("ambiente_nome") = IIf(cAmbiente.Ambiente = Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade, "QUALIDADE", "PRODUCAO")
        '------------------------------------------------------------------------------------------------------

        lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice")
        lblNavegacao.Text = lblNavegacao.Text & " -> Estipulante: " & Session("estipulante")
        lblNavegacao.Text = lblNavegacao.Text & "<br>Subgrupo: " & Session("nomeSubgrupo")
        lblNavegacao.Text = lblNavegacao.Text & "<br>Fun��o: Imprimir/Consultar Faturas Pendentes da Ap�lice"

        If Not IsPostBack Then
            ListarFaturasPendentes()
            'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - in�cio
            Call EsconderGridProrrogacao()
            'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - fim
            'R.FOUREAUX - ESCONDENDO COLUNA DO BOLETO REGISTRADO
            GridFaturasPendentes.Columns(7).Visible = False
        End If

        'Catch ex As Exception
        'Dim excp As New clsException(ex)
        'End Try

    End Sub

    'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - in�cio
    Private Function CarregarFaturasPendentes() As DataTable
        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)

        bd.ListaFaturasPendentes(Session("ramo"), Session("apolice"))
        CarregarFaturasPendentes = bd.ExecutaSQL_DT()

        Return CarregarFaturasPendentes

    End Function

    Private Sub ListarFaturasPendentes()
        GridFaturasPendentes.DataSource = CarregarFaturasPendentes()
        GridFaturasPendentes.DataBind()
    End Sub
    'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - fim

     Protected Sub btImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btImprimir.Click

        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        Dim v_iTransacao As Integer
        Dim v_bExibeFatura As Boolean
        Dim v_bExibeBoleto As Boolean
        Dim v_bExibeRelVidas As Boolean

        Try

            If chkImpFatura.Checked = True Then
                v_bExibeFatura = True
            Else
                v_bExibeFatura = False
            End If
            If chkImpBoletos.Checked = True Then
                v_bExibeBoleto = True
            Else
                v_bExibeBoleto = False
            End If
            If chkImpRelVidas.Checked = True Then
                v_bExibeRelVidas = True
            Else
                v_bExibeRelVidas = False
            End If

            If chkImpFatura.Checked = False And chkImpBoletos.Checked = False And chkImpRelVidas.Checked = False Then

                ShowMessageBox("Selecione pelo menos uma das op��es para consulta e/ou impress�o.")

            Else

                'Inicio 01/08/2019 - Demanda MU-2018-070495 - Cleiton Queiroz - Confitec SP

                If chkImpBoletos.Checked = True Then ' se tiver selecionado para imprimir boletos

                    Dim flag As Boolean = False

                    For Each v_row As GridViewRow In Me.GridFaturasPendentes.Rows
                        Dim v_oCb As CheckBox = v_row.FindControl("ckSelecionado")

                        If v_oCb.Checked Then
                            flag = True
                            EscreverImprimir(GridFaturasPendentes.DataKeys(v_row.RowIndex).Values("fatura_id").ToString())
                        End If

                    Next

                Else

                    v_iTransacao = bd.IniciaTransacao()

                    Dim flag As Boolean = False

                    bd.DeletaTemporaria(Session("usuario")) 'deleta tempor�ria

                    'Insere fatura_id na temporaria
                    For Each v_row As GridViewRow In Me.GridFaturasPendentes.Rows
                        Dim v_oCb As CheckBox = v_row.FindControl("ckSelecionado")
                        If v_oCb.Checked Then

                            flag = True

                            bd.InsereTemporariaFaturaId(GridFaturasPendentes.DataKeys(v_row.RowIndex).Values("fatura_id").ToString(), _
                                                        GridFaturasPendentes.DataKeys(v_row.RowIndex).Values("proposta_id").ToString(), _
                                                        GridFaturasPendentes.DataKeys(v_row.RowIndex).Values("num_cobranca").ToString(), _
                                                        v_bExibeFatura, v_bExibeBoleto, v_bExibeRelVidas, Session("usuario"))
                            bd.ExecutaSQL_Trans(v_iTransacao)
                        End If
                    Next


                    If flag Then

                        ImprimirPDF(v_iTransacao)

                        bd.DeletaTemporaria(Session("usuario")) 'deleta tempor�ria
                        bd.ExecutaSQL_Trans(v_iTransacao)

                    Else

                        ShowMessageBox("Selecione pelo menos uma fatura para consulta e/ou impress�o.")

                    End If

                    'Fim 01/08/2019 - Demanda MU-2018-070495 - Cleiton Queiroz - Confitec SP
					
					bd.CommitTransacao(v_iTransacao)
					bd = Nothing

                End If               

            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
            bd.RollBackTransacao(v_iTransacao)
            bd = Nothing

            Dim mensagemErro As String = Replace(Replace(ex.Message, vbCrLf, ""), "5 - ExecuteDataset - Erro", "")
            Call ShowMessageBox(mensagemErro)
        End Try
    End Sub
	
	''' <summary>
    ''' 27/12/2018 - Demanda MU-2018-070495 - Cleiton Queiroz - Confitec SP 
    ''' </summary>
    ''' <param name="fatura_id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function EscreverImprimir(ByVal fatura_id As String) As Boolean

        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        Dim Dt As New DataTable
        Dim Caminho As String

        Dim js As String

        Try
            Dim Cobranca As Integer

            bd.Obtem_NumCobranca(Session("apolice"), Session("ramo"), fatura_id)
            Dt = bd.ExecutaSQL_DT()

            If Dt.Rows.Count > 0 Then
                Cobranca = Dt.Rows(0).Item("Num_Cobranca")
            End If

            bd.ImprimeBoleto(Session("apolice"), Session("ramo"))
            Dt = bd.ExecutaSQL_DT()

            'Caminho = "http://localhost/impressao/boleto.asp?proposta="
            Caminho = "http://" & Request.ServerVariables("SERVER_NAME") & "/internet/serv/boleto/impressao/boleto.asp?proposta="
            Caminho &= Dt.Rows(0).Item(0).ToString()
            Caminho &= "&cobranca="
          
            Caminho &= Cobranca
            Caminho &= "&SLinkSeguro=" & Request("SLinkSeguro")

            js = "         abre_janela('" & Caminho & "','janela_boleto','no','yes','750','500','20','20');" & vbNewLine

            Me.ClientScript.RegisterClientScriptBlock(Me.GetType(), "AbreJanela", js, True)
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Function

    Private Sub ImprimirPDF(ByVal v_iTransacao As Integer, Optional ByVal novoVencimento As String = Nothing)
        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        Dim dtGeral As DataTable
        Dim dtFaturas As DataTable
        Dim dtVidas As DataTable
        Dim dtBoletos As DataTable

        Try

        'SELECIONA TEMPORARIA
        bd.ListaSelecao(Session("usuario"))
        dtGeral = bd.ExecutaSQL_DT_Trans(v_iTransacao)

        'FATURA
        bd.ListaImpressaoFaturas(Session("apolice"), 0, 6785, Session("ramo"), Session("usuario"))
        dtFaturas = bd.ExecutaSQL_DT_Trans(v_iTransacao)
        'BOLETO
        bd.ListaImpressaoRelacaoDeVidas(Session("usuario"))
        dtVidas = bd.ExecutaSQL_DT_Trans(v_iTransacao)
        'BOLETO
        bd.ListaImpressaoBoletos(Session("usuario"), novoVencimento)
        dtBoletos = bd.ExecutaSQL_DT_Trans(v_iTransacao)

        If Not IsNothing(novoVencimento) Then
            Dim proposta_id As String = gdvProrrogacao.DataKeys(0).Values("proposta_id").ToString()
            Dim dt_agendamento As String = novoVencimento
            Dim val_cobranca As String = Replace(gdvProrrogacao.Rows(0).Cells(3).Text, ",", ".")
            Dim fatura_id As String = gdvProrrogacao.Rows(0).Cells(0).Text
            Dim endosso_id As String = dtBoletos.Rows(0).ItemArray(3).ToString

            bd.GravarHistoricoProrrogacao(proposta_id, dt_agendamento, val_cobranca, fatura_id, endosso_id)
            bd.ExecutaSQL()
        End If

        Fatura_GeraPDF(dtGeral, dtFaturas, dtVidas, dtBoletos, "rel_geral.rpt", "Relatorio.pdf")

        Catch ex As Exception
            Throw ex
        End Try

    End Sub


    Dim v_oReportDocument As New ReportDocument
    Dim v_oSubReportDocument As New ReportDocument
    Dim v_oSubReportDocument2 As New ReportDocument
    Dim v_oSubReportDocument3 As New ReportDocument

    Private Sub Fatura_GeraPDF(ByVal dtGeral As DataTable, ByVal dtFatura As DataTable, _
                                ByVal dtVidas As DataTable, ByVal dtBoleto As DataTable, _
                                ByVal v_sRPT As String, ByVal v_sNomeArquivo As String)

        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        Dim dtParam As DataTable
        Dim v_sCaminho As String
        Dim url As String

        'Try

        Dim v_sReportPath As String = Server.MapPath("~/RPT/" & v_sRPT)
        v_oReportDocument.Load(v_sReportPath)

        'Abre o sub-relat�rio
        v_oSubReportDocument = v_oReportDocument.OpenSubreport("rel_fatura.rpt")
        v_oSubReportDocument2 = v_oReportDocument.OpenSubreport("rel_vidas.rpt")
        v_oSubReportDocument3 = v_oReportDocument.OpenSubreport("rel_boleto.rpt")

        'Passa o datatable
        v_oReportDocument.SetDataSource(dtGeral)
        v_oSubReportDocument.SetDataSource(dtFatura)
        v_oSubReportDocument2.SetDataSource(dtVidas)
        v_oSubReportDocument3.SetDataSource(dtBoleto)

        Dim crExportOptions As ExportOptions
        Dim crDiskFileDestinationOptions As DiskFileDestinationOptions

        bd.ObterCaminhoServidor("SEGBR", "FATURA_WEB", "CAMINHO", Session("ambiente_nome"))
        dtParam = bd.ExecutaSQL_DT()
        v_sCaminho = dtParam.Rows(0).Item(0).ToString()

        'Dim strPDFTmp As String = Server.MapPath("~\temp_pdf") & "\" & Session.SessionID.ToString & ".pdf" 'TESTE
        Dim strPDFTmp As String = v_sCaminho & "\" & Session.SessionID.ToString & ".pdf" 'PRODU��O

        crDiskFileDestinationOptions = New DiskFileDestinationOptions()
        crDiskFileDestinationOptions.DiskFileName = strPDFTmp
        crExportOptions = v_oReportDocument.ExportOptions

        With crExportOptions

            .DestinationOptions = crDiskFileDestinationOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat

        End With

        v_oReportDocument.Export()

        'escrevendo o pdf para o browser do cliente
        Response.ClearContent()
        Response.ClearHeaders()
        Response.ContentType = "application/pdf"
        'Response.AddHeader("content-disposition", "inline; filename=ReportName.pdf")
        Response.AddHeader("content-disposition", "attachment; filename=" & v_sNomeArquivo)
        Response.WriteFile(strPDFTmp)
        'Response.TransmitFile(strPDFTmp)

        Response.Flush()
        Response.Close()

        'removendo o pdf do servidor
        'System.IO.File.Delete(strPDFTmp)


        'Catch ex As Exception
        'Dim excp As New clsException(ex)
        'End Try
    End Sub

    Public Sub ShowMessageBox(ByVal mensagem As String)

        Dim pagina As Page = TryCast(HttpContext.Current.Handler, Page)

        If pagina IsNot Nothing Then

            mensagem = mensagem.Replace("'", "'")
            Dim script As String = String.Format("alert('{0}');", mensagem)
            pagina.ClientScript.RegisterStartupScript(pagina.[GetType](), "Alerta", script, True)

        End If

    End Sub

    'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - in�cio
#Region "Prorroga��o"

    Private Sub ExibirGridProrrogacao(ByVal btnProrrogarSelecionado As Button, _
                                      ByVal GridRow As GridViewRow)

        Call GerarGridProrrogacao(btnProrrogarSelecionado, GridRow)
        lblProrrogacao.Visible = True
        gdvProrrogacao.Visible = True
        btnConfirmarProrrogacao.Visible = True
    End Sub

    ''' <summary>
    ''' 27/12/2018 - Demanda MU-2018-070495 - Cleiton Queiroz - Confitec SP
    ''' </summary>
    ''' <param name="fatura_id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function Redirecionar(ByVal fatura_id As Integer) As Boolean

        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        Dim Dt As New DataTable
        Dim Caminho As String

        Dim js As String

        Try

            Dim Cobranca As Integer

            bd.Obtem_NumCobranca(Session("apolice"), Session("ramo"), fatura_id)
            Dt = bd.ExecutaSQL_DT()

            If Dt.Rows.Count > 0 Then
                Cobranca = Dt.Rows(0).Item("Num_Cobranca")
            End If

            bd.ImprimeBoleto(Session("apolice"), Session("ramo"))
            Dt = bd.ExecutaSQL_DT()

            'Caminho = "http://localhost/impressao/linhaDigitavel.asp?proposta="
            Caminho = "http://" & Request.ServerVariables("SERVER_NAME") & "/internet/serv/boleto/impressao/linhaDigitavel.asp?proposta="
            Caminho &= Dt.Rows(0).Item(0).ToString()
            Caminho &= "&cobranca="

            Caminho &= Cobranca

            js = "         abre_janela('" & Caminho & "','janela_boleto','no','no','675','225','200','200');" & vbNewLine


            Me.ClientScript.RegisterClientScriptBlock(Me.GetType(), "AbreJanela", js, True)
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Function


    Private Sub EsconderGridProrrogacao()
        lblProrrogacao.Visible = False
        gdvProrrogacao.Visible = False
        btnConfirmarProrrogacao.Visible = False
    End Sub

    Private Function CarregarDadosProrrogacao(ByVal btnProrrogarSelecionado As Button, _
                                                  ByVal GridRow As GridViewRow) As DataTable

        Dim dtDadosFaturasPendentes As DataTable = CarregarFaturasPendentes()
        Dim count As Integer = 0
        Dim linha As Integer = 0

        For count = 0 To dtDadosFaturasPendentes.Rows.Count - 1
            If dtDadosFaturasPendentes.Rows(linha).ItemArray(0) <> GridRow.Cells(1).Text Then
                dtDadosFaturasPendentes.Rows(linha).Delete()
                dtDadosFaturasPendentes.AcceptChanges()
            Else
                linha = linha + 1
            End If
        Next count

        CarregarDadosProrrogacao = dtDadosFaturasPendentes

        Return CarregarDadosProrrogacao

    End Function

    Public Sub GerarGridProrrogacao(ByVal btnProrrogarSelecionado As Button, _
                                    ByVal GridRow As GridViewRow)

        Dim dtFaturaProrrogacao As DataTable

        dtFaturaProrrogacao = CarregarDadosProrrogacao(btnProrrogarSelecionado, GridRow)

        gdvProrrogacao.DataSource = dtFaturaProrrogacao
        gdvProrrogacao.DataBind()

    End Sub

    Private Sub GerarBoletoProrrogado()
        Dim bd As New cConsultarFaturas(cDadosBasicos.eBanco.web_seguros_db)
        Dim txtVencimentoEscolhido As TextBox = gdvProrrogacao.Rows(0).FindControl("txtVencimentoEscolhido")
        Dim v_iTransacao As Integer

        Try

            If txtVencimentoEscolhido.Text <> "" Then

                v_iTransacao = bd.IniciaTransacao()

                bd.DeletaTemporaria(Session("usuario")) 'deleta tempor�ria

                bd.InsereTemporariaFaturaId(gdvProrrogacao.DataKeys(0).Values("fatura_id").ToString(), _
                                            gdvProrrogacao.DataKeys(0).Values("proposta_id").ToString(), _
                                            gdvProrrogacao.DataKeys(0).Values("num_cobranca").ToString(), _
                                            False, True, False, Session("usuario"))

                bd.ExecutaSQL_Trans(v_iTransacao)

                ImprimirPDF(v_iTransacao, txtVencimentoEscolhido.Text)

                bd.DeletaTemporaria(Session("usuario")) 'deleta tempor�ria
                bd.ExecutaSQL_Trans(v_iTransacao)

                bd.CommitTransacao(v_iTransacao)
		bd = Nothing

                Call EsconderGridProrrogacao()

            Else
                Call ShowMessageBox("N�o foi escolhida uma data v�lida para o novo vencimento.")
            End If

        Catch ex As Exception
	    bd.RollBackTransacao(v_iTransacao)
	    bd = Nothing
            Dim mensagemErro As String = Replace(Replace(ex.Message, vbCrLf, ""), "5 - ExecuteDataset - Erro", "")
            Call ShowMessageBox(mensagemErro)	    
        End Try

    End Sub

    Public Sub GravarHistoricoBoleto()

    End Sub

    Protected Sub btnProrrogar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProrrogar.Click
        Dim btnProrrogarSelecionado As Button = sender
        Dim GridRow As GridViewRow = btnProrrogarSelecionado.Parent.Parent


        'Inicio 27/12/2018 - Demanda MU-2018-070495 - Cleiton Queiroz - Confitec SP
        'Call ExibirGridProrrogacao(btnProrrogarSelecionado, GridRow)

        Dim fatura_id As Integer = CInt(GridFaturasPendentes.Rows(GridRow.RowIndex).Cells(1).Text)
        Redirecionar(fatura_id)

        'Fim 27/12/2018 - Demanda MU-2018-070495 - Cleiton Queiroz - Confitec SP

    End Sub

    Private Sub btnConfirmarProrrogacao_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirmarProrrogacao.Click
        Call GerarBoletoProrrogado()
    End Sub

#End Region
    'bnery - Nova Consultoria - 03/10/2012 - 12885579 - Prorroga��o de Boletos na Ferramenta WEB - fim

End Class