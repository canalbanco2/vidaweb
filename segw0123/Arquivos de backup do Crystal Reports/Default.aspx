<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="SEGW0123._Default" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet" />
    <script src="scripts/overlib421/overlib.js" type="text/javascript"></script>
    <script src="scripts/script.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        &nbsp;<div>
            &nbsp;
            &nbsp;<asp:Label ID="lblNavegacao" runat="server" Text="Label" CssClass="Caminhotela" Font-Bold="True" Font-Size="12px"></asp:Label><br />
        <br />
        <asp:GridView ID="GridFaturasPendentes" runat="server" AutoGenerateColumns="False" BackColor="White"
            BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" Font-Size="11px"
            GridLines="Vertical" DataKeyNames="fatura_id,proposta_id,num_cobranca" Width="700px">
            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <Columns>
                <asp:TemplateField HeaderText="Sel">
                    <ItemTemplate>
                        &nbsp;<asp:CheckBox ID="ckSelecionado" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="fatura_id" HeaderText="N&#250;mero Fatura" >
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="sub_grupo_id" HeaderText="SubGrupo" >
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="competencia" HeaderText="Compet&#234;ncia da Fatura">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="dt_recebimento" HeaderText="Vencimento" >
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="val_bruto" HeaderText="Valor do Pr&#234;mio" >
                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
               <%--  Inicio 27/12/2018 - Demanda MU-2018-070495 - Cleiton Queiroz - Confitec SP--%>    
                <asp:TemplateField HeaderText="Atualiza��o do Boleto">                
                    <ItemTemplate>
            		    <asp:Button ID="btnProrrogar" runat="server" CssClass="Botao" Text="Atualizar" Width="80px"
            		                OnClick="btnProrrogar_Click" />
                    </ItemTemplate>
                    <%--  Fim 27/12/2018 - Demanda MU-2018-070495 - Cleiton Queiroz - Confitec SP--%>    
                    
            	    <HeaderStyle HorizontalAlign="Left" Width="80px" />
                    <ItemStyle HorizontalAlign="Left" Width="80px" />
                </asp:TemplateField>
                <asp:BoundField DataField="fl_boleto_registrado" />
            </Columns>
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="Gainsboro" />
        </asp:GridView>
        &nbsp;<br />
        <asp:CheckBox ID="chkImpFatura" runat="server" Text="Consultar/Imprimir Fatura" />&nbsp;
        <asp:CheckBox ID="chkImpRelVidas" runat="server" Text="Consultar/Imprimir Rela��o de Vidas" />&nbsp;
        <asp:CheckBox ID="chkImpBoletos" runat="server" Text="Consultar/Imprimir Boletos" />&nbsp;<br />
        <br />
        &nbsp;<asp:Button ID="btImprimir" runat="server" CssClass="Botao" Text="Exportar para PDF" /><br />
    </div>
    <div>
        <br />
        <asp:Label ID="lblProrrogacao" runat="server" Text="Prorrogar vencimento da fatura:" 
                   CssClass="Caminhotela" Font-Bold="True" Font-Size="12px" />
        <br />
        <br />
        <asp:GridView ID="gdvProrrogacao" runat="server" AutoGenerateColumns="False" BackColor="White"
            BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" Font-Size="11px"
            GridLines="Vertical" DataKeyNames="fatura_id,proposta_id,num_cobranca" Width="700px">
            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <Columns>                
                <asp:BoundField DataField="fatura_id" HeaderText="N�mero Fatura" >
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="sub_grupo_id" HeaderText="Subgrupo" >
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="competencia" HeaderText="Compet�ncia da Fatura">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="val_bruto" HeaderText="Valor do Pr�mio" >
                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
                <asp:BoundField DataField="dt_recebimento" HeaderText="Vencimento Real" >
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Vencimento Escolhido">
                    <ItemTemplate>
            		    <asp:TextBox ID="txtVencimentoEscolhido" runat="server" BorderWidth="0px" Width="100%" Text="" 
            		                 OnKeyPress="formatar(this, '##/##/####')" OnBlur="validarData(this,this.value)" 
            		                 MaxLength="10" BackColor="transparent" />
                    </ItemTemplate>
                    
                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                </asp:TemplateField>
            </Columns>
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="Gainsboro" />
        </asp:GridView>
        <br />
        <asp:button ID="btnConfirmarProrrogacao" runat="server" CssClass="Botao"
		            Text="Confirmar" Width="150px" />
		<br />
		<br />
    </div>
    </form>
    <script>
		top.escondeaguarde();
	</script>
</body>
</html>
