			
//Pega o tipo de acesso do usu�rio em quest�o
var parametros = window.location.search.split("&");
var acesso = parametros[parametros.length-2].split("=")[1];
var GB_IMG_DIR = "scripts/box/";
GreyBox.preloadGreyBoxImages();

function confirmaDescarte(){
	var confirma = confirm("O arquivo enviado de protocolo 1850 ser� descartado.\nConfirma a opera��o?");
	if (confirma==true){
		//alert('Vidas criticadas descartadas com sucesso.');
		location.href='?fatura=ok';
		return false;
	}else{
		return false;
	}
}
function Loader() {
	top.escondeaguarde();
}
	
function popup(numero){
	var tamW = (screen.width/2)-200;
	var tamH = (screen.height/2)-200;
	//1
	window.open('../segw0060/ajudaF.aspx','',"resizable=1,scrollbars=1,width=400,height=400,top="+tamH+",left="+tamW+",toolbar=0,location=0,directories=0,status=0,menubar=0");
	return false
}
					
function exibeDependentes(id,src) {
		
	if (src.src.indexOf('imgMenos') != -1)
		src.src = '../segw0060/Images/imgMais.gif';
	else
		src.src = '../segw0060/Images/imgMenos.gif';
	
	for(i=0;i<=5;i++) {					
		var tr = eval("document.getElementById('grdPesquisa_"+id+"_"+i+"')");
		if (tr == null)
			break;
		if (tr.style.display == 'none') 
			tr.style.display = '';
		else
			tr.style.display = 'none';
	}		
}	
	//id="grdPesquisa_1_0" 
	
function exibePesquisa() {
	document.getElementById("divSelectCampoPesquisa").style.display = "block";
}

function ocultaPesquisa() { 
	document.getElementById("divSelectCampoPesquisa").style.display = "none";
}
	
function escondeDiv(div){
	var idDiv = document.getElementById(div);
	if (idDiv.style.display==''){
		idDiv.style.display='none';
	}else{
		idDiv.style.display='';
	}
}

function vidaExcluir(numero){
	if (numero)
		location.href="MovimentacaoUmaUm.aspx?excluir=1&expandir=1";
	else
		location.href="MovimentacaoUmaUm.aspx&expandir=1";
}

function confirmaExclusao(){
	var divExcluir = document.getElementById("lblAcao");
	var nomeSegurado = document.getElementById("nomeAlvo").value;
	var confirmacao = confirm('Confirma a exclus�o do segurado "' + nomeSegurado + '"?\nSe existirem c�njuges para este segurado todos ser�o exclu�dos.')
	
	return confirmacao
}

function confirmaDesfazer(){
	var confirmacao = confirm('Ser�o desfeitas todas as altera��es realizadas para o Subgrupo "<%= Session("subgrupo")%>".\n\nConfirma a opera��o?');
}

function getPesquisaLoad(valor) {				
									
	if(valor == "CPF" || valor == "Nome") {
		document.getElementById("divCampoPesquisa").style.display = "block";
	} else {
		document.getElementById("divCampoPesquisa").style.display = "none";
	}				
}
function getPesquisa(valor) {				
	
	document.Form1.campoPesquisa.value = "";
	
	if(valor == "CPF" || valor == "Nome") {
		document.getElementById("divCampoPesquisa").style.display = "block";
	} else {
		document.getElementById("divCampoPesquisa").style.display = "none";
	}				
}
function buscaDados() {
	var valor = document.Form1.DropDownList2.value;
	
	document.Form1.submit();	
}
var teste = 0;
var id;

function alteracoes(valor, tipo, nome, idTitular, alvo, operacao) {								
																				
	idTemp = "grdPesquisa_" + alvo;
	
	top.document.id_selecionado_SEGW0065 = alvo;
												
	idTemp2 = document.getElementById("alvoTemp").value; 
	
	if(document.getElementById(idTemp2)) {															
		document.getElementById(idTemp2).style.background='#ffffff';
	} 									
	
	document.getElementById(idTemp).style.background='#f8ef00';
		
	document.getElementById("alvo").value = valor;				
	document.getElementById("alvoTemp").value = idTemp;
	document.getElementById("nomeAlvo").value = nome;
					
	var acesso = document.getElementById('ind_acesso').value;
					
	if(tipo == "Titular") {
		if (document.getElementById("divBtnIncluiDependente")) {
			if ((acesso != 2 && acesso != 3) && document.Form1.btnIncluiDep)
				document.Form1.btnIncluiDep.disabled = false;
		}
		
		if (acesso != 2 && acesso != 3) {
			document.Form1.btnAlterar.disabled = false;
			document.Form1.btnExcluir.disabled = false;
			document.getElementById("txtBtnExcluir").className="txtBtnEnabled";
			document.getElementById("txtBtnAlterar").className="txtBtnEnabled";
		}
		
	} else {
		if (document.getElementById("divBtnIncluiDependente")) {
			if ((acesso != 2 && acesso != 3) && document.Form1.btnIncluiDep)
				document.Form1.btnIncluiDep.disabled = true;
		}
		
		if (acesso != 2 && acesso != 3) {
			document.Form1.btnAlterar.disabled = false;						
			document.Form1.btnExcluir.disabled = false;
			document.getElementById("txtBtnExcluir").className="txtBtnEnabled";
			document.getElementById("txtBtnAlterar").className="txtBtnEnabled";
		}
	}
	
	if (operacao == 'Exclus�o') {
		document.Form1.btnAlterar.disabled = true;
		document.Form1.btnExcluir.disabled = true;
		document.getElementById("txtBtnExcluir").className="txtBtnDisabled";
		document.getElementById("txtBtnAlterar").className="txtBtnDisabled";
		if(document.Form1.btnIncluiDep) {
			document.Form1.btnIncluiDep.disabled = true;
		}
	}
		
	if (operacao == 'Inclus�o') {
		document.Form1.btnAlterar.disabled = true;					
		document.getElementById("txtBtnAlterar").className="txtBtnDisabled";
	}
		
	if (operacao == 'Altera��o') {					
		document.getElementById("txtBtnAlterar").className="txtBtnDisabled";
		document.Form1.btnAlterar.disabled = true;
	}
	
	document.Form1.idTitular.value = idTitular;
}

function validaMouseOut(src,color) {
	//alert(teste);
	//if (id != src.id)
	src.style.background=color;			
		
	teste = 0;
}
function mO(cor, alvo) {												
	if(alvo.style.background != '#f8ef00')
		alvo.style.background=cor;							
}

function alteraRegistro() {
	if (document.getElementById("divBtnIncluiDependente")) {					
		if(document.Form1.btnIncluiDep.disabled == true){
			return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=alterardependente&id=' + document.getElementById("alvo").value + "&idTitular=" + document.Form1.idTitular.value, 240, 450);
		} else {
			return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=alterar&id=' + document.getElementById("alvo").value, 280, 450);
		}
	} else {
		return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=alterar&id=' + document.getElementById("alvo").value, 280, 450);
	}
}
function incluiDependente() {
	try {										
		idTemp2 = document.getElementById("alvoTemp").value; 
		
		var dep = document.getElementById(idTemp2 + "_0");
		if (dep != null) {
			alert("Voc� j� informou um c�njuge para o titular escolhido.");
			return false;
		}
		
	} catch(ex) {
	
	}
	
	//alert(document.Form1.idTitular.value);
	
	return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=incluirdependente&id=' + document.getElementById("alvo").value + "&idTitular=" + document.Form1.idTitular.value, 240, 450);
}
function excluiRegistro() {	
	try {				
		if (document.Form1.btnIncluiDep.disabled == true) {							
				return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluirdependente&id=' + document.getElementById("alvo").value + "&idTitular=" + document.Form1.idTitular.value, 240, 450);									
		} else {
			return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluir&id=' + document.getElementById("alvo").value, 280, 450);				
		}
	} catch(ex) {
		return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluir&id=' + document.getElementById("alvo").value, 280, 450);								
	}
}

function FormataCPF(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres){
	var wTecla, wVr, wTam;

	wTecla = pTeclaPres.keyCode;
	wVr = pForm[pCampo].value;
	wVr = wVr.toString().replace( "-", "" );
	wVr = wVr.toString().replace( ".", "" );
	wVr = wVr.toString().replace( ".", "" );
	wVr = wVr.toString().replace( "/", "" );
	wTam = wVr.length ;

	if(wTam > pTamMax) {
		wTam = pTamMax;
		wVr = wVr.substr(0, pTamMax)
	}

	if (wTam < pTamMax && wTecla != 8) { 
		wTam = wVr.length + 1 ; 
	}

	if (wTecla == 8 ) { 
		wTam = wTam - 1 ; 
	}

	if ( wTecla == 8 || wTecla == 88 || wTecla >= 48 && wTecla <= 57 || wTecla >= 96 && wTecla <= 105 ){
		if ( wTam <= 2 ){
			pForm[pCampo].value = wVr ;
		}
		if (wTam > pPosTraco && wTam <= pTamMax) {
			wVr = wVr.substr(0, wTam - pPosTraco) + '-' + wVr.substr(wTam - pPosTraco, wTam);
		}
		if ( wTam == pTamMax){
			wVr = wVr.substr( 0, wTam - pPos1 ) + '.' + wVr.substr(wTam - pPos1, 3) + '.' + wVr.substr(wTam - pPos2, wTam);
		}
		pForm[pCampo].value = wVr;
	}
}
function formataCampo(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres) {		
	if(pForm.DropDownList2.value == "CPF") { 
		FormataCPF(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres);
	}		
}

function GB_showConfirm() {
	alert("Segurado inclu�do com sucesso.");
	GB_hide();			
	GB_show('Movimenta��o on-line', '../../../segw0065/confirm.aspx', 100, 200);					
}			
function GB_showConfirmPost() {
	GB_hide();			
	GB_show('Movimenta��o on-line', '../../../segw0065/confirmPost.aspx?reloaded=1', 100, 200);		
}				
function GB_showConfirmSim() {
	GB_hide();
	GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=incluir', 280, 450);
}
function GB_showConfirmNao() {						
	GB_reloadOnClose('true');
	GB_hide();
}		