//exclui um registro da tabela
function ExcluirUm(){	
	$.blockUI( "<div id='ag'><h4>aguarde...</h4></div><div id='frm'><form name='frmExcluir' id='frmExcluir' METHOD=POST ACTION='"+window.location.href+"&acao=2'><h4>Deseja realmente excluir este registro?</h4><br>"
					+"<input id='bt1__' type='button' value='Sim' class='botao'>"
					+"<input id='bt2__' type='button' value='N&atilde;o' class='botao'>"
					+"<INPUT id='_id_cli' type='hidden' name='_id_cli'>"
			  +"</form></div>" );	
	
	// bot&Atilde;o n&Atilde;o
	$('#bt2__').bind('click', function(){
		//alert(window.location.href)
		$.unblockUI();
	});

	$('#ag').hide();
	
	// bot&Atilde;o sim
	$('#bt1__').bind('click', function(){
		$('#_id_cli').val($('#_id_cli_').val());
		$('#frm').hide();
		$('#ag').show();
		document.all.frmExcluir.submit();
		//alert($('#_id_cli').val()
	});
}

//exclui todos os registros da apolice
function ExcluirTodos(){	
	$.blockUI( "<div id='ag'><h4>aguarde...</h4></div><div id='frm'><form name='frmExcluirTudo' METHOD=POST ACTION='"+window.location.href+"&acao=3'><h4>Deseja realmente excluir todos os registro?</h4><br>"
					+"<input id='bt1_' type='button' value='Sim' class='botao'>"
					+"<input id='bt2_' type='button' value='N&atilde;o' class='botao'>"
					+"<INPUT id='_id_cli' type='hidden' name='_id_cli'>"
			  +"</form></div>" );	
	
	// bot&Atilde;o n&Atilde;o
	$('#bt2_').bind('click', function(){
		$.unblockUI();
		//alert(teste);
	});

	$('#ag').hide();
	
	// bot&Atilde;o sim
	$('#bt1_').bind('click', function(){
		//alert('op&Ccedil;&Atilde;o excluir tudo!')
		$('#frm').hide();
		$('#ag').show();
		document.all.frmExcluirTudo.submit();		
	});
}

function MontaFormulario2(listCriticas){
	var wi = listCriticas.split(',').length * 80;
	
	$('#popup').html('<IFRAME id="frame1" name=palco src="validacao.aspx?lista='+listCriticas+'&_id='+$('#_id_cli_').val()+'" frameBorder=0 scrolling=auto width=300px height='+wi+'px ></IFRAME>');
	$('#popup').show();
	$('#popup').css('z-index' , '1000');
}