<%@ Register TagPrefix="uc1" TagName="paginacaogrid" Src="paginacaogrid.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="vidascriticadas.aspx.vb" Inherits="segw0068.VidasCriticadas" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD html 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
		<title>Downloads</title>
<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
<meta content="JavaScript" name="vs_defaultClientScript" />
<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema" />
<link href="CSS/EstiloBase.css" type="text/css" rel="stylesheet" />
<link href="CSS/base0068.css" type="text/css" rel="stylesheet" />
<link media="all" href="scripts/box/box.css" type="text/css" rel="stylesheet" />
<script src="scripts/box/AJS.js" type="text/javascript"></script>

<script src="scripts/box/box.js" type="text/javascript"></script>

<script src="scripts/overlib421/mini/overlibY.js" type="text/javascript"></script>

<script src="scripts/jquery.js" type="text/javascript"></script>

<script src="scripts/jquery.maskedinput-1.1.2.js" type="text/javascript"></script>

<script src="scripts/validacao.js" type="text/javascript"></script>

<script src="scripts/jquery.blockUI.js" type="text/javascript"></script>

<style type="text/css">.jqmWindow { BORDER-RIGHT: #cccccc 1px solid; PADDING-RIGHT: 12px; BORDER-TOP: #cccccc 1px solid; DISPLAY: none; PADDING-LEFT: 12px; LEFT: 50%; PADDING-BOTTOM: 12px; MARGIN-LEFT: -175px; BORDER-LEFT: #cccccc 1px solid; WIDTH: 350px; COLOR: #333; PADDING-TOP: 12px; BORDER-BOTTOM: #cccccc 1px solid; POSITION: fixed; TOP: 25%; BACKGROUND-COLOR: #fff }
	.jqmOverlay { BACKGROUND-COLOR: #fff }
	html .jqmWindow { POSITION: absolute; ; TOP: expression((document.documentElement.scrollTop || document.body.scrollTop) + Math.round(25 * (document.documentElement.offsetHeight || document.body.clientHeight) / 100) + 'px') }
	</style>

<script type="text/javascript">
	//Pega o tipo de acesso do usu�rio em quest�o
	var parametros = window.location.search.split("&");
	var acesso = parametros[parametros.length-2].split("=")[1];
	var GB_IMG_DIR = "scripts/box/";
	GreyBox.preloadGreyBoxImages();
	
	
	

	
	
	// Projeto 539202 - Jo�o Ribeiro - 29/04/2009
	function verificaSessao() {
		
		if (document.getElementById('ind_acesso').value == 6) {
			
			document.Form1.btnExcluir.disabled = true;
			document.getElementById("txtBtnExcluir").className="txtBtnDisabled";

			document.Form1.btnExcluirTodos.disabled = true;
			document.getElementById("txtBtnExcluirTudo").className="txtBtnDisabled";

			document.Form1.btnProcessar.disabled = true;
			
		}
		
	}
	// Projeto 539202 - Fim

	function confirmaDescarte(){
		var confirma = confirm("O arquivo enviado de protocolo 1850 ser� descartado.\nConfirma a opera��o?");
		if (confirma==true){
			//alert('Vidas criticadas descartadas com sucesso.');
			location.href='?fatura=ok';
			return false;
		}else{
			return false;
		}
	}
		
	function Loader() {
		top.escondeaguarde();						
		
		for(i=0; i<=4; i++) {
			try {
				if(document.getElementById("grdPesquisa_0_cell_" + i)) {										
					add = "";					
					if(i == 4) {
						add = "OOOOOO";
					}
					if (document.getElementById("header" + i))
						document.getElementById("header" + i).innerhtml = document.getElementById("txtMax" + i).value + add;
					if (document.getElementById("headerFake" + i))
						document.getElementById("headerFake" + i).innerhtml = document.getElementById("txtMax" + i).value;								
					if(document.getElementById("hFake" + i))
						document.getElementById("hFake" + i).innerhtml = document.getElementById("txtMax" + i).value + add;
				}
			} catch (e) { }
		}														
		
		/*for(i=1; i<=9; i++) {					
			if(document.getElementById("grdPesquisa_0_cell_" + i)) {
				if(document.getElementById("grdPesquisa_0_cell_" + i).style.display == "none") {
					document.getElementById("tdheader" + i).style.display = "none";
				}
			}					
		}						*/
	}
		
	function popup(numero){
		var tamW = (screen.width/2)-200;
		var tamH = (screen.height/2)-200;
		//1
		window.open('../segw0060/ajudaF.aspx','',"resizable=1,scrollbars=1,width=400,height=400,top="+tamH+",left="+tamW+",toolbar=0,location=0,directories=0,status=0,menubar=0");
		return false
	}
						
	function exibeDependentes(id,src) {
			
		if (src.src.indexOf('imgMenos') != -1)
			src.src = '../segw0060/Images/imgMais.gif';
		else
			src.src = '../segw0060/Images/imgMenos.gif';
		
		for(i=0;i<=5;i++) {					
			var tr = eval("document.getElementById('grdPesquisa_"+id+"_"+i+"')");
			if (tr == null)
				break;
			if (tr.style.display == 'none') 
				tr.style.display = '';
			else
				tr.style.display = 'none';
		}		
	}	

	function escondeDiv(div){
		var idDiv = document.getElementById(div);
		if (idDiv.style.display==''){
			idDiv.style.display='none';
		}else{
			idDiv.style.display='';
		}
	}

	function vidaExcluir(numero){
		if (numero)
			location.href="MovimentacaoUmaUm.aspx?excluir=1&expandir=1";
		else
			location.href="MovimentacaoUmaUm.aspx&expandir=1";
	}

	function confirmaExclusao(){
		var divExcluir = document.getElementById("lblAcao");
		var nomeSegurado = document.getElementById("nomeAlvo").value;
		var confirmacao = confirm('Confirma a exclus�o do segurado "' + nomeSegurado + '"?\nSe existirem c�njuges para este segurado todos ser�o exclu�dos.')
		
		return confirmacao
	}

	function confirmaDesfazer(){
		var confirmacao = confirm('Ser�o desfeitas todas as altera��es realizadas para o Subgrupo "<%= Session("subgrupo")%>".\n\nConfirma a opera��o?');
	}

	function getPesquisaLoad(valor) {				
										
		if(valor == "CPF" || valor == "Nome") {
			document.getElementById("divCampoPesquisa").style.display = "block";
		} else {
			document.getElementById("divCampoPesquisa").style.display = "none";
		}				
	}
	function getPesquisa(valor) {				
		
		document.Form1.campoPesquisa.value = "";
		
		if(valor == "CPF" || valor == "Nome") {
			document.getElementById("divCampoPesquisa").style.display = "block";
		} else {
			document.getElementById("divCampoPesquisa").style.display = "none";
		}				
	}
	function buscaDados() {
		var valor = document.Form1.DropDownList2.value;
		
		document.Form1.submit();	
	}
	var teste = 0;
	var id;

	function alteracoes(valor, tipo, nome, idTitular, alvo, operacao) {								
													
		idTemp = "grdPesquisa_" + alvo;
		
		top.document.id_selecionado_SEGW0065 = alvo;
													
		idTemp2 = document.getElementById("alvoTemp").value; 
		
		if(document.getElementById(idTemp2)) {															
			document.getElementById(idTemp2).style.background='#ffffff';
		} 									
		
		document.getElementById(idTemp).style.background='#f8ef00';
			
		document.getElementById("alvo").value = valor;				
		document.getElementById("alvoTemp").value = idTemp;
		document.getElementById("nomeAlvo").value = nome;
		
		var acesso = document.getElementById('ind_acesso').value;
		
		if(tipo == "Titular") {
		
		    if (document.getElementById("divBtnIncluiDependente")) {
				if ((acesso != 2 && acesso != 3) && document.Form1.btnIncluiDep)
					document.Form1.btnIncluiDep.disabled = false;
			}
			
			if (acesso != 2 && acesso != 3) {
				document.Form1.btnAlterar.disabled = false;
							
				if(($('#lista_criticas').val().lastIndexOf('23') == -1)){
					document.Form1.btnAlterar.disabled = false;	
				}else{
					document.Form1.btnAlterar.disabled = true;	
				}
				
				document.getElementById("txtBtnExcluir").className="txtBtnEnabled";
				
				if(($('#lista_criticas').val().lastIndexOf('23') == -1)){
					document.getElementById("txtBtnAlterar").className="txtBtnEnabled";
				}else{
					document.getElementById("txtBtnAlterar").className="txtBtnDisabled";
				}
			}
			
		} else {
		
			if (document.getElementById("divBtnIncluiDependente")) {
				if ((acesso != 2 && acesso != 3) && document.Form1.btnIncluiDep)
					document.Form1.btnIncluiDep.disabled = true;
			}
			
			if (acesso != 2 && acesso != 3) {
				if(($('#lista_criticas').val().lastIndexOf('23') == -1)){
					document.Form1.btnAlterar.disabled = false;	
				}else{
					document.Form1.btnAlterar.disabled = true;	
				}	
			
			    //pmarques - 13/05/2010
				if(($('#lista_criticas').val().lastIndexOf('29') == -1)){
					document.Form1.btnAlterar.disabled = false;	
				}else{
					document.Form1.btnAlterar.disabled = true;	
				}	
				//-----------------------
							
				document.Form1.btnExcluir.disabled = false;
				
				document.getElementById("txtBtnExcluir").className="txtBtnEnabled";
				
				if(($('#lista_criticas').val().lastIndexOf('23') == -1)){
					document.getElementById("txtBtnAlterar").className="txtBtnEnabled";
				}else{
					document.getElementById("txtBtnAlterar").className="txtBtnDisabled";
				}
				document.getElementById("txtBtnExcluirTudo").className="txtBtnEnabledBig";
			}
		}
		
		if (operacao == 'Exclus�o') {
			document.Form1.btnAlterar.disabled = true;
			document.Form1.btnExcluir.disabled = true;
			
			document.getElementById("txtBtnExcluir").className="txtBtnDisabled";
			document.getElementById("txtBtnAlterar").className="txtBtnDisabled";
			if(document.Form1.btnIncluiDep) {
				document.Form1.btnIncluiDep.disabled = true;
			}
		}
			
		if (operacao == 'Inclus�o') {
			document.Form1.btnAlterar.disabled = true;					
			document.getElementById("txtBtnAlterar").className="txtBtnDisabled";
		}
			
		if (operacao == 'Altera��o') {					
			document.getElementById("txtBtnAlterar").className="txtBtnDisabled";
			document.Form1.btnAlterar.disabled = true;
		}
		
		document.Form1.idTitular.value = idTitular;
		
		// Projeto 539202 - Jo�o Ribeiro - 29/04/2009
		verificaSessao();
		// Projeto 539202 - Fim
	}

	function validaMouseOut(src,color) {
		//alert(teste);
		//if (id != src.id)
		src.style.background=color;			
			
		teste = 0;
	}
	function mO(cor, alvo) {												
		if(alvo.style.background != '#f8ef00')
			alvo.style.background=cor;							
	}

	function alteraRegistro() {
		/*if (document.getElementById("divBtnIncluiDependente")) {					
			if(document.Form1.btnIncluiDep.disabled == true){
				return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=alterardependente&id=' + document.getElementById("alvo").value + "&idTitular=" + document.Form1.idTitular.value, 240, 450);
			} else {
				return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=alterar&id=' + document.getElementById("alvo").value, 280, 450);
			}
		} else { */
			//return GB_show('Vida Criticada', 'http://localhost/testeseguranca/SEGW0068/alterarVidas.aspx?acao=alterar&id=' + document.getElementById("alvo").value, 180, 400);
			//window.open('alterarVidas.aspx?acao=alterar&id=' + document.getElementById("alvo").value + '&lista=' + document.getElementById("lista_criticas").value,'');
			//alert(document.getElementById('lista_criticas').value);
			MontaFormulario2(document.getElementById('lista_criticas').value);
			document.getElementById('_id_cli').value = document.getElementById('_id_cli_').value;
		//}../../../segw0068/
	}
	
	function incluiDependente() {
		try {										
			idTemp2 = document.getElementById("alvoTemp").value; 
			
			var dep = document.getElementById(idTemp2 + "_0");
			if (dep != null) {
				alert("Voc� j� informou um c�njuge para o titular escolhido.");
				return false;
			}
			
		} catch(ex) {
		
		}
		
		//alert(document.Form1.idTitular.value);
		
		return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=incluirdependente&id=' + document.getElementById("alvo").value + "&idTitular=" + document.Form1.idTitular.value, 240, 450);
	}
	function excluiRegistro() {	
		/*try {				
			if (document.Form1.btnIncluiDep.disabled == true) {							
					return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluirdependente&id=' + document.getElementById("alvo").value + "&idTitular=" + document.Form1.idTitular.value, 240, 450);									
			} else {
				return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluir&id=' + document.getElementById("alvo").value, 280, 450);				
			}
		} catch(ex) { */
		
			//return GB_show('Resultado do Processamento do Arquivo', '../../../segw0068/confirm.aspx?acao=excluir&id=' + document.getElementById("alvo").value, 140, 320);								
			//alert('excluindo um registro')
			ExcluirUm();
		//}
	}

	function excluiTodosRegistros() {	
		/*try {				
			if (document.Form1.btnIncluiDep.disabled == true) {							
					return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluirdependente&id=' + document.getElementById("alvo").value + "&idTitular=" + document.Form1.idTitular.value, 240, 450);									
			} else {
				return GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=excluir&id=' + document.getElementById("alvo").value, 280, 450);				
			}
		} catch(ex) { */
			//return GB_show('Resultado do Processamento do Arquivo', '../../../segw0068/confirm.aspx?acao=excluirTodos&id=' + document.getElementById("alvo").value, 140, 320);											
			//alert('excluindo todos os registro')
			
			if(!($('#lblSemUsuario').text() == 'Nenhum Registro encontrado.'))
			{
				ExcluirTodos();
			}						
		//}
	}

	function FormataCPF(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres){
		var wTecla, wVr, wTam;

		wTecla = pTeclaPres.keyCode;
		wVr = pForm[pCampo].value;
		wVr = wVr.toString().replace( "-", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( "/", "" ); 
		wTam = wVr.length ;

		if(wTam > pTamMax) {
			wTam = pTamMax;
			wVr = wVr.substr(0, pTamMax)
		}

		if (wTam < pTamMax && wTecla != 8) { 
			wTam = wVr.length + 1 ; 
		}

		if (wTecla == 8 ) { 
			wTam = wTam - 1 ; 
		}

		if ( wTecla == 8 || wTecla == 88 || wTecla >= 48 && wTecla <= 57 || wTecla >= 96 && wTecla <= 105 ){
			if ( wTam <= 2 ){
				pForm[pCampo].value = wVr ;
			}
			if (wTam > pPosTraco && wTam <= pTamMax) {
				wVr = wVr.substr(0, wTam - pPosTraco) + '-' + wVr.substr(wTam - pPosTraco, wTam);
			}
			if ( wTam == pTamMax){
				wVr = wVr.substr( 0, wTam - pPos1 ) + '.' + wVr.substr(wTam - pPos1, 3) + '.' + wVr.substr(wTam - pPos2, wTam);
			}
			pForm[pCampo].value = wVr;
		}
	}
	function formataCampo(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres) {		
		if(pForm.DropDownList2.value == "CPF") { 
			FormataCPF(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres);
		}		
	}

	function GB_showConfirm() {
		alert("Segurado inclu�do com sucesso.");
		GB_hide();			
		GB_show('Movimenta��o on-line', '../../../segw0065/confirm.aspx', 100, 200);					
	}			
	function GB_showConfirmPost() {
		GB_hide();			
		GB_show('Movimenta��o on-line', '../../../segw0065/confirmPost.aspx?reloaded=1', 100, 200);		
	}				
	function GB_showConfirmSim() {
		GB_hide();
		GB_show('Movimenta��o on-line', '../../../segw0065/MovimentacaoUmaUm_Form.aspx?acao=incluir', 280, 450);
	}
	function GB_showConfirmNao() {						
		GB_reloadOnClose('true');
		GB_hide();
	}		
	function exibePesquisa() {
		//document.getElementById("divSelectCampoPesquisa").style.display = "block";
	}

	function ocultaPesquisa() { 
		//document.getElementById("divSelectCampoPesquisa").style.display = "none";
	}
		
					</script>

      <!--<link href="SEGW0019_style.css" rel="styleSHEET" type="text/css" />-->


      <link href="SEGW0019_style.css" rel="STYLESHEET" type="text/css" />
      <link href="SEGW0019_style.css" rel="STYLESHEET" type="text/css" />
      <link href="SEGW0019_style.css" rel="STYLESHEET" type="text/css" />
      <link href="SEGW0019_style.css" rel="STYLESHEET" type="text/css" />
      <link href="SEGW0019_style.css" rel="STYLESHEET" type="text/css" />
      <link href="SEGW0019_style.css" rel="STYLESHEET" type="text/css" />
      <link href="SEGW0019_style.css" rel="STYLESHEET" type="text/css" />
      <link href="SEGW0019_style.css" rel="STYLESHEET" type="text/css" />
      <link href="SEGW0019_style.css" rel="STYLESHEET" type="text/css" />
      <link href="SEGW0019_style.css" rel="STYLESHEET" type="text/css" />


</head>
<body onload="Loader()" MS_POSITIONING="FlowLayout">
<form id="Form1" method="post" runat="server">
<div>
<div class="jqmWindow" id="popup"></div>
<p><asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela" Visible="False"></asp:label><br />
<asp:label id="lblVigencia" runat="server" CssClass="Caminhotela"></asp:label></p>
	<br />
	<asp:Label id="Label17" runat="server" CssClass="Caminhotela">Condi&ccedil;�es do subgrupo</asp:Label>
    <table id="Table6" border="1" bordercolor="#cccccc" cellpadding="1" cellspacing="0"
        style="border-collapse: collapse" width="100%">
        <!-- PRIMEIRA LINHA -->
        <tr class="titulo-tabela sem-sublinhado">
            <td>
                Capital Segurado</td>
            <td id="td_valor_capital" runat="server">
                Valor Capital</td>
            <td>
                Limite M�nimo</td>
            <td>
                Limite M�ximo</td>
            <td>
                M�lt. Salarial</td>
            <td>
                Idade M�n. Mov.</td>
            <td>
                Idade M�x. Mov</td>
            <td>
                C�njuge</td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblCapitalSegurado" runat="server"></asp:Label></td>
            <td id="td_valor_capital_lbl" runat="server" align="center">
                <asp:Label ID="lblValorCapital" runat="server"></asp:Label></td>
            <td id="tdLimiteMinimo" runat="server" align="right">
                <asp:Label ID="LblLimiteMinimo" runat="server"></asp:Label></td>
            <td id="tdLimiteMaximo" runat="server" align="right">
                <asp:Label ID="lblLimiteMaximo" runat="server"></asp:Label></td>
            <td align="center">
                <asp:Label ID="lblMultSalarial" runat="server"></asp:Label></td>
            <td align="center">
                <asp:Label ID="lblIdadeMinima" runat="server"></asp:Label></td>
            <td align="center">
                <asp:Label ID="lblIdadeMaxima" runat="server"></asp:Label></td>
            <td align="center">
                <asp:Label ID="lblConjuge" runat="server"></asp:Label></td>
        </tr>
    </table>
<asp:literal id="criticaScript" runat="server"></asp:literal><br />&nbsp; 
<asp:panel id="pnlAlerta" runat="server" CssClass="fundo-azul-claro" Visible="False" Width="60%" Height="70px" HorizontalAlign="Center"><br />
<asp:Label id="lblListagemEnviada" runat="server" CssClass="Caminhotela"><br />Vidas criticadas descartadas com sucesso.</asp:Label></asp:panel>
<asp:Panel id="pageUnderContruction" Runat="server" Height="26px"></asp:Panel>
<asp:Panel id="pnlFormulario" runat="server">
<asp:Label id="Label1" runat="server" CssClass="Caminhotela" Width="266px" style="z-index: 100; left: 0px; position: absolute; top: 280px">Resultado do processamento do arquivo</asp:Label>
<table id="Table1" style="BORDER-COLLAPSE: collapse; width: 268px; z-index: 107; left: 0px; position: absolute; top: 296px;" bordercolor="#cccccc" height="32" cellspacing="0" cellpadding="1" border="1">
  <tr>
    <td class="titulo-tabela sem-sublinhado" bordercolor="#cccccc" align="center" colspan="2" height="13">Registros aceitos</td>
    <td class="titulo-tabela sem-sublinhado" bordercolor="#cccccc" align="center" height="13">Registros recusados</td></tr>
  <tr>
    <td bordercolor="#cccccc" align="center" colspan="2" height="13">
<asp:Label id="lblQtdAceitos" Runat="server"></asp:Label></td>
    <td bordercolor="#cccccc" align="center" height="13">
<asp:Label id="lblQtdRecusados" Runat="server"></asp:Label></td></tr></table>
    <br />
    <table style="width: 432px; z-index: 106; left: 308px; position: absolute; top: 268px; height: 68px;">
        <tr>
            <td class="titulo-tabela sem-sublinhado" bordercolor="black" style="width: 261px; background-color: #000066; height: 2px; border-right: black 1px solid; border-top: black 1px solid; border-left: black 1px solid; border-bottom: black 1px solid;" >
                &nbsp; &nbsp; Visualizar o arquivo importado</td>
        </tr>
        <tr>
            <td style="height: 12px; height: 4px;">
                <asp:LinkButton ID="lbtLink1" runat="server" Height="1px" Width="426px" style="border-right: black 1px solid; border-top: black 1px solid; border-left: black 1px solid; border-bottom: black 1px solid"><%=lblDescricao1.Text%></asp:LinkButton>
                
            </td>               
        </tr>
        <tr>
            <td style="width: 261px; height: 4px;">
                <asp:LinkButton ID="lbtLink2"  runat="server" Height="1px" Width="426px" style="border-right: black 1px solid; border-top: black 1px solid; border-left: black 1px solid; border-bottom: black 1px solid"><%=lblDescricao2.Text%></asp:LinkButton>
                
        </tr>
        <tr>
            <td style="width: 261px; height: 4px;">
                <asp:LinkButton ID="lbtLink3"  runat="server" Height="3px" Width="426px" style="border-right: black 1px solid; border-top: black 1px solid; border-left: black 1px solid; border-bottom: black 1px solid"><%=lblDescricao3.Text%></asp:LinkButton>
                
        </tr>
    </table>
   
 

    
    <br />
    <br />
    <br />
    <br />
    <br />
<div id="downloads">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    &nbsp;&nbsp;<br />
    &nbsp;<br />
    <br />
    <br />
<br />
<table cellspacing="0" cellpadding="0" width="100%" border="0">
  <tr>
    <td colspan="2">
<asp:Label id="lblListagemVidas" runat="server" CssClass="Caminhotela">Lista das vidas recusadas</asp:Label></td>
    <td colspan="2">
      <div id="divBtnExcluirTudo" onmouseover="ocultaPesquisa(); return overlib('Click para excluir todos os registros.');" style="DISPLAY: block; FLOAT: left; POSITION: relative; z-index: 102;" onmouseout="exibePesquisa(); return nd();">
        <div class="txtBtnDisabledBig" id="txtBtnExcluirTudo"  onclick="document.getElementById('btnExcluirTodos').click()">
            Excluir Tudo
        </div>
        <input class="Botao" id="btnExcluirTodos" style="WIDTH: 88px" onclick="excluiTodosRegistros()" type="button" name="btnExcluirTodos" /> 
      </div>
      <div id="divBtnAlterar" onmouseover="ocultaPesquisa();return overlib('Para alterar um registro clique na lista abaixo');" style="DISPLAY: block; Z-INDEX: 104; LEFT: 5px; FLOAT: left; POSITION: relative" onmouseout="exibePesquisa();return nd();">
        <div class="txtBtnDisabled" id="txtBtnAlterar" onclick="document.getElementById('btnAlterar').click()">
            Alterar
        </div>
        <input class="Botao" id="btnAlterar" style="WIDTH: 63px" onclick="alteraRegistro()" type="button" name="btnAlterar" disabled /> 
      </div>
      <div id="divBtnExcluir" onmouseover="ocultaPesquisa(); return overlib('Para excluir um registro clique na lista abaixo');" style="DISPLAY: block; Z-INDEX: 105; LEFT: 10px; FLOAT: left; POSITION: relative" onmouseout="exibePesquisa(); return nd();">
        <div class="txtBtnDisabled" id="txtBtnExcluir" onclick="document.getElementById('btnExcluir').click()">
            Excluir
        </div>
        <input class="Botao" id="btnExcluir" style="WIDTH: 63px" disabled onclick="excluiRegistro()" type="button" name="btnExcluir" /> 
      </div> &nbsp;
      <input class="Botao" id="btnGerar" onclick="document.Form1.gerarXml.value=1;document.Form1.submit();" type="button" value="Gerar Excel" name="btnGerar" />
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <asp:Button id="btnProcessar" runat="server" CssClass="botao" Width="82px" Text="Processar"></asp:Button></td></tr>
  <tr>
    <td colspan="4">
      
<%--      <style type="text/css">.headerFixDiv {
	FONT-WEIGHT: bold; WIDTH: 100%; COLOR: #ffffff; POSITION: absolute; TOP: 1px; BACKGROUND-COLOR: #003399
}
.headerFake {
	POSITION: absolute; TOP: 2px
}
.headerFixDiv2 {
	VISIBILITY: hidden; WIDTH: 100%
}
.hFake {
	VISIBILITY: hidden; LINE-HEIGHT: 0; TOP: -2px
}
.headerFakeDiv {
	VISIBILITY: hidden; LINE-HEIGHT: 0
}
.headerGrid {
	FONT-WEIGHT: bold; COLOR: #ffffff; BACKGROUND-COLOR: #003399
}
</style>
--%>      
      <table id="gridMovimentacao" width="100%" border="0" runat="server">
        <tr>
          <td><!-- DIV id="divScrollGrid" id="divGridMovimentacao" style="WIDTH: 740px; HEIGHT: 178px" -->
            <table style="BORDER-COLLAPSE: collapse" cellspacing="0" cellpadding="0" border="1">
              <tr>
                <td>
                  <div id="lista" style="z-index: 103">
                  <div id="divScrollGrid">
<asp:datagrid id="grdPesquisa" runat="server" bordercolor="#DDDDDD" AllowPaging="True" PageSize="50" AutoGenerateColumns="False" cellpadding="0">
																						<Footerstyle CssClass="0019GridFooter"></Footerstyle>
																						<SelectedItemstyle CssClass="ponteiro"></SelectedItemstyle>
																						<AlternatingItemstyle CssClass="ponteiro"></AlternatingItemstyle>
																						<Itemstyle CssClass="ponteiro"></Itemstyle>
																						<headerstyle CssClass="th"></headerstyle>
																						<Columns>
																							<asp:BoundColumn Visible="False" DataField="arquivo_vida_criticada_id">
																								<Itemstyle Width="0px"></Itemstyle>
																							</asp:BoundColumn>
																							<asp:BoundColumn DataField="operacao" headerText="Opera&#231;&#227;o">
																								<Itemstyle Width="70px"></Itemstyle>
																							</asp:BoundColumn>
																							<asp:BoundColumn DataField="nome" headerText="Nome">
																								<Itemstyle Width="220px"></Itemstyle>
																							</asp:BoundColumn>
																							<asp:BoundColumn DataField="cpf" headerText="CPF">
																								<Itemstyle Width="100px"></Itemstyle>
																							</asp:BoundColumn>
																							<asp:BoundColumn DataField="dt_nascimento" headerText="Dt. Nasc.">
																								<Itemstyle HorizontalAlign="Center" Width="80px"></Itemstyle>
																							</asp:BoundColumn>
																							<asp:BoundColumn DataField="motivo_critica" headerText="Motivo">
																								<Itemstyle HorizontalAlign="Left" Width="300px"></Itemstyle>
																							</asp:BoundColumn>
																							<asp:BoundColumn DataField="dt_inicio_vigencia_sbg" headerText="Dt. In&#237;cio Vig&#234;ncia">
																								<Itemstyle HorizontalAlign="Left" Width="110px"></Itemstyle>
																							</asp:BoundColumn>
																							<asp:BoundColumn DataField="val_salario" headerText="Sal&#225;rio">
																								<Itemstyle HorizontalAlign="Right" Width="100px"></Itemstyle>
																							</asp:BoundColumn>
																							<asp:BoundColumn DataField="val_capital_segurado" headerText="Capital (R$)">
																								<Itemstyle HorizontalAlign="Right" Width="100px"></Itemstyle>
																							</asp:BoundColumn>
																							<asp:BoundColumn Visible="False" DataField="motivo_critica_id">
																								<Itemstyle Width="0px"></Itemstyle>
																							</asp:BoundColumn>
																						</Columns>
																						<Pagerstyle Visible="False"></Pagerstyle>
																					</asp:datagrid></div></div></td></tr></table></td></tr></table>
      <center>
<asp:Label id="lblSemUsuario" Visible="False" CssClass="Caminhotela" Runat="server"></asp:Label></CENTER>
<uc1:paginacaogrid id="ucPaginacao" runat="server"></uc1:paginacaogrid></td></tr></table>&nbsp;<br />
<asp:Label id="Label2" runat="server" CssClass="Caminhotela2">Enquanto os registros n�o forem corrigidos, n�o far�o parte da movimenta��o referente � compet�ncia 
<asp:Label ID="lblCompetencia" Runat="server"></asp:Label></asp:Label><br /></div>
<div style="DISPLAY: none; WIDTH: 99%" align="center">
<asp:Button id="Button1" runat="server" CssClass="Botao" Width="164px" Text="Descartar arquivo enviado"></asp:Button>&nbsp; 
<%--<asp:Image id="Image1" runat="server" ImageUrl="Images/help.gif"></asp:Image>
--%>
</div>
<input id="alvo" type="hidden" name="alvo" />
<input id="alvoTemp" type="hidden" value="0" name="alvo" />
<input id="nomeAlvo" type="hidden" name="nomeAlvo" /> 
<input id="idTitular" type="hidden" name="idTitular" /> 
<input id="lista_criticas" type="hidden" name="lista_criticas" />
<input id="_id_cli_" type="hidden" name="_id_cli_" />
<input type="hidden" name="gerarXml" /> 
</asp:Panel><!-- Fim do painel de cadastro -->

</div></form>
<script type="text/javascript" language="javascript">



		function GerarCookie(strCookie, strValor, lngDias)
		{
			var dtmData = new Date();

			if(lngDias)
			{
				dtmData.setTime(dtmData.getTime() + (lngDias * 24 * 60 * 60 * 1000));
				var strExpires = "; expires=" + dtmData.toGMTString();
			}
			else
			{
				var strExpires = "";
			}
			document.cookie = strCookie + "=" + strValor + strExpires + "; path=/";
		}

		function LerCookie(strCookie)
		{
			var strNomeIgual = strCookie + "=";
			var arrCookies = document.cookie.split(';');

			for(var i = 0; i < arrCookies.length; i++)
			{
				var strValorCookie = arrCookies[i];
				while(strValorCookie.charAt(0) == ' ')
				{
					strValorCookie = strValorCookie.substring(1, strValorCookie.length);
				}
				if(strValorCookie.indexOf(strNomeIgual) == 0)
				{
					return strValorCookie.substring(strNomeIgual.length, strValorCookie.length);
				}
			}
			return null;
		}

		function ExcluirCookie(strCookie)
		{
			GerarCookie(strCookie, '', -1);
		}

		
		function Reflesh(){		
				
				$.blockUI( '<h5>atualizando o resumo...</h5>' );
				
				$.post("ajax.aspx", function(data){
					
					var _s = data.split('|')[1];
					var _arr = eval(_s);
					
					for(var i=0; i<_arr.length-1; i++){
						window.parent.document.getElementById(_arr[i].obj).innerhtml = _arr[i].value;
					}
					
					if(data.split('|')[2] != ''){eval(data.split('|')[2])}
					
					ExcluirCookie("reflesh");
					$.unblockUI();
					
				});
		}
		
		if(LerCookie("reflesh") == '1'){Reflesh();}
		
			$('#btnProcessar').bind('click', function(){
				$(document.getElementById('divAguardeAbrir')).click();
			});
			
			$(document.getElementById('divAguardeFechar')).click();
			
			
		// Projeto 539202 - Jo�o Ribeiro - 29/04/2009
		verificaSessao();
		// Projeto 539202 - Fim
		
		</script>

<div id="divAguardeAbrir" style="DISPLAY: none" onclick="$.blockUI( '<h4>aguarde...</h4>' );"></div>
<div id="divAguardeFechar" style="DISPLAY: none" onclick="$.unblockUI();">
    <asp:Label ID="lblDescricao1" runat="server" Text="Label" Visible="False"></asp:Label><asp:Label
        ID="lblNome1" runat="server" Text="Label"></asp:Label><asp:Label ID="lblNome2" runat="server"
            Text="Label"></asp:Label><asp:Label ID="lblNome3" runat="server" Text="Label"></asp:Label><asp:Label ID="lblDescricao2" runat="server" Text="Label" Visible="False"></asp:Label><asp:Label ID="lblDescricao3" runat="server" Text="Label" Visible="False"></asp:Label>
    <asp:Label ID="lblCaminho1" runat="server" Text="Label"></asp:Label>
    <asp:Label ID="lblCaminho2" runat="server" Text="Label"></asp:Label>
    <asp:Label ID="lblCaminho3" runat="server" Text="Label"></asp:Label></div>
</body>
</html>
