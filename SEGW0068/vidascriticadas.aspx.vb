Imports System.Web.UI.WebControls
Imports System.Data
Imports System.IO
Public Class VidasCriticadas
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblVigencia As System.Web.UI.WebControls.Label
    Protected WithEvents lblNavegacao As System.Web.UI.WebControls.Label
    Protected WithEvents lblListagemVidas As System.Web.UI.WebControls.Label
    Protected WithEvents lblListagemEnviada As System.Web.UI.WebControls.Label
    Protected WithEvents pnlAlerta As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlFormulario As System.Web.UI.WebControls.Panel
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button
    Protected WithEvents Image1 As System.Web.UI.WebControls.Image
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents grdPesquisa As System.Web.UI.WebControls.DataGrid
    Protected WithEvents gridMovimentacao As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents ucPaginacao As paginacaogrid
    Protected WithEvents lblSemUsuario As System.Web.UI.WebControls.Label
    Protected WithEvents lblQtdAceitos As System.Web.UI.WebControls.Label
    Protected WithEvents lblQtdRecusados As System.Web.UI.WebControls.Label
    Protected WithEvents lblCompetencia As System.Web.UI.WebControls.Label
    Protected WithEvents pageUnderContruction As System.Web.UI.WebControls.Panel
    Protected WithEvents criticaScript As System.Web.UI.WebControls.Literal
    Protected WithEvents btnProcessar As System.Web.UI.WebControls.Button
    Protected WithEvents lblIdadeMaxima As System.Web.UI.WebControls.Label
    Protected WithEvents lblIdadeMinima As System.Web.UI.WebControls.Label
    Protected WithEvents lblMultSalarial As System.Web.UI.WebControls.Label
    Protected WithEvents lblCapitalSegurado As System.Web.UI.WebControls.Label
    Protected WithEvents lblConjuge As System.Web.UI.WebControls.Label
    Protected WithEvents td_valor_capital As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents td_valor_capital_lbl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblLimiteMaximo As System.Web.UI.WebControls.Label
    Protected WithEvents LblLimiteMinimo As System.Web.UI.WebControls.Label
    Protected WithEvents lblValorCapital As System.Web.UI.WebControls.Label
    Protected WithEvents tdLimiteMaximo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdLimiteMinimo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents divBtnIncluiDependente As System.Web.UI.WebControls.Panel

    'rsouza --------------------------------------------------------'

    Protected WithEvents lbtLink1 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbtLink2 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbtLink3 As System.Web.UI.WebControls.LinkButton

    Protected WithEvents lblNome1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblNome2 As System.Web.UI.WebControls.Label
    Protected WithEvents lblNome3 As System.Web.UI.WebControls.Label

    Protected WithEvents lblCaminho1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblCaminho2 As System.Web.UI.WebControls.Label
    Protected WithEvents lblCaminho3 As System.Web.UI.WebControls.Label

    Protected WithEvents lblDescricao1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblDescricao2 As System.Web.UI.WebControls.Label
    Protected WithEvents lblDescricao3 As System.Web.UI.WebControls.Label
    'rsouza --------------------------------------------------------'



    'Protected WithEvents btnGerar As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim linkseguro As Alianca.Seguranca.Web.LinkSeguro

        'Implementa��o da SABL0101
        linkseguro = New Alianca.Seguranca.Web.LinkSeguro
        Dim usuario As String = linkseguro.LerUsuario("", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))
        If Request.QueryString("fatura") = "" Then
            If usuario = "" Then
                Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
            End If

            Session("usuario_id") = linkseguro.Usuario_ID
            Session("usuario") = linkseguro.Login_REDE
            Session("cpf") = linkseguro.CPF
        End If

        Dim cAmbiente As Alianca.Seguranca.Web.ControleAmbiente
        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
      
        cAmbiente.ObterAmbiente(url)
        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
        Else
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
        End If

        Alianca.Seguranca.BancoDados.cCon.ConnectionTimeout = Alianca.Seguranca.BancoDados.cCon.TimeTimeout.Grande

        Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)

        If Not Page.IsPostBack Then
            For Each m As String In Request.QueryString.Keys
                If m <> "" Then
                    Session(m) = Request.QueryString(m)
                End If
            Next
            Session("pagina") = 0
        End If

        Dim fatura As String = Request.QueryString("fatura")
        If fatura <> "" Then
            pnlAlerta.Visible = True
            pnlFormulario.Visible = False
        End If
        lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomeSubgrupo") & "<br>Fun��o: Resultado processamento arquivo"
        lblNavegacao.Visible = True
        lblVigencia.Text = "Per�odo de Compet�ncia: " & getPeriodoCompetenciaSession()
        lblCompetencia.Text = getPeriodoCompetenciaSession()

        Response.Write("<input type='hidden' value='" & Session("acesso") & "' id='ind_acesso' name='ind_acesso'>")

        'condi��es do subgrupo
        getCondicoesSubgrupo()

        Try
            'cUtilitarios.br(Request.Form("__EVENTTARGET"))
            If Request.Form("__EVENTTARGET") = "" Then
                If Not Request.Form("gerarXml") Is Nothing AndAlso Request.Form("gerarXml") <> "" Then
                    If Request.Form("gerarXml") = 1 Then
                        geraXml()
                        'cUtilitarios.escreveScript("document.Form1.gerarXml.value='';")
                        Response.End()
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.GetType.ToString <> "System.Threading.ThreadAbortException" Then
                Dim excp As New clsException(ex)
            End If
        End Try

        Dim acao = Request.QueryString("acao")

        If Not acao Is Nothing Then
            acao = acao.Split(",")(acao.Split(",").Length - 1)

            If acao = "1" Then


            ElseIf acao = "2" Then '' EXCLUIR UMA VIDA!!!
                Dim _id_cli = Undefned(Request.Form("_id_cli"))

                Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
                bd.SEGS7044_SPD(_id_cli, 1, Session("apolice"), Session("ramo"), Session("subgrupo_id"))
                ''Session("usuario")
                Try
                    bd.ExecutaSQL()
                Catch ex As Exception
                    Dim excp As New clsException(ex)
                End Try
            ElseIf acao = "3" Then '' EXCLUIR TODAS AS VIDAS!!!                
                Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
                bd.SEGS7044_SPD(-1, 1, Session("apolice"), Session("ramo"), Session("subgrupo_id"))
                ''Session("usuario")
                Try
                    bd.ExecutaSQL()
                Catch ex As Exception
                    Dim excp As New clsException(ex)
                End Try
            End If

        End If
        ''''''''''''''

        'rsouza -----------------'
        Call mConsultaLinksArqs()
        'rsouza -----------------'


        lblQtdAceitos.Text = Me.getQtdAceitos()
        lblQtdRecusados.Text = Me.getQtdRecusados()

        mConsultaRegistros()

        criticaScript.Text = retornaScriptCarregamentoCriticas()

        Me.btnProcessar.Enabled = VerificaPendentes()
    End Sub

    Private Sub geraXml()
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.GeraExcel(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

        Try
            Dim dt As Data.DataTable = bd.ExecutaSQL_DT

            ExportadorXLS.Exportar(dt)
            Exit Sub
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Dim indexMaxArray As Integer = 0

    Private Sub getCondicoesSubgrupo()

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dr As Data.SqlClient.SqlDataReader = Nothing

        bd.SEGS5705_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

        Try

            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    Me.lblIdadeMaxima.Text = IIf(dr.GetValue(1).ToString.Trim = "", " --- ", dr.GetValue(1).ToString.Trim)
                    Me.lblIdadeMinima.Text = IIf(dr.GetValue(0).ToString.Trim = "", " --- ", dr.GetValue(0).ToString.Trim)
                Else
                    Me.lblIdadeMaxima.Text = " --- "
                    Me.lblIdadeMinima.Text = " --- "
                End If
                If Not dr.IsClosed Then dr.Close()
            End If

            bd.SEGS5706_SPS(Session("apolice"), Session("ramo"), 6785, 0, Session("subgrupo_id"))

            dr = bd.ExecutaSQL_DR()
            If Not dr Is Nothing Then
                If dr.Read Then
                    Me.lblMultSalarial.Text = IIf(dr.GetValue(2).ToString.Trim = "", " --- ", dr.GetValue(2).ToString.Trim)
                    Me.lblCapitalSegurado.Text = IIf(dr.GetValue(1).ToString.Trim = "", " --- ", dr.GetValue(1).ToString.Trim)
                    Me.lblConjuge.Text = IIf(dr.GetValue(0).ToString.Trim = "", " --- ", dr.GetValue(0).ToString.Trim)
                Else
                    Me.lblMultSalarial.Text = " --- "
                    Me.lblCapitalSegurado.Text = " --- "
                    Me.lblConjuge.Text = " --- "
                End If

                If Me.lblCapitalSegurado.Text = "Capital Global" Then
                    Me.td_valor_capital.Visible = True
                    Me.td_valor_capital_lbl.Visible = True
                Else
                    Me.td_valor_capital.Visible = False
                    Me.td_valor_capital_lbl.Visible = False
                End If
                If Not dr.IsClosed Then dr.Close()
            End If

            bd.SEGS5744_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

            dr = bd.ExecutaSQL_DR()

            Dim valor_capital_global As String = "0"

            If Not dr Is Nothing Then
                If dr.Read Then
                    Me.lblLimiteMaximo.Text = IIf(dr.GetValue(0).ToString.Trim = "", " 0,00 ", cUtilitarios.trataMoeda(dr.GetValue(0).ToString.Trim))
                    Me.LblLimiteMinimo.Text = cUtilitarios.trataMoeda(cUtilitarios.getLimMinCapital())
                    If Me.LblLimiteMinimo.Text = 0 Or Me.LblLimiteMinimo.Text = "" Then
                        Me.LblLimiteMinimo.Text = "0,00"
                    End If
                    Me.lblValorCapital.Text = IIf(dr.GetValue(4).ToString.Trim = "", " --- ", cUtilitarios.trataMoeda(dr.GetValue(4).ToString.Trim))
                Else
                    Me.tdLimiteMaximo.Align = "center"
                    Me.lblLimiteMaximo.Text = " 0,00 "
                    Me.tdLimiteMinimo.Align = "center"
                    Me.LblLimiteMinimo.Text = " 0,00 "
                    Me.lblValorCapital.Text = " --- "
                End If

                ''Se capital fixo, entao limite min vai ser igual ao maximo [13/04/07]
                If Me.lblCapitalSegurado.Text = "Capital fixo" Then
                    Me.LblLimiteMinimo.Text = Me.lblLimiteMaximo.Text
                End If
                ''-----------------------------------------------------
                If Not dr.IsClosed Then dr.Close()
                dr = Nothing
            End If

            'If Me.lblConjuge.Text <> "C�njuge facultativo" Then
            '    Me.divBtnIncluiDependente.Visible = False
            'Else
            '    Me.divBtnIncluiDependente.Visible = True
            'End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
    End Sub

    Function ValoresSeguradoPost() As cSegurado

        '"Recupera��o dosa valores do formul�rio"
        Dim _id_cli As String = Undefned(Request.Form("_id_cli"))
        Dim crit1 As String = Undefned(Request.Form("crit1"))
        Dim crit2 As String = Undefned(Request.Form("crit2"))
        Dim crit2_1 As String = Undefned(Request.Form("crit2_1"))
        Dim crit2_2 As String = Undefned(Request.Form("crit2_2"))
        Dim crit3 As String = Undefned(Request.Form("crit3"))
        Dim crit4 As String = Undefned(Request.Form("crit4"))
        Dim crit5 As String = Undefned(Request.Form("crit5"))
        Dim crit6 As String = Undefned(Request.Form("crit6"))
        Dim crit7 As String = Undefned(Request.Form("crit7"))
        Dim crit8 As String = Undefned(Request.Form("crit8"))
        Dim crit9 As String = Undefned(Request.Form("crit9"))
        Dim crit10 As String = Undefned(Request.Form("crit10"))
        Dim crit11 As String = Undefned(Request.Form("crit11"))
        Dim crit12 As String = Undefned(Request.Form("crit12"))
        Dim crit12_1 As String = Undefned(Request.Form("crit12_1"))
        Dim crit13 As String = Undefned(Request.Form("crit13"))
        Dim crit14 As String = Undefned(Request.Form("crit14"))
        Dim crit14_1 As String = Undefned(Request.Form("crit14_1"))
        Dim crit14_2 As String = Undefned(Request.Form("crit14_2"))
        Dim crit15 As String = Undefned(Request.Form("crit15"))
        Dim crit15_1 As String = Undefned(Request.Form("crit15_1"))
        Dim crit16 As String = Undefned(Request.Form("crit16"))
        Dim crit17 As String = Undefned(Request.Form("crit17"))
        Dim crit18 As String = Undefned(Request.Form("crit18"))
        Dim crit19 As String = Undefned(Request.Form("crit19"))
        Dim crit20 As String = Undefned(Request.Form("crit20"))
        Dim crit21 As String = Undefned(Request.Form("crit21"))
        Dim crit22 As String = Undefned(Request.Form("crit22"))
        Dim crit24 As String = Undefned(Request.Form("crit24"))
        Dim crit25 As String = Undefned(Request.Form("crit25"))

        Dim segurado As New cSegurado

        Dim par(,) As String = { _
                                {crit1, "", "", "", "", ""}, _
                                {crit4, crit6, crit11, crit13, crit16, crit21}, _
                                {crit2, crit7, crit8, crit14, "", ""}, _
                                {crit5, "", "", "", "", ""}, _
                                {crit12, crit15, "", "", "", ""}, _
                                {crit12_1, crit15_1, "", "", "", ""}, _
                                {crit3, crit18, crit20, crit22, crit24, ""}, _
                                {crit17, crit19, crit25, "", "", ""} _
                               }
        Me.indexMaxArray = 6


        segurado.Id = _id_cli
        segurado.Nome = RetornaValorString(par, 0)
        segurado.Cpf = cUtilitarios.destrataCPF(RetornaValorString(par, 1))
        segurado.Dt_nasc = RetornaValorString(par, 2)
        segurado.Sexo = RetornaValorString(par, 3)
        segurado.Val_salario = RetornaValorString(par, 4)
        segurado.Val_capital_seguado = RetornaValorString(par, 5)
        segurado.Dt_desligamento = RetornaValorString(par, 6)
        segurado.Dt_inicio_vigencia_sbg = RetornaValorString(par, 7)

        Return segurado
    End Function

    Function RetornaValorString(ByVal colStr(,) As String, ByVal index As Integer) As String
        For i As Integer = 0 To Me.indexMaxArray - 1
            If colStr(index, i) <> "" Or colStr(index, i) <> Nothing Then
                Return colStr(index, i)
            End If
        Next
        Return ""
    End Function

    Function ValoresSeguradoBanco(ByVal id As String) As cSegurado

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.getArquivoVidaCriticado(id)

        Dim segurado As New cSegurado
        Dim dr As Data.SqlClient.SqlDataReader

        Try
            dr = bd.ExecutaSQL_DR()
            If dr.HasRows Then
                If dr.Read() Then
                    segurado.Nome = dr.Item("nome")
                    segurado.Cpf = dr.Item("cpf_titular")
                    segurado.Dt_nasc = dr.Item("dt_nascimento")
                    segurado.Sexo = dr.Item("sexo")
                    segurado.Val_capital_seguado = dr.Item("val_capital_segurado")
                    segurado.Val_salario = dr.Item("val_salario")
                    segurado.Dt_desligamento = dr.Item("dt_desligamento")

                    segurado.Controla_processo_id = dr.Item("controla_processo_id")
                    segurado.Seq_linha = dr.Item("seq_linha")
                    segurado.Dt_inicio_vigencia_sbg = dr.Item("dt_inicio_vigencia_sbg")
                    segurado.Ind_operacao = dr.Item("ind_operacao")
                    segurado.Cpf_conjuge = dr.Item("cpf_conjuge")
                End If
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return segurado
    End Function

    Function MesclaValoresSegurado(ByVal segPost As cSegurado, ByVal segBD As cSegurado) As cSegurado
        Dim retorno As New cSegurado

        retorno.Id = segPost.Id

        '' carregando informa��e que s�o recuperadas apenas no banco de dados
        retorno.Controla_processo_id = segBD.Controla_processo_id
        retorno.Seq_linha = segBD.Seq_linha
        retorno.Ind_operacao = segBD.Ind_operacao
        retorno.Cpf_conjuge = segBD.Cpf_conjuge

        '' mesclando infortma��es
        retorno.Nome = IIf(segPost.Nome = "", segBD.Nome, segPost.Nome)
        retorno.Cpf = IIf(segPost.Cpf = "", segBD.Cpf, segPost.Cpf)
        retorno.Dt_nasc = IIf(segPost.Dt_nasc = "", segBD.Dt_nasc, segPost.Dt_nasc)
        retorno.Sexo = IIf(segPost.Sexo = "", segBD.Sexo, segPost.Sexo)
        'retorno.Val_capital_seguado = CType(IIf(segPost.Val_capital_seguado = "", segBD.Val_capital_seguado, segPost.Val_capital_seguado), String).Replace(",", "")
        'retorno.Val_salario = CType(IIf(segPost.Val_salario = "", segBD.Val_salario, segPost.Val_salario), String).Replace(",", "")
        retorno.Val_capital_seguado = IIf(segPost.Val_capital_seguado = "", segBD.Val_capital_seguado, segPost.Val_capital_seguado)
        retorno.Val_salario = IIf(segPost.Val_salario = "", segBD.Val_salario, segPost.Val_salario)
        retorno.Dt_desligamento = IIf(segPost.Dt_desligamento = "", segBD.Dt_desligamento, segPost.Dt_desligamento)
        retorno.Dt_inicio_vigencia_sbg = IIf(segPost.Dt_inicio_vigencia_sbg = "", segBD.Dt_inicio_vigencia_sbg, segPost.Dt_inicio_vigencia_sbg)

        Return retorno
    End Function

    Function TrocapontoVirgula(ByVal val As String) As String
        val = val.Replace(",", "-")
        val = val.Replace(".", ",")
        val = val.Replace("-", ".")
        Return val
    End Function

    Function TrocaVirgulaponto(ByVal val As String) As String
        val = val.Replace(",", ".")
        Return val
    End Function

    Function retornaScriptCarregamentoCriticas()

        Dim script As String = ""
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.RetornaScriptJsCriticas()
        Dim dt As Data.DataTable

        Try
            dt = bd.ExecutaSQL_DT()

            For i As Integer = 0 To dt.Rows.Count - 1
                script &= dt.Rows(i)(0)
            Next

            script &= " "

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return "<script>var strCriticas = [" & script & "{C:''}];</script>"
    End Function

    Function FormataStringData(ByVal str As String)
        Dim retorno As String

        If str = "00:00:00" Then
            Return ""
        End If

        If str.Length < 6 Then
            retorno = ""
        Else
            retorno = IIf(str.Split("/")(0).Length = 1, "0" & str.Split("/")(0), str.Split("/")(0)) & "/" & IIf(str.Split("/")(1).Length = 1, "0" & str.Split("/")(1), str.Split("/")(1)) & "/" & str.Split("/")(2)
        End If

        Return retorno
    End Function

    Function Undefned(ByVal value As String)
        If value = "undefined" Then
            Return ""
        End If
        Return value
    End Function


    'rsouza ---------------------------------------------------------------------------------------------------------'
    Private Sub mConsultaLinksArqs()
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.buscaDadosArquivosPasta(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
        Try
            Dim dt As Data.DataTable = bd.ExecutaSQL_DT
            Dim cont As Integer = 0
            Dim Caminho As String

            lblDescricao1.Text = ""
            lblDescricao2.Text = ""
            lblDescricao3.Text = ""

            For Each drRow As DataRow In dt.Rows

                Caminho = drRow("path_arquivo").ToString()

                'Verifica se o arquivo existe. Se n�o existir, passa para o arquivo seguinte.
                If IO.File.Exists(Caminho.ToString()) Then

                    Select Case (cont)

                        '''''Monta o primeiro link de download.
                        Case 0

                            lblNome1.Text = drRow("nome_arquivo").ToString() 'drRow("path_arquivo").ToString().Replace("\\SISAB101\POST\vida_web\", "").Replace("\\SISAB051\post\vida_web\", "")
                            lblCaminho1.Text = drRow("path_arquivo").ToString()
                            lblDescricao1.Text = FormatDateTime(drRow("dt_inclusao"), DateFormat.ShortDate) + " - " + drRow("desc_historico").ToString() & " - Por: " & drRow("usuario_upload")

                            'Monta o segundo link de download.
                        Case 1

                            lblNome2.Text = drRow("nome_arquivo").ToString() 'drRow("path_arquivo").ToString().Replace("\\SISAB101\POST\vida_web\", "").Replace("\\SISAB051\post\vida_web\", "")
                            lblCaminho2.Text = drRow("path_arquivo").ToString()
                            lblDescricao2.Text = FormatDateTime(drRow("dt_inclusao"), DateFormat.ShortDate) + " - " + drRow("desc_historico").ToString() & " - Por: " & drRow("usuario_upload")

                            'Monta o terceiro link de download.
                        Case 2

                            lblNome3.Text = drRow("nome_arquivo").ToString() 'drRow("path_arquivo").ToString().Replace("\\SISAB101\POST\vida_web\", "").Replace("\\SISAB051\post\vida_web\", "")
                            lblCaminho3.Text = drRow("path_arquivo").ToString()
                            lblDescricao3.Text = FormatDateTime(drRow("dt_inclusao"), DateFormat.ShortDate) + " - " + drRow("desc_historico").ToString() & " - Por: " & drRow("usuario_upload")

                            'Sai do for ap�s a montagem dos tr�s links.
                        Case 3

                            Exit For

                    End Select

                    cont = cont + 1

                End If
            Next

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub
    'rsouza ----------------------------------------------------------------------------------------------------------'



    Private Sub mConsultaRegistros()
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.listaRegistros(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

        Try
            Dim dt As Data.DataTable = bd.ExecutaSQL_DT

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then

                Me.grdPesquisa.CurrentPageIndex = Session("pagina")
                Me.grdPesquisa.SelectedIndex = -1

                Me.ucPaginacao._valor = "2"

                Me.ucPaginacao.GridDataBind(Me.grdPesquisa, dt)
                Session("grdDescarte") = dt

                Me.lblSemUsuario.Visible = False
                ucPaginacao.Visible = True
                grdPesquisa.Visible = True
                'gridMovimentacao.Visible = True
            Else
                Session.Remove("indice")
                Session.Remove("grdDescarte")
                Me.lblSemUsuario.Text = "<br><br><br>Nenhum Registro encontrado.<br><br><br><br>"
                Me.lblSemUsuario.Visible = True

                ucPaginacao.Visible = False
                grdPesquisa.Visible = False
                gridMovimentacao.Visible = False
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Function getQtdRecusados() As Integer
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.buscaQtdAceitosRecusados(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
        Dim dr As Data.SqlClient.SqlDataReader

        Try
            dr = bd.ExecutaSQL_DR()
            If dr.HasRows Then
                If dr.Read() Then
                    Return dr.Item("numRecusados")
                End If
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return 0
    End Function

    Private Function getQtdAceitos() As Integer
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.buscaQtdAceitosRecusados(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

        Dim dr As Data.SqlClient.SqlDataReader

        Try
            dr = bd.ExecutaSQL_DR()
            If dr.HasRows Then
                If dr.Read() Then
                    Return dr.Item("numAceitos")
                End If
            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return 0
    End Function


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

    End Sub

    Private Function getPeriodoCompetenciaSession()
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
        Dim data_inicio, data_fim, retorno As String

        If dr.HasRows Then
            While dr.Read()

                data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)
                cUtilitarios.escreveScript("var dt_ini_comp = '" & data_inicio & "';")
                cUtilitarios.escreveScript("var dt_fim_comp = '" & data_fim & "';")
                cUtilitarios.escreveScript("var dt_fim_comp_2_meses = '" & CType(data_inicio, DateTime).AddMonths(-2) & "';")

            End While

            retorno = data_inicio & " a " & data_fim
        Else
            'Session.Abandon()
            'cUtilitarios.br("N�o existe nenhuma Compet�ncia de Fatura em aberto")
            'System.Web.HttpContext.Current.Response.Write("<script>parent.window.location=parent.window.location;</script>")
        End If
        dr.Close()
        dr = Nothing
        Return retorno
    End Function

    Private Function mInsereSeguradoTemporario(ByVal apolice As Integer, ByVal ramo As Integer, ByVal sub_grupo_id As Integer, ByVal nome As String, ByVal cpf As String, ByVal dt_nascimento As String, ByVal dt_admissao As String, ByVal sexo As String, ByVal val_capital_segurado As String, ByVal val_salario As String) As Integer

        Dim retorno As Integer = -1

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        bd.SEGS5655_SPS(apolice, ramo, 6785, 0, sub_grupo_id, cUtilitarios.destrataCPF(cpf), "null")

        Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

        If dr.Read Then

            If dr.GetValue(4).ToString = "Titular" And dr.GetValue(2).ToString = cUtilitarios.destrataCPF(cpf) Then
                cUtilitarios.br("J� existe um titular com o cpf informado.")
                retorno = 1
            Else
                bd.SEGS5656_SPI(0, 0, apolice, 0, 6785, ramo, sub_grupo_id, dt_admissao, nome, 1, cpf, dt_nascimento, sexo, val_capital_segurado, val_salario, "getDate()", Session("usuario"), "n", "n", 0)
                'cUtilitarios.br(bd.SQL & "-*-5<br>")
                'Response.End()

                Try
                    bd.ExecutaSQL()
                    Session("alterado") = 1
                    cUtilitarios.escreveScript("top.document.id_selecionado_SEGW0065 = 1;")
                    If IsPendente(cpf) = False Then
                        cUtilitarios.escreveScript("parent.parent.GB_showConfirm();")
                    Else
                        cUtilitarios.escreveScript("parent.parent.GB_showConfirmP();")
                    End If

                Catch ex As Exception
                    Dim excp As New clsException(ex)
                    'Response.Write(ex.ToString & ex.StackTrace)
                    'Response.Write("<!--" & bd.SQL & "-->")
                    'Response.End()
                End Try
                retorno = 2
            End If
        Else
            bd.SEGS5656_SPI(0, 0, apolice, 0, 6785, ramo, sub_grupo_id, dt_admissao, nome, 1, cpf, dt_nascimento, sexo, val_capital_segurado, val_salario, "getDate()", Session("usuario"), "n", "n", 0)

            Try

                bd.ExecutaSQL()
                Session("alterado") = 1
                cUtilitarios.escreveScript("top.document.id_selecionado_SEGW0065 = 1;")

                If IsPendente(cpf) = False Then
                    cUtilitarios.escreveScript("parent.parent.GB_showConfirm();")
                Else
                    cUtilitarios.escreveScript("parent.parent.GB_showConfirmP();")
                End If

                retorno = 3

            Catch ex As Exception
                Dim excp As New clsException(ex)
                'Response.Write(ex.ToString & ex.StackTrace)
                Response.Write("<!--" & bd.SQL & "-->")
                Response.End()
            End Try
        End If
        dr.Close()
        dr = Nothing

        Return retorno
    End Function

    Private Function IsPendente(ByVal cpf As String) As Boolean

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim result As Boolean = False

        bd.SEGS6570_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"))

        Try

            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            While dr.Read
                If dr.GetValue(2).ToString.Trim = cUtilitarios.destrataCPF(cpf.Trim) Then
                    result = True
                End If
            End While

            dr.Close()

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return result

    End Function

    Private Sub btnProcessar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcessar.Click
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        'Dim segBD As New cSegurado
        'segBD = ValoresSeguradoBanco(segPost.Id)
        'segBD.Controla_processo_id
        Try
            bd.SEGS7121_SPI(Session("apolice"), _
                            Session("ramo"), _
                            Session("subgrupo_id"), _
                            Session("usuario"))

            bd.ExecutaSQL()
        Catch ex As Exception
            Dim excp As New clsException(ex)
        Finally

        End Try

        lblQtdAceitos.Text = Me.getQtdAceitos()
        lblQtdRecusados.Text = Me.getQtdRecusados()

        mConsultaRegistros()
        Me.btnProcessar.Enabled = VerificaPendentes()

        Response.SetCookie(New System.Web.HttpCookie("reflesh", "1"))
    End Sub

    Function VerificaPendentes() As Boolean
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim dt As New Data.DataTable

        Try
            bd.VerificaRegistrosPendentes(Session("apolice"), _
                            Session("ramo"), _
                            Session("subgrupo_id"))

            dt = bd.ExecutaSQL_DT()
        Catch ex As Exception
            Dim excp As New clsException(ex)
        Finally

        End Try

        Return IIf(Convert.ToInt32(dt.Rows(0)(0)) > 0, True, False)

    End Function

    Private Sub ucPaginacao_ePaginaAlterada(ByVal Argumento As String) Handles ucPaginacao.ePaginaAlterada

        paginacaogrid.iNumPaginas = grdPesquisa.PageCount

        grdPesquisa.CurrentPageIndex = Session("pagina")

        grdPesquisa.DataBind()

        ''''''''''''''''''''''''''
        Dim tipo As String
        Dim cell As Web.UI.WebControls.DataGridItem
        Dim pai As Web.UI.WebControls.DataGridItem
        Dim count As Int16 = 0
        Dim pai_id As String = ""
        Dim count2 As Integer = 0
        Dim w As Integer = 1
        Dim t As Integer = 1
        Dim primeiro As Boolean = True
        Dim tamanho(10) As Integer
        Dim textoCell As String = ""
        tamanho(0) = 0
        tamanho(1) = 5
        tamanho(2) = 10
        tamanho(3) = 5
        tamanho(4) = 5

        tamanho(5) = 15
        tamanho(6) = 5
        tamanho(7) = 5
        tamanho(8) = 5
        tamanho(9) = 0

        Dim txtMax(10) As String
        txtMax(0) = ""
        txtMax(1) = ""
        txtMax(2) = ""
        txtMax(3) = ""
        txtMax(4) = ""

        txtMax(5) = ""
        txtMax(6) = ""
        txtMax(7) = ""
        txtMax(8) = ""
        txtMax(9) = ""


        If cUtilitarios.MostraColunaSalario() Then
            w = 1
        Else
            w = 0
        End If

        If cUtilitarios.MostraColunaCapital() Then
            t = 1
        Else
            t = 0
        End If

        If grdPesquisa.Items.Count > 0 Then

            For Each x As Web.UI.WebControls.DataGridItem In grdPesquisa.Items

                For i As Integer = 0 To x.Cells.Count - 1
                    x.Cells.Item(i).ID = "cell_" & i
                    x.Cells.Item(i).Wrap = False
                Next

                For i As Integer = 0 To x.Cells.Count - 1
                    textoCell = x.Cells.Item(i).Text.ToString.Replace("&atilde;", "a").Replace("&ccedil;", "c")

                    If tamanho(i) <= textoCell.Length Then
                        tamanho(i) = textoCell.Length
                        txtMax(i) = x.Cells.Item(i).Text
                    End If
                Next

                pai = x
                x.ID = count2
                pai_id = x.ID
                count2 += 1

                x.Cells.Item(0).HorizontalAlign = HorizontalAlign.Center
                x.Cells.Item(1).HorizontalAlign = HorizontalAlign.Center
                x.Cells.Item(2).HorizontalAlign = HorizontalAlign.Left
                x.Cells.Item(3).HorizontalAlign = HorizontalAlign.Center
                x.Cells.Item(3).HorizontalAlign = HorizontalAlign.Center

                x.Cells.Item(2).Text = x.Cells.Item(2).Text.ToUpper.Replace("&NBSP;", "")

                x.Cells.Item(3).Text = cUtilitarios.trataCPF(x.Cells.Item(3).Text)

                x.Cells.Item(7).Text = cUtilitarios.trataMoeda(x.Cells.Item(7).Text)

                x.Attributes.Add("onmouseover", "mO('#E8F1FF', this)")
                x.Attributes.Add("onmouseout", "mO('#FFFFFF', this)") 'lista_criticas   MontaFormulario(""" & x.Cells.Item(9).Text & """);
                ''x.Attributes.Add("onclick", "document.getElementById('lista_criticas').value = '" & x.Cells.Item(5).Text & "'; alteracoes('" & x.Cells.Item(0).Text & "', '1', '" & x.Cells.Item(1).Text & "','" + x.Cells.Item(0).Text + "', '" & x.ID & "', 'I')")
                x.Attributes.Add("onclick", "document.getElementById('lista_criticas').value = '" & x.Cells.Item(9).Text & "'; document.getElementById('_id_cli_').value = '" & x.Cells.Item(0).Text & "'; alteracoes('" & x.Cells.Item(0).Text & "', '1', '" & x.Cells.Item(1).Text & "','" + x.Cells.Item(0).Text + "', '" & x.ID & "', 'I')")

                If x.Cells.Item(1).Text = "Hist�rico" Then
                    x.Cells.Item(1).Text = "&nbsp;"
                End If

                If primeiro Then
                    primeiro = False
                    For i As Integer = 0 To x.Cells.Count - 1
                        x.Cells.Item(i).Text = "<div class='headerFakeDiv'id='headerFake" & i & "'></div>" & x.Cells.Item(i).Text
                    Next
                End If


            Next

            Dim z As Integer = 0
            Dim texto As String
            Dim tamMin(10) As Integer
            tamMin(0) = 6
            tamMin(1) = 12
            tamMin(2) = 40
            tamMin(3) = 16
            tamMin(4) = 10

            For Each m As String In txtMax
                Try
                    texto = txtMax(z).ToString.Trim.Replace(" ", "O").Replace("&atilde;", "a").Replace("&ccedil;", "c").Replace("-", "O").PadRight(tamMin(z), "O")
                Catch ex As Exception

                End Try

                Response.Write("<input type='hidden' value='" & texto & "' id='txtMax" & z & "'>" & vbCrLf)
                z = z + 1
            Next

        End If

    End Sub

    '-- rsouza -----------------------------------------------------------------------------------------------------'
    Protected Sub lbtLink1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbtLink1.Click
        Dim arquivo As FileInfo
        arquivo = New FileInfo(lblCaminho1.Text)


        Response.Clear()
        Response.AddHeader("Content-Disposition", "attachment; filename=" & lblNome1.Text)
        'Response.AddHeader("Content-Length", 100)
        Response.AddHeader("Content-Length", arquivo.Length.ToString)
        Response.ContentType = "Application/download"

        If IO.File.Exists(lblCaminho1.Text) Then
            Response.WriteFile(lblCaminho1.Text)
        Else
            Response.Write("O Arquivo " & lblNome1.Text & " n�o foi encontrado.")
        End If

        Response.End()

    End Sub

    Protected Sub lbtLink2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbtLink2.Click

        Dim arquivo As FileInfo
        arquivo = New FileInfo(lblCaminho2.Text)

        Response.Clear()
        Response.AddHeader("Content-Disposition", "attachment; filename=" & lblNome2.Text)
        'Response.AddHeader("Content-Length", 100)
        Response.AddHeader("Content-Length", arquivo.Length.ToString)

        Response.ContentType = "Application/download"

        If IO.File.Exists(lblCaminho2.Text) Then
            Response.WriteFile(lblCaminho2.Text)
        Else
            Response.Write("O Arquivo " & lblNome2.Text & " n�o foi encontrado.")
        End If

        Response.End()

    End Sub

    Protected Sub lbtLink3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbtLink3.Click
        Dim arquivo As FileInfo
        Response.Clear()
        arquivo = New FileInfo(lblCaminho3.Text)
        Response.AddHeader("Content-Disposition", "attachment; filename=" & lblNome3.Text)
        'Response.AddHeader("Content-Length", 100)
        Response.AddHeader("Content-Length", arquivo.Length.ToString)
        Response.ContentType = "Application/download"

        If IO.File.Exists(lblCaminho3.Text) Then
            Response.WriteFile(lblCaminho3.Text)
        Else
            Response.Write("O Arquivo " & lblNome3.Text & " n�o foi encontrado.")
        End If

        Response.End()

    End Sub
    '-- rsouza -----------------------------------------------------------------------------------------------------'
End Class