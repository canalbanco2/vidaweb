
Public Class cAcompanhamento

#Region "Heran�a"
    Inherits cBanco
#End Region

    'rsouza ----------------------------------------------------------------------------------------------------------'
    Public Sub buscaDadosArquivosPasta(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)

        SQL = "     SELECT path_arquivo " & vbCrLf
        SQL = SQL & "    , nome_arquivo " & vbCrLf
        SQL = SQL & "    , dt_importacao " & vbCrLf
        SQL = SQL & "    , desc_historico " & vbCrLf
        SQL = SQL & "    , usuario_upload = upload.usuario " & vbCrLf
        'SQL = SQL & "    , usuario_log    = log_usuario.usuario " & vbCrLf
        'SQL = SQL & "    , log_usuario.dt_inclusao " & vbCrLf
        SQL = SQL & "    , upload.dt_inclusao " & vbCrLf
        SQL = SQL & "    , log_acompanha_usuario_id " & vbCrLf
        SQL = SQL & " INTO #tmp " & vbCrLf
        SQL = SQL & " FROM vida_web_db.dbo.controla_processo_upload_tb upload     (nolock) " & vbCrLf
        SQL = SQL & " JOIN vida_web_db.dbo.log_acompanha_usuario_tb    log_usuario(nolock) " & vbCrLf
        SQL = SQL & "   ON upload.apolice_id         = log_usuario.apolice_id " & vbCrLf
        SQL = SQL & "  AND upload.ramo_id            = log_usuario.ramo_id " & vbCrLf
        SQL = SQL & "WHERE upload.apolice_id         =  " & apolice_id & vbCrLf
        SQL = SQL & "  AND upload.ramo_id            =  " & ramo_id & vbCrLf
        SQL = SQL & "  AND upload.sub_grupo_id            =  " & subgrupo_id & vbCrLf
		SQL = SQL & "  AND desc_historico            = 'Realizada a movimenta��o por arquivo' " & vbCrLf

        SQL = SQL & " SELECT path_arquivo " & vbCrLf
        SQL = SQL & "      , nome_arquivo " & vbCrLf
        SQL = SQL & "      , usuario_upload " & vbCrLf
        SQL = SQL & "      , desc_historico " & vbCrLf
        SQL = SQL & "      , dt_inclusao = MAX(dt_inclusao) " & vbCrLf
        SQL = SQL & "   INTO #tmp2" & vbCrLf
        SQL = SQL & "   FROM #tmp " & vbCrLf
        SQL = SQL & "  GROUP " & vbCrLf
        SQL = SQL & "     BY path_arquivo " & vbCrLf
        SQL = SQL & "      , nome_arquivo " & vbCrLf
        SQL = SQL & "      , desc_historico " & vbCrLf
        SQL = SQL & "      , usuario_upload " & vbCrLf
        SQL = SQL & "  ORDER " & vbCrLf
        SQL = SQL & "     BY dt_inclusao DESC " & vbCrLf


        SQL = SQL & " SELECT * " & vbCrLf
        SQL = SQL & "   FROM #tmp2 " & vbCrLf

        SQL = SQL & " DROP TABLE #tmp, #tmp2 "
    End Sub
    'rsouza -----------------------------------------------------------------------------------------------------------'


    Public Sub buscaQtdAceitosRecusados(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)
        SQL = "exec VIDA_WEB_DB..segs5803_sps "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @sub_grupo_id=" & subgrupo_id
        SQL &= ", @tipo = 0"
    End Sub

    Public Sub excluiRegistros(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)

        SQL = "exec VIDA_WEB_DB..segs5804_spu @cpf = null, @reg = null, @del = 1 "
        SQL &= ", @apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @sub_grupo_id=" & subgrupo_id

    End Sub

    Public Sub excluiRegistros(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal id As Integer)

        SQL = "exec VIDA_WEB_DB..segs5804_spu @cpf = null, @reg = " & id & ", @del = 1 "
        SQL &= ", @apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @sub_grupo_id=" & subgrupo_id


    End Sub

    Public Sub atualizaRegistros(ByVal cpf As String, ByVal reg As Integer, ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal usuario As String)

        SQL = "exec VIDA_WEB_DB..segs5804_spu @cpf = " & cpf & ", @reg = " & reg
        SQL &= ", @apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @sub_grupo_id=" & subgrupo_id
        SQL &= ", @usuario=" & usuario

    End Sub

    Public Sub listaRegistros(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)
        SQL = "exec VIDA_WEB_DB..segs5803_sps "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @sub_grupo_id=" & subgrupo_id
        SQL &= ", @tipo = 1"
    End Sub

    Public Sub getRegistro(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal id As Integer)
        SQL = "exec VIDA_WEB_DB..segs5803_sps "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @sub_grupo_id=" & subgrupo_id
        SQL &= ", @tipo = 1"
        SQL &= ", @id = " & id
    End Sub

    Public Sub GeraExcel(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)
        SQL = "exec VIDA_WEB_DB..segs5803_sps "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @sub_grupo_id=" & subgrupo_id
        SQL &= ", @tipo = 2"
    End Sub

    'Busca o Resumo da Fatura atual (anterior) da tela de abertura do Vida
    Public Sub ResumoFaturaAtual_Anterior(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal usuario As String, ByVal wf_id As Int64)

        'Busca Resumo da Fatura Atual Anterior
        SQL = "exec VIDA_WEB_DB..SEGS5732_SPS "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @subgrupo_id=" & subgrupo_id
        SQL &= ", @usuario=" & trStr(usuario)
        SQL &= ", @wf_id = " & wf_id

    End Sub
    Public Sub getCapitalFaixaEtaria(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal dtInicioVigencia As String, ByVal dt_nascimento As String)

        SQL = "exec VIDA_WEB_DB..SEGS5782_SPS "
        SQL &= " @lApoliceId=" & apolice_id
        SQL &= ", @iRamoId=" & ramo_id
        SQL &= ", @iSubGrupoId=" & subgrupo_id
        SQL &= ", @dtInicioVigencia=" & dtInicioVigencia
        SQL &= ", @dt_nascimento=" & dt_nascimento

    End Sub

    'Retorna o per�odo da ap�lice
    Public Sub GetDadosSubgrupoE1(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)

        SQL = "set nocount on exec VIDA_WEB_DB..SEGS5751_SPS "
        SQL &= "  @apolice_id=" & trInt(apolice_id)
        SQL &= ", @ramo_id=" & trInt(ramo_id)
        SQL &= ", @subgrupo_id=" & trInt(subgrupo_id)


    End Sub

    'Busca o Resumo da Fatura atual (Inclusoes, Altera��es e Exclusoes) da tela de abertura do Vida
    Public Sub ResumoFaturaAtual_IAE(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer, ByVal usuario As String, ByVal wf_id As Int64)

        'Busca Resumo da Fatura Atual: IAE
        SQL = "exec VIDA_WEB_DB..SEGS5733_SPS "
        SQL &= "@apolice_id=" & apolice_id
        SQL &= ", @ramo_id=" & ramo_id
        SQL &= ", @subgrupo_id=" & subgrupo_id
        SQL &= ", @usuario=" & trStr(usuario)
        SQL &= ", @wf_id = " & wf_id

    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String)

        SQL = "exec VIDA_WEB_DB..SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @cpf = '" & cpf & "'"
        SQL &= ", @nome = " & nome

    End Sub

    Public Sub SEGS5696_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal workflow As Integer, ByVal valor_id As Integer)
        SQL = "exec segs5696_sps  @apolice_id = " & apolice_id
        SQL &= ", @subgrupo_id = " & subgrupo
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @tp_wf_id = " & workflow
        SQL &= ", @tp_versao_id = " & valor_id
    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String, ByVal id As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @cpf = " & cpf
        SQL &= ", @nome = " & nome
        SQL &= ", @id = " & id
    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String, ByVal id As String, ByVal tipo As String)

        SQL = "exec VIDA_WEB_DB..SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @cpf = " & cpf
        SQL &= ", @nome = " & nome
        SQL &= ", @id = " & id
        SQL &= ", @tipo = " & tipo

    End Sub

    Public Sub SEGS5655_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal seg_cod_susep As Integer, ByVal suc_seg_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String, ByVal nome As String, ByVal id As String, ByVal tipo As String, ByVal flag As String)

        SQL = "exec VIDA_WEB_DB..SEGS5655_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seg_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & suc_seg_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @cpf = " & cpf
        SQL &= ", @nome = " & nome
        SQL &= ", @id = " & id
        SQL &= ", @tipo = " & tipo
        SQL &= ", @flag = " & flag

    End Sub

    Public Sub SEGS5658_SPD(ByVal id As Integer, ByVal dt As String)
        SQL = "exec VIDA_WEB_DB..SEGS5658_SPD "
        SQL &= " @id = " & id
        SQL &= ", @dtDesligamento  = " & "'" & cUtilitarios.trataDataDB(dt) & "'"
    End Sub

    Public Sub SEGS5657_SPU(ByVal id As Integer, ByVal seq_cliente_id As Integer, ByVal seq_faturamento As Integer, ByVal apolice_id As Integer, ByVal sucursal_seguradora_id As Integer, ByVal seguradora_cod_susep As Integer, ByVal ramo_id As Integer, ByVal sub_grupo_id As Integer, ByVal dt_inicio_vigencia_sbg As String, ByVal nome As String, ByVal tp_componente_id As Integer, ByVal cpf As String, ByVal dt_nascimento As String, ByVal sexo As String, ByVal val_capital_segurado As String, ByVal val_salario As String, ByVal dt_inclusao As String, ByVal usuario As String, ByVal pendente As String, ByVal permanente As String, ByVal alteracoes As String)
        apolice_id = Int(apolice_id)
        ramo_id = Int(ramo_id)
        seguradora_cod_susep = Int(seguradora_cod_susep)
        sucursal_seguradora_id = Int(sucursal_seguradora_id)
        sub_grupo_id = Int(sub_grupo_id)
        nome = "'" & nome & "'"
        cpf = "'" & cUtilitarios.destrataCPF(cpf) & "'"
        dt_nascimento = "'" & cUtilitarios.trataDataDB(dt_nascimento) & "'"
        sexo = "'" & sexo.Substring(0, 1) & "'"
        seq_cliente_id = Int(seq_cliente_id)
        tp_componente_id = Int(tp_componente_id)
        dt_inicio_vigencia_sbg = "'" & cUtilitarios.trataDataDB(dt_inicio_vigencia_sbg) & "'"
        usuario = "'" & usuario & "'"
        val_capital_segurado = val_capital_segurado.Replace(",", ".")
        val_salario = val_salario.Replace(",", ".")
        val_capital_segurado = val_capital_segurado.Replace(",", ".")

        SQL = "exec VIDA_WEB_DB..SEGS5657_SPU @id=" & id
        SQL &= ",  @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seguradora_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & sucursal_seguradora_id
        SQL &= ", @sub_grupo_id = " & sub_grupo_id
        SQL &= ", @nome = " & nome
        SQL &= ", @cpf = " & cpf
        SQL &= ", @dt_nascimento = " & dt_nascimento
        SQL &= ", @sexo = " & sexo
        SQL &= ", @val_capital_segurado = " & val_capital_segurado
        SQL &= ", @val_salario = " & val_salario
        SQL &= ", @seq_faturamento = " & seq_faturamento
        SQL &= ", @seq_cliente_id = " & seq_cliente_id
        SQL &= ", @tp_componente_id = " & tp_componente_id
        SQL &= ", @dt_inicio_vigencia_sbg = " & dt_inicio_vigencia_sbg
        SQL &= ", @pendente = " & pendente
        SQL &= ", @permanente = " & permanente
        SQL &= ", @usuario = " & usuario & " "
        SQL &= ", @alteracoes = '" & alteracoes & "'"


    End Sub

    Public Sub SEGS5656_SPI(ByVal seq_cliente_id As Integer, ByVal seq_faturamento As Integer, ByVal apolice_id As Integer, ByVal sucursal_seguradora_id As Integer, ByVal seguradora_cod_susep As Integer, ByVal ramo_id As Integer, ByVal sub_grupo_id As Integer, ByVal dt_inicio_vigencia_sbg As String, ByVal nome As String, ByVal tp_componente_id As Integer, ByVal cpf As String, ByVal dt_nascimento As String, ByVal sexo As String, ByVal val_capital_segurado As String, ByVal val_salario As String, ByVal dt_inclusao As String, ByVal usuario As String, ByVal pendente As String, ByVal permanente As String, ByVal id As Integer)

        apolice_id = Int(apolice_id)
        ramo_id = Int(ramo_id)
        seguradora_cod_susep = Int(seguradora_cod_susep)
        sucursal_seguradora_id = Int(sucursal_seguradora_id)
        sub_grupo_id = Int(sub_grupo_id)
        nome = "'" & nome & "'"
        cpf = "'" & cUtilitarios.destrataCPF(cpf) & "'"
        dt_nascimento = "'" & cUtilitarios.trataDataDB(dt_nascimento) & "'"
        sexo = "'" & sexo.Substring(0, 1) & "'"
        seq_cliente_id = Int(seq_cliente_id)
        tp_componente_id = Int(tp_componente_id)
        dt_inicio_vigencia_sbg = "'" & cUtilitarios.trataDataDB(dt_inicio_vigencia_sbg) & "'"
        usuario = "'" & usuario & "'"
        val_salario = val_salario.Replace(",", ".")
        val_capital_segurado = val_capital_segurado.Replace(",", ".")

        SQL = "exec VIDA_WEB_DB..SEGS5656_SPI "
        SQL &= "  @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & seguradora_cod_susep
        SQL &= ", @sucursal_seguradora_id = " & sucursal_seguradora_id
        SQL &= ", @sub_grupo_id = " & sub_grupo_id
        SQL &= ", @nome = " & nome
        SQL &= ", @cpf = " & cUtilitarios.destrataCPF(cpf)
        SQL &= ", @dt_nascimento = " & dt_nascimento
        SQL &= ", @sexo = " & sexo
        SQL &= ", @val_capital_segurado = " & val_capital_segurado
        SQL &= ", @val_salario = " & val_salario
        SQL &= ", @seq_faturamento = " & seq_faturamento
        SQL &= ", @seq_cliente_id = " & seq_cliente_id
        SQL &= ", @tp_componente_id = " & tp_componente_id
        SQL &= ", @dt_inicio_vigencia_sbg = " & dt_inicio_vigencia_sbg
        SQL &= ", @pendente = " & pendente
        SQL &= ", @permanente = " & permanente
        SQL &= ", @usuario = " & usuario & " "
        SQL &= ", @idAnterior = " & id & " "
    End Sub
    '''Consulta as condi��es do subgrupo
    Public Sub SEGS5705_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5705_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub
    '''Consulta as condi��es do subgrupo
    Public Sub SEGS5706_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5706_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub
    '''Consulta as condi��es do subgrupo (valor lim�te m�ximo)
    Public Sub SEGS5744_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5744_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub
    '''Consulta as condi��es do subgrupo SEGS5750_SPS
    Public Sub coberturas(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer, ByVal componente As String)

        SQL = "exec VIDA_WEB_DB..SEGS5750_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @componente = " & componente
        SQL &= ", @ind_acesso = 0"
        SQL &= ", @cpf_id = '0'"
        SQL &= ", @usuario = 0"
        SQL &= ", @seguradora_cod_susep = 6785"
        SQL &= ", @sucursal_seguradora_id = 0 "

    End Sub

    Public Sub getDataIniVigenciaApolice(ByVal apolice_id As String, ByVal ramo_id As String)
        SQL = "select convert(varchar,dt_inicio_vigencia,103) as data "
        SQL &= " from seguros_db..apolice_tb"
        SQL &= " where apolice_id = " + apolice_id + " and"
        SQL &= " ramo_id = " + ramo_id + " and "
        SQL &= " sucursal_seguradora_id = 0 and"
        SQL &= " seguradora_cod_susep = 6785"

    End Sub

    Public Sub getTpApoliceConjuge(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As String)
        SQL = "select isnull(tp_clausula_conjuge,'N')"
        SQL &= " from seguros_db..sub_grupo_apolice_tb"
        SQL &= " where apolice_id = " & apolice_id & " and"
        SQL &= " sucursal_seguradora_id = 0 and"
        SQL &= " seguradora_cod_susep = 6785 and"
        SQL &= " ramo_id = " & ramo_id & " and"
        SQL &= " sub_grupo_id = " & subgrupo_id
    End Sub

    Public Sub SEGS5744_SPS(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As String)
        SQL = "exec VIDA_WEB_DB..SEGS5744_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @cod_susep = 6785"
        SQL &= ", @seguradora_id = 0"
        SQL &= ", @subgrupo_id = " & subgrupo_id
        SQL &= ", @usuario = ''"
        SQL &= ", @ind_acesso = ''"
        SQL &= ", @cpf_id = ''"
    End Sub

    Public Sub SEGS5773_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal cod_susep As Integer, ByVal seguradora_id As Integer, ByVal subgrupo As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS5773_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @seguradora_cod_susep = " & cod_susep
        SQL &= ", @sucursal_seguradora_id = " & seguradora_id
        SQL &= ", @subgrupo_id = " & subgrupo
    End Sub

    Public Sub getCapitalConjuge(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As String)

        SQL = "Select isnull(a.perc_basica, 0) / 100"
        SQL &= " FROM seguros_db..escolha_sub_grp_tp_cob_comp_tb a(NOLOCK)"
        SQL &= " INNER JOIN seguros_db..tp_cob_comp_tb cc (NOLOCK)"
        SQL &= " ON cc.tp_cob_comp_id = a.tp_cob_comp_id"
        SQL &= " WHERE a.apolice_id = " & apolice_id
        SQL &= " AND a.sucursal_seguradora_id = 0"

        SQL &= " AND a.seguradora_cod_susep = 6785"
        SQL &= " AND a.ramo_id = " & ramo_id
        SQL &= " AND a.sub_grupo_id = " & subgrupo_id
        SQL &= " AND isnull(a.class_tp_cobertura, 'a') = 'b'"
        SQL &= " AND cc.tp_componente_id = 3"

    End Sub


    Public Sub getLmtCapValIs(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As String)

        SQL = "Select Case val_lim_cap_global = isnull(VAL_LIM_MAX_IS, 0),  val_is_basica = isnull(escolha_sub_grp_tp_cob_comp_tb.val_is, 0), APOLICE_ID "
        SQL &= " FROM seguros_db..escolha_sub_grp_tp_cob_comp_tb a(NOLOCK)"
        SQL &= " INNER JOIN seguros_db..tp_cob_comp_tb cc (NOLOCK)"
        SQL &= " ON cc.tp_cob_comp_id = a.tp_cob_comp_id"
        SQL &= " WHERE a.apolice_id = " & apolice_id
        SQL &= " AND a.ramo_id = " & ramo_id
        SQL &= " AND a.sub_grupo_id = " & subgrupo_id
        SQL &= " AND isnull(a.class_tp_cobertura, 'a') = 'b'"
        SQL &= " AND cc.apolice_id = a.apolice_id "

    End Sub

    Public Sub getCountSegTempTb(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As String, ByVal dt_ini_vig_fat As String, ByVal dt_fim_vig_fat As String)

        SQL = "SELECT COUNT(*)�as num_seg� "
        SQL &= " FROM VIDA_WEB_DB..segurado_temporario_web_tb "
        SQL &= " WHERE apolice_id = " & apolice_id
        SQL &= "    AND ramo_id = " & ramo_id
        SQL &= "    AND sub_grupo_id = " & subgrupo_id
        SQL &= "    AND dt_inicio_vigencia_sbg <= " & dt_fim_vig_fat
        SQL &= "    AND (dt_fim_vigencia_sbg is null "
        SQL &= "      OR dt_fim_vigencia_sbg >= " & dt_ini_vig_fat & " ) "
        SQL &= "    AND pendente = 'n'� "
        SQL &= "    AND recusado = 'n' "

    End Sub

    Public Sub SobreposicaoLayout(ByVal apolice_id As String, ByVal ramo_id As String, ByVal subgrupo_id As String)

        SQL = " Select sobreposicao "
        SQL &= " FROM 	layout_arquivo_vida_tb LAYOUT "
        SQL &= " WHERE LAYOUT.apolice_id = " & apolice_id
        SQL &= " AND LAYOUT.ramo_id = " & ramo_id
        SQL &= " AND LAYOUT.subgrupo_id = " & subgrupo_id
        SQL &= " AND LAYOUT.dt_fim_vigencia IS NULL"

    End Sub

    Public Sub SEGS6570_SPS(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer)
        SQL = "exec VIDA_WEB_DB..SEGS6570_SPS "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @sub_grupo_id = " & subgrupo
    End Sub

    Public Sub getArquivoVidaCriticado(ByVal arquivo_vida_criticada_id As String)

        SQL = "SELECT CONTROLA_PROCESSO_ID, SEQ_LINHA, NOME, CPF_TITULAR, DT_NASCIMENTO, DT_INICIO_VIGENCIA_SBG, SEXO, VAL_CAPITAL_SEGURADO, VAL_SALARIO, DT_DESLIGAMENTO, IND_OPERACAO, 'CPF_CONJUGE' = ISNULL(CPF_CONJUGE, '') FROM ARQUIVO_VIDA_CRITICADA_TB"
        SQL &= " where arquivo_vida_criticada_id = " & arquivo_vida_criticada_id

    End Sub

    Public Sub SEGS7044_SPD(ByVal arquivo_vida_critica_id As Integer, ByVal flag_operacao As Integer, ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo_id As Integer)

        SQL = "exec VIDA_WEB_DB..SEGS7044_SPD "
        SQL &= " @arquivo_vida_criticada_id = " & arquivo_vida_critica_id
        SQL &= IIf(Not flag_operacao = -1, ", @FLAG_OPERACAO = " & flag_operacao, "")
        SQL &= IIf(Not apolice_id = -1, ", @APOLICE_ID = " & apolice_id, "")
        SQL &= IIf(Not ramo_id = -1, ", @RAMO_ID = " & ramo_id, "")
        SQL &= IIf(Not subgrupo_id = -1, ", @SUBGRUPO_ID = " & subgrupo_id, "")

    End Sub

    'Public Sub getDtInicioVigenciaApolice(ByVal apolice_id As Integer, ByVal ramo_id As Integer)
    '    SQL = "SELECT DT_INICIO_VIGENCIA FROM SEGUROS_DB..APOLICE_TB WHERE APOLICE_ID = " & apolice_id & " AND RAMO_ID = " & ramo_id
    'End Sub

    Public Sub verificaCpfAtivo(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal cpf As String)
        SQL = "SELECT count(*) "
        SQL &= "  FROM seguros_db..fatura_segurado_tb fatura_segurado_tb   "
        SQL &= " INNER JOIN seguros_db..pessoa_fisica_tb pessoa_fisica_tb  "
        SQL &= "    ON pessoa_fisica_tb.pf_cliente_id = fatura_segurado_tb.prop_cliente_id  "
        SQL &= "   AND pessoa_fisica_tb.cpf = '" & cpf & "'  "
        SQL &= "        WHERE fatura_segurado_tb.apolice_id = " & apolice_id
        SQL &= "   AND fatura_segurado_tb.ramo_id = " & ramo_id
        SQL &= "   AND fatura_segurado_tb.fatura_id = (SELECT MAX(fatura_tb.fatura_id)   "
        SQL &= "                                         FROM seguros_db..fatura_tb fatura_tb  "
        SQL &= "                                        WHERE fatura_tb.apolice_id = " & apolice_id
        SQL &= "                                          AND fatura_tb.ramo_id = " & ramo_id
        SQL &= "                                          AND fatura_tb.sub_grupo_id = " & subgrupo
        SQL &= "                                          AND fatura_tb.situacao <> 'C')  "
        SQL &= "   AND fatura_segurado_tb.operacao IN ('I', 'N')"
    End Sub


    Public Sub retornaDataFimVigencia(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer)
        SQL = " SELECT apolice_workflow_tb.dt_fim_vigencia_apl"
        SQL &= "  FROM seguros_db..apolice_workflow_tb apolice_workflow_tb  "
        SQL &= "        WHERE(apolice_workflow_tb.apolice_id = " & apolice_id & ")"
        SQL &= "   AND apolice_workflow_tb.ramo_id = " & ramo_id
        SQL &= "   AND apolice_workflow_tb.subgrupo_id = " & subgrupo
        SQL &= "   AND apolice_workflow_tb.tp_wf_id = 209 "
        SQL &= "   AND apolice_workflow_tb.tp_versao_id = 1  "
        SQL &= "   AND apolice_workflow_tb.wf_id IN (SELECT MAX(aw.wf_id)     "
        SQL &= "                                       FROM seguros_db..apolice_workflow_tb aw,   "
        SQL &= "                                            [sisab003].workflow_db.dbo.atividade_tb a    "
        SQL &= "                                        WHERE(a.tp_ativ_id = 2)"
        SQL &= "                                        AND aw.wf_id = a.wf_id    "
        SQL &= "                                        AND aw.tp_wf_id = apolice_workflow_tb.tp_wf_id    "
        SQL &= "                                        AND aw.tp_versao_id = apolice_workflow_tb.tp_versao_id    "
        SQL &= "                                        AND aw.apolice_id = apolice_workflow_tb.apolice_id    "
        SQL &= "                                        AND aw.subgrupo_id = apolice_workflow_tb.subgrupo_id    "
        SQL &= "                                        AND aw.ramo_id = apolice_workflow_tb.ramo_id)"
    End Sub

    Public Sub SEGS7048_SPI(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal usuario As String)
        SQL = "exec VIDA_WEB_DB..SEGS7048_SPI "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @usuario = '" & usuario & "'"
    End Sub

    Public Sub SEGS7121_SPI(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal usuario As String)
        SQL = "exec VIDA_WEB_DB..SEGS7121_SPI "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @usuario = '" & usuario & "'"
    End Sub

    Public Sub SEGS7053_SPU(ByVal apolice_id As Integer, _
                            ByVal ramo_id As Integer, _
                            ByVal subgrupo As Integer, _
                            ByVal ARQUIVO_VIDA_CRITICADA_ID As Integer, _
                            ByVal CONTROLA_PROCESSO_ID As Integer, _
                            ByVal SEQ_LINHA As Integer, _
                            ByVal NOME As String, _
                            ByVal CPF As String, _
                            ByVal DT_NASCIMENTO As String, _
                            ByVal SEXO As String, _
                            ByVal VAL_CAPITAL_SEGIRADO As String, _
                            ByVal VAL_SALARIO As String, _
                            ByVal DT_INICIO_VIGENCIA_SBG As String, _
                            ByVal DT_DESLIGAMENTO As String, _
                            ByVal IND_OPERACAO As String, _
                            ByVal CPF_CONJUGE As String, _
                            ByVal usuario As String)

        SQL = "exec VIDA_WEB_DB..SEGS7053_SPU "
        SQL &= "@APOLICE_ID = " & apolice_id
        SQL &= ",@RAMO_ID = " & ramo_id
        SQL &= ",@SUB_GRUPO_ID = " & subgrupo
        SQL &= ",@ARQUIVO_VIDA_CRITICADA_ID = " & ARQUIVO_VIDA_CRITICADA_ID
        SQL &= ",@CONTROLA_PROCESSO_ID = " & CONTROLA_PROCESSO_ID
        SQL &= ",@SEQ_LINHA = " & SEQ_LINHA
        SQL &= ",@NOME = '" & NOME
        SQL &= "' ,@CPF = '" & CPF
        SQL &= "',@DT_NASCIMENTO = '" & DT_NASCIMENTO
        SQL &= "' ,@SEXO = '" & SEXO
        SQL &= "',@VAL_CAPITAL_SEGURADO = '" & VAL_CAPITAL_SEGIRADO
        SQL &= "',@VAL_SALARIO = '" & VAL_SALARIO
        SQL &= "',@DT_INICIO_VIGENCIA_SBG = '" & DT_INICIO_VIGENCIA_SBG
        SQL &= "' ,@DT_DESLIGAMENTO = '" & DT_DESLIGAMENTO
        SQL &= "' ,@IND_OPERACAO = '" & IND_OPERACAO
        SQL &= "',@CPF_CONJUGE = '" & CPF_CONJUGE
        SQL &= "' ,@USUARIO = '" & usuario & "'"
    End Sub

    Public Sub SEGS5913_SPI(ByVal apolice_id As Integer, _
                            ByVal ramo_id As Integer, _
                            ByVal subgrupo As Integer, _
                            ByVal CONTROLA_PROCESSO_ID As Integer, _
                            ByVal usuario As String)

        SQL = "exec VIDA_WEB_DB..SEGS5913_SPI "
        SQL &= "@APOLICE_ID = " & apolice_id
        SQL &= ",@RAMO_ID = " & ramo_id
        SQL &= ",@SUB_GRUPO_ID = " & subgrupo
        SQL &= ",@CONTROLA_PROCESSO_ID = " & CONTROLA_PROCESSO_ID
        SQL &= ",@USUARIO = '" & usuario & "'"
    End Sub


    Public Sub RetornaScriptJsCriticas()
        ''strCriticas = [
        SQL = "select '	{C:''' + motivo_critica + '''},' from motivo_critica_tb"
        '']; 
    End Sub

    Public Sub RetornaCriticas()
        SQL = "select motivo_critica_id, motivo_critica from motivo_critica_tb"
    End Sub

    Public Sub retornaDataFimVigenciaUltFatAtiva(ByVal apolice_id As Integer, _
                        ByVal ramo_id As Integer, _
                        ByVal subgrupo As Integer)

        SQL = "SELECT fatura_tb.dt_inicio_vigencia, "
        SQL &= "fatura_tb.dt_fim_vigencia "
        SQL &= "FROM seguros_db..fatura_tb fatura_tb "
        SQL &= "WHERE(fatura_tb.apolice_id = " & apolice_id & " )"
        SQL &= "AND fatura_tb.sub_grupo_id = " & subgrupo & " "
        SQL &= "AND fatura_tb.ramo_id = " & ramo_id & " "
        SQL &= "AND fatura_tb.fatura_id = ( "
        SQL &= "                   Select MAX(fatura_tb.fatura_id) "
        SQL &= "                     FROM seguros_db..fatura_tb fatura_tb "
        SQL &= "                    WHERE (fatura_tb.apolice_id = " & apolice_id & " ) "
        SQL &= "                      AND fatura_tb.ramo_id = " & ramo_id & " "
        SQL &= "                      AND fatura_tb.sub_grupo_id = " & subgrupo & " "
        SQL &= "                      AND fatura_tb.situacao <> 'C' "
        SQL &= "                    ) "
    End Sub


    Public Sub SelectCritica24(ByVal apolice_id As Integer, _
                               ByVal ramo_id As Integer, _
                               ByVal subgrupo As Integer, _
                               ByVal dt As String, _
                               ByVal id As String)

        SQL = "SELECT count(*) "
        SQL &= "  FROM SEGURADO_TEMPORARIO_WEB_TB "
        SQL &= " WHERE APOLICE_ID = " & apolice_id & " "
        SQL &= "   AND RAMO_ID = " & ramo_id & " "
        SQL &= "   AND SUB_GRUPO_ID = " & subgrupo & " "
        SQL &= "   AND DT_TRANSFERENCIA IS NULL "
        SQL &= "   AND CPF = ("
        SQL &= "                 SELECT CPF_TITULAR "
        SQL &= "                   FROM ARQUIVO_VIDA_CRITICADA_TB "
        SQL &= "                  WHERE APOLICE_ID = " & apolice_id & " "
        SQL &= "                    AND RAMO_ID = " & ramo_id & " "
        SQL &= "                    AND SUB_GRUPO_ID = " & subgrupo & " "
        SQL &= "                    AND ARQUIVO_VIDA_CRITICADA_ID = " & id & " "
        SQL &= "              ) "
        SQL &= "   AND DT_ENTRADA_SBG >= '" & dt & "' "
        ''SQL &= "   AND IND_OPERACAO IN ('P', 'I')"

    End Sub

    Public Sub SelectCritica16(ByVal apolice_id As Integer, _
                               ByVal ramo_id As Integer, _
                               ByVal subgrupo As Integer, _
                               ByVal cpf As String)

        SQL = "SELECT COUNT(*) "
        SQL &= "FROM SEGURADO_TEMPORARIO_WEB_TB "
        SQL &= "WHERE APOLICE_ID = " & apolice_id & " "
        SQL &= "AND RAMO_ID = " & ramo_id & " "
        SQL &= "AND SUB_GRUPO_ID = " & subgrupo & " "
        SQL &= "AND DT_TRANSFERENCIA IS NULL "
        SQL &= "AND CPF = '" & cpf & "' "

    End Sub

    Public Sub Recalcula_Resumo_Operacoes(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer, ByVal usuario As String)
        SQL = "exec VIDA_WEB_DB..SEGS7048_SPI "
        SQL &= " @apolice_id = " & apolice_id
        SQL &= ", @ramo_id = " & ramo_id
        SQL &= ", @sub_grupo_id = " & subgrupo
        SQL &= ", @usuario = '" & usuario & "'"
    End Sub

    Public Sub VerificaRegistrosPendentes(ByVal apolice_id As Integer, ByVal ramo_id As Integer, ByVal subgrupo As Integer)
        SQL = " SELECT count(1) "
        SQL &= "  FROM arquivo_bulk_hist_tb "
        SQL &= " WHERE corrigido Is Not NULL "
        SQL &= "   AND apolice_id = " & apolice_id
        SQL &= "   AND ramo_id = " & ramo_id
        SQL &= "   AND subgrupo_id = " & subgrupo
    End Sub

    Public Sub New(Optional ByVal banco As cDadosBasicos.eBanco = cDadosBasicos.eBanco.VIDA_WEB_DB)
        MyBase.New(banco)
        Alianca.Seguranca.BancoDados.cCon.BancodeDados = banco.ToString

    End Sub

    Function trInt(ByVal valor As Object) As String

        If valor = 0 Then
            Return "null"
        Else
            Return valor
        End If

    End Function

    Function trStr(ByVal valor As Object) As String

        If valor = "" Then
            Return "null"
        Else
            Return "'" & valor.ToString.Replace("'", "''") & "'"
        End If

    End Function

End Class


