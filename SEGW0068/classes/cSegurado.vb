Public Class cSegurado
    Private _id As String

    Private _operacao As String
    Private _nome As String
    Private _cpf As String
    Private _dt_nasc As String
    Private _dt_admissao As String
    Private _val_capital_seguado As String
    Private _val_salario As String
    Private _dt_desligamento As String
    Private _sexo As String

    Private _controla_processo_id As String
    Private _seq_linha As String
    Private _dt_inicio_vigencia_sbg As String
    Private _ind_operacao As String
    Private _cpf_conjuge As String

    Public Property Id() As String
        Get
            Return Me._id
        End Get
        Set(ByVal Value As String)
            Me._id = Value
        End Set
    End Property

    Public Property Operacao() As String
        Get
            Return Me._operacao
        End Get
        Set(ByVal Value As String)
            Me._operacao = Value
        End Set
    End Property

    Public Property Nome() As String
        Get
            Return Me._nome
        End Get
        Set(ByVal Value As String)
            Me._nome = Value
        End Set
    End Property

    Public Property Cpf() As String
        Get
            Return Me._cpf
        End Get
        Set(ByVal Value As String)
            Me._cpf = Value
        End Set
    End Property

    Public Property Dt_nasc() As String
        Get
            Return Me._dt_nasc
        End Get
        Set(ByVal Value As String)
            Me._dt_nasc = Value
        End Set
    End Property

    Public Property Dt_admissao() As String
        Get
            Return Me._dt_admissao
        End Get
        Set(ByVal Value As String)
            Me._dt_admissao = Value
        End Set
    End Property

    Public Property Val_capital_seguado() As String
        Get
            Return Me._val_capital_seguado
        End Get
        Set(ByVal Value As String)
            Me._val_capital_seguado = Value
        End Set
    End Property

    Public Property Val_salario() As String
        Get
            Return Me._val_salario
        End Get
        Set(ByVal Value As String)
            Me._val_salario = Value
        End Set
    End Property

    Public Property Dt_desligamento() As String
        Get
            Return Me._dt_desligamento
        End Get
        Set(ByVal Value As String)
            Me._dt_desligamento = Value
        End Set
    End Property

    Public Property Sexo() As String
        Get
            Return Me._sexo
        End Get
        Set(ByVal Value As String)
            Me._sexo = Value
        End Set
    End Property

    Public Property Controla_processo_id() As String
        Get
            Return Me._controla_processo_id
        End Get
        Set(ByVal Value As String)
            Me._controla_processo_id = Value
        End Set
    End Property

    Public Property Seq_linha() As String
        Get
            Return Me._seq_linha
        End Get
        Set(ByVal Value As String)
            Me._seq_linha = Value
        End Set
    End Property

    Public Property Dt_inicio_vigencia_sbg() As String
        Get
            Return Me._dt_inicio_vigencia_sbg
        End Get
        Set(ByVal Value As String)
            Me._dt_inicio_vigencia_sbg = Value
        End Set
    End Property

    Public Property Ind_operacao() As String
        Get
            Return Me._ind_operacao
        End Get
        Set(ByVal Value As String)
            Me._ind_operacao = Value
        End Set
    End Property

    Public Property Cpf_conjuge() As String
        Get
            Return Me._cpf_conjuge
        End Get
        Set(ByVal Value As String)
            Me._cpf_conjuge = Value
        End Set
    End Property
End Class
