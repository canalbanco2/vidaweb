Public Class confirm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
    Protected WithEvents lblMsg As System.Web.UI.WebControls.Label
    Protected WithEvents btnSim As System.Web.UI.WebControls.Button
    Protected WithEvents btnNao As System.Web.UI.WebControls.Button
    Protected WithEvents pnl As System.Web.UI.WebControls.Panel



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        If Session("acesso") = "6" Then ' Projeto 539202 - Jo�o Ribeiro - 29/04/2009
            btnSim.Enabled = False
        Else

            Select Case Request("acao")
                Case "excluir"
                    lblMsg.Text = "Confirma a exclus�o deste registro?"
                Case "excluirTodos"
                    lblMsg.Text = "Confirma a exclus�o de todos os registros?"
            End Select

        End If

    End Sub

    Private Sub btnSim_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSim.Click

        Dim acao As String = Request("acao")
        Dim id As Integer
        Try
            'Dim id As Integer = CType(Request("id"), Integer)
            id = Convert.ToInt32(Request.QueryString("id"))
        Catch ex As Exception
            id = 0
        End Try

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        Select Case acao
            Case "excluir"
                bd.excluiRegistros(Session("apolice"), Session("ramo"), Session("subgrupo_id"), id)
            Case "excluirTodos"
                bd.excluiRegistros(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
        End Select

        Try
            bd.ExecutaSQL()
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        cUtilitarios.escreveScript("parent.parent.GB_reloadOnClose('true');")
        cUtilitarios.escreveScript("parent.parent.GB_hide();")
        cUtilitarios.br("Registro exclu�do com sucesso")
    End Sub
End Class

