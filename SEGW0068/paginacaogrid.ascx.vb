Imports System.Drawing
Imports System.Web.UI.WebControls
Imports System.Data

Public Class paginacaogrid
    Inherits System.Web.UI.UserControl

    Public _valor As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents a1 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents a2 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents a3 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents a4 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents a5 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents a6 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents a7 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lblPaginacao As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub

#End Region

    Public Shared iNumPaginas As Integer

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub

    Public Event ePaginaAlterada(ByVal Argumento As String)
    Public Event ePaginaAlteradaA(ByVal Argumento As String)

    Private Sub mDisparaEvento(ByVal Argumento As String)
        RaiseEvent ePaginaAlterada(Argumento)
    End Sub
    Private Sub mDisparaEventoA(ByVal Argumento As String)
        RaiseEvent ePaginaAlteradaA(Argumento)
    End Sub

    Public Sub mPaginacaoClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles a1.Command, a2.Command, a3.Command, a4.Command, a5.Command, a6.Command, a7.Command

        If CType(sender, LinkButton).Text = "Pr�xima" Then
            Session("pagina") = CType(Session("pagina"), Integer) + 1
        ElseIf CType(sender, LinkButton).Text = "Anterior" Then
            Session("pagina") = CType(Session("pagina"), Integer) - 1
        Else
            Session("pagina") = CType(CType(sender, LinkButton).Text, Integer) - 1
        End If

        mDisparaEvento(e.CommandArgument)

        mMontaPaginacao(Session("pagina"), iNumPaginas)

    End Sub

    Private Sub aPaginacaoClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles a1.Command, a2.Command, a3.Command, a4.Command, a5.Command, a6.Command, a7.Command
        mDisparaEventoA(e.CommandArgument)
    End Sub

    Private Sub mMontaPaginacao(ByVal GridPaginaAtual As Integer, ByVal GridPaginasTotal As Integer)
        Dim paginainicial As Integer
        Dim paginafinal As Integer
        Dim paginaatual As Integer = GridPaginaAtual + 1

        If (paginaatual / 5) > 0 Then
            If (paginaatual Mod 5) <> 0 Then
                paginainicial = (Int(paginaatual / 5) * 5) + 1
            Else
                paginainicial = (Int((paginaatual - 1) / 5) * 5) + 1
            End If
            paginafinal = paginainicial + 4
            If paginafinal > GridPaginasTotal Then paginafinal = GridPaginasTotal
        Else
            paginainicial = 1
            paginafinal = 5
            If paginafinal > GridPaginasTotal Then paginafinal = GridPaginasTotal
        End If

        Me.a1.Visible = False
        Me.a2.Visible = False
        Me.a3.Visible = False
        Me.a4.Visible = False
        Me.a5.Visible = False
        Me.a6.Visible = False
        Me.a7.Visible = False
        If paginaatual > 1 Then
            a1.Text = "Anterior"
            a1.ForeColor = Color.White
            a1.Style.Add("font-family", "verdana")
            a1.Style.Add("font-size", "10px")
            a1.Style.Add("font-weight", "bold")
            a1.Style.Add("text-decoration", "underline")
            a1.Visible = True
        End If
        Dim a As Integer = 2
        For i As Integer = paginainicial To paginafinal
            CType(Me.FindControl("a" + a.ToString), LinkButton).CommandArgument = (i - 1).ToString
            CType(Me.FindControl("a" + a.ToString), LinkButton).Text = i.ToString
            CType(Me.FindControl("a" + a.ToString), LinkButton).Visible = True
            If i = paginaatual Then
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("color", "white")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("text-decoration", "none")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-family", "verdana")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-size", "10px")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-weight", "bold")

            Else
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("color", "white;")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("text-decoration", "underline")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-family", "verdana")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-size", "10px")
                CType(Me.FindControl("a" + a.ToString), LinkButton).Style.Add("font-weight", "bold")

            End If
            a += 1
        Next
        If paginaatual <> GridPaginasTotal Then
            a7.Text = "Pr�xima"
            a7.ForeColor = Color.White
            a7.Style.Add("font-family", "verdana")
            a7.Style.Add("font-size", "10px")
            a7.Style.Add("font-weight", "bold")
            a7.Style.Add("text-decoration", "underline")
            a7.Visible = True
        End If
    End Sub

    Function RetiraCaracter(ByVal sEntrada As String) As String

        Dim iCont As Integer
        Dim sCaracter As String = ""

        For iCont = 1 To Len(sEntrada)
            If IsNumeric(Mid(sEntrada, iCont, 1)) Or Mid(sEntrada, iCont, 1) = "," Or Mid(sEntrada, iCont, 1) = "." Then
                sCaracter = sCaracter & Mid(sEntrada, iCont, 1)
            End If
        Next

        Return sCaracter

    End Function

    Public Sub GridDataBind(ByVal Grid As DataGrid, ByVal dt As DataTable)

        Dim dttConcat As New DataTable

        dttConcat.Columns.Add("arquivo_vida_criticada_id", System.Type.GetType("System.Int32"))
        dttConcat.Columns.Add("operacao", System.Type.GetType("System.String"))
        dttConcat.Columns.Add("nome", System.Type.GetType("System.String"))
        dttConcat.Columns.Add("cpf", System.Type.GetType("System.String"))
        dttConcat.Columns.Add("dt_nascimento", System.Type.GetType("System.String"))
        dttConcat.Columns.Add("motivo_critica", System.Type.GetType("System.String"))
        dttConcat.Columns.Add("dt_inicio_vigencia_sbg", System.Type.GetType("System.String"))
        dttConcat.Columns.Add("val_salario", System.Type.GetType("System.String"))
        dttConcat.Columns.Add("val_capital_segurado", System.Type.GetType("System.String"))

        dttConcat.Columns.Add("motivo_critica_id", System.Type.GetType("System.String"))

        Dim r As DataRow

        ' preenchedo c�pia
        For i As Integer = 0 To dt.Select().Length - 1

            r = dttConcat.NewRow

            r("arquivo_vida_criticada_id") = dt.Select()(i)("arquivo_vida_criticada_id")
            r("operacao") = dt.Select()(i)("operacao")

            Dim sNomeFracionado As String = String.Empty
            Dim sNomeInteiro As String = dt.Select()(i)("nome").ToString.ToUpper

            If (dt.Select()(i)("nome").ToString.Length > 25) Then
                sNomeFracionado = dt.Select()(i)("nome").ToString.Substring(0, 25).ToUpper.Trim + "..."
            Else
                sNomeFracionado = dt.Select()(i)("nome").ToString.ToUpper
            End If

            r("nome") = "<span style=""white-space:nowrap"" onmouseover=""return overlib('" + _
                            sNomeInteiro + "');"" onmouseout=""return nd();"">" + sNomeFracionado + "</span>"

            r("cpf") = dt.Select()(i)("cpf")
            r("dt_nascimento") = dt.Select()(i)("dt_nascimento")
            r("motivo_critica") = dt.Select()(i)("motivo_critica")
            r("dt_inicio_vigencia_sbg") = dt.Select()(i)("dt_inicio_vigencia_sbg")
            r("val_salario") = dt.Select()(i)("val_salario")
            'If IsNumeric(r("val_capital_segurado")) Then
            'r("val_capital_segurado") = Val(dt.Select()(i)("val_capital_segurado"))
            'Else
                r("val_capital_segurado") = dt.Select()(i)("val_capital_segurado")
            'End If

            r("motivo_critica_id") = dt.Select()(i)("motivo_critica_id")

            dttConcat.Rows.Add(r)

        Next

        Dim flagDownload As Boolean = False

        ' concatenando
        For i As Integer = 0 To dttConcat.Rows.Count - 1
            For j As Integer = 0 To dt.Select("arquivo_vida_criticada_id=" & dttConcat.Rows(i)("arquivo_vida_criticada_id").ToString()).Length - 1
                If j <> dt.Select("arquivo_vida_criticada_id=" & dttConcat.Rows(i)("arquivo_vida_criticada_id").ToString()).Length - 1 Then
                    dttConcat.Rows(i)("motivo_critica") &= ", " & dt.Select("arquivo_vida_criticada_id=" & dttConcat.Rows(i)("arquivo_vida_criticada_id").ToString())(j)("motivo_critica")
                    dttConcat.Rows(i)("motivo_critica_id") &= ", " & dt.Select("arquivo_vida_criticada_id=" & dttConcat.Rows(i)("arquivo_vida_criticada_id").ToString())(j)("motivo_critica_id")

                    If dt.Select("arquivo_vida_criticada_id=" & dttConcat.Rows(i)("arquivo_vida_criticada_id").ToString())(j)("motivo_critica_id") = "2" Then
                        flagDownload = True
                    End If

                End If
            Next
        Next

        If flagDownload Then
            Response.Write("<script>var exibeDownload = true;</script>")
        Else
            Response.Write("<script>var exibeDownload = false;</script>")
        End If


        ' removendo linhas repetidas
        For i As Integer = dttConcat.Rows.Count - 1 To 0 Step -1
            If dttConcat.Select("arquivo_vida_criticada_id=" & dttConcat.Rows(i)("arquivo_vida_criticada_id").ToString()).Length > 1 Then
                dttConcat.Rows.Remove(dttConcat.Select("arquivo_vida_criticada_id=" & dttConcat.Rows(i)("arquivo_vida_criticada_id").ToString())(0))
            End If
        Next

        'Grid.DataSource = dt
        Grid.DataSource = dttConcat
        Grid.DataBind()

        Dim tipo As String
        Dim cell As Web.UI.WebControls.DataGridItem
        Dim pai As Web.UI.WebControls.DataGridItem
        Dim count As Int16 = 0
        Dim pai_id As String = ""
        Dim count2 As Integer = 0
        Dim w As Integer = 1
        Dim t As Integer = 1
        Dim primeiro As Boolean = True
        Dim tamanho(10) As Integer
        Dim textoCell As String = ""
        tamanho(0) = 0
        tamanho(1) = 5
        tamanho(2) = 10
        tamanho(3) = 5
        tamanho(4) = 5

        tamanho(5) = 15
        tamanho(6) = 5
        tamanho(7) = 5
        tamanho(8) = 5
        tamanho(9) = 0

        Dim txtMax(10) As String
        txtMax(0) = ""
        txtMax(1) = ""
        txtMax(2) = ""
        txtMax(3) = ""
        txtMax(4) = ""

        txtMax(5) = ""
        txtMax(6) = ""
        txtMax(7) = ""
        txtMax(8) = ""
        txtMax(9) = ""


        If cUtilitarios.MostraColunaSalario() Then
            w = 1
        Else
            w = 0
        End If

        If cUtilitarios.MostraColunaCapital() Then
            t = 1
        Else
            t = 0
        End If

        If Grid.Items.Count > 0 Then

            For Each x As Web.UI.WebControls.DataGridItem In Grid.Items

                For i As Integer = 0 To x.Cells.Count - 1
                    x.Cells.Item(i).ID = "cell_" & i
                    x.Cells.Item(i).Wrap = False
                Next

                For i As Integer = 0 To x.Cells.Count - 1
                    textoCell = x.Cells.Item(i).Text.ToString.Replace("&atilde;", "a").Replace("&ccedil;", "c")

                    If tamanho(i) <= textoCell.Length Then
                        tamanho(i) = textoCell.Length
                        txtMax(i) = x.Cells.Item(i).Text
                    End If
                Next

                pai = x
                x.ID = count2
                pai_id = x.ID
                count2 += 1

                x.Cells.Item(0).HorizontalAlign = HorizontalAlign.Center
                x.Cells.Item(1).HorizontalAlign = HorizontalAlign.Center
                x.Cells.Item(2).HorizontalAlign = HorizontalAlign.Left
                x.Cells.Item(3).HorizontalAlign = HorizontalAlign.Center
                x.Cells.Item(3).HorizontalAlign = HorizontalAlign.Center

                x.Cells.Item(2).Text = x.Cells.Item(2).Text.Replace("&NBSP;", "")
                x.Cells.Item(3).Text = cUtilitarios.trataCPF(x.Cells.Item(3).Text)

                Try
                    x.Cells.Item(7).Text = cUtilitarios.trataMoeda(RetiraCaracter(x.Cells.Item(7).Text))
                Catch ex As Exception

                End Try
                'x.Cells.Item(7).Text = cUtilitarios.trataMoeda(RetiraCaracter(x.Cells.Item(7).Text))

                x.Attributes.Add("onmouseover", "mO('#E8F1FF', this)")
                x.Attributes.Add("onmouseout", "mO('#FFFFFF', this)") 'lista_criticas   MontaFormulario(""" & x.Cells.Item(9).Text & """);
                ''x.Attributes.Add("onclick", "document.getElementById('lista_criticas').value = '" & x.Cells.Item(5).Text & "'; alteracoes('" & x.Cells.Item(0).Text & "', '1', '" & x.Cells.Item(1).Text & "','" + x.Cells.Item(0).Text + "', '" & x.ID & "', 'I')")
                x.Attributes.Add("onclick", "document.getElementById('lista_criticas').value = '" & x.Cells.Item(9).Text & "'; document.getElementById('_id_cli_').value = '" & x.Cells.Item(0).Text & "'; alteracoes('" & x.Cells.Item(0).Text & "', '1', '" & x.Cells.Item(1).Text & "','" + x.Cells.Item(0).Text + "', '" & x.ID & "', 'I')")

                If x.Cells.Item(1).Text = "Hist�rico" Then
                    x.Cells.Item(1).Text = "&nbsp;"
                End If

                If primeiro Then
                    primeiro = False
                    For i As Integer = 0 To x.Cells.Count - 1
                        x.Cells.Item(i).Text = "<div class='headerFakeDiv'id='headerFake" & i & "'></div>" & x.Cells.Item(i).Text
                    Next
                End If


            Next

            Dim z As Integer = 0
            Dim texto As String
            Dim tamMin(10) As Integer
            tamMin(0) = 6
            tamMin(1) = 12
            tamMin(2) = 40
            tamMin(3) = 16
            tamMin(4) = 10

            For Each m As String In txtMax
                Try

                    ' flow 137967 - Patr�cia Marques - 14/05/2009
                    If Not IsNothing(txtMax(z)) Then
                        texto = txtMax(z).ToString.Trim.Replace(" ", "O").Replace("&atilde;", "a").Replace("&ccedil;", "c").Replace("-", "O").PadRight(tamMin(z), "O")

                        If texto.IndexOf("returnOoverlib('") > 0 Then
                            texto = texto.Substring(1)
                            texto = texto.Substring(texto.IndexOf(">") + 1, texto.IndexOf("<") - texto.IndexOf(">"))
                        End If
                    End If
                    ' fim

                Catch ex As Exception

                End Try

                Response.Write("<input type='hidden' value='" & texto & "' id='txtMax" & z & "'>" & vbCrLf)
                z = z + 1
            Next

        End If

        ''Me.mMontaPaginacao(Grid.CurrentPageIndex, Grid.PageCount)
        Me.mMontaPaginacao(Session("pagina"), Grid.PageCount)

    End Sub

    Public Property CssClass() As String
        Get
            Return Me.lblPaginacao.Attributes.Item("class")
        End Get
        Set(ByVal Value As String)
            Me.lblPaginacao.Attributes.Add("class", Value)
        End Set
    End Property

End Class
