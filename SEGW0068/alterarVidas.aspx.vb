Public Class alterarVidas
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
    Protected WithEvents lblNome As System.Web.UI.WebControls.Label
    Protected WithEvents txtLblNome As System.Web.UI.WebControls.Label
    Protected WithEvents lblCpf As System.Web.UI.WebControls.Label
    Protected WithEvents lblNasc As System.Web.UI.WebControls.Label
    Protected WithEvents txtLblNasc As System.Web.UI.WebControls.Label
    Protected WithEvents txtLblCpf As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnGravar As System.Web.UI.WebControls.Button


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


#Region "[Mensagens]"
    Private Const ModeloAMensagem = "Esclarecemos que ser� necess�rio o preenchimento do Cart�o Proposta com " & _
       "Declara��o Pessol de Sa�de (DPS) para a inclus�o do referido proponente.  Para isso imprima o " & _
       "modelo no menu Download e siga as instru��es contidas no corpo do documento.\n\nSomente ap�s a an�lise " & _
       "das informa��es do proponente em quest�o, bem como o parecer favor�vel da Companhia, ser� autorizada essa " & _
       "inclus�o."

    Private Const ModeloBMensagem = "Esclaremos que ser� necess�rio o preenchimento do Cart�o Proposta " & _
    "com Declara��o Pessoal de Sa�de (DPS) e da Ficha Financeira para a inclus�o do referido proponente.  " & _
    "Para isso imprima o modelo no menu Download e siga as instru��es contidas no corpo do documento.  " & _
    "\n\nSomente ap�s a an�lise das informa��es do proponente em quest�o, bem como o parecer favor�vel da " & _
    "Companhia, ser� autorizada essa inclus�o."
#End Region

#Region "[Propriedades]"

    Private Property Mensagem() As String
        Get
            If ViewState("mensagem") <> Nothing Then
                Return CType(ViewState("mensagem"), String)
            Else
                Return String.Empty
            End If
        End Get
        Set(ByVal Value As String)
            ViewState("mensagem") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        Dim id As Integer = CType(Request("id"), Integer)

        If Not Page.IsPostBack Then
            getDadosRegistro(id)
        End If

    End Sub

    Private Sub getDadosRegistro(ByVal id As Integer)

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.getRegistro(Session("apolice"), Session("ramo"), Session("subgrupo_id"), id)

        Dim dr As Data.SqlClient.SqlDataReader

        Try
            dr = bd.ExecutaSQL_DR()

            dr.Read()

            Dim cpf As String = cUtilitarios.trataCPF(dr.Item("cpf"))
            Dim Nasc As String = dr.Item("dt_nascimento")
            Dim Nome As String = dr.Item("nome")
            Dim capital As Decimal = dr.Item("val_capital_segurado")
            Dim operacao As String = dr.Item("ind_operacao")


            If operacao = "P" Then
                If capital > 100000.01 And capital < 500000 Then
                    Mensagem = ModeloAMensagem
                ElseIf capital > 500000 Then
                    Mensagem = ModeloBMensagem
                End If
            End If

            txtLblCpf.Text = cpf
            txtLblNasc.Text = Nasc
            txtLblNome.Text = Nome

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Private Sub btnGravar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGravar.Click

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim cpf As String = "'" & cUtilitarios.destrataCPF(txtLblCpf.Text) & "'"

        If Mensagem = String.Empty Then

            bd.atualizaRegistros(cpf, Request("id"), Session("apolice"), Session("ramo"), Session("subgrupo_id"), "'" & Session("usuario") & "'")

            Dim dr As Data.SqlClient.SqlDataReader
            Try
                bd.ExecutaSQL()
            Catch ex As Exception
                Dim excp As New clsException(ex)
            End Try

            cUtilitarios.escreveScript("parent.parent.GB_reloadOnClose('true');")
            cUtilitarios.escreveScript("parent.parent.GB_hide();")
            cUtilitarios.br("Registro alterado com sucesso")

        Else

            cUtilitarios.escreveScript("alert('" & Mensagem & "');")

        End If


    End Sub
End Class
