Public Class ajax
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        CarregaResumoSEGW0060()
    End Sub


    Sub CarregaResumoSEGW0060()

        Print("|[")
        Dim _lblVidasFA As String = ""
        Dim _lblInclusoes_vidas As String = ""
        Dim _lblExclusoes_vidas As String = ""
        Dim _lblCapitalFA As String = ""
        Dim _lblPremioFA As String = ""
        Dim _lblInclusoes_capital As String = ""
        Dim _lblInclusoes_premio As String = ""
        Dim _lblExclusoes_capital As String = ""
        Dim _lblExclusoes_premio As String = ""
        Dim link As String = ""

        'Pega wf_id para Fatura Atual
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

        Try
            Dim dr As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()
            Dim wf_id As String

            If dr.HasRows Then
                While dr.Read()
                    wf_id = dr.GetValue(0).ToString()
                End While
            End If
            dr.Close()
            'Mostra Resumo da Fatura Atual

            'Dim bMovimentacaoAberta As Boolean = True
            'bd.VerificaMovimentacaoEncerrada(Session("apolice"), _
            '                                Session("ramo"), _
            '                                Session("subgrupo_id"))
            'Try
            '    Dim dr1 As Data.SqlClient.SqlDataReader = bd.ExecutaSQL_DR()

            '    If dr1.HasRows Then
            '        While dr1.Read()
            '            If UCase(dr1.GetValue(0).ToString()) <> "A" Then
            '                bMovimentacaoAberta = False
            '            End If
            '        End While
            '    End If
            '    dr1.Close()
            'Catch ex As Exception
            '    Dim excp As New clsException(ex)
            'End Try

            'If bMovimentacaoAberta Then

            '******************************************************************
            'Recalcular o resumo das opera��es - VIDA_WEB_DB..SEGS7048_SPI
            '******************************************************************
            bd.Recalcula_Resumo_Operacoes(Session("apolice"), _
                                            Session("ramo"), _
                                            Session("subgrupo_id"), _
                                            Session("usuario"))

            Try
                bd.ExecutaSQL()
            Catch ex As Exception
                Dim excp As New clsException(ex)
            End Try
            'End If

            bd.ResumoFaturaAtual_Anterior(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Session("usuario"), wf_id)
            'cUtilitarios.escreve(bd.SQL)
            dr = bd.ExecutaSQL_DR

            If dr.Read() Then
                Print("{obj:'lblVidasFA', value:'" & dr.GetValue(8).ToString & "'},")
                _lblVidasFA = dr.GetValue(8).ToString
                ''lblVidasFA.Text = dr.GetValue(8).ToString
                Print("{obj:'lblCapitalFA', value:'" & cUtilitarios.trataMoeda(dr.GetValue(6).ToString) & "'},")
                _lblCapitalFA = cUtilitarios.trataMoeda(dr.GetValue(6).ToString)
                ''lblCapitalFA.Text = cUtilitarios.trataMoeda(dr.GetValue(6).ToString)
                Print("{obj:'lblPremioFA', value:'" & cUtilitarios.trataMoeda(dr.GetValue(7).ToString) & "'},")
                _lblPremioFA = cUtilitarios.trataMoeda(dr.GetValue(7).ToString)
                ''lblPremioFA.Text = cUtilitarios.trataMoeda(dr.GetValue(7).ToString)
            Else
                Print("{obj:'lblVidasFA', value:'0'},")
                ''lblVidasFA.Text = "0"
                Print("{obj:'lblCapitalFA', value:'0,00'},")
                ''lblCapitalFA.Text = "0,00"
                Print("{obj:'lblPremioFA', value:'0,00'},")
                ''lblPremioFA.Text = "0,00"
            End If

            dr.Close()
            'Resumo de Inclusoes, Altera��es e Exclus�es
            bd.ResumoFaturaAtual_IAE(Session("apolice"), Session("ramo"), Session("subgrupo_id"), Session("usuario"), wf_id)

            dr = bd.ExecutaSQL_DR

            Dim totalVida As Integer, totalCapital As Double, totalPremio As Double

            While dr.Read()
                'cUtilitarios.br(dr.Item("operacao"))
                If dr.Item("operacao") = "I" Then
                    Print("{obj:'lblInclusoes_vidas', value:'" & dr.GetValue(0).ToString & "'},")
                    _lblInclusoes_vidas = dr.GetValue(0).ToString
                    ''lblInclusoes_vidas.Text = dr.GetValue(0).ToString
                    Print("{obj:'lblInclusoes_capital', value:'" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "'},")
                    _lblInclusoes_capital = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                    ''lblInclusoes_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                    Print("{obj:'lblInclusoes_premio', value:'" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "'},")
                    _lblInclusoes_premio = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                    ''lblInclusoes_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)

                    'Soma
                    'totalVida = totalVida + dr.GetValue(0).ToString
                    'totalCapital = totalCapital + cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                    'totalPremio = totalPremio + cUtilitarios.trataMoeda(dr.GetValue(2).ToString)

                ElseIf dr.Item("operacao") = "E" Then
                    Print("{obj:'lblExclusoes_vidas', value:'" & dr.GetValue(0).ToString & "'},")
                    _lblExclusoes_vidas = dr.GetValue(0).ToString
                    ''lblExclusoes_vidas.Text = dr.GetValue(0).ToString
                    Print("{obj:'lblExclusoes_capital', value:'" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "'},")
                    _lblExclusoes_capital = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                    ''lblExclusoes_capital.Text = "-" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                    Print("{obj:'lblExclusoes_premio', value:'" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "'},")
                    _lblExclusoes_premio = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                    ''lblExclusoes_premio.Text = "-" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                    'Subtrai
                    'totalVida = totalVida - dr.GetValue(0).ToString
                    'totalCapital = totalCapital - cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                    'totalPremio = totalPremio - cUtilitarios.trataMoeda(dr.GetValue(2).ToString)

                ElseIf dr.Item("operacao") = "A" Then
                    'cUtilitarios.br("Entrou")
                    'cUtilitarios.br(dr.GetValue(0).ToString)
                    Print("{obj:'lblAlteracoes_vidas', value:'" & dr.GetValue(0).ToString & "'},")
                    ''lblAlteracoes_vidas.Text = dr.GetValue(0).ToString
                    Print("{obj:'lblAlteracoes_capital', value:'" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "'},")
                    ''lblAlteracoes_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                    Print("{obj:'lblAlteracoes_premio', value:'" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "'},")
                    ''lblAlteracoes_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                ElseIf dr.Item("operacao") = "R" Then
                    Print("{obj:'lblAcertos_vidas', value:'" & dr.GetValue(0).ToString & "'},")
                    ''lblAcertos_vidas.Text = dr.GetValue(0).ToString
                    Print("{obj:'lblAcertos_capital', value:'" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "'},")
                    ''lblAcertos_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                    Print("{obj:'lblAcertos_premio', value:'" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "'},")
                    ''lblAcertos_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                ElseIf dr.Item("operacao") = "P" Then
                    Dim varPendencia As Integer
                    Print("{obj:'lblDPS_vidas', value:'" & dr.GetValue(0).ToString & "'},")
                    ''lblDPS_vidas.Text = dr.GetValue(0).ToString
                    'lblDPS_vidas.Text = "1"
                    varPendencia = CInt(dr.GetValue(0).ToString)

                    If varPendencia > 0 Then
                        ''link80.Style.Add("COLOR", "RED")
                        link += "window.parent.document.getElementById('link80').style.color = 'red';"
                    Else
                        ''link80.Style.Add("COLOR", "#555555")
                        link += "window.parent.document.getElementById('link80').style.color = '#555555';"
                    End If

                    Print("{obj:'lblDPS_capital', value:'" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "'},")
                    ''lblDPS_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                    Print("{obj:'lblDPS_premio', value:'" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "'},")
                    ''lblDPS_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                ElseIf dr.Item("operacao") = "B" Then
                    Print("{obj:'lblExcAgen_vidas', value:'" & dr.GetValue(0).ToString & "'},")
                    ''lblExcAgen_vidas.Text = dr.GetValue(0).ToString
                    Print("{obj:'lblExcAgen_capital', value:'" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "'},")
                    ''lblExcAgen_capital.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                    Print("{obj:'lblExcAgen_premio', value:'" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "'},")
                    ''lblExcAgen_premio.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                ElseIf dr.Item("operacao") = "T" Then
                    Print("{obj:'lblVidasFT', value:'" & dr.GetValue(0).ToString & "'},")
                    ''lblVidasFT.Text = dr.GetValue(0).ToString
                    Print("{obj:'lblCapitalFT', value:'" & cUtilitarios.trataMoeda(dr.GetValue(1).ToString) & "'},")
                    ''lblCapitalFT.Text = cUtilitarios.trataMoeda(dr.GetValue(1).ToString)
                    Print("{obj:'lblPremioFT', value:'" & cUtilitarios.trataMoeda(dr.GetValue(2).ToString) & "'},")
                    ''lblPremioFT.Text = cUtilitarios.trataMoeda(dr.GetValue(2).ToString)
                End If

            End While

            Dim totalFA As Int32 = 0
            Dim totalIncVidas As Int32 = 0
            Dim totalExcVidas As Int32

            Try
                totalFA = Int32.Parse(IIf(_lblVidasFA = "", "0", _lblVidasFA))
            Catch ex As Exception

            End Try

            Try
                totalIncVidas = Int32.Parse(IIf(_lblInclusoes_vidas = "", "0", _lblInclusoes_vidas))
            Catch ex As Exception

            End Try

            Try
                totalExcVidas = Int32.Parse(IIf(_lblExclusoes_vidas = "", "0", _lblExclusoes_vidas))
            Catch ex As Exception

            End Try


            Print("{obj:'lblSubTotal_vidas', value:'" & totalFA + totalIncVidas - totalExcVidas & "'},")
            ''Me.lblSubTotal_vidas.Text = totalFA + totalIncVidas - totalExcVidas
            Print("{obj:'lblSubTotal_capital', value:'" & cUtilitarios.trataMoeda(Decimal.Parse(IIf(_lblCapitalFA = "", "0", _lblCapitalFA)) + Decimal.Parse(IIf(_lblInclusoes_capital = "", "0", _lblInclusoes_capital)) - Decimal.Parse(IIf(_lblExclusoes_capital = "", "0", _lblExclusoes_capital))) & "'},")
            ''Me.lblSubTotal_capital.Text = cUtilitarios.trataMoeda(Decimal.Parse(lblCapitalFA.Text) + Decimal.Parse(lblInclusoes_capital.Text) + Decimal.Parse(lblExclusoes_capital.Text))
            Print("{obj:'lblSubTotal_premio', value:'" & cUtilitarios.trataMoeda(Decimal.Parse(IIf(_lblPremioFA = "", "0", _lblPremioFA)) + Decimal.Parse(IIf(_lblInclusoes_premio = "", "0", _lblInclusoes_premio)) - Decimal.Parse(IIf(_lblExclusoes_premio = "", "0", _lblExclusoes_premio))) & "'},")
            ''Me.lblSubTotal_premio.Text = cUtilitarios.trataMoeda(Decimal.Parse(lblPremioFA.Text) + Decimal.Parse(lblInclusoes_premio.Text) + Decimal.Parse(lblExclusoes_premio.Text))

            dr.Close()
            dr = Nothing
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try
        '----------------------------------------------

        Print("{obj:'', value:''}]|" & link & "|")

    End Sub

    Sub Print(ByVal str As String)
        Response.Write(str)
    End Sub

End Class
