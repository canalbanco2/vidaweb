Public Class validacao
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents Label5 As System.Web.UI.WebControls.Label
    Protected WithEvents Label6 As System.Web.UI.WebControls.Label
    Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    Protected WithEvents Label8 As System.Web.UI.WebControls.Label
    Protected WithEvents lblNome As System.Web.UI.WebControls.Label
    Protected WithEvents lblCPF As System.Web.UI.WebControls.Label
    Protected WithEvents lblDtNasc As System.Web.UI.WebControls.Label
    Protected WithEvents lblSexo As System.Web.UI.WebControls.Label
    Protected WithEvents lblSalario As System.Web.UI.WebControls.Label
    Protected WithEvents lblCapital As System.Web.UI.WebControls.Label
    Protected WithEvents lblDtDeslig As System.Web.UI.WebControls.Label
    Protected WithEvents lblDtRisco As System.Web.UI.WebControls.Label
    Protected WithEvents tblNome As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblCPF As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblDtNasc As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblSexo As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblSalario As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblCapital As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblDtRisco As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblDtDeslig As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents btnEnviar As System.Web.UI.WebControls.Button
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button
    Protected WithEvents txtNome As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCPF As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtDtNasc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtSalario As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCapital As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtDtDeslig As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtDtRisco As System.Web.UI.WebControls.TextBox
    Protected WithEvents Literal1 As System.Web.UI.WebControls.Literal
    Protected WithEvents rdSexo As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents lbl9 As System.Web.UI.WebControls.Label
    Protected WithEvents lbl10 As System.Web.UI.WebControls.Label
    Protected WithEvents tblCritica9 As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblCritica10 As System.Web.UI.HtmlControls.HtmlTable

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not Page.IsPostBack Then
            GeraFormCriticas(Undefned(Request.QueryString("lista")))
        End If

        ' Projeto 539202 - Jo�o Ribeiro - 29/04/2009
        If Session("acesso") = "6" Then
            btnEnviar.Enabled = False
        End If

    End Sub


    Sub GeraFormCriticas(ByVal codCritias As String, Optional ByVal flgCriticaDisparadaPorPoust As Boolean = False)

        Dim wi As Integer = 0
        escondeTabelas()

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.RetornaCriticas()
        Dim dt As Data.DataTable

        ' flow 137967 - Patr�cia Marques - 14/05/2009
        Dim capitalSegurado As String = Nothing
        ' fim

        Try
            dt = bd.ExecutaSQL_DT()

            ' flow 137967 - Patr�cia Marques - 14/05/2009
            capitalSegurado = cUtilitarios.getCapitalSeguradoSession()
            ' fim

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        For i As Integer = 0 To codCritias.Split(",").Length - 1
            If codCritias.Split(",")(i) = 1 Then
                If lblNome.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblNome.Visible = True
                lblNome.Text = getCritica(1, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtNome.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 2 Then
                If lblDtNasc.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblDtNasc.Visible = True
                lblDtNasc.Text = getCritica(2, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtDtNasc.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 3 Then
                If lblDtDeslig.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblDtDeslig.Visible = True
                lblDtDeslig.Text = getCritica(3, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtDtDeslig.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 4 Then
                If lblCPF.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblCPF.Visible = True
                lblCPF.Text = getCritica(4, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtCPF.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 5 Then
                If lblSexo.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblSexo.Visible = True
                lblSexo.Text = getCritica(5, dt)
            End If

            If codCritias.Split(",")(i) = 6 Then
                If lblCPF.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblCPF.Visible = True
                lblCPF.Text = getCritica(6, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtCPF.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 7 Then
                If lblDtNasc.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblDtNasc.Visible = True
                lblDtNasc.Text = getCritica(7, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtDtNasc.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 8 Then
                If lblDtNasc.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblDtNasc.Visible = True
                lblDtNasc.Text = getCritica(8, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtDtNasc.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 9 Then
                If lbl9.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblCritica9.Visible = True
                lbl9.Text = getCritica(9, dt)
            End If

            If codCritias.Split(",")(i) = 10 Then
                If lbl9.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblCritica10.Visible = True
                lbl10.Text = getCritica(10, dt)
            End If

            If codCritias.Split(",")(i) = 11 Then
                If lblCPF.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblCPF.Visible = True
                lblCPF.Text = getCritica(11, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtCPF.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 12 Then
                If lblCapital.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblCapital.Visible = True
                lblCapital.Text = getCritica(12, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtCapital.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 13 Then
                If lblCPF.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblCPF.Visible = True
                lblCPF.Text = getCritica(13, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtCPF.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 14 Then
                If lblDtNasc.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblDtNasc.Visible = True
                lblDtNasc.Text = getCritica(14, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtDtNasc.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 15 Then
                If lblSalario.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblSalario.Visible = True
                lblSalario.Text = getCritica(15, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtSalario.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 16 Then
                If lblCPF.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblCPF.Visible = True
                lblCPF.Text = getCritica(16, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtCPF.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 17 Then
                If lblDtRisco.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblDtRisco.Visible = True
                lblDtRisco.Text = getCritica(17, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtDtRisco.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 18 Then
                If lblDtDeslig.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblDtDeslig.Visible = True
                lblDtDeslig.Text = getCritica(18, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtDtDeslig.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 19 Then
                If lblDtRisco.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblDtRisco.Visible = True
                lblDtRisco.Text = getCritica(19, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtDtRisco.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 20 Then
                If lblDtDeslig.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblDtDeslig.Visible = True
                lblDtDeslig.Text = getCritica(20, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtDtDeslig.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 21 Then
                If lblCPF.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblCPF.Visible = True
                lblCPF.Text = getCritica(21, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtCPF.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 22 Then
                If lblDtDeslig.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblDtDeslig.Visible = True
                lblDtDeslig.Text = getCritica(22, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtDtDeslig.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 23 Then
                'tblSexo.Visible = True
                'lblNome.Text = getCritica(23, dt)
            End If

            If codCritias.Split(",")(i) = 24 Then
                If lblDtDeslig.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblDtDeslig.Visible = True
                lblDtDeslig.Text = getCritica(24, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtDtDeslig.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 25 Then
                If lblDtRisco.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblDtRisco.Visible = True
                lblDtRisco.Text = getCritica(25, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtDtRisco.CssClass = "erro"
                End If
            End If

            If codCritias.Split(",")(i) = 26 Then
                If lblDtDeslig.Text = String.Empty Then
                    wi = wi + 1
                End If

                tblDtRisco.Visible = True
                lblDtRisco.Text = getCritica(26, dt)

                If flgCriticaDisparadaPorPoust Then
                    txtDtRisco.CssClass = "erro"
                End If
            End If


            ' flow 137967 - Patr�cia Marques - 14/05/2009
            If codCritias.Split(",")(i) = 27 Then

                If capitalSegurado = "M�ltiplo sal�rio" Then


                    If lblSalario.Text = String.Empty Then
                        wi = wi + 1
                    End If

                    tblSalario.Visible = True
                    lblSalario.Text = getCritica(15, dt)

                    If flgCriticaDisparadaPorPoust Then
                        txtSalario.CssClass = "erro"
                    End If

                ElseIf capitalSegurado = "Capital informado" Then

                    If lblCapital.Text = String.Empty Then
                        wi = wi + 1
                    End If

                    tblCapital.Visible = True
                    lblCapital.Text = getCritica(12, dt)

                    If flgCriticaDisparadaPorPoust Then
                        txtCapital.CssClass = "erro"
                    End If

                End If

            End If
            ' fim 
        Next

        Literal1.Text = "<script>$(function(){$(parent.document.getElementById('frame1')).css('height', '" & (wi * 80) & "px')})</script>"
    End Sub

    Sub escondeTabelas()
        tblCapital.Visible = False
        tblCPF.Visible = False
        tblDtNasc.Visible = False
        tblDtRisco.Visible = False
        tblNome.Visible = False
        tblSalario.Visible = False
        tblSexo.Visible = False
        tblDtDeslig.Visible = False
        tblCritica9.Visible = False
        tblCritica10.Visible = False

        txtCapital.CssClass = ""
        txtCPF.CssClass = ""
        txtDtNasc.CssClass = ""
        txtDtRisco.CssClass = ""
        txtNome.CssClass = ""
        txtSalario.CssClass = ""
        txtDtDeslig.CssClass = ""

        lblNome.Text = ""
        lblDtNasc.Text = ""
        lblDtDeslig.Text = ""
        lblCPF.Text = ""
        lblSexo.Text = ""
        lblCPF.Text = ""
        lblDtNasc.Text = ""
        lblDtNasc.Text = ""
        lblCPF.Text = ""
        lblCapital.Text = ""
        lblCPF.Text = ""
        lblDtNasc.Text = ""
        lblSalario.Text = ""
        lblCPF.Text = ""
        lblDtRisco.Text = ""
        lblDtDeslig.Text = ""
        lblDtRisco.Text = ""
        lblDtDeslig.Text = ""
        lblCPF.Text = ""
        lblDtDeslig.Text = ""
        lblDtDeslig.Text = ""
        lblDtRisco.Text = ""
        lbl9.Text = ""
        lbl10.Text = ""

    End Sub

    Sub VerificaCriticas()

        If tblDtNasc.Visible Then
            If Not IsDate(txtDtNasc.Text) Then
                txtDtNasc.CssClass = "erro"
                Literal1.Text = "<script>alert('Data inv�lida!');</script>"
                Exit Sub
            End If
            If DateValue(txtDtNasc.Text) < "01/01/1900" Or DateValue(txtDtNasc.Text) > "01/01/2050" Then
                txtDtNasc.CssClass = "erro"
                Literal1.Text = "<script>alert('Data fora do per�odo suportado!');</script>"
                Exit Sub
            End If
        End If

        If tblDtRisco.Visible Then
            If Not IsDate(txtDtRisco.Text) Then
                txtDtRisco.CssClass = "erro"
                Literal1.Text = "<script>alert('Data inv�lida!');</script>"
                Exit Sub
            End If
            If DateValue(txtDtRisco.Text) < "01/01/1900" Or DateValue(txtDtRisco.Text) > "01/01/2050" Then
                txtDtRisco.CssClass = "erro"
                Literal1.Text = "<script>alert('Data fora do per�odo suportado!');</script>"
                Exit Sub
            End If
        End If

        If tblDtDeslig.Visible Then
            If Not IsDate(txtDtDeslig.Text) Then
                txtDtDeslig.CssClass = "erro"
                Literal1.Text = "<script>alert('Data inv�lida!');</script>"
                Exit Sub
            End If

            If DateValue(txtDtDeslig.Text) < "01/01/1900" Or DateValue(txtDtDeslig.Text) > "01/01/2050" Then
                txtDtDeslig.CssClass = "erro"
                Literal1.Text = "<script>alert('Data fora do per�odo suportado!');</script>"
                Exit Sub
            End If
        End If

        Dim _dt As New System.Data.DataTable

        Dim segPost As New cSegurado
        segPost = ValoresSeguradoPost()

        Dim segBD As New cSegurado
        segBD = ValoresSeguradoBanco(segPost.Id)

        Dim segurado As New cSegurado
        segurado = MesclaValoresSegurado(segPost, segBD)

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)

        Try
            bd.SEGS7053_SPU(Session("apolice"), _
                            Session("ramo"), _
                            Session("subgrupo_id"), _
                            segurado.Id, _
                            segurado.Controla_processo_id, _
                            segurado.Seq_linha, _
                            segurado.Nome, _
                            Replace(Replace(segurado.Cpf, ".", ""), "-", ""), _
                            FormataStringData(segurado.Dt_nasc), _
                            segurado.Sexo, _
                            Format(Val(segurado.Val_capital_seguado), "##,####,##0.00"), _
                            Format(Val(segurado.Val_salario), "##,####,##0.00"), _
                            FormataStringData(segurado.Dt_inicio_vigencia_sbg), _
                            FormataStringData(segurado.Dt_desligamento), _
                            segurado.Ind_operacao, _
                            Replace(Replace(segurado.Cpf_conjuge, ".", ""), "-", ""), _
                            Session("usuario"))

            _dt = bd.ExecutaSQL_DT()

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        bd.RetornaCriticas()
        Dim dt As Data.DataTable

        Try
            dt = bd.ExecutaSQL_DT()
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Dim strListaErros As String = ""

        If Not _dt Is Nothing Then
            For i As Integer = 0 To _dt.Rows.Count - 1
                strListaErros &= ", " & _dt.Rows(i)("MOTIVO_CRITICA_ID")
            Next
        End If

        If strListaErros <> "" Then
            GeraFormCriticas(strListaErros.Substring(1), True)
        Else
            Literal1.Text = "<script>parent.document.location = parent.document.location.href;</script>"
        End If

    End Sub

    Function VerificaMotivoCriticaID(ByVal id As Integer, ByVal dt As Data.DataTable) As Boolean
        For i As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(i)("MOTIVO_CRITICA_ID") = id Then
                Return True
            End If
        Next

        Return False
    End Function

    Dim indexMaxArray As Integer = 0

    Function ValoresSeguradoPost() As cSegurado

        Dim segurado As New cSegurado

        segurado.Id = Request.QueryString("_id")
        segurado.Nome = txtNome.Text
        segurado.Cpf = txtCPF.Text
        segurado.Dt_nasc = txtDtNasc.Text
        segurado.Sexo = rdSexo.SelectedValue
        segurado.Val_salario = txtSalario.Text
        segurado.Val_capital_seguado = txtCapital.Text
        segurado.Dt_desligamento = txtDtDeslig.Text
        segurado.Dt_inicio_vigencia_sbg = txtDtRisco.Text

        Return segurado
    End Function

    Function RetornaValorString(ByVal colStr(,) As String, ByVal index As Integer) As String
        For i As Integer = 0 To Me.indexMaxArray - 1
            If colStr(index, i) <> "" Or colStr(index, i) <> Nothing Then
                Return colStr(index, i)
            End If
        Next
        Return ""
    End Function

    Function ValoresSeguradoBanco(ByVal id As String) As cSegurado

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        bd.getArquivoVidaCriticado(id)

        Dim segurado As New cSegurado
        Dim dr As Data.SqlClient.SqlDataReader

        Try
            dr = bd.ExecutaSQL_DR()
            If dr.HasRows Then
                If dr.Read() Then
                    segurado.Nome = dr.Item("nome")
                    segurado.Cpf = dr.Item("cpf_titular")
                    segurado.Dt_nasc = dr.Item("dt_nascimento")
                    segurado.Sexo = dr.Item("sexo")
                    segurado.Val_capital_seguado = dr.Item("val_capital_segurado")
                    segurado.Val_salario = dr.Item("val_salario")
                    segurado.Dt_desligamento = dr.Item("dt_desligamento")

                    segurado.Controla_processo_id = dr.Item("controla_processo_id")
                    segurado.Seq_linha = dr.Item("seq_linha")
                    segurado.Dt_inicio_vigencia_sbg = dr.Item("dt_inicio_vigencia_sbg")
                    segurado.Ind_operacao = dr.Item("ind_operacao")
                    segurado.Cpf_conjuge = dr.Item("cpf_conjuge")
                End If
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        Return segurado
    End Function

    Function MesclaValoresSegurado(ByVal segPost As cSegurado, ByVal segBD As cSegurado) As cSegurado
        Dim retorno As New cSegurado
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.VIDA_WEB_DB)
        Dim bSobreposicao As Boolean = False
        Dim dr As Data.SqlClient.SqlDataReader

        bd.SobreposicaoLayout(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
        Try
            dr = bd.ExecutaSQL_DR()
            If dr.HasRows Then
                If dr.Read() Then
                    bSobreposicao = (dr.Item("sobreposicao") = 1)
                End If
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

        retorno.Id = segPost.Id

        '' carregando informa��e que s�o recuperadas apenas no banco de dados
        retorno.Controla_processo_id = segBD.Controla_processo_id
        retorno.Seq_linha = segBD.Seq_linha
        If Not bSobreposicao Then
            retorno.Ind_operacao = segBD.Ind_operacao
        Else
            retorno.Ind_operacao = ""
        End If
        retorno.Cpf_conjuge = segBD.Cpf_conjuge

        '' mesclando infortma��es
        retorno.Nome = IIf(segPost.Nome = "", segBD.Nome, segPost.Nome)
        retorno.Cpf = IIf(segPost.Cpf = "", segBD.Cpf, segPost.Cpf)
        retorno.Dt_nasc = IIf(segPost.Dt_nasc = "", segBD.Dt_nasc, segPost.Dt_nasc)
        retorno.Sexo = IIf(segPost.Sexo = "", segBD.Sexo, segPost.Sexo)
        'retorno.Val_capital_seguado = CType(IIf(segPost.Val_capital_seguado = "", segBD.Val_capital_seguado, segPost.Val_capital_seguado), String).Replace(",", "")
        'retorno.Val_salario = CType(IIf(segPost.Val_salario = "", segBD.Val_salario, segPost.Val_salario), String).Replace(",", "")
        retorno.Val_capital_seguado = IIf(segPost.Val_capital_seguado = "", segBD.Val_capital_seguado, segPost.Val_capital_seguado)
        retorno.Val_salario = IIf(segPost.Val_salario = "", segBD.Val_salario, segPost.Val_salario)
        retorno.Dt_desligamento = IIf(segPost.Dt_desligamento = "", segBD.Dt_desligamento, segPost.Dt_desligamento)
        retorno.Dt_inicio_vigencia_sbg = IIf(segPost.Dt_inicio_vigencia_sbg = "", segBD.Dt_inicio_vigencia_sbg, segPost.Dt_inicio_vigencia_sbg)

        Return retorno
    End Function

    Function FormataStringData(ByVal str As String)
        Dim retorno As String

        If str = "00:00:00" Then
            Return ""
        End If

        If str.Length < 6 Then
            retorno = ""
        Else
            retorno = IIf(str.Split("/")(0).Length = 1, "0" & str.Split("/")(0), str.Split("/")(0)) & "/" & IIf(str.Split("/")(1).Length = 1, "0" & str.Split("/")(1), str.Split("/")(1)) & "/" & str.Split("/")(2)
        End If

        Return retorno
    End Function

    Private Sub btnEnviar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnviar.Click
        '' aplicar procedure e recuperar criticas
        'GeraFormCriticas("2, 5, 8")
        VerificaCriticas()


    End Sub
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Function getCritica(ByVal n As Integer, ByVal dt As Data.DataTable) As String
        For i As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(i)(0) = n Then
                Return dt.Rows(i)(1)
            End If
        Next
    End Function

    Function Undefned(ByVal value As String)
        If value = "undefined" Then
            Return ""
        End If
        Return value
    End Function

End Class
