<%@ Page Language="vb" AutoEventWireup="false" Codebehind="alterarVidas.aspx.vb" Inherits="segw0068.alterarVidas"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>alterarVidas</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
			<LINK href="CSS/base0068.css" type="text/css" rel="stylesheet">
				<link href="scripts/box/box.css" rel="stylesheet" type="text/css" media="all">
					<script type="text/javascript" src="scripts/box/AJS.js"></script>
					<script type="text/javascript" src="scripts/box/box.js"></script>
					<script type="text/javascript" src="scripts/overlib421/mini/overlibY.js"></script>
					<script>		
		
		function validaCampos() {
		var cpf = document.getElementById("txtLblCpf").value;
			if(!verifica_cpf(cpf)) {			
				alert('Informe um CPF v�lido.');				
				return false;				
			}
			
			return true;
		}			
		function isEmpty(str) 
		{ 
			if (str==null) return true
			
			for (var intLoop = 0; intLoop < str.length; intLoop++)
				if (" " != str.charAt(intLoop))
					return false;            
			return true; 
		}
		function verifica_cpf(valor) 
		{
			if (isEmpty(valor))
			return false;

			valor = valor.toString()
			
			valor = valor.replace( "-", "" );
			valor = valor.replace( "-", "" );
			valor = valor.replace( ".", "" );
			valor = valor.replace( ".", "" );
			valor = valor.replace( ".", "" );		

			if ((isNaN(valor)) && (valor.length != 11))
			return false
			
			Mult1 = 10   
			Mult2 = 11
			dig1 = 0
			dig2 = 0
			
			for(var i=0;i<=8;i++)
			{
				ind = valor.charAt(i)
				dig1 += ((parseFloat(ind))* Mult1)
				Mult1--
			}
			
			for(var i=0;i<=9;i++)
			{
				ind = valor.charAt(i)
				dig2 += ((parseFloat(ind))* Mult2)
				Mult2--
			}

			dig1 = (dig1 * 10) % 11   
			dig2 = (dig2 * 10) % 11   
			
			if (dig1 == 10)
			dig1 = 0
		      
			if(dig2 == 10)
			dig2 = 0
			 
			if (parseFloat(valor.charAt(9)) != dig1)
				return false   
			if (parseFloat(valor.charAt(10)) != dig2)
				return false   
			
			//Verificar a digita��o de CPF com todos os d�gitos iguais
			igual = new Array();
			for (var i=0;i<=10;i++)
			{
				if (i == 0) {
					anterior = valor.substring(i,i+1);
				}
				else {
					if (anterior == valor.substring(i,i+1)) {
						igual[i] = "sim";
					}
					anterior = valor.substring(i,i+1);
				}
			}
			var cont = 1;
			for (var i=0;i<=10;i++)
			{
				if (igual[i] == "sim") {
					cont++;
				}
			}
			if (cont == 11)
				return false
			
			return true
		}
	
	
	function FormataCPF(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres){
		var wTecla, wVr, wTam;

		wTecla = pTeclaPres.keyCode;
		wVr = pForm[pCampo].value;
		wVr = wVr.toString().replace( "-", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( ".", "" );
		wVr = wVr.toString().replace( "/", "" );
		wTam = wVr.length ;

		if(wTam > pTamMax) {
			wTam = pTamMax;
			wVr = wVr.substr(0, pTamMax)
		}

		if (wTam < pTamMax && wTecla != 8) { 
			wTam = wVr.length + 1 ; 
		}

		if (wTecla == 8 ) { 
			wTam = wTam - 1 ; 
		}

		if ( wTecla == 8 || wTecla == 88 || wTecla >= 48 && wTecla <= 57 || wTecla >= 96 && wTecla <= 105 ){
			if ( wTam <= 2 ){
				pForm[pCampo].value = wVr ;
			}
			if (wTam > pPosTraco && wTam <= pTamMax) {
				wVr = wVr.substr(0, wTam - pPosTraco) + '-' + wVr.substr(wTam - pPosTraco, wTam);
			}
			if ( wTam == pTamMax){
				wVr = wVr.substr( 0, wTam - pPos1 ) + '.' + wVr.substr(wTam - pPos1, 3) + '.' + wVr.substr(wTam - pPos2, wTam);
			}
			pForm[pCampo].value = wVr;
		}
	}
	function formataCampo(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres) {		
		if(pForm.DropDownList2.value == "CPF") { 
			FormataCPF(pForm,pCampo,pTamMax,pPos1,pPos2,pPosTraco,pTeclaPres);
		}		
	}

					</script>
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server" onsubmit="return validaCampos()">
			<TABLE id="Table1" height="142" cellSpacing="1" cellPadding="1" width="100%" border="0">
				<TR>
					<TD vAlign="middle" align="left" colSpan="2" height="28">
						<span id="lblTitulo" class="TituloUsuario" style="FONT-WEIGHT:bold;WIDTH:200px">:: Vida Criticada</span>&nbsp;
					</TD>
				</TR>
				<TR>
					<TD noWrap align="right">
						<asp:Label id="lblNome" runat="server" CssClass="DescricaoCampo">Nome:</asp:Label>
					</TD>
					<TD>
						<asp:Label id="txtLblNome" runat="server" CssClass="DescricaoCampo"></asp:Label>
					</TD>
				</TR>
				<TR>
					<TD vAlign="middle" align="left" colSpan="2" height="10"></TD>
				</TR>
				<TR>
					<TD noWrap align="right">
						<asp:Label id="lblCpf" runat="server" CssClass="DescricaoCampo">CPF:</asp:Label></TD>
					<TD>
						<asp:TextBox id="txtLblCpf" runat="server" CssClass="DescricaoCampo" MaxLength="14" onkeyup="FormataCPF(document.getElementById('Form1'),'txtLblCpf',11,8,5,2,event);"></asp:TextBox>
					</TD>
				</TR>
				<TR>
					<TD vAlign="middle" align="left" colSpan="2" height="10"></TD>
				</TR>
				<TR>
					<TD noWrap align="right">
						<asp:Label id="lblNasc" runat="server" CssClass="DescricaoCampo">Data de Nascimento:</asp:Label></TD>
					<TD>
						<asp:Label id="txtLblNasc" runat="server" CssClass="DescricaoCampo"></asp:Label>
					</TD>
				</TR>
				<TR>
					<TD vAlign="middle" align="left" colSpan="2" height="18"></TD>
				</TR>
				<TR>
					<TD align="center" colSpan="2">
						<asp:Button id="btnGravar" runat="server" Width="65px" CssClass="Botao" Text="Gravar"></asp:Button>
						<INPUT class="Botao" id="btnFecharDependente" name="btnFecharDependente" Width="62px" value="Cancelar"
							type="button" onclick="parent.parent.GB_reloadOnClose('true');parent.parent.GB_hide();"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
