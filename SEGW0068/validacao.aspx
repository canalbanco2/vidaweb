<%@ Page Language="vb" AutoEventWireup="false" Codebehind="validacao.aspx.vb" Inherits="segw0068.validacao"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>validacao</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<LINK href="CSS/EstiloBase.css" type="text/css" rel="stylesheet">
			<LINK href="CSS/base0068.css" type="text/css" rel="stylesheet">
				<LINK media="all" href="scripts/box/box.css" type="text/css" rel="stylesheet">
					<script src="scripts/jquery.js" type="text/javascript"></script>
					<script src="scripts/jquery.maskedinput-1.1.2.js" type="text/javascript"></script>
					<script src="scripts/jquery.blockUI.js" type="text/javascript"></script>
					<style>
					.erro { BORDER-RIGHT: red 2px solid; BORDER-TOP: red 2px solid; BACKGROUND: #fcf884; BORDER-LEFT: red 2px solid; BORDER-BOTTOM: red 2px solid }
					</style>
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblNome" cellSpacing="1" cellPadding="1" border="0" runat="server">
				<TR>
					<TD colSpan="2" align="center" bgColor="#cccccc"><asp:label id="lblNome" runat="server" Font-Bold="True"></asp:label></TD>
				</TR>
				<TR>
					<TD width="150">
						<P align="right"><asp:label id="Label1" runat="server">Nome:</asp:label></P>
					</TD>
					<TD width="150"><asp:textbox id="txtNome" runat="server" Width="144px"></asp:textbox></TD>
				</TR>
			</TABLE>
			<TABLE id="tblCPF" cellSpacing="1" cellPadding="1" border="0" runat="server">
				<TR>
					<TD colSpan="2" align="center" bgColor="#cccccc"><asp:label id="lblCPF" runat="server" Font-Bold="True"></asp:label></TD>
				</TR>
				<TR>
					<TD width="150">
						<P align="right"><asp:label id="Label2" runat="server">CPF:</asp:label></P>
					</TD>
					<TD width="150"><asp:textbox id="txtCPF" runat="server" Width="144px"></asp:textbox></TD>
				</TR>
			</TABLE>
			<TABLE id="tblDtNasc" cellSpacing="1" cellPadding="1" border="0" runat="server">
				<TR>
					<TD colSpan="2" align="center" bgColor="#cccccc"><asp:label id="lblDtNasc" runat="server" Font-Bold="True"></asp:label></TD>
				</TR>
				<TR>
					<TD width="150">
						<P align="right"><asp:label id="Label3" runat="server">Data de Nascimento:</asp:label></P>
					</TD>
					<TD width="150"><asp:textbox id="txtDtNasc" runat="server" Width="144px"></asp:textbox></TD>
				</TR>
			</TABLE>
			<TABLE id="tblSexo" cellSpacing="1" cellPadding="1" border="0" runat="server">
				<TR>
					<TD colSpan="2" align="center" bgColor="#cccccc"><asp:label id="lblSexo" runat="server" Font-Bold="True"></asp:label></TD>
				</TR>
				<TR>
					<TD width="150">
						<P align="right"><asp:label id="Label4" runat="server">Sexo:</asp:label></P>
					</TD>
					<TD width="150">
						<asp:RadioButtonList id="rdSexo" runat="server" RepeatDirection="Horizontal">
							<asp:ListItem Value="M">Masculino</asp:ListItem>
							<asp:ListItem Value="F">Feminino</asp:ListItem>
						</asp:RadioButtonList></TD>
				</TR>
			</TABLE>
			<TABLE id="tblSalario" cellSpacing="1" cellPadding="1" border="0" runat="server">
				<TR>
					<TD colSpan="2" align="center" bgColor="#cccccc"><asp:label id="lblSalario" runat="server" Font-Bold="True"></asp:label></TD>
				</TR>
				<TR>
					<TD width="150">
						<P align="right"><asp:label id="Label5" runat="server">Salário:</asp:label></P>
					</TD>
					<TD width="150"><asp:textbox id="txtSalario" runat="server" Width="144px"></asp:textbox></TD>
				</TR>
			</TABLE>
			<TABLE id="tblCapital" cellSpacing="1" cellPadding="1" border="0" runat="server">
				<TR>
					<TD colSpan="2" align="center" bgColor="#cccccc"><asp:label id="lblCapital" runat="server" Font-Bold="True"></asp:label></TD>
				</TR>
				<TR>
					<TD width="150">
						<P align="right"><asp:label id="Label6" runat="server">Capital:</asp:label></P>
					</TD>
					<TD width="150"><asp:textbox id="txtCapital" runat="server" Width="144px"></asp:textbox></TD>
				</TR>
			</TABLE>
			<TABLE id="tblDtDeslig" cellSpacing="1" cellPadding="1" border="0" runat="server">
				<TR>
					<TD colSpan="2" align="center" bgColor="#cccccc"><asp:label id="lblDtDeslig" runat="server" Font-Bold="True"></asp:label></TD>
				</TR>
				<TR>
					<TD width="150">
						<P align="right"><asp:label id="Label7" runat="server">Data de Desligamento</asp:label></P>
					</TD>
					<TD width="150"><asp:textbox id="txtDtDeslig" runat="server" Width="144px"></asp:textbox></TD>
				</TR>
			</TABLE>
			<TABLE id="tblDtRisco" cellSpacing="1" cellPadding="1" border="0" runat="server">
				<TR>
					<TD colSpan="2" height="22" align="center" bgColor="#cccccc"><asp:label id="lblDtRisco" runat="server" Font-Bold="True"></asp:label></TD>
				</TR>
				<TR>
					<TD width="150" align="right"><asp:label id="Label8" runat="server">Data de Risco:</asp:label></TD>
					<TD width="150"><asp:textbox id="txtDtRisco" runat="server" Width="144px"></asp:textbox></TD>
				</TR>
			</TABLE>
			<TABLE id="tblCritica9" cellSpacing="1" cellPadding="1" border="0" runat="server" width="300">
				<TR>
					<TD colSpan="2" height="22" align="center" bgColor="#cccccc"><asp:label id="lbl9" runat="server" Font-Bold="True"></asp:label></TD>
				</TR>
				<TR>
					<td colSpan="2">Link para Download: <a href="Downloads/Proposta_de_Adesao_com_DPS_-_BB.doc">
							Formulário DPS</a></td>
				</TR>
			</TABLE>
			<TABLE id="tblCritica10" cellSpacing="1" cellPadding="1" border="0" runat="server" width="300">
				<TR>
					<TD colSpan="2" height="22" align="center" bgColor="#cccccc"><asp:label id="lbl10" runat="server" Font-Bold="True"></asp:label></TD>
				</TR>
				<TR>
					<td colSpan="2">Link para Download: <a href="Downloads/FICHA_DE_INFORMACOES_FINANCEIRAS_BB.doc">
							Formulário de Ficha financeira</a></td>
				</TR>
			</TABLE>
			<asp:Button id="btnEnviar" runat="server" Text="OK" CssClass="botao"></asp:Button>&nbsp;
			<INPUT id="FrameButton" type="button" class="botao" value="Cancelar" onclick="$(parent.document.getElementById('popup')).hide();$(parent.document.getElementById('popup')).empty();">
			<asp:Literal id="Literal1" runat="server"></asp:Literal></form>
		<script>
			$('#btnEnviar').bind('click', function(){
				$(parent.document.getElementById('divAguardeAbrir')).click();
			});
				
			$("#txtCPF").mask("999.999.999-99");
			$("#txtDtNasc").mask("99/99/9999");			
			$("#txtDtDeslig").mask("99/99/9999");
			$("#txtDtRisco").mask("99/99/9999");
			
			$(parent.document.getElementById('divAguardeFechar')).click();
		</script>
	</body>
</HTML>
