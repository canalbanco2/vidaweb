Imports System.Reflection
Imports System.Data
Imports System.Web.UI
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.IO
Imports Alianca.Seguranca.BancoDados


Partial Class MinutasEndosso
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents pnlDependente As System.Web.UI.WebControls.Panel
    Protected WithEvents imgPlus As System.Web.UI.WebControls.ImageButton



    Protected WithEvents pnlConstrucao As System.Web.UI.WebControls.Panel

    Dim sbuffer As New System.Text.StringBuilder


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblMensagem.Visible = False

        'Implementa��o do LinkSeguro
        Dim linkseguro As New Alianca.Seguranca.Web.LinkSeguro

        Dim usuario As String = linkseguro.LerUsuario("MinutasEndosso.aspx", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))

        '------alterar ao terminar
        If usuario = "" Then
            Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
        End If
        '--------------------------------------------------

        Session("usuario_id") = linkseguro.Usuario_ID
        Session("usuario") = linkseguro.Login_WEB
        Session("cpf") = linkseguro.CPF

        'Implementa��o do controle de ambiente
        Dim cAmbiente As New Alianca.Seguranca.Web.ControleAmbiente

        If Not Page.IsPostBack Then
            Session("indice") = 0
            For Each m As String In Request.QueryString.Keys
                If m <> "" Then
                    Session(m) = Request.QueryString(m)
                End If
            Next
        End If

        Try
            If Session("apolice") = "" Then
                cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
            End If
        Catch ex As Exception
            cUtilitarios.escreveScript("top.window.location='http://www.aliancadobrasil.com.br/funcao/erro_secao.asp';")
            Response.End()
        End Try

        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
        cAmbiente.ObterAmbiente(url)
        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
        Else
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
        End If

        Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)

        If Not Page.IsPostBack Then

            lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomeSubgrupo") & "<br>Fun��o: Inserir minutas de endosso"

            lblVigencia.Text = getPeriodoCompetenciaSession()

            'Carrega a grid de minutas
            CarregarMinutas()

            Dim uploader As New MinutaUploader(Me.MapPath("."), Session("GLAMBIENTE_ID").ToString, Session("usuario"))
            uploader.VerificarPermissaoEscrita()
        End If

        If Session("acesso") > 3 AndAlso Session("acesso") < 7 Then
            btnImportacaoMassiva.Visible = False
        End If
        btnImportacaoMassiva.PostBackUrl = "ImportacaoMassiva.aspx?SLinkSeguro=" & Request("SLinkSeguro")

    End Sub

    Private Sub ExibeMensagem(ByVal mensagem As String, ByVal tipo As String)
        If mensagem = "" Then
            Return
        End If

        Select Case tipo
            Case "Alerta"
                lblMensagem.ForeColor = Color.DarkOrange
            Case "Sucesso"
                lblMensagem.ForeColor = Color.Green
            Case "Erro"
                lblMensagem.ForeColor = Color.Red
        End Select
        lblMensagem.Visible = True
        lblMensagem.Text = mensagem
    End Sub

    'O alerta dever� ser exibido sempre que houver minuta_tb.situacao <> "v"
    '   pendente com cliente = p (setado na importa��o massiva)
    '   pendente com usu�rio = u (setado no versionamento)
    '   visto                = v (setado no download)
    'Ser� criado alterar ou criar procedure para que possa setar o documento como visto
    Private Sub MostrarAlertaDocumentosPendentes()
        lblAlertaPendencias.Visible = True
    End Sub

    Private Sub AbrirDetalheInclusaoMinuta()
        hdnDadosMinuta.Value = String.Empty
        fileMinuta.Attributes.Add("value", "")
    End Sub

    Private Sub CarregarMinutas()
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.vida_web_db)
        Dim dr As SqlDataReader = Nothing

        Try
            hdnDadosMinuta.Value = String.Empty
            bd.ObterListaMinutas(Session("apolice"), Session("ramo"), Session("subgrupo_id"))
            dr = bd.ExecutaSQL_DR()

            Dim minutas As New Generic.List(Of Minuta)

            While dr.Read()
                'atribuir resultados � lista de objetos para que a pagina��o nativa da gridview aconte�a
                Dim minuta As New Minuta
                With minuta
                    .ID = Convert.ToInt32(dr("minuta_id"))
                    .ApoliceId = Convert.ToInt32(dr("apolice_id"))
                    .RamoId = Convert.ToInt32(dr("ramo_id"))
                    .SubgrupoId = Convert.ToInt32(IIf(IsDBNull(dr("subgrupo_id")), 0, dr("subgrupo_id")))
                    .NomeArquivo = dr("nome_arquivo").ToString
                    .Situacao = Convert.ToChar(dr("situacao"))
                    .Versao = Convert.ToInt32(dr("versao"))
                    .AgenciaId = Convert.ToInt32(IIf(IsDBNull(dr("agencia_id")), 0, dr("agencia_id")))
                End With
                minutas.Add(minuta)
            End While

            grdMinutas.DataSource = minutas
            grdMinutas.DataBind()

        Catch ex As Exception
            Dim excp As New clsException(ex, "N�o foi poss�vel carregar a lista de minutas.")
            System.Web.HttpContext.Current.Response.Write("<script>try{top.escondeaguarde();}catch(err){}</script>")

        Finally
            dr.Close()
            dr = Nothing
        End Try

    End Sub
#Region "M�TODOS DA GRID DE MINUTAS"

    Sub grdMinutas_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)

        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim minuta As Minuta = DirectCast(e.Row.DataItem, Minuta)

            Dim lbtDownloadArquivo As LinkButton = DirectCast(e.Row.FindControl("lbtDownloadArquivo"), LinkButton)
            Dim lbtIncluirVersao As LinkButton = DirectCast(e.Row.FindControl("lbtIncluirVersao"), LinkButton)

            Dim linkButton As LinkButton = DirectCast(e.Row.Cells(2).Controls(0), LinkButton)
            linkButton.OnClientClick = "return confirm('Deseja remover a minuta selecionada e todas as suas vers�es?');"

            lbtDownloadArquivo.CommandArgument = minuta.NomeArquivo
            lbtDownloadArquivo.Text = minuta.NomeArquivo
            lbtIncluirVersao.CommandArgument = minuta.ID & ";" & minuta.Versao 'ID da minuta ; Versao Atual

            'Checar a situa��o, caso o label de aviso de pend�ncia ainda esteja escondido e situacao <> "v"
            If Not lblAlertaPendencias.Visible And minuta.Situacao <> "v" Then
                Call MostrarAlertaDocumentosPendentes()
            End If
        End If
    End Sub

    Sub grdMinutas_RowDeleting(ByVal sender As [Object], ByVal e As GridViewDeleteEventArgs)
        Try
            Dim idStr As String = grdMinutas.DataKeys(e.RowIndex).Value.ToString()
            Dim idMinuta As Integer
            If Integer.TryParse(idStr, idMinuta) Then
                Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.vida_web_db)
                bd.ExcluirMinuta(idMinuta)
                bd.ExecutaSQL()
                CarregarMinutas()
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex, "N�o foi poss�vel excluir a minuta.")
            System.Web.HttpContext.Current.Response.Write("<script>try{top.escondeaguarde();}catch(err){}</script>")
        End Try
    End Sub

    Protected Sub grdMinutas_RowCommand(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdMinutas.RowCommand

        Dim idMinutaInt As Integer = 0

        If e.CommandName = "download" Then
            Try
                Dim nomeArquivo As String = e.CommandArgument

                Dim control As Control = DirectCast(e.CommandSource, Control)
                Dim row As GridViewRow = DirectCast(control.NamingContainer, GridViewRow)

                Dim IdMinuta As Integer = Convert.ToInt32(grdMinutas.DataKeys(row.RowIndex).Value.ToString)
                Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.vida_web_db)

                'Atualizar a situacao da Minuta a ser baixada para "v"
                bd.IncluirVersaoMinuta(Session("apolice"), Session("ramo"), Session("subgrupo_id"), IdMinuta, "v", nomeArquivo, Session("usuario"))
                bd.ExecutaSQL()

                Dim uploader As New MinutaUploader(Me.MapPath("."), Session("GLAMBIENTE_ID").ToString, Session("usuario"))
                Dim objFile As System.IO.FileInfo = New IO.FileInfo(uploader.ObterCaminhoCompletoArquivo(nomeArquivo))
                If objFile.Exists Then
                    Response.Clear()
                    Response.AddHeader("Content-Disposition", "attachment; filename=" & nomeArquivo)
                    Response.AddHeader("Content-Length", objFile.Length.ToString())
                    Response.ContentType = "application/octet-stream"
                    Response.WriteFile(objFile.FullName)
                    Response.Flush()
                Else
                    System.Web.HttpContext.Current.Response.Write("<script>alert('Arquivo inexistente ou diret�rio inv�lido. " & nomeArquivo & "');</script>")
                End If
            Catch ex As Exception
                Dim excp As New clsException(ex, "N�o foi poss�vel fazer download da minuta.")
                System.Web.HttpContext.Current.Response.Write("<script>try{top.escondeaguarde();}catch(err){}</script>")
            End Try
        ElseIf e.CommandName = "versao" Then
            Try
                hdnDadosMinuta.Value = e.CommandArgument
                AlternarExibicaoDetalheMinuta(True)
            Catch ex As Exception
                Dim excp As New clsException(ex, "N�o foi poss�vel exibir a vers�o da minuta.")
                System.Web.HttpContext.Current.Response.Write("<script>try{top.escondeaguarde();}catch(err){}</script>")
            End Try
        End If
    End Sub

    Protected Sub grdMinutas_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdMinutas.PageIndexChanging
        Try
            grdMinutas.PageIndex = e.NewPageIndex

            Call CarregarMinutas()
        Catch ex As Exception
            Dim excp As New clsException(ex, "N�o foi poss�vel exibir a lista de minutas.")
            System.Web.HttpContext.Current.Response.Write("<script>try{top.escondeaguarde();}catch(err){}</script>")
        End Try
    End Sub
#End Region

    Protected Sub btnNovaMinuta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNovaMinuta.Click
        AlternarExibicaoDetalheMinuta(True)
        AbrirDetalheInclusaoMinuta()
    End Sub

    Protected Sub btnGravar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGravar.Click
        Try
            AlternarExibicaoDetalheMinuta(False)

            Dim idMinuta As Integer = 0
            Dim versaoMinuta As Integer = 0
            Dim uploader As New MinutaUploader(Me.MapPath("."), Session("GLAMBIENTE_ID").ToString, Session("usuario"))

            If Not String.IsNullOrEmpty(hdnDadosMinuta.Value) Then 'Adi��o de vers�o
                Dim dadosMinuta() As String = hdnDadosMinuta.Value.Split(";")
                If Integer.TryParse(dadosMinuta(0), idMinuta) And _
                    Integer.TryParse(dadosMinuta(1), versaoMinuta) Then
                    uploader.IncluirVersaoMinuta(fileMinuta.PostedFile, "", False, idMinuta, versaoMinuta, Session("apolice"), Session("ramo"), Session("subgrupo_id"))
                End If
            Else 'Nova Minuta
                uploader.IncluirVersaoMinuta(fileMinuta.PostedFile, "", True, 0, 0, Session("apolice"), Session("ramo"), Session("subgrupo_id"))
            End If
            ExibeMensagem(uploader.Mensagem, uploader.TipoMensagem)
            CarregarMinutas()
        Catch ex As Exception
            Dim excp As New clsException(ex, "N�o foi poss�vel gravar a vers�o da minuta.")
            System.Web.HttpContext.Current.Response.Write("<script>try{top.escondeaguarde();}catch(err){}</script>")
        End Try
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        AlternarExibicaoDetalheMinuta(False)
        hdnDadosMinuta.Value = String.Empty
    End Sub

    Private Sub AlternarExibicaoDetalheMinuta(ByVal mostrar As Boolean)
        pnlDadosMinuta.Visible = mostrar
        btnNovaMinuta.Enabled = Not mostrar
    End Sub

    Private Function getPeriodoCompetenciaSession() As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.vida_web_db)
        Dim dr As SqlDataReader = Nothing

        Try
            bd.SEGS5696_SPS(Session("apolice"), Session("ramo"), Session("subgrupo_id"), cConstantes.CWORKFLOW, cConstantes.CVERSAOID)

            dr = bd.ExecutaSQL_DR()
            Dim data_inicio As String = String.Empty
            Dim data_fim As String = String.Empty
            Dim retorno As String = String.Empty

            If dr.HasRows Then
                While dr.Read()

                    data_inicio = cUtilitarios.trataData(dr.GetValue(1).ToString()).Split(" ")(0)
                    data_fim = cUtilitarios.trataData(dr.GetValue(2).ToString()).Split(" ")(0)

                End While

                retorno = "Per�odo de Compet�ncia: " & data_inicio & " a " & data_fim
            Else
                Try
                    If Session("sessionValidate") Is Nothing Then
                        If Request.QueryString.Keys.Count > 0 Then
                            For Each m As String In Request.QueryString.Keys
                                If m <> "" Then
                                    Session(m) = Request.QueryString(m)
                                End If
                            Next

                            Session("sessionValidate") = 1

                            If (Not Session("apolice") Is Nothing) And (Not Session("ramo") Is Nothing) And (Not Session("subgrupo_id") Is Nothing) Then
                                getPeriodoCompetenciaSession()
                                Response.End()
                            End If
                        End If
                    End If
                Catch ex As Exception

                End Try

            End If

            Return retorno

        Catch ex As Exception
            Dim excp As New clsException(ex, "N�o foi poss�vel obter o per�odo de compet�ncia.")
            System.Web.HttpContext.Current.Response.Write("<script>try{top.escondeaguarde();}catch(err){}</script>")

        Finally
            dr.Close()
            dr = Nothing
        End Try

        Return Nothing
    End Function

End Class