<%@ Page Language="vb" AutoEventWireup="false" Codebehind="MinutasEndosso.aspx.vb" Inherits="segw0183.MinutasEndosso" trace = "false" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>MinutasEndosso</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie3-2nav3-0" name="vs_targetSchema">
		<link href="../segw0060/CSS/EstiloBase.css" type="text/css" rel="stylesheet" />		
		<link href="css/MinutasEndosso.css" type="text/css" rel="stylesheet" />		
	</HEAD>
	<body>
		<form id="Form1" method="post" encType="multipart/form-data" runat="server">
            <asp:HiddenField ID="hdnDadosMinuta" runat="server" /><%--idMinuta;versaoMinuta --%>
            <asp:panel id="pnlSimples" Runat="server">
				<P>
					<asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela"></asp:label><BR>
					<BR>
					<asp:Label id="lblVigencia" runat="server" CssClass="Caminhotela"></asp:Label></P>
			</asp:Panel>
			<asp:Label ID="lblAlertaPendencias" Visible="false" CSSClass="Caminhotela" runat="server" ForeColor="red">Existem documentos pendentes com o usu�rio.</asp:Label>
            <asp:Panel ID="pnlMinutas" runat="server" Height="50px" Width="500px">
                <fieldset class="FieldSetMinuta">
                    <legend class="LegendFieldSetMinuta">Minutas de Endosso</legend>
                    <asp:GridView ID="grdMinutas" runat="server" AutoGenerateColumns="False" BorderColor="#999999" PageSize="15" AllowPaging=true
                    BorderStyle="None" BorderWidth="1px" CellPadding="2" DataKeyNames="ID" OnPageIndexChanging="grdMinutas_PageIndexChanging"
                    Width="700px" OnRowDataBound="grdMinutas_RowDataBound" OnRowDeleting="grdMinutas_RowDeleting"
                    OnRowCommand="grdMinutas_RowCommand" EmptyDataText="N�o h� registro de minutas para a ap�lice selecionada" EmptyDataRowStyle-HorizontalAlign="Center">
					<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="titulo-tabela sem-sublinhado"></HeaderStyle>
					<PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <PagerSettings Mode="Numeric" Position="Bottom" PageButtonCount="10" />
                    <AlternatingRowStyle BackColor="Gainsboro" />
                    <Columns>
                        <asp:TemplateField HeaderText="Arquivo" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtDownloadArquivo" runat="server" CommandName="download"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Versao" HeaderText="Vers&#227;o" />
                        <asp:CommandField HeaderText="Excluir" ShowDeleteButton="true" ItemStyle-Width="65px" ItemStyle-HorizontalAlign="Center"
                        DeleteText="<img src='../segw0060/images/btnfechar.gif' alt='Excluir Minuta' border='0'>" />
                        <asp:TemplateField HeaderText="Incluir Vers�o" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="65px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtIncluirVersao" runat="server" CommandName="versao"
                                Text="<img src='../segw0060/images/imgmais.gif' alt='Incluir Vers&#227;o' border='0'>"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                </fieldset>
                <br>
                <asp:Label ID="lblMensagem" runat="server" ></asp:Label>
                <div style="float: right">
                    <asp:Button ID="btnNovaMinuta" runat="server" Text="Nova minuta" CssClass="Botao" />&nbsp;
                    <asp:Button ID="btnImportacaoMassiva" runat="server" Text="Importa��o massiva" CssClass="Botao" />
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDadosMinuta" runat="server" Height="50px" Width="125px" Visible="False">
                <fieldset style="padding-bottom: 10px; padding-left: 10px; width: 100%; padding-right: 10px; padding-top: 0px;">
                    <legend style="color: blue; font-size: 13px; font-weight: bold;">Inclus�o de Vers�o</legend>
                    <table>
                        <tr>
                            <td>Arquivo:</td>
                            <td>
                                <input class="Botao2" id="fileMinuta" type="file" size="59" name="fileMinuta" runat="server" enableviewstate="true"><br />
                                <asp:RegularExpressionValidator ID="regexFileMinuta" runat="server" 
                                    ErrorMessage="O arquivo deve ter extens�o .pdf"
                                    ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.pdf|.PDF)$" 
                                    ControlToValidate="fileMinuta"></asp:RegularExpressionValidator>                            
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <asp:Button ID="btnGravar" runat="server" Text="Gravar" CssClass="Botao" />
                                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="Botao" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </asp:Panel>            
		</FORM>
	</body>
	<script language="javascript" type="text/javascript">
	    if (typeof top.escondeaguarde === "function") {
            top.escondeaguarde();
        }
    </script>
</HTML>
