Public Partial Class WebForm1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblNavegacao.Text = "Ramo/Ap�lice: (" & Session("ramo") & ") " & Session("apolice") & " -> Estipulante: " & Session("estipulante") & "<br>Subgrupo: " & Session("subgrupo_id") & " - " & Session("nomeSubgrupo") & "<br>Fun��o: Importa��o massiva de minutas de endosso"

    End Sub

    Protected Sub btnImportarMinutas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportarMinutas.Click
        Try
            Dim uploader As New MinutaUploader(Me.MapPath("."), Session("GLAMBIENTE_ID").ToString, Session("usuario"))
            uploader.Importar(HttpContext.Current.Request.Files)
            lblMensagem.Text = uploader.ListarArquivosImportados()
        Catch ex As Exception
            Dim excp As New clsException(ex, "N�o foi poss�vel realizar a importa��o massiva das minutas.")
            System.Web.HttpContext.Current.Response.Write("<script>try{top.escondeaguarde();}catch(err){}</script>")
        End Try
    End Sub


End Class