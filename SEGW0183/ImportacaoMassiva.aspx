<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ImportacaoMassiva.aspx.vb" Inherits="segw0183.WebForm1" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
    <title>Importa��o massiva de minutas de endosso</title>
    <link href="../segw0060/css/EstiloBase.css" type="text/css" rel="stylesheet">	
    <link href="css/MinutasEndosso.css" type="text/css" rel="stylesheet" />		
    <script src="js/MinutasEndosso.js" type="text/javascript"></script>
</head>
<body>
	<form id="Form1" method="post" encType="multipart/form-data" runat="server" onsubmit="return MinutasEndosso.confirmUpload(this)">
	    
        <asp:panel id="pnlSimples" Runat="server">
			<P>
				<asp:label id="lblNavegacao" runat="server" CssClass="Caminhotela"></asp:label><BR>
				<BR>
				<asp:Label id="lblVigencia" runat="server" CssClass="Caminhotela"></asp:Label>
			</P>
		</asp:Panel>
		
		<asp:Panel ID="pnlUpload" runat="server" Height="50px" Width="500px">
		    <asp:Label ID="lblMensagem" runat="server" ></asp:Label>
		    <fieldset class="FieldSetMinuta">
	            <legend class="LegendFieldSetMinuta">Selecione um ou mais arquivos a serem importados (com extens�o .pdf):</legend>
                <p id="upload_list">
                    <input class="Botao2" id="fileMinuta1" type="file" size="60" name="fileMinuta1" enableviewstate="true" onchange="MinutasEndosso.addFileInput(this)">
                </p>  
                <ul id="file_list"></ul>
            </fieldset>
            <br>
            <p style="float: right">
                <asp:Button ID="btnImportarMinutas" runat="server" Text="Importar minutas" CssClass="Botao" />
            </p>
        </asp:Panel>
	</form>
	<a id="lnkMinutas" class="CaminhoTela" href="MinutasEndosso.aspx?SLinkSeguro=<%= Request("SLinkSeguro") %>">Retornar � tela de minutas de endosso</a>
	<script language="javascript" type="text/javascript">
	    if (typeof top.escondeaguarde === "function") {
            top.escondeaguarde();
        }
    </script>
</body>
</html>
