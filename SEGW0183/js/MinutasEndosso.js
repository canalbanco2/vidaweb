﻿MinutasEndosso = {
    lastAssignedId: 1,
    
    addFileInput: function(actualFile) {
        if (actualFile.value.toLowerCase().indexOf('.pdf', actualFile.value.length - 4) === -1) {
            alert('O arquivo deve ter extensão .pdf')
            return false
        }
    
        var uploadList = document.getElementById("upload_list");

        // Criar nova caixa de upload
        var newFileInput = document.createElement("input");
        newFileInput.type = "file";
        newFileInput.size = "60";
        newFileInput.className = "Botao2";
        newFileInput.onchange = actualFile.onchange;
        
        // Gerar e setar um novo id
        var totalFileInputs = this.lastAssignedId + 1;
        this.lastAssignedId = totalFileInputs;
        
        var newFileInputId = "fileMinuta" + totalFileInputs;
                
        newFileInput.setAttribute("id", newFileInputId);
        newFileInput.setAttribute("name", newFileInputId);
        
        // Esconder o upload que está sendo feito nesse momento
        actualFile.style.display = "none";
        
        // Incluir o novo upload na lista
        uploadList.appendChild(newFileInput);
        
        // Incluir o nome do arquivo que acabou de ser escolhido em uma lista
        var fileList = document.getElementById("file_list");
        var listItem = document.createElement("li");
        listItem.setAttribute("id", "item_" + actualFile.id);
        listItem.setAttribute("name", "item_" + actualFile.id);
        listItem.innerHTML = actualFile.value.match(/[^\/\\]+$/) + 
            "&nbsp;<a href='#' onclick=\"MinutasEndosso.remove('" + actualFile.id + 
            "')\"><img src='../segw0060/images/btnfechar.gif' alt='Excluir Minuta' border='0'></a>";
        fileList.appendChild(listItem);
         
        return true
    },
    
    confirmUpload: function(e) {
        return confirm('Deseja efetuar o envio das minutas selecionadas?');
    },
    
    remove: function(id) {
        // Input file escondido
        var fileInput = document.getElementById(id)
        fileInput.parentNode.removeChild(fileInput);

        // Link do arquivo
        var listItem = document.getElementById("item_" + id)
        listItem.parentNode.removeChild(listItem);
    }
}