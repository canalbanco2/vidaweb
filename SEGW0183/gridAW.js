 function $(id) {
	return document.getElementById(id);
 }

function makeGrid(idOrigem, maxHeight, maxWidth)	{				
	//var idOrigem =  "gridContent";
		
	var isIE = document.all;		
	var origem = document.getElementById(idOrigem);
	
	tabelaOrig = origem.innerHTML;		
	if(document.getElementById("tabelaOriginal")) {
		origem.innerHTML = $("tabelaOriginal").innerHTML; 
	}
	
	/*/Encontra a tabela a ser processada, 
	considera-se a primeira tabela encontrada dentro da div alvo; //*/	
	if(isIE) {
		var tabela =  origem.childNodes[0];
	} else {
		var tabela =  origem.childNodes[1];
	}
		
   		
	if(tabela.hasChildNodes()) {
		if(tabela.childNodes[0].childNodes[0])								//Para o Internet Explorer
			var header = tabela.childNodes[0].childNodes[0];				
		else
			var header = tabela.childNodes[1].childNodes[0];				//Para o Firefox								
	} else {
		alert("ERROR CHILD!!")
	}
						
	header.id = idOrigem + "_header"; 		//Define o ID do header
	
	var idTmp = "";
	var tamHeader = new Array();				
	var textHeader = new Array();		
	var z = 0;
	var tamanhoMax = 0;
	
	for(i=0; i<header.childNodes.length; i++) {	
		idTmp = idOrigem + "_f_" + i;
		
		header.childNodes[i].id = idTmp;
		
		if(header.childNodes[i].offsetWidth > 0) {
			tamHeader[z] = parseInt(header.childNodes[i].offsetWidth);
			tamanhoMax += tamHeader[z];
			textHeader[z] = header.childNodes[i].innerHTML;
			height = header.childNodes[i].offsetHeight;
			z++;
		}
	}	
	
	//Prepara a grid externa para receber o overflow
	var difer = 25;	
	var diferHeight = 40;
	
	if(isIE)
		diferHeight = 10;
		
	//corrige o height no caso de scroll horizontal
	if(maxWidth > 0 && tabela.offsetWidth > maxWidth) {		
		maxHeight = maxHeight - difer;		
	}					
				
	//busca seus atributos
	var atributos = "";		
	if(isIE) {		
		for(z=0; z<tabela.attributes.length;z++) {
			value = tabela.attributes[z].nodeValue;
			name = tabela.attributes[z].nodeName
			
			if(value != "" && value != null) {			
				atributos += name + "='" + value + "' ";				
			}
		}			
	} else {
		for(z=0; z<tabela.attributes.length;z++) {		
			atributos += tabela.attributes[z].name + "='" + tabela.attributes[z].value + "' ";				
		}
	}

	//Prepara os atributos da tabela interna
	atributos.replace('width', 'oldWidth');			
	atributos.replace('height', 'oldheight');				
	atributos.replace('border', 'oldborder');				
	
	var atributosInterna = atributos;					
	//recalcula o width			
	widthTable = tamanhoMax + difer;
	tamHeader[tamHeader.length - 1] = tamHeader[tamHeader.length - 1] + difer - 2;	
	widthInternalTable = tamanhoMax;
	
	//recalcula o height
	var tmp = parseInt(maxHeight) + parseInt(difer) + height;	
	atributos += "height='" + tmp + "' ";			
	tmp = tmp + parseInt(diferHeight); 
	atributosInterna += "height='" + tmp + "' ";
			
	atributosInterna += "width='" + widthInternalTable + "'";	
	atributos += "width='" + widthTable + "'";	
	
	//Remove o header da tabela
	tabela.deleteRow(0);
										  	
	var conteudo = tabela.innerHTML;
	var numCols = tamHeader.length;
	
	var headerNovo = "<tr style='border:0;'>";
	
	//tamHeader[1] = tamHeader[1] + 5;
	for(i=0; i<tamHeader.length; i++) {
		headerNovo += "<td width='" + tamHeader[i] + "' style='border-bottom:0;'>" + textHeader[i] + "</td>";
	}
	headerNovo += "</tr>";	
	
	var tmp = parseInt(maxHeight) + parseInt(diferHeight);
								
	var tabelaExterna = "<table " + atributos + ">";
		tabelaExterna += headerNovo;		
		tabelaExterna += "<tr><td colspan=" + numCols + " valign='top'>";
		tabelaExterna += "<div style='overflow-y:auto;overflow-x:hide;height:" + tmp + "px;'>";
		tabelaExterna += "<table id='" + idOrigem + "_tabint' " + atributosInterna + ">";
		tabelaExterna += conteudo;
		tabelaExterna += "</table></td></tr></table>";	
	
	if(maxWidth > 0) {
		tabelaExterna = "<div style='overflow-x:auto;overflow-y:hide;width:" + maxWidth + "'>" + tabelaExterna + "</div>";		
	}
	
	//tabelaExterna += "<div id='tabelaOriginal' style='display:none'>" + tabelaOrig + "</div>";
	origem.innerHTML = tabelaExterna;	
}