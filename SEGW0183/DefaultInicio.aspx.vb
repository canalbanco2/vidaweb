Partial Public Class DefaultInicio
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim v_sUsuario As String = "algrigorio"

        Dim v_sValorParametro As String

        Dim v_oParametro As New Alianca.Seguranca.Web.LinkSeguro
        v_sValorParametro = v_oParametro.GerarParametro("MinutasEndosso.aspx", Alianca.Seguranca.Web.LinkSeguro.TempoExpiracao.Grande, Request.UserHostAddress, "algrigorio", Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)

        Dim teste2 As String = v_oParametro.LerUsuario(Nothing, Request.UserHostAddress, v_sValorParametro)

        v_oParametro = Nothing

        Session("acesso") = 1
        Session("ramo") = 93
        Session("apolice") = 17299
        Session("estipulante") = "ALIANCA"
        Session("subgrupo_id") = 1
        Session("nomesubGrupo") = "ALIANCA"

        Response.Redirect("MinutasEndosso.aspx?SLinkSeguro=" & v_sValorParametro & "&pqsId=" & Request.QueryString("pqsId") & _
            "&acesso=" & Session("acesso") & _
            "&ramo=" & Session("ramo") & _
            "&apolice=" & Session("apolice") & _
            "&estipulante=" & Session("estipulante") & _
            "&subgrupo_id=" & Session("subgrupo_id") & _
            "&nomesubGrupo=" & Session("nomesubGrupo"))

    End Sub

End Class
