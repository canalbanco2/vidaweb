Imports System.Data.SqlClient
Imports System.IO
Imports Alianca.Seguranca.BancoDados
Imports System.Collections.Generic
Imports System.Text.RegularExpressions

Public Class MinutaUploader
    ' pablo.dias (Nova Consultoria) - 27/12/2011
    ' 9420650 - Melhorias no Sistema Vida Web 
    ' Classe respons�vel pelo envio das minutas para o servidor

    Private _mensagem As String
    Private _tipo_mensagem As String
    Private _appPath As String
    Private _ambiente As String
    Private _usuario As String

    Private arquivosSucesso As New List(Of String)
    Private arquivosErro As New List(Of String)

    Public ReadOnly Property Mensagem() As String
        Get
            Return _mensagem
        End Get
    End Property

    Public ReadOnly Property TipoMensagem() As String
        Get
            Return _tipo_mensagem
        End Get
    End Property

    Public Sub New(ByVal appPath As String, ByVal ambiente As String, ByVal usuario As String)
        _appPath = appPath
        _ambiente = ambiente
        _mensagem = ""
        _tipo_mensagem = ""
        _usuario = usuario
    End Sub

    Public Sub Importar(ByVal uploads As HttpFileCollection)
        For i As Integer = 0 To uploads.Count - 1
            If (uploads(i).ContentLength > 0) Then
                Dim nomeArquivo As String = System.IO.Path.GetFileName(uploads(i).FileName)
                If arquivosSucesso.IndexOf(nomeArquivo) < 0 Then
                    IncluirVersaoMinuta(uploads(i), nomeArquivo)
                End If
            End If
        Next i
    End Sub

    Public Function ListarArquivosImportados() As String
        Dim lista As String = ""

        If arquivosSucesso.Count > 0 Then
            lista = lista & "<b>Arquivos importados com sucesso:</b><br>"
            For Each arquivo As String In arquivosSucesso
                lista = lista & arquivo & "<br>"
            Next
            lista = lista & "<br>"
        End If

        If arquivosErro.Count > 0 Then
            lista = lista & "<b>Arquivos processados com erro:</b><br>"
            For Each arquivo As String In arquivosErro
                lista = lista & arquivo & "<br>"
            Next
            lista = lista & "<br>"
        End If

        Return lista
    End Function

    Public Sub IncluirVersaoMinuta(ByVal fileMinuta As HttpPostedFile, ByVal nomeArquivo As String)
        If ValidarNomeArquivo(nomeArquivo) Then
            Dim dadosMinuta() As String
            dadosMinuta = nomeArquivo.ToUpper().Trim().Split("_")

            Dim ramo As Integer = 0
            Integer.TryParse(RetornaNumeros(dadosMinuta(0)), ramo)
            Dim apolice As Integer = 0
            Integer.TryParse(RetornaNumeros(dadosMinuta(1)), apolice)
            Dim agencia As Integer = 0
            Dim subgrupo_id As Integer = 0
            If dadosMinuta.Length > 2 Then
                Integer.TryParse(RetornaNumeros(dadosMinuta(2)), agencia)
            End If
            If dadosMinuta.Length > 3 Then
                Integer.TryParse(RetornaNumeros(dadosMinuta(3)), subgrupo_id)
            End If

            IncluirVersaoMinuta(fileMinuta, nomeArquivo, True, 0, 0, apolice, ramo, subgrupo_id)            
        Else
            arquivosErro.Add(nomeArquivo)
        End If
    End Sub

    Private Function ValidarNomeArquivo(ByVal nomeArquivo As String) As Boolean

        Const FORMATO_ARQUIVO_MINUTA As String = ".PDF"

        Dim dadosMinuta() As String
        nomeArquivo = nomeArquivo.ToUpper().Trim()
        dadosMinuta = nomeArquivo.Split("_")

        ' O nome do arquivo deve conter o separador
        If dadosMinuta.Length < 1 Then
            Return False
        End If

        ' 1� parte deve ser ramo
        If Mid(dadosMinuta(0), 1, 1) <> "R" Then
            Return False
        End If

        ' 2� parte deve ser ap�lice
        If Mid(dadosMinuta(1), 1, 2) <> "AP" Then
            Return False
        End If

        ' Extens�o deve ser pdf
        If Right(nomeArquivo, Len(FORMATO_ARQUIVO_MINUTA)) <> FORMATO_ARQUIVO_MINUTA Then
            Return False
        End If

        ' Ramo deve conter um n�mero   
        If Not IsNumeric(RetornaNumeros(dadosMinuta(0))) Then
            Return False
        End If

        ' Ap�lice deve conter um n�mero
        If Not IsNumeric(RetornaNumeros(dadosMinuta(1))) Then
            Return False
        End If

        'Se existir, 3� parte deve conter ag�ncia ou subgrupo
        If UBound(dadosMinuta) >= 2 Then
            If Mid(dadosMinuta(2), 1, 1) <> "S" And Mid(dadosMinuta(2), 1, 1) <> "A" Then
                Return False
            End If
            If Not IsNumeric(RetornaNumeros(dadosMinuta(2))) Then
                Return False
            End If
        End If

        'Se existir, 4� parte ser� subgrupo
        If UBound(dadosMinuta) >= 3 Then
            If Mid(dadosMinuta(3), 1, 1) <> "S" Then
                Return False
            End If
            If Not IsNumeric(RetornaNumeros(dadosMinuta(3))) Then
                Return False
            End If
        End If

        Return True
    End Function

    Private Function RetornaNumeros(ByVal texto As String) As String
        Return Regex.Match(texto, "\d+").Value
    End Function

    Public Sub IncluirVersaoMinuta(ByVal fileMinuta As HttpPostedFile, ByVal fileName As String, ByVal versaoInicial As Boolean, ByVal minuta_id As Integer, ByVal versao As Integer, ByVal apolice As String, ByVal ramo As String, ByVal subgrupo_id As String)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.vida_web_db)
        Dim dr As SqlDataReader = Nothing
        Dim nomeArquivoEnviado As String = String.Empty
        Dim nomeArquivoGerado As String = String.Empty
        Dim dadosCorretos As Boolean = False

        If Not VerificarPermissaoEscrita() Then
            SetarMensagem("N�o h� permiss�o de escrita para envio das minutas.", "Erro", fileName)
            Return
        End If

        Try
            nomeArquivoEnviado = System.IO.Path.GetFileName(fileMinuta.FileName)
            If nomeArquivoEnviado <> "" And System.IO.Path.GetExtension(nomeArquivoEnviado) = ".pdf" Then

                ' Caso venha vazio, o nome f�sico do arquivo ser� gerado pela procedure
                bd.IncluirVersaoMinuta(apolice, ramo, subgrupo_id, IIf(versaoInicial, 0, minuta_id), _
                    IIf(fileName = "", "u", "p"), fileName, _usuario)
                dr = bd.ExecutaSQL_DR()
                If dr.HasRows Then
                    While dr.Read()
                        dadosCorretos = dr.GetValue(0) > 0
                        nomeArquivoGerado = dr.GetValue(1).ToString()
                    End While
                End If

                If dadosCorretos Then
                    If EnviarArquivo(fileMinuta, ObterCaminhoCompletoArquivo(nomeArquivoGerado)) Then
                        EnviarEmails(apolice, ramo, subgrupo_id)
                        SetarMensagem("Vers�o de minuta inclu�da com sucesso!", "Sucesso", fileName)
                    Else
                        SetarMensagem("Falha ao realizar o upload do arquivo, vers�o de minuta n�o inserida", "Erro", fileName)
                    End If
                Else
                    SetarMensagem("Os valores informados no nome do arquivo n�o existem.", "Erro", fileName)
                End If

            Else
                System.Web.HttpContext.Current.Response.Write("<script>alert('Arquivo inexistente ou formato n�o permitido (deve ser extens�o .pdf')</script>")
            End If
        Catch ex As Exception
            Dim excp As New clsException(ex, "N�o foi poss�vel incluir a vers�o da minuta.")
            System.Web.HttpContext.Current.Response.Write("<script>try{top.escondeaguarde();}catch(err){}</script>")
        Finally
            dr.Close()
            dr = Nothing
        End Try
    End Sub

    Private Function EnviarEmails(ByVal apolice As String, ByVal ramo As String, ByVal subgrupo_id As String) As Boolean
        Dim retorno As Boolean = True
        Try
        'Envio de e-mails s� deve ocorrer no ambiente de produ��o
            Dim enumAmb As cCon.Ambientes
            enumAmb = CType(System.Enum.Parse(GetType(cCon.Ambientes), _ambiente), cCon.Ambientes)
            If enumAmb.Equals(cCon.Ambientes.Produ��o) Or enumAmb.Equals(cCon.Ambientes.PRODUCAO_ABS) Then
                '2- Administradores de Ap�lice 
                EnviarEmail(apolice, ramo, subgrupo_id, 2)
                '3 - Administrador de Subgrupo
                EnviarEmail(apolice, ramo, subgrupo_id, 3)
                '4 - Operador
                EnviarEmail(apolice, ramo, subgrupo_id, 4)
                '7 - Master
                EnviarEmail(apolice, ramo, subgrupo_id, 7)
            End If


            'Renato Vasconcelos
            'Flow 14582231
            'Inicio
            EnviarEmailMinuta(apolice, ramo, subgrupo_id)
            'Fim
        Catch ex As Exception
            System.Web.HttpContext.Current.Response.Write("<script>alert('Falha ao enviar e-mail de aviso de nova minuta.')</script>")
            retorno = False
        End Try

        EnviarEmails = retorno
    End Function

    Private Sub SetarMensagem(ByVal mensagem As String, ByVal tipoMensagem As String, ByVal nomeArquivo As String)
        _mensagem = mensagem
        _tipo_mensagem = tipoMensagem
        If nomeArquivo <> "" Then
            If tipoMensagem = "Sucesso" Then
                arquivosSucesso.Add(nomeArquivo)
            End If
            If tipoMensagem = "Erro" Then
                arquivosErro.Add(nomeArquivo)
            End If
        End If
    End Sub

    Private Sub ApagarArquivo(ByVal arquivo As String)
        If File.Exists(arquivo) Then
            File.Delete(arquivo)
        End If
    End Sub

    Private Sub EnviarEmail(ByVal apolice As Integer, ByVal ramo As Integer, ByVal subgrupo_id As Integer, ByVal acesso As Integer)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.vida_web_db)
        Dim dr As SqlDataReader = Nothing
        Dim email As String = String.Empty
        Dim nome As String = String.Empty
        Dim msgHtml As String = String.Empty
        Dim assunto As String = String.Concat("Ap�lice ", apolice, " - Subgrupo ", subgrupo_id)

        bd.ObterNomeEmailDestinatario(apolice, ramo, subgrupo_id, acesso)
        dr = bd.ExecutaSQL_DR()

        While dr.Read()
            If Not String.IsNullOrEmpty(dr("Email")) Then
                email = email & dr("Email") & ";"
                nome = nome & dr("nome") & ", "
            End If
        End While

        'retira a �ltima v�rgula
        If Not String.IsNullOrEmpty(email) Then email = Left(email, Len(email) - 1)
        If Not String.IsNullOrEmpty(nome) Then nome = Left(nome, Len(nome) - 2)

        'Deve ser inserido email do desenvolvedor ao testar em desenvolvimento
        'nome = "Nome Desenv"
        'email = "desenvolvedor@teste.com.br"

        If Not String.IsNullOrEmpty(email) Then
            msgHtml = MontarEmailHtml(apolice, ramo, subgrupo_id, nome)
            bd.GravarEmail(email, assunto, msgHtml, _usuario)
            bd.ExecutaSQL()
        End If

    End Sub

    'Renato Vasconcelos
    'Flow  14582231 
    'Inicio
    Private Sub EnviarEmailMinuta(ByVal apolice As Integer, ByVal ramo As Integer, ByVal subgrupo_id As Integer)
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.vida_web_db)
        Dim dr As SqlDataReader = Nothing
        Dim email As String = String.Empty
        Dim msgHtml As String = String.Empty
        Dim assunto As String = String.Concat("Ap�lice ", apolice, " - Subgrupo ", subgrupo_id)
        bd.SEGS10381_SPS()
        dr = bd.ExecutaSQL_DR()

        If dr.Read() Then
            email = dr("email")
        End If




        'retira a �ltima v�rgula
        If Not String.IsNullOrEmpty(email) Then email = Left(email, Len(email) - 1)


        'Deve ser inserido email do desenvolvedor ao testar em desenvolvimento
        'nome = "Nome Desenv"
        'email = "desenvolvedor@teste.com.br"

        If Not String.IsNullOrEmpty(email) Then
            msgHtml = MontarEmailHtml(apolice, ramo, subgrupo_id)
            bd.GravarEmail(email, assunto, msgHtml, _usuario)
            bd.ExecutaSQL()
        End If

    End Sub
    'Fim

    Public Function ObterCaminhoCompletoArquivo(ByVal arquivo As String) As String
        ObterCaminhoCompletoArquivo = ObterDiretorio() & arquivo
    End Function

    Private Function EnviarArquivo(ByVal postedFile As HttpPostedFile, ByVal fileName As String) As Boolean
        Dim retorno As Boolean = True
        Try
            postedFile.SaveAs(fileName)
        Catch ex As Exception
            System.Web.HttpContext.Current.Response.Write("<script>alert('N�o foi poss�vel anexar o arquivo.')</script>")
            retorno = False
        End Try

        EnviarArquivo = retorno
    End Function

    Private Function ObterDiretorio() As String
        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.vida_web_db)
        Dim dr As SqlDataReader = Nothing
        Dim dir As String = String.Empty

        'Obter o n�mero do ambiente do sistema que foi setado na pageload 'Session("GLAMBIENTE_ID")'
        Dim enumAmb As cCon.Ambientes
        enumAmb = CType(System.Enum.Parse(GetType(cCon.Ambientes), _ambiente), cCon.Ambientes)
        Dim numAmbStr As String = enumAmb

        bd.ObterDiretorioArquivos(numAmbStr)
        dr = bd.ExecutaSQL_DR()

        While dr.Read()
            If Not String.IsNullOrEmpty(dr("valor")) Then dir = dr("valor")
        End While

        ObterDiretorio = dir & "\processados\"
    End Function

    ' pablo.dias (Nova Consultoria) - 25/11/2011
    ' 9420650 - Melhorias no Sistema Vida Web 
    ' Fun��o para teste de permiss�o de escrita na pasta usada pela aplica��o
    Public Function VerificarPermissaoEscrita() As Boolean
        Dim diretorio As String = ObterDiretorio()
        Dim retorno As Boolean = True
        Dim nomeArquivoTeste As String = diretorio + System.Guid.NewGuid.ToString
        Try
            Dim fs As FileStream = File.Create(nomeArquivoTeste, 1024)
            fs.Close()
            ApagarArquivo(nomeArquivoTeste)
        Catch ex As Exception
            Dim mensagemErro As String
            mensagemErro = "<script>alert('O aplicativo SEGW0183 n�o possui permiss�o de escrita: " + ex.Message.Replace(vbCrLf, "") + "')</script>"
            System.Web.HttpContext.Current.Response.Write(mensagemErro)
            retorno = False
        End Try

        VerificarPermissaoEscrita = retorno

    End Function ' pablo.dias (Nova Consultoria) - 25/11/2011

    Private Function MontarEmailHtml(ByVal apolice As Integer, ByVal ramo As Integer, ByVal subgrupo_id As Integer, ByVal Nome As String) As String
        Dim sDiretorio As String = _appPath
        Dim htmlEmail As String = File.ReadAllText(sDiretorio & "/emailMinuta.htm")
        htmlEmail = htmlEmail.Replace("[DIA]", Day(Now))
        htmlEmail = htmlEmail.Replace("[MES]", ObterNomeMes())
        htmlEmail = htmlEmail.Replace("[ANO]", Year(Now))
        htmlEmail = htmlEmail.Replace("[RAMO]", ramo)
        htmlEmail = htmlEmail.Replace("[APOLICE]", apolice)
        htmlEmail = htmlEmail.Replace("[SUBGRUPO]", subgrupo_id)
        htmlEmail = htmlEmail.Replace("[NOME]", Nome)

        MontarEmailHtml = htmlEmail
    End Function

    'Renato Vasconcelos
    'Flow 14582231 
    'Inicio
    Private Function MontarEmailHtml(ByVal apolice As Integer, ByVal ramo As Integer, ByVal subgrupo_id As Integer) As String
        Dim sDiretorio As String = _appPath
        Dim htmlEmail As String = File.ReadAllText(sDiretorio & "/textoMinuta2.htm")
        htmlEmail = htmlEmail.Replace("[DIA]", Day(Now))
        htmlEmail = htmlEmail.Replace("[MES]", ObterNomeMes())
        htmlEmail = htmlEmail.Replace("[ANO]", Year(Now))
        htmlEmail = htmlEmail.Replace("[RAMO]", ramo)
        htmlEmail = htmlEmail.Replace("[APOLICE]", apolice)
        htmlEmail = htmlEmail.Replace("[SUBGRUPO]", subgrupo_id)


        MontarEmailHtml = htmlEmail
    End Function
    'Fim

    Private Function ObterNomeMes() As String
        Dim mes As String = String.Empty

        Select Case Month(Now())
            Case 1
                mes = "Janeiro"
            Case 2
                mes = "Fevereiro"
            Case 3
                mes = "Mar�o"
            Case 4
                mes = "Abril"
            Case 5
                mes = "Maio"
            Case 6
                mes = "Junho"
            Case 7
                mes = "Julho"
            Case 8
                mes = "Agosto"
            Case 9
                mes = "Setembro"
            Case 10
                mes = "Agosto"
            Case 11
                mes = "Outubro"
            Case 12
                mes = "Dezembro"
        End Select

        ObterNomeMes = mes
    End Function

End Class