Public Class Minuta
    Private _minuta_id As Integer
    Private _apolice_id As Integer
    Private _ramo_id As Integer
    Private _subgrupo_id As Integer
    Private _situacao As Char
    Private _nome_arquivo As String
    Private _versao As Integer
    Private _agencia_id As Integer

    Public Property ID() As Integer
        Get
            Return Me._minuta_id
        End Get
        Set(ByVal value As Integer)
            Me._minuta_id = value
        End Set
    End Property

    Public Property ApoliceId() As Integer
        Get
            Return Me._apolice_id
        End Get
        Set(ByVal value As Integer)
            Me._apolice_id = value
        End Set
    End Property

    Public Property RamoId() As Integer
        Get
            Return Me._ramo_id
        End Get
        Set(ByVal value As Integer)
            Me._ramo_id = value
        End Set
    End Property

    Public Property SubgrupoId() As Integer
        Get
            Return Me._subgrupo_id
        End Get
        Set(ByVal value As Integer)
            Me._subgrupo_id = value
        End Set
    End Property

    Public Property Situacao() As Char
        Get
            Return Me._situacao
        End Get
        Set(ByVal value As Char)
            Me._situacao = value
        End Set
    End Property

    Public Property NomeArquivo() As String
        Get
            Return Me._nome_arquivo
        End Get
        Set(ByVal value As String)
            Me._nome_arquivo = value
        End Set
    End Property

    Public Property Versao() As Integer
        Get
            Return Me._versao
        End Get
        Set(ByVal value As Integer)
            Me._versao = value
        End Set
    End Property

    Public Property AgenciaId() As Integer
        Get
            Return Me._agencia_id
        End Get
        Set(ByVal value As Integer)
            Me._agencia_id = value
        End Set
    End Property
End Class
