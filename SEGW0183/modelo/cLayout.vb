Public Class cLayout

#Region "Atributos"
    Private _layoutID As Integer
    Private _separador As String
    Private _tipoArquivo As String
#End Region

#Region "Propriedades"
    Public Property LAYOUT_ID()
        Get
            Return Me._layoutID
        End Get
        Set(ByVal Value)
            Me._layoutID = Value
        End Set
    End Property
    Public Property SEPARADOR()
        Get
            Return Me._separador
        End Get
        Set(ByVal Value)
            Me._separador = Value
        End Set
    End Property
    Public Property TIPOARQUIVO()
        Get
            Return Me._tipoArquivo
        End Get
        Set(ByVal Value)
            Me._tipoArquivo = Value
        End Set
    End Property
#End Region

#Region "Construtores"

    Public Sub New()

    End Sub

    Public Sub New(ByVal layoutid As Integer)
        Me.LAYOUT_ID = layoutid
    End Sub

    Public Sub New(ByVal layoutid As Integer, ByVal separador As String)
        Me.LAYOUT_ID = layoutid
        Me.SEPARADOR = separador
    End Sub

    Public Sub New(ByVal layoutid As Integer, ByVal separador As String, ByVal tipo_arquivo As String)
        Me.LAYOUT_ID = layoutid
        Me.SEPARADOR = separador
        Me.TIPOARQUIVO = tipo_arquivo
    End Sub
#End Region


#Region "M�todos St�ticos"
    Public Shared Function getLayout(ByVal apolice As Int64, ByVal ramo As Int16, ByVal subgrupo As Int16) As cLayout

        Dim bd As New cAcompanhamento(cDadosBasicos.eBanco.web_seguros_db)

        'Pega dados do Layout ativo
        bd.SEGS5763_SPS(apolice, ramo, subgrupo)
        Dim dt As DataTable = bd.ExecutaSQL_DT()
        Dim layout_id As Integer = dt.Rows(0).Item(1)
        'Session("sIdLayout") = layout_id
        Dim separador As Char = CType(dt.Rows(0).Item(2), String).Chars(0)

        Dim layout As New cLayout(layout_id, separador)
        bd = Nothing

        Return layout
    End Function
#End Region

End Class
