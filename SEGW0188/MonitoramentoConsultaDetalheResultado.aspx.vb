Imports System.Data
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Globalization
Imports SEGL0315
Imports Alianca.FrameWork
Imports Alianca.Seguranca.BancoDados

Partial Public Class _DefaultDetalhe
    Inherits System.Web.UI.Page

#Region " Membros "

    Private m_oRN As ClsDetalheMonitoramentoWebRN

#End Region

#Region " Propriedades "

    Public Shadows ReadOnly Property RN() As ClsDetalheMonitoramentoWebRN
        Get
            If IsNothing(m_oRN) Then
                m_oRN = New ClsDetalheMonitoramentoWebRN
            End If
            Return m_oRN
        End Get
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then

            lblNavegacao.Text = "Fun��o: Monitoramento das a��es no sistema Vida Web"

            If Request.QueryString("mensagemSucesso") = 1 Then
                GerarAlertMensagem(eMensagem.AlteracaoSucesso, Me.Page)
            End If

            Session("usuario") = Request.QueryString("usuario")
            Session("usuarioLinkSeguro") = Request.QueryString("usuarioLinkSeguro")

        End If

    End Sub

    Public Function Select_DataTableDetalhe(ByVal dt_inicio_vigencia As String, _
                                            ByVal dt_fim_vigencia As String, _
                                            ByVal prazo_final As String, _
                                            ByVal agencia As String, _
                                            ByVal criterio_agencia As String, _
                                            ByVal ordenacao As String, _
                                            ByVal monitoramento_id As Integer) As DataTable

        Dim v_oDtConsulta As DataTable
        Dim v_aArrayParametros As New ArrayList

        v_aArrayParametros.Add(New ParametroDB("@dt_inicio_vigencia", IIf(String.IsNullOrEmpty(dt_inicio_vigencia), DBNull.Value, dt_inicio_vigencia)))
        v_aArrayParametros.Add(New ParametroDB("@dt_fim_vigencia", IIf(String.IsNullOrEmpty(dt_fim_vigencia), DBNull.Value, dt_fim_vigencia)))
        v_aArrayParametros.Add(New ParametroDB("@prazo_final", IIf(String.IsNullOrEmpty(prazo_final), DBNull.Value, prazo_final)))
        v_aArrayParametros.Add(New ParametroDB("@agencia", IIf(String.IsNullOrEmpty(agencia), DBNull.Value, agencia)))
        v_aArrayParametros.Add(New ParametroDB("@criterio_agencia", IIf(String.IsNullOrEmpty(criterio_agencia), DBNull.Value, criterio_agencia)))
        v_aArrayParametros.Add(New ParametroDB("@ordenacao", IIf(String.IsNullOrEmpty(ordenacao), DBNull.Value, ordenacao)))
        v_aArrayParametros.Add(New ParametroDB("@monitoramento_id", IIf(monitoramento_id = 0, DBNull.Value, monitoramento_id)))
        'v_aArrayParametros.Add(New ParametroDB("@list_monitoramento_ids", IIf(String.IsNullOrEmpty(Session("list_monitoramento_ids")), DBNull.Value, Session("list_monitoramento_ids"))))

        v_oDtConsulta = Me.RN.Listar(v_aArrayParametros).Tables(0)

        Return v_oDtConsulta

    End Function

    Public Sub gravar_dados_detalhe(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gravar.Click

        Dim GridRow As GridViewRow

        Dim idx_monitoramentoId As Integer = IndiceColuna(gvDetalhe, "monitoramento_Id")
        Dim idx_apoliceId As Integer = IndiceColuna(gvDetalhe, "Ap�lice")
        Dim idx_ramoId As Integer = IndiceColuna(gvDetalhe, "ramo_id")
        Dim idx_subgrupoId As Integer = IndiceColuna(gvDetalhe, "N� Subgrupo")

        Dim idx_Data_Envio As Integer = IndiceColuna(gvDetalhe, "dt_envio")
        Dim idx_Num_Carta_Envio As Integer = IndiceColuna(gvDetalhe, "numero_envio")
        Dim idx_Comentario As Integer = IndiceColuna(gvDetalhe, "comentario")
        Dim idx_Fatura_Id As Integer = IndiceColuna(gvDetalhe, "Fatura")

        Try
            For Each GridRow In gvDetalhe.Rows
                If GridRow.RowType = DataControlRowType.DataRow Then

                    Dim txtCometario As TextBox = gvDetalhe.Rows(GridRow.RowIndex()).FindControl("txtComentario")
                    Dim txtDt_Envio As TextBox = gvDetalhe.Rows(GridRow.RowIndex()).FindControl("txtDt_Envio")
                    Dim txtNumEnvio As TextBox = gvDetalhe.Rows(GridRow.RowIndex()).FindControl("txtNum_envio")


                    Dim monitoramentoId As Integer = CType(GridRow.Cells(idx_monitoramentoId).Text, Integer)
                    Dim apoliceId As Integer = CType(GridRow.Cells(idx_apoliceId).Text, Integer)
                    Dim ramoId As Integer = CType(GridRow.Cells(idx_ramoId).Text, Integer)

                    'Dim faturaId As Integer = CType(GridRow.Cells(idx_Fatura_Id).Text, Integer)
                    Dim faturaId As Integer = CType(GridRow.Cells(idx_Fatura_Id).Text.ToUpper.Trim.Replace("&NBSP;", 0), Integer)

                    Dim comentario As String = CType(txtCometario.Text, String)
                    Dim comentarioBase As String = HttpUtility.HtmlDecode(GridRow.Cells(idx_Comentario).Text.ToUpper.Trim.Replace("&NBSP;", String.Empty)).Trim()
                    Dim dataEnvioStr As String = txtDt_Envio.Text.Trim()
                    Dim dataEnvioBase As String = HttpUtility.HtmlDecode(GridRow.Cells(idx_Data_Envio).Text.ToUpper.Trim.Replace("&NBSP;", String.Empty).Replace("00:00:00", String.Empty)).Trim()
                    Dim numeroEnvio As String = txtNumEnvio.Text.Trim()
                    Dim numeroEnvioBase As String = HttpUtility.HtmlDecode(GridRow.Cells(idx_Num_Carta_Envio).Text.ToUpper.Trim.Replace("&NBSP;", String.Empty)).Trim()

                    Dim subgrupoIdParse As Integer
                    Dim subgrupoId As Integer
                    If (Integer.TryParse(GridRow.Cells(idx_subgrupoId).Text, subgrupoIdParse)) Then
                        subgrupoId = subgrupoIdParse
                    End If

                    If Not comentario.Equals(comentarioBase) Or _
                       Not dataEnvioStr.Equals(dataEnvioBase) Or _
                       Not numeroEnvio.Equals(numeroEnvioBase) Then

                        Dim dataEnvio As Nullable(Of Date) = Nothing

                        Dim dataEnvioParse As Date
                        If Date.TryParse(dataEnvioStr, dataEnvioParse) Then
                            dataEnvio = dataEnvioParse
                        End If

                        Me.RN.MonitoramentoId = monitoramentoId
                        Me.RN.SubgrupoId = subgrupoId
                        Me.RN.ApoliceId = apoliceId
                        Me.RN.RamoId = ramoId
                        Me.RN.Comentario = comentario
                        Me.RN.Numero_Envio = numeroEnvio
                        Me.RN.Dt_Envio = dataEnvio
                        Me.RN.Fatura_Id = faturaId

                        Me.RN.Alterar()
                        Me.RN.AlterarComentarioRelatorio()

                    End If

                End If
            Next

            hdnMensagemSucesso.Value = 1
            'GerarAlertMensagem(eMensagem.AlteracaoSucesso, Me.Page)

            Response.Redirect(Request.RawUrl & "&mensagemSucesso=1")


        Catch ex As Exception

            Throw New Exception(ex.Message)

        End Try

    End Sub

    Public Sub Ocultar_Coluna(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvDetalhe.RowDataBound

        If e.Row.RowType = DataControlRowType.Header Or _
           e.Row.RowType = DataControlRowType.DataRow Or _
           e.Row.RowType = DataControlRowType.Footer Then

            Dim idx_monitoramentoId As Integer = IndiceColuna(gvDetalhe, "monitoramento_Id")
            Dim idx_ramoId As Integer = IndiceColuna(gvDetalhe, "ramo_id")
            Dim idx_Data_Envio As Integer = IndiceColuna(gvDetalhe, "dt_envio")
            Dim idx_Num_Carta_Envio As Integer = IndiceColuna(gvDetalhe, "numero_envio")
            Dim idx_Comentario As Integer = IndiceColuna(gvDetalhe, "comentario")

            Dim idx_Data_Envio_Txt As Integer = IndiceColuna(gvDetalhe, "C. Den�ncia - Dt. de envio")
            Dim idx_Num_Carta_Envio_Txt As Integer = IndiceColuna(gvDetalhe, "N� da Carta")

            e.Row.Cells(idx_monitoramentoId).Visible = False
            e.Row.Cells(idx_ramoId).Visible = False
            e.Row.Cells(idx_Data_Envio).Visible = False
            e.Row.Cells(idx_Num_Carta_Envio).Visible = False
            e.Row.Cells(idx_Comentario).Visible = False

            Dim monitoramentoID As Integer = CInt(Request.QueryString("monitoramento_id"))

            Dim monitoramentoID_6_7_19 As Boolean
            Dim monitoramentoID_9 As Boolean
            Dim monitoramentoID_10 As Boolean
            Dim monitoramentoID_13_14_15_16 As Boolean
            Dim monitoramentoID_16 As Boolean
            Dim monitoramentoID_18 As Boolean
            Dim monitoramentoID_20 As Boolean

            monitoramentoID_6_7_19 = (monitoramentoID = 6) Or (monitoramentoID = 7) Or (monitoramentoID = 19)
            monitoramentoID_9 = (monitoramentoID = 9)
            monitoramentoID_10 = (monitoramentoID = 10)
            monitoramentoID_13_14_15_16 = (monitoramentoID = 13) Or (monitoramentoID = 14) Or (monitoramentoID = 15) Or (monitoramentoID = 16)
            monitoramentoID_16 = (monitoramentoID = 16)
            monitoramentoID_18 = (monitoramentoID = 18)
            monitoramentoID_20 = (monitoramentoID = 20)

            'COLUNA: FATURA
            '10 - Ap�lices com duas ou mais faturas inadimplentes
            e.Row.Cells(12).Visible = monitoramentoID_10

            'COLUNA: QUANTIDADE DE VIDA
            '5 - Repeti��o de faturamento para ap�lices abaixo de 8 vidas											
            e.Row.Cells(13).Visible = (monitoramentoID = 5)


            'COLUNA: ENCERRAM.																						
            '6 - Subgrupos encerrados, mas sem fatura gerada											
            '7 - Subgrupos com faturamento emitido, mas n�o virou a compet�ncia	
            '19 - Faturas pendentes de emiss�o	            									
            If monitoramentoID_6_7_19 Then
                Dim parsedDate As Date
                If DateTime.TryParse(e.Row.Cells(14).Text, parsedDate) Then
                    e.Row.Cells(14).Text = parsedDate.ToString("d")
                End If
            End If

            e.Row.Cells(14).Visible = monitoramentoID_6_7_19

            'COLUNA: MOTIVO DA PEND�NCIA
            '9 - Subgrupos que possuem pend�ncias de DPS abaixo ou igual a 350 dias	
            e.Row.Cells(15).Visible = monitoramentoID_9

            'COLUNA: TOT. PR�MIO
            '10 - Ap�lices com duas ou mais faturas inadimplentes
            e.Row.Cells(16).Visible = monitoramentoID_10

            'COLUNA: FAT. PENDENTES	
            '10 - Ap�lices com duas ou mais faturas inadimplentes
            e.Row.Cells(17).Visible = monitoramentoID_10

            'COLUNA EMISS�O
            '13 - Ap�lices/subgrupos com o 1� faturamento e n�o migradas para Vida Web											
            '14 - Ap�lices/subgrupos sem nenhum administrador cadastrado na web											
            '15 - Ap�lices/subgrupos sem nenhum administrador associado na web											
            '16 - Ap�lices/subgrupos migrados na web, mas n�o migraram as vidas no sistema											
            If monitoramentoID_13_14_15_16 Then
                Dim parsedDate As Date
                If DateTime.TryParse(e.Row.Cells(18).Text, parsedDate) Then
                    e.Row.Cells(18).Text = parsedDate.ToString("d")
                End If
            End If

            e.Row.Cells(18).Visible = monitoramentoID_13_14_15_16

            'COLUNA MIGRA��O
            '16 - Ap�lices/subgrupos migrados na web, mas n�o migraram as vidas no sistema											
            If monitoramentoID_16 Then
                Dim parsedDate As Date
                If DateTime.TryParse(e.Row.Cells(19).Text, parsedDate) Then
                    e.Row.Cells(19).Text = parsedDate.ToString("d")
                End If
            End If

            e.Row.Cells(19).Visible = monitoramentoID_16

            'CAMPO FIM VIG AP
            '18 - Ap�lices n�o renovadas											
            If monitoramentoID_18 Then
                Dim parsedDate As Date
                If DateTime.TryParse(e.Row.Cells(20).Text, parsedDate) Then
                    e.Row.Cells(20).Text = parsedDate.ToString("d")
                End If
            End If

            e.Row.Cells(20).Visible = monitoramentoID_18

            'CAMPO ULT FATURAM	
            '18 - Ap�lices n�o renovadas											
            If monitoramentoID_18 Then
                Dim parsedDate As Date
                If DateTime.TryParse(e.Row.Cells(21).Text, parsedDate) Then
                    e.Row.Cells(21).Text = parsedDate.ToString("d")
                End If
            End If

            e.Row.Cells(21).Visible = monitoramentoID_18

            'CAMPO ENC AUTO	
            '20 - Ap�lices com encerramento autom�tico ativo ou faturamento inibido																					
            If monitoramentoID_20 Then
                Dim flag As Boolean
                If (Boolean.TryParse(e.Row.Cells(22).Text, flag)) Then
                    If (flag) Then
                        e.Row.Cells(22).Text = "Sim"
                    Else
                        e.Row.Cells(22).Text = "N�o"
                    End If
                End If
            End If

            e.Row.Cells(22).Visible = monitoramentoID_20

            'CAMPO FAT INIBIDO		
            '20 - Ap�lices com encerramento autom�tico ativo ou faturamento inibido																					
            If monitoramentoID_20 Then
                Dim flag As Boolean
                If (Boolean.TryParse(e.Row.Cells(23).Text, flag)) Then
                    If (flag) Then
                        e.Row.Cells(23).Text = "Sim"
                    Else
                        e.Row.Cells(23).Text = "N�o"
                    End If
                End If
            End If

            e.Row.Cells(23).Visible = monitoramentoID_20

            'CAMPO OPERACAO
            '9 - Subgrupos que possuem pend�ncias de DPS abaixo ou igual a 350 dias	
            e.Row.Cells(24).Visible = monitoramentoID_9

            'CAMPO DATA OPERACAO
            '9 - Subgrupos que possuem pend�ncias de DPS abaixo ou igual a 350 dias	
            If monitoramentoID_9 Then
                Dim parsedDate As Date
                If DateTime.TryParse(e.Row.Cells(25).Text, parsedDate) Then
                    e.Row.Cells(25).Text = parsedDate.ToString("d")
                End If
            End If

            e.Row.Cells(25).Visible = monitoramentoID_9

            'CAMPO NOME SEGURADO
            '9 - Subgrupos que possuem pend�ncias de DPS abaixo ou igual a 350 dias	
            e.Row.Cells(26).Visible = monitoramentoID_9

            'CAMPO vida_sinistrada
            '2 - Exclus�o de vidas sinistradas antes da data do evento
            e.Row.Cells(28).Visible = (monitoramentoID = 2)

            'CAMPO data envio e numero envio
            e.Row.Cells(idx_Num_Carta_Envio_Txt).Visible = monitoramentoID_10
            e.Row.Cells(idx_Data_Envio_Txt).Visible = monitoramentoID_10


        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim num As Int64
            If Int64.TryParse(e.Row.Cells(5).Text, num) Then
                Dim strFormat As String = String.Empty
                strFormat = String.Format("{0:##\.###\.###\/####-##}", num)
                e.Row.Cells(5).Text = strFormat.PadLeft(18, "0")
            Else
                e.Row.Cells(5).Text = String.Empty
            End If
        End If


    End Sub

    Protected Sub sair_detalhe_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sair_detalhe.Click
        Response.Redirect("MonitoramentoConsulta.aspx?usuario=" & Session("usuario") & "&usuarioLinkSeguro=" & Session("usuarioLinkSeguro"), True)
    End Sub

    Protected Sub gvDetalhe_DataBound(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gvDetalhe.DataBound
        If (Not String.IsNullOrEmpty(Request.QueryString("monitoramento_id"))) Then
            Dim monitoramentoId As Integer = Convert.ToInt32(Request.QueryString("monitoramento_id"))
            Select Case monitoramentoId
                Case 8, 10, 12
                    lblTotal.Text = String.Format("Total de {0} ap�lices", gvDetalhe.Rows.Count)
                Case Else
                    lblTotal.Text = String.Format("Total de {0} subgrupos", gvDetalhe.Rows.Count)
            End Select
        End If
    End Sub

    Protected Sub gvDetalhe_RowCreated(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetalhe.RowCreated
    End Sub

    ''' <summary>
    ''' Retorna o indice das colunas do gridview
    ''' </summary>
    ''' <param name="grid">Gridview utilizado para busca</param>
    ''' <param name="headerTexto">Texto do cabecalho da coluna para busca</param>
    ''' <returns>Indice da coluna</returns>
    ''' <autor>Nova Consultoria - Daniel Hespanhol</autor>
    ''' <remarks>Caso o texto da coluna n�o for encontrado o resultado da fun��o ser� zero</remarks>
    Public Function IndiceColuna(ByVal grid As GridView, ByVal headerTexto As String) As Integer

        For Each column As DataControlField In grid.Columns
            If column.HeaderText.ToLower() = headerTexto.ToLower() Then

                Dim columnID As Integer = grid.Columns.IndexOf(column)
                Return columnID

            End If
        Next

        Return 0
    End Function

End Class