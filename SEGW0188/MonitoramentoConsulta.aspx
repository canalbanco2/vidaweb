<%@ Page Language="vb" AutoEventWireup="false" Codebehind="MonitoramentoConsulta.aspx.vb"
    Inherits="SEGW0188._Default" EnableEventValidation="false" EnableViewStateMac="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Monitoramento Vida Web - Consulta</title>
    <link href="css/EstiloBase.css" type="text/css" rel="stylesheet" />
    <link href="css/formulario.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript">
    function selecionar_tudo(){ 
    var master = document.getElementById("ckAll").checked;
    
       for (i=0;i<document.form1.elements.length;i++) 
          if(document.form1.elements[i].type == "checkbox")	
             document.form1.elements[i].checked=master   
    } 
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel ID="pnlCabecalho" runat="server">
            <p>
                <br />
                <asp:Label ID="lblNavegacao" runat="server" /><br />
            </p>
        </asp:Panel>
        <br />
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="false"
            EnableScriptGlobalization="true"/>
        <div class="clsDivGrid">
            <asp:Panel ID="Panel1" runat="server" Width="100%">
                <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                    <cc1:TabPanel runat="server" ID="TabPanel1">
                        <HeaderTemplate>
                            Resultado
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:GridView ID="gvResultado" runat="server" AutoGenerateColumns="False"
                                DataSourceID="odsResultado" Width="100%" BorderWidth="1px" DataKeyNames="monitoramento_id"
                                EmptyDataText="Nenhum resultado encontrado." CssClass="grid-view">
                                <HeaderStyle Font-Bold="False" CssClass="header" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnMonitoramentoId" runat="server" Value='<%# Eval("monitoramento_id") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Todos">
                                        <HeaderTemplate>
                                            <input type="checkbox" id="ckAll" onclick="selecionar_tudo()" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkRelat" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Titulo">
                                        <ItemTemplate>
                                            <asp:HyperLink CssClass="clsLink" ID="hlTitulo" runat="server" Text='<%# Eval("titulo") %>'
                                                NavigateUrl='<%# String.Format("~/MonitoramentoConsultaDetalheResultado.aspx?monitoramento_id={0}&titulo={1}&usuario={2}&usuarioLinkSeguro={3}", Eval("monitoramento_id"), Eval("titulo"), Session("usuario"), Session("usuarioLinkSeguro")) %>' />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="80%" />
                                        <ItemStyle HorizontalAlign="Left" Width="80%" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="quantidade" HeaderText="Quantidade">
                                        <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                        <ItemStyle HorizontalAlign="Center" Width="20%" />
                                    </asp:BoundField>
                                </Columns>
                                <PagerStyle CssClass="header" ForeColor="White" />
                                <SelectedRowStyle BackColor="DeepSkyBlue" Font-Bold="True" Font-Italic="False" />
                                <EmptyDataRowStyle Font-Bold="True" ForeColor="Red" />
                                <RowStyle CssClass="normal" />
                                <AlternatingRowStyle CssClass="alternate" />
                                <FooterStyle ForeColor="#C0FFFF" />
                            </asp:GridView>
                            <asp:ObjectDataSource ID="odsResultado" runat="server" SelectMethod="Select_DataTableResultado"
                                TypeName="SEGW0188._Default"></asp:ObjectDataSource>
                            <center>
                            <br />
                                <asp:Button runat="server" ID="btnExtrairResultados" Text="Extrair Resultados" />
                            </center>
                            <div class="alinhaDiv">
                                <asp:Button ID="sair_resultado" runat="server" CssClass="Botao" Text="Sair" Width="80px"
                                    OnClick="sair_resultado_Click" />
                            </div>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" ID="TabPanel2">
                        <HeaderTemplate>
                            Relat�rio
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td align="right">
                                        <label>
                                            Tipo de relat�rio:</label>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rbSintetico" GroupName="tipoRelatorio" runat="server" Text="Sint�tico"
                                            Checked="True" />
                                    </td>
                                    <td colspan="3">
                                        <asp:RadioButton ID="rbAnalitico" GroupName="tipoRelatorio" runat="server" Text="Anal�tico" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>
                                            Informe o Per�odo:</label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="periodo_de" runat="server" CssClass="clsCampoData" />
                                        <cc1:CalendarExtender ID="periodo_de_calendario" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="periodo_de" />
                                        <cc1:MaskedEditExtender ID="periodo_de_mascara" TargetControlID="periodo_de" Mask="99/99/9999"
                                            MaskType="Date" runat="server" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                            CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                            CultureTimePlaceholder="" Enabled="True" />
                                        <cc1:MaskedEditValidator ID="periodo_de_mascara_valida" runat="server" ControlExtender="periodo_de_mascara"
                                            MinimumValue="01/01/1910" ControlToValidate="periodo_de" Display="Dynamic" InvalidValueMessage="*"
                                            MaximumValue="31/12/2099" ErrorMessage="periodo_de_mascara_valida" />
                                    </td>
                                    <td align="right">
                                        <label>
                                            at�:</label>
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox ID="periodo_ate" runat="server" CssClass="clsCampoData" />
                                        <cc1:CalendarExtender ID="periodo_ate_calendario" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="periodo_ate" />
                                        <cc1:MaskedEditExtender ID="periodo_ate_mascara" TargetControlID="periodo_ate" Mask="99/99/9999"
                                            MaskType="Date" runat="server" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                            CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                            CultureTimePlaceholder="" Enabled="True" />
                                        <cc1:MaskedEditValidator ID="periodo_ate_mascara_valida" runat="server" ControlExtender="periodo_ate_mascara"
                                            MinimumValue="01/01/1910" ControlToValidate="periodo_ate" Display="Dynamic" InvalidValueMessage="*"
                                            MaximumValue="31/12/2099" ErrorMessage="periodo_ate_mascara_valida" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <label>
                                            Prazo final:</label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="prazo_final" runat="server" CssClass="clsCampoData" />
                                        <cc1:CalendarExtender ID="prazo_final_calendario" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="prazo_final" />
                                        <cc1:MaskedEditExtender ID="prazo_final_mascara" TargetControlID="prazo_final" Mask="99/99/9999"
                                            MaskType="Date" runat="server" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                            CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                            CultureTimePlaceholder="" Enabled="True" />
                                        <cc1:MaskedEditValidator ID="prazo_final_mascara_valida" runat="server" ControlExtender="prazo_final_mascara"
                                            MinimumValue="01/01/1910" ControlToValidate="prazo_final" Display="Dynamic" InvalidValueMessage="*"
                                            MaximumValue="31/12/2099" ErrorMessage="prazo_final_mascara_valida" />
                                    </td>
                                    <td>
                                        <label>
                                            Ag�ncia:</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="agencia" runat="server">
                                            <asp:ListItem />
                                            <asp:ListItem Value="Cont&#233;m" />
                                            <asp:ListItem Value="N&#227;o cont&#233;m" />
                                            <asp:ListItem Value="Come&#231;a com" />
                                            <asp:ListItem Value="Termina com" />
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        &nbsp;<asp:TextBox ID="texto_agencia" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <label>
                                            Ordena��o:</label>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="crescente" GroupName="ordenacao" runat="server" Text="Crescente" />
                                    </td>
                                    <td colspan="3">
                                        <asp:RadioButton ID="decrescente" GroupName="ordenacao" runat="server" Text="Decrescente"
                                            Checked="True" />
                                    </td>
                                </tr>
                            </table>
                            <div class="alinhaDiv">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="pesquisar" runat="server" CssClass="Botao" Text="Pesquisar" Width="80px" />
                                        </td>
                                        <td>
                                            <asp:Button ID="sair_relatorio" runat="server" CssClass="Botao" Text="Sair" Width="80px"
                                                OnClick="sair_resultado_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </cc1:TabPanel>
                </cc1:TabContainer>
            </asp:Panel>
            &nbsp;
        </div>
    </form>
</body>

<script language="javascript" type="text/javascript">
    if (typeof top.escondeaguarde === "function") {
        top.escondeaguarde();
    }
</script>

</html>
