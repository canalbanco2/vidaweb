Imports System.Data
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Globalization
Imports SEGL0315
Imports Alianca.FrameWork
Imports Alianca.Seguranca.BancoDados
Imports CrystalDecisions.CrystalReports.Engine

Partial Public Class _Default
    Inherits System.Web.UI.Page

#Region " Membros "
    Private m_oRN As ClsConsultaMonitoramentoWebRN
    Private m_oRelatorioSinteticoRN As clsRelatorioMonitoramentoSintetico
    Private m_oRelatorioAnaliticoRN As clsRelatorioMonitoramentoAnalitico
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Implementa��o do LinkSeguro
        Dim linkseguro As New Alianca.Seguranca.Web.LinkSeguro

        Dim usuario As String = linkseguro.LerUsuario("MonitoramentoConsulta.aspx", Request.ServerVariables("REMOTE_ADDR"), Request("SLinkSeguro"))

        '------alterar ao terminar
        If usuario = "" Then
            If Session("usuarioLinkSeguro") <> "" Then
                usuario = Session("usuarioLinkSeguro")
            Else
                Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
            End If
        Else
            Session("usuarioLinkSeguro") = usuario
        End If

        Session("usuario") = linkseguro.Login_WEB
        '--------------------------------------------------

        'Implementa��o do controle de ambiente
        Dim cAmbiente As New Alianca.Seguranca.Web.ControleAmbiente

        If Not Page.IsPostBack Then
            Session("indice") = 0
            For Each m As String In Request.QueryString.Keys
                If m <> "" Then
                    Session(m) = Request.QueryString(m)
                End If
            Next
        Else
            Session("valida") = "nao"
        End If

        Dim url As String

        cAmbiente = New Alianca.Seguranca.Web.ControleAmbiente
        url = "http://" & Request.ServerVariables("SERVER_NAME") & "/"
        cAmbiente.ObterAmbiente(url)

        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
        Else
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cAmbiente.Ambiente)
        End If

        Session.Add("GLAMBIENTE_ID", cAmbiente.Ambiente)

        If Not Page.IsPostBack Then

            'Dim v_oDtConsulta As DataTable
            'v_oDtConsulta = Me.RN.GetUltimaDataAtualizacao().Tables(0)
            'Dim dataUltAtualizacao As Date = CDate(v_oDtConsulta.Rows(0).ItemArray.GetValue(0))

            Dim v_oDtConsulta As DataTable

            Dim bd As New cAcompanhamento(eBanco.web_seguros_db)
            bd.GetDataInclusao()

            v_oDtConsulta = bd.ExecutaSQL_DT()

            Dim dataUltAtualizacao As Date = CDate(v_oDtConsulta.Rows(0).ItemArray.GetValue(0))

            lblNavegacao.Text = "Fun��o: Monitoramento das a��es no sistema Vida Web <br> Data da �ltima atualiza��o: " & dataUltAtualizacao.ToShortDateString

        End If

    End Sub



#Region " Propriedades "

    Public Shadows ReadOnly Property RN() As ClsConsultaMonitoramentoWebRN
        Get
            If IsNothing(m_oRN) Then
                m_oRN = New ClsConsultaMonitoramentoWebRN
            End If
            Return m_oRN
        End Get
    End Property

    Public Shadows ReadOnly Property RelatorioSinteticoRN() As clsRelatorioMonitoramentoSintetico
        Get
            If IsNothing(m_oRelatorioSinteticoRN) Then
                m_oRelatorioSinteticoRN = New clsRelatorioMonitoramentoSintetico
            End If
            Return m_oRelatorioSinteticoRN
        End Get
    End Property

    Public Shadows ReadOnly Property RelatorioAnaliticoRN() As clsRelatorioMonitoramentoAnalitico
        Get
            If IsNothing(m_oRelatorioAnaliticoRN) Then
                m_oRelatorioAnaliticoRN = New clsRelatorioMonitoramentoAnalitico
            End If
            Return m_oRelatorioAnaliticoRN
        End Get
    End Property

#End Region

    Public Function Select_DataTableResultado() As DataTable
        Dim v_oDtConsulta As DataTable

        v_oDtConsulta = Me.RN.Listar().Tables(0)

        Return v_oDtConsulta

    End Function

    Protected Sub sair_resultado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sair_resultado.Click
        Response.Redirect("../SEGW0060/Centro.aspx?usuario=" & Session("usuario"), True)
	End Sub

    Protected Sub pesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pesquisar.Click
        Dim ds As DataSet
        Dim rpt As New ReportDocument
        Dim nomeRelatorio, caminhoRelatorio As String

        Dim filtros() As Object = ObterFiltros()

        caminhoRelatorio = Server.MapPath("~/reports/{0}.rpt")

        If rbSintetico.Checked Then
            nomeRelatorio = "relMonitoramentoSintetico"
            ds = Me.RelatorioSinteticoRN.Pesquisar(filtros)
        Else
            nomeRelatorio = "relMonitoramentoAnalitico"
            ds = Me.RelatorioAnaliticoRN.Pesquisar(filtros)
        End If
        caminhoRelatorio = String.Format(caminhoRelatorio, nomeRelatorio)

        rpt.Load(caminhoRelatorio)
        rpt.SetDataSource(ds.Tables(0))
        rpt.SetParameterValue("periodo_de", periodo_de.Text)
        rpt.SetParameterValue("periodo_ate", periodo_ate.Text)

        rpt.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.Excel, Response, False, nomeRelatorio)
    End Sub

    ' Retorna os filtros que ser�o passados para a procedure
    Private Function ObterFiltros() As Object()
        Dim filtros(6) As Object

        If periodo_de.Text = "" Then filtros(0) = DBNull.Value Else filtros(0) = periodo_de.Text
        If periodo_ate.Text = "" Then filtros(1) = DBNull.Value Else filtros(1) = periodo_ate.Text
        If prazo_final.Text = "" Then filtros(2) = DBNull.Value Else filtros(2) = prazo_final.Text
        If texto_agencia.Text = "" Then filtros(3) = DBNull.Value Else filtros(3) = texto_agencia.Text
        If agencia.Text = "" Then filtros(4) = DBNull.Value Else filtros(4) = agencia.Text
        If decrescente.Checked Then
            filtros(5) = "DESC"
        Else
            filtros(5) = "ASC"
        End If
        filtros(6) = DBNull.Value
        Return filtros
    End Function

    Protected Sub btnExtrairResultados_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExtrairResultados.Click
        Dim MonitoramentoList As ArrayList = New ArrayList()
        Dim i As Integer = 0

        Try

            While i < gvResultado.Rows.Count
                Dim row As GridViewRow = gvResultado.Rows(i)
                Dim isChecked As Boolean = DirectCast(row.FindControl("chkRelat"), CheckBox).Checked
                If isChecked Then
                    Dim MonitoramentoId As String = DirectCast(row.FindControl("hdnMonitoramentoId"), HiddenField).Value
                    MonitoramentoList.Add(MonitoramentoId)
                End If
                i += 1
            End While

            If (MonitoramentoList.Count = 0) Then
                Response.Write("<script>alert('Nenhum monitoramento foi selecionado!');</script>")
            Else
                Dim list_monitoramento_ids As StringBuilder = New StringBuilder()
                Dim str As String

                
                For Each str In MonitoramentoList
                    list_monitoramento_ids.Append(str).Append(", ")
                Next



                list_monitoramento_ids.Remove(list_monitoramento_ids.Length - 2, 2)

                Session("list_monitoramento_ids") = list_monitoramento_ids.ToString()

                'Response.Redirect(String.Format("~/MonitoramentoConsultaDetalheResultado.aspx?monitoramento_id={0}&titulo={1}&usuario={2}&usuarioLinkSeguro={3}", String.Empty, String.Empty, Session("usuario"), Session("usuarioLinkSeguro")))

                Dim ds As DataSet
                Dim rpt As New ReportDocument
                Dim nomeRelatorio, caminhoRelatorio As String

                Dim filtros() As Object = ObterFiltros()
                filtros(6) = list_monitoramento_ids.ToString()
                caminhoRelatorio = Server.MapPath("~/reports/{0}.rpt")

                nomeRelatorio = "relMonitoramentoAnalitico"
                ds = Me.RelatorioAnaliticoRN.Pesquisar(filtros)

                caminhoRelatorio = String.Format(caminhoRelatorio, nomeRelatorio)

                rpt.Load(caminhoRelatorio)
                rpt.SetDataSource(ds.Tables(0))
                rpt.SetParameterValue("periodo_de", String.Empty)
                rpt.SetParameterValue("periodo_ate", String.Empty)

                rpt.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.Excel, Response, False, nomeRelatorio)


            End If

        Catch ex As Exception
            Dim excp As New clsException(ex)
        End Try

    End Sub

    Public Enum eBanco As Byte
        web_intranet_db = 1
        segab_db = 2
        Imagem_db = 3
        Interface_db = 4
        web_seguros_db = 5
    End Enum

End Class