<%@ Page Language="vb" AutoEventWireup="false" Codebehind="MonitoramentoConsultaDetalheResultado.aspx.vb"
    Inherits="SEGW0188._DefaultDetalhe" EnableEventValidation="false" EnableViewStateMac="false" %>
    
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Monitoramento Vida Web - Detalhe</title>
    <link href="css/EstiloBase.css" type="text/css" rel="stylesheet" />
    <link href="css/formulario.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField runat= "server" ID="hdnMensagemSucesso"/>
        <asp:Panel ID="pnlCabecalho" runat="server">
            <p>
                <br />
                <asp:Label ID="lblNavegacao" runat="server" /><br />
            </p>
        </asp:Panel>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <br />
        <div class="divTitulo">
            <label class="labelTitulo">
                Detalhamento:
                <%= Request.QueryString("titulo") %>
            </label>
        </div>
        <div class="clsDivGridScroll">
            <asp:Panel ID="Panel1" runat="server" Width="1700px">
                <asp:GridView ID="gvDetalhe" runat="server" AutoGenerateColumns="False" DataSourceID="odsDetalhe"
                    Width="100%" BorderWidth="1px" OnRowDataBound="Ocultar_Coluna" EmptyDataText="Nenhum Detalhe encontrado."
                    CssClass="grid-view" DataKeyNames="monitoramento_id">
                    <HeaderStyle Font-Bold="False" CssClass="header" />
                    <Columns>
                        <asp:BoundField DataField="apolice" HeaderText="Ap&#243;lice">
                            <HeaderStyle HorizontalAlign="Left" Width="50px" />
                            <ItemStyle HorizontalAlign="Left" Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="num_subgrupo" HeaderText="N&#186; Subgrupo">
                            <HeaderStyle HorizontalAlign="Left" Width="80px" />
                            <ItemStyle HorizontalAlign="Left" Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="nome_subgrupo" HeaderText="Nome Subgrupo">
                            <HeaderStyle HorizontalAlign="Left" Width="150px" />
                            <ItemStyle HorizontalAlign="Left" Width="450px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ramo_id" HeaderText="ramo_id" />
                        <asp:BoundField DataField="dt_inicio_vigencia_apl" HeaderText="In&#237;cio Vig&#234;ncia" DataFormatString="{0:dd/MM/yyyy}">
                            <HeaderStyle HorizontalAlign="Left" Width="80px" />
                            <ItemStyle HorizontalAlign="center" Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="cnpj" HeaderText="CNPJ" DataFormatString="{0:000\.000\.000\/0000\-00}">
                            <HeaderStyle HorizontalAlign="Left" Width="150px" />
                            <ItemStyle HorizontalAlign="Left" Width="200px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="dt_fim_vigencia_apl" HeaderText="Fim Vig&#234;ncia" DataFormatString="{0:dd/MM/yyyy}">
                            <HeaderStyle HorizontalAlign="Left" Width="80px" />
                            <ItemStyle HorizontalAlign="center" Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="prazo_final" HeaderText="Prazo final" DataFormatString="{0:dd/MM/yyyy}">
                            <HeaderStyle HorizontalAlign="Left" Width="80px" />
                            <ItemStyle HorizontalAlign="Left" Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="num_agencia" HeaderText="N&#186; Ag&#234;ncia">
                            <HeaderStyle HorizontalAlign="Left" Width="80px" />
                            <ItemStyle HorizontalAlign="Left" Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="nome_agencia" HeaderText="Nome Ag&#234;ncia">
                            <HeaderStyle HorizontalAlign="Left" Width="150px" />
                            <ItemStyle HorizontalAlign="Left" Width="150px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ddd" HeaderText="DDD">
                            <HeaderStyle HorizontalAlign="Center" Width="30px" />
                            <ItemStyle HorizontalAlign="Center" Width="30px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="telefone" HeaderText="Telefone">
                            <HeaderStyle HorizontalAlign="Left" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="fatura" HeaderText="Fatura">
                            <HeaderStyle HorizontalAlign="Left" Width="50px"  />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="quantidade_vida" HeaderText="Quantidade de Vida">
                            <HeaderStyle HorizontalAlign="Left" Width="50px"  />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="dt_encerramento" HeaderText="Encerram.">
                            <HeaderStyle HorizontalAlign="Left" Width="50px"  />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="motivo_pendencia" HeaderText="Motivo da Pend&#234;ncia">
                           <HeaderStyle HorizontalAlign="Left" Width="150px" />
                           <ItemStyle HorizontalAlign="Left" Width="300px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="valor" HeaderText="tot. pr&#234;mio">
                            <HeaderStyle HorizontalAlign="Left" Width="50px"  />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="qtd_faturas" HeaderText="Fat. Pendentes">
                            <HeaderStyle HorizontalAlign="Left" Width="50px"  />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="dt_emissao" HeaderText="emiss&#227;o">
                            <HeaderStyle HorizontalAlign="Left" Width="50px"  />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="dt_migracao" HeaderText="migra&#231;&#227;o">
                            <HeaderStyle HorizontalAlign="Left" Width="50px"  />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="dt_fim_vigencia" HeaderText="fim vig ap">
                            <HeaderStyle HorizontalAlign="Left" Width="50px"  />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="dt_ult_faturamento" HeaderText="ult faturam">
                            <HeaderStyle HorizontalAlign="Left" Width="50px"  />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="encerramento_automatico" HeaderText="enc auto">
                            <HeaderStyle HorizontalAlign="Left" Width="50px"  />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="faturamento_inibido" HeaderText="fat inibido">
                            <HeaderStyle HorizontalAlign="Left" Width="50px"  />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:BoundField>
                        
                        <asp:BoundField DataField="operacao" HeaderText="Opera&#231;&#227;o">
                            <HeaderStyle HorizontalAlign="Left" Width="150px" />
                            <ItemStyle HorizontalAlign="Left" Width="300px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="data_operacao" HeaderText="Data Opera&#231;&#227;o">
                            <HeaderStyle HorizontalAlign="Left" Width="50px"  />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="nome_segurado" HeaderText="Segurado">
                            <HeaderStyle HorizontalAlign="Left" Width="150px" />
                            <ItemStyle HorizontalAlign="Left" Width="300px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="documentos" HeaderText="Documentos" Visible="False">
                            <HeaderStyle HorizontalAlign="Left" Width="150px" />
                            <ItemStyle HorizontalAlign="Left" Width="300px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="nome_sinistrado" HeaderText="Vida Sinistrada">
                            <HeaderStyle HorizontalAlign="Left" Width="150px" />
                            <ItemStyle HorizontalAlign="Left" Width="300px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="dt_envio" HeaderText="dt_envio" DataFormatString="{0:dd/MM/yyyy}"/> 
                        
                        <asp:TemplateField  HeaderText="C. Den&#250;ncia - Dt. de envio" >
                            <ItemTemplate>
                            <span id="ctrl_Dt_Envio" >
                            <asp:TextBox ID="txtDt_envio" runat="server" Width="85%" Text='<%# Eval("dt_envio") %>' MaxLength="1" style="text-align:justify" ValidationGroup="MKE" />  
                            <cc1:MaskedEditExtender ID="mskDt_Envio" runat="server"  
                                TargetControlID="txtDt_envio"  
                                Mask="99/99/9999"  
                                MessageValidatorTip="true"  
                                OnFocusCssClass="MaskedEditFocus"  
                                OnInvalidCssClass="MaskedEditError"  
                                MaskType="Date"  
                                DisplayMoney="Left"  
                                AcceptNegative="Left"  
                                ErrorTooltipEnabled="True" />  
                            <cc1:MaskedEditValidator ID="mskVal_Dt_Envio" runat="server"  
                                ControlExtender="mskDt_Envio"  
                                ControlToValidate="txtDt_envio"  
                                InvalidValueMessage="Data � inv�lida"  
                                Display="Dynamic"  
                                EmptyValueBlurredText="*"  
                                InvalidValueBlurredMessage="*"  
                                ValidationGroup="ErroDataEnvio"
                                 />&nbsp;</span>
                        
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" Width="250px" />
                        <ItemStyle HorizontalAlign="Left" Width="250px" />
                        <ControlStyle CssClass="TemplateControl" />              
                    </asp:TemplateField>
                    	<asp:BoundField DataField="numero_envio" HeaderText="numero_envio" />  
                    
                     <asp:TemplateField  HeaderText="N&#186; da Carta">
                       <ItemTemplate>
                            <asp:TextBox ID="txtNum_Envio" Width="100%" runat="server" MaxLength = "50" Text='<%# Eval("numero_envio") %>'/>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" Width="250px" />
                        <ItemStyle HorizontalAlign="Left" Width="250px" />
                        <ControlStyle CssClass="TemplateControl" />              
                     </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Coment&#225;rio">
                            <ItemTemplate>
                                <asp:TextBox ID="txtComentario" Width="100%" runat="server" Text='<%# Eval("comentario") %>' />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" Width="250px" />
                            <ItemStyle HorizontalAlign="Left" Width="250px" />
                            <ControlStyle CssClass="TemplateControl" />
                        </asp:TemplateField>
                        
                        
                        <asp:BoundField DataField="monitoramento_id" HeaderText="monitoramento_id" />
                        <asp:BoundField DataField="comentario" HeaderText="comentario" />
                        
                        
                        
                    </Columns>
                    <PagerStyle CssClass="header" ForeColor="White" />
                    <SelectedRowStyle BackColor="DeepSkyBlue" Font-Bold="True" Font-Italic="False" />
                    <EmptyDataRowStyle Font-Bold="True" ForeColor="Red" />
                    <RowStyle CssClass="normal" />
                    <AlternatingRowStyle CssClass="alternate" />
                    <FooterStyle ForeColor="#C0FFFF" />
                </asp:GridView>
                <asp:ObjectDataSource ID="odsDetalhe" runat="server" SelectMethod="Select_DataTableDetalhe"
                    TypeName="SEGW0188._DefaultDetalhe">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="" Name="dt_inicio_vigencia" Type="String" />
                        <asp:Parameter DefaultValue="" Name="dt_fim_vigencia" Type="String" />
                        <asp:Parameter DefaultValue="" Name="prazo_final" Type="String" />
                        <asp:Parameter Name="agencia" Type="String" />
                        <asp:Parameter Name="criterio_agencia" Type="String" />
                        <asp:Parameter Name="ordenacao" Type="String" />
                        <asp:QueryStringParameter DefaultValue="" Name="monitoramento_id" QueryStringField="monitoramento_id"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </asp:Panel>
        </div>
        <div class="divTitulo">
            <asp:Label runat="server" ID="lblTotal" CssClass="labelTitulo">Total de x ap�lices e com y subgrupos</asp:Label></div>
        <div class="alinhaDiv clsDivGrid">
            <table>
                <tr>
                    <td>
                        <asp:Button ID="gravar"  runat="server" CssClass="Botao" Text="Gravar" OnClick="gravar_dados_detalhe"
                            Width="80px" />
                    </td>
                    <td>
                        <asp:Button ID="sair_detalhe" runat="server" CssClass="Botao" Text="Sair" Width="80px" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
