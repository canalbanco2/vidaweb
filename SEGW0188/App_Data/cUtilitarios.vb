Public Class cUtilitarios

    'Trata a data para o seguinte formato dd/mm/aaaa hh:mm
    Public Shared Function trataDataHora(ByVal data As String) As String

        Try
            Dim dt_final As String

            Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim ano As String = CType(data, DateTime).Year

            Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = dia + "/" + mes + "/" + ano


            Return dt_final + " " + hora + ":" + minuto

        Catch ex As System.Exception

            Return ""

        End Try

    End Function

    'Trata a data para o seguinte formato dd/mm/aaaa 
    Public Shared Function trataData(ByVal data As String) As String

        Try

            Dim dt_arr() As String = data.Split("/")
            Dim dt_final As String

            'Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim dia As String = dt_arr(0).PadLeft(2, "0")
            'Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim mes As String = dt_arr(1).PadLeft(2, "0")
            'Dim ano As String = CType(data, DateTime).Year
            Dim ano As String = dt_arr(2)

            'Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            'Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = dia + "/" + mes + "/" + ano


            Return dt_final

        Catch ex As System.Exception

            Return ""

        End Try

    End Function

    'Trata a data para o seguinte formato dd/mm/aaaa 
    Public Shared Function trataDataDB(ByVal data As String) As String

        Try

            Dim dt_arr() As String = data.Split("/")
            Dim dt_final As String

            'Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim dia As String = dt_arr(0).PadLeft(2, "0")
            'Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim mes As String = dt_arr(1).PadLeft(2, "0")
            'Dim ano As String = CType(data, DateTime).Year
            Dim ano As String = dt_arr(2)

            'Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            'Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = ano + mes + dia


            Return dt_final

        Catch ex As System.Exception

            Return ""

        End Try

    End Function

    Public Shared Function trataSmalldate(ByVal data As String) As String

        Try
            Dim dt_final As String

            Dim dia As String = CType(data, DateTime).Day.ToString.PadLeft(2, "0")
            Dim mes As String = CType(data, DateTime).Month.ToString.PadLeft(2, "0")
            Dim ano As String = CType(data, DateTime).Year

            Dim hora As String = CType(data, DateTime).Hour.ToString.PadLeft(2, "0")
            Dim minuto As String = CType(data, DateTime).Minute.ToString.PadLeft(2, "0")

            dt_final = ano + mes + dia


            Return dt_final + " " + hora + ":" + minuto

        Catch ex As System.Exception

            Return ""

        End Try

    End Function

    '''Exibe o valor em um alert na tela
    Public Shared Sub br(ByVal valor As String)

        Dim texto As String

        texto = "<script>" + vbNewLine
        texto &= "alert(""" + valor + """);"
        texto &= "</script>"

        System.Web.HttpContext.Current.Response.Write(texto)
    End Sub

    Public Shared Sub escreveScript(ByVal valor As String)
        Web.HttpContext.Current.Response.Write("<script>" + valor + "</script>")
    End Sub

    Public Shared Function destrataCPF(ByVal cpf As String) As String

        If cpf.Length = 14 Then
            Return cpf.Replace(".", "").Replace("-", "")
        Else
            Return cpf
        End If

    End Function

    Public Shared Function trataCPF(ByVal cpf As String) As String

        If cpf.Length = 11 Then
            Return cpf.Substring(0, 3) + "." + cpf.Substring(3, 3) + "." + cpf.Substring(6, 3) + "-" + cpf.Substring(9, 2)
        Else
            Return cpf
        End If

    End Function

    Public Shared Function trataMoeda(ByVal valor As String) As String
        If valor = "&nbsp;" Or valor = "" Then
            Return ""
        Else
            'Return String.Format(String.Format("{0:c}", CDbl(valor))).Substring(3)
            Return String.Format(String.Format("{0:c}", CDbl(valor))).Substring(3).Replace("(", "").Replace(")", "")
        End If


    End Function


    Public Shared Function deParaDLL(ByVal valor As Integer, ByVal limiteMinIdade As String, ByVal limiteMaxIdade As String, ByVal segcapital As String, ByVal idade As String, ByVal nomeConjuge As String) As String

        Select Case valor
            Case 0
                Return ""
            Case 1
                Return "Data de In�cio de Cobertura menor que o in�cio de vig�ncia da ap�lice."
            Case 2
                Return "Data de Entrada no Subgrupo menor que o in�cio de vig�ncia da ap�lice."
            Case 3
                Return "Capital Segurado diferente do capital do subgrupo."
            Case 4
                Return "Capital Segurado n�o informado."
            Case 6
                Return "Sal�rio do Segurado n�o informado."
            Case 7
                Return "Capital Segurado abaixo do limite permitido para o subgrupo."
            Case 8
                Return "Capital Segurado acima do limite permitido para o subgrupo."
            Case 9
                Return "Titular n�o informado."
            Case 10
                Return "Titular j� associado a outro c�njuge."
            Case 11
                Return "Data de In�cio de Vig�ncia do C�njuge menor que a Data de In�cio de Vig�ncia do Titular."
            Case 14
                Return "N�o existe faixas et�rias para o Subgrupo na vig�ncia do segurado."
            Case 15
                Return "Idade do segurado menor que o limite permitido."
            Case 16
                Return "Idade do segurado maior que o limite permitido."
            Case 17
                Return "Inclus�o de segurado j� existente."
            Case 18
                Return "Data de In�cio de Vig�ncia � menor que a data de fim de vig�ncia anterior."
            Case 19
                Return "As faixas et�rias correspondentes n�o possuem um capital segurado."
            Case 20
                Return "N�o existe faixa et�ria correspondente a idade em quest�o."
            Case 21
                Return "Segurado inativo, deve ser feita incluls�o ao inv�s da altera��o."
            Case 22
                Return "Segurado inativo, n�o � poss�vel a exclus�o."
            Case 23
                Return "Segurado n�o encontrado para altera��o."
            Case 24
                Return "Segurado n�o encontrado para exclus�o."
            Case 25
                Return "C�njuge n�o deve ser informado:."
            Case 26
                Return "J� existe um cliente ativo neste subgrupo com o mesmo CPF, Data de Nascimento e sexo."
            Case Else
                Return "Erro n�o encontrado."
        End Select

    End Function

  

    



   

   
   

   
 
End Class
