Public Class cAcompanhamento

#Region "Heran�a"
    Inherits cBanco
#End Region

    Public Sub New(Optional ByVal banco As cDadosBasicos.eBanco = cDadosBasicos.eBanco.VIDA_WEB_DB)
        MyBase.New(banco)
        Alianca.Seguranca.BancoDados.cCon.BancodeDados = banco.ToString

    End Sub

    Public Sub CriaTabelaTemporariaMonitoramentos()
        SQL = "CREATE TABLE ##list_monitoramento_tb(monitoramento_id INT)"
    End Sub

    Public Sub InsertTemporariaMonitoramentos(ByVal monitoramentoId As String)
        Dim str As StringBuilder = New StringBuilder

        str.Append("INSERT into ##list_monitoramento_tb VALUES (")
        str.Append(monitoramentoId)
        str.Append(")")

        SQL = str.ToString()
    End Sub

    Public Sub GetDataInclusao()
        SQL = "select top 1 dt_inclusao from vida_web_db..relatorio_consulta_monitoramento_tb"
    End Sub

End Class
