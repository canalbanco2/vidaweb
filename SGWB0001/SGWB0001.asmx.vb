Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Configuration.ConfigurationManager
Imports System.Xml
Imports System.IO

'<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class SGBW0001
    Inherits System.Web.Services.WebService

    <WebMethod(Description:="Consulta CPF e retorna o numero da sorte.")> _
    Public Function RetornaDataSet(ByVal p_sXmlParametros As String) As DataSet
        'Par�metro de Entrada (p_sXmlParametros)
        '-----------------------------------------
        '<usuario>
        '   <parametros>
        '	    <cpf_cnpj>00000000000</cpf_cnpj>
        '	    <cod_brasilcap>0000</cod_brasilcap>
        '   </parametros>
        '</usuario>


        Dim obj_mXMLDocumento As New XmlDocument
        Dim obj_mParametros As New List(Of ClsClienteNumeroSorteRN.cParametro)

        Dim enu_mAB_Ambiente As Alianca.Seguranca.BancoDados.cCon.Ambientes

        Try
            ' Recupera o ambiente da AB a ser utilizado (Desenvolvimento / Qualidade / Produ��o / Extrator) e o configura
            enu_mAB_Ambiente = CType(AppSettings("Ambiente"), Alianca.Seguranca.BancoDados.cCon.Ambientes)
            ConfiguraAmbienteAlianca(enu_mAB_Ambiente)

            ' Carrega XML enviado como par�metro
            obj_mXMLDocumento.LoadXml(p_sXmlParametros)

            'Recupera Par�metros do XML
            'obj_mParametros = Obter_Parametros(obj_mXMLDocumento)
            Dim obj_mXMLParametros As XmlNodeList
            obj_mXMLParametros = obj_mXMLDocumento.GetElementsByTagName("parametros")       ' Par�metros
            Dim obj_mParametro As New ClsClienteNumeroSorteRN.cParametro
            obj_mParametros = Obter_Parametros(obj_mParametros, obj_mParametro, obj_mXMLParametros)

            Dim v_oVlsClienteNumeroSorte As New ClsClienteNumeroSorteRN
            Dim v_oDsRetorno As DataSet = v_oVlsClienteNumeroSorte.Retornar_NumeroSorte(obj_mParametros)

            Return v_oDsRetorno

        Catch ex As Exception
            Dim dst_mDataSetIntegracao = New DataSet()
            dst_mDataSetIntegracao.Tables.Add(Gera_Resultado(1, ex.Message))

            Return dst_mDataSetIntegracao
        End Try

        Return Nothing
    End Function

    <WebMethod(Description:="Consulta CPF e retorna o numero da sorte.")> _
    Public Function RetornaXml(ByVal p_sXmlParametros As String) As String
        'Par�metro de Entrada (p_sXmlParametros)
        '-----------------------------------------
        '<usuario>
        '   <parametros>
        '	    <cpf_cnpj>00000000000</cpf_cnpj>
        '	    <cod_brasilcap>0000</cod_brasilcap>
        '   </parametros>
        '</usuario>
        Dim obj_mXMLDocumento As New XmlDocument
        Dim obj_mParametros As New List(Of ClsClienteNumeroSorteRN.cParametro)

        Dim enu_mAB_Ambiente As Alianca.Seguranca.BancoDados.cCon.Ambientes

        Try
            ' Recupera o ambiente da AB a ser utilizado (Desenvolvimento / Qualidade / Produ��o / Extrator) e o configura
            enu_mAB_Ambiente = CType(AppSettings("Ambiente"), Alianca.Seguranca.BancoDados.cCon.Ambientes)
            ConfiguraAmbienteAlianca(enu_mAB_Ambiente)

            ' Carrega XML enviado como par�metro
            obj_mXMLDocumento.LoadXml(p_sXmlParametros)

            'Recupera Par�metros do XML
            'obj_mParametros = Obter_Parametros(obj_mXMLDocumento)
            Dim obj_mXMLParametros As XmlNodeList
            obj_mXMLParametros = obj_mXMLDocumento.GetElementsByTagName("parametros")       ' Par�metros
            Dim obj_mParametro As New ClsClienteNumeroSorteRN.cParametro
            obj_mParametros = Obter_Parametros(obj_mParametros, obj_mParametro, obj_mXMLParametros)

            Dim v_oVlsClienteNumeroSorte As New ClsClienteNumeroSorteRN
            Dim v_oDsRetorno As DataSet = v_oVlsClienteNumeroSorte.Retornar_NumeroSorte(obj_mParametros)
            Dim v_sXML As String = ""
            If v_oDsRetorno.Tables(0).Rows.Count > 0 Then
                v_sXML = "<?xml version='1.0' standalone='yes' ?><root><envio><usuario><parametros><cpf_cnpj>" & v_oDsRetorno.Tables(0).Rows(0)("cpf_cnpj") & "</cpf_cnpj>" & _
                "<cod_brasilcap>" & v_oDsRetorno.Tables(0).Rows(0)("cod_brasilcap") & "</cod_brasilcap></parametros></usuario></envio>"
                v_sXML &= "<resultado>"
                For Each v_oRrow As DataRow In v_oDsRetorno.Tables(0).Rows
                    v_sXML &= "<numero_sorte>"
                    v_sXML &= "<numero_titulo>" & v_oRrow("numero_titulo") & "</numero_titulo>"
                    v_sXML &= "<dt_sorteio>" & CDate(v_oRrow("dt_sorteio")).ToString("dd/MM/yyyy") & "</dt_sorteio>"
                    v_sXML &= "<produto>" & v_oRrow("produto") & "</produto>"
                    v_sXML &= "<codigo_serie>" & v_oRrow("codigo_serie") & "</codigo_serie>"
                    v_sXML &= "</numero_sorte>"
                Next
            Else
                v_sXML = "<?xml version='1.0' standalone='yes' ?><root><envio><usuario><parametros><cpf_cnpj>" & obj_mParametros.Item(0).valor & "</cpf_cnpj><cod_brasilcap>" & _
                obj_mParametros.Item(1).valor & "</cod_brasilcap></parametros></usuario></envio>"
                v_sXML &= "<resultado><numero_sorte><numero_titulo></numero_titulo><dt_sorteio></dt_sorteio><produto></produto><codigo_serie></codigo_serie></numero_sorte>"
            End If
            v_sXML &= "</resultado></root>"

            'Dim xml_mRetorno As New XmlDocument()
            'Dim result As XmlDataDocument = New XmlDataDocument(v_oDsRetorno)
            'Return result.ToString()

            Return v_sXML

        Catch ex As Exception
            Dim dst_mDataSetIntegracao As DataSet = New DataSet()
            dst_mDataSetIntegracao.Tables.Add(Gera_Resultado(1, ex.Message))

            Return dst_mDataSetIntegracao.GetXml
        End Try

        Return Nothing
    End Function

    <WebMethod(Description:="Retorna numeros sorteados.")> _
    Public Function RetornaXmlNumeroSorteado(ByVal p_sXmlParametros As String) As String

        'Par�metro de Entrada (p_sXmlParametros)
        '-----------------------------------------
        '<usuario>
        '   <parametros>
        '	    <cod_brasilcap>0000</cod_brasilcap>
        '   </parametros>
        '</usuario>
        Dim obj_mXMLDocumento As New XmlDocument
        Dim obj_mParametros As New List(Of ClsNumeroSorteadoRN.cParametro)

        Try
            Dim enu_mAB_Ambiente As Alianca.Seguranca.BancoDados.cCon.Ambientes
            ' Recupera o ambiente da AB a ser utilizado (Desenvolvimento / Qualidade / Produ��o / Extrator) e o configura
            enu_mAB_Ambiente = CType(AppSettings("Ambiente"), Alianca.Seguranca.BancoDados.cCon.Ambientes)
            ConfiguraAmbienteAlianca(enu_mAB_Ambiente)

            ' Carrega XML enviado como par�metro
            obj_mXMLDocumento.LoadXml(p_sXmlParametros)

            'Recupera Par�metros do XML
            ' obj_mParametros = Obter_Parametros(obj_mXMLDocumento)

            'Recupera Par�metros do XML
            'obj_mParametros = Obter_Parametros(obj_mXMLDocumento)
            Dim obj_mXMLParametros As XmlNodeList
            obj_mXMLParametros = obj_mXMLDocumento.GetElementsByTagName("parametros")       ' Par�metros
            Dim obj_mParametro As New ClsNumeroSorteadoRN.cParametro
            obj_mParametros = Obter_Parametros(obj_mParametros, obj_mParametro, obj_mXMLParametros)

            Dim v_oClsNumeroSorteado As New ClsNumeroSorteadoRN
            Dim v_oDsRetorno As DataSet = v_oClsNumeroSorteado.Retorna_NumeroSorteado(obj_mParametros.Item(0))
            Dim v_sXML As String = ""

            '<numero_sorteado_tb>
            '    <numero_sorteado_id></numero_sorteado_id>
            '    <sorteio_id></sorteio_id>
            '    <numero_titulo></numero_titulo>
            '    <val_sorteado></val_sorteado>
            '    <dt_sorteio></dt_sorteio>
			'	 <produto></produto>
            '    <codigo_serie></codigo_serie>
            '</numero_sorteado_tb>
            v_sXML = "<?xml version='1.0' standalone='yes' ?><root><envio></envio>"
            v_sXML &= "<resultado>"
            If v_oDsRetorno.Tables(0).Rows.Count > 0 Then
                For Each v_oRrow As DataRow In v_oDsRetorno.Tables(0).Rows
                    v_sXML &= "<numero_sorteado>"
                    v_sXML &= " <numero_sorteado_id>" & v_oRrow("numero_sorteado_id") & "</numero_sorteado_id>"
                    v_sXML &= " <sorteio_id>" & v_oRrow("sorteio_id") & "</sorteio_id>"
                    v_sXML &= " <numero_titulo>" & v_oRrow("numero_titulo") & "</numero_titulo>"
                    v_sXML &= " <val_sorteado>" & v_oRrow("val_sorteado") & "</val_sorteado>"
                    v_sXML &= " <dt_sorteio>" & CDate(v_oRrow("dt_sorteio")).ToString("dd/MM/yyyy") & "</dt_sorteio>"
                    v_sXML &= " <produto>" & v_oRrow("produto") & "</produto>"
                    v_sXML &= " <codigo_serie>" & v_oRrow("codigo_serie") & "</codigo_serie>"
                    v_sXML &= "</numero_sorteado>"
                Next
            Else
                v_sXML &= "<numero_sorteado>"
                v_sXML &= " <numero_sorteado_id></numero_sorteado_id>"
                v_sXML &= " <sorteio_id></sorteio_id>"
                v_sXML &= " <numero_titulo></numero_titulo>"
                v_sXML &= " <val_sorteado></val_sorteado>"
                v_sXML &= " <dt_sorteio></dt_sorteio>"
                v_sXML &= " <produto></produto>"
                v_sXML &= " <codigo_serie></codigo_serie>"
                v_sXML &= "</numero_sorteado>"
            End If
            v_sXML &= "</resultado></root>"

            Return v_sXML
        Catch ex As Exception
            Dim dst_mDataSetIntegracao As DataSet = New DataSet()
            dst_mDataSetIntegracao.Tables.Add(Gera_Resultado(1, ex.Message))

            Return dst_mDataSetIntegracao.GetXml
        End Try

    End Function

#Region "M�todos Privados"

    Private Sub ConfiguraAmbienteAlianca(ByVal enu_pAmbiente As Alianca.Seguranca.BancoDados.cCon.Ambientes)
        Try
            If Alianca.Seguranca.BancoDados.cCon.configurado Then
                Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, enu_pAmbiente)
            Else
                Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, enu_pAmbiente)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Private Function Gera_Resultado(ByVal str_pTipo As String, ByVal str_pMensagem As String) As DataTable
        Dim dtb_mResultado As DataTable = New DataTable("Resultado")
        Dim drw_mResultado As DataRow

        dtb_mResultado.Columns.Add("tipo", GetType(String))
        dtb_mResultado.Columns.Add("mensagem", GetType(String))

        drw_mResultado = dtb_mResultado.NewRow
        drw_mResultado("tipo") = str_pTipo
        drw_mResultado("mensagem") = str_pMensagem
        dtb_mResultado.Rows.Add(drw_mResultado)

        Return dtb_mResultado
    End Function

    'Private Function Gera_Retorno(ByVal dst_pDataSetIntegracao As DataSet, ByVal str_pEvento As String) As String
    '    Dim obj_mStringWriter As StringWriter
    '    Dim str_mRetorno As String

    '    obj_mStringWriter = New StringWriter()
    '    dst_pDataSetIntegracao.DataSetName = str_pEvento
    '    dst_pDataSetIntegracao.WriteXml(obj_mStringWriter, XmlWriteMode.WriteSchema)
    '    str_mRetorno = obj_mStringWriter.ToString()

    '    Return str_mRetorno
    'End Function

    Private Function Obter_Parametros(ByVal objs As List(Of ClsClienteNumeroSorteRN.cParametro), ByVal obj As ClsClienteNumeroSorteRN.cParametro, ByVal obj_mXMLParametros As XmlNodeList) As List(Of ClsClienteNumeroSorteRN.cParametro)
        'Sobrecarga de m�todo para objeto ClsClienteNumeroSorteRN
        Try
            ' Recupera Par�metros
            For int_mLoopParam As Integer = 0 To obj_mXMLParametros(0).ChildNodes.Count - 1
                obj.nome = obj_mXMLParametros(0).ChildNodes(int_mLoopParam).Name
                obj.valor = obj_mXMLParametros(0).ChildNodes(int_mLoopParam).InnerText
                objs.Add(obj)
            Next

            Return objs

        Catch ex As Exception
            Throw New Exception("Obter_Parametros: " & ex.Message)
        End Try

    End Function

    Private Function Obter_Parametros(ByVal objs As List(Of ClsNumeroSorteadoRN.cParametro), ByVal obj As ClsNumeroSorteadoRN.cParametro, ByVal obj_mXMLParametros As XmlNodeList) As List(Of ClsNumeroSorteadoRN.cParametro)
        'Sobrecarga de m�todo para objeto ClsNumeroSorteadoRN
        Try
            ' Recupera Par�metros
            For int_mLoopParam As Integer = 0 To obj_mXMLParametros(0).ChildNodes.Count - 1
                obj.nome = obj_mXMLParametros(0).ChildNodes(int_mLoopParam).Name
                obj.valor = obj_mXMLParametros(0).ChildNodes(int_mLoopParam).InnerText
                objs.Add(obj)
            Next

            Return objs

        Catch ex As Exception
            Throw New Exception("Obter_Parametros: " & ex.Message)
        End Try

    End Function

#End Region

End Class