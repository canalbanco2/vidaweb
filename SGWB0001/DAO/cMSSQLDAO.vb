'---------------------------------------------------------------------------------------
' Classe    : cMSSQLDAO
' Autor     : MFrasca - Marcelo Valverde Frasca
' Data      : 26/01/2010
' Demanda   : 2619415 - Integra��o First One
' Objetivo  : Classe que disponibiliza os m�todos para acesso ao banco de dados
'             por interm�dio da SABL0101
'---------------------------------------------------------------------------------------

Imports Alianca.Seguranca.BancoDados
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text


Public MustInherit Class cMSSQLDAO

    Private oDS As DataSet
    Private oDR As SqlDataReader
    Private _UsuarioLogado As String
    Private _banco As eBanco

    Private _Parametros As New ArrayList

    Public Enum eBanco As Byte
        seguros_db = 1
        integracao_evento_db = 2
    End Enum

#Region "Propriedades"

    '---------------------------------------------------------------------------------------
    ' Propriedade : pBanco
    ' Autor       : MFrasca - Marcelo Valverde Frasca
    ' Data        : 26/01/2010
    ' Demanda     : 2619415 - Integra��o First One
    ' Objetivo    : Armazena e determina o banco de dados onde o comando SQL ser� executado.
    '---------------------------------------------------------------------------------------
    Public Property pBanco() As eBanco
        Get
            Return _banco
        End Get
        Set(ByVal Value As eBanco)
            _banco = Value
            'cCon.BancodeDados = _banco.ToString()
        End Set
    End Property

    '---------------------------------------------------------------------------------------
    ' Propriedade : pUsuarioLogado
    ' Autor       : MFrasca - Marcelo Valverde Frasca
    ' Data        : 26/01/2010
    ' Demanda     : 2619415 - Integra��o First One
    ' Objetivo    : Armazena o usu�rio logado na aplica��o.
    '---------------------------------------------------------------------------------------
    Public Property pUsuarioLogado() As String
        Get
            Return _UsuarioLogado
        End Get
        Set(ByVal Value As String)
            _UsuarioLogado = Value
        End Set
    End Property

    '---------------------------------------------------------------------------------------
    ' Propriedade : pAmbiente
    ' Autor       : MFrasca - Marcelo Valverde Frasca
    ' Data        : 26/01/2010
    ' Demanda     : 2619415 - Integra��o First One
    ' Objetivo    : Exibe o ambiente onde a aplica��o foi conectada.
    '---------------------------------------------------------------------------------------
    Public ReadOnly Property pAmbiente() As cCon.Ambientes
        Get
            Return cCon.Ambiente
        End Get
    End Property

    '---------------------------------------------------------------------------------------
    ' Propriedade : DS
    ' Autor       : MFrasca - Marcelo Valverde Frasca
    ' Data        : 26/01/2010
    ' Demanda     : 2619415 - Integra��o First One
    ' Objetivo    : Propriedade onde � disponibilizado o DataSet retornado pelo m�todo 
    '               ExecutaSQL
    '---------------------------------------------------------------------------------------
    Public Property DS() As DataSet
        Get
            Return oDS
        End Get
        Set(ByVal Value As DataSet)
            oDS = Value
        End Set
    End Property

    '---------------------------------------------------------------------------------------
    ' Propriedade : DR
    ' Autor       : MFrasca - Marcelo Valverde Frasca
    ' Data        : 26/01/2010
    ' Demanda     : 2619415 - Integra��o First One
    ' Objetivo    : Propriedade onde � disponibilizado o SqlDataReader retornado pelo m�todo 
    '               ExecuteReader
    '---------------------------------------------------------------------------------------
    Public Property DR() As SqlDataReader
        Get
            Return oDR
        End Get
        Set(ByVal Value As SqlDataReader)
            oDR = Value
        End Set
    End Property

#End Region

#Region "M�todo Criador"

    Sub New(ByVal Bd As eBanco)
        cCon.BancodeDados = Bd.ToString()
    End Sub

#End Region

#Region "M�todos P�blicos"

    '---------------------------------------------------------------------------------------
    ' M�todo      : adicionarParametro
    ' Autor       : MFrasca - Marcelo Valverde Frasca
    ' Data        : 26/01/2010
    ' Demanda     : 2619415 - Integra��o First One
    ' Objetivo    : Adiciona par�metros na lista de par�metros a ser utilizada pelos m�todos 
    '               que executam os comandos SQL como, por exemplo, ExecutaSQL, ExecuteNonQuery, etc.
    '---------------------------------------------------------------------------------------
    Public Sub adicionarParametro(ByVal Nome As String, ByVal Valor As String)
        _Parametros.Add(New SqlParameter(TrataParametro(Nome), Valor))
    End Sub

    '---------------------------------------------------------------------------------------
    ' M�todo      : LimparParametros
    ' Autor       : MFrasca - Marcelo Valverde Frasca
    ' Data        : 26/01/2010
    ' Demanda     : 2619415 - Integra��o First One
    ' Objetivo    : Limpa os par�metros carregados na lista de par�metros
    '---------------------------------------------------------------------------------------
    Public Sub LimparParametros()
        _Parametros.Clear()
    End Sub

    '---------------------------------------------------------------------------------------
    ' M�todo      : IniciaTransacao
    ' Autor       : MFrasca - Marcelo Valverde Frasca
    ' Data        : 26/01/2010
    ' Demanda     : 2619415 - Integra��o First One
    ' Objetivo    : Inicia uma nova transa��o no banco de dados retornando o n�mero dessa
    '               transa��o aberta.
    '---------------------------------------------------------------------------------------
    Public Function IniciaTransacao() As Integer

        Return cCon.BeginTransaction()

    End Function

    '---------------------------------------------------------------------------------------
    ' M�todo      : CommitTransacao
    ' Autor       : MFrasca - Marcelo Valverde Frasca
    ' Data        : 26/01/2010
    ' Demanda     : 2619415 - Integra��o First One
    ' Objetivo    : Efetiva as altera��es realizadas na transa��o.
    '---------------------------------------------------------------------------------------
    Public Sub CommitTransacao(ByVal _numTran As Integer)
        Try
            cCon.CommitTransaction(_numTran)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    '---------------------------------------------------------------------------------------
    ' M�todo      : RollBackTransacao
    ' Autor       : MFrasca - Marcelo Valverde Frasca
    ' Data        : 26/01/2010
    ' Demanda     : 2619415 - Integra��o First One
    ' Objetivo    : Retorna as altera��es realizada na transa��o ao estado inicial.
    '---------------------------------------------------------------------------------------
    Public Sub RollBackTransacao(ByVal _numTran As Integer)
        Try
            cCon.RollBackTransaction(_numTran)
        Catch ex As Exception
            'Ignorar o erro
        End Try
    End Sub

    '---------------------------------------------------------------------------------------
    ' M�todo      : ExecutaSQL
    ' Autor       : MFrasca - Marcelo Valverde Frasca
    ' Data        : 26/01/2010
    ' Demanda     : 2619415 - Integra��o First One
    ' Objetivo    : Executa uma procedure ou comando SQL utilizando os par�metros carregados
    '               na vari�veis _Parametros atrav�s do m�todo adicionarParametro(). O retorno 
    '               � disponibilizado na propriedade
    '---------------------------------------------------------------------------------------
    Public Sub ExecutaSQL(ByVal ProcedureName As String)
        Try

            Dim sb As New StringBuilder
            Dim first As Boolean = True

            For Each p As SqlParameter In _Parametros
                If first Then
                    first = False
                    sb.Append(p.ParameterName & " = " & CStr(p.Value))
                Else
                    sb.Append(", " & p.ParameterName & " = " & CStr(p.Value))
                End If
            Next

            cCon.ConnectionTimeout = cCon.TimeTimeout.M�dio

            oDS = cCon.ExecuteDataset(CommandType.Text, ProcedureName & " " & sb.ToString())

        Catch ex As Exception
            Dim errMessage As String = "cMSSQLDAO.ExecutaSQL - "
            Dim tempException As Exception = ex

            While (Not tempException Is Nothing)
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine
                tempException = tempException.InnerException
            End While

            Throw New Exception(errMessage)

        End Try

    End Sub

    '---------------------------------------------------------------------------------------
    ' M�todo      : ExecutaSQL
    ' Autor       : MFrasca - Marcelo Valverde Frasca
    ' Data        : 26/01/2010
    ' Demanda     : 2619415 - Integra��o First One
    ' Objetivo    : Executa uma procedure ou comando SQL utilizando os par�metros carregados
    '               na vari�veis _Parametros atrav�s do m�todo adicionarParametro() 
    '               utilizando a tarnsa��o passada. O retorno � disponibilizado na 
    '               propriedade DS
    '---------------------------------------------------------------------------------------
    Public Sub ExecutaSQL(ByVal ProcedureName As String, ByVal _numTran As Integer)
        Try

            Dim sb As New StringBuilder
            Dim first As Boolean = True

            For Each p As SqlParameter In _Parametros
                If first Then
                    first = False
                    sb.Append(p.ParameterName & " = " & CStr(p.Value))
                Else
                    sb.Append(", " & p.ParameterName & " = " & CStr(p.Value))
                End If
            Next

            cCon.ConnectionTimeout = cCon.TimeTimeout.M�dio

            oDS = cCon.ExecuteDataset(_numTran, CommandType.Text, ProcedureName & " " & sb.ToString())

        Catch ex As Exception
            Dim errMessage As String = "cMSSQLDAO.ExecutaSQL - "
            Dim tempException As Exception = ex

            While (Not tempException Is Nothing)
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine
                tempException = tempException.InnerException
            End While

            Throw New Exception(errMessage)

        End Try

    End Sub

    '---------------------------------------------------------------------------------------
    ' M�todo      : ExecuteScalar
    ' Autor       : MFrasca - Marcelo Valverde Frasca
    ' Data        : 26/01/2010
    ' Demanda     : 2619415 - Integra��o First One
    ' Objetivo    : Executa uma procedure ou comando SQL utilizando os par�metros carregados
    '               na vari�veis _Parametros atrav�s do m�todo adicionarParametro().
    '---------------------------------------------------------------------------------------
    Public Function ExecuteScalar(ByVal ProcedureName As String) As Integer
        Try

            Dim sb As New StringBuilder
            Dim first As Boolean = True

            For Each p As SqlParameter In _Parametros
                If first Then
                    first = False
                    sb.Append(p.ParameterName & " = " & CStr(p.Value))
                Else
                    sb.Append(", " & p.ParameterName & " = " & CStr(p.Value))
                End If
            Next

            cCon.ConnectionTimeout = cCon.TimeTimeout.M�dio

            Dim _return As Integer = 0
            Dim _ds As DataSet = cCon.ExecuteDataset(CommandType.Text, ProcedureName & " " & sb.ToString())

            If _ds.Tables.Count > 0 Then
                If _ds.Tables(0).Rows.Count > 0 Then
                    _return = CInt(IIf(IsDBNull(_ds.Tables(0).Rows(0).Item(0)), 0, _ds.Tables(0).Rows(0).Item(0)))
                End If
            End If

            _ds = Nothing

            Return _return

        Catch ex As Exception
            Dim errMessage As String = "cMSSQLDAO.ExecuteScalar - "
            Dim tempException As Exception = ex

            While (Not tempException Is Nothing)
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine
                tempException = tempException.InnerException
            End While

            Throw New Exception(errMessage)

        End Try

    End Function

    '---------------------------------------------------------------------------------------
    ' M�todo      : ExecuteScalar
    ' Autor       : MFrasca - Marcelo Valverde Frasca
    ' Data        : 26/01/2010
    ' Demanda     : 2619415 - Integra��o First One
    ' Objetivo    : Executa uma procedure ou comando SQL utilizando os par�metros carregados
    '               na vari�veis _Parametros atrav�s do m�todo adicionarParametro() 
    '               utilizando a tarnsa��o passada.
    '---------------------------------------------------------------------------------------
    Public Function ExecuteScalar(ByVal ProcedureName As String, ByVal _numTran As Integer) As Integer
        Try

            Dim sb As New StringBuilder
            Dim first As Boolean = True

            For Each p As SqlParameter In _Parametros
                If first Then
                    first = False
                    sb.Append(p.ParameterName & " = " & CStr(p.Value))
                Else
                    sb.Append(", " & p.ParameterName & " = " & CStr(p.Value))
                End If
            Next

            cCon.ConnectionTimeout = cCon.TimeTimeout.M�dio

            Dim _return As Integer = 0
            Dim _ds As DataSet = cCon.ExecuteDataset(_numTran, CommandType.Text, ProcedureName & " " & sb.ToString())

            If _ds.Tables.Count > 0 Then
                If _ds.Tables(0).Rows.Count > 0 Then
                    _return = CInt(IIf(IsDBNull(_ds.Tables(0).Rows(0).Item(0)), 0, _ds.Tables(0).Rows(0).Item(0)))
                End If
            End If

            _ds = Nothing

            Return _return

        Catch ex As Exception
            Dim errMessage As String = "cMSSQLDAO.ExecuteScalar - "
            Dim tempException As Exception = ex

            While (Not tempException Is Nothing)
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine
                tempException = tempException.InnerException
            End While

            Throw New Exception(errMessage)

        End Try

    End Function

    '---------------------------------------------------------------------------------------
    ' M�todo      : ExecuteNonQuery
    ' Autor       : MFrasca - Marcelo Valverde Frasca
    ' Data        : 26/01/2010
    ' Demanda     : 2619415 - Integra��o First One
    ' Objetivo    : Executa uma procedure ou comando SQL utilizando os par�metros carregados
    '               na vari�veis _Parametros atrav�s do m�todo adicionarParametro().
    '---------------------------------------------------------------------------------------
    Public Function ExecuteNonQuery(ByVal ProcedureName As String) As Integer

        Try

            Dim sb As New StringBuilder
            Dim first As Boolean = True

            For Each p As SqlParameter In _Parametros
                If first Then
                    first = False
                    sb.Append(p.ParameterName & " = " & CStr(p.Value))
                Else
                    sb.Append(", " & p.ParameterName & " = " & CStr(p.Value))
                End If
            Next

            cCon.ConnectionTimeout = cCon.TimeTimeout.M�dio

            Return cCon.ExecuteNonQuery(CommandType.Text, ProcedureName & " " & sb.ToString())

        Catch e As Exception
            Dim errMessage As String = "cMSSQLDAO.ExecuteNonQuery - "
            Dim tempException As Exception = e

            While (Not tempException Is Nothing)
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine
                tempException = tempException.InnerException
            End While

            Throw New Exception(errMessage)

        End Try

    End Function

    '---------------------------------------------------------------------------------------
    ' M�todo      : ExecuteNonQuery
    ' Autor       : MFrasca - Marcelo Valverde Frasca
    ' Data        : 26/01/2010
    ' Demanda     : 2619415 - Integra��o First One
    ' Objetivo    : Executa uma procedure ou comando SQL utilizando os par�metros carregados
    '               na vari�veis _Parametros atrav�s do m�todo adicionarParametro() 
    '               utilizando a tarnsa��o passada.
    '---------------------------------------------------------------------------------------
    Public Function ExecuteNonQuery(ByVal ProcedureName As String, ByVal _numTran As Integer) As Integer

        Try

            Dim sb As New StringBuilder
            Dim first As Boolean = True

            For Each p As SqlParameter In _Parametros
                If first Then
                    first = False
                    sb.Append(p.ParameterName & " = " & CStr(p.Value))
                Else
                    sb.Append(", " & p.ParameterName & " = " & CStr(p.Value))
                End If
            Next

            cCon.ConnectionTimeout = cCon.TimeTimeout.M�dio

            Return cCon.ExecuteNonQuery(_numTran, CommandType.Text, ProcedureName & " " & sb.ToString())

        Catch e As Exception
            Dim errMessage As String = "cMSSQLDAO.ExecuteNonQuery - "
            Dim tempException As Exception = e

            While (Not tempException Is Nothing)
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine
                tempException = tempException.InnerException
            End While

            Throw New Exception(errMessage)

        End Try

    End Function

    '---------------------------------------------------------------------------------------
    ' M�todo      : ExecuteReader
    ' Autor       : MFrasca - Marcelo Valverde Frasca
    ' Data        : 26/01/2010
    ' Demanda     : 2619415 - Integra��o First One
    ' Objetivo    : Executa uma procedure ou comando SQL utilizando os par�metros carregados
    '               na vari�veis _Parametros atrav�s do m�todo adicionarParametro().
    '---------------------------------------------------------------------------------------
    Public Sub ExecuteReader(ByVal ProcedureName As String)

        Try

            Dim sb As New StringBuilder
            Dim first As Boolean = True

            For Each p As SqlParameter In _Parametros
                If first Then
                    first = False
                    sb.Append(p.ParameterName & " = " & CStr(p.Value))
                Else
                    sb.Append(", " & p.ParameterName & " = " & CStr(p.Value))
                End If
            Next

            cCon.ConnectionTimeout = cCon.TimeTimeout.M�dio

            oDR = cCon.ExecuteReader(CommandType.Text, ProcedureName & " " & sb.ToString())

        Catch e As Exception
            Dim errMessage As String = "cMSSQLDAO.ExecuteReader - "
            Dim tempException As Exception = e

            While (Not tempException Is Nothing)
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine
                tempException = tempException.InnerException
            End While

            Throw New Exception(errMessage)

        End Try

    End Sub

    '---------------------------------------------------------------------------------------
    ' M�todo      : ExecuteReader
    ' Autor       : MFrasca - Marcelo Valverde Frasca
    ' Data        : 26/01/2010
    ' Demanda     : 2619415 - Integra��o First One
    ' Objetivo    : Executa uma procedure ou comando SQL utilizando os par�metros carregados
    '               na vari�veis _Parametros atrav�s do m�todo adicionarParametro() 
    '               utilizando a tarnsa��o passada.
    '---------------------------------------------------------------------------------------
    Public Sub ExecuteReader(ByVal ProcedureName As String, ByVal _numTran As Integer)

        Try

            Dim sb As New StringBuilder
            Dim first As Boolean = True

            For Each p As SqlParameter In _Parametros
                If first Then
                    first = False
                    sb.Append(p.ParameterName & " = " & CStr(p.Value))
                Else
                    sb.Append(", " & p.ParameterName & " = " & CStr(p.Value))
                End If
            Next

            cCon.ConnectionTimeout = cCon.TimeTimeout.M�dio

            oDR = cCon.ExecuteReader(_numTran, CommandType.Text, ProcedureName & " " & sb.ToString())

        Catch e As Exception
            Dim errMessage As String = "cMSSQLDAO.ExecuteReader - "
            Dim tempException As Exception = e

            While (Not tempException Is Nothing)
                errMessage += tempException.Message + Environment.NewLine + Environment.NewLine
                tempException = tempException.InnerException
            End While

            Throw New Exception(errMessage)

        End Try

    End Sub

#End Region

#Region "M�todos Utilitarios"

    Public Function TrataString(ByVal Str As String) As String
        If Str Is Nothing Then
            Return "null"
        Else
            If Str.ToString.Trim = String.Empty Then
                Return "null"
            Else
                Return "'" & Str.Replace("'", "''") & "'"
            End If
        End If
    End Function

    Public Function TrataParametro(ByVal Str As String) As String
        If Str Is Nothing Then
            Throw New Exception("Parametro inv�lido.")
        Else
            If Str.ToString.Trim = String.Empty Then
                Throw New Exception("Parametro inv�lido.")
            Else
                Return IIf(Str.IndexOf("@") = -1, "@" & Str.ToUpper(), Str.ToUpper()).ToString().Replace("=", "").ToString()
            End If
        End If
    End Function

    Public Function TrataInteger(ByVal Str As Object) As String
        If Str Is Nothing Then
            Return "null"
        Else
            If Str.ToString.Trim = String.Empty Then
                Return "null"
            Else
                Return Str.ToString.Replace(".", "").Replace(",", ".")
            End If
        End If
    End Function

    Public Function TrataData(ByVal Dia As String, ByVal Mes As String, ByVal Ano As String) As String
        If Dia = "" And Mes = "" And Ano = "" Then
            Return "null"
        Else
            Dim Data As Double
            Data = Integer.Parse(Dia)
            Data += Integer.Parse(Mes) * 100
            Data += Integer.Parse(Ano) * 10000

            Return "'" & Data & "'"

        End If
    End Function

    Public Function TrataData(ByVal data As String) As String
        Dim dia, mes, ano, Adata() As String
        dia = ""
        mes = ""
        ano = ""
        If data.Trim <> "" Then
            Adata = data.Split(CChar(" "))(0).Split(CChar("/"))
            If Adata.Length >= 3 Then
                dia = Adata(0)
                mes = Adata(1)
                ano = Adata(2)
            End If
        End If

        Return TrataData(dia, mes, ano)

    End Function

#End Region

End Class