Imports System.Xml
Imports System.IO

Partial Public Class SGWB0001
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim v_oWebService As New SGBW0001
        Dim ds As New DataSet

        'v_oWebService.Timeout = 60000

        ' Carrega XML

        'PARA TESTAR
        'Comentar a linha abaixo
        Dim v_sXML As String = "<?xml version='1.0' standalone='yes' ?><usuario><parametros><cpf_cnpj>" & Request("cpf_cnpj") & "</cpf_cnpj><cod_brasilcap>" & Request("cod_brasilcap") & "</cod_brasilcap></parametros></usuario>"
        'Remover comentário da linha abaixo e editar os parâmetros de entrada
        'Dim v_sXML As String = "<?xml version='1.0' standalone='yes' ?><usuario><parametros><cpf_cnpj>62536036391</cpf_cnpj><cod_brasilcap>99</cod_brasilcap></parametros></usuario>"

        Dim v As String = v_oWebService.RetornaXml(v_sXML.ToString())
        Response.ContentType = "text/xml"
        Response.Write(v)

    End Sub

End Class