Public Partial Class SGWB0001_2
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim v_oWebService As New SGBW0001

        'PARA TESTAR
        'Comentar a linha abaixo:
        Dim v_sXML As String = "<?xml version='1.0' standalone='yes' ?><usuario><parametros><cod_brasilcap>" & Request("cod_brasilcap") & "</cod_brasilcap></parametros></usuario>"
        'remover comentário da linha abaixo, editando o cod_brasilcap
        'Dim v_sXML As String = "<?xml version='1.0' standalone='yes' ?><usuario><parametros><cod_brasilcap>" & "99" & "</cod_brasilcap></parametros></usuario>"

        Dim v As String = v_oWebService.RetornaXmlNumeroSorteado(v_sXML.ToString())
        Response.ContentType = "text/xml"
        Response.Write(v)

    End Sub

End Class