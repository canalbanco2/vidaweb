Imports System.IO

Public Class ClsClienteNumeroSorteRN
    Inherits cMSSQLDAO

#Region "Declara��es"
    Private str_cRetorno As String
    Private int_cRetorno As Integer
    Private v_oDsRetorno As DataSet
    Private obj_cStringWriter As StringWriter

    Enum cTipo_Procedure
        Selecao = 1
        Inclusao = 2
    End Enum
#End Region

#Region "Construtores"
    Sub New()
        MyClass.New(cMSSQLDAO.eBanco.seguros_db)
    End Sub

    Sub New(ByVal bd As eBanco)
        MyBase.New(bd)
        Me.pBanco = bd

        str_cRetorno = ""
        int_cRetorno = 0
    End Sub
#End Region

#Region "Structures"

    Public Structure cParametro
        Dim nome As String
        Dim valor As String
    End Structure

    Public Structure cDocumento
        Dim cliente_numero_sorte_id As Integer
        'Dim nome_arquivo_frente As String
        'Dim path_completo_frente As String
        'Dim tamanho_frente As String
        'Dim data_criacao_frente As String
        'Dim conteudo_arquivo_frente As String
        'Dim nome_arquivo_verso As String
        'Dim path_completo_verso As String
        'Dim tamanho_verso As String
        'Dim data_criacao_verso As String
        'Dim conteudo_arquivo_verso As String
        'Dim tp_documento_id As String
        'Dim tipo_documento As String
    End Structure

#End Region

    ''' <summary>
    ''' M�todo respons�vel por retornar dados
    ''' </summary>
    ''' <param name="obj_Parametros"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Retornar_NumeroSorte(ByVal obj_Parametros As List(Of cParametro)) As DataSet
        Try
            Dim int_Indice_Table As Integer = 0
            Dim int_Tables_Retorno As Integer = 0
            'Dim obj_Parametro As cParametro
            Dim str_Procedure As String = String.Empty
            v_oDsRetorno = New DataSet()

            'Obt�m a procedure de sele��o relacionada ao evento
            str_Procedure = "SEGS8411_SPS"

            'Limpa par�metros da �ltima execu��o no banco
            LimparParametros()

            'Obt�m e armazena os 2 par�metros passados
            'For Each obj_Parametro In obj_Parametros
            '   adicionarParametro(obj_Parametro.nome, TrataString(obj_Parametro.valor))
            'Next
            adicionarParametro(obj_Parametros.Item(0).nome, TrataString(obj_Parametros.Item(0).valor))
            adicionarParametro(obj_Parametros.Item(1).nome, TrataInteger(obj_Parametros.Item(1).valor))

            'Executa procedure no banco
            ExecutaSQL(str_Procedure)

            'Obt�m o retorno da procedure
            v_oDsRetorno = MyBase.DS

            If v_oDsRetorno.Tables.Count > 0 Then
                Dim v_oNovoDtRetorno As DataTable = Gera_NovoDataTable(v_oDsRetorno.Tables(0), "cpf_cnpj", "numero_titulo", "dt_sorteio", "cod_brasilcap", "codigo_serie", "produto")
                v_oDsRetorno.Tables.Clear()

                v_oDsRetorno.Tables.Add(v_oNovoDtRetorno)

                Return v_oDsRetorno
            Else
                Throw New Exception("A pesquisa realizada n�o retornou dados.")
            End If

        Catch ex As Exception
            ' Retorno com mensagem de erro
            v_oDsRetorno.Tables.Add(Gera_Resultado(1, ex.Message))
        Finally
            ' Monta retorno
            str_cRetorno = Gera_Retorno("Retorno")
        End Try

        Return Nothing
    End Function

    Private Function Gera_Resultado(ByVal str_pTipo As String, ByVal str_pMensagem As String) As DataTable
        Dim dtb_mResultado As DataTable = New DataTable("Resultado")
        Dim drw_mResultado As DataRow

        dtb_mResultado.Columns.Add("tipo", GetType(String))
        dtb_mResultado.Columns.Add("mensagem", GetType(String))

        drw_mResultado = dtb_mResultado.NewRow
        drw_mResultado("tipo") = str_pTipo
        drw_mResultado("mensagem") = str_pMensagem
        dtb_mResultado.Rows.Add(drw_mResultado)

        Return dtb_mResultado
    End Function

    Private Function Gera_Retorno(ByVal str_pEvento As String) As String
        Dim str_mRetorno As String

        obj_cStringWriter = New StringWriter()
        v_oDsRetorno.DataSetName = str_pEvento
        v_oDsRetorno.WriteXml(obj_cStringWriter, XmlWriteMode.WriteSchema)
        str_mRetorno = obj_cStringWriter.ToString()

        Return str_mRetorno
    End Function

    Private Function Gera_NovoDataTable(ByVal p_oDtResultado As DataTable, ByVal ParamArray coloumnNames() As String) As DataTable
        Dim v_oDataTable As New DataTable

        Dim v_oDataView As DataView = New DataView(p_oDtResultado)
        v_oDataTable = v_oDataView.ToTable(True, coloumnNames)

        Return v_oDataTable
    End Function

End Class
