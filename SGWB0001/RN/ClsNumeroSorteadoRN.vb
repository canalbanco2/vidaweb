Imports System.IO
Public Class ClsNumeroSorteadoRN
    Inherits cMSSQLDAO

#Region "Declara��es"
    Private str_cRetorno As String
    Private int_cRetorno As Integer
    Private v_oDsRetorno As DataSet
    Private obj_cStringWriter As StringWriter

    Enum cTipo_Procedure
        Selecao = 1
        Inclusao = 2
    End Enum
#End Region

#Region "Construtores"
    Sub New()
        MyClass.New(cMSSQLDAO.eBanco.seguros_db)
    End Sub

    Sub New(ByVal bd As eBanco)
        MyBase.New(bd)
        Me.pBanco = bd

        str_cRetorno = ""
        int_cRetorno = 0
    End Sub
#End Region

#Region "Structures"

    Public Structure cParametro
        Dim nome As String
        Dim valor As String
    End Structure

#End Region

    Public Function Retorna_NumeroSorteado(ByVal obj_Parametro As cParametro) As DataSet
        Try
            Dim str_Procedure As String = String.Empty
            v_oDsRetorno = New DataSet()

            'Obt�m a procedure de sele��o relacionada ao evento
            str_Procedure = "SEGS8560_SPS"

            'Limpa par�metros da �ltima execu��o no banco
            LimparParametros()

            'Adiciona o par�metro cod_brasilcap para execu��o da procedure
            adicionarParametro(obj_Parametro.nome, TrataInteger(obj_Parametro.valor))

            'Executa procedure no banco
            ExecutaSQL(str_Procedure)

            'Obt�m o retorno da procedure
            v_oDsRetorno = MyBase.DS

            If v_oDsRetorno.Tables.Count > 0 Then
                Return v_oDsRetorno
            Else
                Throw New Exception("A pesquisa realizada n�o retornou dados.")
            End If

        Catch ex As Exception
            ' Retorno com mensagem de erro
            v_oDsRetorno.Tables.Add(Gera_Resultado(1, ex.Message))
        End Try

        Return Nothing
    End Function

    Private Function Gera_Resultado(ByVal str_pTipo As String, ByVal str_pMensagem As String) As DataTable
        Dim dtb_mResultado As DataTable = New DataTable("Resultado")
        Dim drw_mResultado As DataRow

        dtb_mResultado.Columns.Add("tipo", GetType(String))
        dtb_mResultado.Columns.Add("mensagem", GetType(String))

        drw_mResultado = dtb_mResultado.NewRow
        drw_mResultado("tipo") = str_pTipo
        drw_mResultado("mensagem") = str_pMensagem
        dtb_mResultado.Rows.Add(drw_mResultado)

        Return dtb_mResultado
    End Function
End Class
