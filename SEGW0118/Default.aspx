<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="SEGW0118._Default" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <script language="javascript" type="text/javascript">
        function ExibirHelp() {
            var v_sTexto = 'Os campos a serem inclusos no arquivo para preenchimento autom�tico\n no momento de realizar o download pelo cliente s�o:\n \n';
            v_sTexto = v_sTexto + '[APOLICE] - Campo N�mero da Ap�lice;\n'
            v_sTexto = v_sTexto + '[RAMO] - Campo C�digo do Ramo;\n'
            v_sTexto = v_sTexto + '[SUBGRUPO] - Campo C�digo do Subgrupo;\n'
            v_sTexto = v_sTexto + '[NOMESUBGRUPO] - Campo Nome do Subgrupo;\n'
            v_sTexto = v_sTexto + '[ENDERECOSUBGRUPO] - Campo Endere�o do Subgrupo;\n'
            v_sTexto = v_sTexto + '[CNPJSUBGRUPO] - Campo CNPJ do Subgrupo;\n'
            v_sTexto = v_sTexto + '[CNPJESTIPULANTE] - Campo CNPJ do Estipulante;\n'
            v_sTexto = v_sTexto + '[INICIOVIGENCIAAPOLICE] - Campo Data Inicio Vig�ncia do Seguro;\n'
            v_sTexto = v_sTexto + '[FIMVIGENCIAAPOLICE] - Campo Data Fim Vig�ncia do Seguro;\n'
            v_sTexto = v_sTexto + '[AGENCIA] - Campo Agencia da Ap�lice;\n'
            
            alert(v_sTexto)
            
            return false;
        }
    </script>    
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <br />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
            <asp:View ID="View1" runat="server">
                <asp:GridView ID="gvDocumentos" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    CssClass="grid-view" DataSourceID="odsDocumentos" EmptyDataText="Nenhum �tem encontrado."
                    Width="100%" DataKeyNames="documento_vida_web_id,tipo_doc_vida_web_id">
                    <Columns>
                        <asp:BoundField DataField="descricao" HeaderText="Tipo Documento">
                            <HeaderStyle Width="350px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Caminho Rede">
                            <ItemTemplate>
                                <asp:Label ID="lblCaminho" runat="server"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="arquivo" HeaderText="Arquivo" >
                            <HeaderStyle Width="200px" />
                        </asp:BoundField>
                    </Columns>
                    <PagerStyle CssClass="header" />
                    <SelectedRowStyle BackColor="DeepSkyBlue" Font-Bold="True" Font-Italic="False" />
                    <EmptyDataRowStyle ForeColor="Red" /> 
                    <HeaderStyle CssClass="header" />
                    <RowStyle CssClass="normal" />
                    <AlternatingRowStyle CssClass="alternate" />
                </asp:GridView>
                <asp:ObjectDataSource ID="odsDocumentos" runat="server" SelectMethod="Select_GvDocumentos"
                    TypeName="SEGW0118._Default"></asp:ObjectDataSource>
                <br />
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnIncluir" runat="server" CssClass="Botao" Text="Incluir" Width="80px" />
                            <asp:Button ID="btnAlterar" runat="server" CssClass="Botao" Text="Alterar" Width="80px" Enabled="False" />
                            <asp:Button ID="Button3" runat="server" CssClass="Botao" Text="Cancelar" Width="80px" Visible="False" /></td>
                    </tr>
                </table>
            </asp:View>
            &nbsp;
            <asp:View ID="View2" runat="server">
                <asp:Panel ID="Panel1" runat="server" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                    Width="98%">
                    <table border="0" cellpadding="1" cellspacing="1" style="width: 98%">
                        <tr>
                            <td width="150">
                                <asp:Label ID="Label1" runat="server" Text="Tipo Documento"></asp:Label></td>
                            <td width="15">
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlTipoDoc" runat="server" DataSourceID="odsDdlTipoDoc" DataTextField="nova_descricao"
                                    DataValueField="tipo_doc_vida_web_id">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvTpDocumento" runat="server" ControlToValidate="ddlTipoDoc"
                                    Display="None" ErrorMessage=" - Tipo Documento" InitialValue="##!##L-I-N-H-A-E-X-T-R-A##!##"
                                    SetFocusOnError="True" ValidationGroup="grpArquivo" Visible="true">*</asp:RequiredFieldValidator>
                                <asp:ObjectDataSource ID="odsDdlTipoDoc" runat="server" SelectMethod="Select_DdlTipoDocumentos"
                                    TypeName="SEGW0118._Default"></asp:ObjectDataSource>
                                <asp:Label ID="lblTipoDoc" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text="Arquivo"></asp:Label></td>
                            <td>
                            </td>
                            <td>
                                <asp:FileUpload ID="FileUpload1" runat="server" Width="75%"  /><br />
                                <asp:Label ID="lblArquivo" runat="server" ForeColor="Gray"></asp:Label><br />
                                <asp:RegularExpressionValidator ID="Regularexpressionvalidator1" runat="server" ControlToValidate="FileUpload1"
                                    ErrorMessage="Selecione um arquivo v�lido *.doc file!" Font-Size="11px" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.doc|.docx)$"
                                    ValidationGroup="grpArquivo"></asp:RegularExpressionValidator></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <table border="0" cellpadding="1" cellspacing="1" style="width: 400px">
                                    <tr>
                                        <td align="center" valign="middle">
                                <asp:RadioButtonList ID="rblTipoCorretor" runat="server" RepeatDirection="Horizontal">
                                </asp:RadioButtonList></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td align="center">
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br />
                <table border="0" cellpadding="1" cellspacing="1" style="width: 98%">
                    <tr>
                        <td width="150">
                            <asp:Button ID="btnAjuda" runat="server" CssClass="Botao" Text="Ajuda Arquivo" Width="120px" CausesValidation="False" /></td>
                        <td width="15">
                        </td>
                        <td align="center">
                            <asp:Button ID="btnGravar" runat="server" CssClass="Botao" Text="Gravar" Width="80px" ValidationGroup="grpArquivo" />
                            <asp:Button ID="btnCancelar" runat="server" CssClass="Botao" Text="Cancelar" Width="80px" /></td>
                    </tr>
                </table>
            </asp:View>
        </asp:MultiView></div>
    </form>
</body>
</html>
