Imports SEGL0325
Imports System.IO

Partial Public Class _Default
    Inherits PaginaBaseSEGW

#Region " Variavei e Propriedades "
    Dim v_sCaminho As String = "D:\Confitec_Inetpub\SEG\POST\VIDA_WEB"

    Private m_sRN As ClsDocumentoVidaWebRN
    Private Shadows ReadOnly Property RN() As ClsDocumentoVidaWebRN
        Get
            If IsNothing(m_sRN) Then
                Me.m_sRN = New ClsDocumentoVidaWebRN
            End If
            Return m_sRN
        End Get
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            Me.HelperSEGW.Preencher_rblTipoCorretor(Me.rblTipoCorretor)
            Me.rblTipoCorretor.Items(0).Selected = True
            Me.HelperSEGW.GerarScript(Me.Page, "top.escondeaguarde();", "escondeaguarde")

            Me.btnAjuda.Attributes.Add("onclick", "JavaScript:return ExibirHelp();")

            Dim v_sAmbiente As String = MyBase.Ambiente

            Try
                'Session("Caminho") = Me.HelperSEGW.RetornaDataTable_Parametros("SEGBR", "ARQUIVO_WEB", "CAMINHO", "DESENVOLVIMENTO").Rows(0)("Valor")
                Session("Caminho") = Me.HelperSEGW.RetornaDataTable_Parametros("SEGBR", "ARQUIVO_WEB", "CAMINHO", v_sAmbiente).Rows(0)("Valor")
            Catch ex As Exception
                Me.HelperSEGW.GerarAlert(Me.Page, "Caminho para Upload de documentos n�o encontrado no banco.", "sucesso")
            End Try

        End If
    End Sub

#Region " M�todo para DataSource "
    Public Function Select_GvDocumentos() As DataTable
        Dim v_oClsDocumentoVidaWebRN As New ClsDocumentoVidaWebRN
        Dim v_oResultado As DataTable = v_oClsDocumentoVidaWebRN.ListarDocumentos()

        Return v_oResultado
    End Function

    Public Function Select_DdlTipoDocumentos() As DataTable
        Dim v_oClsTipoDocVidaWebRN As New ClsTipoDocVidaWebRN
        Dim v_oResultado As DataTable = v_oClsTipoDocVidaWebRN.Listar_TipoDocumento()

        Return v_oResultado
    End Function
#End Region

#Region " Eventos Botao "

    Protected Sub btnIncluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIncluir.Click
        Me.ddlTipoDoc.Visible = True
        Me.ddlTipoDoc.Enabled = True
        Me.rfvTpDocumento.Enabled = True
        Me.lblTipoDoc.Visible = False

        Me.MultiView1.SetActiveView(Me.View2)
    End Sub

    Protected Sub btnAlterar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAlterar.Click
        If Me.gvDocumentos.SelectedIndex <> -1 Then
            Me.ddlTipoDoc.Visible = False
            Me.ddlTipoDoc.Enabled = False
            Me.rfvTpDocumento.Enabled = False
            Me.lblTipoDoc.Visible = True
            Try
                Dim v_oClsTipoDoc As New ClsTipoDocVidaWebRN
                v_oClsTipoDoc.TipoDocVidaWebId = Me.gvDocumentos.SelectedDataKey.Item("tipo_doc_vida_web_id")
                If v_oClsTipoDoc.Obter() Then
                    Me.lblTipoDoc.Text = v_oClsTipoDoc.TipoDocVidaWebId & " - " & v_oClsTipoDoc.Descricao
                End If

                Me.lblArquivo.Text = Session("Caminho") & "\" & Me.gvDocumentos.Rows(Me.gvDocumentos.SelectedIndex).Cells(2).Text
            Catch ex As Exception
                Throw ex
            End Try
            Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub

    Protected Sub btnGravar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGravar.Click
        Dim v_bSalvo As Boolean = False
        v_sCaminho = Session("Caminho")

        If Not FileUpload1.HasFile Then
            'Me.HelperSEGW.GerarAlert(Me.Page, "Nenhum arquivo selecionado.", "erro")
            'Exit Sub

            If Me.gvDocumentos.SelectedIndex > -1 Then
                Dim v_sFileName As String = Me.gvDocumentos.Rows(Me.gvDocumentos.SelectedIndex).Cells(2).Text

                'ALTERAR DADOS DO REGISTRO ANTIGO
                Me.RN.DocumentoVidaWebId = Me.gvDocumentos.SelectedDataKey("documento_vida_web_id")
                Me.RN.TipoDocVidaWebId = Me.gvDocumentos.SelectedDataKey("tipo_doc_vida_web_id")
                Me.RN.Arquivo = v_sFileName
                'Me.RN.TpCorretor = Me.rblTipoCorretor.SelectedValue
                Me.RN.Usuario = MyBase.LoginWeb
                Me.RN.Alterar_Documento()

                'INCLUIR NOVO DOCUMENTO
                Me.RN.TipoDocVidaWebId = Me.gvDocumentos.SelectedDataKey("tipo_doc_vida_web_id")
                Me.RN.Arquivo = v_sFileName
                Me.RN.TpCorretor = Me.rblTipoCorretor.SelectedValue
                Me.RN.Usuario = MyBase.LoginWeb
                Me.RN.Incluir_Documento()

                v_bSalvo = True

            End If
        Else
            If Me.gvDocumentos.SelectedIndex = -1 Then
                'INCLUIR
                Dim v_sFileName As String = FileUpload1.FileName.ToString()
                'FileUpload1.SaveAs(v_oDtParametros.Rows(0)("Valor") & "\" & FileUpload1.FileName.ToString())

                Try
                    FileUpload1.SaveAs(v_sCaminho & "\" & v_sFileName)
                    'FileUpload1.SaveAs(Session("Caminho") & "\" & FileUpload1.FileName.ToString())
                    v_bSalvo = True
                Catch ex As IO.IOException
                    v_bSalvo = False
                    Me.HelperSEGW.GerarAlert(Me.Page, "N�o foi possivel salvar arquivo", "erro")
                    Throw ex
                Catch ex As Exception
                    Throw ex
                End Try

                If v_bSalvo Then
                    Dim v_oClsDocumentoVidaWebRN As New ClsDocumentoVidaWebRN
                    v_oClsDocumentoVidaWebRN.TipoDocVidaWebId = Me.ddlTipoDoc.SelectedValue
                    v_oClsDocumentoVidaWebRN.Arquivo = v_sFileName
                    v_oClsDocumentoVidaWebRN.TpCorretor = Me.rblTipoCorretor.SelectedValue
                    v_oClsDocumentoVidaWebRN.Usuario = MyBase.LoginWeb
                    v_oClsDocumentoVidaWebRN.Incluir_Documento()
                End If
            Else
                'ALTERAR
                Me.RN.DAO.IniciarTransacao(Me)
                Try
                    Dim v_sFileName As String = Me.gvDocumentos.Rows(Me.gvDocumentos.SelectedIndex).Cells(2).Text
                    Dim v_sFileUpload As String = FileUpload1.FileName.ToString()
                    Dim v_sFileNameNovo As String = v_sFileName.Split(".")(0)
                    v_sFileNameNovo &= "_" & Now.Day.ToString() & Now.Month.ToString() & Now.Year.ToString()
                    v_sFileNameNovo &= "_" & DateAndTime.TimeString.Replace(":", "")
                    v_sFileNameNovo &= "." & v_sFileName.Split(".")(1)

                    'ALTERAR DADOS DO REGISTRO ANTIGO
                    Me.RN.DocumentoVidaWebId = Me.gvDocumentos.SelectedDataKey("documento_vida_web_id")
                    Me.RN.TipoDocVidaWebId = Me.gvDocumentos.SelectedDataKey("tipo_doc_vida_web_id")
                    Me.RN.Arquivo = v_sFileNameNovo
                    Me.RN.Usuario = MyBase.LoginWeb
                    Me.RN.Alterar_Documento()

                    'INCLUIR NOVO DOCUMENTO
                    Me.RN.TipoDocVidaWebId = Me.gvDocumentos.SelectedDataKey("tipo_doc_vida_web_id")
                    Me.RN.Arquivo = v_sFileUpload
                    Me.RN.TpCorretor = Me.rblTipoCorretor.SelectedValue
                    Me.RN.Usuario = MyBase.LoginWeb
                    Me.RN.Incluir_Documento()

                    If File.Exists(v_sCaminho & "\" & v_sFileName) Then
                        Try
                            File.Move(v_sCaminho & "\" & v_sFileName, v_sCaminho & "\" & v_sFileNameNovo)
                        Catch ex As IO.IOException
                            Me.HelperSEGW.GerarAlert(Me.Page, "N�o foi possivel renomear o antigo arquivo.", "erro")
                            Throw ex
                        Catch ex As Exception
                            Throw ex
                        End Try
                    End If

                    Try
                        FileUpload1.SaveAs(v_sCaminho & "\" & v_sFileUpload)
                        v_bSalvo = True
                    Catch ex As IO.IOException
                        v_bSalvo = False
                        Me.HelperSEGW.GerarAlert(Me.Page, "N�o foi possivel salvar arquivo.", "erro")
                        Throw ex
                    Catch ex As Exception
                        Throw ex
                    End Try

                    Me.RN.DAO.FinalizarTransacao(True, Me)
                Catch ex As Exception
                    Me.RN.DAO.FinalizarTransacao(False, Me)
                    v_bSalvo = False
                    Throw ex
                End Try
            End If
        End If

        If v_bSalvo Then

            Me.gvDocumentos.DataBind()
            Me.ddlTipoDoc.DataBind()
            Limpar()
            Me.MultiView1.SetActiveView(Me.View1)
            Me.HelperSEGW.GerarAlert(Me.Page, "Enviado com sucesso.", "sucesso")
        End If
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Limpar()
        Me.btnAlterar.Enabled = False
        Me.MultiView1.SetActiveView(Me.View1)
    End Sub
#End Region

#Region " Eventos GridView "

    Private Sub gvDocumentos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDocumentos.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Me.HelperSEGW.CarregarTrocaCorLinhaGridView(e, , HelperSEGW.EnumTipoCursor.Hand)
            e.Row.Attributes.Add("onClick", ClientScript.GetPostBackEventReference(Me.gvDocumentos, "Select$" + e.Row.RowIndex.ToString()))

            DirectCast(e.Row.FindControl("lblCaminho"), Label).Text = Session("Caminho")
        End If
    End Sub

    Private Sub gvDocumentos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDocumentos.SelectedIndexChanged
        If gvDocumentos.SelectedIndex <> -1 Then
            Me.btnAlterar.Enabled = True
        End If
    End Sub

#End Region

    Private Sub Limpar()
        Me.FileUpload1 = Nothing
        Me.gvDocumentos.SelectedIndex = -1
        Me.lblArquivo.Text = ""
        Me.lblTipoDoc.Text = ""
        Me.rblTipoCorretor.SelectedIndex = 0
        Me.btnAlterar.Enabled = False
    End Sub
End Class