﻿Imports SEGL0343.Task
Imports SEGL0343.EO
Imports System.Collections.Generic
Imports System.Text

Partial Public Class SEGW0158Evento
    Inherits BasePages

    Private objSource As List(Of SEGS8705_SPS_EO_OUT)
    Private taskSolicitante As New SEGW0157Solicitante_TK

#Region "Propriedades"

    Private iEvento As Integer
    Public sProduto As Integer

    Private Property EVENTO() As Integer
        Get
            Return iEvento
        End Get
        Set(ByVal value As Integer)
            iEvento = value
        End Set
    End Property

    Private Property Produto() As Integer
        Get
            Return sProduto
        End Get
        Set(ByVal value As Integer)
            sProduto = value
        End Set
    End Property

    Private ReadOnly Property EVENTO_SINISTRO_PERIODICO() As Integer
        Get
            Return 2
        End Get
    End Property

    Private ReadOnly Property TIPO_RAMO() As Integer
        Get
            Return 2
        End Get
    End Property

    Public ReadOnly Property Cliente_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("Cliente_ID")) Then
                Return Request.QueryString("Cliente_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property Sinistro_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("SINISTRO_ID")) Then
                Return Request.QueryString("SINISTRO_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property PS() As Integer
        Get
            If Not IsNothing(Request.QueryString("PS")) Then
                Return Request.QueryString("PS")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Private ReadOnly Property EVENTO_SINISTRO_PONTUAL() As Integer
        Get
            Return 1
        End Get
    End Property

    Private Property SourceMunicipio() As List(Of SEGS8707_SPS_EO_OUT)
        Get
            Return CType(ViewState("SourceMunicipio"), List(Of SEGS8707_SPS_EO_OUT))
        End Get
        Set(ByVal value As List(Of SEGS8707_SPS_EO_OUT))
            ViewState("SourceMunicipio") = value
        End Set
    End Property

    Private Property SourceUF() As List(Of SEGS8708_SPS_EO_OUT)
        Get
            Return CType(ViewState("SourceUF"), List(Of SEGS8708_SPS_EO_OUT))
        End Get
        Set(ByVal value As List(Of SEGS8708_SPS_EO_OUT))
            ViewState("SourceUF") = value
        End Set
    End Property

    Public ReadOnly Property Evento_ID() As Integer
        Get
            Return Me.cboEvento.SelectedValue
        End Get
    End Property
    Public ReadOnly Property SubEvento_ID() As Integer
        Get
            Return IIf(Me.cboSubEvento.SelectedValue = "", 0, Me.cboSubEvento.SelectedValue)
        End Get
    End Property
    Public ReadOnly Property sProposta() As Boolean
        Get
            If Not IsNothing(Request.QueryString("sProposta")) Then
                Return Request.QueryString("sProposta")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property CPF_CNPJ() As String
        Get
            If Not IsNothing(Request.QueryString("CPF_CNPJ")) Then
                Return Request.QueryString("CPF_CNPJ")
            Else
                Return Nothing
            End If
        End Get
    End Property


#End Region

#Region "Eventos"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim objAux As New SEGW0157RegistraOcorrencia_passo2_TK()
            Me.txtDataSistema.Value = CDate(objAux.BuscaDataOperacional()).ToString("dd/MM/yyyy").Trim 'objAux.BuscaDataOperacional()

            If IsNothing(Session("DadosCliente")) Then
                AjustaNavegacao("Evento")
            Else
                AjustaNavegacao("DadosCliente")
            End If

            LkbEvento.Text = " ► Evento"
            LkbEvento.ForeColor = Drawing.Color.Blue

            If Not Page.IsPostBack Then
                txtVistoriaCEP.Focus()
                Me.CarregaComboTipoTelefone()
                'Me.CarregarMunicipio()
                Me.cboVistoriaMunicipio.Items.Insert(0, New ListItem("Escolha o município", ""))

                Me.CarregarUF()
                Me.CarregarComboEventos()

                Me.txtDescrEvento.Attributes.Add("onkeydown", "javascript:valMax(this,700);")
                Me.txtDescrEvento.Attributes.Add("onkeyup", "javascript:valMax(this,700);")
                Me.txtDescrEvento.Attributes.Add("onpaste", "javascript:valMax(this,700);")

                If Me.Sinistro_ID > 0 Then
                    Dim TaskPassosSinistro As New SEGW158SinistroInserirPassosDanos_TK
                    Dim EO_IN As New SEGS8744_SPS_EO_IN
                    Dim Eo_out As List(Of SEGS8744_SPS_EO_OUT)

                    EO_IN.Sinistro_Id = Me.Sinistro_ID

                    Eo_out = TaskPassosSinistro.Consultar_Passo_Danos(EO_IN)


                    cboEvento.SelectedValue = Eo_out.Item(0).codigo_evento
                    If IsNothing(Eo_out.Item(0).data_ocorrencia) Then
                        txtDataIniOcorrencia.Text = ""
                    Else
                        txtDataIniOcorrencia.Text = Eo_out.Item(0).data_ocorrencia
                    End If
                    txtHoraOcorrencia.Text = Eo_out.Item(0).hora_ocorrencia
                    If Eo_out.Item(0).sub_evento <> 0 Then
                        Me.CarregarComboSubEventos()
                        cboSubEvento.SelectedValue = Eo_out.Item(0).sub_evento
                    End If
                    txtDescrEvento.Text = Eo_out.Item(0).descricao_evento

                    If Eo_out.Item(0).contato_vistoria <> "" Then
                        txtVistoriaNome.Text = Eo_out.Item(0).contato_vistoria
                    Else

                    End If

                    txtVistoriaEndereco.Text = Eo_out.Item(0).endereco_vistoria
                    txtVistoriaBairro.Text = Eo_out.Item(0).bairro_vistoria
                    txtVistoriaCEP.Text = Eo_out.Item(0).cep_vistoria
                    txtVistoriaDDD.Text = Eo_out.Item(0).ddd_fone_vistoria
                    txtVistoriaTelefone.Text = Eo_out.Item(0).fone_vistoria
                    If Not String.IsNullOrEmpty(Eo_out.Item(0).tipo_fone_vistoria) Then
                        cboVistoriaTipoTelefone.Text = RetornaCodTipoTel(Eo_out.Item(0).tipo_fone_vistoria)
                    End If
                    txtVistoriaDDD2.Text = Eo_out.Item(0).ddd_fone2_vistoria
                    txtVistoriaTelefone2.Text = Eo_out.Item(0).fone2_vistoria
                    If Not String.IsNullOrEmpty(Eo_out.Item(0).tipo_fone2_vistoria) Then
                        cboVistoriaTipoTelefone2.Text = RetornaCodTipoTel(Eo_out.Item(0).tipo_fone2_vistoria)
                    End If
                    cboVistoriaUF.Text = Eo_out.Item(0).uf_vistoria
                    PreencheComboMunicipio(Me.cboVistoriaUF.SelectedItem.Text)
                    If Not String.IsNullOrEmpty(Eo_out.Item(0).municipio_vistoria) Then
                        Dim objEoIn_SEGS8927 As New SEGS8927_SPS_EO_IN
                        Dim objEoBus_SEGS8927 As New SEGL0343.Business.SEGS8927_SPS_Business
                        Dim lstObjEoOut_SEGS8927 As New List(Of SEGS8927_SPS_EO_OUT)
                        objEoIn_SEGS8927.Uf = Eo_out.Item(0).uf_vistoria
                        objEoIn_SEGS8927.Municipio = Eo_out.Item(0).municipio_vistoria

                        lstObjEoOut_SEGS8927 = objEoBus_SEGS8927.seleciona(objEoIn_SEGS8927)
                        If lstObjEoOut_SEGS8927.Count > 0 Then
                            cboVistoriaMunicipio.Text = lstObjEoOut_SEGS8927.Item(0).municipio_id 'Eo_out.Item(0).municipio_vistoria
                        End If

                    End If

                End If
            End If
        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro Ao Processar Load do formulario de Eventos " & ex.Message.ToString & "')", True)
        End Try
    End Sub

    Public Function GravarPasso2() As Boolean

        Try
            Dim aux As Date = CDate(txtDataIniOcorrencia.Text)
        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Data de ocorrência inválida');", True)
            txtDataIniOcorrencia.Focus()
            Return False
        End Try
        Try

            Dim objOut_8744 As New List(Of SEGS8744_SPS_EO_OUT)
            Dim objIn_8744 As New SEGS8744_SPS_EO_IN
            Dim PassoFinal As Boolean = False

            Dim objGeral As New SEGW0158Geral_TK()

            objIn_8744.Sinistro_Id = Me.Sinistro_ID

            objOut_8744 = objGeral.Buscar(objIn_8744)

            Dim objIn_8747 As New SEGS8747_SPU_EO_IN()

            If objOut_8744.Count <> 0 Then
                objIn_8747 = objGeral.PreencheTodosCamposU(objOut_8744(0))
            End If

            objIn_8747.Codigo_Evento = Me.cboEvento.SelectedValue
            objIn_8747.Codigo_Passo = 1


            objIn_8747.Usuario = Usuario.LoginWeb


            objIn_8747.Sinistro_Id = Sinistro_ID
            objIn_8747.Codigo_Evento = cboEvento.SelectedValue

            objIn_8747.Data_Ocorrencia = CDate(txtDataIniOcorrencia.Text)
            objIn_8747.Hora_Ocorrencia = txtHoraOcorrencia.Text.Trim
            If Not String.IsNullOrEmpty(Me.cboSubEvento.SelectedValue) Then
                objIn_8747.Sub_Evento = cboSubEvento.SelectedValue
            End If
            objIn_8747.Descricao_Evento = txtDescrEvento.Text.Trim
            objIn_8747.Contato_Vistoria = Me.txtVistoriaNome.Text.Trim
            objIn_8747.Endereco_Vistoria = txtVistoriaEndereco.Text.Trim
            objIn_8747.Bairro_Vistoria = Me.txtVistoriaBairro.Text.Trim
            objIn_8747.Cep_Vistoria = Me.txtVistoriaCEP.Text.Trim
            objIn_8747.Ddd_Fone_Vistoria = Me.txtVistoriaDDD.Text.Trim
            objIn_8747.Fone_Vistoria = Me.txtVistoriaTelefone.Text.Trim
            objIn_8747.Tipo_Fone_Vistoria = Me.cboVistoriaTipoTelefone.SelectedItem.Text.Trim
            objIn_8747.Ddd_Fone2_Vistoria = Me.txtVistoriaDDD2.Text.Trim
            objIn_8747.Fone2_Vistoria = Me.txtVistoriaTelefone2.Text.Trim
            objIn_8747.Tipo_Fone2_Vistoria = Me.cboVistoriaTipoTelefone2.SelectedItem.Text.Trim
            objIn_8747.Uf_Vistoria = Me.cboVistoriaUF.SelectedItem.Text.Trim
            objIn_8747.Municipio_Vistoria = Me.cboVistoriaMunicipio.SelectedItem.Text.Trim
            objIn_8747.Evento = cboEvento.SelectedItem.Text
            'objIn_8747.Cliente_Id = Me.Cliente_ID

            objGeral.Atualizar(objIn_8747)

            Return True
        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Data de ocorrência inválida');", True)
            Return False
        End Try
    End Function

    Protected Sub btnContinuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinuar.Click
        If Me.GravarPasso2() Then

            If Me.Sinistro_ID > 0 Then

                Dim objGeral As New SEGW0158Geral_TK
                objGeral.AtualizaPasso(Sinistro_ID, 2)

                Dim sb As New StringBuilder()
                sb.Append("SEGW0158DadosSolicitante.aspx")
                sb.Append("?SINISTRO_ID=" & Me.Sinistro_ID)
                sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

                Response.Redirect(sb.ToString())
            End If
        End If

    End Sub

#End Region

#Region "Métodos"

    Public Function ValidaCamposFormulario() As String
        Dim strErros As New StringBuilder
        Try
            If cboEvento.SelectedItem.Text.Trim = String.Empty Then
                strErros.Append("Indique o evento causador da ocorrência.\n")
                Return strErros.ToString
                Exit Function
            End If

            If txtDataIniOcorrencia.Text.Trim = String.Empty Then
                strErros.Append("Indique a Data de Ocorrência do Sinistro.\n")
                Return strErros.ToString
                Exit Function
            End If
            ValidarDataOcorrencia()

            If txtVistoriaNome.Text.Trim = String.Empty Then
                strErros.Append("Indique o contato da vistoria.\n")
                Return strErros.ToString
                Exit Function
            End If

            If txtVistoriaEndereco.Text.Trim = String.Empty Then
                strErros.Append("Indique o Endereço da vistoria.\n")
                Return strErros.ToString
                Exit Function
            End If

            If txtVistoriaBairro.Text = String.Empty Then
                strErros.Append("Indique o Bairro da vistoria.\n")
                Return strErros.ToString
                Exit Function
            End If

            If Me.cboVistoriaTipoTelefone.SelectedItem.Text = String.Empty Or Me.cboVistoriaTipoTelefone.SelectedItem.Text = "-Selecione-" Then
                strErros.Append("Indique o tipo de telefone para contato.\n")
                Return strErros.ToString
                Exit Function
            End If

            Return strErros.ToString

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
            Return strErros.ToString
        End Try
    End Function

    Public Sub CarregarMunicipio()
        Try
            If IsNothing(Me.SourceMunicipio) Then
                Dim ObjEO_IN As New SEGS8707_SPS_EO_IN

                Me.SourceMunicipio = Me.taskSolicitante.CarregaMunicipio(ObjEO_IN)

                If Not IsNothing(Me.SourceMunicipio) Then
                    Me.cboVistoriaMunicipio.DataSource = Me.SourceMunicipio
                    Me.cboVistoriaMunicipio.DataTextField = "NOME"
                    Me.cboVistoriaMunicipio.DataValueField = "MUNICIPIO_ID"
                    Me.cboVistoriaMunicipio.DataBind()
                    cboVistoriaMunicipio.Items.Insert(0, New ListItem("Escolha o município", ""))
                    cboVistoriaMunicipio.SelectedValue = ""
                End If
            Else
                Exit Sub
            End If

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Public Sub CarregarUF()
        Try
            If IsNothing(Me.SourceUF) Then

                Dim ObjEO_IN As New SEGS8708_SPS_EO_IN

                Me.SourceUF = Me.taskSolicitante.CarregaUF(ObjEO_IN)

                If Not IsNothing(Me.SourceUF) Then
                    Me.cboVistoriaUF.DataSource = Me.SourceUF
                    Me.cboVistoriaUF.DataTextField = "NOME"
                    Me.cboVistoriaUF.DataValueField = "NOME"
                    Me.cboVistoriaUF.DataBind()
                End If

            Else
                Exit Sub
            End If

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Public Sub CarregarComboEventos()
        Try
            Dim TaskEvento As New SEGW0158ConsultaEventos
            Dim EO_IN As New SEGS8714_SPS_EO_IN

            EO_IN.Tipo_Ramo = Me.TIPO_RAMO

            Me.cboEvento.DataSource = TaskEvento.ConsultarEventos(EO_IN)
            Me.cboEvento.DataTextField = "NOME"
            Me.cboEvento.DataValueField = "EVENTO_SINISTRO_ID"
            Me.cboEvento.DataBind()
            Me.cboEvento.Items.Insert(0, New ListItem(" ", ""))

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Public Sub CarregarComboSubEventos()
        Try
            Dim TaskEvento As New SEGW0158ConsultaEventos
            Dim EO_IN As New SEGS8715_SPS_EO_IN
            EO_IN.Evento_Sinistro_Id = Me.cboEvento.SelectedValue

            Me.cboSubEvento.DataSource = TaskEvento.ConsultarSubEventos(EO_IN)
            Me.cboSubEvento.DataTextField = "NOME"
            Me.cboSubEvento.DataValueField = "SUBEVENTO_SINISTRO_ID"
            Me.cboSubEvento.DataBind()

            If CType(Me.cboSubEvento.DataSource, List(Of SEGS8715_SPS_EO_OUT)).Count > 0 Then
                Me.cboSubEvento.Enabled = True
            Else
                Me.cboSubEvento.Enabled = False
            End If

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Public Sub CarregaTipoTel(ByRef cmbTelefone As DropDownList)
        Try
            With cmbTelefone
                .Items.Add(New ListItem("-Selecione-", "0"))
                .Items.Add(New ListItem("Residencial", "1"))
                .Items.Add(New ListItem("Comercial", "2"))
                .Items.Add(New ListItem("Celular", "3"))
                .Items.Add(New ListItem("Fax", "4"))
                .Items.Add(New ListItem("Recado", "5"))
                .SelectedIndex = 0
            End With

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Private Function RetornaCodTipoTel(ByVal Descricao As String)
        Try
            Select Case Descricao
                Case "-Selecione-"
                    RetornaCodTipoTel = "0"
                Case "Residencial"
                    RetornaCodTipoTel = "1"
                Case "Comercial"
                    RetornaCodTipoTel = "2"
                Case "Celular"
                    RetornaCodTipoTel = "3"
                Case "Fax"
                    RetornaCodTipoTel = "4"
                Case "Recado"
                    RetornaCodTipoTel = "5"
            End Select

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Function

    Private Sub CarregaComboTipoTelefone()
        Try
            CarregaTipoTel(Me.cboVistoriaTipoTelefone)
            CarregaTipoTel(Me.cboVistoriaTipoTelefone2)

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

#End Region

    Public Sub AjustaNavegacao(ByVal But As String)
        Select Case But

            Case "Evento"
                LkbPesquisa.Enabled = True
            Case "LkbSolicitante"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
            Case "LkbAgencia"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
            Case "LkbSinistroCA"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
            Case "LkbObjsegurado"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
            Case "LkbOutrosSeguros"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
                LkbObjsegurado.Enabled = True
            Case "Ressumo"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
                LkbObjsegurado.Enabled = True
            Case "DadosCliente"
                LkbPesquisa.Enabled = True
                LkbDadosCliente.Enabled = True
        End Select
    End Sub

    Protected Sub cboEvento_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Me.CarregarComboSubEventos()
            Me.txtDataIniOcorrencia.Focus()
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub btnVoltar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoltar.Click

        Dim sb As New StringBuilder()
        sb.Append("SEGW0158ConsultaSinistrado.aspx")
        sb.Append("?Cliente_ID=" & Cliente_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())

    End Sub

    Private Sub PreencheComboMunicipio()
        Dim Obj_TK As New UCAgencia_TK()
        Dim Obj_EO_IN As New SEL_MUNICIPIO_SPS_EO_IN()
        Try
            With Obj_EO_IN
                .Estado = Me.cboVistoriaUF.SelectedValue
                .Municipio = String.Empty
                .Municipio_Id = String.Empty
            End With

            With cboVistoriaMunicipio
                .DataSource = Obj_TK.ObtemListaMunicipio(Obj_EO_IN)
                .DataTextField = "nome"
                .DataValueField = "municipio_id"
                .DataBind()

                .Items.Insert(0, New ListItem("Escolha o município", ""))
            End With

            cboVistoriaMunicipio.Focus()

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub cboVistoriaUF_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVistoriaUF.SelectedIndexChanged
        Try
            If cboVistoriaUF.SelectedValue <> String.Empty Then
                PreencheComboMunicipio(cboVistoriaUF.Text)
            End If

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click
        Try
            Me.GravarPasso2()

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub ibtPesquisarCEP_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtPesquisarCEP.Click
        Try
            If Me.txtVistoriaCEP.Text <> String.Empty Then
                BindEnderecoObjSolicitante()
                txtVistoriaNome.Focus()
            End If
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Private Sub BindEnderecoObjSolicitante()
        'base dos correios
        Dim Obj_TK As New UCDadosSolicitanteVida_TK()
        Dim Obj_EO_IN As New SEGS8839_SPS_EO_IN()
        Dim Obj_EO_OUT As New SEGS8839_SPS_EO_OUT()
        'base interna
        Dim municipio As New municipio_spi_EO_IN
        Dim lista_interna_in As New SEL_MUNICIPIO_SPS_EO_IN
        Dim lista_interna_out As New List(Of SEL_MUNICIPIO_SPS_EO_OUT)
        'atualização
        Dim atualiza As New municipio_spu_EO_IN
        Try
            Obj_EO_IN.Cep = txtVistoriaCEP.Text.Replace("-", "")
            Obj_EO_OUT = Obj_TK.ObtemEnderecoObjSegurado(Obj_EO_IN)
            'verifica se o municipio está na tabela dos correios
            If Not IsNothing(Obj_EO_OUT) Then
                Me.txtVistoriaBairro.Text = Obj_EO_OUT.Bairro.Trim
                Me.txtVistoriaCEP.Text = Obj_EO_OUT.CEP.Trim
                Me.txtVistoriaEndereco.Text = Obj_EO_OUT.Endereco.Trim
                Me.cboVistoriaUF.Text = Obj_EO_OUT.UF.Trim

                PreencheComboMunicipio(Me.cboVistoriaUF.SelectedItem.Text)

                cboVistoriaMunicipio.Text = Obj_EO_OUT.Municipio_ID.ToString.Trim

            End If
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Public Sub PreencheComboMunicipio(ByVal UF As String)
        Dim Obj_TK As New UCDadosSolicitanteVida_TK()
        Dim Obj_EO_IN As New SEL_MUNICIPIO_SPS_EO_IN()
        Try
            Obj_EO_IN.Estado = UF
            Obj_EO_IN.Municipio = String.Empty
            Obj_EO_IN.Municipio_Id = String.Empty

            cboVistoriaMunicipio.DataSource = Obj_TK.ObtemListaMunicipio(Obj_EO_IN)
            cboVistoriaMunicipio.DataTextField = "nome"
            cboVistoriaMunicipio.DataValueField = "municipio_id"
            cboVistoriaMunicipio.DataBind()

            cboVistoriaMunicipio.Items.Insert(0, New ListItem("Escolha o município", ""))
            Me.cboVistoriaMunicipio.SelectedIndex = 0
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Private Sub ValidarDataOcorrencia()
        Try

            Dim objAux As New SEGW0157RegistraOcorrencia_passo2_TK()
            Dim dataAtual As Date = objAux.BuscaDataOperacional()

            Dim x As Integer

            x = DateDiff(DateInterval.Day, CDate(Me.txtDataIniOcorrencia.Text), dataAtual)

            If x < 0 Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Data de Ocorrência Posterior a Data Operacional (" & dataAtual.ToString("dd/MM/yyyy").Trim & ").');", True)
                Exit Sub
            End If

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub cboVistoriaTipoTelefone_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.txtVistoriaDDD2.Focus()
    End Sub

    Protected Sub cboVistoriaTipoTelefone2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.btnContinuar.Focus()
    End Sub

    Protected Sub LkbPesquisa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbPesquisa.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158ConsultaSinistrado.aspx")
        sb.Append("?Cliente_ID=" & Cliente_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())
    End Sub

    Protected Sub LkbDadosCliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbDadosCliente.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158DadosCliente.aspx")
        sb.Append("?SINISTRO_ID=" & Me.Sinistro_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())
    End Sub
End Class