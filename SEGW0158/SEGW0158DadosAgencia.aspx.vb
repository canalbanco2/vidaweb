﻿Imports SEGL0343.Task
Imports SEGL0343.EO
Imports SEGL0343.Business

Partial Public Class SEGW0158DadosAgencia
    Inherits BasePages
    Dim objOut_8744 As New SEGS8744_SPS_EO_OUT
    Dim objIn_8744 As New SEGS8744_SPS_EO_IN

#Region "Propriedades"

    Public ReadOnly Property SINISTRO_ID() As String
        Get
            If Not IsNothing(Request.QueryString("SINISTRO_ID")) Then
                Return Request.QueryString("SINISTRO_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

#End Region

#Region "Metodos"

    Private Sub Voltar()
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158DadosSolicitante.aspx")
        sb.Append("?SINISTRO_ID=" + Me.SINISTRO_ID)
        sb.Append("&SLinkSeguro=" + Request("SLinkSeguro"))
        Response.Redirect(sb.ToString())
    End Sub

    Private Sub Avancar()
        Dim objGeral As New SEGW0158Geral_TK
        objGeral.AtualizaPasso(Me.SINISTRO_ID, 4)

        Dim sb As New StringBuilder()
        sb.Append("SEGW0158AnaliseSinistroCA.aspx")
        sb.Append("?SINISTRO_ID=" + Me.SINISTRO_ID)
        sb.Append("&PS=5")
        sb.Append("&SLinkSeguro=" + Request("SLinkSeguro"))
        Response.Redirect(sb.ToString())
    End Sub

    Private Function Gravar() As Boolean

        Try
            Dim PassoFinal As Boolean = False

            Dim objGeral As New SEGW0158Geral_TK()

            objIn_8744.Sinistro_Id = Me.SINISTRO_ID

            objOut_8744 = objGeral.Buscar(objIn_8744)(0)

            If Not IsNothing(objOut_8744) Then
                If Me.DadosAgencia1.CodigoAgenciaVw = "" Then
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('É preciso especificar a agência de contato!')", True)
                    Return False
                End If
                Dim objIn_8747 As New SEGS8747_SPU_EO_IN()

                objIn_8747 = objGeral.PreencheTodosCamposU(objOut_8744)

                With objIn_8747
                    .Codigo_Passo = 3
                    .Nome_Agencia = Me.DadosAgencia1.NomeAgencia
                    .Codigo_Agencia = Me.DadosAgencia1.CodigoAgenciaVw
                    .Codigo_Dv_Agencia = Me.DadosAgencia1.DV
                    .Endereco_Agencia = Me.DadosAgencia1.Endereco
                    .Bairro_Agencia = Me.DadosAgencia1.Bairro
                    .Uf_Agencia = Me.DadosAgencia1.UFtxt
                    .Municipio_Agencia = Decimal.Parse(Me.DadosAgencia1.Cidade)
                    .Municipio_Agencia = Me.DadosAgencia1.Cidade_Nome
                End With
                objGeral.Atualizar(objIn_8747)
                Return True
            Else
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Dados de Sinistros não encontrados')", True)
            End If
        Catch
            Return False
        End Try
    End Function

    Private Sub RedirecionaContinuar(ByVal pCaminho As String, ByVal pSINISTRO_ID As String)
        Response.Redirect(pCaminho + "?SINISTRO_ID=" + pSINISTRO_ID)
    End Sub

#End Region

#Region "Eventos"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                CarregarDados()
                form1.DefaultButton = "DadosAgencia1$ibtPesquisarCodigoAgencia"
            End If
        Catch ex As Exception
            '   ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Ocorreu um erro no sistema e foi reportado, Reinicie o proceso ou aguarde correção.');", True)
            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

        If IsNothing(Session("DadosCliente")) Then
            AjustaNavegacao("LkbAgencia")
        Else
            AjustaNavegacao("DadosCliente")
        End If
        LkbAgencia.Text = " ► Agência"
        LkbAgencia.ForeColor = Drawing.Color.Blue
    End Sub
    Public Sub AjustaNavegacao(ByVal But As String)
        Select Case But
            Case "Evento"
                LkbPesquisa.Enabled = True
            Case "LkbSolicitante"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
            Case "LkbAgencia"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
            Case "LkbSinistroCA"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
            Case "LkbObjsegurado"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
            Case "LkbOutrosSeguros"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
                LkbObjsegurado.Enabled = True
            Case "Ressumo"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
                LkbObjsegurado.Enabled = True
            Case "DadosCliente"
                LkbDadosCliente.Enabled = True
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
        End Select
    End Sub

    Private Sub CarregarDados()
        Try
            Dim objGeral As New SEGW0158Geral_TK()

            objIn_8744.Sinistro_Id = Me.SINISTRO_ID

            If objGeral.Buscar(objIn_8744).Count > 0 Then
                objOut_8744 = objGeral.Buscar(objIn_8744)(0)
                PreencheCampos(objOut_8744)
            End If
        Catch ex As Exception
            '  ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Ocorreu um erro no sistema e foi reportado, Reinicie o proceso ou aguarde correção.');", True)
            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Private Sub PreencheCampos(ByVal objOut_8744 As SEGS8744_SPS_EO_OUT)
        Try
            Me.DadosAgencia1.NomeAgencia = objOut_8744.nome_agencia
            Me.DadosAgencia1.CodigoAgenciaVw = objOut_8744.codigo_agencia
            Me.DadosAgencia1.DV = objOut_8744.codigo_dv_agencia
            Me.DadosAgencia1.Endereco = objOut_8744.endereco_agencia
            Me.DadosAgencia1.Bairro = objOut_8744.bairro_agencia
            Me.DadosAgencia1.UFtxt = objOut_8744.uf_agencia
            Me.DadosAgencia1.Cidade_Nome = objOut_8744.municipio_agencia

            Me.DadosAgencia1.CodigoAgencia = objOut_8744.codigo_agencia
        Catch ex As Exception
            ' ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Ocorreu um erro no sistema e foi reportado, Reinicie o proceso ou aguarde correção.');", True)
            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click
        Try
            If Me.Gravar() Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Dados salvos com sucesso!')", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Erro ao tentar salvar passos!')", True)
            End If
        Catch ex As Exception
            '   ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Ocorreu um erro no sistema e foi reportado, Reinicie o proceso ou aguarde correção.');", True)
            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub btnContinuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinuar.Click
        If Me.Gravar() Then
            Me.Avancar()
        Else
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Erro ao tentar salvar passos!')", True)
        End If
    End Sub

    Protected Sub btnVoltar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoltar.Click
        Try
            Me.Voltar()
        Catch ex As Exception
            '     ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Ocorreu um erro no sistema e foi reportado, Reinicie o proceso ou aguarde correção.');", True)
            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

#End Region


    Protected Sub LkbPesquisa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbPesquisa.Click, LkbAgencia.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158ConsultaSinistrado.aspx")
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())
    End Sub

    Protected Sub LkbEvento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbEvento.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158Evento.aspx")
        sb.Append("?SINISTRO_ID=" & Me.SINISTRO_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))
        Response.Redirect(sb.ToString())
    End Sub

    Protected Sub LkbSolicitante_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbSolicitante.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158DadosSolicitante.aspx")
        sb.Append("?SINISTRO_ID=" + Me.SINISTRO_ID)
        sb.Append("&SLinkSeguro=" + Request("SLinkSeguro"))
        Response.Redirect(sb.ToString())
    End Sub
    Protected Sub LkbDadosCliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbDadosCliente.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158DadosCliente.aspx")
        sb.Append("?SINISTRO_ID=" & Me.Sinistro_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())
    End Sub
End Class