<%@ Page Language="vb" EnableViewStateMac="false" AutoEventWireup="false" Codebehind="SEGW0158AnaliseSinistroCA.aspx.vb"
    Inherits="SEGW0158.SEGW0158AnaliseSinistroCA" %>

<%@ Register Src="UserControls/UCCoberturaPropostas.ascx" TagName="UCCoberturaPropostas"
    TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SUSAB - Aviso de Sinistro RE - An�lise Sinistro CA</title>
    <link href="CSS/Sinistro.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src='<%= ResolveUrl("~/js/SEGW0158Scripts.js") %>'></script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="Div2" style="width: 900px; text-align: left; height: 60px">
            <div class="n900">
           <div class="n900">
                    <fieldset class="n100" style="text-align: center; background-color:   #fffff0;">
                        <asp:LinkButton ID="LkbPesquisa" Enabled="false" CssClass="BtnNavega" runat="server"
                           >Pausar Processo</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n100" style="text-align: center; ">
                        <asp:LinkButton ID="LkbDadosCliente" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Dados do Cliente</asp:LinkButton></fieldset>
                    <fieldset style="width: 70px; text-align: center; background-color:   #fffff0;">
                        <asp:LinkButton ID="LkbEvento" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Evento</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n110" style="text-align: center; background-color: #fffff0;">
                       <asp:LinkButton ID="LkbSolicitante" Enabled="false" CssClass="BtnNavega"
                            runat="server">Dados Solicitante</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n70" style=" text-align: center;background-color:  #fffff0;">
                        <asp:LinkButton ID="LkbAgencia" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Ag�ncia</asp:LinkButton></fieldset>
                    <fieldset class="n120" style=" text-align: center; background-color:  #F0E68C;">
                       <asp:LinkButton ID="LkbSinistroCA" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Analise de Sinistro</asp:LinkButton></fieldset>
                    <fieldset class="n100" style=" text-align: center;">
                       <asp:LinkButton ID="LkbObjsegurado" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Objeto Segurado</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n100" style=" text-align: center;">
                        <asp:LinkButton ID="LkbOutrosSeguros" Enabled="false" CssClass="BtnNavega"
                            runat="server">Outros Seguros</asp:LinkButton></fieldset>
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="width: 100px; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu " style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 80px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 130px; border: none; background: url(Image/menu_on_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 110px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 110px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <input type="hidden" id="flag_aviso" runat="server" />
        <input type="hidden" id="hfcomponente" runat="server" />
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="upFiltros" runat="server">
            <ContentTemplate>
                <fieldset style="width: 750px; text-align: center; height: 230px;">
                    <legend class="TitCaixa"><span>Propostas do Cliente</span></legend>
                    <div id="Panel" style="width: 750px; height: 230px">
                        <div class="n750 divLeft">
                            <div class="n250 divLeft">
                                <asp:Panel ID="Panel1" runat="server" Height="180px" ScrollBars="Both" Width="750px">
                                    <asp:GridView ID="GVwSinistrados" runat="server" AutoGenerateColumns="False" OnRowDataBound="GVwSinistrados_RowDataBound"
                                        OnSelectedIndexChanged="GVwSinistrados_SelectedIndexChanged" Width="1000px" CssClass="divLeft"
                                        PageSize="10000">
                                        <PagerStyle Font-Size="Large" CssClass="pager" />
                                        <SelectedRowStyle CssClass="txtTabela3" />
                                        <RowStyle CssClass="txtTabela" HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <Columns>
                                            <asp:CommandField ButtonType="Image" SelectImageUrl="~/image/icon_bseta_menu_unsel.gif"
                                                ShowSelectButton="True" ControlStyle-Width="13px"></asp:CommandField>
                                            <asp:BoundField HeaderText="Indicador" DataField="indicador" />
                                            <asp:BoundField HeaderText="Proposta ID" DataField="proposta_id" />
                                            <asp:BoundField HeaderText="proposta BB" DataField="proposta_bb" />
                                            <asp:BoundField HeaderText="Situa��o" DataField="situacao" />
                                            <asp:BoundField HeaderText="Produto ID" DataField="produto_id" />
                                            <asp:BoundField HeaderText="Produto" DataField="produto" ControlStyle-Width="350px" />
                                            <asp:BoundField HeaderText="Tipo Ramo ID" DataField="tp_ramo_id" />
                                            <asp:BoundField HeaderText="Data In�cio Vig�ncia" DataField="dt_inicio_vigencia" />
                                            <asp:BoundField HeaderText="Data Fim Vig�ncia" DataField="dt_fim_vigencia" />
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:HiddenField runat="server" ID="hdFlagReanalise" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            Cliente n�o localizado na Base
                                        </EmptyDataTemplate>
                                        <HeaderStyle CssClass="header_tabela" HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <AlternatingRowStyle CssClass="txtTabela2" />
                                    </asp:GridView>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset style="width: 750px; text-align: center; height: 100px;">
                    <legend class="TitCaixa"><span>Legenda</span></legend>
                    <div id="Div1">
                        <div class="n750">
                            <div class="n750 divLeft" style="padding-left: 10px;">
                                <table class="box" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td class="header_legenda">
                                            <strong>C�digo</strong></td>
                                        <td class="header_legenda">
                                            <strong>Descri��o</strong></td>
                                        <td class="header_legenda">
                                            <strong>C�digo</strong></td>
                                        <td class="header_legenda">
                                            <strong>Descri��o</strong></td>
                                    </tr>
                                    <tr class="itemLegenda">
                                        <td style="width: 100px">
                                            AVSR</td>
                                        <td>
                                            A Avisar</td>
                                        <td style="width: 100px">
                                            PSCB</td>
                                        <td style="width: 222px">
                                            N�o Avisar - Proposta sem cobertura</td>
                                    </tr>
                                    <tr class="itemLegenda  ">
                                        <td>
                                            RNLS</td>
                                        <td>
                                            Solicita��o de Rean�lise</td>
                                        <td>
                                            NAVS</td>
                                        <td>
                                            N�o Avisar</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="n790 divLeft">
                    <div class="n250 divLeft">
                        &nbsp;</div>
                    <asp:HiddenField ID="hfEvento" runat="server" />
                    <asp:HiddenField ID="hfNrVrsEndosso" runat="server" />
                    <asp:HiddenField ID="hfNRCTRSGROBB" runat="server" />
                    <asp:HiddenField ID="hfDtOcorrencia" runat="server" />
                    <asp:HiddenField ID="hfSubEvento" runat="server" />
                    <asp:HiddenField ID="hfSinistro_bb_aux" runat="server" />
                    <asp:HiddenField ID="hfsinistroid_aux" runat="server" />
                    <asp:HiddenField ID="hfDataocorrencia" runat="server" />
                    <asp:HiddenField ID="hfTxtEvento" runat="server" />
                    <asp:HiddenField ID="hfTxtSubEvt" runat="server" />
                </div>
                <br />
                <br />
                <fieldset style="width: 900px; text-align: center; height: auto; border: none;">
                    <div id="Div3" style="width: 900px;">
                        <div class="n900">
                            <div class="n900 divLeft">
                                <div class="BotaoFora n300 divLeft alignLeft">
                                    <div class="BotaoE">
                                    </div>
                                    <asp:Button ID="btnAvisar" runat="server" CssClass="BotaoM n120" Text="Avisar" Visible="true"
                                        Width="120px" ValidationGroup="Avisos_validate" />
                                    <div class="BotaoD">
                                    </div>
                                </div>
                                <div class="BotaoFora n300 divLeft alignLeft">
                                    <div class="BotaoE">
                                    </div>
                                    <asp:Button ID="btnNAvisar" runat="server" CssClass="BotaoM n50" Text="N�o Avisar"
                                        Visible="true" Width="120px" />
                                    <div class="BotaoD">
                                        <asp:CustomValidator ID="cvtFields" runat="server" ClientValidationFunction="AvisosValidate"
                                            Display="None" ErrorMessage="Informe pelo menos um filtro para pesquisa." ValidationGroup="Avisos_validate">
                                        </asp:CustomValidator>
                                    </div>
                                </div>
                                <div class="BotaoFora n300 divLeft alignLeft">
                                    <div class="BotaoE">
                                    </div>
                                    <asp:Button ID="btnReanalise" runat="server" CssClass="BotaoM n120" Text="Reanalise"
                                        ValidationGroup="filter" Visible="true" Width="120px" />
                                    <div class="BotaoD">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                </fieldset>
                <div class="900px">
                    <div class="900px divLeft">
                        <div class="BotaoFora n300 divLeft alignLeft">
                            <div class="BotaoE">
                            </div>
                            <asp:Button ID="btnVoltar" runat="server" Text="<< Voltar" CssClass="BotaoM n120"
                                TabIndex="1001" />
                            <div class="BotaoD">
                            </div>
                        </div>
                        <div class="BotaoFora n300 divLeft alignLeft">
                            <div class="BotaoE">
                            </div>
                            <asp:Button ID="btnSalvar" runat="server" Text="Salvar" CssClass="BotaoM n120" TabIndex="1002" />
                            <div class="BotaoD">
                            </div>
                        </div>
                        <div class="BotaoFora n300 divLeft alignLeft">
                            <div class="BotaoE">
                            </div>
                            <asp:Button ID="btnContinuar" runat="server" Text="Continuar >>" CssClass="BotaoM n120"
                                TabIndex="1003" />
                            <div class="BotaoD">
                            </div>
                        </div>
                    </div>
                </div>
                &nbsp;<br />
                <br />
                <asp:Panel ID="PanelGrid" runat="server" Style="display: none; font-size: 8pt; padding: 5px;
                    background-color: White; height: auto;" Width="790px">
                    &nbsp;<uc1:UCCoberturaPropostas ID="ucCoberturaPropostas" runat="server" />
                    <br />
                    <div style="text-align: center; width: auto; padding: 5px;">
                        <div class="BotaoFora n300 divLeft alignLeft">
                            <div class="BotaoE">
                            </div>
                            <asp:Button ID="btnFecha" CssClass="Botao" runat="server" Text="Fechar" />
                            <div class="BotaoD">
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="mpe" runat="server" TargetControlID="PanelGrid" PopupControlID="PanelGrid"
                    BackgroundCssClass="modalBackground" OkControlID="btnFecha" OnOkScript="fecha()" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="1">
            <ProgressTemplate>
                <div class="esmaecer">
                    <div class="icon">
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </form>
    <%--<script type="text/javascript">
    var oTable = document.getElementById('GVwSinistrados');
    var RowsLength = oTable.rows.length;

    for (var i=0; i < RowsLength; i++){
        var oCells = oTable.rows.item(i).cells;
        oCells.item(10).style.display = "none";
    }
</script>
--%>
</body>
</html>
