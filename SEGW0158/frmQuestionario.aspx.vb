﻿Imports SEGL0343
Imports SEGL0343.Task
Imports SEGL0343.EO
Imports SEGL0343.Business
Partial Public Class frmQuestionario
    Inherits BasePages

    Dim objQuestionario As New SEGW158frmQuestionario_tk
    Dim ObjSegurado As New SEGW0158ObjSegurado_TK


    Enum TipoControle

        NUMERO = 1
        TEXTO = 2
        DATA = 3
        HORA = 4
        TIMESTAMP = 5
        VALOR = 6
        PERCENTUAL = 7
        MEMO = 8
        RADIO = 7200
        CHECKBOX = 7201
        COMBO = 7202
        LABEL = 7203

    End Enum


#Region "Propriedades"

    Public ReadOnly Property ProdutoQuestionario() As Integer
        Get
            If Not IsNothing(Request.QueryString("ProdutoQuestionario")) Then
                Return Request.QueryString("ProdutoQuestionario")
            Else
                Return 605
            End If
        End Get
    End Property

    Public ReadOnly Property Sinistro_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("SINISTRO_ID")) Then
                Return Request.QueryString("SINISTRO_ID")
            Else
                Return "776"
            End If
        End Get
    End Property



    Public Property gsResultadoQuestionario() As String
        Get
            Return ViewState("gsResultadoQuestionario")
        End Get
        Set(ByVal value As String)

            ViewState("gsResultadoQuestionario") = value

        End Set
    End Property



#End Region



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'CriarControle()

        If Not IsPostBack Then

            ConstruirQuestionario()


        End If

    End Sub

    Sub ConstruirQuestionario()

        Dim objEoInSEGS8950_SPS As New SEGS8950_SPS_EO_IN

        objEoInSEGS8950_SPS.Questionario_Id = Me.ProdutoQuestionario

        objEoInSEGS8950_SPS.Sinistro_Id_Passos = Me.Sinistro_ID

        Dim lstQuestionario As New List(Of SEGS8950_SPS_EO_OUT)
        lstQuestionario = objQuestionario.CarregarQuestionario(objEoInSEGS8950_SPS)

        ViewState("lstQuestionario") = lstQuestionario

        If PersistirInformacoes(lstQuestionario) Then

            objEoInSEGS8950_SPS.Questionario_Id = Me.ProdutoQuestionario

            objEoInSEGS8950_SPS.Sinistro_Id_Passos = Me.Sinistro_ID

            lstQuestionario = objQuestionario.CarregarQuestionario(objEoInSEGS8950_SPS)


        End If

        If lstQuestionario.Count > 0 Then

            Dim i As Integer = 0

            Dim div_maior(lstQuestionario.Count - 1) As HtmlGenericControl
            Dim div_medio(lstQuestionario.Count - 1) As HtmlGenericControl
            Dim div_menor(lstQuestionario.Count - 1) As HtmlGenericControl

            Dim label_questionario(lstQuestionario.Count - 1) As HtmlGenericControl


            '<div id="Panel" style="width:750px; height:auto">
            '	<div Class="n750">
            '		<div Class="n750 divLeft">
            '			<label Class="label">
            '               Meu Teste
            '			</label>
            '			<select name="ctl06">
            '			</select>
            '		</div>
            '	</div>
            '</div>

            For i = 0 To lstQuestionario.Count - 1

                'div_maior(i) = New HtmlGenericControl("div_maior_" & i.ToString)
                div_maior(i) = New HtmlGenericControl("div")
                'div_maior(i).ID = "Panel"
                div_maior(i).ID = "Panel_" & i
                div_maior(i).Attributes.Add("style", "width:750px; height:auto")

                div_medio(i) = New HtmlGenericControl("div")
                div_medio(i).Attributes.Add("Class", "n750")
                div_medio(i).ID = "div_medio_" & i

                div_menor(i) = New HtmlGenericControl("div")
                div_menor(i).Attributes.Add("Class", "n750 divLeft")
                div_menor(i).ID = "div_menor_" & i



                label_questionario(i) = New HtmlGenericControl("label")
                label_questionario(i).Attributes.Add("Class", "label n300")
                label_questionario(i).InnerHtml = (i + 1).ToString & " - " & lstQuestionario(i).nome_pergunta & " " & IIf(label_questionario(i).InnerHtml.ToUpper = "S", "(*)", "")
                label_questionario(i).ID = "label_questionario_" & i


                div_menor(i).Controls.Add(label_questionario(i))




                'lstQuestionario(i).resposta_determinada

                div_menor(i).Controls.Add(CriarControle(lstQuestionario(i).formato_resposta_id, _
                                                            lstQuestionario(i).habilitado, _
                                                            lstQuestionario(i).tamanho_resposta, i, _
                                                            lstQuestionario(i).funcao, _
                                                            lstQuestionario(i).obrigatório, _
                                                            lstQuestionario(i).resposta _
                                                            ))


                Dim hdd As New HiddenField

                hdd.ID = "Campo_Obrigatorio_" & i

                If lstQuestionario(i).obrigatório = "S" Then

                    hdd.Value = lstQuestionario(i).obrigatório

                ElseIf lstQuestionario(i).obrigatório = "N" Or lstQuestionario(i).obrigatório = "" Then

                    hdd.Value = "N"


                End If

                div_menor(i).Controls.Add(hdd)

                div_medio(i).Controls.Add(div_menor(i))

                div_maior(i).Controls.Add(div_medio(i))

                Me.fieldset_questionario.Controls.Add(div_maior(i))

            Next

            'ViewState("fieldset_questionario") = Me.fieldset_questionario

        End If

    End Sub


    Function CriarControle(ByVal intFormatoRespostaId As Integer, _
                            ByVal strHabilitado As String, _
                            ByVal intTamanhoResposta As Integer, _
                            ByVal intIndice As Integer, _
                            ByVal strFuncao As String, _
                            ByVal strObrigatorio As String, _
                            Optional ByVal strResposta As String = "") As Object
        'lstQuestionario(i).habilitado, _
        'lstQuestionario(i).tamanho_resposta, _
        'lstQuestionario(i).resposta, _


        '//onkeypress="return formatar(this, '(??) ????-????', event);"
        '// Usar ? para números e ! para letras

        '            'Me.btnContinuar.Attributes.Add("onclick", "javascript:return confirmaAviso('" & Me.flag_aviso.ClientID & "');")

        Dim objControle As New clsControleDinamico


        Select Case intFormatoRespostaId

            Case TipoControle.CHECKBOX

                Dim chkCheckBox As New CheckBox
                'txt n70

                chkCheckBox.Enabled = (strFuncao = "") 'COLOCAR TRATAMENTO NA PÁGINA

                'chkCheckBox.ID = TipoControle.CHECKBOX & "/" & intIndice
                chkCheckBox.ID = "Pergunta_CHECKBOX_" & intIndice


                If strFuncao <> "" Then

                    If strHabilitado = "" Or strHabilitado = "N" Then
                        chkCheckBox.Enabled = False
                        'goControle(iTotalPerguntas * 2).BackColor = &H80000011  '&H80000011&

                    Else

                        chkCheckBox.Enabled = True

                    End If

                End If

                If strResposta.Length > 0 Then

                    chkCheckBox.Checked = strResposta

                End If


                Return chkCheckBox


            Case TipoControle.COMBO

                Dim ddlCombo As New DropDownList

                'ddlCombo.ID = TipoControle.COMBO & "/" & intIndice
                ddlCombo.ID = "Pergunta_COMBO_" & intIndice

                ddlCombo.Enabled = (strFuncao = "") 'COLOCAR TRATAMENTO NA PÁGINA


                If strFuncao <> "" Then

                    If strHabilitado = "" Or strHabilitado = "N" Then
                        ddlCombo.Enabled = False
                        'goControle(iTotalPerguntas * 2).BackColor = &H80000011  '&H80000011&

                    Else

                        ddlCombo.Enabled = True

                        'goControle(iTotalPerguntas * 2).Enabled = True

                    End If

                End If

                If strResposta.Length > 0 Then

                    ddlCombo.SelectedValue = strResposta

                End If


                Return ddlCombo


            Case TipoControle.DATA

                Dim txtData As New TextBox


                'txtData.ID = TipoControle.DATA & "/" & intIndice
                txtData.ID = "Pergunta_DATA_" & intIndice 'TipoControle.DATA & "/" & intIndice



                txtData.Attributes.Add("Class", "txt n70")

                txtData.Attributes.Add("onkeypress", "javascript:return formatar(this, '??/??/????', event);")
                'txtData.Attributes.Add("onblur", "javascript:return VerificaData(this.value);")

                txtData.MaxLength = intTamanhoResposta

                'txtData.ID = "Pergunta_" & intIndice.ToString

                txtData.Enabled = (strFuncao = "")

                If strFuncao <> "" Then


                    If strHabilitado = "" Or strHabilitado = "N" Then

                        txtData.Enabled = False
                        'goControle(iTotalPerguntas * 2).BackColor = &H80000011  '&H80000011&

                    Else

                        txtData.Enabled = True
                        'goControle(iTotalPerguntas * 2).Enabled = True

                    End If



                End If

                If strResposta.Length > 0 Then

                    txtData.Text = strResposta

                End If


                Return txtData

            Case TipoControle.HORA

                Dim txtHora As New TextBox


                'txtHora.ID = TipoControle.HORA & "/" & intIndice

                txtHora.ID = "Pergunta_HORA_" & intIndice 'TipoControle.DATA & "/" & intIndice


                txtHora.Attributes.Add("Class", "txt n70")


                txtHora.Attributes.Add("onkeypress", "javascript:return formatar(this, '??:??:??', event);")
                txtHora.MaxLength = intTamanhoResposta


                txtHora.Enabled = (strFuncao = "")

                If strFuncao <> "" Then

                    If strHabilitado = "" Or strHabilitado = "N" Then

                        txtHora.Enabled = False
                        'goControle(iTotalPerguntas * 2).BackColor = &H80000011  '&H80000011&

                    Else

                        txtHora.Enabled = True
                        'goControle(iTotalPerguntas * 2).Enabled = True

                    End If


                End If



                If strResposta.Length > 0 Then

                    txtHora.Text = strResposta

                End If


                Return txtHora

            Case TipoControle.LABEL

                Dim lblLabel As New Label

                lblLabel.ID = "Pergunta_LABEL_" & intIndice


                'lblLabel.Attributes.Add("onkeypress", "javascript:return formatar(this, '???,????', event);")
                'lblLabel.MaxLength = intTamanhoResposta + 1

                lblLabel.Attributes.Add("Class", "txt n200")

                lblLabel.Enabled = (strFuncao = "")


                If strFuncao <> "" Then

                    If strHabilitado = "" Or strHabilitado = "N" Then

                        lblLabel.Enabled = False
                        'goControle(iTotalPerguntas * 2).BackColor = &H80000011  '&H80000011&

                    Else

                        lblLabel.Enabled = True
                        'goControle(iTotalPerguntas * 2).Enabled = True

                    End If

                End If


                If strResposta.Length > 0 Then

                    lblLabel.Text = strResposta

                End If


                Return lblLabel

            Case TipoControle.MEMO

                Dim txtMemo As New TextBox

                'txtMemo.ID = TipoControle.MEMO & "/" & intIndice
                txtMemo.ID = "Pergunta_MEMO_" & intIndice

                txtMemo.TextMode = TextBoxMode.MultiLine

                txtMemo.Enabled = (strFuncao = "")

                txtMemo.Attributes.Add("Class", "txt n200")


                If strFuncao <> "" Then

                    If strHabilitado = "" Or strHabilitado = "N" Then

                        txtMemo.Enabled = False

                    Else

                        txtMemo.Enabled = True

                    End If

                End If

                If strResposta.Length > 0 Then

                    txtMemo.Text = strResposta

                End If

                Return txtMemo



            Case TipoControle.PERCENTUAL

                Dim txtPercentual As New TextBox

                txtPercentual.ID = "Pergunta_PERCENTUAL_" & intIndice

                txtPercentual.Attributes.Add("Class", "txt n200")


                txtPercentual.Attributes.Add("onkeypress", "javascript:return formatar(this, '???,????', event);")
                txtPercentual.MaxLength = intTamanhoResposta + 1

                'txtPercentual.Enabled = Not (strHabilitado = "N" Or strHabilitado = "")

                txtPercentual.Enabled = (strFuncao = "")


                If strFuncao <> "" Then

                    If strHabilitado = "" Or strHabilitado = "N" Then

                        txtPercentual.Enabled = False

                    Else

                        txtPercentual.Enabled = True

                    End If

                End If

                If strResposta.Length > 0 Then

                    txtPercentual.Text = strResposta

                End If

                Return txtPercentual

            Case TipoControle.NUMERO



                Dim txtNumero As New TextBox


                txtNumero.ID = "Pergunta_NUMERO_" & intIndice

                'txtNumero.Attributes.Add("onkeypress", "javascript:return somente_numero(this);")
                txtNumero.Attributes.Add("onkeypress", "javascript:return SomenteNumero(event);")
                txtNumero.MaxLength = intTamanhoResposta + 1
                'txtNumero.Enabled = Not (strHabilitado = "N" Or strHabilitado = "")

                txtNumero.Attributes.Add("Class", "txt n200")

                txtNumero.Enabled = (strFuncao = "")


                If strFuncao <> "" Then


                    If strHabilitado = "" Or strHabilitado = "N" Then

                        txtNumero.Enabled = False

                    Else

                        txtNumero.Enabled = True

                    End If

                End If

                If strResposta.Length > 0 Then

                    txtNumero.Text = strResposta

                End If


                Return txtNumero

            Case TipoControle.RADIO

                'txtPercentual.Enabled = (strFuncao = "")

                If strFuncao <> "" Then

                    If strHabilitado = "" Or strHabilitado = "N" Then

                        'txtNumero.Enabled = False

                    Else

                        'txtNumero.Enabled = True

                    End If

                End If


            Case TipoControle.TEXTO

                Dim txtTexto As New TextBox

                'txtTexto.ID = TipoControle.TEXTO & "/" & intIndice

                txtTexto.ID = "Pergunta_TEXTO_" & intIndice


                txtTexto.MaxLength = intTamanhoResposta

                txtTexto.Attributes.Add("Class", "txt n200")

                txtTexto.Enabled = (strFuncao = "")

                If strFuncao <> "" Then

                    If strHabilitado = "" Or strHabilitado = "N" Then

                        txtTexto.Enabled = False

                    Else

                        txtTexto.Enabled = True

                    End If

                End If

                If strResposta.Length > 0 Then

                    txtTexto.Text = strResposta

                End If

                Return txtTexto

            Case TipoControle.TIMESTAMP


            Case TipoControle.VALOR

                Dim txtValor As New TextBox

                txtValor.Attributes.Add("onkeypress", "javascript:return formatar(this, '???????????,????', event);")
                txtValor.MaxLength = intTamanhoResposta

                txtValor.ID = "Pergunta_VALOR_" & intIndice

                txtValor.Attributes.Add("Class", "txt n200")

                txtValor.Enabled = (strFuncao = "")


                If strFuncao <> "" Then

                    If strHabilitado = "" Or strHabilitado = "N" Then

                        txtValor.Enabled = False

                    Else

                        txtValor.Enabled = True

                    End If

                End If

                'IsNull(RS("habilitado")) Or RS("habilitado") = "N"

                If strResposta.Length > 0 Then

                    txtValor.Text = strResposta

                End If


                Return txtValor

        End Select


    End Function

    Function PersistirInformacoes(ByRef listaQuestionario As List(Of SEGS8950_SPS_EO_OUT)) As Boolean

        Dim blnTemDominio As Boolean = False

        PersistirInformacoes = False

        'Exclui dados da tabela re_passos_questionario_aviso_sinistro_tb
        Dim objEo_InSEGS8946_SPD As New SEGS8946_SPD_EO_IN

        objEo_InSEGS8946_SPD.Sinistro_Id_Passos = Me.Sinistro_ID

        If ObjSegurado.ExcluirQuestionarioAvisoSinistro(objEo_InSEGS8946_SPD) = True Then

            Dim objEoInTempQuest As New SEGS8945_SPI_EO_IN

            Dim itemQuestionario As New SEGS8950_SPS_EO_OUT

            For Each itemQuestionario In listaQuestionario

                With objEoInTempQuest

                    If IsNothing(itemQuestionario.drid) = False Then

                        .Dominio_Resposta_Id = itemQuestionario.drid

                    End If

                    If IsNothing(itemQuestionario.pid) = False Then

                        .Pergunta_Id = itemQuestionario.pid

                    End If

                    .Questionario_Id = itemQuestionario.questionario_id

                    'AQUI ID DOS PASSOS
                    .Sinistro_Id_Passos = Me.Sinistro_ID

                    If itemQuestionario.funcao.Trim.Length > 0 Then

                        Dim objEoInTextoResposta As New SEGS8969_SPS_EO_IN

                        objEoInTextoResposta.Funcao = itemQuestionario.funcao

                        objEoInTextoResposta.Sinistro_Id_Passos = Me.Sinistro_ID

                        Dim lstTextoResposta As New List(Of SEGS8969_SPS_EO_OUT)

                        lstTextoResposta = objQuestionario.RetornarTextoResposta(objEoInTextoResposta)


                        If lstTextoResposta(0).Valor = "__/__/____" Then

                            .Texto_Resposta = ""


                        Else

                            .Texto_Resposta = lstTextoResposta(0).Valor

                        End If

                    Else


                        If Not IsNothing(itemQuestionario.resposta) Then

                            .Texto_Resposta = itemQuestionario.resposta

                        Else

                            .Texto_Resposta = ""


                        End If





                    End If

                    objQuestionario.InserirTempQuestionario(objEoInTempQuest)

                End With
            Next
            PersistirInformacoes = True

        End If



    End Function


    Function ValidaCampos(ByRef listaQuestionario As List(Of SEGS8950_SPS_EO_OUT)) As Boolean

        'ValidaCampos = False

        'Dim lstControlesValidar As New List(Of clsControleDinamico)

        'Dim itemQuestionario As New SEGS8950_SPS_EO_OUT

        'lstControlesValidar = ViewState("lstControles")



        'Dim itemControle As New clsControleDinamico

        'For Each itemControle In lstControlesValidar

        '    Select Case itemControle.TipoControle

        '        Case TipoControle.NUMERO

        '            Dim txtNumero As TextBox = Me.FindControl(itemControle.NomeControle)

        '            ScriptManager.RegisterClientScriptBlock(Page, _
        '                                                    Me.GetType(), _
        '                                                    Guid.NewGuid().ToString(), _
        '                                                    "alert(' " & txtNumero.Text & " ')", True)



        '            'Dim iframeContent As HtmlControl = CType(Me.FindControl("iframeContent"), HtmlControl)
        '        Case TipoControle.TEXTO
        '        Case TipoControle.DATA
        '        Case TipoControle.HORA
        '        Case TipoControle.TIMESTAMP
        '        Case TipoControle.VALOR
        '        Case TipoControle.PERCENTUAL
        '        Case TipoControle.MEMO
        '        Case TipoControle.RADIO
        '        Case TipoControle.CHECKBOX
        '        Case TipoControle.COMBO
        '        Case TipoControle.LABEL

        '    End Select

        'Next













        For Each ctr As Control In Page.Controls

            If Not IsNothing(ctr) Then

                ScriptManager.RegisterClientScriptBlock(Page, _
                                                        Me.GetType(), _
                                                        Guid.NewGuid().ToString(), _
                                                        "alert(' " & ctr.ID & " ')", True)
            End If

        Next

        'For Each ctr As Control In Me.Controls



        '    If Not IsNothing(ctr.ID) Then


        '        ScriptManager.RegisterClientScriptBlock(Page, _
        '                                                Me.GetType(), _
        '                                                Guid.NewGuid().ToString(), _
        '                                                "alert(' " & ctr.ID & " ')", True)


        '    End If



        'Next





        ValidaCampos = True

    End Function

    Protected Sub btnVoltar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoltar.Click


        Dim lstQuestionario As New List(Of SEGS8950_SPS_EO_OUT)
        lstQuestionario = ViewState("lstQuestionario")

        If PersistirInformacoes(lstQuestionario) = True Then

            gsResultadoQuestionario = "V"

            Dim sb As New StringBuilder()
            sb.Append("SEGW0158ObjSegurado.aspx")
            sb.Append("?Sinistro_ID=" & Me.Sinistro_ID)
            sb.Append("&gsResultadoQuestionario=" & gsResultadoQuestionario)

            Response.Redirect(sb.ToString())

        Else

            ScriptManager.RegisterClientScriptBlock(Page, _
                                                    Me.GetType(), _
                                                    Guid.NewGuid().ToString(), _
                                                    "alert('Erro ao gravar questionario')", True)


        End If



    End Sub

    Protected Sub btnContinuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinuar.Click

        Dim lstQuestionario As New List(Of SEGS8950_SPS_EO_OUT)
        lstQuestionario = ViewState("lstQuestionario")


        'Dim MeuFieldSet As New HtmlGenericControl("fieldset")

        'ViewState("fieldset_questionario") = Me.fieldset_questionario

        'MeuFieldSet = ViewState("fieldset_questionario")



        If ValidaCampos(lstQuestionario) = True Then





        End If

    End Sub

    Public Sub Mensagem(ByVal lTipo As Long, ByVal sPergunta As String)
        Dim sMensagem As String
        Select Case lTipo
            Case -1 ' campo texto de preenchimento obrigatório
                sMensagem = "O campo deve ser preenchido."
            Case -2 ' campo de seleção obrigatória
                sMensagem = "Selecione um dos itens."
            Case 1, 6, 7
                sMensagem = "O campo deve ser um número."
            Case 3
                sMensagem = "O campo deve estar no formato de data (dd/mm/aaaa)."
            Case 4
                sMensagem = "O campo deve estar no formato de hora (hh:mm:ss)."
            Case 5
                sMensagem = "Valor deve ser positivo."
            Case 8
                sMensagem = "Valor de percentual deve estar entre 0 e 100."
        End Select
        ScriptManager.RegisterClientScriptBlock(Page, _
                                                Me.GetType(), _
                                                Guid.NewGuid().ToString(), _
                                                "alert('" & sPergunta & "\n" & sMensagem & "')", True)

    End Sub

    Protected Sub btnCalcular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalcular.Click

    End Sub
End Class