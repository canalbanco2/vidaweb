﻿Imports SEGL0343
Imports SEGL0343.EO
Imports SEGL0343.Task


Partial Public Class SEGW0158DadosCliente
    Inherits BasePages
    Private taskSolicitante As New SEGW0157Solicitante_TK
    Private SINISTRO_ID_ As String

#Region "Propriedades"
    Public ReadOnly Property Sinistro_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("SINISTRO_ID")) Then
                Return Request.QueryString("SINISTRO_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property
#End Region
#Region "Métodos"
    Public Sub CarregarUF()
        Try
            Dim ObjEO_IN As New SEGL0343.EO.SEGS8708_SPS_EO_IN
            Dim b As New SEGL0343.Business.SEGS8708_SPS_Business

            Me.cboUF.DataSource = b.seleciona(ObjEO_IN)
            Me.cboUF.DataTextField = "NOME"
            Me.cboUF.DataValueField = "NOME"
            Me.cboUF.DataBind()

        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro Ao Carregar UF " & "')", True)
        End Try
    End Sub
    Private Sub PreencheComboMunicipio()
        Dim Obj_TK As New SEGL0343.Task.UCAgencia_TK()
        Dim Obj_EO_IN As New SEGL0343.EO.SEL_MUNICIPIO_SPS_EO_IN()

        With Obj_EO_IN
            .Estado = Me.cboUF.SelectedValue
            .Municipio = String.Empty
            .Municipio_Id = String.Empty
        End With

        With Me.cboMunicipio
            .DataSource = Obj_TK.ObtemListaMunicipio(Obj_EO_IN)
            .DataTextField = "nome"
            .DataValueField = "municipio_id"
            .DataBind()
            .Items.Insert(0, New ListItem("Escolha o município", ""))
            .SelectedIndex = 0
        End With

        Me.cboUF.Focus()
    End Sub
    Public Function ValidaCamposFormulario() As String
        Dim strErros As New StringBuilder
        Try
            If Me.txtNomeSegurado.Text.Trim = String.Empty Then
                strErros.Append("Indique o Nome do Segurado.\n")
            End If

            If Me.txtCPF_CNPJ.Text.Trim = String.Empty Then
                strErros.Append("Indique o C.P.F./C.N.P.J. do Segurado.\n")
            End If

            If Me.txtPropostaBB.Text.Trim = String.Empty Then
                strErros.Append("Indique o Número da Proposta BB.\n")
            End If

            If Me.txtEndereco.Text.Trim = String.Empty Then
                strErros.Append("Indique o Endereço do Segurado.\n")
            End If

            If Me.txtBairro.Text.Trim = String.Empty Then
                strErros.Append("Indique o Bairro do Segurado.\n")
            End If

            If Me.cboUF.SelectedValue = String.Empty Then
                strErros.Append("Indique a Unidade da Federação do Segurado.\n")
            End If

            If Me.cboMunicipio.SelectedValue = String.Empty Then
                strErros.Append("Indique o Município do Segurado.\n")
            End If

            If Me.txtCEP.Text.Trim = String.Empty Then
                strErros.Append("Indique o CEP do Segurado.\n")
            End If

            Return strErros.ToString

        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro Ao Validar os campos do formulário " & ex.Message.ToString & "')", True)
            strErros.Append(ex.Message.ToString)
            Return strErros.ToString
        End Try
    End Function
    Public Sub Gravar()
        Try

            If Sinistro_ID = 0 Then
                Dim TaskGravaPassoSinistro As New SEGW158SinistroInserirPassosDanos_TK
                Dim EO_IN As New SEGS8866_SPI_EO_IN
                Dim EO_OUT As List(Of EO.SEGS8866_SPI_EO_OUT)

                EO_IN.Nome_Segurado = Me.txtNomeSegurado.Text
                EO_IN.Cpf_Cnpj_Segurado = Me.txtCPF_CNPJ.Text
                EO_IN.Proposta_Bb_Segurado = Me.txtPropostaBB.Text
                EO_IN.Endereco_Segurado = Me.txtEndereco.Text
                EO_IN.Bairro_Segurado = Me.txtBairro.Text
                EO_IN.Cep_Segurado = Me.txtCEP.Text
                EO_IN.Uf_Segurado = Me.cboUF.Text
                EO_IN.Municipio_Segurado = Me.cboMunicipio.Text

                EO_IN.Codigo_Passo = 8
                EO_IN.Dt_Inclusao = Now.Date

                EO_OUT = TaskGravaPassoSinistro.Inserir_Passo_Danos(EO_IN)

                SINISTRO_ID_ = EO_OUT.Item(0).sinistro_id
            Else

                Dim objOut_8744 As New List(Of SEGS8744_SPS_EO_OUT)
                Dim objIn_8744 As New SEGS8744_SPS_EO_IN
                Dim PassoFinal As Boolean = False

                Dim objGeral As New SEGW0158Geral_TK()

                objIn_8744.Sinistro_Id = Me.Sinistro_ID

                objOut_8744 = objGeral.Buscar(objIn_8744)

                Dim objIn_8747 As New SEGS8747_SPU_EO_IN()

                If Not IsNothing(objOut_8744) Then
                    objIn_8747 = objGeral.PreencheTodosCamposU(objOut_8744(0))
                End If

                objIn_8747.Nome_Segurado = Me.txtNomeSegurado.Text
                objIn_8747.Cpf_Cnpj_Segurado = Me.txtCPF_CNPJ.Text
                objIn_8747.Proposta_Bb_Segurado = Me.txtPropostaBB.Text
                objIn_8747.Endereco_Segurado = Me.txtEndereco.Text
                objIn_8747.Bairro_Segurado = Me.txtBairro.Text
                objIn_8747.Cep_Segurado = Me.txtCEP.Text
                objIn_8747.Uf_Segurado = Me.cboUF.Text
                objIn_8747.Municipio_Segurado = Me.cboMunicipio.Text

                objIn_8747.Codigo_Passo = 8
                objGeral.Atualizar(objIn_8747)

            End If


        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro Ao Processar Gravação Passo 2. " & ex.Message.ToString & "')", True)
        End Try
    End Sub
    Public Sub Carregar()
        Dim parametros As New SEGL0343.EO.SEGS8744_SPS_EO_IN
        Dim dadosAtuais As List(Of SEGL0343.EO.SEGS8744_SPS_EO_OUT)
        Dim b As New SEGL0343.Business.SEGS8744_SPS_Business
        '  parametros.Sinistro_Id = Me.Sinistro_ID    Alterado por David Azevedo 30/07 as 11:00
        parametros.Sinistro_Id = Long.Parse(Me.Sinistro_ID)
        dadosAtuais = b.seleciona(parametros)
        If dadosAtuais.Count > 0 Then
            Me.txtNomeSegurado.Text = dadosAtuais.Item(0).nome_segurado
            Me.txtCPF_CNPJ.Text = dadosAtuais.Item(0).cpf_cnpj_segurado
            Me.txtPropostaBB.Text = dadosAtuais.Item(0).proposta_bb_segurado
            Me.txtEndereco.Text = dadosAtuais.Item(0).endereco_segurado
            Me.txtBairro.Text = dadosAtuais.Item(0).bairro_segurado
            Me.txtCEP.Text = dadosAtuais.Item(0).cep_segurado
            Me.cboUF.Text = dadosAtuais.Item(0).uf_segurado
            PreencheComboMunicipio(Me.cboUF.SelectedItem.Text)
            Me.cboMunicipio.Text = dadosAtuais.Item(0).municipio_segurado
        End If
    End Sub
#End Region
#Region "Eventos"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                Me.CarregarUF()
                Me.Carregar()
            End If
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

        AjustaNavegacao("Evento")
        Session.Add("DadosCliente", "S")

        LkbDadosCliente.Text = " ► Dados do Cliente"
        LkbDadosCliente.ForeColor = Drawing.Color.Blue
    End Sub
    Public Sub AjustaNavegacao(ByVal But As String)
        Select Case But
            Case "Evento"
                LkbPesquisa.Enabled = True
            Case "LkbSolicitante"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
            Case "LkbAgencia"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
            Case "LkbSinistroCA"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
            Case "LkbObjsegurado"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
            Case "LkbOutrosSeguros"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
                LkbObjsegurado.Enabled = True
            Case "Ressumo"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
                LkbObjsegurado.Enabled = True
            Case "SemPropostaCliente"
                LkbDadosCliente.Enabled = True
        End Select
    End Sub
    Protected Sub cboUF_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUF.SelectedIndexChanged
        Try
            If Me.cboUF.SelectedValue <> String.Empty Then
                PreencheComboMunicipio()
            Else
                Me.cboMunicipio.Items.Clear()
                Me.cboMunicipio.Items.Insert(0, New ListItem("Escolha a UF", ""))
                Me.cboMunicipio.SelectedIndex = 0
            End If
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub
    Protected Sub btnContinuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim strErros As String = Me.ValidaCamposFormulario()
        Dim sb As New StringBuilder()

        Try

            If Not strErros = String.Empty Then
                ScriptManager.RegisterStartupScript(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & strErros.Replace("'", " ") & "');", True)
                Exit Sub
            End If
            Me.Gravar()

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

        sb.Append("SEGW0158Evento.aspx")
        If Sinistro_ID > 0 Then
            sb.Append("?SINISTRO_ID=" & Sinistro_ID)
        Else
            sb.Append("?SINISTRO_ID=" & SINISTRO_ID_)
        End If
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())


    End Sub
    Protected Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Gravar()
    End Sub
    Protected Sub btnVoltar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim sb As New StringBuilder()
        sb.Append("SEGW0158ConsultaSinistrado.aspx")
        'sb.Append("?SINISTRO_ID=" & Me.Sinistro_ID)
        sb.Append("?SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())

    End Sub
#End Region

    Protected Sub ibtPesquisarCEP_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If Me.txtCEP.Text <> String.Empty Then
                BindEnderecoObjSolicitante()
                txtNomeSegurado.Focus()
            End If
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub
    Private Sub BindEnderecoObjSolicitante()
        Try
            'base dos correios
            Dim Obj_TK As New UCDadosSolicitanteVida_TK()
            Dim Obj_EO_IN As New SEGS8839_SPS_EO_IN()
            Dim Obj_EO_OUT As New SEGS8839_SPS_EO_OUT()
            'base interna
            Dim municipio As New municipio_spi_EO_IN
            Dim lista_interna_in As New SEL_MUNICIPIO_SPS_EO_IN
            Dim lista_interna_out As New List(Of SEL_MUNICIPIO_SPS_EO_OUT)
            'atualização
            Dim atualiza As New municipio_spu_EO_IN

            Obj_EO_IN.Cep = txtCEP.Text.Replace("-", "")
            Obj_EO_OUT = Obj_TK.ObtemEnderecoObjSegurado(Obj_EO_IN)
            'verifica se o municipio está na tabela dos correios
            If Not IsNothing(Obj_EO_OUT) Then
                Me.txtBairro.Text = Obj_EO_OUT.Bairro.Trim
                Me.txtCEP.Text = Obj_EO_OUT.CEP.Trim
                Me.txtEndereco.Text = Obj_EO_OUT.Endereco.Trim
                Me.cboUF.Text = Obj_EO_OUT.UF.Trim

                PreencheComboMunicipio()
                cboMunicipio.Text = Obj_EO_OUT.Municipio_ID.ToString

            End If
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub
    Public Sub PreencheComboMunicipio(ByVal UF As String)
        Dim Obj_TK As New UCDadosSolicitanteVida_TK()
        Dim Obj_EO_IN As New SEL_MUNICIPIO_SPS_EO_IN()
        Try
            Obj_EO_IN.Estado = UF
            Obj_EO_IN.Municipio = String.Empty
            Obj_EO_IN.Municipio_Id = String.Empty

            cboMunicipio.DataSource = Obj_TK.ObtemListaMunicipio(Obj_EO_IN)
            cboMunicipio.DataTextField = "nome"
            cboMunicipio.DataValueField = "municipio_id"
            cboMunicipio.DataBind()

            cboMunicipio.Items.Insert(0, New ListItem("Escolha o município", ""))
            Me.cboMunicipio.SelectedIndex = 0
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub
    Protected Sub LkbPesquisa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbPesquisa.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158ConsultaSinistrado.aspx")
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())
    End Sub
End Class