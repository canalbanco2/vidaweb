﻿Imports SEGL0343
Imports SEGL0343.Task
Imports SEGL0343.EO
Imports SEGL0343.Business
Imports System.Text

Imports System.Configuration

Partial Public Class SEGW0158ObjSegurado
    Inherits BasePages

    Dim ObjGeral As New Task.SEGW0158Geral_TK
    Dim ObjSegurado As New SEGW0158ObjSegurado_TK
    Const EVENTO_SINISTRO_PONTUAL = 1
    Const EVENTO_SINISTRO_PERIODICO = 2
    Dim destino As New DataTable
    Dim objQuestionario As New SEGW158frmQuestionario_tk


    Dim objLogEoIn As New SEGS9100_SPS_EO_IN
    Dim objLogBus As New SEGS9100_SPS_Business

    Enum TipoControle

        NUMERO = 1
        TEXTO = 2
        DATA = 3
        HORA = 4
        TIMESTAMP = 5
        VALOR = 6
        PERCENTUAL = 7
        MEMO = 8
        RADIO = 7200
        CHECKBOX = 7201
        COMBO = 7202
        LABEL = 7203

    End Enum


    Dim intProdutoQuestionario As Integer
    Dim gsResultadoQuestionario As String


#Region "Propriedades Objeto Segurado"
    Private _proposta_id As String
    Private _processa_reintegracao_is As String
    Private _sResposta As String

    Public ReadOnly Property Evento_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("Evento_ID")) Then
                Return Request.QueryString("Evento_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property SubEvento_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("SubEvento_ID")) Then
                Return Request.QueryString("SubEvento_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property Sinistro_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("SINISTRO_ID")) Then
                Return Request.QueryString("SINISTRO_ID")
            Else
                'Return Nothing
                'Return "995"
                'Return "1014"
                'Return "1054" 'Reanalise
                'Return "1395" 'IMPORTANTE
                Return "1554"

                'Return "1044"
                'Return "972"
                'Return "989"
            End If
        End Get
    End Property

    Public ReadOnly Property Cliente_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("Cliente_ID")) Then
                Return Request.QueryString("Cliente_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property Dt_Ocorrencia() As String
        Get
            If Not IsNothing(Request.QueryString("Dt_Ocorrencia")) Then
                Return Request.QueryString("Dt_Ocorrencia")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property CPF_CNPJ() As String
        Get
            If Not IsNothing(Request.QueryString("CPF_CNPJ")) Then

                Return Request.QueryString("CPF_CNPJ")

            Else

                Return Nothing

            End If
        End Get
    End Property

    Public Property Processa_Reintegracao_Is() As String
        Get
            Return _processa_reintegracao_is
        End Get
        Set(ByVal value As String)
            _processa_reintegracao_is = value
        End Set
    End Property

    Public ReadOnly Property sResposta() As Integer
        Get
            If Not IsNothing(Request.QueryString("sResposta")) Then
                Return Request.QueryString("sResposta")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public Property listaPropostasExcluir() As String
        Get
            Return CType(Session("listaPropostasExcluir"), String)
        End Get
        Set(ByVal value As String)
            Session("listaPropostasExcluir") = value
        End Set
    End Property

    Public Property listaPropostas() As String
        Get
            Return CType(ViewState("listaPropostas"), String)
        End Get
        Set(ByVal value As String)
            ViewState("listaPropostas") = value
        End Set
    End Property

    Public Property DestinoPropostasDoAviso() As DataTable
        Get
            Return CType(ViewState("destino"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState("destino") = value
        End Set
    End Property

    Public Property Proposta_ID() As String
        Get
            Return CType(Session("_proposta_id"), String)
        End Get
        Set(ByVal value As String)
            Session("_proposta_id") = value
        End Set
    End Property

    Public Property ProtutoQuestionario() As Integer
        Get
            Return CType(ViewState("ProtutoQuestionario"), Integer)
        End Get
        Set(ByVal value As Integer)
            ViewState("ProtutoQuestionario") = value
        End Set
    End Property


#End Region

#Region "Eventos Objeto Segurado"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            HabilitaControles(False)
            CarregaDados()
            CarregaPropostas()
            ListarPropostaAvisoVoltarContinuar()

            ControlaExibicaoFieldPropostas(False)

            If grdPropostas.Rows.Count = 1 Then

                grdPropostas.SelectedIndex = 0
                grdPropostas_SelectedIndexChanged(grdPropostas, Nothing)

                'MontarQuestionarioProduto(ProtutoQuestionario)

                If ProtutoQuestionario > 0 Then

                    Me.mpeQuestionario.Show()

                End If


            End If

        End If
        If Observacoes1.Observacao.Length = 1 Then

            Observacoes1.Observacao = Observacoes1.Observacao.Replace(",", "")
        End If
        If IsNothing(Session("DadosCliente")) Then
            AjustaNavegacao("LkbObjsegurado")
        Else
            AjustaNavegacao("DadosCliente")
        End If
        LkbObjsegurado.Text = " ► Objeto Segurado"
        LkbObjsegurado.ForeColor = Drawing.Color.Blue
    End Sub
    Public Sub AjustaNavegacao(ByVal But As String)
        Select Case But
            Case "Evento"
                LkbPesquisa.Enabled = True
            Case "LkbSolicitante"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
            Case "LkbAgencia"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
            Case "LkbSinistroCA"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
            Case "LkbObjsegurado"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
            Case "LkbOutrosSeguros"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
                LkbObjsegurado.Enabled = True
            Case "Ressumo"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
                LkbObjsegurado.Enabled = True
            Case "DadosCliente"
                LkbDadosCliente.Enabled = True
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
        End Select
    End Sub

    Protected Sub btnVoltar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoltar.Click

        Me.Voltar()

    End Sub

    Protected Sub btnAdicionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdicionar.Click
        Dim i As Integer
        Dim Eo_outPassos As List(Of SEGS8744_SPS_EO_OUT)

        Eo_outPassos = ViewState("EoPassos")

        If Not IsNothing(Me.DestinoPropostasDoAviso) Then

            destino = Me.DestinoPropostasDoAviso

        End If

        'If IsNothing(Me.grdPropostas.SelectedRow) Then
        '    ScriptManager.RegisterClientScriptBlock(Page, _
        '                                            Me.GetType(), _
        '                                            Guid.NewGuid().ToString(), _
        '                                            "alert('Selecione uma Proposta no Grid PROPOSTAS!')", True)
        '    Exit Sub
        'End If



        If Me.ddlCobAtingida.SelectedIndex = -1 Then
            ScriptManager.RegisterClientScriptBlock(Page, _
                                                    Me.GetType(), _
                                                    Guid.NewGuid().ToString(), _
                                                    "alert('É preciso especificar a cobertura afetada!')", _
                                                    True)

            If Me.ddlCobAtingida.Enabled = True Then

                Me.ddlCobAtingida.Focus()

            End If

            Exit Sub
        End If

        If radReintegracao.Visible = True Then

            If Me.radReintegracao.SelectedIndex = -1 Then
                ScriptManager.RegisterClientScriptBlock(Page, _
                                                        Me.GetType(), _
                                                        Guid.NewGuid().ToString(), _
                                                        "alert('É necessário indicar se há reintegração de IS!')", _
                                                        True)
                Exit Sub

            ElseIf Me.radReintegracao.SelectedIndex = 0 Then


                ViewState("Processa_Reintegracao_Is") = "S"


            ElseIf Me.radReintegracao.SelectedIndex = 1 Then


                ViewState("Processa_Reintegracao_Is") = "N"


            End If

        End If



        ddlCobAtingida.Enabled = False

        Dim origem As New List(Of SEGS8915_SPS_EO_OUT)

        Dim r As DataRow

        Dim lstObjEoOutSEGS8811_SPS As New List(Of SEGS8811_SPS_EO_OUT)
        Dim objOutSEGS8892_SPS As New SEGS8892_SPS_EO_OUT


        objOutSEGS8892_SPS = Session("objOutSEGS8892_SPS")

        lstObjEoOutSEGS8811_SPS = ViewState("gridPropostas")

        'Dim objOutSEGS8811_SPS As New SEGS8811_SPS_EO_OUT

        Dim objOut As New SEGS8811_SPS_EO_OUT

        objOut.proposta_id = grdPropostas.SelectedRow.Cells(1).Text

        Dim objOutSEGS8811_SPS As SEGS8811_SPS_EO_OUT = lstObjEoOutSEGS8811_SPS.Find(New Predicate(Of SEGS8811_SPS_EO_OUT) _
                                            (AddressOf objOut.findProposta))

        Session("objOutSEGS8892_SPS") = objOutSEGS8892_SPS


        If objOutSEGS8811_SPS.proposta_id >= 0 Then

            If grdPropostas.Rows.Count > 0 Then

                If destino.Columns.Count = 0 Then

                    With destino.Columns

                        .Add("proposta_id") 'ok
                        .Add("proposta_bb") 'ok
                        .Add("cod_produto") 'ok
                        .Add("ramo_id")     'ok
                        .Add("apolice_id")  'ok
                        .Add("nom_produto") 'ok

                        If grdDescrObjSegurado.Rows.Count > 0 Then

                            If objOutSEGS8811_SPS.produto_id = 1152 Then

                                .Add("cultura")         'ok
                                .Add("endereco")        'ok
                                .Add("municipio")       'ok
                                .Add("uf")              'ok
                            Else

                                .Add("endereco")
                            End If

                        End If

                        .Add("cod_evento_sinistro")
                        .Add("nom_evento_sinistro")

                        .Add("cod_cobertura_afetada")
                        .Add("dsc_cobertura")

                        .Add("cod_objeto_segurado")
                        .Add("nom_objeto_segurado")
                        .Add("valor_prejuizo")
                        .Add("valor_is")
                        .Add("processa_reintegracao_is")

                        .Add("motivo_reanalise_sinistro_id")
                        .Add("descricao_motivo_reanalise")
                        .Add("texto_motivo_reanalise")






                    End With

                End If

                If objOutSEGS8811_SPS.proposta_id <> 0 Then

                    r = destino.NewRow
                    r.Item("proposta_id") = objOutSEGS8811_SPS.proposta_id
                    r.Item("proposta_bb") = objOutSEGS8811_SPS.proposta_bb

                    r.Item("cod_produto") = objOutSEGS8811_SPS.produto_id
                    r.Item("nom_produto") = objOutSEGS8811_SPS.nome 'grdPropostas.SelectedRow.Cells(8).Text
                    r.Item("apolice_id") = objOutSEGS8811_SPS.apolice_id ' grdPropostas.SelectedRow.Cells(7).Text

                    r.Item("ramo_id") = objOutSEGS8811_SPS.ramo_id

                    r.Item("nom_evento_sinistro") = Eo_outPassos(0).evento
                    r.Item("valor_prejuizo") = Me.txtValPrejuizo.Text
                    r.Item("dsc_cobertura") = Me.ddlCobAtingida.SelectedItem.Text

                    If objOutSEGS8811_SPS.produto_id = 1152 Or objOutSEGS8811_SPS.produto_id = 1108 Then
                        r.Item("cod_objeto_segurado") = "1"

                    End If

                    If objOutSEGS8811_SPS.produto_id = 1152 Then


                        r.Item("nom_objeto_segurado") = "Cultura: " & grdDescrObjSegurado.Rows(5).Cells(1).Text & _
                                                        ", Endereço: " & grdDescrObjSegurado.Rows(7).Cells(1).Text.Trim & _
                                                        ", Município: " & grdDescrObjSegurado.Rows(9).Cells(1).Text & _
                                                        ", UF: " & grdDescrObjSegurado.Rows(8).Cells(1).Text

                    ElseIf objOutSEGS8811_SPS.produto_id = 1108 Then

                        r.Item("nom_objeto_segurado") = grdDescrObjSegurado.Rows(0).Cells(1).Text.Trim.ToUpper


                    Else

                        r.Item("cod_objeto_segurado") = grdDescrObjSegurado.Rows(0).Cells(0).Text
                        r.Item("nom_objeto_segurado") = grdDescrObjSegurado.Rows(0).Cells(1).Text

                    End If



                    r.Item("cod_evento_sinistro") = Eo_outPassos(0).codigo_evento
                    r.Item("cod_cobertura_afetada") = Me.ddlCobAtingida.SelectedValue
                    r.Item("valor_is") = IIf(Me.txtValIS.Text.Trim = "", "0", Me.txtValIS.Text.Trim)
                    r.Item("processa_reintegracao_is") = ViewState("Processa_Reintegracao_Is")

                    r.Item("motivo_reanalise_sinistro_id") = IIf(IsNothing(ViewState("motivo_reanalise_sinistro_id")), "", ViewState("motivo_reanalise_sinistro_id"))
                    r.Item("descricao_motivo_reanalise") = IIf(IsNothing(ViewState("descricao_motivo_reanalise")), "", ViewState("descricao_motivo_reanalise"))
                    r.Item("texto_motivo_reanalise") = IIf(IsNothing(ViewState("texto_motivo_reanalise")), "", ViewState("texto_motivo_reanalise"))

                    destino.Rows.Add(r)

                Else

                    r = destino.NewRow
                    r.Item("proposta_id") = "0"
                    r.Item("proposta_bb") = "0"
                    r.Item("cod_produto") = "0"
                    r.Item("nom_produto") = "Sem produto"
                    r.Item("apolice_id") = "0"
                    r.Item("ramo_id") = "0"
                    r.Item("nom_objeto_segurado") = txtObjetoSegurado.Text
                    r.Item("nom_evento_sinistro") = Eo_outPassos(0).evento
                    r.Item("valor_prejuizo") = Me.txtValPrejuizo.Text
                    r.Item("dsc_cobertura") = Me.ddlCobAtingida.SelectedItem.Text
                    r.Item("cod_objeto_segurado") = Eo_outPassos(0).codigo_objeto_segurado
                    r.Item("cod_evento_sinistro") = Eo_outPassos(0).codigo_evento
                    r.Item("cod_cobertura_afetada") = Me.ddlCobAtingida.SelectedValue
                    r.Item("valor_is") = Me.txtValIS.Text
                    r.Item("processa_reintegracao_is") = ViewState("Processa_Reintegracao_Is")

                    r.Item("motivo_reanalise_sinistro_id") = IIf(IsNothing(ViewState("motivo_reanalise_sinistro_id")), "", ViewState("motivo_reanalise_sinistro_id"))
                    r.Item("descricao_motivo_reanalise") = IIf(IsNothing(ViewState("descricao_motivo_reanalise")), "", ViewState("descricao_motivo_reanalise"))
                    r.Item("texto_motivo_reanalise") = IIf(IsNothing(ViewState("texto_motivo_reanalise")), "", ViewState("texto_motivo_reanalise"))


                    destino.Rows.Add(r)

                End If


        End If

        End If

        Me.grdPropostasAviso.DataSource = destino
        Me.grdPropostasAviso.DataBind()

        'guarda no viewstate para efetuar a gravação


        ViewState("destino") = destino

        Me.DestinoPropostasDoAviso = destino

        With propostas_do_aviso

            .Attributes.CssStyle.Clear()
            .Attributes.CssStyle.Add("width", "790px")
            .Attributes.CssStyle.Add("height", "236px")
            .Attributes.CssStyle.Add("display", "block")

        End With

        Me.listaPropostasExcluir = ""

        For i = 0 To grdPropostasAviso.Rows.Count - 1

            Me.listaPropostasExcluir = Me.listaPropostasExcluir + "," + grdPropostasAviso.Rows(i).Cells(2).Text

        Next

        If Mid(listaPropostasExcluir.Trim, 1, 1) = "," Then

            Me.listaPropostasExcluir = Mid(listaPropostasExcluir.Trim, 2, listaPropostasExcluir.Trim.Length)

        End If

        CarregaPropostas(grdPropostasAviso.Rows.Count > 0)


        Me.grdDescrObjSegurado.DataSource = Nothing
        Me.grdDescrObjSegurado.DataBind()


        Me.ddlCobAtingida.Enabled = True
        Me.ddlCobAtingida.Items.Clear()
        'VERIFICAR
        Me.ddlCobAtingida.SelectedIndex = -1
        Me.txtObjetoSegurado.Text = ""
        Me.txtValIS.Text = ""
        Me.txtValPrejuizo.Text = ""
        Me.txtFranquia.Text = ""

        HabilitaControles(False)

        If Me.grdPropostas.Rows.Count = 0 Then
            grdPropostas.Visible = False
            grdDescrObjSegurado.Visible = False


        Else
            grdPropostas.Visible = True
            grdDescrObjSegurado.Visible = True
            txtObjetoSegurado.Visible = True
        End If

        Me.btnAlterar.Enabled = (Me.grdPropostasAviso.Rows.Count > 0)




    End Sub

    Protected Sub btnAlterar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAlterar.Click

        Dim objEo_In As New SEGS8918_SPD_EO_IN
        Dim objEoBus As New SEGS8918_SPD_Business

        Dim destino As New DataTable

        destino = ViewState("destino")

        If Me.grdPropostasAviso.Rows.Count = 1 Then

            objEo_In.Sinistro_Id_Passos = Me.Sinistro_ID
            objEo_In.Proposta_Id = Integer.Parse(Me.grdPropostasAviso.Rows(0).Cells(2).Text)

            If InStr(Me.listaPropostasExcluir, objEo_In.Proposta_Id & ",") > 0 Then

                Me.listaPropostasExcluir = Me.listaPropostasExcluir.Replace(objEo_In.Proposta_Id & ",", "")

            ElseIf InStr(Me.listaPropostasExcluir, objEo_In.Proposta_Id) > 0 Then

                Me.listaPropostasExcluir = Me.listaPropostasExcluir.Replace(objEo_In.Proposta_Id, "")

            End If

            Me.listaPropostasExcluir = Me.listaPropostasExcluir.Replace(Me.grdPropostasAviso.Rows(0).Cells(0).Text & ",", "")

            objEoBus.Exclui(objEo_In)

            destino.Rows(0).Delete()

        Else

            If Me.grdPropostasAviso.SelectedIndex = -1 Then

                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), _
                                                        Guid.NewGuid().ToString(), _
                                                        "alert('Selecione um item na lista Propostas do Aviso.')", _
                                                        True)
                Exit Sub

            Else

                destino.Rows(Me.grdPropostasAviso.SelectedRow.RowIndex).Delete()

                objEo_In.Sinistro_Id_Passos = Me.Sinistro_ID
                objEo_In.Proposta_Id = Integer.Parse(Me.grdPropostasAviso.SelectedRow.Cells(2).Text)

                objEoBus.Exclui(objEo_In)






            End If

        End If

        'reseta o estado dos campos
        'Me.ddlCobAtingida.Enabled = True
        'Me.ddlCobAtingida.SelectedIndex = 0
        'Me.txtObjetoSegurado.Text = ""
        'Me.txtValIS.Text = ""
        'Me.txtValPrejuizo.Text = ""
        'Me.txtFranquia.Text = ""

        If destino.Rows.Count > 0 Then

            Me.grdPropostasAviso.DataSource = destino

        Else

            Me.grdPropostasAviso.DataSource = Nothing

        End If

        Me.grdPropostasAviso.DataBind()


        Me.listaPropostasExcluir = ""

        For i As Integer = 0 To grdPropostasAviso.Rows.Count - 1

            Me.listaPropostasExcluir = Me.listaPropostasExcluir + "," + grdPropostasAviso.Rows(i).Cells(2).Text

        Next

        If Mid(listaPropostasExcluir.Trim, 1, 1) = "," Then

            Me.listaPropostasExcluir = Mid(listaPropostasExcluir.Trim, 2, listaPropostasExcluir.Trim.Length)

        End If

        'Exclui os itens da grid

        Me.grdDescrObjSegurado.DataSource = Nothing
        Me.grdDescrObjSegurado.DataBind()

        CarregaPropostas()

        If Me.grdPropostas.Rows.Count = 0 Then

            grdPropostas.Visible = False
            grdDescrObjSegurado.Visible = False
            txtObjetoSegurado.Visible = False

        Else

            grdPropostas.Visible = True
            txtObjetoSegurado.Visible = True

        End If

        ddlCobAtingida.Enabled = True
        ddlCobAtingida.Items.Clear()
        txtFranquia.Text = ""
        Me.radReintegracao.Items(0).Value = False
        Me.radReintegracao.Items(1).Value = False

        Me.btnAlterar.Enabled = (grdPropostasAviso.Rows.Count > 0)



    End Sub

    Protected Sub btnContinuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinuar.Click

        If Me.Gravar() Then

            Me.Avancar()

        Else
        End If

    End Sub


    Protected Sub grdPropostas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdPropostas.SelectedIndexChanged

        Try

            objLogEoIn.Teste = Date.Now.ToString() & " grdPropostas_SelectedIndexChanged - INÍCIO"
            objLogBus.Insere(objLogEoIn)

            Dim processa_reintegracao_is As String = ""

            Dim Proposta As Integer

            Dim Eo_outPassos As List(Of SEGS8744_SPS_EO_OUT)

            Eo_outPassos = ViewState("EoPassos")

            Me.Proposta_ID = Me.grdPropostas.SelectedRow.Cells(1).Text

            If Me.Proposta_ID = "0" Then


                Me.Processa_Reintegracao_Is = ""
                ViewState("Processa_Reintegracao_Is") = Me.Processa_Reintegracao_Is
                ddlCobAtingida.Enabled = True
                btnAdicionar.Enabled = True
                txtFranquia.ReadOnly = True

                radReintegracao.Enabled = False
                radReintegracao.Visible = False


                Call carregaCoberturas(Me.Proposta_ID, _
                                        Format(Eo_outPassos(0).data_ocorrencia, "yyyyMMdd"), _
                                        Eo_outPassos(0).codigo_evento, _
                                        0, _
                                        0, _
                                        0, _
                                        Eo_outPassos(0).codigo_objeto_segurado)


                'txtValPrejuizo.Focus()

                With obj_segurado

                    desc_objeto_segurado.Attributes.CssStyle.Clear()
                    desc_objeto_segurado.Attributes.CssStyle.Add("display", "none")
                    .Attributes.CssStyle.Add("width", "790px")
                    .Attributes.CssStyle.Add("height", "auto")
                    .Attributes.CssStyle.Add("display", "block")

                End With

                txtObjetoSegurado.Focus()

            Else

                Dim lstObjEoOut_Propostas As New List(Of SEGS8892_SPS_EO_OUT)

                lstObjEoOut_Propostas = ViewState("prop")

                Dim objOut As New SEGS8892_SPS_EO_OUT

                objOut.SINISTPROPOSTA = Me.Proposta_ID

                Dim objOutSEGS8892_SPS As SEGS8892_SPS_EO_OUT = lstObjEoOut_Propostas.Find(New Predicate(Of SEGS8892_SPS_EO_OUT) _
                                                    (AddressOf objOut.findProposta))


                If Me.grdPropostas.SelectedRow.Cells(1).Text = "0" Then

                    radReintegracao.Enabled = False
                    radReintegracao.Visible = False


                    Me.Processa_Reintegracao_Is = ""

                    Call carregaCoberturas(Me.Proposta_ID, _
                                            Format(Eo_outPassos(0).data_ocorrencia, "yyyyMMdd"), _
                                            Eo_outPassos(0).codigo_evento, _
                                            objOutSEGS8892_SPS.SINISTPRODUTO, _
                                            objOutSEGS8892_SPS.SINISTRAMO, _
                                            objOutSEGS8892_SPS.SINISTSUBRAMO, _
                                            Eo_outPassos(0).codigo_objeto_segurado)

                    ddlCobAtingida.Enabled = True
                    btnAdicionar.Enabled = True



                Else

                    If VerificaReintegracaoIS(Long.Parse(Me.grdPropostas.SelectedRow.Cells(3).Text), _
                                                Long.Parse(Me.grdPropostas.SelectedRow.Cells(4).Text)) Then


                        reintegracao_is.Attributes.CssStyle.Add("display", "block")
                        radReintegracao.Enabled = True
                        radReintegracao.Visible = True

                        If Me.Processa_Reintegracao_Is = "" Then

                            Me.Processa_Reintegracao_Is = "S"

                        End If

                    Else

                        reintegracao_is.Attributes.CssStyle.Add("display", "none")


                        radReintegracao.Enabled = False
                        radReintegracao.Visible = False

                    End If


                    Dim objEo_In As New SEGS8910_SPS_EO_IN

                    Dim lstobjEo_Out As New List(Of SEGS8910_SPS_EO_OUT)

                    objEo_In.Proposta_Id = Decimal.Parse(Me.Proposta_ID)

                    Dim dtDescrObjSegurado As New DataTable


                    dtDescrObjSegurado = ObjSegurado.PreencheDescricaoObjSegurado(Me.Proposta_ID)

                    grdDescrObjSegurado.DataSource = dtDescrObjSegurado
                    grdDescrObjSegurado.DataBind()
                    grdDescrObjSegurado.Visible = True
                    If dtDescrObjSegurado.Columns.Count > 5 Then

                        Me.panel_desc_objeto_segurado.ScrollBars = ScrollBars.Horizontal
                        grdDescrObjSegurado.Width = "2000"

                    Else

                        Me.panel_desc_objeto_segurado.ScrollBars = ScrollBars.Vertical
                        grdDescrObjSegurado.Width = "650"

                    End If



                    Session("objOutSEGS8892_SPS") = objOutSEGS8892_SPS
                    Dim Sinistro As New SEGL0343.GeraSinistroRE_Business
                    Call Sinistro.VerificaPropostaAgricola(Me.Proposta_ID, objOutSEGS8892_SPS.SINISTPRODUTO, 0, "")
                    If Sinistro.sProdutoAgricola <> "REANALISE" Then

                        'Aviso.ObterQuestionarioSinistro(grdPropostas.get_TextMatrix(grdPropostas.RowSel, 3))

                        Dim objEoIn_ObterQuestionarioSinistro As New SEGS8923_SPS_EO_IN

                        Dim lstObjEoOut_ObterQuestionarioSinistro As New List(Of SEGS8923_SPS_EO_OUT)

                        objEoIn_ObterQuestionarioSinistro.Produto_Id = objOutSEGS8892_SPS.SINISTPRODUTO

                        lstObjEoOut_ObterQuestionarioSinistro = ObjSegurado.ObterQuestionarioSinistro(objEoIn_ObterQuestionarioSinistro)

                        ViewState("lstObjEoOut_ObterQuestionarioSinistro") = lstObjEoOut_ObterQuestionarioSinistro

                        If lstObjEoOut_ObterQuestionarioSinistro.Count > 0 Then

                            Dim intProdutoQuestionario As Integer = lstObjEoOut_ObterQuestionarioSinistro(0).produtoQuestionario




                            If intProdutoQuestionario > 0 Then

                                objLogEoIn.Teste = Date.Now.ToString() & " intProdutoQuestionario - " & intProdutoQuestionario.ToString
                                objLogBus.Insere(objLogEoIn)

                                ProtutoQuestionario = intProdutoQuestionario

                                If Not MontarQuestionarioProduto(intProdutoQuestionario) Then

                                    objLogEoIn.Teste = Date.Now.ToString() & " Não Montou o Questinário - " & intProdutoQuestionario.ToString
                                    objLogBus.Insere(objLogEoIn)


                                    Exit Sub

                                End If
                            End If

                        End If

                        Call carregaCoberturas(Me.Proposta_ID, _
                                                Format(Eo_outPassos(0).data_ocorrencia, "yyyyMMdd"), _
                                                Eo_outPassos(0).codigo_evento, _
                                                objOutSEGS8892_SPS.SINISTPRODUTO, _
                                                objOutSEGS8892_SPS.SINISTRAMO, _
                                                objOutSEGS8892_SPS.SINISTSUBRAMO, _
                                                Eo_outPassos(0).codigo_objeto_segurado)

                        Proposta = Integer.Parse(objOutSEGS8892_SPS.SINISTPROPOSTA)

                        ddlCobAtingida.Enabled = True
                        'ddlCobAtingida.SelectedIndex = -1
                        btnAdicionar.Enabled = True
                        txtFranquia.ReadOnly = True
                        txtValPrejuizo.Focus()

                        ViewState("Processa_Reintegracao_Is") = Me.Processa_Reintegracao_Is


                        ddlCobAtingida_SelectedIndexChanged(ddlCobAtingida, Nothing)

                        'reintegracao_is.Attributes.CssStyle.Add("display", "block")

                    Else

                        Call carregaCoberturas(Me.Proposta_ID, _
                                                Format(Eo_outPassos(0).data_ocorrencia, "yyyyMMdd"), _
                                                Eo_outPassos(0).codigo_evento, _
                                                objOutSEGS8892_SPS.SINISTPRODUTO, _
                                                objOutSEGS8892_SPS.SINISTRAMO, _
                                                objOutSEGS8892_SPS.SINISTSUBRAMO, _
                                                Eo_outPassos(0).codigo_objeto_segurado)

                        Dim objEo_In_consultar_aviso_sinistro_re_sps As New consultar_aviso_sinistro_re_sps_EO_IN

                        'Dim objBus_consultar_aviso_sinistro_re_sps As New consultar_aviso_sinistro_re_sps_Business

                        Dim lstObjEo_Out_consultar_aviso_sinistro_re_sps As New List(Of consultar_aviso_sinistro_re_sps_EO_OUT)

                        objEo_In_consultar_aviso_sinistro_re_sps.Sinistro_Id = Eo_outPassos(0).sinistro_re_id

                        lstObjEo_Out_consultar_aviso_sinistro_re_sps = ObjGeral.ConsultarAvisoPropostas(objEo_In_consultar_aviso_sinistro_re_sps)

                        If lstObjEo_Out_consultar_aviso_sinistro_re_sps.Count > 0 Then

                            With Me.ddlCobAtingida

                                .Items.Clear()

                                .Items.Add(New ListItem(lstObjEo_Out_consultar_aviso_sinistro_re_sps(0).nome_cobertura, lstObjEo_Out_consultar_aviso_sinistro_re_sps(0).tp_cobertura_id))

                                ddlCobAtingida.SelectedValue = lstObjEo_Out_consultar_aviso_sinistro_re_sps(0).tp_cobertura_id ' Rs("nome_cobertura")
                                ddlCobAtingida.Enabled = False


                            End With

                            txtValPrejuizo.Text = lstObjEo_Out_consultar_aviso_sinistro_re_sps(0).val_estimativa ' Rs("val_estimativa")

                            If lstObjEo_Out_consultar_aviso_sinistro_re_sps(0).val_estimativa = 0 Then

                                Dim objEo_InSEGS8039_SPS As New SEGS8039_SPS_EO_IN

                                Dim lstObjEo_OutSEGS8039_SPS As New List(Of SEGS8039_SPS_EO_OUT)

                                objEo_InSEGS8039_SPS.Sinistro_Id = Eo_outPassos(0).sinistro_re_id     'sSinistro_ID

                                lstObjEo_OutSEGS8039_SPS = ObjSegurado.ObterPrecoEstimadoPrejuizo(objEo_InSEGS8039_SPS)

                                If lstObjEo_OutSEGS8039_SPS.Count > 0 Then

                                    txtValPrejuizo.Text = lstObjEo_OutSEGS8039_SPS(0).val_estimado

                                End If

                            End If

                        End If

                        txtValPrejuizo.Enabled = True

                        Dim objEo_InSEGS7752_SPS As New SEGS7752_SPS_EO_IN

                        Dim lstobjEo_OutSEGS7752_SPS As New List(Of SEGS7752_SPS_EO_OUT)

                        Dim objItemEo_OutSEGS7752_SPS As New SEGS7752_SPS_EO_OUT

                        objEo_InSEGS7752_SPS.Dt_Ocorrencia = Eo_outPassos(0).data_ocorrencia

                        objEo_InSEGS7752_SPS.Proposta_Id = Decimal.Parse(Me.Proposta_ID)

                        lstobjEo_OutSEGS7752_SPS = ObjSegurado.ObterValorImportanciaSegurada(objEo_InSEGS7752_SPS)

                        For Each objItemEo_OutSEGS7752_SPS In lstobjEo_OutSEGS7752_SPS

                            If Not IsNothing(lstObjEo_Out_consultar_aviso_sinistro_re_sps) Then

                                If lstObjEo_Out_consultar_aviso_sinistro_re_sps.Count > 0 Then

                                    If objItemEo_OutSEGS7752_SPS.tp_cobertura_id = lstObjEo_Out_consultar_aviso_sinistro_re_sps(0).tp_cobertura_id Then

                                        txtValIS.Text = objItemEo_OutSEGS7752_SPS.val_is

                                    End If

                                End If

                            End If

                        Next

                        Dim objEoInSEGS7752_SPS As New SEGS7752_SPS_EO_IN

                        objEoInSEGS7752_SPS.Proposta_Id = Me.Proposta_ID

                        objEoInSEGS7752_SPS.Dt_Ocorrencia = Eo_outPassos(0).data_ocorrencia

                        Dim objBusSEGS7752_SPS As New SEGS7752_SPS_Business


                        lstobjEo_OutSEGS7752_SPS = objBusSEGS7752_SPS.seleciona(objEoInSEGS7752_SPS)

                        If lstobjEo_OutSEGS7752_SPS.Count > 0 Then

                            ViewState("lstObjEo_OutSEGS7752_SPS") = lstobjEo_OutSEGS7752_SPS

                            If Not IsNothing(lstObjEo_Out_consultar_aviso_sinistro_re_sps) Then

                                If lstObjEo_Out_consultar_aviso_sinistro_re_sps.Count > 0 Then

                                    txtFranquia.Text = ObjSegurado.ObtemTexto_franquia(lstObjEo_Out_consultar_aviso_sinistro_re_sps(0).tp_cobertura_id, _
                                                                                    lstobjEo_OutSEGS7752_SPS)

                                End If

                            End If


                        End If


                        ViewState("Processa_Reintegracao_Is") = Me.Processa_Reintegracao_Is


                        Me.CarregaComboMotReanalise()

                        Me.mpeMotivoReanalise.Show()
                        'Me.mpeMotivoReanalise.Visible = True

                        Me.btnAdicionar.Enabled = True


                        ddlCobAtingida_SelectedIndexChanged(ddlCobAtingida, Nothing)

                    End If
                End If

            End If

            HabilitaControles(True)
        Catch ex As Exception

            If Not IsNothing(ex.InnerException) Then

                ObjGeral.TrataErro(ex.Message.ToString, "grdPropostas_SelectedIndexChanged  - Erro ao Selecionar Propostas! - " & Me.Sinistro_ID, ex.StackTrace.ToString, ex.InnerException.InnerException.Message.ToString)

            Else

                ObjGeral.TrataErro(ex.Message.ToString, "grdPropostas_SelectedIndexChanged  - Erro ao Selecionar Propostas! - " & Me.Sinistro_ID, ex.StackTrace.ToString, "")

            End If

        End Try


    End Sub

    Protected Sub ddlCobAtingida_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCobAtingida.SelectedIndexChanged


        Try
            Dim Aviso As Object
            Dim cValPrejuizo As Decimal
            Dim oPrejuizo As Object

            Dim Eo_outPassos As List(Of SEGS8744_SPS_EO_OUT)

            Eo_outPassos = ViewState("EoPassos")

            If ddlCobAtingida.SelectedIndex <> -1 Then


                Me.txtValPrejuizo.Focus()

                'ObtemISRE da DLL
                'Aviso = CreateObject("SEGL0144.cls00385")
                'oPrejuizo = CreateObject("SEGL0144.cls00326")

                Dim objEo_InSEGS7752_SPS As New SEGS7752_SPS_EO_IN

                Dim lstobjEo_OutSEGS7752_SPS As New List(Of SEGS7752_SPS_EO_OUT)

                Dim objItemEo_OutSEGS7752_SPS As New SEGS7752_SPS_EO_OUT

                'Dim objBus_SEGS7752_SPS As New SEGS7752_SPS_Business

                objEo_InSEGS7752_SPS.Dt_Ocorrencia = Eo_outPassos(0).data_ocorrencia

                If Me.Proposta_ID = "Aviso sem proposta" Then

                    objEo_InSEGS7752_SPS.Proposta_Id = 0

                Else

                    objEo_InSEGS7752_SPS.Proposta_Id = Decimal.Parse(Me.Proposta_ID)

                End If


                lstobjEo_OutSEGS7752_SPS = ObjSegurado.ObterValorImportanciaSegurada(objEo_InSEGS7752_SPS)


                For Each objItemEo_OutSEGS7752_SPS In lstobjEo_OutSEGS7752_SPS

                    If objItemEo_OutSEGS7752_SPS.tp_cobertura_id = Me.ddlCobAtingida.SelectedValue Then

                        txtValIS.Text = Format(objItemEo_OutSEGS7752_SPS.val_is, "0.00")

                    End If

                Next

                txtFranquia.Text = ObjSegurado.ObtemTexto_franquia(CInt(ddlCobAtingida.SelectedValue), lstobjEo_OutSEGS7752_SPS)

                Dim lstObjEoOut_ObterQuestionarioSinistro As New List(Of SEGS8923_SPS_EO_OUT)

                lstObjEoOut_ObterQuestionarioSinistro = ViewState("lstObjEoOut_ObterQuestionarioSinistro")


                If Not IsNothing(lstObjEoOut_ObterQuestionarioSinistro) Then

                    If lstObjEoOut_ObterQuestionarioSinistro.Count > 0 Then

                        If lstObjEoOut_ObterQuestionarioSinistro(0).produtoQuestionario <> 0 Then

                            cValPrejuizo = ObjSegurado.ObtemValPrejuizoCalculado(Me.Proposta_ID, Double.Parse(txtValIS.Text), BasePages.Usuario.LoginWeb, Me.Sinistro_ID)

                            'cValPrejuizo = oPrejuizo.ObtemValPrejuizoCalculado(grdPropostas.TextMatrix(grdPropostas.RowSel, 1), CCur(mskIS.Text), cUserName, "#aviso_sinistro_tb", oSABL0100)

                        End If

                    End If

                End If


                'vbarbosa - 15/12/2006
                'If produtoQuestionario <> 0 Then
                '    cValPrejuizo = oPrejuizo.ObtemValPrejuizoCalculado(grdPropostas.TextMatrix(grdPropostas.RowSel, 1), CCur(mskIS.Text), cUserName, "#aviso_sinistro_tb", oSABL0100)
                'End If

                'asouza 16.03.06
                If cValPrejuizo <> 0 Then

                    txtValPrejuizo.ReadOnly = True

                Else

                    txtValPrejuizo.ReadOnly = False

                End If

                'fazer o calculo automatico do prejuizo
                'adicionado por Afonso Filho - GPTI
                'DATA: 13/11/2008
                'If Propostas.Count > 0 Then

                '    produtoId = grdPropostas.TextMatrix(grdPropostas.RowSel, 3)

                'Else

                '    produto_id = 0

                'End If

                If Me.grdPropostas.SelectedRow.Cells(3).Text = "1152" Then

                    Dim objEoInSEGS9000 As New SEGS9000_SPS_EO_IN

                    Dim objBusSEGS9000 As New SEGS9000_SPS_Business


                    Dim lstObjEoOutSEGS9000 As New List(Of SEGS9000_SPS_EO_OUT)


                    objEoInSEGS9000.Sinistro_Id_Passos = Me.Sinistro_ID

                    lstObjEoOutSEGS9000 = objBusSEGS9000.seleciona(objEoInSEGS9000)


                    If lstObjEoOutSEGS9000.Count > 0 Then
                        If lstObjEoOutSEGS9000(0).VALOR = 0 Then
                            cValPrejuizo = 0
                        Else
                            cValPrejuizo = (txtValIS.Text * lstObjEoOutSEGS9000(0).VALOR / 100)
                        End If
                        txtValPrejuizo.Text = Format(cValPrejuizo, "0.00")

                    End If

                End If

            End If

        Catch ex As Exception

            ObjGeral.TrataErro(ex.Message.ToString, "ddlCobAtingida_SelectedIndexChanged  - Erro ao Selecionar Coberrtura Atingida! - " & Me.Sinistro_ID, ex.StackTrace.ToString, ex.InnerException.InnerException.Message.ToString)

        End Try



    End Sub

    Protected Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click

        If Me.Gravar() Then
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Dados salvos com sucesso!')", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Erro ao tentar salvar passos!')", True)
        End If

    End Sub

#End Region

#Region "Metodos Objeto Segurado"

    Private Sub Voltar()
        Dim sb As New StringBuilder()

        sb.Append("SEGW0158AnaliseSinistroCA.aspx")
        sb.Append("?PS=" & 6)
        sb.Append("&SINISTRO_ID=" & Me.Sinistro_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())

    End Sub

    Private Sub carregaCoberturas(ByVal proposta As Integer, _
                                    ByVal dtOcorrencia As String, _
                                    ByVal Evento_Sinistro_Id As Integer, _
                                    ByVal Produto_Id As Integer, _
                                    ByVal Ramo_Id As Integer, _
                                    ByVal Subramo_Id As Integer, _
                                    ByVal Tp_Cobertura_Id As Integer, _
                                    Optional ByVal cod_objeto_segurado As Integer = 0)

        With Me.ddlCobAtingida

            .Items.Clear()

            Dim objEo_InSEGS7752_SPS As New SEGS7752_SPS_EO_IN

            Dim lstObjEo_OutSEGS7752_SPS As New List(Of SEGS7752_SPS_EO_OUT)

            Dim objItemSEGS7752_SPS As New SEGS7752_SPS_EO_OUT

            objEo_InSEGS7752_SPS.Proposta_Id = proposta
            Dim Ano As String
            Ano = dtOcorrencia.Substring(0, 4)
            Dim mes As String
            mes = dtOcorrencia.Substring(4, 2)
            Dim dia As String
            dia = dtOcorrencia.Substring(6, 2)

            objEo_InSEGS7752_SPS.Dt_Ocorrencia = Date.Parse(dia & "/" & mes & "/" & Ano)

            lstObjEo_OutSEGS7752_SPS = ObjSegurado.ObterValorImportanciaSegurada(objEo_InSEGS7752_SPS)

            Session("lstObjEo_OutSEGS7752_SPS") = lstObjEo_OutSEGS7752_SPS

            '.Items.Add(New ListItem("", 0))

            For Each objItemSEGS7752_SPS In lstObjEo_OutSEGS7752_SPS

                If ExisteEstimativa(Produto_Id, Evento_Sinistro_Id, objItemSEGS7752_SPS.tp_cobertura_id, Ramo_Id, Subramo_Id) Then

                    .Items.Add(New ListItem(objItemSEGS7752_SPS.nome, objItemSEGS7752_SPS.tp_cobertura_id))

                End If

            Next


            If .Items.Count = 0 Then

                .Items.Add(New ListItem("COBERTURA GENÉRICA", 1))

            End If

            txtValPrejuizo.Enabled = True
            Me.ddlCobAtingida.SelectedIndex = -1
            txtFranquia.Text = ""
            txtValIS.Text = "0,00"
            txtValPrejuizo.Text = "0,00"

            'Me.grdDescrObjSegurado.DataSource = Nothing
            'Me.grdDescrObjSegurado.DataBind()

        End With

    End Sub

    Private Sub CarregarDados()

        Dim objOut_8744 As New List(Of EO.SEGS8744_SPS_EO_OUT)
        Dim objIn_8744 As New SEGS8744_SPS_EO_IN

        Dim objGeral As New SEGW0158Geral_TK()

        objIn_8744.Sinistro_Id = Me.Sinistro_ID

        objOut_8744 = objGeral.Buscar(objIn_8744)

        'PreencheCampos(objOut_8744)

    End Sub

    Private Sub CarregaPropostas(Optional ByVal blnPropostasDoAviso As Boolean = False)

        Dim i As Short
        'Dim listaPropostas As String = ""
        'Dim listaPropostasExcluir As String = ""
        Dim excluiDoGrid As String
        Dim lproposta() As String


        Dim Eo_outPassos As List(Of SEGS8744_SPS_EO_OUT)

        Eo_outPassos = ViewState("EoPassos")

        Dim objEoin_Propostas As New SEGS8892_SPS_EO_IN

        Dim lstObjEoOut_Propostas As New List(Of SEGS8892_SPS_EO_OUT)

        Dim itemObjEoOut_Propostas As New SEGS8892_SPS_EO_OUT

        objEoin_Propostas.Sinistro_Id_Passos = Me.Sinistro_ID

        lstObjEoOut_Propostas = ObjSegurado.ObtemListaPropostas(objEoin_Propostas)

        'listaPropostas = ""

        'MONTA A LISTA DE PROPOSTAS SELECIONADAS NA TELA ANTERIOR COMO "AVSR"
        'For Each itemObjEoOut_Propostas In lstObjEoOut_Propostas

        '    If lstObjEoOut_Propostas.Count = 1 And itemObjEoOut_Propostas.SINISTPROPOSTA <> "0" Then

        '        'listaPropostas = itemObjEoOut_Propostas.proposta_id
        '        listaPropostas = itemObjEoOut_Propostas.SINISTPROPOSTA

        '    ElseIf itemObjEoOut_Propostas.SINISTPROPOSTA <> "0" Then

        '        listaPropostas = listaPropostas & ", " + itemObjEoOut_Propostas.SINISTPROPOSTA

        '    End If

        'Next


        listaPropostas = ""

        For Each itemObjEoOut_Propostas In lstObjEoOut_Propostas

            If Mid(itemObjEoOut_Propostas.AVISO, 1, 1) = "A" Or _
                Mid(itemObjEoOut_Propostas.AVISO, 1, 1) = "R" Then

                If lstObjEoOut_Propostas.Count = 1 Then

                    'listaPropostas = itemObjEoOut_Propostas.proposta_id
                    listaPropostas = itemObjEoOut_Propostas.PROPOSTA_AB

                Else 
                    listaPropostas = listaPropostas & ", " + itemObjEoOut_Propostas.PROPOSTA_AB

                End If

            End If

        Next

        If Mid(listaPropostas, 1, 1) = "," Then

            listaPropostas = Mid(listaPropostas, 2, Len(listaPropostas) - 1).Trim

        End If

        If lstObjEoOut_Propostas.Count > 0 Then

            ViewState("prop") = lstObjEoOut_Propostas

        End If

        If Not IsNothing(listaPropostas) And Not IsNothing(listaPropostasExcluir) Then

            If listaPropostas.Trim.Length = 0 And listaPropostasExcluir.Length = 0 Then

                ScriptManager.RegisterClientScriptBlock(Page, _
                                                        Me.GetType(), _
                                                        Guid.NewGuid().ToString(), _
                                                        "alert('Não há proposta a serem exibidas')", _
                                                        True)

                Exit Sub


            End If


        End If



        Dim lstObjEoOutSEGS8811_SPS As New List(Of SEGS8811_SPS_EO_OUT)

        If lstObjEoOut_Propostas.Count > 0 And Eo_outPassos(0).ind_sem_proposta = False Then

            Dim objEoInSEGS8811_SPS As New SEGS8811_SPS_EO_IN


            objEoInSEGS8811_SPS.Listapropostas = listaPropostas
            objEoInSEGS8811_SPS.Listapropostasexcluir = listaPropostasExcluir

            'objEoInSEGS8811_SPS.Listapropostas

            lstObjEoOutSEGS8811_SPS = ObjSegurado.ConsultaPropostas(objEoInSEGS8811_SPS)

            Me.grdPropostas.DataSource = lstObjEoOutSEGS8811_SPS

            'armazena conteúdo dos dados na sessão para paginamento posterior
            ViewState("gridPropostas") = lstObjEoOutSEGS8811_SPS 'verificar 

            Me.grdPropostas.DataBind()

        ElseIf Eo_outPassos(0).ind_sem_proposta = True Then


            Dim objEoOutSEGS8811_SPS As New SEGS8811_SPS_EO_OUT

            ListarPropostaAvisoVoltarContinuar()

            If grdPropostasAviso.Rows.Count = 0 And grdPropostasAviso.Rows.Count = 0 Then

                With objEoOutSEGS8811_SPS

                    .proposta_id = 0
                    .proposta_bb = 0
                    .produto_id = 0
                    .ramo_id = 0
                    .apolice_id = 0
                    .nome = "Aviso Sem proposta"
                    .subramo_id = 0

                End With


                lstObjEoOutSEGS8811_SPS.Add(objEoOutSEGS8811_SPS)

                Me.grdPropostas.DataSource = lstObjEoOutSEGS8811_SPS

                'armazena conteúdo dos dados na sessão para paginamento posterior
                ViewState("gridPropostas") = lstObjEoOutSEGS8811_SPS 'verificar 


            Else


                Me.grdPropostas.DataSource = Nothing


            End If


            Me.grdPropostas.DataBind()

        End If


        If blnPropostasDoAviso = False Then

            'ControlaExibicaoFieldPropostas(lstObjEoOut_Propostas.Count > 0)

            ControlaExibicaoFieldPropostas(Eo_outPassos(0).ind_sem_proposta)

        End If

    End Sub

    Private Sub ControlaExibicaoFieldPropostas(ByVal blnTemPropostas As Boolean)
        Dim Eo_outPassos As List(Of SEGS8744_SPS_EO_OUT)

        Eo_outPassos = ViewState("EoPassos")

        If Eo_outPassos(0).ind_sem_proposta = True Then

            With obj_segurado

                desc_objeto_segurado.Attributes.CssStyle.Clear()
                desc_objeto_segurado.Attributes.CssStyle.Add("display", "none")

                .Attributes.CssStyle.Add("width", "790px")
                .Attributes.CssStyle.Add("height", "auto")
                .Attributes.CssStyle.Add("display", "block")


            End With

        Else

            With desc_objeto_segurado

                obj_segurado.Attributes.CssStyle.Clear()
                obj_segurado.Attributes.CssStyle.Add("display", "none")
                .Attributes.CssStyle.Add("width", "790px")
                .Attributes.CssStyle.Add("height", "auto")
                .Attributes.CssStyle.Add("display", "block")

                'Me.Panel1.ScrollBars = ScrollBars.None

            End With

        End If


        If grdPropostasAviso.Rows.Count = 0 Then

            propostas_do_aviso.Attributes.CssStyle.Clear()
            propostas_do_aviso.Attributes.CssStyle.Add("display", "none")

           
        End If
        reintegracao_is.Attributes.CssStyle.Add("display", "none")



    End Sub

    Private Function Gravar() As Boolean

        Try
            Gravar = False

            If grdPropostasAviso.Rows.Count = 0 Then

                ScriptManager.RegisterClientScriptBlock(Page, _
                                                        Me.GetType(), _
                                                        Guid.NewGuid().ToString(), _
                                                        "alert('Adicione uma proposta antes de prosseguir.')", _
                                                        True)

                Exit Function

            End If


            Dim objEo_In As New SEGS8918_SPD_EO_IN

            objEo_In.Sinistro_Id_Passos = Me.Sinistro_ID
            'objEo_In.Proposta_Id = 

            Dim objEoBus As New SEGS8918_SPD_Business

            objEoBus.Exclui(objEo_In)

            'atualização dos campos - tavbela de passos
            Dim Eo_outPassos As List(Of SEGS8744_SPS_EO_OUT)

            Eo_outPassos = ViewState("EoPassos")

            Dim objIn_8747 As New SEGS8747_SPU_EO_IN()

            If Not IsNothing(Eo_outPassos) Then

                objIn_8747 = ObjGeral.PreencheTodosCamposU(Eo_outPassos(0))

            End If


            objIn_8747.Usuario = Usuario.LoginWeb


            If Not IsNothing(Me.listaPropostas) Then
                objIn_8747.lista_propostas = IIf(IsNothing(Me.listaPropostas) = True, "", Me.listaPropostas)
            End If

            If Not IsNothing(Me.listaPropostasExcluir) Then
                objIn_8747.lista_propostas_excluir = IIf(IsNothing(Me.listaPropostasExcluir) = True, "", Me.listaPropostasExcluir)
            End If

            ObjGeral.Atualizar(objIn_8747)

            'fim

            Dim i As Integer
            Dim destino As New DataTable

            Dim column As DataColumn
            Dim row As DataRow

            destino = ViewState("destino")

            If Not IsNothing(destino) Then

                Dim currentRows() As DataRow = destino.Select("", Nothing, DataViewRowState.CurrentRows)

                For Each row In currentRows

                    Dim objEoInSEGS8917_SPI As New SEGS8917_SPI_EO_IN

                    Dim objEoBusSEGS8917_SPI As New SEGS8917_SPI_Business

                    objEoInSEGS8917_SPI.Sinistro_Id_Passos = Me.Sinistro_ID

                    For Each column In destino.Columns

                        If column.ColumnName = "proposta_id" Then

                            objEo_In.Proposta_Id = Integer.Parse(row(column).ToString)
                            objEoBus.Exclui(objEo_In)

                        End If

                        Select Case column.ColumnName

                            Case "proposta_id"

                                objEoInSEGS8917_SPI.Proposta_Id = Integer.Parse(row(column).ToString)

                            Case "proposta_bb"

                                If Integer.Parse(row(column).ToString) = 0 And Eo_outPassos(0).ind_sem_proposta = True Then

                                    If Not IsNothing(Eo_outPassos(0).proposta_bb_segurado) Then

                                        objEoInSEGS8917_SPI.Proposta_Bb = Integer.Parse(Eo_outPassos(0).proposta_bb_segurado)

                                    End If



                                Else

                                    objEoInSEGS8917_SPI.Proposta_Bb = Integer.Parse(row(column).ToString)

                                End If


                            Case "cod_produto"

                                objEoInSEGS8917_SPI.Cod_Produto = Integer.Parse(row(column).ToString)

                            Case "nom_produto"

                                objEoInSEGS8917_SPI.Nom_Produto = row(column).ToString

                            Case "apolice_id"

                                objEoInSEGS8917_SPI.Apolice_Id = Integer.Parse(row(column).ToString)

                            Case "ramo_id"

                                objEoInSEGS8917_SPI.Ramo_Id = Integer.Parse(row(column).ToString)

                            Case "nom_objeto_segurado"

                                objEoInSEGS8917_SPI.Nom_Objeto_Segurado = row(column).ToString

                            Case "nom_evento_sinistro"

                                objEoInSEGS8917_SPI.Nom_Evento_Sinistro = row(column).ToString

                            Case "valor_prejuizo"

                                objEoInSEGS8917_SPI.Valor_Prejuizo = Decimal.Parse(row(column).ToString)

                            Case "dsc_cobertura"

                                objEoInSEGS8917_SPI.Dsc_Cobertura = row(column).ToString

                            Case "cod_objeto_segurado"

                                objEoInSEGS8917_SPI.Cod_Objeto_Segurado = Integer.Parse(row(column).ToString)

                            Case "cod_evento_sinistro"

                                objEoInSEGS8917_SPI.Cod_Evento_Sinistro = Integer.Parse(row(column).ToString)

                            Case "cod_cobertura_afetada"

                                objEoInSEGS8917_SPI.Cod_Cobertura_Afetada = Integer.Parse(row(column).ToString)

                            Case "valor_is"

                                objEoInSEGS8917_SPI.Valor_Is = Decimal.Parse(row(column).ToString)

                            Case "processa_reintegracao_is"

                                objEoInSEGS8917_SPI.Processa_Reintegracao_Is = row(column).ToString


                            Case "motivo_reanalise_sinistro_id"


                                If IsNothing(ViewState("motivo_reanalise_sinistro_id")) Then

                                    objEoInSEGS8917_SPI.Motivo_Reanalise_Sinistro_id = 0

                                Else

                                    objEoInSEGS8917_SPI.Motivo_Reanalise_Sinistro_id = Integer.Parse(ViewState("motivo_reanalise_sinistro_id"))

                                End If


                            Case "descricao_motivo_reanalise"

                                If IsNothing(ViewState("descricao_motivo_reanalise")) Then

                                    objEoInSEGS8917_SPI.descricao_motivo_reanalise = ""

                                Else

                                    objEoInSEGS8917_SPI.descricao_motivo_reanalise = ViewState("descricao_motivo_reanalise")

                                End If


                            Case "texto_motivo_reanalise"

                                If IsNothing(ViewState("texto_motivo_reanalise")) Then

                                    objEoInSEGS8917_SPI.texto_motivo_reanalise = ""

                                Else

                                    objEoInSEGS8917_SPI.texto_motivo_reanalise = ViewState("texto_motivo_reanalise")

                                End If

                        End Select

                    Next

                    objEoBusSEGS8917_SPI.Insere(objEoInSEGS8917_SPI)

                Next

            End If

            Gravar = True

        Catch ex As Exception

            ObjGeral.TrataErro(ex.Message.ToString, "Gravar Passos Objeto Segurado  - Erro ao Gravar Passos! - " & Me.Sinistro_ID, ex.StackTrace.ToString, ex.InnerException.InnerException.Message.ToString)

        End Try



    End Function

    Private Sub Avancar()

        Dim cProdutoBBProtecao As String = ""
        cProdutoBBProtecao = ObjGeral.CarregaProdutoBBProtecaoVida()

        If grdPropostas.Rows.Count > 0 Then
            For i As Integer = 0 To grdPropostasAviso.Rows.Count - 1
                'If InStr(1, cProdutoBBProtecao, grdPropostasAviso.TextMatrix(CInt(i), 3), vbTextCompare) Then
                If ObjGeral.VerificarProdutoBB(cProdutoBBProtecao, grdPropostasAviso.Rows(i).Cells(4).Text) Then

                    ScriptManager.RegisterClientScriptBlock(Page, _
                                                            Me.GetType(), _
                                                            Guid.NewGuid().ToString(), _
                                                            "alert('Devem ser selecionadas todas as propostas do BB Proteção.')", _
                                                            True)

                    Exit Sub

                End If

            Next i

        End If


        ObjGeral.AtualizaPasso(Me.Sinistro_ID, 6)

        Dim sb As New StringBuilder()
        sb.Append("SEGW0158OutrosSeguros.aspx")
        sb.Append("?SINISTRO_ID=" + IIf(IsNothing(Me.Sinistro_ID), "", Me.Sinistro_ID.ToString))
        sb.Append("&SLinkSeguro=" + Request("SLinkSeguro"))
        Response.Redirect(sb.ToString())


    End Sub

    Private Sub ListarPropostaAvisoVoltarContinuar()

        'Dim Eo_out As List(Of SEGS8744_SPS_EO_OUT)

        'Eo_out = ViewState("EoPassos")


        If Not IsNothing(Me.listaPropostasExcluir) Then

            If Me.listaPropostasExcluir.Trim.Length > 0 Then

                'propostas_do_aviso.Attributes.CssStyle.Add("display", "block")

                With propostas_do_aviso

                    .Attributes.CssStyle.Clear()
                    .Attributes.CssStyle.Add("width", "790px")
                    .Attributes.CssStyle.Add("height", "236px")
                    .Attributes.CssStyle.Add("display", "block")

                End With

                'propostas_do_aviso.Attributes.CssStyle.Add("style", "width: 790px; height: 130px;")


                grdPropostasAviso.Visible = True


                Dim objEoInSEGS8915 As New SEGS8915_SPS_EO_IN

                objEoInSEGS8915.Sinistro_Id_Passos = Me.Sinistro_ID

                Dim objBusSEGS8915 As New SEGS8915_SPS_Business

                Dim lstObjEoOutSEGS8915 As New List(Of SEGS8915_SPS_EO_OUT)

                lstObjEoOutSEGS8915 = objBusSEGS8915.seleciona(objEoInSEGS8915)



                Dim itemSEGS8915 As New SEGS8915_SPS_EO_OUT




                If destino.Columns.Count = 0 Then

                    With destino.Columns

                        .Add("proposta_id") 'ok
                        .Add("proposta_bb") 'ok
                        .Add("cod_produto") 'ok
                        .Add("ramo_id")     'ok
                        .Add("apolice_id")  'ok
                        .Add("nom_produto") 'ok
                        .Add("endereco")
                        .Add("cod_evento_sinistro")
                        .Add("nom_evento_sinistro")
                        .Add("cod_cobertura_afetada")
                        .Add("dsc_cobertura")
                        .Add("cod_objeto_segurado")
                        .Add("nom_objeto_segurado")
                        .Add("valor_prejuizo")
                        .Add("valor_is")
                        .Add("processa_reintegracao_is")
                        .Add("motivo_reanalise_sinistro_id")
                        .Add("descricao_motivo_reanalise")
                        .Add("texto_motivo_reanalise")

                    End With

                End If

                Dim r As DataRow

                For Each itemSEGS8915 In lstObjEoOutSEGS8915

                    If itemSEGS8915.proposta_id <> 0 Then

                        r = destino.NewRow
                        r.Item("proposta_id") = itemSEGS8915.proposta_id
                        r.Item("proposta_bb") = itemSEGS8915.proposta_bb

                        r.Item("cod_produto") = itemSEGS8915.cod_produto
                        r.Item("nom_produto") = itemSEGS8915.nom_produto 'grdPropostas.SelectedRow.Cells(8).Text
                        r.Item("apolice_id") = itemSEGS8915.apolice_id ' grdPropostas.SelectedRow.Cells(7).Text

                        r.Item("ramo_id") = itemSEGS8915.ramo_id

                        r.Item("nom_evento_sinistro") = itemSEGS8915.nom_evento_sinistro
                        r.Item("valor_prejuizo") = itemSEGS8915.valor_prejuizo
                        r.Item("dsc_cobertura") = itemSEGS8915.dsc_cobertura

                        r.Item("cod_objeto_segurado") = itemSEGS8915.cod_objeto_segurado
                        r.Item("nom_objeto_segurado") = itemSEGS8915.nom_objeto_segurado

                        r.Item("cod_evento_sinistro") = itemSEGS8915.cod_evento_sinistro
                        r.Item("cod_cobertura_afetada") = itemSEGS8915.cod_cobertura_afetada
                        r.Item("valor_is") = itemSEGS8915.valor_is
                        r.Item("processa_reintegracao_is") = itemSEGS8915.processa_reintegracao_is

                        r.Item("motivo_reanalise_sinistro_id") = itemSEGS8915.Motivo_Reanalise_Sinistro_id
                        r.Item("descricao_motivo_reanalise") = itemSEGS8915.descricao_motivo_reanalise
                        r.Item("texto_motivo_reanalise") = itemSEGS8915.texto_motivo_reanalise
                        destino.Rows.Add(r)

                    ElseIf destino.Rows.Count = 0 Then

                        r = destino.NewRow
                        r.Item("proposta_id") = itemSEGS8915.proposta_id
                        r.Item("proposta_bb") = itemSEGS8915.proposta_bb

                        r.Item("cod_produto") = itemSEGS8915.cod_produto
                        r.Item("nom_produto") = itemSEGS8915.nom_produto 'grdPropostas.SelectedRow.Cells(8).Text
                        r.Item("apolice_id") = itemSEGS8915.apolice_id ' grdPropostas.SelectedRow.Cells(7).Text

                        r.Item("ramo_id") = itemSEGS8915.ramo_id

                        r.Item("nom_evento_sinistro") = itemSEGS8915.nom_evento_sinistro
                        r.Item("valor_prejuizo") = itemSEGS8915.valor_prejuizo
                        r.Item("dsc_cobertura") = itemSEGS8915.dsc_cobertura

                        r.Item("cod_objeto_segurado") = itemSEGS8915.cod_objeto_segurado
                        r.Item("nom_objeto_segurado") = itemSEGS8915.nom_objeto_segurado

                        r.Item("cod_evento_sinistro") = itemSEGS8915.cod_evento_sinistro
                        r.Item("cod_cobertura_afetada") = itemSEGS8915.cod_cobertura_afetada
                        r.Item("valor_is") = itemSEGS8915.valor_is
                        r.Item("processa_reintegracao_is") = itemSEGS8915.processa_reintegracao_is

                        r.Item("motivo_reanalise_sinistro_id") = itemSEGS8915.Motivo_Reanalise_Sinistro_id
                        r.Item("descricao_motivo_reanalise") = itemSEGS8915.descricao_motivo_reanalise
                        r.Item("texto_motivo_reanalise") = itemSEGS8915.texto_motivo_reanalise
                        destino.Rows.Add(r)


                    End If


                Next

                Me.DestinoPropostasDoAviso = destino

                grdPropostasAviso.DataSource = destino

                grdPropostasAviso.DataBind()

                If grdPropostas.Rows.Count = 0 Then

                    grdPropostas.Visible = False

                Else

                    grdPropostas.Visible = True

                End If

                Me.btnAlterar.Enabled = (Me.grdPropostasAviso.Rows.Count > 0)

            End If

        End If



    End Sub

    Private Sub CarregaDados()

        Try


            Dim EO_IN As New SEGS8744_SPS_EO_IN
            Dim Eo_out As List(Of SEGS8744_SPS_EO_OUT)

            EO_IN.Sinistro_Id = Me.Sinistro_ID

            Eo_out = ObjGeral.Buscar(EO_IN)


            Me.listaPropostas = Eo_out(0).lista_propostas

            Me.listaPropostasExcluir = Eo_out(0).lista_propostas_excluir


            If Not IsNothing(Me.listaPropostasExcluir) Then

                Dim objEoInSEGS8915 As New SEGS8915_SPS_EO_IN

                objEoInSEGS8915.Sinistro_Id_Passos = Me.Sinistro_ID

                Dim objBusSEGS8915 As New SEGS8915_SPS_Business

                Dim lstObjEoOutSEGS8915 As New List(Of SEGS8915_SPS_EO_OUT)

                lstObjEoOutSEGS8915 = objBusSEGS8915.seleciona(objEoInSEGS8915)

                If lstObjEoOutSEGS8915.Count = 0 Then

                    Me.listaPropostasExcluir = ""

                End If



            End If


            ViewState("EoPassos") = Eo_out

        Catch ex As Exception

            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), _
                                                    Guid.NewGuid().ToString(), _
                                                    "alert('Erro ao carregar passos ')", True)

        End Try

    End Sub

    Public Function VerificaReintegracaoIS(ByRef produto_id As Long, ByRef ramo_id As Long) As Boolean

        VerificaReintegracaoIS = False

        Dim objEo_In As New SEGS8912_SPS_EO_IN

        objEo_In.Produto_Id = produto_id
        objEo_In.Ramo_Id = ramo_id

        Dim lstObjEo_Out As New List(Of SEGS8912_SPS_EO_OUT)

        Dim objBusiness As New SEGS8912_SPS_Business

        lstObjEo_Out = objBusiness.seleciona(objEo_In)

        If lstObjEo_Out.Count > 0 Then

            VerificaReintegracaoIS = (lstObjEo_Out(0).processa_reintegracao_is = "S")


        End If

    End Function

    Public Function MontarQuestionarioProduto(ByVal parProdutoQuestionario As Integer, Optional ByVal blnCalcular As Boolean = False) As Boolean


        objLogEoIn.Teste = Date.Now.ToString() & " Public Function MontarQuestionarioProduto - " & parProdutoQuestionario.ToString
        objLogBus.Insere(objLogEoIn)


        Dim Eo_outPassos As List(Of SEGS8744_SPS_EO_OUT)

        Eo_outPassos = ViewState("EoPassos")

        Dim sLerQuestionario As String = ""

        Dim objEo_InSEGS8920_SPI As New SEGS8920_SPI_EO_IN

        With objEo_InSEGS8920_SPI

            .Cod_Objeto_Segurado = Eo_outPassos(0).codigo_objeto_segurado 'VERIFICAR
            .Dt_Aviso_Sinistro = Date.Now
            .Dt_Ocorrencia = Eo_outPassos(0).data_ocorrencia
            .Proposta_Id = Decimal.Parse(Me.Proposta_ID)
            .Sinistro_Id_Passos = Me.Sinistro_ID
            ' Este iif não se aplica pois data final de vigência de ocorrência não está sendo utilizado
            ' no sistema
            '.Tp_Aviso = IIf(frmEvento.mskDataOcorrenciaFim.Visible, EVENTO_SINISTRO_PERIODICO, EVENTO_SINISTRO_PONTUAL)
            .Tp_Aviso = EVENTO_SINISTRO_PONTUAL

        End With

        ' exclui os dados da tabela re_passos_aviso_sinistro_tb WHERE Sinistro_Id_Passos

        Dim objEoIn_SEGS8922_SPD As New SEGS8922_SPD_EO_IN

        objEoIn_SEGS8922_SPD.Sinistro_Id_Passos = Me.Sinistro_ID

        If ObjSegurado.ExcluirRegistroTemporariaAviso(objEoIn_SEGS8922_SPD) = True Then

            objLogEoIn.Teste = Date.Now.ToString() & " ObjSegurado.ExcluirRegistroTemporariaAviso - " & parProdutoQuestionario.ToString
            objLogBus.Insere(objLogEoIn)


            'Inclui na tabela re_passos_aviso_sinistro_tb usada no cálculo de campos de questionário
            If ObjSegurado.IncluirRegistroTemporariaAviso(objEo_InSEGS8920_SPI) = True Then

                Dim sResposta As String = ""

                'Exclui dados da tabela re_passos_questionario_aviso_sinistro_tb
                Dim objEo_InSEGS8946_SPD As New SEGS8946_SPD_EO_IN

                objEo_InSEGS8946_SPD.Sinistro_Id_Passos = Me.Sinistro_ID

                objEo_InSEGS8946_SPD.Proposta_Id = Me.Proposta_ID


                If ObjSegurado.ExcluirQuestionarioAvisoSinistro(objEo_InSEGS8946_SPD) = True Then



                    objLogEoIn.Teste = Date.Now.ToString() & "  ObjSegurado.ExcluirQuestionarioAvisoSinistro(objEo_InSEGS8946_SPD) - " & parProdutoQuestionario.ToString
                    objLogBus.Insere(objLogEoIn)





                    If ConstruirQuestionario(parProdutoQuestionario, blnCalcular) = True Then


                        objLogEoIn.Teste = Date.Now.ToString() & "  ConstruirQuestionario(parProdutoQuestionario, blnCalcular) = True - "
                        objLogBus.Insere(objLogEoIn)


                        objLogEoIn.Teste = Date.Now.ToString() & "  Vai Acontecer o Show - "
                        objLogBus.Insere(objLogEoIn)



                        'Me.mpeQuestionario.SetRenderMethodDelegate()

                        Me.Observacoes1.Observacao = Nothing


                        Me.mpeQuestionario.Show()


                    End If

                    MontarQuestionarioProduto = True


                    'Dim sb As New StringBuilder()
                    'sb.Append("frmQuestionario.aspx")
                    'sb.Append("?SINISTRO_ID=" & Me.Sinistro_ID)

                    'Response.Redirect(sb.ToString())



                    'sResposta = ObjSegurado.PegarResposta(parProdutoQuestionario, "", Double.Parse(Me.Proposta_ID))

                    'If gsResultadoQuestionario = "S" Then

                    '    'SairAplicacao() 'Verificar
                    '    'verificar necessidade de redirecionar para página de pesquisa

                    'Else

                    '    MontarQuestionarioProduto = gsResultadoQuestionario = "C"

                    'End If

                End If

            End If


        End If





    End Function

    Function ExisteEstimativa(ByVal produto_id As String, _
                                        ByVal evento_sinistro_id As Integer, _
                                        ByVal tp_cobertura_id As Integer, _
                                        ByVal ramo_id As Integer, _
                                        ByVal subramo_id As Long) As Boolean


        ExisteEstimativa = False

        Dim objEo_InSEGS8930_SPS As New SEGS8930_SPS_EO_IN
        Dim lstobjEo_OutSEGS8930_SPS As New List(Of SEGS8930_SPS_EO_OUT)
        Dim objBus_SEGS8930_SPS As New SEGS8930_SPS_Business

        With objEo_InSEGS8930_SPS

            .Produto_Id = produto_id
            .Evento_Sinistro_Id = evento_sinistro_id
            .Tp_Cobertura_Id = tp_cobertura_id
            .Ramo_Id = ramo_id
            .Subramo_Id = subramo_id

        End With

        lstobjEo_OutSEGS8930_SPS = ObjGeral.ExisteEstimativa(objEo_InSEGS8930_SPS)

        If lstobjEo_OutSEGS8930_SPS.Count > 0 Then

            ExisteEstimativa = True

        End If

    End Function

    Function ConvAmbiente(ByVal eAmbiente As String) As String
        Select Case (eAmbiente)
            Case CStr(2)
                ConvAmbiente = "PRODUCAO"
            Case CStr(3)
                ConvAmbiente = "QUALIDADE"
            Case CStr(4)
                ConvAmbiente = "DESENVOLVIMENTO"
                'mathayde
            Case CStr(6)
                ConvAmbiente = "PRODUCAO_ABS"

            Case CStr(7)
                ConvAmbiente = "QUALIDADE_ABS"

            Case CStr(8)
                ConvAmbiente = "DESENVOLVIMENTO_ABS"

        End Select
    End Function

    Sub HabilitaControles(ByVal blnEstado As Boolean)

        Me.btnAdicionar.Enabled = blnEstado
        'Me.btnAlterar.Enabled = blnEstado
        'me.txtValIS.Enabled = blnEstado
        Me.txtValPrejuizo.Enabled = blnEstado


    End Sub


#End Region

#Region "Propriedades Questionario"


    Public Property gbSairCalcular() As Boolean
        Get
            Return CType(Session("gbSairCalcular"), Boolean)
        End Get
        Set(ByVal value As Boolean)
            Session("gbSairCalcular") = value
        End Set
    End Property

    Public Property gbSair() As Boolean
        Get
            Return CType(Session("gbSair"), Boolean)
        End Get
        Set(ByVal value As Boolean)
            Session("gbSair") = value
        End Set
    End Property

#End Region

#Region "Eventos Questionário"
    Protected Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Me.mpeQuestionario.Hide()
    End Sub
    Protected Sub btnVoltarQuest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoltarQuest.Click

        Dim lstQuestionario As New List(Of SEGS8950_SPS_EO_OUT)
        lstQuestionario = ViewState("lstQuestionario")

        If PersistirInformacoes(lstQuestionario) = True Then
            mpeQuestionario.Hide()
        Else
            ScriptManager.RegisterClientScriptBlock(Page, _
                                                    Me.GetType(), _
                                                    Guid.NewGuid().ToString(), _
                                                    "alert('Erro ao gravar questionario')", True)
        End If
    End Sub

    Protected Sub btnContinuarQuest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinuarQuest.Click

        Dim lstQuestionario As New List(Of SEGS8950_SPS_EO_OUT)
        lstQuestionario = ViewState("lstQuestionario")
        If PersistirInformacoes(lstQuestionario, True) = True Then
            If Not IsNothing(mpeQuestionario) Then

                mpeQuestionario.Hide()


                Dim cValPrejuizo As Decimal

                If Me.grdPropostas.SelectedRow.Cells(3).Text = "1152" Then

                    Dim objEoInSEGS9000 As New SEGS9000_SPS_EO_IN

                    Dim objBusSEGS9000 As New SEGS9000_SPS_Business

                    Dim lstObjEoOutSEGS9000 As New List(Of SEGS9000_SPS_EO_OUT)

                    objEoInSEGS9000.Sinistro_Id_Passos = Me.Sinistro_ID

                    lstObjEoOutSEGS9000 = objBusSEGS9000.seleciona(objEoInSEGS9000)

                    If lstObjEoOutSEGS9000.Count > 0 Then

                        If lstObjEoOutSEGS9000(0).VALOR = 0 Then

                            cValPrejuizo = 0

                        Else

                            cValPrejuizo = (txtValIS.Text * lstObjEoOutSEGS9000(0).VALOR / 100)


                        End If


                    End If


                End If

                txtValPrejuizo.Text = Format(cValPrejuizo, "0.00")

            End If

        Else

            ScriptManager.RegisterClientScriptBlock(Page, _
                                                    Me.GetType(), _
                                                    Guid.NewGuid().ToString(), _
                                                    "alert('Erro ao gravar questionario');", True)

        End If

    End Sub



    Protected Sub btnCalcular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalcular.Click

        Dim lstQuestionario As New List(Of SEGS8950_SPS_EO_OUT)

        lstQuestionario = ViewState("lstQuestionario")

        LimparCamposResposta(lstQuestionario.Count)


        If PersistirInformacoes(lstQuestionario, True, btnCalcular.ID) = True Then

            Me.mpeQuestionario.Show()
            MontarQuestionarioProduto(ProtutoQuestionario, True)


        Else

            ScriptManager.RegisterClientScriptBlock(Page, _
                                                    Me.GetType(), _
                                                    Guid.NewGuid().ToString(), _
                                                    "alert('Erro ao gravar questionario');", True)

        End If


    End Sub

#End Region

#Region "Métodos Questionário"

    Function ConstruirQuestionario(ByVal parProdutoQuestionario As Integer, Optional ByVal blnCalcular As Boolean = False) As Boolean

        ConstruirQuestionario = False

        Dim objEoInSEGS8950_SPS As New SEGS8950_SPS_EO_IN

        objEoInSEGS8950_SPS.Questionario_Id = parProdutoQuestionario

        objEoInSEGS8950_SPS.Sinistro_Id_Passos = Me.Sinistro_ID
        objEoInSEGS8950_SPS.PesquisaSemId = "S"

        Dim lstQuestionario As New List(Of SEGS8950_SPS_EO_OUT)
        lstQuestionario = objQuestionario.CarregarQuestionario(objEoInSEGS8950_SPS)

        ViewState("lstQuestionario") = lstQuestionario

        LimparCamposResposta(lstQuestionario.Count)

        If PersistirInformacoes(lstQuestionario, blnCalcular) Then

            objEoInSEGS8950_SPS.Questionario_Id = parProdutoQuestionario

            objEoInSEGS8950_SPS.Sinistro_Id_Passos = Me.Sinistro_ID

            objEoInSEGS8950_SPS.PesquisaSemId = "N"

            objEoInSEGS8950_SPS.Proposta_Id = Me.Proposta_ID

            lstQuestionario = objQuestionario.CarregarQuestionario(objEoInSEGS8950_SPS)


        End If

        If lstQuestionario.Count > 0 Then

            Dim i As Integer = 0

            Dim div_maior(lstQuestionario.Count - 1) As HtmlGenericControl
            Dim div_medio(lstQuestionario.Count - 1) As HtmlGenericControl
            Dim div_menor(lstQuestionario.Count - 1) As HtmlGenericControl

            Dim label_questionario(lstQuestionario.Count - 1) As HtmlGenericControl


            '<div id="Panel" style="width:750px; height:auto">
            '	<div Class="n750">
            '		<div Class="n750 divLeft">
            '			<label Class="label">
            '               Meu Teste
            '			</label>
            '			<select name="ctl06">
            '			</select>
            '		</div>
            '	</div>
            '</div>

            For i = 0 To lstQuestionario.Count - 1

                'div_maior(i) = New HtmlGenericControl("div_maior_" & i.ToString)
                div_maior(i) = New HtmlGenericControl("div")
                'div_maior(i).ID = "Panel"
                div_maior(i).ID = "Panel_" & i
                div_maior(i).Attributes.Add("style", "width:750px;")
                div_maior(i).Attributes.Add("runnat", "server")


                div_medio(i) = New HtmlGenericControl("div")
                div_medio(i).Attributes.Add("Class", "n750")
                div_medio(i).ID = "div_medio_" & i

                div_menor(i) = New HtmlGenericControl("div")
                div_menor(i).Attributes.Add("Class", "n750 divLeft")
                div_menor(i).ID = "div_menor_" & i


                'AspLabel.Attributes.Add(

                label_questionario(i) = New HtmlGenericControl("label")
                label_questionario(i).Attributes.Add("Class", "txtLabel divLeft n250")
                label_questionario(i).InnerHtml = (i + 1).ToString & " - " & lstQuestionario(i).nome_pergunta & " " & IIf(label_questionario(i).InnerHtml.ToUpper = "S", "(*)", "")
                label_questionario(i).ID = "label_questionario_" & i


                div_menor(i).Controls.Add(label_questionario(i))




                'lstQuestionario(i).resposta_determinada

                div_menor(i).Controls.Add(CriarControle(lstQuestionario(i).formato_resposta_id, _
                                                            lstQuestionario(i).habilitado, _
                                                            lstQuestionario(i).tamanho_resposta, i, _
                                                            lstQuestionario(i).funcao, _
                                                            lstQuestionario(i).obrigatório, _
                                                            lstQuestionario(i).resposta _
                                                            ))


                Dim hdd As New HiddenField

                hdd.ID = "Campo_Obrigatorio_" & i

                If lstQuestionario(i).obrigatório = "S" Then

                    hdd.Value = lstQuestionario(i).obrigatório

                ElseIf lstQuestionario(i).obrigatório = "N" Or lstQuestionario(i).obrigatório = "" Then

                    hdd.Value = "N"


                End If

                div_menor(i).Controls.Add(hdd)

                div_medio(i).Controls.Add(div_menor(i))

                div_maior(i).Controls.Add(div_medio(i))

                Me.fieldset_questionario.Controls.Add(div_maior(i))

            Next

            'ViewState("fieldset_questionario") = Me.fieldset_questionario


            ConstruirQuestionario = True

        End If

    End Function

    Function CriarControle(ByVal intFormatoRespostaId As Integer, _
                            ByVal strHabilitado As String, _
                            ByVal intTamanhoResposta As Integer, _
                            ByVal intIndice As Integer, _
                            ByVal strFuncao As String, _
                            ByVal strObrigatorio As String, _
                            Optional ByVal strResposta As String = "") As Object  ' alterador por Sergio -  AS TextBox 28/07 as 17:30

        'lstQuestionario(i).habilitado, _
        'lstQuestionario(i).tamanho_resposta, _
        'lstQuestionario(i).resposta, _


        '//onkeypress="return formatar(this, '(??) ????-????', event);"
        '// Usar ? para números e ! para letras

        '            'Me.btnContinuar.Attributes.Add("onclick", "javascript:return confirmaAviso('" & Me.flag_aviso.ClientID & "');")

        Dim objControle As New clsControleDinamico

        Dim lstControle As New List(Of clsControleDinamico)

        If Not IsNothing(ViewState("lstControle")) Then

            lstControle = ViewState("lstControle")

        End If


        Select Case intFormatoRespostaId

            Case TipoControle.CHECKBOX

                Dim chkCheckBox As New CheckBox
                'txt n70

                chkCheckBox.Enabled = (strFuncao = "") 'COLOCAR TRATAMENTO NA PÁGINA

                'chkCheckBox.ID = TipoControle.CHECKBOX & "/" & intIndice
                chkCheckBox.ID = "Pergunta_CHECKBOX_" & intIndice




                If strFuncao <> "" Then

                    If strHabilitado = "" Or strHabilitado = "N" Then
                        chkCheckBox.Enabled = False
                        'goControle(iTotalPerguntas * 2).BackColor = &H80000011  '&H80000011&

                    Else

                        chkCheckBox.Enabled = True

                    End If

                End If

                If strResposta.Length > 0 Then

                    chkCheckBox.Checked = strResposta

                End If

                objControle.Indice = intIndice
                objControle.NomeControle = "Pergunta_CHECKBOX_" & intIndice

                lstControle.Add(objControle)

                ViewState("lstControle") = lstControle


                'Return chkCheckBox


            Case TipoControle.COMBO

                Dim ddlCombo As New DropDownList

                'ddlCombo.ID = TipoControle.COMBO & "/" & intIndice
                ddlCombo.ID = "Pergunta_COMBO_" & intIndice

                ddlCombo.Enabled = (strFuncao = "") 'COLOCAR TRATAMENTO NA PÁGINA


                If strFuncao <> "" Then

                    If strHabilitado = "" Or strHabilitado = "N" Then
                        ddlCombo.Enabled = False
                        'goControle(iTotalPerguntas * 2).BackColor = &H80000011  '&H80000011&

                    Else

                        ddlCombo.Enabled = True

                        'goControle(iTotalPerguntas * 2).Enabled = True

                    End If

                End If

                If strResposta.Length > 0 Then

                    ddlCombo.SelectedValue = strResposta

                End If

                objControle.Indice = intIndice
                objControle.NomeControle = "Pergunta_COMBO_" & intIndice

                lstControle.Add(objControle)

                ViewState("lstControle") = lstControle



                ' Return ddlCombo


            Case TipoControle.DATA

                Dim txtData As New TextBox


                'txtData.ID = TipoControle.DATA & "/" & intIndice
                txtData.ID = "Pergunta_DATA_" & intIndice 'TipoControle.DATA & "/" & intIndice






                txtData.Attributes.Add("Class", "txtCampo divLeft n70")

                txtData.Attributes.Add("onkeypress", "javascript:return formatar(this, '??/??/????', event);")
                'txtData.Attributes.Add("onblur", "javascript:return VerificaData(this.value);")

                txtData.MaxLength = intTamanhoResposta

                'txtData.ID = "Pergunta_" & intIndice.ToString

                txtData.Enabled = (strFuncao = "")

                If strFuncao <> "" Then


                    If strHabilitado = "" Or strHabilitado = "N" Then

                        txtData.Enabled = False
                        'goControle(iTotalPerguntas * 2).BackColor = &H80000011  '&H80000011&

                    Else

                        txtData.Enabled = True
                        'goControle(iTotalPerguntas * 2).Enabled = True

                    End If



                End If

                If strResposta.Length > 0 Then

                    txtData.Text = strResposta

                End If

                objControle.Indice = intIndice
                objControle.NomeControle = "Pergunta_DATA_" & intIndice



                lstControle.Add(objControle)

                ViewState("lstControle") = lstControle


                Return txtData

            Case TipoControle.HORA

                Dim txtHora As New TextBox


                'txtHora.ID = TipoControle.HORA & "/" & intIndice

                txtHora.ID = "Pergunta_HORA_" & intIndice 'TipoControle.DATA & "/" & intIndice


                txtHora.Attributes.Add("Class", "txtCampo divLeft n70")


                txtHora.Attributes.Add("onkeypress", "javascript:return formatar(this, '??:??:??', event);")
                txtHora.MaxLength = intTamanhoResposta


                txtHora.Enabled = (strFuncao = "")

                If strFuncao <> "" Then

                    If strHabilitado = "" Or strHabilitado = "N" Then

                        txtHora.Enabled = False
                        'goControle(iTotalPerguntas * 2).BackColor = &H80000011  '&H80000011&

                    Else

                        txtHora.Enabled = True
                        'goControle(iTotalPerguntas * 2).Enabled = True

                    End If


                End If



                If strResposta.Length > 0 Then

                    txtHora.Text = strResposta

                End If

                objControle.Indice = intIndice
                objControle.NomeControle = "Pergunta_HORA_" & intIndice

                lstControle.Add(objControle)

                ViewState("lstControle") = lstControle


                Return txtHora

            Case TipoControle.LABEL

                Dim lblLabel As New Label

                lblLabel.ID = "Pergunta_LABEL_" & intIndice


                'lblLabel.Attributes.Add("onkeypress", "javascript:return formatar(this, '???,????', event);")
                'lblLabel.MaxLength = intTamanhoResposta + 1

                lblLabel.Attributes.Add("Class", "txtLabel divLeft n250")

                lblLabel.Enabled = (strFuncao = "")


                If strFuncao <> "" Then

                    If strHabilitado = "" Or strHabilitado = "N" Then

                        lblLabel.Enabled = False
                        'goControle(iTotalPerguntas * 2).BackColor = &H80000011  '&H80000011&

                    Else

                        lblLabel.Enabled = True
                        'goControle(iTotalPerguntas * 2).Enabled = True

                    End If

                End If


                If strResposta.Length > 0 Then

                    lblLabel.Text = strResposta

                End If

                objControle.Indice = intIndice
                objControle.NomeControle = "Pergunta_LABEL_" & intIndice

                lstControle.Add(objControle)

                ViewState("lstControle") = lstControle


                'Return lblLabel

            Case TipoControle.MEMO

                Dim txtMemo As New TextBox

                'txtMemo.ID = TipoControle.MEMO & "/" & intIndice
                txtMemo.ID = "Pergunta_MEMO_" & intIndice

                txtMemo.TextMode = TextBoxMode.MultiLine

                txtMemo.Enabled = (strFuncao = "")

                txtMemo.Attributes.Add("Class", "txtCampo divLeft n200")


                If strFuncao <> "" Then

                    If strHabilitado = "" Or strHabilitado = "N" Then

                        txtMemo.Enabled = False

                    Else

                        txtMemo.Enabled = True

                    End If

                End If

                If strResposta.Length > 0 Then

                    txtMemo.Text = strResposta

                End If

                objControle.Indice = intIndice
                objControle.NomeControle = "Pergunta_MEMO_" & intIndice

                lstControle.Add(objControle)

                ViewState("lstControle") = lstControle

                Return txtMemo



            Case TipoControle.PERCENTUAL

                Dim txtPercentual As New TextBox

                txtPercentual.ID = "Pergunta_PERCENTUAL_" & intIndice

                txtPercentual.Attributes.Add("Class", "txtCampo divLeft n200")


                txtPercentual.Attributes.Add("onkeypress", "javascript:return formatar(this, '???', event);")
                txtPercentual.MaxLength = intTamanhoResposta + 1

                'txtPercentual.Enabled = Not (strHabilitado = "N" Or strHabilitado = "")

                txtPercentual.Enabled = (strFuncao = "")


                If strFuncao <> "" Then

                    If strHabilitado = "" Or strHabilitado = "N" Then

                        txtPercentual.Enabled = False

                    Else

                        txtPercentual.Enabled = True

                    End If

                End If

                If strResposta.Length > 0 Then

                    txtPercentual.Text = strResposta.Replace(",0000", "")

                End If

                objControle.Indice = intIndice
                objControle.NomeControle = "Pergunta_PERCENTUAL_" & intIndice

                lstControle.Add(objControle)


                ViewState("lstControle") = lstControle

                Return txtPercentual

            Case TipoControle.NUMERO



                Dim txtNumero As New TextBox


                txtNumero.ID = "Pergunta_NUMERO_" & intIndice

                'txtNumero.Attributes.Add("onkeypress", "javascript:return somente_numero(this);")
                txtNumero.Attributes.Add("onkeypress", "javascript:return SomenteNumero(event);")
                txtNumero.MaxLength = intTamanhoResposta + 1
                'txtNumero.Enabled = Not (strHabilitado = "N" Or strHabilitado = "")

                txtNumero.Attributes.Add("Class", "txtCampo divLeft n200")

                txtNumero.Enabled = (strFuncao = "")


                If strFuncao <> "" Then


                    If strHabilitado = "" Or strHabilitado = "N" Then

                        txtNumero.Enabled = False

                    Else

                        txtNumero.Enabled = True

                    End If

                End If

                If strResposta.Length > 0 Then

                    txtNumero.Text = strResposta

                End If

                objControle.Indice = intIndice
                objControle.NomeControle = "Pergunta_NUMERO_" & intIndice


                ViewState("lstControle") = lstControle

                lstControle.Add(objControle)


                Return txtNumero

            Case TipoControle.RADIO

                'txtPercentual.Enabled = (strFuncao = "")

                If strFuncao <> "" Then

                    If strHabilitado = "" Or strHabilitado = "N" Then

                        'txtNumero.Enabled = False

                    Else

                        'txtNumero.Enabled = True

                    End If

                End If


            Case TipoControle.TEXTO

                Dim txtTexto As New TextBox

                'txtTexto.ID = TipoControle.TEXTO & "/" & intIndice

                txtTexto.ID = "Pergunta_TEXTO_" & intIndice

                txtTexto.MaxLength = intTamanhoResposta

                txtTexto.Attributes.Add("Class", "txtCampo divLeft  n200")

                txtTexto.Enabled = (strFuncao = "")

                If strFuncao <> "" Then

                    If strHabilitado = "" Or strHabilitado = "N" Then

                        txtTexto.Enabled = False

                    Else

                        txtTexto.Enabled = True

                    End If

                End If

                If strResposta.Length > 0 Then

                    txtTexto.Text = strResposta

                End If

                objControle.Indice = intIndice
                objControle.NomeControle = "Pergunta_TEXTO_" & intIndice


                lstControle.Add(objControle)

                ViewState("lstControle") = lstControle

                Return txtTexto

            Case TipoControle.TIMESTAMP


            Case TipoControle.VALOR

                Dim txtValor As New TextBox

                txtValor.Attributes.Add("onkeypress", "javascript:return formatar(this, '???????????,????', event);")
                txtValor.MaxLength = intTamanhoResposta

                txtValor.ID = "Pergunta_VALOR_" & intIndice

                txtValor.Attributes.Add("Class", "txtCampo divLeft  n200")

                txtValor.Enabled = (strFuncao = "")


                If strFuncao <> "" Then

                    If strHabilitado = "" Or strHabilitado = "N" Then

                        txtValor.Enabled = False

                    Else

                        txtValor.Enabled = True

                    End If

                End If

                'IsNull(RS("habilitado")) Or RS("habilitado") = "N"

                If strResposta.Length > 0 Then

                    txtValor.Text = strResposta

                End If

                objControle.Indice = intIndice
                objControle.NomeControle = "Pergunta_VALOR_" & intIndice

                lstControle.Add(objControle)

                ViewState("lstControle") = lstControle


                Return txtValor

        End Select


    End Function

    Function PersistirInformacoes(ByRef listaQuestionario As List(Of SEGS8950_SPS_EO_OUT), _
                                    Optional ByVal blnCalcular As Boolean = False, _
                                    Optional ByVal strBotao As String = "") As Boolean

        Dim blnTemDominio As Boolean = False

        PersistirInformacoes = False

        'Exclui dados da tabela re_passos_questionario_aviso_sinistro_tb
        Dim objEo_InSEGS8946_SPD As New SEGS8946_SPD_EO_IN

        objEo_InSEGS8946_SPD.Sinistro_Id_Passos = Me.Sinistro_ID

        objEo_InSEGS8946_SPD.Proposta_Id = Me.Proposta_ID

        Dim i As Integer = 0

        Dim blnAlterarResposta As New Boolean

        blnAlterarResposta = False


        If ObjSegurado.ExcluirQuestionarioAvisoSinistro(objEo_InSEGS8946_SPD) = True Then

            Dim objEoInTempQuest As New SEGS8945_SPI_EO_IN

            Dim itemQuestionario As New SEGS8950_SPS_EO_OUT

            For Each itemQuestionario In listaQuestionario

                With objEoInTempQuest


                    .Proposta_Id = Me.Proposta_ID

                    If IsNothing(itemQuestionario.drid) = False Then

                        .Dominio_Resposta_Id = itemQuestionario.drid

                        blnAlterarResposta = True

                    End If

                    If IsNothing(itemQuestionario.pid) = False Then

                        .Pergunta_Id = itemQuestionario.pid

                        blnAlterarResposta = True

                    End If

                    .Questionario_Id = itemQuestionario.questionario_id

                    'AQUI ID DOS PASSOS
                    .Sinistro_Id_Passos = Me.Sinistro_ID

                    If itemQuestionario.funcao.Trim.Length > 0 Then

                        Dim objEoInTextoResposta As New SEGS8969_SPS_EO_IN

                        objEoInTextoResposta.Funcao = itemQuestionario.funcao

                        objEoInTextoResposta.Sinistro_Id_Passos = Me.Sinistro_ID

                        Dim lstTextoResposta As New List(Of SEGS8969_SPS_EO_OUT)

                        lstTextoResposta = objQuestionario.RetornarTextoResposta(objEoInTextoResposta)


                        If lstTextoResposta(0).Valor = "__/__/____" Then

                            .Texto_Resposta = ""


                        Else

                            If itemQuestionario.formato = "3,4" Then


                                If InStr(lstTextoResposta(0).Valor, ",00000") > 0 Then

                                    .Texto_Resposta = lstTextoResposta(0).Valor.Replace(",00000", "")

                                    blnAlterarResposta = False

                                Else

                                    .Texto_Resposta = Mid(lstTextoResposta(0).Valor, 1, InStr(lstTextoResposta(0).Valor, ","))


                                    If strBotao = "btnCalcular" Then

                                        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), _
                                                        Guid.NewGuid().ToString(), _
                                                        "document.getElementById('Pergunta_PERCENTUAL_" & i & "').value='" & Mid(lstTextoResposta(0).Valor, 1, InStr(lstTextoResposta(0).Valor, ",") - 1) & "';", True)


                                        CType(FindControl("hdd_" & i), HiddenField).Value = Mid(lstTextoResposta(0).Valor, 1, InStr(lstTextoResposta(0).Valor, ","))

                                    End If

                                    blnAlterarResposta = False


                                End If

                            Else

                                    .Texto_Resposta = lstTextoResposta(0).Valor

                            End If

                            '.Texto_Resposta = lstTextoResposta(0).Valor

                        End If

                    Else


                        If Not IsNothing(itemQuestionario.resposta) Then

                            .Texto_Resposta = itemQuestionario.resposta

                            blnAlterarResposta = True

                        Else

                            .Texto_Resposta = ""

                            blnAlterarResposta = True


                        End If


                    End If


                    If blnCalcular = True Then

                        If Not IsNothing(Request("hdd_" & i)) And blnAlterarResposta = True Then

                            If Request("hdd_" & i).Trim.Length > 0 Then

                                .Texto_Resposta = Request("hdd_" & i)

                            End If

                        End If

                    End If

                    objQuestionario.InserirTempQuestionario(objEoInTempQuest)

                End With

                i = i + 1
            Next

            PersistirInformacoes = True

        End If



    End Function

    Sub LimparCamposResposta(ByVal intQtdPerguntas)

        For i As Integer = 0 To intQtdPerguntas


            CType(Me.FindControl("hdd_" & i), HiddenField).Value = ""

        Next

    End Sub

#End Region

#Region "Métodos Motivo Reanalise"

    Private Sub CarregaComboMotReanalise()

        Dim ObjEoIn_SEGS8928_SPS As New SEGS8928_SPS_EO_IN

        Dim lstObjEoOut_SEGS8928_SPS As New List(Of SEGS8928_SPS_EO_OUT)

        Dim objBus As New SEGS8928_SPS_Business

        ObjEoIn_SEGS8928_SPS.Tipo_Ramo = Convert.ToInt32(ConfigurationManager.AppSettings("TipoRamo"))

        lstObjEoOut_SEGS8928_SPS = objBus.seleciona(ObjEoIn_SEGS8928_SPS)

        Me.ddlMotivoReanalise.DataValueField = "motivo_reanalise_sinistro_id"
        Me.ddlMotivoReanalise.DataTextField = "descricao"



        Me.ddlMotivoReanalise.DataSource = lstObjEoOut_SEGS8928_SPS

        Me.ddlMotivoReanalise.DataBind()

    End Sub

    Private Function GravarMotReanalise() As Boolean

        GravarMotReanalise = False


        If ddlMotivoReanalise.SelectedIndex = -1 Then

            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), _
                                                    Guid.NewGuid().ToString(), _
                                                    "alert('É preciso especificar o motivo de reanálise!')", True)
            Exit Function

        End If

        If ddlMotivoReanalise.SelectedValue = 3 And Observacoes1.Observacao.Trim.Length = 0 Then

            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), _
                                                    Guid.NewGuid().ToString(), _
                                                    "alert('É preciso descrever o motivo de reanalise, caso seja escolhido a opção Outros.!')", True)
            Exit Function

            ddlMotivoReanalise.SelectedIndex = -1

            Me.Observacoes1.Observacao = Nothing

            ddlMotivoReanalise.Focus()

        Else

            ViewState("motivo_reanalise_sinistro_id") = Me.ddlMotivoReanalise.SelectedValue

            ViewState("descricao_motivo_reanalise") = Me.ddlMotivoReanalise.SelectedItem.Text

            ViewState("texto_motivo_reanalise") = Me.Observacoes1.Observacao

            GravarMotReanalise = True

        End If





    End Function

    Private Sub AvancarMotReanalise()

        Me.mpeMotivoReanalise.Hide()
        pnQuestionario.Visible = False

    End Sub

#End Region

#Region "Evento Motivo Reanalise"


    Protected Sub ddlMotivoReanalise_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlMotivoReanalise.SelectedIndexChanged


        If ddlMotivoReanalise.SelectedValue = 3 Then

            'habilita observacao
            Observacoes1.ObservacaoReadOnly = False


            Observacoes1.Focus = True


        Else

            'limpa e desabilita
            'habilita observacao

            Observacoes1.Observacao = ""
            Observacoes1.ObservacaoReadOnly = True

        End If

        Me.mpeMotivoReanalise.Show()


    End Sub

    Protected Sub btnContinuarMotReanalise_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinuarMotReanalise.Click

        If Me.GravarMotReanalise() Then

            Me.AvancarMotReanalise()

        Else

            Exit Sub

            'ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Erro ao tentar salvar Motivo de Reanalise!')", True)

        End If


    End Sub


    Protected Sub btnVoltarMotReanalise_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoltarMotReanalise.Click

        Me.mpeMotivoReanalise.Hide()

    End Sub

#End Region


    Protected Sub LkbPesquisa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbPesquisa.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158ConsultaSinistrado.aspx")
        sb.Append("?Cliente_ID=" & Cliente_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())
    End Sub

    Protected Sub LkbEvento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbEvento.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158Evento.aspx")
        sb.Append("?SINISTRO_ID=" & Me.Sinistro_ID.ToString)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))
        Response.Redirect(sb.ToString())
    End Sub

    Protected Sub LkbSolicitante_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbSolicitante.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158DadosSolicitante.aspx")
        sb.Append("?SINISTRO_ID=" + Me.Sinistro_ID.ToString)
        sb.Append("&SLinkSeguro=" + Request("SLinkSeguro"))
        Response.Redirect(sb.ToString())
    End Sub

    Protected Sub LkbAgencia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbAgencia.Click
        Dim objDel_IN As New SEGS8895_SPD_EO_IN
        Dim objDel_Business As New SEGS8895_SPD_Business

        objDel_IN.Sinistro_Id_Passos = Me.Sinistro_ID
        objDel_Business.Exclui(objDel_IN)

        Dim sb As New StringBuilder()

        sb.Append("SEGW0158DadosAgencia.aspx")
        sb.Append("?SINISTRO_ID=" + IIf(IsNothing(Me.Sinistro_ID), "", Me.Sinistro_ID.ToString))
        sb.Append("&SLinkSeguro=" + Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())
    End Sub
    Protected Sub LkbDadosCliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbDadosCliente.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158DadosCliente.aspx")
        sb.Append("?SINISTRO_ID=" & Me.Sinistro_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())
    End Sub
    Protected Sub LkbSinistroCA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbSinistroCA.Click
        Voltar()
    End Sub
End Class