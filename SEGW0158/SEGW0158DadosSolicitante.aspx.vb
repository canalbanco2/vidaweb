﻿Imports SEGL0343.Task
Imports SEGL0343.EO

Partial Public Class SEGW0158DadosSolicitante
    Inherits BasePages

#Region "Propriedades"

    Public ReadOnly Property Sinistro_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("SINISTRO_ID")) Then
                Return Request.QueryString("SINISTRO_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property Cliente_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("Cliente_ID")) Then
                Return Request.QueryString("Cliente_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property Evento_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("Evento_ID")) Then
                Return Request.QueryString("Evento_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property SubEvento_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("SubEvento_ID")) Then
                Return Request.QueryString("SubEvento_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property Dt_Ocorrencia() As String
        Get
            If Not IsNothing(Request.QueryString("Dt_Ocorrencia")) Then
                Return Request.QueryString("Dt_Ocorrencia")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property CPF_CNPJ() As String
        Get
            If Not IsNothing(Request.QueryString("CPF_CNPJ")) Then
                Return Request.QueryString("CPF_CNPJ")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property sProposta() As Boolean
        Get
            If Not IsNothing(Request.QueryString("sProposta")) Then
                Return Request.QueryString("sProposta")
            Else
                Return Nothing
            End If
        End Get
    End Property

#End Region

#Region "Eventos"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If IsNothing(Session("DadosCliente")) Then
                AjustaNavegacao("LkbSolicitante")
            Else
                AjustaNavegacao("DadosCliente")
            End If

            LkbSolicitante.Text = " ► Dados Solicitante"
            LkbSolicitante.ForeColor = Drawing.Color.Blue

            If Not Page.IsPostBack Then

                Me.UCDadosSolicitanteVida1.btnSegurado_Visible = True

                If Not IsNothing(Me.Sinistro_ID) Then
                    If Me.Sinistro_ID > 0 Then
                        CarregarDados()
                    End If
                End If

                Dim txtCEP As TextBox = UCDadosSolicitanteVida1.FindControl("txtCEP")
                Dim txtNome As TextBox = UCDadosSolicitanteVida1.FindControl("txtNome")

                If Not IsPostBack Then
                    txtCEP.Focus()
                ElseIf txtCEP.Text <> "" Then
                    txtNome.Focus()
                End If

            End If
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub btnVoltar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoltar.Click

        Me.Voltar()

    End Sub

    Protected Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click
        Try
            If Me.Gravar() Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Dados salvos com sucesso!')", True)
            Else
            End If
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub btnContinuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinuar.Click
        If Gravar() Then
            Avancar()
        End If
    End Sub

#End Region

#Region "Funções"

    Private Sub CarregarDados()
        Dim TaskPassosSinistro As New SEGL0343.Task.SEGW158SinistroInserirPassosDanos_TK
        Dim parametros As New SEGL0343.EO.SEGS8744_SPS_EO_IN
        Dim dadosAtuais As List(Of SEGL0343.EO.SEGS8744_SPS_EO_OUT)
        Try
            parametros.Sinistro_Id = Me.Sinistro_ID
            dadosAtuais = TaskPassosSinistro.Consultar_Passo_Danos(parametros)
            If Not IsNothing(dadosAtuais) Then
                Me.UCDadosSolicitanteVida1.Nome = dadosAtuais.Item(0).nome_solicitante
                Me.UCDadosSolicitanteVida1.Bairro = dadosAtuais.Item(0).bairro_solicitante
                Me.UCDadosSolicitanteVida1.CEP = dadosAtuais.Item(0).cep_solicitante
                Me.UCDadosSolicitanteVida1.Email = dadosAtuais.Item(0).email_solicitante
                Me.UCDadosSolicitanteVida1.Endereco = dadosAtuais.Item(0).endereco_solicitante

                Me.UCDadosSolicitanteVida1.PreencheComboUF()
                If Not IsNothing(dadosAtuais.Item(0).uf_solicitante) Then
                    Me.UCDadosSolicitanteVida1.UFText = dadosAtuais.Item(0).uf_solicitante
                End If
                If Not IsNothing(dadosAtuais.Item(0).uf_solicitante) Then ' String.IsNullOrEmpty(dadosAtuais.Item(0).municipio_solicitante) Then
                    Me.UCDadosSolicitanteVida1.MunicipioText = dadosAtuais.Item(0).municipio_solicitante
                End If
                Me.UCDadosSolicitanteVida1.DDD1 = dadosAtuais.Item(0).ddd_solicitante
                Me.UCDadosSolicitanteVida1.DDD2 = dadosAtuais.Item(0).ddd_solicitante1
                Me.UCDadosSolicitanteVida1.DDD3 = dadosAtuais.Item(0).ddd_solicitante2
                Me.UCDadosSolicitanteVida1.DDD4 = dadosAtuais.Item(0).ddd_solicitante3
                Me.UCDadosSolicitanteVida1.DDD5 = dadosAtuais.Item(0).ddd_solicitante4
                Me.UCDadosSolicitanteVida1.Telefone1 = dadosAtuais.Item(0).fone_solicitante
                Me.UCDadosSolicitanteVida1.Telefone2 = dadosAtuais.Item(0).fone_solicitante1
                Me.UCDadosSolicitanteVida1.Telefone3 = dadosAtuais.Item(0).fone_solicitante2
                Me.UCDadosSolicitanteVida1.Telefone4 = dadosAtuais.Item(0).fone_solicitante3
                Me.UCDadosSolicitanteVida1.Telefone5 = dadosAtuais.Item(0).fone_solicitante4
                Me.UCDadosSolicitanteVida1.Ramal1 = dadosAtuais.Item(0).ramal_solicitante
                Me.UCDadosSolicitanteVida1.Ramal2 = dadosAtuais.Item(0).ramal_solicitante1
                Me.UCDadosSolicitanteVida1.Ramal3 = dadosAtuais.Item(0).ramal_solicitante2
                Me.UCDadosSolicitanteVida1.Ramal4 = dadosAtuais.Item(0).ramal_solicitante3
                Me.UCDadosSolicitanteVida1.Ramal5 = dadosAtuais.Item(0).ramal_solicitante4
                Me.UCDadosSolicitanteVida1.TipoTelefone1 = RetornaCodTipoTel(dadosAtuais.Item(0).tipo_fone_solicitante)
                Me.UCDadosSolicitanteVida1.TipoTelefone2 = RetornaCodTipoTel(dadosAtuais.Item(0).tipo_fone_solicitante1)
                Me.UCDadosSolicitanteVida1.TipoTelefone3 = RetornaCodTipoTel(dadosAtuais.Item(0).tipo_fone_solicitante2)
                Me.UCDadosSolicitanteVida1.TipoTelefone4 = RetornaCodTipoTel(dadosAtuais.Item(0).tipo_fone_solicitante3)
                Me.UCDadosSolicitanteVida1.TipoTelefone5 = RetornaCodTipoTel(dadosAtuais.Item(0).tipo_fone_solicitante4)

                Me.UCDadosSolicitanteVida1.Cliente_ID = dadosAtuais.Item(0).cliente_id
            End If

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try


    End Sub
    Private Function RetornaCodTipoTel(ByVal Descricao As String)
        Try
            Select Case Descricao
                Case "-Selecione-"
                    RetornaCodTipoTel = "0"
                Case "Residencial"
                    RetornaCodTipoTel = "1"
                Case "Comercial"
                    RetornaCodTipoTel = "2"
                Case "Celular"
                    RetornaCodTipoTel = "3"
                Case "Fax"
                    RetornaCodTipoTel = "4"
                Case "Recado"
                    RetornaCodTipoTel = "5"
            End Select
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Function

    Private Sub Voltar()

        Dim sb As New StringBuilder()
        sb.Append("SEGW0158Evento.aspx")
        sb.Append("?SINISTRO_ID=" & Me.Sinistro_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))
        Response.Redirect(sb.ToString())

    End Sub

    Private Function Gravar() As Boolean
        Try

            Dim objOut_8744 As New List(Of SEGS8744_SPS_EO_OUT)
            Dim objIn_8744 As New SEGS8744_SPS_EO_IN
            Dim objGeral As New SEGW0158Geral_TK()

            objIn_8744.Sinistro_Id = Me.Sinistro_ID
            'objIn_8744.Sinistro_Id = 667
            objOut_8744 = objGeral.Buscar(objIn_8744)

            Dim objIn_8747 As New SEGS8747_SPU_EO_IN()

            If Not IsNothing(objOut_8744) Then
                objIn_8747 = objGeral.PreencheTodosCamposU(objOut_8744(0))
            End If

            objIn_8747.Codigo_Passo = 2

            If Not IsNothing(Usuario) Then
                objIn_8747.Usuario = Usuario.LoginWeb
            End If

            objIn_8747.Nome_Solicitante = Me.UCDadosSolicitanteVida1.Nome
            objIn_8747.Endereco_Solicitante = Me.UCDadosSolicitanteVida1.Endereco.Trim
            objIn_8747.Bairro_Solicitante = Me.UCDadosSolicitanteVida1.Bairro.Trim
            objIn_8747.Municipio_Solicitante = Me.UCDadosSolicitanteVida1.Municipio.Trim
            objIn_8747.Cep_Solicitante = Me.UCDadosSolicitanteVida1.CEP.Trim
            objIn_8747.Uf_Solicitante = Me.UCDadosSolicitanteVida1.UF.Trim

            objIn_8747.Ddd_Solicitante = Me.UCDadosSolicitanteVida1.DDD1
            objIn_8747.Fone_Solicitante = Me.UCDadosSolicitanteVida1.Telefone1
            objIn_8747.Ramal_Solicitante = Me.UCDadosSolicitanteVida1.Ramal1
            objIn_8747.Tipo_Fone_Solicitante = Me.UCDadosSolicitanteVida1.TipoTelefone1

            objIn_8747.Ddd_Solicitante1 = Me.UCDadosSolicitanteVida1.DDD2
            objIn_8747.Fone_Solicitante1 = Me.UCDadosSolicitanteVida1.Telefone2
            objIn_8747.Ramal_Solicitante1 = Me.UCDadosSolicitanteVida1.Ramal2
            objIn_8747.Tipo_Fone_Solicitante1 = Me.UCDadosSolicitanteVida1.TipoTelefone2

            objIn_8747.Ddd_Solicitante2 = Me.UCDadosSolicitanteVida1.DDD3
            objIn_8747.Fone_Solicitante2 = Me.UCDadosSolicitanteVida1.Telefone3
            objIn_8747.Ramal_Solicitante2 = Me.UCDadosSolicitanteVida1.Ramal3
            objIn_8747.Tipo_Fone_Solicitante2 = Me.UCDadosSolicitanteVida1.TipoTelefone3

            objIn_8747.Ddd_Solicitante3 = Me.UCDadosSolicitanteVida1.DDD4
            objIn_8747.Fone_Solicitante3 = Me.UCDadosSolicitanteVida1.Telefone4
            objIn_8747.Ramal_Solicitante3 = Me.UCDadosSolicitanteVida1.Ramal4
            objIn_8747.Tipo_Fone_Solicitante3 = Me.UCDadosSolicitanteVida1.TipoTelefone4

            objIn_8747.Ddd_Solicitante4 = Me.UCDadosSolicitanteVida1.DDD5
            objIn_8747.Fone_Solicitante4 = Me.UCDadosSolicitanteVida1.Telefone5
            objIn_8747.Ramal_Solicitante4 = Me.UCDadosSolicitanteVida1.Ramal5
            objIn_8747.Tipo_Fone_Solicitante4 = Me.UCDadosSolicitanteVida1.TipoTelefone5

            objIn_8747.Email_Solicitante = Me.UCDadosSolicitanteVida1.Email.Trim

            objGeral.Atualizar(objIn_8747)

            Return True
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Function

    Private Sub Avancar()
        Dim sb As New StringBuilder()
        Dim objGeral As New SEGW0158Geral_TK
        objGeral.AtualizaPasso(Sinistro_ID, 3)
        sb.Append("SEGW0158DadosAgencia.aspx")
        sb.Append("?SINISTRO_ID=" + IIf(IsNothing(Me.Sinistro_ID), "", Me.Sinistro_ID.ToString))
        sb.Append("&SLinkSeguro=" + Request("SLinkSeguro"))
        Response.Redirect(sb.ToString())

    End Sub

    Public Sub AjustaNavegacao(ByVal But As String)
        Select Case But
            Case "Evento"
                LkbPesquisa.Enabled = True
            Case "LkbSolicitante"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
            Case "LkbAgencia"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
            Case "LkbSinistroCA"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
            Case "LkbObjsegurado"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
            Case "LkbOutrosSeguros"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
                LkbObjsegurado.Enabled = True
            Case "Ressumo"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
                LkbObjsegurado.Enabled = True
            Case "DadosCliente"
                LkbDadosCliente.Enabled = True
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
        End Select
    End Sub

#End Region

    Protected Sub LkbPesquisa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbPesquisa.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158ConsultaSinistrado.aspx")
        sb.Append("?Cliente_ID=" & Cliente_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())
    End Sub

    Protected Sub LkbEvento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbEvento.Click
            Me.Voltar()
    End Sub
    Protected Sub LkbDadosCliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbDadosCliente.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158DadosCliente.aspx")
        sb.Append("?SINISTRO_ID=" & Me.Sinistro_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())
    End Sub
End Class