﻿Imports SEGL0343.Task
Imports SEGL0343.EO
Imports SEGL0343.Business

Partial Public Class SEGW0158Documentos
    Inherits BasePages
#Region "Propriedades"
    Public ReadOnly Property sinistros() As DataTable
        Get
            If Not IsNothing(Session("dtSinistrosAvisados")) Then
                Return CType(Session("dtSinistrosAvisados"), DataTable)
            Else
                Return Nothing
            End If
        End Get
    End Property
    Public ReadOnly Property Sinistro_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("SINISTRO_ID")) Then
                Return CInt(Request.QueryString("SINISTRO_ID"))
            Else
                Return -1
            End If
        End Get
    End Property
#End Region
#Region "Eventos"



    Protected Sub GVwSinistrados_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Try
            GVwSinistrados.PageIndex = e.NewPageIndex
            CarregarDocs
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            Try
                'popula os dados do grid

                CarregarDocs()

                Dim SEGS8744_In As New SEGL0343.EO.SEGS8744_SPS_EO_IN
                Dim s As List(Of SEGL0343.EO.SEGS8744_SPS_EO_OUT)
                Dim b As New SEGL0343.Business.SEGS8744_SPS_Business
                SEGS8744_In.Sinistro_Id = Me.Sinistro_ID
                s = b.seleciona(SEGS8744_In)

                If String.IsNullOrEmpty(s.Item(0).email_solicitante) Or (GVwSinistrados.Rows.Count = 0) Then
                    chkEnvio.Enabled = False
                Else
                    chkEnvio.Enabled = True
                End If

            Catch ex As Exception

                Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
                objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
            End Try


        End If
    End Sub

    Protected Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "confirmaFechaBrowser();", True)
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub btnNovoSinistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158ConsultaPropostasPendentes.aspx")
        sb.Append("?SLinkSeguro=" + Request("SLinkSeguro"))
        Response.Redirect(sb.ToString())
    End Sub
    Protected Sub chkEnvio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            If Me.chkEnvio.Checked Then
                EnviaMensagem()
                chkEnvio.Enabled = False
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('A Lista de documentos foi enviada por E-mail.');", True)
            End If
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

#End Region
#Region "Métodos"
    Private Sub EnviaMensagem()
        Try

            If Me.GVwSinistrados.Rows.Count = 0 Then
                Exit Sub
            End If

            Dim msg As New StringBuilder

            Dim e As New SEGL0343.EO.SEGS8744_SPS_EO_IN
            Dim s As List(Of SEGL0343.EO.SEGS8744_SPS_EO_OUT)
            Dim b As New SEGL0343.Business.SEGS8744_SPS_Business
            e.Sinistro_Id = Me.Sinistro_ID
            s = b.seleciona(e)
            Dim LogoTipo As String = "http://www.aliancadobrasil.com.br/alianca/home/img/logotipo.gif"


            msg.Append("<html>")

            msg.Append("<table width='100%'>")
            msg.Append("<tr>")
            msg.Append("<td>")
            msg.Append(" <a title='Home - Alian&ccedil;a do Brasil'><img src=" & LogoTipo & " alt='Home - Alian&ccedil;a do Brasil' /></a><br />")
            msg.Append("<br />")
            msg.Append("Prezado(a)," & " &nbsp; " & s(0).nome_solicitante & " <br />")
            msg.Append("<br />")
            msg.Append("Segue a lista de documentos necessária para a análise do sinistro comunicado.<br />")
            msg.Append("<br />")
            msg.Append("Estes documentos deverão ser entregues à agência de contato, informada no momento")
            msg.Append(" do aviso:<br />")
            msg.Append("<br />")
            msg.Append("Agência de contato:   " & s(0).codigo_agencia & "-" & s(0).codigo_dv_agencia & "&nbsp;")
            msg.Append("<br />")
            msg.Append("Nome:   " & " &nbsp; " & s(0).nome_agencia & "<br />")
            msg.Append("<br />")
            msg.Append("Endereço:" & " &nbsp; " & s(0).endereco_agencia & "<br />")
            msg.Append("<br />")
            msg.Append("---------------------------------------------- <br />")
            msg.Append("Documentos: <br />")
            msg.Append("<td>")
            msg.Append("</tr>")
            msg.Append("</table>")
            msg.Append("<table width='100%'>")

            'For i As Integer = 0 To Me.GVwSinistrados.Rows.Count - 1 Step 1
            '    msg.Append(Me.GVwSinistrados.Rows(i).Cells(0).Text & " " & LCase(Me.GVwSinistrados.Rows(i).Cells(2).Text) & " (" & UCase(Me.GVwSinistrados.Rows(i).Cells(1).Text) & ") <br />")
            '    msg.Append("<br />")
            'Next i

            For i As Integer = 0 To Me.GVwSinistrados.Rows.Count - 1 Step 1
                msg.Append("<tr>")
                msg.Append("<td width='5%' rowspan='2' valign='top'>")
                msg.Append(IIf(i = 0, 1, i + 1) & "º")
                msg.Append("</td>")
                'msg.Append("</tr>")
                'msg.Append("<tr>")
                msg.Append("<td width='95%'>")
                msg.Append(Me.GVwSinistrados.Rows(i).Cells(0).Text & " " & LCase(Me.GVwSinistrados.Rows(i).Cells(2).Text) & " (" & UCase(Me.GVwSinistrados.Rows(i).Cells(1).Text) & ") ")
                msg.Append("</td>")
                msg.Append("</tr>")
                msg.Append("<tr>")
                msg.Append("<td width='95%'>")
                msg.Append("    Local para obter a documentação: " & LCase(Me.GVwSinistrados.Rows(i).Cells(2).Text))
                msg.Append("</td>")
                msg.Append("<br />")
                msg.Append("</tr>")
            Next i
            'msg.Append("</table>")
            msg.Append("<tr>")
            msg.Append("<td colspan='2' width='100%'>")
            msg.Append("---------------------------------------------- ----------------------------------------------")
            msg.Append("<br />")
            msg.Append("A SEGURADORA RESERVA-SE O DIREITO DE SOLICITAR QUALQUER DOCUMENTO QUE JULGAR NECESSÁRIO")
            msg.Append("PARA A EFETIVA COMPROVAÇÃO DA COBERTURA DO SINISTRO<br />")
            msg.Append("<br />")
            msg.Append("Atenciosamente,<br />")
            msg.Append("<br />")
            msg.Append("Aliança do Brasil")
            msg.Append("<br />")
            msg.Append("Central de Atendimento aos Clientes")
            msg.Append("<br />")
            msg.Append("Tel.: 0800 729 7000")
            msg.Append("<br />")
            msg.Append("<br />")
            msg.Append("---------------------------------------------- ----------------------------------------------")
            msg.Append("<br />")
            msg.Append("Comunicação Corporativa Aliança do Brasil   ")
            msg.Append("</td>")
            msg.Append("</tr>")
            msg.Append("</table>")
            msg.Append("</div>")
            msg.Append("</body>")
            msg.Append("</html>")

            SEGL0343.Util.SendMail.Send(msg.ToString, "Documentação Básica para o aviso de sinistro ", s(0).email_solicitante)
            'ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('A lista de documentos foi enviada por e-mail.');", True)
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub
    Private Sub CarregarDocs()
        Dim en As New SEGL0343.EO.SEGS8911_SPS_EO_IN
        Dim s As New List(Of SEGL0343.EO.SEGS8911_SPS_EO_OUT)
        Dim b As New SEGL0343.Business.SEGS8911_SPS_Business

        Dim Obj8941In As New SEGS8941_SPS_EO_IN
        Dim Obj8941Out As New List(Of SEGS8941_SPS_EO_OUT)
        Dim Objt8941 As New SEGS8941_SPS_Business

        Obj8941In.Sinistro_Id = Sinistro_ID
        Obj8941Out = Objt8941.seleciona(Obj8941In)

        If Not Obj8941Out Is Nothing Then
            'recupera a lista de sinistros do datatable em memória
            For i As Integer = 0 To Obj8941Out.Count - 1 Step 1
                en.Lista &= Obj8941Out.Item(i).SINISTRO_GERADO_ID.ToString
                If i <> (Obj8941Out.Count - 1) Then
                    en.Lista &= ","
                End If
            Next i
            s = b.seleciona(en)
            Me.GVwSinistrados.DataSource = s
            Me.GVwSinistrados.DataBind()
        End If
    End Sub
#End Region

    Protected Sub GVwSinistrados_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class