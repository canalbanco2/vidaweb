﻿Imports SEGL0329
Imports SEGL0331
Imports SEGL0328
Imports SEGL0343
Imports SEGL0343.EO
Imports SEGL0343.Task
Imports System.Collections.Generic
Imports System.Text

Partial Public Class SEGW0158ConsultaSinistrado
    Inherits BasePages

#Region "Fields"

    Private objTask As New SEGW0158ConsultaSinistrado_TK
    Private objSource As List(Of SEGS8929_SPS_EO_OUT)
    Private SINISTRO_ID As String

#End Region

#Region "Propriedades"


    Public Property CPF_CNPJ() As String
        Get
            Return CType(ViewState("CPF_CNPJ"), String)
        End Get
        Set(ByVal value As String)
            ViewState("CPF_CNPJ") = value
        End Set
    End Property

    Public Property Cliente_ID() As Integer
        Get
            Return CType(ViewState("Cliente_ID"), Integer)
        End Get
        Set(ByVal value As Integer)
            ViewState("Cliente_ID") = value
        End Set
    End Property

    Public Property sProposta() As Boolean
        Get
            Return CType(ViewState("sProposta"), Integer)
        End Get
        Set(ByVal value As Boolean)
            ViewState("sProposta") = value
        End Set
    End Property


    Public ReadOnly Property PS() As Integer
        Get
            If Not IsNothing(Request.QueryString("PS")) Then
                Return Request.QueryString("PS")
            Else
                Return Nothing
            End If
        End Get
    End Property

#End Region

#Region "Funções"

    Private Sub Voltar()

        Dim sb As New StringBuilder()
        sb.Append("SEGW0158ConsultaPropostasPendentes.aspx")
        sb.Append("?SLinkSeguro=" + Request.QueryString("slinkseguro"))
        Response.Redirect(sb.ToString())

    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Me.PS > 0 Then
                'Response.Write("Gravar retorno Consulta ViewState e direcionar para o frmSolicitante")
                'Me.txtValorPesquisa.Text = Me.PS.ToString
            End If

            If Not Page.IsPostBack Then
                'btnAvisarSemProposta.Visible = False
                Me.MontarComboTipoPesquisa()

            End If

            If Me.ddlSinistradoTipoPesquisa.SelectedItem.Text.Equals("-Selecione-") Then
                Me.txtValorPesquisa.Visible = False
                btnPesquisar_fora.Visible = False
                txtLabelValPesquisa.Visible = False
            Else
                Me.txtValorPesquisa.Visible = True
                btnPesquisar_fora.Visible = True
                txtLabelValPesquisa.Visible = True
                Me.txtValorPesquisa.Focus()
            End If

            Me.txtValorPesquisa.Focus()            
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub gvwSinistro_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Try
            gvwSinistro.PageIndex = e.NewPageIndex
            Me.GenerateGridSinistrado(False)
            Me.btnContinuar_fora.Visible = False

        Catch ex As Exception
            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            'Limpar grid
            Me.gvwSinistro.DataSource = Nothing
            meeCPF_CNPJ.ClearMaskOnLostFocus = True

            Me.txtValorPesquisa.Text = Me.txtValorPesquisa.Text.Replace(".", "")
            Me.txtValorPesquisa.Text = Me.txtValorPesquisa.Text.Replace("-", "")
            Me.txtValorPesquisa.Text = Me.txtValorPesquisa.Text.Replace("/", "")

            If Me.txtValorPesquisa.Text = "_________" Then
                Exit Sub
            End If

            If Me.ddlSinistradoTipoPesquisa.SelectedItem.Text.Equals("-Selecione-") Then
                ScriptManager.RegisterStartupScript(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Escolha um critério de pesquisa.\n" & "')", True)
                ScriptManager.RegisterStartupScript(Page, Me.GetType(), Guid.NewGuid().ToString(), "document.getElementById('txtValorPesquisa').focus();", True)
                btnPesquisar_fora.Visible = False
                Me.ddlSinistradoTipoPesquisa.Focus()
                Exit Sub
            End If

            If Me.txtValorPesquisa.Text.Length = 0 Then

                ScriptManager.RegisterStartupScript(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Informe o " & Me.ddlSinistradoTipoPesquisa.SelectedItem.Text & " para pesquisa" & "')", True)
                ScriptManager.RegisterStartupScript(Page, Me.GetType(), Guid.NewGuid().ToString(), "document.getElementById('txtValorPesquisa').focus();", True)
                Me.txtValorPesquisa.Focus()
                Exit Sub
            End If

            If Me.ddlSinistradoTipoPesquisa.SelectedItem.Text.Equals("Proposta AB") Then
                Me.sProposta = False
            End If

            If Me.ddlSinistradoTipoPesquisa.SelectedItem.Text.Equals("C.P.F.") Then
                If txtValorPesquisa.Text.Length < 11 Then
                    ScriptManager.RegisterStartupScript(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Informe o " & Me.ddlSinistradoTipoPesquisa.SelectedItem.Text & " para pesquisa" & "')", True)
                    txtValorPesquisa.Text = ""
                    ScriptManager.RegisterStartupScript(Page, Me.GetType(), Guid.NewGuid().ToString(), "document.getElementById('txtValorPesquisa').focus();", True)
                    Me.txtValorPesquisa.Focus()
                    Exit Sub
                End If
            End If


            If Me.ddlSinistradoTipoPesquisa.SelectedItem.Text.Equals("C.N.P.J.") Then
                If txtValorPesquisa.Text.Length < 14 Then
                    ScriptManager.RegisterStartupScript(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Informe o " & Me.ddlSinistradoTipoPesquisa.SelectedItem.Text & " para pesquisa" & "')", True)
                    txtValorPesquisa.Text = ""
                    ScriptManager.RegisterStartupScript(Page, Me.GetType(), Guid.NewGuid().ToString(), "document.getElementById('txtValorPesquisa').focus();", True)
                    Me.txtValorPesquisa.Focus()
                    Exit Sub
                End If
            End If

            Me.GenerateGridSinistrado(True)
            Me.txtValorPesquisa.Focus()


        Catch ex As Exception
            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    ''' <summary>
    ''' Trata Seleção de dados do Grid, preenche parametros
    ''' para o Proximo Passo.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Flávio Fernandes BSI 10/06/2010.</remarks>

    Protected Sub gvwSinistro_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim rowSelecionado As GridViewRow = Me.gvwSinistro.SelectedRow

            If Not IsNothing(rowSelecionado) Then
                Me.Cliente_ID = CType(rowSelecionado.Cells(3).Text, Integer)
                Me.CPF_CNPJ = CType(rowSelecionado.Cells(2).Text, Double).ToString
            End If

            If Me.Cliente_ID > 0 Then
                Me.btnContinuar_fora.Visible = True
            End If

            ''Valida o passo e so direciona para solicitante quando PS for > 0
            If Me.PS > 0 Then
                DirecionarPaginaSolicitante(Me.Cliente_ID)
            End If

        Catch ex As Exception
            '       ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Ocorreu um erro no sistema e foi reportado, Reinicie o proceso ou aguarde correção.');", True)
            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Public Sub DirecionarPaginaSolicitante(ByVal Cliente_ID As Integer)

        Dim sb As New StringBuilder()
        sb.Append("SEGW0158Solicitante.aspx")
        sb.Append("?Cliente_ID=" & Cliente_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())


    End Sub

    Protected Sub MontarComboTipoPesquisa()
        Try
            With Me.ddlSinistradoTipoPesquisa
                .Items.Add(New ListItem("-Selecione-", "0"))
                .Items.Add(New ListItem("C.P.F.", "1"))
                .Items.Add(New ListItem("C.N.P.J.", "2"))
                .Items.Add(New ListItem("Nome do Cliente", "3"))
                .Items.Add(New ListItem("Proposta AB", "4"))
                .Items.Add(New ListItem("Aviso Sinistro AB", "5"))
                .Items.Add(New ListItem("Aviso Sinistro BB", "6"))
            End With
            Me.txtValorPesquisa.Visible = False
            btnPesquisar_fora.Visible = False
        Catch ex As Exception
            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Private Sub GenerateGridSinistrado(ByVal reNew As Boolean)
        Try
            'If reNew Then
            '    objSource = objTask.ConsultarSinistrados(GetFilter())
            '    ' ViewState("grdSinistrado") = objSource
            '    'Else
            '    '    objSource = CType(ViewState("grdSinistrado"), List(Of SEGS8929_SPS_EO_OUT))
            'End If
            objSource = objTask.ConsultarSinistrados(GetFilter())
            gvwSinistro.DataSource = objSource
            gvwSinistro.DataBind()
            '  gvwSinistro.SelectedIndex = -1

            If objSource.Count > 0 Then
                btnAvisarSemProposta_fora.Visible = False
            Else
                btnAvisarSemProposta_fora.Visible = True
                btnContinuar_fora.Visible = False
            End If

            If objSource.Count = 1 Then
                gvwSinistro.SelectedIndex = 0

                Dim rowSelecionado As GridViewRow = Me.gvwSinistro.SelectedRow

                If Not IsNothing(rowSelecionado) Then
                    Me.Cliente_ID = CType(rowSelecionado.Cells(3).Text, Integer)
                    Me.CPF_CNPJ = CType(rowSelecionado.Cells(2).Text, Double).ToString
                End If

                If Me.Cliente_ID > 0 Then
                    Me.btnContinuar_fora.Visible = True
                End If

                ''Valida o passo e so direciona para solicitante quando PS for > 0
                If Me.PS > 0 Then
                    DirecionarPaginaSolicitante(Me.Cliente_ID)
                End If
            Else
                If objSource.Count > 1 Then
                    gvwSinistro.SelectedIndex = -1
                End If
            End If
        Catch ex As Exception
            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Private Function GetFilter() As SEGS8929_SPS_EO_IN
        Try
            Dim objSinistrado As New SEGS8929_SPS_EO_IN()
            objSinistrado.Parametro = Me.txtValorPesquisa.Text
            objSinistrado.Tipo_Consulta = Me.ddlSinistradoTipoPesquisa.SelectedItem.Text

            Return objSinistrado

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Function

    Protected Sub ddlSinistradoTipoPesquisa_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.txtValorPesquisa.Focus()
        Try
            If Me.ddlSinistradoTipoPesquisa.SelectedItem.Text.Equals("Proposta AB") Then
                Me.txtValorPesquisa.Text = String.Empty
                Me.meeCPF_CNPJ.Enabled = True
                Me.meeCPF_CNPJ.Mask = "999999999"
                Me.txtValorPesquisa.Width = 100
                Me.txtValorPesquisa.MaxLength = 9
                Me.txtValorPesquisa.Focus()
                Exit Sub
            End If

            If Me.ddlSinistradoTipoPesquisa.SelectedItem.Text.Equals("Aviso Sinistro AB") Then
                Me.txtValorPesquisa.Text = String.Empty
                Me.meeCPF_CNPJ.Enabled = True
                Me.meeCPF_CNPJ.Mask = "999999999"
                Me.txtValorPesquisa.Width = 100
                Me.txtValorPesquisa.MaxLength = 10
                Me.txtValorPesquisa.Focus()
                Exit Sub
            End If

            If Me.ddlSinistradoTipoPesquisa.SelectedItem.Text.Equals("Aviso Sinistro BB") Then
                Me.txtValorPesquisa.Text = String.Empty
                Me.meeCPF_CNPJ.Enabled = True
                Me.meeCPF_CNPJ.Mask = "999999999"
                Me.txtValorPesquisa.Width = 100
                Me.txtValorPesquisa.MaxLength = 10
                Me.txtValorPesquisa.Focus()
                Exit Sub
            End If

            If Me.ddlSinistradoTipoPesquisa.SelectedItem.Text.Equals("C.P.F.") Then
                Me.txtValorPesquisa.Text = String.Empty
                Me.meeCPF_CNPJ.Mask = "999,999,999-99"
                Me.meeCPF_CNPJ.Enabled = True
                Me.txtValorPesquisa.Width = 100
                Me.txtValorPesquisa.Focus()
                Exit Sub
            End If

            If Me.ddlSinistradoTipoPesquisa.SelectedItem.Text.Equals("C.N.P.J.") Then
                Me.txtValorPesquisa.Text = String.Empty
                Me.meeCPF_CNPJ.Mask = "99,999,999/9999-99"
                Me.txtValorPesquisa.Width = 100
                Me.txtValorPesquisa.Focus()
                Me.meeCPF_CNPJ.Enabled = True
                Exit Sub
            Else
                Me.txtValorPesquisa.Text = String.Empty
                Me.meeCPF_CNPJ.Enabled = False
                Me.txtValorPesquisa.MaxLength = 60
                Me.txtValorPesquisa.Width = 320
                Me.txtValorPesquisa.Focus()
                Exit Sub
            End If

            Me.txtValorPesquisa.Visible = True
            btnPesquisar_fora.Visible = True
            txtLabelValPesquisa.Visible = True




            Me.txtValorPesquisa.Focus()

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub btnContinuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim sb As New StringBuilder()
        Try
            Me.GravarPasso_1_Sinistro()

            Dim objGeral As New SEGW0158Geral_TK

            objGeral.AtualizaPasso(SINISTRO_ID, 1)

        Catch ex As Exception


            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
            sb = Nothing

        End Try

        sb.Append("SEGW0158Evento.aspx")
        sb.Append("?Cliente_ID=" & Cliente_ID)
        sb.Append("&SINISTRO_ID=" & SINISTRO_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))
        Response.Redirect(sb.ToString())


    End Sub
    Public Sub GeraIDSinistroSemProposta()
        Try
            Dim TaskGravaPassoSinistro As New SEGW158SinistroInserirPassosDanos_TK
            Dim EO_IN As New SEGS8866_SPI_EO_IN
            Dim EO_OUT As List(Of EO.SEGS8866_SPI_EO_OUT)

            EO_IN.Codigo_Passo = 0

            EO_IN.Usuario = Usuario.LoginWeb
            EO_IN.Cliente_Id = Me.Cliente_ID
            EO_IN.Dt_Inclusao = Now.Date


            EO_OUT = TaskGravaPassoSinistro.Inserir_Passo_Danos(EO_IN)

            SINISTRO_ID = EO_OUT.Item(0).sinistro_id


        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Public Sub GravarPasso_1_Sinistro()
        Try
            Dim TaskGravaPassoSinistro As New SEGW158SinistroInserirPassosDanos_TK
            Dim EO_IN As New SEGS8866_SPI_EO_IN
            Dim EO_OUT As List(Of EO.SEGS8866_SPI_EO_OUT)

            If Me.Cliente_ID <> 0 Then
                EO_IN.Cliente_Id = Me.Cliente_ID
                EO_IN.Nome_Segurado = gvwSinistro.SelectedRow.Cells(1).Text
                EO_IN.Cpf_Cnpj_Segurado = gvwSinistro.SelectedRow.Cells(2).Text
            End If


            EO_IN.Codigo_Passo = 0
            EO_IN.Usuario = Usuario.LoginWeb


            EO_IN.Dt_Inclusao = Now.Date

            EO_OUT = TaskGravaPassoSinistro.Inserir_Passo_Danos(EO_IN)

            SINISTRO_ID = EO_OUT.Item(0).sinistro_id


        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub btnVoltar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Voltar()
    End Sub

    Protected Sub btnAvisarSemProposta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAvisarSemProposta.Click

        Dim sb As New StringBuilder()
        Dim objGeral As New SEGW0158Geral_TK

            GeraIDSinistroSemProposta()

            sb.Append("SEGW0158DadosCliente.aspx")
            sb.Append("?SINISTRO_ID=" & SINISTRO_ID)
            sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())

    End Sub

    Protected Sub txtValorPesquisa_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.btnAvisarSemProposta.Focus()
    End Sub
End Class
