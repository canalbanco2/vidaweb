﻿<%@ Page Language="vb" EnableViewStateMac="false"  AutoEventWireup="false" CodeBehind="frmQuestionario.aspx.vb" Inherits="SEGW0158.frmQuestionario" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>SUSAB - Aviso de Sinistro RE - Questionário</title>
    <link href="CSS/BaseStyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src='<%= ResolveUrl("~/js/SEGW0158Scripts.js") %>'></script>     
</head>
<body>
    <form id="form1" runat="server">
        <fieldset id="fieldset_questionario" runat="server" style="width:790px; height:auto">
            <legend >Questionário</legend>                                  
        </fieldset>  
        <div class="n790">
            <br />
            <div class="n197 divLeft alignLeft">
                <asp:Button ID="btnVoltar" runat="server" CssClass="Botao" Text="<< Voltar" Visible="true" Width="150px" /> 
            </div>
            <div class="n197 divLeft alignLeft">
                <asp:Button ID="btnContinuar" runat="server" CssClass="Botao" Text="Continuar >>" Visible="true" Width="150px" ValidationGroup="filter"/>
            </div>
            <div class="n197 divLeft alignRight">
                <asp:Button ID="btnSair" runat="server" CssClass="Botao" Text="Sair" Visible="true" Width="150px" />
            </div>
            <div class="n197 divLeft alignRight">                    
                <asp:Button ID="btnCalcular" runat="server" CssClass="Botao" Text="Calcular" Visible="true" Width="150px" ValidationGroup="filter"/>            
                <asp:CustomValidator ID="cvtFields" runat="server" ClientValidationFunction="ValidarQuestionario"
                    Display="None" ErrorMessage="Informe pelo menos um filtro para pesquisa." ValidationGroup="filter">
                </asp:CustomValidator>
                
            </div>
        </div>
    </form>
</body>
</html>
