﻿<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SEGW058Navegacao.aspx.vb"
    Inherits="SEGW0158.SEGW051Navegacao" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SUSAB - Aviso de Sinistro RE - Dados da Ocorrência</title>
    <link href="CSS/Sinistro.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src='<%= ResolveUrl("~/js/SEGW0158Scripts.js") %>'></script>

</head>
<body>
    <form id="form1" runat="server" style="text-align: center;">
        <fieldset class="n800" runat="server" id="fildBotoes">
            <div id="Div1" style="width: 950px; height: 60px">
                <div class="n950">
                    <div class="n950">
                    <br />
                        <fieldset class="n120">
                            <asp:LinkButton ID="LkbPesquisa" Enabled="false" CssClass="BtnNavega" runat="server" Font-Bold="True">Pausar Processo</asp:LinkButton>
                        </fieldset>
                        <!--  &nbsp;&nbsp;|&nbsp;&nbsp; -->
                        <fieldset class="n80">
                            <asp:LinkButton ID="LkbEvento" Enabled="false"  CssClass="BtnNavega" Font-Bold="True" runat="server">Evento</asp:LinkButton>
                        </fieldset>
                        <!--   &nbsp;&nbsp;|&nbsp;&nbsp; -->
                        <fieldset class="n120">
                            <asp:LinkButton ID="LkbSolicitante"  Enabled="false" CssClass="BtnNavega" Font-Bold="True"  runat="server">Dados Solicitante</asp:LinkButton>
                        </fieldset>
                        <!--   &nbsp;&nbsp;|&nbsp;&nbsp; -->
                        <fieldset class="n120">
                            <asp:LinkButton ID="LkbSinistroCA"  Enabled="false" CssClass="BtnNavega"  Font-Bold="True" runat="server">Analise de Sinistro</asp:LinkButton></fieldset>
                        <fieldset class="n120">
                            <asp:LinkButton ID="LkbObjsegurado"  Enabled="false" CssClass="BtnNavega"  Font-Bold="True" runat="server">Objeto Segurado</asp:LinkButton>
                        </fieldset>
                        <fieldset class="n120">
                            <asp:LinkButton ID="LkbDadosCliente"  Enabled="false" CssClass="BtnNavega" Font-Bold="True"  runat="server">Dados do Cliente</asp:LinkButton></fieldset>
                        <fieldset class="n120">
                            <asp:LinkButton ID="LkbOutrosSeguros"  Enabled="false" CssClass="BtnNavega"  Font-Bold="True" runat="server">Outros Seguros</asp:LinkButton></fieldset>
                        <fieldset class="n80">
                            <asp:LinkButton ID="LkbAgencia"  Enabled="false" CssClass="BtnNavega"  Font-Bold="True" runat="server">Agência</asp:LinkButton></fieldset>
                    <br />
                    </div>
                </div>
            </div>
        </fieldset>
    </form>
</body>
</html>
