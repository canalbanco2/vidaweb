﻿Imports SEGL0343.EO
Imports SEGL0343.Business
Imports SEGL0343.Task
Imports SEGL0329


Partial Public Class SEGW0158ResumoSinistroRE
    Inherits BasePages
    Dim Sinistro As New SEGL0343.GeraSinistroRE_Business
    Private _proposta_id As String
    Private SinistroID As Integer

#Region "Propriedades"

    Public ReadOnly Property Evento_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("Evento_ID")) Then
                Return Request.QueryString("Evento_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property SubEvento_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("SubEvento_ID")) Then
                Return Request.QueryString("SubEvento_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property Sinistro_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("SINISTRO_ID")) Then
                Return Request.QueryString("SINISTRO_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property Cliente_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("Cliente_ID")) Then
                Return Request.QueryString("Cliente_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property Dt_Ocorrencia() As String
        Get
            If Not IsNothing(Request.QueryString("Dt_Ocorrencia")) Then
                Return Request.QueryString("Dt_Ocorrencia")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public Property Proposta_ID() As String
        Get
            Return _proposta_id
        End Get
        Set(ByVal value As String)
            _proposta_id = value
        End Set
    End Property

    Public ReadOnly Property CPF_CNPJ() As String
        Get
            If Not IsNothing(Request.QueryString("CPF_CNPJ")) Then

                Return Request.QueryString("CPF_CNPJ")

            Else

                Return Nothing

            End If
        End Get
    End Property

#End Region

#Region "Eventos"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                CarregaDados()
                Me.DadosSolicitante1.btnSegurado_Visible = False
            End If
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try
    End Sub

    Protected Sub btnVoltar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoltar.Click
        Try
            Me.Voltar()
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try
    End Sub

    Protected Sub btnAvisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAvisar.Click

        Try

            If Me.hddConfirm.Value = "n" Then
                Exit Sub
            End If
            Sinistro.MsgErro = ""
            Sinistro.Aviso(Sinistro_ID)
            If Not String.IsNullOrEmpty(Sinistro.MsgErro) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & Sinistro.MsgErro & "');", True)

            End If
            Session("listaPropostasExcluir") = ""


        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

        Avancar()

    End Sub

#End Region


#Region "Métodos"

    Private Sub Voltar()
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158OutrosSeguros.aspx")
        sb.Append("?SINISTRO_ID=" & Me.Sinistro_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())

    End Sub

    Private Sub Avancar()
        Dim objGeral As New SEGW0158Geral_TK
        Dim sb As New StringBuilder()

        Try

            objGeral.AtualizaPasso(Sinistro_ID, 99)


            sb.Append("SEGW0158SinistroAvisado.aspx")
            sb.Append("?SINISTRO_ID=" & Me.Sinistro_ID)
            sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))


        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

        Response.Redirect(sb.ToString())
    End Sub

    Private Sub CarregaDados()
        Try
            'Dim i As Integer
            Dim campo As BoundField

            Dim TaskPassosSinistro As New SEGW158SinistroInserirPassosDanos_TK
            Dim EO_IN As New SEGS8744_SPS_EO_IN
            Dim Eo_out As List(Of SEGS8744_SPS_EO_OUT)

            Dim EO_In_Propostas As New SEGS8892_SPS_EO_IN
            Dim EO_Out_Propostas As List(Of SEGS8892_SPS_EO_OUT)
            Dim EO_Business_Propostas As New SEGS8892_SPS_Business

            EO_IN.Sinistro_Id = Sinistro_ID


            Eo_out = TaskPassosSinistro.Consultar_Passo_Danos(EO_IN)

            UCAgencia.CodigoAgencia = Eo_out.Item(0).codigo_agencia
            UCAgencia.CodigoAgenciaReadOnly = True
            UCAgencia.DVAgencia = Eo_out.Item(0).codigo_dv_agencia
            UCAgencia.DVAgenciaReadOnly = True
            UCAgencia.NomeAgencia = Eo_out.Item(0).nome_agencia
            UCAgencia.NomeAgenciaReadOnly = True
            UCAgencia.EnderecoAgencia = Eo_out.Item(0).endereco_agencia
            UCAgencia.EnderecoAgenciaReadOnly = True
            UCAgencia.BairroAgencia = Eo_out.Item(0).bairro_agencia
            UCAgencia.BairroAgenciaReadOnly = True
            UCAgencia.Municipio = Eo_out.Item(0).municipio_agencia
            UCAgencia.MunicipioReadOnly = True
            UCAgencia.UF = Eo_out.Item(0).uf_agencia
            UCAgencia.UFReadOnly = True

            UCOcorrencia1.DataOcorrencia = Eo_out.Item(0).data_ocorrencia
            UCOcorrencia1.DataOcorrenciaReadOnly = True
            UCOcorrencia1.HoraOcorrencia = Eo_out.Item(0).hora_ocorrencia
            UCOcorrencia1.HoraOcorrenciaReadOnly = True

            EO_In_Propostas.Sinistro_Id_Passos = Sinistro_ID
            EO_Out_Propostas = EO_Business_Propostas.seleciona(EO_In_Propostas)

            If Eo_out.Item(0).ind_sem_proposta = True Then
                UCOcorrencia1.Reanalise = ""
                UCOcorrencia1.lblSinitro_text = "Sinistro AB"
            ElseIf String.IsNullOrEmpty(EO_Out_Propostas.Item(0).TXT_REANALISE) Then
                If String.IsNullOrEmpty(EO_Out_Propostas.Item(0).TP_AVISO_CAPTION) Then
                    UCOcorrencia1.Reanalise = "Não"
                    UCOcorrencia1.lblSinitro_text = "Reanálise"
                Else
                    UCOcorrencia1.Reanalise = IIf(String.IsNullOrEmpty(EO_Out_Propostas.Item(0).TXT_REANALISE), "", EO_Out_Propostas.Item(0).TXT_REANALISE)
                    UCOcorrencia1.lblSinitro_text = EO_Out_Propostas.Item(0).TP_AVISO_CAPTION
                End If
            Else
                UCOcorrencia1.Reanalise = EO_Out_Propostas.Item(0).TXT_REANALISE
                UCOcorrencia1.lblSinitro_text = EO_Out_Propostas.Item(0).TP_AVISO_CAPTION
            End If
            UCOcorrencia1.lblSinitro_visible = True 'IIf(EO_Out_Propostas.Item(0).LBL_SINISTRO = "True", True, False)
            UCOcorrencia1.ReanaliseVisible = True   'IIf(EO_Out_Propostas.Item(0).LBL_SINISTRO = "True", True, False)

            UCOcorrencia1.ReanaliseReadOnly = True

            UCOcorrencia1.Evento = Eo_out.Item(0).codigo_evento
            UCOcorrencia1.EventoEnabled = False

            UCOcorrencia1.CarregaSubEventos(Eo_out.Item(0).sub_evento)
            UCOcorrencia1.SubEvento = Eo_out.Item(0).sub_evento.ToString()
            UCOcorrencia1.SubEventoEnabled = False

            UCOcorrencia1.Observacao = Eo_out.Item(0).descricao_evento
            UCOcorrencia1.ObservacaoReadOnly = True

            DadosSolicitante1.Nome = Eo_out.Item(0).nome_solicitante
            DadosSolicitante1.NomeReadOnly = True

            DadosSolicitante1.Endereco = Eo_out.Item(0).endereco_solicitante
            DadosSolicitante1.EnderecoReadOnly = True

            DadosSolicitante1.Bairro = Eo_out.Item(0).bairro_solicitante
            DadosSolicitante1.BairroReadOnly = True

            DadosSolicitante1.PreencheComboUF()
            DadosSolicitante1.UF = Eo_out.Item(0).uf_solicitante
            DadosSolicitante1.UFEnabled = False

            DadosSolicitante1.PreencheComboMunicipio(DadosSolicitante1.UF)
            DadosSolicitante1.Municipio = Eo_out.Item(0).municipio_solicitante
            DadosSolicitante1.MunicipioEnabled = False

            DadosSolicitante1.CEP = Eo_out.Item(0).cep_solicitante
            DadosSolicitante1.CEPReadOnly = True

            DadosSolicitante1.DDD1 = Eo_out.Item(0).ddd_solicitante
            DadosSolicitante1.DDD1ReadOnly = True
            DadosSolicitante1.DDD2 = Eo_out.Item(0).ddd_solicitante1
            DadosSolicitante1.DDD2ReadOnly = True
            DadosSolicitante1.DDD3 = Eo_out.Item(0).ddd_solicitante2
            DadosSolicitante1.DDD3ReadOnly = True
            DadosSolicitante1.DDD4 = Eo_out.Item(0).ddd_solicitante3
            DadosSolicitante1.DDD4ReadOnly = True
            DadosSolicitante1.DDD5 = Eo_out.Item(0).ddd_solicitante4
            DadosSolicitante1.DDD5ReadOnly = True

            DadosSolicitante1.Telefone1 = Eo_out.Item(0).fone_solicitante
            DadosSolicitante1.Telefone1ReadOnly = True
            DadosSolicitante1.Telefone2 = Eo_out.Item(0).fone_solicitante1
            DadosSolicitante1.Telefone2ReadOnly = True
            DadosSolicitante1.Telefone3 = Eo_out.Item(0).fone_solicitante2
            DadosSolicitante1.Telefone3ReadOnly = True
            DadosSolicitante1.Telefone4 = Eo_out.Item(0).fone_solicitante3
            DadosSolicitante1.Telefone4ReadOnly = True
            DadosSolicitante1.Telefone5 = Eo_out.Item(0).fone_solicitante4
            DadosSolicitante1.Telefone5ReadOnly = True

            DadosSolicitante1.Ramal1 = Eo_out.Item(0).ramal_solicitante
            DadosSolicitante1.Ramal1ReadOnly = True
            DadosSolicitante1.Ramal2 = Eo_out.Item(0).ramal_solicitante1
            DadosSolicitante1.Ramal2ReadOnly = True
            DadosSolicitante1.Ramal3 = Eo_out.Item(0).ramal_solicitante2
            DadosSolicitante1.Ramal3ReadOnly = True
            DadosSolicitante1.Ramal4 = Eo_out.Item(0).ramal_solicitante3
            DadosSolicitante1.Ramal4ReadOnly = True
            DadosSolicitante1.Ramal5 = Eo_out.Item(0).ramal_solicitante4
            DadosSolicitante1.Ramal5ReadOnly = True


            DadosSolicitante1.TipoTelefone1 = RetornaCodTipoTel(Eo_out.Item(0).tipo_fone_solicitante)
            DadosSolicitante1.TipoTelefone1Enabled = False
            DadosSolicitante1.TipoTelefone2 = RetornaCodTipoTel(Eo_out.Item(0).tipo_fone_solicitante1)
            DadosSolicitante1.TipoTelefone2Enabled = False
            DadosSolicitante1.TipoTelefone3 = RetornaCodTipoTel(Eo_out.Item(0).tipo_fone_solicitante2)
            DadosSolicitante1.TipoTelefone3Enabled = False
            DadosSolicitante1.TipoTelefone4 = RetornaCodTipoTel(Eo_out.Item(0).tipo_fone_solicitante3)
            DadosSolicitante1.TipoTelefone4Enabled = False
            DadosSolicitante1.TipoTelefone5 = RetornaCodTipoTel(Eo_out.Item(0).tipo_fone_solicitante4)
            DadosSolicitante1.TipoTelefone5Enabled = False

            DadosSolicitante1.Email = Eo_out.Item(0).email_solicitante
            DadosSolicitante1.EmailReadOnly = True

            DadosSolicitante1.ibtPesquisarCEPVisible = False

            Dim objInsPropostas_In As New SEGS8892_SPS_EO_IN
            Dim objInsPropostas_Out As List(Of SEGS8892_SPS_EO_OUT)
            Dim objInsPropostas_Business As New SEGS8892_SPS_Business

            objInsPropostas_In.Sinistro_Id_Passos = Sinistro_ID
            objInsPropostas_In.Aviso = "AVSR"
            objInsPropostas_Out = objInsPropostas_Business.seleciona(objInsPropostas_In)


            campo = New BoundField
            campo.DataField = "Aviso"
            campo.SortExpression = "Aviso"
            campo.HeaderText = "Aviso"
            Me.grdPropostasAfetadas2.Columns.Add(campo)

            campo = New BoundField
            campo.DataField = "PROPOSTA_AB"
            campo.SortExpression = "PROPOSTA_AB"
            campo.HeaderText = "Proposta AB"
            Me.grdPropostasAfetadas2.Columns.Add(campo)

            campo = New BoundField
            campo.DataField = "PROPOSTA_BB"
            campo.SortExpression = "PROPOSTA_AB"
            campo.HeaderText = "Proposta BB"
            Me.grdPropostasAfetadas2.Columns.Add(campo)

            campo = New BoundField
            campo.DataField = "SITUACAO"
            campo.SortExpression = "SITUACAO"
            campo.HeaderText = "Situação"
            Me.grdPropostasAfetadas2.Columns.Add(campo)

            campo = New BoundField
            campo.DataField = "PRODUTO"
            campo.SortExpression = "PRODUTO"
            campo.HeaderText = "Produto"
            Me.grdPropostasAfetadas2.Columns.Add(campo)

            campo = New BoundField
            campo.DataField = "NOME_PRODUTO"
            campo.SortExpression = "NOME_PRODUTO"
            campo.HeaderText = "Nome do Produto"
            Me.grdPropostasAfetadas2.Columns.Add(campo)

            campo = New BoundField
            campo.DataField = "TIPO_RAMO"
            campo.SortExpression = "TIPO_RAMO"
            campo.HeaderText = "Tipo Ramo"
            Me.grdPropostasAfetadas2.Columns.Add(campo)

            campo = New BoundField
            campo.DataField = "INICIO_VIGENCIA"
            campo.SortExpression = "INICIO_VIGENCIA"
            campo.HeaderText = "Inicio de Vigência"
            Me.grdPropostasAfetadas2.Columns.Add(campo)

            campo = New BoundField
            campo.DataField = "FIM_VIGENCIA"
            campo.SortExpression = "FIM_VIGENCIA"
            campo.HeaderText = "Fim de Vigência"
            Me.grdPropostasAfetadas2.Columns.Add(campo)

            Me.grdPropostasAfetadas2.DataSource = objInsPropostas_Out
            Me.grdPropostasAfetadas2.DataBind()

            Me.DadosAviso1.Sinistro_Id = Sinistro_ID
            Me.DadosAviso1.load_UC()

            Me.UCVistora.PreencheComboUF()
            Me.UCVistora.PreencheCombosTpTelefone()

            Me.UCVistora.Identificado = False
            Me.UCVistora.IdentificadoEnabled = False

            Me.UCVistora.LCEndereco = Eo_out.Item(0).endereco_vistoria
            Me.UCVistora.LCEnderecoReadOnly = True
            Me.UCVistora.LCBairro = Eo_out.Item(0).bairro_vistoria
            Me.UCVistora.LCBairroReadOnly = True
            Me.UCVistora.LCUFText = Eo_out.Item(0).uf_vistoria
            Me.UCVistora.LCUFTextEnable = False
            Me.UCVistora.LCMunicipioText = Eo_out.Item(0).municipio_vistoria
            Me.UCVistora.LCMunicipioTextEnable = False
            Me.UCVistora.LCCEP = Eo_out.Item(0).cep_vistoria
            Me.UCVistora.LCCEPReadOnly = True

            Me.UCVistora.VistNomeContato = Eo_out.Item(0).contato_vistoria
            Me.UCVistora.VistNomeContatoReadOnly = True
            Me.UCVistora.VistEndereco = Eo_out.Item(0).endereco_vistoria
            Me.UCVistora.VistEnderecoReadOnly = True
            Me.UCVistora.VistBairro = Eo_out.Item(0).bairro_vistoria
            Me.UCVistora.VistBairroReadOnly = True
            Me.UCVistora.VistUFText = Eo_out.Item(0).uf_vistoria
            Me.UCVistora.VistUFTextEnable = False
            Me.UCVistora.VistMunicipioText = Eo_out.Item(0).municipio_vistoria
            Me.UCVistora.VistMunicipioTextEnable = False
            Me.UCVistora.VistCEP = Eo_out.Item(0).cep_vistoria
            Me.UCVistora.VistCEPReadOnly = True
            Me.UCVistora.VistDDD1 = Eo_out.Item(0).ddd_fone_vistoria
            Me.UCVistora.VistDDD1ReadOnly = True
            Me.UCVistora.VistDDD2 = Eo_out.Item(0).ddd_fone2_vistoria
            Me.UCVistora.VistDDD2ReadOnly = True
            Me.UCVistora.VistTelefone1 = Eo_out.Item(0).fone_vistoria
            Me.UCVistora.VistTelefone1ReadOnly = True
            Me.UCVistora.VistTelefone2 = Eo_out.Item(0).fone2_vistoria
            Me.UCVistora.VistTelefone2ReadOnly = True
            Me.UCVistora.VistTpTelefone1Text = Eo_out.Item(0).tipo_fone_vistoria
            Me.UCVistora.VistTpTelefone1TextEnable = False
            Me.UCVistora.VistTpTelefone2Text = Eo_out.Item(0).tipo_fone2_vistoria
            Me.UCVistora.VistTpTelefone2TextEnable = False

            Dim objPropostas_IN As New SEGS8892_SPS_EO_IN
            Dim objPropostas_OUT As New List(Of SEGS8892_SPS_EO_OUT)
            Dim objPropostas_Business As New SEGS8892_SPS_Business

            objPropostas_IN.Sinistro_Id_Passos = Sinistro_ID
            objPropostas_OUT = objPropostas_Business.seleciona(objPropostas_IN)

            Me.btnAvisar.Attributes.Add("OnClick", "confirmaGeracaoAviso(" & Me.hddConfirm.ClientID & ", '" & IIf(Eo_out.Item(0).ind_sem_proposta, "False", "True") & "')")



            Dim Obj_busSinistrado As New SEGW0158ConsultaSinistrado_TK
            Dim Obj_outSinistrado As List(Of SEGL0343.EO.SEGS8929_SPS_EO_OUT)
            Dim Obj_inSinistrado As New SEGL0343.EO.SEGS8929_SPS_EO_IN
            Obj_inSinistrado.Tipo_Consulta = "cliente_id"
            Obj_inSinistrado.Parametro = Eo_out.Item(0).cliente_id
            Obj_outSinistrado = Obj_busSinistrado.ConsultarSinistrados(Obj_inSinistrado)

            Sinistro.Sinistrado = Obj_outSinistrado
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try
    End Sub

    Private Function RetornaCodTipoTel(ByVal Descricao As String)
        Try
            Select Case Descricao
                Case "-Selecione-"
                    RetornaCodTipoTel = "0"
                Case "Residencial"
                    RetornaCodTipoTel = "1"

                Case "Comercial"
                    RetornaCodTipoTel = "2"
                Case "Celular"
                    RetornaCodTipoTel = "3"
                Case "Fax"
                    RetornaCodTipoTel = "4"
                Case "Recado"
                    RetornaCodTipoTel = "5"
            End Select
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try
    End Function
#End Region

End Class