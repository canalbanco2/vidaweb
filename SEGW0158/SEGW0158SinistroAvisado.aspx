﻿<%@ Page Language="vb" EnableViewStateMac="false"  AutoEventWireup="false" Codebehind="SEGW0158SinistroAvisado.aspx.vb"
    Inherits="SEGW0158.SEGW0158SinistroAvisado" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SUSAB - Aviso de Sinistro RE - Sinistro Avisado</title>
    <link href="CSS/Sinistro.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src='<%= ResolveUrl("~/js/SEGW0157Scripts.js") %>'></script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            &nbsp;</div>
        <asp:Panel ID="pnlPrincipal" runat="server" Style="width: 790px; height: auto;">
            <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
            </asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                  <fieldset style="width: 790px; height: auto; padding-left: 10px;">
                        <legend class="TitCaixa">Sinistro Avisado</legend>
                        <asp:Label ID="lblTitulo" CssClass="txtLabelLeft" runat="server" Text="Aviso de Sinistro Efetuado com Sucesso!"
                            Font-Bold="True" Font-Size="10pt" Width="742px"></asp:Label><br />
                        <br />
                    <asp:GridView ID="grdSinistros" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        OnPageIndexChanging="grdSinistros_PageIndexChanging" Font-Bold="True" Font-Size="Small"
                        Height="28px" Width="393px">
                        <PagerStyle Font-Size="Large" CssClass="pager" />
                        <SelectedRowStyle CssClass="txtTabela3" />
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="header_tabela" />
                        <RowStyle CssClass="txtTabela" HorizontalAlign="Center" VerticalAlign="Middle" />
                        <AlternatingRowStyle CssClass="txtTabela2" />
                        <Columns>
                            <asp:BoundField DataField="Sinistro" FooterText="SinistroAB" HeaderText="SinistroAB">
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Proposta" FooterText="PropostaAB" HeaderText="PropostaAB">
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Produto" FooterText="Produto" HeaderText="Produto">
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                     <br />
                     <asp:Label ID="LabelAvisos" runat="server" Text="" Font-Bold="True" class="txtLabel"></asp:Label><br />
                    </fieldset>
                    <br />
                    <br />
                    <br />
                    <div class="BotaoFora n300 divLeft alignLeft">
                        <div class="BotaoE">
                        </div>
                        <asp:Button ID="btnDocumentos" runat="server" CssClass="BotaoM n120" Text="Documentos"
                            OnClick="btnDocumentos_Click" />
                        <div class="BotaoD">
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div class="esmaecer">
                        <div class="icon">
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </asp:Panel>
    </form>
</body>
</html>
