﻿<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SEGW0158Evento.aspx.vb"
    Inherits="SEGW0158.SEGW0158Evento" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SUSAB - Aviso de Sinistro RE - Dados da Ocorrência</title>
    <link href="CSS/Sinistro.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src='<%= ResolveUrl("~/js/SEGW0158Scripts.js") %>'></script>

</head>
<body>
    <form id="form1" runat="server" defaultfocus="txtNome">
        <div id="Div2" style="width: 900px; text-align: left; height: 60px">
            <div class="n900">
                <div class="n900">
                    <fieldset class="n100" style="text-align: center; background-color:  #fffff0;">
                        <asp:LinkButton ID="LkbPesquisa" Enabled="false" CssClass="BtnNavega" runat="server"
                           >Pausar Processo</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n100" style="text-align: center; color: Blue;">
                        <asp:LinkButton ID="LkbDadosCliente" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Dados do Cliente</asp:LinkButton></fieldset>
                    <fieldset style="width: 70px; text-align: center; background-color:  #F0E68C;">
                        <asp:LinkButton ID="LkbEvento" Enabled="false" CssClass="Btn_on_Navega" Font-Bold="True" ForeColor="blue"
                            runat="server">Evento</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n100" style="text-align: center;">
                       <asp:LinkButton ID="LkbSolicitante" Enabled="false" CssClass="BtnNavega"
                            runat="server">Dados Solicitante</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n70" style=" text-align: center;">
                        <asp:LinkButton ID="LkbAgencia" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Agência</asp:LinkButton></fieldset>
                    <fieldset class="n100" style=" text-align: center;">
                       <asp:LinkButton ID="LkbSinistroCA" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Analise de Sinistro</asp:LinkButton></fieldset>
                    <fieldset class="n100" style=" text-align: center;">
                       <asp:LinkButton ID="LkbObjsegurado" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Objeto Segurado</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n100" style=" text-align: center;">
                        <asp:LinkButton ID="LkbOutrosSeguros" Enabled="false" CssClass="BtnNavega"
                            runat="server">Outros Seguros</asp:LinkButton></fieldset>
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="width: 100px; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu " style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_on_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 80px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 110px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 110px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            AsyncPostBackTimeout="600">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="upd_Filtro" runat="server">
            <Triggers>
                <asp:PostBackTrigger ControlID="ibtPesquisarCEP" />
                <asp:PostBackTrigger ControlID="cboEvento" />
                <asp:PostBackTrigger ControlID="cboVistoriaTipoTelefone" />
                <asp:PostBackTrigger ControlID="cboVistoriaTipoTelefone2" />
            </Triggers>
            <ContentTemplate>
                <asp:Panel runat="server" ID="Panel11" Width="750px">
                    <fieldset class="n790">
                        <legend class="TitCaixa"><span>Ocorrência</span></legend>
                        <div id="Div1" style="width: 750px; height: 150px">
                            <div class="n750">
                                <div class="n750 divLeft">
                                    <label for="txtNome" class="txtLabel divLeft n200">
                                        Evento Causador da Ocorrência: </label><asp:DropDownList ID="cboEvento" runat="server" CssClass="txtCampo divLeft n400"
                                        AutoPostBack="True" OnSelectedIndexChanged="cboEvento_SelectedIndexChanged" ValidationGroup="filter">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="FieldValidatorcboEvento" runat="server" ControlToValidate="cboEvento"
                                        Display="None" ErrorMessage="*Indique o Evento Causador da Ocorrência." SetFocusOnError="True"
                                        ValidationGroup="vldsResumo">
                                    </asp:RequiredFieldValidator>
                                </div>
                                <div class="n750">
                                    <div class="n750 divLeft">
                                        <label for="txtNome" class="txtLabel divLeft n200">
                                            Data Ocorrência:
                                        </label>
                                        <asp:TextBox ID="txtDataIniOcorrencia" CssClass="txtCampo divLeft n60" runat="server"></asp:TextBox>&nbsp;
                                        <asp:RequiredFieldValidator ID="vldDataInicial" runat="server" ControlToValidate="txtDataIniOcorrencia"
                                            Display="None" ErrorMessage="*Indique a Data de Ocorrência do Sinistro." SetFocusOnError="True"
                                            ValidationGroup="vldsResumo">
                                        </asp:RequiredFieldValidator>
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txtDataIniOcorrencia"
                                            Mask="99/99/9999" CultureName="pt-BR" MaskType="Date" ClearMaskOnLostFocus="true">
                                        </cc1:MaskedEditExtender>
                                    </div>
                                </div>
                                <%--<div class="n750">
                                    <div class="n750 divLeft">
                                        <label for="txtNome" class="txtLabel divLeft n200">
                                            Data Fim:
                                        </label>
                                        <asp:TextBox ID="txtDataFimOcorrencia" Visible="false" runat="server" CssClass="txtCampo divLeft n70">
                                        </asp:TextBox>
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtDataFimOcorrencia"
                                            Mask="99/99/9999" CultureName="pt-BR" MaskType="Date" ClearMaskOnLostFocus="true">
                                        </cc1:MaskedEditExtender>
                                    </div>
                                </div>--%>
                                <div class="n750">
                                    <div class="n750 divLeft">
                                        <label for="txtNome" class="txtLabel divLeft n200">
                                            Hora:
                                        </label>
                                        <asp:TextBox ID="txtHoraOcorrencia" onblur="javascript: preencheHora('document.form1.txtHoraOcorrencia');"
                                            onkeypress="javascript: mascaraHora('document.form1.txtHoraOcorrencia'); javascript: ValidarNumerico2();"
                                            runat="server" CssClass="txtCampo divLeft n40" Onfocus="javascript: mascaraHora('document.form1.txtHoraOcorrencia');">
                                        </asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtHoraOcorrencia"
                                            Display="None" ErrorMessage="*Indique a Hora da Ocorrência do Sinistro." SetFocusOnError="True"
                                            ValidationGroup="vldsResumo">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="n750">
                                    <div class="n750 divLeft">
                                        <label for="txtNome" class="txtLabel divLeft n200">
                                            Sub Evento:
                                        </label>
                                        <asp:DropDownList ID="cboSubEvento" CssClass="txtCampo divLeft n400" runat="server"
                                            Enabled="False">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                    </fieldset>
                </asp:Panel>
                <asp:Panel runat="server" ID="Panel22" Width="750px" Height="170px">
                    <fieldset id="flsDescEvento" class="n790">
                        <legend class="TitCaixa">Descrição do Evento</legend>
                        <div id="Panel" style="text-align: center; height: 170px;">
                            <div class="n750">
                                <div class="n750 divLeft">
                                    <asp:TextBox ID="txtDescrEvento" runat="server" CssClass="txtCampo divLeft n750"
                                        MaxLength="700" Height="150px" TextMode="MultiLine" Rows="20"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </asp:Panel>
                <br />
                <br />
                <asp:Panel runat="server" ID="Panel3" Width="750px">
                    <fieldset id="flsSolicitante" class="n790">
                        <legend class="TitCaixa">Dados da Vistoria</legend>
                        <div id="Panel2" style="text-align: left; height: 180px;">
                            <div class="n750">
                                <div class="n750 divLeft">
                                    <label for="txtNome" class="txtLabel divLeft n200">
                                        Nome de contato:
                                    </label>
                                    <asp:TextBox ID="txtVistoriaNome" runat="server" CssClass="txtCampo divLeft n400"
                                        MaxLength="60">
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtVistoriaNome"
                                        Display="None" ErrorMessage="*Indique o Nome de contato." SetFocusOnError="True"
                                        ValidationGroup="vldsResumo">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="n750">
                                <div class="n750 divLeft">
                                    <label for="txtNome" class="txtLabel divLeft n200">
                                        Endereço:
                                    </label>
                                    <asp:TextBox ID="txtVistoriaEndereco" CssClass="txtCampo divLeft n400" runat="server"
                                        MaxLength="60">
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldtxtVistoriaEndereco" runat="server"
                                        ControlToValidate="txtVistoriaEndereco" Display="None" ErrorMessage="*Indique o Endereço de contato."
                                        SetFocusOnError="True" ValidationGroup="vldsResumo">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="n750">
                                <div class="n750 divLeft">
                                    <label for="txtNome" class="txtLabel divLeft n200">
                                        Bairro:
                                    </label>
                                    <asp:TextBox ID="txtVistoriaBairro" runat="server" CssClass="txtCampo divLeft n400"
                                        MaxLength="30">
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldtxtVistoriaBairro" runat="server" ControlToValidate="txtVistoriaBairro"
                                        Display="None" ErrorMessage="*Indique o Bairro de contato." SetFocusOnError="True"
                                        ValidationGroup="vldsResumo">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="n750">
                                <div class="n250 divLeft">
                                    <label for="txtNome" class="txtLabel divLeft n200">
                                        UF:
                                    </label>
                                    <asp:DropDownList ID="cboVistoriaUF" CssClass="txtCampo divLeft n45" runat="server"
                                        AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                                <div class="n450 divLeft">
                                    <label for="txtNome" class="txtLabel divLeft n110">
                                        Município:
                                    </label>
                                    <asp:DropDownList ID="cboVistoriaMunicipio" CssClass="txtCampo divLeft n250" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="n750">
                                <div class="n750 divLeft">
                                    <label for="txtNome" class="txtLabel divLeft n200">
                                        CEP:
                                    </label>
                                    <asp:TextBox ID="txtVistoriaCEP" runat="server" CssClass="txtCampo divLeft n60">
                                    </asp:TextBox>
                                    <asp:ImageButton ID="ibtPesquisarCEP" runat="server" CausesValidation="false" ImageUrl="~/Image/Search.gif"
                                        TabIndex="3" ToolTip="Pesquisar" />
                                    <cc1:MaskedEditExtender ID="meeCep" runat="server" Mask="99999-999" TargetControlID="txtVistoriaCEP"
                                        CultureName="pt-BR" ClearMaskOnLostFocus="true">
                                    </cc1:MaskedEditExtender>
                                </div>
                            </div>
                            <div class="n750">
                                <div class="n250 divLeft">
                                    <label class="txtLabel divLeft n200" for="txtNome">
                                        DDD:
                                    </label>
                                    <asp:TextBox ID="txtVistoriaDDD" CssClass="txtCampo divLeft n20" runat="server">
                                    </asp:TextBox>
                                    <cc1:MaskedEditExtender ID="maskDDD" runat="server" Mask="(99)" TargetControlID="txtVistoriaDDD"
                                        CultureName="pt-BR" ClearMaskOnLostFocus="true">
                                    </cc1:MaskedEditExtender>
                                </div>
                                <div class="n150 divLeft">
                                    <label for="txtNome" class="txtLabel divLeft n50">
                                        Telefone:
                                    </label>
                                    <asp:TextBox ID="txtVistoriaTelefone" CssClass="txtCampo divLeft n70" runat="server">
                                    </asp:TextBox>
                                    <cc1:MaskedEditExtender ID="meeTelefone" runat="server" Mask="9999-9999" TargetControlID="txtVistoriaTelefone"
                                        CultureName="pt-BR" ClearMaskOnLostFocus="true">
                                    </cc1:MaskedEditExtender>
                                </div>
                                <div class="n250     divLeft">
                                    <label for="txtNome" class="txtLabel divLeft n100">
                                        Tipo Telefone:
                                    </label>
                                    <asp:DropDownList ID="cboVistoriaTipoTelefone" AutoPostBack="true" CssClass="txtCampo divLeft n100"
                                        runat="server" OnSelectedIndexChanged="cboVistoriaTipoTelefone_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="n750">
                                <div class="n250 divLeft">
                                    <label for="txtNome" class="txtLabel divLeft n200">
                                        DDD:
                                    </label>
                                    <asp:TextBox ID="txtVistoriaDDD2" CssClass="txtCampo divLeft n20" runat="server">
                                    </asp:TextBox>
                                    <cc1:MaskedEditExtender ID="maskDDD2" runat="server" Mask="(99)" TargetControlID="txtVistoriaDDD2"
                                        CultureName="pt-BR" ClearMaskOnLostFocus="true">
                                    </cc1:MaskedEditExtender>
                                </div>
                                <div class="n150 divLeft">
                                    <label for="txtNome" class="txtLabel divLeft n50">
                                        Telefone:
                                    </label>
                                    <asp:TextBox ID="txtVistoriaTelefone2" CssClass="txtCampo divLeft n70" runat="server">
                                    </asp:TextBox>&nbsp;
                                    <cc1:MaskedEditExtender ID="meeTelefone2" runat="server" Mask="9999-9999" TargetControlID="txtVistoriaTelefone2"
                                        CultureName="pt-BR" ClearMaskOnLostFocus="true">
                                    </cc1:MaskedEditExtender>
                                </div>
                                <div class="n250 divLeft">
                                    <label for="txtNome" class="txtLabel divLeft n100">
                                        Tipo Telefone:
                                    </label>
                                    <asp:DropDownList ID="cboVistoriaTipoTelefone2" AutoPostBack="true" CssClass="txtCampo divLeft n100"
                                        runat="server" OnSelectedIndexChanged="cboVistoriaTipoTelefone2_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <br />
                </asp:Panel>
                <fieldset class="n900" style="text-align: center; border: none;">
                    <div class="n900">
                        <div class="n900 divLeft">
                            <div class="BotaoFora n325 divLeft alignLeft">
                                <div class="BotaoE">
                                </div>
                                <asp:Button ID="btnVoltar" runat="server" Text="<< Voltar" CssClass="BotaoM n120"
                                    TabIndex="1001" />
                                <div class="BotaoD">
                                </div>
                            </div>
                            <div class="BotaoFora n325 divLeft alignLeft">
                                <div class="BotaoE">
                                </div>
                                <asp:Button ID="btnSalvar" runat="server" Text="Salvar" CssClass="BotaoM n120" ValidationGroup="filter"
                                    TabIndex="1002" />
                                <div class="BotaoD">
                                </div>
                            </div>
                            <div class="BotaoFora n300 divLeft alignLeft">
                                <div class="BotaoE">
                                </div>
                                <asp:Button ID="btnContinuar" runat="server" Text="Continuar >>" CssClass="BotaoM n120"
                                    ValidationGroup="filter" TabIndex="1003" />
                                <asp:CustomValidator ID="cvtFields" runat="server" ClientValidationFunction="EventClientValidate"
                                    Display="None" ErrorMessage="Informe pelo menos um filtro para pesquisa." ValidationGroup="filter">
                                </asp:CustomValidator>
                                <div class="BotaoD">
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <input type="hidden" runat="server" id="txtDataSistema" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="1">
            <ProgressTemplate>
                <div class="esmaecer">
                    <div class="icon">
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </form>
</body>
</html>
