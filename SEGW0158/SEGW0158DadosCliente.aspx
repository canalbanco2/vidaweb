﻿<%@ Page Language="vb" EnableViewStateMac="false" AutoEventWireup="false" Codebehind="SEGW0158DadosCliente.aspx.vb"
    Inherits="SEGW0158.SEGW0158DadosCliente" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SUSAB - Aviso de Sinistro RE - Dados do Cliente</title>
    <link href="CSS/Sinistro.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src='<%= ResolveUrl("~/js/SEGW0158Scripts.js") %>'></script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="Div2" style="width: 900px; text-align: left; height: 60px">
            <div class="n900">
                  <div class="n900">
                    <fieldset class="n100" style="text-align: center; background-color:  #fffff0;">
                        <asp:LinkButton ID="LkbPesquisa" Enabled="false" CssClass="BtnNavega" runat="server"
                           >Pausar Processo</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n110" style="text-align: center;  background-color:  #F0E68C;">
                        <asp:LinkButton ID="LkbDadosCliente" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Dados do Cliente</asp:LinkButton></fieldset>
                    <fieldset style="width: 70px; text-align: center; ">
                        <asp:LinkButton ID="LkbEvento" Enabled="false" CssClass="Btn_on_Navega" Font-Bold="True" ForeColor="blue"
                            runat="server">Evento</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n100" style="text-align: center;">
                       <asp:LinkButton ID="LkbSolicitante" Enabled="false" CssClass="BtnNavega"
                            runat="server">Dados Solicitante</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n70" style=" text-align: center;">
                        <asp:LinkButton ID="LkbAgencia" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Agência</asp:LinkButton></fieldset>
                    <fieldset class="n100" style=" text-align: center;">
                       <asp:LinkButton ID="LkbSinistroCA" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Analise de Sinistro</asp:LinkButton></fieldset>
                    <fieldset class="n100" style=" text-align: center;">
                       <asp:LinkButton ID="LkbObjsegurado" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Objeto Segurado</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n100" style=" text-align: center;">
                        <asp:LinkButton ID="LkbOutrosSeguros" Enabled="false" CssClass="BtnNavega"
                            runat="server">Outros Seguros</asp:LinkButton></fieldset>
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="width: 110px; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu " style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_on_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 90px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 80px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 110px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 120px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
            </asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <Triggers>
                    <asp:PostBackTrigger ControlID="ibtPesquisarCEP" />
                </Triggers>
                <ContentTemplate>
                    <fieldset id="flsSegurado" style="text-align: left; width: 770px; height: 100px;">
                        <legend class="TitCaixa"><b>Dados do Segurado</b></legend>
                        <div id="Panel2" style="text-align: left; height: auto;" />
                        <div class="n750">
                            <div class="n750 divLeft">
                                <asp:Label ID="lblNomeSegurado" runat="server" Text="Nome: " CssClass="txtLabel divLeft n200"></asp:Label>
                                <asp:TextBox ID="txtNomeSegurado" CssClass="txtCampo divLeft n400" MaxLength="60"
                                    runat="server"></asp:TextBox>
                            </div>
                            <div class="n750 divLeft">
                                <asp:Label ID="lblCPF_CNPJ" CssClass="txtLabel divLeft n200" runat="server" Text="C.P.F./C.N.P.J.: "></asp:Label>
                                <asp:TextBox ID="txtCPF_CNPJ" CssClass="txtCampo divLeft n100" MaxLength="14" runat="server"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="meCPF_CNPJ" runat="server" Mask="999,999,999-99" TargetControlID="txtCPF_CNPJ"
                                    CultureName="pt-BR" ClearMaskOnLostFocus="true">
                                </cc1:MaskedEditExtender>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset id="Fieldset1" style="text-align: left; width: 770px; height: 100px;">
                        <legend class="TitCaixa"><b>Dados da Proposta</b></legend>
                        <div class="n750">
                            <div class="n750 divLeft">
                                <asp:Label ID="lblPropostaBB" CssClass="txtLabel divLeft n200" runat="server" Text="Proposta BB: "></asp:Label>
                                <asp:TextBox ID="txtPropostaBB" CssClass="txtCampo divLeft n100" MaxLength="9" runat="server"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" ClearMaskOnLostFocus="true"
                                    CultureName="pt-BR" Mask="99999999" TargetControlID="txtPropostaBB">
                                </cc1:MaskedEditExtender>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div>
                        </div>
                    </fieldset>
                    <fieldset id="Fieldset2" style="text-align: left; width: 770px; height: 200px;">
                        <legend class="TitCaixa"><b>Local de Risco</b></legend>
                        <div id="Panel" style="text-align: left; height: auto;" />
                        <div class="n750">
                            <div class="n750 divLeft">
                                <asp:Label ID="Label1" CssClass="txtLabel divLeft n200" runat="server" Text="Endereço: "></asp:Label>
                                <asp:TextBox ID="txtEndereco" CssClass="txtCampo divLeft n400" MaxLength="60" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="n750">
                            <div class="n750 divLeft">
                                <asp:Label ID="lblBairro" CssClass="txtLabel divLeft n200" runat="server" Text="Bairro: "></asp:Label>
                                <asp:TextBox ID="txtBairro" CssClass="txtCampo divLeft n400" MaxLength="30" runat="server"></asp:TextBox>
                            </div>
                            <div class="n750 divLeft">
                                <asp:Label ID="lblMunicipio" runat="server" CssClass="txtLabel divLeft n200" Text="Município: "></asp:Label>
                                <asp:DropDownList runat="server" ID="cboMunicipio" CssClass="txtCampo divLeft n400">
                                </asp:DropDownList>
                            </div>
                            <div class="n750 divLeft">
                                <asp:Label ID="lblUF" CssClass="txtLabel divLeft n200" runat="server" Text="UF: "></asp:Label>
                                <asp:DropDownList runat="server" CssClass="txtCampo divLeft n40" ID="cboUF" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <div class="n750 divLeft">
                                <asp:Label ID="lblCEP" CssClass="txtLabel divLeft n200" runat="server" Text="CEP: "></asp:Label>
                                <asp:TextBox runat="server" ID="txtCEP" CssClass="txtCampo divLeft n70" MaxLength="10"></asp:TextBox>
                                <asp:ImageButton ID="ibtPesquisarCEP" runat="server" CausesValidation="false" ImageUrl="~/Image/Search.gif"
                                    OnClick="ibtPesquisarCEP_Click" TabIndex="3" ToolTip="Pesquisar" />
                                <cc1:MaskedEditExtender ID="meeCep" runat="server" ClearMaskOnLostFocus="true" CultureName="pt-BR"
                                    Mask="99999-999" TargetControlID="txtCEP">
                                </cc1:MaskedEditExtender>
                            </div>
                        </div>
                        <br />
                        <div>
                        </div>
                    </fieldset>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="n900" style="text-align: left; height: 220px;">
                        <div class="n900 divLeft">
                            <div class="BotaoFora n300 divLeft alignLeft">
                                <div class="BotaoE">
                                </div>
                                <asp:Button ID="btnVoltar" runat="server" Text="<< Voltar" CssClass="BotaoM n120"
                                    TabIndex="1001" OnClick="btnVoltar_Click" />
                                <div class="BotaoD">
                                </div>
                            </div>
                            <div class="BotaoFora n300 divLeft alignLeft">
                                <div class="BotaoE">
                                </div>
                                <asp:Button ID="btnSalvar" runat="server" Text="Salvar" CssClass="BotaoM n120" TabIndex="1002"
                                    OnClick="btnSalvar_Click" />
                                <div class="BotaoD">
                                </div>
                            </div>
                            <div class="BotaoFora n300 divLeft alignLeft">
                                <div class="BotaoE">
                                </div>
                                <asp:Button ID="btnContinuar" runat="server" Text="Continuar >>" CssClass="BotaoM n120"
                                    TabIndex="1003" OnClick="btnContinuar_Click" />
                                <div class="BotaoD">
                                </div>
                            </div>
                            <asp:CustomValidator ID="cvtFields" runat="server" ClientValidationFunction="EventClientValidate"
                                Display="None" ErrorMessage="Informe pelo menos um filtro para pesquisa." ValidationGroup="filter">
                            </asp:CustomValidator>
                        </div>
                        <input type="hidden" runat="server" id="txtDataSistema" />
                        <table>
                            <tr>
                                <td>
                                    &nbsp;&nbsp;<br />
                                    <br />
                                    <br />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="1">
                <ProgressTemplate>
                    <div class="esmaecer">
                        <div class="icon">
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </form>
</body>
</html>
