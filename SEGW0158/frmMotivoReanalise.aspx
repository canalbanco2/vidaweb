﻿<%@ Page Language="vb" EnableViewStateMac="false"  AutoEventWireup="false" CodeBehind="frmMotivoReanalise.aspx.vb" Inherits="SEGW0158.frmMotivoReanalise" %>

<%@ Register Src="~/UserControls/UCObservacoes.ascx" TagName="Observacoes" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>SUSAB - Aviso de Sinistro RE - Motivo Reanálise</title>
    <link href="CSS/BaseStyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src='<%= ResolveUrl("~/js/SEGW0158Scripts.js") %>'></script> 
    
</head>
<body>
    <form id="form1" runat="server">
        <fieldset style="width:790px; height:170px">
            <legend >Motivo de Reanalise</legend>        
            <div id="Panel" style="width:750px; height:130px">
                <div class="n750">
                    <div class="n750 divLeft">
                        <label for="ddlMotivoReanalise" class="label">
                            Motivo de Reanálise:
                        </label>
                        <asp:DropDownList ID="ddlMotivoReanalise" runat="server" AutoPostBack="true" CssClass="dropDownPadrao n605" TabIndex="8">
                        </asp:DropDownList>                                                                           
                    </div>
                </div>            
                <uc1:Observacoes ID="Observacoes1" runat="server"></uc1:Observacoes>
            </div>               
        </fieldset>  
        <div class="n790">
            <br />
            <div class="n264 divLeft alignLeft">
                <asp:Button ID="btnVoltar" runat="server" CssClass="Botao" Text="<< Voltar" Visible="true" /> 
            </div>
            <div class="n264 divLeft alignCenter">
                <asp:Button ID="btnSalvar" runat="server" CssClass="Botao" Text="Salvar" Visible="true" ValidationGroup="filter"/>
            </div>
            <div class="n262 divLeft alignRight">
                    <asp:Button ID="btnContinuar" runat="server" CssClass="Botao" Text="Continuar >>" Visible="true" ValidationGroup="filter"/>
                    <asp:CustomValidator ID="cvtFields" runat="server" ClientValidationFunction="Validar_MotivoReanalise"
                                    Display="None" ErrorMessage="Informe pelo menos um filtro para pesquisa." ValidationGroup="filter">
                    </asp:CustomValidator>
            </div>
        </div>
    </form>
</body>
</html>
