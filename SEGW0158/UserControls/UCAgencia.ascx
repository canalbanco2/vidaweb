﻿<%@ Control Language="vb"   AutoEventWireup="false" Codebehind="UCAgencia.ascx.vb"
    Inherits="SEGW0158.UCAgencia" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<div>
    <fieldset>
        <legend>
            <asp:Literal ID="litLegend" runat="server"></asp:Literal>
        </legend>
        <div id="Panel">
            <div class="n750">
                <div class="n200 divLeft">
                    <label for="txtCodigoAgencia" class="txtLabel divLeft n100">
                        Código da agência:
                    </label>
                    <asp:TextBox ID="txtCodigoAgencia" CssClass="txtCampo divLeft n70" runat="server"
                        MaxLength="10"></asp:TextBox>
                    <asp:Button ID="btnSearchbyCodAgencia" runat="server" CssClass="Botao" Text="..." />
                    &nbsp;
                    <br />
                    <div class="n200 divLeft">
                        <label for="cboEstado" class="txtLabel divLeft n100">
                            UF:
                        </label>
                        <asp:DropDownList ID="cboEstado" runat="server" Width="69px" ValidationGroup="Filtro">
                        </asp:DropDownList>
                    </div>
                    <div class="n350 divLeft">
                        <label for="cboMunicipio" class="txtLabel divLeft n100">
                            Município:
                        </label>
                        <asp:DropDownList ID="cboMunicipio" runat="server" Width="350px" ValidationGroup="Filtro">
                        </asp:DropDownList><asp:Button ID="btnPesquisar" runat="server" CssClass="Botao"
                            Text="..." ValidationGroup="Filtro" />
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <asp:UpdatePanel runat="server" ID="updFiltro">
        <ContentTemplate>
            <br />
            <asp:GridView ID="GVwAgencia" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                OnSelectedIndexChanged="GVwAgencia_SelectedIndexChanged">
                <SelectedRowStyle CssClass="txtTabela3" />
                <RowStyle CssClass="txtTabela" />
                <PagerStyle Font-Size="Large" CssClass="pager" />
                <Columns>
                    <asp:CommandField ButtonType="Image" SelectImageUrl="~/Image/icon_bseta_menu_unsel.gif"
                        SelectText="Selecionar" ShowSelectButton="True" />
                    <asp:BoundField DataField="AGENCIA" HeaderText="Ag&#234;ncia">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="dv" HeaderText="DV" />
                    <asp:BoundField DataField="nome" HeaderText="Nome">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="endereco" HeaderText="Endere&#231;o" />
                    <asp:BoundField DataField="bairro" HeaderText="Bairro">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataTemplate>
                    Nenhuma agência foi encontrada!
                </EmptyDataTemplate>
            </asp:GridView>
            &nbsp;<br />
            <fieldset>
                <legend>
                    <asp:Literal ID="litAgencia" runat="server"></asp:Literal>
                </legend>
                <div id="Div5">
                    <div id="Div6" class="box">
                        <br />
                        Código da agência:
                        <asp:TextBox ID="txtCodigoAgenciaGrid" runat="server" MaxLength="4" Width="74px"></asp:TextBox>
                        DV<asp:TextBox ID="txtDVGrid" runat="server" MaxLength="1" Width="30px"></asp:TextBox>&nbsp;<br />
                        Nome da agência:
                        <asp:TextBox ID="txtNomeAgenciaGrid" runat="server" MaxLength="60" Width="445px"></asp:TextBox><br />
                        Endereço:
                        <asp:TextBox ID="txtEnderecoGrid" runat="server" MaxLength="60" Width="241px"></asp:TextBox>
                        Bairro:
                        <asp:TextBox ID="txtBairroGrid" runat="server" MaxLength="60" Width="241px"></asp:TextBox><br />
                        Município:
                        <asp:TextBox ID="txtMunicipioGrid" runat="server" MaxLength="60" Width="486px"></asp:TextBox>
                        UF:
                        <asp:TextBox ID="txtUFGrid" runat="server" MaxLength="2" Width="36px"></asp:TextBox><br />
                    </div>
                </div>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
    &nbsp;
    <asp:CustomValidator ID="cvtFields" runat="server" ErrorMessage="Informe pelo menos um filtro para pesquisa."
        Display="None" ClientValidationFunction="ClientValidate" ValidationGroup="Filtro" /><br />
    <asp:ValidationSummary ID="VlSFields" runat="server" DisplayMode="SingleParagraph"
        ShowMessageBox="True" ShowSummary="False" ValidationGroup="Filtro" />
    <br />
    <asp:ValidationSummary ID="vlfFiltroAgencia" runat="server" ShowMessageBox="True"
        ShowSummary="False" />
</div>
