﻿Imports SEGL0343.Task
Imports SEGL0343.EO
Partial Public Class UCSinistrado
    Inherits System.Web.UI.UserControl

#Region "Propriedades"

    'Public Property Nome() As String
    '    Get
    '        Return Me.txtNome.Text
    '    End Get
    '    Set(ByVal value As String)

    '        Me.txtNome.Text = value
    '    End Set
    'End Property

    'Public Property NomeReadOnly() As Boolean
    '    Get
    '        Return Me.txtNome.ReadOnly
    '    End Get
    '    Set(ByVal value As Boolean)

    '        Me.txtNome.ReadOnly = value
    '    End Set
    'End Property

    'Public Property CPF() As String
    '    Get
    '        Return Me.txtCPFCNPJ.Text
    '    End Get
    '    Set(ByVal value As String)

    '        Me.txtCPFCNPJ.Text = value
    '    End Set
    'End Property

    'Public Property CPFReadOnly() As Boolean
    '    Get
    '        Return Me.txtCPFCNPJ.ReadOnly
    '    End Get
    '    Set(ByVal value As Boolean)

    '        Me.txtCPFCNPJ.ReadOnly = value
    '    End Set
    'End Property

    'Public Property Data() As String
    '    Get
    '        Return Me.txtDataNascimento.Text
    '    End Get
    '    Set(ByVal value As String)

    '        Me.txtDataNascimento.Text = value
    '    End Set
    'End Property

    'Public Property DataReadOnly() As Boolean
    '    Get
    '        Return Me.txtDataNascimento.ReadOnly
    '    End Get
    '    Set(ByVal value As Boolean)

    '        Me.txtDataNascimento.ReadOnly = value
    '    End Set
    'End Property

    'Public Property Sexo() As String
    '    Get
    '        Return Me.ddlSexo.SelectedValue
    '    End Get
    '    Set(ByVal value As String)

    '        Me.ddlSexo.SelectedValue = value

    '    End Set
    'End Property
    'Public Property SexoReadOnly() As Boolean
    '    Get
    '        Return Not Me.ddlSexo.Enabled
    '    End Get
    '    Set(ByVal value As Boolean)

    '        Me.ddlSexo.Enabled = Not value

    '    End Set
    'End Property
    Public Property CodigoAgencia() As String
        Get
            Return Me.txtCodigoAgenciaVw.Text
        End Get
        Set(ByVal value As String)
            Me.txtCodigoAgenciaVw.Text = value
        End Set
    End Property
    Public Property CodigoAgenciaReadOnly() As Boolean
        Get
            Return Me.txtCodigoAgenciaVw.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtCodigoAgenciaVw.ReadOnly = value
        End Set
    End Property

    Public Property DVAgencia() As String
        Get
            Return Me.txtDV.Text
        End Get
        Set(ByVal value As String)
            Me.txtDV.Text = value
        End Set
    End Property
    Public Property DVAgenciaReadOnly() As Boolean
        Get
            Return Me.txtDV.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtDV.ReadOnly = value
        End Set
    End Property

    Public Property NomeAgencia() As String
        Get
            Return Me.txtNomeAgencia.Text
        End Get
        Set(ByVal value As String)

            Me.txtNomeAgencia.Text = value
        End Set
    End Property
    Public Property NomeAgenciaReadOnly() As Boolean
        Get
            Return Me.txtNomeAgencia.ReadOnly
        End Get
        Set(ByVal value As Boolean)

            Me.txtNomeAgencia.ReadOnly = value
        End Set
    End Property

    Public Property EnderecoAgencia() As String
        Get
            Return Me.txtEndereco.Text
        End Get
        Set(ByVal value As String)
            Me.txtEndereco.Text = value
        End Set
    End Property
    Public Property EnderecoAgenciaReadOnly() As Boolean
        Get
            Return Me.txtEndereco.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtEndereco.ReadOnly = value
        End Set
    End Property

    Public Property BairroAgencia() As String
        Get
            Return Me.txtBairro.Text
        End Get
        Set(ByVal value As String)
            Me.txtBairro.Text = value
        End Set
    End Property
    Public Property BairroAgenciaReadOnly() As Boolean
        Get
            Return Me.txtBairro.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtBairro.ReadOnly = value
        End Set
    End Property

    Public Property Municipio() As String
        Get
            Return Me.txtCidade.Text
        End Get
        Set(ByVal value As String)
            Me.txtCidade.Text = value
        End Set
    End Property
    Public Property MunicipioReadOnly() As Boolean
        Get
            Return Me.txtCidade.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtCidade.ReadOnly = value
        End Set
    End Property

    Public Property UF() As String
        Get
            Return Me.txtUF.Text
        End Get
        Set(ByVal value As String)
            Me.txtUF.Text = value
        End Set
    End Property
    Public Property UFReadOnly() As Boolean
        Get
            Return Me.txtUF.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtUF.ReadOnly = value
        End Set
    End Property

#End Region

#Region "Evento"

    Public Function RetornaMunicipio(ByVal strCodMunicipio As String, ByVal strUF As String, Optional ByVal strMunicipio As String = "") As Boolean

        Dim Obj_TK As New UCAgencia_TK()
        Dim Obj_EO_IN As New SEL_MUNICIPIO_SPS_EO_IN()

        Dim Obj_EO_OUT As List(Of SEL_MUNICIPIO_SPS_EO_OUT)

        Obj_EO_IN.Municipio_Id = strCodMunicipio
        Obj_EO_IN.Estado = strUF
        Obj_EO_IN.Municipio = strMunicipio


        Obj_EO_OUT = Obj_TK.ObtemListaMunicipio(Obj_EO_IN)

        If Obj_EO_OUT.Count > 0 Then

            Me.txtCidade.Text = Obj_EO_OUT(0).nome

        End If

    End Function

#End Region

#Region "Métodos"

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Me.txtDataNascimento.Attributes.Add("onblur", "javascript:ValidaData(this);")
    End Sub

End Class