﻿<%@ Control Language="vb"  AutoEventWireup="false" Codebehind="UCOcorrenciaResumo.ascx.vb"
    Inherits="SEGW0158.UCOcorrenciaResumo" %>
<%@ Register Src="~/UserControls/UCObservacoes.ascx" TagName="Observacoes" TagPrefix="uc3" %>
<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
</asp:ScriptManager>--%>
<fieldset style="width: auto; height: 230px;">
    <legend class="TitCaixa"><span>Ocorrência</span></legend>
    <div id="Panel_Pessoa1" style="width: 95%; height: 150px; font-size: 8pt;">
        <div class="n750 divLeft">
            <div class="n280 divLeft">
                <label class="txtLabel divLeft n150" for="txtDataOcorrencia">
                    Data Ocorrência:
                </label>
                <asp:TextBox ID="txtDataOcorrencia" runat="server" TabIndex="1" CssClass="txtCampo divLeft n80" onkeypress="return formatar(this, '??/??/????', event);"></asp:TextBox>
            </div>
            <div class="n120 divLeft">
                <label class="txtLabel divLeft n50" for="txtHoraOcorrencia">
                    Hora:
                </label>
                <asp:TextBox ID="txtHoraOcorrencia" runat="server" TabIndex="2" CssClass="txtCampo divLeft n50"
                    onkeypress="return formatar(this, '??:??', event);">
                </asp:TextBox>
            </div>
            <div class="n350 divLeft">
                <asp:Label ID="lblSinistro" CssClass="txtLabel divLeft n100" runat="server" Text="Reanálise:"></asp:Label>
                <asp:TextBox ID="txtReanalise" runat="server" TabIndex="3" CssClass="txtCampo divLeft n150"></asp:TextBox></div>
        </div>
        <div class="n750">
            <div class="n750 divLeft">
                <label for="ddlEvento" class="txtLabel divLeft n150">
                    Evento:
                </label>
                <asp:DropDownList ID="ddlEvento" runat="server" TabIndex="4" AutoPostBack="true" CssClass="txtCampo divLeft n505">
                </asp:DropDownList>
            </div>
        </div>
        <div class="n750">
            <div class="n750 divLeft">
                <label for="ddlSubEvento" class="txtLabel divLeft n150">
                    SubEvento:
                </label>
                <asp:DropDownList ID="ddlSubEvento" runat="server" TabIndex="5" AutoPostBack="true" CssClass="txtCampo divLeft n505">
                </asp:DropDownList>
            </div>
        </div>
        <uc3:Observacoes ID="Observacoes" runat="server"></uc3:Observacoes>
    </div>
</fieldset>
