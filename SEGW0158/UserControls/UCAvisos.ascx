﻿<%@ Control Language="vb" AutoEventWireup="false" Codebehind="UCAvisos.ascx.vb" Inherits="SEGW0158.UCAvisos" %>
<%@ Register Src="~/UserControls/UCTelefoneSolicitante.ascx" TagName="TelsSolicitante"
    TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<fieldset style="font-size: 8pt; width: auto; height: 150px; text-align: center;">
    <legend class="TitCaixa"><span>Avisos</span></legend>
    <div id="Panel" style="font-size: 8pt; width: 95%; height: 140px">
        <asp:Panel ID="pnlGridPropostas2" ScrollBars="Both" runat="server" Width="100%" Height="80%">
            <asp:GridView ID="grdPropostas" runat="server" ScrollBars="Both" Width="1000px" AutoGenerateColumns="False">
                <EmptyDataTemplate>
                    Nenhum Aviso encontrado.
                </EmptyDataTemplate>
                <PagerStyle Font-Size="Large" CssClass="pager" />
                <SelectedRowStyle CssClass="txtTabela3" />
                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="header_tabela" />
                <RowStyle CssClass="txtTabela" HorizontalAlign="Center" VerticalAlign="Middle" />
                <AlternatingRowStyle CssClass="txtTabela2" />
                <Columns>
                    <asp:BoundField DataField="proposta_id" HeaderText="Propostas">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="proposta_bb" HeaderText="Proposta BB">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="cod_produto" HeaderText="Produto">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="nom_produto" HeaderText="Nome do Produto" HeaderStyle-Width="150px"
                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="150px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="apolice_id" HeaderText="Ap&#243;lice">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ramo_id" HeaderText="Ramo">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="nom_evento_sinistro" HeaderText="Evento do Sinistro" HeaderStyle-Width="200px">
                        <HeaderStyle HorizontalAlign="Center" Width="200px" />
                        <ItemStyle HorizontalAlign="Center" Width="200px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="valor_prejuizo" HeaderText="Valor do Preju&#237;zo" DataFormatString="{0:C2}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="nom_objeto_segurado" HeaderText="Descrição do Objeto Segurado">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </div>
</fieldset>
<br />
<br />
<fieldset style="font-size: 8pt; text-align: center; width: 100%; height: 180px">
    <legend class="TitCaixa"><span>Outros Seguros sobre o Objeto Sinistrado</span></legend>
    <div id="Div1" style="font-size: 8pt; width: 95%; height: auto">
        <asp:GridView ID="grdOutrosSeguros" runat="server" Width="95%" AutoGenerateColumns="False">
            <PagerStyle Font-Size="Large" CssClass="pager" />
            <SelectedRowStyle CssClass="txtTabela3" />
            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="header_tabela" />
            <RowStyle CssClass="txtTabela" HorizontalAlign="left" VerticalAlign="Middle" />
            <AlternatingRowStyle CssClass="txtTabela2" />
            <EmptyDataTemplate>
                Nenhuma Apólice Inclusa.
            </EmptyDataTemplate>
            <Columns>
                <asp:BoundField DataField="apolice" HeaderText="Ap&#243;lice" SortExpression="apolice" />
                <asp:BoundField DataField="nomeSeguradora" HeaderText="Nome da Seguradora" SortExpression="nomeSeguradora" />
            </Columns>
        </asp:GridView>
    </div>
</fieldset>
&nbsp; 