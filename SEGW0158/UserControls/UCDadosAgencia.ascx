<%@ Control Language="vb"   AutoEventWireup="false" Codebehind="UCDadosAgencia.ascx.vb"
    Inherits="SEGW0158.UCDadosAgencia" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Panel runat="server" ID="Panel2" Width="750px">
    <fieldset style="width: 750px; height: auto">
        <legend class="TitCaixa"><span>Pesquisa de Ag�ncia de Contato</span></legend>
        <div id="Panel" style="width: 750px; height: 60">
            <div class="n750">
                <div class="n250 divLeft">
                    <label for="txtCodigoAgencia" class="txtLabel divLeft n100">
                        C�d da Ag�ncia:
                    </label>
                    <asp:TextBox ID="txtCodigoAgencia"  onkeypress="ValidarNumerico2();" runat="server" CssClass="txtCampo divLeft n70"
                        MaxLength="4">
                    </asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" BorderWidth="0px"
                        ControlToValidate="txtCodigoAgencia" ErrorMessage="Valor Inv�lido para a Ag�ncia"
                        ValidationExpression="^[0-9]+$">*</asp:RegularExpressionValidator>
                    <asp:ImageButton ToolTip="Pesquisar por C�digo de Ag�ncia" ID="ibtPesquisarCodigoAgencia"
                        runat="server" CausesValidation="false" ImageUrl="~/image/Search.gif" />
                </div>
                <div class="n200 divLeft">
                    <label for="ddlUF" class="txtLabel divLeft n100">
                        UF:
                    </label>
                    <asp:DropDownList ID="ddlUF" runat="server" AutoPostBack="true" CssClass="txtCampo divLeft n50"
                        TabIndex="1">
                    </asp:DropDownList>
                </div>
                <div class="n300 divLeft">
                    <label for="ddlCidade" class="txtLabel divLeft n100">
                        Munic�pio:
                    </label>
                    <asp:DropDownList ID="ddlCidade" runat="server" CssClass="txtCampo divLeft n100"
                        TabIndex="2">
                    </asp:DropDownList>
                    <asp:ImageButton ToolTip="Pesquisar por Estado e Municipio" ID="ibtPesquisaEstadoMunicipio"
                        runat="server" CausesValidation="false" ImageUrl="~/image/Search.gif" />
                </div>
            </div>
        </div>
    </fieldset>
    <br />
    <%--<asp:GridView ID="GVwAgencia" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px"
                        CellPadding="3" GridLines="Vertical">
--%>
    <asp:GridView ID="GVwAgencia" runat="server" AutoGenerateColumns="False" AllowPaging="True">
        <RowStyle CssClass="txtTabela" />
        <Columns>
            <asp:CommandField ButtonType="Image" SelectImageUrl="~/image/icon_bseta_menu_unsel.gif"
                SelectText="Selecionar" ShowSelectButton="True" />
            <asp:BoundField DataField="agencia_id" HeaderText="Ag&#234;ncia">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="dv_prefixo" HeaderText="DV" />
            <asp:BoundField DataField="nome" HeaderText="Nome">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="endereco" HeaderText="Endere&#231;o" />
            <asp:BoundField DataField="bairro" HeaderText="Bairro">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="municipio" HeaderText="Municipio" />
            <asp:BoundField DataField="estado" HeaderText="Estado" />
        </Columns>
        <AlternatingRowStyle CssClass="txtTabela2" />
        <SelectedRowStyle CssClass="txtTabela3" />
        <PagerStyle CssClass="pager" />
        <HeaderStyle CssClass="header_tabela" />
        <RowStyle CssClass="txtTabela" />
        <EmptyDataTemplate>
            Nenhuma ag�ncia foi encontrada!
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    <fieldset style="width: 750px; height: auto">
        <legend class="TitCaixa"><span>Ag�ncia de Contato</span></legend>
        <div id="Panel_Pesquisa1" style="width: 750px; height: auto">
            <div class="n750">
                <div class="n200 divLeft">
                    <label for="txtDV" class="txtLabel divLeft n100">
                        C�d da Ag�ncia:
                    </label>
                    <asp:TextBox ID="txtCodigoAgenciaVw" runat="server" CssClass="txtCampo divLeft n50"
                        MaxLength="4" onkeypress="return formatar(this, '????', event);" ReadOnly="True"
                        TabIndex="3"></asp:TextBox>
                </div>
                <div class="n150 divLeft">
                    <label for="txtDV" class="txtLabel divLeft n80">
                        DV:
                    </label>
                    <asp:TextBox ID="txtDV" runat="server" CssClass="txtCampo divLeft n20" MaxLength="1"
                        onkeypress="return formatar(this, '?', event);" ReadOnly="True" TabIndex="4"></asp:TextBox>
                </div>
                <div class="n400 divLeft">
                    <label for="txtNomeAgencia" class="txtLabel divLeft n150">
                        Nome da Ag�ncia:
                    </label>
                    <asp:TextBox ID="txtNomeAgencia" runat="server" CssClass="txtCampo divLeft n200"
                        ReadOnly="True" TabIndex="5"></asp:TextBox>
                </div>
            </div>
            <div class="n750">
                <div class="n400 divLeft">
                    <label for="txtEndereco" class="txtLabel divLeft n100">
                        Endere�o:
                    </label>
                    <asp:TextBox ID="txtEndereco" runat="server" CssClass="txtCampo divLeft n200" ReadOnly="True"></asp:TextBox>
                </div>
                <div class="n350 divLeft">
                    <label for="txtBairro" class="txtLabel divLeft n100">
                        Bairro:
                    </label>
                    <asp:TextBox ID="txtBairro" runat="server" CssClass="txtCampo divLeft n200" ReadOnly="True"
                        TabIndex="6"></asp:TextBox>
                </div>
            </div>
            <div class="n750">
                <div class="n400 divLeft">
                    <label for="txtCidade" class="txtLabel divLeft n100">
                        Munic�pio:
                    </label>
                    <asp:TextBox ID="txtCidade" runat="server" CssClass="txtCampo divLeft n200" ReadOnly="True"
                        TabIndex="7"></asp:TextBox>
                </div>
                <div class="n300 divLeft">
                    <label for="txtUF" class="txtLabel divLeft n100">
                        UF:
                    </label>
                    <asp:TextBox ID="txtUF" runat="server" CssClass="txtCampo divLeft n30" ReadOnly="True"
                        TabIndex="8"></asp:TextBox>
                </div>
            </div>
        </div>
    </fieldset>
    <br />
</asp:Panel>
<input id="hddPesquisa" runat="server" type="hidden" />