﻿Public Partial Class UCTelefoneSolicitante
    Inherits System.Web.UI.UserControl
#Region "Propriedades"


    Public Property DDD() As String
        Get
            Return Me.txtDDD.Text
        End Get
        Set(ByVal value As String)
            Me.txtDDD.Text = value
        End Set
    End Property

    Public Property DDDReadOnly() As Boolean
        Get
            Return txtDDD.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            txtDDD.ReadOnly = value
        End Set
    End Property


    Public Property Telefone() As String
        Get
            Return Me.txtTelefone.Text
        End Get
        Set(ByVal value As String)
            Me.txtTelefone.Text = value
        End Set
    End Property

    Public Property TelefoneReadOnly() As Boolean
        Get
            Return txtTelefone.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            txtTelefone.ReadOnly = value
        End Set
    End Property

    Public Property Ramal() As String
        Get

            Return Me.txtRamal.Text

        End Get
        Set(ByVal value As String)

            Me.txtRamal.Text = value

        End Set
    End Property

    Public Property RamalReadOnly() As Boolean
        Get
            Return txtRamal.ReadOnly
        End Get
        Set(ByVal value As Boolean)

            txtRamal.ReadOnly = value

        End Set
    End Property

    Public Property TipoTelefone() As String
        Get

            Return Me.ddlTipoTelefone.SelectedValue

        End Get
        Set(ByVal value As String)

            Me.ddlTipoTelefone.SelectedValue = value

        End Set
    End Property

    Public Property TipoTelefoneItem() As String
        Get

            Return Me.ddlTipoTelefone.SelectedItem.Text

        End Get
        Set(ByVal value As String)

            Me.ddlTipoTelefone.Text = value

        End Set
    End Property

    Public Property TipoTelefoneEnabled() As Boolean
        Get
            Return ddlTipoTelefone.Enabled
        End Get
        Set(ByVal value As Boolean)

            ddlTipoTelefone.Enabled = value

        End Set
    End Property


#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

End Class