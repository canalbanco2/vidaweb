﻿Public Partial Class UCObservacoes
    Inherits System.Web.UI.UserControl
    Private tamanho As Integer
    Private _focus As Boolean
#Region "Propriedades"

    Public Property Observacao() As String
        Get
            Return Me.txtObservacao.Text
        End Get
        Set(ByVal value As String)

            If IsNothing(value) Then

                value = ""

            End If

            Me.txtObservacao.Text = value
        End Set
    End Property

    Public Property ObservacaoReadOnly() As Boolean
        Get
            Return Me.txtObservacao.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtObservacao.ReadOnly = value
        End Set
    End Property

    Public Property ObservacaoMaxLength() As Integer
        Set(ByVal value As Integer)
            tamanho = value
        End Set
        Get
            Return tamanho
        End Get
    End Property

    Public Property Focus() As Boolean

        Set(ByVal value As Boolean)

            If value = True Then

                Me.txtObservacao.Focus()

            End If
            _focus = value
        End Set

        Get

            Return _focus

        End Get

    End Property


#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.txtObservacao.Attributes.Add("onkeydown", "javascript:valMax(this," & Me.ObservacaoMaxLength & ");")
        Me.txtObservacao.Attributes.Add("onkeyup", "javascript:valMax(this," & Me.ObservacaoMaxLength & ");")
        Me.txtObservacao.Attributes.Add("onpaste", "javascript:valMax(this," & Me.ObservacaoMaxLength & ");")
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        
    End Sub
End Class