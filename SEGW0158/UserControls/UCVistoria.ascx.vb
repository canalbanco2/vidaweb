﻿Imports SEGL0343.Task
Imports SEGL0343.EO
Partial Public Class UCVistoria
    Inherits System.Web.UI.UserControl


#Region "Propriedades"
    Private taskSolicitante As New SEGW0157Solicitante_TK

    Public Property Identificado() As Boolean
        Get
            Return Me.chkIdentificado.Checked
        End Get
        Set(ByVal value As Boolean)
            Me.chkIdentificado.Checked = value
        End Set
    End Property
    Public Property IdentificadoEnabled() As Boolean
        Get
            Return Me.chkIdentificado.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me.chkIdentificado.Enabled = value
        End Set
    End Property

    Public Property LCEndereco() As String
        Get
            Return Me.txtLCEndereco.Text.Replace("'", "´")
        End Get
        Set(ByVal value As String)
            Me.txtLCEndereco.Text = value.Replace("'", "´")
        End Set
    End Property

    Public Property LCEnderecoReadOnly() As Boolean
        Get
            Return Me.txtLCEndereco.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtLCEndereco.ReadOnly = value
        End Set
    End Property

    Public Property LCBairro() As String
        Get
            Return Me.txtLCBairro.Text.Replace("'", "´")
        End Get
        Set(ByVal value As String)
            Me.txtLCBairro.Text = value.Replace("'", "´")
        End Set
    End Property

    Public Property LCBairroReadOnly() As Boolean
        Get
            Return Me.txtLCBairro.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtLCBairro.ReadOnly = value
        End Set
    End Property

    Public Property LCCEP() As String
        Get
            Return Me.txtLCCEP.Text.Replace("'", "´")
        End Get
        Set(ByVal value As String)
            Me.txtLCCEP.Text = value.Replace("'", "´")
        End Set
    End Property

    Public Property LCCEPReadOnly() As Boolean
        Get
            Return Me.txtLCCEP.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtLCCEP.ReadOnly = value
        End Set
    End Property

    Public Property LCUFText() As String
        Get
            Return cboLCUF.SelectedItem.Text
        End Get
        Set(ByVal value As String)
            PreencheComboMunicipio(value)
            cboLCUF.SelectedItem.Text = value
        End Set
    End Property

    Public Property LCUFTextEnable() As Boolean
        Get
            Return Me.cboLCUF.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me.cboLCUF.Enabled = value
        End Set
    End Property

    Public Property LCMunicipioText() As String
        Get
            Return cboLCMunicipio.SelectedItem.Text
        End Get
        Set(ByVal value As String)
            cboLCMunicipio.SelectedItem.Text = value
        End Set
    End Property

    Public Property LCMunicipioTextEnable() As Boolean
        Get
            Return Me.cboLCMunicipio.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me.cboLCMunicipio.Enabled = value
        End Set
    End Property

    Public Property VistNomeContato() As String
        Get
            Return Me.txtVistNomeContato.Text.Replace("'", "´")
        End Get
        Set(ByVal value As String)
            Me.txtVistNomeContato.Text = value.Replace("'", "´")
        End Set
    End Property

    Public Property VistNomeContatoReadOnly() As Boolean
        Get
            Return Me.txtVistNomeContato.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtVistNomeContato.ReadOnly = value
        End Set
    End Property

    Public Property VistEndereco() As String
        Get
            Return Me.txtVistEndereco.Text.Replace("'", "´")
        End Get
        Set(ByVal value As String)
            Me.txtVistEndereco.Text = value.Replace("'", "´")
        End Set
    End Property

    Public Property VistEnderecoReadOnly() As Boolean
        Get
            Return Me.txtVistNomeContato.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtVistEndereco.ReadOnly = value
        End Set
    End Property

    Public Property VistBairro() As String
        Get
            Return Me.txtVistBairro.Text.Replace("'", "´")
        End Get
        Set(ByVal value As String)
            Me.txtVistBairro.Text = value.Replace("'", "´")
        End Set
    End Property

    Public Property VistBairroReadOnly() As Boolean
        Get
            Return Me.txtVistBairro.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtVistBairro.ReadOnly = value
        End Set
    End Property

    Public Property VistUFText() As String
        Get
            Return cboVistUF.SelectedItem.Text
        End Get
        Set(ByVal value As String)
            PreencheComboMunicipio(value)
            cboVistUF.SelectedItem.Text = value
        End Set
    End Property

    Public Property VistUFTextEnable() As Boolean
        Get
            Return Me.cboVistUF.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me.cboVistUF.Enabled = value
        End Set
    End Property

    Public Property VistMunicipioText() As String
        Get
            Return cboVistMunicipio.SelectedItem.Text
        End Get
        Set(ByVal value As String)
            cboVistMunicipio.SelectedItem.Text = value
        End Set
    End Property

    Public Property VistMunicipioTextEnable() As Boolean
        Get
            Return Me.cboVistMunicipio.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me.cboVistMunicipio.Enabled = value
        End Set
    End Property

    Public Property VistCEP() As String
        Get
            Return Me.txtVistCEP.Text.Replace("'", "´")
        End Get
        Set(ByVal value As String)
            Me.txtVistCEP.Text = value.Replace("'", "´")
        End Set
    End Property

    Public Property VistCEPReadOnly() As Boolean
        Get
            Return Me.txtVistCEP.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtVistCEP.ReadOnly = value
        End Set
    End Property

    Public Property VistDDD1() As String
        Get
            Return Me.txtVistDDD1.Text.Replace("'", "´")
        End Get
        Set(ByVal value As String)
            Me.txtVistDDD1.Text = value.Replace("'", "´")
        End Set
    End Property

    Public Property VistDDD1ReadOnly() As Boolean
        Get
            Return Me.txtVistDDD1.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtVistDDD1.ReadOnly = value
        End Set
    End Property

    Public Property VistDDD2() As String
        Get
            Return Me.txtVistDDD2.Text.Replace("'", "´")
        End Get
        Set(ByVal value As String)
            Me.txtVistDDD2.Text = value.Replace("'", "´")
        End Set
    End Property

    Public Property VistDDD2ReadOnly() As Boolean
        Get
            Return Me.txtVistDDD2.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtVistDDD2.ReadOnly = value
        End Set
    End Property


    Public Property VistTelefone1() As String
        Get
            Return Me.txtVistTelefone1.Text.Replace("'", "´")
        End Get
        Set(ByVal value As String)
            Me.txtVistTelefone1.Text = value.Replace("'", "´")
        End Set
    End Property

    Public Property VistTelefone1ReadOnly() As Boolean
        Get
            Return Me.txtVistTelefone1.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtVistTelefone1.ReadOnly = value
        End Set
    End Property

    Public Property VistTelefone2() As String
        Get
            Return Me.txtVistTelefone2.Text.Replace("'", "´")
        End Get
        Set(ByVal value As String)
            Me.txtVistTelefone2.Text = value.Replace("'", "´")
        End Set
    End Property

    Public Property VistTelefone2ReadOnly() As Boolean
        Get
            Return Me.txtVistTelefone2.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtVistTelefone2.ReadOnly = value
        End Set
    End Property

    Public Property VistTpTelefone1Text() As String
        Get
            Return cboVistTpTelefone1.SelectedItem.Text
        End Get
        Set(ByVal value As String)
            cboVistTpTelefone1.SelectedItem.Text = value
        End Set
    End Property

    Public Property VistTpTelefone1TextEnable() As Boolean
        Get
            Return Me.cboVistTpTelefone1.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me.cboVistTpTelefone1.Enabled = value
        End Set
    End Property

    Public Property VistTpTelefone2Text() As String
        Get
            Return cboVistTpTelefone2.SelectedItem.Text
        End Get
        Set(ByVal value As String)
            cboVistTpTelefone2.SelectedItem.Text = value
        End Set
    End Property

    Public Property VistTpTelefone2TextEnable() As Boolean
        Get
            Return Me.cboVistTpTelefone2.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me.cboVistTpTelefone2.Enabled = value
        End Set
    End Property

    Private Property SourceMunicipio() As List(Of SEGS8707_SPS_EO_OUT)
        Get
            Return CType(ViewState("SourceMunicipio"), List(Of SEGS8707_SPS_EO_OUT))
        End Get
        Set(ByVal value As List(Of SEGS8707_SPS_EO_OUT))
            ViewState("SourceMunicipio") = value
        End Set
    End Property
    Private Property SourceUF() As List(Of SEGS8708_SPS_EO_OUT)
        Get
            Return CType(ViewState("SourceUF"), List(Of SEGS8708_SPS_EO_OUT))
        End Get
        Set(ByVal value As List(Of SEGS8708_SPS_EO_OUT))
            ViewState("SourceUF") = value
        End Set
    End Property

#End Region

    Public Sub PreencheComboMunicipio(ByVal UF As String)

        If IsNothing(Me.SourceMunicipio) Then
            Dim ObjEO_IN As New SEGS8707_SPS_EO_IN

            Me.SourceMunicipio = Me.taskSolicitante.CarregaMunicipio(ObjEO_IN)

            If Not IsNothing(Me.SourceMunicipio) Then
                Me.cboVistMunicipio.DataSource = Me.SourceMunicipio
                Me.cboVistMunicipio.DataTextField = "NOME"
                Me.cboVistMunicipio.DataValueField = "NOME" '"MUNICIPIO_ID"
                Me.cboVistMunicipio.DataBind()

                Me.cboLCMunicipio.DataSource = Me.SourceMunicipio
                Me.cboLCMunicipio.DataTextField = "NOME"
                Me.cboLCMunicipio.DataValueField = "NOME" '"MUNICIPIO_ID"
                Me.cboLCMunicipio.DataBind()

            End If
        Else
            Exit Sub
        End If

    End Sub

    Public Sub PreencheComboUF()

        Dim ObjEO_IN As New SEGS8708_SPS_EO_IN

        Me.SourceUF = Me.taskSolicitante.CarregaUF(ObjEO_IN)

        If Not IsNothing(Me.SourceUF) Then
            Me.cboLCUF.DataSource = Me.SourceUF
            Me.cboLCUF.DataTextField = "NOME"
            Me.cboLCUF.DataValueField = "NOME"
            Me.cboLCUF.DataBind()

            Me.cboVistUF.DataSource = Me.SourceUF
            Me.cboVistUF.DataTextField = "NOME"
            Me.cboVistUF.DataValueField = "NOME"
            Me.cboVistUF.DataBind()
        End If
        cboLCUF.Items.Insert(0, New ListItem("Escolha o estado", ""))
        cboVistUF.Items.Insert(0, New ListItem("Escolha o estado", ""))

    End Sub
    Public Sub PreencheCombosTpTelefone()

        With Me.cboVistTpTelefone1
            .Items.Add(New ListItem("-Selecione-", "0"))
            .Items.Add(New ListItem("Residencial", "1"))
            .Items.Add(New ListItem("Comercial", "2"))
            .Items.Add(New ListItem("Celular", "3"))
            .Items.Add(New ListItem("Fax", "4"))
            .Items.Add(New ListItem("Recado", "5"))
        End With
        With Me.cboVistTpTelefone2
            .Items.Add(New ListItem("-Selecione-", "0"))
            .Items.Add(New ListItem("Residencial", "1"))
            .Items.Add(New ListItem("Comercial", "2"))
            .Items.Add(New ListItem("Celular", "3"))
            .Items.Add(New ListItem("Fax", "4"))
            .Items.Add(New ListItem("Recado", "5"))
        End With

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



    End Sub

End Class