﻿Imports SEGL0343.Task
Imports SEGL0343.EO

Partial Public Class UCDadosSolicitanteVida
    Inherits System.Web.UI.UserControl
    Private taskSolicitante As New SEGW0157Solicitante_TK

    Private objSource As List(Of SEGS8705_SPS_EO_OUT)

    Dim _Cliente_ID As Integer
    Dim ObjMunicipio As List(Of SEL_MUNICIPIO_SPS_EO_OUT)
    Dim objSourceUF As List(Of SEGS8708_SPS_EO_OUT)

#Region "Propriedades"

    Public Property Nome() As String
        Get
            Return txtNome.Text
        End Get
        Set(ByVal value As String)
            txtNome.Text = value
        End Set
    End Property

    Public Property NomeReadOnly() As Boolean
        Get
            Return txtNome.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            txtNome.ReadOnly = value
        End Set
    End Property

    Public Property CEP() As String
        Get
            Return txtCEP.Text
        End Get
        Set(ByVal value As String)
            txtCEP.Text = value
        End Set
    End Property

    Public Property CEPReadOnly() As Boolean
        Get
            Return txtCEP.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            txtCEP.ReadOnly = value
        End Set
    End Property

    Public Property Municipio() As String
        Get
            Return ddlCidade.SelectedValue
        End Get
        Set(ByVal value As String)
            ddlCidade.SelectedValue = value
        End Set
    End Property

    Public Property MunicipioText() As String
        Get
            Return ddlCidade.Text
        End Get
        Set(ByVal value As String)
            ddlCidade.Text = value
        End Set
    End Property

    Public Property MunicipioEnabled() As Boolean
        Get
            Return ddlCidade.Enabled
        End Get
        Set(ByVal value As Boolean)
            ddlCidade.Enabled = value
        End Set
    End Property

    Public Property Endereco() As String
        Get
            Return txtEndereco.Text
        End Get
        Set(ByVal value As String)
            txtEndereco.Text = value
        End Set
    End Property

    Public Property EnderecoReadOnly() As Boolean
        Get
            Return txtEndereco.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            txtEndereco.ReadOnly = value
        End Set
    End Property

    Public Property UF() As String
        Get
            Return ddlUF.SelectedValue
        End Get
        Set(ByVal value As String)
            ddlUF.SelectedValue = value
            PreencheComboMunicipio(value)
        End Set
    End Property

    Public Property UFText() As String
        Get
            Return ddlUF.Text
        End Get
        Set(ByVal value As String)
            ddlUF.Text = value
            PreencheComboMunicipio(value)
        End Set
    End Property

    Public Property UFEnabled() As Boolean
        Get
            Return ddlUF.Enabled
        End Get
        Set(ByVal value As Boolean)
            If Not String.IsNullOrEmpty(value) Then
                ddlUF.Enabled = value
            End If
        End Set
    End Property

    Public Property Bairro() As String
        Get
            Return txtBairro.Text
        End Get
        Set(ByVal value As String)
            txtBairro.Text = value
        End Set
    End Property

    Public Property BairroReadOnly() As Boolean
        Get
            Return txtBairro.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            txtBairro.ReadOnly = value
        End Set
    End Property

    Public Property ibtPesquisarCEPVisible() As Boolean
        Get
            Return ibtPesquisarCEP.Visible
        End Get
        Set(ByVal value As Boolean)
            ibtPesquisarCEP.Visible = value
        End Set
    End Property

    Public Property ibtPesquisarCEPClienteID() As String
        Get
            Return ibtPesquisarCEP.ClientID.ToString
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public WriteOnly Property ddlUF_Habilitado() As Boolean
        Set(ByVal value As Boolean)
            ddlUF.Enabled = value
        End Set
    End Property

    Public WriteOnly Property ddlCidade_Habilitado() As Boolean
        Set(ByVal value As Boolean)
            ddlCidade.Enabled = value
        End Set
    End Property

    Public Property BotaoCEPVisible() As Boolean
        Get
            Return ibtPesquisarCEP.Visible
        End Get
        Set(ByVal value As Boolean)
            ibtPesquisarCEP.Visible = value
        End Set
    End Property

    ' TELEFONE 1

    Public Property DDD1() As String
        Get
            Return Me.TelSolicitante1.DDD
        End Get
        Set(ByVal value As String)
            Me.TelSolicitante1.DDD = value
        End Set
    End Property

    Public Property DDD1ReadOnly() As Boolean
        Get
            Return TelSolicitante1.DDDReadOnly
        End Get
        Set(ByVal value As Boolean)
            TelSolicitante1.DDDReadOnly = value
        End Set
    End Property

    Public Property Telefone1() As String
        Get
            Return Me.TelSolicitante1.Telefone
        End Get
        Set(ByVal value As String)
            Me.TelSolicitante1.Telefone = value
        End Set
    End Property

    Public Property Telefone1ReadOnly() As Boolean
        Get
            Return TelSolicitante1.TelefoneReadOnly
        End Get
        Set(ByVal value As Boolean)
            TelSolicitante1.TelefoneReadOnly = value
        End Set
    End Property

    Public Property Ramal1() As String
        Get

            Return Me.TelSolicitante1.Ramal

        End Get
        Set(ByVal value As String)

            Me.TelSolicitante1.Ramal = value

        End Set
    End Property

    Public Property Ramal1ReadOnly() As Boolean
        Get
            Return TelSolicitante1.RamalReadOnly
        End Get
        Set(ByVal value As Boolean)
            TelSolicitante1.RamalReadOnly = value
        End Set
    End Property


    Public Property TipoTelefone1() As String
        Get

            Return Me.TelSolicitante1.TipoTelefoneItem

        End Get
        Set(ByVal value As String)
            If Not String.IsNullOrEmpty(value) Then
                Me.TelSolicitante1.TipoTelefoneItem = value
            End If

        End Set
    End Property

    Public Property TipoTelefone1Enabled() As Boolean
        Get

            Return Me.TelSolicitante1.TipoTelefoneEnabled

        End Get
        Set(ByVal value As Boolean)

            Me.TelSolicitante1.TipoTelefoneEnabled = value

        End Set
    End Property


    ' TELEFONE 2
    Public Property DDD2() As String
        Get
            Return Me.TelSolicitante2.DDD
        End Get
        Set(ByVal value As String)
            Me.TelSolicitante2.DDD = value
        End Set
    End Property

    Public Property DDD2ReadOnly() As Boolean
        Get
            Return TelSolicitante2.DDDReadOnly
        End Get
        Set(ByVal value As Boolean)
            TelSolicitante2.DDDReadOnly = value
        End Set
    End Property

    Public Property Telefone2() As String
        Get
            Return Me.TelSolicitante2.Telefone
        End Get
        Set(ByVal value As String)
            Me.TelSolicitante2.Telefone = value
        End Set
    End Property

    Public Property Telefone2ReadOnly() As Boolean
        Get
            Return TelSolicitante2.TelefoneReadOnly
        End Get
        Set(ByVal value As Boolean)
            TelSolicitante2.TelefoneReadOnly = value
        End Set
    End Property

    Public Property Ramal2() As String
        Get

            Return Me.TelSolicitante2.Ramal

        End Get
        Set(ByVal value As String)

            Me.TelSolicitante2.Ramal = value

        End Set
    End Property

    Public Property Ramal2ReadOnly() As Boolean
        Get
            Return TelSolicitante2.RamalReadOnly
        End Get
        Set(ByVal value As Boolean)
            TelSolicitante2.RamalReadOnly = value
        End Set
    End Property

    Public Property TipoTelefone2() As String
        Get

            Return Me.TelSolicitante2.TipoTelefoneItem

        End Get
        Set(ByVal value As String)
            If Not String.IsNullOrEmpty(value) Then
                Me.TelSolicitante2.TipoTelefoneItem = value
            End If
        End Set
    End Property

    Public Property TipoTelefone2Enabled() As Boolean
        Get

            Return Me.TelSolicitante2.TipoTelefoneEnabled

        End Get
        Set(ByVal value As Boolean)

            Me.TelSolicitante2.TipoTelefoneEnabled = value

        End Set
    End Property

    ' TELEFONE 3
    Public Property DDD3() As String
        Get
            Return Me.TelSolicitante3.DDD
        End Get
        Set(ByVal value As String)
            Me.TelSolicitante3.DDD = value
        End Set
    End Property

    Public Property DDD3ReadOnly() As Boolean
        Get
            Return TelSolicitante3.DDDReadOnly
        End Get
        Set(ByVal value As Boolean)
            TelSolicitante3.DDDReadOnly = value
        End Set
    End Property

    Public Property Telefone3() As String
        Get
            Return Me.TelSolicitante3.Telefone
        End Get
        Set(ByVal value As String)
            Me.TelSolicitante3.Telefone = value
        End Set
    End Property

    Public Property Telefone3ReadOnly() As Boolean
        Get
            Return TelSolicitante3.TelefoneReadOnly
        End Get
        Set(ByVal value As Boolean)
            TelSolicitante3.TelefoneReadOnly = value
        End Set
    End Property

    Public Property Ramal3() As String
        Get

            Return Me.TelSolicitante3.Ramal

        End Get
        Set(ByVal value As String)

            Me.TelSolicitante3.Ramal = value

        End Set
    End Property

    Public Property Ramal3ReadOnly() As Boolean
        Get
            Return TelSolicitante3.RamalReadOnly
        End Get
        Set(ByVal value As Boolean)
            TelSolicitante3.RamalReadOnly = value
        End Set
    End Property

    Public Property TipoTelefone3() As String
        Get

            Return Me.TelSolicitante3.TipoTelefoneItem

        End Get
        Set(ByVal value As String)
            If Not String.IsNullOrEmpty(value) Then
                Me.TelSolicitante3.TipoTelefoneItem = value
            End If
        End Set
    End Property

    Public Property TipoTelefone3Enabled() As Boolean
        Get

            Return Me.TelSolicitante3.TipoTelefoneEnabled

        End Get
        Set(ByVal value As Boolean)

            Me.TelSolicitante3.TipoTelefoneEnabled = value

        End Set
    End Property

    Public Property DDD4() As String
        Get
            Return Me.TelSolicitante4.DDD
        End Get
        Set(ByVal value As String)
            Me.TelSolicitante4.DDD = value
        End Set
    End Property

    Public Property DDD4ReadOnly() As Boolean
        Get
            Return TelSolicitante4.DDDReadOnly
        End Get
        Set(ByVal value As Boolean)
            TelSolicitante4.DDDReadOnly = value
        End Set
    End Property

    Public Property Telefone4() As String
        Get
            Return Me.TelSolicitante4.Telefone
        End Get
        Set(ByVal value As String)
            Me.TelSolicitante4.Telefone = value
        End Set
    End Property

    Public Property Telefone4ReadOnly() As Boolean
        Get
            Return TelSolicitante4.TelefoneReadOnly
        End Get
        Set(ByVal value As Boolean)
            TelSolicitante4.TelefoneReadOnly = value
        End Set
    End Property

    Public Property Ramal4() As String
        Get

            Return Me.TelSolicitante4.Ramal

        End Get
        Set(ByVal value As String)

            Me.TelSolicitante4.Ramal = value

        End Set
    End Property

    Public Property Ramal4ReadOnly() As Boolean
        Get
            Return TelSolicitante4.RamalReadOnly
        End Get
        Set(ByVal value As Boolean)
            TelSolicitante4.RamalReadOnly = value
        End Set
    End Property

    Public Property TipoTelefone4() As String
        Get

            Return Me.TelSolicitante4.TipoTelefoneItem

        End Get
        Set(ByVal value As String)
            If Not String.IsNullOrEmpty(value) Then
                Me.TelSolicitante4.TipoTelefoneItem = value
            End If
        End Set
    End Property

    Public Property TipoTelefone4Enabled() As Boolean
        Get

            Return Me.TelSolicitante4.TipoTelefoneEnabled

        End Get
        Set(ByVal value As Boolean)

            Me.TelSolicitante4.TipoTelefoneEnabled = value

        End Set
    End Property

    ' TELEFONE 5
    Public Property DDD5() As String
        Get
            Return Me.TelSolicitante5.DDD
        End Get
        Set(ByVal value As String)
            Me.TelSolicitante5.DDD = value
        End Set
    End Property

    Public Property DDD5ReadOnly() As Boolean
        Get
            Return TelSolicitante5.DDDReadOnly
        End Get
        Set(ByVal value As Boolean)
            TelSolicitante5.DDDReadOnly = value
        End Set
    End Property

    Public Property Telefone5() As String
        Get
            Return Me.TelSolicitante5.Telefone
        End Get
        Set(ByVal value As String)
            Me.TelSolicitante5.Telefone = value
        End Set
    End Property

    Public Property Telefone5ReadOnly() As Boolean
        Get
            Return TelSolicitante5.TelefoneReadOnly
        End Get
        Set(ByVal value As Boolean)
            TelSolicitante5.TelefoneReadOnly = value
        End Set
    End Property

    Public Property Ramal5() As String
        Get

            Return Me.TelSolicitante5.Ramal

        End Get
        Set(ByVal value As String)

            Me.TelSolicitante5.Ramal = value

        End Set
    End Property

    Public Property Ramal5ReadOnly() As Boolean
        Get
            Return TelSolicitante5.RamalReadOnly
        End Get
        Set(ByVal value As Boolean)
            TelSolicitante5.RamalReadOnly = value
        End Set
    End Property

    Public Property TipoTelefone5() As String
        Get
            Return Me.TelSolicitante5.TipoTelefoneItem
        End Get
        Set(ByVal value As String)
            If Not String.IsNullOrEmpty(value) Then
                Me.TelSolicitante5.TipoTelefoneItem = value
            End If
        End Set
    End Property

    Public Property TipoTelefone5Enabled() As Boolean
        Get

            Return Me.TelSolicitante5.TipoTelefoneEnabled

        End Get
        Set(ByVal value As Boolean)

            Me.TelSolicitante5.TipoTelefoneEnabled = value

        End Set
    End Property

    Public Property Email() As String
        Get

            Return txtEmail.Text

        End Get
        Set(ByVal value As String)

            txtEmail.Text = value

        End Set
    End Property

    Public Property EmailReadOnly() As Boolean
        Get
            Return txtEmail.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            txtEmail.ReadOnly = value
        End Set
    End Property

    Private Property SourceUF() As List(Of SEGS8708_SPS_EO_OUT)
        Get
            Return CType(objSourceUF, List(Of SEGS8708_SPS_EO_OUT))
        End Get
        Set(ByVal value As List(Of SEGS8708_SPS_EO_OUT))
            objSourceUF = value
        End Set
    End Property

    Private Property SourceMunicipio() As List(Of SEL_MUNICIPIO_SPS_EO_OUT) 'SEGS8707_SPS_EO_OUT)
        Get
            Return CType(ObjMunicipio, List(Of SEL_MUNICIPIO_SPS_EO_OUT)) 'SEGS8707_SPS_EO_OUT))
        End Get
        Set(ByVal value As List(Of SEL_MUNICIPIO_SPS_EO_OUT)) 'SEGS8707_SPS_EO_OUT))
            ObjMunicipio = value
        End Set
    End Property

    Public Property Cliente_ID() As Integer
        Get
            Return ViewState("Cliente_ID")
        End Get
        Set(ByVal value As Integer)
            ViewState("Cliente_ID") = value
        End Set
    End Property

    Public Property Cidade_Sel() As String
        Get
            Return ViewState("Cidade_Sel")
        End Get
        Set(ByVal value As String)
            ViewState("Cidade_Sel") = value
        End Set
    End Property

    Public Property btnSegurado_Visible() As Boolean
        Get
            Return Me.btnSegurado_fora.Visible
        End Get
        Set(ByVal value As Boolean)
            Me.btnSegurado_fora.Visible = value
        End Set
    End Property

#End Region

#Region "Eventos"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.Page.IsPostBack Then

            If Me.ddlCidade.Items.Count = 0 Then
                ddlCidade.Items.Insert(0, New ListItem("Escolha o município", ""))
                Me.ddlCidade.SelectedIndex = 0
            End If

            Me.txtEmail.Attributes.Add("onblur", "javascript:ValidaEmail(this);")


            Dim txtRamal As TextBox = TelSolicitante1.FindControl("txtRamal")
            Dim txtDDD As TextBox = TelSolicitante1.FindControl("txtDDD")
            Dim txtTelefone As TextBox = TelSolicitante1.FindControl("txtTelefone")
            Dim ddlTipoTelefone As DropDownList = TelSolicitante1.FindControl("ddlTipoTelefone")

            Dim txtRamal1 As TextBox = TelSolicitante2.FindControl("txtRamal")
            Dim txtDDD1 As TextBox = TelSolicitante2.FindControl("txtDDD")
            Dim txtTelefone1 As TextBox = TelSolicitante2.FindControl("txtTelefone")
            Dim ddlTipoTelefone1 As DropDownList = TelSolicitante2.FindControl("ddlTipoTelefone")

            Dim txtRamal2 As TextBox = TelSolicitante3.FindControl("txtRamal")
            Dim txtDDD2 As TextBox = TelSolicitante3.FindControl("txtDDD")
            Dim txtTelefone2 As TextBox = TelSolicitante3.FindControl("txtTelefone")
            Dim ddlTipoTelefone2 As DropDownList = TelSolicitante3.FindControl("ddlTipoTelefone")


            Dim txtRamal3 As TextBox = TelSolicitante4.FindControl("txtRamal")
            Dim txtDDD3 As TextBox = TelSolicitante4.FindControl("txtDDD")
            Dim txtTelefone3 As TextBox = TelSolicitante4.FindControl("txtTelefone")
            Dim ddlTipoTelefone3 As DropDownList = TelSolicitante4.FindControl("ddlTipoTelefone")

            Dim txtRamal4 As TextBox = TelSolicitante5.FindControl("txtRamal")
            Dim txtDDD4 As TextBox = TelSolicitante5.FindControl("txtDDD")
            Dim txtTelefone4 As TextBox = TelSolicitante5.FindControl("txtTelefone")
            Dim ddlTipoTelefone4 As DropDownList = TelSolicitante5.FindControl("ddlTipoTelefone")


            txtDDD.TabIndex = 91
            txtTelefone.TabIndex = 92
            txtRamal.TabIndex = 93
            ddlTipoTelefone.TabIndex = 94

            txtDDD1.TabIndex = 95
            txtTelefone1.TabIndex = 96
            txtRamal1.TabIndex = 97
            ddlTipoTelefone1.TabIndex = 98

            txtDDD4.TabIndex = 99
            txtTelefone4.TabIndex = 100
            txtRamal4.TabIndex = 101
            ddlTipoTelefone4.TabIndex = 102

            txtDDD3.TabIndex = 103
            txtTelefone3.TabIndex = 104
            txtRamal3.TabIndex = 105
            ddlTipoTelefone3.TabIndex = 106

            txtDDD4.TabIndex = 107
            txtTelefone4.TabIndex = 108
            txtRamal4.TabIndex = 109
            ddlTipoTelefone4.TabIndex = 110
        End If

    End Sub

    Protected Sub ddlUF_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlUF.SelectedIndexChanged

        If Not String.IsNullOrEmpty(ddlUF.SelectedValue) Then
            PreencheComboMunicipio(ddlUF.SelectedValue)
        End If

    End Sub


#End Region

#Region "Métodos"

    Public Sub PreencheComboUF()

        Dim ObjEO_IN As New SEGS8708_SPS_EO_IN

        Me.SourceUF = Me.taskSolicitante.CarregaUF(ObjEO_IN)

        If Not IsNothing(Me.SourceUF) Then
            Me.ddlUF.DataSource = Me.SourceUF
            Me.ddlUF.DataTextField = "NOME"
            Me.ddlUF.DataValueField = "NOME"
            Me.ddlUF.DataBind()
        End If

    End Sub

    Private Sub PreencheComboMunicipio()

        Dim Obj_TK As New UCDadosSolicitanteVida_TK()
        Dim Obj_EO_IN As New SEL_MUNICIPIO_SPS_EO_IN()

        Obj_EO_IN.Estado = Me.UF
        Obj_EO_IN.Municipio = String.Empty
        Obj_EO_IN.Municipio_Id = String.Empty

        ddlCidade.DataSource = Obj_TK.ObtemListaMunicipio(Obj_EO_IN)
        ddlCidade.DataTextField = "nome"
        ddlCidade.DataValueField = "nome"
        ddlCidade.DataBind()
        ddlCidade.Items.Insert(0, New ListItem("Escolha o município", ""))
        Me.ddlCidade.SelectedIndex = 0

        If Not IsNothing(ViewState("Cidade_Sel")) Then
            Me.ddlCidade.Text = ViewState("Cidade_Sel").ToString
        End If

    End Sub

    Public Sub PreencheComboMunicipio(ByVal UF As String)

        Dim Obj_TK As New UCDadosSolicitanteVida_TK()
        Dim Obj_EO_IN As New SEL_MUNICIPIO_SPS_EO_IN()

        Obj_EO_IN.Estado = UF
        Obj_EO_IN.Municipio = String.Empty
        Obj_EO_IN.Municipio_Id = String.Empty

        Dim lstListaMunicipio As New List(Of SEL_MUNICIPIO_SPS_EO_OUT)
        Dim ItemListaMunicipio As New SEL_MUNICIPIO_SPS_EO_OUT

        'lstListaMunicipio = Obj_TK.ObtemListaMunicipio(Obj_EO_IN)

        For Each ItemListaMunicipio In Obj_TK.ObtemListaMunicipio(Obj_EO_IN)

            ItemListaMunicipio.nome = ItemListaMunicipio.nome.Trim
            lstListaMunicipio.Add(ItemListaMunicipio)


        Next


        'Me.SourceMunicipio = Obj_TK.ObtemListaMunicipio(Obj_EO_IN)
        Me.SourceMunicipio = lstListaMunicipio


        If Not IsNothing(Me.SourceMunicipio) Then
            Me.ddlCidade.DataSource = Me.SourceMunicipio
            Me.ddlCidade.DataTextField = "NOME"
            Me.ddlCidade.DataValueField = "NOME"
            Me.ddlCidade.DataBind()
            ddlCidade.Items.Insert(0, New ListItem("Escolha o município", ""))
            Me.ddlCidade.SelectedIndex = 0

        End If

    End Sub

    Public Sub PreencheComboMunicipio(ByVal UF As String, ByVal strMunicipio As String)
        Try
            Dim Obj_TK As New UCDadosSolicitanteVida_TK()
            Dim Obj_EO_IN As New SEL_MUNICIPIO_SPS_EO_IN()

            Obj_EO_IN.Estado = UF
            ddlCidade.DataSource = Obj_TK.ObtemListaMunicipio(Obj_EO_IN)
            ddlCidade.DataTextField = "NOME"
            ddlCidade.DataValueField = "NOME"
            ddlCidade.DataBind()
            ddlCidade.Items.Insert(0, New ListItem("Escolha o município", ""))
            Me.ddlCidade.Text = strMunicipio.Trim()

        Catch ex As Exception

        End Try

    End Sub


    Private Sub BindEnderecoObjSolicitante()
        'base dos correios
        Dim Obj_TK As New UCDadosSolicitanteVida_TK()
        Dim Obj_EO_IN As New SEGS8839_SPS_EO_IN()
        Dim Obj_EO_OUT As New SEGS8839_SPS_EO_OUT()

        'base interna
        Dim municipio As New municipio_spi_EO_IN
        Dim lista_interna_in As New SEL_MUNICIPIO_SPS_EO_IN
        Dim lista_interna_out As New List(Of SEL_MUNICIPIO_SPS_EO_OUT)
        'atualização
        Dim atualiza As New municipio_spu_EO_IN

        Obj_EO_IN.Cep = Me.CEP.Replace("-", "")
        Obj_EO_OUT = Obj_TK.ObtemEnderecoObjSegurado(Obj_EO_IN)

        'verifica se o municipio está na tabela dos correios
        If Not IsNothing(Obj_EO_OUT) Then
            Me.Bairro = Obj_EO_OUT.Bairro.Trim
            Me.CEP = Obj_EO_OUT.CEP.Trim
            Me.Endereco = Obj_EO_OUT.Endereco.Trim
            PreencheComboUF()
            Me.UF = Obj_EO_OUT.UF.Trim
            PreencheComboMunicipio(Me.UF)
            If Not String.IsNullOrEmpty(Obj_EO_OUT.Municipio.ToString) Then
                Try
                    Me.ddlCidade.Text = Obj_EO_OUT.Municipio.ToString.Trim()
                Catch
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Município não encontrado." & "')", True)
                End Try
            End If
        End If

    End Sub


    Public Function RetornaMunicipio(ByVal strCodMunicipio As String, ByVal strUF As String, Optional ByVal strMunicipio As String = "") As Boolean

        Dim Obj_TK As New UCAgencia_TK()
        Dim Obj_EO_IN As New SEL_MUNICIPIO_SPS_EO_IN()

        Dim Obj_EO_OUT As List(Of SEL_MUNICIPIO_SPS_EO_OUT)

        Obj_EO_IN.Municipio_Id = strCodMunicipio
        Obj_EO_IN.Estado = strUF
        Obj_EO_IN.Municipio = strMunicipio


        Obj_EO_OUT = Obj_TK.ObtemListaMunicipio(Obj_EO_IN)

        If Obj_EO_OUT.Count > 0 Then

            Me.Municipio = Obj_EO_OUT(0).nome

        End If

    End Function

#End Region

    Protected Sub ibtPesquisarCEP_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtPesquisarCEP.Click
        Dim Obj_TK As New UCAgencia_TK

        If Me.CEP <> String.Empty Then
            Me.txtNome.Text = ""
            BindEnderecoObjSolicitante()
            Me.txtNome.Text = ""
            Me.txtNome.Focus()            
        End If
    End Sub

    Protected Sub btnSegurado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSegurado.Click
        Try

            CarregarDadosCliente(Me.Cliente_ID)

        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro ao direcionar para o formulário de Consulta Sinistrado" & "')", True)
        End Try
    End Sub

    Public Sub CarregarDadosCliente(ByVal Cliente_ID As Integer)
        Try
            Dim ObjEO_IN As New SEGS8705_SPS_EO_IN
            ObjEO_IN.Cliente_Id = Cliente_ID

            objSource = taskSolicitante.ObtemDadosSegurado(ObjEO_IN)

            'Preenche controles da tela
            If Not IsNothing(objSource) And objSource.Count <> 0 Then
                Me.PreencherControlesTela(objSource(0))
            Else
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Cliente não encontrado!" & "')", True)
            End If

        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro ao carregar dados do Cliente" & "')", True)
        End Try
    End Sub

    Private Sub PreencherControlesTela(ByRef objEO_OUT As SEGS8705_SPS_EO_OUT)
        Try
            Me.txtNome.Text = objEO_OUT.NOME
            Me.txtEndereco.Text = objEO_OUT.ENDERECO
            Me.txtBairro.Text = objEO_OUT.BAIRRO
            If Not String.IsNullOrEmpty(objEO_OUT.ESTADO) Then
                Me.ddlUF.Text = objEO_OUT.ESTADO
            End If


            PreencheComboMunicipio(Me.UF)
            If Not String.IsNullOrEmpty(objEO_OUT.MUNICIPIO) Then
                Try
                    Me.ddlCidade.Text = objEO_OUT.MUNICIPIO
                Catch
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Município não encontrado." & "')", True)
                End Try
            End If
            Me.txtCEP.Text = objEO_OUT.CEP

            If Not String.IsNullOrEmpty(objEO_OUT.DDD) Then
                Me.TelSolicitante1.DDD = CStr(CInt(objEO_OUT.DDD))
            End If
            Me.TelSolicitante1.Telefone = objEO_OUT.TELEFONE

        Catch ex As Exception
        End Try
    End Sub

End Class