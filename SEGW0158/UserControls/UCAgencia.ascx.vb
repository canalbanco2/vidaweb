﻿Imports SEGL0343.Task
Imports SEGL0343.EO

Partial Public Class UCAgencia
    Inherits System.Web.UI.UserControl

    Private taskConsultaAgencia As New SEGW0158ConsultaAgencia_Task
    Private taskSolicitante As New SEGW0157Solicitante_TK
    Private taskUCAgencia_TK As New UCAgencia_TK


#Region "Propriedades"

    Public Property Estado() As Integer
        Get
            Return cboEstado.SelectedValue
        End Get
        Set(ByVal value As Integer)
            cboEstado.SelectedValue = value
        End Set
    End Property

    Public Property Municipio() As Integer
        Get
            Return cboMunicipio.SelectedValue
        End Get
        Set(ByVal value As Integer)
            cboMunicipio.SelectedValue = value
        End Set
    End Property

    Public Property AgenciaGrid() As String
        Get
            Return Me.txtCodigoAgenciaGrid.Text
        End Get
        Set(ByVal value As String)
            Me.txtCodigoAgenciaGrid.Text = value
        End Set
    End Property

    Public Property DVGrid() As String
        Get
            Return Me.txtDVGrid.Text
        End Get
        Set(ByVal value As String)
            Me.txtDVGrid.Text = value
        End Set
    End Property

    Public Property NomeAgenciaGrid() As String
        Get
            Return Me.txtNomeAgenciaGrid.Text
        End Get
        Set(ByVal value As String)
            Me.txtNomeAgenciaGrid.Text = value
        End Set
    End Property

    Public Property EnderecoGrid() As String
        Get
            Return Me.txtEnderecoGrid.Text
        End Get
        Set(ByVal value As String)
            Me.txtEnderecoGrid.Text = value
        End Set
    End Property

    Public Property BairroGrid() As String
        Get
            Return Me.txtBairroGrid.Text
        End Get
        Set(ByVal value As String)
            Me.txtBairroGrid.Text = value
        End Set
    End Property

    Public Property MunicipioGrid() As String
        Get
            Return Me.txtMunicipioGrid.Text
        End Get
        Set(ByVal value As String)
            Me.txtMunicipioGrid.Text = value
        End Set
    End Property

    Public Property UFGrid() As String
        Get
            Return Me.txtUFGrid.Text()
        End Get
        Set(ByVal value As String)
            Me.txtUFGrid.Text = value
        End Set
    End Property

    Private Property SourceMunicipio() As List(Of SEGS8707_SPS_EO_OUT)
        Get
            Return CType(ViewState("SourceMunicipio"), List(Of SEGS8707_SPS_EO_OUT))
        End Get
        Set(ByVal value As List(Of SEGS8707_SPS_EO_OUT))
            ViewState("SourceMunicipio") = value
        End Set
    End Property

    Private Property SourceUF() As List(Of SEGS8708_SPS_EO_OUT)
        Get
            Return CType(ViewState("SourceUF"), List(Of SEGS8708_SPS_EO_OUT))
        End Get
        Set(ByVal value As List(Of SEGS8708_SPS_EO_OUT))
            ViewState("SourceUF") = value
        End Set
    End Property

#End Region

#Region "Eventos"



    Public Sub CarregarMunicipio()
        Try
            If IsNothing(Me.SourceMunicipio) Then
                Dim ObjEO_IN As New SEGS8707_SPS_EO_IN

                Me.SourceMunicipio = Me.taskSolicitante.CarregaMunicipio(ObjEO_IN)

                If Not IsNothing(Me.SourceMunicipio) Then
                    Me.cboMunicipio.DataSource = Me.SourceMunicipio
                    Me.cboMunicipio.DataTextField = "NOME"
                    Me.cboMunicipio.DataValueField = "MUNICIPIO_ID"
                    Me.cboMunicipio.DataBind()
                End If
            Else
                Exit Sub
            End If

        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro Ao Carregar UF " & "')", True)
        End Try
    End Sub

    Public Sub CarregarUF()
        Try
            If IsNothing(Me.SourceUF) Then

                Dim ObjEO_IN As New SEGS8708_SPS_EO_IN

                Me.SourceUF = Me.taskSolicitante.CarregaUF(ObjEO_IN)

                If Not IsNothing(Me.SourceUF) Then
                    Me.cboEstado.DataSource = Me.SourceUF
                    Me.cboEstado.DataTextField = "NOME"
                    Me.cboEstado.DataValueField = "NOME"
                    Me.cboEstado.DataBind()
                End If

            Else
                Exit Sub
            End If

        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro Ao Carregar UF " & "')", True)
        End Try
    End Sub
    Protected Sub btnSearchbyCodAgencia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchbyCodAgencia.Click
        Try
            Dim objEO_IN As New SEGS8883_SPS_EO_IN
            Dim ObjEO_OUT As List(Of SEGS8883_SPS_EO_OUT)

            objEO_IN.Prefixo = Me.txtCodigoAgencia.Text
            ObjEO_OUT = taskUCAgencia_TK.Consultar_Agencia_by_codigo(objEO_IN)

            If ObjEO_OUT.Count > 0 Then
                Me.Carrega_Agencia_byCod(ObjEO_OUT.Item(0))
            Else
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Agência Não Encontrada." & "')", True)
                Exit Sub
            End If

        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro Ao Carregar UF " & "')", True)
        End Try

    End Sub

    Protected Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Try
            Dim objEO_IN As New SEGS8884_SPS_EO_IN
            Dim ObjEO_OUT As List(Of SEGS8884_SPS_EO_OUT)

            objEO_IN.Municipio_Id = Me.cboMunicipio.SelectedValue
            ObjEO_OUT = taskUCAgencia_TK.Consultar_Agencia_by_municipio(objEO_IN)

            If ObjEO_OUT.Count > 0 Then
                Me.GVwAgencia.DataSource = ObjEO_OUT
                Me.GVwAgencia.DataBind()
            Else
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Agência Não Encontrada." & "')", True)
                Exit Sub
            End If

        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro Ao Carregar UF " & "')", True)
        End Try
    End Sub

    Public Sub Carrega_Agencia(ByRef objOUT As SEGS8748_SPS_EO_OUT)
        Try
            Me.txtCodigoAgencia.Text = objOUT.PREFIXO
            Me.txtNomeAgenciaGrid.Text = objOUT.NOME
            Me.txtEnderecoGrid.Text = objOUT.ENDERECO
            Me.txtMunicipioGrid.Text = objOUT.MUNICIPIO
            Me.txtDVGrid.Text = objOUT.DV_PREFIXO
            Me.txtUFGrid.Text = objOUT.ESTADO
            Me.txtCodigoAgenciaGrid.Text = objOUT.PREFIXO
            Me.txtBairroGrid.Text = objOUT.BAIRRO


        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro Ao Carregar Agência " & "')", True)
        End Try
    End Sub

    Public Sub Carrega_Agencia_byCod(ByRef objOUT As SEGS8883_SPS_EO_OUT)
        Try
            Me.txtCodigoAgencia.Text = objOUT.PREFIXO
            Me.txtNomeAgenciaGrid.Text = objOUT.NOME
            Me.txtEnderecoGrid.Text = objOUT.ENDERECO
            Me.txtMunicipioGrid.Text = objOUT.MUNICIPIO
            Me.txtDVGrid.Text = objOUT.DV_PREFIXO
            Me.txtUFGrid.Text = objOUT.ESTADO
            Me.txtCodigoAgenciaGrid.Text = objOUT.PREFIXO
            Me.txtBairroGrid.Text = objOUT.BAIRRO


        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro Ao Carregar Agência " & "')", True)
        End Try
    End Sub

#End Region





End Class