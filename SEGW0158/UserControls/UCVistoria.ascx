﻿<%@ Control Language="vb"  AutoEventWireup="false" Codebehind="UCVistoria.ascx.vb"
    Inherits="SEGW0158.UCVistoria" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<fieldset style="width: 100%; height: 95%;">
    <legend class="TitCaixa"><span>Local de Risco</span></legend>
    <div id="Div" style="width: 95%; height: 200px; font-size: 8pt;">
        <div class="n750">
            <div class="n750 divLeft">
                <label for="txtNome" class="txtLabel divLeft n150">
                </label>
                <asp:CheckBox ID="chkIdentificado" CssClass="txtCampo divLeft n300" runat="server"
                    TextAlign="right" Text="Local de risco não identificado na apólice" />
            </div>
            <div class="n750 divLeft">
                <label for="txtNome" class="txtLabel divLeft n150">
                    Endereço:
                </label>
                <asp:TextBox ID="txtLCEndereco" CssClass="txtCampo divLeft n270" runat="server" MaxLength="60"></asp:TextBox>
            </div>
            <div class="n750 divLeft">
                <label for="txtNome" class="txtLabel divLeft n150">
                    Bairro:
                </label>
                <asp:TextBox ID="txtLCBairro" CssClass="txtCampo divLeft n100" runat="server" MaxLength="30"></asp:TextBox>
            </div>
            <div class="n750 divLeft">
                <label for="txtNome" class="txtLabel divLeft n150">
                    UF:
                </label>
                <asp:DropDownList ID="cboLCUF" CssClass="txtCampo divLeft n40" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </div>
            <div class="n750 divLeft">
                <label for="txtNome" class="txtLabel divLeft n150">
                    Município:
                </label>
                <asp:DropDownList ID="cboLCMunicipio" CssClass="txtCampo divLeft n250" runat="server">
                </asp:DropDownList>
            </div>
            <div class="n750 divLeft">
                <label for="txtNome" class="txtLabel divLeft n150">
                    CEP:
                </label>
                <asp:TextBox ID="txtLCCEP" runat="server" CssClass="txtCampo divLeft n70"></asp:TextBox>
            </div>
        </div>
    </div>
</fieldset>
<fieldset style="width: 100%; height: 250px;">
    <legend class="TitCaixa"><span>Dados da Vistoria</span></legend>
    <div id="Panel" style="font-size: 8pt; width: 95%;">
        <div class="n750">
            <div class="n750 divLeft">
                <label for="txtNome" class="txtLabel divLeft n150">
                    Nome de contato:
                </label>
                <asp:TextBox ID="txtVistNomeContato" runat="server" MaxLength="60" CssClass="txtCampo divLeft n400"></asp:TextBox>
            </div>
            <div class="n750 divLeft">
                <label for="txtNome" class="txtLabel divLeft n150">
                    Endereço:
                </label>
                <asp:TextBox ID="txtVistEndereco" runat="server" CssClass="txtCampo divLeft n250"
                    MaxLength="60"></asp:TextBox>
            </div>
            <div class="n750 divLeft">
                <label for="txtNome" class="txtLabel divLeft n150">
                    Bairro:
                </label>
                <asp:TextBox ID="txtVistBairro" runat="server" CssClass="txtCampo divLeft n100" MaxLength="30"></asp:TextBox>
            </div>
            <div class="n750 divLeft">
                <label for="txtNome" class="txtLabel divLeft n150">
                    UF:
                </label>
                <asp:DropDownList ID="cboVistUF" runat="server" CssClass="txtCampo divLeft n40" AutoPostBack="True">
                </asp:DropDownList>
            </div>
            <div class="n750 divLeft">
                <label for="txtNome" class="txtLabel divLeft n150">
                    Município:
                </label>
                <asp:DropDownList ID="cboVistMunicipio" CssClass="txtCampo divLeft n300" runat="server">
                </asp:DropDownList>
            </div>
            <div class="n750 divLeft">
                <label for="txtNome" class="txtLabel divLeft n150">
                    CEP:
                </label>
                <asp:TextBox ID="txtVistCEP" CssClass="txtCampo divLeft n70" runat="server"></asp:TextBox>
            </div>
            <div class="n750">
                <div class="n200 divLeft">
                    <label class="txtLabel divLeft n150" for="txtNome">
                        DDD:
                    </label>
                    <asp:TextBox ID="txtVistDDD1" CssClass="txtCampo divLeft n20" runat="server">
                    </asp:TextBox>
                    <cc1:MaskedEditExtender ID="maskDDD" runat="server" Mask="(99)" TargetControlID="txtVistDDD1"
                        CultureName="pt-BR" ClearMaskOnLostFocus="true">
                    </cc1:MaskedEditExtender>
                </div>
                <div class="n150 divLeft">
                    <label for="txtNome" class="txtLabel divLeft n50">
                        Telefone:
                    </label>
                    <asp:TextBox ID="txtVistTelefone1" CssClass="txtCampo divLeft n70" runat="server">
                    </asp:TextBox>
                    <cc1:MaskedEditExtender ID="meeTelefone" runat="server" Mask="9999-9999" TargetControlID="txtVistTelefone1"
                        CultureName="pt-BR" ClearMaskOnLostFocus="true">
                    </cc1:MaskedEditExtender>
                </div>
                <div class="n250     divLeft">
                    <label for="txtNome" class="txtLabel divLeft n100">
                        Tipo Telefone:
                    </label>
                    <asp:DropDownList ID="cboVistTpTelefone1" CssClass="txtCampo divLeft n100" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="n750">
                <div class="n200 divLeft">
                    <label for="txtNome" class="txtLabel divLeft n150">
                        DDD:
                    </label>
                    <asp:TextBox ID="txtVistDDD2" CssClass="txtCampo divLeft n20" runat="server">
                    </asp:TextBox>
                    <cc1:MaskedEditExtender ID="maskDDD2" runat="server" Mask="(99)" TargetControlID="txtVistDDD2"
                        CultureName="pt-BR" ClearMaskOnLostFocus="true">
                    </cc1:MaskedEditExtender>
                </div>
                <div class="n150 divLeft">
                    <label for="txtNome" class="txtLabel divLeft n50">
                        Telefone:
                    </label>
                    <asp:TextBox ID="txtVistTelefone2" CssClass="txtCampo divLeft n70" runat="server">
                    </asp:TextBox>&nbsp;
                    <cc1:MaskedEditExtender ID="meeTelefone2" runat="server" Mask="9999-9999" TargetControlID="txtVistTelefone2"
                        CultureName="pt-BR" ClearMaskOnLostFocus="true">
                    </cc1:MaskedEditExtender>
                </div>
                <div class="n250 divLeft">
                    <label for="txtNome" class="txtLabel divLeft n100">
                        Tipo Telefone:
                    </label>
                    <asp:DropDownList ID="cboVistTpTelefone2" CssClass="txtCampo divLeft n100"
                        runat="server">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</fieldset>
