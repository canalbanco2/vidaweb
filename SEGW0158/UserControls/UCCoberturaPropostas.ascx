﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCCoberturaPropostas.ascx.vb" Inherits="SEGW0158.UCCoberturaPropostas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<fieldset style="width:98%; height:auto">
    <legend ><span>Cobertura das Propostas</span></legend>
    <div id="Panel" style="width:95%; height:auto">
        <div class="n750">
            <div class="n750">
                <div class="n750 divLeft">
                    <label for="lblCliente" class="label">
                        Cliente:
                    </label>
                    <asp:Label ID="lblCliente" runat="server" Text="Label" CssClass="txt n595"></asp:Label>
                </div>
            </div>       
        </div> 
        <div class="n750">
            <div class="n750">
                <div class="n350 divLeft">
                    <label for="lblProposta" class="label">
                        Proposta:
                    </label>
                    <asp:Label ID="lblProposta" runat="server" Text="Label" CssClass="txt n300"></asp:Label>
                </div>
                <div class="n400 divLeft">
                    <label for="lblProduto" class="label">
                        Produto:
                    </label>
                    <asp:Label ID="lblProduto" runat="server" Text="Label" CssClass="txt n300"></asp:Label>
                </div>                
            </div>       
        </div>                      
        <div class="n750">
            <div class="n750">
                <div class="n750 divLeft">
                    <label for="lblEventoSinistro" class="label">
                        Evento Sinistro:
                    </label>
                    <asp:Label ID="lblEventoSinistro" runat="server" Text="Label" CssClass="txt n595"></asp:Label>
                </div>
            </div>       
        </div>                      
        <div class="n750">
            <div class="n750">
                <div class="n750 divLeft">
                    <label for="lblSubEvento" class="label">
                        SubEvento:
                    </label>
                    <asp:Label ID="lblSubEvento" runat="server" Text="Label" CssClass="txt n595"></asp:Label>
                </div>
            </div>       
        </div>                             
    </div>     
</fieldset>

    <asp:GridView id="GVwCoberturas" runat="server" CellPadding="0" AutoGenerateColumns="False" AllowPaging="True" ForeColor="#333333" Width="100%" >
    <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />    
    <Columns>
        <asp:BoundField DataField="cobertura" HeaderText="Cobertura">
            <HeaderStyle HorizontalAlign="Left" Font-Size="X-Small" VerticalAlign="Middle" />
            <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
            <FooterStyle HorizontalAlign="Left" VerticalAlign="Middle" />
        </asp:BoundField>
        <asp:BoundField DataField="Imp_Segur" HeaderText="Imp. Seguro" DataFormatString="{0:C6}" NullDisplayText="&quot;&quot;" >
            <HeaderStyle Font-Size="X-Small" HorizontalAlign="Right" VerticalAlign="Middle" />
            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" VerticalAlign="Middle" />
        </asp:BoundField>
        <asp:BoundField DataField="dt_ini_vig" HeaderText="Dt Ini Vig" DataFormatString="{0:D}" NullDisplayText="&quot;&quot;">
            <HeaderStyle HorizontalAlign="Left" Font-Size="X-Small" VerticalAlign="Middle" />
            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
        </asp:BoundField>
        <asp:BoundField DataField="dt_fim_vig" HeaderText="Dt Fim Vig" >
            <HeaderStyle Font-Size="X-Small" HorizontalAlign="Left" VerticalAlign="Middle" />
            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField HeaderText="Cobert.Atingida" DataField="Cobert_atingida" NullDisplayText="&quot;&quot;">
            <HeaderStyle HorizontalAlign="Left" Font-Size="X-Small" VerticalAlign="Middle" />
            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
        </asp:BoundField>
        <asp:BoundField DataField="utiliza_perc_subevento" HeaderText="Ut. % SubEvento" NullDisplayText="&quot;&quot;" >
            <HeaderStyle Font-Size="X-Small" HorizontalAlign="Left" VerticalAlign="Middle" />
            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
        </asp:BoundField>
    </Columns>
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" Font-Size="Large" />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333"  />
    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"  />
    <AlternatingRowStyle BackColor="White" ForeColor="#284775"  />
    <EmptyDataTemplate>
        Não existem coberturas vigentes.
    </EmptyDataTemplate>
        <EditRowStyle BackColor="#999999" />
</asp:GridView> 

