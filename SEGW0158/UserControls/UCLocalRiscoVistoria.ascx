﻿<%@ Control Language="vb"  AutoEventWireup="false" CodeBehind="UCLocalRiscoVistoria.ascx.vb" Inherits="SEGW0158.UCLocaRiscoVistoria" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<fieldset id="flsSolicitante" style="width: 784px; height: 210px; text-align: left">
    <legend><b><span style="font-size: 8pt; font-family: Verdana">Dados da Vistoria</span></b></legend>
    <div>
        <table>
            <tr>
                <td style="width: 2px; height: 22px; text-align: right" ></td>
                <td><strong></strong></td>
            </tr>        
            <tr>
                <td style="text-align: right" >
                    <asp:CheckBox ID="chkIdentificado" runat="server" TextAlign="Left"/></td>
                    <td><strong>Local de Risco não identificado na apólice </strong></td>
            </tr>
            <tr>
                <td style="width: 2px; height: 22px; text-align: right" ></td>
                <td><strong></strong></td>
            </tr>        
            <tr>
                <td style="width: 2px; height: 18px; text-align: right">
                    <span style="font-size: 8pt"><strong style="font-size: 8pt; width: 226px; height: 18px;
                        text-align: right">Endereço:</strong></span></td>
                <td style="width: 586px; height: 18px; text-align: left">
                    <asp:TextBox ID="txtVistoriaEndereco" runat="server" MaxLength="60" Width="249px"></asp:TextBox>
                    &nbsp;&nbsp; <span style="font-size: 8pt"><strong>Bairro: </strong>
                        <asp:TextBox ID="txtVistoriaBairro" runat="server" MaxLength="30" Width="103px"></asp:TextBox>&nbsp;
                    </span>
                </td>
            </tr>
            <tr>
                <td style="width: 2px; height: 24px; text-align: right">
                    <span style="font-size: 8pt; width: 226px; height: 18px; text-align: right"><strong>
                        UF:</strong><span style="font-size: 10pt">&nbsp;</span><span style="font-size: 8pt"><strong>
                        </strong></span></span>
                </td>
                <td style="font-size: 8pt; width: 586px; height: 24px; text-align: left" valign="middle">
                    <span style="font-size: 8pt"></span>
                    <asp:DropDownList ID="cboVistoriaUF" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    <strong>Município:</strong>&nbsp;<asp:DropDownList ID="cboVistoriaMunicipio" runat="server"
                        Width="295px">
                    </asp:DropDownList>
                    <span style="font-size: 8pt"></span>
                </td>
            </tr>
            <tr>
                <td style="width: 2px; height: 17px; text-align: right">
                    <strong><span style="font-size: 8pt; width: 226px; height: 18px; text-align: right">
                        CEP:</span></strong></td>
                <td style="width: 586px; height: 17px; text-align: left">
                    <strong><span style="font-size: 8pt">
                        <asp:TextBox ID="txtVistoriaCEP" runat="server" Width="103px"></asp:TextBox>&nbsp;
                        <cc1:maskededitextender id="meeCep" runat="server" clearmaskonlostfocus="true" culturename="pt-BR"
                            mask="99999-999" targetcontrolid="txtVistoriaCEP"></cc1:maskededitextender>
                    </span></strong>
                </td>
            </tr>
            <tr>
                <td style="width: 2px; height: 25px; text-align: right">
                    <strong><span style="font-size: 8pt; width: 226px; height: 18px; text-align: right">
                        DDD:</span></strong></td>
                <td style="width: 586px; height: 25px; text-align: left">
                    <strong><span style="font-size: 7pt; font-family: Verdana">&nbsp;<asp:TextBox ID="txtDDD1"
                        runat="server" Width="31px"></asp:TextBox>
                        Telefone:
                        <asp:TextBox ID="txtTelefone1" runat="server" Width="109px"></asp:TextBox>
                        Ramal:
                        <asp:TextBox ID="txtRamal1" runat="server" Width="40px"></asp:TextBox>
                        Tipo Telefone:
                        <asp:DropDownList ID="cboTipoTelefone1" runat="server">
                        </asp:DropDownList></span></strong></td>
            </tr>
            <tr>
                <td style="width: 2px; height: 25px; text-align: right">
                    <strong><span style="font-size: 8pt; width: 226px; height: 18px; text-align: right">
                        DDD:</span></strong></td>
                <td style="width: 586px; height: 25px; text-align: left"><span style="font-size: 7pt; font-family: Verdana">&nbsp;<asp:TextBox ID="txtDDD2"
                        runat="server" Width="31px"></asp:TextBox>
                    <strong>Telefone: </strong>
                    <asp:TextBox ID="txtTelefone2" runat="server" Width="109px"></asp:TextBox><strong> Ramal:
                    </strong>
                    <asp:TextBox ID="txtRamal2" runat="server" Width="40px"></asp:TextBox>
                    <strong>Tipo Telefone: </strong>
                    <asp:DropDownList ID="cboTipoTelefone2" runat="server">
                    </asp:DropDownList></span></td>
            </tr>

        </table>
    </div>
</fieldset>
<br />
<cc1:MaskedEditExtender ID="maskDDD1" runat="server" ClearMaskOnLostFocus="true"
    CultureName="pt-BR" Mask="(99)" TargetControlID="txtDDD1">
</cc1:MaskedEditExtender>
<cc1:MaskedEditExtender ID="meeTelefone1" runat="server" ClearMaskOnLostFocus="true"
    CultureName="pt-BR" Mask="9999-9999" TargetControlID="txtTelefone1">
</cc1:MaskedEditExtender>
<br />
<cc1:MaskedEditExtender ID="maskDDD2" runat="server" ClearMaskOnLostFocus="true"
    CultureName="pt-BR" Mask="(99)" TargetControlID="txtDDD2">
</cc1:MaskedEditExtender>
<cc1:MaskedEditExtender ID="meeTelefone2" runat="server" ClearMaskOnLostFocus="true"
    CultureName="pt-BR" Mask="9999-9999" TargetControlID="txtTelefone2">
</cc1:MaskedEditExtender>
