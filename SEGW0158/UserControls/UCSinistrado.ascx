﻿<%@ Control Language="vb"  AutoEventWireup="false" Codebehind="UCSinistrado.ascx.vb"
    Inherits="SEGW0158.UCSinistrado" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
&nbsp;&nbsp;<br />
<fieldset style="width: 100%; height: 95%;">
    <legend class="TitCaixa"><span>Agência de Contato</span></legend>
    <div id="Panel_Pesquisa1" style="width: 95%; height: 95%; font-size: 8pt;">
        <div class="n750">
            <div class="n250 divLeft">
                <label class="txtLabel divLeft n150" for="txtDV">
                    Código da Agência:
                </label>
                <asp:TextBox ID="txtCodigoAgenciaVw" CssClass="txtCampo divLeft n30" runat="server"
                    MaxLength="4" ReadOnly="true">
                </asp:TextBox>
            </div>
            <div class="n200 divLeft">
                <label class="txtLabel divLeft n150" for="txtDV">
                    DV:
                </label>
                <asp:TextBox ID="txtDV" runat="server"   CssClass="txtCampo divLeft n7" MaxLength="1">
                </asp:TextBox>
            </div>
            <div class="n300 divLeft">
                <label class="txtLabel divLeft n50" for="txtNomeAgencia">
                    Nome:
                </label>
                <asp:TextBox ID="txtNomeAgencia" runat="server" CssClass="txtCampo divLeft n200"
                    ReadOnly="true">
                </asp:TextBox>
            </div>
        </div>
        <div class="n750">
            <div class="450 divLeft">
                <label class="txtLabel divLeft n150" for="txtEndereco">
                    Endereço:
                </label>
                <asp:TextBox ID="txtEndereco" runat="server" CssClass="txtCampo divLeft n260" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="n300 divLeft">
                <label class="txtLabel divLeft n80" for="txtBairro">
                    Bairro:
                </label>
                <asp:TextBox ID="txtBairro" runat="server" CssClass="txtCampo divLeft n100" ReadOnly="true"></asp:TextBox>
            </div>
        </div>
        <div class="n750">
            <div class="n450 divLeft">
                <label class="txtLabel divLeft n150" for="txtCidade">
                    Cidade:
                </label>
                <asp:TextBox ID="txtCidade" runat="server" CssClass="txtCampo divLeft n260" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="n300 divLeft">
                <label class="txtLabel divLeft n50"  for="txtUF">
                    UF:
                </label>
                <asp:TextBox ID="txtUF" runat="server" CssClass="txtCampo divLeft n20" ReadOnly="true"></asp:TextBox>
            </div>
        </div>
    </div>
</fieldset>
