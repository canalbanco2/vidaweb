﻿Imports SEGL0343.Task
Imports SEGL0343.EO

Partial Public Class UCDadosAgencia
    Inherits System.Web.UI.UserControl
    Private objSourceAgenciaMunicioId As List(Of SEGS8700_SPS_EO_OUT)
    Private objSourceCodigoAgencia As List(Of SEGS8704_SPS_EO_OUT)

#Region "Propriedades"

    Public Property CodigoAgencia() As String
        Get
            Return txtCodigoAgencia.Text
        End Get
        Set(ByVal value As String)
            txtCodigoAgencia.Text = value
        End Set
    End Property

    Public Property UF() As String
        Get
            Return ddlUF.SelectedValue
        End Get
        Set(ByVal value As String)
            ddlUF.SelectedValue = value
        End Set
    End Property

    Public Property Cidade() As Integer
        Get
            Return IIf(ddlCidade.SelectedValue = "", "1", ddlCidade.SelectedValue)

        End Get
        Set(ByVal value As Integer)
            ddlCidade.SelectedValue = value
        End Set
    End Property

    Public Property Cidade_Nome() As String
        Get
            Return txtCidade.Text
        End Get
        Set(ByVal value As String)
            txtCidade.Text = value
        End Set
    End Property

    Public Property NomeAgencia() As String
        Get
            Return txtNomeAgencia.Text
        End Get
        Set(ByVal value As String)
            txtNomeAgencia.Text = value
        End Set
    End Property

    Public Property CodigoAgenciaVw() As String
        Get
            Return txtCodigoAgenciaVw.Text
        End Get
        Set(ByVal value As String)
            txtCodigoAgenciaVw.Text = value
        End Set
    End Property


    Public Property DV() As String
        Get
            Return txtDV.Text
        End Get
        Set(ByVal value As String)
            txtDV.Text = value
        End Set
    End Property


    Public Property Endereco() As String
        Get
            Return txtEndereco.Text
        End Get
        Set(ByVal value As String)
            txtEndereco.Text = value
        End Set
    End Property

    Public Property Bairro() As String
        Get
            Return txtBairro.Text
        End Get
        Set(ByVal value As String)
            txtBairro.Text = value
        End Set
    End Property


    Public Property UFtxt() As String
        Get
            Return txtUF.Text
        End Get
        Set(ByVal value As String)
            txtUF.Text = value
        End Set
    End Property

    'Public Property BairroReadOnly() As Boolean
    '    Get
    '        Return txtBairro.ReadOnly
    '    End Get
    '    Set(ByVal value As Boolean)
    '        txtBairro.ReadOnly = value
    '    End Set
    'End Property

#End Region

#Region "Métodos"

    Private Sub PreencheComboUF()

        Dim Obj_TK As New UCAgencia_TK()
        ddlUF.DataSource = Obj_TK.ObtemListaUF()
        ddlUF.DataValueField = "SiglaUnidadeFederacao"
        ddlUF.DataTextField = "SiglaUnidadeFederacao"
        ddlUF.DataBind()

        ddlUF.Items.Insert(0, New ListItem("Escolha o estado", ""))

    End Sub

    Private Sub PreencheComboMunicipio()
        Dim Obj_TK As New UCAgencia_TK()
        Dim Obj_EO_IN As New SEL_MUNICIPIO_SPS_EO_IN()

        With Obj_EO_IN

            .Estado = Me.UF
            .Municipio = String.Empty
            .Municipio_Id = String.Empty
        End With

        With ddlCidade
            .DataSource = Obj_TK.ObtemListaMunicipio(Obj_EO_IN)
            .DataTextField = "nome"
            .DataValueField = "municipio_id"
            .DataBind()

            .Items.Insert(0, New ListItem("Escolha o município", ""))

        End With


    End Sub

    Public Sub PreencheComboMunicipio(ByVal UF As String)

        Dim Obj_TK As New UCAgencia_TK()
        Dim Obj_EO_IN As New SEL_MUNICIPIO_SPS_EO_IN()

        With Obj_EO_IN

            .Estado = Me.UF
            .Municipio = String.Empty
            .Municipio_Id = String.Empty

        End With

        With ddlCidade

            .DataSource = Obj_TK.ObtemListaMunicipio(Obj_EO_IN)
            .DataTextField = "nome"
            .DataValueField = "municipio_id"
            .DataBind()

            .Items.Insert(0, New ListItem("Escolha o município", ""))

        End With

    End Sub

    Private Function ValidarCampos(ByVal parTipoPesquisa As String) As Boolean

        ValidarCampos = True

        Select Case parTipoPesquisa
            Case "CodigoAgencia"

                If IsNumeric(txtCodigoAgencia.Text.Trim) = False Then

                    Return False

                End If

            Case "EstadoMunicipio"

                If ddlCidade.SelectedValue.Trim = "" Or ddlUF.SelectedValue.Trim = "" Then

                    Return False

                End If

        End Select


    End Function

    Private Sub GenerateGridSinistrados(ByVal reNew As Boolean, ByVal strTipoPesquisa As String)

        Dim objTask As New UCAgencia_TK()
        If reNew Then

            Select Case strTipoPesquisa

                Case "CodigoAgencia"

                    objSourceCodigoAgencia = objTask.ConsultarAgencias(GetFilterCodigoAgencia(CStr(CInt(txtCodigoAgencia.Text))))

                    ViewState("GVwAgencia") = objSourceCodigoAgencia

                Case "EstadoMunicipio"

                    Dim intTeste As Decimal = ddlCidade.SelectedValue

                    objSourceAgenciaMunicioId = objTask.ConsultarAgenciasIdPrefixo(GetFilterIdMunicipio(ddlCidade.SelectedValue, ddlUF.SelectedValue))

                    ViewState("GVwAgencia") = objSourceAgenciaMunicioId

            End Select


        Else

            Select Case strTipoPesquisa

                Case "CodigoAgencia"

                    objSourceCodigoAgencia = CType(ViewState("GVwAgencia"), List(Of SEGS8704_SPS_EO_OUT))


                Case "EstadoMunicipio"

                    objSourceAgenciaMunicioId = CType(ViewState("GVwAgencia"), List(Of SEGS8700_SPS_EO_OUT))

            End Select


        End If

        With GVwAgencia
            Select Case strTipoPesquisa
                Case "CodigoAgencia"
                    .DataSource = objSourceCodigoAgencia
                Case "EstadoMunicipio"
                    .DataSource = objSourceAgenciaMunicioId
            End Select

            .DataBind()
            If GVwAgencia.Rows.Count = 1 Then
                .SelectedIndex = 0
                Dim row As GridViewRow = GVwAgencia.SelectedRow

                With row
                    txtCodigoAgenciaVw.Text = .Cells(1).Text
                    txtDV.Text = .Cells(2).Text
                    txtNomeAgencia.Text = .Cells(3).Text
                    txtEndereco.Text = .Cells(4).Text
                    txtBairro.Text = .Cells(5).Text
                    txtCidade.Text = .Cells(6).Text
                    txtUF.Text = .Cells(7).Text
                End With
                .Visible = False
            Else
                .SelectedIndex = -1
                .Visible = True
            End If
            'btnContinuar.Visible = .Rows.Count > 0
        End With
    End Sub

    Private Sub GenerateGridSinistrados2(ByVal reNew As Boolean)
        Dim objTask As New UCAgencia_TK()

        If reNew Then

            'Dim intTeste As Decimal = ddlCidade.SelectedValue

            objSourceAgenciaMunicioId = objTask.ConsultarAgenciasIdPrefixo(GetFilterIdMunicipio(ddlCidade.SelectedValue, ddlUF.SelectedValue))

            ViewState("GVwAgencia") = objSourceAgenciaMunicioId

            GVwAgencia.DataSource = objSourceCodigoAgencia

            GVwAgencia.DataBind()
            GVwAgencia.SelectedIndex = -1

        Else

            objSourceAgenciaMunicioId = CType(ViewState("GVwAgencia"), List(Of SEGS8700_SPS_EO_OUT))

        End If






    End Sub

    Private Function GetFilterIdMunicipio(ByVal parMunicipoId As Integer, ByVal parEstado As String) As SEGS8700_SPS_EO_IN
        Try

            Dim objAgenciaPorIdMunicipio As New SEGS8700_SPS_EO_IN()

            With objAgenciaPorIdMunicipio

                .Banco_Id = 1

                .Municipio_Id = parMunicipoId

                .Estado = parEstado


            End With

            Return objAgenciaPorIdMunicipio


        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function GetFilterCodigoAgencia(ByVal parPrefixo As String) As SEGS8704_SPS_EO_IN
        Try

            Dim objAgenciaPorCodigoAgencia As New SEGS8704_SPS_EO_IN()

            With objAgenciaPorCodigoAgencia

                .Banco_Id = 1
                .Valor_Pesquisa = parPrefixo

            End With

            Return objAgenciaPorCodigoAgencia


        Catch ex As Exception
            Throw
        End Try
    End Function
#End Region

#Region "Eventos"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            PreencheComboUF()
            Me.txtCodigoAgencia.Focus()
        End If

    End Sub


    Protected Sub ddlUF_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlUF.SelectedIndexChanged

        If ddlUF.SelectedValue <> String.Empty Then
            PreencheComboMunicipio(ddlUF.Text)
        End If


    End Sub

    Protected Sub ibtPesquisarCodigoAgencia_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtPesquisarCodigoAgencia.Click

        hddPesquisa.Value = "CodigoAgencia"

        If ValidarCampos(hddPesquisa.Value) Then

            GenerateGridSinistrados(True, hddPesquisa.Value)

        Else

            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert(' Informe o Código da Agência! ')", True)
            txtCodigoAgencia.Text = ""
            txtCodigoAgencia.Focus()

        End If


    End Sub

    Protected Sub ibtPesquisaEstadoMunicipio_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtPesquisaEstadoMunicipio.Click

        'GenerateGridSinistrados2(True)
        hddPesquisa.Value = "EstadoMunicipio"

        If ValidarCampos(hddPesquisa.Value) Then

            GenerateGridSinistrados(True, hddPesquisa.Value)

        Else

            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Informe o estado e município para Pesquisa! ')", True)
            ddlUF.SelectedValue = ""
            ddlCidade.SelectedValue = ""
            ddlUF.Focus()

        End If



    End Sub

    Private Sub GVwAgencia_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVwAgencia.PageIndexChanging

        GVwAgencia.PageIndex = e.NewPageIndex

        GenerateGridSinistrados(False, hddPesquisa.Value)

    End Sub

    Protected Sub GVwAgencia_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GVwAgencia.SelectedIndexChanged

        Dim row As GridViewRow = GVwAgencia.SelectedRow

        With row

            txtCodigoAgenciaVw.Text = .Cells(1).Text
            txtDV.Text = .Cells(2).Text
            txtNomeAgencia.Text = .Cells(3).Text
            txtEndereco.Text = .Cells(4).Text
            txtBairro.Text = .Cells(5).Text
            txtCidade.Text = .Cells(6).Text
            txtUF.Text = .Cells(7).Text


        End With



    End Sub

#End Region



End Class