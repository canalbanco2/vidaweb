﻿<%@ Control Language="vb"   AutoEventWireup="false" Codebehind="UCTelefoneSolicitante.ascx.vb"
    Inherits="SEGW0158.UCTelefoneSolicitante" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<div class="n750">  
    <div class="n200 divLeft">
    <br />
        <label for="txtDDD"  class="txtLabel divLeft n100">
            &nbsp;DDD:
        </label>
        <asp:TextBox ID="txtDDD" runat="server" CssClass="txtCampo divLeft n20" MaxLength="2"
                        TextMode="SingleLine" TabIndex="99" ValidationGroup="val"></asp:TextBox>
    </div>
    <br />
    <div class="n200 divLeft">
        <label for="txtTelefone"   class="txtLabel divLeft n50">
            Telefone:
        </label>
        <asp:TextBox ID="txtTelefone" runat="server" CssClass="txtCampo divLeft n70"
                    TextMode="SingleLine"  TabIndex="100" ValidationGroup="val"></asp:TextBox>
    </div>
    <div class="n130 divLeft">
        <label for="txtRamal"  class="txtLabel divLeft n40" >
            Ramal:
        </label>
        <asp:TextBox    ID="txtRamal" runat="server" CssClass="txtCampo divLeft n32"  
                        TextMode="SingleLine"  TabIndex="101"></asp:TextBox>
    </div>
     <div class="n200 divLeft">
        <label for="ddlTipoTelefone"  class="txtLabel divLeft n50">
            Tipo:
        </label>
        <asp:DropDownList ID="ddlTipoTelefone" runat="server" CssClass="txtCampo divLeft n100" TabIndex="102" ValidationGroup="val" Width="124px">
            <asp:ListItem Value="0">Selecione</asp:ListItem>
            <asp:ListItem Value="1">Residencial</asp:ListItem>
            <asp:ListItem Value="2">Comercial</asp:ListItem>
            <asp:ListItem Value="3">Celular</asp:ListItem>
            <asp:ListItem Value="4">Fax</asp:ListItem>
            <asp:ListItem Value="5">Recado</asp:ListItem>            
        </asp:DropDownList>                                                           
         <cc1:MaskedEditExtender ID="mskDDD" runat="server" Mask="99" MaskType="Number" TargetControlID="txtDDD">
         </cc1:MaskedEditExtender>
     </div>   
</div>  
<cc1:MaskedEditExtender ID="mskFone" runat="server" ClearTextOnInvalid="True" Mask="9999-9999"
    MaskType="Number" TargetControlID="txtTelefone">
</cc1:MaskedEditExtender>
<cc1:MaskedEditExtender ID="mskRamal" runat="server" Mask="9999" MaskType="Number" TargetControlID="txtRamal" AutoComplete="false" >
</cc1:MaskedEditExtender>
