﻿Imports SEGL0343.Task
Imports SEGL0343.EO

Partial Public Class UCOcorrenciaResumo
    Inherits System.Web.UI.UserControl

#Region "Propriedades"

    Public Property DataOcorrencia() As String
        Get
            Return Me.txtDataOcorrencia.Text
        End Get
        Set(ByVal value As String)
            Me.txtDataOcorrencia.Text = value
        End Set
    End Property
    Public Property DataOcorrenciaReadOnly() As Boolean
        Get
            Return Me.txtDataOcorrencia.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtDataOcorrencia.ReadOnly = value
        End Set
    End Property
    Public Property HoraOcorrencia() As String
        Get
            Return Me.txtHoraOcorrencia.Text
        End Get
        Set(ByVal value As String)
            Me.txtHoraOcorrencia.Text = value
        End Set
    End Property
    Public Property HoraOcorrenciaReadOnly() As Boolean
        Get
            Return Me.txtHoraOcorrencia.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtHoraOcorrencia.ReadOnly = value
        End Set
    End Property
    Public Property lblSinitro_text() As String
        Get
            Return Me.lblSinistro.Text
        End Get
        Set(ByVal value As String)
            Me.lblSinistro.Text = value
        End Set
    End Property
    Public Property lblSinitro_visible() As Boolean
        Get
            Return Me.lblSinistro.Visible
        End Get
        Set(ByVal value As Boolean)
            Me.lblSinistro.Visible = value
        End Set
    End Property

    Public Property Reanalise() As String
        Get
            Return Me.txtReanalise.Text
        End Get
        Set(ByVal value As String)
            Me.txtReanalise.Text = value
        End Set
    End Property
    Public Property ReanaliseReadOnly() As Boolean
        Get
            Return Me.txtReanalise.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtReanalise.ReadOnly = value
        End Set
    End Property
    Public Property ReanaliseVisible() As Boolean
        Get
            Return Me.txtReanalise.Visible
        End Get
        Set(ByVal value As Boolean)
            Me.txtReanalise.Visible = value
        End Set
    End Property
    Public Property Evento() As String
        Get
            Return Me.ddlEvento.SelectedValue
        End Get
        Set(ByVal value As String)
            Me.ddlEvento.SelectedValue = value
        End Set
    End Property

    Public Property EventoEnabled() As Boolean
        Get
            Return Me.ddlEvento.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me.ddlEvento.Enabled = value
        End Set
    End Property

    Public Property SubEvento() As String
        Get
            Return Me.ddlSubEvento.SelectedValue
        End Get
        Set(ByVal value As String)
            Me.ddlSubEvento.SelectedValue = value
        End Set
    End Property

    Public Property SubEventoEnabled() As Boolean
        Get
            Return Me.ddlSubEvento.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me.ddlSubEvento.Enabled = value
        End Set
    End Property

    Public Property Observacao() As String
        Get
            Return Me.Observacoes.Observacao
        End Get
        Set(ByVal value As String)
            Me.Observacoes.Observacao = value
        End Set
    End Property

    Public Property ObservacaoReadOnly() As Boolean
        Get
            Return Me.Observacoes.ObservacaoReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.Observacoes.ObservacaoReadOnly = value
        End Set
    End Property

#End Region
#Region "Métodos"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            CarregaComboEventos()
            'Me.txtDataOcorrencia.Attributes.Add("onblur", "javascript:ValidaData(this);")
        End If

    End Sub

#End Region
#Region "Evento"

    Public Sub CarregaComboEventos()

        Dim TaskEvento As New SEGW0158ConsultaEventos
        Dim EO_IN As New SEGS8714_SPS_EO_IN

        EO_IN.Tipo_Ramo = 2

        Me.ddlEvento.DataSource = TaskEvento.ConsultarEventos(EO_IN)
        Me.ddlEvento.DataTextField = "NOME"
        Me.ddlEvento.DataValueField = "EVENTO_SINISTRO_ID"
        Me.ddlEvento.DataBind()

        ddlEvento.Items.Insert(0, New ListItem("Escolha um Evento", ""))
        'ddlEvento.Items.Insert(ddlEvento.Items.Count, New ListItem("CAUSA GENÉRICA PARA SINISTRO", ""))


    End Sub

    Public Sub CarregaSubEventos(ByVal intEvento As Integer)

        Dim objTK As New UCPessoasSinistro_TK()

        Dim Obj_EO_IN As New SEGS8695_SPS_EO_IN()

        With ddlSubEvento

            .Items.Clear()

            If (ddlEvento.SelectedValue <> "") Or ddlEvento.Enabled = False Then

                Dim lstSubEvento As List(Of SEGS8695_SPS_EO_OUT) = objTK.CarregaSubEventos(intEvento)

                If lstSubEvento.Count > 0 Then

                    ddlSubEvento.Enabled = True

                    .DataSource = lstSubEvento
                    .DataValueField = "subevento_sinistro_id"
                    .DataTextField = "nome"
                    .DataBind()

                    .Items.Insert(0, New ListItem("Escolha um Evento", ""))

                Else

                    .Items.Clear()
                    ddlSubEvento.Enabled = False

                End If


            End If

        End With

    End Sub

#End Region

End Class