﻿Public Partial Class UCAvisos
    Inherits System.Web.UI.UserControl

    Private lSinistro_id As String
    Private pObj_out As List(Of SEGL0343.EO.SEGS8915_SPS_EO_OUT)

    Public Property Propostas() As List(Of SEGL0343.EO.SEGS8915_SPS_EO_OUT)
        Get
            Return pObj_out
        End Get
        Set(ByVal value As List(Of SEGL0343.EO.SEGS8915_SPS_EO_OUT))
            pObj_out = value
        End Set
    End Property

    Public Property Sinistro_Id() As String
        Get
            Return lSinistro_id
        End Get
        Set(ByVal value As String)
            lSinistro_id = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Sub load_UC()
        Dim o As List(Of SEGL0343.EO.SEGS8899_SPS_EO_OUT)
        Dim b As New SEGL0343.Business.SEGS8899_SPS_Business
        Dim e As New SEGL0343.EO.SEGS8899_SPS_EO_IN

        If lSinistro_id > 0 Then
            e.Sinistro_Id = lSinistro_id
            o = b.seleciona(e)
            If o.Count > 0 Then
                Me.grdOutrosSeguros.DataSource = o
                Me.grdOutrosSeguros.DataBind()
            End If
        End If

        Dim Obj_out As List(Of SEGL0343.EO.SEGS8915_SPS_EO_OUT)

        Dim Obj_bss As New SEGL0343.Business.SEGS8915_SPS_Business
        Dim Obj_in As New SEGL0343.EO.SEGS8915_SPS_EO_IN

        If lSinistro_id > 0 Then
            Obj_in.Sinistro_Id_Passos = Me.Sinistro_Id
            Obj_out = Obj_bss.seleciona(Obj_in)
            Propostas = Obj_out
            If Obj_out.Count > 0 Then
                Me.grdPropostas.DataSource = Obj_out
                Me.grdPropostas.DataBind()
            End If
        End If


    End Sub


End Class