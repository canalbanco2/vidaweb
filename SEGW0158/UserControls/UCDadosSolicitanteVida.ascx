﻿<%@ Control Language="vb"  AutoEventWireup="false" Codebehind="UCDadosSolicitanteVida.ascx.vb"
    Inherits="SEGW0158.UCDadosSolicitanteVida" %>
<%@ Register Src="~/UserControls/UCTelefoneSolicitante.ascx" TagName="TelsSolicitante"
    TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<fieldset style="width: auto; height: auto; font-size: 8pt;">
    <legend class="TitCaixa"><span>Dados do Solicitante</span></legend>
    <div id="Panel" style="width: 95%; height: auto; font-size: 8pt;">
        <div class="n750">
            <div class="n750">
                <div class="n550 divLeft">
                    <label for="txtNome" class="txtLabel divLeft n100">
                        Nome:
                    </label>
                    <asp:TextBox ID="txtNome" runat="server" CssClass="txtCampo divLeft n400" MaxLength="60"
                        ValidationGroup="val" TabIndex="1"></asp:TextBox>&nbsp;
                </div>
                <div class="n200 divLeft">
                    <div class="BotaoFora" runat="server" id="btnSegurado_fora" visible="false" style="text-align: center; width: 160px; float: left;">
                        <div class="BotaoE">
                        </div>
                        <asp:Button ID="btnSegurado" CssClass="BotaoM n120" runat="server" Text="Segurado" />
                        <div class="BotaoD">
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="n750">
                <div class="n750 divLeft">
                    <label class="txtLabel divLeft n100" for="txtCEP">
                        CEP:
                    </label>
                    <asp:TextBox ID="txtCEP" runat="server" CssClass="txtCampo divLeft n60" TabIndex="2"
                        ValidationGroup="val"  ></asp:TextBox>
                    <asp:ImageButton ToolTip="Pesquisar" ID="ibtPesquisarCEP" runat="server" CausesValidation="false"
                        ImageUrl="~/image/Search.gif" TabIndex="3" />
                </div>
            </div>
            <div class="n750">
                <div class="n750 divLeft">
                    <label for="txtEndereco" class="txtLabel divLeft n100">
                        Endereço:
                    </label>
                    <asp:TextBox ID="txtEndereco" runat="server" CssClass="txtCampo divLeft n650" MaxLength="60"
                        ValidationGroup="val" TabIndex="4"></asp:TextBox>&nbsp;
                </div>
            </div>
            <div class="n750">
                <div class="n300 divLeft">
                    <label for="txtBairro" class="txtLabel divLeft n100">
                        Bairro:
                    </label>
                    <asp:TextBox ID="txtBairro" runat="server" CssClass="txtCampo divLeft n150" MaxLength="30"
                        TabIndex="5" ValidationGroup="val"></asp:TextBox>&nbsp;
                </div>
                <div class="n200 divLeft">
                    <label for="ddlUF" class="txtLabel divLeft n100">
                        UF:
                    </label>
                    <asp:DropDownList ID="ddlUF" runat="server" AutoPostBack="true" CssClass="txtCampo divLeft n40"
                        TabIndex="6" ValidationGroup="val">
                    </asp:DropDownList>
                </div>
                <div class="n250 divLeft">
                    <label for="txtCidade" class="txtLabel divLeft n60">
                        Cidade:
                    </label>
                    <asp:DropDownList ID="ddlCidade" runat="server" CssClass="txtCampo divLeft n150"
                        TabIndex="7" ValidationGroup="val">
                    </asp:DropDownList>
                    <cc1:MaskedEditExtender ID="mskCEP" runat="server" Mask="99999-999" MaskType="Number" AutoComplete="false"
                        TargetControlID="txtCEP">
                    </cc1:MaskedEditExtender>
                </div>
            </div>
        </div>
</fieldset>
<br />
<fieldset style="width: auto; height: auto; font-size: 8pt;">
    <legend><span></span></legend>
    <div id="Panel_Telefone" style="width: 95%; height: 150px; font-size: 8pt;">
        <uc2:TelsSolicitante ID="TelSolicitante1" runat="server"></uc2:TelsSolicitante>
        <uc2:TelsSolicitante ID="TelSolicitante2" runat="server"></uc2:TelsSolicitante>
        <uc2:TelsSolicitante ID="TelSolicitante3" runat="server"></uc2:TelsSolicitante>
        <uc2:TelsSolicitante ID="TelSolicitante4" runat="server"></uc2:TelsSolicitante>
        <uc2:TelsSolicitante ID="TelSolicitante5" runat="server"></uc2:TelsSolicitante>
    </div>
</fieldset>
<br />
<fieldset style="width: auto; height: auto; font-size: 8pt;">
    <div id="Panel_Email_Parenstesco" style="width: 95%; height: 80px; font-size: 8pt;">
        <br />
        <div class="n750">
            <div class="n750 divLeft">
                <label for="txtEmail" class="txtLabel divLeft n110">
                    E-Mail:
                </label>
                <asp:TextBox ID="txtEmail" runat="server" CssClass="txtCampo divLeft n650" MaxLength="60"
                    TabIndex="111" ValidationGroup="val" ></asp:TextBox>&nbsp;
            </div>
        </div>
        <div class="n750">
            <div class="n750 divLeft" visible="false">
                <asp:DropDownList ID="ddlGrauParentesco" runat="server"  visible="false" AutoPostBack="true" CssClass="txtCampo  n150"
                    TabIndex="112" ValidationGroup="val">
                </asp:DropDownList>
            </div>
        </div>
    </div>
</fieldset>
