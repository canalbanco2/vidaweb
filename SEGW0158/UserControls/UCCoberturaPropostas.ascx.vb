﻿Imports SEGL0343.Task
Imports SEGL0343.Util
Imports SEGL0343.EO

Partial Public Class UCCoberturaPropostas
    Inherits System.Web.UI.UserControl

#Region "Propriedades"

    Public Property NomeCliente() As String
        Get
            Return lblCliente.Text
        End Get
        Set(ByVal value As String)
            lblCliente.Text = value
        End Set
    End Property

    Public Property Proposta() As String
        Get

            Return lblProposta.Text

        End Get
        Set(ByVal value As String)

            lblProposta.Text = value

        End Set

    End Property

    Public Property Produto() As String
        Get

            Return lblProduto.Text

        End Get
        Set(ByVal value As String)

            lblProduto.Text = value

        End Set

    End Property

    Public Property EventoSinistro() As String
        Get

            Return lblEventoSinistro.Text

        End Get
        Set(ByVal value As String)

            lblEventoSinistro.Text = value

        End Set

    End Property

    Public Property SubEvento() As String
        Get

            Return lblSubEvento.Text

        End Get
        Set(ByVal value As String)

            lblSubEvento.Text = value

        End Set

    End Property
#End Region

#Region "Eventos"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



    End Sub

    Public Sub PopulaGridCoberturas(ByVal dtOcorrencia As DateTime, ByVal proposta_id As Decimal, ByVal sub_grupo As Integer, ByVal Tp_Componente_Id As Integer, ByVal Evento_sinistro_id As Integer, ByVal SubEvento_sinistro_id As Integer)

        Dim objTK As New SEGW157AnaliseSinistroCA
        Dim objIn_consulta As New consultar_coberturas_sps_EO_IN
        Dim objOutLista As List(Of consultar_coberturas_sps_EO_OUT)


        'dtOcorrencia = dtOcorrencia.ToString("yyyyMMdd")

        'objIn_consulta.Cod_Objeto_Segurado = 
        objIn_consulta.Dt_Ocorrencia = dtOcorrencia.ToString("yyyyMMdd")
        'objIn_consulta.Numero_Erro = 
        objIn_consulta.Proposta_Id = proposta_id
        If sub_grupo > 0 Then
            objIn_consulta.Sub_Grupo_Id = sub_grupo
        Else
            objIn_consulta.Sub_Grupo_Id = Nothing
        End If
        objIn_consulta.Tipo_Ramo = 1
        objIn_consulta.Tp_Componente_Id = Tp_Componente_Id

        objOutLista = objTK.SelecionarCoberturas(objIn_consulta)

        Dim obj_TKestimativa As New SEGW157AnaliseSinistroCA
        Dim lsCoberturas As New List(Of cobertura)

        For index As Integer = 0 To objOutLista.Count - 1

            Dim uCober As New cobertura

            uCober.Cobertura = objOutLista(index).tp_cobertura_id.ToString + " - " + objOutLista(index).nome_cobertura
            uCober.Imp_segur = objOutLista(index).val_is

            Dim estim_cobertura As estimativa = obj_TKestimativa.getEstimativa(proposta_id, objOutLista(index).tp_cobertura_id, Evento_sinistro_id, SubEvento_sinistro_id, Tp_Componente_Id, dtOcorrencia)

            uCober.Cobert_atingida = IIf(estim_cobertura.Atinge = "true", "Sim", "Não")
            uCober.Dt_ini_vig = IIf(Not IsNothing(objOutLista(index).dt_inicio_vigencia), objOutLista(index).dt_inicio_vigencia, "Em vigência")
            uCober.Dt_fim_vig = IIf(Not IsNothing(objOutLista(index).dt_fim_vigencia), objOutLista(index).dt_fim_vigencia, "")

            uCober.Utiliza_perc_subevento = IIf(estim_cobertura.Utiliza_Percentual_Subevento = "S", "Sim", "Não")

            lsCoberturas.Add(uCober)

        Next

        If Not IsNothing(objOutLista) Then
            GVwCoberturas.DataSource = lsCoberturas
            GVwCoberturas.DataBind()
        End If
    End Sub

#End Region

    Protected Sub GVwCoberturas_RowDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVwCoberturas.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells(3).Text = "00:00:00" Then
                e.Row.Cells(3).Text = ""
            End If

            e.Row.Cells(2).Text = IIf(e.Row.Cells(2).Text <> "00:00:00", e.Row.Cells(2).Text, "Em vigência")

        End If
    End Sub
End Class