﻿Imports SEGL0343.Task
Imports SEGL0343.EO
Imports SEGL0343.Business

Partial Public Class SEGW0158AnaliseSinistroCA
    Inherits BasePages

    Dim ObjGeral As New SEGW0158Geral_TK
    Dim Sinistro As New SEGL0343.GeraSinistroRE_Business

#Region "Propriedades"

    Dim blnNaoAvisar As Boolean
    Dim nomeSin As String
    Dim nomeSeg As String
    Dim objIn_8810 As New SEGS8810_SPS_EO_IN
    Dim objOut_8810 As New List(Of SEGS8810_SPS_EO_OUT)
    Dim objGeral8810 As New SEGW0157GridResumo_TK
    Dim objIn_8863 As New SEGS8863_SPD_EO_IN
    Public NaoPodeAvisar As Boolean
    Public bSemProposta As Boolean
    Dim Cliente_ID As Long
    Dim GeraSinistroRE As New SEGL0343.GeraSinistroRE_Business

    Public Property cProdutoBBProtecao() As String
        Get
            Return CType(ViewState("cProdutoBBProtecao"), String)
        End Get
        Set(ByVal value As String)
            ViewState("cProdutoBBProtecao") = value
        End Set
    End Property

    Public Property NomeSinistrado() As String
        Get
            Return nomeSin
        End Get
        Set(ByVal value As String)
            nomeSin = value
        End Set
    End Property

    Public Property NomeSegurado() As String
        Get
            Return nomeSeg
        End Get
        Set(ByVal value As String)
            nomeSeg = value
        End Set
    End Property
    Public ReadOnly Property Fluxo() As String
        Get
            If Not IsNothing(Request.QueryString("fluxo")) Then
                Return Request.QueryString("fluxo")
            Else
                Return "0"
            End If
        End Get
    End Property

    Public Property _NaoAvisar() As Boolean
        Get
            Return blnNaoAvisar
        End Get
        Set(ByVal value As Boolean)
            blnNaoAvisar = value
        End Set
    End Property

    Public Property propostasAvisadas() As String
        Get
            Return CType(ViewState("propostasAvisadas"), String)
        End Get
        Set(ByVal value As String)
            ViewState("propostasAvisadas") = value
        End Set
    End Property

    Public Property propostasReanalise() As String
        Get
            Return CType(ViewState("propostasReanalise"), String)
        End Get
        Set(ByVal value As String)
            ViewState("propostasReanalise") = value
        End Set
    End Property

    Public Property clientesPropostas() As String
        Get
            Return CType(ViewState("clientesPropostas"), String)
        End Get
        Set(ByVal value As String)
            ViewState("clientesPropostas") = value
        End Set
    End Property

    Public ReadOnly Property Sinistro_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("SINISTRO_ID")) Then
                Return Request.QueryString("SINISTRO_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property


    Public ReadOnly Property PS() As Integer
        Get
            If Not IsNothing(Request.QueryString("PS")) Then
                Return Request.QueryString("PS")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property sProposta() As Boolean
        Get
            If Not IsNothing(Request.QueryString("sProposta")) Then
                Return Request.QueryString("sProposta")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property SubEvento_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("SubEvento_ID")) Then
                Return Request.QueryString("SubEvento_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public Property Dt_Ocorrencia() As String
        Get
            Return ViewState("Dt_ocorrencia")
        End Get
        Set(ByVal value As String)

            ViewState("Dt_ocorrencia") = value.Replace(" 00:00:00", "")

        End Set
    End Property
    Public Property Evento_ID() As Long
        Get
            Return ViewState("Evento_ID")
        End Get
        Set(ByVal value As Long)

            ViewState("Evento_ID") = value

        End Set
    End Property

    Public Property CPF_CNPJ() As String 'Long
        Get
            Return ViewState("CPF_CNPJ")
        End Get
        Set(ByVal value As String)

            ViewState("CPF_CNPJ") = value

        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            PopularGridView()
            Me.CarregaProdutoBBProtecaoVida()

            ConsultaPropostaAfetadas()

            If (GVwSinistrados.Rows.Count = 0 And PS = "6") Then
                Me.Voltar()
            ElseIf (GVwSinistrados.Rows.Count = 0 And PS = "5") Then
                Dim objInsPropostas_IN As New SEGS8893_SPI_EO_IN
                Dim objInsPropostas_Business As New SEGS8893_SPI_Business

                objInsPropostas_IN.Aviso = "AVSR"
                objInsPropostas_IN.Sinistramo = 0
                objInsPropostas_IN.Sinistsubramo = 0
                objInsPropostas_IN.Proposta_Ab = 0
                objInsPropostas_IN.Proposta_Bb = 0
                objInsPropostas_IN.Situacao = "Sem Proposta"
                objInsPropostas_IN.Sinistproduto = ""
                objInsPropostas_IN.Produto = 0
                objInsPropostas_IN.Nome_Produto = "Sem Produto"
                objInsPropostas_IN.Sinistramo = ""
                objInsPropostas_IN.Tipo_Ramo = 0
                objInsPropostas_IN.Sinistro_Id_Passos = Me.Sinistro_ID
                objInsPropostas_IN.Dt_Ocorrencia = CDate(Dt_Ocorrencia).ToString("yyyy-MM-dd")
                objInsPropostas_IN.Usuario = BasePages.Usuario.LoginWeb
                objInsPropostas_IN.Sinistproposta = 0
                objInsPropostas_IN.Sinistsituacaoproposta = "Sem Proposta"
                objInsPropostas_IN.Lbl_Sinistro = "True"
                objInsPropostas_IN.Tp_Aviso_Caption = "Sinistro AB"

                objInsPropostas_Business.Insere(objInsPropostas_IN)

                Me.Avancar()
            End If

        End If

        If IsNothing(Session("DadosCliente")) Then
            AjustaNavegacao("LkbSinistroCA")
        Else
            AjustaNavegacao("DadosCliente")
        End If

        LkbSinistroCA.Text = " ► Analise de Sinistro"
        LkbSinistroCA.ForeColor = Drawing.Color.Blue
    End Sub
    Public Sub AjustaNavegacao(ByVal But As String)
        Select Case But
            Case "Evento"
                LkbPesquisa.Enabled = True
            Case "LkbSolicitante"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
            Case "LkbAgencia"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
            Case "LkbSinistroCA"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
            Case "LkbObjsegurado"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
            Case "LkbOutrosSeguros"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
                LkbObjsegurado.Enabled = True
            Case "Ressumo"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
                LkbObjsegurado.Enabled = True
            Case "DadosCliente"
                LkbDadosCliente.Enabled = True
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
        End Select
    End Sub

    Private Sub SetarValores(ByVal objOut_8775 As SEGS8775_SPS_EO_OUT)
        Try
            Me.hfTxtEvento.Value = objOut_8775.Evento_texto
            Me.hfEvento.Value = objOut_8775.evento_causador_ocorrencia
            Me.hfDataocorrencia.Value = objOut_8775.data_ocorrencia
            Me.hfTxtSubEvt.Value = objOut_8775.SubEvento_texto
            Me.hfSubEvento.Value = objOut_8775.sub_evento_causador_ocorrencia
            Me.hfDtOcorrencia.Value = objOut_8775.data_ocorrencia
            Me.NomeSegurado = objOut_8775.nome
            Me.NomeSinistrado = objOut_8775.nome_sinistrado
            Me.hfcomponente.Value = 1
        Catch ex As Exception
            '   ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Ocorreu um erro no sistema e foi reportado, Reinicie o proceso ou aguarde correção.');", True)
            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Private Sub PopularGridView()
        Try
            Dim objGeral_TK As New SEGW0158Geral_TK
            Dim EO_IN As New SEGS8744_SPS_EO_IN
            Dim Eo_out As List(Of SEGS8744_SPS_EO_OUT)

            EO_IN.Sinistro_Id = Me.Sinistro_ID

            Eo_out = objGeral_TK.Buscar(EO_IN)



            Dim obj_in As New SEGS7758_SPS_EO_IN
            Dim obj_out As New List(Of SEGL0343.EO.SEGS7758_SPS_EO_OUT)

            obj_in.Cliente_Id = Eo_out(0).cliente_id
            obj_in.Dt_Ocorrencia = Eo_out(0).data_ocorrencia
            obj_in.Evento_Sinistro_Id = Eo_out(0).codigo_evento

            Me.Dt_Ocorrencia = Eo_out(0).data_ocorrencia.ToString

            Me.Evento_ID = Long.Parse(Eo_out(0).codigo_evento)
            Me.CPF_CNPJ = Eo_out(0).cpf_cnpj_segurado

            Dim objTK As New SEGW158AvisoPropostas_TK

            obj_out = objTK.Consultar(obj_in)
            GVwSinistrados.DataSource = obj_out
            GVwSinistrados.DataBind()

            Dim i, j As Integer
            Dim objEoin_Propostas As New SEGS8892_SPS_EO_IN
            Dim lstObjEoOut_Propostas As New List(Of SEGS8892_SPS_EO_OUT)

            Dim ObjSegurado As New SEGW0158ObjSegurado_TK

            objEoin_Propostas.Sinistro_Id_Passos = Me.Sinistro_ID

            lstObjEoOut_Propostas = ObjSegurado.ObtemListaPropostas(objEoin_Propostas)

            For i = 0 To GVwSinistrados.Rows.Count - 1
                If lstObjEoOut_Propostas.Count <> 0 Then
                    For j = 0 To lstObjEoOut_Propostas.Count - 1
                        If GVwSinistrados.Rows(i).Cells(2).Text = lstObjEoOut_Propostas.Item(j).PROPOSTA_AB Then
                            GVwSinistrados.Rows(i).Cells(1).Text = lstObjEoOut_Propostas.Item(j).AVISO
                            Exit For
                        End If
                    Next j
                End If
            Next i
            Dim flagReanalise As String

            For i = 0 To GVwSinistrados.Rows.Count - 1
                flagReanalise = Sinistro.VerificaReanaliseRE(CLng(Me.GVwSinistrados.Rows(i).Cells(2).Text), Convert.ToDateTime(Dt_Ocorrencia).ToString("yyyy-MM-dd"), Me.Evento_ID, Me.CPF_CNPJ)
                If GVwSinistrados.Rows(i).Cells(5).Text = "1152" Then
                    Sinistro.VerificaPropostaAgricola(CDbl(GVwSinistrados.Rows(i).Cells(2).Text), CInt(GVwSinistrados.Rows(i).Cells(5).Text))
                    If Sinistro.MsgErro <> "" Then
                        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & Sinistro.MsgErro & "');", True)
                        Sinistro.MsgErro = ""
                    End If
                    If Sinistro.sProdutoAgricola = "REANALISE" Then
                        GVwSinistrados.Rows(i).Cells(1).Text = "RNLS"
                    End If
                Else

                    If flagReanalise = "RNLS" Then
                        GVwSinistrados.Rows(i).Cells(1).Text = "RNLS"
                    End If
                End If


                Dim hd As HiddenField = CType(GVwSinistrados.Rows(i).Cells(10).FindControl("hdFlagReanalise"), HiddenField)
                If (Not IsNothing(hd)) Then
                    hd.Value = flagReanalise
                End If

            Next i
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Sub ConsultaPropostaAfetadas()
        Try
            Dim objTK As New SEGW0157ResumoSinistroPelaCA_TK
            Dim strTextoAviso As String = ""
            Dim objSEGS8810_SPS_EO_IN As New SEGS8810_SPS_EO_IN
            Dim listaPropostas As New List(Of SEGS8810_SPS_EO_OUT)

            objIn_8810.Sinistro_Vida_Id = Me.Sinistro_ID
            objOut_8810 = objGeral8810.ConsultaGrid(objIn_8810)
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Function ExisteProdutoBB(ByVal propostaAB As Integer) As Boolean
        Try
            Dim objTk As New SEGW157AnaliseSinistroCA()
            Dim objIn_8785 As New SEGS8785_SPS_EO_IN
            objIn_8785.Proposta_Id = propostaAB

            Return objTk.ExisteProdutoBB(objIn_8785)
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Function

    Private Function ExisteEventoSinistroProduto(ByVal evento As Integer, ByVal codproduto As Integer) As Boolean
        Try
            Dim objTk As New SEGW157AnaliseSinistroCA()
            Return objTk.ExisteEventoSinistroProduto(evento, codproduto)
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Function

    Private Function VerificaCoincideSinistradoSegurado(Optional ByVal segurado As String = "") As Boolean
        Try
            Dim primeiroNomeSinis As String
            Dim ultimoNomeSinis As String
            Dim primeiroNomeSegu As String
            Dim ultimoNomeSegu As String
            Dim ultimoEspacoSinis As Integer
            Dim ultimoEspacoSegu As Integer

            If segurado = "" Then
                segurado = Me.NomeSegurado
            End If

            VerificaCoincideSinistradoSegurado = False

            'tratamento para cliente sem sobrenome
            If InStr(1, Me.NomeSinistrado, " ", CompareMethod.Text) > 0 Then
                primeiroNomeSinis = Mid(Me.NomeSinistrado, 1, InStr(1, Me.NomeSinistrado, " ", CompareMethod.Text) - 1)
                primeiroNomeSegu = Mid(segurado, 1, InStr(1, segurado, " ", CompareMethod.Text) - 1)
            Else
                primeiroNomeSinis = Me.NomeSinistrado
                primeiroNomeSegu = segurado
            End If

            ultimoEspacoSinis = InStrRev(Me.NomeSinistrado, " ", , CompareMethod.Text)
            ultimoNomeSinis = Mid(Me.NomeSinistrado, ultimoEspacoSinis + 1, Len(Me.NomeSinistrado) - ultimoEspacoSinis)

            ultimoEspacoSegu = InStrRev(segurado, " ", , CompareMethod.Text)
            ultimoNomeSegu = Mid(segurado, ultimoEspacoSegu + 1, Len(segurado) - ultimoEspacoSegu)

            If primeiroNomeSinis = primeiroNomeSegu Then
                If ultimoNomeSinis = ultimoNomeSegu Then
                    Return True
                End If
            End If
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Function

    Public Sub CarregaProdutoBBProtecaoVida()
        Try
            cProdutoBBProtecao = String.Empty
            Dim TaskAvisoPropostas As New SEGW158AvisoPropostas_TK
            Dim EO_IN As New SEGS8758_SPS_EO_IN
            Dim LstOut As List(Of SEGS8758_SPS_EO_OUT)

            LstOut = TaskAvisoPropostas.CarregaProdutoBBProtecaoVida(EO_IN)

            For Each objEO_OUT As SEGS8758_SPS_EO_OUT In LstOut
                Me.cProdutoBBProtecao = Me.cProdutoBBProtecao & "," & objEO_OUT.produto_id
            Next

            cProdutoBBProtecao = Mid(cProdutoBBProtecao, 2)

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Private Function VerificaReanalise(ByVal proposta_id As Long, ByVal dtOcorrencia As String, ByVal Evento_sinistro_id As Integer, ByVal CPF As String, ByVal dt_nascimento As String, ByRef sinistro_id As String, ByRef sinistro_bb_id As String) As String

        Dim objAnaliseTK As New SEGW157AnaliseSinistroCA()
        Dim objIn_VerificaReanalise As New verifica_reanalise_sps_EO_IN
        Dim objOut_VerificaReanalise As verifica_reanalise_sps_EO_OUT
        Dim _retorno As String = ""

        Try


            objIn_VerificaReanalise.Cpf_Sinistrado = CPF
            If CDate(dt_nascimento) <> DateTime.MinValue Then
                objIn_VerificaReanalise.Dt_Nascimento = CDate(dt_nascimento)
            Else
                objIn_VerificaReanalise.Dt_Nascimento = Nothing
            End If
            objIn_VerificaReanalise.Dt_Ocorrencia = CDate(dtOcorrencia)
            objIn_VerificaReanalise.Evento_Sinistro_Id = Evento_sinistro_id
            objIn_VerificaReanalise.Proposta_Id = proposta_id


            objOut_VerificaReanalise = objAnaliseTK.VerificaReanalise(objIn_VerificaReanalise)

            If Not IsNothing(objOut_VerificaReanalise) Then
                sinistro_id = objOut_VerificaReanalise.sinistro_ID
                sinistro_bb_id = objOut_VerificaReanalise.sinistro_bb

                Select Case Trim(objOut_VerificaReanalise.situacao)
                    Case "0", "1", ""
                        _retorno = "NAVS"
                    Case "2"
                        _retorno = "RNLS"
                    Case Else
                        _retorno = "RNLS"
                End Select
            End If

            If IsNothing(objOut_VerificaReanalise) Then
                _retorno = ""
            End If


        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

        Return _retorno
    End Function

    Private Sub NaoAvisar()
        Try
            Dim subevento_sinistro_aux As Long
            Dim i As Long

            If Me.GVwSinistrados.SelectedIndex = -1 Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Selecione uma proposta para avisar.')", True)
                Exit Sub
            End If

            subevento_sinistro_aux = Me.SubEvento_ID
            If Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(7).Text = 2 And _
               Left(Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(1).Text, 1) <> "*" Then

                If Not Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(1).Text = "NAVS" Then
                    If Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(1).Text = "RNLS" Then
                        Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(1).Text = "*NAVS"
                    Else
                        Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(1).Text = "NAVS"

                    End If

                    If VerificarProdutoBB(cProdutoBBProtecao, Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(5).Text) Then

                        For i = 0 To Me.GVwSinistrados.Rows.Count - 1
                            If VerificarProdutoBB(cProdutoBBProtecao, Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(5).Text) Then

                                If Me.GVwSinistrados.Rows(i).Cells(1).Text = "RNLS" Then
                                    Me.GVwSinistrados.Rows(i).Cells(1).Text = "*NAVS"
                                Else

                                    Me.GVwSinistrados.Rows(i).Cells(1).Text = "NAVS"

                                End If
                            End If

                        Next i
                        'ATUALIZAR PROPOSTAS SEM COBERTURA - não passa o indice para não atualizar a linha atual
                        Call AtualizaPropostasSemCobertura()
                    End If

                End If
            End If

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Private Sub Avisar()
        Try
            Dim flagReanalise As String
            Dim svalor_anterior As String
            Dim Sinistro As New SEGL0343.GeraSinistroRE_Business
            Dim i As Long

            NaoPodeAvisar = False

            If Me.GVwSinistrados.SelectedIndex = -1 Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Selecione uma proposta para avisar.')", True)
                Exit Sub
            End If


            svalor_anterior = Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(1).Text

            If Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(1).Text <> "AVSR" And _
               Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(1).Text <> "RNLS" Then

                'Verifica se existe proposta BB e se ela é numérica
                If IsNumeric(Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(3).Text) Then
                    'Correção na passagem  dos parâmetros para VerificaPropostaBB
                    Call VerificaPropostaBB(CLng("0" & Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(3).Text), Dt_Ocorrencia)
                End If

                flagReanalise = Sinistro.VerificaReanaliseRE(CLng(Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(2).Text), Convert.ToDateTime(Dt_Ocorrencia).ToString("yyyy-MM-dd"), Me.Evento_ID, Me.CPF_CNPJ)

                If flagReanalise = "RNLS" Or flagReanalise = "AVSR" Then
                    If Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(1).Text = "PSCB" Then
                        'If MsgBox("Confirma o registro em proposta sem cobertura?", vbYesNo, "Sinistro sem proposta") = vbNo Then Exit Sub
                    End If
                    If Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(1).Text = "*NAVS" Then
                        Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(1).Text = "RNLS"
                    Else
                        Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(1).Text = flagReanalise
                    End If
                    If VerificarProdutoBB(cProdutoBBProtecao, Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(5).Text) Then
                        For i = 1 To Me.GVwSinistrados.Rows.Count - 1
                            If VerificarProdutoBB(cProdutoBBProtecao, Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(5).Text) Then
                                Me.GVwSinistrados.Rows(Str(i)).Cells(1).Text = flagReanalise
                            End If
                        Next i
                        'ATUALIZAR PROPOSTAS SEM COBERTURA
                        Call AtualizaPropostasSemCobertura(Me.GVwSinistrados.SelectedIndex, svalor_anterior)
                    End If

                Else

                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Esta proposta já possui um aviso de sinistro em aberto\n para os parametros: proposta, evento de sinistro, \n data de ocorrência, CPF ou CNPJ do sinistrado.\n Não é possível avisar esta proposta.');", True)

                    NaoPodeAvisar = True
                End If

            End If

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Private Sub AtualizaPropostasSemCobertura(Optional ByVal indice As Integer = 0, Optional ByVal valor As String = "")

        Dim obj_in As New SEGS7758_SPS_EO_IN
        Dim obj_out As New List(Of SEGL0343.EO.SEGS7758_SPS_EO_OUT)
        Try
            If Me.GVwSinistrados.SelectedIndex = -1 Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Selecione uma proposta para avisar.')", True)
                Exit Sub
            End If
            Dim TaskPassosSinistro As New SEGW158SinistroInserirPassosDanos_TK
            Dim EO_IN As New SEGS8744_SPS_EO_IN
            Dim Eo_out As List(Of SEGS8744_SPS_EO_OUT)

            EO_IN.Sinistro_Id = Me.Sinistro_ID

            Eo_out = TaskPassosSinistro.Consultar_Passo_Danos(EO_IN)
            Cliente_ID = Eo_out.Item(0).cliente_id



            obj_in.Cliente_Id = Cliente_ID 'Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(3).Text
            obj_in.Evento_Sinistro_Id = Me.Evento_ID
            obj_in.Dt_Ocorrencia = Me.Dt_Ocorrencia

            Dim objTK As New SEGW158AvisoPropostas_TK

            obj_out = objTK.Consultar(obj_in)


            If Not IsNothing(obj_out) Then
                For i As Integer = 0 To obj_out.Count - 1
                    If obj_out.Item(i).proposta_id = Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(2).Text Then
                        If (obj_out.Item(i).indicador = "PSCB") And (valor <> "PSCB") Then
                            Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(1).Text = "PSCB"
                        End If
                    End If
                Next i
            End If
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Public Function VerificarProdutoBB(ByVal lista As String, ByVal value As String) As Boolean
        Try
            VerificarProdutoBB = False
            Dim arr As Object
            Dim i As Long

            arr = Split(lista, ",")

            For i = 0 To UBound(arr)
                If Trim(CStr(arr(i))) = Trim(CStr(value)) Then
                    VerificarProdutoBB = True
                    Exit For
                End If
            Next

            Exit Function
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
            VerificarProdutoBB = False
        End Try

    End Function

    Private Sub ExibirCoberturas()
        Try
            If Me.GVwSinistrados.SelectedIndex = -1 Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Selecione uma proposta para exibir as coberturas.')", True)
                Exit Sub
            End If
            If Trim(Me.GVwSinistrados.SelectedRow.Cells(2).Text) = "" Then Exit Sub

            With Me.ucCoberturaPropostas
                .Proposta = Me.GVwSinistrados.SelectedRow.Cells(5).Text
                .NomeCliente = Me.GVwSinistrados.SelectedRow.Cells(9).Text
                .Produto = Me.GVwSinistrados.SelectedRow.Cells(4).Text
                .EventoSinistro = Me.hfTxtEvento.Value
                .SubEvento = Me.hfTxtSubEvt.Value
                .PopulaGridCoberturas(CDate(Me.hfDataocorrencia.Value), Me.GVwSinistrados.SelectedRow.Cells(5).Text, Me.GVwSinistrados.SelectedRow.Cells(14).Text, Me.hfcomponente.Value, Me.hfEvento.Value, Me.hfSubEvento.Value)
                .Visible = True
            End With
            Me.mpe.Show()
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Private Sub Reanalise()
        Try
            If Me.GVwSinistrados.SelectedIndex = -1 Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Selecione uma proposta para avisar.')", True)
                Exit Sub
            End If

            If Not Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(1).Text = "RNLS" And _
               Left(Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(1).Text, 1) = "*" Then

                Me.GVwSinistrados.Rows(Me.GVwSinistrados.SelectedIndex).Cells(1).Text = "RNLS"
            Else
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Não é possível abrir esta proposta para reanálise de sinistro.')", True)
            End If
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Private Function VerificaSePodeAvisar(ByVal dt_inicio_vigencia As String, ByVal dt_fim_vigencia As String) As Boolean
        Try
            VerificaSePodeAvisar = False

            'Verifica vigencia
            If Trim(dt_inicio_vigencia) <> "" Then
                If Trim(dt_fim_vigencia) <> "" Then
                    If Date.Parse(dt_inicio_vigencia) <= Date.Parse(hfDataocorrencia.Value) And _
                       Date.Parse(dt_fim_vigencia) >= Date.Parse(hfDataocorrencia.Value) Then
                        VerificaSePodeAvisar = True
                    End If
                Else
                    If Date.Parse(dt_inicio_vigencia) <= Date.Parse(hfDataocorrencia.Value) Then
                        VerificaSePodeAvisar = True
                    End If
                End If
            Else
                If dt_fim_vigencia <> "" Then
                    If Date.Parse(dt_fim_vigencia) >= Date.Parse(hfDataocorrencia.Value) Then
                        VerificaSePodeAvisar = True
                    End If
                End If
            End If

            Return VerificaSePodeAvisar

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try


    End Function

    Private Function ObtemPropostasAvisadas() As Boolean

        Dim i As Long
        Dim propNAVSBBProtecao As String
        Dim propAVSRBBProtecao As String
        Try
            propostasAvisadas = ""
            propAVSRBBProtecao = ""
            propNAVSBBProtecao = ""

            ObtemPropostasAvisadas = True

            'Obtem as propostas e os clientes afetados
            For i = 0 To Me.GVwSinistrados.Rows.Count - 1
                If Me.GVwSinistrados.Rows(i).Cells(1).Text = "AVSR" Then

                    If propostasAvisadas <> "" Then propostasAvisadas = propostasAvisadas & " , "
                    propostasAvisadas = propostasAvisadas & Me.GVwSinistrados.Rows(i).Cells(2).Text

                ElseIf Me.GVwSinistrados.Rows(i).Cells(1).Text = "RNLS" Then

                    If propostasReanalise <> "" Then propostasReanalise = propostasReanalise & " , "
                    propostasReanalise = propostasReanalise & Me.GVwSinistrados.Rows(i).Cells(2).Text

                End If
            Next i

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Function

    Private Function VerificaCobertAtingida(ByVal proposta_id As Long, _
                        ByVal dtOcorrencia As String, _
                        ByVal tp_componente_id As Integer, _
                        ByVal Evento_sinistro_id As Long, _
                        ByVal SubEvento_sinistro_id As Long, _
                        ByVal sub_grupo_id As Integer) As Boolean

        Try
            Dim taskAnaliseCA As New SEGW157AnaliseSinistroCA
            Dim objIn_ConsCobertura As New consultar_coberturas_sps_EO_IN

            objIn_ConsCobertura.Dt_Ocorrencia = dtOcorrencia
            objIn_ConsCobertura.Tp_Componente_Id = tp_componente_id
            objIn_ConsCobertura.Sub_Grupo_Id = sub_grupo_id
            objIn_ConsCobertura.Proposta_Id = proposta_id
            objIn_ConsCobertura.Tipo_Ramo = 1

            Return taskAnaliseCA.VerificaCoberturaAtingida(objIn_ConsCobertura, proposta_id, Evento_sinistro_id, SubEvento_sinistro_id, tp_componente_id, dtOcorrencia)

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Function

    Function VerificaPropostaBB(ByVal Proposta_BB As Long, ByVal dt_ocorrencia As String) As Boolean
        'ObtemVersaoEndosso
        Dim objTK As New SEGW157AnaliseSinistroCA
        Dim objInObtemVersaoEndosso As New Obtem_Versao_Endosso_sps_EO_IN
        Dim objOutObtemVersaoEndosso As New Obtem_Versao_Endosso_sps_EO_OUT
        Try
            objInObtemVersaoEndosso.Dt_Ocorrencia = CDate(dt_ocorrencia)
            objInObtemVersaoEndosso.Proposta_Bb = Proposta_BB

            objOutObtemVersaoEndosso = objTK.ObtemVersaoEndosso(objInObtemVersaoEndosso)

            If IsNothing(objOutObtemVersaoEndosso) Then
                VerificaPropostaBB = False
                Me.hfNrVrsEndosso.Value = "0"
                Me.hfNRCTRSGROBB.Value = "0"
            Else
                VerificaPropostaBB = True
                Me.hfNrVrsEndosso.Value = objOutObtemVersaoEndosso.NR_VRS_EDS
                Me.hfNRCTRSGROBB.Value = objOutObtemVersaoEndosso.NR_CTR_SGRO_BB
            End If
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

        Return VerificaPropostaBB

    End Function

    Protected Sub GVwSinistrados_RowDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVwSinistrados.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim sinistro_id As String = ""
                Dim Sinistro_bb As String = ""
                Dim subevento_sinistro_aux As Integer = 0
                If Me.hfSubEvento.Value <> String.Empty And Me.hfSubEvento.Value <> "0" Then
                    If (Me.hfSubEvento.Value) = -1 Then
                        subevento_sinistro_aux = 0
                    Else
                        subevento_sinistro_aux = Me.hfSubEvento.Value
                    End If
                End If

                If Trim(e.Row.Cells(2).Text) = "" Then Exit Sub
                If IsNumeric(e.Row.Cells(6).Text) Then

                    Call VerificaPropostaBB(CLng("0" & e.Row.Cells(6).Text), Me.hfDtOcorrencia.Value) 'IIf(Me.hfDtOcorrencia.Value = "", "", "'" & Format(Me.hfDtOcorrencia.Value, "yyyymmdd") & "'"))


                End If

                If e.Row.Cells(2).Text = "Vida" And ConfigurationManager.AppSettings("TipoRamo") <> 1 Then
                    e.Row.Cells(1).Text = "NAVS"
                ElseIf e.Row.Cells(1).Text = "Elementar" And ConfigurationManager.AppSettings("TipoRamo") <> 2 Then
                    e.Row.Cells(1).Text = "NAVS"
                Else

                End If

            End If
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub GVwSinistrados_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GVwSinistrados.SelectedIndexChanged
        Try
            'Dim sb As New StringBuilder()
            'sb.Append(vbCrLf)
            'sb.Append("<script type=""text/javascript"">" & vbCrLf)
            'sb.Append("var oTable = document.getElementById('GVwSinistrados');" & vbCrLf)
            'sb.Append("    var RowsLength = oTable.rows.length;" & vbCrLf)
            'sb.Append("    var rowscon = 0" & vbCrLf)
            'sb.Append("    for (var i=0; i < RowsLength; i++){" & vbCrLf)
            'sb.Append("        var oCells = oTable.rows.item(i).cells;" & vbCrLf)
            'sb.Append("        oCells.item(10).style.display = ""none"";" & vbCrLf)
            'sb.Append("    }" & vbCrLf)
            'sb.Append("</script>")
            'ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), sb.ToString(), False)

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub btnContinuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinuar.Click

        If Gravar() Then
            Avancar()
        End If

    End Sub

    Protected Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click
        Try
            If Me.Gravar() Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Dados salvos com sucesso!')", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Erro ao tentar salvar passos!')", True)
            End If
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub btnVoltar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoltar.Click
        Try
            Me.Voltar()
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Sub Voltar()

        Dim objDel_IN As New SEGS8895_SPD_EO_IN
        Dim objDel_Business As New SEGS8895_SPD_Business

        objDel_IN.Sinistro_Id_Passos = Me.Sinistro_ID
        objDel_Business.Exclui(objDel_IN)

        Dim sb As New StringBuilder()

        sb.Append("SEGW0158DadosAgencia.aspx")
        sb.Append("?SINISTRO_ID=" + IIf(IsNothing(Me.Sinistro_ID), "", Me.Sinistro_ID.ToString))
        sb.Append("&SLinkSeguro=" + Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())

    End Sub

    Function RetornaStatusPropostas() As String
        Try
            Dim rowSelecionado As GridViewRow = Me.GVwSinistrados.SelectedRow
            Return rowSelecionado.Cells(1).Text.Trim()
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Function

    Sub Avancar()
        Dim sb As New StringBuilder()
        Try
            GravaPasso()


            Dim objGeral As New SEGW0158Geral_TK
            objGeral.AtualizaPasso(Sinistro_ID, 5)

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

        sb.Append("SEGW0158ObjSegurado.aspx")
        sb.Append("?SINISTRO_ID=" & Me.Sinistro_ID)
        sb.Append("&SLinkSeguro=" + Request("SLinkSeguro"))
        Response.Redirect(sb.ToString())

    End Sub
    Sub GravaPasso()
        Try
            Dim objOut_8744 As New List(Of SEGS8744_SPS_EO_OUT)
            Dim objIn_8744 As New SEGS8744_SPS_EO_IN
            Dim PassoFinal As Boolean = False

            Dim objGeral As New SEGW0158Geral_TK()

            objIn_8744.Sinistro_Id = Me.Sinistro_ID

            objOut_8744 = objGeral.Buscar(objIn_8744)

            Dim objIn_8747 As New SEGS8747_SPU_EO_IN()

            If Not IsNothing(objOut_8744) Then
                objIn_8747 = objGeral.PreencheTodosCamposU(objOut_8744(0))
            End If

            objIn_8747.Codigo_Passo = 5
            'If GVwSinistrados.Rows.Count = 0 Then
            '    objIn_8747.Ind_Sem_Proposta = True
            'Else
            '    objIn_8747.Ind_Sem_Proposta = False
            'End If
            objIn_8747.Ind_Sem_Proposta = True

            For index As Integer = 1 To GVwSinistrados.Rows.Count
                If (GVwSinistrados.Rows(index - 1).Cells(1).Text = "AVSR" Or GVwSinistrados.Rows(index - 1).Cells(1).Text = "RNLS") Then
                    objIn_8747.Ind_Sem_Proposta = False
                    Exit For
                End If
            Next

            objGeral.Atualizar(objIn_8747)

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try


    End Sub
    Function VerificaNAvisadaNReanalise() As Integer

        Dim aux As Integer
        Try
            aux = 0
            For index As Integer = 1 To GVwSinistrados.Rows.Count
                GVwSinistrados.Rows(index).Cells(1).Text = "NAVS"
            Next
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

        Return aux

    End Function

    Function Gravar() As Boolean
        Try

            Dim Ramo, SubRamo, i, j As Integer
            Dim flagReanalise As String = ""
            Dim Sinistro As New SEGL0343.GeraSinistroRE_Business

            Dim objPropostas_IN As New SEGS8892_SPS_EO_IN
            Dim objPropostas_OUT As New List(Of SEGS8892_SPS_EO_OUT)
            Dim objPropostas_Business As New SEGS8892_SPS_Business

            Dim objInsPropostas_IN As New SEGS8893_SPI_EO_IN
            Dim objInsPropostas_Business As New SEGS8893_SPI_Business

            Dim objUpPropostas_IN As New SEGS8894_SPU_EO_IN
            Dim objUpPropostas_Business As New SEGS8894_SPU_Business

            Dim objDel_IN As New SEGS8895_SPD_EO_IN
            Dim objDel_Business As New SEGS8895_SPD_Business



            objDel_IN.Sinistro_Id_Passos = Me.Sinistro_ID
            objDel_Business.Exclui(objDel_IN)



            If ObtemPropostasAvisadas() = False Or NaoPodeAvisar Then Exit Function
            bSemProposta = False

            For i = 0 To Me.GVwSinistrados.Rows.Count - 1

                If Mid(Me.GVwSinistrados.Rows(i).Cells(1).Text, 1, 1) = "A" Or Mid(Me.GVwSinistrados.Rows(i).Cells(1).Text, 1, 1) = "R" Then

                    Dim eo_out As New List(Of SEGS7366_SPS_EO_OUT)
                    Dim eo_in As New SEGS7366_SPS_EO_IN

                    eo_in.Data_Sistema = Date.Today.ToString("yyyyMMdd")
                    eo_in.Proposta_Id = Me.GVwSinistrados.Rows(i).Cells(2).Text

                    Dim objTK As New SEGS7366_SPS_Business

                    eo_out = objTK.seleciona(eo_in)

                    If Not eo_out.Count = 0 Then
                        Ramo = eo_out.Item(0).ramo_id
                        SubRamo = eo_out.Item(0).subramo_id
                    End If

                    'Reanalise
                    Call Sinistro.VerificaPropostaAgricola(Me.GVwSinistrados.Rows(i).Cells(2).Text, Me.GVwSinistrados.Rows(i).Cells(5).Text, CInt(i), Me.GVwSinistrados.Rows(i).Cells(1).Text)
                    If Sinistro.MsgErro <> "" Then
                        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & Sinistro.MsgErro & "');", True)
                        Sinistro.MsgErro = ""
                    End If

                    If Sinistro.sProdutoAgricola = "ERRO" Then
                        Return False
                    End If

                    If Sinistro.sProdutoAgricola = "INC_OCORRENCIA" Then
                        objInsPropostas_IN.Tp_Aviso_Caption = SEGL0343.GeraSinistroRE_Business.sInclusaoOcorrencia
                        objInsPropostas_IN.Txt_Reanalise = "Não"
                        objInsPropostas_IN.Sinistro_Visible = "True"
                        objInsPropostas_IN.Lbl_Sinistro = "True"
                    ElseIf Sinistro.sProdutoAgricola = "ABERTURA" Then
                        objInsPropostas_IN.Tp_Aviso_Caption = SEGL0343.GeraSinistroRE_Business.sAbertura
                        objInsPropostas_IN.Txt_Reanalise = "Não"
                        objInsPropostas_IN.Sinistro_Visible = "False"
                        objInsPropostas_IN.Lbl_Sinistro = "False"
                    ElseIf Sinistro.sProdutoAgricola <> "REANALISE" Then
                        flagReanalise = Sinistro.VerificaReanaliseRE(CLng(Me.GVwSinistrados.Rows(i).Cells(2).Text), Convert.ToDateTime(Dt_Ocorrencia).ToString("yyyy-MM-dd"), CLng(Me.Evento_ID.ToString()), Me.CPF_CNPJ)

                        If flagReanalise = "RNLS" Then
                            Call Sinistro.PreencheDadosSinistro()
                            objInsPropostas_IN.Tp_Aviso_Caption = SEGL0343.GeraSinistroRE_Business.sReabertura

                        ElseIf flagReanalise = "NAVS" Then
                            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Esta proposta já possui um aviso de sinistro em aberto \n " & _
                                        "para os parametros: proposta, CPF/CNPJ do Segurado, evento de sinistro e \n" & _
                                        "data de ocorrência.\n" & _
                                        "Sinistro AB: " & Sinistro.sSinistro_id & "\n" & _
                                        "Sinistro BB: " & Sinistro.sSinistro_bb & "\n" & _
                                        "Não é possível avisar esta proposta.');", True)


                            Exit Function
                        ElseIf flagReanalise = "AVSR" Then
                            objInsPropostas_IN.Tp_Aviso_Caption = SEGL0343.GeraSinistroRE_Business.sAbertura
                            objInsPropostas_IN.Txt_Reanalise = "Não"
                            objInsPropostas_IN.Sinistro_Visible = "False"
                            objInsPropostas_IN.Lbl_Sinistro = "False"
                        End If
                    Else

                        If Me.GVwSinistrados.Rows(i).Cells(1).Text = "RNLS" Then
                            objInsPropostas_IN.Tp_Aviso_Caption = "Sinsitro AB"
                            objInsPropostas_IN.Txt_Reanalise = "Não"
                            objInsPropostas_IN.Sinistro_Visible = "Sim"
                            objInsPropostas_IN.Lbl_Sinistro = "True"
                        Else
                            objInsPropostas_IN.Tp_Aviso_Caption = "Reanálise"
                            objInsPropostas_IN.Txt_Reanalise = "Sim"
                            objInsPropostas_IN.Sinistro_Visible = "Sim"
                            objInsPropostas_IN.Lbl_Sinistro = "True"
                            If String.IsNullOrEmpty(flagReanalise) Then
                                flagReanalise = "RNLS"
                            End If
                        End If

                    End If


                    If Not objPropostas_OUT.Count = 0 Then

                        objInsPropostas_IN.Sinistproposta = Me.GVwSinistrados.Rows(i).Cells(2).Text.Trim
                        objInsPropostas_IN.Sinistproduto = Me.GVwSinistrados.Rows(i).Cells(5).Text.Trim
                        objInsPropostas_IN.Sinistramo = Ramo
                        objInsPropostas_IN.Sinistsubramo = SubRamo
                        objInsPropostas_IN.Sinistsituacaoproposta = Me.GVwSinistrados.Rows(i).Cells(4).Text.Trim
                        If Sinistro.sProdutoAgricola = "RNLS" Then
                            objInsPropostas_IN.Tp_Aviso_Caption = "REANALISE"
                        Else
                            objInsPropostas_IN.Tp_Aviso_Caption = Sinistro.sProdutoAgricola
                        End If

                    End If

                End If

                ' SÓ PERSISTE SE FOR AVISO OU REANALIZE
                If Mid(Me.GVwSinistrados.Rows(i).Cells(1).Text, 1, 1) = "A" Or _
                    Mid(Me.GVwSinistrados.Rows(i).Cells(1).Text, 1, 1) = "R" Then

                    objInsPropostas_IN.Aviso = Me.GVwSinistrados.Rows(i).Cells(1).Text 'flagReanalise
                    objInsPropostas_IN.Sinistramo = Ramo
                    objInsPropostas_IN.Sinistsubramo = SubRamo

                    objInsPropostas_IN.Proposta_Ab = Me.GVwSinistrados.Rows(i).Cells(2).Text
                    objInsPropostas_IN.Proposta_Bb = Me.GVwSinistrados.Rows(i).Cells(3).Text
                    objInsPropostas_IN.Situacao = Me.GVwSinistrados.Rows(i).Cells(4).Text
                    objInsPropostas_IN.Sinistproduto = Me.GVwSinistrados.Rows(i).Cells(5).Text
                    objInsPropostas_IN.Produto = Me.GVwSinistrados.Rows(i).Cells(5).Text
                    objInsPropostas_IN.Nome_Produto = GeraSinistroRE.RetornaFraseSemASCII(Me.GVwSinistrados.Rows(i).Cells(6).Text)

                    objInsPropostas_IN.Tipo_Ramo = ConfigurationManager.AppSettings("TipoRamo")
                    objInsPropostas_IN.Inicio_Vigencia = CDate(Me.GVwSinistrados.Rows(i).Cells(8).Text).ToString("yyyy-MM-dd")
                    objInsPropostas_IN.Fim_Vigencia = CDate(Me.GVwSinistrados.Rows(i).Cells(9).Text).ToString("yyyy-MM-dd")
                    objInsPropostas_IN.Sinistro_Id_Passos = Me.Sinistro_ID
                    objInsPropostas_IN.Dt_Ocorrencia = CDate(Dt_Ocorrencia).ToString("yyyy-MM-dd")
                    objInsPropostas_IN.Usuario = BasePages.Usuario.LoginWeb
                    objInsPropostas_IN.Sinistproposta = Me.GVwSinistrados.Rows(i).Cells(2).Text.Trim
                    objInsPropostas_IN.Sinistsituacaoproposta = Me.GVwSinistrados.Rows(i).Cells(4).Text.Trim

                    'Tratamento para controles - adequação conforme VB6.0
                    If objInsPropostas_IN.Lbl_Sinistro = "False" Then
                        objInsPropostas_IN.Tp_Aviso_Caption = "Reanálise"
                        objInsPropostas_IN.Txt_Reanalise = "Não"
                        objInsPropostas_IN.Sinistro_Visible = "True"
                    End If

                    objInsPropostas_Business.Insere(objInsPropostas_IN)
                Else
                    objInsPropostas_IN.Aviso = Me.GVwSinistrados.Rows(i).Cells(1).Text
                    objInsPropostas_IN.Sinistramo = 0
                    objInsPropostas_IN.Sinistsubramo = 0
                    objInsPropostas_IN.Proposta_Ab = Me.GVwSinistrados.Rows(i).Cells(2).Text
                    objInsPropostas_IN.Proposta_Bb = Me.GVwSinistrados.Rows(i).Cells(3).Text
                    objInsPropostas_IN.Situacao = ""
                    objInsPropostas_IN.Sinistproduto = ""
                    objInsPropostas_IN.Produto = 0
                    objInsPropostas_IN.Nome_Produto = ""
                    objInsPropostas_IN.Sinistramo = ""
                    objInsPropostas_IN.Tipo_Ramo = 0
                    objInsPropostas_IN.Sinistro_Id_Passos = Me.Sinistro_ID
                    objInsPropostas_IN.Dt_Ocorrencia = CDate(Dt_Ocorrencia).ToString("yyyy-MM-dd")
                    objInsPropostas_IN.Usuario = BasePages.Usuario.LoginWeb
                    objInsPropostas_IN.Sinistproposta = 0
                    objInsPropostas_IN.Sinistsituacaoproposta = Me.GVwSinistrados.Rows(i).Cells(4).Text.Trim

                    'Tratamento para controles - adequação conforme VB6.0
                    If objInsPropostas_IN.Lbl_Sinistro = "False" Then
                        objInsPropostas_IN.Tp_Aviso_Caption = "Reanálise"
                        objInsPropostas_IN.Txt_Reanalise = "Não"
                        objInsPropostas_IN.Sinistro_Visible = "True"
                    End If

                    objInsPropostas_Business.Insere(objInsPropostas_IN)
                End If
                'LIMPA VARIÁVEIS 
                flagReanalise = ""
            Next

            objPropostas_IN.Sinistro_Id_Passos = Me.Sinistro_ID
            objPropostas_OUT = objPropostas_Business.seleciona(objPropostas_IN)

            If objPropostas_OUT.Count = 0 Then
                bSemProposta = True

            End If

            'sergio.sn - Implementação da graavação dos passo
            Dim lstobjPassosOut As New List(Of SEGS8744_SPS_EO_OUT)

            Dim objOut_8744 As New List(Of SEGS8744_SPS_EO_OUT)
            Dim objIn_8744 As New SEGS8744_SPS_EO_IN

            objIn_8744.Sinistro_Id = Me.Sinistro_ID

            objOut_8744 = ObjGeral.Buscar(objIn_8744)

            If Not IsNothing(objOut_8744) Then

                Dim objIn_8747 As New SEGS8747_SPU_EO_IN()

                objIn_8747 = ObjGeral.PreencheTodosCamposU(objOut_8744(0))

                With objIn_8747

                    .Codigo_Ramo = Ramo
                    .Codigo_Sub_Ramo = SubRamo

                    If bSemProposta = True Then

                        .Ind_Sem_Proposta = 1

                    ElseIf bSemProposta = False Then

                        .Ind_Sem_Proposta = 0

                    End If

                    .sinistro_re_id = Sinistro.sSinistro_id
                    .produto_agricula = Sinistro.sProdutoAgricola

                    ObjGeral.Atualizar(objIn_8747)

                End With

            End If

            Return True
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try


    End Function

    Protected Sub btnAvisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAvisar.Click
        Try
            Me.Avisar()
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub btnReanalise_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReanalise.Click
        Try
            Me.Reanalise()
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub btnNAvisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNAvisar.Click
        Try
            Me.NaoAvisar()
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub GVwSinistrados_PageIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GVwSinistrados.PageIndexChanged
        Try

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try
    End Sub

    Protected Sub GVwSinistrados_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVwSinistrados.PageIndexChanging
        Try
            GVwSinistrados.PageIndex = e.NewPageIndex
            PopularGridView()
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub btnFecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFecha.Click
        Try
            Me.mpe.Hide()
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub LkbPesquisa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbPesquisa.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158ConsultaSinistrado.aspx")
        sb.Append("?Cliente_ID=" & Cliente_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())
    End Sub

    Protected Sub LkbEvento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbEvento.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158Evento.aspx")
        sb.Append("?SINISTRO_ID=" & Me.Sinistro_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))
        Response.Redirect(sb.ToString())

    End Sub

    Protected Sub LkbSolicitante_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbSolicitante.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158DadosSolicitante.aspx")
        sb.Append("?SINISTRO_ID=" + Me.Sinistro_ID)
        sb.Append("&SLinkSeguro=" + Request("SLinkSeguro"))
        Response.Redirect(sb.ToString())
    End Sub
    Protected Sub LkbDadosCliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbDadosCliente.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158DadosCliente.aspx")
        sb.Append("?SINISTRO_ID=" & Me.Sinistro_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())
    End Sub
    Protected Sub LkbAgencia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbAgencia.Click
        Voltar()
    End Sub
End Class


