﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterSEGW0158.Master" CodeBehind="SEGW0158AvisoPropostas.aspx.vb" Inherits="SEGW0158.SEGW0158AvisoPropostas" 
    EnableEventValidation="false" title="Untitled Page" %>
    
 <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeaderPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<asp:ScriptManager ID="ScriptManager1" runat="server">
 </asp:ScriptManager>
 
 <asp:UpdatePanel ID="pagina" runat="server">
 <ContentTemplate>
 
<fieldset id="flsSolicitante" style="text-align: left; width: 730px; height: 530px;">
<legend><b><span style="font-size: 8pt; font-family: Verdana">Propostas do Cliente</span></b></legend>
<asp:Label ID="lblPasso" runat="server" 
Font-Bold="True" 
Font-Names="Verdana"
Font-Size="15pt" Text="Passo 5">
</asp:Label>
<br /> 
<asp:GridView ID="grdResultadoPesquisa" runat="server" AutoGenerateColumns="False" BackColor="White"
BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
GridLines="Vertical" AllowPaging="True" AllowSorting="True" HorizontalAlign="Left" Width="666px" 
Font-Strikeout="False" Height="277px" AutoGenerateSelectButton="True" OnSelectedIndexChanged="grdResultadoPesquisa_SelectedIndexChanged">
<FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
<RowStyle BackColor="#EEEEEE" ForeColor="Black" Font-Size="X-Small" 
HorizontalAlign="Left" VerticalAlign="Middle" Font-Bold="False" Font-Names="Verdana" />
<Columns>
<asp:TemplateField ShowHeader="False">
<ItemTemplate>
<asp:ImageButton ID="ImgBtnDetalhes" runat="server"
CommandName="Select" CommandArgument='<%# Eval("proposta_id") %>'
ImageUrl="~/Image/icon_bseta_menu_unsel.gif"/>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField ShowHeader="False" Visible="False">
<ItemTemplate>
<asp:Button ID="btnDetalhes" runat="server" Text="..." 
Visible="False" />
</ItemTemplate>
</asp:TemplateField>
<asp:BoundField HeaderText="Indicador" DataField="Indicador">
<HeaderStyle HorizontalAlign="Center" />
<ItemStyle HorizontalAlign="Center" />
</asp:BoundField>
<asp:BoundField HeaderText="Proposta" DataField="proposta_id" >
<HeaderStyle HorizontalAlign="Center" />
<ItemStyle HorizontalAlign="Center" />                           
</asp:BoundField>
<asp:BoundField HeaderText="Proposta BB" DataField="proposta_bb" >
<HeaderStyle HorizontalAlign="Center" />
<ItemStyle HorizontalAlign="Center" />
</asp:BoundField>  

<asp:BoundField HeaderText="Situa&#231;&#227;o" DataField="situacao" >
<HeaderStyle HorizontalAlign="Center" />
<ItemStyle HorizontalAlign="Center" />
</asp:BoundField>  
<asp:BoundField HeaderText="Produto" DataField="produto_id" >
<HeaderStyle HorizontalAlign="Center" />
<ItemStyle HorizontalAlign="Center" />
</asp:BoundField>  
<asp:BoundField HeaderText="Produto" DataField="produto" >
<HeaderStyle HorizontalAlign="Center" />
<ItemStyle HorizontalAlign="Center" Width="100px" />
</asp:BoundField>  
<asp:BoundField HeaderText="Tipo Ramo" DataField="tp_ramo_id" >
<HeaderStyle HorizontalAlign="Center" />
<ItemStyle HorizontalAlign="Center" />
</asp:BoundField>  
<asp:BoundField HeaderText="Dt Inicio Vig&#234;ncia" DataField="dt_inicio_vigencia" >
<HeaderStyle HorizontalAlign="Center" />
<ItemStyle HorizontalAlign="Center" />
</asp:BoundField>  
<asp:BoundField HeaderText="Dt Fim Vig&#234;ncia" DataField="dt_fim_vigencia" >
<HeaderStyle HorizontalAlign="Center" />
<ItemStyle HorizontalAlign="Center" />
</asp:BoundField>  
</Columns>
<PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
<EmptyDataTemplate>
Nenhum registro foi encontrado!
</EmptyDataTemplate>
<SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
<HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" Font-Size="X-Small" Font-Names="Verdana" />
<AlternatingRowStyle BackColor="Gainsboro" />
<EmptyDataRowStyle Font-Bold="True" Font-Size="XX-Small" HorizontalAlign="Left" VerticalAlign="Middle" />
</asp:GridView> 
    <br />
</fieldset>
    &nbsp;&nbsp;&nbsp;&nbsp;<br />
     <br /><br />
     
 <fieldset id="Fieldset1" style="text-align: left; width: 720px; height: 200px;">
     <br />
     <table>
         <tr style="background-color: #000084">
             <td style="width: 100px; height: 12px">
                 <strong><span style="font-size: 8pt; color: #ffffff">Código</span></strong></td>
             <td style="width: 209px; height: 12px">
                 <strong><span style="font-size: 8pt; color: #ffffff">Descrição</span></strong></td>
             <td style="width: 100px; height: 12px">
                 <strong><span style="font-size: 8pt; color: #ffffff">Código</span></strong></td>
             <td style="width: 222px; height: 12px">
                 <strong><span style="font-size: 8pt; color: #ffffff">Descrição</span></strong></td>
         </tr>
         <tr style="background-color: #e8f1ff">
             <td style="width: 100px">
                 <span style="font-size: 8pt">
                AVSR</span></td>
             <td style="width: 209px">
                 <span style="font-size: 8pt">
                A Avisar</span></td>
             <td style="width: 100px">
                 <span style="font-size: 8pt">
                PSCB</span></td>
             <td style="width: 222px">
                 <span style="font-size: 8pt">
                Não Avisar - Proposta sem cobertura</span></td>
         </tr>
         <tr style="background-color: #e8f1ff">
             <td style="width: 100px; height: 12px">
                 <span style="font-size: 8pt">
                RNLS</span></td>
             <td style="width: 209px; height: 12px">
                 <span style="font-size: 8pt">
                Solicitação de Reanálise</span></td>
             <td style="width: 100px; height: 12px">
                 <span style="font-size: 8pt">
                NAVS</span></td>
             <td style="width: 222px; height: 12px">
                 <span style="font-size: 8pt">Não Avisar</span></td>
         </tr>
     </table>
    <br />
    &nbsp;<br />
    <asp:Button ID="btnAvisar" runat="server" CssClass="Botao"
                Text="Avisar" OnClick="btnAvisar_Click" />
    <asp:Button ID="btnNaoAvisar" runat="server" CssClass="Botao"
                Text="Não Avisar" ValidationGroup="vldsResumo" OnClick="btnNaoAvisar_Click" />
    <asp:Button ID="btnReanalize" runat="server" CssClass="Botao"
                Text="Reanalise" ValidationGroup="vldsResumo" OnClick="btnReanalize_Click" /></fieldset>
     <br />
     <br />
     <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="1">
         <ProgressTemplate>
             <div class="esmaecer">
                 <div class="icon">
                 </div>
             </div>
         </ProgressTemplate>
     </asp:UpdateProgress>
    <br />
    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnVoltar" runat="server" CssClass="Botao"
                Text="<< Voltar" OnClick="btnVoltar_Click" />
            <asp:Button ID="btnContinuar" runat="server" CssClass="Botao"
                Text="Continuar >>" ValidationGroup="vldsResumo" OnClick="btnContinuar_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />

</ContentTemplate>
 </asp:UpdatePanel>

</asp:Content>