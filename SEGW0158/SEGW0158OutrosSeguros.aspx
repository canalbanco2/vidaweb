﻿<%@ Page Language="vb" EnableViewStateMac="false"  AutoEventWireup="false" Codebehind="SEGW0158OutrosSeguros.aspx.vb"
    Inherits="SEGW0158.SEGW0158OutrosSeguros" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SUSAB - Aviso de Sinistro RE - Outros Seguros</title>
    <link href="css/Sinistro.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src='<%= ResolveUrl("~/js/SEGW0158Scripts.js") %>'></script>

</head>
<body>
    <form id="form1" runat="server">
      <div id="Div2" style="width: 900px; text-align: left; height: 60px">
            <div class="n900">
                  <div class="n900">
                    <fieldset class="n100" style="text-align: center; background-color:  #fffff0;">
                        <asp:LinkButton ID="LkbPesquisa" Enabled="false" CssClass="BtnNavega" runat="server"
                           >Pausar Processo</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n100" style="text-align: center; ">
                        <asp:LinkButton ID="LkbDadosCliente" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Dados do Cliente</asp:LinkButton></fieldset>
                    <fieldset style="width: 70px; text-align: center; background-color:  #fffff0;">
                        <asp:LinkButton ID="LkbEvento" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Evento</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n110" style="text-align: center; background-color:  #fffff0;">
                       <asp:LinkButton ID="LkbSolicitante" Enabled="false" CssClass="BtnNavega"
                            runat="server">Dados Solicitante</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n70" style=" text-align: center; background-color: #fffff0;">
                        <asp:LinkButton ID="LkbAgencia" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Agência</asp:LinkButton></fieldset>
                    <fieldset class="n100" style=" text-align: center; background-color: #fffff0;">
                       <asp:LinkButton ID="LkbSinistroCA" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Analise de Sinistro</asp:LinkButton></fieldset>
                    <fieldset class="n110" style=" text-align: center; background-color: #fffff0; ">
                       <asp:LinkButton ID="LkbObjsegurado" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Objeto Segurado</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n100" style=" text-align: center; background-color:#F0E68C;">
                        <asp:LinkButton ID="LkbOutrosSeguros" Enabled="false" CssClass="BtnNavega"
                            runat="server">Outros Seguros</asp:LinkButton></fieldset>
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="width: 100px; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu " style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 110px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 110px; border: none; background: url(Image/menu_on_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
            </asp:ScriptManager>
        </div>
        <asp:UpdatePanel ID="upd_Filtro" runat="server">
            <ContentTemplate>
                <asp:Panel runat="server" ID="Panel11" Width="750px">
                    <fieldset style="width: 790px; height: 80px; font-size: 8pt;">
                        <legend class="TitCaixa"><span>Outros Seguros para o mesmo Bem</span></legend>
                        <br />
                        <div id="Panel" style="height: auto; text-align: center; font-size: 8pt;">
                            <asp:Panel ID="seguros" runat="server">
                                <div class="n750">
                                    <div class="n200 divLeft">
                                        <asp:Label CssClass="txtLabel divLeft n50" ID="lblApolice" runat="server" Text="Apólice"></asp:Label>
                                        <asp:TextBox ID="txtApolice" CssClass="txtCampo divLeft n100" runat="server" MaxLength="9"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="MEApolice" runat="server" Mask="999999999" MaskType="Number"
                                            TargetControlID="txtApolice" InputDirection="righttoleft" AutoComplete="false"
                                            CultureName="pt-BR" ClearMaskOnLostFocus="true">
                                        </cc1:MaskedEditExtender>
                                    </div>
                                    <div class="n350 divLeft">
                                        <asp:Label CssClass="txtLabel divLeft n150" ID="lblNomeSeguradora" runat="server"
                                            Text="Nome da Seguradora">
                                        </asp:Label>
                                        <asp:TextBox ID="txtNomeSeguradora" CssClass="txtCampo divLeft n100" runat="server"
                                            MaxLength="60"></asp:TextBox>
                                    </div>
                                    <div class="BotaoFora n200 divLeft">
                                        <div class="BotaoE">
                                        </div>
                                        <asp:Button runat="server" ID="btnIncluir" Text="Incluir" CssClass="BotaoM n120" />
                                        <div class="BotaoD">
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </fieldset>
                    <br />
                    <br />
                    <fieldset style="width: 790px; height: 200px; text-align: center; font-size: 8pt;">
                        <div class="floatLeft">
                            <br />
                            <asp:GridView ID="GvApolices" runat="server" ScrollBars="Both" Width="95%" AutoGenerateColumns="False">
                                <PagerStyle Font-Size="Large" />
                                <EmptyDataTemplate>
                                    Nenhuma Apólice Inclusa.
                                </EmptyDataTemplate>
                                <PagerStyle Font-Size="Large" CssClass="pager" />
                                <SelectedRowStyle CssClass="txtTabela3" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="header_tabela" />
                                <AlternatingRowStyle CssClass="txtTabela2" />
                                <RowStyle CssClass="txtTabela" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <Columns>
                                    <asp:CommandField ButtonType="Image" SelectImageUrl="~/Image/icon_bseta_menu_unsel.gif"
                                        SelectText="" ShowSelectButton="True" />
                                    <asp:BoundField DataField="apolice" HeaderText="Ap&#243;lice" SortExpression="apolice" />
                                    <asp:BoundField DataField="nomeSeguradora" HeaderText="Nome da Seguradora" SortExpression="nomeSeguradora" />
                                    <asp:CommandField DeleteText="Excluir" ShowCancelButton="False" ShowDeleteButton="True" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </fieldset>
                    <br />
                </asp:Panel>
                <div class="n900">
                    <br />
                    <div class="BotaoFora n325 divLeft">
                        <div class="BotaoE">
                        </div>
                        <asp:Button ID="btnVoltar" runat="server" CssClass="BotaoM n120" Text="<< Voltar"
                            Visible="true" />
                        <div class="BotaoD">
                        </div>
                    </div>
                    <div class="BotaoFora n325 divLeft">
                        <div class="BotaoE">
                        </div>
                        <asp:Button ID="btnSalvar" runat="server" CssClass="BotaoM n120" Text="Salvar" Visible="true" />
                        <div class="BotaoD">
                        </div>
                    </div>
                    <div class="BotaoFora n300 divLeft">
                        <div class="BotaoE">
                        </div>
                        <asp:Button ID="btnContinuar" runat="server" CssClass="BotaoM n120" Text="Continuar >>"
                            Visible="true" />
                        <div class="BotaoD">
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="1" AssociatedUpdatePanelID="upd_Filtro">
            <ProgressTemplate>
                <div class="esmaecer">
                    <div class="icon">
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </form>
</body>
</html>
