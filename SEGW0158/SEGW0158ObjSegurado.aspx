﻿<%@ Page Language="vb" EnableViewStateMac="false"  AutoEventWireup="false" Codebehind="SEGW0158ObjSegurado.aspx.vb"
    Inherits="SEGW0158.SEGW0158ObjSegurado" Title="SUSAB - Aviso de Sinistro RE - Objeto Segurado" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UserControls/UCObservacoes.ascx" TagName="Observacoes" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"  >
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SUSAB - Aviso de Sinistro RE - Objeto Segurado</title>
    <link href="CSS/Sinistro.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src='<%= ResolveUrl("~/js/SEGW0158Scripts.js") %>'></script>

</head>
<body>
    <form id="form1" runat="server">
      <div id="Div2" style="width: 900px; text-align: left; height: 60px">
            <div class="n900">
                 <div class="n900">
                    <fieldset class="n100" style="text-align: center; background-color:  #fffff0;">
                        <asp:LinkButton ID="LkbPesquisa" Enabled="false" CssClass="BtnNavega" runat="server"
                           >Pausar Processo</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n100" style="text-align: center; ">
                        <asp:LinkButton ID="LkbDadosCliente" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Dados do Cliente</asp:LinkButton></fieldset>
                    <fieldset style="width: 70px; text-align: center; background-color:  #fffff0;">
                        <asp:LinkButton ID="LkbEvento" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Evento</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n110" style="text-align: center; background-color:  #fffff0;">
                       <asp:LinkButton ID="LkbSolicitante" Enabled="false" CssClass="BtnNavega"
                            runat="server">Dados Solicitante</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n70" style=" text-align: center; background-color: #fffff0;">
                        <asp:LinkButton ID="LkbAgencia" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Agência</asp:LinkButton></fieldset>
                    <fieldset class="n100" style=" text-align: center; background-color: #fffff0;">
                       <asp:LinkButton ID="LkbSinistroCA" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Analise de Sinistro</asp:LinkButton></fieldset>
                    <fieldset class="n110" style=" text-align: center; background-color:#F0E68C;">
                       <asp:LinkButton ID="LkbObjsegurado" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Objeto Segurado</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n100" style=" text-align: center;">
                        <asp:LinkButton ID="LkbOutrosSeguros" Enabled="false" CssClass="BtnNavega"
                            runat="server">Outros Seguros</asp:LinkButton></fieldset>
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="width: 100px; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu " style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 110px; border: none; background: url(Image/menu_on_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 110px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="upd_Filtro" runat="server">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnContinuarQuest" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnVoltarQuest" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnSair" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnCalcular" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnVoltarMotReanalise" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnContinuarMotReanalise" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <fieldset runat="server" id="PanelProposta" style="width: 790px; height: auto; text-align: center;">
                    <legend class="TitCaixa"><span>Propostas</span></legend>
                    <div id="div_proposta" style="width: 750px; height: 100px">
                        <div class="n750 divLeft">
                            <div class="n250 divLeft">
                                <asp:Panel ID="d"  runat="server" Height="90px" ScrollBars="Vertical" Width="750px">
                                    <asp:GridView ID="grdPropostas" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        OnSelectedIndexChanged="grdPropostas_SelectedIndexChanged" Width="95%">
                                        <PagerStyle Font-Size="Large" CssClass="pager" />
                                        <SelectedRowStyle CssClass="txtTabela3" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="header_tabela" />
                                        <RowStyle CssClass="txtTabela" HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <AlternatingRowStyle CssClass="txtTabela2" />
                                        <Columns>
                                            <asp:CommandField ButtonType="Image" SelectImageUrl="~/image/icon_bseta_menu_unsel.gif"
                                                ShowSelectButton="True">
                                                <ItemStyle Width="5px" />
                                            </asp:CommandField>
                                            <asp:BoundField DataField="proposta_id" HeaderText="Proposta"></asp:BoundField>
                                            <asp:BoundField DataField="proposta_bb" HeaderText="Proposta BB"></asp:BoundField>
                                            <asp:BoundField DataField="produto_id" HeaderText="Produto"></asp:BoundField>
                                            <asp:BoundField DataField="ramo_id" HeaderText="Ramo"></asp:BoundField>
                                            <asp:BoundField DataField="apolice_id" HeaderText="Ap&#243;lice"></asp:BoundField>
                                            <asp:BoundField DataField="nome" HeaderText="Nome Produto"></asp:BoundField>
                                            <asp:BoundField DataField="subramo_id" HeaderText="SubRamo">
                                                <ItemStyle HorizontalAlign="Center" Width="40px" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle Font-Size="Large" />
                                        <EmptyDataTemplate>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    <br />
                                </asp:Panel>
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset id="obj_segurado" runat="server" style="text-align: center; width: 790px;
                    height: 100px;">
                    <legend class="TitCaixa">Objeto Segurado</legend>
                    <div id="div_objeto_segurado" style="width: 750px; height: 30px">
                        <div class="n750">
                            <div class="n750 divLeft">
                                <asp:TextBox ID="txtObjetoSegurado" CssClass="txtCampo divLeft n700" Style="height: 15px"
                                    Width="100%" runat="server"></asp:TextBox>
                            </div>
                        </div>
                </fieldset>
                <fieldset id="desc_objeto_segurado" runat="server" style="text-align: center; width: 790px;
                    height: 200px;">
                    <legend class="TitCaixa">Descrição do Objeto Segurado</legend>
                    <div id="div_desc_objeto_segurado" style="width: 750px; height: 140px">
                        <br />
                        <asp:Panel ID="panel_desc_objeto_segurado" runat="server" Height="85%" ScrollBars="Vertical"
                            Width="98%">
                            <asp:GridView ID="grdDescrObjSegurado" runat="server" Font-Size="8pt" PageSize="3">
                                <PagerStyle Font-Size="Large" CssClass="pager" />
                                <SelectedRowStyle CssClass="txtTabela3" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="header_tabela" />
                                <RowStyle CssClass="txtTabela" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <AlternatingRowStyle CssClass="txtTabela2" />
                                <EmptyDataTemplate>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </asp:Panel>
                    </div>
                </fieldset>
                <fieldset runat="server" style="text-align: center; width: 790px; height: 180px">
                    <legend class="TitCaixa">Coberturas</legend>
                    <div class="n750">
                        <div class="n750 divLeft">
                            <label for="ddlCobAtingida" class="txtLabel divLeft n200">
                                Cobertura Afetada:
                            </label>
                            <asp:DropDownList ID="ddlCobAtingida" AutoPostBack="true" runat="server" CssClass="txtCampo divLeft n500"
                                OnSelectedIndexChanged="ddlCobAtingida_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <div class="n750 divLeft">
                            <label for="txtFranquia" class="txtLabel divLeft n200">
                                Franquia:
                            </label>
                            <asp:TextBox ID="txtFranquia" runat="server" CssClass="txtCampo divLeft n500" ValidationGroup="val"></asp:TextBox>
                        </div>
                        <div class="n750 divLeft">
                            <div class="n500 divLeft">
                                <label for="txtValPrejuizo" class="txtLabel divLeft n200">
                                    Valor do Prejuízo:
                                </label>
                                <asp:TextBox ID="txtValPrejuizo" runat="server" Text="" ValidationGroup="val" onkeypress="ValidarNumerico2();" onKeydown="Formata(this,20,event,2)"
                                    CssClass="txtCampo divLeft n260" MaxLength="15"></asp:TextBox>                                  
                            </div>
                            <div class="n250divLeft">
                                <label for="txtValIS" class="txtLabel divLeft n80">
                                    Valor da IS:
                                </label>
                                <asp:TextBox ID="txtValIS" runat="server" CssClass="txtCampo divLeft n120" ValidationGroup="val" OnBlur="ValidarNumerico2();" onkeypress="ValidarNumerico2();" onKeydown="Formata(this,20,event,2)"
                                    Enabled="False"></asp:TextBox>
                            </div>
                        </div>
                        <div class="n750 divLeft">
                            <div id="reintegracao_is" class="txtLabel divLeft n750" runat="server">
                                <label id="lblReintegra" for="radReintegracao" runat="server" class="txtLabel divLeft n200">
                                </label>
                                <span class="txtLabel divLeft">Reintegração da IS?</span>
                                <asp:RadioButtonList ID="radReintegracao" runat="server" CssClass="txtCampo divLeft n150"
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Value="S">Sim</asp:ListItem>
                                    <asp:ListItem Value="N">Não</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="n900 divLeft">
                            <label id="Label1" runat="server" class="txtLabel divLeft n200">
                            </label>
                            <div class="BotaoFora n200 divLeft ">
                                <div class="BotaoE">
                                </div>
                                <asp:Button ID="btnAdicionar" runat="server" CssClass="BotaoM n120" OnClick="btnAdicionar_Click"
                                    Text="Adicionar" Width="120px" />
                                <div class="BotaoD">
                                </div>
                            </div>
                            <div class="BotaoFora n200 divLeft ">
                                <div class="BotaoE">
                                </div>
                                <asp:Button ID="btnAlterar" runat="server" CssClass="BotaoM n120&#13;&#10;                                          "
                                    OnClick="btnAlterar_Click" Text="Alterar" Width="120px" Enabled="False" />
                                <div class="BotaoD">
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <br />
                <fieldset id="propostas_do_aviso" runat="server" style="text-align: center; width: 100%;">
                    <legend class="TitCaixa">Propostas do Aviso</legend>
                    <asp:Panel ID="Panel2" runat="server" Style="text-align: center;" Height="200px"
                        ScrollBars="Vertical" Width="790px">
                        <asp:GridView ID="grdPropostasAviso" Width="2100px" CssClass="divLeft" runat="server"
                            AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True">
                            <PagerStyle Font-Size="Large" CssClass="pager" />
                            <SelectedRowStyle CssClass="txtTabela3" />
                            <HeaderStyle CssClass="header_tabela" />
                            <AlternatingRowStyle CssClass="txtTabela2" />
                            <RowStyle CssClass="txtTabela" />
                            <PagerStyle Font-Size="Large" CssClass="pager" />
                            <Columns>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImgBtnDetalhes" runat="server" CommandName="Select" CommandArgument='<%# Eval("proposta_id") %>'
                                            ImageUrl="~/Image/icon_bseta_menu_unsel.gif" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="False" Visible="False">
                                    <ItemTemplate>
                                        <asp:Button ID="btnDetalhes" runat="server" Text="..." Visible="False" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Proposta" DataField="proposta_id">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Proposta BB" DataField="Proposta_BB">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Produto" DataField="cod_produto">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Nome do Produto" DataField="nom_produto">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" Width="200px" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Ap&#243;lice" DataField="apolice_id">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Ramo" DataField="ramo_id">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Objeto Segurado" DataField="nom_objeto_segurado">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Evento do Sinistro" DataField="nom_evento_sinistro">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Valor do Prezu&#237;zo" DataField="valor_prejuizo">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Cobertura" DataField="dsc_cobertura">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Justify" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Código do objeto segurado" DataField="cod_objeto_segurado">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="C&#243;digo da Cobertura" DataField="cod_cobertura_afetada">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Valor da IS" DataField="valor_is">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Processa Reintegra&#231;&#227;o de IS" DataField="Processa_Reintegracao_IS">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                Nenhum registro foi encontrado!
                            </EmptyDataTemplate>
                            <HeaderStyle Font-Size="X-Small" Font-Names="Verdana" CssClass="header_tabela" HorizontalAlign="Left"
                                VerticalAlign="Middle" />
                            <EmptyDataRowStyle Font-Bold="True" Font-Size="XX-Small" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:GridView>
                    </asp:Panel>
                </fieldset>
                <br />
                <fieldset style="width: 950px; text-align: center; height: auto; border: none;">
                    <div class="n950">
                        <div class="n950 divLeft">
                            <div class="BotaoFora n325 divLeft alignLeft">
                                <div class="BotaoE">
                                </div>
                                <asp:Button ID="btnVoltar" runat="server" CssClass="BotaoM n120" Text="<< Voltar"
                                    Visible="true" Width="120px" OnClick="btnVoltar_Click" />
                                <div class="BotaoD">
                                </div>
                            </div>
                            <div class="BotaoFora n325 divLeft alignLeft">
                                <div class="BotaoE">
                                </div>
                                <asp:Button ID="btnSalvar" runat="server" CssClass="BotaoM n120" Text="Salvar" ValidationGroup="filter"
                                    Visible="true" Width="120px" OnClick="btnSalvar_Click" />
                                <div class="BotaoD">
                                </div>
                            </div>
                            <div class="BotaoFora n300 divLeft alignLeft">
                                <div class="BotaoE">
                                </div>
                                <asp:Button ID="btnContinuar" runat="server" CssClass="BotaoM n120" Text="Continuar >>"
                                   ValidationGroup="filter" Visible="true" Width="120px" OnClick="btnContinuar_Click" />
                                <asp:CustomValidator ID="cvtFields" runat="server" Display="None" ErrorMessage="Informe pelo menos um filtro para pesquisa."
                                    ValidationGroup="filter">
                                </asp:CustomValidator>
                                <div class="BotaoD">
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                &nbsp;
                <asp:Panel ID="pnQuestionario" runat="server" Style="display: none; font-size: 8pt;
                    background-color: White; height: 400px;" Width="790px">
                    <fieldset id="fieldset2" runat="server" class="popup_Container" style="width: 95%;
                        text-align: center; height: 400px">
                        <asp:Panel ID="pnBarraTitulo" runat="server" CssClass="popup_Titlebar" >
                            <div id="fieldset_questionario" runat="server" class="popup_Container" style="width: 100%;">
                                <div style="font-size: 8pt; background-color: #F0E68C; height: 20px; width: 100%">
                                    <span class="TitCaixa">Questionário de Aviso de Sinistro</span>
                                </div>
                                <br />
                            </div>
                        </asp:Panel>
                        <br />
                        <br />
                        <div class="n900" style="text-align: center;">
                            <div class="popup_Container">
                                <div class="BotaoFora n200 divLeft alignLeft">
                                    <div class="BotaoE">
                                    </div>
                                    <asp:Button ID="btnVoltarQuest" runat="server" CssClass="BotaoM n120" Text="<< Voltar"
                                        Visible="true" />
                                    <div class="BotaoD">
                                    </div>
                                </div>
                                <div class="BotaoFora n200 divLeft alignLeft">
                                    <div class="BotaoE">
                                    </div>
                                    <asp:Button ID="btnContinuarQuest" runat="server" CssClass="BotaoM n120" Text="Continuar >>"
                                        Visible="true" ValidationGroup="filter" />
                                    <div class="BotaoD">
                                    </div>
                                </div>
                                <div class="BotaoFora n200 divLeft alignLeft">
                                    <div class="BotaoE">
                                    </div>
                                    <asp:Button ID="btnSair" runat="server" CssClass="BotaoM n120" Text="Sair" Visible="true"
                                        OnClick="btnSair_Click" ValidationGroup="filterSair" />
                                    <asp:CustomValidator ID="cvSair" runat="server" ClientValidationFunction="confirmaSair"
                                        Display="None" ErrorMessage="Informe pelo menos um filtro para pesquisa." ValidationGroup="filterSair">
                                    </asp:CustomValidator>
                                    <div class="BotaoD">
                                    </div>
                                </div>
                                <div class="BotaoFora n190 divLeft alignLeft">
                                    <div class="BotaoE">
                                    </div>
                                    <asp:Button ID="btnCalcular" runat="server" CssClass="BotaoM n120" Text="Calcular"
                                        Visible="true" ValidationGroup="filter" />
                                    <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="ValidarQuestionario"
                                        Display="None" ErrorMessage="Informe pelo menos um filtro para pesquisa." ValidationGroup="filter">
                                    </asp:CustomValidator>
                                    <div class="BotaoD">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <asp:HiddenField ID="hdd_0" runat="server" />
                            <asp:HiddenField ID="hdd_1" runat="server" />
                            <asp:HiddenField ID="hdd_2" runat="server" />
                            <asp:HiddenField ID="hdd_3" runat="server" />
                            <asp:HiddenField ID="hdd_4" runat="server" />
                            <asp:HiddenField ID="hdd_5" runat="server" />
                            <asp:HiddenField ID="hdd_6" runat="server" />
                            <asp:HiddenField ID="hdd_7" runat="server" />
                            <asp:HiddenField ID="hdd_8" runat="server" />
                            <asp:HiddenField ID="hdd_9" runat="server" />
                            <asp:HiddenField ID="hdd_10" runat="server" />
                            <asp:HiddenField ID="hdd_11" runat="server" />
                            <asp:HiddenField ID="hdd_12" runat="server" />
                            <asp:HiddenField ID="hdd_13" runat="server" />
                            <asp:HiddenField ID="hdd_14" runat="server" />
                            <asp:HiddenField ID="hdd_15" runat="server" />
                            <asp:HiddenField ID="hdd_16" runat="server" />
                            <asp:HiddenField ID="hdd_17" runat="server" />
                            <asp:HiddenField ID="hdd_18" runat="server" />
                            <asp:HiddenField ID="hdd_19" runat="server" />
                            <asp:HiddenField ID="hdd_20" runat="server" />
                            <asp:HiddenField ID="hdd_21" runat="server" />
                            <asp:HiddenField ID="hdd_22" runat="server" />
                            <asp:HiddenField ID="hdd_23" runat="server" />
                            <asp:HiddenField ID="hdd_24" runat="server" />
                            <asp:HiddenField ID="hdd_25" runat="server" />
                            <asp:HiddenField ID="hdd_26" runat="server" />
                            <asp:HiddenField ID="hdd_27" runat="server" />
                            <asp:HiddenField ID="hdd_28" runat="server" />
                            <asp:HiddenField ID="hdd_29" runat="server" />
                            <asp:HiddenField ID="hdd_30" runat="server" />
                            <asp:HiddenField ID="hdd_31" runat="server" />
                            <asp:HiddenField ID="hdd_32" runat="server" />
                            <asp:HiddenField ID="hdd_33" runat="server" />
                            <asp:HiddenField ID="hdd_34" runat="server" />
                            <asp:HiddenField ID="hdd_35" runat="server" />
                            <asp:HiddenField ID="hdd_36" runat="server" />
                            <asp:HiddenField ID="hdd_37" runat="server" />
                            <asp:HiddenField ID="hdd_38" runat="server" />
                            <asp:HiddenField ID="hdd_39" runat="server" />
                            <asp:HiddenField ID="hdd_40" runat="server" />
                        </div>
                    </fieldset>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="mpeQuestionario" TargetControlID="pnBarraTitulo" PopupControlID="pnQuestionario"                 
                    BackgroundCssClass="esmaecer" runat="server" />
                    <br />
                <asp:Panel ID="pnMotivoReanalise" runat="server" Style="display: none; text-align: center;
                    width: 790px; background-color: White; height: 250px" >
                    <fieldset id="fieldset1" runat="server" class="popup_Container" style="width: 95%;
                        height: 250px">
                        <asp:Panel ID="pnBarraTituloMot" runat="server">
                            <div style="font-size: 8pt; background-color: #F0E68C; height: 20px; width: 100%">
                                <span class="TitCaixa">Motivo de Reanálise</span>
                            </div>
                            <br />
                            <div class="n750">
                                <div class="n750 divLeft">
                                    <label for="ddlMotivoReanalise" class="txtLabel divLeft n150">
                                        Motivo de Reanálise:
                                    </label>
                                    <asp:DropDownList ID="ddlMotivoReanalise" runat="server" AutoPostBack="true" CssClass="txtCampo divLeft n510"
                                        TabIndex="8">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <uc2:Observacoes ID="Observacoes1" runat="server"></uc2:Observacoes>
                            </asp:Panel>
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <div class="n750">
                                <div class="n750 divLeft">
                                    <div class="n750 divLeft">
                                        <label for="Separador_" class="txtLabel divLeft n150">
                                        </label>
                                        <div class="BotaoFora n200 divLeft alignLeft">
                                            <div class="BotaoE">
                                            </div>
                                            <asp:Button ID="btnVoltarMotReanalise" runat="server" CssClass="BotaoM n120" Text="<< Voltar"
                                                Visible="true" OnClick="btnVoltarMotReanalise_Click" />
                                            <div class="BotaoD">
                                            </div>
                                        </div>
                                        <div class="BotaoFora n200 divLeft alignLeft">
                                            <div class="BotaoE">
                                            </div>
                                            <asp:Button ID="btnContinuarMotReanalise" runat="server" CssClass="BotaoM n120" Text="Continuar >>"
                                                Visible="true" ValidationGroup="filterMotivo" />
                                            <div class="BotaoD">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                    </fieldset>
                </asp:Panel>                
                <cc1:ModalPopupExtender ID="mpeMotivoReanalise" TargetControlID="pnBarraTituloMot" PopupControlID="pnMotivoReanalise"
                    
                    BackgroundCssClass="esmaecer" runat="server" >
                </cc1:ModalPopupExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upd_Filtro"
            DisplayAfter="0">
            <ProgressTemplate>
                <div class="esmaecer" style="left: 0px; width: 100%; top: 0px">
                    <div class="icon">
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <br />
        <input id="hddConfirm" runat="server" type="hidden" />
    </form>
</body>
</html>
