<%@ Page Language="vb" EnableViewStateMac="false"  AutoEventWireup="false" Codebehind="SEGW0158ResumoSinistroRE.aspx.vb"
    Inherits="SEGW0158.SEGW0158ResumoSinistroRE" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="~/UserControls/UCSinistrado.ascx" TagName="UCSinistrados" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/UCOcorrenciaResumo.ascx" TagName="UCOcorrencia"
    TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/UCDadosSolicitanteVida.ascx" TagName="DadosSolicitante"
    TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/UCAvisos.ascx" TagName="DadosAviso" TagPrefix="uc3" %>
<%@ Register Src="UserControls/UCVistoria.ascx" TagName="UCVistoria" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SUSAB - Aviso de Sinistro RE - Resumo Sinistro</title>
    <link href="CSS/Sinistro.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="js/SEGW0158Scripts.js"></script>

</head>
<body id="hdnMessage">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
             <fieldset runat="server" id="FildBorder" style="width: 751px; text-align: lft; height: 105%;">
                        <legend class="TitCaixa"><span>Resumo</span></legend>                        
                <cc1:TabContainer ID="TabResumo" runat="server" Width="750px" ActiveTabIndex="0">
                    <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            Ag�ncia
                        </HeaderTemplate>
                        <ContentTemplate>
                            <uc1:UCSinistrados ID="UCAgencia" runat="server" DVAgenciaReadOnly="true" CodigoAgenciaReadOnly="true"
                                BairroAgenciaReadOnly="true" EnderecoAgenciaReadOnly="true" MunicipioReadOnly="true"
                                NomeAgenciaReadOnly="true"></uc1:UCSinistrados>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <uc2:UCOcorrencia ID="UCOcorrencia1" runat="server" DataOcorrenciaReadOnly="true"
                                HoraOcorrenciaReadOnly="true" ObservacaoReadOnly="true" ReanaliseReadOnly="true" />
                        </ContentTemplate>
                        <HeaderTemplate>
                            Ocorr�ncia
                        </HeaderTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel3" runat="server" HeaderText="TabPanel3">
                        <ContentTemplate>
                            <uc3:DadosSolicitante ID="DadosSolicitante1" runat="server" BairroReadOnly="true"
                                CEPReadOnly="true" DDD1ReadOnly="true" DDD2ReadOnly="true" DDD3ReadOnly="true"
                                DDD4ReadOnly="true" DDD5ReadOnly="true" EmailReadOnly="true" EnderecoReadOnly="true"
                                NomeReadOnly="true" Ramal1ReadOnly="true" Ramal2ReadOnly="true" Ramal3ReadOnly="true"
                                Ramal4ReadOnly="true" Ramal5ReadOnly="true" Telefone1ReadOnly="true" Telefone2ReadOnly="true"
                                Telefone3ReadOnly="true" Telefone4ReadOnly="true" Telefone5ReadOnly="true" UFEnabled="true" />
                        </ContentTemplate>
                        <HeaderTemplate>
                            Solicitante
                        </HeaderTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel4" runat="server" HeaderText="TabPanel4">
                        <ContentTemplate>
                            <fieldset style="width: 98%; text-align: center; height: 150px;">
                                <legend class="TitCaixa"><span>Propostas do Cliente</span></legend>
                                <asp:Panel ID="pnlGridPropostas2" ScrollBars="Both" runat="server" Width="100%" Height="80%">
                                    <asp:GridView ID="grdPropostasAfetadas2" runat="server" AutoGenerateColumns="False"
                                        AllowPaging="True" Font-Size="8pt" Width="1500">
                                        <AlternatingRowStyle CssClass="txtTabela2" Font-Size="8pt" />
                                        <EmptyDataTemplate>
                                            Nenhuma Proposta Encontrada!
                                        </EmptyDataTemplate>
                                        <HeaderStyle Font-Size="8pt" CssClass="header_tabela" HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <PagerStyle Font-Size="8pt" CssClass="pager" />
                                        <RowStyle Font-Size="8pt" CssClass="txtTabela" HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <SelectedRowStyle Font-Size="8pt" CssClass="txtTabela3" />
                                    </asp:GridView>
                                </asp:Panel>
                            </fieldset>
                            <br />
                            <fieldset style="width: 98%; text-align: center; height: 100px;">
                                <legend class="TitCaixa"><span>Legenda</span></legend>
                                <table class="box" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td class="header_legenda">
                                            <strong>C�digo</strong></td>
                                        <td class="header_legenda">
                                            <strong>Descri��o</strong></td>
                                        <td class="header_legenda">
                                            <strong>C�digo</strong></td>
                                        <td class="header_legenda">
                                            <strong>Descri��o</strong></td>
                                    </tr>
                                    <tr class="itemLegenda">
                                        <td style="width: 100px">
                                            AVSR</td>
                                        <td style="width: 209px">
                                            A Avisar</td>
                                        <td style="width: 100px">
                                            PSCB</td>
                                        <td style="width: 222px">
                                            N�o Avisar - Proposta sem cobertura</td>
                                    </tr>
                                    <tr class="itemLegenda  ">
                                        <td>
                                            RNLS</td>
                                        <td>
                                            Solicita��o de Rean�lise</td>
                                        <td>
                                            NAVS</td>
                                        <td>
                                            N�o Avisar</td>
                                    </tr>
                                </table>
                                <br />
                            </fieldset>
                        </ContentTemplate>
                        <HeaderTemplate>
                            Propostas Afetadas
                        </HeaderTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel5" runat="server" HeaderText="TabPanel5">
                        <ContentTemplate>
                            <uc3:DadosAviso ID="DadosAviso1" runat="server" />
                        </ContentTemplate>
                        <HeaderTemplate>
                            Avisos
                        </HeaderTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel6" runat="server" HeaderText="TabPanel6">
                        <ContentTemplate>
                            <uc4:UCVistoria ID="UCVistora" runat="server" />
                        </ContentTemplate>
                        <HeaderTemplate>
                            Local de Risco / Vistoria
                        </HeaderTemplate>
                    </cc1:TabPanel>
                </cc1:TabContainer>
                <br />
                </fieldset>
                <br />
                <input id="Hidden1" type="hidden" /><br />
                <asp:HiddenField ID="hddConfirm" runat="server" />
        <div class="n800">
            <div class="BotaoFora n375 divLeft alignLeft">
                <div class="BotaoE">
                </div>
                <asp:Button ID="btnVoltar" runat="server" Text="<< Voltar" CssClass="BotaoM n120" />
                <div class="BotaoD">
                </div>
            </div>
            <div class="BotaoFora n230 divLeft alignLeft">
                <div class="">
                </div>
                <asp:Button Visible="false" ID="Button1" runat="server" Text="<< Voltar" CssClass="BotaoM n120" />
                <div class="">
                </div>
            </div>
            <div class="BotaoFora n375 divLeft alignLeft">
                <div class="BotaoE">
                </div>
                <asp:Button ID="btnAvisar" runat="server" Text="Avisar" CssClass="BotaoM n120" ValidationGroup="ValidarCampos" />
                <div class="BotaoD">
                </div>
            </div>
        </div>
          <br />
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div class="esmaecer" style="left: 0px; width: 100%; top: 0px">
                            <div class="icon">
                            </div>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
