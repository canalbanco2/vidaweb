﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SEGW0158AplicErro.aspx.vb" Inherits="SEGW0158.SEGW0158AplicErro" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SUSAB - Aviso de Sinistro RE - Consulta Propostas Pendentes</title>
    <link href="CSS/Sinistro.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="JS/SEGW0158Scripts.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="botoes" runat="server" style="display: block; width: 790px; text-align: center;
            margin-top: 30px; margin-bottom: 20px;">
            <fieldset style="width: 600px; text-align: center; height: 20px">
                <div style="font-size: 8pt; background-color: #F0E68C; height: 20px; width: 100%">
                    <span class="TitCaixa">Seguros de Danos</span>
                </div>
                <!--<legend><span class="TitCaixa" style="text-align: right;">Seguros de Danos</span></legend>-->
            </fieldset>
            <fieldset style="width: 600px; text-align: center; height: 90px">
                <legend><span class="TitCaixa">Ocorreu um erro no sistema</span></legend>
                <div class="n600 divLeft">
                <!--    <label class="txtLabel divLeft n100" for="txtDataFinal"> -->
                        <span style="width: 550px; text-align: left;" class="txtLabel">Desculpe, Ocorreu um erro no sistema e foi reportado ou sua sessão foi encerrada, &nbsp;porem você podera continuar com o processo em sinistros pendentes na tela
                            Inicial. Click em Voltar!.</span>
                   <!-- </label> -->
                </div>
            </fieldset>
            <br />
            <br />
            <fieldset style=" border: none; width: 600px; text-align: center; height: 90px">
                <div class="n300 divLeft">
                    <div class="BotaoFora">
                        <div class="BotaoE">
                        </div>
                        <asp:Button ID="btnVoltar" runat="server" CssClass="BotaoM n120" Text="Voltar" />
                        <div class="BotaoD">
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </form>
</body>
</html>
