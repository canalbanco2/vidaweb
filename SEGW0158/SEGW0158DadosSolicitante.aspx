﻿<%@ Page Language="vb" EnableViewStateMac="false" AutoEventWireup="false" Codebehind="SEGW0158DadosSolicitante.aspx.vb"
    Inherits="SEGW0158.SEGW0158DadosSolicitante" EnableEventValidation="false" %>

<%@ Register Src="~/UserControls/UCDadosSolicitanteVida.ascx" TagName="DadosSolicitanteVida"
    TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SUSAB - Aviso de Sinistro RE - Dados do Solicitante</title>
    <link href="CSS/Sinistro.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src='<%= ResolveUrl("~/js/SEGW0158Scripts.js") %>'></script>

</head>
<body>
    <form id="form1" runat="server" defaultbutton="UCDadosSolicitanteVida1$ibtPesquisarCEP">
        <div id="Div2" style="width: 900px; text-align: LEFT; height: 60px">
            <div class="n900">
                <div class="n900">
                    <fieldset class="n100" style="text-align: center; background-color:   #fffff0;">
                        <asp:LinkButton ID="LkbPesquisa" Enabled="false" CssClass="BtnNavega" runat="server"
                           >Pausar Processo</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n100" style="text-align: center; ">
                        <asp:LinkButton ID="LkbDadosCliente" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Dados do Cliente</asp:LinkButton></fieldset>
                    <fieldset style="width: 70px; text-align: center; background-color:   #fffff0;">
                        <asp:LinkButton ID="LkbEvento" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Evento</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n110" style="text-align: center; background-color:  #F0E68C;">
                       <asp:LinkButton ID="LkbSolicitante" Enabled="false" CssClass="BtnNavega"
                            runat="server">Dados Solicitante</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n70" style=" text-align: center;">
                        <asp:LinkButton ID="LkbAgencia" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Agência</asp:LinkButton></fieldset>
                    <fieldset class="n110" style=" text-align: center;">
                       <asp:LinkButton ID="LkbSinistroCA" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Analise de Sinistro</asp:LinkButton></fieldset>
                    <fieldset class="n100" style=" text-align: center;">
                       <asp:LinkButton ID="LkbObjsegurado" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Objeto Segurado</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n100" style=" text-align: center;">
                        <asp:LinkButton ID="LkbOutrosSeguros" Enabled="false" CssClass="BtnNavega"
                            runat="server">Outros Seguros</asp:LinkButton></fieldset>
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="width: 100px; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu " style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_on_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 80px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 110px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 110px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600"
            EnableScriptGlobalization="true">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="upd_Filtro" runat="server">
            <Triggers>
            </Triggers>
            <ContentTemplate>
                <input type="hidden" runat="server" id="hfFluxo" />
                <div style="width: 730px; height: auto;">
                    <uc2:DadosSolicitanteVida ID="UCDadosSolicitanteVida1" runat="server"></uc2:DadosSolicitanteVida>
                </div>
                <br />
                <br />
                <fieldset class="n900" style="text-align: center; border: none;">
                    <div class="n900">
                        <div class="n900 divLeft">
                            <div class="BotaoFora n280 divLeft alignLeft">
                                <div class="BotaoE">
                                </div>
                                <asp:Button ID="btnVoltar" runat="server" Text="<< Voltar" CssClass="BotaoM n120"
                                    TabIndex="1001" />
                                <div class="BotaoD">
                                </div>
                            </div>
                            <div class="BotaoFora n300 divLeft alignLeft">
                                <div class="BotaoE">
                                </div>
                                <asp:Button ID="btnSalvar" runat="server" Text="Salvar" CssClass="BotaoM n120" ValidationGroup="val"
                                    TabIndex="1002" />
                                <div class="BotaoD">
                                </div>
                            </div>
                            <div class="BotaoFora n300 divLeft alignLeft">
                                <div class="BotaoE">
                                </div>
                                <asp:Button ID="btnContinuar" runat="server" Text="Continuar >>" CssClass="BotaoM n120"
                                    ValidationGroup="val" TabIndex="1003" />
                                <asp:CustomValidator ID="cvtFields" runat="server" Enabled="true" ClientValidationFunction="Validar_DadosSolicitante"
                                    Display="None" ErrorMessage="Existem campos a ser preenchidos." ValidationGroup="val">
                                </asp:CustomValidator>
                                <div class="BotaoD">
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <%-- <div style="width: 790px; height: auto;">
                    <div class="n264 divLeft alignLeft">
                        <div class="BotaoFora" style="text-align: center; width: 160px; float: left;">
                            <div class="BotaoE">
                            </div>
                            <asp:Button ID="btnVoltar" runat="server" Text="<< Voltar" CssClass="BotaoM n90"
                                TabIndex="31" />
                            <div class="BotaoD">
                            </div>
                        </div>
                        <div class="BotaoFora" style="text-align: center; width: 160px; float: left;">
                            <div class="BotaoE">
                            </div>
                            <asp:Button ID="btnSalvar" runat="server" Text="Salvar" CssClass="BotaoM n90" ValidationGroup="val"
                                TabIndex="30" />
                            <div class="BotaoD">
                            </div>
                        </div>
                        <div class="BotaoFora" style="text-align: center; width: 160px; float: left;">
                            <div class="BotaoE">
                            </div>
                            <asp:Button ID="btnContinuar" runat="server" CssClass="BotaoM n90" OnClick="btnContinuar_Click"
                                ValidationGroup="val" Text="Continuar >>" TabIndex="33" />
                            <asp:CustomValidator ID="cvtFields" runat="server" ClientValidationFunction="Validar_DadosSolicitante"
                                Display="None" ErrorMessage="Existem campos a ser preenchidos." ValidationGroup="val">
                            </asp:CustomValidator>
                            <div class="BotaoD">
                            </div>
                        </div>
                </div>--%>
                <div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="UCDadosSolicitanteVida1$ibtPesquisarCEP" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="1">
            <ProgressTemplate>
                <div class="esmaecer">
                    <div class="icon">
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </form>
</body>
</html>
