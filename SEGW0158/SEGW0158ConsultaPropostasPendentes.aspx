﻿<%@ Page Language="vb" EnableViewStateMac="false"  AutoEventWireup="false" Codebehind="SEGW0158ConsultaPropostasPendentes.aspx.vb"
    Inherits="SEGW0158.SEGW0158ConsultaPropostasPendentes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SUSAB - Aviso de Sinistro RE - Consulta Propostas Pendentes</title>
    <link href="CSS/Sinistro.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="JS/SEGW0158Scripts.js"></script>

</head>
<body>
    <form id="form1" runat="server" defaultbutton="btnConsultar">
        <asp:ScriptManager ID="ScriptManager" runat="server" AsyncPostBackTimeout="600">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="upFiltros" runat="server">
            <ContentTemplate>
                <div id="botoes" runat="server"  style="display: block; width: 790px; text-align: center; margin-top: 30px;
            margin-bottom: 20px;">
                    <fieldset style="width: 600px; text-align: center; height: 20px">
                        <div style="font-size: 8pt; background-color: #F0E68C; height: 20px; width: 100%">
                            <span class="TitCaixa">Seguros de Danos</span>
                        </div>
                        <!--<legend><span class="TitCaixa" style="text-align: right;">Seguros de Danos</span></legend>-->
                    </fieldset>
                    <fieldset style="width: 600px; text-align: center; height: 90px">
                        <legend><span class="TitCaixa">Qual Operação deseja realizar?</span></legend>
                        <br />
                        <label for="txtNome" class="txtLabel divLeft n70">
                        </label>
                        <div class="BotaoFora divLeft n250">
                            <div class="BotaoE">
                            </div>
                            <asp:Button ID="btnNovoSinistro" ToolTip="Efetua registro de um novo sinistro!" runat="server"
                                Text="Novo Sinistro" CssClass="BotaoM n180" />
                            <div class="BotaoD">
                            </div>
                        </div>
                        <div class="BotaoFora divLeft divLeft n250">
                            <div class="BotaoE">
                            </div>
                            <asp:Button ID="btnComunicados" runat="server" ToolTip="Recupera um sinistro que estava em processo de cadastro!"
                                CssClass="BotaoM n180" Text="Comunicados em Andamento" />
                            <div class="BotaoD">
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div id="mainn" runat="server" visible="false">
                    <fieldset class="n790">
                        <legend class="TitCaixa"><span>Comunicados em Andamento:</span></legend>
                        <div id="Panel" style="width: 750px; height: 100px">
                            <div class="n750">
                                <div class="n750 divLeft">
                                    <label for="txtNome" class="txtLabel divLeft n70">
                                        Nome:
                                    </label>
                                    <asp:TextBox ID="txtNome" runat="server" CssClass="txtCampo divLeft n400" MaxLength="60"></asp:TextBox>&nbsp;
                                </div>
                            </div>
                            <div class="n750 divLeft">
                                <div class="n300 divLeft">
                                    <label for="txtCPFCNPJ" class="txtLabel divLeft n70">
                                        CPF/CNPJ:
                                    </label>
                                    <asp:TextBox ID="txtCPFCNPJ" runat="server" CssClass="txtCampo divLeft" Width="81px"
                                        MaxLength="14"></asp:TextBox>&nbsp;
                                    <cc1:MaskedEditExtender ID="meeCPF_CNPJ" runat="server" Mask="999,999,999-99" TargetControlID="txtCPFCNPJ"
                                        CultureName="pt-BR" ClearMaskOnLostFocus="true">
                                    </cc1:MaskedEditExtender>
                                </div>
                            </div>
                            <div class="n750">
                                <div class="n750 divLeft">
                                    <label class="txtLabel divLeft n70" for="txtDataInicial">
                                        Intervalo:
                                    </label>
                                    <asp:TextBox ID="txtDataInicial" runat="server" CssClass="txtCampo divLeft" Width="60px"></asp:TextBox>
                                    <cc1:MaskedEditExtender ID="mmeDataInicial" runat="server" MaskType="Date" Mask="99/99/9999"
                                        TargetControlID="txtDataInicial" CultureName="pt-BR" ClearMaskOnLostFocus="true">
                                    </cc1:MaskedEditExtender>
                                    <label class="txtLabel divLeft n20" for="txtDataFinal">
                                        até
                                    </label>
                                    <asp:TextBox ID="txtDataFinal" runat="server" CssClass="txtCampo divLeft" Width="60px"></asp:TextBox>
                                    <cc1:MaskedEditExtender ID="mmeDataFinal" runat="server" MaskType="Date" Mask="99/99/9999"
                                        TargetControlID="txtDataFinal" CultureName="pt-BR" ClearMaskOnLostFocus="true">
                                    </cc1:MaskedEditExtender>
                                </div>
                            </div>
                            <div class="n750" style="height: 15px;">
                            </div>
                        </div>
                        <br />
                        <fieldset style="text-align: center; height: 50px; border: none;">
                            <div class="n750">
                                <div class="n300 divLeft">
                                    <label class="txtLabel divLeft n50" for="txtDataFinal">
                                    </label>
                                    <div class="BotaoFora">
                                        <div class="BotaoE">
                                        </div>
                                        <asp:Button ID="btnVoltar" runat="server" CssClass="BotaoM n120" Text="Voltar" />
                                        <div class="BotaoD">
                                        </div>
                                    </div>
                                </div>
                                <div class="n350 divLeft">
                                    <div class="BotaoFora">
                                        <div class="BotaoE">
                                        </div>
                                        <asp:Button ID="btnConsultar" runat="server" CssClass="BotaoM n120" Text="Consultar"
                                            ValidationGroup="filter" />
                                        <div class="BotaoD">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </fieldset>
                    <br />
                    <br />
                    <fieldset class="n790">
                        <legend class="TitCaixa"><span>Comunicados em Andamento:</span></legend>
                        <div style="margin-left: 10px; margin-right: 10px; margin-bottom: 30px; margin-top: 5px;">
                            <asp:GridView ID="GVwSinistrados" runat="server" Width="760px" AllowPaging="True"
                                AutoGenerateColumns="False">
                                <RowStyle CssClass="txtTabela" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <Columns>
                                    <asp:CommandField ButtonType="Image" SelectImageUrl="~/Image/icon_bseta_menu_unsel.gif"
                                        SelectText="Selecionar" ShowSelectButton="True"></asp:CommandField>
                                    <asp:BoundField DataField="CLIENTE_ID" HeaderText="C&#243;digo">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NOME_SOLICITANTE" HeaderText="Nome">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CPF_CNPJ_SOLICITANTE" HeaderText="CPF/CNPJ">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DATA_NASCIMENTO" HeaderText="Data de Nascimento" DataFormatString="{0:dd/MM/yyyy}">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DT_INCLUSAO" HeaderText="Data Inclus&#227;o" DataFormatString="{0:dd/MM/yyyy}">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CODIGO_PASSO" HeaderText="Passo">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CODIGO_FLUXO" HeaderText="Fluxo">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="SINISTRO_ID" HeaderText="sinistro_id" HtmlEncode="False">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="usuario" HeaderText="Usuario" HtmlEncode="False">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnContinuar" runat="server" OnClick="lbtnContinuar_Click">Continuar</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle Font-Size="Large" CssClass="pager" />
                                <SelectedRowStyle CssClass="txtTabela3" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="header_tabela" />
                                <AlternatingRowStyle CssClass="txtTabela2" />
                                <EmptyDataTemplate>
                                    Nenhum cliente foi encontrado!
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </fieldset>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upFiltros"
            DisplayAfter="1">
            <ProgressTemplate>
                <div class="esmaecer">
                    <div class="icon">
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </form>
</body>
</html>
