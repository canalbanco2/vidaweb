﻿<%@ Page Language="vb" EnableViewStateMac="false"  AutoEventWireup="false" Codebehind="SEGW0158Documentos.aspx.vb"
    Inherits="SEGW0158.SEGW0158Documentos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SUSAB - Aviso de Sinistro RE - Finalização de Documento</title>
    <link href="css/Sinistro.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src='<%= ResolveUrl("~/js/SEGW0158Scripts.js") %>'></script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div>
                    <br />
                     <fieldset style="width: 830px; text-align: center; height: 400px;">
                        <legend class="TitCaixa"><span>Lista de Documentos</span></legend>
                    <asp:Panel ID="Panel1" runat="server" Height="300px" ScrollBars="Vertical" HorizontalAlign="Center"
                                        Width="810px">
                        <asp:GridView ID="GVwSinistrados" runat="server" AutoGenerateColumns="False" 
                        OnPageIndexChanging="GVwSinistrados_PageIndexChanging" Width="810px" OnSelectedIndexChanged="GVwSinistrados_SelectedIndexChanged" >
                            <AlternatingRowStyle CssClass="txtTabela2" />
                            <SelectedRowStyle CssClass="txtTabela3" />
                            <HeaderStyle CssClass="header_tabela" />
                            <RowStyle CssClass="txtTabela" />
                            <Columns>
                                <asp:BoundField DataField="Tipo_Copia" HeaderText="C&#243;pia/Original">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Documento" HeaderText="Documento" />
                                <asp:BoundField DataField="Local" HeaderText="Local para obter a documenta&#231;&#227;o" />
                            </Columns>
                            <PagerStyle Font-Size="Large" CssClass="pager" />
                            <EmptyDataTemplate>
                                Não existem documentos para este aviso
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </asp:Panel>
                    <br />

                    <asp:Label runat="server" ID="texto" CssClass="txtLabelLeft TitCaixa" Font-Bold="true" Text="A seguradora reserva-se o direito de solicitar qualquer documento que julgar
                necessário para a efetiva comprovação da cobertura do sinistro" Height="36px" Width="790px"></asp:Label>
                    </fieldset>
                    <br />
                    <br />
                           <asp:CheckBox ID="chkEnvio" CssClass="txtLabelLeft TitCaixa" runat="server" Text="Enviar lista de documentos para o(a) Solicitante?"
                            Height="40px" Width="690px" OnCheckedChanged="chkEnvio_CheckedChanged" AutoPostBack="True" />
                    <br />
                   <div class="n750">
                        <div class="BotaoFora TitCaixa" runat="server" visible="false">
                            <div class="BotaoE">
                            </div>
                            <asp:Button ID="btnOK" runat="server" visible="false" Text="Sair" CssClass="BotaoM n100" OnClick="btnOK_Click" />
                            <div class="BotaoD">
                            </div>
                        </div>
                        <div class="BotaoFora divLeft TitCaixa">
                            <div class="BotaoE">
                            </div>
                            <asp:Button ID="btnNovoSinistro" runat="server" CssClass="BotaoM n100 " Text="Novo Aviso"
                                OnClick="btnNovoSinistro_Click" />
                            <div class="BotaoD">
                            </div>
                        </div>
                    </div>
                    <div style="text-align: left; float: left; width: 750px; height: 40px; margin-top: 20px;
                        clear: both;">
                        
                    </div>
                    <br />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
            DisplayAfter="0">
            <ProgressTemplate>
                <div class="esmaecer">
                    <div class="icon">
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </form>
</body>
</html>
