﻿Imports SEGL0331
Imports SEGL0328
Imports SEGL0343
Imports SEGL0343.EO
Imports SEGL0343.Task
Imports System.Collections.Generic
Imports System.Text

Partial Public Class SEGW0158AvisoPropostas
    Inherits BasePages


#Region "Propriedades"

    Public Property lNR_CTR_SGRO() As Integer
        Get
            Return CType(ViewState("lNR_CTR_SGRO"), Integer)
        End Get
        Set(ByVal value As Integer)
            ViewState("lNR_CTR_SGRO") = value
        End Set
    End Property

    Public Property lNR_VRS_EDS() As Integer
        Get
            Return CType(ViewState("lNR_VRS_EDS"), Integer)
        End Get
        Set(ByVal value As Integer)
            ViewState("lNR_VRS_EDS") = value
        End Set
    End Property

    Public Property CLIENTE_DOC() As String
        Get
            Return CType(ViewState("CLIENTE_DOC"), String)
        End Get
        Set(ByVal value As String)
            ViewState("CLIENTE_DOC") = value
        End Set
    End Property

    Public Property cProdutoBBProtecao() As String
        Get
            Return CType(ViewState("cProdutoBBProtecao"), String)
        End Get
        Set(ByVal value As String)
            ViewState("cProdutoBBProtecao") = value
        End Set
    End Property

    Public Property propostasAvisadas() As String
        Get
            Return CType(ViewState("propostasAvisadas"), String)
        End Get
        Set(ByVal value As String)
            ViewState("propostasAvisadas") = value
        End Set
    End Property

    Public Property propostasReanalise() As String
        Get
            Return CType(ViewState("propostasAvisadas"), String)
        End Get
        Set(ByVal value As String)
            ViewState("propostasAvisadas") = value
        End Set
    End Property

    Public Property NOME() As String
        Get
            Return CType(ViewState("NOME"), String)
        End Get
        Set(ByVal value As String)
            ViewState("NOME") = value
        End Set
    End Property

    Public Property LISTA_CLIENTE() As String
        Get
            Return CType(ViewState("LISTA_CLIENTE"), String)
        End Get
        Set(ByVal value As String)
            ViewState("LISTA_CLIENTE") = value
        End Set
    End Property

    Public Property NaoPodeAvisar() As Boolean
        Get
            Return CType(ViewState("NaoPodeAvisar"), Boolean)
        End Get
        Set(ByVal value As Boolean)
            ViewState("NaoPodeAvisar") = value
        End Set
    End Property

    Public Property Motivo_Reanalise() As Integer
        Get
            Return CType(ViewState("Motivo_Reanalise"), Integer)
        End Get
        Set(ByVal value As Integer)
            ViewState("Motivo_Reanalise") = value
        End Set
    End Property

    Public ReadOnly Property Cliente_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("Cliente_ID")) Then
                Return Request.QueryString("Cliente_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property PS() As Integer
        Get
            If Not IsNothing(Request.QueryString("PS")) Then
                Return Request.QueryString("PS")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property Proposta_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("Proposta_ID")) Then
                Return Request.QueryString("Proposta_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property Evento_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("Evento_ID")) Then
                Return Request.QueryString("Evento_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property Dt_Ocorrencia() As String
        Get
            If Not IsNothing(Request.QueryString("Dt_Ocorrencia")) Then
                Return Request.QueryString("Dt_Ocorrencia")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property CPF_CNPJ() As String
        Get
            If Not IsNothing(Request.QueryString("CPF_CNPJ")) Then
                Return Request.QueryString("CPF_CNPJ")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public Property sProposta() As Boolean
        Get
            If Not IsNothing(Request.QueryString("sProposta")) Then
                Return Request.QueryString("sProposta")
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Boolean)
            Request.QueryString("sProposta") = value
        End Set
    End Property

    Public Property SemProposta() As Boolean
        Get
            Return CType(ViewState("SemProposta"), Boolean)
        End Get
        Set(ByVal value As Boolean)
            ViewState("SemProposta") = value
        End Set
    End Property

    Public Property Proposta_ID_Grid() As String
        Get
            Return CType(ViewState("Proposta_Grid"), String)
        End Get
        Set(ByVal value As String)
            ViewState("Proposta_Grid") = value
        End Set
    End Property

    Public Property Indicador_Grid() As String
        Get
            Return CType(ViewState("IndicadorGrid"), String)
        End Get
        Set(ByVal value As String)
            ViewState("IndicadorGrid") = value
        End Set
    End Property

    Public Property Proposta_BB_Grid() As String
        Get
            Return CType(ViewState("Proposta_BB"), String)
        End Get
        Set(ByVal value As String)
            ViewState("Proposta_BB") = value
        End Set
    End Property


    Dim tblHashTable As New Hashtable

    Public Property GridRows() As Hashtable
        Get
            Return tblHashTable
        End Get
        Set(ByVal value As Hashtable)
            Me.tblHashTable = value
        End Set
    End Property

    Public ReadOnly Property Sinistro_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("SINISTRO_ID")) Then
                Return Request.QueryString("SINISTRO_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property


#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                Me.CarregaProdutoBBProtecaoVida()


                If Me.Cliente_ID > 1 And Me.sProposta = False Then
                    'Carrega grid de propostas selecionar
                    'se o grid de proposta for = 1 setar sProposta = true e exibir form ObjSegurado
                    Me.CarregarPropostasGrid()
                Else
                    Me.sProposta = True
                    'chamar formulario Objsegurado
                End If
            End If

        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro ao processar Load da pagina" & "')", True)
        End Try
    End Sub

    Public Sub CarregarPropostasGrid()
        Try
            Dim TaskAvisoProposta As New SEGW158AvisoPropostas_TK
            Dim EO_IN As New SEGS7758_SPS_EO_IN
            Dim ListEO_OUT As New List(Of SEGS7758_SPS_EO_OUT)

            EO_IN.Cliente_Id = Me.Cliente_ID
            EO_IN.Dt_Ocorrencia = CType(Me.Dt_Ocorrencia.ToString, Date)
            EO_IN.Evento_Sinistro_Id = Me.Evento_ID

            ListEO_OUT = TaskAvisoProposta.Consultar(EO_IN)

            Me.grdResultadoPesquisa.DataSource = ListEO_OUT
            Me.grdResultadoPesquisa.DataBind()

        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro ao carregar propostas grid" & "')", True)
        End Try
    End Sub

    Protected Sub btnNaoAvisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            If IsNothing(Me.grdResultadoPesquisa.SelectedRow) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Selecione uma Proposta no Grid!" & "')", True)
                Exit Sub
            End If

            'Recupera linha selecionada do Grid
            Dim rowGridSelecionado As GridViewRow = Me.grdResultadoPesquisa.SelectedRow
            Dim subevento_sinistro_aux As Long = 0

            If rowGridSelecionado.Cells(9).Text = 2 And _
               rowGridSelecionado.Cells(3).Text <> "*" Then

                If Not rowGridSelecionado.Cells(3).Text = "NAVS" Then
                    If rowGridSelecionado.Cells(3).Text = "RNLS" Then
                        rowGridSelecionado.Cells(3).Text = "*NAVS"
                    Else
                        rowGridSelecionado.Cells(3).Text = "NAVS"
                    End If

                    'If InStr(1, cProdutoBBProtecao, Format(grdResultadoPesquisa.TextMatrix(grdResultadoPesquisa.Row, 5), "0000"), vbTextCompare) Then
                    If VerificarProdutoBB(cProdutoBBProtecao, rowGridSelecionado.Cells(7).Text) Then

                        For i As Integer = 1 To grdResultadoPesquisa.Rows.Count - 1
                            If VerificarProdutoBB(cProdutoBBProtecao, rowGridSelecionado.Cells(3).Text) Then
                                Dim rowTemp As GridViewRow = Me.grdResultadoPesquisa.Rows(i)
                                If rowTemp.Cells(3).Text = "RNLS" Then
                                    rowTemp.Cells(3).Text = "*NAVS"
                                Else
                                    rowTemp.Cells(3).Text = "NAVS"
                                End If
                            End If

                        Next i
                        'ATUALIZAR PROPOSTAS SEM COBERTURA - não passa o indice para não atualizar a linha atual
                        'Call AtualizaPropostasSemCobertura()
                    End If

                End If
            End If

        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro ao clicar no botão Não Avisar" & "')", True)
        End Try
    End Sub

    Protected Sub btnAvisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            If IsNothing(Me.grdResultadoPesquisa.SelectedRow) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Selecione uma Proposta no Grid para Avisar!" & "')", True)
                Exit Sub
            End If

            'Recupera linha selecionada do Grid
            Dim rowGridSelecionado As GridViewRow = Me.grdResultadoPesquisa.SelectedRow

            Dim flagReanalise As String
            Dim svalor_anterior As String

            Me.NaoPodeAvisar = False
            svalor_anterior = rowGridSelecionado.Cells(3).Text

            'indice 5 proposta BB
            If rowGridSelecionado.Cells(3).Text <> "AVSR" And _
            rowGridSelecionado.Cells(3).Text <> "RNLS" Then

                'BSI Flávio Fernandes - 10/06/2010
                'Verifica se existe proposta BB e se ela é numérica
                If IsNumeric(rowGridSelecionado.Cells(5).Text) Then

                    Me.VerificaPropostaBB(CType(rowGridSelecionado.Cells(5).Text, Long), Me.Dt_Ocorrencia)
                End If

            End If

            Dim Aviso = CreateObject("SEGL0144.cls00385")
            Aviso.mvarAmbienteId = CType(ConfigurationManager.AppSettings("Ambiente").ToString, Long)
            Aviso.mvarSiglaSistema = ConfigurationManager.AppSettings("SiglaSistema").ToString
            Aviso.mvarSiglaRecurso = ConfigurationManager.AppSettings("SiglaRecurso").ToString
            Aviso.mvarDescricaoRecurso = ConfigurationManager.AppSettings("DescricaoRecurso").ToString



            flagReanalise = Aviso.VerificaReanaliseRE(CType(rowGridSelecionado.Cells(4).Text, Long), _
                            Format(CDate(Me.Dt_Ocorrencia), "yyyyMMdd"), _
                            Me.Evento_ID, _
                            Me.CPF_CNPJ, _
                            0, _
                            0)

            If flagReanalise = "RNLS" Or flagReanalise = "AVSR" Then
                If rowGridSelecionado.Cells(3).Text = "PSCB" Then
                    '        'If MsgBox("Confirma o registro em proposta sem cobertura?", vbYesNo, "Sinistro sem proposta") = vbNo Then Exit Sub
                End If
                If rowGridSelecionado.Cells(3).Text = "*NAVS" Then
                    rowGridSelecionado.Cells(3).Text = "RNLS"
                Else
                    rowGridSelecionado.Cells(3).Text = flagReanalise
                End If

                'indice 7

                If VerificarProdutoBB(cProdutoBBProtecao, rowGridSelecionado.Cells(7).Text) Then
                    For i As Integer = 1 To grdResultadoPesquisa.Rows.Count - 1
                        Dim rowTemp As GridViewRow = grdResultadoPesquisa.Rows(i)
                        If VerificarProdutoBB(cProdutoBBProtecao, rowTemp.Cells(7).Text) Then
                            rowTemp.Cells(3).Text = flagReanalise
                        End If
                    Next i
                    'ATUALIZAR PROPOSTAS SEM COBERTURA
                    'Call AtualizaPropostasSemCobertura(grdResultadoPesquisa.Row, svalor_anterior)
                End If

            Else
                Dim sb As New StringBuilder
                sb.Append("Esta proposta já possui um aviso de sinistro em aberto\n")
                sb.Append("para os parametros: proposta, evento de sinistro,\n")
                sb.Append("data de ocorrência, CPF ou CNPJ do sinistrado.\n")
                sb.Append("Não é possível avisar esta proposta.\n")

                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & sb.ToString & "')", True)


                NaoPodeAvisar = True
            End If

            'Atualiza Hash Table na Sessao com indice e o objeto a ser recuperado
            Me.GridRows.Add(rowGridSelecionado.RowIndex, rowGridSelecionado)

        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro ao Avisar Proposta" & ex.Message.ToString & "')", True)
        End Try
    End Sub


    Public Function VerificarProdutoBB(ByVal lista As String, ByVal value As String) As Boolean

        VerificarProdutoBB = False
        Dim arr As Object
        arr = Split(lista, ",")

        Dim contador As Integer = 6

        For i As Integer = 0 To contador
            If Trim(CStr(arr(i))) = Trim(CStr(value)) Then
                VerificarProdutoBB = True
                Exit For
            End If
        Next

        VerificarProdutoBB = False

    End Function


    Public Function VerificaPropostaBB(ByVal proposta_bb As Long, ByVal dt_ocorrencia As String) As Boolean
        Try
            Dim TaskAvisoProposta As New SEGW158AvisoPropostas_TK
            Dim blnVerificaPropostaBB As Boolean = True
            Dim ListEndossoEO_OUT As List(Of EO.Obtem_Versao_Endosso_sps_EO_OUT)
            Dim EO_IN As New EO.Obtem_Versao_Endosso_sps_EO_IN

            EO_IN.Proposta_Bb = proposta_bb
            EO_IN.Dt_Ocorrencia = dt_ocorrencia

            ListEndossoEO_OUT = TaskAvisoProposta.Obtem_Versao_Endosso(EO_IN)

            If ListEndossoEO_OUT.Count > 0 Then
                VerificaPropostaBB = True 'NR_CTR_SGRO
                lNR_VRS_EDS = ListEndossoEO_OUT.Item(0).NR_CTR_SGRO
                lNR_CTR_SGRO = ListEndossoEO_OUT.Item(0).NR_CTR_SGRO_BB
            Else
                VerificaPropostaBB = False
            End If

            Return VerificaPropostaBB
            
        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro ao Verificar Proposta BB" & "')", True)
        End Try
    End Function

    Public Sub CarregaProdutoBBProtecaoVida()
        Try
            cProdutoBBProtecao = String.Empty
            Dim TaskAvisoPropostas As New SEGW158AvisoPropostas_TK
            Dim EO_IN As New EO.SEGS8758_SPS_EO_IN
            Dim LstOut As List(Of EO.SEGS8758_SPS_EO_OUT)

            LstOut = TaskAvisoPropostas.CarregaProdutoBBProtecaoVida(EO_IN)

            For Each objEO_OUT As EO.SEGS8758_SPS_EO_OUT In LstOut
                Me.cProdutoBBProtecao = Me.cProdutoBBProtecao & "," & objEO_OUT.produto_id
            Next
            
            cProdutoBBProtecao = Mid(cProdutoBBProtecao, 2)

        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro ao carregar produto bb proteção Vida" & "')", True)
        End Try
    End Sub

    Protected Sub grdResultadoPesquisa_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim rowGridSelecionado As GridViewRow = Me.grdResultadoPesquisa.SelectedRow
            Me.Indicador_Grid = rowGridSelecionado.Cells(3).Text.ToString
            Me.Proposta_ID_Grid = rowGridSelecionado.Cells(4).Text.ToString
            Me.Proposta_BB_Grid = rowGridSelecionado.Cells(5).Text.ToString

            If Not IsNothing(Me.GridRows) Then
                For Each rowsGrid As GridViewRow In Me.grdResultadoPesquisa.Rows
                    If Me.GridRows.Contains(rowsGrid.RowIndex) Then
                        Me.grdResultadoPesquisa.Rows(rowsGrid.RowIndex).Cells(3).Text = CType(Me.GridRows(rowsGrid.RowIndex), GridViewRow).Cells(3).Text
                    End If
                Next
            End If

        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro ao Verificar Proposta BB" & "')", True)
        End Try
    End Sub


    Protected Sub btnReanalize_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            If IsNothing(Me.grdResultadoPesquisa.SelectedRow) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Selecione uma Proposta no Grid para Reanalise!" & "')", True)
                Exit Sub
            End If

            Dim rowGridSelecionado As GridViewRow = Me.grdResultadoPesquisa.SelectedRow

            If Not rowGridSelecionado.Cells(3).Text = "RNLS" And _
            Left(rowGridSelecionado.Cells(3).Text, 1) = "*" Then
                rowGridSelecionado.Cells(3).Text = "RNLS"
            Else
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Não é possível abrir esta proposta para reanálise de sinistro." & "')", True)
            End If

            
        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro ao clicar em Reanalise" & "')", True)
        End Try
    End Sub
    Public Function VerificaPropostaAgricola(ByVal dProposta As Double, ByVal iProduto As Integer, Optional ByVal iIndice As Integer = 0) As Boolean
        Dim EO_IN_Sinistro As New SEGS8818_SPS_EO_IN
        Dim EO_IN_Propostas As New SEGS8819_SPS_EO_IN
        Dim Sinistros As New List(Of SEGS8818_SPS_EO_OUT)


        'sProdutoAgricola = "ABERTURA" 'asouza 16/05/06



        'Rs = ExecutarSQL(gsSIGLASISTEMA, _
        '                     glAmbiente_id, _
        '                     App.Title, _
        '                     App.FileDescription, _
        '                     Sql, _
        '                     True)

        'If Not Rs.EOF Then
        '    If Not IsNull(Rs("situacao")) Then
        '        If Rs("situacao") = 0 Or Rs("situacao") = 1 Then ' Aberto / Reaberto
        '            sProdutoAgricola = "INC_OCORRENCIA"
        '        ElseIf Rs("situacao") = 2 Then ' Encerrado
        '            sProdutoAgricola = "REANALISE"
        '            iEventoId = Rs("evento_sinistro_id")
        '        End If
        '        sSinistro_id = Rs("sinistro_id")
        '    End If
        '    ' G&P - Diego Flow 332195 - 24/04/2008
        '    'Verifica se existe um aviso na evento_segbr_sinistro_atual_tb
        'Else
        '    Sql = ""
        '    Sql = Sql & " SELECT *" & vbNewLine
        '    Sql = Sql & " FROM evento_segbr_sinistro_atual_tb"
        '    Sql = Sql & " WHERE proposta_id = " & dProposta

        '    Rs2 = ExecutarSQL(gsSIGLASISTEMA, _
        '                     glAmbiente_id, _
        '                     App.Title, _
        '                     App.FileDescription, _
        '                     Sql, _
        '                     True)
        '    If Not Rs2.EOF Then
        '        sProdutoAgricola = "ERRO"
        '        MsgBox("Já existe um aviso de sinistro para a proposta " & dProposta & ". Sinistro " & Rs2("sinistro_id"), vbCritical, "Aviso Sinistro  RE")
        '        'Caso exista exibe a mensagem de erro.
        '    End If
        '    Rs2.Close()
        'End If
        'Rs.Close()
        'Else
        'sProdutoAgricola = frmAvisoPropostas.grdResultadoPesquisa.TextMatrix(CInt(iIndice), 1)

        'End If

    End Function

    Protected Sub btnVoltar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            Dim sb As New StringBuilder()
            sb.Append("SEGW0158Agencia.aspx")
            sb.Append("?SINISTRO_ID=" & Me.Sinistro_ID)
            sb.Append("&SLinkSeguro=" + Request("SLinkSeguro"))

            Response.Redirect(sb.ToString())

        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro Ao executar Passo 3" & "')", True)
        End Try
    End Sub

    Protected Sub btnContinuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim Ramo, SubRamo As Integer
            If ObtemPropostasAvisadas() = False Or NaoPodeAvisar Then Exit Sub
            Me.SemProposta = False
            Dim EO_OUT As EO.SEGS7366_SPS_EO_OUT

            'For i As Integer = 1 To Me.grdResultadoPesquisa.Rows.Count - 1
            '    Dim rowTemp As GridViewRow = Me.grdResultadoPesquisa.Rows(i)
            '    If Left(rowTemp.Cells(3).Text, 1) = "A" Or Left(rowTemp.Cells(3).Text, 1) = "R" Then

            '        EO_OUT = Me.Consultar_SEGS7366_SPS(rowTemp.Cells(4).Text, Me.Dt_Ocorrencia).Item(0)


            '        If Not IsNothing(EO_OUT) Then
            '            Ramo = EO_OUT.ramo_id
            '            SubRamo = EO_OUT.subramo_id
            '        End If

            '        Call VerificaPropostaAgricola(grdResultadoPesquisa.TextMatrix(Str(i), 2), grdResultadoPesquisa.TextMatrix(Str(i), 5), CInt(i))
            '        If sProdutoAgricola = "ERRO" Then
            '            Me.MousePointer = vbNormal
            '            Exit Sub
            '        End If

            '        Aviso = CreateObject("SEGL0144.cls00385")

            '        Aviso.mvarAmbienteId = glAmbiente_id
            '        Aviso.mvarSiglaSistema = gsSIGLASISTEMA
            '        Aviso.mvarSiglaRecurso = App.Title
            '        Aviso.mvarDescricaoRecurso = App.FileDescription

            '        If sProdutoAgricola = "INC_OCORRENCIA" Then
            '            frmAviso.lblTpAviso.Caption = sInclusaoOcorrencia
            '            frmAviso.txtReanalise.Text = "Não"
            '            frmAviso.txtSinistro.Visible = True
            '            frmAviso.lblSinistro.Visible = True
            '        ElseIf sProdutoAgricola = "ABERTURA" Then
            '            frmAviso.lblTpAviso.Caption = sAbertura
            '            frmAviso.txtReanalise.Text = "Não"
            '            frmAviso.txtSinistro.Visible = False 'asouza - novo
            '            frmAviso.lblSinistro.Visible = False 'asouza - novo
            '        ElseIf sProdutoAgricola <> "REANALISE" Then
            '            flagReanalise = Aviso.VerificaReanaliseRE(grdResultadoPesquisa.TextMatrix(Str(i), 2), _
            '                                        Format(frmEvento.mskDataOcorrencia.Text, "yyyymmdd"), _
            '                                        frmEvento.cboEvento.ItemData(frmEvento.cboEvento.ListIndex), _
            '                                        frmConsulta.grdResultadoPesquisa.TextMatrix(frmConsulta.grdResultadoPesquisa.RowSel, 2), sSinistro_id, sSinistro_bb)


            '            If flagReanalise = "RNLS" Then

            '                Call PreencheDadosSinistro(sSinistro_id)
            '                Me.MousePointer = vbDefault
            '                frmAviso.lblTpAviso.Caption = sReabertura 'asouza

            '            ElseIf flagReanalise = "NAVS" Then
            '                MsgBox("Esta proposta já possui um aviso de sinistro em aberto " & vbCrLf & _
            '                            "para os parametros: proposta, CPF/CNPJ do Segurado, evento de sinistro e " & vbCrLf & _
            '                            "data de ocorrência." & vbCrLf & _
            '                            "Sinistro AB: " & sSinistro_id & vbCrLf & _
            '                            "Sinistro BB: " & sSinistro_bb & vbCrLf & _
            '                            "Não é possível avisar esta proposta.", vbCritical, "Aviso de Sinistro")

            '                Me.MousePointer = vbDefault
            '                Exit Sub
            '            ElseIf flagReanalise = "AVSR" Then
            '                frmAviso.lblTpAviso.Caption = sAbertura
            '                frmAviso.txtReanalise.Text = "Não"
            '                frmAviso.txtSinistro.Visible = False
            '                frmAviso.lblSinistro.Visible = False
            '            End If
            '        End If

            '        Me.Hide()

            '        If Not existeProposta(grdResultadoPesquisa.TextMatrix(CDbl(i), 2)) Then
            '            tProposta = New cProposta
            '            With tProposta
            '                .Proposta = Str(grdResultadoPesquisa.TextMatrix(CDbl(i), 2))
            '                .Produto = grdResultadoPesquisa.TextMatrix(CDbl(i), 5)
            '                .Ramo = Ramo
            '                .SubRamo = SubRamo
            '                .SituacaoProposta = grdResultadoPesquisa.TextMatrix(CDbl(i), 4)
            '                If sProdutoAgricola = "RNLS" Then
            '                    .TipoAviso = "REANALISE"
            '                Else
            '                    .TipoAviso = sProdutoAgricola
            '                End If
            '            End With

            '            Propostas.Add(tProposta)
            '        End If
            '    Else
            '        If existeProposta(grdResultadoPesquisa.TextMatrix(CDbl(i), 2)) Then
            '            Propostas.Remove(getIndiceProposta(grdResultadoPesquisa.TextMatrix(CDbl(i), 2)))
            '            'remover do grid obj
            '            For j = 1 To frmObjSegurado.grdPropostas.Rows - 1
            '                If frmObjSegurado.grdPropostas.TextMatrix(CDbl(j), 1) = grdResultadoPesquisa.TextMatrix(CDbl(i), 2) Then
            '                    If frmObjSegurado.grdPropostas.Rows = 2 Then
            '                        frmObjSegurado.grdPropostas.AddItem(" ")
            '                    End If
            '                    frmObjSegurado.grdPropostas.RemoveItem(j)
            '                    Exit For
            '                End If
            '            Next j
            '        End If
            '    End If

            'Next

            'If Propostas.Count = 0 Then
            '    bSemProposta = True
            'End If

            'Call frmObjSegurado.CarregaPropostas()
            'Me.MousePointer = vbDefault
            'Me.Hide()
            'frmObjSegurado.Show()


        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro ao clicar em continuar" & "')", True)
        End Try
    End Sub

    Private Function Consultar_SEGS7366_SPS(ByVal proposta_id As String, ByVal DataSistema As String) As List(Of EO.SEGS7366_SPS_EO_OUT)
        Try
            Dim taskAvisoPropostas As New SEGW158AvisoPropostas_TK
            Dim obj_EO_IN As New EO.SEGS7366_SPS_EO_IN

            obj_EO_IN.Data_Sistema = DataSistema
            obj_EO_IN.Proposta_Id = proposta_id

            Return taskAvisoPropostas.Consulta_SEGS7366_SPS(obj_EO_IN)

        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro ao clicar em continuar" & "')", True)
        End Try
    End Function

    Private Function ObtemPropostasAvisadas() As Boolean
        Try
            Dim propNAVSBBProtecao As String
            Dim propAVSRBBProtecao As String
            Dim blnPropostaEncontrada As Boolean

            blnPropostaEncontrada = False

            propostasAvisadas = ""
            propAVSRBBProtecao = ""
            propNAVSBBProtecao = ""

            For i As Integer = 1 To Me.grdResultadoPesquisa.Rows.Count - 1
                Dim RowTemp As GridViewRow = Me.grdResultadoPesquisa.Rows(i)
                If RowTemp.Cells(3).Text = "AVSR" Then
                    If Me.propostasAvisadas <> "" Then Me.propostasAvisadas = Me.propostasAvisadas & " , "
                    Me.propostasAvisadas = Me.propostasAvisadas & RowTemp.Cells(4).Text
                    blnPropostaEncontrada = True
                ElseIf RowTemp.Cells(3).Text = "RNLS" Then
                    blnPropostaEncontrada = True
                    If Me.propostasReanalise <> "" Then Me.propostasReanalise = Me.propostasReanalise & " , "
                    Me.propostasReanalise = Me.propostasReanalise & RowTemp.Cells(4).Text
                End If
            Next

            Return blnPropostaEncontrada

        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('" & "Erro ao obter propostas Avisadas" & "')", True)
        End Try
    End Function


End Class