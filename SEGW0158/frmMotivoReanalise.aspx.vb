﻿Imports SEGL0343
Imports SEGL0343.EO
Imports SEGL0343.Task
Imports SEGL0343.Business
Imports System.Collections.Generic
Imports System.Text

Partial Public Class frmMotivoReanalise
    Inherits System.Web.UI.Page

#Region "Propriedades"

    Public ReadOnly Property Sinistro_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("SINISTRO_ID")) Then
                Return Request.QueryString("SINISTRO_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

#End Region

#Region "Metodos"

    Private Sub CarregaCombo()

        Dim ObjEoIn_SEGS8928_SPS As New SEGS8928_SPS_EO_IN



        Dim lstObjEoOut_SEGS8928_SPS As New List(Of SEGS8928_SPS_EO_OUT)

        Dim objBus As New SEGS8928_SPS_Business

        ObjEoIn_SEGS8928_SPS.Tipo_Ramo = Convert.ToInt32(ConfigurationManager.AppSettings("TipoRamo"))

        lstObjEoOut_SEGS8928_SPS = objBus.seleciona(ObjEoIn_SEGS8928_SPS)

        Me.ddlMotivoReanalise.DataValueField = "motivo_reanalise_sinistro_id"
        Me.ddlMotivoReanalise.DataTextField = "descricao"



        Me.ddlMotivoReanalise.DataSource = lstObjEoOut_SEGS8928_SPS

        Me.ddlMotivoReanalise.DataBind()

    End Sub

    Private Function Gravar() As Boolean
        Dim objOut_8744 As New List(Of SEGS8744_SPS_EO_OUT)
        Dim objIn_8744 As New SEGS8744_SPS_EO_IN
        Dim PassoFinal As Boolean = False

        Dim objGeral As New SEGW0158Geral_TK()

        objIn_8744.Sinistro_Id = Me.Sinistro_ID

        objOut_8744 = objGeral.Buscar(objIn_8744)

        If Not IsNothing(objOut_8744) Then
            Dim objIn_8747 As New SEGS8747_SPU_EO_IN()

            objIn_8747 = objGeral.PreencheTodosCamposU(objOut_8744(0))

            With objIn_8747


                'VERIFICAR VERIFICAÇÃO DOS PASSOS

                'If objIn_8747.codigo_fluxo <> 1 Then
                '    .Codigo_Fluxo = 2
                'End If

                'If objOut_8775.codigo_passo < 3 Then
                '    .Codigo_Passo = 3
                'End If
                .Motivo_Reanalise_Sinistro_Id = Me.ddlMotivoReanalise.SelectedValue
                .Descricao_Motivo_Reanalise = Me.ddlMotivoReanalise.Text

                Select Case Math.Truncate((Me.Observacoes1.Observacao.Length / 70))
                    Case 0
                        .Motivo_Reanalise_1 = Me.Observacoes1.Observacao.Substring(0, 70)
                    Case 1
                        .Motivo_Reanalise_1 = Me.Observacoes1.Observacao.Substring(0, 70)
                        If Me.Observacoes1.Observacao.Length > 70 Then
                            .Motivo_Reanalise_2 = Me.Observacoes1.Observacao.Substring(70, Math.Abs(70 - Me.Observacoes1.Observacao.Length))
                        End If

                    Case 2
                        .Motivo_Reanalise_1 = Me.Observacoes1.Observacao.Substring(0, 70)
                        .Motivo_Reanalise_2 = Me.Observacoes1.Observacao.Substring(70, 70)
                        If Me.Observacoes1.Observacao.Length > 140 Then
                            .Motivo_Reanalise_3 = Me.Observacoes1.Observacao.Substring(140, Math.Abs(140 - Me.Observacoes1.Observacao.Length))
                        End If

                    Case 3
                        .Motivo_Reanalise_1 = Me.Observacoes1.Observacao.Substring(0, 70)
                        .Motivo_Reanalise_2 = Me.Observacoes1.Observacao.Substring(70, 70)
                        .Motivo_Reanalise_3 = Me.Observacoes1.Observacao.Substring(140, 70)
                        If Me.Observacoes1.Observacao.Length > 210 Then
                            .Motivo_Reanalise_4 = Me.Observacoes1.Observacao.Substring(210, Math.Abs(210 - Me.Observacoes1.Observacao.Length))

                        End If

                    Case 4
                        .Motivo_Reanalise_1 = Me.Observacoes1.Observacao.Substring(0, 70)
                        .Motivo_Reanalise_2 = Me.Observacoes1.Observacao.Substring(70, 70)
                        .Motivo_Reanalise_3 = Me.Observacoes1.Observacao.Substring(140, 70)
                        .Motivo_Reanalise_4 = Me.Observacoes1.Observacao.Substring(210, 70)

                        If Me.Observacoes1.Observacao.Length > 280 Then
                            .Motivo_Reanalise_5 = Me.Observacoes1.Observacao.Substring(280, Math.Abs(280 - Me.Observacoes1.Observacao.Length))

                        End If

                End Select


                objGeral.Atualizar(objIn_8747)

                'If objOut_8775.codigo_passo < 6 Then
                '    .Codigo_Passo = 6
                'End If

                '.Codigo_Fluxo = 3

            End With


            Return True
        End If

    End Function

    Private Sub Avancar()



        'I M P O R T A N T E

        'Veficar Tratamento para Objeto Segura

        Dim sb As New StringBuilder()
        sb.Append("SEGW0158ObjSegurado.aspx")
        sb.Append("?SinistroID=" + Me.Sinistro_ID)
        sb.Append("&SLinkSeguro=" + Request("SLinkSeguro"))
        Response.Redirect(sb.ToString())


    End Sub

#End Region

#Region "Eventos"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not IsPostBack Then

            CarregaCombo()

        End If

    End Sub

    Protected Sub ddlMotivoReanalise_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlMotivoReanalise.SelectedIndexChanged


        If ddlMotivoReanalise.SelectedValue = 3 Then

            'habilita observacao
            Observacoes1.ObservacaoReadOnly = False


            Observacoes1.Focus = True


        Else

            'limpa e desabilita
            'habilita observacao

            Observacoes1.Observacao = ""
            Observacoes1.ObservacaoReadOnly = True

        End If


    End Sub

#End Region

    Protected Sub btnContinuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinuar.Click

        If Me.Gravar() Then

            Me.Avancar()

        Else

            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Erro ao tentar salvar passos!')", True)

        End If


    End Sub

    Protected Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click

        If Me.Gravar() Then

            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Dados salvos com sucesso!')", True)

        Else

            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Erro ao tentar salvar passos!')", True)

        End If
    End Sub

    Protected Sub btnVoltar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoltar.Click

    End Sub
End Class