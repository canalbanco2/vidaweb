<%@ Page Language="vb" EnableViewStateMac="false"  AutoEventWireup="false" CodeBehind="SEGW0158DadosAgencia.aspx.vb" Inherits="SEGW0158.SEGW0158DadosAgencia" %>

<%@ Register Src="~/UserControls/UCDadosAgencia.ascx" TagName="DadosAgencia" TagPrefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>SUSAB - Aviso de Sinistro RE - Dados da Ag�ncia</title>
    <link href="CSS/sinistro.css" rel="stylesheet" type="text/css" />   
    <script type="text/javascript" src='<%= ResolveUrl("~/js/SEGW0158Scripts.js") %>'></script>            
</head>
<body>
    <form id="form1" runat="server" defaultbutton="DadosAgencia1$ibtPesquisarCodigoAgencia">
      <div id="Div2" style="width: 900px; text-align: left; height: 60px">
            <div class="n900">
                <div class="n900">
                    <fieldset class="n100" style="text-align: center; background-color:  #fffff0;">
                        <asp:LinkButton ID="LkbPesquisa" Enabled="false" CssClass="BtnNavega" runat="server"
                           >Pausar Processo</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n100" style="text-align: center; ">
                        <asp:LinkButton ID="LkbDadosCliente" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Dados do Cliente</asp:LinkButton></fieldset>
                    <fieldset style="width: 70px; text-align: center; background-color:  #fffff0;">
                        <asp:LinkButton ID="LkbEvento" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Evento</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n110" style="text-align: center; background-color:  #fffff0;">
                       <asp:LinkButton ID="LkbSolicitante" Enabled="false" CssClass="BtnNavega"
                            runat="server">Dados Solicitante</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n70" style=" text-align: center; background-color:  #F0E68C;">
                        <asp:LinkButton ID="LkbAgencia" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Ag�ncia</asp:LinkButton></fieldset>
                    <fieldset class="n100" style=" text-align: center;">
                       <asp:LinkButton ID="LkbSinistroCA" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Analise de Sinistro</asp:LinkButton></fieldset>
                    <fieldset class="n100" style=" text-align: center;">
                       <asp:LinkButton ID="LkbObjsegurado" Enabled="false" CssClass="BtnNavega" 
                            runat="server">Objeto Segurado</asp:LinkButton>
                    </fieldset>
                    <fieldset class="n100" style=" text-align: center;">
                        <asp:LinkButton ID="LkbOutrosSeguros" Enabled="false" CssClass="BtnNavega"
                            runat="server">Outros Seguros</asp:LinkButton></fieldset>
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="width: 100px; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu " style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_on_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 110px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                            <td style="width: 100px; border: none; background: url(Image/menu_off_meio.gif) center;">
                                <div class="tx_nm_menu" style="float: left;">
                                </div>
                                <div style="float: left;">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600" EnableScriptGlobalization="true">
        </asp:ScriptManager>  
        <asp:UpdatePanel ID="upd_Filtro" runat="server">
        <Triggers>
        <asp:AsyncPostBackTrigger EventName="Click" ControlID="DadosAgencia1$ibtPesquisarCodigoAgencia"/>
        </Triggers>
        <ContentTemplate>    
        <div>
            <uc1:DadosAgencia ID="DadosAgencia1" runat="server"></uc1:DadosAgencia>     
        </div>
        <br />
      <div class="n900">
                    <div class="n900 divLeft">
                        <div class="BotaoFora n300 divLeft alignLeft">
                            <div class="BotaoE">
                            </div>
                            <asp:Button ID="btnVoltar" runat="server" Text="<< Voltar" CssClass="BotaoM n120" />
                            <div class="BotaoD">
                            </div>
                        </div>
                        <div class="BotaoFora n300 divLeft alignLeft">
                            <div class="BotaoE">
                            </div>
                            <asp:Button ID="btnSalvar" runat="server" Width="120px" Text="Salvar" CssClass="BotaoM n120"
                                ValidationGroup="ValidarCampos" />
                            <div class="BotaoD">
                            </div>
                        </div>
                        <div class="BotaoFora n300 divLeft alignLeft">
                            <div class="BotaoE">
                            </div>
                            <asp:Button ID="btnContinuar" runat="server" Text="Continuar >>" CssClass="BotaoM n120"
                                ValidationGroup="ValidarCampos" />
                            <div class="BotaoD">
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>        
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upd_Filtro" DisplayAfter="0">
            <ProgressTemplate>
                <div class="esmaecer">
                    <div class="icon">
                    </div>
                </div>                
            </ProgressTemplate>
    </asp:UpdateProgress>      
    </form>
</body>
</html>
