﻿Imports SEGL0343.Task
Imports SEGL0343.EO
Imports SEGL0343.Business

Partial Public Class SEGW0158SinistroAvisado
    Inherits BasePages

#Region "Propriedades"

    Public lstSinistro As New List(Of String)
    Public str As String = ""

    Public ReadOnly Property SinistroID() As String
        Get
            If Not IsNothing(Request.QueryString("SINISTRO_ID")) Then
                Return Request.QueryString("SINISTRO_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property SinistroGerado() As String
        Get
            If Not IsNothing(Request.QueryString("SinistroGerado")) Then
                Return Request.QueryString("SinistroGerado")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property Doc() As String
        Get
            If Not IsNothing(Request.QueryString("Doc")) Then
                Return Request.QueryString("Doc")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property ProdutoID() As String
        Get
            If Not IsNothing(Request.QueryString("ProdutoID")) Then
                Return Request.QueryString("ProdutoID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property EventoID() As String
        Get
            If Not IsNothing(Request.QueryString("EventoID")) Then
                Return Request.QueryString("EventoID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property Ramo() As String
        Get
            If Not IsNothing(Request.QueryString("Ramo")) Then
                Return Request.QueryString("Ramo")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public Property grvAviso() As GridView
        Get
            Return ViewState("GrvAvisos")
        End Get
        Set(ByVal value As GridView)
            value = ViewState("GrvAvisos")
        End Set
    End Property

#End Region

#Region "Eventos"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                PopulaGvAvisado(True)
            End If
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try
    End Sub

    Protected Sub grdSinistros_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Try
            Me.grdSinistros.PageIndex = e.NewPageIndex
            PopulaGvAvisado(False)
        Catch ex As Exception
            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try
    End Sub

    Protected Sub btnDocumentos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Avancar()
    End Sub

#End Region

#Region "Funções"

    Public Function MontaParametrosConsulta(ByVal Auxstr As String)
        Try
            If lstSinistro.Count <> 0 Then
                Auxstr = "'" + Auxstr + "',"
            Else
                Auxstr = Auxstr.Substring(Auxstr.Length - 1, 1)
                Auxstr = Auxstr.PadLeft(Auxstr.Length - 1)
            End If

        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

        Return Auxstr
    End Function

    Sub PopulaGvAvisado(ByVal BuscaBanco As Boolean)
        Try
            Dim Obj8941In As New SEGS8941_SPS_EO_IN
            Dim Obj8941Out As New List(Of SEGS8941_SPS_EO_OUT)
            Dim Objt8941 As New SEGS8941_SPS_Business

            Obj8941In.Sinistro_Id = SinistroID
            Obj8941Out = Objt8941.seleciona(Obj8941In)

            Dim dt As New DataTable
            dt.Columns.Add("Sinistro", GetType(String))
            dt.Columns.Add("proposta", GetType(String))
            dt.Columns.Add("Produto", GetType(String))

            For Each o As SEGS8941_SPS_EO_OUT In Obj8941Out
                Dim dr As DataRow

                dr = dt.NewRow
                dr("Sinistro") = o.SINISTRO_GERADO_ID.ToString
                dr("proposta") = o.PROPOSTA_ID.ToString
                dr("Produto") = IIf(o.PRODUTO_ID.ToString = "0", "Sem produto", o.PRODUTO.ToString)

                dt.Rows.Add(dr)
            Next
            dt.AcceptChanges()

            grdSinistros.DataSource = dt
            grdSinistros.DataBind()
            LabelAvisos.Text = dt.Rows.Count.ToString + "  Sinistro(s) Avisado(s)."

        Catch ex As Exception
            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try
    End Sub

    Sub Avancar()
        Dim n As New StringBuilder

        n.Append("SEGW0158Documentos.aspx")
        n.Append("?SINISTRO_ID=" + Me.SinistroID)
        n.Append("&SLinkSeguro=" + Request("SLinkSeguro"))

        Response.Redirect(n.ToString())

    End Sub

#End Region




End Class