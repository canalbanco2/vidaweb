﻿Imports SEGL0343.Task
Imports SEGL0343.EO
Imports SEGL0343.Business

Partial Public Class SEGW0158ConsultaPropostasPendentes
    Inherits BasePages

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Timeout") = "Expired" Then
            Response.Redirect("SEGW0158FimSession.aspx?SLinkSeguro=" + Request("SLinkSeguro"))
        End If
    End Sub

    Protected Sub btnNovoSinistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNovoSinistro.Click
        Dim sb As New StringBuilder()

        sb.Append("SEGW0158ConsultaSinistrado.aspx")
        sb.Append("?SLinkSeguro=" + Request("SLinkSeguro"))
        Response.Redirect(sb.ToString())

    End Sub

    Protected Sub lbtnContinuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            If Me.GVwSinistrados.SelectedIndex <> -1 Then

                Dim lbtnContinuar As LinkButton = DirectCast(sender, LinkButton)
                Dim strAux As String() = lbtnContinuar.CommandArgument.Split("|")

                '1.	Aviso de Sinistro -> frmConsulta
                '2.	Dados da Ocorrência ->frmEvento
                '3.	Dados do Solicitante ->frmSolicitante
                '4.	Agência ->frmAgencia
                '5.	Propostas ->frmObjSegurado
                '6.	Outros Seguros ->frmOutrosSeguros
                '7.	Resumo Final ->frmAviso
                '8.	Sinistros Avisados ->frmAvisosRealizados
                '9.	Lista de documentos ->frmDocumentos

                Select Case Me.GVwSinistrados.SelectedRow.Cells(6).Text
                    Case 1
                        RedirecionaContinuar("SEGW0158Evento.aspx", Me.GVwSinistrados.SelectedRow.Cells(8).Text, Me.GVwSinistrados.SelectedRow.Cells(7).Text)
                    Case 2
                        RedirecionaContinuar("SEGW0158DadosSolicitante.aspx", Me.GVwSinistrados.SelectedRow.Cells(8).Text, Me.GVwSinistrados.SelectedRow.Cells(7).Text)
                    Case 3
                        RedirecionaContinuar("SEGW0158DadosAgencia.aspx", Me.GVwSinistrados.SelectedRow.Cells(8).Text, Me.GVwSinistrados.SelectedRow.Cells(7).Text)
                    Case 4
                        RedirecionaContinuar("SEGW0158AnaliseSinistroCA.aspx", Me.GVwSinistrados.SelectedRow.Cells(8).Text, Me.GVwSinistrados.SelectedRow.Cells(7).Text)
                    Case 5
                        RedirecionaContinuar("SEGW0158ObjSegurado.aspx", Me.GVwSinistrados.SelectedRow.Cells(8).Text, Me.GVwSinistrados.SelectedRow.Cells(7).Text)
                    Case 6
                        RedirecionaContinuar("SEGW0158OutrosSeguros.aspx", Me.GVwSinistrados.SelectedRow.Cells(8).Text, Me.GVwSinistrados.SelectedRow.Cells(7).Text)
                    Case 7
                        RedirecionaContinuar("SEGW0158ResumoSinistroRE.aspx", Me.GVwSinistrados.SelectedRow.Cells(8).Text, Me.GVwSinistrados.SelectedRow.Cells(7).Text)
                    Case 8
                        RedirecionaContinuar("SEGW0158DadosCliente.aspx", Me.GVwSinistrados.SelectedRow.Cells(8).Text, Me.GVwSinistrados.SelectedRow.Cells(7).Text)
                End Select
            End If
        Catch ex As Exception
            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub
    Private Function ValidaCPF() As Boolean
        Dim strCpfCnpj As String = ""
        Try
            strCpfCnpj = txtCPFCNPJ.Text.Trim()
            strCpfCnpj = strCpfCnpj.Replace(",", "")
            strCpfCnpj = strCpfCnpj.Replace("-", "")
            strCpfCnpj = strCpfCnpj.Replace("/", "")
            strCpfCnpj = strCpfCnpj.Replace(".", "")

            If strCpfCnpj.Length = 11 Or strCpfCnpj.Length = 14 Or strCpfCnpj.Length = 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Function

    Private Sub btnConsultar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        Dim objConsulta As New SEGS8909_SPS_EO_IN
        Dim objRetorno As New List(Of SEGS8909_SPS_EO_OUT)
        Dim buscador As New SEGS8909_SPS_Business
        Dim paramCount As Integer = 0

        Dim auxConvertDate As Date
        Try

            If Not String.IsNullOrEmpty(Me.txtDataInicial.Text) Then
                auxConvertDate = CDate(Me.txtDataInicial.Text)
            End If
        Catch ex As Exception
            Me.txtDataInicial.Focus()
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Data inicial inválida');", True)
            Exit Sub
        End Try
        Try
            If Not String.IsNullOrEmpty(Me.txtDataFinal.Text) Then
                auxConvertDate = CDate(Me.txtDataFinal.Text)
            End If
        Catch ex As Exception
            Me.txtDataFinal.Focus()
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Data final inválida');", True)
            Exit Sub
        End Try


        Try
            'objConsulta.Cpf_Cnpj = ""
            'objConsulta.Dt_Inclusao_Fim = ""
            'objConsulta.Dt_Inclusao_Ini = ""
            'objConsulta.Nome_Sinistrado = ""


            If Me.txtNome.Text <> "" Then
                objConsulta.Nome_Sinistrado = Me.txtNome.Text
                paramCount += 1
            End If
            If Me.txtCPFCNPJ.Text <> "" Then

                Dim strCpfCnpj = Me.txtCPFCNPJ.Text.Trim()

                strCpfCnpj = strCpfCnpj.Replace(",", "")
                strCpfCnpj = strCpfCnpj.Replace("-", "")
                strCpfCnpj = strCpfCnpj.Replace("/", "")
                strCpfCnpj = strCpfCnpj.Replace(".", "")

                objConsulta.Cpf_Cnpj = strCpfCnpj

                paramCount += 1
            End If
            If (Me.txtDataInicial.Text <> "" And Me.txtDataFinal.Text = "") Or (Me.txtDataInicial.Text = "" And Me.txtDataFinal.Text <> "") Then
                paramCount = -1
            Else
                If Me.txtDataInicial.Text <> "" And Me.txtDataFinal.Text <> "" Then
                    If IsDate(Me.txtDataInicial.Text.ToString) And IsDate(Me.txtDataFinal.Text.ToString) Then
                        If Me.txtDataInicial.Text = Me.txtDataFinal.Text Then
                            objConsulta.Dt_Inclusao_Ini = Me.txtDataInicial.Text + " 00:00"
                            objConsulta.Dt_Inclusao_Fim = Me.txtDataFinal.Text + " 23:59"
                        Else
                            objConsulta.Dt_Inclusao_Ini = Me.txtDataInicial.Text
                            objConsulta.Dt_Inclusao_Fim = Me.txtDataFinal.Text
                        End If
                        paramCount += 1
                    Else
                        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Data Invalida')", True)
                        Exit Sub
                    End If

                End If
            End If

            If paramCount = 0 Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Por favor, informe ao menos um parâmetro.')", True)
            ElseIf paramCount = -1 Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('É necessário especificar a data inicial e a data final, quando for preenchido algum dos campos.')", True)
            Else
                If ValidaCPF() Then

                    'ViewState("PropPendentes") = buscador.seleciona(objConsulta)
                    objRetorno = buscador.seleciona(objConsulta)
                    Session("PropPendentes") = objRetorno
                    Me.GVwSinistrados.DataSource = objRetorno
                    Me.GVwSinistrados.DataBind()
                    If GVwSinistrados.Rows.Count > 0 Then
                        Me.GVwSinistrados.Visible = True
                    Else
                        Me.GVwSinistrados.Visible = False
                    End If
                    'upLista.Update()
                Else
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), Guid.NewGuid().ToString(), "alert('Verifique se você digitou corretamente seu cpf ou cnpj no campo.')", True)
                End If
            End If
        Catch ex As Exception
            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Private Sub RedirecionaContinuar(ByVal pCaminho As String, ByVal pSinistroId As String, ByVal pFluxo As String)

        Response.Redirect(pCaminho + "?SINISTRO_ID=" + pSinistroId + "&fluxo=" + pFluxo + "&SLinkSeguro=" + Request("SLinkSeguro"))

    End Sub

    Private Sub GVwSinistrados_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVwSinistrados.PageIndexChanging
        Try
            Me.GVwSinistrados.PageIndex = e.NewPageIndex
            Me.GVwSinistrados.DataSource = Session("PropPendentes")
            Me.GVwSinistrados.DataBind()
        Catch ex As Exception
            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub btnComunicados_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnComunicados.Click
        Try
            botoes.Visible = False
            mainn.Visible = True
        Catch ex As Exception
             Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

    Protected Sub btnVoltar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoltar.Click

        Response.Redirect("SEGW0158ConsultaPropostasPendentes.aspx?SLinkSeguro=")

    End Sub
End Class