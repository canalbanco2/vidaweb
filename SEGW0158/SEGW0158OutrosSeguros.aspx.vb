﻿Imports SEGL0343.Task
Imports SEGL0343.EO

'Tiago Feitosa - 05/07/2010
Partial Public Class SEGW0158OutrosSeguros
    Inherits BasePages
    Const ap As Integer = 5
    Private _proposta_id As String

#Region "Propriedades"

    Class Apolices

        Private _apolice As String
        Private _nomeSeguradora As String


        Public Property apolice() As String
            Get
                Return _apolice
            End Get
            Set(ByVal value As String)
                _apolice = value
            End Set
        End Property
        Public Property nomeSeguradora() As String
            Get
                Return _nomeSeguradora
            End Get
            Set(ByVal value As String)
                _nomeSeguradora = value
            End Set
        End Property

    End Class

    Public ReadOnly Property Evento_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("Evento_ID")) Then
                Return Request.QueryString("Evento_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property SubEvento_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("SubEvento_ID")) Then
                Return Request.QueryString("SubEvento_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property Sinistro_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("SINISTRO_ID")) Then
                Return Request.QueryString("SINISTRO_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property Cliente_ID() As Integer
        Get
            If Not IsNothing(Request.QueryString("Cliente_ID")) Then
                Return Request.QueryString("Cliente_ID")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property Dt_Ocorrencia() As String
        Get
            If Not IsNothing(Request.QueryString("Dt_Ocorrencia")) Then
                Return Request.QueryString("Dt_Ocorrencia")
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public Property Proposta_ID() As String
        Get
            Return _proposta_id
        End Get
        Set(ByVal value As String)
            _proposta_id = value
        End Set
    End Property

    Public ReadOnly Property CPF_CNPJ() As String
        Get
            If Not IsNothing(Request.QueryString("CPF_CNPJ")) Then

                Return Request.QueryString("CPF_CNPJ")

            Else

                Return Nothing

            End If
        End Get
    End Property

#End Region

#Region "Métodos"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim o As New List(Of SEGL0343.EO.SEGS8899_SPS_EO_OUT)
            If Not Me.Page.IsPostBack Then
                RecuperaDadosGrid(Me.Sinistro_ID, o)
                'RecuperaDadosGrid(444, o)
            End If
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try
        If IsNothing(Session("DadosCliente")) Then
            AjustaNavegacao("LkbOutrosSeguros")
        Else
            AjustaNavegacao("DadosCliente")
        End If
        LkbOutrosSeguros.Text = " ► Outros Seguros"
        LkbOutrosSeguros.ForeColor = Drawing.Color.Blue
    End Sub
    Public Sub AjustaNavegacao(ByVal But As String)
        Select Case But
            Case "Evento"
                LkbPesquisa.Enabled = True
            Case "LkbSolicitante"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
            Case "LkbAgencia"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
            Case "LkbSinistroCA"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
            Case "LkbObjsegurado"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
            Case "LkbOutrosSeguros"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
                LkbObjsegurado.Enabled = True
            Case "Ressumo"
                LkbPesquisa.Enabled = True
                LkbEvento.Enabled = True
                LkbSolicitante.Enabled = True
                LkbAgencia.Enabled = True
                LkbSinistroCA.Enabled = True
                LkbObjsegurado.Enabled = True
            Case "SemPropostaCliente"
                LkbDadosCliente.Enabled = True
        End Select
    End Sub
    Protected Function RetornaDadosGrid() As List(Of Apolices)

        Dim listaA As New List(Of Apolices)
        Dim a As Apolices

        Try

            'popula a lista com os itens existentes
            For i As Integer = 0 To Me.GvApolices.Rows.Count - 1
                a = New Apolices
                a.apolice = Me.GvApolices.Rows(i).Cells(1).Text
                a.nomeSeguradora = Me.GvApolices.Rows(i).Cells(2).Text
                listaA.Add(a)
            Next i
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

        Return listaA

    End Function

    Protected Function RecuperaDadosGrid(ByVal id As Integer, ByRef s As List(Of SEGL0343.EO.SEGS8899_SPS_EO_OUT)) As Boolean
        Try
            Dim b As New SEGL0343.Business.SEGS8899_SPS_Business
            Dim e As New SEGL0343.EO.SEGS8899_SPS_EO_IN
            If id = -1 Then Return False
            e.Sinistro_Id = id
            s = b.seleciona(e)
            If s.Count > 0 Then
                Me.GvApolices.DataSource = s
                Me.GvApolices.DataBind()
                Return True
            Else
                Return False
            End If
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Function

    Protected Sub btnIncluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIncluir.Click

        Dim listaA As List(Of Apolices)
        Dim a As Apolices
        Try
            If Me.GvApolices.Rows.Count >= ap Then
                'retorna erro
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), _
                                                        Guid.NewGuid().ToString(), _
                                                        "alert('Já foi atingido o número máximo de " & ap.ToString & " apólices')", _
                                                        True)
                Exit Sub
            End If
            If Me.txtApolice.Text <> "" And Me.txtNomeSeguradora.Text <> "" Then
                listaA = RetornaDadosGrid()
                For i As Integer = 0 To listaA.Count - 1 Step 1
                    If Me.txtApolice.Text.Trim.ToUpper = listaA(i).apolice.Trim.ToUpper And Me.txtNomeSeguradora.Text.Trim.ToUpper = listaA(i).nomeSeguradora.Trim.ToUpper Then
                        'retorna erro
                        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), _
                                                                Guid.NewGuid().ToString(), _
                                                                "alert('Essa apólice já foi adicionada!')", _
                                                                True)
                        Exit Sub
                    End If
                Next i
                If Not IsNumeric(Me.txtApolice.Text.Trim) Then
                    'retorna erro
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), _
                                                            Guid.NewGuid().ToString(), _
                                                            "alert('O campo da apólice deve ser numérico!')", _
                                                            True)
                    Exit Sub
                End If
                'preenche a lista com o item a adicionar
                a = New Apolices
                a.apolice = Me.txtApolice.Text.Trim.ToUpper
                a.nomeSeguradora = Me.txtNomeSeguradora.Text.Trim.ToUpper
                listaA.Add(a)
                Me.GvApolices.DataSource = listaA
                Me.GvApolices.DataBind()
            End If
            txtApolice.Text = ""
            txtNomeSeguradora.Text = ""
        Catch ex As Exception


            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)

            If ex.InnerException Is Nothing Then
                'retorna erro
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), _
                                                        Guid.NewGuid().ToString(), _
                                                        "alert('Erro ao adicionar Apólice: " & ex.Message & "')", _
                                                        True)
            Else
                'retorna erro
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), _
                                                        Guid.NewGuid().ToString(), _
                                                        "alert('Erro ao adicionar Apólice: " & ex.InnerException.Message & "')", _
                                                        True)
            End If
        End Try

    End Sub

    Protected Function gravar() As Boolean

        Dim b As New SEGL0343.Business.SEGS8900_SPI_Business
        Dim e As SEGL0343.EO.SEGS8900_SPI_EO_IN
        Dim o As New List(Of SEGL0343.EO.SEGS8899_SPS_EO_OUT)
        Try
            'recupera dados do grid
            Dim lista As List(Of Apolices) = RetornaDadosGrid()
            RecuperaDadosGrid(Me.Sinistro_ID, o)
            'RecuperaDadosGrid(444, o)
            Dim excluir As String = ""

            If lista.Count > 0 Then

                'confere se a apolice ja existe
                If o.Count > 0 Then
                    For i As Integer = 0 To lista.Count - 1 Step 1
                        For n As Integer = 0 To o.Count - 1 Step 1
                            If o(n).apolice = lista(i).apolice And o(n).nomeSeguradora = lista(i).nomeSeguradora Then
                                If excluir = "" Then
                                    excluir = o(n).apolice.ToString & "," & o(n).nomeSeguradora
                                Else
                                    excluir = excluir & "|" & o(n).apolice.ToString & "," & o(n).nomeSeguradora
                                End If
                            End If
                        Next
                    Next i
                End If

                If excluir <> "" Then
                    For u As Integer = 0 To excluir.Split("|").Length - 1 Step 1
                        lista.RemoveAt(RetornaIndiceApolice(lista, excluir.Split("|")(u)))
                    Next
                End If

                'insere os dados no banco
                For i As Integer = 0 To lista.Count - 1 Step 1
                    e = New SEGL0343.EO.SEGS8900_SPI_EO_IN
                    e.Sinistro_Id = Me.Sinistro_ID
                    'e.Sinistro_Id = 444
                    e.Apolice = lista(i).apolice.Trim.ToUpper
                    e.Nomeseguradora = lista(i).nomeSeguradora.Trim.ToUpper
                    b.Insere(e)
                Next i

                RecuperaDadosGrid(Me.Sinistro_ID, o)
                'RecuperaDadosGrid(444, o)

            End If

            Return True

        Catch ex As Exception



            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)

            If ex.InnerException Is Nothing Then

                'retorna erro
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), _
                                                        Guid.NewGuid().ToString(), _
                                                        "alert('Erro ao Salvar Apólices: " & ex.Message & "')", _
                                                        True)
            Else
                'retorna erro
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), _
                                                        Guid.NewGuid().ToString(), _
                                                        "alert('Erro ao Salvar Apólices: " & ex.InnerException.Message & "')", _
                                                        True)
            End If

            Return False

        End Try

    End Function

    Function RetornaIndiceApolice(ByVal lista As List(Of Apolices), ByVal apoliceSeguradora As String) As Integer
        'Dim indice As Integer
        Try
            For n As Integer = 0 To apoliceSeguradora.Split("|").Length - 1
                For l As Integer = 0 To lista.Count - 1 Step 1
                    If apoliceSeguradora.Split(",")(0) = lista(l).apolice And apoliceSeguradora.Split(",")(1) = lista(l).nomeSeguradora Then
                        RetornaIndiceApolice = l
                        Exit Function
                    End If
                Next
            Next
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Function

    Protected Sub btnContinuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinuar.Click
        If gravar() Then
            Dim sb As New StringBuilder()
            sb.Append("SEGW0158ResumoSinistroRE.aspx")
            sb.Append("?SINISTRO_ID=" + IIf(IsNothing(Me.Sinistro_ID), "", Me.Sinistro_ID.ToString))
            sb.Append("&SLinkSeguro=" + Request("SLinkSeguro"))
            Response.Redirect(sb.ToString())
        End If
    End Sub

    Private Sub GvApolices_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles GvApolices.RowDeleted

    End Sub

    Private Sub GvApolices_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GvApolices.RowDeleting
        Dim b As New SEGL0343.Business.SEGS8901_SPD_Business
        Dim en As SEGL0343.EO.SEGS8901_SPD_EO_IN
        Try
            'recupera dados do grid
            Dim lista As List(Of Apolices) = RetornaDadosGrid()
            Dim _Apolice As String = ""
            Dim _Seguradora As String = ""
            'insere os dados no banco
            For i As Integer = 0 To lista.Count - 1 Step 1
                If lista(i).apolice = Me.GvApolices.Rows(e.RowIndex).Cells(1).Text And _
                lista(i).nomeSeguradora = Me.GvApolices.Rows(e.RowIndex).Cells(2).Text Then
                    _Apolice = Me.GvApolices.Rows(e.RowIndex).Cells(1).Text
                    _Seguradora = Me.GvApolices.Rows(e.RowIndex).Cells(2).Text
                    lista.RemoveAt(i)
                    Exit For
                End If
            Next i
            Me.GvApolices.DataSource = lista
            Me.GvApolices.DataBind()
            'apaga os dados no banco
            en = New SEGL0343.EO.SEGS8901_SPD_EO_IN
            en.Sinistro_Id = Me.Sinistro_ID
            'en.Sinistro_Id = 444
            en.Apolice = _Apolice
            en.Nomeseguradora = _Seguradora
            b.Exclui(en)
        Catch ex As Exception


            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)


            If ex.InnerException Is Nothing Then
                'retorna erro
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), _
                                                        Guid.NewGuid().ToString(), _
                                                        "alert('Erro ao Excluir Apólices: " & ex.Message & "')", _
                                                        True)
            Else
                'retorna erro
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), _
                                                        Guid.NewGuid().ToString(), _
                                                        "alert('Erro ao Excluir Apólices: " & ex.InnerException.Message & "')", _
                                                        True)
            End If
        End Try
    End Sub

    Protected Sub btnVoltar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoltar.Click

        Dim sb As New StringBuilder()

        sb.Append("SEGW0158ObjSegurado.aspx")
        sb.Append("?SINISTRO_ID=" + IIf(IsNothing(Me.Sinistro_ID), "", Me.Sinistro_ID.ToString))
        sb.Append("&SLinkSeguro=" + Request("SLinkSeguro"))
        'sb.Append("?Cliente_ID=" & Me.Cliente_ID)
        'sb.Append("&PS=" & 5)
        'sb.Append("&SINISTRO_ID=" & Me.Sinistro_ID)
        'sb.Append("&Evento_ID=" & Me.Evento_ID)
        'sb.Append("&SubEvento_ID=" & Me.SubEvento_ID)
        'sb.Append("&Dt_Ocorrencia=" & Me.Dt_Ocorrencia)
        'sb.Append("&CPF_CNPJ=" & Me.CPF_CNPJ)
        Response.Redirect(sb.ToString())


    End Sub

    Protected Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click
        Try
            gravar()
        Catch ex As Exception

            Dim objGeraSinistro As New SEGL0343.GeraSinistro_Business
            objGeraSinistro.TrataErro(ex.Message.ToString, "Page_Load", ex.StackTrace.ToString, ex.StackTrace.ToString, ex.Source.ToString & "        " & ex.TargetSite.ToString)
        End Try

    End Sub

#End Region

    Protected Sub LkbPesquisa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbPesquisa.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158ConsultaSinistrado.aspx")
        sb.Append("?Cliente_ID=" & Cliente_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())
    End Sub

    Protected Sub LkbEvento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbEvento.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158Evento.aspx")
        sb.Append("?SINISTRO_ID=" & Me.Sinistro_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))
        Response.Redirect(sb.ToString())

    End Sub

    Protected Sub LkbSolicitante_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbSolicitante.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158DadosSolicitante.aspx")
        sb.Append("?SINISTRO_ID=" + Me.Sinistro_ID.ToString)
        sb.Append("&SLinkSeguro=" + Request("SLinkSeguro"))
        Response.Redirect(sb.ToString())
    End Sub

    Protected Sub LkbAgencia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbAgencia.Click
        Dim objDel_IN As New SEGS8895_SPD_EO_IN
        Dim objDel_Business As New SEGL0343.Business.SEGS8895_SPD_Business

        objDel_IN.Sinistro_Id_Passos = Me.Sinistro_ID
        objDel_Business.Exclui(objDel_IN)

        Dim sb As New StringBuilder()

        sb.Append("SEGW0158DadosAgencia.aspx")
        sb.Append("?SINISTRO_ID=" + IIf(IsNothing(Me.Sinistro_ID), "", Me.Sinistro_ID.ToString))
        sb.Append("&SLinkSeguro=" + Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())
    End Sub

    Protected Sub LkbObjsegurado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbObjsegurado.Click
        Dim sb As New StringBuilder()

        sb.Append("SEGW0158ObjSegurado.aspx")
        sb.Append("?SINISTRO_ID=" + IIf(IsNothing(Me.Sinistro_ID), "", Me.Sinistro_ID.ToString))
        sb.Append("&SLinkSeguro=" + Request("SLinkSeguro"))
        'sb.Append("?Cliente_ID=" & Me.Cliente_ID)
        'sb.Append("&PS=" & 5)
        'sb.Append("&SINISTRO_ID=" & Me.Sinistro_ID)
        'sb.Append("&Evento_ID=" & Me.Evento_ID)
        'sb.Append("&SubEvento_ID=" & Me.SubEvento_ID)
        'sb.Append("&Dt_Ocorrencia=" & Me.Dt_Ocorrencia)
        'sb.Append("&CPF_CNPJ=" & Me.CPF_CNPJ)
        Response.Redirect(sb.ToString())
    End Sub

    Protected Sub LkbDadosCliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LkbDadosCliente.Click
        Dim sb As New StringBuilder()
        sb.Append("SEGW0158DadosCliente.aspx")
        sb.Append("?SINISTRO_ID=" & Me.Sinistro_ID)
        sb.Append("&SLinkSeguro=" & Request("SLinkSeguro"))

        Response.Redirect(sb.ToString())
    End Sub
End Class
