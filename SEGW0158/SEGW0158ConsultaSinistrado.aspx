﻿<%@ Page Language="vb" EnableViewStateMac="false" AutoEventWireup="false" Codebehind="SEGW0158ConsultaSinistrado.aspx.vb"
    Inherits="SEGW0158.SEGW0158ConsultaSinistrado" %>

<%@ Register Src="UserControls/Aguarde.ascx" TagName="Aguarde" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SUSAB - Aviso de Sinistro RE - Pesquisa Sinistrados</title>
    <link href="css/Sinistro.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server" defaultbutton="btnPesquisar">
        <div id="main" style="display: block;">
            <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600"
                EnableScriptGlobalization="true">
            </asp:ScriptManager>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upd_Filtro"
                DisplayAfter="1">
                <ProgressTemplate>
                    <div class="esmaecer">
                        <div class="icon">
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="upd_Filtro" runat="server">
                <Triggers>
                    <asp:PostBackTrigger ControlID="ddlSinistradoTipoPesquisa" />
                    <asp:PostBackTrigger ControlID="txtValorPesquisa" />
                </Triggers>
                <ContentTemplate>
                    <asp:Panel runat="server" ID="Panel2" Width="790px">
                        <fieldset id="flsSolicitante" style="text-align: left; width: 750px; height: 100px;">
                            <legend class="TitCaixa">Pesquisa do Sinistrado</legend>
                            <br />
                            <div class="n750">
                                <div class="n750">
                                    <div class="n750 divLeft">
                                        <label class="txtLabel  divLeft n110">
                                            Tipo de Pesquisa:
                                        </label>
                                        <asp:DropDownList ID="ddlSinistradoTipoPesquisa" ToolTip="Selecione o tipo de valor para acionar a pesquisa!" runat="server" Width="120px" CssClass="txtCampo divLeft n250"
                                            OnSelectedIndexChanged="ddlSinistradoTipoPesquisa_SelectedIndexChanged" AutoPostBack="True">
                                        </asp:DropDownList>
                                        <div class="n750 divLeft">
                                        <label id="txtLabelValPesquisa"  runat="server" class="txtLabel divLeft n110">
                                            Valor Pesquisa:
                                        </label>
                                        <asp:TextBox ID="txtValorPesquisa" ToolTip="Digite um argumento para acionar a pesquisa!" CssClass="txtCampo divLeft n150" runat="server"
                                            ValidationGroup="OnlyNumbers(event, null)" OnTextChanged="txtValorPesquisa_TextChanged"
                                            TabIndex="1001"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="meeCPF_CNPJ" runat="server" Mask="999,999,999-99" TargetControlID="txtValorPesquisa"
                                            CultureName="pt-BR" ClearMaskOnLostFocus="true">
                                        </cc1:MaskedEditExtender>
                                        <div id="btnPesquisar_fora" runat="server"  class="BotaoFora" style="text-align: center;
                                            float: left; padding-left: 10px; vertical-align: top;">
                                            <div class="BotaoE">
                                            </div>
                                            <asp:Button ID="btnPesquisar" ToolTip="Deseja Acionar Pesquisa? Digite um valor correspondente ao modo selecionado!" runat="server" TabIndex="1002" CssClass="BotaoM n120"
                                                Text="Pesquisar" OnClick="btnPesquisar_Click" />
                                            <div class="BotaoD">
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <br />
                        <fieldset id="Fieldset1" runat="server" style="text-align: left; width: 750px; height: 320px;">
                            <legend class="TitCaixa">Resultado</legend>
                            <asp:GridView ID="gvwSinistro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                OnPageIndexChanging="gvwSinistro_PageIndexChanging" OnSelectedIndexChanged="gvwSinistro_SelectedIndexChanged"
                                Width="750px">
                                <AlternatingRowStyle CssClass="txtTabela2" />
                                <SelectedRowStyle CssClass="txtTabela3" />
                                <PagerStyle CssClass="pager" />
                                <HeaderStyle CssClass="header_tabela" />
                                <RowStyle CssClass="txtTabela" />
                                <Columns>
                                    <asp:CommandField ButtonType="Image" SelectImageUrl="~/image/icon_bseta_menu_unsel.gif"
                                        ShowSelectButton="True"></asp:CommandField>
                                    <asp:BoundField HeaderText="Nome" DataField="Nome">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CPF_CGC" HeaderText="CPF" />
                                    <asp:BoundField HeaderText="Cliente ID" DataField="Cliente_ID">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Seg Cliente" DataField="seg_cliente">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <label class="txtLabel">
                                        Cliente não Localizado na Base.
                                    </label>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </fieldset>
                        <br />
                        <br />
                        <div id="btnAvisarSemProposta_fora" visible="False" runat="server" class="BotaoFora"
                            style="text-align: center; width: 200px; float: left;">
                            <div class="BotaoE">
                            </div>
                            <asp:Button ID="btnAvisarSemProposta" runat="server" CssClass="BotaoM n120" Text="Avisar sem Proposta"
                                OnClick="btnAvisarSemProposta_Click" />
                            <div class="BotaoD">
                            </div>
                        </div>
                    </asp:Panel>
                    <br />
                    <br />
                    <fieldset style="width: 950px; text-align: center; height: auto; border: none;">
                        <div id="Div3" style="width: 950px;">
                            <div class="n950">
                                <div class="n950 divLeft">
                                    <div class="BotaoFora n600 divLeft alignLeft">
                                        <div class="BotaoE">
                                        </div>
                                        <asp:Button ID="btnVoltar" runat="server" Text="<< Voltar" CssClass="BotaoM n120"
                                            Visible="true" OnClick="btnVoltar_Click" />
                                        <div class="BotaoD">
                                        </div>
                                    </div>
                                    <div id="btnContinuar_fora" class="BotaoFora n300 divLeft" visible="False" runat="server">
                                        <div class="BotaoE">
                                        </div>
                                        <asp:Button ID="btnContinuar" runat="server" Text="Continuar >>" CssClass="BotaoM n120"
                                            ValidationGroup="filter" OnClick="btnContinuar_Click" />
                                        <div class="BotaoD">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
