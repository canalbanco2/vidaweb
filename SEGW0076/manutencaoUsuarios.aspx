<%@ Page Language="vb" AutoEventWireup="false" Codebehind="manutencaoUsuarios.aspx.vb" EnableEventValidation="false" EnableViewStateMac="false"
    Inherits="SEGW0076._Default" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Manuten��o de usu�rios</title>
</head>
<body id="idBody" runat="server">

    <script language="javascript" type="text/javascript">
        function ChangeAllCheckBoxStates(p_sGrupo, p_oSelecionado)
        {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            if (p_sGrupo != null)
            {
                frm = document.getElementsByTagName("form");

                for(i=0;i<frm.length;i++) {
                    // reference to the elements of the form
		            e = frm[i].elements;
		            // loop over the length of those elements
            		for(j=0;j<e.length;j++) {
            		    if(e[j].getAttribute("type") == "checkbox") {
            		        //alert(p_sGrupo + ' / ' + p_oSelecionado);
            		        //alert(e[j].getAttribute("Grupo"));
            		        
            		        if (e[j].getAttribute("Grupo") == p_sGrupo) {
            		            if (e[j].getAttribute("id") != p_oSelecionado) {
        		                    e[j].checked = false;
            		            }
            		            else {
            		                e[j].checked = true;
            		            }
            		        }
            		    }
            		}
                }                 
            }
        }

        function ValidaCPF1(p_oTxtCPF) {
            v_sValue = p_oTxtCPF.value;
            
            v_sValue = v_sValue.toString().replace( "-", "" );
	        v_sValue = v_sValue.toString().replace( ".", "" );
	        v_sValue = v_sValue.toString().replace( ".", "" );
	        v_sValue = v_sValue.toString().replace( "/", "" );   
	        
	        if (v_sValue != '11111111111'){
	            return true;	            
	        }
	        else {
	            p_oTxtCPF.value = '';
	            p_oTxtCPF.focus();
	            alert('CPF com valor 11111111111 n�o pode ser cadastrado.')
	            return false;
	        }
        }

        // Valida��o de CPF e CNPJ
        function valida_CPFCNPJ(oSrc,args){
            s = args.Value;
            s = s.toString().replace( "-", "" );
	        s = s.toString().replace( ".", "" );
	        s = s.toString().replace( ".", "" );
	        s = s.toString().replace( "/", "" );
	        	        
            if (s.length == 11) {
                valida_CPF(oSrc,args);
            }
            else if(s.length == 11) {
               valida_CNPJ(oSrc, args);
            }
            else {
                return args.IsValid = false;
            }
        }

        //Valida��o de CPF
        function valida_CPF(oSrc,args){
            
            s = args.Value;
            //args.isValid = (s >= 3);
            //document.write(oSrc.Value + ',' + args.Value);
            
            s = s.toString().replace( "-", "" );
	        s = s.toString().replace( ".", "" );
	        s = s.toString().replace( ".", "" );
	        s = s.toString().replace( "/", "" );        
           
            if (isNaN(s)) {
                return args.IsValid = false;
            }

            var i;
            var c = s.substr(0,9);
            var dv = s.substr(9,2);
            var d1 = 0;

            for (i = 0; i < 9; i++) {
                d1 += c.charAt(i)*(10-i);
            }

            if (d1 == 0){
                return args.IsValid = false;
            } 

            d1 = 11 - (d1 % 11);

            if (d1 > 9) d1 = 0; 

            if (dv.charAt(0) != d1) {
                return args.IsValid = false; 
            }

            d1 *= 2;

            for (i = 0; i < 9; i++) {
                d1 += c.charAt(i)*(11-i);
            }

            d1 = 11 - (d1 % 11);

            if (d1 > 9) d1 = 0;

            if (dv.charAt(1) != d1) {
                return args.IsValid = false;
            }

            __doPostBack('TabContainer1_TabPanel1_fvUsuario_txtCPF', '');
            return args.IsValid = true;
            
            alert(args.value);
        } 

        //Valida��o de CNPJ
        function valida_CNPJ(oSrc, args){
            s = args.Value;
            
            s = s.toString().replace( "-", "" );
	        s = s.toString().replace( ".", "" );
	        s = s.toString().replace( ".", "" );
	        s = s.toString().replace( "/", "" );
	        
            if (isNaN(s)) {
                return args.IsValid = false;
            }
            alert(s);
            var i;
            var c = s.substr(0,12);
            var dv = s.substr(12,2);
            var d1 = 0;

            for (i = 0; i <12; i++){
                d1 += c.charAt(11-i)*(2+(i % 8));
            }

            if (d1 == 0) 
                return args.IsValid = false;

            d1 = 11 - (d1 % 11);

            if (d1 > 9) d1 = 0;

            if (dv.charAt(0) != d1){
                return args.IsValid = false;
            }

            d1 *= 2;

            for (i = 0; i < 12; i++){
                d1 += c.charAt(11-i)*(2+((i+1) % 8));
            }

            d1 = 11 - (d1 % 11);

            if (d1 > 9) 
            d1 = 0;

            if (dv.charAt(1) != d1){
                return args.IsValid = false;
            }

            return args.IsValid = true;

        } 

        function onKeyDown() {
          // current pressed key
          var pressedKey = String.fromCharCode(event.keyCode).toLowerCase();
            
          if (event.ctrlKey && (pressedKey == "c" || 
                                pressedKey == "v")) {
                                //alert(pressedKey);
            // disable key press porcessing
            event.returnValue = false;
          }

        } // onKeyDown
        
        function LimparCPF(p_oCPF) {
            var wVr = p_oCPF.value;
            
            wVr = wVr.toString().replace( "-", "" );
	        wVr = wVr.toString().replace( ".", "" );
	        wVr = wVr.toString().replace( ".", "" );
	        wVr = wVr.toString().replace( "/", "" );
	        
	        p_oCPF.value = wVr;	        
        }
        
        function FormataCPF_Onblur(p_oCPF) {
             var wVr = p_oCPF.value;
             var v_sCPF = wVr.substr(0,3) + '.' + wVr.substr(3,3) + '.' + wVr.substr(6,3) + '-' + wVr.substr(9,2);
             p_oCPF.value = v_sCPF;
        }
        
        function CPF_TextChanged(p_oTextCPF){
            var wVr = p_oTextCPF.value;
            
            wVr = wVr.toString().replace( "-", "" );
	        wVr = wVr.toString().replace( ".", "" );
	        wVr = wVr.toString().replace( ".", "" );
	        wVr = wVr.toString().replace( "/", "" );
                alert(wVr);	        
            if (wVr.lenght == 11){
                if (wVr != '11111111111'){
                    alert(p_oTextCPF.id)
                    __doPostBack('TabContainer1_TabPanel1_fvUsuario_txtCPF', 'TextChanged');
                }
                else {
                    alert('2')
                    alert('CPF com valor 11111111111 n�o pode ser cadastrado.');
                }
            }        
        }
    </script>

    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True">
        </asp:ScriptManager>
        <asp:FormView ID="fvApolice" runat="server" DataSourceID="odsApolice">
            <ItemTemplate>
                <table border="0" cellpadding="0" cellspacing="2" style="width: 100%">
                    <tr>
                        <td width="120">
                            <asp:Label ID="Label1" runat="server" CssClass="TituloFormulario" Text="Ramo/Apolice:"
                                Font-Bold="True"></asp:Label></td>
                        <td width="200">
                            <asp:Label ID="lblRamo_Apolice" runat="server" Text='<%# "(" & eval("ramo_id") & ") " & eval("apolice_id") %>'></asp:Label></td>
                        <td width="120">
                            <asp:Label ID="Label6" runat="server" CssClass="TituloFormulario" Text="Estipulante:"
                                Font-Bold="True"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblEstipulante" runat="server" Text="<%# bind('nome') %>"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label2" runat="server" CssClass="TituloFormulario" Text="SubGrupo:"
                                Font-Bold="True"></asp:Label></td>
                        <td colspan="3">
                            <asp:Label ID="lblSubGrupo" runat="server" Text='<%# Eval("sub_grupo_id") & " - " & Eval("sub_grupo") %>'></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label3" runat="server" CssClass="TituloFormulario" Text="Fun��o:"
                                Font-Bold="True"></asp:Label></td>
                        <td colspan="3">
                            <asp:Label ID="lblFuncao" runat="server">Manuten��o de Usu�rios</asp:Label></td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:FormView>
        <asp:ObjectDataSource ID="odsApolice" runat="server" SelectMethod="Select_DataTableApolice"
            TypeName="SEGW0076._Default">
            <SelectParameters>
                <asp:ControlParameter ControlID="hdnApoliceId" Name="ApoliceId" PropertyName="Value"
                    Type="Int32" />
                <asp:ControlParameter ControlID="hdnRamo" Name="RamoId" PropertyName="Value" Type="Int32" />
                <asp:ControlParameter ControlID="hdnSubgrupoId" Name="SubGrupoId" PropertyName="Value"
                    Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <br />
        <asp:Panel ID="Panel1" runat="server" Width="100%">
            <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="4">
                <cc1:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1">
                    <HeaderTemplate>
                        Dados B�sicos Usu�rios
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                            <asp:View ID="View1" runat="server">
                                <asp:GridView ID="gvUsuarios" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    DataSourceID="odsUsuarios" Width="100%" BorderWidth="1px" DataKeyNames="cpf,ramo_id,apolice_id"
                                    EmptyDataText="Nenhum usu�rio encontrado." CssClass="grid-view">
                                    <HeaderStyle Font-Bold="False" CssClass="header" />
                                    <Columns>
                                        <asp:BoundField DataField="login_web" HeaderText="Login">
                                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="nome" HeaderText="Nome">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="CPF">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CPF") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCPF" runat="server" Text='<%# CPF_Format(DataBinder.Eval(Container,"DataItem.CPF")) %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="120px" />
                                            <ItemStyle HorizontalAlign="Left" Width="120px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="email" HeaderText="E-mail">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerStyle CssClass="header" ForeColor="White" />
                                    <SelectedRowStyle BackColor="DeepSkyBlue" Font-Bold="True" Font-Italic="False" />
                                    <EmptyDataRowStyle Font-Bold="True" ForeColor="Red" />
                                    <RowStyle CssClass="normal" />
                                    <AlternatingRowStyle CssClass="alternate" />
                                    <FooterStyle ForeColor="#C0FFFF" />
                                </asp:GridView>
                                <asp:ObjectDataSource ID="odsUsuarios" runat="server" SelectMethod="Select_DataTableUsuarios"
                                    TypeName="SEGW0076._Default">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="hdnApoliceId" Name="ApoliceId" PropertyName="Value"
                                            Type="Int32" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                                <br />
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="btnSelecionar" runat="server" CssClass="Botao" Text="Incluir" Width="80px" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:View>
                            <asp:View ID="View2" runat="server">
                                <asp:Panel ID="pnUsuarios" runat="server" GroupingText="Usu�rio" Width="100%" ForeColor="#003399">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:FormView ID="fvUsuario" runat="server" DefaultMode="Insert" Width="100%" DataSourceID="odsFvUsuario">
                                                <InsertItemTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="130" align="right">
                                                                <asp:Label ID="lblNome" runat="server" Text="Nome:" CssClass="input"></asp:Label>
                                                                <asp:RequiredFieldValidator ID="rfvNome" runat="server" ControlToValidate="txtNome"
                                                                    Display="None" ErrorMessage=" - Nome � obrigat�rio." SetFocusOnError="True" ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator></td>
                                                            <td width="10">
                                                            </td>
                                                            <td width="450">
                                                                <asp:TextBox ID="txtNome" runat="server" MaxLength="100" Text="<%# bind('nome') %>"
                                                                    Width="90%"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Label ID="lblCPF" runat="server" Text="CPF:" CssClass="input"></asp:Label>
                                                                <asp:RequiredFieldValidator ID="rfvCPF" runat="server" ControlToValidate="txtCPF"
                                                                    Display="None" ErrorMessage=" - CPF � obrigat�rio." SetFocusOnError="True" ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtCPF" runat="server" MaxLength="11" OnTextChanged="txtCPF_TextChanged"
                                                                    Text="<%# bind('CPF') %>" Width="120px"></asp:TextBox>
                                                                <asp:CustomValidator ID="cvCPF" runat="server" ClientValidationFunction="valida_CPFCNPJ"
                                                                    ControlToValidate="txtCPF" ErrorMessage=" - CPF inv�lido." SetFocusOnError="True"
                                                                    ValidateEmptyText="True" ValidationGroup="grpUsuario">Inv�lido.</asp:CustomValidator></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Label ID="lblLogin" runat="server" Text="Login:" CssClass="input"></asp:Label>
                                                                <asp:RequiredFieldValidator ID="rfvLogin" runat="server" ControlToValidate="txtLogin"
                                                                    Display="None" ErrorMessage=" - Login � obrigat�rio." SetFocusOnError="True"
                                                                    ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator></td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtLogin" runat="server" MaxLength="8" Text="<%# bind('login_web') %>"
                                                                    Width="100px"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="revTxtLogin" runat="server" ControlToValidate="txtLogin"
                                                                    ErrorMessage="Login deve conter 8 caracteres." ValidationExpression=".{8,8}"></asp:RegularExpressionValidator></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Label ID="lblEmail" runat="server" Text="E-mail:" CssClass="input"></asp:Label>
                                                                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                                                                    Display="None" ErrorMessage=" - E-mail � obrigat�rio." SetFocusOnError="True"
                                                                    ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator></td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtEmail" runat="server" MaxLength="60" Text="<%# bind('Email') %>"
                                                                    Width="90%"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Label ID="lblEmail2" runat="server" Text="Confirma e-mail:" CssClass="input"></asp:Label>
                                                                <asp:RequiredFieldValidator ID="rfvEmail2" runat="server" ControlToValidate="txtEmail2"
                                                                    Display="None" ErrorMessage=" - Confirma��o de email � obrigat�rio." SetFocusOnError="True"
                                                                    ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator><asp:CompareValidator
                                                                        ID="cvEmail" runat="server" ControlToCompare="txtEmail2" ControlToValidate="txtEmail"
                                                                        ErrorMessage="Emails diferentes." SetFocusOnError="True" ValidationGroup="grpUsuario">*</asp:CompareValidator></td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtEmail2" runat="server" MaxLength="60" Width="90%"></asp:TextBox></td>
                                                        </tr>
                                                    </table>
                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List"
                                                        HeaderText="Campos com erro de preenchimento ou de  obrigat�riedade:" ValidationGroup="grpUsuario"
                                                        ShowMessageBox="True" />
                                                    &nbsp;<br />
                                                    <br />
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="right">
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Button ID="btnIncluir" runat="server" Text="Incluir" Width="80px" CommandName="Insert"
                                                                    CssClass="Botao" ValidationGroup="grpUsuario" />
                                                                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" Width="80px" CssClass="Botao"
                                                                    OnClick="btnCancelar_Click" /></td>
                                                        </tr>
                                                    </table>
                                                </InsertItemTemplate>
                                                <EditItemTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td width="130" align="right">
                                                                <asp:Label ID="lblNome" runat="server" Text="Nome:"></asp:Label>
                                                                <asp:RequiredFieldValidator ID="rfvNome" runat="server" ControlToValidate="txtNome"
                                                                    Display="None" ErrorMessage=" - Nome � obrigat�rio." SetFocusOnError="True" ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator></td>
                                                            <td width="10" style="color: #000000">
                                                            </td>
                                                            <td style="color: #000000">
                                                                <asp:TextBox ID="txtNome" runat="server" MaxLength="100" Text="<%# bind('nome') %>"
                                                                    Width="90%"></asp:TextBox></td>
                                                        </tr>
                                                        <tr style="color: #000000">
                                                            <td align="right">
                                                                <asp:Label ID="lblCPF" runat="server" Text="CPF:"></asp:Label>
                                                                <asp:RequiredFieldValidator ID="rfvCPF" runat="server" ControlToValidate="txtCPF"
                                                                    Display="None" ErrorMessage=" - CPF � obrigat�rio." SetFocusOnError="True" ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator></td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtCPF" runat="server" Enabled="False" MaxLength="11" OnTextChanged="txtCPF_TextChanged"
                                                                    Text="<%# bind('CPF') %>" Width="120px"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Label ID="lblLogin" runat="server" Text="Login:"></asp:Label>
                                                                <asp:RequiredFieldValidator ID="rfvLogin" runat="server" ControlToValidate="txtLogin"
                                                                    Display="None" ErrorMessage=" - Login � obrigat�rio." SetFocusOnError="True"
                                                                    ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator></td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtLogin" runat="server" Enabled="False" MaxLength="10" Text="<%# bind('login_web') %>"
                                                                    Width="100px"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Label ID="lblEmail" runat="server" Text="E-mail:"></asp:Label>
                                                                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                                                                    Display="None" ErrorMessage=" - E-mail � obrigat�rio." SetFocusOnError="True"
                                                                    ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator></td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtEmail" runat="server" MaxLength="60" Text="<%# bind('Email') %>"
                                                                    Width="90%"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Label ID="lblEmail2" runat="server" Text="Confirma e-mail:"></asp:Label>
                                                                <asp:RequiredFieldValidator ID="rfvEmail2" runat="server" ControlToValidate="txtEmail2"
                                                                    Display="None" ErrorMessage=" - Confirma��o de email � obrigat�rio." SetFocusOnError="True"
                                                                    ValidationGroup="grpUsuario">*</asp:RequiredFieldValidator>
                                                                <asp:CompareValidator ID="cvEmail" runat="server" ControlToCompare="txtEmail" ControlToValidate="txtEmail2"
                                                                    ErrorMessage="Emails diferentes." SetFocusOnError="True" ValidationGroup="grpUsuario">*</asp:CompareValidator></td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtEmail2" runat="server" MaxLength="60" Text='<%# Eval("Email") %>'
                                                                    Width="90%"></asp:TextBox></td>
                                                        </tr>
                                                    </table>
                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List"
                                                        HeaderText="Campos com erro de preenchimento ou de  obrigat�riedade:" ValidationGroup="grpUsuario" />
                                                    <br />
                                                    <br />
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="right">
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Button ID="btnRevogar" runat="server" OnClick="btnRevogar_Click" Text="Revogar Senha "
                                                                    CssClass="Botao" />
                                                                <asp:Button ID="btnIncluir" runat="server" Text="Alterar" Width="80px" CommandName="Update"
                                                                    CssClass="Botao" ValidationGroup="grpUsuario" />
                                                                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" Width="80px" CssClass="Botao"
                                                                    OnClick="btnCancelar_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <br />
                                                    <br />
                                                    <br />
                                                </EditItemTemplate>
                                            </asp:FormView>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="gvUsuarios" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="btnSelecionar" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:ObjectDataSource ID="odsFvUsuario" runat="server" InsertMethod="Incluir_Usuario"
                                        SelectMethod="Select_DataTableFvUsuario" TypeName="SEGW0076._Default" UpdateMethod="Alterar_Usuario">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="gvUsuarios" Name="CPF" PropertyName="SelectedDataKey(0)"
                                                Type="String" />
                                            <asp:ControlParameter ControlID="gvUsuarios" Name="ramo_id" PropertyName="SelectedDataKey(&quot;ramo_id&quot;)"
                                                Type="Int32" />
                                            <asp:ControlParameter ControlID="gvUsuarios" Name="apolice_id" PropertyName="SelectedDataKey(&quot;apolice_id&quot;)"
                                                Type="Int32" />
                                        </SelectParameters>
                                        <InsertParameters>
                                            <asp:ControlParameter ControlID="hdnApoliceId" Name="apolice_id" PropertyName="Value"
                                                Type="Int32" />
                                            <asp:ControlParameter ControlID="hdnRamo" Name="ramo_id" PropertyName="Value" Type="Int32" />
                                            <asp:ControlParameter ControlID="hdnSubgrupoId" Name="subgrupo_id" PropertyName="Value"
                                                Type="Int32" />
                                            <asp:Parameter Name="nome" Type="String" />
                                            <asp:Parameter Name="CPF" Type="String" />
                                            <asp:Parameter Name="login_web" Type="String" />
                                            <asp:Parameter Name="email" Type="String" />
                                            <asp:Parameter Name="Usuario" Type="String" />
                                            <asp:ControlParameter ControlID="hdnAmbienteId" Name="AmbienteId" PropertyName="Value"
                                                Type="Int32" />
                                        </InsertParameters>
                                        <UpdateParameters>
                                            <asp:ControlParameter ControlID="hdnApoliceId" Name="apolice_id" PropertyName="Value"
                                                Type="Int32" />
                                            <asp:ControlParameter ControlID="hdnRamo" Name="ramo_id" PropertyName="Value" Type="Int32" />
                                            <asp:ControlParameter ControlID="hdnSubgrupoId" Name="subgrupo_id" PropertyName="Value"
                                                Type="Int32" />
                                            <asp:Parameter Name="nome" Type="String" />
                                            <asp:Parameter Name="CPF" Type="String" />
                                            <asp:Parameter Name="login_web" Type="String" />
                                            <asp:Parameter Name="email" Type="String" />
                                            <asp:Parameter Name="Usuario" Type="String" />
                                            <asp:Parameter Name="AmbienteId" Type="Int32" />
                                        </UpdateParameters>
                                    </asp:ObjectDataSource>
                                </asp:Panel>
                            </asp:View>
                        </asp:MultiView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                </cc1:TabPanel>
                <cc1:TabPanel runat="server" HeaderText="TabPanel2" ID="TabPanel2">
                    <HeaderTemplate>
                        Administrador de Ap�lice
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvAdmApolice" runat="server" AutoGenerateColumns="False" CssClass="grid-view"
                                    DataSourceID="odsGvAdmApolice" DataKeyNames="CPF,email_enviado" EmptyDataText="Nenhum usu�rio encontrado.">
                                    <RowStyle CssClass="normal" />
                                    <Columns>
                                        <asp:BoundField DataField="nome" HeaderText="Nome">
                                            <ItemStyle Width="400px" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Ativo">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ckbAtivo" runat="server" AutoPostBack="True" OnCheckedChanged="ckbAtivo_CheckedChanged" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <AlternatingRowStyle CssClass="alternate" />
                                    <EmptyDataRowStyle Font-Bold="True" ForeColor="Red" />
                                </asp:GridView>
                                <br />
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="btnAdmApolice" runat="server" CssClass="Botao" Text="Gravar Adm. Apolice"
                                                Width="160px" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        &nbsp; &nbsp;&nbsp;&nbsp;<br />
                        <asp:ObjectDataSource ID="odsGvAdmApolice" runat="server" SelectMethod="Select_DataTablePermissaoUsuariosApolice"
                            TypeName="SEGW0076._Default">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hdnApoliceId" Name="ApoliceId" PropertyName="Value"
                                    Type="Int32" />
                                <asp:ControlParameter ControlID="hdnRamo" Name="RamoId" PropertyName="Value" Type="Int32" />
                                <asp:Parameter Name="Usuario" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </cc1:TabPanel>
                <cc1:TabPanel runat="server" HeaderText="TabPanel3" ID="TabPanel3">
                    <HeaderTemplate>
                        Administrador de SubGrupo
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                  <%--  Demanda:11937714 sumiu o botao gravar na web
                                        Data:08/08/2011
                                        Autor:Rodrigo.Moura
                                        Objetivo retirar o parametro HorizontalAlign="Left" porque deslocou o bot�o para fora da tela
                                        gerando Scroll: horizontal--%>
                                <asp:GridView ID="gvAdmSubGrupo" runat="server" AutoGenerateColumns="False" DataSourceID="odsGvAdmSubGrupo"
                                    CssClass="grid-view" DataKeyNames="CPF,email_enviado" EmptyDataText="Nenhum usu�rio encontrado." > 
                                    <RowStyle CssClass="normal" />
                                    <Columns>
                                        <asp:BoundField DataField="nome" HeaderText="Nome">
                                            <ItemStyle Width="400px" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Marcar todos">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkTodos" runat="server" AutoPostBack="True" OnCheckedChanged="chkTodos_CheckedChanged"  />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:DataList ID="dlSubGrupo" runat="server" OnItemDataBound="dlSubGrupo_ItemDataBound"
                                                    RepeatDirection="Horizontal" ShowFooter="False" DataKeyField="sub_grupo_id">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="ckbAdmSubGrupo" runat="server" AutoPostBack="True" OnCheckedChanged="CkbAdmSubGrupo_CheckedChanged" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="80px" />
                                                </asp:DataList>
                                                <asp:ObjectDataSource ID="odsDlSubGrupo" runat="server" SelectMethod="Select_DataTablePermissaoUsuariosSubGrupo"
                                                    TypeName="SEGW0076._Default"></asp:ObjectDataSource>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <asp:DataList ID="dlHeaderSubGrupo" runat="server" DataSourceID="odsDlHeaderSubGrupo"
                                                    RepeatDirection="Horizontal" OnItemDataBound="dlHeaderSubGrupo_ItemDataBound">
                                                    <ItemStyle Width="80px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHeaderGrupo" runat="server" ForeColor="White" Font-Bold="True"
                                                            ToolTip='<%# Eval("nome") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                                <asp:ObjectDataSource ID="odsDlHeaderSubGrupo" runat="server" SelectMethod="Select_DataTableSubGrupoApolice"
                                                    TypeName="SEGW0076._Default">
                                                    <SelectParameters>
                                                        <asp:ControlParameter ControlID="hdnApoliceId" Name="ApoliceId" PropertyName="Value"
                                                            Type="Int32" />
                                                        <asp:ControlParameter ControlID="hdnRamo" Name="RamoId" PropertyName="Value" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:ObjectDataSource>
                                            </HeaderTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <AlternatingRowStyle CssClass="alternate" />
                                    <EmptyDataRowStyle Font-Bold="True" ForeColor="Red" />
                                </asp:GridView>
                                <br />
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="btnAdmSubGrupo" runat="server" CssClass="Botao" Text="Gravar Adm. SubGrupo"
                                                Width="160px" /></td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:ObjectDataSource ID="odsGvAdmSubGrupo" runat="server" SelectMethod="Select_DataTablePermissaoUsuariosSubGrupo"
                            TypeName="SEGW0076._Default">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hdnApoliceId" Name="ApoliceId" PropertyName="Value"
                                    Type="Int32" />
                                <asp:ControlParameter ControlID="hdnRamo" Name="RamoId" PropertyName="Value" Type="Int32" />
                                <asp:Parameter Name="Usuario" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <br />
                    </ContentTemplate>
                </cc1:TabPanel>
                <cc1:TabPanel runat="server" HeaderText="TabPanel4" ID="TabPanel4">
                    <HeaderTemplate>
                        Operador
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvAdmOperador" runat="server" AutoGenerateColumns="False" DataSourceID="odsGvAdmOperador"
                                    CssClass="grid-view" DataKeyNames="CPF" EmptyDataText="Nenhum usu�rio encontrado.">
                                    <AlternatingRowStyle CssClass="alternate" />
                                    <Columns>
                                        <asp:BoundField DataField="nome" HeaderText="Nome">
                                            <ItemStyle Width="400px" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Marcar todos">
                                            <ItemTemplate>
                                                &nbsp;<asp:CheckBox ID="chkTodosOperador" runat="server" AutoPostBack="True" OnCheckedChanged="chkTodosOperador_CheckedChanged" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:DataList ID="dlOperador" runat="server" OnItemDataBound="dlOperador_ItemDataBound"
                                                    RepeatDirection="Horizontal" ShowFooter="False" DataKeyField="sub_grupo_id">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="ckbAdmOperador" runat="server" AutoPostBack="True" OnCheckedChanged="ckbAdmOperador_CheckedChanged" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="80px" />
                                                </asp:DataList>
                                                <asp:ObjectDataSource ID="odsDlOperador" runat="server"></asp:ObjectDataSource>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <asp:DataList ID="dlHeaderSubGrupo" runat="server" DataSourceID="odsDlHeaderSubGrupo"
                                                    RepeatDirection="Horizontal" OnItemDataBound="dlHeaderSubGrupo_ItemDataBound">
                                                    <ItemStyle Width="80px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHeaderGrupo" runat="server" Font-Bold="True" ForeColor="White"
                                                            ToolTip='<%# Eval("nome") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                                <asp:ObjectDataSource ID="odsDlHeaderSubGrupo" runat="server" SelectMethod="Select_DataTableSubGrupoApolice"
                                                    TypeName="SEGW0076._Default">
                                                    <SelectParameters>
                                                        <asp:ControlParameter ControlID="hdnApoliceId" Name="ApoliceId" PropertyName="Value"
                                                            Type="Int32" />
                                                        <asp:ControlParameter ControlID="hdnRamo" Name="RamoId" PropertyName="Value" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:ObjectDataSource>
                                            </HeaderTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="normal" />
                                    <EmptyDataRowStyle Font-Bold="True" ForeColor="Red" />
                                </asp:GridView>
                                <br />
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="btnAdmOperadores" runat="server" CssClass="Botao" Text="Gravar Operadores"
                                                Width="160px" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:ObjectDataSource ID="odsGvAdmOperador" runat="server" SelectMethod="Select_DataTablePermissaoUsuariosOperador"
                            TypeName="SEGW0076._Default">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hdnApoliceId" Name="ApoliceId" PropertyName="Value"
                                    Type="Int32" />
                                <asp:ControlParameter ControlID="hdnRamo" Name="RamoId" PropertyName="Value" Type="Int32" />
                                <asp:Parameter Name="Usuario" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <br />
                    </ContentTemplate>
                </cc1:TabPanel>
                <cc1:TabPanel ID="TabPanel5" runat="server" HeaderText="TabPanel5">
                    <ContentTemplate>
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvAdmMaster" runat="server" AutoGenerateColumns="False" DataSourceID="odsGvAdmMaster"
                                    CssClass="grid-view" DataKeyNames="CPF" EmptyDataText="Nenhum usu�rio encontrado.">
                                    <AlternatingRowStyle CssClass="alternate" />
                                    <Columns>
                                        <asp:BoundField DataField="nome" HeaderText="Nome">
                                            <ItemStyle Width="400px" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Marcar todos">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkTodosMaster" runat="server" AutoPostBack="True" OnCheckedChanged="chkTodosMaster_CheckedChanged" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:DataList ID="dlHeaderSubGrupo" runat="server" DataSourceID="odsDlHeaderSubGrupo"
                                                    RepeatDirection="Horizontal" OnItemDataBound="dlHeaderSubGrupo_ItemDataBound">
                                                    <ItemStyle Width="80px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHeaderGrupo" runat="server" Font-Bold="True" ForeColor="White"
                                                            ToolTip='<%# Eval("nome") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                                <asp:ObjectDataSource ID="odsDlHeaderSubGrupo" runat="server" SelectMethod="Select_DataTableSubGrupoApolice"
                                                    TypeName="SEGW0076._Default">
                                                    <SelectParameters>
                                                        <asp:ControlParameter ControlID="hdnApoliceId" Name="ApoliceId" PropertyName="Value"
                                                            Type="Int32" />
                                                        <asp:ControlParameter ControlID="hdnRamo" Name="RamoId" PropertyName="Value" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:ObjectDataSource>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:DataList ID="dlMaster" runat="server" OnItemDataBound="dlMaster_ItemDataBound"
                                                    RepeatDirection="Horizontal" ShowFooter="False" DataKeyField="sub_grupo_id" OnPreRender="dlMaster_PreRender">
                                                    <ItemStyle HorizontalAlign="Center" Width="80px" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="ckbAdmMaster" runat="server" AutoPostBack="True" OnCheckedChanged="ckbAdmMaster_CheckedChanged" />
                                                    </ItemTemplate>
                                                </asp:DataList>
                                                <asp:ObjectDataSource ID="odsDlOperador" runat="server"></asp:ObjectDataSource>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle Font-Bold="True" ForeColor="Red" />
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="normal" />
                                </asp:GridView>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="btnAdmMaster" runat="server" CssClass="Botao" Text="Gravar Master"
                                                Width="160px" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:ObjectDataSource ID="odsGvAdmMaster" runat="server" SelectMethod="Select_DataTablePermissaoUsuariosOperador"
                                    TypeName="SEGW0076._Default">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="hdnApoliceId" Name="ApoliceId" PropertyName="Value"
                                            Type="Int32" />
                                        <asp:ControlParameter ControlID="hdnRamo" Name="RamoId" PropertyName="Value" Type="Int32" />
                                        <asp:Parameter Name="Usuario" Type="String" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                    </ContentTemplate>
                    <HeaderTemplate>
                        Administrador Master
                    </HeaderTemplate>
                </cc1:TabPanel>
            </cc1:TabContainer></asp:Panel>
        <asp:HiddenField ID="hdnAcesso" runat="server" />
        <asp:HiddenField ID="hdnApoliceId" runat="server" />
        <asp:HiddenField ID="hdnRamo" runat="server" />
        <asp:HiddenField ID="hdnSubgrupoId" runat="server" />
        <asp:HiddenField ID="hdnAmbienteId" runat="server" />
        <br />
        <asp:Button ID="Button1" runat="server" Text="Button" Visible="False" />
        <div style="left: 60%; width: 100px; position: absolute; top: 70px; vertical-align: middle;">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <asp:Label ID="lblCarregando" runat="server" Font-Bold="True" ForeColor="White" Text="Aguarde..."
                        BackColor="Red" Width="150px"></asp:Label>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </form>
</body>
</html>
