Imports SEGL0315
Imports Alianca.FrameWork
Imports System.Security.Cryptography

Partial Public Class _Default
    'Inherits System.Web.UI.Page
    Inherits PaginaBaseSEGW

#Region " Membros "
    Private m_oRN As ClsUsuarioApoliceWebRN
#End Region

#Region " Propriedades "

    Public Shadows ReadOnly Property RN() As ClsUsuarioApoliceWebRN
        Get
            If IsNothing(m_oRN) Then
                m_oRN = New ClsUsuarioApoliceWebRN
            End If
            Return m_oRN
        End Get
    End Property

    Private ReadOnly Property Acesso() As Integer
        Get
            Return Me.hdnAcesso.Value
        End Get
    End Property

    Private ReadOnly Property ApoliceId() As Integer
        Get
            Return Request.QueryString("apolice") 'Me.hdnApoliceId.Value
        End Get
    End Property

    Private ReadOnly Property RamoId() As Integer
        Get
            Return IIf(Me.hdnRamo.Value.Equals(""), Request.QueryString("ramo"), Me.hdnRamo.Value)
        End Get
    End Property

    Private ReadOnly Property SubGrupoId() As Integer
        Get
            Return IIf(Me.hdnSubgrupoId.Value.Equals(""), Request.QueryString("subgrupo_id"), Me.hdnSubgrupoId.Value)
        End Get
    End Property

    Private ReadOnly Property GLAMBIENTE_ID() As Integer
        Get
            Return Me.hdnAmbienteId.Value 'Me.AmbienteID
            'Me.HelperSEGW.ObterSetting("GLAMBIENTE_ID")
        End Get
    End Property

    Dim m_oDtApolice As DataTable
    Private Property DtApolice(ByVal p_iApoliceId As Integer, ByVal p_iRamoId As Integer, ByVal p_iSubGrupoId As Integer) As Data.DataTable
        Get
            If IsNothing(Session("DtApolice")) Then
                m_oDtUsuarios = Me.HelperSEGW.RetornaDataTable_DadosApolice(p_iApoliceId _
                                                                            , p_iRamoId _
                                                                            , p_iSubGrupoId)
                Session("DtApolice") = m_oDtUsuarios
            End If

            Return Session("DtApolice")
        End Get
        Set(ByVal value As DataTable)
            Session("DtApolice") = value
        End Set
    End Property

    Dim m_oDtUsuarios As DataTable
    Private Property DtUsuarios(ByVal p_iApoliceId As Integer, ByVal p_iRamoId As Integer, ByVal p_sUsuario As String) As Data.DataTable
        Get
            If IsNothing(Session("DtUsuarios")) Then
                Dim v_aArrayParametros As New ArrayList

                v_aArrayParametros.Add(New ParametroDB("@apolice_id", p_iApoliceId))
                v_aArrayParametros.Add(New ParametroDB("@ramo_id", p_iRamoId))
                v_aArrayParametros.Add(New ParametroDB("@login_web", p_sUsuario))

                m_oDtUsuarios = Me.RN.Listar(v_aArrayParametros).Tables(0)

                Session("DtUsuarios") = m_oDtUsuarios
            End If

            Return Session("DtUsuarios")
        End Get
        Set(ByVal value As DataTable)
            Session("DtUsuarios") = value
        End Set
    End Property

    Dim m_oDtSubGrupo As DataTable
    Private Property DtSubGrupo(ByVal p_iApoliceId As Integer, ByVal p_iRamoId As Integer) As Data.DataTable
        Get
            If IsNothing(Session("DtSubGrupo")) Then
                m_oDtSubGrupo = Me.HelperSEGW.RetornaDataTable_DadosSubGrupoApolice(p_iApoliceId _
                                                                                    , p_iRamoId)
                Session("DtSubGrupo") = m_oDtSubGrupo
            End If

            Return Session("DtSubGrupo")
        End Get
        Set(ByVal value As DataTable)
            Session("DtSubGrupo") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            Me.DtApolice(Nothing, Nothing, Nothing) = Nothing
            Me.DtSubGrupo(Nothing, Nothing) = Nothing
            Me.DtUsuarios(Nothing, Nothing, Nothing) = Nothing

            Dim v_oTxtCPF As TextBox = DirectCast(Me.fvUsuario.FindControl("txtCPF"), TextBox)
            If Not IsNothing(v_oTxtCPF) Then
                'v_oTxtCPF.Attributes.Add("onBlur", "javascript: __doPostBack('" & v_oTxtCPF.ClientID & "', '');")
                v_oTxtCPF.Attributes.Add("onfocus", "LimparCPF(this);")
                v_oTxtCPF.Attributes.Add("onBlur", "//return ValidaCPF1(this); FormataCPF_Onblur(this);")
                v_oTxtCPF.Attributes.Add("onkeyDown", "return ValidaCPF1(this);")
                v_oTxtCPF.Attributes.Add("onchange", "return ValidaCPF1(this);")
                Me.HelperSEGW.ConfigurarCampoInteiro(v_oTxtCPF)

            End If

            Session("Acesso") = Request.QueryString("acesso")
            Me.hdnAcesso.Value = Request.QueryString("acesso")
            Me.hdnApoliceId.Value = Request.QueryString("apolice")
            Me.hdnRamo.Value = Request.QueryString("ramo")
            Me.hdnSubgrupoId.Value = Request.QueryString("subgrupo_id")
            Me.hdnAmbienteId.Value = Me.AmbienteID

            Me.HelperSEGW.GerarScript(Me.Page, "top.escondeaguarde();", "escondeaguarde")

            HabilitaDesabilitaTabs()
            VerificaAcessoMaster()
        End If

        'CriarCkbAdmSubGrupo()
        Me.odsGvAdmApolice.SelectParameters("Usuario").DefaultValue = MyBase.LoginWeb
        Me.odsGvAdmSubGrupo.SelectParameters("Usuario").DefaultValue = MyBase.LoginWeb
        Me.odsGvAdmOperador.SelectParameters("Usuario").DefaultValue = MyBase.LoginWeb
        Me.odsGvAdmMaster.SelectParameters("Usuario").DefaultValue = MyBase.LoginWeb
        Me.odsFvUsuario.UpdateParameters("Usuario").DefaultValue = MyBase.LoginWeb
        Me.odsFvUsuario.InsertParameters("Usuario").DefaultValue = MyBase.LoginWeb
    End Sub

#Region " Eventos GridView "
    Protected Sub gvUsuarios_RowDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvUsuarios.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            If Me.Acesso <> 5 Then
                Me.HelperSEGW.CarregarTrocaCorLinhaGridView(e, , HelperSEGW.EnumTipoCursor.Hand)
                e.Row.Attributes.Add("onClick", ClientScript.GetPostBackEventReference(Me.gvUsuarios, "Select$" + e.Row.RowIndex.ToString()))
            End If

            Dim v_olblCPF As Label = e.Row.FindControl("lblCPF")
            If Not IsNothing(v_olblCPF) Then
                v_olblCPF.Attributes.Add("style", "color: #000000;")
            End If
        End If
    End Sub

    Private Sub gvUsuarios_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvUsuarios.SelectedIndexChanged
        If Me.gvUsuarios.SelectedIndex >= 0 Then
            Me.MultiView1.SetActiveView(Me.View2)

            Me.fvUsuario.ChangeMode(FormViewMode.Edit)
            Me.fvUsuario.DataBind()

            Dim v_oTxtEmail As TextBox = fvUsuario.FindControl("txtEmail")
            If Not IsNothing(v_oTxtEmail) Then
                v_oTxtEmail.Attributes.Add("onkeydown", "onKeyDown()")
            End If
            Dim v_oTxtEmail2 As TextBox = fvUsuario.FindControl("txtEmail2")
            If Not IsNothing(v_oTxtEmail2) Then
                v_oTxtEmail2.Attributes.Add("onkeydown", "onKeyDown()")
            End If

            ' breno.nery (Nova Consultoria) - 01/08/2011
            ' 9420650 - Melhorias no Sistema Vida Web 
            ' Central de Atendimento (Acesso = 6) com mesmo acesso de Adm. Alian�a do Brasil (Acesso = 1)
            If Me.Acesso <= 6 Then
                Me.TabPanel4.Enabled = False
                Me.TabPanel3.Enabled = False
                Me.TabPanel2.Enabled = False
                'Me.TabPanel4.Visible = False
                'Me.TabPanel3.Visible = False
                'Me.TabPanel2.Visible = False
            Else
                Dim v_oBtnRevogar As Button = fvUsuario.FindControl("btnRevogar")
                Dim v_oBtnIncluir As Button = fvUsuario.FindControl("btnIncluir")
                v_oBtnIncluir.Enabled = False
            End If
        End If
    End Sub

    Private Sub gvAdmApolice_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAdmApolice.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim v_oCkbAtivo As CheckBox = e.Row.FindControl("ckbAtivo")
            If Not IsNothing(v_oCkbAtivo) Then
                Dim v_bAcesso As Boolean = False
                If Not IsDBNull(DataBinder.Eval(e.Row.DataItem, "ind_situacao")) Then
                    If DataBinder.Eval(e.Row.DataItem, "ind_situacao") = "A" Then v_bAcesso = True
                End If

                v_oCkbAtivo.Checked = v_bAcesso
                v_oCkbAtivo.InputAttributes.Add("Grupo", "AdmApoliceAtivo")
                'v_oCkbAtivo.Attributes.Add("onClick", "ChangeAllCheckBoxStates('AdmApoliceAtivo','" & v_oCkbAtivo.ClientID & "')")
            End If
        End If
    End Sub

    Private Sub gvAdmSubGrupo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAdmSubGrupo.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then

            '-------------------
            Dim v_oCkbAtivo As CheckBox = e.Row.FindControl("ckbTodos")
            If Not IsNothing(v_oCkbAtivo) Then
                v_oCkbAtivo.InputAttributes.Add("Grupo", "AdmSubgrupoTodos")
                v_oCkbAtivo.InputAttributes.Add("Todos", "AdmSubgrupoTodos")
            End If

            '-------------------
            Dim v_sCPF As String = DataBinder.Eval(e.Row.DataItem, "CPF")
            If Not IsDBNull(DataBinder.Eval(e.Row.DataItem, "ind_situacao")) Then
                If DataBinder.Eval(e.Row.DataItem, "ind_situacao") = "A" Then e.Row.BackColor = Drawing.Color.Silver
            End If

            DirectCast(e.Row.FindControl("dlSubGrupo"), DataList).DataSource = Me.BindDataListAdm(v_sCPF, 3)
            DirectCast(e.Row.FindControl("dlSubGrupo"), DataList).DataBind()
        End If
    End Sub

    Private Sub gvAdmOperador_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAdmOperador.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim v_sCPF As String = DataBinder.Eval(e.Row.DataItem, "CPF")
            If Not IsDBNull(DataBinder.Eval(e.Row.DataItem, "ind_situacao")) Then
                If DataBinder.Eval(e.Row.DataItem, "ind_situacao") = "A" Then e.Row.BackColor = Drawing.Color.Silver
            End If

            DirectCast(e.Row.FindControl("dlOperador"), DataList).DataSource = Me.BindDataListAdm(v_sCPF, 4)
            DirectCast(e.Row.FindControl("dlOperador"), DataList).DataBind()
        End If
    End Sub

    Private Sub gvAdmMaster_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAdmMaster.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim v_sCPF As String = DataBinder.Eval(e.Row.DataItem, "CPF")
            If Not IsDBNull(DataBinder.Eval(e.Row.DataItem, "ind_situacao")) Then
                If DataBinder.Eval(e.Row.DataItem, "ind_situacao") = "A" Then e.Row.BackColor = Drawing.Color.Silver
            End If

            DirectCast(e.Row.FindControl("dlMaster"), DataList).DataSource = Me.BindDataListAdm(v_sCPF, 7)
            DirectCast(e.Row.FindControl("dlMaster"), DataList).DataBind()
        End If
    End Sub
#End Region

#Region " Eventos Datalist "
    Protected Sub dlSubGrupo_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim v_iSubGrupoId As Integer = DataBinder.Eval(e.Item.DataItem, "sub_grupo_id")

            e.Item.ToolTip = DataBinder.Eval(e.Item.DataItem, "nome_subgrupo")

            Dim v_oCkbAdmSubGrupo As CheckBox = e.Item.FindControl("ckbAdmSubGrupo")
            If Not IsNothing(v_oCkbAdmSubGrupo) Then
                Dim v_bAcesso As Boolean = False
                If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "ind_situacao")) Then
                    If DataBinder.Eval(e.Item.DataItem, "ind_situacao") = "A" Then v_bAcesso = True
                End If

                v_oCkbAdmSubGrupo.Checked = v_bAcesso
                v_oCkbAdmSubGrupo.ToolTip = DataBinder.Eval(e.Item.DataItem, "nome_subgrupo")

                v_oCkbAdmSubGrupo.InputAttributes.Add("Grupo", "AdmSubGrupo_" & e.Item.ItemIndex)
                v_oCkbAdmSubGrupo.InputAttributes.Add("SubGrupoId", v_iSubGrupoId)
            End If
        End If
    End Sub

    Protected Sub dlOperador_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim v_iSubGrupoId As Integer = DataBinder.Eval(e.Item.DataItem, "sub_grupo_id")

            e.Item.ToolTip = DataBinder.Eval(e.Item.DataItem, "nome_subgrupo")

            Dim v_oCkbAdmSubGrupo As CheckBox = e.Item.FindControl("ckbAdmOperador")
            If Not IsNothing(v_oCkbAdmSubGrupo) Then
                Dim v_bAcesso As Boolean = False
                If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "ind_situacao")) Then
                    If DataBinder.Eval(e.Item.DataItem, "ind_situacao") = "A" Then v_bAcesso = True
                End If

                v_oCkbAdmSubGrupo.Checked = v_bAcesso
                v_oCkbAdmSubGrupo.ToolTip = DataBinder.Eval(e.Item.DataItem, "nome_subgrupo")

                v_oCkbAdmSubGrupo.InputAttributes.Add("Grupo", "AdmOperador_" & e.Item.ItemIndex)
                v_oCkbAdmSubGrupo.InputAttributes.Add("SubGrupoId", v_iSubGrupoId)
            End If
        End If
    End Sub

    Protected Sub dlMaster_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim v_iSubGrupoId As Integer = DataBinder.Eval(e.Item.DataItem, "sub_grupo_id")

            e.Item.ToolTip = DataBinder.Eval(e.Item.DataItem, "nome_subgrupo")

            Dim v_oCkbAdmSubGrupo As CheckBox = e.Item.FindControl("ckbAdmMaster")
            If Not IsNothing(v_oCkbAdmSubGrupo) Then
                Dim v_bAcesso As Boolean = False
                If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "ind_situacao")) Then
                    If DataBinder.Eval(e.Item.DataItem, "ind_situacao") = "A" Then
                        v_bAcesso = True

                        HabDesabCheckBoxGrupo(Me.gvAdmOperador.Controls, v_iSubGrupoId, False)
                        HabDesabCheckBoxGrupo(Me.gvAdmSubGrupo.Controls, v_iSubGrupoId, False)
                    End If
                End If

                v_oCkbAdmSubGrupo.Checked = v_bAcesso
                v_oCkbAdmSubGrupo.ToolTip = DataBinder.Eval(e.Item.DataItem, "nome_subgrupo")

                v_oCkbAdmSubGrupo.InputAttributes.Add("Grupo", "AdmMaster_" & e.Item.ItemIndex)
                v_oCkbAdmSubGrupo.InputAttributes.Add("SubGrupoId", v_iSubGrupoId)
            End If
        End If
    End Sub

    Protected Sub dlHeaderSubGrupo_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim v_oLblHeaderGrupo As Label = e.Item.FindControl("lblHeaderGrupo")
            If Not IsNothing(v_oLblHeaderGrupo) Then
                v_oLblHeaderGrupo.Text = "Subgrupo " & DataBinder.Eval(e.Item.DataItem, "sub_grupo_id").ToString()
                'v_oLblHeaderGrupo.ToolTip = DataBinder.Eval(e.Item.DataItem, "nome_subgrupo")
            End If
        End If
    End Sub
#End Region

#Region " Eventos FormView "
    Private Sub fvUsuario_ItemInserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewInsertEventArgs) Handles fvUsuario.ItemInserting
        If DirectCast(DirectCast(sender, FormView).FindControl("txtlogin"), TextBox).Text.Length = 8 Then
            Me.RN.CPF = CPF_Limpar(DirectCast(DirectCast(sender, FormView).FindControl("txtCPF"), TextBox).Text)
            Me.RN.LoginWeb = DirectCast(DirectCast(sender, FormView).FindControl("txtLogin"), TextBox).Text

            If Me.RN.Verificar_Login Then
                e.Cancel = True
                'Me.HelperSEGW.GerarAlert(Me.Page, "Login j� existe.", "LoginExiste")
                ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel1, Me.UpdatePanel1.GetType(), "JavaScript", "alert('Login j� existe.');", True)
            End If
        Else
            e.Cancel = True
            ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel1, Me.UpdatePanel1.GetType(), "JavaScript", "alert('O login deve conter 8 caracteres.');", True)
        End If
    End Sub

    Private Sub fvUsuario_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewInsertedEventArgs) Handles fvUsuario.ItemInserted
        If IsNothing(e.Exception) Then
            'Me.HelperSEGW.GerarAlert(Me.Page, "Usu�rio cadastrado com sucesso.", "Cadastro")
            ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel1, Me.UpdatePanel1.GetType(), "JavaScript", "alert('Usu�rio cadastrado com sucesso.');", True)
            Me.MultiView1.SetActiveView(Me.View1)
            'ATUALIZA DATABASE
            Session("DtUsuarios") = Nothing
            Me.gvAdmMaster.DataBind()
            Me.gvAdmApolice.DataBind()
            Me.gvAdmSubGrupo.DataBind()
            Me.gvAdmOperador.DataBind()
            Me.gvUsuarios.DataBind()
            HabilitaDesabilitaTabs()
        Else
            ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel1, Me.UpdatePanel1.GetType(), "JavaScript", "alert('Ocorreu erro ao cadastrar o usu�rio.\nErro: " & e.Exception.InnerException.InnerException.Message.Replace("'", "").Replace(Chr(13), "\n").Replace(Chr(10), "") & "');", True)
            e.ExceptionHandled = True
            e.KeepInInsertMode = True
        End If
    End Sub

    Private Sub fvUsuario_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewUpdateEventArgs) Handles fvUsuario.ItemUpdating

    End Sub

    Private Sub fvUsuario_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewUpdatedEventArgs) Handles fvUsuario.ItemUpdated
        Me.MultiView1.SetActiveView(Me.View1)
        Me.gvUsuarios.SelectedIndex = -1
        Me.gvUsuarios.DataBind()
        HabilitaDesabilitaTabs()
        ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel1, Me.UpdatePanel1.GetType(), "JavaScript", "alert('Atualiza��o feita com sucesso.');", True)
    End Sub

    Private Sub fvUsuario_ModeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fvUsuario.ModeChanged
        'Desabilita o evento de colar
        Dim v_oTxtEmail As TextBox = fvUsuario.FindControl("txtEmail")
        If Not IsNothing(v_oTxtEmail) Then
            v_oTxtEmail.Attributes.Add("onkeydown", "onKeyDown()")
        End If
        Dim v_oTxtEmail2 As TextBox = fvUsuario.FindControl("txtEmail2")
        If Not IsNothing(v_oTxtEmail2) Then
            v_oTxtEmail2.Attributes.Add("onkeydown", "onKeyDown()")
        End If
    End Sub
#End Region

#Region " M�todo para DataSource "
    Public Function Select_DataTableApolice(ByVal ApoliceId As Integer, ByVal RamoId As Integer, ByVal SubGrupoId As Integer) As DataTable
        Return Me.DtApolice(ApoliceId, RamoId, SubGrupoId)
    End Function

    Public Function Select_DataTableUsuarios(ByVal ApoliceId As Integer) As DataTable
        Dim v_aArrayParametros As New ArrayList
        Dim v_oDtUsuarios As DataTable

        v_aArrayParametros.Add(New ParametroDB("@apolice_id", ApoliceId))

        v_oDtUsuarios = Me.RN.Listar(v_aArrayParametros).Tables(0)

        Return v_oDtUsuarios

    End Function

    Public Sub Incluir_Usuario(ByVal apolice_id As Integer, _
                                ByVal ramo_id As Integer, _
                                ByVal subgrupo_id As Integer, _
                                ByVal nome As String, _
                                ByVal CPF As String, _
                                ByVal login_web As String, _
                                ByVal email As String, _
                                ByVal Usuario As String, _
                                ByVal AmbienteId As Integer)

        If Not IsNothing(CPF) Then
            Dim v_aSenha As ArrayList = Me.GerarSenha()

            Me.RN.DAO.IniciarTransacao(Me)
            Try
                Dim v_bAlterarSenha As Boolean = False
                Dim v_oClsUsuarioTestaRN As New ClsUsuarioRN
                v_oClsUsuarioTestaRN.CPF = CPF_Limpar(CPF)
                If v_oClsUsuarioTestaRN.Obter Then
                    If v_oClsUsuarioTestaRN.LoginRede <> login_web Then v_bAlterarSenha = True
                    If v_oClsUsuarioTestaRN.Email <> email Then v_bAlterarSenha = True

                    If Not v_bAlterarSenha Then v_aSenha(0) = ""
                Else
                    v_bAlterarSenha = True
                End If

                If v_bAlterarSenha Then
                    'CADASTRA/ATUALIZA USUARIO_TB
                    Dim v_oClsUsuarioRN As New ClsUsuarioRN(Me.RN.DAO)
                    v_oClsUsuarioRN.Nome = nome
                    v_oClsUsuarioRN.CPF = CPF_Limpar(CPF)
                    v_oClsUsuarioRN.LoginWeb = login_web
                    v_oClsUsuarioRN.Email = email
                    v_oClsUsuarioRN.SenhaWeb = v_aSenha(1)
                    v_oClsUsuarioRN.Usuario = Usuario
                    v_oClsUsuarioRN.Incluir()
                End If

                'CADASTRA USUARIO_APOLICE_WEB_TB
                Me.RN.ApoliceId = apolice_id
                Me.RN.RamoId = ramo_id
                Me.RN.CPF = CPF_Limpar(CPF)
                Me.RN.Nome = nome
                Me.RN.LoginWeb = login_web
                Me.RN.Email = email
                Me.RN.EmailEnviado = "N"
                Me.RN.Usuario = Usuario
                Me.RN.Incluir()

                If AmbienteId <= 3 Then
                    'ATUALIZA CADASTROS
                    Me.HelperSEGW.AtualizaCadastros(Me.RN.DAO, AmbienteId)
                End If

                'ENVIA EMAIL
                'Dim v_sAssunto As String = "Confirma��o de Cadastro na Web"
                'Dim v_sDe As String = "emissao.vida@aliancadobrasil.com.br"
                'Dim v_sMensagem As String = Me.EmailMensagem(v_aSenha(0), _
                '                                            CPF_Limpar(CPF), _
                '                                            nome, _
                '                                            login_web, _
                '                                            apolice_id, _
                '                                            ramo_id, _
                '                                            subgrupo_id)

                'Dim v_iEmailId As Integer = SEGL0315.HelperSEGW.EnviarEmailBanco(v_sAssunto, _
                '                                                                v_sDe, _
                '                                                                email, _
                '                                                                2, _
                '                                                                v_sMensagem, _
                '                                                                "SEGBR", _
                '                                                                "SEGW0076", _
                '                                                                Usuario, _
                '                                                                "ddmg0802")

                'Me.IncluirLogEmailUsuario(apolice_id, ramo_id, subgrupo_id, v_iEmailId, Usuario, Me.RN.DAO)

                Me.RN.DAO.FinalizarTransacao(True, Me)
            Catch ex As Exception
                Me.RN.DAO.FinalizarTransacao(False, Me)
                Throw ex
            End Try
        End If
    End Sub

    Public Sub Alterar_Usuario(ByVal apolice_id As Integer, _
                                ByVal ramo_id As Integer, _
                                ByVal subgrupo_id As Integer, _
                                ByVal nome As String, _
                                ByVal CPF As String, _
                                ByVal login_web As String, _
                                ByVal email As String, _
                                ByVal Usuario As String, _
                                ByVal AmbienteId As Integer)

        Dim v_bAlterarSenha As Boolean = False
        Dim v_aSenha As ArrayList = Me.GerarSenha()
        Dim v_oEmailAntigo As String = ""

        Dim v_oClsUsuarioRN As New ClsUsuarioRN()
        v_oClsUsuarioRN.CPF = CPF_Limpar(CPF)
        If v_oClsUsuarioRN.Obter() Then
            If v_oClsUsuarioRN.LoginRede <> login_web Then v_bAlterarSenha = True
            If v_oClsUsuarioRN.Email <> email Then v_bAlterarSenha = True

            If Not v_bAlterarSenha Then v_aSenha(0) = ""
        End If

        Me.RN.DAO.IniciarTransacao(Me)
        Try

            If v_bAlterarSenha Then
                'CADASTRA/ATUALIZA USUARIO_TB
                Dim v_oClsUsuarioAlterarRN As New ClsUsuarioRN(Me.RN.DAO)
                v_oClsUsuarioRN.Nome = nome
                v_oClsUsuarioRN.CPF = CPF_Limpar(CPF)
                v_oClsUsuarioRN.LoginWeb = login_web
                v_oClsUsuarioRN.Email = email
                v_oClsUsuarioRN.SenhaWeb = v_aSenha(1)
                v_oClsUsuarioRN.Usuario = Usuario
                v_oClsUsuarioRN.Incluir()

                Dim v_oClsUsuario As New ClsUsuarioRN(Me.RN.DAO)
                v_oClsUsuario.SenhaWeb = v_aSenha(1)
                v_oClsUsuario.CPF = CPF_Limpar(CPF)
                v_oClsUsuario.Usuario = Usuario
                v_oClsUsuario.Revogar()
            End If

            Me.RN.ApoliceId = apolice_id
            Me.RN.RamoId = ramo_id
            Me.RN.CPF = CPF_Limpar(CPF)
            Me.RN.Nome = nome
            Me.RN.LoginWeb = login_web
            Me.RN.Email = email
            Me.RN.Usuario = Usuario
            Me.RN.Alterar()

            If v_bAlterarSenha Then

                If AmbienteId <= 3 Then
                    'ATUALIZA CADASTROS
                    Me.HelperSEGW.AtualizaCadastros(Me.RN.DAO, AmbienteId)
                End If

                'ENVIA EMAIL
                Dim v_sAssunto As String = "Confirma��o de Cadastro na Web"
                Dim v_sDe As String = "emissao.vida@aliancadobrasil.com.br"
                Dim v_sMensagem As String = Me.EmailMensagem(v_aSenha(0), _
                                                            CPF_Limpar(CPF), _
                                                            nome, _
                                                            login_web, _
                                                            apolice_id, _
                                                            ramo_id, _
                                                            subgrupo_id)

                Dim v_iEmailId As Integer = SEGL0315.HelperSEGW.EnviarEmailBanco(v_sAssunto, _
                                                                                v_sDe, _
                                                                                email, _
                                                                                2, _
                                                                                v_sMensagem, _
                                                                                "SEGBR", _
                                                                                "SEGW0076", _
                                                                                Usuario, _
                                                                                "ddmg0802")

                Me.IncluirLogEmailUsuario(apolice_id, ramo_id, subgrupo_id, v_iEmailId, Usuario, Me.RN.DAO)
            End If

            Me.RN.DAO.FinalizarTransacao(True, Me)
        Catch ex As Exception
            Me.RN.DAO.FinalizarTransacao(False, Me)
            Throw ex
        End Try
    End Sub

    Public Function Select_DataTableFvUsuario(ByVal CPF As String, ByVal ramo_id As Integer, ByVal apolice_id As Integer) As DataTable

        Dim v_aArrayParametros As New ArrayList
        Dim v_oDtUsuarios As DataTable

        v_aArrayParametros.Add(New ParametroDB("@CPF", CPF))
        v_aArrayParametros.Add(New ParametroDB("@apolice_id", apolice_id))
        v_aArrayParametros.Add(New ParametroDB("@ramo_id", ramo_id))

        v_oDtUsuarios = Me.RN.Listar(v_aArrayParametros).Tables(0)

        Return v_oDtUsuarios

    End Function

    Private Function BindDataListAdm(ByVal p_sCPF As String, ByVal p_iIndAcesso As Integer) As DataTable
        '3 - ADMINISTRADOR DE SUBGRUPO
        '4 - OPERADOR
        '7 - MASTER

        Dim v_oClsUsuarioApoliceRN As New ClsUsuarioApoliceRN
        Dim v_oDtUsuarios As DataTable

        v_oClsUsuarioApoliceRN.ApoliceId = Me.ApoliceId
        v_oClsUsuarioApoliceRN.RamoId = Me.RamoId
        v_oClsUsuarioApoliceRN.IndAcesso = p_iIndAcesso
        If Not IsNothing(p_sCPF) Then v_oClsUsuarioApoliceRN.CPF = p_sCPF

        v_oDtUsuarios = v_oClsUsuarioApoliceRN.ListarPermissoesSubGrupoSituacao.Tables(0)

        Return v_oDtUsuarios
    End Function

    Public Function Select_DataTablePermissaoUsuariosApolice(ByVal ApoliceId As Integer, ByVal RamoId As Integer, ByVal Usuario As String) As DataTable

        Dim v_oClsUsuarioApoliceRN As New ClsUsuarioApoliceRN
        Dim v_oDtUsuarios As DataTable

        v_oClsUsuarioApoliceRN.ApoliceId = ApoliceId
        v_oClsUsuarioApoliceRN.RamoId = RamoId
        v_oClsUsuarioApoliceRN.Usuario = Usuario
        v_oClsUsuarioApoliceRN.IndAcesso = 2    'administrador de apolice

        v_oDtUsuarios = v_oClsUsuarioApoliceRN.ListarPermissoes().Tables(0)

        Return v_oDtUsuarios

    End Function

    Public Function Select_DataTablePermissaoUsuariosSubGrupo(ByVal ApoliceId As Integer, ByVal RamoId As Integer, ByVal Usuario As String) As DataTable
        Return Me.DtUsuarios(ApoliceId, RamoId, Usuario)
    End Function

    Public Function Select_DataTablePermissaoUsuariosOperador(ByVal ApoliceId As Integer, ByVal RamoId As Integer, ByVal Usuario As String) As DataTable
        Return Me.DtUsuarios(ApoliceId, RamoId, Usuario)
    End Function

    Public Function Select_DataTableSubGrupoApolice(ByVal ApoliceId As Integer, ByVal RamoId As Integer) As DataTable
        Return Me.DtSubGrupo(ApoliceId, RamoId)
    End Function
#End Region

#Region " Eventos Botao "
    Protected Sub btnSelecionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelecionar.Click
        Me.TabPanel4.Enabled = False
        Me.TabPanel3.Enabled = False
        Me.TabPanel2.Enabled = False
        Me.fvUsuario.ChangeMode(FormViewMode.Insert)
        Me.HelperSEGW.LimparForm(Me.fvUsuario.Controls)
        Me.MultiView1.SetActiveView(Me.View2)
        'Desabilita o evento de colar
        Dim v_oTxtEmail As TextBox = fvUsuario.FindControl("txtEmail")
        If Not IsNothing(v_oTxtEmail) Then
            v_oTxtEmail.Attributes.Add("onkeydown", "onKeyDown()")
        End If
        Dim v_oTxtEmail2 As TextBox = fvUsuario.FindControl("txtEmail2")
        If Not IsNothing(v_oTxtEmail2) Then
            v_oTxtEmail2.Attributes.Add("onkeydown", "onKeyDown()")
        End If

    End Sub

    Protected Sub btnIncluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        HabilitaDesabilitaTabs()

        Me.gvUsuarios.SelectedIndex = -1
        Me.MultiView1.SetActiveView(Me.View1)
    End Sub

    Protected Sub btnRevogar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim v_aSenha As ArrayList = Me.GerarSenha()
        Dim CPF As String = Me.gvUsuarios.SelectedDataKey.Item(0)

        Dim v_oClsUsuarioRN As New ClsUsuarioRN()
        v_oClsUsuarioRN.CPF = CPF
        Dim v_obterUsuario As Boolean = v_oClsUsuarioRN.Obter()

        Me.RN.DAO.IniciarTransacao(Me)
        Try
            Dim v_oClsUsuario As New ClsUsuarioRN(Me.RN.DAO)
            v_oClsUsuario.SenhaWeb = v_aSenha(1)
            v_oClsUsuario.CPF = CPF
            v_oClsUsuario.Usuario = MyBase.LoginWeb
            v_oClsUsuario.Revogar()

            If GLAMBIENTE_ID <= 3 Then
                'ATUALIZA CADASTROS
                Me.HelperSEGW.AtualizaCadastros(Me.RN.DAO, GLAMBIENTE_ID)
            End If

            If v_obterUsuario Then
                'Desbloquear usu�rio
                v_oClsUsuario.LoginWeb = v_oClsUsuarioRN.LoginRede
                v_oClsUsuario.Auditoria_Acesso_Login_SPU()

                'ENVIA EMAIL
                Dim v_sAssunto As String = "Confirma��o de Cadastro na Web"
                Dim v_sDe As String = "emissao.vida@aliancadobrasil.com.br"
                Dim v_sMensagem As String = Me.EmailMensagem(v_aSenha(0), _
                                                            CPF, _
                                                            v_oClsUsuarioRN.Nome, _
                                                            v_oClsUsuarioRN.LoginRede, _
                                                            Me.ApoliceId, _
                                                            Me.RamoId, _
                                                            Me.SubGrupoId)

                Dim v_iEmailId As Integer = SEGL0315.HelperSEGW.EnviarEmailBanco(v_sAssunto, _
                                                                                v_sDe, _
                                                                                v_oClsUsuarioRN.Email, _
                                                                                2, _
                                                                                v_sMensagem, _
                                                                                "SEGBR", _
                                                                                "SEGW0076", _
                                                                                MyBase.LoginWeb, _
                                                                                "ddmg0802")

                Me.IncluirLogEmailUsuario(Me.ApoliceId, Me.RamoId, Me.SubGrupoId, v_iEmailId, MyBase.LoginWeb, Me.RN.DAO)
            End If

            HabilitaDesabilitaTabs()
            Me.gvUsuarios.SelectedIndex = -1
            Me.MultiView1.SetActiveView(Me.View1)
            ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel1, Me.UpdatePanel1.GetType(), "JavaScript", "alert('Atualiza��o feita com sucesso.');", True)

            Me.RN.DAO.FinalizarTransacao(True, Me)
        Catch ex As Exception
            Me.RN.DAO.FinalizarTransacao(False, Me)
            Throw ex
        End Try
    End Sub


    Public Function VerificaSenhaCadastrada(ByVal senha As String, ByVal CPF As String) As Boolean
        Dim SQL As String
        SQL = " SELECT count(*) " & _
              "   FROM usuario_tb " & _
              "  WHERE CPF = '" & CPF & "' " & _
              "  AND SENHA_WEB = '" & senha & "' "

        'Me.RN.DAO.ExecQuery_DataReader()
        Dim v_oDtUsuario As DataTable
        v_oDtUsuario = Me.RN.DAO.ExecQuery_DataTable(SQL)
        If v_oDtUsuario.Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnAdmApolice_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdmApolice.Click
        Try
            Me.RN.DAO.IniciarTransacao(Me)

            'REDIMENSIONA A CLASSE PARA O TOTAL DE INSERT
            ReDim Me.RN.ClsUsuarioApoliceRN(Me.gvAdmApolice.Rows.Count - 1)
            For Each v_oRow As GridViewRow In Me.gvAdmApolice.Rows
                Dim v_sSituacao As String = "I"
                Dim v_oCheckBox As CheckBox = v_oRow.FindControl("ckbAtivo")
                Dim v_sCPF As String = Me.gvAdmApolice.DataKeys(v_oRow.RowIndex).Item("CPF")

                If v_oCheckBox.Checked Then
                    v_sSituacao = "A"

                    'VERIFICA SE O USUARIO MARCADO TEM ACESSO A OUTRO PERFIL
                    If ValidaOutroAcesso(Me.gvAdmApolice, v_oRow, 2) Then
                        ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel2, Me.UpdatePanel2.GetType(), "JavaScript", "alert('Usu�rio ativo em outro perfil.');", True)
                        Me.RN.ClsUsuarioApoliceRN() = Nothing
                        v_oCheckBox.Checked = False
                        Exit Sub
                    End If
                End If

                Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex) = New ClsUsuarioApoliceRN(Me.RN.DAO)
                Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).ApoliceId = Me.ApoliceId
                Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).RamoId = Me.RamoId
                Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).CPF = v_sCPF
                Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).IndSituacao = v_sSituacao
                Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).SeguradoraCodSusep = Me.DtApolice(Me.ApoliceId, Me.RamoId, Me.SubGrupoId).Rows(0)("seguradora_cod_susep")
                Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).SucursalSeguradoraId = Me.DtApolice(Me.ApoliceId, Me.RamoId, Me.SubGrupoId).Rows(0)("sucursal_seguradora_id")

                If Me.gvAdmApolice.DataKeys(v_oRow.RowIndex).Item("email_enviado") = "N" Then
                    Me.DtUsuarios(Me.ApoliceId, Me.RamoId, Me.SubGrupoId).Rows(v_oRow.RowIndex).Item("email_enviado") = "S"
                    Me.DtUsuarios(Me.ApoliceId, Me.RamoId, Me.SubGrupoId).AcceptChanges()

                    Dim v_aSenha As ArrayList = Me.GerarSenha()
                    Dim v_sNome As String = ""
                    Dim v_sLoginWeb As String = ""
                    Dim v_sEmail As String = ""

                    Dim v_oClsUsuarioRN As New ClsUsuarioRN()
                    v_oClsUsuarioRN.CPF = v_sCPF
                    If v_oClsUsuarioRN.Obter() Then
                        v_sNome = v_oClsUsuarioRN.Nome
                        v_sLoginWeb = v_oClsUsuarioRN.LoginRede
                        v_sEmail = v_oClsUsuarioRN.Email
                    End If

                    'ATUALIZA EMAIL ENVIADO
                    Me.RN.CPF = v_sCPF
                    Me.RN.EmailEnviado = "S"
                    Me.RN.Usuario = MyBase.LoginWeb

                    'CADASTRA/ATUALIZA USUARIO_TB
                    ReDim Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).ClsUsuarioRN(0)
                    Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).ClsUsuarioRN(0) = New ClsUsuarioRN(Me.RN.DAO)
                    Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).ClsUsuarioRN(0).Nome = v_sNome
                    Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).ClsUsuarioRN(0).CPF = v_sCPF
                    Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).ClsUsuarioRN(0).LoginWeb = v_sLoginWeb
                    Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).ClsUsuarioRN(0).Email = v_sEmail
                    Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).ClsUsuarioRN(0).SenhaWeb = v_aSenha(1)

                    Dim v_bExisteSenha As Boolean
                    If VerificaSenhaCadastrada(v_aSenha(0), v_sCPF) = True Then
                        v_bExisteSenha = True
                    Else
                        v_bExisteSenha = False
                    End If

                    'REDIMENSIONA A CLASSE PARA ENVIO DE EMAIL
                    ReDim Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).ClsEmailRN(0)
                    Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).ClsEmailRN(0) = New clsEmailRN(Me.RN.DAO)
                    Dim v_sMensagem As String = Me.EmailMensagem(v_aSenha(0), _
                                                               v_sCPF, _
                                                               v_sNome, _
                                                               v_sLoginWeb, _
                                                               Me.ApoliceId, _
                                                               Me.RamoId, _
                                                               Me.SubGrupoId, _
                                                               v_bExisteSenha, _
                                                               2)
                    Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).ClsEmailRN(0).Assunto = "Confirma��o de Cadastro na Web"
                    Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).ClsEmailRN(0).De = "emissao.vida@aliancadobrasil.com.br"
                    Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).ClsEmailRN(0).Para = v_sEmail
                    Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).ClsEmailRN(0).Mensagem = v_sMensagem
                    Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).ClsEmailRN(0).SiglaSistema = "SEGBR"
                    Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).ClsEmailRN(0).SiglaRecurso = "SEGW0076"
                    Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).ClsEmailRN(0).Chave = "ddmg0802"
                    Me.RN.ClsUsuarioApoliceRN(v_oRow.RowIndex).ClsEmailRN(0).Formato = 2
                End If
            Next

            Me.RN.Usuario = MyBase.LoginWeb
            'INCLUI PERMISSOES
            Me.RN.Incluir_PermissoesAdmApolice()
            'FINALIZA A CLASSE
            Me.RN.ClsUsuarioApoliceRN() = Nothing

            Me.RN.DAO.FinalizarTransacao(True, Me)

            'ATUALIZA DATABASE
            Me.DtUsuarios(Me.ApoliceId, Me.RamoId, MyBase.LoginWeb) = Nothing
            Me.gvAdmSubGrupo.DataBind()
            Me.gvAdmOperador.DataBind()
            ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel2, Me.UpdatePanel2.GetType(), "JavaScript", "alert('Atualiza��o feita com sucesso.');", True)
        Catch ex As Exception
            Me.RN.DAO.FinalizarTransacao(False, Me)
            ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel2, Me.UpdatePanel2.GetType(), "JavaScript", "alert('Ocorreu um erro.\nErro: " & ex.InnerException.Message.Replace("'", "").Replace(Chr(13), "\n").Replace(Chr(10), "") & "');", True)
        End Try

    End Sub

    Protected Sub btnAdmSubGrupo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdmSubGrupo.Click
        'CALCULA TOTAL DE OPERACOES DE INSERT
        Dim v_iTotOperacoes As Integer = Me.gvAdmSubGrupo.Rows.Count * Me.DtSubGrupo(Me.ApoliceId, Me.RamoId).Rows.Count

        Try
            Me.RN.DAO.IniciarTransacao(Me)

            'REDIMENSIONA A CLASSE PARA O TOTAL DE INSERT
            ReDim Me.RN.ClsUsuarioApoliceRN(v_iTotOperacoes - 1)
            'CRIA OS INSERTS NA CLASSE
            InsertSituacaoAllCheckBox(Me.gvAdmSubGrupo.Controls, 3, 0)
            'SE NOTHING EH PQ EXISTE USUARIO ATIVO EM OUTRO PERFIL
            If IsNothing(Me.RN.ClsUsuarioApoliceRN()) Then
                ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel3, Me.UpdatePanel3.GetType(), "JavaScript", "alert('Usu�rio ativo em outro perfil.');", True)
                Exit Sub
            End If

            Me.RN.Usuario = MyBase.LoginWeb
            'INCLUI PERMISSOES
            Me.RN.Incluir_Permissoes()

            'FINALIZA A CLASSE
            Me.RN.ClsUsuarioApoliceRN() = Nothing

            ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel3, Me.UpdatePanel3.GetType(), "JavaScript", "alert('Atualiza��o feita com sucesso.');", True)

            Me.RN.DAO.FinalizarTransacao(True, Me)
        Catch ex As Exception
            Me.RN.DAO.FinalizarTransacao(False, Me)
            ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel2, Me.UpdatePanel2.GetType(), "JavaScript", "alert('Ocorreu um erro.\nErro: " & ex.InnerException.Message.Replace("'", "").Replace(Chr(13), "\n").Replace(Chr(10), "") & "');", True)
        End Try
    End Sub

    Protected Sub btnAdmOperadores_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdmOperadores.Click
        'CALCULA TOTAL DE OPERACOES DE INSERT
        Dim v_iTotOperacoes As Integer = Me.gvAdmOperador.Rows.Count * Me.DtSubGrupo(Me.ApoliceId, Me.RamoId).Rows.Count
        'REDIMENSIONA A CLASSE PARA O TOTAL DE INSERT
        ReDim Me.RN.ClsUsuarioApoliceRN(v_iTotOperacoes - 1)
        'CRIA OS INSERTS NA CLASSE
        InsertSituacaoAllCheckBox(Me.gvAdmOperador.Controls, 4, 0)
        'SE NOTHING EH PQ EXISTE USUARIO ATIVO EM OUTRO PERFIL
        If IsNothing(Me.RN.ClsUsuarioApoliceRN()) Then
            ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel4, Me.UpdatePanel4.GetType(), "JavaScript", "alert('Usu�rio ativo em outro perfil.');", True)
            Exit Sub
        End If
        Me.RN.Usuario = MyBase.LoginWeb
        'INCLUI PERMISSOES
        Me.RN.Incluir_Permissoes()
        'FINALIZA A CLASSE
        Me.RN.ClsUsuarioApoliceRN() = Nothing
        ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel4, Me.UpdatePanel4.GetType(), "JavaScript", "alert('Atualiza��o feita com sucesso.');", True)
    End Sub

    Protected Sub btnAdmMaster_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdmMaster.Click
        'CALCULA TOTAL DE OPERACOES DE INSERT
        Dim v_iTotOperacoes As Integer = Me.gvAdmOperador.Rows.Count * Me.DtSubGrupo(Me.ApoliceId, Me.RamoId).Rows.Count
        'REDIMENSIONA A CLASSE PARA O TOTAL DE INSERT
        ReDim Me.RN.ClsUsuarioApoliceRN(v_iTotOperacoes - 1)
        'CRIA OS INSERTS NA CLASSE
        InsertSituacaoAllCheckBox(Me.gvAdmMaster.Controls, 7, 0)
        'SE NOTHING EH PQ EXISTE USUARIO ATIVO EM OUTRO PERFIL
        If IsNothing(Me.RN.ClsUsuarioApoliceRN()) Then
            ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel4, Me.UpdatePanel4.GetType(), "JavaScript", "alert('Usu�rio ativo em outro perfil.');", True)
            Exit Sub
        End If
        Me.RN.Usuario = MyBase.LoginWeb
        'INCLUI PERMISSOES
        Me.RN.Incluir_Permissoes()
        'FINALIZA A CLASSE
        Me.RN.ClsUsuarioApoliceRN() = Nothing
        ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel4, Me.UpdatePanel4.GetType(), "JavaScript", "alert('Atualiza��o feita com sucesso.');", True)
        VerificaAcessoMaster()
    End Sub
#End Region

#Region " Eventos CheckBox "
    Protected Sub ckbAtivo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim v_oCkbAdmApolice As CheckBox = DirectCast(sender, CheckBox)
        If v_oCkbAdmApolice.Checked Then
            Dim v_oRow As GridViewRow = DirectCast(sender.Parent.BindingContainer, GridViewRow)
            Dim v_oClsUsuarioApoliceRN As New ClsUsuarioApoliceRN
            Dim v_oDtPermissoes As DataTable

            v_oClsUsuarioApoliceRN.ApoliceId = Me.ApoliceId
            v_oClsUsuarioApoliceRN.RamoId = Me.RamoId
            v_oClsUsuarioApoliceRN.CPF = Me.gvAdmApolice.DataKeys(v_oRow.RowIndex)("CPF")
            v_oClsUsuarioApoliceRN.IndSituacao = "A"

            v_oDtPermissoes = v_oClsUsuarioApoliceRN.ListarPermissoesSituacao.Tables(0)

            Dim v_oDtViewPermissoes As New DataView(v_oDtPermissoes)
            v_oDtViewPermissoes.RowFilter = "ind_acesso IN (3,4)"

            If v_oDtViewPermissoes.Count > 0 Then
                DirectCast(sender, CheckBox).Checked = False
                ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel2, Me.UpdatePanel2.GetType(), "JavaScript", "alert('Usu�rio ativo em outro perfil.');", True)
            Else
                ChangeAllCheckBoxStates(Me.gvAdmApolice.Controls, v_oCkbAdmApolice.InputAttributes.Item("Grupo"), v_oCkbAdmApolice.ClientID)
            End If
        End If
    End Sub


    'Protected Sub CkbAdmSubGrupo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim v_oCkbAdmSubGrupo As CheckBox = DirectCast(sender, CheckBox)
    '    If v_oCkbAdmSubGrupo.Checked Then
    '        Dim v_oDlSubGrupo As DataList = sender.Parent.BindingContainer
    '        Dim v_oDlRow As DataListItem = sender.BindingContainer
    '        Dim v_oGvRow As GridViewRow = sender.Parent.Parent.BindingContainer
    '        Dim v_oClsUsuarioApoliceRN As New ClsUsuarioApoliceRN
    '        Dim v_oDtPermissoes As DataTable

    '        v_oClsUsuarioApoliceRN.ApoliceId = Me.ApoliceId
    '        v_oClsUsuarioApoliceRN.RamoId = Me.RamoId
    '        v_oClsUsuarioApoliceRN.IndAcesso = 4
    '        v_oClsUsuarioApoliceRN.IndSituacao = "A"
    '        v_oClsUsuarioApoliceRN.CPF = Me.gvAdmSubGrupo.DataKeys(v_oGvRow.RowIndex)("CPF")
    '        v_oClsUsuarioApoliceRN.SubGrupoId = v_oDlSubGrupo.DataKeys(v_oDlRow.ItemIndex)

    '        v_oDtPermissoes = v_oClsUsuarioApoliceRN.VerifivaPermissaSubGrupoAcesso.Tables(0)

    '        If v_oDtPermissoes.Rows.Count > 0 Then
    '            DirectCast(sender, CheckBox).Checked = False
    '            ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel3, Me.UpdatePanel3.GetType(), "JavaScript", "alert('Usu�rio ativo em outro perfil.');", True)
    '        Else
    '            ChangeAllCheckBoxStates(Me.gvAdmSubGrupo.Controls, v_oCkbAdmSubGrupo.InputAttributes.Item("Grupo"), v_oCkbAdmSubGrupo.ClientID)
    '        End If
    '        'ChangeAllCheckBoxStates(Me.gvAdmSubGrupo.Controls, v_oCkbAdmSubGrupo.InputAttributes.Item("Grupo"), v_oCkbAdmSubGrupo.ClientID)
    '    End If
    'End Sub



    

    'Private Sub CriarCkbAdmSubGrupo()
    '    Dim v_oSubGrupos As DataTable = Me.HelperSEGW.RetornaDataTable_SubGrupoApolice(Me.ApoliceId, Me.RamoId)
    '    If v_oSubGrupos.Rows.Count > 0 Then
    '        Dim contador As Integer = 1
    '        For Each v_oRow As DataRow In v_oSubGrupos.Rows
    '            Dim template As New TemplateField
    '            Dim v_ockbTemplate As New ckbTemplate
    '            v_ockbTemplate.Grupo = "AdmSubGrupoAtivo"
    '            v_ockbTemplate.Id = "CkbAdmSubGrupo"

    '            AddHandler v_ockbTemplate.AlterarChecked, AddressOf CkbAdmSubGrupo_CheckedChanged

    '            template.ItemTemplate = v_ockbTemplate
    '            template.HeaderText = "SubGrupo_" & contador
    '            template.ItemStyle.HorizontalAlign = HorizontalAlign.Center
    '            Me.gvAdmSubGrupo.Columns.Add(template)

    '            contador += 1
    '        Next
    '    End If
    'End Sub
#End Region

#Region " Metodos Auxiliares "

    Public Function GerarSenha() As ArrayList
        Dim senha As String = Now.Hour.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0") & Now.Millisecond.ToString.PadLeft(2, "0")
        senha = senha.Substring(0, 8)

        'Dim md5 As Object = Server.CreateObject("MD5Hash.MD5")
        'Dim senhahash As String = md5.Hash(senha)

        Dim senhahash As String = GenerateHash(senha)
        Dim v_oArray As New ArrayList

        v_oArray.Add(New String(senha))
        v_oArray.Add(New String(senhahash))

        Return v_oArray

    End Function

    Private Function EmailMensagem(ByVal p_sSenha As String, _
                                    ByVal p_sCPF As String, _
                                    ByVal p_sNome As String, _
                                    ByVal p_sLogin As String, _
                                    ByVal p_iApoliceId As Integer, _
                                    ByVal p_iRamoId As Integer, _
                                    ByVal p_iSubGrupoId As Integer, _
                                    Optional ByVal p_bExisteSenha As Boolean = True, _
                                    Optional ByVal p_iIndAceso As Integer = 0 _
                                    ) As String

        Dim mensagem As String
        Dim v_sEstipulante As String = Me.DtApolice(p_iApoliceId, p_iRamoId, p_iSubGrupoId).Rows(0)("nome")
        Dim v_sSubGrupo As String = Me.DtApolice(p_iApoliceId, p_iRamoId, p_iSubGrupoId).Rows(0)("sub_grupo")

        Dim mes(12) As String
        mes(1) = "janeiro"
        mes(2) = "fevereiro"
        mes(3) = "mar�o"
        mes(4) = "abril"
        mes(5) = "maio"
        mes(6) = "junho"
        mes(7) = "julho"
        mes(8) = "agosto"
        mes(9) = "setembro"
        mes(10) = "outubro"
        mes(11) = "novembro"
        mes(12) = "dezembro"


        Dim ClsUsuarioApolice As New ClsUsuarioApoliceRN()
        ClsUsuarioApolice.ApoliceId = p_iApoliceId
        ClsUsuarioApolice.RamoId = p_iRamoId
        ClsUsuarioApolice.CPF = p_sCPF
        ClsUsuarioApolice.IndSituacao = "A"
        Dim v_oDtAcesso As DataTable = ClsUsuarioApolice.ListarPermissoesSituacao().Tables(0)
        Dim ind_acesso As Integer = 0
        If v_oDtAcesso.Rows.Count > 0 Then
            ind_acesso = v_oDtAcesso.Rows(0)("ind_acesso")
        Else
            ind_acesso = p_iIndAceso
        End If
        Dim TpAcesso As String = ""

        Select Case CInt(ind_acesso)
            Case 1
                TpAcesso = "Administrador da Alian�a do Brasil"
            Case 2
                TpAcesso = "Administrador de Ap�lice"
            Case 3
                TpAcesso = "Administrador de Subgrupo"
            Case 4
                TpAcesso = "Operador"
            Case 5
                TpAcesso = "Consulta"
            Case 6
                TpAcesso = "Central de Atendimento"
            Case 7 ' Demanda 4370246
                TpAcesso = "Administrador Master"
            Case Else
                TpAcesso = "Acesso n�o Configurado"
        End Select

        mensagem = "<html><body>"
        mensagem &= "<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""font-face:Arial;font-size:16px;"">"

        mensagem &= "<tr>"
        mensagem &= "   <td align=""right""><IMG src=""http://qld.aliancadobrasil.com.br/SEG/segw0071/files_print/bnb_alianca_apolice.gif"" border=""0""><br><br></td>"
        mensagem &= "</tr>"
        mensagem &= "<tr>"
        mensagem &= "   <td align=""right"">S�o Paulo, " & Date.Now.Day.ToString.PadLeft(2, "0") & " de " & mes(Date.Now.Month) & " de " & Date.Now.Year & "<br><br></td>"
        mensagem &= "</tr>"
        mensagem &= "<tr>"
        mensagem &= "   <td>Ramo/Ap�lice/Nome da Empresa: " & p_iRamoId & " / " & p_iApoliceId & " / " & v_sEstipulante & "<br><br></td>"
        mensagem &= "</tr>"

        If CInt(Session("Acesso")) <> 2 Then
            mensagem &= "<tr>"
            mensagem &= "   <td>SubGrupo:" & v_sSubGrupo & "<br><br></td>"
            mensagem &= "</tr>"
        End If

        mensagem &= "<tr>"
        mensagem &= "   <td>" & getPeriodoCompetencia(p_iApoliceId, p_iRamoId, p_iSubGrupoId) & "</td>"
        mensagem &= "</tr>"
        mensagem &= "<tr>"
        mensagem &= "   <td>Data Limite para Encerramento da Movimenta��o de Vidas: " & getPreenchePrazoRestante(p_iApoliceId, p_iRamoId, p_iSubGrupoId) & "<br><br></td>"
        mensagem &= "</tr>"
        mensagem &= "<tr>"
        mensagem &= "   <td>Sr(a)." & p_sNome.Replace("'", "''") & " - " & TpAcesso & "<br><br></td>"
        mensagem &= "</tr>"
        mensagem &= "<tr>"
        mensagem &= "   <td>� com satisfa��o que lhe damos as boas-vindas ao Sistema de Movimenta��o de Vidas da Companhia de Seguros Alian�a do Brasil.<br><br></td>"
        mensagem &= "</tr>"
        mensagem &= "<tr>"
        mensagem &= "   <td>A partir desse Sistema, voc� poder� atualizar a movimenta��o de vidas da sua empresa, o que permitir� a gera��o da fatura para o pagamento do seguro que mant�m conosco.<br><br></td>"
        mensagem &= "</tr>"


        If p_sSenha.Trim.Length > 0 Then

            If p_bExisteSenha = False Then
                mensagem &= "<tr>"
                mensagem &= "   <td>Voc� poder� acessar o sistema com o seu Login e Senha j� cadastrados.<br><br></td>"
                mensagem &= "</tr>"

            Else

                mensagem &= "<tr>"
                mensagem &= "   <td>Voc� est� recebendo uma senha provis�ria que dever� ser alterada no momento do primeiro login no Sistema.<br><br></td>"
                mensagem &= "</tr>"

                mensagem &= "<tr>"
                mensagem &= "   <td> Login: " & p_sLogin & "<br><br></td>"
                mensagem &= "</tr>"

                mensagem &= "<tr>"
                mensagem &= "   <td> Senha: " & p_sSenha & "<br><br></td>"
                mensagem &= "</tr>"

            End If



        End If

        mensagem &= "<tr>"
        mensagem &= "   <td>Para fazer a altera��o da senha, acesse o site www.aliancadobrasil.com.br, no menu extranet e digite o login e a senha.<br><br></td>"
        mensagem &= "</tr>"
        mensagem &= "<tr>"
        'mensagem &= "   <td>Em caso de d�vida, ligue para nossa <b>Central de Atendimento aos Clientes � 0800 729 7000.</b><br><br></td>"
        mensagem &= "<td><b>Em caso de d�vida, ligue para nossa Central de Atendimento aos Clientes - 0800 729 7000 (op��o 5, em seguida op��o 1) ou Central de Atendimento aos Deficientes Auditivos ou de Fala 0800 729 0088.</b><br><br></td>"
        mensagem &= "</tr>"
        mensagem &= "<tr>"
        mensagem &= "   <td>Atenciosamente,<br><br></td>"
        mensagem &= "</tr>"
        mensagem &= "<tr>"
        mensagem &= "   <td><b>Companhia de Seguros Alian�a do Brasil</b><br><br></td>"
        mensagem &= "</tr>"
        mensagem &= "</table>"
        mensagem &= "</body></html>"

        Return mensagem

    End Function

    Private Function getPeriodoCompetencia(ByVal p_iApoliceId As Integer, ByVal p_iRamoId As Integer, ByVal p_iSubgrupoId As Integer) As String

        Dim v_oDtDados As DataTable = Me.HelperSEGW.RecuperarPeriodoCompetencia(p_iApoliceId, p_iRamoId, p_iSubgrupoId)
        Dim data_inicio As String = ""
        Dim data_fim As String = ""
        Dim retorno As String = ""

        If v_oDtDados.Rows.Count > 0 Then
            data_inicio = v_oDtDados.Rows(0)(1)
            data_fim = v_oDtDados.Rows(0)(2)

            retorno = "Per�odo de Compet�ncia: " & data_inicio & " at� " & data_fim
        Else
            System.Web.HttpContext.Current.Session("abandonar") = 1
            'System.Web.HttpContext.Current.Response.Write("<script>parent.window.location=parent.window.location;</script>")
            System.Web.HttpContext.Current.Response.Redirect("http://www.aliancadobrasil.com.br/funcao/erro_secao.asp")
            System.Web.HttpContext.Current.Response.End()
            retorno = ""
        End If

        Return retorno
    End Function

    Public Function getPreenchePrazoRestante(ByVal p_iApoliceId As Integer, ByVal p_iRamoId As Integer, ByVal p_iSubgrupoId As Integer) As String

        Dim v_oDtDadosWF As DataTable = Me.HelperSEGW.RecuperarPeriodoCompetencia(p_iApoliceId, p_iRamoId, p_iSubgrupoId)
        Dim wf_id As Integer = v_oDtDadosWF.Rows(0)(0)
        Dim v_oDtDadosPrazo As DataTable = Me.HelperSEGW.RecuperarPrazoLimite(p_iApoliceId, p_iRamoId, p_iSubgrupoId, wf_id)

        Try
            If v_oDtDadosPrazo.Rows.Count > 0 Then
                Try
                    Return Convert.ToDateTime(v_oDtDadosPrazo.Rows(0)("dt_fim_faturamento")).Day & "/" & Convert.ToDateTime(v_oDtDadosPrazo.Rows(0)("dt_fim_faturamento")).Month & "/" & Convert.ToDateTime(v_oDtDadosPrazo.Rows(0)("dt_fim_faturamento")).Year
                Catch ex As Exception
                    Return v_oDtDadosPrazo.Rows(0)("dt_fim_faturamento").ToString().Substring(0, 9)
                End Try
            Else
                Return "Prazo n�o encontrado"
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return ""
    End Function

    Private Function GenerateHash(ByVal SourceText As String) As String
        Dim Md5 As Object = Server.CreateObject("MD5Hash.MD5")
        Dim senhahash As String = Md5.Hash(SourceText & "")

        Return senhahash
    End Function

    Private Sub VerificaAcessoMaster()
        Dim v_oClsUsuarioApolice As New ClsUsuarioApoliceRN
        v_oClsUsuarioApolice.ApoliceId = Me.ApoliceId
        v_oClsUsuarioApolice.RamoId = Me.RamoId
        v_oClsUsuarioApolice.IndAcesso = 7
        v_oClsUsuarioApolice.IndSituacao = "A"
        Dim v_oDtPermissoes As DataTable = v_oClsUsuarioApolice.ListarPermissoesSituacao().Tables(0)
        Dim v_oDvPermissoes As New DataView(v_oDtPermissoes)
        Dim v_oDtDistinctAtivo As DataTable = v_oDvPermissoes.ToTable(True, "subgrupo_id")
        Dim v_sSubGrupoAtivo As String = ""
        For Each v_oRow As DataRow In v_oDtDistinctAtivo.Rows
            HabDesabCheckBoxGrupo(Me.gvAdmOperador.Controls, v_oRow("subgrupo_id"), False)
            HabDesabCheckBoxGrupo(Me.gvAdmSubGrupo.Controls, v_oRow("subgrupo_id"), False)
            v_sSubGrupoAtivo &= v_oRow("subgrupo_id") & ","
        Next
        If v_oDtDistinctAtivo.Rows.Count > 0 Then
            v_sSubGrupoAtivo = v_sSubGrupoAtivo.Substring(0, v_sSubGrupoAtivo.Length - 1)
        End If


        v_oClsUsuarioApolice.IndSituacao = "I"
        v_oDtPermissoes = v_oClsUsuarioApolice.ListarPermissoesSituacao().Tables(0)
        v_oDvPermissoes = New DataView(v_oDtPermissoes)
        If v_oDtDistinctAtivo.Rows.Count > 0 Then
            v_oDvPermissoes.RowFilter = "subgrupo_id NOT IN (" & v_sSubGrupoAtivo & ")"
        End If
        Dim v_oDtDistinctInativo As DataTable = v_oDvPermissoes.ToTable(True, "subgrupo_id")
        For Each v_oRow As DataRow In v_oDtDistinctInativo.Rows
            HabDesabCheckBoxGrupo(Me.gvAdmOperador.Controls, v_oRow("subgrupo_id"), True)
            HabDesabCheckBoxGrupo(Me.gvAdmSubGrupo.Controls, v_oRow("subgrupo_id"), True)
        Next
    End Sub

    Private Sub HabDesabCheckBoxGrupo(ByVal pControles As ControlCollection, ByVal p_iSubGrupoId As Integer, ByVal p_bDesabilita As Boolean)
        ' Declara��o da vari�veis
        Dim vlControle As System.Web.UI.Control

        ' Percorre todos os controles da cole��o de Administrador de subgrupo
        For Each vlControle In pControles
            ' Teste para controle Checkbox
            If vlControle.GetType Is GetType(System.Web.UI.WebControls.CheckBox) Then
                Dim controle As CheckBox = DirectCast(vlControle, CheckBox)
                If controle.InputAttributes.Item("SubGrupoId") = p_iSubGrupoId Then
                    controle.Enabled = p_bDesabilita
                End If
            End If

            ' Chamada recursiva da fun��o
            HabDesabCheckBoxGrupo(vlControle.Controls, p_iSubGrupoId, p_bDesabilita)
        Next
    End Sub

    Private Sub ChangeAllCheckBoxStates(ByVal pControles As ControlCollection, ByVal p_sGrupo As String, ByVal p_sSelecionado As String)
        ' Declara��o da vari�veis
        Dim vlControle As System.Web.UI.Control

        ' Percorre todos os controles da cole��o de controles informada
        For Each vlControle In pControles
            ' Teste para controle Checkbox
            If vlControle.GetType Is GetType(System.Web.UI.WebControls.CheckBox) Then
                Dim controle As CheckBox = DirectCast(vlControle, CheckBox)
                If controle.InputAttributes.Item("Grupo") = p_sGrupo Then
                    If controle.ClientID <> p_sSelecionado Then
                        controle.Checked = False
                    Else
                        controle.Checked = True
                    End If
                End If
            End If

            ' Chamada recursiva da fun��o
            ChangeAllCheckBoxStates(vlControle.Controls, p_sGrupo, p_sSelecionado)
        Next
    End Sub

    Private Sub HabDesabCheckBoxGrupoTodos(ByVal pControles As ControlCollection, ByVal p_bDesabilita As Boolean)


        ' Declara��o da vari�veis
        Dim vlControle As System.Web.UI.Control

        ' Percorre todos os controles da cole��o de Administrador de subgrupo
        For Each vlControle In pControles
            ' Teste para controle Checkbox
            If vlControle.GetType Is GetType(System.Web.UI.WebControls.CheckBox) Then
                Dim controle As CheckBox = DirectCast(vlControle, CheckBox)

                If controle.ID = "chkTodos" Or controle.ID = "chkTodosOperador" Or controle.ID = "chkTodosMaster" Then
                    controle.Checked = p_bDesabilita
                End If

            End If

            ' Chamada recursiva da fun��o
            HabDesabCheckBoxGrupoTodos(vlControle.Controls, p_bDesabilita)
        Next

    End Sub

    'Private Sub ChangeAllCheckBoxTodos(ByVal pControles As ControlCollection, ByVal p_sGrupo As String)
    '    ' Declara��o da vari�veis
    '    Dim vlControle As System.Web.UI.Control

    '    ' Percorre todos os controles da cole��o de controles informada
    '    For Each vlControle In pControles
    '        ' Teste para controle Checkbox
    '        If vlControle.GetType Is GetType(System.Web.UI.WebControls.CheckBox) Then
    '            Dim controle As CheckBox = DirectCast(vlControle, CheckBox)
    '            Dim v_sGrupo As String = controle.InputAttributes.Item("Grupo")

    '            If v_sGrupo <> "" Then

    '                Dim v_iPosicao As Integer = v_sGrupo.IndexOf("_")

    '                v_sGrupo = Mid(v_sGrupo, 1, v_iPosicao) & "Todos"

    '                If v_sGrupo = p_sGrupo Then
    '                    controle.Checked = False
    '                End If

    '            End If


    '        End If

    '        ' Chamada recursiva da fun��o
    '        ChangeAllCheckBoxTodos(vlControle.Controls, p_sGrupo)
    '    Next
    'End Sub


    Private Sub ChangeAllCheckBoxStates(ByVal pControles As ControlCollection, ByVal check As Boolean, ByVal pChkTodos As String)
        ' Declara��o da vari�veis
        Dim vlControle As System.Web.UI.Control

        ' Percorre todos os controles da cole��o de controles informada
        For Each vlControle In pControles
            ' Teste para controle Checkbox
            If vlControle.GetType Is GetType(System.Web.UI.WebControls.CheckBox) Then
                Dim controle As CheckBox = DirectCast(vlControle, CheckBox)
                Dim v_oDlSubGrupo As DataList = controle.Parent.BindingContainer
                Dim v_oDlRow As DataListItem = controle.BindingContainer
                Dim v_oGvRow As GridViewRow = controle.Parent.Parent.BindingContainer
                Dim v_oGv As GridView = v_oGvRow.BindingContainer


                If controle.Enabled Then

                    If check Then


                        Dim v_oClsUsuarioApoliceRN As New ClsUsuarioApoliceRN
                        Dim v_oDtPermissoes As DataTable
                        Dim p_sGrupo As String = controle.InputAttributes.Item("Grupo").ToString
                        Dim p_sSelecionado As String = controle.ClientID.ToString


                        If v_oGv.ID <> "gvAdmMaster" Then

                            '-------Verifica Permissoes para Administrador de Subgrupo e Operador
                            v_oClsUsuarioApoliceRN.ApoliceId = Me.ApoliceId
                            v_oClsUsuarioApoliceRN.RamoId = Me.RamoId
                            Select Case v_oGv.ID
                                Case "gvAdmSubGrupo"
                                    v_oClsUsuarioApoliceRN.IndAcesso = 4
                                Case "gvAdmOperador"
                                    v_oClsUsuarioApoliceRN.IndAcesso = 3
                            End Select
                            v_oClsUsuarioApoliceRN.IndSituacao = "A"
                            v_oClsUsuarioApoliceRN.CPF = v_oGv.DataKeys(v_oGvRow.RowIndex)("CPF")
                            v_oClsUsuarioApoliceRN.SubGrupoId = v_oDlSubGrupo.DataKeys(v_oDlRow.ItemIndex)

                            v_oDtPermissoes = v_oClsUsuarioApoliceRN.VerifivaPermissaSubGrupoAcesso.Tables(0)

                            If v_oDtPermissoes.Rows.Count > 0 Then
                                DirectCast(v_oGvRow.FindControl(pChkTodos), CheckBox).Checked = False
                                DirectCast(controle, CheckBox).Checked = False
                                ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel3, Me.UpdatePanel3.GetType(), "JavaScript", "alert('Usu�rio ativo em outro perfil no subgrupo " & v_oClsUsuarioApoliceRN.SubGrupoId.ToString & ".');", True)

                                Select Case v_oGv.ID
                                    Case "gvAdmSubGrupo"
                                        ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel3, Me.UpdatePanel3.GetType(), "JavaScript", "alert('Usu�rio ativo em outro perfil no subgrupo " & v_oClsUsuarioApoliceRN.SubGrupoId.ToString & ".');", True)
                                    Case "gvAdmOperador"
                                        ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel4, Me.UpdatePanel4.GetType(), "JavaScript", "alert('Usu�rio ativo em outro perfil no subgrupo " & v_oClsUsuarioApoliceRN.SubGrupoId.ToString & ".');", True)
                                End Select

                            Else
                                ChangeAllCheckBoxStates(v_oGv.Controls, p_sGrupo, p_sSelecionado)
                                'controle.Checked = True
                            End If

                        Else
                            ChangeAllCheckBoxStates(v_oGv.Controls, p_sGrupo, p_sSelecionado)
                        End If





                    Else
                        controle.Checked = False
                    End If

                End If
                '--douglas
                'DirectCast(v_oGvRow.FindControl(pChkTodos), CheckBox).Checked = False
            End If

            ' Chamada recursiva da fun��o
            ChangeAllCheckBoxStates(vlControle.Controls, check, pChkTodos)

        Next
    End Sub


    Private Sub InsertSituacaoAllCheckBox(ByVal pControles As ControlCollection, ByVal p_iIndAceso As Integer, ByRef p_iContador As Integer)
        ' Declara��o da vari�veis
        Dim vlControle As System.Web.UI.Control

        If IsNothing(Me.RN.ClsUsuarioApoliceRN()) Then Exit Sub

        ' Percorre todos os controles da cole��o de controles informada
        For Each vlControle In pControles
            ' Teste para controle Checkbox
            If vlControle.GetType Is GetType(System.Web.UI.WebControls.CheckBox) Then
                If vlControle.ID <> "chkTodos" And vlControle.ID <> "chkTodosOperador" And vlControle.ID <> "chkTodosMaster" Then
                    Dim controle As CheckBox = DirectCast(vlControle, CheckBox)
                    Dim v_oGvRow As GridViewRow = controle.BindingContainer.BindingContainer.BindingContainer
                    Dim v_oGridview As GridView = controle.BindingContainer.BindingContainer.BindingContainer.BindingContainer
                    Dim v_oDlSubGrupo As DataList = controle.BindingContainer.BindingContainer
                    Dim v_oDlRow As DataListItem = controle.BindingContainer
                    Dim v_sCPF As String = v_oGridview.DataKeys(v_oGvRow.RowIndex).Item("CPF")
                    Dim v_sSituacao As String = "I"
                    Dim v_sSubGrupo As Integer = controle.InputAttributes.Item("SubGrupoId")

                    If controle.Checked Then
                        v_sSituacao = "A"
                        Dim v_iSubGrupoId As Integer = v_oDlSubGrupo.DataKeys(v_oDlRow.ItemIndex)

                        'VERIFICA SE O USUARIO MARCADO TEM ACESSO A OUTRO PERFIL
                        If ValidaOutroAcessoSubGrupo(v_oGridview, v_oGvRow, p_iIndAceso, v_iSubGrupoId) Then
                            controle.Checked = False
                            Me.RN.ClsUsuarioApoliceRN() = Nothing
                            Exit Sub
                        End If
                    End If

                    If p_iIndAceso.Equals(7) Then

                    End If

                    'ATUALIZA DADOS DO USUARIO
                    'Me.RN.ApoliceId = Me.ApoliceId
                    'Me.RN.RamoId = Me.RamoId
                    'Me.RN.CPF = v_sCPF
                    'Me.RN.Usuario = MyBase.LoginWeb

                    'ATUALIZA SITUA�AO 
                    Me.RN.ClsUsuarioApoliceRN(p_iContador) = New ClsUsuarioApoliceRN(Me.RN.DAO)
                    Me.RN.ClsUsuarioApoliceRN(p_iContador).ApoliceId = Me.ApoliceId
                    Me.RN.ClsUsuarioApoliceRN(p_iContador).RamoId = Me.RamoId
                    Me.RN.ClsUsuarioApoliceRN(p_iContador).SubGrupoId = v_sSubGrupo
                    Me.RN.ClsUsuarioApoliceRN(p_iContador).CPF = v_sCPF
                    Me.RN.ClsUsuarioApoliceRN(p_iContador).IndSituacao = v_sSituacao
                    Me.RN.ClsUsuarioApoliceRN(p_iContador).IndAcesso = p_iIndAceso
                    Me.RN.ClsUsuarioApoliceRN(p_iContador).SeguradoraCodSusep = Me.DtApolice(Me.ApoliceId, Me.RamoId, Me.SubGrupoId).Rows(0)("seguradora_cod_susep")
                    Me.RN.ClsUsuarioApoliceRN(p_iContador).SucursalSeguradoraId = Me.DtApolice(Me.ApoliceId, Me.RamoId, Me.SubGrupoId).Rows(0)("sucursal_seguradora_id")

                    If controle.Checked Then
                        'If v_oGridview.DataKeys(v_oGvRow.RowIndex).Item("email_enviado") = "N" Then
                        If Me.DtUsuarios(Me.ApoliceId, Me.RamoId, Me.SubGrupoId).Rows(v_oGvRow.RowIndex).Item("email_enviado") = "N" Then
                            Me.DtUsuarios(Me.ApoliceId, Me.RamoId, Me.SubGrupoId).Rows(v_oGvRow.RowIndex).Item("email_enviado") = "S"
                            Me.DtUsuarios(Me.ApoliceId, Me.RamoId, Me.SubGrupoId).AcceptChanges()

                            Dim v_aSenha As ArrayList = Me.GerarSenha()
                            Dim v_sNome As String = ""
                            Dim v_sLoginWeb As String = ""
                            Dim v_sEmail As String = ""

                            Dim v_oClsUsuarioRN As New ClsUsuarioRN()
                            v_oClsUsuarioRN.CPF = v_sCPF
                            If v_oClsUsuarioRN.Obter() Then
                                v_sNome = v_oClsUsuarioRN.Nome
                                v_sLoginWeb = v_oClsUsuarioRN.LoginRede
                                v_sEmail = v_oClsUsuarioRN.Email
                            End If

                            'ATUALIZA EMAIL ENVIADO
                            Me.RN.CPF = v_sCPF
                            Me.RN.EmailEnviado = "S"
                            Me.RN.Usuario = MyBase.LoginWeb

                            'CADASTRA/ATUALIZA USUARIO_TB
                            ReDim Me.RN.ClsUsuarioApoliceRN(p_iContador).ClsUsuarioRN(0)
                            Me.RN.ClsUsuarioApoliceRN(p_iContador).ClsUsuarioRN(0) = New ClsUsuarioRN(Me.RN.DAO)
                            Me.RN.ClsUsuarioApoliceRN(p_iContador).ClsUsuarioRN(0).Nome = v_sNome
                            Me.RN.ClsUsuarioApoliceRN(p_iContador).ClsUsuarioRN(0).CPF = v_sCPF
                            Me.RN.ClsUsuarioApoliceRN(p_iContador).ClsUsuarioRN(0).LoginWeb = v_sLoginWeb
                            Me.RN.ClsUsuarioApoliceRN(p_iContador).ClsUsuarioRN(0).Email = v_sEmail
                            Me.RN.ClsUsuarioApoliceRN(p_iContador).ClsUsuarioRN(0).SenhaWeb = v_aSenha(1)

                            'REDIMENSIONA A CLASSE PARA ENVIO DE EMAIL
                            ReDim Me.RN.ClsUsuarioApoliceRN(p_iContador).ClsEmailRN(0)
                            Me.RN.ClsUsuarioApoliceRN(p_iContador).ClsEmailRN(0) = New clsEmailRN(Me.RN.DAO)
                            Dim v_sMensagem As String = Me.EmailMensagem(v_aSenha(0), _
                                                                       v_sCPF, _
                                                                       v_sNome, _
                                                                       v_sLoginWeb, _
                                                                       Me.ApoliceId, _
                                                                       Me.RamoId, _
                                                                       v_sSubGrupo, _
                                                                       True, _
                                                                       p_iIndAceso)
                            Me.RN.ClsUsuarioApoliceRN(p_iContador).ClsEmailRN(0).Assunto = "Confirma��o de Cadastro na Web"
                            Me.RN.ClsUsuarioApoliceRN(p_iContador).ClsEmailRN(0).De = "emissao.vida@aliancadobrasil.com.br"
                            Me.RN.ClsUsuarioApoliceRN(p_iContador).ClsEmailRN(0).Para = v_sEmail
                            Me.RN.ClsUsuarioApoliceRN(p_iContador).ClsEmailRN(0).Mensagem = v_sMensagem
                            Me.RN.ClsUsuarioApoliceRN(p_iContador).ClsEmailRN(0).SiglaSistema = "SEGBR"
                            Me.RN.ClsUsuarioApoliceRN(p_iContador).ClsEmailRN(0).SiglaRecurso = "SEGW0076"
                            Me.RN.ClsUsuarioApoliceRN(p_iContador).ClsEmailRN(0).Chave = "ddmg0802"
                            Me.RN.ClsUsuarioApoliceRN(p_iContador).ClsEmailRN(0).Formato = 2
                        End If
                    End If

                    p_iContador += 1
                End If
end if
            ' Chamada recursiva da fun��o
            InsertSituacaoAllCheckBox(vlControle.Controls, p_iIndAceso, p_iContador)
        Next
    End Sub

    Private Sub HabilitaDesabilitaTabs()
        Select Case Me.Acesso
            Case 7  'master
                'Nova Consultoria 12/01/2012 - Fabricio Brandi
                ' Alterado o comportamento para o usu�rio master de acordo com a Demanda AB: 12784293
                Me.TabPanel4.Enabled = True
                Me.TabPanel3.Enabled = True
                Me.TabPanel2.Enabled = True
                Me.TabPanel1.Enabled = True
                Me.TabPanel4.Visible = True
                Me.TabPanel3.Visible = True
                Me.TabPanel2.Visible = True
                Me.TabPanel1.Visible = True
                Me.btnSelecionar.Enabled = True
            Case 6
                '4.1.	Manuten��o de Usu�rios (SEGW0076 - VS2005)
                '4.1.1.	Liberar acesso a funcionalidade Revogar Senha para todos os usu�rios cadastrados na ap�lice.
                '4.1.2.	Permitir a visualiza��o em tela das informa��es do usu�rio selecionado (tela de detalhamento)
                '4.1.3.	O bot�o GRAVAR / ATUALIZAR dever� ficar desabilitado para o perfil Central de Atendimento (IND_ACESSO = 6).
                'Me.TabPanel4.Enabled = False
                'Me.TabPanel3.Enabled = False
                'Me.TabPanel2.Enabled = False
                'Me.TabPanel1.Enabled = True
                'Me.TabPanel4.Visible = True
                'Me.TabPanel3.Visible = True
                'Me.TabPanel2.Visible = True
                'Me.TabPanel1.Visible = True

                'fsa - 27/07/2011
                'Me.gvAdmMaster.Enabled = False
                'Me.btnAdmMaster.Enabled = False
                'Me.gvAdmApolice.Enabled = False
                'Me.btnAdmApolice.Enabled = False
                'Me.gvAdmSubGrupo.Enabled = False
                'Me.btnAdmSubGrupo.Enabled = False
                'Me.gvAdmOperador.Enabled = False
                'Me.btnAdmOperadores.Enabled = False

                Me.gvAdmMaster.Enabled = True
                Me.btnAdmMaster.Enabled = True
                Me.gvAdmApolice.Enabled = True
                Me.btnAdmApolice.Enabled = True
                Me.gvAdmSubGrupo.Enabled = True
                Me.btnAdmSubGrupo.Enabled = True
                Me.gvAdmOperador.Enabled = True
                Me.btnAdmOperadores.Enabled = True

                'fsa - 25/07/2011
                'Me.btnSelecionar.Enabled = False
                Me.btnSelecionar.Enabled = True

                ' breno.nery (Nova Consultoria) - 01/08/2011
                ' 9420650 - Melhorias no Sistema Vida Web 
                ' Central de Atendimento com mesmo acesso de Adm. Alian�a do Brasil
                Me.TabPanel4.Enabled = True
                Me.TabPanel3.Enabled = True
                Me.TabPanel2.Enabled = True
                Me.TabPanel1.Enabled = True
                Me.TabPanel4.Visible = True
                Me.TabPanel3.Visible = True
                Me.TabPanel2.Visible = True
                Me.TabPanel1.Visible = True
            Case 5
                Me.Panel1.Enabled = False
            Case 4  'operador
                Me.TabPanel5.Enabled = False
                Me.TabPanel4.Enabled = True
                Me.TabPanel3.Enabled = False
                Me.TabPanel2.Enabled = False
                Me.TabPanel1.Enabled = True
                Me.TabPanel5.Visible = False
                Me.TabPanel4.Visible = True
                Me.TabPanel3.Visible = False
                Me.TabPanel2.Visible = False
                Me.TabPanel1.Visible = False
                Me.btnSelecionar.Enabled = False
            Case 3  'adm subgrupo
                gvAdmMaster.Enabled = False
                Me.btnAdmMaster.Enabled = False
                Me.TabPanel5.Enabled = True
                Me.TabPanel4.Enabled = True
                Me.TabPanel3.Enabled = False
                Me.TabPanel2.Enabled = False
                Me.TabPanel1.Enabled = True
                Me.TabPanel5.Visible = True
                Me.TabPanel4.Visible = True
                Me.TabPanel3.Visible = False
                Me.TabPanel2.Visible = False
                Me.TabPanel1.Visible = False
                Me.btnSelecionar.Enabled = True
            Case 2  'adm apolice
                Me.TabPanel4.Enabled = True
                Me.TabPanel3.Enabled = True
                'Inicio-Demanda:12149172: Autor:Rodrigo.Moura(Confitec) Data:30/08/2011 
                'Me.TabPanel2.Enabled = False
                Me.TabPanel2.Enabled = True
                'Fim-Demanda:12149172: Autor:Rodrigo.Moura(Confitec) Data:30/08/2011 
                Me.TabPanel1.Enabled = True
                Me.TabPanel4.Visible = True
                Me.TabPanel3.Visible = True
                'Inicio-Demanda:12149172: Autor:Rodrigo.Moura(Confitec) Data:30/08/2011 
                'Me.TabPanel2.Visible = False
                Me.TabPanel2.Visible = True
                'Fim-Demanda:12149172: Autor:Rodrigo.Moura(Confitec) Data:30/08/2011 

                Me.TabPanel1.Visible = True
                Me.btnSelecionar.Enabled = True
            Case 1 'adm alianca
                Me.TabPanel4.Enabled = True
                Me.TabPanel3.Enabled = True
                Me.TabPanel2.Enabled = True
                Me.TabPanel1.Enabled = True
                Me.TabPanel4.Visible = True
                Me.TabPanel3.Visible = True
                Me.TabPanel2.Visible = True
                Me.TabPanel1.Visible = True
                Me.btnSelecionar.Enabled = True
        End Select
    End Sub

    Private Sub HabilitaDesabilitaTabsMaster(ByVal p_bDesabilita As Boolean)
        'Me.gvAdmApolice.Enabled = IIf(p_bDesabilita = True, False, True)
        'Me.btnAdmApolice.Enabled = IIf(p_bDesabilita = True, False, True)
        Me.gvAdmSubGrupo.Enabled = IIf(p_bDesabilita = True, False, True)
        Me.btnAdmSubGrupo.Enabled = IIf(p_bDesabilita = True, False, True)
        Me.gvAdmOperador.Enabled = IIf(p_bDesabilita = True, False, True)
        Me.btnAdmOperadores.Enabled = IIf(p_bDesabilita = True, False, True)
        Me.btnSelecionar.Enabled = IIf(p_bDesabilita = True, False, True)
    End Sub

    Function ValidaOutroAcesso(ByVal p_oGridviewAdm As GridView, ByVal p_oRowAdm As GridViewRow, ByVal p_iIndAcesso As Integer) As Boolean
        Dim v_oClsUsuarioApoliceRN As New ClsUsuarioApoliceRN
        Dim v_oDtPermissoes As DataTable

        v_oClsUsuarioApoliceRN.ApoliceId = Me.ApoliceId
        v_oClsUsuarioApoliceRN.RamoId = Me.RamoId
        v_oClsUsuarioApoliceRN.CPF = p_oGridviewAdm.DataKeys(p_oRowAdm.RowIndex)("CPF")
        v_oClsUsuarioApoliceRN.IndSituacao = "A"

        v_oDtPermissoes = v_oClsUsuarioApoliceRN.ListarPermissoesSituacao.Tables(0)

        Dim v_oDtViewPermissoes As New DataView(v_oDtPermissoes)

        Select Case p_iIndAcesso
            Case 2
                v_oDtViewPermissoes.RowFilter = "ind_acesso IN (3,4)"
            Case 3
                v_oDtViewPermissoes.RowFilter = "ind_acesso IN (2,4)"
            Case 4
                v_oDtViewPermissoes.RowFilter = "ind_acesso IN (2,3)"
        End Select

        If v_oDtViewPermissoes.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Function ValidaOutroAcessoSubGrupo(ByVal p_oGridviewAdm As GridView, ByVal p_oRowAdm As GridViewRow, ByVal p_iIndAcesso As Integer, ByVal p_iSubGrupoId As Integer) As Boolean
        Dim v_oClsUsuarioApoliceRN As New ClsUsuarioApoliceRN
        Dim v_oDtPermissoes As DataTable

        v_oClsUsuarioApoliceRN.ApoliceId = Me.ApoliceId
        v_oClsUsuarioApoliceRN.RamoId = Me.RamoId
        Select Case p_iIndAcesso
            Case 3
                v_oClsUsuarioApoliceRN.IndAcesso = 4
            Case 4
                v_oClsUsuarioApoliceRN.IndAcesso = 3
        End Select
        v_oClsUsuarioApoliceRN.IndSituacao = "A"
        v_oClsUsuarioApoliceRN.CPF = p_oGridviewAdm.DataKeys(p_oRowAdm.RowIndex)("CPF")
        v_oClsUsuarioApoliceRN.SubGrupoId = p_iSubGrupoId

        v_oDtPermissoes = v_oClsUsuarioApoliceRN.VerifivaPermissaSubGrupoAcesso.Tables(0)

        If v_oDtPermissoes.Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub IncluirLogEmailUsuario(ByVal p_iApoliceId As Integer, _
                                            ByVal p_iRamoId As Integer, _
                                            ByVal p_iSubGrupoId As Integer, _
                                            ByVal p_iLogEmailId As String, _
                                            ByVal p_sUsuario As String, _
                                            ByVal p_oDAO As DAO)

        Dim v_iSeguradoraCodSusep As Integer = Me.DtApolice(p_iApoliceId, p_iRamoId, p_iSubGrupoId).Rows(0)("seguradora_cod_susep")
        Dim v_iSucursalSeguradoraId As Integer = Me.DtApolice(p_iApoliceId, p_iRamoId, p_iSubGrupoId).Rows(0)("sucursal_seguradora_id")

        Me.HelperSEGW.IncluirLogEmailUsuario(p_iApoliceId, _
                                                p_iRamoId, _
                                                p_iSubGrupoId, _
                                                v_iSeguradoraCodSusep, _
                                                v_iSucursalSeguradoraId, _
                                                p_iLogEmailId, _
                                                p_sUsuario, _
                                                p_oDAO)
    End Sub
#End Region

#Region " Outros Metodos "
    Protected Sub txtCPF_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim v_otxtCPF As TextBox = DirectCast(sender, TextBox)
        If v_otxtCPF.Text.Length = 11 Then
            If Not v_otxtCPF.Text.Equals("11111111111") Then
                Dim v_aArrayParametros As New ArrayList
                'Dim v_oDtUsuario As DataTable
                'Dim v_clsUsuarioApoliceRN As New ClsUsuarioApoliceRN
                'v_clsUsuarioApoliceRN.CPF = v_otxtCPF.Text
                'v_clsUsuarioApoliceRN.ApoliceId = Me.ApoliceId
                ''v_clsUsuarioApoliceRN.IndAcesso = "A"
                'v_oDtUsuario = v_clsUsuarioApoliceRN.Listar.Tables(0)

                'If v_oDtUsuario.Rows.Count >= 1 Then
                Dim v_oClsUsuarioRN As New ClsUsuarioRN
                v_oClsUsuarioRN.CPF = v_otxtCPF.Text
                If v_oClsUsuarioRN.Obter Then
                    DirectCast(Me.fvUsuario.FindControl("txtLogin"), TextBox).Text = v_oClsUsuarioRN.LoginRede
                    DirectCast(Me.fvUsuario.FindControl("txtEmail"), TextBox).Text = v_oClsUsuarioRN.Email
                    DirectCast(Me.fvUsuario.FindControl("txtEmail2"), TextBox).Text = v_oClsUsuarioRN.Email
                Else
                    DirectCast(Me.fvUsuario.FindControl("txtLogin"), TextBox).Text = ""
                    DirectCast(Me.fvUsuario.FindControl("txtEmail"), TextBox).Text = ""
                    DirectCast(Me.fvUsuario.FindControl("txtEmail2"), TextBox).Text = ""
                End If

                v_otxtCPF.Text = CPF_Format(v_otxtCPF.Text)
            Else
                v_otxtCPF.Text = ""
                v_otxtCPF.Focus()
                ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel1, Me.UpdatePanel1.GetType(), "JavaScript", "//alert('CPF com valor 11111111111 n�o pode ser cadastrado.'); document.getElementById('" & v_otxtCPF.ClientID & "').focus();", True)
            End If
        End If
        'ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel1, Me.UpdatePanel1.GetType(), "JavaScript", "document.getElementById('" & Me.fvUsuario.FindControl("txtLogin").ClientID & "').focus();", True)
        'DirectCast(Me.fvUsuario.FindControl("txtLogin"), TextBox).Focus()
    End Sub

    Public Function CPF_Format(ByVal cpf As String) As String
        Try
            'Return cpf
            Dim iCpf As Int64 = Convert.ToInt64(cpf)

            Return String.Format("{0:000\.###\.###-##}", iCpf)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function CPF_Limpar(ByVal cpf As String) As String
        Try
            cpf = cpf.ToString().Replace("-", "")
            cpf = cpf.ToString().Replace(".", "")
            cpf = cpf.ToString().Replace(".", "")
            cpf = cpf.ToString().Replace("/", "")

            Return cpf

        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region

    Protected Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim v_ssenha As String = "15151334"
        Dim senhahash As String = GenerateHash(v_ssenha)
        Response.Write(senhahash)
    End Sub

    Protected Sub dlMaster_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'HabilitaDesabilitaTabsMaster(Me.ExisteMaster)
    End Sub


    Protected Sub chkTodos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim v_oGvRow As GridViewRow = sender.Parent.BindingContainer
        Dim v_oDlSubGrupo As DataList = DirectCast(v_oGvRow.FindControl("dlSubGrupo"), DataList)
        Dim v_oCkbTodos As CheckBox = DirectCast(sender, CheckBox)

        If v_oCkbTodos.Checked Then

            ChangeAllCheckBoxStates(Me.gvAdmSubGrupo.Controls, v_oCkbTodos.InputAttributes.Item("Grupo"), v_oCkbTodos.ClientID)

            ChangeAllCheckBoxStates(v_oDlSubGrupo.Controls, True, v_oCkbTodos.ID)
        Else
            ChangeAllCheckBoxStates(v_oDlSubGrupo.Controls, False, v_oCkbTodos.ID)
        End If
    End Sub

    Protected Sub CkbAdmSubGrupo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim v_oCkbAdmSubGrupo As CheckBox = DirectCast(sender, CheckBox)
        If v_oCkbAdmSubGrupo.Checked Then
            Dim v_oDlSubGrupo As DataList = sender.Parent.BindingContainer
            Dim v_oDlRow As DataListItem = sender.BindingContainer
            Dim v_oGvRow As GridViewRow = sender.Parent.Parent.BindingContainer
            Dim v_oClsUsuarioApoliceRN As New ClsUsuarioApoliceRN
            Dim v_oDtPermissoes As DataTable

            v_oClsUsuarioApoliceRN.ApoliceId = Me.ApoliceId
            v_oClsUsuarioApoliceRN.RamoId = Me.RamoId
            v_oClsUsuarioApoliceRN.IndAcesso = 4
            v_oClsUsuarioApoliceRN.IndSituacao = "A"
            v_oClsUsuarioApoliceRN.CPF = Me.gvAdmSubGrupo.DataKeys(v_oGvRow.RowIndex)("CPF")
            v_oClsUsuarioApoliceRN.SubGrupoId = v_oDlSubGrupo.DataKeys(v_oDlRow.ItemIndex)

            v_oDtPermissoes = v_oClsUsuarioApoliceRN.VerifivaPermissaSubGrupoAcesso.Tables(0)

            If v_oDtPermissoes.Rows.Count > 0 Then
                DirectCast(sender, CheckBox).Checked = False
                ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel3, Me.UpdatePanel3.GetType(), "JavaScript", "alert('Usu�rio ativo em outro perfil.');", True)
            Else

                'ChangeAllCheckBoxTodos(Me.gvAdmSubGrupo.Controls, "AdmSubGrupoTodos")
                ChangeAllCheckBoxStates(Me.gvAdmSubGrupo.Controls, v_oCkbAdmSubGrupo.InputAttributes.Item("Grupo"), v_oCkbAdmSubGrupo.ClientID)
                HabDesabCheckBoxGrupoTodos(Me.gvAdmSubGrupo.Controls, False)
            End If
            'ChangeAllCheckBoxStates(Me.gvAdmSubGrupo.Controls, v_oCkbAdmSubGrupo.InputAttributes.Item("Grupo"), v_oCkbAdmSubGrupo.ClientID)
        Else
            HabDesabCheckBoxGrupoTodos(Me.gvAdmSubGrupo.Controls, False)
        End If

    End Sub




    Protected Sub ckbAdmOperador_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim v_oCkbAdmOperador As CheckBox = DirectCast(sender, CheckBox)
        If v_oCkbAdmOperador.Checked Then
            Dim v_oDlSubGrupo As DataList = sender.Parent.BindingContainer
            Dim v_oDlRow As DataListItem = sender.BindingContainer
            Dim v_oGvRow As GridViewRow = sender.Parent.Parent.BindingContainer
            Dim v_oClsUsuarioApoliceRN As New ClsUsuarioApoliceRN
            Dim v_oDtPermissoes As DataTable

            v_oClsUsuarioApoliceRN.ApoliceId = Me.ApoliceId
            v_oClsUsuarioApoliceRN.RamoId = Me.RamoId
            v_oClsUsuarioApoliceRN.IndAcesso = 3
            v_oClsUsuarioApoliceRN.IndSituacao = "A"
            v_oClsUsuarioApoliceRN.CPF = Me.gvAdmOperador.DataKeys(v_oGvRow.RowIndex)("CPF")
            v_oClsUsuarioApoliceRN.SubGrupoId = v_oDlSubGrupo.DataKeys(v_oDlRow.ItemIndex)

            v_oDtPermissoes = v_oClsUsuarioApoliceRN.VerifivaPermissaSubGrupoAcesso.Tables(0)

            If v_oDtPermissoes.Rows.Count > 0 Then
                DirectCast(sender, CheckBox).Checked = False
                ScriptManager.RegisterClientScriptBlock(Me.UpdatePanel4, Me.UpdatePanel4.GetType(), "JavaScript", "alert('Usu�rio ativo em outro perfil.');", True)
            Else
                ChangeAllCheckBoxStates(Me.gvAdmOperador.Controls, v_oCkbAdmOperador.InputAttributes.Item("Grupo"), v_oCkbAdmOperador.ClientID)
                HabDesabCheckBoxGrupoTodos(Me.gvAdmOperador.Controls, False)
            End If
            'ChangeAllCheckBoxStates(Me.gvAdmOperador.Controls, v_oCkbAdmOperador.InputAttributes.Item("Grupo"), v_oCkbAdmOperador.ClientID)
        Else
            HabDesabCheckBoxGrupoTodos(Me.gvAdmOperador.Controls, False)
        End If
    End Sub

    Protected Sub ckbAdmMaster_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim v_bDesabilita As Boolean = False
        Dim v_oCkbAdmMaster As CheckBox = DirectCast(sender, CheckBox)
        If v_oCkbAdmMaster.Checked Then
            v_bDesabilita = False
            ChangeAllCheckBoxStates(Me.gvAdmMaster.Controls, v_oCkbAdmMaster.InputAttributes.Item("Grupo"), v_oCkbAdmMaster.ClientID)
            'ChangeAllCheckBoxStates(Me.gvAdmMaster.Controls, True, v_oCkbAdmMaster.ID)
            HabDesabCheckBoxGrupoTodos(Me.gvAdmMaster.Controls, False)
        Else
            v_bDesabilita = True
            'ChangeAllCheckBoxStates(Me.gvAdmMaster.Controls, False, v_oCkbAdmMaster.ID)
            HabDesabCheckBoxGrupoTodos(Me.gvAdmMaster.Controls, False)
        End If
        Me.gvAdmSubGrupo.DataBind()
        Me.gvAdmOperador.DataBind()
        VerificaAcessoMaster()

    End Sub

    Protected Sub chkTodosOperador_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim v_oGvRow As GridViewRow = sender.Parent.BindingContainer
        Dim v_oDl As DataList = DirectCast(v_oGvRow.FindControl("dlOperador"), DataList)
        Dim v_oCkbTodos As CheckBox = DirectCast(sender, CheckBox)

        If v_oCkbTodos.Checked Then
            ChangeAllCheckBoxStates(v_oDl.Controls, v_oCkbTodos.InputAttributes.Item("Grupo"), v_oCkbTodos.ClientID)
            ChangeAllCheckBoxStates(v_oDl.Controls, True, v_oCkbTodos.ID)
        Else
            ChangeAllCheckBoxStates(v_oDl.Controls, False, v_oCkbTodos.ID)
        End If

    End Sub

    Protected Sub chkTodosMaster_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim v_bDesabilita As Boolean = False

        Dim v_oGvRow As GridViewRow = sender.Parent.BindingContainer
        Dim v_oDl As DataList = DirectCast(v_oGvRow.FindControl("dlMaster"), DataList)
        Dim v_oCkbTodos As CheckBox = DirectCast(sender, CheckBox)

        If v_oCkbTodos.Checked Then
            ChangeAllCheckBoxStates(v_oDl.Controls, v_oCkbTodos.InputAttributes.Item("Grupo"), v_oCkbTodos.ClientID)
            ChangeAllCheckBoxStates(v_oDl.Controls, True, v_oCkbTodos.ID)
        Else
            ChangeAllCheckBoxStates(v_oDl.Controls, False, v_oCkbTodos.ID)
        End If

        Me.gvAdmSubGrupo.DataBind()
        Me.gvAdmOperador.DataBind()
        VerificaAcessoMaster()
    End Sub





    'ChangeAllCheckBoxStates(Me.gvAdmSubGrupo.Controls, v_oCkbAdmSubGrupo.InputAttributes.Item("Grupo"), v_oCkbAdmSubGrupo.ClientID)
    'HabDesabCheckBoxGrupoTodos(Me.gvAdmSubGrupo.Controls, False)
End Class
