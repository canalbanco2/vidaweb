/* FUN��ES PARA A PESQUISA DE ITEM NO FORMUL�RIO PADR�O */

var v_oForm;
var m_sXmlFiltro; /* XML COM OS CAMPOS UTILIZADOS NO FILTRO */
var m_iExibicao; /* TIPOS DE EXIBICAO DO ITEM (TELA PRINCIPAL DA APLICACAO OU SELECAO EM JANELA) */

function PreencherDDLFiltros() {
	v_oForm = document.forms[0];

	v_oForm.ddlCamposFiltroPesquisa.options.length = 0;

	var v_oXmlFiltros = new ActiveXObject("MSXML2.DOMDocument");
	v_oXmlFiltros.async = false;
	v_oXmlFiltros.load(m_sXmlFiltro);

	var v_sLinhaExtra = v_oXmlFiltros.documentElement.getAttribute("LinhaExtra");
	var v_sTextoLinhaExtra = "Todos";

	if (v_sLinhaExtra != null) {
		if (v_sLinhaExtra == "1") v_sTextoLinhaExtra = "Todas";
		if (v_sLinhaExtra == "3") v_sTextoLinhaExtra = "Selecione";
	}

	if (v_sLinhaExtra != "0") {
		v_oForm.ddlCamposFiltroPesquisa.options[0] = new Option(v_sTextoLinhaExtra, m_cValuePadraoLinhaExtraCombo);
	}

	var v_oItens = v_oXmlFiltros.documentElement.getElementsByTagName("ItemFiltro");
	var v_sValue = "";
	var v_sText = "";

	for (var v_iCount = 0; v_iCount < v_oItens.length; v_iCount++) {
		var v_oItemExibicao = v_oItens[v_iCount].selectSingleNode("Exibicao").childNodes;

		for (var v_iCountExibicao = 0; v_iCountExibicao < v_oItemExibicao.length; v_iCountExibicao++) {
			if (v_oItemExibicao[v_iCountExibicao].firstChild.nodeValue == m_iExibicao) {
				v_sValue = v_oItens[v_iCount].selectSingleNode("IdNameCampo").firstChild.nodeValue;
				v_sText = v_oItens[v_iCount].selectSingleNode("DescricaoCampo").firstChild.nodeValue;

				v_oForm.ddlCamposFiltroPesquisa.options[v_oForm.ddlCamposFiltroPesquisa.options.length] = new Option(v_sText, v_sValue);
			}
		}
	}

	if (v_oForm.hdnFiltroSelecionado.value != "") {
		v_oForm.ddlCamposFiltroPesquisa.value = v_oForm.hdnFiltroSelecionado.value;
	}
}

function SelecionarFiltroPesquisa(p_bChange) {
	v_oForm = document.forms[0];
	var v_oDDLCamposFiltroPesquisa = v_oForm.ddlCamposFiltroPesquisa;
	var v_oTDCampoPesquisa = document.getElementById("tdCampoPesquisa");

	v_oTDCampoPesquisa.innerHTML = "";
	v_oForm.hdnFiltroSelecionado.value = "";

	if (p_bChange) v_oForm.hdnValorFiltro.value = "";

	if (v_oDDLCamposFiltroPesquisa.value == m_cValuePadraoLinhaExtraCombo) return;

	ExibirAguarde();

	var v_oXmlFiltros = new ActiveXObject("MSXML2.DOMDocument");
	v_oXmlFiltros.async = false;
	v_oXmlFiltros.load(m_sXmlFiltro);

	var v_oItemFiltro = v_oXmlFiltros.documentElement.getElementsByTagName("ItemFiltro[IdNameCampo = '" + v_oDDLCamposFiltroPesquisa.value + "']");
	var v_sTipoCampo = v_oItemFiltro[0].selectSingleNode("TipoCampo").firstChild.nodeValue;
	var v_sFuncaoJavaScript = v_oItemFiltro[0].selectSingleNode("FuncaoJavaScript");
	var v_bPreenchimentoObrigatorio = (v_oItemFiltro[0].getAttribute("PreenchimentoObrigatorio") == "true");
	var v_oCampoPesquisa;

	if (v_sTipoCampo == "C") {
		try {
			var v_sXMLPreenchimento = v_oItemFiltro[0].selectSingleNode("TipoCampo").getAttribute("XMLPreenchimento");

			if (v_oForm.hdnLinkSeguroCombo) {
				if (v_sXMLPreenchimento.indexOf("?") == -1) {
					v_sXMLPreenchimento += "?";
				}
				else {
					v_sXMLPreenchimento += "&";
				}

				v_sXMLPreenchimento += "pqsSeguranca=" + v_oForm.hdnLinkSeguroCombo.value;
			}

			var v_oXmlDadosPreenchimento = new ActiveXObject("MSXML2.DOMDocument");
			v_oXmlDadosPreenchimento.async = false;
			v_oXmlDadosPreenchimento.load(v_sXMLPreenchimento);

			v_oCampoPesquisa = document.createElement("SELECT");

			v_oCampoPesquisa.options.length = 0;

			var v_oOptions = v_oXmlDadosPreenchimento.documentElement.getElementsByTagName("OptionDDL");
			var v_sValue = "";
			var v_sText = "";

			for (var v_iCount = 0; v_iCount < v_oOptions.length; v_iCount++) {
				v_sValue = v_oOptions[v_iCount].selectSingleNode("Value").firstChild.nodeValue;
				v_sText = v_oOptions[v_iCount].selectSingleNode("Text").firstChild.nodeValue;

				v_oCampoPesquisa.options[v_iCount + 1] = new Option(v_sText, v_sValue);
			}

			v_oCampoPesquisa.options[0] = null;
		}
		catch(e) {
			alert("Problemas no preenchimento da combo de pesquisa!\nEntre em contato com o administrador do sistema.");
			EsconderAguarde();
			return false;
		}
	}
	else {
		v_oCampoPesquisa = document.createElement("INPUT");

		var v_sDirecao = "left";
		var v_sDirecaoTexto = v_oItemFiltro[0].selectSingleNode("TipoCampo").getAttribute("DirecaoTexto");
		var v_sMascara = v_oItemFiltro[0].selectSingleNode("TipoCampo").getAttribute("Mascara");

		if (v_sDirecaoTexto != null) {
			if (v_sDirecaoTexto == "D") v_sDirecao = "right";

			v_oCampoPesquisa.style.textAlign = v_sDirecao;
		}

		if (v_sMascara != null && v_sTipoCampo != "D" && v_sTipoCampo != "C") {
			v_oCampoPesquisa.onkeyup = function() { Mascara(this, v_sMascara, v_sTipoCampo, v_sDirecao, event); }
		}
	}

	v_oCampoPesquisa.id = "CampoPesquisa";
	v_oCampoPesquisa.name = "CampoPesquisa";

	if (v_bPreenchimentoObrigatorio) {
		v_oCampoPesquisa.TagMensagemObrigatorio = v_oDDLCamposFiltroPesquisa.options[v_oDDLCamposFiltroPesquisa.selectedIndex].text;
	}

	if (v_sTipoCampo == "N") {
		v_oCampoPesquisa.style.width = 120;
		v_oCampoPesquisa.onkeypress = function() { OnlyNumbers(null, event); }
	}
	else if (v_sTipoCampo == "L") {
		v_oCampoPesquisa.style.width = 250;
		v_oCampoPesquisa.onkeypress = function() { OnlyLetters(null, event); }
	}
	else if (v_sTipoCampo == "D") {
		v_oCampoPesquisa.maxLength = 10;
		v_oCampoPesquisa.style.width = 80;
		v_oCampoPesquisa.onkeyup = function() { Mascara(this, "##/##/####", "N", "E", event); }
		v_oCampoPesquisa.onblur = function() { ValidarData(this); }
	}
	else if (v_sTipoCampo != "C") {
		v_oCampoPesquisa.style.width = 350;
	}

	v_oForm.hdnFiltroSelecionado.value = v_oDDLCamposFiltroPesquisa.value;
	v_oCampoPesquisa.value = v_oForm.hdnValorFiltro.value;

	v_oTDCampoPesquisa.appendChild(v_oCampoPesquisa);

	document.getElementById("CampoPesquisa").focus();

	if (v_sFuncaoJavaScript != null) {
		if (v_sFuncaoJavaScript.firstChild != null) {
			eval(v_sFuncaoJavaScript.firstChild.nodeValue);
		}
	}

	EsconderAguarde();
}

function ExecutarSelecionar() {
	if (!VerificarPreenchimentoObrigatorio()) return false;

	v_oForm = document.forms[0];

	if (v_oForm.ddlCamposFiltroPesquisa.value == m_cValuePadraoLinhaExtraCombo) return;

	v_oForm.hdnValorFiltro.value = v_oForm.CampoPesquisa.value;
}

function SelecionarItemPesquisa(p_oObjeto) {
	window.returnValue = p_oObjeto.parentNode.TagRetorno;
    window.close();
}
