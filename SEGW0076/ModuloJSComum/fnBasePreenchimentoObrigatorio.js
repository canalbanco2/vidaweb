/* FUNCOES PARA TRATAR O PREENCHIMENTO OBRIGATORIO DE CAMPOS NOS FORMULARIOS */

var m_cValuePadraoLinhaExtraCombo = "##!##L-I-N-H-A-E-X-T-R-A##!##"

/* FUNCAO PARA VERIFICAR PREENCHIMENTO OBRIGATORIO */
function VerificarPreenchimentoObrigatorio() {
	if (ObjExist(document.forms[0].hdnCamposObrigatorios)) {
		return VerificarPreenchimentoObrigatorioCampo();
	}
	else {
		return VerificarPreenchimentoObrigatorioTag();
	}
}

/* FUN��O QUE VERIFICA O PREENCHIMENTO OBRIGAT�RIO DOS CAMPOS QUE POSSUEM A TAG: "TagMensagemObrigatorio" */
function VerificarPreenchimentoObrigatorioTag() {
	var v_oForm = document.forms[0];
	var v_oCampoFoco;

	try {
		for (i = 0; i < v_oForm.elements.length; i++) {
			/* VERIFICAR SE O OBJETO POSSUI A TAG PARA DIZER SE � OBRIGAT�RIO */
			if (ObjExist(v_oForm.elements[i].TagMensagemObrigatorio)) {
				/* SE EXISTIR O ATRIBUTO TagCampoFoco � PORQUE O FOCO N�O � O PR�PRIO CAMPO */
				if (v_oForm.elements[i].TagCampoFoco) {
					v_oCampoFoco = document.getElementById(v_oForm.elements[i].TagCampoFoco);
				}
				else {
					v_oCampoFoco = v_oForm.elements[i];
				}

				/* SE FOR OBRIGAT�RIO, ENT�O VERIFICAR TIPO DO OBJETO E FAZER A VALIDA��O */
				if ((v_oForm.elements[i].type == "text") || (v_oForm.elements[i].type == "textarea") || (v_oForm.elements[i].type == "file")) {
					if (Trim(v_oForm.elements[i].value) == "") {
						v_oCampoFoco.focus();
						alert("O campo -" + v_oForm.elements[i].TagMensagemObrigatorio + "- � de preenchimento obrigat�rio.");
						return false;
					}
				}

				if (v_oForm.elements[i].type == "select-one") {
					if (Trim(v_oForm.elements[i].value) == m_cValuePadraoLinhaExtraCombo) {
						v_oCampoFoco.focus();
						alert("O campo -" + v_oForm.elements[i].TagMensagemObrigatorio + "- � de preenchimento obrigat�rio.");
						return false;
					}
				}

				if(v_oForm.elements[i].type == "hidden") {
					if (Trim(v_oForm.elements[i].value) == "") {
						v_oCampoFoco.focus();
						alert("O campo -" + v_oForm.elements[i].TagMensagemObrigatorio + "- � de preenchimento obrigat�rio.");
						return false;
					
					}
				}
			}
		}

		return true;
	}
	catch(e) {
		alert("Problemas na verifica��o dos campos obrigat�rios!\nEntre em contato com o administrador do sistema.")
		return false;
	}
}

/* FUNCAO QUE VERIFICA O PREENCHIMENTO OBRIGATORIO DOS CAMPOS QUE ESTAO NO CAMPO hdnCamposObrigatorios
   O value DO CAMPO DEVERA SER PREENCHIDO DA SEGUINTE FORMA: 
   "NomeDoCampo1||Descri��o do Erro1#..#NomeDoCampo2||Descri��o do Erro2" */
function VerificarPreenchimentoObrigatorioCampo() {
	alert("Problemas na verifica��o dos campos obrigat�rios!\nEntre em contato com o administrador do sistema.")
	return false;
}

/* FUNCAO PARA OBRIGAR A SELECAO DE PELO MENOS UM OBJETO
   v_sTipoObjeto = "checkbox" ou "radio"
   v_sMensagem = MENSAGEM QUE SERA EXIBIDA
*/
function VerificarObjetoSelecionado(v_sTipoObjeto, v_sNomeObjeto, v_sMensagem) {
	var v_oForm = document.forms[0];

	for (v_iCount = 0; v_iCount < v_oForm.elements.length; v_iCount++) {
		if (v_oForm.elements[v_iCount].type == v_sTipoObjeto) {
			if (v_sNomeObjeto == null) {
				if(v_oForm.elements[v_iCount].checked == true) return true;
			}
			else {
				if (v_oForm.elements[v_iCount].name == v_sNomeObjeto) {
					if(v_oForm.elements[v_iCount].checked == true) return true;
				}
			}
		}
	}

	alert(v_sMensagem);
	return false;
}
