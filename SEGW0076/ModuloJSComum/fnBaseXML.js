/* FUNCOES PARA TRABALHAR COM XML */

function PreencheComboPadrao(p_oDropDownList, p_sUrlComboSubramo) {
	try {
		var v_oXmlDadosPreenchimento = new ActiveXObject("MSXML2.DOMDocument");
		v_oXmlDadosPreenchimento.async = false;
		v_oXmlDadosPreenchimento.load(p_sUrlComboSubramo);

		p_oDropDownList.options.length = 0;
		
		var v_oOptions = v_oXmlDadosPreenchimento.documentElement.getElementsByTagName("OptionDDL");
		var v_sValue = "";
		var v_sText = "";

		for (var v_iCount = 0; v_iCount < v_oOptions.length; v_iCount++) {
			v_sValue = v_oOptions[v_iCount].selectSingleNode("Value").firstChild.nodeValue;
			v_sText = v_oOptions[v_iCount].selectSingleNode("Text").firstChild.nodeValue;

			p_oDropDownList.options[v_iCount + 1] = new Option(v_sText, v_sValue);
		}

		p_oDropDownList.options[0] = null;

		return true;
	}
	catch(e) {
		alert("Problemas no preenchimento da combo!\nEntre em contato com o administrador do sistema.");
		return false;
	}
}

function PreencheListPadrao(p_oDropDownList, p_sUrlComboSubramo) {
	try {
		var v_oXmlDadosPreenchimento = new ActiveXObject("MSXML2.DOMDocument");
		v_oXmlDadosPreenchimento.async = false;
		v_oXmlDadosPreenchimento.load(p_sUrlComboSubramo);

		p_oDropDownList.options.length = 0;

		var v_oOptions = v_oXmlDadosPreenchimento.documentElement.getElementsByTagName("OptionDDL");
		var v_sValue = "";
		var v_sText = "";
		//alert(v_oOptions.length);
		for (var v_iCount = 0; v_iCount < v_oOptions.length; v_iCount++) {
			v_sValue = v_oOptions[v_iCount].selectSingleNode("Value").firstChild.nodeValue;
			v_sText = v_oOptions[v_iCount].selectSingleNode("Text").firstChild.nodeValue;

			p_oDropDownList.options[v_iCount + 1] = new Option(v_sText, v_sValue);
		}

		p_oDropDownList.options[0] = null;

		return true;
	}
	catch(e) {
		alert("Problemas no preenchimento da combo!\nEntre em contato com o administrador do sistema.");
		return false;
	}
}


if (window.ActiveXObject){
	var ajax = new ActiveXObject("Microsoft.XMLHTTP");
}
else if (window.XMLHttpRequest) {
	var ajax = new XMLHttpRequest();
}

function PreencheListPadraoOld(p_oListBox, p_sUrlListBox) {		

		ajax.open("GET", p_sUrlListBox, true);
		ajax.onreadystatechange = atualizar;
		ajax.send();
}

	
function atualizar(p_sUrlListBox){
	if (ajax.readyState==4){
		var v_oXmlDadosPreenchimento = new ActiveXObject("MSXML2.DOMDocument.3.0");
		v_oXmlDadosPreenchimento.async = false;
		v_oXmlDadosPreenchimento.load(p_sUrlListBox);

		var objNodes = v_oXmlDadosPreenchimento.selectNodes(".");

		if (v_oXmlDadosPreenchimento.parseError.errorCode != 0) {
		    alert("DOM Not Loaded")
		}
		
		alert(ajax.responseText);
		alert(objNodes.length);

		try {	
			p_oListBox.options.length = 0;
			
			var v_oOptions = v_oXmlDadosPreenchimento.documentElement.getElementsByTagName("OptionDDL");
			var v_sValue = "";
			var v_sText = "";
			
			for (var v_iCount = 0; v_iCount < v_oOptions.length; v_iCount++) {
				v_sValue = v_oOptions[v_iCount].selectSingleNode("Value").firstChild.nodeValue;
				v_sText = v_oOptions[v_iCount].selectSingleNode("Text").firstChild.nodeValue;

				p_oListBox.options[v_iCount + 1] = new Option(v_sText, v_sValue);
			}

			p_oListBox.options[0] = null;

			return true;

		}
		catch(e) {
			alert("Problemas no preenchimento da combo!\nEntre em contato com o administrador do sistema.");
			return false;
		}
	}
}