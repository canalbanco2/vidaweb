VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Connection"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Function ConnectionDefault()
  ConnectionDefault = "Provider=SQLOLEDB; Data Source=webdesenv; Initial Catalog=wbra_internet_db; User Id=desenv; Password=desenv"
End Function

Public Function ConnectionSeguros()
  ConnectionSeguros = "Provider=SQLOLEDB; Data Source=webdesenv; Initial Catalog=wbrseguros_db; User Id=desenv; Password=desenv"
End Function

Public Function ConnectionSEGBR()
  ConnectionSEGBR = "Provider=SQLOLEDB; Data Source=aliancasp09; Initial Catalog=webdesenv_seguros_db; User Id=prodinter; Password=schumman"
End Function

