<!--#include virtual = "/padrao.asp"-->
<!--#include virtual = "/funcao/formatar_data.asp"-->
<%
preenchechaves
set result = objCon.mLerCotacao()
%>
<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha" ID="Table1">
	<tr><td colspan="6" class="td_label"><span class="sub_titulo" style="font-size:12px"><b>:: Dados B�sicos da Cota��o</b></span> </td></tr>
	<tr>
		<td class="td_label" nowrap>&nbsp;Inst�ncia</td>
		<td class="td_dado">&nbsp;<%=wf_id%></td>
	</tr>
	<tr>
		<td class="td_label" nowrap>&nbsp;SubRamo</td>
		<td class="td_dado" width="800">&nbsp;<%=CD_ITEM_MDLD%> - <%= testaDadoVazio(result,"nome")%></td>
	</tr>
	<%If testaDadoVazio(result,"CD_CNL_VND_CTC") = 2 Then%>
		<tr>
			<td class="td_label" nowrap>&nbsp;Cota��o/Vers�o</td>
			<td class="td_dado">&nbsp;<%=testaDadoVazio(result,"nr_ctc_sgro_bb")%>/<%=nr_vrs_ctc%></td>
		</tr>
	<%Else%>
	<tr>
		<td class="td_label" nowrap>&nbsp;Cota��o/Vers�o</td>
		<td class="td_dado">&nbsp;<%=nr_ctc_sgro%>/<%=nr_vrs_ctc%></td>
	</tr>
	<%End If%>
	<tr>
		<td class="td_label" nowrap>&nbsp;Ag�ncia</td>
		<td class="td_dado">&nbsp;<%= testaDadoVazio(result,"cd_uor_ctc")%> - <%= testaDadoVazio(result,"nome_agencia")%></td>
	</tr>
	<tr>
		<td class="td_label" nowrap>&nbsp;Proponente</td>
		<td class="td_dado">&nbsp;<%= testaDadoVazio(result,"nm_prpn_ctc")%></td>
	</tr>
	<tr>
		<td class="td_label" nowrap>&nbsp;CPF/CNPJ</td>
		<td class="td_dado">&nbsp;<%= trata_cpf_cnpj(result,"NR_SRF_PRPN_CTC")%></td>
	</tr>
	<tr>
		<td class="td_label" nowrap>&nbsp;Tipo de Opera��o</td>
		<td class="td_dado">&nbsp;<%= testaDadoVazio(result,"CD_TIP_OPR_CTC")%> - <%= mTipoOperacao(result,"CD_TIP_OPR_CTC")%> </td>
	</tr>
	<tr>
		<td class="td_label" nowrap>&nbsp;Canal de Venda</td>
		<td class="td_dado">&nbsp;<%= testaDadoVazio(result,"CD_CNL_VND_CTC")%> - <%= mCanalVenda(result,"CD_CNL_VND_CTC")%> </td>
	</tr>
	<tr>
		<td class="td_label" nowrap>&nbsp;Data de Solicita��o da primeira Cota��o</td>
		<td class="td_dado">&nbsp;<%= testaDadoVazioData(result,"DT_SLCT_CTC")%> </td>
	</tr>
	<%If tp_wf_id <> Application("WF_EDSC_CANCELAMENTO") Then
		if cint("0" & F_operacao) = cint(Application("ENDOSSO")) then
			sTexto = " Endosso"
		else
			sTexto = " Seguro"
		end if
		%>
		<tr>
			<td class="td_label" nowrap>&nbsp;Data de In�cio de Vig�ncia do <%=sTexto%></td>
			<td class="td_dado">&nbsp;<%
			dataI = testaDadoVazioData(result,"DT_INC_VGC_CTC")
			dataF = testaDadoVazioData(result,"DT_FIM_VGC_CTC")

			if trim(dataI) <> "" and trim(dataI) <> "---" then
				dataAtual = formatar_data(now())
				if dataF < dataAtual then
					dataDias = datediff("d",dataF,dataI)
				else
					dataDias = datediff("d",dataAtual,dataI)
				end if
				if dataI < dataAtual then
					Response.Write("<font color=""red""><b>" & dataI & "</b> &nbsp;&nbsp;&nbsp;Cota��o com prazo de vig�ncia retroativo. </font>")
				else
					Response.Write(dataI)
				end if

				if dataDias > 365 then
					Response.Write(" In�cio de vig�ncia do " & sTexto & " posterior a 365 dias.")
				elseif dataDias < -365 then
					Response.Write(" In�cio de vig�ncia do " & sTexto & " anterior a 365 dias.")
				end if
			Else
				Response.Write dataI
			end if
			%> </td>
		</tr>
		<tr>
			<td class="td_label" nowrap>&nbsp;Data de Fim de Vig�ncia do <%=sTexto%></td>
			<td class="td_dado">&nbsp;<%= testaDadoVazioData(result,"DT_FIM_VGC_CTC")%>
			<%If (DataF = DataI) And DataI <> "---" Then Response.Write "&nbsp; <font color=red>A vig�ncia deve ser de ao menos 1 dia.</font>" End If%>
			</td>
		</tr>	
		<tr>
			<td class="td_label" nowrap>&nbsp;Quantidade de dias de validade da Cota��o&nbsp;</td>
			<td class="td_dado">&nbsp;<%= testaDadoVazio(result,"QT_DD_VLD_CTC")%> </td>
		</tr>
		<tr>
			<td class="td_label" nowrap>&nbsp;Ap�lice anterior</td>
			<td class="td_dado">&nbsp;<%= testaDadoVazio(result,"NR_CTR_CTC")%> </td>
		</tr>
		<%if bLiberarOptResseguro then%>
		<tr>
			<td class="td_label" nowrap>&nbsp;Resseguro Facultativo</td>
			<td class="td_dado">
				<%
				
				if isnull(result("Resseguro_Facultativo")) then
					Resseguro_Facultativo = "N"
				else
					Resseguro_Facultativo = result("Resseguro_Facultativo")
				end if
				
				sResseguro_Facultativo0 = ""
				sResseguro_Facultativo1 = ""
				if trim(Resseguro_Facultativo) = "S" then
					sResseguro_Facultativo0 = "checked"
				else
					sResseguro_Facultativo1 = "checked"
				end if
				%>
				<input type="radio" name="rResseguro_Facultativo" value="S" disabled="true" <%=sResseguro_Facultativo0%> onclick="javascript:AtribuirIndicaResseguroHidden();">&nbsp;Sim&nbsp;
				<input type="radio" name="rResseguro_Facultativo" value="N" disabled="true" <%=sResseguro_Facultativo1%> onclick="javascript:AtribuirIndicaResseguroHidden();">&nbsp;N�o
			</td>
		</tr>
		<%end if
	End If%>
</table>