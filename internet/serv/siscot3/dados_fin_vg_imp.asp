	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
				<tr>
					<td colspan="2" class="td_label_negrito">Dados Financeiros</td>
				</tr>
<%
set rsDados = Objcon.mLerDadosFinanceiros(2)
rsDados.ActiveConnection = Nothing
%>
	<tr>
		<td nowrap class="td_label" width="200">&nbsp;Moeda do seguro da cota��o</td>
		<td class="td_dado">&nbsp;<%=testaDadoVazio(rsDados,"CD_MOE_SGRO_CTC")%> - <%=testaDadoVazio(rsDados,"NOME_MOE_SGRO")%></td>
	</tr>												
	<tr>
		<td nowrap class="td_label">&nbsp;Moeda de origem da cota��o</td>
		<td class="td_dado">&nbsp;<%=testaDadoVazio(rsDados,"CD_MOE_OGM_CTC")%> - <%=testaDadoVazio(rsDados,"NOME_MOE_OGM")%></td>
	</tr>												
	<tr>
		<td nowrap class="td_label">&nbsp;Percentual de corretagem</td>
		<td class="td_dado">&nbsp;<%=testaDadoVazioN(rsDados,"PC_CRE_CTC")%></td>
	</tr>				
	<tr>
		<td nowrap class="td_label">&nbsp;Percentual IOF</td>
		<td class="td_dado">&nbsp;<%=testaDadoVazioN(rsDados,"PC_IOF_CTC")%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor IOF</td>
		<td class="td_dado">&nbsp;<%=testaDadoVazioN(rsDados,"VL_IOF_CTC")%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Qtd m�xima de parcelas pgto</td>
		<td class="td_dado">&nbsp;<%=testaDadoVazio(rsDados,"QT_PCL_PGTO_CTC")%></td>
	</tr>
	<%
	if bSubgrupo then
	%>
	<tr>
		<td nowrap class="td_label">&nbsp;Taxa Personalizada por subgrupo</td>
		<td class="td_dado">&nbsp;<%=trata_sim(testaDadoVazio(rsDados,"IN_FAT_UNCO_SGR"))%></td>
	</tr>
	<%
	end if
	
	percentual_corretagem = 0
	percentual_da = 0
	percentual_pro_labore = 0
	percentual_iof = 0
	valor_premio_puro = 0
	valor_premio_liquido = 0

	percentual_corretagem = cdbl("0" & testaDadoVazioN(rsDados,"PC_CRE_CTC"))
	percentual_iof = cdbl("0" & testaDadoVazioN(rsDados,"PC_IOF_CTC"))
	percentual_da = cdbl("0" & testaDadoVazioN(rsDados,"PC_DA_CTC"))
	percentual_pro_labore = cdbl("0" & testaDadoVazioN(rsDados,"PC_ETLE_CTC"))
	valor_premio_puro = cdbl("0" & testaDadoVazioN(rsDados,"VL_PREM_PURO"))
	valor_is = cdbl("0" & testaDadoVazioN(rsDados,"VL_IPTC_MOEN_CTC"))
	valor_premio_liquido = cdbl("0" & testaDadoVazioN(rsDados,"VL_LQDO_MOEN_CTC"))
	per_fat_seguro =  cdbl("0" & testaDadoVazioN(rsDados,"PER_FAT_SEGURO"))

	if valor_premio_liquido > 0 and valor_is > 0 and per_fat_seguro > 0 then
		taxa_comercial = CortaCasasDecimais(((valor_premio_liquido * per_fat_seguro) / valor_is) * 1000, 4)
	else
		taxa_comercial = 0
	end if

	if percentual_pro_labore > 0 then
		valor_pro_labore = valor_premio_puro / (1 - (percentual_da/100)) / (1 - ((percentual_pro_labore/100) + (percentual_corretagem/100)))
		valor_pro_labore = CortaCasasDecimais(valor_pro_labore,0) /1000
	else
		valor_pro_labore = 0
	end if

	valor_premio_puro_total = valor_premio_puro_total + valor_premio_puro
	valor_premio_liquido_total = valor_premio_liquido_total + valor_premio_liquido
	valor_is_total = valor_is_total + valor_is
	%>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Percentual pro-labore</td>
		<td class="td_dado">&nbsp;<%=FormatNumber(percentual_pro_labore, 2)%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor pro-labore</td>
		<td class="td_dado">&nbsp;<%=FormatNumber(valor_pro_labore, 4)%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor pr�mio puro</td>
		<td class="td_dado">&nbsp;<%=FormatNumber(valor_premio_puro, 2)%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Percentual DA</td>
		<td class="td_dado">&nbsp;<%=FormatNumber(percentual_da, 2)%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Tipo de Capital Segurado</td>
		<td class="td_dado">&nbsp;</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor IS</td>
		<td class="td_dado">&nbsp;<%=FormatNumber(valor_is, 2)%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Taxa comercial
		<%
				select case cint(per_fat_seguro)
				case 1
					Response.Write "mensal"
				case 3
					Response.Write "trimestral"
				case 6
					Response.Write "semestral"
				case 12
					Response.Write "anual"
				end select		
		%>
		</td>
		<td class="td_dado">&nbsp;<%=FormatNumber(taxa_comercial, 4)%></td>
	</tr>
<%
set rsDados = Nothing
%>
</table>
</td>
</tr>