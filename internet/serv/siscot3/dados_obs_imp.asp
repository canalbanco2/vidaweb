<%
function NmNegociador(pCodigo)
	If pCodigo = "1" Then
		NmNegociador = "Ag�ncia"
	ElseIf pCodigo = "2" Then
		NmNegociador = "Seguradora"
	End If
End Function

vTextoFormalCompleto = ""
vTextoInformalCompleto = ""

If opcaoPorContrato = "T" Then
    Set oRs = objCon.mLerDadosObservacao(1,True)
Else
    Set oRs = objCon.mLerDadosObservacao(1,False)
End If
oRs.ActiveConnection = Nothing

If Not oRs.EOF Then
	vTextoFormal = ""
	vTextoInformal = ""
	Do While not oRs.EOF
		If oRs("in_tx_fmal_ctc") = "S" Then
			vTextoFormal = vTextoFormal & trim(oRs("tx_lnh_ctc"))
		ElseIf oRs("in_tx_fmal_ctc") = "N" And opcaoPorContrato <> "T" Then
			If vNegociadorInformal <> oRs("cd_tip_negr") Then
				vTextoInformal = vTextoInformal & "<p>" & NmNegociador(oRs("cd_tip_negr"))
				if not isnull(oRs("dt_inclusao")) then
					vTextoInformal = vTextoInformal & " (" & MontaDataCompleta(oRs, "dt_inclusao") & ")"
				end if
				vTextoInformal = vTextoInformal & "<br>--------------------------------------------------" 
								
				vNegociadorInformal = oRs("cd_tip_negr")
			End If
			vTextoInformal =  vTextoInformal & "<br>" & RTrim(oRs("tx_lnh_ctc"))		
		End If
		oRs.MoveNext
	Loop
End if

vTextoFormalCompleto = vTextoFormalCompleto & vTextoFormal
vTextoInformalCompleto = vTextoInformalCompleto & vTextoInformal
%>
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
				<tr>
					<td class="td_label_negrito">Observa��o - Texto Formal</td>
				</tr>
				<tr>
					<td class="td_dado"><p>
						<%
						if trim(vTextoFormalCompleto) <> "" then
							Response.Write replace(vTextoFormalCompleto, vbcrlf, "<br>")
						else
							Response.Write "<br>Nenhuma observa��o relacionada.<br><br>"
						end if
						%>
					</td>
				</tr>
<%If opcaoPorContrato = "" Then%>
				<tr>
					<td class="td_label_negrito">Observa��o - Texto Informal</td>
				</tr>
				<tr>
					<td class="td_dado"><p>
						<%
						if trim(vTextoInformalCompleto) <> "" then
							Response.Write replace(vTextoInformalCompleto, vbcrlf, "<br>")
						else
							Response.Write "<br>Nenhuma observa��o relacionada.<br><br>"
						end if
						%>
					</td>
				</tr>
<%End If%>
			</table>
		</td>
	</tr>