<%
objPerguntas.pQuestionario_id = trim(questionario_id)
Set objDataSet = objPerguntas.mCarrega_questionario_pergunta()
oArvore = mGeraArrayQuestionario(objDataSet)
ordem = CalcularOrdem(oArvore)

intIndice = 0
%>
<br>
<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
<%
Do While ordem(intIndice) <> ""
identacao = replace(oArvore(intIndice).tabs,"|","&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")
resposta = Respostas(oArvore(intIndice).pergunta_id)

if resposta <> "" then
%>
	<tr>
		<td class="td_dado">
			<% = identacao & oArvore(intIndice).ordem_resposta & ") " & oArvore(intIndice).nome_pergunta%>
		</td>
	</tr>
	<tr>
		<td class="td_dado"><i><font color="red">
			<% = identacao & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & resposta %>
		</font></i></td>
	</tr>

<%
	end if
	intIndice = ordem(intIndice)
Loop
%>
</table>
