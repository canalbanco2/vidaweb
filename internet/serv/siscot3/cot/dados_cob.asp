<!--#include virtual = "/padrao.asp"-->

<script>
	var taxaNet = "";

	function rValorFocus(valor){
		taxaNet = (valor.replace('.','')).replace(',','.');
	}
	//	function rValorBlur(){
	//	if(taxaNet!=document.formProc.pc_prem_cbt_ctc.value){
	//		recalculaPremioNet();
	//	}
	//}
	//function recalculaPremioNet(){
	//	vl_iptc_cbt_ctc = document.formProc.vl_iptc_cbt_ctc.value;
	//	pc_prem_cbt_ctc = document.formProc.pc_prem_cbt_ctc.value;
	//	if((vl_iptc_cbt_ctc!="")&&(pc_prem_cbt_ctc!="")){
	//		vl_iptc_cbt_ctc = converteNumeroJavascript(vl_iptc_cbt_ctc) * 10000;
	//		pc_prem_cbt_ctc = converteNumeroJavascript(pc_prem_cbt_ctc) * 10000;
	//		vl_prem_cbt_ctc = (vl_iptc_cbt_ctc * pc_prem_cbt_ctc)/10000000000;
	//		vl_prem_cbt_ctc = converteNumeroJavascriptTruncado(vl_prem_cbt_ctc, 2);
	//		vl_prem_cbt_ctc = FormatNumber(vl_prem_cbt_ctc.replace('.',','), 2, 3);
	//		pc_prem_cbt_ctc = document.formProc.pc_prem_cbt_ctc.value;
	//		pc_prem_cbt_ctc = FormatNumber(pc_prem_cbt_ctc.replace('.',','), 4);
	//		document.formProc.pc_prem_cbt_ctc.value = pc_prem_cbt_ctc;
	//		document.formProc.vl_prem_cbt_ctc.value = vl_prem_cbt_ctc;
	//	}
	//}
   	function converteNumeroJavascriptTruncado(valor,casas){
		valor = valor + '';
		valor = valor.split('.');
		valorNovo = '';
		for(f=0;f<valor[0].length;f++){
			if((valor[0].charCodeAt(f)>=48)&&(valor[0].charCodeAt(f)<=57)){
				valorNovo=valorNovo+valor[0].charAt(f);
			}
		}
		valorNovo = valorNovo + '.';

		for(f=0;f<casas;f++){
			if(valor.length>1){
				if(valor[1].length>f){
					valorNovo = valorNovo + valor[1].charAt(f);
				} else {
					valorNovo = valorNovo + '0';
				}
			} else {
				valorNovo = valorNovo + '0';
			}
		}
		return valorNovo;
	}
	function converteNumeroJavascript(valor){
		valor = valor.split(',');
		valorNovo = '';
		for(f=0;f<valor[0].length;f++){
			if((valor[0].charCodeAt(f)>=48)&&(valor[0].charCodeAt(f)<=57)){
				valorNovo=valorNovo+valor[0].charAt(f);
			}
		}
		if(valor.length>1){
			valorNovo = valorNovo + '.' + valor[1];
		}
		return valorNovo;
	}
	function preencheCampos(numero){
		try{document.formProc.R[numero].checked = true;}
		catch(ex){document.formProc.R.checked=true;}

		document.formProc.numero.value = numero;
  	    document.formProc.numero_selecao.value = numero;
		document.formProc.CD_CBT_ALT.value = document.formProc["CD_CBT" + numero].value;
		document.formProc.DT_INC_VCL_ALT.value = document.formProc["DT_INC_VCL" + numero].value;
		document.formProc.nome.value = document.formProc["nome" + numero].value;
		document.formProc.vl_iptc_cbt_ctc.value = document.formProc["vl_iptc_cbt_ctc" + numero].value;
		aFranquia = franquias[numero].split('|');
		apagaOptions(document.formProc.tx_rgr_frqu);
		document.formProc.tx_rgr_frqu_edicao.value = '';
		
		//colocaOption(document.formProc.tx_rgr_frqu,'--Escolha Abaixo--');
		for(f=0;f<aFranquia.length;f++)
		{
			colocaOption(document.formProc.tx_rgr_frqu,aFranquia[f]);
			
			if (f == 0)
				document.formProc.tx_rgr_frqu_edicao.value = aFranquia[f];
		}

		selecionaOption(document.formProc.tx_rgr_frqu,document.formProc["tx_rgr_frqu" + numero].value);

		if (document.formProc["tx_rgr_frqu" + numero].value != '')
			document.formProc.tx_rgr_frqu_edicao.value = document.formProc["tx_rgr_frqu" + numero].value;
		document.formProc.tx_rgr_frqu_edicao.focus();

		document.formProc.pc_prem_cbt_ctc.value = document.formProc["pc_prem_cbt_ctc" + numero].value;
		document.formProc.vl_prem_cbt_ctc.value = document.formProc["vl_prem_cbt_ctc" + numero].value;
  	    document.formProc.numero_atual.value = numero;
		
		if (document.formProc["LIMITE_CAPITAL" + numero].value == "-1")
			document.all.aviso.innerHTML = 'O Valor IS � menor que o m�nimo cadastrado';
		else if (document.formProc["LIMITE_CAPITAL" + numero].value == "-1")
			document.all.aviso.innerHTML = 'O Valor IS � maior que o m�ximo cadastrado';
		else
			document.all.aviso.innerHTML = '';
	}
	function selecionaOption(obj,valor){
		for(f=0;f<obj.length;f++){
			if(obj[f].value == valor){
				obj[f].selected = true;
			}
		}
	}
	function colocaOption(obj,valor){
		option = new Option('value','text');
		option.value = valor;
		option.text = valor;
		obj[obj.length] = option;
	}
	function apagaOptions(obj){
		while(obj.length>0){
			obj[0] = null;
		}
	}
	function calculaNET(tipo)
	{
		var valor_is = parseFloat('0' + converteNumeroJavascript(document.formProc.vl_iptc_cbt_ctc.value));
	
		if (tipo == 'T') //calcula a taxa
		{
			var premio_net = parseFloat('0' + converteNumeroJavascript(document.formProc.vl_prem_cbt_ctc.value));
			if (parseFloat(taxaNet)!=parseFloat(premio_net)){
 			    var taxa_net = converteNumeroJavascriptTruncado(((premio_net / valor_is) * 100), 5);
			    document.formProc.pc_prem_cbt_ctc.value = FormatNumber(taxa_net.toString().replace(".", ","), 5);
				document.formProc.salvarcob.value = 'S';
			}
		}
		else if (tipo == 'P') //calcula o premio
		{
			var taxa_net = parseFloat('0' + converteNumeroJavascript(document.formProc.pc_prem_cbt_ctc.value));
			var premio_net = converteNumeroJavascriptTruncado(((taxa_net * valor_is) / 100), 2);
			document.formProc.vl_prem_cbt_ctc.value = FormatNumber(premio_net.toString().replace(".", ","), 2);
			document.formProc.salvarcob.value = 'S';
		}
	}
</script>
<br>
<%if arrParametro(9) <> Application("WF_HAB") then%>
&nbsp;&nbsp;&nbsp;
<!--input type="button" name="alterar" value="Gravar" style="width:80px;font-size:10px;margin: 2px 2px 2px 2px;" onclick="alterarDados()"-->
<%end if%>
<input type="hidden" name="salvarcob" value="N">
<input type="hidden" name="numero_selecao" value="">
<input type="hidden" name="numero_atual" value="">

<br><br>
<%
strError = ""

iContador = 0
Set rs = objCon.mLerSubgrupoCotacao
rs.ActiveConnection = Nothing

If Trim(Request("salvarcob")) = "S" and Trim(Request("NR_SEQL_CBT_CTC")) <> "" and Trim(Request("CD_CBT_ALT")) <> "" Then
	set rsCotacao = objCon.mLerCotacao
	rsCotacao.ActiveConnection = Nothing

'	If UCase(rsCotacao("CD_ULT_EVT_CTC")) = "T" Then
		bAtualizaCotacao = False
'	Else
'        objcon.pCD_ULT_EVT_CTC = "T"
'		bAtualizaCotacao = True
'	End If

	If Trim(request("NM_PRPN_CTC")) = "" Then
		NR_CTC_SGRO_subgrupo = NR_CTC_SGRO
		NR_VRS_CTC_subgrupo  = NR_VRS_CTC
	Else
		arrSubGrupoCotacao   = split(trim(request("NM_PRPN_CTC")),"|")
		NR_CTC_SGRO_subgrupo = arrSubGrupoCotacao(0)
		NR_VRS_CTC_subgrupo  = arrSubGrupoCotacao(1)
	End If

	objCon.pCD_CBT          = Trim(request("CD_CBT_ALT"))
	objCon.pNR_SEQL_CBT_CTC = Trim(request("NR_SEQL_CBT_CTC"))
	objCon.pDT_INC_VCL      = Trim(request("DT_INC_VCL_ALT"))
	objCon.ptx_rgr_frqu     = Trim(request("tx_rgr_frqu_edicao"))
	objCon.ppc_prem_cbt_ctc = Trim(request("pc_prem_cbt_ctc"))
	objCon.pvl_prem_cbt_ctc = Trim(request("vl_prem_cbt_ctc"))

	objCon.pWF_ID = WF_ID
	
	'response.Write "CD_CBT" & objCon.pCD_CBT
	'response.Write "pNR_SEQL_CBT_CTC" & objCon.pNR_SEQL_CBT_CTC
	'response.Write "pDT_INC_VCL" & objCon.pDT_INC_VCL
	'response.Write "ptx_rgr_frqu" & objCon.ptx_rgr_frqu
	'response.Write "ppc_prem_cbt_ctc" & objCon.ppc_prem_cbt_ctc
	'response.Write "pvl_prem_cbt_ctc" & objCon.pvl_prem_cbt_ctc
	'response.Write "NR_CTC_SGRO_subgrupo" & NR_CTC_SGRO_subgrupo
	'response.Write "NR_VRS_CTC_subgrupo" & NR_VRS_CTC_subgrupo
	'response.Write " bAtualizaCotacao" & bAtualizaCotacao
	'response.End
	
	retorno = objCon.mAtualizarCobertura(NR_CTC_SGRO_subgrupo, NR_VRS_CTC_subgrupo, bAtualizaCotacao)

	if trim(retorno) <> "" then
		Response.redirect("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
		Response.End 
	end if

	chave
End If
%>
<script>
function processasubgrupo()
{
	try { document.formProc.NR_SEQL_CBT_CTC.selectedIndex = 0; } catch (ex) { }
	submitcobertura();
}

function alterarDados()
{
	if (validaform())
	{
		document.formProc.salvarcob.value = 'S';
		submitcobertura();
	}
}

function validaform()
{
	if (document.formProc.numero_selecao.value != '')
	{
		if (Trim(document.formProc.tx_rgr_frqu_edicao.value) == '')
		{
			document.all.msgerro.innerHTML = '� necess�rio escolher uma franquia<br><br>';
			return false;
		}
	}
	return true;
}

function submitcobertura(){
	document.formProc.action = 'liberar_cotacao_cob.asp';
    document.formProc.cursor = 'wait';
	document.formProc.submit();
}

function alterasequencial()
{
	document.formProc.numero_selecao.value = '';
	submitcobertura();
}
function selecionarcobertura(numero)
{   
	if (document.formProc.numero_selecao.value=='')
	{   
		document.formProc.numero_selecao.value = numero;
        submitcobertura();
	}
    else
	{
		document.formProc.numero_selecao.value = numero;
		alterarDados();
	}
}
</script>
<table cellspacing="0" border="0" width="826" class="escolha" height="10">
	<tr>
		<td colspan="2" class="td_dado" width="824"><font color="red"><span id="msgerro"></span></font></td>
	</tr>
<%
If rs.EOF Then
	bSubgrupo = False
Else
	bSubgrupo = True
%>
	<tr>
		<td class="td_label" width="150">
			Subgrupo
		</td>
		<td class="td_dado" width="672">&nbsp;
			<select name="NM_PRPN_CTC" onchange="processasubgrupo();" ID="Select1">
			<option value="">--Escolha abaixo--</option>
			<%
			Do while not rs.EOF
				if trim(request("NM_PRPN_CTC")) = trim(rs("NR_CTC_SGRO")) & "|" & trim(rs("NR_VRS_CTC")) then
					sSelecionado = " selected"
				else
					sSelecionado = ""
				end if
				%>
				<option value="<%=Trim(rs("NR_CTC_SGRO")) & "|" & trim(rs("NR_VRS_CTC"))%>"<%=sSelecionado%>><%=right(00 & rs("subgrupo_id"),2) & " (" & trim(rs("NR_CTC_SGRO_BB")) & ")"%> - <%=trim(rs("NM_PRPN_CTC"))%></option>
				<%
				rs.MoveNext
			Loop
			%>
			</select>
		</td>
	</tr>
<%
	If trim(request("NM_PRPN_CTC")) <> "" then 
		arrSubGrupoCotacao = split(trim(request("NM_PRPN_CTC")),"|")

		objCon.pNR_CTC_SGRO = arrSubGrupoCotacao(0)
		objCon.pNR_VRS_CTC = arrSubGrupoCotacao(1)
		NR_CTC_SGRO = objCon.pNR_CTC_SGRO
		NR_VRS_CTC = objCon.pNR_VRS_CTC
	End If
End If

If not bSubgrupo or trim(request("NM_PRPN_CTC")) <> "" Then
	set rsSeqlCobertura = objcon.mLerSeqlCobertura()
	rsSeqlCobertura.ActiveConnection = Nothing
%>
	<tr>
		<td class="td_label" width="150">
			Objeto do Risco
		</td>
		<td class="td_dado" width="672">&nbsp;
			<select name="NR_SEQL_CBT_CTC" onchange="alterasequencial();"<%If vLMI Then Response.Write " style='width: 100px;'" End If%> ID="Select2">
				<option value="">--Escolha abaixo--</option>
				<%ArrayJS=""
				Do while not rsSeqlCobertura.eof%>
					<option value="<% = rsSeqlCobertura("NR_SEQL_CBT_CTC") %>"<%if trim(request("NR_SEQL_CBT_CTC")) = trim(rsSeqlCobertura("NR_SEQL_CBT_CTC")) then Response.Write " selected " End if%>>
						<%If CInt(rsSeqlCobertura("NR_SEQL_CBT_CTC")) = 9999 Then
						     Response.Write "LMI"
						  Else
						     Response.Write rsSeqlCobertura("NR_SEQL_CBT_CTC")
						  End If%></option>
					<%ArrayJS = ArrayJS & "," & rsSeqlCobertura("NR_SEQL_CBT_CTC")
					rsSeqlCobertura.movenext
				Loop
				ArrayJS = right(ArrayJS,len(ArrayJS)-1)%>
			</select>
		</td>
			<script>
				if(document.formProc.NR_SEQL_CBT_CTC.length==2&&document.formProc.NR_SEQL_CBT_CTC.selectedIndex!=1){document.formProc.NR_SEQL_CBT_CTC.selectedIndex=1;document.formProc.action='';document.formProc.submit();}
            </script>
	</tr>
</table>
<script>
	var franquias = new Array();
	var valoresCobertura = new Array(<% = ArrayJS %>);
	function apagaCelulas(){
		for(x=0;x<document.formProc.contador.value;x++) {
			for(f=0;f<valoresCobertura.length;f++){
				if(document.getElementById('tr' +  valoresCobertura[f] + '_' + x)!=null) {
					document.getElementById('tr' +  valoresCobertura[f] + '_' + x).style.display = 'none';
				}
			}
		}
		for(x=0;x<document.formProc.contador.value;x++) {
			document.getElementById('questionario' + x).style.display = 'none';
			eval('document.forms[0].exibe' + x + '.value = "N"');
		}
	}
	function acendeCelulas(ID){
		apagaCelulas();
		if(ID!=""){
			for(x=0;x<document.formProc.contador.value;x++) {
				if(document.getElementById('tr' + ID + '_' + x)!=null) {
					document.getElementById('tr' + ID + '_' + x).style.display = '';
					eval('document.forms[0].exibe' + x + '.value = "S"');
				}
			}
		}
	}
    
    function modificado(){
        if (document.getElementById('salvarAofinalizar'))  
        {
            document.getElementById('salvarAofinalizar').value = 'N';
        }
        if(document.getElementById('__liberar_cotacao_fin___movimentacao_realizada'))
        {
            document.getElementById('__liberar_cotacao_fin___movimentacao_realizada').value = 'false';
        }

        if(document.getElementById('__liberar_cotacao_fin___salvarAofinalizar'))
        {
            document.getElementById('__liberar_cotacao_fin___salvarAofinalizar').value="N"
        }
        
            //alert('Alterado objeto');

        
    }
</script>
<%
 If Trim(Request("NR_SEQL_CBT_CTC")) <> "" Then
	objCon.pNR_SEQL_CBT_CTC = Trim(Request("NR_SEQL_CBT_CTC"))
	Set rsCobertura = objcon.mLerDadosCobertura()
	If NOT rsCobertura.EOF Then
		
		valor_is_total = 0
		valor_premio_net_total = 0
%>
<table cellpadding="3" cellspacing="0" border="1" width="827" class="escolha" bordercolor="#EEEEEE" ID="Table1">
	<tr>
		<td class="td_label" width="20"></td>
		<td class="td_label" width="30"><b>C�d. Cob</b></td>
		<td class="td_label" width="260"><b>Cobertura</b></td>
		<td class="td_label" width="73"><b>Valor IS</b></td>
		<td class="td_label" width="185"><b>Franquia</b></td>
		<%if arrParametro(9) <> Application("WF_HAB") then%>
			<td class="td_label" width="70"><b>Taxa NET</b></td>
			<td class="td_label" width="82"><b>Pr�mio NET</b></td-->
		<%end if%>
	</tr>
</table>



<DIV style="OVERFLOW: scroll; WIDTH: 100%; HEIGHT: 254px">
<table cellpadding="3" cellspacing="0" border="1" width="827" class="escolha" bordercolor="#EEEEEE" ID="Table3">
	<%
	   Do while not rsCobertura.EOF
  	      Redim Preserve arCoberturas(iContador)
	      arCoberturas(iContador) = testaDadoVazioN(rsCobertura,"CD_CBT")
		  alterna_cor
	  	  pc_prem_cbt_ctc = testaDadoVazioFormataN(rsCobertura,"pc_prem_cbt_ctc",5)
		  vl_prem_cbt_ctc = testaDadoVazioFormataN(rsCobertura,"vl_prem_cbt_ctc",2)

		  if testaDadoVazioFormataN(rsCobertura,"vl_iptc_cbt_ctc",2) <> "" and not isnull(rsCobertura("ACUMULA_IS")) then
		 	 if ucase(trim(rsCobertura("ACUMULA_IS"))) = "S" then
				valor_is_total = cdbl(valor_is_total) + cdbl(testaDadoVazioFormataN(rsCobertura,"vl_iptc_cbt_ctc",2))
			 end if
		  end if
		  if trim(vl_prem_cbt_ctc) <> "" then
			 valor_premio_net_total = cdbl(valor_premio_net_total) + cdbl(vl_prem_cbt_ctc)
		  end if
		%>
		<tr id="tr<%=testaDadoVazioN(rsCobertura,"NR_SEQL_CBT_CTC")%>_<%=iContador%>">
			<td width="20" class="td_dado" style="<%=cor_zebra%>">
				<%if arrParametro(9) <> Application("WF_HAB") then%>
					<input type="radio" name="R" onClick="selecionarcobertura(<%= iContador%>);" style="cursor:hand;" ID="Radio1" VALUE="Radio1">
				<%end if%>
				&nbsp;
			</td>		
			<td width="30" class="td_dado" style="<%=cor_zebra%>">
				<input type="hidden" name="CD_CBT<%= iContador%>" value="<% = arCoberturas(iContador)%>" ID="Hidden1">
				&nbsp;<%= testaDadoVazioN(rsCobertura,"CD_CBT")%>
			</td>
			<td width="260" class="td_dado" style="<%=cor_zebra%>">
				<input type="hidden" name="nome<%= iContador%>" value="<%= testaDadoVazioN(rsCobertura,"nome")%>" ID="Hidden2">
				<input type="hidden" name="NR_SEQL_CBT_CTC<%= iContador%>" value="<%= testaDadoVazioN(rsCobertura,"NR_SEQL_CBT_CTC")%>" ID="Hidden3">
				<input type="hidden" name="DT_INC_VCL<%= iContador%>" value="<%= testaDadoVazioN(rsCobertura,"DT_INC_VCL")%>" ID="Hidden4">
				&nbsp;<%= testaDadoVazioN(rsCobertura,"nome")%>
			</td>
			<td width="73" class="td_dado" style="<%=cor_zebra%>">
				<input type="hidden" name="vl_iptc_cbt_ctc<%= iContador%>" value="<%= testaDadoVazioFormataN(rsCobertura,"vl_iptc_cbt_ctc",2)%>" ID="Hidden5">
				&nbsp;<%= testaDadoVazioFormataN(rsCobertura,"vl_iptc_cbt_ctc",2)%>
				<input type="hidden" name="pc_prem_cbt_ctc<%= iContador%>" value="<%= pc_prem_cbt_ctc %>" ID="Hidden6">
				<input type="hidden" name="vl_prem_cbt_ctc<%= iContador%>" value="<%= vl_prem_cbt_ctc %>" ID="Hidden7">
				<input type="hidden" name="LIMITE_CAPITAL<%= iContador%>" value="<%=testaDadoVazioN(rsCobertura,"LIMITE_CAPITAL") %>" ID="Hidden8">
			</td>			
			<td width="186" class="td_dado" style="<%=cor_zebra%>">
			<%
			  objCon.pCD_CBT = rsCobertura("CD_CBT")
			  Set rsFranquia = objcon.mLerDadosFranquia
			  rsFranquia.ActiveConnection = Nothing
			  tx_rgr_frquFim = ""
			  tx_rgr_frquTotal = ""
			  If NOT rsFranquia.EOF Then
				 Do while NOT rsFranquia.EOF
					tx_rgr_frqu = (trim(rsFranquia("tx_rgr_frqu")))
					tx_rgr_frquTotal = tx_rgr_frquTotal & "|" & tx_rgr_frqu
					if tx_rgr_frquAux = "" then
						if ucase(trim(rsFranquia("in_frqu_pdro"))) = "S" then
							tx_rgr_frquAux = tx_rgr_frqu
						end if
					end if
					if tx_rgr_frquAux = tx_rgr_frqu then
						tx_rgr_frquFim = tx_rgr_frqu
					end if
					rsFranquia.movenext
				 Loop
				 tx_rgr_frquTotal = right(tx_rgr_frquTotal,len(tx_rgr_frquTotal)-1)
				 If tx_rgr_frquFim = "" then
					rsFranquia.movefirst
					tx_rgr_frquFim = server.HTMLEncode(trim(rsFranquia("tx_rgr_frqu")))
				 End If
				%>
				<input type="hidden" name="tx_rgr_frqu<%= iContador%>" value="<%= testaDadoVazioN(rsCobertura,"TX_FRQU_CTC")%>" ID="Hidden9">
				<span align="right" id="ltx_rgr_frqu<%= iContador%>"><%= testaDadoVazio(rsCobertura,"TX_FRQU_CTC")%></span>
			<%else%>
				<input type="hidden" name="tx_rgr_frqu<%= iContador%>" value="" ID="Hidden10">
				<span align="right" id="Span1">-- N�o h� franquias --</span>
			<%end if%>
				<script>
					franquias[<%= iContador%>] = '<% = tx_rgr_frquTotal %>';
				</script>
			</td>
			<%if arrParametro(9) <> Application("WF_HAB") then%>
				<td width="70" class="td_dado" style="<%=cor_zebra%>">
					<div align="right" id="lpc_prem_cbt_ctc<%= iContador%>"><%= pc_prem_cbt_ctc %></div>
				</td>
				<td class="td_dado" style="<%=cor_zebra%>" width="81">
					<div align="right" id="lvl_prem_cbt_ctc<%= iContador%>"><%= vl_prem_cbt_ctc %></div>
				</td-->
			<%end if%>
		</tr>
		<input type="hidden" value="" name="exibe<%=iContador%>" ID="Hidden11">
		<%
		   iContador = iContador + 1
		   rsCobertura.movenext
	    Loop%>
	</table>
	<br>
	<table cellpadding="3" cellspacing="0" border="1" width="827" class="escolha" bordercolor="#EEEEEE" ID="Table2">
		<tr>
			<td class="td_label_negrito" width="150">Valor IS Total:</td>
			<td class="td_dado" width="659"><%=FormatNumber(valor_is_total,2)%>&nbsp;</td>
		</tr>
		<%if arrParametro(9) <> Application("WF_HAB") then%>
		<tr>
			<td class="td_label_negrito" width="150">Pr�mio NET Total:</td>
			<td class="td_dado" width="659"><%=FormatNumber(valor_premio_net_total,2)%>&nbsp;</td>
		</tr>
		<%end if%>
	</table>
	</div>
	<br>
	<input type="hidden" name="numero" ID="Hidden12">
	<input type="hidden" name="contador" value="<% = iContador %>" ID="Hidden13">
	<%if arrParametro(9) <> Application("WF_HAB") then%>
		<table cellpadding="1" cellspacing="4" border="0" width="828" class="escolha" ID="Table4">
			<tr>
				<td class="td_label" width="150">&nbsp;Cobertura
				<input type="hidden" name="CD_CBT_ALT" ID="Hidden14">
				<input type="hidden" value="" name="DT_INC_VCL_ALT" ID="Hidden15">
				</td>
				<td class="td_dado" width="662" >&nbsp;<input type="text" name="nome" readonly style="background-color:#EEEEEE;" size="80" ID="Text1"></td>
			</tr>
			<tr>
				<td class="td_label" width="150">&nbsp;Valor IS</td>
				<td class="td_dado" width="662">&nbsp;<input type="text" name="vl_iptc_cbt_ctc" readonly style="background-color:#EEEEEE;text-align:right;" size="19" ID="Text2">
				&nbsp;&nbsp;&nbsp;<font color="red"><span id="aviso"></span></font>
				</td>
			</tr>
			<tr>
				<td class="td_label" width="150">&nbsp;Franquia</td>
				<td class="td_dado" width="662" >&nbsp;<select name="tx_rgr_frqu" style="width:250px" onChange="if (this.selectedIndex > 0) document.formProc.tx_rgr_frqu_edicao.value = this.value; else document.formProc.tx_rgr_frqu_edicao.value = '';" ID="Select3">
				</select>&nbsp;<input type="text" name="tx_rgr_frqu_edicao" size="38" maxlength="60" ID="Text3"></td>
			</tr>
			<tr>
				<td class="td_label" width="150">&nbsp;Taxa NET</td>
				<td class="td_dado" width="662" >&nbsp;<input type="text" name="pc_prem_cbt_ctc" onfocus="rValorFocus(this.value);" onKeyUp="FormatValorContinuo(this,5);" onBlur="calculaNET('P');  modificado();" style="text-align:right;" size="9" maxlength="9" ID="Text4"></td>
			</tr>
			<tr>

        <!--'Inicio - Wander - 17797882 - HABILITA��O DE CAMPOS NO FLOW - SISTEMA DE COTA��ES-->
        <% Set rs = objcon.mLerDadosFinanceiros(1) '1 = RE  %>
        <%rs.ActiveConnection = Nothing                     %>
        <%If NOT rs.EOF Then                                %>
           <%If Trim(request("salvarfin")) = "S" Then       %>
		        <% pCD_MVTC_FNCR_PREM = request("CD_MVTC_FNCR_PREM")             %>
	        <%Else                                          %>
        <%pCD_MVTC_FNCR_PREM = testaDadoVazioN(rs,"CD_MVTC_FNCR_PREM")         %>
        <%	End If                                          %>
        <%end if                                            %>


        <%  If trim(pCD_MVTC_FNCR_PREM) <> "1" Then%>
	        <td class="td_label" width="150">&nbsp;Pr�mio NET</td>
	        <td class="td_dado" width="662">&nbsp;<input type="text" name="vl_prem_cbt_ctc" onfocus="rValorFocus(this.value);"  onKeyUp="FormatValorContinuo(this,2);" onBlur="calculaNET('T'); modificado();"  style="text-align:right;" size="19" maxlength="19" ID="Text5"></td>		
        <% else %>
            <td class="td_label" width="150">&nbsp;Pr�mio NET</td>
	        <td class="td_dado" width="662">&nbsp;<input type="text" name="vl_prem_cbt_ctc" onfocus="rValorFocus(this.value);" readonly style="background-color:#EEEEEE;text-align:right;"  " onKeyUp="FormatValorContinuo(this,2);" onBlur="calculaNET('T');"  size="19" maxlength="19" ID="Text6"></td>		
        <% end if%>
        <!--'Fim - Wander - 17797882 - HABILITA��O DE CAMPOS NO FLOW - SISTEMA DE COTA��ES-->

			</tr>
		</table>
		<br>
	<%end if%>
	<!--#include virtual = "/internet/serv/siscot3/funcoes_dados_qst.asp"-->
	<%	
 	   If Request("numero_selecao") <> "" Then
	      htmlExibir = ""
	      rsCobertura.movefirst
	      objcon.pCD_CBT = Request("CD_CBT"&Request("numero_selecao"))
	      objcon.pCD_TIP_QSTN = "3"

	      Set rsQST = objcon.mLerQuestionarioId()
	      rsQST.ActiveConnection = Nothing
		%><table border="0" id="questionario<% = contador %>"><tr><td><%

	      If NOT rsQST.EOF Then
			 questionario_id = rsQST("NR_QSTN")
		 	 objcon.pNR_QSTN = questionario_id

			 objCon.pNR_SEQL_QSTN_CTC = Request("NR_SEQL_CBT_CTC")
			 Set rsNomeQSTN = objcon.mLerLocalRisco()
			 rsNomeQSTN.ActiveConnection = Nothing		
			 If NOT rsNomeQSTN.EOF Then

				If Trim(rsNomeQSTN("NR_CD_TIP_QSTN_CTC")) <> "0" And Trim(rsNomeQSTN("NR_CD_TIP_QSTN_CTC")) <> "" Then
					htmlExibir = "<font face=""arial"" size=""2""><b>" & rsNomeQSTN("nome") & "</b></font>"
					%>
					<!--#include virtual = "/internet/serv/siscot3/funcoes_dados_qst.asp"-->
					<%
				End If
			 End If
	      End If%>
		</td></tr></table>
<%     End If

	   If Trim(Request("numero_selecao")) <> "" and Trim(strError) = "" Then%>
	<script>preencheCampos(<%=Trim(Request("numero_selecao"))%>);</script>
     <%ElseIf Trim(strError) <> "" then%>
	<script>preencheCampos(<%=Trim(Request("numero_atual"))%>);</script>
     <%End If
     
   Else
   %>
		<tr><td class="td_label">&nbsp;Nenhuma cobertura relacionada.</td></tr>
	</table>
<% End If %>
</div>
<%
 End If
End If%>