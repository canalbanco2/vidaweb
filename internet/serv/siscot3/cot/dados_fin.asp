<!--#include virtual = "/padrao.asp"-->
<%

Set rs = objCon.mLerDadosFinanceiros(1) '1 = RE

%>
<!--p>METODO ERRO: <% = objCon.pMetodoErro %></p><br>
<p>NUMERO ERRO: <% = objCon.pNumeroErro %></p><br>
<p>ERRO: <% = objCon.pErro %></p><br>
<p>SQL ERRO: <% = objCon.pSQLERRO %></p><br-->
<%
rs.ActiveConnection = Nothing
If NOT rs.EOF Then
    If Request("primeiro") = "false" Then
 	   VL_IPTC_MOEN_CTC = Request("VL_IPTC_MOEN_CTC")
	   VL_PREM_MOEN_CTC = Request("VL_PREM_MOEN_CTC")
	   VL_PRMO_MOEN_CTC = Request("VL_PRMO_MOEN_CTC")
	   VL_SGRA_MOEN_CTC = Request("VL_SGRA_MOEN_CTC")
	   VL_PCL_MOEN_CTC  = Request("VL_PCL_MOEN_CTC")
	   VL_LQDO_MOEN_CTC = Request("VL_LQDO_MOEN_CTC")
	   VL_IPTC_MOEE_CTC = Request("VL_IPTC_MOEE_CTC")
	   VL_PREM_MOEE_CTC = Request("VL_PREM_MOEE_CTC")
	   VL_PRMO_MOEE_CTC = Request("VL_PRMO_MOEE_CTC")
	   VL_SGRA_MOEE_CTC = Request("VL_SGRA_MOEE_CTC")
	   VL_PCL_MOEE_CTC  = Request("VL_PCL_MOEE_CTC")
	   VL_LQDO_MOEE_CTC = Request("VL_LQDO_MOEE_CTC")
	   Margem_Comercial = Request("Margem_Comercial")
	   QT_PCL_PGTO_CTC  = Request("QT_PCL_PGTO_CTC")
	   VL_CST_APLC_CTC  = Request("VL_CST_APLC_CTC")
	   PC_DSC_TCN_CTC   = Request("PC_DSC_TCN_CTC")
	   VL_DSC_TCN_CTC   = Request("VL_DSC_TCN_CTC")
	   PC_DSC_CML_CTC   = Request("PC_DSC_CML_CTC")
	   VL_DSC_CML_CTC   = Request("VL_DSC_CML_CTC")
	   VL_IOF_CTC       = Request("VL_IOF_CTC")
	Else
	   CD_TIP_OPR_CTC   = testaDadoVazioN(rs,"CD_TIP_OPR_CTC")
	   VL_IPTC_MOEN_CTC = testaDadoVazioN(rs,"VL_IPTC_MOEN_CTC")
	   VL_PREM_MOEN_CTC = testaDadoVazioN(rs,"VL_PREM_MOEN_CTC")
	   VL_PRMO_MOEN_CTC = testaDadoVazioN(rs,"VL_PRMO_MOEN_CTC")
	   VL_SGRA_MOEN_CTC = testaDadoVazioN(rs,"VL_SGRA_MOEN_CTC")
	   VL_PCL_MOEN_CTC  = testaDadoVazioN(rs,"VL_PCL_MOEN_CTC")
	   VL_LQDO_MOEN_CTC = testaDadoVazioN(rs,"VL_LQDO_MOEN_CTC")
	   If VL_LQDO_MOEN_CTC = "" Then VL_LQDO_MOEN_CTC = 0 End If
	   VL_IPTC_MOEE_CTC = testaDadoVazioN(rs,"VL_IPTC_MOEE_CTC")
	   VL_PREM_MOEE_CTC = testaDadoVazioN(rs,"VL_PREM_MOEE_CTC")
	   VL_PRMO_MOEE_CTC = testaDadoVazioN(rs,"VL_PRMO_MOEE_CTC")
	   VL_SGRA_MOEE_CTC = testaDadoVazioN(rs,"VL_SGRA_MOEE_CTC")
	   VL_PCL_MOEE_CTC  = testaDadoVazioN(rs,"VL_PCL_MOEE_CTC")
	   VL_LQDO_MOEE_CTC = testaDadoVazioN(rs,"VL_LQDO_MOEE_CTC")
	   VL_CST_APLC_CTC  = testaDadoVazioN(rs,"VL_CST_APLC_CTC")
	   VL_IOF_CTC       = testaDadoVazioN(rs,"VL_IOF_CTC")
	   PC_DSC_CML_CTC   = testaDadoVazioN(rs,"PC_DSC_CML_CTC")
	   VL_DSC_CML_CTC   = testaDadoVazioN(rs,"VL_DSC_CML_CTC")
	   PC_DSC_TCN_CTC   = testaDadoVazioN(rs,"PC_DSC_TCN_CTC")
 	   Margem_Comercial = testaDadoVazioN(rs,"Margem_Comercial")
	   QT_PCL_PGTO_CTC  = testaDadoVazioN(rs,"QT_PCL_PGTO_CTC")
	   VL_DSC_TCN_CTC   = testaDadoVazioN(rs,"VL_DSC_TCN_CTC")

    End If

    Set rsCotacao = objcon.mLerCotacao()
    rsCotacao.ActiveConnection = Nothing
    data_fim_vigencia     = rsCotacao("DT_INC_VGC_CTC")
    data_inicio_vigencia  = rsCotacao("DT_FIM_VGC_CTC")
    val_prem_min_prpt     = CDbl(rsCotacao("VL_PREM_MIN_PRPT"))
    num_parc_max_pmt      = rsCotacao("NR_MAX_PCL_PMT")
    perc_custo_apolice    = rsCotacao("PC_CST_APLC")
    val_max_custo_apolice = rsCotacao("VL_MAX_CST_APLC")
    Set rsCotacao = Nothing

    If NOT isnull(data_fim_vigencia) And NOT isnull(data_inicio_vigencia) Then
	   dias = datediff("d",data_fim_vigencia,data_inicio_vigencia)
 	   parcelas = int(dias/30)
	   QT_PCL_PGTO_CTC = parcelas
    Else
	   dias = 0
	   parcelas = QT_PCL_PGTO_CTC
    End If
    If num_parc_max_pmt < parcelas Then
       parcelas = num_parc_max_pmt
       QT_PCL_PGTO_CTC = parcelas
    End If

    If NOT IsNull(val_prem_min_prpt) And val_prem_min_prpt > 0 And Len(parcelas) > 0 Then
       parcelasx = VL_LQDO_MOEN_CTC / val_prem_min_prpt
       If parcelasx < parcelas Then
          QT_PCL_PGTO_CTC = parcelasx
       End If
    End If
    If Trim(VL_IPTC_MOEN_CTC) = "" Then
	   VL_IPTC_MOEN_CTC = "0,00"
    End If
    If Trim(VL_PREM_MOEN_CTC) = "" Then
	   VL_PREM_MOEN_CTC = "0,00"
    End If
    If Trim(Margem_Comercial) = "" Then
	   Margem_Comercial = "0,00"
    End If
    If Trim(PC_DSC_TCN_CTC) = "" Then
       PC_DSC_TCN_CTC = "0,00"
    End If
    
    QT_PCL_PGTO_CTC = Int(QT_PCL_PGTO_CTC)
%>
<script language="JavaScript">
<!--
function FormatNumber(vr,tamdec,truncate){
	//A fun��o espera receber casas decimais com V�RGULA
	// truncate = 1 - n�o altera o valor de casas decimais, caso seja maior do que o tamanho decidido
	// truncate = 2 - remove os valores superiores as casa decimais
	// truncate = 3 - remove os valores superiores as casa decimais e arredonda caso haja necessidade
	vp = '';
	for(f=0;f<vr.length;f++){
		if(((vr.charCodeAt(f)>=48)&&(vr.charCodeAt(f)<=57))||(vr.charCodeAt(f)==44)){
			vp=vp+vr.charAt(f);
		}
	}
	a=1;
	vr = vp;
	if(vr.length>0){
		vr = vr.split(',');
		vrIni = vr[0];
		vrIniTemp = '';
		for(f=0;f<vrIni.length;f++){
			vrIniTemp = vrIni.charAt(vrIni.length-f-1) + vrIniTemp;
			if((a==3)&&(vrIni.length!=f+1)){
				vrIniTemp = '.' + vrIniTemp;
				a=0;
			}
			a++;
		}
		vrIni = vrIniTemp;
		vrFim = "";
		if(vr.length>1){
			vrFim = vr[1];
			if(vrFim.length>tamdec){
				if(truncate==3){
					valorFinal = vrFim.substring(tamdec,tamdec + 1);
					vrFim = vrFim.substring(0,tamdec);
					if(valorFinal>4){
						vrFim = vrFim/1 + 1;
					}
				} else if(truncate==2){
					vrFim = vrFim.substring(0,tamdec);
				}
			} else {
				for(f=vrFim.length;f<tamdec;f++){
					vrFim = vrFim + '0';
				}
			}
		} else {
			for(f=0;f<tamdec;f++){
				vrFim = vrFim + '0';
			}
		}
		vr = vrIni + ',' + vrFim;
		return vr;
	}
}

function converteNumeroJavascript(valor){
	valor = valor.split(',');
	valorNovo = '';
	for(f=0;f<valor[0].length;f++){
		if((valor[0].charCodeAt(f)>=48)&&(valor[0].charCodeAt(f)<=57)){
			valorNovo=valorNovo+valor[0].charAt(f);
		}
	}
	if(valor.length>1){
		valorNovo = valorNovo + '.' + valor[1];
	}
	return valorNovo;
}

function ValidaMargemComercial()
{
	var margem_comercial = parseFloat('0' + converteNumeroJavascript(document.all.Margem_Comercial.value));

	if (margem_comercial > 100)
	{
		alert('A Margem Comercial n�o pode ser superior a 100,00');
		document.all.Margem_Comercial.value = '';
	}
}

function ValidaPercTecnico()
{
	var per_tecn = parseFloat('0' + converteNumeroJavascript(document.all.PC_DSC_TCN_CTC.value));

	if (per_tecn > 100)
	{
		alert('Percentual Desconto Tecnico n�o pode ser superior a 100,00');
		document.all.PC_DSC_TCN_CTC.value = '';
	}
}

function calculaMovimentacao()
{
	var desconto_tecnico = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_DSC_TCN_CTC.value));
    var desconto_comercial = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_DSC_CML_CTC.value));
	var percentual_iof = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_IOF_CTC.value));
	var percentual_corretagem = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_CRE_CTC.value));
	var perc_custo_apolice = parseFloat('0' + converteNumeroJavascript(document.formProc.perc_custo_apolice.value));
	var val_max_custo_apolice = parseFloat('0' + converteNumeroJavascript(document.formProc.val_max_custo_apolice.value));
    var percentual_subvencao = parseFloat('0' + converteNumeroJavascript(document.formProc.PERC_SUBVENCAO.value));
    var val_prem_min = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_premio_minimo.value));

	var premio_net = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_PREM_MOEN_CTC.value));
    var premio_net_desc = premio_net - desconto_tecnico;

	if ((premio_net > 0) && (percentual_corretagem > 0))
		var premio_liquido = (premio_net_desc / (1 - (percentual_corretagem / 100))) - desconto_comercial;
	else
		var premio_liquido = 0;

    if ((premio_liquido<val_prem_min)&&(premio_liquido>0)) premio_liquido = val_prem_min;  

    var custo_apolice = 0;
	if (parseFloat(premio_liquido) > 0) custo_apolice = premio_liquido * perc_custo_apolice / 100;
    if (parseFloat(custo_apolice) < 0)  custo_apolice = 0;
    if (parseFloat(custo_apolice) > parseFloat(val_max_custo_apolice)) custo_apolice = val_max_custo_apolice;
 
	var valor_iof = 0;
	if (percentual_iof > 0)          valor_iof = ((premio_liquido + custo_apolice) * (percentual_iof / 100));
    if (parseFloat(valor_iof)<0)     valor_iof = 0;

	var valor_premio_bruto = (premio_liquido + custo_apolice + valor_iof);
	
	var val_min      = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_prem_min_prpt.value));
    var par_por_vig  = parseInt(converteNumeroJavascript(document.formProc.qtd_parc_por_vigencia.value));
    val_min = premio_liquido / val_min;
    val_min = parseInt(val_min);
    if(val_min<par_por_vig) par_por_vig=val_min;
    document.formProc.QT_PCL_PGTO_CTC.value = par_por_vig.toString();

    if(percentual_subvencao<1){
       var val_sub_1 = (premio_liquido * percentual_subvencao) + custo_apolice;
       var val_sub_2 = premio_liquido * (1-percentual_subvencao);
       document.formProc.VL_PRIM_PARCELA.value = FormatNumber(val_sub_1.toString().replace(".", ","), 2, 2);
       document.formProc.VL_SEG_PARCELA.value = FormatNumber(val_sub_2.toString().replace(".", ","), 2, 2);
    }

    document.formProc.premio_net_desconto_tecnico.value = FormatNumber(premio_net_desc.toString().replace(".", ","), 2, 2);
	document.formProc.VL_CST_APLC_CTC.value = FormatNumber(custo_apolice.toString().replace(".", ","), 2, 2);
	document.formProc.VL_IOF_CTC.value = FormatNumber(valor_iof.toString().replace(".", ","), 2, 2);
	document.formProc.VL_PREM_MOEN_CTC.value = FormatNumber(premio_net.toString().replace(".", ","), 2, 2);
	document.formProc.VL_LQDO_MOEN_CTC.value = FormatNumber(premio_liquido.toString().replace(".", ","), 2, 2);
	document.formProc.valor_premio_bruto.value = FormatNumber(valor_premio_bruto.toString().replace(".", ","), 2, 2);
}

function calculaPremioNetDescontoTecnico(){
    var valor_des = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_PREM_MOEN_CTC.value));
    var perc_des  = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_DSC_TCN_CTC.value));

    valor_des = valor_des * perc_des / 100;

    document.formProc.VL_DSC_TCN_CTC.value = FormatNumber(valor_des.toString().replace(".", ","), 2, 2);
    document.formProc.PC_DSC_TCN_CTC.value = FormatNumber(perc_des.toString().replace(".", ","), 2, 2);

    calculaMovimentacao();
}

function submitdadosfinanceiros()
{
	document.formProc.action = 'liberar_cotacao_fin.asp';
	document.formProc.submit();
}

function alterarDados()
{
	var margem_comercial = parseFloat('0' + converteNumeroJavascript(document.all.Margem_Comercial.value));
	if (margem_comercial == 0)
	{
		if (!confirm('A Margem Comercial deveria ser maior que 0,00, o valor est� correto?'))
			return;
	}
	
	document.formProc.salvarfin.value = 'S';
	submitdadosfinanceiros();
}
-->
</script>
<%
    If Trim(request("salvarfin")) = "S" Then
   	   Set rsCotacao = objCon.mLerCotacao
       rsCotacao.ActiveConnection = Nothing
	   'If UCase(rsCotacao("CD_ULT_EVT_CTC")) = "T" Then
	   '   bAtualizaCotacao = False
	   'Else
		'  objcon.pCD_ULT_EVT_CTC = "T"
	 	'  bAtualizaCotacao = True
	   'End If
	
	   objCon.pVL_IPTC_MOEN_CTC = VL_IPTC_MOEN_CTC
	   objCon.pVL_PREM_MOEN_CTC = VL_PREM_MOEN_CTC
	   objCon.pVL_PRMO_MOEN_CTC = VL_PRMO_MOEN_CTC
	   objCon.pVL_SGRA_MOEN_CTC = VL_SGRA_MOEN_CTC
	   objCon.pVL_PCL_MOEN_CTC  = VL_PCL_MOEN_CTC
	   objCon.pVL_LQDO_MOEN_CTC = VL_LQDO_MOEN_CTC
	   objCon.pVL_IPTC_MOEE_CTC = VL_IPTC_MOEE_CTC
	   objCon.pVL_PREM_MOEE_CTC = VL_PREM_MOEE_CTC
	   objCon.pVL_PRMO_MOEE_CTC = VL_PRMO_MOEE_CTC
	   objCon.pVL_SGRA_MOEE_CTC = VL_SGRA_MOEE_CTC
	   objCon.pVL_PCL_MOEE_CTC  = VL_PCL_MOEE_CTC
	   objCon.pVL_LQDO_MOEE_CTC = VL_LQDO_MOEE_CTC
	   objCon.pMargem_Comercial = Margem_Comercial
	   objCon.pQT_PCL_PGTO_CTC  = Int(QT_PCL_PGTO_CTC)
	   objCon.pVL_CST_APLC_CTC  = VL_CST_APLC_CTC
	   objCon.pPC_DSC_TCN_CTC   = PC_DSC_TCN_CTC
	   objCon.pVL_DSC_TCN_CTC   = VL_DSC_TCN_CTC
	   objCon.pPC_DSC_CML_CTC   = PC_DSC_CML_CTC
	   objCon.pVL_DSC_CML_CTC   = VL_DSC_CML_CTC
	   objCon.pVL_IOF_CTC       = VL_IOF_CTC

	   chave
	   objCon.pWF_ID = WF_ID
 	   
 	   retorno = objCon.mAtualizarDadosFinanceiros(bAtualizaCotacao)
 	   
	   If trim(retorno) <> "" Then
		  Response.redirect("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
		  Response.End 
	   End If
	
	   chave
    End If
  
    If isnull(rs("VL_IPTC_MOEN_CTC")) Then 
 	   VL_IPTC_MOEN_CTC = 0
    Else
	   VL_IPTC_MOEN_CTC = rs("VL_IPTC_MOEN_CTC")
    End If
    
    VL_PREM_MOEN_CTC = 0
    If not isnull(rs("VL_PREM_MOEN_CTC")) then  
        VL_PREM_MOEN_CTC = testaDadoVazioN(rs,"VL_PREM_MOEN_CTC")
    End If
    
    If not isnull(rs("VL_PREM_CBT_CTC")) then
        If cdbl(VL_PREM_MOEN_CTC) <> cdbl(rs("VL_PREM_CBT_CTC")) then
            VL_PREM_MOEN_CTC = testaDadoVazioN(rs,"VL_PREM_CBT_CTC")
        End If
    End If
	
End If%>
	<br>
	<%dim arrParametro
	arrParametro = Session("parametro")
	if arrParametro(9) <> Application("WF_HAB") then%>
	&nbsp;&nbsp;&nbsp;
	<!--input type="button" name="alterar" value="Gravar" style="width:80px;font-size:10px;margin: 2px 2px 2px 2px;" onclick="alterarDados()"-->
	<%end if%>
	<input type="hidden" name="salvarfin" value="N">
	<br>
	<input type="hidden" name="primeiro" value="false">
	<table cellpadding="2" cellspacing="1" border="0" width="900" class="escolha" border=1>
		<tr>
			<td nowrap class="td_label" width="257">&nbsp;Moeda do seguro da cota��o</td>
			<td class="td_dado" colspan="2" width="200">&nbsp;
				<%=testaDadoVazio(rs,"CD_MOE_SGRO_CTC") & " - " & testaDadoVazio(rs,"NOME_MOE_SGRO")%>
			</td>
		</tr>												
		<tr>
			<td nowrap class="td_label" width="257">&nbsp;Moeda de origem da cota��o</td>
			<td class="td_dado" colspan="2" width="200">&nbsp;
				<%=testaDadoVazio(rs,"CD_MOE_OGM_CTC") & " - " & testaDadoVazio(rs,"NOME_MOE_OGM")%>
			</td>
		</tr>
		<%if arrParametro(9) = Application("WF_HAB") then%>
		<tr>
			<td nowrap class="td_label" width="257">&nbsp;Valor IS</td>
			<td class="td_dado" width="124" colspan="2">&nbsp;<input type="text" name="VL_IPTC_MOEN_CTC" size="16" value="<%=VL_IPTC_MOEN_CTC%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly ID="Text1"></td>
		</tr>
		<%end if%>
		<tr>
			<td nowrap class="td_label" width="257">&nbsp;Percentual do custo de ap�lice</td>
			<td class="td_dado" colspan="2" width="200">&nbsp;<input type="text" name="PC_CST_APLC_CTC" size="16" value="<%=testaDadoVazioN(rs,"PC_CST_APLC_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="257">&nbsp;Percentual IOF</td>
			<td class="td_dado" width="173">
      			 &nbsp;<input type="text" name="PC_IOF_CTC" size="16" value="<%=testaDadoVazioN(rs,"PC_IOF_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
			<%percentual_subvencao = "1"

 			  If CD_ITEM_MDLD = "201" Then
	             Set rsSubvenc = objCon.mLerValoresSubvencao
                 rsSubvenc.ActiveConnection = Nothing 
            %>
      		<td rowspan="5" width="417">
            <table cellpadding="1" cellspacing="1" border="1" width="415" class="escolha" height="127"  border=2>
      		<tr><td height="123" width="407" class="td_label">&nbsp;
    		   <%If NOT rsSubvenc.EOF Then
      		        If rsSubvenc("TP_SUBVENCAO") = "Estadual" Then
      		            Response.Write "<center><b>Subven��o Estadual</b></center>"
      		        Else
      		            If rsSubvenc("SUBVENCAO_GOVERNO") = "Sim" Then
      		               Response.Write "&nbsp;<b>Subven��o Federal</b><br>"
      		               Response.Write "&nbsp;&nbsp;&nbsp;<b>Primeira Parcela:</b>&nbsp;&nbsp;<input type=""text"" name=""VL_PRIM_PARCELA"" size=""16"" value=""" & rsSubvenc("1_parc")  & """ style=""text-align:right; background-color:#EEEEEE;"" readonly><br>"
      		               Response.Write "&nbsp;&nbsp;&nbsp;<b>Segunda Parcela:</b>&nbsp;&nbsp;<input type=""text"" name=""VL_SEG_PARCELA"" size=""16"" value=""" & rsSubvenc("2_parc") & """ style=""text-align:right; background-color:#EEEEEE;"" readonly><br>"
      		               percentual_subvencao = rsSubvenc("PERC_SUBVENCAO")
      		            Else
      		               Response.Write "<center><b>Sem Subven��o</b></center>"
      		            End If
      		        End If
      		     End If%>
       		</td></tr>
      		</table></td>
      		<%End If%>
            <input type="hidden" name="PERC_SUBVENCAO" value="<%=percentual_subvencao%>">
		</tr>
		<tr>
			<td nowrap class="td_label" width="257">&nbsp;Percentual de corretagem</td>
			<td class="td_dado" width="173">&nbsp;<input type="text" name="PC_CRE_CTC" size="16" value="<%=testaDadoVazioN(rs,"PC_CRE_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<%if arrParametro(9) <> Application("WF_HAB") then%>
		<tr>
			<td nowrap class="td_label" width="257">&nbsp;Percentual desconto t�cnico</td>
			<td class="td_dado" width="173">&nbsp;<input type="text" name="PC_DSC_TCN_CTC" size="16" value="<%=PC_DSC_TCN_CTC%>" onblur="javascript:ValidaPercTecnico();calculaPremioNetDescontoTecnico();" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right;">
			<!--td class="td_dado" width="173">&nbsp;<input type="text" name="PC_DSC_TCN_CTC" size="16" value="<%=PC_DSC_TCN_CTC%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly-->
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="257">&nbsp;Margem Comercial&nbsp;</td>
			<td class="td_dado" width="173">&nbsp;<input type="text" name="Margem_Comercial" size="16" OnBlur="javascript:ValidaMargemComercial();" value="<%=Margem_Comercial%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right;">
			<!--td class="td_dado" width="173">&nbsp;<input type="text" name="Margem_Comercial" size="16" value="<%=Margem_Comercial%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly-->
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="257">&nbsp;Qtd m�xima parcelas pgto do pr�mio</td>
			<td class="td_dado" width="173">&nbsp;<input type="text" name="QT_PCL_PGTO_CTC" size="2" value="<%=QT_PCL_PGTO_CTC%>" maxlength="2" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="qtd_parc_por_vigencia" value="<%=parcelas%>">
				<input type="hidden" name="valor_prem_min_prpt" value="<%=valor_prem_min_prpt%>">
				<input type="hidden" name="perc_custo_apolice" value="<%=perc_custo_apolice%>">
				<input type="hidden" name="val_max_custo_apolice" value="<%=val_max_custo_apolice%>">
			</td>
		</tr>
		<%end if%>
        <br>
		<tr>
			<td colspan="3" width="386">
<%
Set rsSubgrupo = objcon.mLerSubgrupoCotacao
rsSubGrupo.ActiveConnection = Nothing

If not rsSubgrupo.EOF Then
	Do while not rsSubgrupo.EOF

		objCon.pNR_CTC_SGRO = rsSubgrupo("NR_CTC_SGRO")
		objCon.pNR_VRS_CTC =  rsSubgrupo("NR_VRS_CTC")

		Set rs = objcon.mLerDadosFinanceiros(1) '1 = RE
		rs.ActiveConnection = Nothing

		if not rs.EOF Then
			if not isnull(rs("VL_IPTC_MOEN_CTC")) then
				VL_IPTC_MOEN_CTC = cdbl(VL_IPTC_MOEN_CTC) + cdbl(rs("VL_IPTC_MOEN_CTC"))
			end if

			if not isnull(rs("VL_PREM_MOEN_CTC")) then
				VL_PREM_MOEN_CTC = cdbl(VL_PREM_MOEN_CTC) + cdbl(rs("VL_PREM_MOEN_CTC"))
			end if
		end if

		rsSubgrupo.MoveNext
	Loop
End If

VL_IPTC_MOEN_CTC = FormatNumber(VL_IPTC_MOEN_CTC,2)
VL_PREM_MOEN_CTC = FormatNumber(VL_PREM_MOEN_CTC,2)%>
			</td>
		</tr>
		
		<%
		if arrParametro(9) <> Application("WF_HAB") then%>
		<tr>
			<td nowrap class="td_label" width="257">&nbsp;Valor IS</td>
			<td class="td_dado" width="124" colspan="2">&nbsp;<input type="text" name="VL_IPTC_MOEN_CTC" size="16" value="<%=VL_IPTC_MOEN_CTC%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly></td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="257">&nbsp;Valor pr�mio NET</td>
			<td class="td_dado" width="124" colspan="2">&nbsp;<input type="text" name="VL_PREM_MOEN_CTC" size="16" value="<%=VL_PREM_MOEN_CTC%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly></td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="257">&nbsp;Valor desconto t�cnico</td>
			<td class="td_dado" width="124" colspan="2">&nbsp;<input type="text" name="VL_DSC_TCN_CTC" size="16" value="<%=VL_DSC_TCN_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly></td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="257">&nbsp;Valor pr�mio NET com desconto t�cnico</td>
			<td class="td_dado" width="124" colspan="2">&nbsp;<input type="text" name="premio_net_desconto_tecnico" size="16" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly></td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor de corretagem</td>
			<td class="td_dado" colspan="2">&nbsp;
				<input type="text" name="valor_corretagem" size="16" value="" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="257">&nbsp;Valor do pr�mio l�quido</td>
			<td class="td_dado" width="124" colspan="2">&nbsp;<input type="text" name="VL_LQDO_MOEN_CTC" size="16" value="<%=VL_LQDO_MOEN_CTC%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly></td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="257">&nbsp;Valor do custo da ap�lice</td>
			<td class="td_dado" width="124" colspan="2">&nbsp;<input type="text" name="VL_CST_APLC_CTC" size="16" value="<%=VL_CST_APLC_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly></td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="257">&nbsp;Valor IOF</td>
			<td class="td_dado" width="124" colspan="2">&nbsp;<input type="text" name="VL_IOF_CTC" size="16" value="<%=VL_IOF_CTC%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly></td>
		</tr>
		<tr>
			<td nowrap class="td_label" width="257">&nbsp;Valor do pr�mio bruto</td>
			<td class="td_dado" width="124" colspan="2">&nbsp;<input type="text" name="valor_premio_bruto" size="16" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly></td>
		</tr>
		<%end if%>
	</table>
	<input type="hidden" value="<%=IN_PG_ATO_CTC%>">
	<input type="hidden" name="VL_PRMO_MOEN_CTC" value="<%=VL_PRMO_MOEN_CTC%>">
	<input type="hidden" name="VL_SGRA_MOEN_CTC" value="<%=VL_PREM_MOEN_CTC%>">
	<input type="hidden" name="VL_PCL_MOEN_CTC" value="<%=VL_PCL_MOEN_CTC%>">
	<input type="hidden" name="PC_DSC_CML_CTC" value="<%=PC_DSC_CML_CTC%>">
	<input type="hidden" name="VL_DSC_CML_CTC" value="<%=VL_DSC_CML_CTC%>">
	<input type="hidden" name="valor_premio_minimo" value="<%=val_prem_min_prpt%>">
	
	<script language="JavaScript">
	<!--
	calculaMovimentacao();
	//-->
	</script>