<!--#include virtual = "/padrao.asp"-->
<%
set rs = objcon.mLerDadosFinanceiros()
rs.ActiveConnection = Nothing

contador = request("__liberar_cotacao___contador")
valor_is_cob = 0
valor_premio_cob = 0
for f=0 to contador -1
	liberar_cotacao_vl_iptc_cbt_ctc = request("__liberar_cotacao___vl_iptc_cbt_ctc" & f)
	if trim(liberar_cotacao_vl_iptc_cbt_ctc) <> "" then
		valor_is_cob = valor_is_cob + cdbl(liberar_cotacao_vl_iptc_cbt_ctc)
	end if
	
	liberar_cotacao_vl_prem_cbt_ctc = request("__liberar_cotacao___vl_prem_cbt_ctc" & f)
	if trim(liberar_cotacao_vl_prem_cbt_ctc) <> "" then
		valor_premio_cob = valor_premio_cob + cdbl(liberar_cotacao_vl_prem_cbt_ctc)
	end if
next 
valor_is_cob = formatNumber(valor_is_cob,2)
valor_premio_cob = formatNumber(valor_premio_cob,2)

if request("primeiro") = "false" then
	VL_IPTC_MOEN_CTC = request("VL_IPTC_MOEN_CTC")
	VL_PREM_MOEN_CTC = request("VL_PREM_MOEN_CTC")
	VL_PRMO_MOEN_CTC = request("VL_PRMO_MOEN_CTC")
	'VL_PG_ATO_MOEN_CTC = request("VL_PG_ATO_MOEN_CTC")
	VL_PCL_MOEN_CTC = request("VL_PCL_MOEN_CTC")
	VL_LQDO_MOEN_CTC = request("VL_LQDO_MOEN_CTC")
	VL_IPTC_MOEE_CTC = request("VL_IPTC_MOEE_CTC")
	VL_PREM_MOEE_CTC = request("VL_PREM_MOEE_CTC")
	VL_PRMO_MOEE_CTC = request("VL_PRMO_MOEE_CTC")
	'VL_PG_ATO_MOEE_CTC = request("VL_PG_ATO_MOEE_CTC")
	VL_PCL_MOEE_CTC = request("VL_PCL_MOEE_CTC")
	VL_LQDO_MOEE_CTC = request("VL_LQDO_MOEE_CTC")
	QT_PCL_PGTO_CTC = request("QT_PCL_PGTO_CTC")
	VL_CST_APLC_CTC = request("VL_CST_APLC_CTC")
	PC_DSC_TCN_CTC = request("PC_DSC_TCN_CTC")
	Margem_Comercial = request("Margem_Comercial")
	PC_DSC_CML_CTC = request("PC_DSC_CML_CTC")
	VL_DSC_TCN_CTC = request("VL_DSC_TCN_CTC")
	VL_DSC_CML_CTC = request("VL_DSC_CML_CTC")
	IN_PG_ATO_CTC = request("IN_PG_ATO_CTC")
	VL_SGRA_MOEN_CTC = request("VL_SGRA_MOEN_CTC")
	VL_SGRA_MOEE_CTC = request("VL_SGRA_MOEE_CTC")
else
	VL_IPTC_MOEN_CTC = testaDadoVazio(rs,"VL_IPTC_MOEN_CTC")
	VL_PREM_MOEN_CTC = testaDadoVazio(rs,"VL_PREM_MOEN_CTC")
	VL_PRMO_MOEN_CTC = testaDadoVazio(rs,"VL_PRMO_MOEN_CTC")
	'VL_PG_ATO_MOEN_CTC = testaDadoVazio(rs,"VL_PG_ATO_MOEN_CTC")
	VL_PCL_MOEN_CTC = testaDadoVazio(rs,"VL_PCL_MOEN_CTC")
	VL_LQDO_MOEN_CTC = testaDadoVazio(rs,"VL_LQDO_MOEN_CTC")
	VL_IPTC_MOEE_CTC = testaDadoVazio(rs,"VL_IPTC_MOEE_CTC")
	VL_PREM_MOEE_CTC = testaDadoVazio(rs,"VL_PREM_MOEE_CTC")
	VL_PRMO_MOEE_CTC = testaDadoVazio(rs,"VL_PRMO_MOEE_CTC")
	'VL_PG_ATO_MOEE_CTC = testaDadoVazio(rs,"VL_PG_ATO_MOEE_CTC")
	VL_PCL_MOEE_CTC = testaDadoVazio(rs,"VL_PCL_MOEE_CTC")
	VL_LQDO_MOEE_CTC = testaDadoVazio(rs,"VL_LQDO_MOEE_CTC")
	VL_CST_APLC_CTC = testaDadoVazio(rs,"VL_CST_APLC_CTC")

	PC_DSC_TCN_CTC = testaDadoVazio(rs,"PC_DSC_TCN_CTC")
	Margem_Comercial = testaDadoVazio(rs,"Margem_Comercial")
	PC_DSC_CML_CTC = testaDadoVazio(rs,"PC_DSC_CML_CTC")
	VL_DSC_TCN_CTC = testaDadoVazio(rs,"VL_DSC_TCN_CTC")
	VL_DSC_CML_CTC = testaDadoVazio(rs,"VL_DSC_CML_CTC")

	IN_PG_ATO_CTC = testaDadoVazio(rs,"IN_PG_ATO_CTC")
	VL_SGRA_MOEN_CTC = testaDadoVazio(rs,"VL_SGRA_MOEN_CTC")
	VL_SGRA_MOEE_CTC = testaDadoVazio(rs,"VL_SGRA_MOEE_CTC")

	Set rsCotacao = objcon.mLerCotacao()
	rsCotacao.ActiveConnection = Nothing
	data_fim_vigencia = rsCotacao("DT_INC_VGC_CTC")
	data_inicio_vigencia = rsCotacao("DT_FIM_VGC_CTC")
	set rsCotacao = nothing
	
	if not isnull(data_fim_vigencia) and not isnull(data_inicio_vigencia) then
		dias = datediff("d",data_fim_vigencia,data_inicio_vigencia)
		parcelas = int(dias/30)
	else
		dias = 0
		QT_PCL_PGTO_CTC = ""
	end if
	QT_PCL_PGTO_CTC = testaDadoVazio(rs,"QT_PCL_PGTO_CTC")
	if QT_PCL_PGTO_CTC > parcelas then
		QT_PCL_PGTO_CTC = parcelas
	end if

	set rsM = objcon.mLerValorIs()
	rsM.ActiveConnection = Nothing
	'if testaDadoVazio(rs,"CD_MOE_SGRO_CTC") = "79" or testaDadoVazio(rs,"CD_MOE_SGRO_CTC") = "80" then
	if testaDadoVazio(rs,"CD_MOE_SGRO_CTC") = "790" or testaDadoVazio(rs,"CD_MOE_SGRO_CTC") = "220" then
		VL_IPTC_MOEN_CTC = valor_is_cob
		VL_PREM_MOEN_CTC = valor_premio_cob
		retorno = " readonly style=""background-color:#EEEEEE;text-align:right;"""
		if not rsM.eof then
			VL_IPTC_MOEN_CTC = rsM(0)
			VL_IPTC_MOEE_CTC = "0,00"
		else
			VL_IPTC_MOEN_CTC = "0,00"
			VL_IPTC_MOEE_CTC = "0,00"
		end if
	else
		VL_IPTC_MOEN_CTC = valor_is_cob
		VL_PREM_MOEN_CTC = valor_premio_cob
		retorno = ""
		if not rsM.eof then
			VL_IPTC_MOEN_CTC = "0,00"
			VL_IPTC_MOEE_CTC = rsM(0)
		else
			VL_IPTC_MOEN_CTC = "0,00"
			VL_IPTC_MOEE_CTC = "0,00"
		end if
	end if
end if

%>
<script language="JavaScript">
<!--
function FormatNumber(vr,tamdec,truncate){
	//A fun��o espera receber casas decimais com V�RGULA
	// truncate = 1 - n�o altera o valor de casas decimais, caso seja maior do que o tamanho decidido
	// truncate = 2 - remove os valores superiores as casa decimais
	// truncate = 3 - remove os valores superiores as casa decimais e arredonda caso haja necessidade
	vp = '';
	for(f=0;f<vr.length;f++){
		if(((vr.charCodeAt(f)>=48)&&(vr.charCodeAt(f)<=57))||(vr.charCodeAt(f)==44)){
			vp=vp+vr.charAt(f);
		}
	}
	a=1;
	vr = vp;
	if(vr.length>0){
		vr = vr.split(',');
		vrIni = vr[0];
		vrIniTemp = '';
		for(f=0;f<vrIni.length;f++){
			vrIniTemp = vrIni.charAt(vrIni.length-f-1) + vrIniTemp;
			if((a==3)&&(vrIni.length!=f+1)){
				vrIniTemp = '.' + vrIniTemp;
				a=0;
			}
			a++;
		}
		vrIni = vrIniTemp;
		vrFim = "";
		if(vr.length>1){
			vrFim = vr[1];
			if(vrFim.length>tamdec){
				if(truncate==3){
					valorFinal = vrFim.substring(tamdec,tamdec + 1);
					vrFim = vrFim.substring(0,tamdec);
					if(valorFinal>4){
						vrFim = vrFim/1 + 1;
					}
				} else if(truncate==2){
					vrFim = vrFim.substring(0,tamdec);
				}
			} else {
				for(f=vrFim.length;f<tamdec;f++){
					vrFim = vrFim + '0';
				}
			}
		} else {
			for(f=0;f<tamdec;f++){
				vrFim = vrFim + '0';
			}
		}
		vr = vrIni + ',' + vrFim;
		return vr;
	}
}

function CalculaDesconto( pTotal, pDesc, pCampo ){
	vDesc = pDesc.replace(",", ".");
	vTotal = pTotal.replace(".", "");
	vTotal = vTotal.replace(",", ".");

	if (vDesc <= 100){
		vVDesc = ( (vTotal*vDesc) / 100 );
		pCampo.value = vVDesc;
		pCampo.value = vVDesc ;
		pCampo.value = document.formProc.VL_DSC_TCN_CTC.value.replace(".", ",");
		pCampo.value = FormatNumber(document.formProc.VL_DSC_TCN_CTC.value, 2, 3);
	} else {
		alert('O desconto n�o pode ser superior a 100,00');
		pCampo.value = "";
	}
}

function CalculaDesconto2(pTotal, pDesc, pCampo){
	var vDesc = pDesc.value.replace(",", ".");
	var vTotal = pTotal.value.replace(".", "").replace(",", ".");

	if (vDesc == '') vDesc = 0;
	if (vTotal == '') vTotal = 0;

	if (vDesc <= 100){
		vVDesc = ((vTotal*vDesc) / 100);
		pCampo.value = FormatNumber(vVDesc.toString().replace(".", ","), 2, 3);
		
	} else {
		alert('O desconto n�o pode ser superior a 100,00');
		pCampo.value = "";
	}
}

function ValidaPercentualDesconto() {
	var pMargem = document.all.Margem_Comercial;
	var pPerc = document.all.PC_DSC_CML_CTC;
	var pDesc = document.all.VL_DSC_CML_CTC;
		
	var vMargem = pMargem.value.replace(".", "").replace(",",".");
	var vPerc = pPerc.value.replace(".", "").replace(",",".");

	if (vMargem == '') vMargem = 0;
	if (vPerc == '') vPerc = 0;

	if (vPerc > vMargem)
	{
		alert('O Percentual de Desconto n�o pode ser maior que a Margem Comercial');
		pPerc.value = '';
		pDesc.value = '';
		return false;
	}
	
	return true;
}

function CalculaMargem() {
	
	if (!ValidaPercentualDesconto()) return;


	if (!document.all.VL_LQDO_MOEN_CTC.disabled)
		var pValorLiquido = document.all.VL_LQDO_MOEN_CTC;
	else if (!document.all.VL_LQDO_MOEE_CTC.disabled)
		var pValorLiquido = document.all.VL_LQDO_MOEE_CTC;


	CalculaDesconto2(pValorLiquido, document.all.PC_DSC_CML_CTC, document.all.VL_DSC_CML_CTC);

}

-->
</script>
	<input type="hidden" name="primeiro" value="false">
	<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
		<tr>
			<td nowrap class="td_label">&nbsp;Moeda do seguro da cota��o</td>
			<td class="td_dado" width="800">&nbsp;
				<%=testaDadoVazio(rs,"CD_MOE_SGRO_CTC")%> - <%=testaDadoVazio(rs,"NOME_MOE_SGRO")%>
			</td>
		</tr>												
		<tr>
			<td nowrap class="td_label">&nbsp;Moeda de origem da cota��o</td>
			<td class="td_dado">&nbsp;
				<%=testaDadoVazio(rs,"CD_MOE_OGM_CTC")%> - <%=testaDadoVazio(rs,"NOME_MOE_OGM")%>
			</td>
		</tr>												
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual do custo de ap�lice</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_CST_APLC_CTC" size="16" value="<%=testaDadoVazio(rs,"PC_CST_APLC_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>				
		<tr>
			<td nowrap class="td_label">&nbsp;Valor do custo da ap�lice</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_CST_APLC_CTC" size="16" value="<%=VL_CST_APLC_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual de corretagem</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_CRE_CTC" size="16" value="<%=testaDadoVazio(rs,"PC_CRE_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual IOF</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_IOF_CTC" size="16" value="<%=testaDadoVazio(rs,"PC_IOF_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor IOF</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_IOF_CTC" size="16" value="<%=testaDadoVazio(rs,"VL_IOF_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual desconto t�cnico</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_DSC_TCN_CTC" OnBlur="javascript: CalculaDesconto(document.formProc.VL_PREM_MOEN_CTC.value, document.formProc.PC_DSC_TCN_CTC.value, document.formProc.VL_DSC_TCN_CTC);" size="16" value="<%=PC_DSC_TCN_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor desconto t�cnico</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_DSC_TCN_CTC" size="16" value="<%=VL_DSC_TCN_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Margem Comercial&nbsp;</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="Margem_Comercial" size="16" OnBlur="javascript:ValidaPercentualDesconto();" value="<%=Margem_Comercial%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual desconto comercial&nbsp;</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_DSC_CML_CTC" OnBlur="javascript:CalculaMargem();" size="16" value="<%=PC_DSC_CML_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor desconto comercial</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_DSC_CML_CTC" size="16" value="<%=VL_DSC_CML_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual estipulante</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_ETLE_CTC" size="16" value="<%=testaDadoVazio(rs,"PC_ETLE_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Qtd parcelas pgto do pr�mio</td>
			<td class="td_dado">&nbsp;
				<!--input type="text" name="QT_PCL_PGTO_CTC" size="16" value="<%=QT_PCL_PGTO_CTC%>" onKeyUp="FormatValorContinuo(this,0);" maxlength="16" style="text-align:right;"-->
				<select disabled="true" name="rQT_PCL_PGTO_CTC" onChange="javascript:AtribuirPagtoParcelaHidden();">
					<%For i = QT_PCL_PGTO_CTC To 0 Step -1
						Response.Write "<option value='" & i & "'"
						If QT_PCL_PGTO_CTC = i Then Response.Write " SELECTED"
						Response.Write ">" & CStr(i) & "</option>"
					Next%>
				</select>
				<input type="hidden" name="QT_PCL_PGTO_CTC" value="">
				</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Pagamento no ato</td>
			<td class="td_dado">
			<%
			sIN_PG_ATO_CTC0 = ""
			sIN_PG_ATO_CTC1 = ""
			if trim(IN_PG_ATO_CTC) = "S" then
				sIN_PG_ATO_CTC0 = "checked"
			else
				sIN_PG_ATO_CTC1 = "checked"
			end if
			%>
			<input type="radio" name="rIN_PG_ATO_CTC" value="S" disabled="true" <%=sIN_PG_ATO_CTC0%> onclick="javascript:AtribuirIndicaPagtoHidden();">&nbsp;Sim&nbsp;
			<input type="radio" name="rIN_PG_ATO_CTC" value="N" disabled="true" <%=sIN_PG_ATO_CTC1%> onclick="javascript:AtribuirIndicaPagtoHidden();">&nbsp;N�o
			<input type="hidden" name="IN_PG_ATO_CTC" value="">
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="100%" cellspacing="3" border="0">
					<tr>
						<td width="50%">
							<table width="100%" border="0">
								<tr>
									<td colspan="2" class="td_label"><span class="sub_titulo" style="font-size:12px"><b>:: Moeda Nacional</b></span></td>								
								</tr>
								<tr>
									<td nowrap class="td_label">&nbsp;Valor IS</td>
									<td class="td_dado">&nbsp;<input type="text" name="VL_IPTC_MOEN_CTC" size="16" value="<%=VL_IPTC_MOEN_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly></td>
								</tr>
								<tr>
									<td nowrap class="td_label">&nbsp;Valor pr�mio NET</td>
									<td class="td_dado">&nbsp;<input type="text" name="VL_PREM_MOEN_CTC" size="16" value="<%=VL_PREM_MOEN_CTC%>" onKeyUp="FormatValorContinuo(this,2);" OnBlur="javascript: CalculaDesconto(document.formProc.VL_PREM_MOEN_CTC.value, document.formProc.PC_DSC_TCN_CTC.value, document.formProc.VL_DSC_TCN_CTC );" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly></td>
								</tr>
								<!--tr>
									<td nowrap class="td_label">&nbsp;Valor 1�. Parcela</td>
									<td class="td_dado">&nbsp;<input type="text" name="VL_PRMO_MOEN_CTC" size="16" value="<%=VL_PRMO_MOEN_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right;"></td>
								</tr>
								<tr>
									<td nowrap class="td_label">&nbsp;Valor pago no ato</td>
									<td class="td_dado">&nbsp;<input type="text" name="VL_PG_ATO_MOEN_CTC" size="16" value="<%=VL_PG_ATO_MOEN_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right;"></td>
								</tr-->
								<input type="hidden" name="VL_PRMO_MOEN_CTC" value="<%=VL_PRMO_MOEN_CTC%>">
								<!--input type="hidden" name="VL_PG_ATO_MOEN_CTC" value="<%=VL_PG_ATO_MOEN_CTC%>"-->
								<input type="hidden" name="VL_PCL_MOEN_CTC" value="<%=VL_PCL_MOEN_CTC%>">
								<!--tr>
									<td nowrap class="td_label">&nbsp;Valor outras parcelas</td>
									<td class="td_dado">&nbsp;<input type="text" name="VL_PCL_MOEN_CTC" size="16" value="<%=VL_PCL_MOEN_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right;"></td>
								</tr-->
								<tr>
									<td nowrap class="td_label">&nbsp;Valor do pr�mio l�quido</td>
									<td class="td_dado">&nbsp;<input type="text" name="VL_LQDO_MOEN_CTC" size="16" value="<%=VL_LQDO_MOEN_CTC%>" OnBlur="javascript:CalculaMargem();" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly></td>
								</tr>
								<tr>
									<td nowrap class="td_label">&nbsp;Valor do pr�mio seguradora</td>
									<td class="td_dado">&nbsp;<input type="text" name="VL_SGRA_MOEN_CTC" size="16" value="<%=VL_SGRA_MOEN_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly></td>
								</tr>
							</table>
						</td>
						<td width="50%">
							<table width="100%" border="0">
								<tr>
									<td colspan="2" class="td_label"><span class="sub_titulo" style="font-size:12px"><b>:: Moeda Estrangeira</b></span></td>
								</tr>
								<tr>
									<td nowrap class="td_label">&nbsp;Valor IS</td>
									<td class="td_dado">&nbsp;<input type="text" name="VL_IPTC_MOEE_CTC" size="16" value="<%=VL_IPTC_MOEE_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16"  style="text-align:right; background-color:#EEEEEE;" <% = retorno %> readonly></td>
								</tr>
								<tr>
									<td nowrap class="td_label">&nbsp;Valor pr�mio NET</td>
									<td class="td_dado">&nbsp;<input type="text" name="VL_PREM_MOEE_CTC" size="16" value="<%=VL_PREM_MOEE_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" <% = retorno %> readonly></td>
								</tr>
								<!--tr>
									<td nowrap class="td_label">&nbsp;Valor 1�. Parcela</td>
									<td class="td_dado">&nbsp;<input type="text" name="VL_PRMO_MOEE_CTC" size="16" value="<%=VL_PRMO_MOEE_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right;" <% = retorno %>></td>
								</tr>
								<tr>
									<td nowrap class="td_label">&nbsp;Valor pago no ato</td>
									<td class="td_dado">&nbsp;<input type="text" name="VL_PG_ATO_MOEE_CTC" size="16" value="<%=VL_PG_ATO_MOEE_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right;" <% = retorno %>></td>
								</tr-->
								<input type="hidden" name="VL_PRMO_MOEE_CTC" value="<%=VL_PRMO_MOEE_CTC%>">
								<!--input type="hidden" name="VL_PG_ATO_MOEE_CTC" value="<%=VL_PG_ATO_MOEE_CTC%>"-->
								<input type="hidden" name="VL_PCL_MOEE_CTC" value="<%=VL_PCL_MOEE_CTC%>">
								<!--tr>
									<td nowrap class="td_label">&nbsp;Valor outras parcelas</td>
									<td class="td_dado">&nbsp;<input type="text" name="VL_PCL_MOEE_CTC" size="16" value="<%=VL_PCL_MOEE_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right;" <% = retorno %>></td>
								</tr-->
								<tr>
									<td nowrap class="td_label">&nbsp;Valor do pr�mio l�quido</td>
									<td class="td_dado">&nbsp;<input type="text" name="VL_LQDO_MOEE_CTC" size="16" value="<%=VL_LQDO_MOEE_CTC%>" OnBlur="javascript:CalculaMargem();" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" <% = retorno %> readonly></td>
								</tr>
								<tr>
									<td nowrap class="td_label">&nbsp;Valor do pr�mio seguradora</td>
									<td class="td_dado">&nbsp;<input type="text" name="VL_SGRA_MOEE_CTC" size="16" value="<%=VL_SGRA_MOEE_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" <% = retorno %> readonly></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<script language="JavaScript">
	//atualiza o desconto ao carregar a p�gina
	if (document.formProc.PC_DSC_TCN_CTC.value != ""){
		CalculaDesconto(document.formProc.VL_PREM_MOEN_CTC.value, document.formProc.PC_DSC_TCN_CTC.value, document.formProc.VL_DSC_TCN_CTC );
	}
</script>
<script language="JavaScript">
function ValidarEdicao(bExibe)
{
	document.all.rIN_PG_ATO_CTC(0).disabled = !bExibe;
	document.all.rIN_PG_ATO_CTC(1).disabled = !bExibe;
}

function AtribuirIndicaPagtoHidden()
{
	if (document.all.rIN_PG_ATO_CTC(0).checked)
		document.all.IN_PG_ATO_CTC.value = 'S';
	else if (document.all.rIN_PG_ATO_CTC(1).checked)
		document.all.IN_PG_ATO_CTC.value = 'N';
	else
		document.all.IN_PG_ATO_CTC.value = '';
}

function AtribuirPagtoParcelaHidden()
{
	document.all.QT_PCL_PGTO_CTC.value = document.all.rQT_PCL_PGTO_CTC.value;
}

AtribuirIndicaPagtoHidden(); //atualiza indicador de pagamento ao carregar a p�gina
AtribuirPagtoParcelaHidden(); //atualiza qtde de parcelas ao carregar a p�gina

</script>
<%if bLiberarOptPagto then%>
<script>ValidarEdicao(true);</script>
<%end if%>