<!--#include virtual = "/padrao.asp"-->
<%
set rsDados = objcon.mLerSequencialObservacao()
rsDados.ActiveConnection = Nothing
%>
<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
	<tr>
		<td class="td_label" width="15%">&nbsp;Objeto do Risco</td>
		<td class="td_dado" width="85%">&nbsp;
			<Select name="nr_seql_cnd_ctc" class="texto" onchange="javascript: mudaTexto( this.value )">
				<%
				if not rsDados.eof then
					obs = ""
					%><option value="">-- Escolha Abaixo --</option><%
					while not rsDados.eof
						%>
						<option value="<%=rsDados("nr_seql_cnd_ctc")%>"<%if trim(rsDados("nr_seql_cnd_ctc")) = "1" then Response.Write(" selected") end if%>><%= rsDados("nr_seql_cnd_ctc") %></option>
						<%
						rsDados.movenext
					wend
				else
					%><option value="#">-- Sem Observação --</option><%
					obs = " disabled style=""background-color:#F5F5F5;"""
				end if
				%>
			</select>
		</td>
	</tr>
	<input type="hidden" name="cd_tip_negr" value="2"></td>
	<tr>
		<td class="td_label" width="15%" valign=top>&nbsp;Texto Formal</td>
		<td class="td_dado">
			<iframe name="textoFormal" width=515 height=180 src="textoFormal.asp?seq=1" hidefocus marginheight=0 marginwidth=5 frameborder=0></iframe>
			<input type=hidden name="tx_lnh_ctcF" value="<%=Request("tx_lnh_ctcF")%>">
			<input type=hidden name="in_tx_fmal_ctcF" value="S" >
		</td>
	</tr>
	<tr>
		<td class="td_label" width="15%" valign=top>&nbsp;Texto Informal</td>
		<td class="td_dado">
			<iframe name="textoInformal" width=515 height=180 src="textoInformal.asp?seq=1" hidefocus marginheight=0 marginwidth=5 frameborder=0></iframe>
			<input type=hidden name="tx_lnh_ctcI" value="<%=Request("tx_lnh_ctcI")%>">
			<input type=hidden name="in_tx_fmal_ctcI" value="N" >
		</td>
	</tr>
</table>
<br>
<script language="JavaScript">
function mudaTexto( Sequencial ) {
	document.all.textoInformal.src = "textoInformal.asp?seq=" + Sequencial;
	document.all.textoFormal.src = "textoFormal.asp?seq=" + Sequencial;
}

</script>