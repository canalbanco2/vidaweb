<!--#include virtual = "/padrao.asp"-->
<br>
<script>
function processasubgrupo()
{
	try { document.formProc.NR_SEQL_CBT_CTC.selectedIndex = 0; } catch (ex) { }
	submitcobertura();
}

function submitcobertura()
{
	document.formProc.action = 'liberar_cotacao_cob.asp';
	document.formProc.submit();
}

function preencheCampos(numero){
	for(x=0;x<document.formProc.contador.value;x++) {
		document.getElementById('questionario' + x).style.display = 'none';
	}
	document.getElementById('questionario' + numero).style.display = '';
}

</script>
<%
Set rsFin = objcon.mLerDadosFinanceiros()
rsFin.ActiveConnection = Nothing
IN_FAT_UNCO_SGR = trata_sim(testaDadoVazio(rsFin,"IN_FAT_UNCO_SGR"))
set rsFin = Nothing

set rs = objcon.mLerCotacao
rs.ActiveConnection = Nothing
%>
<table cellspacing="0" border="0" width="100%" class="escolha" height="10">
<%
iContador = 0
set rsSubgrupo = objcon.mLerSubgrupoCotacao
rsSubGrupo.ActiveConnection = Nothing

If rsSubgrupo.EOF Then
	bSubgrupo = False
Else
	bSubgrupo = True
%>
	<!--tr>
		<td class="td_label" width="200">
			Taxa Personalizada por Subgrupo
		</td>
		<td class="td_dado">&nbsp;<%'IN_FAT_UNCO_SGR%></td>
	</tr-->
	<tr>
		<td class="td_label" width="200">
			Subgrupo
		</td>
		<td class="td_dado">&nbsp;
			<select name="NM_PRPN_CTC" onchange="processasubgrupo();">
			<option value="">--Escolha abaixo--</option>
			<%
			do while not rsSubgrupo.eof
				if trim(request("NM_PRPN_CTC")) = trim(rsSubgrupo("NR_CTC_SGRO")) & "|" & trim(rsSubgrupo("NR_VRS_CTC")) then
					sSelecionado = " selected"
				else
					sSelecionado = ""
				end if
				%>
				<option value="<%=trim(rsSubgrupo("NR_CTC_SGRO")) & "|" & trim(rsSubgrupo("NR_VRS_CTC"))%>"<%=sSelecionado%>><%=right(00 & rsSubgrupo("subgrupo_id"),2) & " (" & trim(rsSubgrupo("NR_CTC_SGRO_BB")) & ")"%> - <%=trim(rsSubgrupo("NM_PRPN_CTC"))%></option>
				<%
				rsSubgrupo.movenext
			loop
			%>
			</select>
		</td>
	</tr>
<%
	if trim(request("NM_PRPN_CTC")) <> "" then 
		arrSubGrupoCotacao = split(trim(request("NM_PRPN_CTC")),"|")

		objCon.pNR_CTC_SGRO = arrSubGrupoCotacao(0)
		objCon.pNR_VRS_CTC  = arrSubGrupoCotacao(1)
		NR_CTC_SGRO = objCon.pNR_CTC_SGRO
		NR_VRS_CTC  = objCon.pNR_VRS_CTC
	end if
end if
						
If  bSubgrupo Then 
    NR_SEQL_CBT_CTC = 1
Else
    NR_SEQL_CBT_CTC = Trim(Request("NR_SEQL_CBT_CTC"))

	set rsSeqlCobertura = objcon.mLerSeqlCobertura()
	rsSeqlCobertura.ActiveConnection = Nothing
%>	
	<tr>
		<td class="td_label" width="200">
			Objeto do Risco
		</td>
		<td class="td_dado">&nbsp;
			<select name="NR_SEQL_CBT_CTC" class="texto" onchange="submitcobertura();">
				<%
				If not rsSeqlCobertura.eof then
                   If NR_SEQL_CBT_CTC = "" Then
                      NR_SEQL_CBT_CTC = rsSeqlCobertura("NR_SEQL_CBT_CTC")
                   End If
					%><option value="">-- Escolha Abaixo --</option><%
					while not rsSeqlCobertura.eof
						%>
						<option value="<%=rsSeqlCobertura("NR_SEQL_CBT_CTC")%>" <% if trim(request("NR_SEQL_CBT_CTC")) = trim(rsSeqlCobertura("NR_SEQL_CBT_CTC")) OR rsSeqlCobertura.RecordCount = 1 then %> selected <%end if%>><%=rsSeqlCobertura("NR_SEQL_CBT_CTC")%></option>
						<%
						rsSeqlCobertura.movenext
					wend
				end if
				%>
			</select>
		</td>
	</tr>
</table>
<%
End If%>

<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
<%
   objCon.pNR_SEQL_CBT_CTC = NR_SEQL_CBT_CTC
   
   set rsCobertura = objcon.mLerDadosCobertura()
   rsCobertura.ActiveConnection = Nothing

   If rsCobertura.eof Then   
%>
	<tr>
		<td class="td_label">&nbsp;Nenhuma cobertura relacionada.</td>
		</tr>
  </table>
<% Else%>
<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
	<tr>
		<td class="td_label" width="20"></td>
		<td class="td_label" width="25"><b>C�d. Cob</b></td>
		<td class="td_label" width="500"><b>Cobertura</b></td>
		<td class="td_label" width="70"><b>% da cob</b></td>
		<td class="td_label" width="70"><b>Taxa NET</b></td>
		<td class="td_label"><b>Pr�mio NET</b></td>
	</tr>
</table>
<DIV style="OVERFLOW: scroll; WIDTH: 100%; HEIGHT: 254px">
<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
	<%
	  iQtdeCoberturas = 0
	  if not rsCobertura.eof then
		 do while not rsCobertura.eof
			iQtdeCoberturas = iQtdeCoberturas + 1	
			rsCobertura.movenext
		 loop
		
		 rsCobertura.movefirst
	  end if
	
	  do while not rsCobertura.eof
		 alterna_cor %>
		<tr id="tr<%=testaDadoVazioN(rsCobertura,"NR_SEQL_CBT_CTC")%>_<%=iContador%>">
			<td width="20" class="td_dado" style="text-align:center<%=cor_zebra%>">
				<%
				objcon.pCD_CBT = rsCobertura("cd_cbt")
				objcon.pCD_TIP_QSTN = "3"
				set rsTemp = objcon.mLerQuestionarioId()
				rsTemp.ActiveConnection = Nothing
				if not rsTemp.eof then
					objcon.pNR_QSTN = rsTemp("NR_QSTN")
					set rsTemp1 = objcon.mLerLocalRisco()
					rsTempl.ActiveConnection = Nothing
					if not rsTemp1.eof then
						if trim(rsTemp1("NR_CD_TIP_QSTN_CTC")) <> "0" and trim(rsTemp1("NR_CD_TIP_QSTN_CTC")) <> "" then%>
							<input type="radio" name="R" onClick="preencheCampos(<%= iContador%>)" style="cursor:hand;">
						<%
						else
							Response.Write("&nbsp;")
						end if
					end if
					set rsTemp1 = nothing
				end if
				set rsTemp = nothing%>
			</td>
			<td width="25" class="td_dado" style="<%=cor_zebra%>">
				<input type="hidden" name="CD_CBT<%= iContador%>" value="<% = testaDadoVazioN(rsCobertura,"CD_CBT") %>">
				&nbsp;<%= testaDadoVazioN(rsCobertura,"CD_CBT")%>
			</td>
			<td width="500" class="td_dado" style="<%=cor_zebra%>">
				<input type="hidden" name="nome<%= iContador%>" value="<%= testaDadoVazioN(rsCobertura,"nome")%>">
				<input type="hidden" name="NR_SEQL_CBT_CTC<%= iContador%>" value="<%= testaDadoVazioN(rsCobertura,"NR_SEQL_CBT_CTC")%>">
				<input type="hidden" name="DT_INC_VCL<%= iContador%>" value="<%= testaDadoVazioN(rsCobertura,"DT_INC_VCL")%>">
				&nbsp;<%= testaDadoVazioN(rsCobertura,"nome")%>
			</td>
			<td width="70" class="td_dado" style="<%=cor_zebra%>">
				<div align="right" id="PC_IPTC_CBT_CTC<%= iContador%>"><%= testaDadoVazioN(rsCobertura,"PC_IPTC_CBT_CTC") %></div>
			</td>
			<%
			if iContador = 0 then
				set rsDadosFin = objCon.mLerDadosFinanceiros(2)
				rsDadosFin.ActiveConnection = Nothing
				
				percentual_da = cdbl("0" & testaDadoVazioN(rsDadosFin,"PC_DA_CTC"))
				valor_premio_puro = cdbl("0" & testaDadoVazioN(rsDadosFin,"VL_PREM_PURO"))
				valor_is = cdbl("0" & testaDadoVazioN(rsDadosFin,"VL_IPTC_MOEN_CTC"))
				valor_premio_net = cdbl("0" & testaDadoVazioN(rsDadosFin,"VL_SGRA_MOEN_CTC"))
				
				if valor_premio_net > 0 and valor_is > 0 then
					taxa_net = CortaCasasDecimais(((valor_premio_net / valor_is) * 100), 4)
				else
					taxa_net = 0
				end if
			%>
			<td width="70" class="td_dado" style="<%=cor_zebra%>" rowspan="<%=iQtdeCoberturas%>">
				<div align="right" id="lpc_prem_cbt_ctc<%= iContador%>"><%=formatnumber(taxa_net, 4)%></div>
			</td>
			<td class="td_dado" style="<%=cor_zebra%>" rowspan="<%=iQtdeCoberturas%>">
				<div align="right" id="lvl_prem_cbt_ctc<%= iContador%>"><%=formatnumber(valor_premio_net,2)%></div>
			</td>
			<%end if%>
		</tr>
		<input type="hidden" value="" name="exibe<%=iContador%>">
		<%
		  iContador = iContador + 1
	  	  rsCobertura.movenext
	  loop%>
	</table>
	</div>
	<input type="hidden" name="contador" value="<% = iContador %>">
	<br>
	<!--#include virtual = "/internet/serv/siscot3/funcoes_dados_qst.asp"-->
	<%
	htmlExibir = ""
	rsCobertura.movefirst
	contador = 0
	do while not rsCobertura.eof
		objcon.pCD_CBT = rsCobertura("CD_CBT")
		objcon.pCD_TIP_QSTN = "3"
		set rsQST = objcon.mLerQuestionarioId()
		rsQST.ActiveConnection = Nothing
		%><table border="0" id="questionario<% = contador %>" style="display:none;"><tr><td><%
		if not rsQST.eof then
			questionario_id = rsQST("NR_QSTN")
			objcon.pNR_QSTN = questionario_id
			set rsNomeQSTN = objcon.mLerLocalRisco()
			rsNomeQSTN.ActiveConnection = Nothing	
			If not rsNomeQSTN.eof Then
				if trim(rsNomeQSTN("NR_CD_TIP_QSTN_CTC")) <> "0" and trim(rsNomeQSTN("NR_CD_TIP_QSTN_CTC")) <> "" then
					NR_SEQL_QSTN_CTC = rsNomeQSTN("NR_SEQL_QSTN_CTC")
					objCon.pNR_SEQL_QSTN_CTC = NR_SEQL_QSTN_CTC
					htmlExibir = "<font face=""arial"" size=""2""><b>" & rsNomeQSTN("nome") & "</b></font>"
					%>
					
					<!--#include virtual = "/internet/serv/siscot3/dados_qst.asp"-->
					<%
				end if
			end if
		end if%>
		</td></tr></table>
		<%
		rsCobertura.movenext
		contador = contador + 1
	loop
	rsCobertura.movefirst
 End If%>
<br>