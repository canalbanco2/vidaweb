<!--include virtual = "/padrao.asp"-->
<!--#include virtual = "/internet/serv/siscot2/classe/citemtreeview.asp"-->
<script>
	var taxaNet = "";
	function rValorFocus(){
		taxaNet = document.formProc.pc_prem_cbt_ctc.value;
	}
	function rValorBlur(){
		if(taxaNet!=document.formProc.pc_prem_cbt_ctc.value){
			reclaculaPremioNet();
		}
	}
	function reclaculaPremioNet(){
		vl_iptc_cbt_ctc = document.formProc.vl_iptc_cbt_ctc.value;
		pc_prem_cbt_ctc = document.formProc.pc_prem_cbt_ctc.value;	
		if((vl_iptc_cbt_ctc!="")&&(pc_prem_cbt_ctc!="")){
			vl_iptc_cbt_ctc = converteNumeroJavascript(vl_iptc_cbt_ctc) * 10000;
			pc_prem_cbt_ctc = converteNumeroJavascript(pc_prem_cbt_ctc) * 10000;
			vl_prem_cbt_ctc = (vl_iptc_cbt_ctc * pc_prem_cbt_ctc)/10000000000;
			vl_prem_cbt_ctc = converteNumeroJavascriptTruncado(vl_prem_cbt_ctc, 2);
			vl_prem_cbt_ctc = FormatNumber(vl_prem_cbt_ctc.replace('.',','), 2, 3);
			document.formProc.vl_prem_cbt_ctc.value = vl_prem_cbt_ctc;
			
		}
	}
	function converteNumeroJavascriptTruncado(valor,casas){
		valor = valor + '';
		valor = valor.split('.');
		valorNovo = '';
		for(f=0;f<valor[0].length;f++){
			if((valor[0].charCodeAt(f)>=48)&&(valor[0].charCodeAt(f)<=57)){
				valorNovo=valorNovo+valor[0].charAt(f);
			}
		}
		valorNovo = valorNovo + '.';
		
		for(f=0;f<casas;f++){
			if(valor.length>1){
				if(valor[1].length>f){
					valorNovo = valorNovo + valor[1].charAt(f);
				} else {
					valorNovo = valorNovo + '0';
				}
			} else {
				valorNovo = valorNovo + '0';
			}
		}
		return valorNovo;
	}
	function converteNumeroJavascript(valor){
		valor = valor.split(',');
		valorNovo = '';
		for(f=0;f<valor[0].length;f++){
			if((valor[0].charCodeAt(f)>=48)&&(valor[0].charCodeAt(f)<=57)){
				valorNovo=valorNovo+valor[0].charAt(f);
			}
		}
		if(valor.length>1){
			valorNovo = valorNovo + '.' + valor[1];
		}
		return valorNovo;
	}
	function preencheCampos(numero){
		document.formProc.numero.value = numero;
		document.formProc.nome.value = document.formProc["nome" + numero].value;
		document.formProc.vl_iptc_cbt_ctc.value = document.formProc["vl_iptc_cbt_ctc" + numero].value;
		aFranquia = franquias[numero].split('|');
		apagaOptions(document.formProc.tx_rgr_frqu);
		for(f=0;f<aFranquia.length;f++){
			colocaOption(document.formProc.tx_rgr_frqu,aFranquia[f]);
		}
		selecionaOption(document.formProc.tx_rgr_frqu,document.formProc["tx_rgr_frqu" + numero].value);
		//document.formProc.tx_rgr_frqu.value = document.formProc["tx_rgr_frqu" + numero].value;
		document.formProc.pc_prem_cbt_ctc.value = document.formProc["pc_prem_cbt_ctc" + numero].value;
		document.formProc.vl_prem_cbt_ctc.value = document.formProc["vl_prem_cbt_ctc" + numero].value;
	}
	function selecionaOption(obj,valor){
		for(f=0;f<obj.length;f++){
			if(obj[f].value == valor){
				obj[f].selected = true;
			}
		}
	}
	function colocaOption(obj,valor){
		option = new Option('value','text');
		option.value = valor;
		option.text = valor;
		obj[obj.length] = option;
	}
	function apagaOptions(obj){
		while(obj.length>0){
			obj[0] = null;
		}
	}
	function alterarDados(){
		numero = document.formProc.numero.value;
		if(numero!=""){
			document.formProc["tx_rgr_frqu" + numero].value = document.formProc.tx_rgr_frqu.value;
			document.formProc["pc_prem_cbt_ctc" + numero].value = document.formProc.pc_prem_cbt_ctc.value;
			document.formProc["vl_prem_cbt_ctc" + numero].value = document.formProc.vl_prem_cbt_ctc.value;
			
			tx_rgr_frqu = document.getElementById("ltx_rgr_frqu" + numero);
			opc_prem_cbt_ctc = document.getElementById("lpc_prem_cbt_ctc" + numero);
			ovl_prem_cbt_ctc = document.getElementById("lvl_prem_cbt_ctc" + numero);
			
			tx_rgr_frqu.innerText = document.formProc.tx_rgr_frqu.value;
			opc_prem_cbt_ctc.innerText = document.formProc.pc_prem_cbt_ctc.value;
			ovl_prem_cbt_ctc.innerText = document.formProc.vl_prem_cbt_ctc.value;
			
			document.formProc.numero.value = "";
			document.formProc.nome.value = "";
			document.formProc.vl_iptc_cbt_ctc.value = "";
			apagaOptions(document.formProc.tx_rgr_frqu);
			//document.formProc.tx_rgr_frqu.value = "";
			document.formProc.pc_prem_cbt_ctc.value = "";
			document.formProc.vl_prem_cbt_ctc.value = "";
		}
	}
</script>

<br>
<%
'Set rsSeqlCobertura = objcon.mLerSeqlCobertura()
'rsSeqlCobertura.ActiveConnection = Nothing
iContador = 0
%>

<script>
	var franquias = new Array();
	var valoresCobertura = new Array(<% = ArrayJS %>);
	function apagaCelulas(){
		for(f=0;f<valoresCobertura.length;f++){
			document.getElementById('tr' +  valoresCobertura[f]).style.display = 'none';
			document.getElementById('questionario' +  valoresCobertura[f]).style.display = 'none';
		}
	}
	function acendeCelulas(ID){
		apagaCelulas();
		if(ID!=""){
			document.getElementById('tr' + ID).style.display = '';
			document.getElementById('questionario' + ID).style.display = '';
		}
	}
</script>
<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
<%
If bLMI Then
   Set rsCobertura = objcon.mLerDadosCoberturaLmi()
   rsCobertura.ActiveConnection = Nothing
Else
   Set rsCobertura = objcon.mLerDadosCobertura()
   rsCobertura.ActiveConnection = Nothing
End If
if not rsCobertura.eof then%>
		<tr>
			<td class="td_label" width="20"></td>
			<td class="td_label"><b>C�d. Cob</b></td>
			<td class="td_label"><b>Cobertura</b></td>
			<td class="td_label"><b>Valor IS</b></td>
			<td class="td_label"><b>Franquia</b></td>
			<td class="td_label"><b>Taxa NET</b></td>
			<td class="td_label"><b>Pr�mio NET</b></td>
		</tr>
	<%
	Do while not rsCobertura.eof
		if trim(nr_seql_cbt_ctc) <> trim(rsCobertura("nr_seql_cbt_ctc")) then
			nr_seql_cbt_ctc = rsCobertura("nr_seql_cbt_ctc")
			%>
			<td colspan="7" class="td_label">Risco <%=nr_seql_cbt_ctc%></td>
			<%
		end if
		
		alterna_cor
		if request("contador") <> "" then
			tx_rgr_frquAux = server.HTMLEncode(request("tx_rgr_frqu" & iContador))
			pc_prem_cbt_ctc = request("pc_prem_cbt_ctc" & iContador)
			vl_prem_cbt_ctc = request("vl_prem_cbt_ctc" & iContador)
		else
			'tx_rgr_frqu = "" 'server.HTMLEncode(testaDadoVazioN(rsCobertura,"tx_rgr_frqu"))
			tx_rgr_frquAux = ""
			pc_prem_cbt_ctc = testaDadoVazioFormataN(rsCobertura,"pc_prem_cbt_ctc",4)
			vl_prem_cbt_ctc = testaDadoVazioFormataN(rsCobertura,"vl_prem_cbt_ctc",2)
		end if
	%>
		<tr id="tr<% = testaDadoVazioN(rsCobertura,"NR_SEQL_CBT_CTC") %>">
			<td class="td_dado" style="<%=cor_zebra%>" width="20">				
			</td>		
			<td class="td_dado" style="<%=cor_zebra%>">
				<input type="hidden" name="CD_CBT<%= iContador%>" value="<% = testaDadoVazioN(rsCobertura,"CD_CBT") %>">
				&nbsp;<%= testaDadoVazioN(rsCobertura,"CD_CBT")%>
			</td>
			<td class="td_dado" style="<%=cor_zebra%>">
				<input type="hidden" name="nome<%= iContador%>" value="<%= testaDadoVazioN(rsCobertura,"nome")%>">
				<input type="hidden" name="NR_SEQL_CBT_CTC<%= iContador%>" value="<%= testaDadoVazioN(rsCobertura,"NR_SEQL_CBT_CTC")%>">
				<input type="hidden" name="DT_INC_VCL<%= iContador%>" value="<%= testaDadoVazioN(rsCobertura,"DT_INC_VCL")%>">
				&nbsp;<%= testaDadoVazioN(rsCobertura,"nome")%>
			</td>
			<td class="td_dado" style="<%=cor_zebra%>">
				<input type="hidden" name="vl_iptc_cbt_ctc<%= iContador%>" value="<%= testaDadoVazioFormataN(rsCobertura,"vl_iptc_cbt_ctc",2)%>">
				&nbsp;<%= testaDadoVazioFormataN(rsCobertura,"vl_iptc_cbt_ctc",2)%>
				<!--input type="hidden" name="tx_rgr_frqu<%= iContador%>" value="<%= trim(tx_rgr_frqu) %>"-->
				<input type="hidden" name="pc_prem_cbt_ctc<%= iContador%>" value="<%= pc_prem_cbt_ctc %>">
				<input type="hidden" name="vl_prem_cbt_ctc<%= iContador%>" value="<%= vl_prem_cbt_ctc %>">
			</td>
			<td class="td_dado" style="<%=cor_zebra%>">
			<%
			objcon.pCD_CBT = rsCobertura("CD_CBT")
			set rsFranquia = objcon.mLerDadosFranquia
			rsFranquia.ActiveConnection = Nothing
			tx_rgr_frquFim = ""
			tx_rgr_frquTotal = ""
			if not rsFranquia.eof then
				Response.Write testaDadoVazio(rsCobertura,"TX_FRQU_CTC")
			else
				Response.Write("-- N�o h� franquias --")
			end if
			%>
			<input type="hidden" name="tx_rgr_frqu<%= iContador%>" value="<%= tx_rgr_frquFim %>">
			<span align="right" id="ltx_rgr_frqu<%= iContador%>"><%= tx_rgr_frquFim %></span>
			<%
			 else %>
				<input type="hidden" name="tx_rgr_frqu<%= iContador%>" value="">
				<span align="right" id="ltx_rgr_frqu<%= iContador%>">-- n�o h� franquias --</span>
			<% end if%>
				<script>
					franquias[<%= iContador%>] = '<% = tx_rgr_frquTotal %>';
				</script>
			</td>
			<td class="td_dado" style="<%=cor_zebra%>">
				<div align="right" id="lpc_prem_cbt_ctc<%= iContador%>"><%= pc_prem_cbt_ctc %></div>
			</td>
			<td class="td_dado" style="<%=cor_zebra%>">
				<div align="right" id="lvl_prem_cbt_ctc<%= iContador%>"><%= vl_prem_cbt_ctc %></div>
			</td>
		</tr>
		<%
		iContador = iContador + 1
		rsCobertura.MoveNext
	Loop%>
	</table>	
	<!-- #include virtual = "/internet/serv/siscot3/funcoes_dados_qst.asp"-->
<%
rsCobertura.MoveFirst
contador = 1
Do while NOT rsCobertura.EOF
	objcon.pCD_CBT = rsCobertura("CD_CBT")
	objcon.pCD_TIP_QSTN = "3"
	Set rsQST = objcon.mLerQuestionarioId()
	rsQST.ActiveConnection = Nothing
    If NOT rsQST.EOF Then
	  'objcon.pNR_SEQL_QSTN_CTC = request("questionario_id")
	   objcon.pNR_QSTN = rsQST(0)
 	   Set rsNomeQSTN = objcon.mLerLocalRisco()
 	   rsNomeQSTN.ActiveConnection = Nothing
%>
<table border="0" id="questionario<% = contador %>" style="display:none;"><tr><td>
<%
	   If NOT rsNomeQSTN.EOF Then
	      objcon.pNR_SEQL_QSTN_CTC = rsNomeQSTN("NR_SEQL_QSTN_CTC")
	      questionario_id = rsQST(0)
	      questionario_id2 = rsQST(0)
%>
	<font face="arial" size="2"><b><% = rsNomeQSTN("nome") %></b></font>
	<!-- #include virtual = "/internet/serv/siscot3/dados_qst.asp"-->
<%     End If
	End If
%>
</td></tr></table>
<%
	rsCobertura.movenext
	contador = contador + 1
loop
else%>
		<tr>
			<td class="td_label">&nbsp;Nenhuma cobertura relacionada.</td>
		</tr>
	</table>
<%end if%>
<script>
	apagaCelulas()
</script>
<br>