<%
 on error resume next
'on error goto 0
 Set rs = objcon.mLerDadosFinanceiros()
 rs.ActiveConnection = Nothing

 If Trim(Request("salvarfin")) = "S" Then
    IN_FAT_UNCO_SGR  = trim(Request("IN_FAT_UNCO_SGR"))
	PC_CRE_PDRO      = trim(Request("PC_CRE_PDRO"))
	PC_IOF_CTC       = trim(Request("PC_IOF_CTC"))
	VL_IOF_CTC       = trim(Request("VL_IOF_CTC"))
	QT_PCL_PGTO_CTC  = trim(Request("QT_PCL_PGTO_CTC"))
	PC_ETLE_CTC      = trim(Request("PC_ETLE_CTC"))
	VL_LQDO_MOEN_CTC = trim(Request("VL_LQDO_MOEN_CTC"))
	PC_DA_CTC        = trim(Request("PC_DA_CTC"))
	VL_PREM_PURO     = trim(Request("VL_PREM_PURO"))
	VL_SGRA_MOEN_CTC = trim(Request("VL_SGRA_MOEN_CTC"))
	VL_IPTC_MOEN_CTC = trim(Request("VL_IPTC_MOEN_CTC"))
	PC_MED_SGR       = trim(Request("PC_MED_SGR"))
 Else
	IN_FAT_UNCO_SGR  = trata_sim(testaDadoVazio(rs,"IN_FAT_UNCO_SGR"))
	PC_CRE_PDRO      = testaDadoVazioN(rs,"PC_CRE_CTC")
	PC_IOF_CTC       = testaDadoVazioN(rs,"PC_IOF_CTC")
	VL_IOF_CTC       = testaDadoVazioN(rs,"VL_IOF_CTC")
	QT_PCL_PGTO_CTC  = testaDadoVazioN(rs,"QT_PCL_PGTO_CTC")
	PC_ETLE_CTC      = testaDadoVazioN(rs,"PC_ETLE_CTC")
	VL_LQDO_MOEN_CTC = testaDadoVazioN(rs,"VL_LQDO_MOEN_CTC")
	PC_DA_CTC        = testaDadoVazioN(rs,"PC_DA_CTC")
	VL_PREM_PURO     = testaDadoVazioN(rs,"VL_PREM_PURO")
	VL_SGRA_MOEN_CTC = testaDadoVazioN(rs,"VL_SGRA_MOEN_CTC")
	VL_IPTC_MOEN_CTC = testaDadoVazioN(rs,"VL_IPTC_MOEN_CTC")
	PC_MED_SGR       = testaDadoVazioN(rs,"PC_MED_SGR")
 End If

 Set rsCotacao = objcon.mLerCotacao()
 rsCotacao.ActiveConnection = Nothing
 data_fim_vigencia = rsCotacao("DT_INC_VGC_CTC")
 data_inicio_vigencia = rsCotacao("DT_FIM_VGC_CTC")
 set rsCotacao = nothing

'Verifica o n�mero m�x. de parcelas aceitos no cadastro de produto
 objcon.pTP_PESQ = "BB"
 qtd_max = objcon.mLerQtdMaxPclSeguro()

 If not isnull(data_fim_vigencia) and not isnull(data_inicio_vigencia) Then
	dias = datediff("d",data_fim_vigencia,data_inicio_vigencia)
	parcelas = int(dias/30)
	If parcelas > qtd_max Then parcelas = qtd_max end If
 Else
	dias = 0
	parcelas = QT_PCL_PGTO_CTC
 End If

 If trim(VL_IPTC_MOEN_CTC) = "" Then
	VL_IPTC_MOEN_CTC = "0,00"
 End If
 If trim(VL_PREM_MOEN_CTC) = "" Then
	VL_PREM_MOEN_CTC = "0,00"
 End If
%>
	<input type="hidden" name="salvarfin" value="">
<%
 strMensagem = ""

 Set rsSubgrupo = objcon.mLerSubgrupoCotacao
 rsSubgrupo.ActiveConnection = Nothing
 If rsSubgrupo.EOF Then
	bSubgrupo = False
 Else
	bSubgrupo = True
 End If

 If Trim(Request("salvarfin")) = "S" Then
	Set rsCotacao = objCon.mLerCotacao
    rsCotacao.ActiveConnection = Nothing

	If ucase(rsCotacao("CD_ULT_EVT_CTC")) = "T" Then
		bAtualizaCotacao = False
	Else
		objCon.pCD_ULT_EVT_CTC = "T"
		bAtualizaCotacao = True
	End If

   'Verifica se o produto aceita averba��o
    objCon.pTP_PESQ = "BB"
    vAceitaAverbacao = UCase(objCon.mLerIndAceitaAverbacao())
    If vAceitaAverbacao = "S" Then
       objCon.pPER_FAT_SEGURO = Request("per_fat_seguro")
    Else
       objCon.pPER_FAT_SEGURO = 1
    End If
	objCon.pVL_IOF_CTC = Abs(VL_IOF_CTC)
	objCon.pQT_PCL_PGTO_CTC = Abs(QT_PCL_PGTO_CTC)
	objCon.pPC_ETLE_CTC = Abs(PC_ETLE_CTC)

	objCon.pPC_DA_CTC = Abs(PC_DA_CTC)
	objCon.pVL_PREM_PURO = Abs(VL_PREM_PURO)
	objCon.pVL_SGRA_MOEN_CTC = Abs(VL_SGRA_MOEN_CTC)
	objCon.pVL_IPTC_MOEN_CTC = Abs(VL_IPTC_MOEN_CTC)
	objCon.pVL_LQDO_MOEN_CTC = Abs(VL_LQDO_MOEN_CTC)
	objCon.pPC_MED_SGR = Abs(PC_MED_SGR)

	If bSubgrupo Then
		NR_CTC_SGRO_subgrupo = Request("NR_CTC_SGRO_subgrupo")
		NR_VRS_CTC_subgrupo  = Request("NR_VRS_CTC_subgrupo")
	Else
		NR_CTC_SGRO_subgrupo = ""
		NR_VRS_CTC_subgrupo  = ""
	End If

    preencheChaves
    objCon.pWF_ID = WF_ID
	retorno = objCon.mAtualizarDadosFinanceiros(NR_CTC_SGRO_subgrupo, _
												NR_VRS_CTC_subgrupo, _
												trim(request("valor_is_total")), _
												trim(request("valor_premio_puro_total")), _
												trim(request("valor_premio_liquido_total")), _
												trim(request("valor_premio_net_total")), _
												trim(request("taxa_media_mensal")), _
												bAtualizaCotacao)
	If Trim(retorno) <> "" Then
		Response.redirect("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
		Response.End 
	End If

	chave

	objCon.pWF_ID = WF_ID
	retorno = objCon.mAlterarPercentualCorretagem(PC_CRE_PDRO)

	If Trim(retorno) <> "" Then
		Response.redirect("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
		Response.End 
	End If

	chave

	If NOT bSubgrupo Then
		Set rs = objcon.mLerDadosFinanceiros(2) '2 = VIDA
		rs.ActiveConnection = Nothing
	End If
 End If
%>
	<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
		<tr>
			<td width="25%" nowrap class="td_label">&nbsp;Moeda do seguro da cota��o</td>
			<td width="25%" class="td_dado">&nbsp;
				<%=testaDadoVazio(rs,"CD_MOE_SGRO_CTC")%> - <%=testaDadoVazio(rs,"NOME_MOE_SGRO")%>
			</td>
			<td width="25%"></td>
			<td width="25%"></td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Moeda de origem da cota��o</td>
			<td class="td_dado">&nbsp;
				<%=testaDadoVazio(rs,"CD_MOE_OGM_CTC")%> - <%=testaDadoVazio(rs,"NOME_MOE_OGM")%>
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual de corretagem</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_CRE_PDRO" onfocus="onfocusTeste(this)" onblur="onblurTeste(this);calculaValores();" size="16" value="<%=PC_CRE_PDRO%>" maxlength="16" style="text-align:right;" onKeyUp="FormatValorContinuo(this,2);">
			</td>
			<td class="td_dado"><input type="button" name="alterar" value="Gravar" style="width:80px;font-size:10px;margin: 2px 2px 2px 2px;" onclick="alterarDados();"></td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual IOF</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_IOF_CTC" size="16" value="<%=testaDadoVazioN(rs,"PC_IOF_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
			
			<td nowrap class="td_label">&nbsp;Qtd m�xima de parcelas pgto</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="QT_PCL_PGTO_CTC" size="2" value="<%=QT_PCL_PGTO_CTC%>" maxlength="2" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<%
		If NOT bSubgrupo Then
			percentual_corretagem = cdbl("0" & testaDadoVazioN(rs,"PC_CRE_CTC"))
			percentual_iof = cdbl("0" & testaDadoVazioN(rs,"PC_IOF_CTC"))
			percentual_da = cdbl("0" & testaDadoVazioN(rs,"PC_DA_CTC"))
			percentual_pro_labore = cdbl("0" & testaDadoVazioN(rs,"PC_ETLE_CTC"))
			valor_premio_puro = cdbl("0" & testaDadoVazioN(rs,"VL_PREM_PURO"))
			valor_is = cdbl("0" & testaDadoVazioN(rs,"VL_IPTC_MOEN_CTC"))
			valor_premio_liquido = cdbl("0" & testaDadoVazioN(rs,"VL_LQDO_MOEN_CTC"))
			valor_premio_net = cdbl("0" & testaDadoVazioN(rs,"VL_SGRA_MOEN_CTC"))
			per_fat_seguro =  cdbl("0" & testaDadoVazioN(rs,"PER_FAT_SEGURO"))

			valor_iof = CortaCasasDecimais(((valor_premio_puro * percentual_iof) / 100), 2)

			If percentual_pro_labore > 0 and valor_premio_liquido > 0 Then
				valor_pro_labore = (valor_premio_net / (1 - ((percentual_pro_labore/100) + (percentual_corretagem/100)))) * percentual_pro_labore/100
				valor_pro_labore = CortaCasasDecimais(valor_pro_labore,5)
			Else
				valor_pro_labore = 0
			End If

			if valor_premio_liquido > 0 and valor_is > 0 and per_fat_seguro > 0 then
				taxa_comercial = CortaCasasDecimais(((valor_premio_liquido * per_fat_seguro) / valor_is) * 1000, 4)
			else
				taxa_comercial = 0
			end if
			
			valor_premio_puro_total = valor_premio_puro
			valor_premio_liquido_total = valor_premio_liquido
			valor_premio_net_total = valor_premio_net
			valor_is_total = valor_is

			if valor_is_total > 0 then
				taxa_media_mensal = CortaCasasDecimais(((valor_premio_liquido_total / valor_is_total) * 1000), 4)
			else
				taxa_media_mensal = 0
			end if
		End If %>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor IOF</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_IOF_CTC" size="16" value="<%=testaDadoVazioN(rs,"VL_IOF_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>

			<td nowrap class="td_label">&nbsp;Taxa Personalizada por subgrupo</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="IN_FAT_UNCO_SGR" size="3" value="<%=IN_FAT_UNCO_SGR%>" style="text-align:right; background-color:#EEEEEE;" readonly ID="Text1">
			</td>

		</tr>
		<tr>
			<td colspan="4" height="10"></td>
		</tr>
		<%If bSubgrupo Then%>
		<tr>
			<td colspan="4">
				<div style="position:relative; overflow:scroll; width:91%; height:110px;">
					<input type="hidden" name="subgrupo" value="">
					<table cellpadding="1" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
						<tr>
							<td class="td_label" width="15%"><div align="center"><b>Subgrupo</b></div></td>
							<td class="td_label" width="17%"><div align="center"><b>Valor IS</b></div></td>
							<td class="td_label" width="17%"><div align="center"><b>Pr�mio net</b></div></td>
							<td class="td_label" width="17%"><div align="center"><b>Pr�mio L�quido</b></div></td>
							<td class="td_label" width="17%"><div align="center"><b>Periodicidade</b></div></td>
							<td class="td_label" width="17%"><div align="center"><b>Pro-labore (%)</b></div></td>
						</tr>
						<%
							iContador = 0
			
							percentual_corretagem = cdbl("0" & testaDadoVazioN(rs,"PC_CRE_CTC"))
							percentual_iof = cdbl("0" & testaDadoVazioN(rs,"PC_IOF_CTC"))

							valor_premio_puro_total = 0
							valor_premio_liquido_total = 0
							valor_is_total = 0
							valor_premio_net_total = 0
							taxa_media_mensal = 0
                            valor_iof = 0

							Do While NOT rsSubgrupo.EOF
								alterna_cor
								iContador = iContador + 1
								
								objCon.pNR_CTC_SGRO = rsSubgrupo("NR_CTC_SGRO")
								objCon.pNR_VRS_CTC =  rsSubgrupo("NR_VRS_CTC")
								
								set rsDadosFin = objCon.mLerDadosFinanceiros
								rsDadosFin.ActiveConnection = Nothing

                                percentual_cor= cdbl("0" & testaDadoVazioN(rsDadosFin,"PC_CRE_CTC"))
								percentual_da = cdbl("0" & testaDadoVazioN(rsDadosFin,"PC_DA_CTC"))
								percentual_pro_labore = cdbl("0" & testaDadoVazioN(rsDadosFin,"PC_ETLE_CTC"))
								valor_premio_puro = cdbl("0" & testaDadoVazioN(rsDadosFin,"VL_PREM_PURO"))
								valor_premio_net = cdbl("0" & testaDadoVazioN(rsDadosFin,"VL_SGRA_MOEN_CTC"))
								valor_is = cdbl("0" & testaDadoVazioN(rsDadosFin,"VL_IPTC_MOEN_CTC"))
								valor_premio_liquido = cdbl("0" & testaDadoVazioN(rsDadosFin,"VL_LQDO_MOEN_CTC"))
								per_fat_seguro =  cdbl("0" & testaDadoVazioN(rsDadosFin,"PER_FAT_SEGURO"))
								
								valor_iof = valor_iof + CortaCasasDecimais(((valor_premio_liquido * percentual_iof) / 100), 2)

								if percentual_pro_labore > 0 and valor_premio_liquido > 0 then
				                  	valor_pro_labore = (valor_premio_net / (1 - ((percentual_pro_labore/100) + (percentual_cor/100)))) * percentual_pro_labore/100
								    valor_pro_labore = CortaCasasDecimais(valor_pro_labore,5)
								else
									valor_pro_labore = 0
								end if
								
								if valor_premio_liquido > 0 and valor_is > 0 and per_fat_seguro > 0 then
									taxa_comercial = CortaCasasDecimais(((valor_premio_liquido * per_fat_seguro) / valor_is) * 1000, 4)
								else
									taxa_comercial = 0
								end if
								
								If taxa_comercial > 0 Then
									If per_fat_seguro = 1 Then
										taxa_media_mensal = CortaCasasDecimais(cdbl(taxa_media_mensal) + cdbl(taxa_comercial), 4)
									Else
										taxa_media_mensal = CortaCasasDecimais(cdbl(taxa_media_mensal) + ((valor_premio_liquido / valor_is) * 1000), 4)
									End If
							    End If
							    							    
 			                    valor_premio_puro_total = valor_premio_puro_total + valor_premio_puro
			                    valor_premio_liquido_total = valor_premio_liquido_total + valor_premio_liquido
			                    valor_premio_net_total = valor_premio_net_total + valor_premio_net
 			                    valor_is_total = valor_is_total + valor_is				
						%>
						<tr style="<%=cor_zebra%>">
							<td class="td_dado" width="15%"><div align="center"><%=FormatNumber(rsSubgrupo("SUBGRUPO_ID"), 0)%></div></td>
							<td class="td_dado" width="17%"><div align="right" id="VL_IPTC_MOEN_CTC<%=iContador%>"><%=FormatNumber(valor_is, 2)%></div></td>
							<td class="td_dado" width="17%"><div align="right"><%=FormatNumber(CortaCasasDecimais((valor_premio_puro * per_fat_seguro), 2), 2)%></div></td>
							<td class="td_dado" width="17%"><div align="right"><%=FormatNumber(CortaCasasDecimais((valor_premio_liquido * per_fat_seguro), 2), 2)%></div></td>
							<td class="td_dado" width="17%"><div align="center"><%=RetornaPeriodicidade(per_fat_seguro, vAceitaAverbacao)%></div></td>
							<td class="td_dado" width="17%"><div align="right" id="PC_ETLE_CTC<%=iContador%>"><%=FormatNumber(percentual_pro_labore, 2)%></div>
							
			                <div style="display:none" id="PC_DA_CTC<%=iContador%>"><%=FormatNumber(percentual_da, 2)%></div>
			                <div style="display:none" id="PC_COR_CTC<%=iContador%>"><%=FormatNumber(percentual_cor, 2)%></div>
							<div style="display:none" id="VL_LQDO_MOEN_CTC<%=iContador%>"><%=FormatNumber(valor_premio_liquido, 2)%></div>
							<div style="display:none" id="VL_PREM_PURO<%=iContador%>"><%=FormatNumber(valor_premio_puro, 2)%></div>
							<div style="display:none" id="valor_iof<%=iContador%>"><%=FormatNumber(valor_iof, 2)%></div>
							<div style="display:none" id="taxa_comercial<%=iContador%>"><%=FormatNumber(taxa_comercial, 4)%></div>
							<div style="display:none" id="per_fat_seguro<%=iContador%>"><%=FormatNumber(per_fat_seguro, 0)%></div>
							<div style="display:none" id="valor_premio_net<%=iContador%>"><%=FormatNumber(valor_premio_net, 2)%></div>
							<input type="hidden" name="NR_CTC_SGRO_subgrupo<%=iContador%>" value="<%=FormatNumber(rsSubgrupo("NR_CTC_SGRO"), 0)%>">
							<input type="hidden" name="NR_VRS_CTC_subgrupo<%=iContador%>" value="<%=FormatNumber(rsSubgrupo("NR_VRS_CTC"), 0)%>">
							</td>
						</tr>
						
						<%
								rsSubgrupo.Movenext
							Loop
							
							If valor_is_total > 0 Then
								taxa_media_mensal = CortaCasasDecimais(((valor_premio_liquido_total / valor_is_total) * 1000), 4)
							Else
								taxa_media_mensal = 0
                            End If
						%>
					</table>
					<input type="hidden" name="NR_CTC_SGRO_subgrupo" value="">
					<input type="hidden" name="NR_VRS_CTC_subgrupo" value="">
				</div>
			</td>
		</tr>
		<%end if%>
		<tr>
			<td colspan="4" height="10"></td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor total de IS</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="valor_is_total" size="16" value="<%=FormatNumber(valor_is_total, 2)%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="valor_is_total_or" value="<%=FormatNumber(valor_is_total, 2)%>">
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor total do pr�mio l�quido</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="valor_premio_liquido_total" size="16" value="<%=FormatNumber(valor_premio_liquido_total, 2)%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="valor_premio_liquido_total_or" value="<%=FormatNumber(valor_premio_liquido_total, 2)%>">
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Taxa m�dia mensal</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="taxa_media_mensal" size="16" value="<%=FormatNumber(taxa_media_mensal, 4)%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="taxa_media_mensal_or" value="<%=FormatNumber(taxa_media_mensal, 4)%>">
				
				<input type="hidden" name="valor_premio_net_total" size="16" value="<%=FormatNumber(valor_premio_net_total, 2)%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
				<input type="hidden" name="valor_premio_net_total_or" value="<%=FormatNumber(valor_premio_net_total, 2)%>">
				<input type="hidden" name="per_fat_seguro" value="<%=FormatNumber(per_fat_seguro, 0)%>">
				<input type="hidden" name="PC_ETLE_CTC" value="<%=FormatNumber(percentual_pro_labore,2)%>">
				<input type="hidden" name="VL_LQDO_MOEN_CTC" value="<%=FormatNumber(valor_premio_liquido, 2)%>">
				<input type="hidden" name="VL_SGRA_MOEN_CTC" value="<%=FormatNumber(valor_premio_net, 2)%>">
                <input type="hidden" name="PC_DA_CTC" value="<%=FormatNumber(percentual_da, 2)%>">
                <input type="hidden" name="VL_PREM_PURO" value="<%=FormatNumber(valor_premio_puro,2)%>">
                <input type="hidden" name="VL_IPTC_MOEN_CTC" value="<%=FormatNumber(valor_is,2)%>">
                <input type="hidden" name="PC_MED_SGR" value="<%=FormatNumber(taxa_comercial, 4)%>">
			</td>
			<td colspan="2"></td>
		</tr>
	</table>

<script language="JavaScript">
<!--
<%If bSubgrupo Then
     Response.Write "document.formProc.VL_IOF_CTC.value = FormatNumber(""" & valor_iof & """,2);"
  End If%>

function FormatNumber(vr,tamdec,truncate){
	//A fun��o espera receber casas decimais com V�RGULA
	// truncate = 1 - n�o altera o valor de casas decimais, caso seja maior do que o tamanho decidido
	// truncate = 2 - remove os valores superiores as casa decimais
	// truncate = 3 - remove os valores superiores as casa decimais e arredonda caso haja necessidade
	vp = '';
	for(f=0;f<vr.length;f++){
		if(((vr.charCodeAt(f)>=48)&&(vr.charCodeAt(f)<=57))||(vr.charCodeAt(f)==44)){
			vp=vp+vr.charAt(f);
		}
	}
	a=1;
	vr = vp;
	if(vr.length>0){
		vr = vr.split(',');
		vrIni = vr[0];
		vrIniTemp = '';
		for(f=0;f<vrIni.length;f++){
			vrIniTemp = vrIni.charAt(vrIni.length-f-1) + vrIniTemp;
			if((a==3)&&(vrIni.length!=f+1)){
				vrIniTemp = '.' + vrIniTemp;
				a=0;
			}
			a++;
		}
		vrIni = vrIniTemp;
		vrFim = "";
		if(vr.length>1){
			vrFim = vr[1];
			if(vrFim.length>tamdec){
				if(truncate==3){
					valorFinal = vrFim.substring(tamdec,tamdec + 1);
					vrFim = vrFim.substring(0,tamdec);
					if(valorFinal>4){
						vrFim = vrFim/1 + 1;
					}
				} else if(truncate==2){
					vrFim = vrFim.substring(0,tamdec);
				}
			} else {
				for(f=vrFim.length;f<tamdec;f++){
					vrFim = vrFim + '0';
				}
			}
		} else {
			for(f=0;f<tamdec;f++){
				vrFim = vrFim + '0';
			}
		}
		vr = vrIni + ',' + vrFim;
		return vr;
	}
}

function AtribuirPagtoParcelaHidden()
{
	document.formProc.QT_PCL_PGTO_CTC.value = document.formProc.rQT_PCL_PGTO_CTC.value;
}

function VerificarParcelamento(qtde_dias)
{
	AtribuirPagtoParcelaHidden();
	
	var qtde_meses = parseFloat((qtde_dias / 30));
	
	if (qtde_meses.toString().indexOf('.') >= 0)
		qtde_meses = parseInt(qtde_meses.toString().substring(0,qtde_meses.toString().indexOf('.')));
	
	if (document.formProc.QT_PCL_PGTO_CTC.value > qtde_meses)
		alert('A quantidade de parcelas � maior que os meses de vig�ncia do seguro');
}

function calculaValores()
{
	var per_fat_seguro = parseFloat('0' + converteNumeroJavascript(document.formProc.per_fat_seguro.value));

	var percentual_corretagem = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_CRE_CTC.value));
	var percentual_iof = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_IOF_CTC.value));
	var percentual_da = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_DA_CTC.value));
	var percentual_pro_labore = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_ETLE_CTC.value));
	var valor_premio_puro = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_PREM_PURO.value));
	var valor_is = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_IPTC_MOEN_CTC.value));

	if ((valor_premio_puro > 0) && (percentual_da > 0))
		var valor_premio_net = (valor_premio_puro / (1 - (percentual_da/100)));
	else
		var valor_premio_net = 0;
		
	if ((valor_premio_puro > 0) && (percentual_da > 0) && (percentual_corretagem > 0))
		var valor_premio_liquido = (valor_premio_puro / (1 - (percentual_da/100))) / (1 - ((percentual_pro_labore/100) + (percentual_corretagem/100)));
	else
		var valor_premio_liquido = 0;

	if (percentual_pro_labore > 0)
		var valor_pro_labore = valor_premio_liquido * percentual_pro_labore / 100;
	else
		var valor_pro_labore = 0;
	
	if ((valor_is > 0) && (valor_premio_liquido > 0) && (per_fat_seguro > 0))
		var taxa_comercial = ((valor_premio_liquido * per_fat_seguro) / valor_is) * 1000;
	else
		var taxa_comercial = 0;

	if (percentual_iof > 0)
		var valor_iof = valor_premio_liquido * percentual_iof / 100;
	else
		var valor_iof = 0;

	document.formProc.VL_LQDO_MOEN_CTC.value = FormatNumber(valor_premio_liquido.toString().replace(".", ","), 2, 2);
	document.formProc.PC_MED_SGR.value = FormatNumber(taxa_comercial.toString().replace(".", ","), 4, 2);
	document.formProc.VL_SGRA_MOEN_CTC.value = FormatNumber(valor_premio_net.toString().replace(".", ","), 2, 2);
	document.formProc.VL_IOF_CTC.value = FormatNumber(valor_iof.toString().replace(".", ","), 2, 2);

	calculaTotais('S');
}

var valor_is_total = 0;
var valor_premio_puro_total = 0;
var valor_premio_liquido_total = 0;
var valor_premio_net_total = 0;

function calculaTotais(tipo)
{

	if (tipo == 'C') //carregar dados
	{
		document.formProc.valor_is_total.value = document.formProc.valor_is_total_or.value;
		document.formProc.valor_premio_puro_total.value = document.formProc.valor_premio_puro_total_or.value;
		document.formProc.valor_premio_liquido_total.value = document.formProc.valor_premio_liquido_total_or.value
		document.formProc.taxa_media_mensal.value = document.formProc.taxa_media_mensal_or.value
		
		valor_is_total = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_is_total.value)) -
						 parseFloat('0' + converteNumeroJavascript(document.formProc.VL_IPTC_MOEN_CTC.value));

		valor_premio_puro_total = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_premio_puro_total.value)) -
								  parseFloat('0' + converteNumeroJavascript(document.formProc.VL_PREM_PURO.value));

		valor_premio_liquido_total = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_premio_liquido_total.value)) -
									 parseFloat('0' + converteNumeroJavascript(document.formProc.VL_LQDO_MOEN_CTC.value));

		valor_premio_net_total = parseFloat('0' + converteNumeroJavascript(document.formProc.valor_premio_net_total.value)) -
								 parseFloat('0' + converteNumeroJavascript(document.formProc.VL_SGRA_MOEN_CTC.value));

	} 
	else if (tipo == 'S') //salvar dados
	{
		var valor_is_total_temp = parseFloat(valor_is_total) +
								  parseFloat('0' + converteNumeroJavascript(document.formProc.VL_IPTC_MOEN_CTC.value));
		
		document.formProc.valor_is_total.value = FormatNumber(valor_is_total_temp.toString().replace(".", ","), 2, 2);


		var valor_premio_puro_total_temp = parseFloat(valor_premio_puro_total) +
										   parseFloat('0' + converteNumeroJavascript(document.formProc.VL_PREM_PURO.value));

		document.formProc.valor_premio_puro_total.value = FormatNumber(valor_premio_puro_total_temp.toString().replace(".", ","), 2, 2);

										  
		var valor_premio_liquido_total_temp = parseFloat(valor_premio_liquido_total) +
											  parseFloat('0' + converteNumeroJavascript(document.formProc.VL_LQDO_MOEN_CTC.value));

		document.formProc.valor_premio_liquido_total.value = FormatNumber(valor_premio_liquido_total_temp.toString().replace(".", ","), 2, 2);

		var valor_premio_net_total_temp = parseFloat(valor_premio_net_total) +
										  parseFloat('0' + converteNumeroJavascript(document.formProc.VL_SGRA_MOEN_CTC.value));

		document.formProc.valor_premio_net_total.value = FormatNumber(valor_premio_net_total_temp.toString().replace(".", ","), 2, 2);

		if (valor_is_total_temp > 0)
			var taxa_media_mensal_temp = parseFloat((valor_premio_liquido_total_temp / valor_is_total_temp) * 1000);
		else
			var taxa_media_mensal_temp = 0;

		document.formProc.taxa_media_mensal.value = FormatNumber(taxa_media_mensal_temp.toString().replace(".", ","), 4, 3);
	}
}

function converteNumeroJavascript(valor){
	valor = valor.split(',');
	valorNovo = '';
	for(f=0;f<valor[0].length;f++){
		if((valor[0].charCodeAt(f)>=48)&&(valor[0].charCodeAt(f)<=57)){
			valorNovo=valorNovo+valor[0].charAt(f);
		}
	}
	if(valor.length>1){
		valorNovo = valorNovo + '.' + valor[1];
	}
	return valorNovo;
}

function submitdadosfinanceiros()
{
	document.formProc.action = 'concluir_negocio_fin.asp';
	document.formProc.submit();
}

function alterarDados()
{
	if (validaform())
	{
		document.formProc.salvarfin.value = 'S';
		submitdadosfinanceiros();
	}
}

function validaform()
{
    var VL_CRE_MIN = parseFloat('0' + converteNumeroJavascript('<%= PC_CRE_MIN %>'));
    var VL_CRE_MAX = parseFloat('0' + converteNumeroJavascript('<%= PC_CRE_MAX %>'));
	var VL_CRE_PDRO = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_CRE_PDRO.value));
       
	if (VL_CRE_PDRO < VL_CRE_MIN || VL_CRE_PDRO > VL_CRE_MAX){
        var msg = 'Valor de corretagem fora dos limites, confirma o envio?';
	    if (confirm(msg)){
	        return true ;
	    }       
	}	
	return true;
}
-->
</script>