<%
 Function colocazero(valor)
	if len(valor)<2 then
		valor = "0" & valor
	end if
	colocazero = valor
 End Function

'Fun��o para grava��o do Objeto de Risco, na pagina dados_obj e registrar_pedido_gravar
 Function mGravaLocalRisco(aba)
    If CBool(ValidaTipoWorkflowGeral(tp_wf_id, GERAL_WF_VG)) And (inStr(1,Request("nome_da_pagina_atual"),"_obj") OR inStr(1,paginaOrigem,"_obj")) Then
       If Request(aba&"NM_PRPN_CTC") = "NOVO" Then    'cria cota��o por subgrupo
          If Session("cpf") = "" Then
             mGravaLocalRisco = "<br><br><div align=""center""><font color=""red"">Sess�o expirada. Favor entrar novamente no sistema.</font></div>"
             Exit Function   
          End If

          Set rsCotacao = objCon.mLerCotacao()
          rsCotacao.ActiveConnection = Nothing

          objCon.pCD_PRD         = ""
          objCon.pCD_MDLD        = ""
          objCon.pCD_ITEM_MDLD   = CD_ITEM_MDLD
          If CStr(tp_wf_id) = CStr(Application("WF_EDSC_CANCELAMENTO")) Then
             objCon.pCD_OPERACAO = "4"
          Else
             objCon.pCD_OPERACAO = "1"
          End If
          objCon.pCD_TIP_OPR_CTC = rsCotacao("CD_TIP_OPR_CTC")
          objCon.pCD_CNL_VND_CTC = rsCotacao("CD_CNL_VND_CTC")
          If isNull(rsCotacao("CD_UOR_CTC")) Then
          objCon.pCD_UOR_CTC     = ""
          Else
             objCon.pCD_UOR_CTC     = rsCotacao("CD_UOR_CTC")
          End If
          dataIni = Year(rsCotacao("DT_INC_VGC_CTC")) & colocazero(Month(rsCotacao("DT_INC_VGC_CTC"))) & colocazero(Day(rsCotacao("DT_INC_VGC_CTC"))) 
          dataFim = Year(rsCotacao("DT_FIM_VGC_CTC")) & colocazero(Month(rsCotacao("DT_FIM_VGC_CTC"))) & colocazero(Day(rsCotacao("DT_FIM_VGC_CTC"))) 
 
          Set rsWF = objCon.mLerProdutoWorkflow()
          rsWF.ActiveConnection = Nothing
          If rsWF.EOF Then
              mGravaLocalRisco   = "Registro n�o encontrado"
              Exit Function
          Else
              objCon.pCD_PRD           = rsWF("CD_PRD_WF")
              objCon.pCD_MDLD          = rsWF("CD_MDLD_WF")
              objCon.pCD_ITEM_MDLD     = CD_ITEM_MDLD
              objCon.ptp_wf_id         = rsWF("tp_wf_id")
              objCon.ptp_versao_id     = rsWF("tp_versao_id")
              objCon.pNR_VRS_CTC       = NR_VRS_CTC
              objCon.pIN_RPSS_ETLE_CTC = "N"
              objCon.pNR_CTC_VCLD      = NR_CTC_SGRO
              objCon.pNR_VRS_CTC_VCLD  = NR_VRS_CTC
              objCon.pIN_SGR_CTC_SGRO  = "S"
              objCon.pSUBGRUPO_ID      = Request(aba&"numProxSubgrupo")

              objCon.pDT_INC_VGC_CTC   = dataIni
              objCon.pDT_FIM_VGC_CTC   = dataFim
              objCon.pUSUARIO          = arrParametro(6)

              objCon.pCD_CTGR_PSS_CTC  = Request(aba&"vd_categoria")
              objCon.pCD_PSS_CTC       = 0
              objCon.pTX_EMAI_PRPN_CTC = ""
              objCon.pNM_PRPN_CTC      = Request(aba&"vd_nome")
              objCon.pNR_SRF_PRPN_CTC  = Request(aba&"vd_cnpj")
              objCon.pcpf = Session("cpf") '"11111111111"

              mGravaLocalRisco = objCon.mIncluirCotacao()
              
              If mGravaLocalRisco <> "" Then
                 Response.Write objCon.pErro
                 Exit Function
              End If
          End If

          preencheChaves()
          Set rsSubgrupo = objcon.mLerSubgrupoCotacao
          rsSubGrupo.ActiveConnection = Nothing
          If NOT rsSubGrupo.EOF Then
              rsSubGrupo.MoveLast()
              var_NR_CTC_ATUAL= rsSubgrupo("NR_CTC_SGRO")&"|"&rsSubgrupo("NR_VRS_CTC")
          End If 
       Else   
          var_NR_CTC_ATUAL = Request(aba&"NM_PRPN_CTC")
       End If

       preencheChaves()
       NR_CTC_NOVO_array   = Split(var_NR_CTC_ATUAL &"|","|")
       objCon.pNR_CTC_SGRO = NR_CTC_NOVO_array(0)
       objCon.pNR_VRS_CTC  = NR_CTC_NOVO_array(1)

       objCon.pIND_EXCLUSAO_CBT = "N"
       mGravaLocalRisco = excluirQuestionario(aba, 9, 0, 1)
       If mGravaLocalRisco = "" Then
          preencheChaves()
          objCon.pNR_CTC_SGRO = NR_CTC_NOVO_array(0)
          objCon.pNR_VRS_CTC  = NR_CTC_NOVO_array(1)
          mGravaLocalRisco = gravarRespostas(aba, 9, 0, 1)
       End If

    Else 'Ramos Elementares
       objCon.pIND_EXCLUSAO_CBT = "N"
       mGravaLocalRisco = excluirQuestionario(aba, 2, 0, Request(aba&"LocalRisco"))
       If mGravaLocalRisco = "" Then
          mGravaLocalRisco = gravarRespostas(aba, 2, 0, Request(aba&"LocalRisco"))
       End If
    End If
    %>
    <script>
    <!--
       document.formProc.<%=aba%>gravarDados.value='N';
    //-->   
    </script>
    <%
 End Function
%>