<html>
<head>
<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Grava��o de dados da cobertura</title>
</head>
<!--#include virtual = "/internet/serv/siscot3/request_geral.asp"-->
<!--#include virtual = "/internet/serv/siscot3/objWorkflowNegocio.asp"-->
<body>
<%'Inclui coberturas, n�o atualiza
  response.write Request("ctc") & "<br>"
  response.write Request("lmi") & "<br>"
  response.write Request("paramLMI")& "<br>"
  response.write Request("local") & "<br>"
  response.write Request("paramCOB") & "<br>"
 
'Grava a ultima cobertura informada, usada na pagina dados_cob.asp para grava��o a cada escolha de cobertura
'� processado dentro do iFrameEscondido

 preencheChaves
 If Request("ctc") <> "" Then       ' para gravar Cota��es Subgrupo
    NR_CTC_NOVO_array   = Split(Request("ctc")&"|","|")
    objCon.pNR_CTC_SGRO = NR_CTC_NOVO_array(0)
    objCon.pNR_VRS_CTC  = NR_CTC_NOVO_array(1)
 End If

 If Request("lmi") = "S" Then
    objCon.pNR_SEQL_LIM_CTC = 1
    objCon.pNR_SEQL_CBT_CTC = Request("paramLMI") & "|"
 Else
    objCon.pNR_SEQL_CBT_CTC = Request("local")
 End If

 'Estrutura arrCobert:Conj.Cob|Cod.Cob|C�d.Qstn|Exibicao|Item Pai|Valor IPTC.Gravado|Nome|Acumula|Lim.Perc.Min|Lim.Perc.Max|Lim.Valor Min|Lim.Valor Max|Cob.Basica|Perc.IPTC.Gravado|
 arrCobert = Split(Request("paramCOB"),"|")

 objCon.pCD_CJT_CBT = arrCobert(0) & "|"
 objCon.pCD_CBT     = arrCobert(1) & "|"
 If arrCobert(13) = "" Then
    objCon.pPC_IPTC_CBT_CTC = "0|"
 Else
    objCon.pPC_IPTC_CBT_CTC = Replace(arrCobert(13),".",",") & "|"
 End If
 If arrCobert(5) = "" Then
    objCon.pVL_IPTC_CBT_CTC = "0|"
 Else
    objCon.pVL_IPTC_CBT_CTC = Replace(arrCobert(5),".",",") & "|"
 End If
 objCon.pNR_SEQL_FRQU_CTC   = "null|"
 objCon.pPC_FRQU_CTC        = "null|"
 objCon.pPC_PREM_CBT_CTC    = "0|"
 objCon.pVL_PREM_CBT_CTC    = "0|"
 objCon.pPC_DSC_TCN_CBT_CTC = "null|"
 objCon.pTX_FRQU_CTC        = "0|"

 perguntas = Split(Request("paramQST"),",")
 If Request("lmi") = "S" Then
    locaisLMI = Split(Request("paramLMI")&",",",")
    For iLL = 0 To Ubound(locaisLMI)-1
        For iQst = 0 To Ubound(perguntas)
            respostas = Split(perguntas(iQst),"@")

            objCon.pNR_SEQL_QSTN_CTC = objCon.pNR_SEQL_QSTN_CTC & locaisLMI(iLL) & "|"
            objCon.pNR_QSTN          = objCon.pNR_QSTN & arrCobert(2) & "|"                'questionario
            objCon.pNR_QST           = objCon.pNR_QST & respostas(0) & "|"                 'quest�o
            objCon.pNR_ORD_QSTN_CTC  = objCon.pNR_ORD_QSTN_CTC & (iQst + 1) & "|"          'contador
            objCon.pTX_DCR_RPST_CTC  = objCon.pTX_DCR_RPST_CTC & respostas(1) & "|"        'resposta
        Next
    Next
 Else
    For iQst = 0 To Ubound(perguntas)
        respostas = Split(perguntas(iQst),"@")
        objCon.pNR_SEQL_QSTN_CTC = objCon.pNR_SEQL_QSTN_CTC & Request("local") & "|"
        objCon.pNR_QSTN          = objCon.pNR_QSTN & arrCobert(2) & "|"                    'questionario
        objCon.pNR_QST           = objCon.pNR_QST & respostas(0) & "|"                     'quest�o
        objCon.pNR_ORD_QSTN_CTC  = objCon.pNR_ORD_QSTN_CTC & (iQst + 1) & "|"              'contador
        objCon.pTX_DCR_RPST_CTC  = objCon.pTX_DCR_RPST_CTC & respostas(1) & "|"            'resposta
    Next
 End If

 objCon.pCD_TIP_QSTN        = 3                                                       'tipo questionario - coberturas
 objCon.pNR_SEQL_QSTN_CTC   = objCon.pNR_SEQL_QSTN_CTC & ","
 objCon.pNR_CD_TIP_QSTN_CTC = arrCobert(1) & ","
 objCon.pNR_QSTN            = objCon.pNR_QSTN & "," 
 objCon.pNR_QST             = objCon.pNR_QST & "," 
 objCon.pNR_ORD_QSTN_CTC    = objCon.pNR_ORD_QSTN_CTC & ","
 objCon.pTX_DCR_RPST_CTC    = objCon.pTX_DCR_RPST_CTC & ","

 If objCon.pCD_CBT <> "" Then
    If Request("lmi") = "S" Then
       erroX = objCon.mAtualizarCoberturasLMI(False)
    Else
       erroX = objCon.mAtualizarCoberturas(False)
    End If
 End If

 response.write erroX
 'response.Write objcon.psqlerro 
 'response.Write objcon.perro

 Set objCon = Nothing %>
 <script>
    if(window.parent.document.formProc.controleFrames.value=='1') window.parent.document.formProc.controleFrames.value='2';
    else window.parent.document.formProc.controleFrames.value='1';
<% 
 If Request("pag") <> "" And erroX = "" Then%>
    window.parent.document.formProc.gravar_por_abas.value='S';
    window.parent.document.formProc.action='redistribuicao.asp?pagina_a_ser_enviada=<%=Request("pag")%>&pagina_de_onde_vim=registrar_pedido_cob';
    window.parent.document.formProc.submit();
<%
 End If%>
</script>
</body>
</html>