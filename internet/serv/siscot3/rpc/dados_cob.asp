<br>
<input type="hidden" name="gravarDados" value="N">
<input type="hidden" name="excluirDados" value="N">
<input type="hidden" value="N" name="gravar">
<input type="hidden" value="N" name="coletarDadosBanco">
<input type="hidden" name="controleFrames" value="1">
<%
objCon.pTP_PESQ = "BB"
Set rsSelCob    = objCon.mLerTipoSelCobertura
rsSelCob.ActiveConnection = Nothing
If NOT rsSelCob.EOF Then
    tipoSelCob  = rsSelCob("TP_SEL_CBT")
End If
Set rsSelCob    = Nothing
%>
<script>
<!--
	function teste(){
		document.formProc.action = "";
		document.formProc.submit();
	}

    var opcaoAnterior='';
    function gravaAba(pg){
        if(opcaoAnterior=='A'||opcaoAnterior=='I'){
           if(opcaoAnterior=='A') Fgravar(0);
           else{
              if(Fgravar('G')){
                 var pagina = 'dados_cob_gravar.asp?pag='+pg+'&ctc='+nr_cot_atu+'&lmi=<%=Request("valor_lmi")%>&local='+codLocalRisco+'&item='+itemA+'&paramCOB='+arrCoberturas[itemA]+'&paramQST='+eval('document.formProc.cpoGrav_QSTN_'+itemA+'.value')+'&paramLMI='+eval('document.formProc.cpoGrav_LocR_'+itemA+'.value'); 
                 if(document.formProc.controleFrames.value=='1') window.open(pagina,'iFrameEscondido1');
                 else                                            window.open(pagina,'iFrameEscondido2');
              }
           }
        }else if(pg!=''){
           document.formProc.gravar_por_abas.value='S';
           document.formProc.action='redistribuicao.asp?pagina_a_ser_enviada='+pg+'&pagina_de_onde_vim='+document.formProc.nome_da_pagina_atual.value;
           document.formProc.submit();
        }
    }

    function Fgravar(operacao){
        if(operacao!=0){                             // se exclui cobertura, nao precisa checar dados digitados
            for(v=1;v<arrLimitesSelecao.length;v++){
   	            var limites = arrLimitesSelecao[v].split('@');
                if(limites[0]>limites[2]){
                    window.alert("Faltam coberturas " + (limites[0] - limites[2]) + " para o conjunto " + intI + ".");
                    return false;
                }
	        }
            for(v=1;v<arrCoberturas.length;v++){     // verifica se as coberturas selecionadas tem valor
	            if(operacao!='A') v=itemA;
	            cob = eval("document.formProc.cpoGrav_CodC_"+v+".value");
	            if(cob!=''){
	             <%If Request("valor_lmi") = "S" Then%>
	                if(eval("document.formProc.cpoGrav_LocR_"+v+".value==''")){
   	                   window.alert("Selecione os locais de risco para a cobertura "+cob+".");
	                   return false;
	                }
	             <%End If
                   If tipoSelCob = 2 Then%>
              	    var cobAtual  = arrCoberturas[v].split('|');
                    if(cobAtual[12]=='S'&&eval("document.formProc.perc_is_"+cob+".value==''")&&eval("document.formProc.valor_is_"+cob+".value==''")){
                       window.alert("Preencha o percentual para a cobertura "+cob+".");
	                   eval("document.formProc.perc_is_"+cob+".focus()");
	                   return false;
	                }
                    else if(cobAtual[12]=='N'&&eval("document.formProc.perc_is_"+cob+".value==''")){
	                   window.alert("Preencha o percentual para a cobertura "+cob+".");
	                   eval("document.formProc.perc_is_"+cob+".focus()");
	                   return false;
	                }
	             <%ElseIf tipoSelCob = 3 Then%>
	                if(eval("document.formProc.valor_is_"+cob+".value==''")){
	                   window.alert("Preencha o valor para a cobertura "+cob+".");
	                   eval("document.formProc.valor_is_"+cob+".focus()");
	                   return false;
	                }
	             <%End If%>   
	            }

	            var qst = eval("document.formProc.cpoGravCQSTN_"+v+".value");
	            if(qst>0&&cob!=''){
	                var ps = eval("document.formProc.cpoGrav_QSTN_"+v+".value");
	                var pergs = ps.split(',');
	                for(vp=0;vp<pergs.length;vp++){     // verifica se as perguntas obrigatorias foram preenchidas
	                    resps = pergs[vp].split('@');
	                    if(resps[1]==''&&resps[2]=='S'){
	                       window.alert("Preencha a quest�o "+(vp+1)+" do question�rio da cobertura "+cob+".");
	                       var pagina = '../dados_cob_qst_preench.asp?codCobertura='+cob+'@@'+qst+'@'+v+'@&codLocalRisco='+codLocalRisco+'&codTipQstn=3&combo=1';
                           window.open(pagina,'iQuestionarios');
 	                       return false;
	                       break;
	                    }  
	                }
	            }

	            if(operacao!='A') break;
            }
        }
        opcaoAnterior='';

	    if(operacao=='G') return true;
	    else{
		    gravouDado();
		    document.formProc.action = "";
		    document.formProc.gravarDados.value = "S";
		    document.formProc.submit();
     	}
	}

	function mostra_div(valor){
		if(document.getElementById(valor).style.display=='none'){
			document.getElementById(valor).style.display='';
		}else{
			document.getElementById(valor).style.display='none';
		}
	}

	var valorComboAnterior;
	function trocaObjetoRisco(obj){
		if(aviso()){
			document.formProc.action = "";
			gravouDado();
			document.formProc.submit();
		} else {
			obj.selectedIndex = valorComboAnterior;
		}
	}

	function colocaZeros(valor,casas){
		for(z=valor.length;casas>valor.length;z=z){
			valor = valor + "0";
		}
		return valor;
	}
//-->
</script>
<%
var_PostBack     = False
var_NR_CTC_ATUAL = Request("NM_PRPN_CTC")

If Request("gravardados") = "S" OR Request("excluirDados") = "S" Then
   var_PostBack  = True
End If

CB_CBT = Replace(Request("CB_CBT"),", ","|")
dado_gravado = Request("dado_gravado")

Function checaValorIs(CB_CBT, valor_is)
	If dado_gravado = "N" Then
		checaValorIs = Request("valor_is_" & Trim(CB_CBT))
	Else
		If isnull(valor_is) Then
			checaValorIs = ""
		Else
			checaValorIs = valor_is
		End If
	End If
End Function

'Grava��o de dados
If Request("gravarDados") = "S" Then
    If Request("excluirDados") = "S" Then     'Troca de op��o LMI
	    preencheChaves
	    If var_NR_CTC_ATUAL <> "" Then
           NR_CTC_NOVO_array   = Split(var_NR_CTC_ATUAL&"|","|")
           objCon.pNR_CTC_SGRO = NR_CTC_NOVO_array(0)
           objCon.pNR_VRS_CTC  = NR_CTC_NOVO_array(1)
        End If
	    objCon.pNR_SEQL_CBT_CTC = ""
	    erroX = objCon.mExcluirCoberturas()
   	Else                                      'Grava coberturas
  	    arr_CB_CBT = split(CB_CBT,"|")
	    perg_id = ""
	    valorIsTodos = ""
        aba = ""
 
  	    If Trim(erroX) = "" Then
	   	   preencheChaves
	       If var_NR_CTC_ATUAL <> "" Then
              NR_CTC_NOVO_array   = Split(var_NR_CTC_ATUAL&"|","|")
              objCon.pNR_CTC_SGRO = NR_CTC_NOVO_array(0)
              objCon.pNR_VRS_CTC  = NR_CTC_NOVO_array(1)
           End If

	   	   If Request("valor_lmi") = "S" Then
    	 	  objCon.pNR_SEQL_LIM_CTC = 1
           Else
              objCon.pNR_SEQL_CBT_CTC = Request("seq_risc")
           End If

           For item = 0 To Request("qtdCoberturas")
              If Request("cpoGrav_CodC_"&item) <> "" And (Request("valor_lmi") = "N" Or Request("cpoGrav_LocR_"&item) <> "") Then
                 If Request("valor_lmi") = "S" Then
                     objCon.pNR_SEQL_CBT_CTC = objCon.pNR_SEQL_CBT_CTC & Request("cpoGrav_LocR_"&item) & "|"
                 End If
                 objCon.pCD_CJT_CBT = objCon.pCD_CJT_CBT & Request("cpoGrav_CjtC_"&item) & "|"
	  	         objCon.pCD_CBT = objCon.pCD_CBT & Request("cpoGrav_CodC_"&item) & "|"
	  	         If Request("perc_is_"&Request("cpoGrav_CodC_"&item)) = "" Then
                     objCon.pPC_IPTC_CBT_CTC = objCon.pPC_IPTC_CBT_CTC & "0|"
	  	         Else
                     objCon.pPC_IPTC_CBT_CTC = objCon.pPC_IPTC_CBT_CTC & Request("perc_is_"&Request("cpoGrav_CodC_"&item)) & "|"
                 End If
	  	         If Request("valor_is_"&Request("cpoGrav_CodC_"&item)) = "" Then
                     objCon.pVL_IPTC_CBT_CTC = objCon.pVL_IPTC_CBT_CTC & "0|"
                 Else
	  	             objCon.pVL_IPTC_CBT_CTC = objCon.pVL_IPTC_CBT_CTC & Request("valor_is_"&Request("cpoGrav_CodC_"&item)) & "|"
                 End If
	  	         objCon.pNR_SEQL_FRQU_CTC = objCon.pNR_SEQL_FRQU_CTC & "null|"
	  	         objCon.pPC_FRQU_CTC = objCon.pPC_FRQU_CTC & "null|"
                 objCon.pPC_PREM_CBT_CTC = objCon.pPC_PREM_CBT_CTC & "0|"
                 objCon.pVL_PREM_CBT_CTC = objCon.pVL_PREM_CBT_CTC & "0|"
                 objCon.pPC_DSC_TCN_CBT_CTC = objCon.pPC_DSC_TCN_CBT_CTC & "null|"
                 objCon.pTX_FRQU_CTC = objCon.pTX_FRQU_CTC & "0|"

                 perguntas = Split(Request("cpoGrav_QSTN_"&item),",")
                 If Request("valor_lmi") = "S" Then
                    locaisLMI = Split(Request("cpoGrav_LocR_"&item)&",",",")
                    For iLL = 0 To Ubound(locaisLMI)-1
                        For iQst = 0 To Ubound(perguntas)
                            respostas = Split(perguntas(iQst),"@")

                            objCon.pNR_SEQL_QSTN_CTC = objCon.pNR_SEQL_QSTN_CTC & locaisLMI(iLL) & "|"
                            objCon.pNR_QSTN = objCon.pNR_QSTN & Request("cpoGravCQSTN_"&item) & "|"       'questionario
                            objCon.pNR_QST = objCon.pNR_QST & respostas(0) & "|"                          'quest�o
                            objCon.pNR_ORD_QSTN_CTC = objCon.pNR_ORD_QSTN_CTC & (iQst + 1) & "|"          'contador
                            objCon.pTX_DCR_RPST_CTC = objCon.pTX_DCR_RPST_CTC & respostas(1) & "|"        'resposta
                        Next
                    Next
                 Else
                    For iQst = 0 To Ubound(perguntas)
                        respostas = Split(perguntas(iQst),"@")

                        objCon.pNR_SEQL_QSTN_CTC = objCon.pNR_SEQL_QSTN_CTC & Request("seq_risc") & "|"
                        objCon.pNR_QSTN = objCon.pNR_QSTN & Request("cpoGravCQSTN_"&item) & "|"           'questionario
                        objCon.pNR_QST = objCon.pNR_QST & respostas(0) & "|"                              'quest�o
                        objCon.pNR_ORD_QSTN_CTC = objCon.pNR_ORD_QSTN_CTC & (iQst + 1) & "|"              'contador
                        objCon.pTX_DCR_RPST_CTC = objCon.pTX_DCR_RPST_CTC & respostas(1) & "|"            'resposta
                    Next
                 End If

                 objCon.pCD_TIP_QSTN = 3                                                                  'tipo questionario - coberturas
                 objCon.pNR_SEQL_QSTN_CTC = objCon.pNR_SEQL_QSTN_CTC & ","
                 objCon.pNR_CD_TIP_QSTN_CTC = objCon.pNR_CD_TIP_QSTN_CTC & Request("cpoGrav_CodC_"&item) & ","
                 objCon.pNR_QSTN = objCon.pNR_QSTN & "," 
                 objCon.pNR_QST = objCon.pNR_QST & "," 
                 objCon.pNR_ORD_QSTN_CTC = objCon.pNR_ORD_QSTN_CTC & ","
                 objCon.pTX_DCR_RPST_CTC = objCon.pTX_DCR_RPST_CTC & ","
              End If
	 	   Next

           If objCon.pCD_CBT <> "" Then
	   	      If Request("valor_lmi") = "S" Then
                 erroX = objCon.mAtualizarCoberturasLMI()
              Else
                 erroX = objCon.mAtualizarCoberturas()
              End If
           End If
        End If
    End If
End If

'If erroX <> "" Then
'	dado_gravado = "N"
'	response.Write objcon.psqlerro & "<br>"
'   response.Write objcon.perro & "<br>"
'	Response.Write "<font color=""red"">" & erroX & "</font>"
'End If

preencheChaves
If CBool(ValidaTipoWorkflowGeral(tp_wf_id, GERAL_WF_VG)) Then
    javascriptLMI = "onclick=""lmiSubmit();"""
    lmi_S = ""
    lmi_N = "checked "

    Set rsSubgrupo = objCon.mLerSubgrupoCotacao
    rsSubGrupo.ActiveConnection = Nothing%>
    
   <table cellpadding="2" cellspacing="1" border="0" class="escolha" width="343">
	 <tr>
		<td class="td_label" width="91">Subgrupo</td>
		<td class="td_dado" width="241">&nbsp;
			<select name="NM_PRPN_CTC" onchange="document.formProc.action='';document.formProc.submit();">
			<option value="">--Escolha abaixo--</option>
			<%
			Do While NOT rsSubgrupo.EOF
				If var_NR_CTC_ATUAL = Trim(rsSubgrupo("NR_CTC_SGRO")) & "|" & Trim(rsSubgrupo("NR_VRS_CTC")) Then
					sSelecionado = " selected"
					objCon.pNR_CTC_SGRO = rsSubgrupo("NR_CTC_SGRO")
                    objCon.pNR_VRS_CTC  = rsSubgrupo("NR_VRS_CTC")
 				Else
					sSelecionado = ""
				End If
				%>
				<option value="<%=Trim(rsSubgrupo("NR_CTC_SGRO")) & "|" & Trim(rsSubgrupo("NR_VRS_CTC"))%>"<%=sSelecionado%>><%=Right(00 & rsSubgrupo("subgrupo_id"),2) & " (" & Trim(rsSubgrupo("NR_CTC_SGRO")) & ")"%> - <%=Trim(rsSubgrupo("NM_PRPN_CTC"))%></option>
				<%
				rsSubgrupo.MoveNext
			Loop%>
			</select>
		</td>
	 </tr>  
   </table>
<%
Else
   objCon.pTP_PESQ = "BB"
   ' = objCon.mLerPermiteLMI
   booPermiteLMI = "N"
   lmi_N = "checked "
   lim_S = ""

   javascriptLMI = "onclick=""lmiSubmit();"""
   'If booPermiteLMI = "S" Then
   '   javascriptLMI = "onclick=""lmiTeste();"""

   '   If Request("valor_lmi") = "S" Then
	'      lmi_S = "checked "
	 '     lmi_N = ""
      'Else
       '   lmi_S = ""
	    '  lmi_N = "checked "  
         ' objCon.pNR_SEQL_CBT_CTC = ""
          'Set rsCoberturaComLMI = objCon.mLerIndCobLmi()
		  'rsCoberturaComLmi.ActiveConnection = Nothing
		  'If NOT rsCoberturaComLMI.EOF Then
	'		 If rsCoberturaComLMI("IN_CBT_CTC_LMI") = "S" Then
   	'             lmi_S = "checked "
	'             lmi_N = ""
    '         End If
    '      End If
	'      Set rsCoberturaComLMI = Nothing
    '  End If
   'End If
End If

Set rsOBJRisc = objCon.mLerSequencialRiscoQuestionario()
rsOBJRisc.ActiveConnection = Nothing
valor = 0
campo = ""
If rsOBJRisc.EOF Then
    If booPermiteLMI <> "" Then%>
<br><br>
<table cellpadding="2" cellspacing="1" border="0" class="escolha" width="841">
	<tr>
		<td class="td_label" width="835"><center>Nenhum Objeto de risco foi encontrado.<br><br> Favor cadastrar um objeto de risco na aba de 'Objeto Segurado'.</center></td>
	<tr>
</table>
<%  End If
Else
    campo = rsOBJRisc("NR_SEQL_QSTN_CTC")
%>
<table cellpadding="2" cellspacing="1" border="0" class="escolha" width="841">
	<tr>
	    <td width="148"><input type="button" name="alterar" value="Gravar" style="width:120px;font-size:10px;margin: 2px 2px 2px 2px;" onclick="Fgravar('A');">&nbsp;&nbsp;</td>
		<td width="682"><table width="551"><tr>
<%  If CBool(ValidaTipoWorkflowGeral(tp_wf_id, GERAL_WF_VG)) Then
         Response.Write "<td width=""365""><input type=""hidden"" name=""valor_lmi"" value=""N""></td>"
    Else%>
		<td class="td_label" width="160">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Cobertura LMI?</td>
		<td class="td_dado" width="185">
		       	<input type="radio" value="N" name="valor_lmi" <% = lmi_N %> <% = javascriptLMI %>>N�o&nbsp;
		        <input type="radio" value="S" name="valor_lmi" <% = lmi_S %> <% = javascriptLMI %>>Sim</td>
<%  End If

    varListaLMI = ""
    If lmi_N = "checked " Then%>
		<td class="td_label" width="165">&nbsp;Risco</td>
		<td class="td_dado" width="46">
			<select name="seq_risc" onchange="trocaObjetoRisco(this)" onclick="valorComboAnterior = this.selectedIndex;"><%
				Do While NOT rsOBJRisc.EOF
					If Trim(Request("seq_risc")) = Trim(rsOBJRisc("NR_SEQL_QSTN_CTC")) Then
						selected = " selected "
						campo = rsOBJRisc("NR_SEQL_QSTN_CTC")
					Else
						selected = " "
					End If
			%>
				<option value="<% = rsOBJRisc("NR_SEQL_QSTN_CTC") %>"<% = selected %>><% = rsOBJRisc("NR_SEQL_QSTN_CTC") %></option>
			<%
					rsOBJRisc.MoveNext
 		   	    Loop
			%>
			</select>
		</td>
<%  Else
	    Do While NOT rsOBJRisc.EOF
	 	    varListaLMI = varListaLMI & rsOBJRisc("NR_SEQL_QSTN_CTC") & "|N|@"
		    rsOBJRisc.MoveNext
 	    Loop
%>
        <td colspan="2"></td>
<%  End If%>
</tr></table></td></tr></table>
<script>
<!--
	function converteValorJavascript(valor){
		valor = valor.replace(".","");
		valor = valor.replace(".","");
		valor = valor.replace(".","");
		valor = valor.replace(".","");
		valor = valor.replace(".","");
		if(valor==""){
			valor = 0;
		}
		valor = valor + '';
		valor = valor.split(",")
		valorF = "";
		if(valor.length>1){
			valorF = valor[0] + "" + colocaZeros(valor[1],4);
		}else{
			valorF = valor[0];
		}
		return valorF;
	}

    var nr_cot_atu = '<%=var_NR_CTC_ATUAL%>';
    var CJTAnt = 0; var CJTAnt2 = 0; var bMostraCobert = false; var marc1 = ''; var marc2 = '';
    var strCabSelec='<tr><td class="td_label_negrito" width="40">&nbsp;</td><td class="td_label_negrito" colspan="4">Conjunto: $$CONJ$$ (M�nimo: $$MIN$$ e m�ximo: $$MAX$$)</td></tr>';
    var strCabMais ='<tr><td class="td_label_negrito" width="40"><a href="javascript:habilita_grp(0,$$CONJ$$)"><img src="/img/icone_menu_mais.gif" border="0"></a></td><td class="td_label_negrito" colspan="4">Conjunto: $$CONJ$$ (M�nimo: $$MIN$$ e m�ximo: $$MAX$$)</td></tr>';
    var strCabMenos='<tr><td class="td_label_negrito" width="40"><a href="javascript:habilita_grp(1,$$CONJ$$)"><img src="/img/icone_menu_menos.gif" border="0"></a></td><td class="td_label_negrito" colspan="4">Conjunto: $$CONJ$$ (M�nimo: $$MIN$$ e m�ximo: $$MAX$$)</td></tr>';
    var valorAcum=0; var valorCobBasica=0;

    function mudaOpcaoBasica(item){
	    var cobAtual  = arrCoberturas[item].split('|');
  	    if(cobAtual[3]!='S')  checaCoberturasDependentes(item); // n�o precisa passar pela checagem por estar gravado, essa rotina ja esta incluida
	    cobAtual[3]   = 'O';
        arrCoberturas[item] = cobAtual[0] + '|' + cobAtual[1] + '|' + cobAtual[2] + '|' + cobAtual[3] + '|' + cobAtual[4] + '|' + cobAtual[5] + '|' + cobAtual[6] + '|' + cobAtual[7] + '|' + cobAtual[8] + '|' + cobAtual[9] + '|' + cobAtual[10] + '|' + cobAtual[11] + '|' + cobAtual[12] + '|' + cobAtual[13] + '|' ;
        opcaoAnterior = '';
    }

 	function checaCoberturasDependentes(item){
        gravaAba('');
        if(opcaoAnterior!='') return false;
        opcaoAnterior= 'I';

// Estrutura arrCoberturas:Conj.Cob|Cod.Cob|C�d.Qstn|Exibicao|Item Pai|Valor IPTC.Gravado|Nome|Acumula|Lim.Perc.Min|Lim.Perc.Max|Lim.Valor Min|Lim.Valor Max|Cob.Basica|Perc.IPTC.Gravado|
//                         Coluna Exibicao: M-(aparece na primeira lista habilitado),D-(aparece na primeira lista desabilitado),S-(aparece na segunda lista)
//                                          E-(n�o � mostrado porque ja foi escolhido em outro conjunto, mas se voltar aparece habilitado)
//                                          F-(n�o � mostrado porque ja foi escolhido em outro conjunto, mas se voltar aparece desabilitado)
// Estrutura arrLimitesSelecao: indexado por codigo de conjunto
//                              Lim.Minimo@Lim.Maximo@Qtd.Cob.Selecionadas@Aparece Fechado/Aberto@
	    var cobAtual = arrCoberturas[item].split("|");
        var cob      = cobAtual[1];
	    var limites  = arrLimitesSelecao[cobAtual[0]].split('@');
	    var qtdAtual = parseInt(limites[2])+1;
	    if(limites[1]<qtdAtual){
	        window.alert("Limite de Coberturas excedido para o conjunto "+cobAtual[0]+".");
	        return false;
	    }
        itemA= item;
        if(cobAtual[12]=='S'&&cobAtual[5]!='') valorCobBasica = cobAtual[5];
        if(cobAtual[12]=='S'&&cobAtual[13]!=0)  valorCobBasica = -99;
	    if(cobAtual[3]=='M')  cobAtual[3] = 'S';
        arrLimitesSelecao[cobAtual[0]] = limites[0] + '@' + limites[1] + '@' + qtdAtual + '@' + limites[3] + '@';
        arrCoberturas[item] = cobAtual[0] + '|' + cobAtual[1] + '|' + cobAtual[2] + '|' + cobAtual[3] + '|' + cobAtual[4] + '|' + cobAtual[5] + '|' + cobAtual[6] + '|' + cobAtual[7] + '|' + cobAtual[8] + '|' + cobAtual[9] + '|' + cobAtual[10] + '|' + cobAtual[11] + '|' + cobAtual[12] + '|' + cobAtual[13] + '|' ;
        eval("document.formProc.cpoGrav_CodC_"+item+".value="+cobAtual[1]);
        if(cobAtual[2]>0){
            var pagina = '../dados_cob_qst_preench.asp?codCobertura='+cobAtual[1]+'@'+cobAtual[6]+'@'+cobAtual[2]+'@'+item+'@&codLocalRisco='+codLocalRisco+'&codTipQstn=3&combo=1';
            window.open(pagina,'iFrameEscondidoQ');
        }
	    for(i=0;i<arrCoberturas.length;i++){
	        cobAtual = arrCoberturas[i].split("|");
 	        if(cobAtual[3]=='D'&&cobAtual[4]==item){       // Checa coberturas dependentes
	           cobAtual[3]='M';
               arrCoberturas[i] = cobAtual[0] + '|' + cobAtual[1] + '|' + cobAtual[2] + '|' + cobAtual[3] + '|' + cobAtual[4] + '|' + cobAtual[5] + '|' + cobAtual[6] + '|' + cobAtual[7] + '|' + cobAtual[8] + '|' + cobAtual[9] + '|' + cobAtual[10] + '|' + cobAtual[11] + '|' + cobAtual[12] + '|' + cobAtual[13] + '|' ;
   	        }
 	        if(cobAtual[1]==cob&&cobAtual[3]=='M'){        // Checa coberturas repetidas
	           cobAtual[3]='E';
               arrCoberturas[i] = cobAtual[0] + '|' + cobAtual[1] + '|' + cobAtual[2] + '|' + cobAtual[3] + '|' + cobAtual[4] + '|' + cobAtual[5] + '|' + cobAtual[6] + '|' + cobAtual[7] + '|' + cobAtual[8] + '|' + cobAtual[9] + '|' + cobAtual[10] + '|' + cobAtual[11] + '|' + cobAtual[12] + '|' + cobAtual[13] + '|' ;
            }
 	        if(cobAtual[1]==cob&&cobAtual[3]=='D'){        // Checa coberturas repetidas
	           cobAtual[3]='F';
               arrCoberturas[i] = cobAtual[0] + '|' + cobAtual[1] + '|' + cobAtual[2] + '|' + cobAtual[3] + '|' + cobAtual[4] + '|' + cobAtual[5] + '|' + cobAtual[6] + '|' + cobAtual[7] + '|' + cobAtual[8] + '|' + cobAtual[9] + '|' + cobAtual[10] + '|' + cobAtual[11] + '|' + cobAtual[12] + '|' + cobAtual[13] + '|' ;
   	        }
   	    }
	    return true;
	}

	function retiraCoberturasDependentes(dep,item){
        gravaAba('');
        if(opcaoAnterior!='') return false;
        opcaoAnterior='E';
	    itemA=-1;

// Estrutura arrCoberturas:Conj.Cob|Cod.Cob|C�d.Qstn|Exibicao|Item Pai|Valor IPTC.Gravado|Nome|Acumula|Lim.Perc.Min|Lim.Perc.Max|Lim.Valor Min|Lim.Valor Max|Cob.Basica|Perc.IPTC.Gravado|
//                         Coluna Exibicao: M-(aparece na primeira lista habilitado),D-(aparece na primeira lista desabilitado),S-(aparece na segunda lista)
//                                          E-(n�o � mostrado porque ja foi escolhido em outro conjunto, mas se voltar aparece habilitado)
//                                          F-(n�o � mostrado porque ja foi escolhido em outro conjunto, mas se voltar aparece desabilitado)
// Estrutura arrLimitesSelecao: indexado por codigo de conjunto
//                              Lim.Minimo@Lim.Maximo@Qtd.Cob.Selecionadas@Aparece Fechado/Aberto@
	    for(i=0;i<arrCoberturas.length;i++){
	        var cobAtual = arrCoberturas[i].split("|");
            if(cobAtual[3]=='S'&&cobAtual[4]==item){      // Checa coberturas dependentes
	           window.alert("Existem coberturas dependentes selecionadas.");
	           return false;
	        }
	        if(i==dep&&(cobAtual[3]=='S'||cobAtual[3]=='O')) itemA=i;   // se a cobertura retirada depende de outra selecionada
        }

	    var cobAtual = arrCoberturas[item].split('|');
        var cob      = cobAtual[1];
	    if(cobAtual[4]==-99||itemA==-1) cobAtual[3]='M';  // nao eh cobertura dependente ou a cobertura-pai esta selecionada
	    else cobAtual[3]='D';
        arrCoberturas[item] = cobAtual[0] + '|' + cobAtual[1] + '|' + cobAtual[2] + '|' + cobAtual[3] + '|' + cobAtual[4] + '|' + cobAtual[5] + '|' + cobAtual[6] + '|' + cobAtual[7] + '|' + cobAtual[8] + '|' + cobAtual[9] + '|' + cobAtual[10] + '|' + cobAtual[11] + '|' + cobAtual[12] + '|' + cobAtual[13] + '|' ;
        if(cobAtual[12]=='S') valorCobBasica = 0;
        eval("document.formProc.cpoGrav_CodC_"+item+".value=''");

	    var limites  = arrLimitesSelecao[cobAtual[0]].split('@');
	    var qtdAtual = parseInt(limites[2])-1;
        arrLimitesSelecao[cobAtual[0]] = limites[0] + '@' + limites[1] + '@' + qtdAtual + '@' + limites[3] + '@';

	    for(i=0;i<arrCoberturas.length;i++){
	        cobAtual = arrCoberturas[i].split('|');
	        if(cobAtual[4]==item){                        // Checa coberturas dependentes
               cobAtual[3]='D';
               arrCoberturas[i] = cobAtual[0] + '|' + cobAtual[1] + '|' + cobAtual[2] + '|' + cobAtual[3] + '|' + cobAtual[4] + '|' + cobAtual[5] + '|' + cobAtual[6] + '|' + cobAtual[7] + '|' + cobAtual[8] + '|' + cobAtual[9] + '|' + cobAtual[10] + '|' + cobAtual[11] + '|' + cobAtual[12] + '|' + cobAtual[13] + '|' ;
            }
 	        if(cobAtual[1]==cob&&cobAtual[3]=='E'){       // Checa coberturas repetidas
	           cobAtual[3]='M';
               arrCoberturas[i] = cobAtual[0] + '|' + cobAtual[1] + '|' + cobAtual[2] + '|' + cobAtual[3] + '|' + cobAtual[4] + '|' + cobAtual[5] + '|' + cobAtual[6] + '|' + cobAtual[7] + '|' + cobAtual[8] + '|' + cobAtual[9] + '|' + cobAtual[10] + '|' + cobAtual[11] + '|' + cobAtual[12] + '|' + cobAtual[13] + '|' ;
            }
 	        if(cobAtual[1]==cob&&cobAtual[3]=='F'){       // Checa coberturas repetidas
	           cobAtual[3]='D';
               arrCoberturas[i] = cobAtual[0] + '|' + cobAtual[1] + '|' + cobAtual[2] + '|' + cobAtual[3] + '|' + cobAtual[4] + '|' + cobAtual[5] + '|' + cobAtual[6] + '|' + cobAtual[7] + '|' + cobAtual[8] + '|' + cobAtual[9] + '|' + cobAtual[10] + '|' + cobAtual[11] + '|' + cobAtual[12] + '|' + cobAtual[13] + '|' ;
   	        }
        }
        return true;
	}

// Estrutura arrLimitesSelecao: indexado por codigo de conjunto
//                              Lim.Minimo@Lim.Maximo@Qtd.Cob.Selecionadas@Aparece Fechado/Aberto@
    function habilita_grp(opcao,cjt){
        var limites = arrLimitesSelecao[cjt].split('@');
        if(opcao==1) limites[3]='S';      
        else limites[3] = 'N';
        arrLimitesSelecao[cjt] = limites[0] + '@' + limites[1] + '@' + limites[2] + '@' + limites[3] + '@';
        listaCoberturas();
    }
 
    function mostraCabecalho(op,cjt){
        var limites = arrLimitesSelecao[cjt].split('@');
        if(op==1){
           if(limites[3]=='S'){
              var str = strCabMais;
              bMostraCobert = false;
           }else{
              var str = strCabMenos; 
              bMostraCobert = true;
           }
           str = str.replace("$$CONJ$$",cjt);
        }
        else var str = strCabSelec;
           str = str.replace("$$CONJ$$",cjt);
           str = str.replace("$$MIN$$",limites[0]);
           str = str.replace("$$MAX$$",limites[1]);
        return str;
    }
 
    function trazCoberturaPai(item){
        var cobAtual = arrCoberturas[item].split("|");
        return "&nbsp;&nbsp;" + cobAtual[0] + "/" + cobAtual[1]; 
    }
 
    function listaCoberturas(){
        var str1 = '';  var str2 = '';
        CJTAnt = 0; CJTAnt2 = 0; marc1 = ''; marc2 = '';

// Estrutura arrCoberturas:Conj.Cob|Cod.Cob|C�d.Qstn|Exibicao|Item Pai|Valor IPTC.Gravado|Nome|Acumula|Lim.Perc.Min|Lim.Perc.Max|Lim.Valor Min|Lim.Valor Max|Cob.Basica|Perc.IPTC.Gravado|
//                         Coluna Exibicao: M-(aparece na primeira lista habilitado),D-(aparece na primeira lista desabilitado),S-(aparece na segunda lista)
//                                          E-(n�o � mostrado porque ja foi escolhido em outro conjunto, mas se voltar aparece habilitado)
//                                          F-(n�o � mostrado porque ja foi escolhido em outro conjunto, mas se voltar aparece desabilitado)
        for(i=0;i<arrCoberturas.length;i++){
 	        var cobAtual = arrCoberturas[i].split("|");
            if(cobAtual[3]!='E'&&cobAtual[3]!='F'){
               if(cobAtual[3]=='M'||cobAtual[3]=='D'){
       	          if(CJTAnt!= cobAtual[0]){
       	             str1   = str1 + mostraCabecalho(1,cobAtual[0]);
       	             CJTAnt = cobAtual[0];
                  }
                  if(bMostraCobert){
       	             str1 = str1 + '<tr><td class="td_label_negrito" width="34" height="25">';
    	             if(cobAtual[3]=='M')  str1 = str1 + '<a href="javascript:if(checaCoberturasDependentes('+i+')){listaCoberturas();}"><img src="/img/bIncluir.gif" border="0"></a>';
                     str1 = str1 + '</td><td class="td_dado"' + marc1 + ' width="58">&nbsp;' + cobAtual[1] + '</td><td class="td_dado"' + marc1 + ' width="328"> &nbsp;' + cobAtual[6] + '</td><td class="td_dado"' +marc1 + ' width="182">';
                     <%If tipoSelCob = 2 Then 'percentual%>
                     str1 = str1 + FormatNumber(cobAtual[8], 2, 2) +'% at� '+ FormatNumber(cobAtual[9], 2, 2) + '%'; 
                     <%ElseIf tipoSelCob = 3 Then%>
                     str1 = str1 + FormatNumber(cobAtual[10], 2, 2) +' at� '+ FormatNumber(cobAtual[11], 2, 2);
                     <%End If%>
                     str1 = str1 + '</td><td class="td_dado"' + marc1 + ' width="138">';
                     if(cobAtual[4]>-99)  str1 = str1 + trazCoberturaPai(cobAtual[4]);
                     str1 = str1 + '</td></tr>';
 		             if(marc1=='') marc1=' style=";background-color:#f1F1f1"';
		             else marc1 = '';
		          }
               }
               else{                                         // se esta selecionado, coloca na segunda tabela
   	              if(CJTAnt2!= cobAtual[0]){
       	             str2    = str2 + mostraCabecalho(2,cobAtual[0]);
       	             CJTAnt2 = cobAtual[0];
                  }
    	          str2 = str2 + '<tr><td class="td_label_negrito" width="34" height="25">';
    	          if(cobAtual[3]=='S') str2 = str2 + '<a href="javascript:if(retiraCoberturasDependentes('+cobAtual[4]+','+i+')){listaCoberturas();Fgravar(0);}"><img src="/img/bExcluir.gif" border="0"></a>';
    	          if(cobAtual[2]>0)    str2 = str2 + '&nbsp;<a target="iQuestionarios" href="../dados_cob_qst_preench.asp?codCobertura='+cobAtual[1]+'@'+cobAtual[6]+'@'+cobAtual[2]+'@'+i+'@&codLocalRisco='+codLocalRisco+'&codTipQstn=3&combo=1&cabec=1"><img src="/img/arquivo.gif" border="0"></a>';
                  str2 = str2 + '</td><td class="td_dado"' + marc2 + ' width="55">&nbsp;' + cobAtual[1] + '</td><td class="td_dado"' + marc2 + ' width="326"> &nbsp;' + cobAtual[6] + '</td>';
                  <%If tipoSelCob = 2 Then 'percentual%>
                  str2 = str2 + '<td class="td_dado"' + marc2 + ' width="160">' + FormatNumber(cobAtual[8], 2, 2) +'% at� '+ FormatNumber(cobAtual[9], 2, 2) + '%</td><td class="td_dado"' + marc2 + ' width="200" align="right">';
                  str2 = str2 + '<input type="text" name="perc_is_' + cobAtual[1] + '" onfocus="onfocusTeste(this);" onblur="if(entra_pc_iptc(this,'+i+')){onblurTeste(this);}"';
                  str2 = str2 + ' maxlength="7" size="5" onKeyUp="FormatValorContinuo(this,2);return autoTab(this, 7, event);" style="text-align:right;" value="';
                  if(parseFloat(cobAtual[13].replace(',','.'))>0) str2 = str2 + FormatNumber(cobAtual[13].toString().replace('.',','), 2, 2);
                  if(cobAtual[12]=='S'){
                     str2 = str2 + '">&nbsp;<input type="text" name="valor_is_' + cobAtual[1] + '" onfocus="onfocusTeste(this);" onblur="if(entra_vl_iptc(this,'+i+')){onblurTeste(this);}"';
                     str2 = str2 + ' maxlength="19" size="17" onKeyUp="FormatValorContinuo(this,2);return autoTab(this, 19, event);" style="text-align:right;" value="';
                     if(cobAtual[5]>0)  str2 = str2 + FormatNumber(cobAtual[5].toString().replace('.',','), 2, 2);
                  }
                  str2 = str2 + '">';
                  <%ElseIf tipoSelCob = 3 Then%>
                  str2 = str2 + '<td class="td_dado"' + marc2 + ' width="160">' + FormatNumber(cobAtual[10], 2, 2) +' at� '+ FormatNumber(cobAtual[11], 2, 2) + '</td><td class="td_dado"' + marc2 + ' width="200" align="right">';
                  str2 = str2 + '<input type="text" name="valor_is_' + cobAtual[1] + '" onfocus="onfocusTeste(this);" onblur="if(entra_vl_iptc(this,'+i+')){onblurTeste(this);}"';
                  str2 = str2 + ' maxlength="19" size="20" onKeyUp="FormatValorContinuo(this,2);return autoTab(this, 19, event);" style="text-align:right;" value="';
                  if(cobAtual[5]>0) str2 = str2 + FormatNumber(cobAtual[5].toString().replace('.',','), 2, 2);
                  str2 = str2 + '">';
                  <%Else%>
                  str2 = str2 + '<td class="td_dado"' + marc2 + ' width="300">&nbsp;';
                  <%End If
                    If lmi_S = "checked " Then%>
                  var varListaLMI = '<%=varListaLMI%>';
                  var locaisLMI   = eval('document.formProc.cpoGrav_LocR_'+i+'.value.split(",")');
                  if(locaisLMI[0]!='')
                      for(iLocR=0;iLocR<locaisLMI.length;iLocR++)
                          varListaLMI = varListaLMI.replace(locaisLMI[iLocR]+'|N|@',locaisLMI[iLocR]+'|S|@');
                  str2 = str2 + '<input type="button" value="locais" onclick='+"'"+'javascript:window.open("../rpc/dados_cob_risco.asp?locais='+varListaLMI+'&nome='+cobAtual[6]+'&item='+i+'","frameLocal","resizable=0,scrollbars=0,width=500,height=300,top=400,left=300,toolbar=0,directories=0,status=0,menubar=0");'+"'"+'>';
                  <%End If%>
 		          str2 = str2 + '</td></tr>';
 		          if(marc2=='') marc2=' style=";background-color:#f1F1f1"';
		          else marc2 = '';
		          if(cobAtual[5]>0) eval("document.formProc.cpoGrav_CodC_"+i+".value=cobAtual[1]");
		       }
            }
        }
        document.all.tblDisponiveis.innerHTML='<table cellpadding="0" cellspacing="0" border="1" width="823" class="escolha" bordercolor="#EEEEEE" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF" style="border-collapse: collapse">'+str1+'</table>';
        document.all.tblSelecionado.innerHTML='<table cellpadding="0" cellspacing="0" border="1" width="823" class="escolha" bordercolor="#EEEEEE" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF" style="border-collapse: collapse">'+str2+'</table>';
    }

    function entra_pc_iptc(obj, item){
        if(itemA!=item){
           gravaAba('');
           opcaoAnterior= 'A';
           itemA= item;
        }
        var valor = obj.value;
        while(valor.indexOf('.')>=0){valor=valor.replace('.','');}
        valor=valor.replace(',','.');

// Estrutura arrCoberturas:Conj.Cob|Cod.Cob|C�d.Qstn|Exibicao|Item Pai|Valor IPTC.Gravado|Nome|Acumula|Lim.Perc.Min|Lim.Perc.Max|Lim.Valor Min|Lim.Valor Max|Cob.Basica|Perc.IPTC.Gravado|
        var cobAtual = arrCoberturas[item].split("|");

	    var valormin = parseFloat(cobAtual[8]+'.00');
 	    var valormax = parseFloat(cobAtual[9]+'.00');
        if((valor<valormin)||(valor>valormax)){
            window.alert("Valor deve estar entre "+ cobAtual[8] +"% e "+cobAtual[9] +"%.");
        }
        if(cobAtual[12]=='S') valorCobBasica = -99;
        else{
            if(valorCobBasica==0){
               window.alert("Preencha antes o valor da Cobertura B�sica.");
               obj.value = dadoTesteCampo;
               return false;
            }
        }
        cobAtual[13]=valor;
        arrCoberturas[item] = cobAtual[0] + '|' + cobAtual[1] + '|' + cobAtual[2] + '|' + cobAtual[3] + '|' + cobAtual[4] + '|' + cobAtual[5] + '|' + cobAtual[6] + '|' + cobAtual[7] + '|' + cobAtual[8] + '|' + cobAtual[9] + '|' + cobAtual[10] + '|' + cobAtual[11] + '|' + cobAtual[12] + '|' + cobAtual[13] + '|' ;
        return true;
    }
 
    function entra_vl_iptc(obj, item){
        if(itemA!=item){
           gravaAba('');
           opcaoAnterior= 'A';
           itemA= item;
        }
        var valor = obj.value;
        while(valor.indexOf('.')>=0){valor=valor.replace('.','');}
        valor=valor.replace(',','.');

        if(valor!=dadoTesteCampo){
	        var cobAtual = arrCoberturas[item].split("|");

	        var valormin = parseFloat(cobAtual[10]+'.00');
 	        var valormax = parseFloat(cobAtual[11]+'.00');
            if((valor<valormin)||(valor>valormax)){
                window.alert("Valor deve estar entre "+FormatNumber(cobAtual[10], 2, 2)+" e "+FormatNumber(cobAtual[11], 2, 2)+".");
            }

            if(cobAtual[12]=='S') valorCobBasica = valor;
            if(valorCobBasica>-99){               // criado para obrigar a entrada do valor da cobertura basica, sem calcular limite das outras coberturas em rela��o � basica
                if(valorCobBasica==0){
                   window.alert("Preencha antes o valor da Cobertura B�sica.");
                   obj.value = dadoTesteCampo;
                   return false;
                }
                if(valor<(valorCobBasica*cobAtual[8]/100)){
                   window.alert("Valor deve ser maior que "+FormatNumber(cobAtual[8], 2, 2)+"% da Cobertura B�sica.");
                   obj.value = dadoTesteCampo;
                   return false;
                }
                if(valor>(valorCobBasica*cobAtual[9]/100)){
                   window.alert("Valor deve ser menor que "+FormatNumber(cobAtual[9], 2, 2)+"% da Cobertura B�sica.");
                   obj.value = dadoTesteCampo;
                   return false;
                }
            }

            cobAtual[5]=valor;
            arrCoberturas[item] = cobAtual[0] + '|' + cobAtual[1] + '|' + cobAtual[2] + '|' + cobAtual[3] + '|' + cobAtual[4] + '|' + cobAtual[5] + '|' + cobAtual[6] + '|' + cobAtual[7] + '|' + cobAtual[8] + '|' + cobAtual[9] + '|' + cobAtual[10] + '|' + cobAtual[11] + '|' + cobAtual[12] + '|' + cobAtual[13] + '|' ;

            if(cobAtual[7]=='S'){
                while(dadoTesteCampo.indexOf('.')>=0){dadoTesteCampo=dadoTesteCampo.replace('.','');}
                dadoTesteCampo = dadoTesteCampo.replace(',','.');
                valorAcum = ((valorAcum*100) + (valor*100) - (dadoTesteCampo*100)) / 100;
                document.all.acumula_is.innerHTML = FormatNumber(valorAcum.toString().replace('.',','), 2, 2);
            }
        }
        return true;
    }

	function lmiTeste(){
	<% if trim(lmi_S) = "" then%>
		valor = 'N';
	<% else %>
		valor = 'S';
	<% end if %>
		if(confirm('A troca do LMI implica na exclus�o completa das coberturas existentes para esse produto.')){
		    gravouDado();
			document.formProc.action = "";
			document.formProc.gravarDados.value = "S";
			document.formProc.excluirDados.value = "S";
			document.formProc.submit();
		} else {
			if(valor=="S"){
				document.formProc.valor_lmi[0].checked = false;
				document.formProc.valor_lmi[1].checked = true;
			} else {
				document.formProc.valor_lmi[0].checked = true;
				document.formProc.valor_lmi[1].checked = false;
			}
		}
	}

	function lmiSubmit(){
		document.formProc.action = "";
		document.formProc.submit();
	}
//-->
</script>
<%
    preencheChaves
    If var_NR_CTC_ATUAL <> "" Then
       NR_CTC_NOVO_array    = Split(var_NR_CTC_ATUAL&"|","|")
       objCon.pNR_CTC_SGRO  = NR_CTC_NOVO_array(0)
       objCon.pNR_VRS_CTC   = NR_CTC_NOVO_array(1)
    End If

    If lmi_S = "checked " Then             'Le as coberturas de todos os locais de risco
       Set rsCoberturas = objCon.mLerCoberturasParaInclusaoLMI()
    Else                                   'Le as coberturas do local de risco escolhido
       objCon.pNR_SEQL_CBT_CTC = campo
       Set rsCoberturas = objCon.mLerCoberturasParaInclusao()
    End If

    rsCoberturas.ActiveConnection = Nothing
    If rsCoberturas.EOF Then
       Response.Write "&nbsp;Nenhuma cobertura associada a este produto foi encontrada."
    Else
       cContador       = 0
       valor_acumulado = 0
       strComandosQst  = "$COB" 'inicia com esse sinal para marcar a posi��o onde inserir o codigo da cobertura, que tem questionario preenchido
       strComandosChk  = ""     'para constru��o do comando de checagem das coberturas dependentes, quando a cobertura-pai j� estiver gravada
       StringJScpt     = ""     'para constru��o do vetor JS arrCoberturas
       numCobBasica    = "#"    'inicia com esse sinal, na primeira cobertura basica � marcado o codigo dela, depois marca sempre com "@", indicando que ha mais de uma cobertura basica para esse produto
       Redim arrCobertASP(rsCoberturas.RecordCount,2)

       Do While NOT rsCoberturas.EOF

'         Array arrCobertASP, dimens�o zero: guarda a chave da cobertura
'                             dimens�o um: usado para montar o script JS, que cria outro vetor carregado no navegador para montagem da lista
'                                         Esse outro vetor, chamado arrCoberturas, esta dividido pelo caracter "|"
'         O arrCobertASP serve apenas para organizar a montagem do vetor arrCoberturas em Javascript.
'         O arrLimitesSelecao � indexado pelo codigo do conjunto, � um vetor JS para checagem de limites de sele��o

            arrCobertASP(cContador,1) = rsCoberturas("CD_CJT_CBT") & "|" & rsCoberturas("CD_CBT")
            arrCobertASP(cContador,0) = rsCoberturas("CD_CJT_CBT") & "|" & rsCoberturas("CD_CBT") & "|" & rsCoberturas("NR_QSTN") & "|"

            If rsCoberturas("CBT_Basica") = "S" Then
               If numCobBasica = "#" Then
                  numCobBasica = cContador
               Else
                  numCobBasica = "@"
               End If
            End If
 
            If isNull(rsCoberturas("CBT_Existente")) Or rsCoberturas("CBT_Existente") = "" Then     'Cobertura n�o selecionada antes da leitura dos dados
               campoLocal = ""
               If isNull(rsCoberturas("CBT_ANT")) Then
		          valorChecado = "M|-99|"             'M- para coberturas que aparecem na primeira lista, disponiveis para serem selecionadas
		       Else
		          valorChecado = "D|#|"               'D- n�o habilita enquanto a cobertura-pai n�o for selecionada
		          For nInt = 0 To Ubound(arrCobertASP) - 1
                      If (rsCoberturas("CJT_CBT_ANT")&"|"&rsCoberturas("CBT_ANT")) = arrCobertASP(nInt,1) Then
                          valorChecado = Replace(valorChecado,"#",nInt)
                          Exit For
                      End If
		          Next
		       End If
            Else
               If lmi_S = "checked " Then             'Cotacao LMI
                  campoLocal  = Left(rsCoberturas("CBT_Existente"),Len(rsCoberturas("CBT_Existente"))-1)
               End If

	           strComandosChk = strComandosChk & "checaCoberturasDependentes(" & cContador & ");opcaoAnterior='';"    'string que checa as coberturas dependentes das ja selecionadas

               If rsCoberturas("NR_QSTN") > 0 Then   'se questionario respondido
                  strComandosQst = Replace(strComandosQst,"$COB",rsCoberturas("CD_CBT")&"@"&rsCoberturas("NOME")&"@"&rsCoberturas("NR_QSTN")&"@"&cContador&"@|$COB")
               End If

		       If isNull(rsCoberturas("CBT_ANT")) Then
		          valorChecado = "S|-99|"             'S- Aparece na segunda lista, porque j� foi gravada
		       Else
		          valorChecado = "S|#|"
		          For nInt = 0 To Ubound(arrCobertASP) - 1
                      If (rsCoberturas("CJT_CBT_ANT")&"|"&rsCoberturas("CBT_ANT")) = arrCobertASP(nInt,1) Then
                          valorChecado = Replace(valorChecado,"#",nInt)
                          Exit For
                      End If
		          Next
		       End If
            End If

            If IsNull(rsCoberturas("VL_IPTC_CBT_CTC")) Then
               arrCobertASP(cContador,0) = arrCobertASP(cContador,0) & valorChecado & "0|"
            Else
               arrCobertASP(cContador,0) = arrCobertASP(cContador,0) & valorChecado & rsCoberturas("VL_IPTC_CBT_CTC") & "|"
               If rsCoberturas("CBT_Acumulativa") = "S" Then
                  valor_acumulado = valor_acumulado + CDbl(rsCoberturas("VL_IPTC_CBT_CTC"))
               End If
            End If
 
            arrCobertASP(cContador,0) = arrCobertASP(cContador,0) & rsCoberturas("NOME") & "|" & rsCoberturas("CBT_Acumulativa") & "|" & rsCoberturas("PC_MIN_IPTC_SGRD") & "|" & rsCoberturas("PC_MAX_IPTC_SGRD") & "|" & rsCoberturas("VL_MIN_IPTC_SGRD") & "|" & rsCoberturas("VL_MAX_IPTC_SGRD") & "|" & rsCoberturas("CBT_Basica") & "|"
            If IsNull(rsCoberturas("PC_IPTC_CBT_CTC")) Then
               arrCobertASP(cContador,0) = arrCobertASP(cContador,0) & "0|"
            Else
               arrCobertASP(cContador,0) = arrCobertASP(cContador,0) & rsCoberturas("PC_IPTC_CBT_CTC") & "|"
            End If
            arrCobertASP(cContador,0) = arrCobertASP(cContador,0) & "';arrLimitesSelecao["&rsCoberturas("CD_CJT_CBT")&"]='"&rsCoberturas("QT_MIN_SEL") & "@" & rsCoberturas("QT_MAX_SEL") & "@0@N@';" & vbCrLf

           'Cria objetos escondidos para guardar valores chaves para grava��o
            Response.Write "<input type=""hidden"" name=""cpoGrav_CjtC_" & cContador & """ value=""" & rsCoberturas("CD_CJT_CBT") & """><input type=""hidden"" name=""cpoGrav_CodC_" & cContador & """>" & vbCrLf
            Response.Write "<input type=""hidden"" name=""cpoGrav_QSTN_" & cContador & """ value=""""><input type=""hidden"" name=""cpoGravCQSTN_" & cContador & """ value=""" & rsCoberturas("NR_QSTN") & """>" & vbCrLf
            Response.Write "<input type=""hidden"" name=""cpoGrav_LocR_" & cContador & """ value=""" & campoLocal & """>" & vbCrLf

 		    rsCoberturas.MoveNext
            cContador = cContador + 1
       Loop
      
       For cContador = 0 To Ubound(arrCobertASP) - 1
           StringJScpt = StringJScpt & "arrCoberturas[" & cContador & "]='" & arrCobertASP(cContador,0)
       Next
       StringJScpt = StringJScpt & "valorAcum=" & valor_acumulado & ";"
%>
<input type="hidden" name="qtdCoberturas" value="<%=rsCoberturas.RecordCount-1%>">
<table cellpadding="0" cellspacing="0" border="0" width="840" class="escolha">
	 <tr><td colspan="2" class="td_label_negrito" width="800" height="25">&nbsp;Coberturas Dispon�veis</td></tr>
	 <tr><td colspan="2" width="840"><table cellpadding="0" cellspacing="0" width="840" border="1" bordercolor="#EEEEEE" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF" style="border-collapse: collapse"><tr>
	   <td class="td_label_negrito" width="39">&nbsp;</td>
	   <td class="td_label_negrito" width="65">C�d. Cob</td>
       <td class="td_label_negrito" width="353">Cobertura</td>
       <td class="td_label_negrito" width="197">Valores Limites</td></td>
       <td class="td_label_negrito" width="164" >Dependente de (Cjt/Cob)</td></tr></table></td></tr>
	 <tr><td colspan="2" width="840">
  	 <div id="tblDisponiveis" style="position:relative; overflow:scroll; width:840; height:153;">&nbsp;</div></td></tr>
     <tr><td colspan="2" class="td_label_negrito" width="800" height="25">&nbsp;Coberturas Selecionadas</td></tr>
	 <tr><td colspan="2" width="840"><table cellpadding="0" cellspacing="0" width="840" border="1" bordercolor="#EEEEEE" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF" style="border-collapse: collapse"><tr>
       <td class="td_label_negrito" width="34">&nbsp;</td>
       <td class="td_label_negrito" width="62">C�d. Cob</td>
       <td class="td_label_negrito" width="307">Cobertura</td>
      <%If tipoSelCob = 1 Then
           Response.Write "<td class=""td_label_negrito"" width=""300"">&nbsp;</td>"
        Else
           Response.Write "<td class=""td_label_negrito"" width=""239"">Valores Limites</td><td class=""td_label_negrito"" width=""115"">Valores</td>" 
        End If%>
       </tr></table></td></tr>
     <tr><td colspan="2" width="840">
   	 <div id="tblSelecionado" style="position:relative; overflow:scroll; width:840; height:153;">&nbsp;</div></td></tr>	
      <%If tipoSelCob = 3 Then%>
	<tr>
		<td class="td_label_negrito" width="75%" height="25">Valor IS Total</td>
		<td class="td_label" id="acumula_is" width="25%"><%=FormatNumber(valor_acumulado,2)%></td>
	</tr>
 	  <%End If%>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">
        <iframe id="iQuestionarios" name="iQuestionarios" scrolling="yes" frameborder="0" width="840" height="166"></iframe></td>
        <iframe id="iFrameEscondidoQ" name="iFrameEscondidoQ" style="DISPLAY: none; VISIBILITY: hidden"></iframe>
       </td>
	</tr>
<%      If strComandosQst = "$COB" Then
           strComandosQst = ""
        Else                                 'usado para preencher os questionarios ja gravados na base de dados
           strComandosQst = "window.open(""../dados_cob_qst_preench.asp?codCobertura="&Replace(strComandosQst,"$COB","")&"&codLocalRisco="&campo&"&codTipQstn=3&combo=1"",""iFrameEscondidoQ"");"    'Comando que serve para preencher questionarios de coberturas ja gravadas
        End If    

        StringJScpt = "var arrCoberturas=new Array();var arrLimitesSelecao=new Array();var codLocalRisco=" & campo & ";var itemA=0; " & vbCrLf & StringJScpt
        If numCobBasica <> "@" Then    'Para produtos que tem uma �nica cobertura b�sica
           StringJScpt = StringJScpt & "mudaOpcaoBasica("& numCobBasica &");"
           strOpcaoAnt = "if(valorCobBasica==0) opcaoAnterior=""I"";"
        End If
        Response.Write "<script>"& StringJScpt & strComandosChk & strComandosQst & "listaCoberturas();" & strOpcaoAnt & "</script>"
    End If
End If%>
        <iframe id="iFrameEscondido1" name="iFrameEscondido1" style="DISPLAY: none; VISIBILITY: hidden"></iframe>
        <iframe id="iFrameEscondido2" name="iFrameEscondido2" style="DISPLAY: none; VISIBILITY: hidden"></iframe>