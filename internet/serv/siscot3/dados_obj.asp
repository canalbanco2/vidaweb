<%
dim arrParametro
arrParametro = Session("parametro")

If opcaoPorContrato = "T" Then
    Set rsDados = objCon.mLerDadosFinanceirosContrato(1)
Else
    Set rsDados = objCon.mLerDadosFinanceiros()
End If
rsDados.ActiveConnection = Nothing

If NOT rsDados.EOF Then
	If isnull(rsDados("VL_IPTC_MOEN_CTC")) then 
	   VL_IPTC_MOEN_CTC = 0
	Else
	   VL_IPTC_MOEN_CTC = rsDados("VL_IPTC_MOEN_CTC")
	End If

	If isnull(rsDados("VL_PREM_MOEN_CTC")) then 
	   VL_PREM_MOEN_CTC = 0
	Else
	   VL_PREM_MOEN_CTC = rsDados("VL_PREM_MOEN_CTC")
	End If

	If opcaoPorContrato = "T" Then
		Set rsSubgrupo = Objcon.mLerSubgrupoCotacao(True)
	Else
		Set rsSubgrupo = Objcon.mLerSubgrupoCotacao(False)
	End If

	If NOT rsSubgrupo.EOF Then
	   
	Do While not rsSubgrupo.EOF

		Set rsDados = Nothing
		objCon.pNR_CTC_SGRO = rsSubgrupo("NR_CTC_SGRO")
		objCon.pNR_VRS_CTC =  rsSubgrupo("NR_VRS_CTC")

		RESPONSE.Write "Dados Financeiros 1: " & opcaoPorContrato%><BR><%
		If opcaoPorContrato = "T" Then
			Set rsDados = Objcon.mLerDadosFinanceirosContrato(1)
		Else
			Set rsDados = Objcon.mLerDadosFinanceiros(1)
		End If
		If NOT rsDados.EOF Then
			if not isnull(rsDados("VL_IPTC_MOEN_CTC")) then
				VL_IPTC_MOEN_CTC = cdbl(VL_IPTC_MOEN_CTC) + cdbl(rsDados("VL_IPTC_MOEN_CTC"))
			end if

			if not isnull(rsDados("VL_PREM_MOEN_CTC")) then
				VL_PREM_MOEN_CTC = cdbl(VL_PREM_MOEN_CTC) + cdbl(rsDados("VL_PREM_MOEN_CTC"))
			end if
		End If

		rsSubgrupo.MoveNext
	Loop
	End If

   VL_IPTC_MOEN_CTC = formatnumber(VL_IPTC_MOEN_CTC,2)
   VL_PREM_MOEN_CTC = formatnumber(VL_PREM_MOEN_CTC,2)
   
%>
	<tr>
		<td nowrap class="td_label">&nbsp;Moeda do seguro da cota��o</td>
		<td class="td_dado" width="800">&nbsp;
			<%=testaDadoVazio(rsDados,"CD_MOE_SGRO_CTC")%> - <%=testaDadoVazio(rsDados,"NOME_MOE_SGRO")%>
		</td>
	</tr>												
	<tr>
		<td nowrap class="td_label">&nbsp;Moeda de origem da cota��o</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazio(rsDados,"CD_MOE_OGM_CTC") & " - " & testaDadoVazio(rsDados,"NOME_MOE_OGM")%>
		</td>
	</tr>
	<%if arrParametro(9) = Application("WF_HAB") then%>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor IS</td>
		<td class="td_dado">&nbsp;
				<%
				'Response.Write "<br>" & objcon.pCD_PRD & ", " & objcon.pCD_MDLD & ", " & objcon.pCD_ITEM_MDLD
				'Response.Write "<P>"
				retorno = objCon.mValidarCapitalSegurado(CStr(VL_IPTC_MOEN_CTC))
				if isnumeric(retorno) then
					'Response.Write "<font color=red>"
					if cint(retorno) = -1 then
						response.Write"<font color=""red""><b>" & VL_IPTC_MOEN_CTC & "</b>"
						Response.Write " O Valor IS � menor que o m�nimo cadastrado"
					elseif cint(retorno) = 1 then
						response.Write"<font color=""red""><b>" & VL_IPTC_MOEN_CTC & "</b>"
						Response.Write " O Valor IS � maior que o m�ximo cadastrado"
					else
						response.Write VL_IPTC_MOEN_CTC 
					end if
						Response.Write "</font>"
				elseif trim(retorno) <> "" then
					Response.Write "<p>OCORREU UM ERRO.<BR>" & retorno
					'Response.redirect("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
					Response.End 
				else
					Response.Write VL_IPTC_MOEN_CTC
				end if
				%>		
		</td>
	</tr>
	<%end if%>
	<tr>
		<td nowrap class="td_label">&nbsp;Percentual do custo de ap�lice</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"PC_CST_APLC_CTC")%>
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Percentual IOF</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"PC_IOF_CTC")%>
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Percentual de corretagem</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"PC_CRE_CTC")%>
		</td>
	</tr>
	<%if arrParametro(9) <> Application("WF_HAB") then%>
	<tr>
		<td nowrap class="td_label">&nbsp;Percentual desconto t�cnico</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"PC_DSC_TCN_CTC")%>
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Margem Comercial&nbsp;</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"Margem_Comercial")%>
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Qtd m�xima parcelas pgto do pr�mio</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"QT_PCL_PGTO_CTC")%>
		</td>
	</tr>
	<%end if%>
	<%if cbool(ValidaAtividadeWorkflow(tp_wf_id, tp_ativ_id, AT_REVISAO_PREMIO)) then%>
	<tr>
		<td nowrap class="td_label">&nbsp;Percentual desconto comercial</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"PC_DSC_CML_CTC")%>
		</td>
	</tr>
	<%end if%>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<%if arrParametro(9) <> Application("WF_HAB") then%>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor IS</td>
		<td class="td_dado">&nbsp;<%=VL_IPTC_MOEN_CTC%>
				<%
				'Response.Write "<br>" & objcon.pCD_PRD & ", " & objcon.pCD_MDLD & ", " & objcon.pCD_ITEM_MDLD
				'Response.Write "<P>"
				retorno = objCon.mValidarCapitalSegurado(CStr(VL_IPTC_MOEN_CTC))
				if isnumeric(retorno) then
					Response.Write "<font color=red>"
					if cint(retorno) = -1 then
						Response.Write " O Valor IS � menor que o m�nimo cadastrado"
					elseif cint(retorno) = 1 then
						Response.Write " O Valor IS � maior que o m�ximo cadastrado"
					end if
						Response.Write "</font>"
				elseif trim(retorno) <> "" then
					Response.Write "<p>OCORREU UM ERRO.<BR>" & retorno
					'Response.redirect("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
					Response.End 
				end if
				%>		
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor pr�mio NET</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"VL_SGRA_MOEN_CTC")%>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor desconto t�cnico</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"VL_DSC_TCN_CTC")%>
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor pr�mio NET com desconto t�cnico</td>
		<td class="td_dado">&nbsp;
			<%
			if trim(rsDados("VL_DSC_TCN_CTC")) <> "" then
				Response.Write formatnumber(cdbl(testaDadoVazioN(rsDados,"VL_SGRA_MOEN_CTC")) - cdbl(rsDados("VL_DSC_TCN_CTC")),2)
			else
				Response.Write "0,00"
			end if
			%>
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor do pr�mio l�quido</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"VL_LQDO_MOEN_CTC")%>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor do custo da ap�lice</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"VL_CST_APLC_CTC")%>
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor IOF</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"VL_IOF_CTC")%>
		</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor do pr�mio bruto</td>
		<td class="td_dado">&nbsp;
			<%
			premio_bruto = 0

			if trim(rsDados("VL_LQDO_MOEN_CTC")) <> "" then
				premio_bruto = premio_bruto + cdbl(rsDados("VL_LQDO_MOEN_CTC"))
			end if
			if trim(rsDados("VL_CST_APLC_CTC")) <> "" then
				premio_bruto = premio_bruto + cdbl(rsDados("VL_CST_APLC_CTC"))
			end if
			if trim(rsDados("VL_IOF_CTC")) <> "" then
				premio_bruto = premio_bruto + cdbl(rsDados("VL_IOF_CTC"))
			end if

			Response.Write formatnumber(premio_bruto,2)
			%>
		</td>
	</tr>
	<%end if%>
	<%If cbool(ValidaAtividadeWorkflow(tp_wf_id, tp_ativ_id, AT_REVISAO_PREMIO)) then%>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor desconto comercial</td>
		<td class="td_dado">&nbsp;
			<%=testaDadoVazioN(rsDados,"VL_DSC_CML_CTC")%></td></tr>
	<%End If

	 'Quando Seguro Agricola
	 Set orstmp = objcon.mlervaloressubvencao()
	 orstmp.activeconnection = nothing
	 
	  If CD_ITEM_MDLD = 201 Then  
         Set rsSubvenc = objCon.mLerValoresSubvencao()
         rsSubvenc.ActiveConnection = Nothing%>
    	<tr><td nowrap class="td_label" colspan=2>&nbsp;<span class="sub_titulo" style="font-size:12px"><b>
    	 <%If NOT rsSubvenc.EOF Then
      	       If rsSubvenc("TP_SUBVENCAO") = "Estadual" Then
      	          Response.Write "<b>::Subven��o Estadual</b>"
      	       Else
     	          If rsSubvenc("SUBVENCAO_GOVERNO") = "Sim" Then
     	          Response.Write "::Subven��o Federal"%>
		    <tr>
				<td nowrap class="td_label">&nbsp;Primeira parcela</td>
					<td class="td_dado">&nbsp;<%=rsSubvenc("1_parc")%></td></tr>
			<tr><td nowrap class="td_label">&nbsp;Segunda parcela</td>
					<td class="td_dado">&nbsp;<%=rsSubvenc("2_parc")%></td></tr>
			<tr><td nowrap class="td_label">&nbsp;Subven��o (%)</td>
					<td class="td_dado">&nbsp;<%=FormatNumber(CDbl(rsSubvenc("perc_subvencao"))*100, 2)%></td></tr>
				<%
      	          Else
      		         Response.Write "Sem Subven��o"
      		      End If
      		   End If
      	   End If
      	   Set rsSubvenc = nothing%></span></td></tr> </b>
      	<%End If
 End If

 Set rsDados = Nothing
%>