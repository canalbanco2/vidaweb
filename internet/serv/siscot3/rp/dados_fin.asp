<!--#include virtual = "/padrao.asp"-->
<%
set rs = objcon.mLerDadosFinanceiros(1) '1 = RE
rs.ActiveConnection = Nothing

if request("primeiro") = "false" then
	VL_IPTC_MOEN_CTC = request("VL_IPTC_MOEN_CTC")
	VL_PREM_MOEN_CTC = request("VL_PREM_MOEN_CTC")
	VL_PRMO_MOEN_CTC = request("VL_PRMO_MOEN_CTC")
	VL_SGRA_MOEN_CTC = request("VL_SGRA_MOEN_CTC")
	VL_PCL_MOEN_CTC = request("VL_PCL_MOEN_CTC")
	VL_LQDO_MOEN_CTC = request("VL_LQDO_MOEN_CTC")
	VL_IPTC_MOEE_CTC = request("VL_IPTC_MOEE_CTC")
	VL_PREM_MOEE_CTC = request("VL_PREM_MOEE_CTC")
	VL_PRMO_MOEE_CTC = request("VL_PRMO_MOEE_CTC")
	VL_SGRA_MOEE_CTC = request("VL_SGRA_MOEE_CTC")
	VL_PCL_MOEE_CTC = request("VL_PCL_MOEE_CTC")
	VL_LQDO_MOEE_CTC = request("VL_LQDO_MOEE_CTC")
	Margem_Comercial = request("Margem_Comercial")
	QT_PCL_PGTO_CTC = request("QT_PCL_PGTO_CTC")
	VL_CST_APLC_CTC = request("VL_CST_APLC_CTC")
	PC_DSC_TCN_CTC = request("PC_DSC_TCN_CTC")
	VL_DSC_TCN_CTC = request("VL_DSC_TCN_CTC")
	PC_DSC_CML_CTC = request("PC_DSC_CML_CTC")
	VL_DSC_CML_CTC = request("VL_DSC_CML_CTC")
	VL_IOF_CTC = request("VL_IOF_CTC")
	
else

	CD_TIP_OPR_CTC = testaDadoVazioN(rs,"CD_TIP_OPR_CTC")
	VL_IPTC_MOEN_CTC = testaDadoVazioN(rs,"VL_IPTC_MOEN_CTC")
	VL_PREM_MOEN_CTC = testaDadoVazioN(rs,"VL_PREM_MOEN_CTC")
	
	VL_PRMO_MOEN_CTC = testaDadoVazioN(rs,"VL_PRMO_MOEN_CTC")
	VL_SGRA_MOEN_CTC = testaDadoVazioN(rs,"VL_SGRA_MOEN_CTC")
	VL_PCL_MOEN_CTC = testaDadoVazioN(rs,"VL_PCL_MOEN_CTC")
	VL_LQDO_MOEN_CTC = testaDadoVazioN(rs,"VL_LQDO_MOEN_CTC")
	VL_IPTC_MOEE_CTC = testaDadoVazioN(rs,"VL_IPTC_MOEE_CTC")
	VL_PREM_MOEE_CTC = testaDadoVazioN(rs,"VL_PREM_MOEE_CTC")
	VL_PRMO_MOEE_CTC = testaDadoVazioN(rs,"VL_PRMO_MOEE_CTC")
	VL_SGRA_MOEE_CTC = testaDadoVazioN(rs,"VL_SGRA_MOEE_CTC")
	VL_PCL_MOEE_CTC = testaDadoVazioN(rs,"VL_PCL_MOEE_CTC")
	VL_LQDO_MOEE_CTC = testaDadoVazioN(rs,"VL_LQDO_MOEE_CTC")
	VL_CST_APLC_CTC = testaDadoVazioN(rs,"VL_CST_APLC_CTC")
	VL_IOF_CTC = testaDadoVazioN(rs,"VL_IOF_CTC")

	PC_DSC_CML_CTC = testaDadoVazioN(rs,"PC_DSC_CML_CTC")
	VL_DSC_CML_CTC = testaDadoVazioN(rs,"VL_DSC_CML_CTC")

	PC_DSC_TCN_CTC = testaDadoVazioN(rs,"PC_DSC_TCN_CTC")
	Margem_Comercial = testaDadoVazioN(rs,"Margem_Comercial")
	QT_PCL_PGTO_CTC = testaDadoVazioN(rs,"QT_PCL_PGTO_CTC")
	VL_DSC_TCN_CTC = testaDadoVazioN(rs,"VL_DSC_TCN_CTC")

end if

set rsCotacao = objcon.mLerCotacao()
rsCotacao.ActiveConnection = Nothing
data_fim_vigencia = rsCotacao("DT_INC_VGC_CTC")
data_inicio_vigencia = rsCotacao("DT_FIM_VGC_CTC")
set rsCotacao = nothing
if not isnull(data_fim_vigencia) and not isnull(data_inicio_vigencia) then
	dias = datediff("d",data_fim_vigencia,data_inicio_vigencia)
	parcelas = int(dias/30)
else
	dias = 0
	parcelas = QT_PCL_PGTO_CTC
end if

if trim(VL_IPTC_MOEN_CTC) = "" then
	VL_IPTC_MOEN_CTC = "0,00"
end if
if trim(VL_PREM_MOEN_CTC) = "" then
	VL_PREM_MOEN_CTC = "0,00"
end if
if trim(Margem_Comercial) = "" then
	Margem_Comercial = "0,00"
end if
%>
<script language="JavaScript">
<!--
function FormatNumber(vr,tamdec,truncate){
	//A fun��o espera receber casas decimais com V�RGULA
	// truncate = 1 - n�o altera o valor de casas decimais, caso seja maior do que o tamanho decidido
	// truncate = 2 - remove os valores superiores as casa decimais
	// truncate = 3 - remove os valores superiores as casa decimais e arredonda caso haja necessidade
	vp = '';
	for(f=0;f<vr.length;f++){
		if(((vr.charCodeAt(f)>=48)&&(vr.charCodeAt(f)<=57))||(vr.charCodeAt(f)==44)){
			vp=vp+vr.charAt(f);
		}
	}
	a=1;
	vr = vp;
	if(vr.length>0){
		vr = vr.split(',');
		vrIni = vr[0];
		vrIniTemp = '';
		for(f=0;f<vrIni.length;f++){
			vrIniTemp = vrIni.charAt(vrIni.length-f-1) + vrIniTemp;
			if((a==3)&&(vrIni.length!=f+1)){
				vrIniTemp = '.' + vrIniTemp;
				a=0;
			}
			a++;
		}
		vrIni = vrIniTemp;
		vrFim = "";
		if(vr.length>1){
			vrFim = vr[1];
			if(vrFim.length>tamdec){
				if(truncate==3){
					valorFinal = vrFim.substring(tamdec,tamdec + 1);
					vrFim = vrFim.substring(0,tamdec);
					if(valorFinal>4){
						vrFim = vrFim/1 + 1;
					}
				} else if(truncate==2){
					vrFim = vrFim.substring(0,tamdec);
				}
			} else {
				for(f=vrFim.length;f<tamdec;f++){
					vrFim = vrFim + '0';
				}
			}
		} else {
			for(f=0;f<tamdec;f++){
				vrFim = vrFim + '0';
			}
		}
		vr = vrIni + ',' + vrFim;
		return vr;
	}
}

function converteNumeroJavascript(valor){
	valor = valor.split(',');
	valorNovo = '';
	for(f=0;f<valor[0].length;f++){
		if((valor[0].charCodeAt(f)>=48)&&(valor[0].charCodeAt(f)<=57)){
			valorNovo=valorNovo+valor[0].charAt(f);
		}
	}
	if(valor.length>1){
		valorNovo = valorNovo + '.' + valor[1];
	}
	return valorNovo;
}

function ValidaMargemComercial()
{
	var margem_comercial = parseFloat('0' + converteNumeroJavascript(document.all.Margem_Comercial.value));

	if (margem_comercial > 100)
	{
		alert('A Margem Comercial n�o pode ser superior a 100,00');
		document.all.Margem_Comercial.value = '';
	}
}

function calculaValores()
{
	var percentual_corretagem = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_CRE_CTC.value));
	var percentual_iof = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_IOF_CTC.value));
	var premio_net = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_PREM_MOEN_CTC.value));
    var desconto_tecnico = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_DSC_TCN_CTC.value));
	var desconto_com     = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_DSC_CML_CTC.value));

    var premio_net_des   = premio_net - desconto_tecnico;

	if ((premio_net > 0) && (percentual_corretagem > 0))
		var premio_liquido = (premio_net_des / (1 - (percentual_corretagem / 100))) - desconto_com;
	else
		var premio_liquido = 0;
 
	if (premio_liquido > 0)
		var custo_apolice_temp = (premio_liquido / 10);
	else
		var custo_apolice_temp = 0;

	if (custo_apolice_temp > 60)
		var custo_apolice = 60;
	else
		var custo_apolice = custo_apolice_temp;	

	if (percentual_iof > 0)
		var valor_iof = ((premio_liquido + custo_apolice) * (percentual_iof / 100));
	else
		var valor_iof = 0;

	var valor_premio_bruto = (premio_liquido + custo_apolice + valor_iof);

    document.formProc.premio_net_desconto_tecnico.value = FormatNumber(premio_net_des.toString().replace(".", ","), 2, 2);
	document.formProc.VL_CST_APLC_CTC.value = FormatNumber(custo_apolice.toString().replace(".", ","), 2, 2);
	document.formProc.VL_IOF_CTC.value = FormatNumber(valor_iof.toString().replace(".", ","), 2, 2);
	document.formProc.VL_PREM_MOEN_CTC.value = FormatNumber(premio_net.toString().replace(".", ","), 2, 2);
	document.formProc.VL_LQDO_MOEN_CTC.value = FormatNumber(premio_liquido.toString().replace(".", ","), 2, 2);
	document.formProc.valor_premio_bruto.value = FormatNumber(valor_premio_bruto.toString().replace(".", ","), 2, 2);
}

function calculaPremioNetDescontoTecnico()
{
	var premio_net = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_PREM_MOEN_CTC.value));
	var percentual_desconto_tecnico = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_DSC_TCN_CTC.value));

	if (premio_net > 0)
		var premio_net_desconto_tecnico = (premio_net - ((premio_net * percentual_desconto_tecnico) / 100));
	else
		var premio_net_desconto_tecnico = premio_net;

	if (premio_net_desconto_tecnico > 0)
		var desconto_tecnico = (premio_net - premio_net_desconto_tecnico);
	else
		var desconto_tecnico = 0;

	document.formProc.premio_net_desconto_tecnico.value = FormatNumber(premio_net_desconto_tecnico.toString().replace(".", ","), 2, 2);
	document.formProc.VL_DSC_TCN_CTC.value = FormatNumber(desconto_tecnico.toString().replace(".", ","), 2, 2);
}

function submitdadosfinanceiros()
{
	document.formProc.action = 'liberar_cotacao_fin.asp';
	document.formProc.submit();
}

function alterarDados()
{
	var margem_comercial = parseFloat('0' + converteNumeroJavascript(document.all.Margem_Comercial.value));

	if (margem_comercial == 0)
	{
		if (!confirm('A Margem Comercial deveria ser maior que 0,00, o valor est� correto?'))
			return;
	}
	
	document.formProc.salvarfin.value = 'S';
	submitdadosfinanceiros();
}

function calculaValorDescontoComercial()
{
	var premio_net = parseFloat('0' + converteNumeroJavascript(document.formProc.VL_PREM_MOEN_CTC.value));
	var percentual_desconto_comercial = parseFloat('0' + converteNumeroJavascript(document.formProc.PC_DSC_CML_CTC.value));
	var margem_comercial = parseFloat('0' + converteNumeroJavascript(document.formProc.Margem_Comercial.value));

	if (percentual_desconto_comercial > margem_comercial)
	{
		alert('O Desconto Comercial n�o pode ser maior que a Margem Comercial');
		document.formProc.PC_DSC_CML_CTC.value = '';
		percentual_desconto_comercial = 0;
	}

	calculaValores();
}
-->
</script>
<%

if trim(request("salvarfin")) = "S" then
	set rsCotacao = objCon.mLerCotacao

	if ucase(rsCotacao("CD_ULT_EVT_CTC")) <> "T" then
		objcon.pCD_ULT_EVT_CTC = "T"
		bAtualizaCotacao = true
	else
		bAtualizaCotacao = false
	end if
	
	objCon.pVL_IPTC_MOEN_CTC = VL_IPTC_MOEN_CTC
	objCon.pVL_PREM_MOEN_CTC = VL_PREM_MOEN_CTC
	objCon.pVL_PRMO_MOEN_CTC = VL_PRMO_MOEN_CTC
	objCon.pVL_SGRA_MOEN_CTC = VL_SGRA_MOEN_CTC
	objCon.pVL_PCL_MOEN_CTC = VL_PCL_MOEN_CTC
	objCon.pVL_LQDO_MOEN_CTC = VL_LQDO_MOEN_CTC
	objCon.pVL_IPTC_MOEE_CTC = VL_IPTC_MOEE_CTC
	objCon.pVL_PREM_MOEE_CTC = VL_PREM_MOEE_CTC
	objCon.pVL_PRMO_MOEE_CTC = VL_PRMO_MOEE_CTC
	objCon.pVL_SGRA_MOEE_CTC = VL_SGRA_MOEE_CTC
	objCon.pVL_PCL_MOEE_CTC = VL_PCL_MOEE_CTC
	objCon.pVL_LQDO_MOEE_CTC = VL_LQDO_MOEE_CTC
	objCon.pMargem_Comercial = Margem_Comercial
	objCon.pQT_PCL_PGTO_CTC = QT_PCL_PGTO_CTC
	objCon.pVL_CST_APLC_CTC = VL_CST_APLC_CTC
	objCon.pPC_DSC_TCN_CTC = PC_DSC_TCN_CTC
	objCon.pVL_DSC_TCN_CTC = VL_DSC_TCN_CTC
	objCon.pPC_DSC_CML_CTC = PC_DSC_CML_CTC
	objCon.pVL_DSC_CML_CTC = VL_DSC_CML_CTC
	objCon.pVL_IOF_CTC = VL_IOF_CTC

	chave
	objCon.pWF_ID = WF_ID
	
	retorno = objCon.mAtualizarDadosFinanceiros(bAtualizaCotacao)
	if trim(retorno) <> "" then
		Response.redirect("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
		Response.End 
	end if
	
	chave
end if

%>
	<br>
	&nbsp;&nbsp;&nbsp;<input type="button" name="alterar" value="Gravar" style="width:80px;font-size:10px;margin: 2px 2px 2px 2px;" onclick="alterarDados()">
	<input type="hidden" name="salvarfin" value="N">
	<br><br>
	<input type="hidden" name="primeiro" value="false">
	<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
		<tr>
			<td nowrap class="td_label">&nbsp;Moeda do seguro da cota��o</td>
			<td class="td_dado" width="800">&nbsp;
				<%=testaDadoVazio(rs,"CD_MOE_SGRO_CTC")%> - <%=testaDadoVazio(rs,"NOME_MOE_SGRO")%>
			</td>
		</tr>												
		<tr>
			<td nowrap class="td_label">&nbsp;Moeda de origem da cota��o</td>
			<td class="td_dado">&nbsp;
				<%=testaDadoVazio(rs,"CD_MOE_OGM_CTC")%> - <%=testaDadoVazio(rs,"NOME_MOE_OGM")%>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual do custo de ap�lice</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_CST_APLC_CTC" size="16" value="<%=testaDadoVazioN(rs,"PC_CST_APLC_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual IOF</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_IOF_CTC" size="16" value="<%=testaDadoVazioN(rs,"PC_IOF_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual de corretagem</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_CRE_CTC" size="16" value="<%=testaDadoVazioN(rs,"PC_CRE_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual desconto t�cnico</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_DSC_TCN_CTC" size="16" value="<%=PC_DSC_TCN_CTC%>" onblur="javascript:calculaPremioNetDescontoTecnico();" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Margem Comercial&nbsp;</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="Margem_Comercial" size="16" OnBlur="javascript:ValidaMargemComercial();" value="<%=Margem_Comercial%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Qtd m�xima parcelas pgto do pr�mio</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="QT_PCL_PGTO_CTC" size="2" value="<%=QT_PCL_PGTO_CTC%>" maxlength="2" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Percentual desconto comercial</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="PC_DSC_CML_CTC" size="16" value="<%=PC_DSC_CML_CTC%>" onBlur="calculaValorDescontoComercial();" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right;">
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
<%
if isnull(rs("VL_IPTC_MOEN_CTC")) then 
	VL_IPTC_MOEN_CTC = 0
else
	VL_IPTC_MOEN_CTC = rs("VL_IPTC_MOEN_CTC")
end if

if isnull(rs("VL_PREM_MOEN_CTC")) then 
	VL_PREM_MOEN_CTC = 0
else
	VL_PREM_MOEN_CTC = rs("VL_PREM_MOEN_CTC")
end if

set rsSubgrupo = objcon.mLerSubgrupoCotacao

if not rsSubgrupo.eof then
	do while not rsSubgrupo.eof

		objCon.pNR_CTC_SGRO = rsSubgrupo("NR_CTC_SGRO")
		objCon.pNR_VRS_CTC =  rsSubgrupo("NR_VRS_CTC")

		set rs = objcon.mLerDadosFinanceiros(1) '1 = RE

		if not rs.eof then
			if not isnull(rs("VL_IPTC_MOEN_CTC")) then
				VL_IPTC_MOEN_CTC = cdbl(VL_IPTC_MOEN_CTC) + cdbl(rs("VL_IPTC_MOEN_CTC"))
			end if

			if not isnull(rs("VL_PREM_MOEN_CTC")) then
				VL_PREM_MOEN_CTC = cdbl(VL_PREM_MOEN_CTC) + cdbl(rs("VL_PREM_MOEN_CTC"))
			end if
		end if

		rsSubgrupo.movenext
	loop
end if

VL_IPTC_MOEN_CTC = formatnumber(VL_IPTC_MOEN_CTC,2)
VL_PREM_MOEN_CTC = formatnumber(VL_PREM_MOEN_CTC,2)

%>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor IS</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_IPTC_MOEN_CTC" size="16" value="<%=VL_IPTC_MOEN_CTC%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
				&nbsp;&nbsp;&nbsp;<font color="red"><span id="aviso">
				<%
				'retorno = objCon.mValidarCapitalSegurado(VL_IPTC_MOEN_CTC)
				if isnumeric(retorno) then
					if cint(retorno) = -1 then
						Response.Write "O Valor IS � menor que o m�nimo cadastrado"
					elseif cint(retorno) = 1 then
						Response.Write "O Valor IS � maior que o m�ximo cadastrado"
					end if
				elseif trim(retorno) <> "" then
					'Response.Write VL_IPTC_MOEN_CTC
					'Response.Write "<p>" & objcon.pCD_PRD & ", " & objcon.pCD_MDLD & ", " & objcon.pCD_ITEM_MDLD & ", " & objcon.pCD_CBT
					'Response.redirect ("msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br>" & retorno)
					'Response.End 
					%>
						<script language="javascript">
							window.document.location = 'msg_erro_cotacao.asp?btvolta=Voltar&strError=Ocorreu um erro.<br><%=retorno%>'
						</script>
					<%
				end if
				%>
				</span></font>	
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor pr�mio NET</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_PREM_MOEN_CTC" size="16" value="<%=VL_PREM_MOEN_CTC%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly></td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor desconto t�cnico</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_DSC_TCN_CTC" size="16" value="<%=VL_DSC_TCN_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor pr�mio NET com desconto t�cnico</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="premio_net_desconto_tecnico" size="16" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<!--tr>
			<td nowrap class="td_label">&nbsp;Valor de corretagem</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="valor_corretagem" size="16" value="" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr-->
		<tr>
			<td nowrap class="td_label">&nbsp;Valor do pr�mio l�quido</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_LQDO_MOEN_CTC" size="16" value="<%=VL_LQDO_MOEN_CTC%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly></td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor do custo da ap�lice</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_CST_APLC_CTC" size="16" value="<%=VL_CST_APLC_CTC%>" onKeyUp="FormatValorContinuo(this,2);" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor IOF</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_IOF_CTC" size="16" value="<%=VL_IOF_CTC%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor do pr�mio bruto</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="valor_premio_bruto" size="16" value="" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
		<tr>
			<td nowrap class="td_label">&nbsp;Valor desconto comercial</td>
			<td class="td_dado">&nbsp;
				<input type="text" name="VL_DSC_CML_CTC" size="16" value="<%=testaDadoVazioN(rs,"VL_DSC_CML_CTC")%>" maxlength="16" style="text-align:right; background-color:#EEEEEE;" readonly>
			</td>
		</tr>
	</table>
	<input type="hidden" value="<% = IN_PG_ATO_CTC %>">
	<input type="hidden" name="VL_PRMO_MOEN_CTC" value="<%=VL_PRMO_MOEN_CTC%>">
	<input type="hidden" name="VL_SGRA_MOEN_CTC" value="<%=VL_PREM_MOEN_CTC%>">
	<input type="hidden" name="VL_PCL_MOEN_CTC" value="<%=VL_PCL_MOEN_CTC%>">
	
	<script language="JavaScript">
	calculaValores();
	</script>