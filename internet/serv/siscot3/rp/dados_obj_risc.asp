<!--#include virtual = "/funcao/Verifica_Data.asp"-->
<%
redim Atp_construcao_id(1,-1)
redim Acod_rubrica(1,-1)
redim Amunicipio_id(1,-1)
redim Atp_regiao_id(1,-1)
redim Asexo(1,-1)
redim Aestado_civil(1,-1)
redim Atp_capital(1,-1)

objcon.pRamo_id = left(CD_ITEM_MDLD,2)
set rs = objcon.mLerRamo()
objcon.pGrupo_ramo_id = rs("grupo_ramo_id")
set rs = objcon.mLerGrupoRamo()
if not rs.eof then
	grupo = rs("grupo")
end if

set rs = objcon.mLerTpConstrucao()
if not rs is nothing then
	do while not rs.eof
		colocaValor Atp_construcao_id,rs("tp_construcao_id"),rs("nome")
		rs.movenext
	loop
end if

if not rs is nothing then
set rs = objcon.mLerRubrica()
	do while not rs.eof
		colocaValor Acod_rubrica,rs("cod_rubrica"),rs("descricao")
		rs.movenext
	loop
end if

if not rs is nothing then
set rs = objcon.mLerMunicipio()
	do while not rs.eof
		colocaValor Amunicipio_id,rs("municipio_id"),rs("nome")
		rs.movenext
	loop
end if

if not rs is nothing then
set rs = objcon.mLerTpRegiao()
	do while not rs.eof
		colocaValor Atp_regiao_id,rs("tp_regiao_id"),rs("nome")
		rs.movenext
	loop
end if

colocaValor Asexo,"M","Masculino"
colocaValor Asexo,"F","Feminino"

colocaValor Aestado_civil,"S","Solteiro"
colocaValor Aestado_civil,"C","Casado"
colocaValor Aestado_civil,"V","Vi�vo"
colocaValor Aestado_civil,"E","Separado"
colocaValor Aestado_civil,"D","Divorciado"
colocaValor Aestado_civil,"N","N�o Informado"

colocaValor Atp_capital,"F","Fixo"
colocaValor Atp_capital,"M","M�ltiplo Salarial"
colocaValor Atp_capital,"I","Por Idade"

sub colocaValor(variavel, valor, texto)
	redim preserve variavel(1,ubound(variavel,2)+1)
	variavel(0,ubound(variavel,2)) = valor
	variavel(1,ubound(variavel,2)) = texto
end sub

function descobreValor(variavel,valor)
	for p = 0 to ubound(variavel,2)
		if trim(variavel(0,p)) = trim(valor) then
			descobreValor = variavel(1,p)
			exit function
		end if
	next
end function

function valorResultante(campo, contador, rsValor, tipo)
	if not rsValor.eof and trim(request(tipo & "executado" & contador)) <> "1" then
		if campo = "PLOC" or campo="VLOC" then
			campo = "LOC"
		end if
		valorResultante = rsValor(campo)
	else
		valorResultante = request(campo & contador)
		if campo = "sexo" then
			valorResultante = descobreValor(Asexo,valorResultante)
		elseif  campo = "estado_civil" then
			valorResultante = descobreValor(Aestado_civil,valorResultante)
		elseif  campo = "tp_construcao_id" then
			valorResultante = descobreValor(Atp_construcao_id,valorResultante)
		elseif  campo = "cod_rubrica" then
			valorResultante = descobreValor(Acod_rubrica,valorResultante)
		elseif  campo = "municipio_id" then
			valorResultante = descobreValor(Amunicipio_id,valorResultante)
		elseif  campo = "tp_regiao_id" then
			valorResultante = descobreValor(Atp_regiao_id,valorResultante)
		elseif  campo = "tp_capital" then
			valorResultante = descobreValor(Atp_capital,valorResultante)
		elseif  campo = "dt_nascimento" then
			valorResultante = request("dia" & contador) & "/" & request("mes" & contador) & "/" & request("ano" & contador)
			if valorResultante = "//" then valorResultante = ""
		end if
	end if
end function
%>
<script>
isNN = false;
	//http://ab.internet.alwaysweb/internet/serv/siscot3/cot/liberar_cotacao_risc.asp
	var taxaNet = "";
	function preenheCampos(campo,tipo){
		document.formProc['numero' + tipo].value = campo;
		if(tipo=='P'){
			document.getElementById("tablePatrimonial").style.display= "";
			document.getElementById("trPatrimonial").style.display= "";
			selecionaOption(document.formProc.tp_construcao_id,document.formProc['tp_construcao_id' + campo].value);
			document.formProc.texto_atividade.value = document.formProc['texto_atividade' + campo].value;
			selecionaOption(document.formProc.cod_rubrica,document.formProc['cod_rubrica' + campo].value);
			document.formProc.endereco.value = document.formProc['endereco' + campo].value;
			document.formProc.bairro.value = document.formProc['bairro' + campo].value;
			document.formProc.cep.value = document.formProc['cep' + campo].value;
			selecionaOption(document.formProc.municipio_id,document.formProc['municipio_id' + campo].value);
			selecionaOption(document.formProc.tp_regiao_id,document.formProc['tp_regiao_id' + campo].value);
			document.formProc.PLOC.value = document.formProc['PLOC' + campo].value;
		}else if(tipo=='V'){
			document.getElementById("tableVida").style.display= "";
			document.getElementById("trVida").style.display= "";
			selecionaOption(document.formProc.tp_capital,document.formProc['tp_capital' + campo].value);
			document.formProc.dia.value = document.formProc['dia' + campo].value;
			document.formProc.mes.value = document.formProc['mes' + campo].value;
			document.formProc.ano.value = document.formProc['ano' + campo].value;
			selecionaOption(document.formProc.sexo,document.formProc['sexo' + campo].value);
			selecionaOption(document.formProc.estado_civil,document.formProc['estado_civil' + campo].value);
		}
	}
	function selecionaOption(obj,valor){
		for(f=0;f<obj.length;f++){
			if(obj[f].value == valor){
				obj[f].selected = true;
			} else {
				obj[f].selected = false;
			}
		}
	}
	function colocaOption(obj,valor){
		option = new Option('value','text');
		option.value = valor;
		option.text = valor;
		obj[obj.length] = option;
	}
	function apagaOptions(obj){
		while(obj.length>0){
			obj[0] = null;
		}
	}
	function cancelar(tipo){
		limparForm(tipo);
		document.formProc[tipo][document.formProc['numero' + tipo].value].selected = false;
		document.formProc['numero' + tipo].value = "";
	}
	function limparForm(tipo){
		if(tipo=='P'){
			document.getElementById("tablePatrimonial").style.display= "none";
			document.getElementById("trPatrimonial").style.display= "none";
			document.formProc.tp_construcao_id.selectedIndex = 0;
			document.formProc.texto_atividade.value = "";
			document.formProc.cod_rubrica.selectedIndex = 0;
			document.formProc.endereco.value = "";
			document.formProc.bairro.value = "";
			document.formProc.cep.value = "";
			document.formProc.municipio_id.selectedIndex = 0;
			document.formProc.tp_regiao_id.selectedIndex = 0;
			document.formProc.PLOC.value = "";
		}else if(tipo=='V'){
			document.getElementById("tableVida").style.display= "none";
			document.getElementById("trVida").style.display= "none";
			document.formProc.tp_capital.selectedIndex = 0;
			document.formProc.dia.value = "";
			document.formProc.mes.value = "";
			document.formProc.ano.value = "";
			document.formProc.sexo.selectedIndex = 0;
			document.formProc.estado_civil.selectedIndex = 0;
		}
	}
	function aplicar(tipo){
		numero = document.formProc['numero' + tipo].value;
		if(numero!=""){
			if(tipo=="P"){
				if(document.formProc.endereco.value==""){
					alert("O campo 'Endere�o' � �brigat�rio!");
					return false;
				}
				if(document.formProc.bairro.value==""){
					alert("O campo 'Bairro' � �brigat�rio!");
					return false;
				}
				if(document.formProc.cep.value==""){
					alert("O campo 'CEP' � �brigat�rio!");
					return false;
				}
				document.formProc['tp_construcao_id' + numero].value = document.formProc.tp_construcao_id.value;
				document.formProc['texto_atividade' + numero].value = document.formProc.texto_atividade.value;
				document.formProc['cod_rubrica' + numero].value = document.formProc.cod_rubrica.value;
				document.formProc['endereco' + numero].value = document.formProc.endereco.value;
				document.formProc['bairro' + numero].value = document.formProc.bairro.value;
				document.formProc['cep' + numero].value = document.formProc.cep.value;
				document.formProc['municipio_id' + numero].value = document.formProc.municipio_id.value;
				document.formProc['tp_regiao_id' + numero].value = document.formProc.tp_regiao_id.value;
				document.formProc['PLOC' + numero].value = document.formProc.PLOC.value;
				
				alteraTexto('Ptp_construcao_id' + numero,document.formProc.tp_construcao_id[document.formProc.tp_construcao_id.selectedIndex].text);
				alteraTexto('Ptexto_atividade' + numero,document.formProc.texto_atividade.value);
				alteraTexto('Pcod_rubrica' + numero,document.formProc.cod_rubrica[document.formProc.cod_rubrica.selectedIndex].text);
				alteraTexto('Pendereco' + numero,document.formProc.endereco.value);
				alteraTexto('Pbairro' + numero,document.formProc.bairro.value);
				alteraTexto('Pcep' + numero,document.formProc.cep.value);
				alteraTexto('Pmunicipio_id' + numero,document.formProc.municipio_id[document.formProc.municipio_id.selectedIndex].text);
				alteraTexto('Ptp_regiao_id' + numero,document.formProc.tp_regiao_id[document.formProc.tp_regiao_id.selectedIndex].text);
				alteraTexto('PPLOC' + numero,document.formProc.PLOC.value);
			} else if(tipo=="V"){
				if((document.formProc.dia.value=="")||(document.formProc.mes.value=="")||(document.formProc.ano.value=="")){
					alert("O campo 'Data de Nascimento' � �brigat�rio!");
					return false;
				}
				if(!Verifica_Data(document.formProc.dia.value, document.formProc.mes.value,document.formProc.ano.value)){
					alert("O campo 'Data de Nascimento' cont�m uma data inv�lida!");
					return false;
				}
				document.formProc['tp_capital' + numero].value = document.formProc.tp_capital.value;
				document.formProc['dia' + numero].value = document.formProc.dia.value;
				document.formProc['mes' + numero].value = document.formProc.mes.value;
				document.formProc['ano' + numero].value = document.formProc.ano.value;
				document.formProc['sexo' + numero].value = document.formProc.sexo.value;
				document.formProc['estado_civil' + numero].value = document.formProc.estado_civil.value;
				
				alteraTexto('Vtp_capital' + numero, document.formProc.tp_capital[document.formProc.tp_capital.selectedIndex].text);
				data = document.formProc.dia.value + "/" + document.formProc.mes.value + "/" + document.formProc.ano.value;
				alteraTexto('Vdt_nascimento' + numero, data);
				alteraTexto('Vsexo' + numero, document.formProc.sexo[document.formProc.sexo.selectedIndex].text);
				
			}
		}
		limparForm(tipo);
	}
	function alteraTexto(campo,valor){
		CAMPO = document.getElementById(campo);
		CAMPO.innerText = valor;
	}
	function atualizarGride(){
		document.formProc.action = 'liberar_cotacao_risc.asp';
		document.formProc.submit();
	}
	valorAntigo = "";

	function objrisc()
	{
		janelaobjrisc = window.open("/internet/serv/siscot3/exibicao_obj_risco.asp<%=str%>","ObjetoSegurado","resizable=yes,height=600,width=586,toolbar=no,menubar=no,status=no,location=no,left=400,top=5,scrollbars=yes");
	}
</script>
<input type="hidden" name="numeroP" value="">
<%
if grupo="P" then
	objcon.pTP_OBJETO = "P"
	set rs = objcon.mLerQtdObjetoRisco()
	if rs.eof then
		patrimonial = 0
	else
		patrimonial = rs(0)
	end if
%>
<input type="hidden" name="patrimonial" value="<% = patrimonial %>">
<input type="hidden" name="grupo" value="<% = grupo %>">
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td height="4"></td>
	</tr>
</table>
<table cellpadding="3" cellspacing="0" border="1" class="escolha" width="100%" bordercolor="#EEEEEE">
	<tr>
		<td class="td_label">&nbsp;&nbsp;<b>Patrimonial</b></td>
	</tr>
	<tr id="trPatrimonial" style="display:none">
		<td class="td_dado">
			<input type="button" name="btnPatrimonial" value="Alterar" style="width:80px;font-size:10px;margin: 2px 2px 2px 2px;" onClick="aplicar('P');" ID="Button3">
			<input type="button" name="btnPatrimonialCancelar" value="Cancelar" style="width:80px;font-size:10px;margin: 2px 2px 2px 2px;" onClick="cancelar('P');" ID="Button4">
		</td>
	</tr>
	<tr>
		<td>
			<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
				<tr>
					<td class="td_label" width="20"></td>
					<td class="td_label"><b>Tipo de Contru��o</b></td>
					<td class="td_label"><b>Atividade</b></td>
					<td class="td_label"><b>Rubrica</b></td>
					<td class="td_label"><b>Endere�o</b></td>
					<td class="td_label"><b>Bairro</b></td>
					<td class="td_label"><b>CEP</b></td>
					<td class="td_label"><b>Munic�pio</b></td>
					<td class="td_label"><b>Regi�o</b></td>
					<td class="td_label"><b>LOC</b></td>
				</tr>
				<%
				set rs = objcon.mLerDadosObjetoRisco("P")
				for f = 1 to patrimonial
				%>
				<tr>
					<td class="td_dado" width="20">
						<input type="radio" name="P" onClick="preenheCampos(<%= f %>,'P')" style="cursor:hand;">
						<input type="hidden" value="<% = request("tp_construcao_id" & f) %>" name="tp_construcao_id<%= f %>">
						<input type="hidden" value="<% = request("texto_atividade" & f) %>" name="texto_atividade<%= f %>">
						<input type="hidden" value="<% = request("cod_rubrica" & f) %>" name="cod_rubrica<%= f %>">
						<input type="hidden" value="<% = request("endereco" & f) %>" name="endereco<%= f %>">
						<input type="hidden" value="<% = request("bairro" & f) %>" name="bairro<%= f %>">
						<input type="hidden" value="<% = request("cep" & f) %>" name="cep<%= f %>">
						<input type="hidden" value="<% = request("municipio_id" & f) %>" name="municipio_id<%= f %>">
						<input type="hidden" value="<% = request("tp_regiao_id" & f) %>" name="tp_regiao_id<%= f %>">
						<input type="hidden" value="<% = request("PLOC" & f) %>" name="PLOC<%= f %>">
						<input type="hidden" name="Pexecutado<%= f %>" value="1">
					</td>
					<td class="td_dado" id="Ptp_construcao_id<%= f %>">
						<% = valorResultante("tp_construcao_id",f,rs,"P") %>&nbsp;
					</td>
					<td class="td_dado" id="Ptexto_atividade<%= f %>">
						<% = valorResultante("texto_atividade",f,rs,"P") %>&nbsp;
					</td>
					<td class="td_dado" id="Pcod_rubrica<%= f %>">
						<% = valorResultante("cod_rubrica",f,rs,"P") %>&nbsp;
					</td>
					<td class="td_dado" id="Pendereco<%= f %>">
						<% = valorResultante("endereco",f,rs,"P") %>&nbsp;
					</td>
					<td class="td_dado" id="Pbairro<%= f %>">
						<% = valorResultante("bairro",f,rs,"P") %>&nbsp;
					</td>
					<td class="td_dado" id="Pcep<%= f %>">
						<% = valorResultante("cep",f,rs,"P") %>&nbsp;
					</td>
					<td class="td_dado" id="Pmunicipio_id<%= f %>">
						<% = valorResultante("municipio_id",f,rs,"P") %>&nbsp;
					</td>
					<td class="td_dado" id="Ptp_regiao_id<%= f %>">
						<% = valorResultante("tp_regiao_id",f,rs,"P") %>&nbsp;
					</td>
					<td class="td_dado" id="PPLOC<%= f %>">
						<% = valorResultante("PLOC",f,rs,"P") %>&nbsp;
					</td>
				</tr>
				<%
				if not rs.eof then
					rs.movenext
				end if
				next %>
			</table>
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td height="4"></td>
				</tr>
			</table>
			<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE" id="tablePatrimonial" style="display:none">
				<tr>
					<td class="td_label" width="150"><b>Tipo de Contru��o</b></td>
					<td class="td_dado">
						<select name="tp_construcao_id">
<%
					for g =0 to ubound(Atp_construcao_id,2)
%>
							<option value="<% = Atp_construcao_id(0,g) %>"><% = Atp_construcao_id(1,g) %></option>
<%
					next
%>
						</select>
					</td>
				</tr>
				<tr>
					<td class="td_label" width="150"><b>Atividade</b></td>
					<td class="td_dado">
						<input type="text" size="50" maxlength="150" name="texto_atividade">
					</td>
				</tr>
				<tr>
					<td class="td_label" width="150"><b>Rubrica</b></td>
					<td class="td_dado">
						<select name="cod_rubrica">
<%
					for g =0 to ubound(Acod_rubrica,2)
%>
							<option value="<% = Acod_rubrica(0,g) %>"><% = Acod_rubrica(1,g) %></option>
<%
					next
%>
						</select>
					</td>
				</tr>
				<tr>
					<td class="td_label" width="150"><b>Endere�o</b></td>
					<td class="td_dado"><input type="text" name="endereco" size="50" maxlength="60"></td>
				</tr>
				<tr>
					<td class="td_label" width="150"><b>Bairro</b></td>
					<td class="td_dado"><input type="text" name="bairro" size="50" maxlength="60"></td>
				</tr>
				<tr>
					<td class="td_label" width="150"><b>CEP</b></td>
					<td class="td_dado"><input type="text" name="cep" size="8" maxlength="8" onKeyUp="FormatValorContinuo(this,0);"></td>
				</tr>
				<tr>
					<td class="td_label" width="150"><b>Munic�pio</b></td>
					<td class="td_dado">
						<select name="municipio_id">
<%
					for g =0 to ubound(Amunicipio_id,2)
%>
							<option value="<% = Amunicipio_id(0,g) %>"><% = Amunicipio_id(1,g) %></option>
<%
					next
%>
						</select>
					</td>
				</tr>
				<tr>
					<td class="td_label" width="150"><b>Regi�o</b></td>
					<td class="td_dado">
						<select name="tp_regiao_id">
<%
					for g =0 to ubound(Atp_regiao_id,2)
%>
							<option value="<% = Atp_regiao_id(0,g) %>"><% = Atp_regiao_id(1,g) %></option>
<%
					next
%>
						</select>
					</td>
				</tr>
				<tr>
					<td class="td_label" width="150"><b>LOC</b></td>
					<td class="td_dado">
						<input type="text" name="PLOC" size="6" maxlength="6" onKeyUp="FormatValorContinuo(this,0);">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%
end if

if grupo="V" then
	set rs = objcon.mLerDadosObjetoRisco("V")
%>
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td height="8"></td>
	</tr>
</table>
<table cellpadding="3" cellspacing="0" border="1" class="escolha" width="100%" bordercolor="#EEEEEE">
	<tr>
		<td class="td_label">&nbsp;&nbsp;<b>Vida</b></td>
	</tr>
	<tr>
		<td>
			<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE" id="tableVida">
				<tr>
					<td class="td_label" width="150"><b>Tipo de Capital</b></td>
					<td class="td_dado">
						<select name="tp_capital">
					<%for g =0 to ubound(Atp_capital,2)
						if not rs.eof then%>
							<option value="<% = Atp_capital(0,g) %>" <% if verificaSelected(Atp_capital(0,g),request("tp_capital"),trim(rs("tp_capital"))) then %> selected <% end if %>><% = Atp_capital(1,g) %></option>
						<%else%>
							<option value="<% = Atp_capital(0,g) %>" <% if verificaSelected(Atp_capital(0,g),request("tp_capital"),"") then %> selected <% end if %>><% = Atp_capital(1,g) %></option>
						<%end if%>
					<%next%>
						</select>
					</td>
				</tr>
				<tr>
					<td class="td_label" width="150"><b>Data de Nascimento</b></td>
					<td class="td_dado">
					<%if not rs.eof then%>
						<input type="text" name="dia" size="2" value="<% = verificaData(request("dia"),trim(rs("dt_nascimento")),"d") %>" maxlength="2" onKeyUp="FormatValorContinuo(this,0);return autoTab(this, 2, event);"> /
						<input type="text" name="mes" size="2" value="<% = verificaData(request("mes"),trim(rs("dt_nascimento")),"m") %>" maxlength="2" onKeyUp="FormatValorContinuo(this,0);return autoTab(this, 2, event);"> /
						<input type="text" name="ano" size="4" value="<% = verificaData(request("ano"),trim(rs("dt_nascimento")),"y") %>" maxlength="4" onKeyUp="FormatValorContinuo(this,0);">
					<%else%>
						<input type="text" name="dia" size="2" value="<% = verificaData(request("dia"),"","d") %>" maxlength="2" onKeyUp="FormatValorContinuo(this,0);return autoTab(this, 2, event);"> /
						<input type="text" name="mes" size="2" value="<% = verificaData(request("mes"),"","m") %>" maxlength="2" onKeyUp="FormatValorContinuo(this,0);return autoTab(this, 2, event);"> /
						<input type="text" name="ano" size="4" value="<% = verificaData(request("ano"),"","y") %>" maxlength="4" onKeyUp="FormatValorContinuo(this,0);">
					<%end if%>
					</td>
				</tr>
				<tr>
					<td class="td_label" width="150"><b>Sexo</b></td>
					<td class="td_dado">
						<select name="sexo">
					<%for g =0 to ubound(Asexo,2)
						if not rs.eof then%>
							<option value="<% = Asexo(0,g) %>"<% if verificaSelected(Asexo(0,g),request("sexo"),trim(rs("sexo"))) then %> selected <% end if %>><% = Asexo(1,g) %></option>
						<%else%>
							<option value="<% = Asexo(0,g) %>"<% if verificaSelected(Asexo(0,g),request("sexo"),"") then %> selected <% end if %>><% = Asexo(1,g) %></option>
						<%end if
					next%>
						</select>
					</td>
				</tr>
				<tr>
					<td class="td_label" width="150"><b>Estado Civil</b></td>
					<td class="td_dado">
						<select name="estado_civil">
					<%for g =0 to ubound(Aestado_civil,2)
						if not rs.eof then%>
							<option value="<% = Aestado_civil(0,g) %>"<% if verificaSelected(Aestado_civil(0,g),request("estado_civil"),trim(rs("estado_civil")))  then %> selected <% end if %>><% = Aestado_civil(1,g) %></option>
						<%else%>
							<option value="<% = Aestado_civil(0,g) %>"<% if verificaSelected(Aestado_civil(0,g),request("estado_civil"),"")  then %> selected <% end if %>><% = Aestado_civil(1,g) %></option>
						<%end if
					next%>
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<% end if

function verificaSelected(valor, request, recordset)
	if trim(request) <> "" then
		if valor = request then
			verificaSelected = true
		else
			verificaSelected = false
		end if
	else
		if trim(recordset) <> "" then
			if valor = recordset then
				verificaSelected = true
			else
				verificaSelected = false
			end if
		else
			verificaSelected = false
		end if
	end if
end function

function verificaData(request, recordset, campo)
	if trim(request) <> "" then
		verificaData = request
	else
		if trim(recordset) <> "" then
			if campo = "d" then
				verificaData = day(recordset)
			elseif campo = "m" then
				verificaData = month(recordset)
			elseif campo = "y" then
				verificaData = year(recordset)
			end if
		else
			verificaData = ""
		end if
	end if
end function
%>
<br>
<input type="hidden" name="numero">
<br>
<table border="0" cellpadding="0" cellspacing="1">
<tr><td><b><a class="sub_titulo" href="#" onclick="objrisc()" style="cursor:hand"><font style="text-decoration:none;color:navy;font-size:12px">:: Objeto Segurado</font></a></b></td></tr>
</table>
