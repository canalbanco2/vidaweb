<%
chave
If opcaoPorContrato = "T" Then
    Set rsDados = objCon.mLerDadosFinanceirosContrato(2)
Else
    Set rsDados = objCon.mLerDadosFinanceiros(2)
End If
rsDados.ActiveConnection = Nothing
If opcaoPorContrato = "T" Then
    Set rsSubgrupo = objCon.mLerSubgrupoCotacao(True)
Else
    Set rsSubgrupo = objCon.mLerSubgrupoCotacao(False)
End If	
rsSubgrupo.ActiveConnection = Nothing
If rsSubgrupo.EOF Then
	bSubgrupo = False
Else
	bSubgrupo = True
End If

objCon.pTP_PESQ = "BB"
vAceitaAverbacao = UCase(objCon.mLerIndAceitaAverbacao())
%>
	<tr>
		<td nowrap class="td_label">&nbsp;Moeda do seguro da cota��o</td>
		<td class="td_dado" width="800">&nbsp;<%=testaDadoVazio(rsDados,"CD_MOE_SGRO_CTC") & " - " & testaDadoVazio(rsDados,"NOME_MOE_SGRO")%></td>
	</tr>												
	<tr>
		<td nowrap class="td_label">&nbsp;Moeda de origem da cota��o</td>
		<td class="td_dado">&nbsp;<%=testaDadoVazio(rsDados,"CD_MOE_OGM_CTC")%> - <%=testaDadoVazio(rsDados,"NOME_MOE_OGM")%></td>
	</tr>												
	<tr>
		<td nowrap class="td_label">&nbsp;Percentual de corretagem</td>
		<td class="td_dado">&nbsp;<%=testaDadoVazioN(rsDados,"PC_CRE_CTC")%></td>
	</tr>				
	<tr>
		<td nowrap class="td_label">&nbsp;Percentual IOF</td>
		<td class="td_dado">&nbsp;<%=testaDadoVazioN(rsDados,"PC_IOF_CTC")%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor IOF</td>
		<td class="td_dado">&nbsp;<%=testaDadoVazioN(rsDados,"VL_IOF_CTC")%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Qtd m�xima de parcelas pgto</td>
		<td class="td_dado">&nbsp;<%=testaDadoVazio(rsDados,"QT_PCL_PGTO_CTC")%></td>
	</tr>
	<%
	percentual_corretagem = 0
	percentual_da = 0
	percentual_pro_labore = 0
	percentual_iof = 0
	valor_premio_puro = 0
	valor_premio_puro_total = 0
	valor_premio_liquido = 0
	valor_premio_liquido_total = 0
	valor_is_total = 0

	If bSubgrupo Then
		Do while NOT rsSubgrupo.EOF
			If opcaoPorContrato = "T" Then
			   objCon.pNR_CTR_SGRO = rsSubgrupo("NR_CTC_SGRO")
			   objCon.pNR_VRS_EDS = rsSubgrupo("NR_VRS_CTC")
               Set rsDados = objCon.mLerDadosFinanceirosContrato(2)
            Else
               objCon.pNR_CTC_SGRO = rsSubgrupo("NR_CTC_SGRO")
			   objCon.pNR_VRS_CTC = rsSubgrupo("NR_VRS_CTC")
               Set rsDados = objCon.mLerDadosFinanceiros(2)
            End If
		    rsDados.ActiveConnection = Nothing
			
			percentual_corretagem = cdbl("0" & testaDadoVazioN(rsDados,"PC_CRE_CTC"))
			percentual_iof = cdbl("0" & testaDadoVazioN(rsDados,"PC_IOF_CTC"))
			percentual_da = cdbl("0" & testaDadoVazioN(rsDados,"PC_DA_CTC"))
			percentual_pro_labore = cdbl("0" & testaDadoVazioN(rsDados,"PC_ETLE_CTC"))
			valor_premio_puro = cdbl("0" & testaDadoVazioN(rsDados,"VL_PREM_PURO"))
			valor_is = cdbl("0" & testaDadoVazioN(rsDados,"VL_IPTC_MOEN_CTC"))
			valor_premio_liquido = cdbl("0" & testaDadoVazioN(rsDados,"VL_LQDO_MOEN_CTC"))
			per_fat_seguro =  cdbl("0" & testaDadoVazioN(rsDados,"PER_FAT_SEGURO"))

			if valor_premio_liquido > 0 and valor_is > 0 and per_fat_seguro > 0 then
				taxa_comercial = CortaCasasDecimais(((valor_premio_liquido * per_fat_seguro) / valor_is) * 1000, 4)
			else
				taxa_comercial = 0
			end if

			if percentual_pro_labore > 0 and valor_premio_liquido > 0 then
	         	valor_pro_labore = valor_premio_puro / (1 - (percentual_da/100)) / (1 - ((percentual_pro_labore/100) + (percentual_corretagem/100))) * percentual_pro_labore /100
				valor_pro_labore = CortaCasasDecimais(valor_pro_labore,5)
			else
				valor_pro_labore = 0
			end if

			valor_premio_puro_total = valor_premio_puro_total + valor_premio_puro
			valor_premio_liquido_total = valor_premio_liquido_total + valor_premio_liquido
			valor_is_total = valor_is_total + valor_is	
%>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2" class="td_label_negrito" style="text-align:center">Subgrupo <%=right(00 & rsSubgrupo("subgrupo_id"),2) & " (" & trim(rsSubgrupo("NR_CTC_SGRO_BB")) & ")"%> - <%=trim(rsSubgrupo("NM_PRPN_CTC"))%>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Percentual pro-labore</td>
		<td class="td_dado">&nbsp;<%=FormatNumber(percentual_pro_labore, 2)%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor pro-labore</td>
		<td class="td_dado">&nbsp;<%=FormatNumber(valor_pro_labore, 2)%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor pr�mio puro</td>
		<td class="td_dado">&nbsp;<%=FormatNumber(valor_premio_puro, 2)%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Percentual DA</td>
		<td class="td_dado">&nbsp;<%=FormatNumber(percentual_da, 2)%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Tipo de Capital Segurado</td>
		<td class="td_dado">&nbsp;</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor IS</td>
		<td class="td_dado">&nbsp;<%=FormatNumber(valor_is, 2)%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Taxa comercial
           <%If vAceitaAverbacao = "S" Then
				select case cint(per_fat_seguro)
				case 1
					Response.Write "mensal"
				case 3
					Response.Write "trimestral"
				case 6
					Response.Write "semestral"
				case 12
					Response.Write "anual"
				end select
		     Else
		        Response.Write "total do per�odo"
		     End If
		%>
		</td>
		<td class="td_dado">&nbsp;<%=FormatNumber(taxa_comercial, 4)%></td>
	</tr>
<%			rsSubgrupo.MoveNext
		Loop
	Else
		percentual_corretagem = cdbl("0" & testaDadoVazioN(rsDados,"PC_CRE_CTC"))
		percentual_iof = cdbl("0" & testaDadoVazioN(rsDados,"PC_IOF_CTC"))
		percentual_da = cdbl("0" & testaDadoVazioN(rsDados,"PC_DA_CTC"))
		percentual_pro_labore = cdbl("0" & testaDadoVazioN(rsDados,"PC_ETLE_CTC"))
		valor_premio_puro = cdbl("0" & testaDadoVazioN(rsDados,"VL_PREM_PURO"))
		valor_is = cdbl("0" & testaDadoVazioN(rsDados,"VL_IPTC_MOEN_CTC"))
		valor_premio_liquido = cdbl("0" & testaDadoVazioN(rsDados,"VL_LQDO_MOEN_CTC"))
		per_fat_seguro =  cdbl("0" & testaDadoVazioN(rsDados,"PER_FAT_SEGURO"))

		if valor_premio_liquido > 0 and valor_is > 0 and per_fat_seguro > 0 then
			taxa_comercial = CortaCasasDecimais(((valor_premio_liquido * per_fat_seguro) / valor_is) * 1000, 4)
		else
			taxa_comercial = 0
		end if

		if percentual_pro_labore > 0 then
	    	valor_pro_labore = valor_premio_puro / (1 - (percentual_da/100)) / (1 - ((percentual_pro_labore/100) + (percentual_corretagem/100))) * percentual_pro_labore /100
			valor_pro_labore = CortaCasasDecimais(valor_pro_labore,5) 
		else
			valor_pro_labore = 0
		end if

		if taxa_comercial > 0 then
			if per_fat_seguro <> 1 then
				taxa_media_mensal = CortaCasasDecimais(taxa_media_mensal + ((valor_premio_liquido / valor_is) * 1000), 4)
			else
				taxa_media_mensal = taxa_media_mensal + taxa_comercial
			end if
		end if

		valor_premio_puro_total = valor_premio_puro
		valor_premio_liquido_total = valor_premio_liquido
		valor_is_total = valor_is
	%>
	<tr>
		<td nowrap class="td_label">&nbsp;Percentual pro-labore</td>
		<td class="td_dado">&nbsp;<%=formatnumber(percentual_pro_labore, 2)%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor pro-labore</td>
		<td class="td_dado">&nbsp;<%=formatnumber(valor_pro_labore, 2)%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor pr�mio puro</td>
		<td class="td_dado">&nbsp;<%=formatnumber(valor_premio_puro, 2)%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Percentual DA</td>
		<td class="td_dado">&nbsp;<%=formatnumber(percentual_da, 2)%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Tipo de Capital Segurado</td>
		<td class="td_dado">&nbsp;</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor IS</td>
		<td class="td_dado">&nbsp;<%=formatnumber(valor_is, 2)%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Taxa comercial
		<%If vAceitaAverbacao = "S" Then
				select case cint(per_fat_seguro)
				case 1
					Response.Write "mensal"
				case 3
					Response.Write "trimestral"
				case 6
					Response.Write "semestral"
				case 12
					Response.Write "anual"
				end select
		  Else
		     Response.Write "total do per�odo"
		  End If
		%>
		</td>
		<td class="td_dado">&nbsp;<%=formatnumber(taxa_comercial, 4)%></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
<%	
	End If
	
	if valor_is_total > 0 then
		taxa_media_mensal = CortaCasasDecimais(((valor_premio_liquido_total / valor_is_total) * 1000), 4)
	else
		taxa_media_mensal = 0
	end if
%>
	<tr>
		<td nowrap class="td_label">&nbsp;Taxa Personalizada por subgrupo</td>
		<td class="td_dado">&nbsp;<%=trata_sim(testaDadoVazio(rsDados,"IN_FAT_UNCO_SGR"))%></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor total de IS</td>
		<td class="td_dado">&nbsp;<%=FormatNumber(valor_is_total, 2)%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor total do pr�mio puro</td>
		<td class="td_dado">&nbsp;<%=FormatNumber(valor_premio_puro_total, 2)%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Valor total do pr�mio l�quido</td>
		<td class="td_dado">&nbsp;<%=FormatNumber(valor_premio_liquido_total, 2)%></td>
	</tr>
	<tr>
		<td nowrap class="td_label">&nbsp;Taxa m�dia mensal</td>
		<td class="td_dado">&nbsp;<%=FormatNumber(taxa_media_mensal, 4)%></td></tr><%
    Set rsDados = Nothing
%>