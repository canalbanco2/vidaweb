<!--#include virtual = "/padrao.asp"-->
<!--#include virtual = "/funcao/max_text_area.asp"-->
<!--#include virtual = "/funcao/jscript.asp"-->
<!--#include virtual = "/funcao/data_corrente.asp"-->
<!--#include virtual = "/funcao/inverter_data.asp"-->
<!--#include virtual = "/internet/serv/siscot3/request_geral.asp"-->
<!--#include virtual = "/internet/serv/siscot3/objWorkflowNegocio.asp"-->
<%
arrParametro(29) = request("tp_tarefa_id")
tp_tarefa_id = arrParametro(29)
session("parametro") = arrParametro

objCon.pTP_WF_ID = tp_wf_id
objCon.pTP_ATIV_ID = tp_ativ_id
set rsNomeAtiv = objCon.mLerNomeAtividade()
%>
<!--#include virtual = "/internet/serv/siscot3/checa_atividade.asp"-->
<html>
	<head>
		<title>Companhia de Seguros Aliança do Brasil</title>
		<LINK href="/internet/serv/siscot3/estilo_acomp.css" rel="STYLESHEET" title="style" type="text/css">
	</head>
	<script>
		function LinkArquivo(valor){
			LblArquivo = document.getElementById('trArquivo');
			LblLink = document.getElementById('trLink');
			if(valor=="A"){
				LblArquivo.style.display="";
				LblLink.style.display="none";
			} else {
				LblArquivo.style.display="none";
				LblLink.style.display="";
			}
		}
		
		function consultar()
		{
			janelahistvis = window.open("/internet/serv/siscot3/exibicao_historico_vistoria.asp?selecao=S","HistóricoVistoria","resizable=yes,height=600,width=586,toolbar=no,menubar=no,status=no,location=no,left=400,top=5,scrollbars=yes");
		}
		
		function selecionarvistoria(arquivo, data_vistoria)
		{
			LinkArquivo('L');
			
			document.formProc.tipoRelatorio[1].checked = true;
			document.formProc.txt_link.value = arquivo;
			
			document.formProc.observacao.value = 'Utilizando a vistoria da data ' + data_vistoria;
			
			document.formProc.dia_vistoria.value = data_vistoria.substring(0,2);
			document.formProc.mes_vistoria.value = data_vistoria.substring(3,5);
			document.formProc.ano_vistoria.value = data_vistoria.substring(6,10);
			
		}
    </script>
	<BODY BGCOLOR="#FFFFFF" LINK="#0000FF" VLINK="#800080" TEXT="#000000" TOPMARGIN="0" LEFTMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0">
		<!--#include virtual = "/internet/serv/siscot3/cabecalho.asp"-->
		<table border="0" class="tabelaform" cellspacing="0" cellpadding="4" align="center" style="WIDTH: 100%">
		<form name="formProc" action="dispensar_vistoria_gravar.asp" method="post" enctype="multipart/form-data">
			<tr valign="top">
				<!--#include virtual = "/internet/serv/siscot3/menu_lateral_tarefas.asp"-->
				<td>
					<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
						<tr>
							<td class="td_titulo" colspan="2"><% = RetornaDado(rsNomeAtiv,"nome")%></td>
						</tr>
						<tr>
							<td class="sub_titulo"><img src="/img/seta_ms.gif" align="absmiddle"><b>Dispensar vistoria</b></td>
							<td class="data"><% = DataCorrente()%></td>
						</tr>
					</table>
					<!--#include virtual = "/internet/serv/siscot3/dados_gerais_tarefa.asp"-->
					<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
						<tr><td style="font-size:5px">&nbsp;</td></tr>
						<tr>
							<td width="350" class="td_label">&nbsp;Histórico de Vistorias</td>
							<td class="td_dado">&nbsp;
							<%
							set rsHistorico = objCon.mLerHistoricoVistoria(1) '1 = data da mais recente
							
							if not rsHistorico.eof then
								if cint(rsHistorico("total")) > 0 then
								%>
								<b><a class="sub_titulo" href="javascript:consultar();" style="cursor:hand"><font style="text-decoration:none;color:navy;font-size:12px">
								Consultar (<%=testaDadoVazioData(rsHistorico,"dt_vistoria")%>)
								</font></a></b>
								<%
								else
									Response.Write "---"
								end if
							else
								Response.Write "---"
							end if
							%>
							<input type="hidden" name="dt_vistoria" value="<% = DataCorrente()%>">
							</td>
						</tr>
						<tr>
							<td width="350" class="td_label">&nbsp;Data</td>
							<td class="td_dado">&nbsp;<% = DataCorrente()%></td>
						</tr>
						<tr>
							<td width="350" class="td_label">&nbsp;Data da Vistoria</td>
							<td class="td_dado">&nbsp;
								<input type="text" name="dia_vistoria" size="2" maxlength="2" style="background-color:#EEEEEE;" readonly> /
								<input type="text" name="mes_vistoria" size="2" maxlength="2" style="background-color:#EEEEEE;" readonly> /
								<input type="text" name="ano_vistoria" size="4" maxlength="4" style="background-color:#EEEEEE;" readonly>
							</td>
						</tr>
						<tr>
							<td class="td_label" valign="top">&nbsp;Relatório</td>
							<td class="td_dado">&nbsp;<input type="radio" name="tipoRelatorio" value="A" onclick="LinkArquivo('A');">Arquivo
								&nbsp;
								<input type="radio" name="tipoRelatorio" value="L" onclick="LinkArquivo('L');" checked>Link
							</td>
						</tr>
						<tr id="trArquivo">
							<td class="td_label" valign="top">&nbsp;</td>
							<td class="td_dado">&nbsp;<INPUT type="file" name="arquivo" size="75" maxlenght="10"></td>
						</tr>
						<tr id="trLink">
							<td class="td_label" valign="top">&nbsp;</td>
							<td class="td_dado">&nbsp;<textarea name="txt_link" cols="68" rows="4" onkeypress="conta_texto_GC(this.value,200,'txt_link')">http://www.spmseguros.com.br/servlet/inspprev?cmd=inspecaoDetalhe&propostaBB=<%=wf_id%></textarea>
							</td>
						</tr>
						<tr>
							<td class="td_label" colspan="2">&nbsp;Observação</td>
						</tr>
						<tr>
							<td class="td_dado" colspan="2">&nbsp;<textarea name="observacao" cols="100" rows="5" onkeypress="conta_texto_GC(this.value,255,'observacao')"></textarea></td>
						</tr>
					</table>
				</td>
			</tr>
		</form>
		<script>
		LinkArquivo('L');
		</script>
		</table>
	</body>
</html>
