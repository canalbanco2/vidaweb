<!--#include virtual = "/funcao/formata_cgc.asp"-->
<!--#include virtual = "/funcao/formata_cpf.asp"-->
<!--#include virtual = "/funcao/formata_cep.asp"-->
<!--#include virtual = "/funcao/extrai_ascii.asp"-->
<!--#include virtual = "/funcao/abre_janela.asp"-->
<%session.LCID = 11274%>
<script>
var janelaExiste = true;
function fecharJanelaAtual(){
	window.close();
}
function autoTab(input,len, e) {
	var keyCode = (isNN) ? e.which : e.keyCode; 
	var filter = (isNN) ? [0,8,9] : [0,8,9,16,17,18,37,38,39,40,46];
	if(input.value.length >= len && !containsElement(filter,keyCode)) {
		input.value = input.value.slice(0, len);
		input.form[(getIndex(input)+1) % input.form.length].focus();
	}
	function containsElement(arr, ele) {
		var found = false, index = 0;
		while(!found && index < arr.length)
			if(arr[index] == ele)
				found = true;
			else
				index++;
		return found;
	}
	function getIndex(input) {
		var index = -1, i = 0, found = false;
		while (i < input.form.length && index == -1)
			if (input.form[i] == input)index = i;
				else i++;
		return index;
	}
	return true;
}
function redirecionaPagina(pagina1,pagina2){
	document.formProc.action = "redistribuicao.asp?pagina_a_ser_enviada=" + pagina2 + "&pagina_de_onde_vim=" + pagina1;
	document.formProc.submit();
}

function FormatValorContinuo(obj,tamdec){
	if (window.event.keyCode == 9) return; //tab

	if(tamdec==0){
		vrPonto = '';
	} else {
		vrPonto = '.';
	}
	vr = obj.value;
	vp = '';
	for(f=0;f<vr.length;f++){
		if((vr.charCodeAt(f)>=48)&&(vr.charCodeAt(f)<=57)){
			vp=vp+vr.charAt(f);
		}
	}
	vr = vp;
	if(vr.length>1){
		if(vr.length<=tamdec){
			vr = vr.substring(0,1) + "," + vr.substring(1,vr.length);
		} else {
			vrIniTemp = '';
			a=1;
			vrIni = vr.substring(0,vr.length-tamdec);
			vrFim = vr.substring(vr.length-tamdec,vr.length);
			for(f=0;f<vrIni.length;f++){
				vrIniTemp = vrIni.charAt(vrIni.length-f-1) + vrIniTemp;
				if((a==3)&&(vrIni.length!=f+1)){
					vrIniTemp = vrPonto + vrIniTemp;
					a=0;
				}
				a++;
			}
			if(tamdec!=0){
				vr = vrIniTemp + ',' + vrFim;
			} else {
				vr = vrIniTemp;			
			}
		}
	}
	obj.value = vr;
}

function FormatNumber(vr,tamdec,truncate){
	//A fun��o espera receber casas decimais com V�RGULA
	// truncate = 1 - n�o altera o valor de casas decimais, caso seja maior do que o tamanho decidido
	// truncate = 2 - remove os valores superiores as casa decimais
	// truncate = 3 - remove os valores superiores as casa decimais e arredonda caso haja necessidade
	vp = '';
	for(f=0;f<vr.length;f++){
		if(((vr.charCodeAt(f)>=48)&&(vr.charCodeAt(f)<=57))||(vr.charCodeAt(f)==44)){
			vp=vp+vr.charAt(f);
		}
	}
	a=1;
	vr = vp;
	if(vr.length>0){
		vr = vr.split(',');
		vrIni = vr[0];
		if(vrIni == 0)
			vrIni = '0';
		vrIniTemp = '';
		for(f=0;f<vrIni.length;f++){
			vrIniTemp = vrIni.charAt(vrIni.length-f-1) + vrIniTemp;
			if((a==3)&&(vrIni.length!=f+1)){
				vrIniTemp = '.' + vrIniTemp;
				a=0;
			}
			a++;
		}
		vrIni = vrIniTemp;
		vrFim = "";
		if(vr.length>1){
			vrFim = vr[1];
			if(vrFim.length>tamdec){
				if(truncate==3){
					valorFinal = vrFim.substring(tamdec,tamdec + 1);
					vrFim = vrFim.substring(0,tamdec);
					if(valorFinal>4){
						vrFim = vrFim/1 + 1;
					}
				} else if(truncate==2){
					vrFim = vrFim.substring(0,tamdec);
				}
			} else {
				for(f=vrFim.length;f<tamdec;f++){
					vrFim = vrFim + '0';
				}
			}
		} else {
			for(f=0;f<tamdec;f++){
				vrFim = vrFim + '0';
			}
		}
		vr = vrIni + ',' + vrFim;
		return vr;
	}
}

function RTrim(valor)
{
	for(i=0; i < valor.length; i++)
	{
		if (valor.charCodeAt(i) != 32) //32 = espa�o
			return valor.substring(i,(valor.length));
	}
	return '';
}

function LTrim(valor)
{
	for(i = (valor.length - 1); i >= 0; i--)
	{
		if (valor.charCodeAt(i) != 32) //32 = espa�o
			return valor.substring(0, (i + 1));
	}
	return '';
}

function Trim(valor)
{
	return RTrim(LTrim(valor));
}

</script>

<%
function getDuracao_ativ(tempo1,tempo2,ativ_estado)
	dim strRet
	if ativ_estado = "1" or ativ_estado = "4" then
		strRet = getTempo(now,tempo1)
	else
		strRet = getTempo(tempo1,tempo2)
	end if
	getDuracao_ativ = strRet 
end function

function getTempo(tempo1, tempo2)
  dim strRet
  if not (IsNull(tempo1) or IsNull(tempo2) or tempo1 = "" or tempo2 = "") then
    dim dias, horas, minutos, resto
    minutos = datediff("n", tempo2, tempo1)
  
    if minutos = 1440 then
      strRet = "1 dia"
    elseif minutos > 1440 then 
      aux = minutos / 1440
      pos = instr(1, aux, ",")
      if pos <> 0 then
        dias = mid(aux, 1, pos - 1)
      else
        dias = aux
      end if
      aux = dias * 1440
      aux1 = minutos - aux
      if aux1 >= 60 then
          aux = aux1 / 60
          pos = instr(1, aux, ",")
          if pos <> 0 then
            horas_aux = mid(aux, 1, pos - 1)
          else
            horas_aux = aux
          end if
          aux2 = horas_aux * 60
          minutos_aux = aux1 - aux2
      else
          minutos_aux = aux1
      end if
      if len(horas_aux) < 2 then
        horas_aux = "0" & horas_aux
      end if
      if len(minutos_aux) < 2 then
        minutos_aux = "0" & minutos_aux
      end if
      strRet = dias & " dia(s) " & horas_aux & ":" & minutos_aux & " (hh:mm)"
    else
 
      if minutos >= 60 then
          aux = minutos / 60
          pos = instr(1, aux, ",")
          if pos <> 0 then
            horas_aux = mid(aux, 1, pos - 1)
          else
            horas_aux = aux
          end if
          aux = horas_aux * 60
          minutos_aux = minutos - aux
      else
          minutos_aux = minutos
      
      end if
      
      if len(horas_aux) < 2 then
        horas_aux = "0" & horas_aux
      end if
      if len(minutos_aux) < 2 then
        minutos_aux = "0" & minutos_aux
      end if
      strRet = horas_aux & ":" & minutos_aux & " (hh:mm)"
    end if
  else
    strRet = "---"
  end if
  if trim(strRet) = "" then
    strRet = "---"
  end if
  
  getTempo = strRet
end function

function tratanull(valor)
if trim(valor) = "" or isnull(valor) or trim(valor) = "null" then tratanull = "---" else tratanull = trim(valor)
end function

function mNegociador(rs,strsit)
	select case trim(testadadovazioN(rs,strsit))
	case "1"
		mNegociador = "Ag�ncia"
	case "2"
		mNegociador = "Seguradora"
	case else
		mTipoOperacao = "---"
	end select
end function

function mTipoOperacao(rs,strsit)
	select case trim(testadadovazioN(rs,strsit))
	case "1"
		mTipoOperacao = "Contrata��o"
	case "2"
		mTipoOperacao = "Renova��o"
	case "3"
		mTipoOperacao = "Endosso"
	case else
		mTipoOperacao = "---"
	end select
end function

function msituacao(strsit)
	select case Ucase(trim(strsit))
	case "A"
		msituacao = "Pedido Inicial"
	case "6"
		msituacao = "Rean�lise"
	case "5"
		msituacao = "Pendente"
	case "V"
		msituacao = "Vistoria"
	case "R"
		msituacao = "Ressegurador"
	case "4"
		msituacao = "Salva"
	case "7"
		msituacao = "Exig�ncia"
	case "3"
		msituacao = "Recusada"
	case "1"
		msituacao = "Aceita"
	case "D"
		msituacao = "Deletada"
	case "C"
		msituacao = "Contratada"
	case else
		msituacao = "---"
	end select
end function

function mCanalVenda(rs,strsit)
	select case Ucase(testadadovazio(rs,strsit))
	case "2"
		mCanalVenda = "Ag�ncia"
	case "3"
		mCanalVenda = "Internet"
	case "4"
		mCanalVenda = "Call Center"
	case else
		mCanalVenda = "---"
	end select
end function

function testaDadoVazio(obj,campo)
	response.flush
	if obj is nothing then
		testaDadoVazio = "---"
	elseif obj.eof then
		testaDadoVazio = "---"
	elseif isnull(obj(campo)) or isEmpty(obj(campo)) then
		testaDadoVazio = "---"
	elseif trim(obj(campo)) = "" then
		testaDadoVazio = "---"
	else
		testaDadoVazio = obj(campo)
	end if
end function

function testaDadoVazioN(obj,campo)
	response.flush
	if obj is nothing then
		testaDadoVazioN = ""
	elseif obj.eof then
		testaDadoVazioN = ""
	elseif isnull(obj(campo)) or isEmpty(obj(campo)) then
		testaDadoVazioN = ""
	elseif trim(obj(campo)) = "" then
		testaDadoVazioN = ""
	else
		testaDadoVazioN = obj(campo)
	end if
end function

function testaDadoVazioData(obj,campo)
	response.flush
	if obj is nothing then
		testaDadoVazioData = "---"
	elseif obj.eof then
		testaDadoVazioData = "---"
	elseif isnull(obj(campo)) or isEmpty(obj(campo)) then
		testaDadoVazioData = "---"
	elseif trim(obj(campo)) = "" then
		testaDadoVazioData = "---"
	else
		dt = Split(obj(campo)," ")
		testaDadoVazioData = dt(0)
	end if
end function

function testaDadoVazioFormata(obj,campo,casas)
	response.flush
	if obj is nothing then
		testaDadoVazioFormata = "---"
	elseif obj.eof then
		testaDadoVazioFormata = "---"
	elseif isnull(obj(campo)) or isEmpty(obj(campo)) then
		testaDadoVazioFormata = "---"
	elseif trim(obj(campo)) = "" then
		testaDadoVazioFormata = "---"
	else
		dt = FormatNumber(obj(campo),2)
		testaDadoVazioFormata = dt
	end if
end function

function testaDadoVazioDataN(obj,campo)
	response.flush
	if obj is nothing then
		testaDadoVazioDataN = ""
	elseif obj.eof then
		testaDadoVazioDataN = ""
	elseif isnull(obj(campo)) or isEmpty(obj(campo)) then
		testaDadoVazioDataN = ""
	elseif trim(obj(campo)) = "" then
		testaDadoVazioDataN = ""
	else
		dt = Split(obj(campo)," ")
		testaDadoVazioDataN = dt(0)
	end if
end function

function testaDadoVazioFormataN(obj,campo,casas)
	response.flush
	if obj is nothing then
		testaDadoVazioFormataN = ""
	elseif obj.eof then
		testaDadoVazioFormataN = ""
	elseif isnull(obj(campo)) or isEmpty(obj(campo)) then
		testaDadoVazioFormataN = ""
	elseif trim(obj(campo)) = "" then
		testaDadoVazioFormataN = ""
	else
		dt = FormatNumber(obj(campo),casas)
		testaDadoVazioFormataN = dt
	end if
end function

function testaDadoVazioFormataN2(obj,campo,casas)
	dt = testaDadoVazioFormataN(obj,campo,casas)
	
	if trim(dt) = "" then
		testaDadoVazioFormataN2 = "---"
	else
		testaDadoVazioFormataN2 = dt
	end if
end function

function testaDadoVazioCompleto(obj,campo)
	response.flush
	if obj is nothing then
		testaDadoVazio = "---"
	elseif obj.eof then
		testaDadoVazio = "---"
	elseif isnull(obj(campo)) or isEmpty(obj(campo)) then
		testaDadoVazio = "---"
	elseif trim(obj(campo)) = "" then
		testaDadoVazio = "---"
	else
		dt = (obj(campo))
		testaDadoVazioCompleto = dt
	end if
end function

function formataMCI(valor)
	Aux = mid(valor,1,3) & "." & mid(valor,4,3) & "." & mid(valor,7,3)
	formataMCI = Aux
end function
	
function formataCNPJ(valor)
	Aux = formata_cgc(valor)
	formataCNPJ = Aux
end function

function trata_sim(str)
Select case Trim(lcase(str))
	case "s"
		trata_sim = "Sim"
	case "n"
		trata_sim = "N�o"
	case else
		trata_sim = "---"
end select
end function

function trata_funcionario(rs,func)
str = testadadovazion(rs,func)
	select case trim(str)
	case "1"
		trata_funcionario = "F�sica"
	case "2"
		trata_funcionario = "Jur�dica"
	case "3"
		trata_funcionario = "Funcion�rio do Banco do Brasil"
	case "4"
		trata_funcionario = "Funcion�rio do Banco Central"
	case "5"
		trata_funcionario = "Funcion�rio P�blico Federal"
	case "6"
		trata_funcionario = "Funcion�rio da Alian�a do Brasil"
	case "7"
		trata_funcionario = "Mutu�rio PREVI"
	case else
		trata_funcionario = "---"
	end select
end function

function trata_cpf_cnpj(rs,cpf)
	str = testadadovazion(rs,cpf)
	if len(trim(str)) = 11 then
		trata_cpf_cnpj = formata_cpf(trim(str))
	elseif len(trim(str)) = 14 then
		trata_cpf_cnpj = formata_cgc(trim(str))
	else
		trata_cpf_cnpj = "---"
	end if
end function

function formataBanco(valor)
	if trim(valor)="" then
		formataBanco = ""
	else
		valor = replace(valor,".","")
		valor = replace(valor,",",".")
		formataBanco = valor
	end if
end function

function RetornaDado(obj,campo)
	if not obj.eof then
		RetornaDado = obj(campo)
	else
		RetornaDado = ""
	end if
end function

function CortaCasasDecimais(valor, qtddigitos)
	if trim(valor) = "" or not isnumeric(valor) or not isnumeric(qtddigitos) then
		CortaCasasDecimais = ""
		exit function
	end if
	
	iVirgula = instr(1,valor,",")
	if iVirgula > 0 then
		iCasasDecimais = len(mid(valor,iVirgula, len(valor - iVirgula + 1))) - 1

		if iCasasDecimais > qtddigitos then
			CortaCasasDecimais = left(valor, len(valor) - (iCasasDecimais - qtddigitos))
		else
			CortaCasasDecimais = valor
		end if
	else
		CortaCasasDecimais = valor
	end if
end function

function MontaDataCompleta(rsData, rsNomeCampo)
	Dim sDataCompleta
			
	sDataCompleta = right("00" & Day(rsData(rsNomeCampo)),2) & "/" & right("00" & Month(rsData(rsNomeCampo)),2) & "/" & Year(rsData(rsNomeCampo))
	sDataCompleta = sDataCompleta & " " & right("00" & Hour(rsData(rsNomeCampo)),2) & ":" & right("00" & Minute(rsData(rsNomeCampo)),2)
			
	MontaDataCompleta = sDataCompleta
end function

function RetornaPeriodicidade(iMes)
	if not isnumeric(iMes) then exit function

	Dim sPeriodicidade
	
	select case cint(iMes)
	case 1
		sPeriodicidade = "mensal"
	case 1
		sPeriodicidade = "bimestral"
	case 3
		sPeriodicidade = "trimestral"
	case 3
		sPeriodicidade = "quadrimestral"
	case 6
		sPeriodicidade = "semestral"
	case 12
		sPeriodicidade = "anual"
	end select
	
	RetornaPeriodicidade = sPeriodicidade
end function

Function NomeTipoRamo(tp_ramo_id)
	if not isnumeric(tp_ramo_id) <> "" then exit function
	
	if cint(tp_ramo_id) = 1 then
		NomeTipoRamo = "Pessoas"
	elseif cint(tp_ramo_id) = 2 then
		NomeTipoRamo = "Ramos Elementares"
	end if
end function

function RetornaTipoWorkflow(iTipoRamo, iTipoContratacao)
	if not IsNumeric(iTipoRamo) or not IsNumeric(iTipoContratacao) then exit function

	Dim iTipoWorkflow

	if cint(iTipoContratacao) = cint(Application("CONTRATACAO")) or cint(iTipoContratacao) = cint(Application("RENOVACAO")) then
		if cint(iTipoRamo) = 1 then
			iTipoWorkflow = Application("WF_VG")
		elseif cint(iTipoRamo) = 2 then
			iTipoWorkflow = Application("WF_RE")
		end if
	elseif cint(iTipoContratacao) = cint(Application("ENDOSSO")) then
		if cint(iTipoRamo) = 1 then
			iTipoWorkflow = Application("WF_VGEDS")
		elseif cint(iTipoRamo) = 2 then
			iTipoWorkflow = Application("WF_REEDS")
		end if
	elseif cint(iTipoContratacao) = cint(Application("ENDOSSO_CANCELAMENTO")) then
		if cint(iTipoRamo) = 1 then
			iTipoWorkflow = Application("WF_ENDOSSO_CANCELAMENTO")
		elseif cint(iTipoRamo) = 2 then
			iTipoWorkflow = Application("WF_ENDOSSO_CANCELAMENTO")
		end if
	end if

	RetornaTipoWorkflow = iTipoWorkflow
end function

'"Constantes" para a fun��o ValidaTipoWorkflow
WF_RE = 1
WF_VG = 2
WF_REEDS = 3
WF_VGEDS = 4
function ValidaTipoWorkflow(tp_wf_id, wf_esperado)
	dim tp_wf_id_validacao
	
	select case cstr(wf_esperado)
	case cstr(WF_RE)
		tp_wf_id_validacao = Application("WF_RE")
	case cstr(WF_VG)
		tp_wf_id_validacao = Application("WF_VG")
	case cstr(WF_REEDS)
		tp_wf_id_validacao = Application("WF_REEDS")
	case cstr(WF_VGEDS)
		tp_wf_id_validacao = Application("WF_VGEDS")
	end select
	
	if cstr(tp_wf_id) = cstr(tp_wf_id_validacao) then
		ValidaTipoWorkflow = true
	else
		ValidaTipoWorkflow = false
	end if
end function

'"Constantes" para a fun��o ValidaTipoWorkflowGeral
GERAL_WF_RE = 1
GERAL_WF_VG = 2
function ValidaTipoWorkflowGeral(tp_wf_id, wf_esperado)
	dim bValida
	bValida = false
	
	if cstr(wf_esperado) = cstr(GERAL_WF_RE) then
		if cstr(tp_wf_id) = cstr(Application("WF_RE")) or cstr(tp_wf_id) = cstr(Application("WF_REEDS")) then
			bValida = true
		end if
	elseif cstr(wf_esperado) = cstr(GERAL_WF_VG) then
		if cstr(tp_wf_id) = cstr(Application("WF_VG")) or cstr(tp_wf_id) = cstr(Application("WF_VGEDS")) then
			bValida = true
		end if
	end if
	
	ValidaTipoWorkflowGeral = bValida
end function

'"Constantes" para a fun��o ValidaAtividadeWorkflow
AT_REVISAO_PREMIO = 1
function ValidaAtividadeWorkflow(tp_wf_id, tp_ativ_id, ativ_esperada)
	dim bValida
	bValida = false
	
	if cstr(tp_wf_id) = cstr(Application("WF_RE")) or cstr(tp_wf_id) = cstr(Application("WF_REEDS")) then
		if cstr(ativ_esperada) = cstr(AT_REVISAO_PREMIO) and cstr(tp_ativ_id) = cstr(Application("AT_RE_REVISAO_PREMIO")) then
			bValida = true		
		end if
	end if
	
	ValidaAtividadeWorkflow = bValida
end function

url = split(request.servervariables("SCRIPT_NAME"),"/")
pagina = Lcase(url(ubound(url)))

set objCon = Server.CreateObject("SCTL0003.cCotacao")
set objWorkflow = server.CreateObject("WFL0003.cls00043")

objCon.pCD_PRD = CD_PRD
objCon.pCD_MDLD = CD_MDLD
objCon.pCD_ITEM_MDLD = CD_ITEM_MDLD
objCon.pNR_CTC_SGRO = NR_CTC_SGRO
objCon.pNR_VRS_CTC = NR_VRS_CTC
objCon.pUSUARIO_ID = USUARIO_ID

sub preencheChaves()
	CD_PRD = CD_PRD
	CD_MDLD = CD_MDLD
	CD_ITEM_MDLD = CD_ITEM_MDLD
	NR_CTC_SGRO = NR_CTC_SGRO
	NR_VRS_CTC = NR_VRS_CTC
	USUARIO_ID = USUARIO_ID
	USUARIO = LOGIN
	objCon.pCD_PRD = CD_PRD
	objCon.pCD_MDLD = CD_MDLD
	objCon.pCD_ITEM_MDLD = CD_ITEM_MDLD
	objCon.pNR_CTC_SGRO = NR_CTC_SGRO
	objCon.pNR_VRS_CTC = NR_VRS_CTC
	objCon.pUSUARIO_ID = USUARIO_ID
	objCon.pUSUARIO = USUARIO
end sub
	
Sub chave
	preenchechaves
	set rsWF_ID = ObjCon.mLerWfId
	objcon.pTP_FORM_ID = TP_FORM_ID
	objcon.pWF_ID = rsWF_ID("WF_ID")
	objcon.pTP_WF_ID = TP_WF_ID
	objcon.pTP_ATIV_ID = TP_ATIV_ID
	objcon.pTP_TAREFA_ID = TP_TAREFA_ID
	objcon.pITEM_DADO = "2|3|4|5|6"
	objcon.pITEM_DADO_CONTEUDO = objCon.pCD_PRD &"|"& objCon.pCD_MDLD &"|"& objCon.pCD_ITEM_MDLD &"|"& objCon.pNR_CTC_SGRO &"|"& objCon.pNR_VRS_CTC 
end sub
EfeitoOver = "OnMouseOver=""this.bgColor='#FFFF66';"" OnMouseOut=""this.bgColor='';"""

'Efeito de zebra da grid
cinza = ";background-color:#f1F1f1"
alter = 0
cor_zebra = ""
sub alterna_cor
	if alter = 1 then
	alter = 0
	cor_zebra = cinza
	else
	alter = 1
	cor_zebra = ""
	end if
end sub
%>
