<%
preencheChaves
If var_NR_CTC_ATUAL <> "" And var_NR_CTC_ATUAL <> "NOVO" Then
   NR_CTC_NOVO_array = Split(var_NR_CTC_ATUAL&"|","|")
   objCon.pNR_CTC_SGRO   = NR_CTC_NOVO_array(0)
   objCon.pNR_VRS_CTC    = NR_CTC_NOVO_array(1)
End If

objCon.pNR_QSTN          = Trim(questionario_id)
objCon.pCD_TIP_QSTN      = var_CD_TIP_QSTN
objCon.pNR_SEQL_QSTN_CTC = var_NR_SEQL_QSTN_CTC
NR_SEQL_QSTN_CTC         = var_NR_SEQL_QSTN_CTC

var_PostBack = False
If Request("gravadados") = "S" OR Request("gravardados") = "S" OR (Request("LocalRisco") = NR_SEQL_QSTN_CTC And Request("excluirDados") <> "S") Then
    var_PostBack = True
End If

Set objDataSet = objCon.mLerPergunasQuestionarioGravado()
objDataSet.ActiveConnection = Nothing
Set oArvore = new cItemTreeView
oArvore = mGeraArrayQuestionario(objDataSet)

ordem = CalcularOrdem(oArvore)
'intContador = 0
'Do While ordem(intIndice) <> ""
'	resposta = Respostas(oArvore(intContador).pergunta_id)
'	intIndice = ordem(intIndice)
'Loop

If UBound(oArvore) > 0 Then
	Response.Write(htmlExibir)
%>
	<br>
	<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
	<input type="hidden" name="<%=codigo_questionario%>LocalRisco" value="<% = var_NR_SEQL_QSTN_CTC%>">
	<input type="hidden" name="<%=codigo_questionario%>questionarioID" value="<% = Trim(questionario_id) %>">
	<%
    aCumiC = ""
    For iC = 0 to UBound(ordem)-1
        intContador = ordem(iC)
        aCumiC = aCumiC & iC & "|"
		identacao = Replace(oArvore(intContador).tabs,"|","&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")
		exibir = true
		pergunta_atual = Trim(oArvore(intContador).pergunta_id)                    'Cod.pergunta que esta sendo preenchida
		pergunta_precedente = Trim(oArvore(intContador).num_pergunta_precedente)   'Cod.pergunta precedente
		If pergunta_precedente <> "0" Then
			If var_PostBack Then
				If Trim(Request(questionario_id & "rsp" & oArvore(intContador).num_pergunta_precedente)) = "" Then
					exibir = false
				ElseIf Trim(oArvore(intContador).resposta_pergunta_precedente_id) <> Trim(Request(questionario_id & "rsp" & oArvore(intContador).num_pergunta_precedente)) Then
					exibir = false
				End If
			Else
				indice = AchaIndiceId(oArvore,pergunta_precedente)                 'Acha posi��o da pergunta precedente em oArvore
				If indice = "" Then
					  exibir = false
				Else
				   If Trim(oArvore(indice).resposta) = "" Then
					  exibir = false
				   ElseIf Trim(oArvore(indice).resposta) <> Trim(oArvore(intContador).resposta_pergunta_precedente_id) Then
					  exibir = false
				   End If
				End If
			End If
		End If%>
		<tr id="linTR_<%= iC%>" style="display:<%If NOT exibir Then Response.Write("none") End If%>">
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="escolha">
				<tr>
					<td class="td_dado">
						<% = identacao & (iC + 1) & ") " & oArvore(intContador).nome_pergunta%>
					</td>
				</tr>
				<tr>
					<td class="td_dado">
						<%								
						 size = oArvore(intContador).tamanho
						 If size > 70 Then
							size  =  70
						 End If
						 Response.Write identacao & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"

						 If oArvore(intContador).grupo_dominio_resposta_id = "null" Then
						     Select case oArvore(intContador).nome_formato
							 	    case "TEXTO" %>
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this)" type="text" size="<% = size %>" name="<% = questionario_id %>rsp<% = pergunta_atual %>" maxlength="<% = oArvore(intContador).tamanho %>" value="<% = retornaValor(questionario_id,oArvore,intContador,var_PostBack) %>">
								  <%case "NUMERO" %>
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this)" type="text" onKeyUp="FormatValorContinuo(this,0);" name="<% = questionario_id %>rsp<% = pergunta_atual %>" size="<% = size %>" maxlength="<% = oArvore(intContador).tamanho %>" value="<% = retornaValor(questionario_id,oArvore,intContador,var_PostBack) %>">
								  <%case "VALOR" %>
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this)" type="text" onKeyUp="FormatValorContinuo(this,2);" name="<% = questionario_id %>rsp<% = pergunta_atual %>" size="19" maxlength="19" value="<% = retornaValor(questionario_id,oArvore,intContador,var_PostBack) %>">
								  <%case "DATA (DD/MM/AAAA)" 
									    valorRespFmtDt = retornaValor(questionario_id,oArvore,intContador,var_PostBack) %>
										<input type="hidden" name="<% = questionario_id %>rsp<% = oArvore(intContador).pergunta_id %>" value="<%=valorRespFmtDt%>">
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this)" type="text" onKeyUp="ConcatenaData(this,this.form);FormatValorContinuo(this,0);" name="<% = questionario_id %>rsp<% = pergunta_atual %>_dia" size="2" maxlength="2" value="<% = Right(valorRespFmtDt,2) %>">
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this)" type="text" onKeyUp="ConcatenaData(this,this.form);FormatValorContinuo(this,0);" name="<% = questionario_id %>rsp<% = pergunta_atual %>_mes" size="2" maxlength="2" value="<% = Mid(valorRespFmtDt,5,2) %>">
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this)" type="text" onKeyUp="ConcatenaData(this,this.form);FormatValorContinuo(this,0);" name="<% = questionario_id %>rsp<% = pergunta_atual %>_ano" size="4" maxlength="4" value="<% = Left(valorRespFmtDt,4) %>">
								  <%case "HORA (HH:MM:SS)" 			
									    valorRespFmtDt = retornaValor(questionario_id,oArvore,intContador,var_PostBack)%>
										<input type="hidden" name="<% = questionario_id %>rsp<% = pergunta_atual %>" value="<%=valorRespFmtDt%>">
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this)" type="text" onKeyUp="FormatValorContinuo(this,0);" name="<% = questionario_id %>rsp<% = pergunta_atual %>_H" size="2" maxlength="2" value="<% = Right(valorRespFmtDt,2)%>">
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this)" type="text" onKeyUp="FormatValorContinuo(this,0);" name="<% = questionario_id %>rsp<% = pergunta_atual %>_M" size="2" maxlength="2" value="<% = Mid(valorRespFmtDt,3,2)%>">
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this)" type="text" onKeyUp="FormatValorContinuo(this,0);" name="<% = questionario_id %>rsp<% = pergunta_atual %>_S" size="2" maxlength="2" value="<% = Left(valorRespFmtDt,2)%>">
								  <%case "PERCENTUAL" %>
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this)" type="text" onKeyUp="FormatValorContinuo(this,2);" name="<% = questionario_id %>rsp<% = pergunta_atual %>" size="8" maxlength="20" value="<% = retornaValor(questionario_id,oArvore,intContador,var_PostBack) %>">
								  <%case "TIMESTAMP" %>
										<input onfocus="onfocusTeste(this)" onblur="onblurTeste(this)" type="text" name="<% = questionario_id %>rsp<% = pergunta_atual %>" maxlength="<% = oArvore(intContador).tamanho %>" size="<% = size %>" value="<% = retornaValor(questionario_id,oArvore,intContador,var_PostBack) %>">
							<%End Select
						 Else
								  If combo Then %>
										<select name="<% = questionario_id %>rsp<% = pergunta_atual %>" onChange='pressionarQuestionario(<%= questionario_id & "," & pergunta_atual %>, this.value);'>
										<option value="">--Escolha abaixo--</option>
							   <% End If
                                  respArray = RespostasDasPerguntas(pergunta_atual)        'Array com as respostas das perguntas(contem tambem a resposta ja gravada)
								  For f=1 to UBound(respArray)
									  If combo Then
									     If retornaValor(questionario_id,oArvore,intContador,var_PostBack) = Trim(respArray(f).dominio_resposta_id) Then
											 selecionado = "selected"
										 Else
											 selecionado = ""
										 End If%>
										<option <% = selecionado %> value="<% = respArray(f).dominio_resposta_id %>"><% = respArray(f).resposta_determinada %></option>
									<%Else
										 If retornaValor(questionario_id,oArvore,intContador,var_PostBack) = Trim(respArray(f).dominio_resposta_id) Then
											 selecionado = "checked"
										 Else
											 selecionado = ""
										 End If%>
										<input type="radio" <% = selecionado %> name="<% = questionario_id %>rsp<% = pergunta_atual %>" value="<% = respArray(f).dominio_resposta_id %>" onclick='pressionarQuestionario(<%=questionario_id & "," & pergunta_atual %>, this.value);'><% = respArray(f).resposta_determinada %>
									<%End If
								  Next
									  If combo Then %>
										</select>
									<%End If
					     End If%>
								<input type="hidden" name="<% = questionario_id %>obrigatorio<% = pergunta_atual %>" value="<% = oArvore(intContador).ind_obrigatoriedade %>">
								<input type="hidden" name="<% = questionario_id %>indice<% = iC %>" value="<% If exibir Then Response.Write(pergunta_atual)%>">
								<input type="hidden" name="<% = questionario_id %>pergunta<% = iC %>" value="<% = oArvore(intContador).nome_pergunta %>">
                         <%If pergunta_precedente <> "0" Then%>
 		   			   	        <input type="hidden" name="<% = questionario_id %>pgpre<% = pergunta_precedente %>rspq<%= oArvore(intContador).resposta_pergunta_precedente_id %>cd<% = pergunta_atual %>" value="<% = iC %>">
                         <%   perguntasAux = Split(oArvore(intContador).colecao_perguntas_precedentes,"|")
                              For iAuxilia = 0 To Ubound(perguntasAux)-1%>
 		   			   	        <input type="hidden" name="<% = questionario_id %>pgpre<% = perguntasAux(iAuxilia) %>rspq##cd<% = pergunta_atual %>" value="<% = iC %>">
                         <%   Next
                           End If%>
							</td>
						</tr>
					</table>
				</td>
			</tr>
<%	Next
%>
 	<input type="hidden" name="<%=codigo_questionario%>pergQuestionario" value="<% = aCumiC %>">
   	</table>
<%
	Response.Write(htmlExibir2)
End If
%>