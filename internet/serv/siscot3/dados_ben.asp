<%
set rs = objcon.mLerSubgrupoCotacao

if rs.eof then
	bSubgrupo = false
else
	bSubgrupo = true
%>
	<tr>
		<td nowrap class="td_label">&nbsp;Subgrupo&nbsp;</td>
		<td class="td_dado" width="800">&nbsp;
		<select name="NM_PRPN_CTC" onchange="document.formProc.submit();">
		<option value="">--Escolha abaixo--</option>
		<%
		do while not rs.eof
			if trim(request("NM_PRPN_CTC")) = trim(rs("NR_CTC_SGRO")) & "|" & trim(rs("NR_VRS_CTC")) then
				sSelecionado = " selected"
			else
				sSelecionado = ""
			end if
			%>
			<option value="<%=trim(rs("NR_CTC_SGRO")) & "|" & trim(rs("NR_VRS_CTC"))%>"<%=sSelecionado%>><%=right(00 & rs("subgrupo_id"),2) & " (" & trim(rs("NR_CTC_SGRO_BB")) & ")"%> - <%=trim(rs("NM_PRPN_CTC"))%></option>
			<%
			rs.movenext
		loop
		%>
		</select>
		</td>
	</tr>
<%
	if trim(request("NM_PRPN_CTC")) <> "" then 
		arrSubGrupoCotacao = split(trim(request("NM_PRPN_CTC")),"|")
								
		objCon.pNR_CTC_SGRO = arrSubGrupoCotacao(0)
		objCon.pNR_VRS_CTC = arrSubGrupoCotacao(1)
		NR_CTC_SGRO = objCon.pNR_CTC_SGRO
		NR_VRS_CTC = objCon.pNR_VRS_CTC
	end if
end if
						
if not bSubgrupo or trim(request("NM_PRPN_CTC")) <> "" then 
	set rsClausula = objcon.mLerDadosBeneficiarios()
%>
<tr>
  <td colspan="2">
	<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
		<%if not rsClausula.eof then%>
		<tr>
			<td class="td_label_negrito" nowrap>&nbsp;Nome Beneficiário</td>
			<td class="td_label_negrito" nowrap>&nbsp;CPF/CNPJ</td>
			<td class="td_label_negrito" nowrap>&nbsp;% Particip.</td>
			<td class="td_label_negrito" nowrap>&nbsp;Endereço</td>
			<td class="td_label_negrito" nowrap>&nbsp;Bairro</td>
			<td class="td_label_negrito" nowrap>&nbsp;Cidade</td>
			<td class="td_label_negrito" nowrap>&nbsp;UF</td>
			<td class="td_label_negrito" nowrap>&nbsp;CEP</td>
		</tr>
			<%do while not rsClausula.eof
				alterna_cor
				%>
			<tr>
				<td class="td_dado" style="<%=cor_zebra%>" nowrap>&nbsp;<%=tratanull(rsClausula("NM_BNFC_CTC"))%>&nbsp;</td>
				<td class="td_dado" style="text-align:right;<%=cor_zebra%>" nowrap>&nbsp;<%= trata_cpf_cnpj(rsClausula,"NR_SRF_BNFC_CTC")%>&nbsp;</td>
				<td class="td_dado" style="text-align:right;<%=cor_zebra%>" nowrap>&nbsp;<%=Trim(tratanull(rsClausula("PC_PRTC_BNFC_CTC")))%>&nbsp;</td>
				<td class="td_dado" style="<%=cor_zebra%>" nowrap>&nbsp;<%=tratanull(rsClausula("NM_LGR_BNFC_CTC"))%>&nbsp;</td>
				<td class="td_dado" style="<%=cor_zebra%>" nowrap>&nbsp;<%=tratanull(rsClausula("NM_BAI_BNFC_CTC"))%>&nbsp;</td>
				<td class="td_dado" style="<%=cor_zebra%>" nowrap>&nbsp;<%=tratanull(rsClausula("NM_CID_BNFC_CTC"))%>&nbsp;</td>
				<td class="td_dado" style="<%=cor_zebra%>" nowrap>&nbsp;<%=tratanull(rsClausula("SG_UF"))%>&nbsp;</td>
				<td class="td_dado" style="text-align:right;<%=cor_zebra%>" nowrap>&nbsp;<%=formata_cep(tratanull(rsClausula("NR_CEP_BNFC_CTC")))%>&nbsp;</td>
			</tr>
				<%rsClausula.movenext
			loop
		else%>
		<tr><td style="font-size:5px">&nbsp;</td></tr>
		<tr>
			<td class="td_label">&nbsp;Nenhum beneficiário relacionado.</td>
		</tr>
		<%end if%>
	</table>
<%end if%>
</td>
</tr>
<br>