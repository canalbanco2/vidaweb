<!--include virtual = "/padrao.asp"-->
<!--#include virtual = "/funcao/formatar_data.asp"-->
<%
Set result = objCon.mLerDadosContrato()
result.ActiveConnection = Nothing

objCon.pCD_PRD       = CD_PRD_ANTER
objCon.pCD_MDLD      = CD_MDLD_ANTER
objCon.pCD_ITEM_MDLD = CD_ITEM_MDLD_ANTER
objCon.pNR_CTC_SGRO  = NR_CTC_SGRO_ANTER
objCon.pNR_VRS_CTC   = NR_VRS_CTC_ANTER
Set resANTER = objCon.mLerCotacao()
resANTER.ActiveConnection = Nothing

If NOT result.EOF Then
vCNPJ = result("NR_SRF_PRPN_CTR")

If Trim(Request("Resseguro_Facultativo")) = "" Then
	If isnull(result("Resseguro_Facultativo")) Then
		Resseguro_Facultativo = "N"
	Else
		Resseguro_Facultativo = result("Resseguro_Facultativo")
	End If
Else
	Resseguro_Facultativo = Request("Resseguro_Facultativo")
End If
End If
%>
<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha" ID="Table1">
	<tr><td colspan="6" class="td_label"><span class="sub_titulo" style="font-size:12px"><b>:: Dados B�sicos do Contrato</b></span> </td></tr>
	<tr>
		<td class="td_label" nowrap>&nbsp;Sub Ramo</td>
		<td class="td_dado" width="800">&nbsp;<%=CD_ITEM_MDLD%> - <%= testaDadoVazio(resANTER,"nome")%></td>
	</tr>
	<tr>
		<td class="td_label" nowrap>&nbsp;Inst�ncia</td>
		<td class="td_dado">&nbsp;<%=wf_id%></td>
	</tr>
	<tr>
		<td class="td_label" nowrap>&nbsp;Contrato/Vers�o</td>
			<td class="td_dado"><b>&nbsp;<%=resANTER("nr_ctr_ctc")&"/"&resANTER("nr_vrs_eds")%></b></td>
	</tr>
    <tr>
		<td class="td_label" nowrap>&nbsp;<%= testaDadoVazio(resANTER,"tp_meio_canal")%></td>
		<td class="td_dado">&nbsp;<%= testaDadoVazio(result,"CD_UOR_CTR")%> - <%= testaDadoVazio(result,"nome_agencia")%></td>
	</tr>
	<tr>
		<td class="td_label" nowrap>&nbsp;Proponente</td>
		<td class="td_dado">&nbsp;<%= testaDadoVazio(result,"NM_PRPN_CTR")%></td>
	</tr>
	<tr>
		<td class="td_label" nowrap>&nbsp;CPF/CNPJ</td>
		<td class="td_dado">&nbsp;<%= trata_cpf_cnpj(result,"NR_SRF_PRPN_CTR")%></td>
	</tr>
	<tr>
		<td class="td_label" nowrap>&nbsp;Tipo de Opera��o</td>
		<td class="td_dado">&nbsp;<%= testaDadoVazio(result,"CD_TIP_OPR_CTR")%> - <%= mTipoOperacao(result,"CD_TIP_OPR_CTR")%> </td>
	</tr>
	<tr>
		<td class="td_label" nowrap>&nbsp;Canal de Venda</td>
		<td class="td_dado">&nbsp;<%= testaDadoVazio(result,"CD_CNL_VND_CTR")%> - <%= mCanalVenda(result,"CD_CNL_VND_CTR")%> </td>
	</tr>
	<tr>
		<td class="td_label" nowrap>&nbsp;Data da Ap�lice</td>
		<td class="td_dado">&nbsp;<%= testaDadoVazioData(result,"DT_CTRC_CTR")%> </td>
	</tr>
	<%'If tp_wf_id <> Application("WF_EDSC_CANCELAMENTO") Then%>
		<tr>
			<td class="td_label" nowrap>&nbsp;Data de In�cio de Vig�ncia da Ap�lice</td>
			<td class="td_dado">&nbsp;<%
			dataI = testaDadoVazioData(result,"DT_INC_VGC_CTR")
			dataF = testaDadoVazioData(result,"DT_FIM_VGC_CTR")

			if trim(dataI) <> "" and trim(dataI) <> "---" then
				dataAtual = formatar_data(now())
				'if CDate(dataF) < CDate(dataAtual) then
					dataDias = datediff("d",CDate(dataF),CDate(dataI))
				'else
				'	dataDias = datediff("d",CDate(dataAtual),CDate(dataI))
				'end if

				if dataI < dataAtual And tp_wf_id <> Application("WF_EDSC_CANCELAMENTO") then
					Response.Write("<font color=""red""><b>" & dataI & "</b> &nbsp;&nbsp;&nbsp;Cota��o com prazo de vig�ncia retroativo. </font>")
				else
					Response.Write(dataI)
				end if

				if dataDias > 365 and tp_wf_id <> Application("WF_EDSC_CANCELAMENTO") then
					Response.Write(" In�cio de vig�ncia do " & sTexto & " posterior a 365 dias.")
				elseif dataDias < -365 then
					Response.Write(" In�cio de vig�ncia do " & sTexto & " anterior a 365 dias.")
				end if
			Else
				Response.Write dataI
			End If
			%> </td>
		</tr>
		<tr>
			<td class="td_label" nowrap>&nbsp;Data de Fim de Vig�ncia da Ap�lice</td>
			<td class="td_dado">&nbsp;<%= testaDadoVazioData(result,"DT_FIM_VGC_CTR")%>
			<%If CStr(tp_wf_id) <> CStr(Application("WF_EDSC_CANCELAMENTO")) Then
				If (DataF = DataI) And DataI <> "---" Then Response.Write "&nbsp; <font color=red>A vig�ncia deve ser de ao menos 1 dia.</font>" End If
			End If%>
			</td>
		</tr>
		<tr>
			<td class="td_label" nowrap>&nbsp;Quantidade de dias de validade da Ap�lice&nbsp;</td>
			<td class="td_dado">&nbsp;<%= dataDias*(-1)%> </td>
		</tr>
	<%
	     If testaDadoVazio(result,"CD_CNL_VND_CTR") = "2" Then%>
		<tr>
			<td class="td_label" nowrap>&nbsp;Cota��o de origem</td>
			<td class="td_dado"><a style="cursor:hand" href="javascript:window.open('/internet/serv/siscot3/exibicao_impressao.asp?cd_prd=<%=CD_PRD%>&cd_mdld=<%=CD_MDLD%>&cd_item_mdld=<%=CD_ITEM_MDLD%>&nr_ctc_sgro=<%If result("NR_CTC_SGRO_BB") <> "" Then Response.Write result("NR_CTC_SGRO_BB") Else Response.Write result("NR_CTC_SGRO") End If%>&nr_vrs_ctc=<%=NR_VRS_CTC%>','Impress�o','resizable=yes,height=600,width=586,toolbar=no,menubar=no,status=no,location=no,left=400,top=5,scrollbars=yes');window.history(-1)">
             <font style="text-decoration:none;color:navy;font-size:12px">&nbsp;<%If result("NR_CTC_SGRO_BB") <> "" Then Response.Write result("NR_CTC_SGRO_BB") Else Response.Write result("NR_CTC_SGRO") End If%>/<%=nr_vrs_ctc%></font></a></td>
	   </tr>
	<%   Else%>
	    <tr>
	 	    <td class="td_label" nowrap>&nbsp;Cota��o de origem</td>
		    <td class="td_dado"><a style="cursor:hand" href="javascript:window.open('/internet/serv/siscot3/exibicao_impressao.asp?cd_prd=<%=CD_PRD%>&cd_mdld=<%=CD_MDLD%>&cd_item_mdld=<%=CD_ITEM_MDLD%>&nr_ctc_sgro=<%=NR_CTC_SGRO%>&nr_vrs_ctc=<%=NR_VRS_CTC%>','Impress�o','resizable=yes,height=600,width=586,toolbar=no,menubar=no,status=no,location=no,left=400,top=5,scrollbars=yes');window.history(-1);">
             <font style="text-decoration:none;color:navy;font-size:12px">&nbsp;<%=nr_ctc_sgro%>/<%=nr_vrs_ctc%></font></a></td>
	   </tr>
	<%   End If
	
 	     If tp_wf_id <> Application("WF_EDSC_CANCELAMENTO") Then%>
		<tr>
			<td class="td_label" nowrap>&nbsp;Indicador de Subgrupo</td>
			<td class="td_dado">&nbsp;<%= trata_sim(testaDadoVazio(result,"IN_SGR_CTR"))%> </td>
		</tr>
		<tr>
			<td class="td_label" nowrap>&nbsp;Resseguro Facultativo</td>
			<td class="td_dado">
				<%
				sResseguro_Facultativo0 = ""
				sResseguro_Facultativo1 = ""
				if trim(Resseguro_Facultativo) = "S" then
					sResseguro_Facultativo0 = "checked"
				else
					sResseguro_Facultativo1 = "checked"
				end if
		        if bLiberarOptResseguro then%>
				<input type="radio" name="rResseguro_Facultativo" value="S" disabled="true" <%=sResseguro_Facultativo0%> onclick="javascript:AtribuirIndicaResseguroHidden();">&nbsp;Sim&nbsp;
				<input type="radio" name="rResseguro_Facultativo" value="N" disabled="true" <%=sResseguro_Facultativo1%> onclick="javascript:AtribuirIndicaResseguroHidden();">&nbsp;N�o
				<input type="hidden" name="Resseguro_Facultativo" value="">
		      <%Else
		 	       If resseguro_facultativo = "N" or resseguro_facultativo = "" Or IsNull(resseguro_facultativo) then
				      Response.Write "&nbsp;N�o"
		 	       Else
				      Response.Write "&nbsp;Sim"
			       End If
		        End If%>
			</td>
		</tr>
	<% End If%>
</table>

<script language="JavaScript">
<!--
function AtribuirIndicaResseguroHidden()
{

	if (document.all.rResseguro_Facultativo(0).checked)
		document.all.Resseguro_Facultativo.value = 'S';
	else if (document.all.rResseguro_Facultativo(1).checked)
		document.all.Resseguro_Facultativo.value = 'N';
	else
		document.all.Resseguro_Facultativo.value = '';

}

function ValidarEdicao(bExibe)
{
	document.all.rResseguro_Facultativo(0).disabled = !bExibe;
	document.all.rResseguro_Facultativo(1).disabled = !bExibe;
}
-->
</script>

<%if bLiberarOptResseguro then%>
<script>
ValidarEdicao(true);
AtribuirIndicaResseguroHidden(); //atualiza indicador de resseguro facultativo ao carregar a p�gina
</script>
</a></b>
<%end if%>