<!--include virtual = "/padrao.asp"-->
<%
'Response.Write "mLerCotacao:" & objCon.pCD_PRD & "-" & objCon.pCD_MDLD & "-" & objCon.pCD_ITEM_MDLD & "-" & objCon.pNR_CTC_SGRO & "-" & objCon.pNR_VRS_CTC

'response.Write "<br>"
'response.Write objCon.pAmbiente

Set result = objCon.mLerCotacao()
result.ActiveConnection = Nothing

If result.EOF Then
    Response.End
End If

NR_CTR_SGRO = result("NR_CTR_CTC")
vCNPJ = result("NR_SRF_PRPN_CTC")

'objCon.pNR_VRS_EDS = result("NR_VRS_EDS")

If Trim(Request("Resseguro_Facultativo")) = "" Then
	If isnull(result("Resseguro_Facultativo")) Then
		Resseguro_Facultativo = "N"
	Else
		Resseguro_Facultativo = result("Resseguro_Facultativo")
	End If
Else
	Resseguro_Facultativo = Request("Resseguro_Facultativo")
End If

%>
<!--#include virtual = "/funcao/formatar_data.asp"-->
<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha" id="Table1">
    <tr>
        <td colspan="6" class="td_label">
            <span class="sub_titulo" style="font-size: 12px"><b>:: Dados B�sicos da Cota��o</b></span>
        </td>
    </tr>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;Sub Ramo
        </td>
        <td class="td_dado" width="800">
            &nbsp;<%=CD_ITEM_MDLD & " - " & testaDadoVazio(result,"nome")%>
        </td>
    </tr>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;Inst�ncia
        </td>
        <td class="td_dado">
            &nbsp;<%=wf_id%>
        </td>
    </tr>
    <% If testaDadoVazio(result,"CD_CNL_VND_CTC") = 2 Then%>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;Cota��o/Vers�o
        </td>
        <td class="td_dado">
            &nbsp;<%If result("nr_ctc_sgro_bb") <> "" Then Response.Write result("nr_ctc_sgro_bb") Else Response.Write result("nr_ctc_sgro") End If%>/<%=nr_vrs_ctc%>
        </td>
    </tr>
    <% Else%>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;Cota��o/Vers�o
        </td>
        <td class="td_dado">
            &nbsp;<%=nr_ctc_sgro%>/<%=nr_vrs_ctc%>
        </td>
    </tr>
    <% End If%>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;Situa��o
        </td>
        <%if not isnull(result("motivo_recusa")) then%>
        <td class="td_dado">
            &nbsp;<%=testaDadoVazio(result, "status")%>
            - Recusa
            <%=testaDadoVazio(result, "motivo_recusa")%>
        </td>
        <%else%>
        <td class="td_dado">
            &nbsp;<%=testaDadoVazio(result, "status")%>
        </td>
        <%end if%>
    </tr>
    <%if not isnull(result("motivo_real_recusa")) then%>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;Motivo da Recusa
        </td>
        <td class="td_dado">
            &nbsp;<%=testaDadoVazio(result, "motivo_real_recusa")%>
        </td>
    </tr>
    <%end if%>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;<%= testaDadoVazio(result,"tp_meio_canal")%>
        </td>
        <td class="td_dado">
            &nbsp;<%= testaDadoVazio(result,"cd_uor_ctc") & " - " & testaDadoVazio(result,"nome_agencia")%>
        </td>
    </tr>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;Proponente
        </td>
        <td class="td_dado">
            &nbsp;<%= testaDadoVazio(result,"nm_prpn_ctc")%>
        </td>
    </tr>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;CPF/CNPJ
        </td>
        <td class="td_dado">
            &nbsp;<%= trata_cpf_cnpj(result,"NR_SRF_PRPN_CTC")%>
        </td>
    </tr>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;Tipo de Opera��o
        </td>
        <td class="td_dado">
            &nbsp;<%= testaDadoVazio(result,"CD_TIP_OPR_CTC") & " - " & mTipoOperacao(result,"CD_TIP_OPR_CTC")%>
        </td>
    </tr>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;Canal de Venda
        </td>
        <td class="td_dado">
            &nbsp;<%= testaDadoVazio(result,"CD_CNL_VND_CTC") & " - " & mCanalVenda(result,"CD_CNL_VND_CTC")%>
        </td>
    </tr>
    <tr>
        <%If testaDadoVazio(result,"CD_TIP_OPR_CTC") = 3 And tp_wf_id = Application("WF_EDSC_CANCELAMENTO") Then%>
        <td class="td_label" nowrap>
            &nbsp;Data de Solicita��o do Cancelamento
        </td>
        <%ElseIf testaDadoVazio(result,"CD_TIP_OPR_CTC") = 3 Then%>
        <td class="td_label" nowrap>
            &nbsp;Data de Solicita��o do Endosso
        </td>
        <%Else%>
        <td class="td_label" nowrap>
            &nbsp;Data de Solicita��o da primeira Cota��o
        </td>
        <%End If%>
        <td class="td_dado">
            &nbsp;<%= testaDadoVazioData(result,"DT_SLCT_CTC")%>
        </td>
    </tr>
    <%
		 If testaDadoVazio(result,"CD_TIP_OPR_CTC") = 3 then
			sTexto = " Endosso"
		 Else
			sTexto = " Seguro"
		 End If

 		 If tp_wf_id <> Application("WF_EDSC_CANCELAMENTO") Then
    %>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;Data de In�cio de Vig�ncia do
            <%=sTexto%>
        </td>
        <td class="td_dado">
            &nbsp;<%
			dataI = testaDadoVazioData(result,"DT_INC_VGC_CTC")
			dataF = testaDadoVazioData(result,"DT_FIM_VGC_CTC")

			If Trim(dataI) <> "" and Trim(dataI) <> "---" Then
				dataAtual = formatar_data(now())
				if dataF < dataAtual then
					dataDias = datediff("d",dataF,dataI)
				else
					dataDias = datediff("d",dataAtual,dataI)
				end if
				
				if (CDate(dataI) < CDate(dataAtual)) And tp_wf_id <> Application("WF_EDSC_CANCELAMENTO") then
					Response.Write("<font color=""red""><b>" & dataI & "</b> &nbsp;&nbsp;&nbsp;Cota��o com prazo de vig�ncia retroativo. </font>")
				else
					Response.Write(dataI)
				end if

				if dataDias > 365 and tp_wf_id <> Application("WF_EDSC_CANCELAMENTO") then
					Response.Write(" In�cio de vig�ncia do " & sTexto & " posterior a 365 dias.")
				elseif dataDias < -365 then
					Response.Write(" In�cio de vig�ncia do " & sTexto & " anterior a 365 dias.")
				end if
			Else
				Response.Write dataI
			End If
            %>
        </td>
    </tr>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;Data de Fim de Vig�ncia do
            <%=sTexto%>
        </td>
        <td class="td_dado">
            &nbsp;<%= testaDadoVazioData(result,"DT_FIM_VGC_CTC")%>
            <%If CStr(tp_wf_id) <> CStr(Application("WF_EDSC_CANCELAMENTO")) Then
				If (DataF = DataI) And DataI <> "---" Then Response.Write "&nbsp; <font color=red>A vig�ncia deve ser de ao menos 1 dia.</font>" End If
			End If%>
        </td>
    </tr>
    <%End If

	     If tp_wf_id <> Application("WF_EDSC_CANCELAMENTO") Then%>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;Quantidade de dias de validade da Cota��o&nbsp;
        </td>
        <td class="td_dado">
            &nbsp;<%= testaDadoVazio(result,"QT_DD_VLD_CTC")%>
        </td>
    </tr>
    <%End If

      If NOT isnull(result("NR_CTR_CTC")) Then
         If testaDadoVazio(result,"NR_CTR_CTC") > 0 Then%>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;Ap�lice
        </td>
        <td class="td_dado">
            <b><a style="cursor: hand" href="javascript:imp_tar('<%Response.Write("contrato=" & testaDadoVazio(result,"NR_CTR_CTC"))%>');">
                <font style="text-decoration: none; color: navy; font-size: 12px">&nbsp;<%=testaDadoVazio(result,"NR_CTR_CTC") & "/" & testaDadoVazio(result,"NR_VRS_EDS")%></font>
            </a></b>
        </td>
    </tr>
    <%
	     End If
	  End If
	
	  If tp_wf_id <> Application("WF_EDSC_CANCELAMENTO") Then%>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;Indicador de Subgrupo
        </td>
        <td class="td_dado">
            &nbsp;<%= trata_sim(testaDadoVazio(result,"IN_SGR_CTC_SGRO"))%>
        </td>
    </tr>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;Resseguro Facultativo
        </td>
        <td class="td_dado">
            <%
		sResseguro_Facultativo0 = ""
		sResseguro_Facultativo1 = ""
		If Trim(Resseguro_Facultativo) = "S" Then
			sResseguro_Facultativo0 = "checked"
		Else
			sResseguro_Facultativo1 = "checked"
		End If

	    If bLiberarOptResseguro then%>
            <input type="radio" name="rResseguro_Facultativo" value="S" disabled="true" <%=sResseguro_Facultativo0%>
                onclick="javascript:AtribuirIndicaResseguroHidden();">&nbsp;Sim&nbsp;
            <input type="radio" name="rResseguro_Facultativo" value="N" disabled="true" <%=sResseguro_Facultativo1%>
                onclick="javascript:AtribuirIndicaResseguroHidden();">&nbsp;N�o
            <input type="hidden" name="Resseguro_Facultativo" value="">
            <%Else
			If UCase(resseguro_facultativo) = "N" or resseguro_facultativo = "" Or IsNull(resseguro_facultativo) then
				Response.Write "&nbsp;N�o"
			Else
				Response.Write "&nbsp;Sim"
			End If
		End If%>
        </td>
    </tr>
    <%End If%>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;Validade da cota��o
        </td>
        <td class="td_dado" width="800">
            &nbsp;<%=testaDadoVazio(result,"QT_DD_VLD_CTC")%>
        </td>
    </tr>
    <tr>
        
        <td class="td_label" nowrap>
            &nbsp;N�mero da Proposta BB
        </td>
        <td class="td_dado" width="800">
            &nbsp;<%=testaDadoVazio(result,"PROPOSTA_BB")%>
        </td>
        
    </tr>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;N�mero da Ap�lice
        </td>
        <td class="td_dado" width="800">
            &nbsp;<%=testaDadoVazio(result,"NR_APOLICE")%>
        </td>
    </tr>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;Produto BB
        </td>
        <td class="td_dado" width="800">
            &nbsp;<%=testaDadoVazio(result,"produto_bb")%>
        </td>
    </tr>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;N�mero cota��o BB
        </td>
        <td class="td_dado" width="800">
            &nbsp;<%=testaDadoVazio(result,"NR_CTC_SGRO_BB")%>
        </td>
    </tr>

    <%'wander 19/04/2013 inicio campos adicionados  %>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;Data de In�cio de Vig�ncia do Seguro
        </td>
        <td class="td_dado" width="800">
            &nbsp;<%=testaDadoVazio(result,"DT_INICIO_VIGENCIA_APOLICE")%>
        </td>
    </tr>
    <tr>
        <td class="td_label" nowrap>
            &nbsp;Tipo de Endosso
        </td>
        <td class="td_dado" width="800">
            &nbsp;<%=testaDadoVazio(result,"TP_ENDOSSO_ID")%> &nbsp; - 
            &nbsp;<%=testaDadoVazio(result,"DESCRICAO_ENDOSSO")%> 
        </td>
    </tr>
	<%'wander 19/04/2013 inicio campos adicionados  %>
	 <tr>
        <td class="td_label" nowrap>
            &nbsp;Flexibilizada
        </td>
        <td class="td_dado" width="800">
            &nbsp;<%=testaDadoVazio(result,"IN_FLEX_CTC")%> 
        </td>
    </tr>
    
</table>
<script language="JavaScript">
<!--
    function AtribuirIndicaResseguroHidden() {
        if (document.all.rResseguro_Facultativo(0).checked)
            document.all.Resseguro_Facultativo.value = 'S';
        else if (document.all.rResseguro_Facultativo(1).checked)
            document.all.Resseguro_Facultativo.value = 'N';
        else
            document.all.Resseguro_Facultativo.value = '';
    }

    function ValidarEdicao(bExibe) {
        document.all.rResseguro_Facultativo(0).disabled = !bExibe;
        document.all.rResseguro_Facultativo(1).disabled = !bExibe;
    }

    function imp_tar(strparam) {
        var janelaimp1 = window.open("/internet/serv/siscot3/exibicao_impressao_ctr.asp?apo=1&" + strparam,
	"Ap�lice", "resizable=yes,height=600,width=586,toolbar=no,menubar=no,status=no,location=no,left=400,top=5,scrollbars=yes");
    }
//-->
</script>
<%if bLiberarOptResseguro then%>
<script>
    ValidarEdicao(true);
    AtribuirIndicaResseguroHidden(); //atualiza indicador de resseguro facultativo ao carregar a p�gina
</script>
</a></b>
<%end if%>
