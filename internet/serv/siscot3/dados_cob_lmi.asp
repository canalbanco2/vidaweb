<input type="hidden" name="trocou_seql_cob" value="">
<script>
function processasubgrupo()
{
	try { document.formProc.NR_SEQL_CBT_CTC.selectedIndex = 0; } catch (ex) { }
	document.formProc.submit();
}

function submitcobertura(i,nrqst){
	document.formProc.action = 'exibicao_cob.asp?indice_procura=' + i + '&questionario_id=' + nrqst;
    document.formProc.cursor = 'wait';
	document.formProc.submit();
}
</script>
<table cellpadding="3" cellspacing="0" border="1" width="832" class="escolha" bordercolor="#EEEEEE">
	<tr>
		<td class="td_label" width="20"></td>
		<td class="td_label" width="41"><b>C�d. Cob</b></td>
		<td class="td_label" width="280"><b>Cobertura</b></td>
		<td class="td_label" width="130"><b>Locais de Risco</b></td>
		<td class="td_label" width="78"><b>LMI</b></td>
		<td class="td_label" width="46"><b>Taxa NET</b></td>
		<td class="td_label" width="82"><b>Pr�mio NET</b></td>
	</tr>
<%
 	Redim aCobertASP(100,11)

    chave
    iContador = 0

    Set rsCobertura = objcon.mLerDadosCoberturaLmi()
    rsCobertura.ActiveConnection = Nothing
	If NOT rsCobertura.EOF Then
		valor_lmi_total = 0
		valor_premio_net_total = 0
        Do while not rsCobertura.EOF
		   pc_prem_cbt_ctc = testaDadoVazioFormataN(rsCobertura,"pc_prem_cbt_ctc",5)
		   vl_prem_cbt_ctc = testaDadoVazioFormataN(rsCobertura,"vl_prem_cbt_ctc",2)
           vl_iptc_cbt_ctc = testaDadoVazioFormataN(rsCobertura,"vl_iptc_cbt_ctc",2)
		
		  'Indexador i, refere-se a cobertura
		   achou = "N"
	       cd_cbt      = testaDadoVazioN(rsCobertura,"CD_CBT")
	       codObjeto   = cd_cbt & "|" & testaDadoVazioN(rsCobertura,"NR_SEQL_LIM_CTC")
	       nr_seql_cbt = testaDadoVazioN(rsCobertura,"NR_LIM_QSTN_CTC")

          'Franquias
		   objcon.pCD_CBT = cd_cbt
		   objcon.pNR_SEQL_CBT_CTC = nr_seql_cbt
		   Set rsFranquia = objCon.mLerDadosFranquia
		   tx_rgr_frquTotal = ""
		   If not rsFranquia.EOF Then
		       Do While NOT rsFranquia.EOF
			      tx_rgr_frquTotal = tx_rgr_frquTotal & Trim(rsFranquia("tx_rgr_frqu")) & "|"
			      If UCase(Trim(rsFranquia("in_frqu_pdro"))) = "S" Then
			 	  	 tx_rgr_frquAux = Trim(rsFranquia("tx_rgr_frqu")) & tx_rgr_frquTotal & "|"
			      End If
			      rsFranquia.MoveNext
		       Loop
		   End If

		   For i = 0 To iContador - 1
		      'Acumula locais de risco
		       If aCobertASP(i,10) = codObjeto Then
		          achou = "S"
		          aCobertASP(i,1) = aCobertASP(i,1) & ", " & nr_seql_cbt
		          aCobertASP(i,7) = aCobertASP(i,7) & testaDadoVazioN(rsCobertura,"TX_FRQU_CTC") & "@"
		          aCobertASP(i,8) = aCobertASP(i,8) & tx_rgr_frquTotal & "@"
		          Exit For
		       End If
		   Next
		   If achou = "N" Then
		       aCobertASP(iContador,0)  = cd_cbt
		       aCobertASP(iContador,1)  = nr_seql_cbt
		       aCobertASP(iContador,2)  = testaDadoVazioN(rsCobertura,"nome")
		       aCobertASP(iContador,3)  = testaDadoVazioN(rsCobertura,"DT_INC_VCL")
		       aCobertASP(iContador,4)  = vl_iptc_cbt_ctc
		       aCobertASP(iContador,5)  = pc_prem_cbt_ctc
		       aCobertASP(iContador,6)  = vl_prem_cbt_ctc
		       aCobertASP(iContador,7)  = testaDadoVazioN(rsCobertura,"TX_FRQU_CTC") & "@"
		       aCobertASP(iContador,8)  = tx_rgr_frquTotal & "@"
		       aCobertASP(iContador,9)  = testaDadoVazioN(rsCobertura,"LIMITE_CAPITAL")
		       aCobertASP(iContador,10) = codObjeto
 		        		       
	  	       If vl_iptc_cbt_ctc <> "" And not isnull(rsCobertura("ACUMULA_IS")) Then
			       If UCase(trim(rsCobertura("ACUMULA_IS"))) = "S" Then
			   	       valor_lmi_total = CDbl(valor_lmi_total) + CDbl(vl_iptc_cbt_ctc)
			       End If
  		       End If
		       If Trim(vl_prem_cbt_ctc) <> "" Then
		 	      valor_premio_net_total = CDbl(valor_premio_net_total) + CDbl(vl_prem_cbt_ctc)
		       End If

		       iContador = iContador + 1
 		   End If
		   rsCobertura.MoveNext
	   Loop
	
       For i = 0 To iContador - 1
		  alterna_cor	%>
		<tr id="tr_<%= i%>">
			<td width="20" class="td_dado" style="<%=cor_zebra%>">
			    <% objCon.pCD_CBT = aCobertASP(i,0)
			       objCon.pCD_TIP_QSTN = "3"
	               Set rsQST = objcon.mLerQuestionarioId()
	               If rsQST.EOF Then%>&nbsp;
	            <% Else%>   
                <input type="radio" name="R" onClick="submitcobertura(<%= i%>,<%=rsQST("NR_QSTN")%>);" style="cursor:hand;"
                    <%If CInt(Request.QueryString("indice_procura")) = i Then Response.Write " checked" End If%>>
                <% End If%>
			</td>		
			<td width="41" class="td_dado" style="<%=cor_zebra%>">
 		  	    <input type="hidden" name="CD_CBT<%= i%>" value="<%= aCobertASP(i,0)%>">
 		  	    <input type="hidden" name="DT_INC_VCL<%= i%>" value="<%= aCobertASP(i,3)%>">
 		  	    <input type="hidden" name="TXT_FRANQUIA<%= i%>" value="<%= aCobertASP(i,7)%>">
 		  	    <input type="hidden" name="TXT_FRAN_COB<%= i%>" value="<%= aCobertASP(i,8)%>">
				&nbsp;<%= aCobertASP(i,0)%>
			</td>
			<td width="280" class="td_dado" style="<%=cor_zebra%>">
			    <input type="hidden" name="nome<%= i%>" value="<%= aCobertASP(i,2)%>">
				&nbsp;<%= aCobertASP(i,2)%>
			</td>
			<td width="130" class="td_dado" style="<%=cor_zebra%>">
				<input type="hidden" name="nr_sequencial_ctc<%= i%>" value="<%= aCobertASP(i,1)%>">
                &nbsp;<%= aCobertASP(i,1)%>
			</td>
			<td width="78" class="td_dado" style="<%=cor_zebra%>">
			    <input type="hidden" name="vl_iptc_cbt_ctc<%= i%>" value="<%= aCobertASP(i,4)%>">
				&nbsp;<%= formatnumber(aCobertASP(i,4),2)%>
			</td>
			<td width="46" class="td_dado" style="<%=cor_zebra%>">
			    <input type="hidden" name="pc_prem_cbt_ctc<%= i%>" value="<%= aCobertASP(i,5)%>">
				<div align="right" id="lpc_prem_cbt_ctc<%= i%>"><%= FormatNumber(aCobertASP(i,5),5)%></div>
			</td>
			<td width="82" class="td_dado" style="<%=cor_zebra%>">
			    <input type="hidden" name="vl_prem_cbt_ctc<%= i%>" value="<%= aCobertASP(i,6)%>">
			    <input type="hidden" name="limite_capital<%= i%>" value="<%= aCobertASP(i,9)%>">
				<div align="right" id="lvl_prem_cbt_ctc<%= i%>"><%= FormatNumber(aCobertASP(i,6),2)%></div>
			</td>
		 </tr>
	 	 <input type="hidden" value="" name="exibe<%= i%>">
     <%Next%>
</table>
	<br>
	<table cellpadding="3" cellspacing="0" border="1" width="831" class="escolha" bordercolor="#EEEEEE">
		<tr>
			<td class="td_label_negrito" width="229">Limite M�ximo de Garantia - 
            LMG</td>
			<td class="td_dado" width="584"><%=FormatNumber(valor_lmi_total,2)%>&nbsp;</td>
		</tr>
		<tr>
			<td class="td_label_negrito" width="229">Pr�mio NET Total:</td>
			<td class="td_dado" width="584"><%=FormatNumber(valor_premio_net_total,2)%>&nbsp;</td>
		</tr>
	</table>

<%  If Request.QueryString("indice_procura") <> "" And Request.QueryString("questionario_id") <> "" Then
        Locais = Split(Trim(aCobertASP(CInt(Request("indice_procura")),1))&", ",", ")
%>
	<br>
		<!-- #include file = "funcoes_dados_qst.asp"-->
		<%
		htmlExibir = "<table border=""0""><tr><td>" & vbcrlf
		objcon.pCD_CBT = aCobertASP(i,0)
		objcon.pCD_TIP_QSTN = "3"

		questionario_id = Request.QueryString("questionario_id")
		objcon.pNR_QSTN = questionario_id
        For indX = 0 To UBound(Locais)-1
		    NR_SEQL_QSTN_CTC = Locais(indX)
		    objCon.pNR_SEQL_QSTN_CTC = NR_SEQL_QSTN_CTC

            Set rsNomeQSTN = objcon.mLerLocalRisco()
            rsNomeQSTN.ActiveConnection = Nothing

		    If NOT rsNomeQSTN.EOF Then
		        If Trim(rsNomeQSTN("NR_CD_TIP_QSTN_CTC")) <> "0" and Trim(rsNomeQSTN("NR_CD_TIP_QSTN_CTC")) <> "" Then
			       htmlExibir = "<font face=""arial"" size=""2""><b>Local de Risco: "& NR_SEQL_QSTN_CTC &"</b></font>" & VbCrLf
			 %><!-- #include file = "dados_qst.asp"--><%
			    End If
		    End If
		Next
		htmlExibir2 = "</td></tr></table>" & vbcrlf
	End If
	End If%></td></tr>