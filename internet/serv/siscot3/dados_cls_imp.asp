	<tr>
		<td colspan="2">
			<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
				<tr><td style="font-size:5px">&nbsp;</td></tr>
				<tr><td colspan="2" class="td_label_negrito" style="text-align:center">Cl�usulas Gerais</td></tr>
				<%
				objcon.pTipo = "1"
	     	    Set rsCobertura = objcon.mLerDadosClausula()
				rsCobertura.ActiveConnection = Nothing
				vClsl = 1
				If NOT rsCobertura.EOF Then
				%>
					<tr>
						<td colspan="2">
						<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
							<tr>
								<td class="td_label_negrito" style="text-align:center" width="80">&nbsp;C�d. Cl�usula</td>
								<td class="td_label_negrito" style="text-align:left">&nbsp;T�tulo</td>
							</tr>
							<%
							While NOT rsCobertura.EOF
								alterna_cor
								%>
								<tr>
									<td class="td_dado" style="text-align:center" style="<%=cor_zebra%>"><%= testaDadoVazio(rsCobertura,"CD_CLSL")%>&nbsp;</td>
									<td class="td_dado" style="<%=cor_zebra%>"><a onclick="mostra_div(tr_<%=rsCobertura("CD_CLSL")%>_<%=vClsl%>)" style="text-decoration:none;color:#0000FF;cursor:hand"><%= testaDadoVazio(rsCobertura,"descr_clausula")%></a>&nbsp;</td>
								</tr>
								<tr id="tr_<%=rsCobertura("CD_CLSL")%>_<%=vClsl%>" style="display:none">
									<td class="td_dado" style="<%=cor_zebra%>">&nbsp;</td>
									<td class="td_dado" style="<%=cor_zebra%>"><%= Replace( testaDadoVazio(rsCobertura,"texto"), Chr(13) + Chr(10), "<br>") %><br></td>
								</tr>
								<%
								DivIds = DivIds & rsCobertura("CD_CLSL") & ","
								vClsl = vClsl + 1
								rsCobertura.MoveNext
							WEnd%>
						</table>
						</td>
					</tr>
				<%else%>
					<tr>
						<td class="td_label" colspan="2" style="text-align:center">&nbsp;Nenhuma cl�usula relacionada.</td>
					</tr>
				<%end if
				Set rsCobertura = nothing%>
				<tr><td style="font-size:5px">&nbsp;</td></tr>
				<tr><td colspan="2" class="td_label_negrito" style="text-align:center">Cl�usulas Especiais</td></tr>
				<%
				objcon.pTipo = "2"
				If opcaoPorContrato = "T" Then
		     	    Set rsCobertura = objcon.mLerDadosClausulaContrato()
				Else
		     	    Set rsCobertura = objcon.mLerDadosClausula()
				End If
				rsCobertura.ActiveConnection = Nothing
				if not rsCobertura.EOF Then
				%>
					<tr>
						<td colspan="2">
						<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
							<tr>
								<td class="td_label_negrito" style="text-align:center" width="80" style="<%=cor_zebra%>">&nbsp;C�d. Cl�usula</td>
								<td class="td_label_negrito" style="text-align:left" style="<%=cor_zebra%>">&nbsp;T�tulo</td>
							</tr>
							<%
							While NOT rsCobertura.EOF
								alterna_cor
								%>
								<tr>
									<td class="td_dado" style="text-align:center" style="<%=cor_zebra%>"><%= testaDadoVazio(rsCobertura,"CD_CLSL")%>&nbsp;</td>
									<td class="td_dado" style="<%=cor_zebra%>"><a onclick="mostra_div(tr_<%=rsCobertura("CD_CLSL")%>_<%=vClsl%>)" style="text-decoration:none;color:#0000FF;cursor:hand"><%= testaDadoVazio(rsCobertura,"descr_clausula")%></a>&nbsp;</td>
								</tr>
								<tr id="tr_<%=rsCobertura("CD_CLSL")%>_<%=vClsl%>" style="display:none">
									<td class="td_dado" style="<%=cor_zebra%>">&nbsp;</td>
									<td class="td_dado" style="<%=cor_zebra%>">
									<%
									objCon.pCD_CLSL = rsCobertura("CD_CLSL")
									set rsTexto = objCon.mLerTextoClausula()
									rsTexto.ActiveConnection = Nothing
									If NOT rsTexto.EOF Then
										Response.Write( Replace(testaDadoVazio(rsTexto,"TEXTO"), chr(13) + Chr(10), "<br>") )
									End If
									Set rsTexto = Nothing
									%>
									<br></td>
								</tr>
								<%
								DivIds = DivIds & rsCobertura("CD_CLSL") & ","
								vClsl = vClsl + 1
								rsCobertura.movenext
							wend%>
						</table>
						</td>
					</tr>
				<%else%>
					<tr>
						<td class="td_label" colspan="2" style="text-align:center">&nbsp;Nenhuma cl�usula relacionada.</td>
					</tr>
				<%end if
				set rsCobertura = nothing%>
				<tr><td style="font-size:5px">&nbsp;</td></tr>
				<tr><td colspan="2" class="td_label_negrito" style="text-align:center">Cl�usulas Facultativas</td></tr>
				<%
				objcon.pTipo = "4"
				If opcaoPorContrato = "T" Then
		     	    Set rsCobertura = objcon.mLerDadosClausulaContrato()
				Else
		     	    Set rsCobertura = objcon.mLerDadosClausula()
				End If
				rsCobertura.ActiveConnection = Nothing
				If NOT rsCobertura.EOF Then
				%>
					<tr>
						<td colspan="2">
						<table cellpadding="3" cellspacing="0" border="1" width="100%" class="escolha" bordercolor="#EEEEEE">
							<tr>
								<td class="td_label_negrito" style="text-align:center" width="80" style="<%=cor_zebra%>">&nbsp;C�d. Cl�usula</td>
								<td class="td_label_negrito" style="text-align:left" style="<%=cor_zebra%>">&nbsp;T�tulo</td>
							</tr>
							<%
							While not rsCobertura.EOF
								alterna_cor
								%>
								<tr>
									<td class="td_dado" style="text-align:center" style="<%=cor_zebra%>"><%= testaDadoVazio(rsCobertura,"CD_CLSL")%>&nbsp;</td>
									<td class="td_dado" style="<%=cor_zebra%>"><a onclick="mostra_div(tr_<%=rsCobertura("CD_CLSL")%>_<%=vClsl%>)" style="text-decoration:none;color:#0000FF;cursor:hand"><%= testaDadoVazio(rsCobertura,"descr_clausula")%></a>&nbsp;</td>
								</tr>
								<tr id="tr_<%=rsCobertura("CD_CLSL")%>_<%=vClsl%>" style="display:none">
									<td class="td_dado" style="<%=cor_zebra%>">&nbsp;</td>
									<td class="td_dado" style="<%=cor_zebra%>"><%= Replace( testaDadoVazio(rsCobertura,"texto"), chr(13) + Chr(10), "<br>") %><br></td>
								</tr>
								<%
								DivIds = DivIds & rsCobertura("CD_CLSL") & ","
								vClsl = vClsl + 1
								rsCobertura.movenext
							WEnd%>
						</table>
						</td>
					</tr>
				<%else%>
					<tr>
						<td class="td_label" colspan="2" style="text-align:center">&nbsp;A ser selecionada na cota��o.</td>
					</tr>
				<%end if
				Set rsCobertura = Nothing
				If DivIds <> "" Then
					DivIds = Mid(DivIds,1,len(DivIds)-1)
				End If
				%>
			</table>
		</td>
	</tr>