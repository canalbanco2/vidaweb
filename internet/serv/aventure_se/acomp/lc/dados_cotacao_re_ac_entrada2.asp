<%
set rs = objNegocio.VerificarExistenciaLMI(agendamento_id)
if rs.EOF then
	MostraRadio = True
	if cLMI = "" then
		cLMI = "N"
	end if
else
	MostraRadio = False
	if CInt(rs("lmi")) = 1 Then
		cLMI = "S"
		cLMI2 = "S"
	elseif CInt(rs("lmi")) = 2 Then
		cLMI = "S"
		cLMI2 = "N"
	else
		cLMI = "N"
	end if
	rs.MoveFirst
end if

set rs_cotacao = objNegocio.LerCotacaoRe(agendamento_id,CStr(cLMI))

'AMARCO - 21/05/2008 Nova regra para o percentual de IOF
data_sistema = objNegocio.ObtemDtOperacional
dt_cotacao1 = testaDadoVazio(rs_cotacao,"dt_cotacao")	

if isdate(dt_cotacao1) then

	dia_a = day(dt_cotacao1)
	mes_a = month(dt_cotacao1)
	ano_a = year(dt_cotacao1)
		
	if len(dia_a) < 2 then dia_a = "0" & dia_a end if
	if len(mes_a) < 2 then mes_a = "0" & mes_a end if
	
	dt_cotacao = ano_a & mes_a & dia_a
else
	dt_cotacao = data_sistema	
end if

IF CDbl(dt_cotacao) > CDbl("20080103") then
	vIOF = "7,38"
else
	vIOF = "7,00"
END IF
%>
<script language="JavaScript">
<!--
function FormataVal(val) {
	var num;
	var valor = "";
	for(i=0;i<val.length;i++) {
		num = val.substr(i,1);
		if(num == ".") { 
			num = "";
		}
		else if (num == ",") {
			num = ".";
		}
		else {
			num = num;
		}
		valor = valor + num;
	}

	return valor;
}

function CalculaPremioBrutoTotal() {
	var PremioBrutoTotal;
	//Formatando valores
	var PremioNetTotal = FormataVal(document.formProc.vl_premio_net_total.value);
	var Corretagem = FormataVal(document.formProc.perc_corretagem.value);
	var CustoApolice = FormataVal(document.formProc.custo_apolice.value);
	var IOF = FormataVal(document.formProc.perc_iof.value);

	if ((PremioNetTotal != "") && (Corretagem != "") && (CustoApolice != "") && (IOF != "")) {
		PremioNetTotal = parseFloat(PremioNetTotal);
		Corretagem = parseFloat(Corretagem);
		CustoApolice = parseFloat(CustoApolice);
		IOF = parseFloat(IOF);
		PremioBrutoTotal=((PremioNetTotal+CustoApolice)/(1-(Corretagem/100)))*(1+(IOF/100))+'';
		document.formProc.vl_premio_bruto_total.value = FormatNumber(PremioBrutoTotal.replace(".",","),2,3);
	}
}
function CalculaCustoApolice() {
	var ValorCustoApolice;
	//Formatando valores
	var PremioNetTotal = FormataVal(document.formProc.vl_premio_net_total.value);
	var Corretagem = FormataVal(document.formProc.perc_corretagem.value);

	if (PremioNetTotal != "") {
		PremioNetTotal = parseFloat(PremioNetTotal);
		if (PremioNetTotal/10 > 60) {
			ValorCustoApolice = 60+'';
		}
		else {
			ValorCustoApolice = (((PremioNetTotal/(1-(Corretagem/100))))/10)+'';
		}
		document.formProc.custo_apolice.value = FormatNumber(ValorCustoApolice.replace(".",","),2,3);
	}
	<%
	meu_num_parcelas = RetornaCampoObj(rs_cotacao,"num_maximo_parcelas","num_maximo_parcelas")
	meu_corretagem = RetornaCampoObj(rs_cotacao,"perc_corretagem","perc_corretagem")
	meu_custo_apolice = RetornaCampoObj(rs_cotacao,"custo_apolice","custo_apolice")
	meu_valor_premio = RetornaCampoObj(rs_cotacao,"vl_premio_net_total","vl_premio_net_total")
	%>
}
//-->
</script>
<form name="formProc">
<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
	<tr>
		<td width="230" class="td_label" >&nbsp;Prazo do seguro</td>
		<td class="td_dado">&nbsp;<input type="hidden" name="prazo_seguro" value="<% = RetornaCampoObj(rs_cotacao,"prazo_seguro","prazo_seguro")%>" size="5" style="text-align:right" readonly> 
		</td>
	</tr>
	<tr>
		<td width="230" class="td_label" >&nbsp;Valor IS Total</td>
		<td class="td_dado">&nbsp;<INPUT type="hidden" name="vl_is_total" value="<% = RetornaCampoObj(rs_cotacao,"vl_is_total","vl_is_total")%>" size="22" style="text-align:right" readonly>
		</td>
	</tr>
	<tr>
		<td width="230" class="td_label" >&nbsp;Valor Pr�mio Net Total</td>
		<td class="td_dado">&nbsp;<INPUT type="hidden" name="vl_premio_net_total" value="<% = RetornaCampoObj(rs_cotacao,"vl_premio_net_total","vl_premio_net_total")%>" size="22" style="text-align:right" readonly>
		<%vl_premio_net_total = RetornaCampoObj(rs_cotacao,"vl_premio_net_total","vl_premio_net_total")%>
		</td>
	</tr>
	<tr>
		<td width="230" class="td_label" >&nbsp;Corretagem (%)</td>
		<td class="td_dado">&nbsp;<INPUT type="hidden" name="perc_corretagem" value="<% = RetornaCampoObj(rs_cotacao,"perc_corretagem","perc_corretagem")%>" size="6" maxlength="6" erro="Corretagem" validacao="numerico" onKeydown="FormataValor('perc_corretagem',6,event)" onKeyUp="return autoTab(this, 6, event);" style="text-align:right" onBlur="return CalculaPremioBrutoTotal();">
		</td>
	</tr>
	<tr>
		<td width="230" class="td_label" >&nbsp;Custo de Ap�lice</td>
		<td class="td_dado">&nbsp;<INPUT type="hidden" name="custo_apolice" value="<% = RetornaCampoObj(rs_cotacao,"custo_apolice","custo_apolice")%>" size="20" maxlength="20" style="text-align:right" onBlur="return CalculaPremioBrutoTotal();" readonly>
		<%custo_apolice = RetornaCampoObj(rs_cotacao,"custo_apolice","custo_apolice")%>
		</td>
	</tr>
	<tr>
		<td width="230" class="td_label">&nbsp;IOF</td>
		<td class="td_dado">&nbsp;<INPUT type="hidden" name="perc_iof" value="<% = vIOF %>" size="6" style="text-align:right" onBlur="return CalculaPremioBrutoTotal();" readonly>
		<%iof = vIOF %>
		</td>
	</tr>
	<tr>
		<td width="230" class="td_label">&nbsp;Valor Pr�mio Bruto Total</td>
		<td class="td_dado">&nbsp;<INPUT type="hidden" name="vl_premio_bruto_total" value="<% = RetornaCampoObj(rs_cotacao,"vl_premio_bruto_total","vl_premio_bruto_total")%>" size="22" style="text-align:right" readonly>
		<%premio_total = RetornaCampoObj(rs_cotacao,"vl_premio_bruto_total","vl_premio_bruto_total")%>
		</td>
	</tr>
<td width="230" class="td_label" >&nbsp;Tipo Pagto</td>
		<td class="td_dado">&nbsp;Parcelado:&nbsp;
			<td class="td_dado">&nbsp;<INPUT type="hidden" name="ind_pgto_parcelado" value="<%=RetornaCampoObj(rs_cotacao,"ind_pgto_parcelado","ind_pgto_parcelado")%>" size="2" readonly>
		</td>
</tr>

	
	<tr>
		<td width="230" class="td_label">&nbsp;N� m�ximo de parcelas</td>
		<td class="td_dado">&nbsp;<INPUT type="hidden" name="num_maximo_parcelas" value="<% = RetornaCampoObj(rs_cotacao,"num_maximo_parcelas","num_maximo_parcelas")%>" size="2" readonly>
	</td>
	</tr>
	<tr>
		<td width="230" class="td_label">&nbsp;Coeficiente de parcelamento</td>
		<td class="td_dado">&nbsp;<INPUT type="hidden" name="vl_coef_parcelamento" value="<%=RetornaCampoObj(rs_cotacao,"vl_coef_parcelamento","vl_coef_parcelamento")%>" size="9" maxlength="9" erro="Coeficiente de parcelamento" validacao="numerico" onKeyUp="FormatValorContinuo(this,7);autoTab(this, 9, event);" style="text-align:right">
		<%coef_pacelamento = RetornaCampoObj(rs_cotacao,"vl_coef_parcelamento","vl_coef_parcelamento")%>
		</td>
	</tr>
	<tr>
		<td width="230" class="td_label">&nbsp;Taxa Juros</td>
		<td class="td_dado">&nbsp;<INPUT type="hidden" name="perc_juros" value="2,30" size="6" style="text-align:right" readonly>
		</td>
	</tr>
	<tr style="display:none">
		<td colspan="2">
			<input type="hidden" name="vl_premio_bruto_vista" value="<% = RetornaCampoObj(rs_cotacao,"vl_premio_bruto_vista","vl_premio_bruto_vista")%>">
			<input type="hidden" name="vl_pgto_ato" value="<% = RetornaCampoObj(rs_cotacao,"vl_pgto_ato","vl_pgto_ato")%>">
			<input type="hidden" name="num_parcelas" value="<% = RetornaCampoObj(rs_cotacao,"num_parcelas","num_parcelas")%>">
			<input type="hidden" name="vl_primeira_parcela" value="<% = RetornaCampoObj(rs_cotacao,"vl_primeira_parcela","vl_primeira_parcela")%>">
			<input type="hidden" name="vl_demais_parcelas" value="<% = RetornaCampoObj(rs_cotacao,"vl_demais_parcelas","vl_demais_parcelas")%>">
		</td>
	</tr>
</table>
</form>
<script>
<!--
CalculaCustoApolice();
CalculaPremioBrutoTotal();
<%
meu_num_parcelas = RetornaCampoObj(rs_cotacao,"num_maximo_parcelas","num_maximo_parcelas")
meu_corretagem = RetornaCampoObj(rs_cotacao,"perc_corretagem","perc_corretagem")
meu_custo_apolice = RetornaCampoObj(rs_cotacao,"custo_apolice","custo_apolice")
meu_valor_premio = RetornaCampoObj(rs_cotacao,"vl_premio_net_total","vl_premio_net_total")
%>
//-->
</script>
