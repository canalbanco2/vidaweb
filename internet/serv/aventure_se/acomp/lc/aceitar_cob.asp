<!--#include virtual = "/funcao/jscript.asp"-->
<!--#include virtual = "/funcao/data_corrente.asp"-->
<!--#include virtual = "/templates/bot_mens.asp"-->
<!--#include virtual = "/internet/serv/aventure_se/acomp/objWorkflowNegocio.asp"-->
<!--#include virtual = "/internet/serv/aventure_se/acomp/request_geral.asp"-->
<!--#include virtual = "/scripts/wbra_internetABS.asp"-->
<!--#include virtual = "/funcao/max_text_area.asp"-->
<%
set result = objNegocio.LerDadoExibicao(wf_id)
set rsNomeAtiv = objNegocio.LerNomeAtividade(tp_wf_id, tp_ativ_id)
LinkSubmit = "aceitar_cob.asp?" & link
%>
<!--#include virtual = "/internet/serv/aventure_se/acomp/checa_atividade.asp"-->
<html>
	<head>
		<title>Companhia de Seguros Aliança do Brasil</title>
		<LINK href="/estilo_internet.css" rel="STYLESHEET" title="style" type="text/css">
		<LINK href="/internet/serv/aventure_se/acomp/estilo_acomp.css" rel="STYLESHEET" title="style" type="text/css">
	</head>
	<BODY BGCOLOR="#ffffff" LINK="#0000ff" VLINK="#800080" TEXT="#000000" TOPMARGIN="0" LEFTMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0"<%if limpar_grid > 0 then%> onload="ChecaZeraGrid('<%=link & "&tp_tarefa_id=" & tp_tarefa_id%>');"<%end if%>><!--#include virtual = "/internet/serv/aventure_se/acomp/cabecalho.asp"-->
		<table border="0" class="tabelaform" cellspacing="0" cellpadding="4" align="center" style="WIDTH: 770px">
		<form name="formProc" action="aceitar_cob.asp" method="post">
		<!--#include virtual = "/internet/serv/aventure_se/acomp/hidden_geral.asp"-->
		<input type="hidden" name="tp_tarefa_id" value="<%=tp_tarefa_id%>">
			<tr valign="top">
				<td class="colunadadosesquerdoCinza" rowspan="2" width="155" align="middle" height="480">
					<font size="1"><br><br><br></font>
					<br>
					<input type="button" value="Finalizar Aceitar" class="BotaoPadrao" onclick="document.formProc.action='finalizar_aceitar.asp';document.formProc.submit();">
					<input type="reset" value="<%=btLimpa%>" class="BotaoPadrao">
					<input type="button" value="Sair (sem anotar)" class="BotaoPadrao" onclick="document.location.href='exibicao.asp?<%=link%>';">
				</td>
				<td>
					<br>
					<table cellpadding="2" cellspacing="1" border="0" width="600" class="escolha">
						<tr>
							<td class="td_titulo" colspan="2"><% = RetornaDado(rsNomeAtiv,"nome")%></td>
						</tr>
						<tr>
							<td class="sub_titulo"><IMG src="/img/seta_ms.gif" align=absMiddle><b>Aceitar</b></td>
							<td class="data"><% = DataCorrente()%></td>
						</tr>
					</table>
					<table cellSpacing="0" cellPadding="0" border="0" WIDTH="600">
						<TR>
							<td style="WIDTH: 10px; BORDER-BOTTOM: #9b9b9b 1px solid"><IMG src="/img/inicio_ini_cinza.jpg"></td>
							<TD background="/img/meio_cinza.jpg" style="FONT-SIZE: 10px; COLOR: #000000; BORDER-BOTTOM: #9b9b9b 1px solid; FONT-FAMILY: Verdana,Arial; TEXT-ALIGN: center"><b>&nbsp;<A style="TEXT-DECORATION: none" href="#" onclick="redirecionaPagina('aceitar_cob','aceitar');" ><font color="#000000">Contratação</font></A>&nbsp;</b></TD>
							<td style="WIDTH: 8px; BORDER-BOTTOM: #9b9b9b 1px solid"><IMG src="/img/fim_cinza.jpg" align=absMiddle></td>
							<td style="WIDTH: 10px"><IMG src="/img/inicio_branco.jpg"></td>
							<TD background="/img/meio_branco.jpg" style="FONT-SIZE: 10px; COLOR: #000000; FONT-FAMILY: Verdana,Arial; TEXT-ALIGN: center"><b>&nbsp;Beneficiário&nbsp;</b></TD>
							<td style="WIDTH: 10px"><IMG src="/img/fim_branco.jpg" align=absMiddle></td>
						</TR>
					</table>
					<!--#include virtual = "/internet/serv/aventure_se/acomp/dados_gerais.asp"-->
					<table cellpadding="2" cellspacing="1" border="0" width="0" class="escolha" style="width:600px">
						<tr>
							<td class="td_label">&nbsp;Nome</td>
							<td class="td_dado">&nbsp;<input type="text" name="nome" size="40" maxlength="60" erro="Nome" zero="no"></td>
						</tr>
						<tr>
							<td class="td_label">&nbsp;CPF / CNPJ</td>
							<td class="td_dado">&nbsp;<input type="text" name="cnpj_cpf" size="14" maxlength="14" erro="CPF / CNPJ" zero="no"></td>
						</tr>
						<tr>
							<td class="td_label">&nbsp;Participação(%)</td>
							<td class="td_dado">&nbsp;<input type="text" erro="Participação(%)" valMax="100" validacao="numerico" zero="no" name="perc_participacao" size="6" maxlength="6" onKeyUp="FormatValorContinuo(this,2);autoTab(this,6,event);" style="text-align:right;"></td>
						</tr>				
						<tr>
							<td class="td_label">&nbsp;Endereço</td>
							<td class="td_dado">&nbsp;<input erro="Endereço" type="text" name="endereco" erro="Endereço" zero="no" size="40" maxlength="60"></td>
						</tr>
						<tr>
							<td class="td_label">&nbsp;Complemento</td>
							<td class="td_dado">&nbsp;<input type="text" name="complemento" erro="Complemento" zero="no" size="20" maxlength="20"></td>
						</tr>
						<tr>
							<td class="td_label" width="20%">&nbsp;Bairro</td>
							<td class="td_dado" width="30%">&nbsp;<input erro="Bairro" type="text" erro="Bairro" zero="no" name="bairro" size="30" maxlength="30"></td>
						</tr>
						<tr>
							<td class="td_label" width="20%">&nbsp;CEP</td>
							<td class="td_dado" width="20%">&nbsp;<input erro="cep" validacao="numerico" zero="no" proximo="cep2" type="text" name="cep" size="5" maxlength="5" onKeyUp="return autoTab(this, 5, event);">&nbsp;<input erro="CEP" validacao="numerico"  type="text" name="cep2" size="3" maxlength="3" onKeyUp="return autoTab(this, 3, event);">
							</td>
						</tr>
						<tr>
							<td class="td_label" width="20%">&nbsp;UF</td>
							<td class="td_dado" width="20%">&nbsp;<select name="uf" erro="UF">
									<!--#include virtual = "/funcao/UF.asp"-->
								</select>
							</td>
						</tr>
						<tr>
							<td class="td_label" width="20%">&nbsp;Município</td>
							<td class="td_dado" width="30%">&nbsp;<input type="text" erro="Município" name="municipio" zero="no" maxlength="30" size="30">
							</td>
						</tr>
						<tr>
							<td colspan="2">
							<BR>
								<table cellpadding="2" cellspacing="1" border="0" width="500" class="escolha">
								<%
									set rs = objNegocio.LerBeneficiario(agendamento_id)
									LinkSubmit = "aceitar_cob.asp?" & link
									
									ShowHeadDb = False
									ReadOnly = False
									cabecalhoManual = True
									ReDim arrCampos(9)
									arrCampos(0) = "agendamento_id"
									arrCampos(1) = "nome"
									arrCampos(2) = "cnpj_cpf"
									arrCampos(3) = "perc_participacao"
									arrCampos(4) = "endereco"
									arrCampos(5) = "complemento"
									arrCampos(6) = "bairro"
									arrCampos(7) = "cep"
									arrCampos(8) = "uf"
									arrCampos(9) = "municipio"

									ReDim arrCabeca(9)
									arrCabeca(0) = ""
									arrCabeca(1) = "Nome"
									arrCabeca(2) = "CPF/CNPJ"
									arrCabeca(3) = "Participação(%)"
									arrCabeca(4) = "Endereço"
									arrCabeca(5) = "Complemento"
									arrCabeca(6) = "Bairro"
									arrCabeca(7) = "CEP"
									arrCabeca(8) = "UF"
									arrCabeca(9) = "Município"

									ReDim arrDominio(9)
									arrDominio(0) = ""
									arrDominio(1) = ""
									arrDominio(2) = ""
									arrDominio(3) = ""
									arrDominio(4) = ""
									arrDominio(5) = ""
									arrDominio(6) = ""
									arrDominio(7) = ""
									arrDominio(8) = ""
									arrDominio(9) = ""

									Dim arrProximos(9)
									arrProximos(0) = ""
									arrProximos(1) = ""
									arrProximos(2) = ""
									arrProximos(3) = ""
									arrProximos(4) = ""
									arrProximos(5) = ""
									arrProximos(6) = ""
									arrProximos(7) = "cep2"
									arrProximos(8) = ""
									arrProximos(9) = ""
								%>
								<!--#include virtual = "/internet/serv/aventure_se/acomp/gride_itens_contato.asp"-->
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!--#include virtual = "/internet/serv/aventure_se/acomp/dados_gerais_abas_externos.asp"-->
		</form>
		</table>
	</BODY>
</html>