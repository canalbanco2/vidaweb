<!--include virtual = "/funcao/jscript.asp"-->
<!--include virtual = "/funcao/abre_janela.asp"-->
<!--#include virtual = "/funcao/data_corrente.asp"-->
<!--#include virtual = "/internet/serv/aventure_se/acomp/objWorkflowNegocio.asp"-->
<!--#include virtual = "/internet/serv/aventure_se/acomp/request_geral.asp"-->

<html>
	<head>
		<title>Companhia de Seguros Alian�a do Brasil</title>
		<LINK href="/estilo_internet.css" rel="STYLESHEET" title="style" type="text/css">
		<LINK href="/internet/serv/aventure_se/acomp/estilo_acomp.css" rel="STYLESHEET" title="style" type="text/css">
	</head>
	<BODY BGCOLOR="#FFFFFF" LINK="#0000FF" VLINK="#800080" TEXT="#000000" TOPMARGIN="0" LEFTMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0">
		<!--#include virtual = "/internet/serv/aventure_se/acomp/cabecalho.asp"-->
<script language="JavaScript"><!--
function y2k(number) { return (number < 1000) ? number + 1900 : number; }

function isDate (day,month,year) {
// checks if date passed is valid
// will accept dates in following format:
// isDate(dd,mm,ccyy), or
// isDate(dd,mm) - which defaults to the current year, or
// isDate(dd) - which defaults to the current month and year.
// Note, if passed the month must be between 1 and 12, and the
// year in ccyy format.

    var today = new Date();
    year = ((!year) ? y2k(today.getYear()):year);
    month = ((!month) ? today.getMonth():month-1);
    if (!day) return false
    var test = new Date(year,month,day);
    if ( (y2k(test.getYear()) == year) &&
         (month == test.getMonth()) &&
         (day == test.getDate()) )
        return true;
    else
        return false
}

function validaForm(){
	var ddIni = document.fFiltro.dd_ini.value;
	var mmIni = document.fFiltro.mm_ini.value;
	var aaIni = document.fFiltro.aa_ini.value;
	// valida a data inicial, caso tenha sido informada
	if ( ddIni != '' & mmIni != '' & aaIni != ''){
		if ( ! isDate( ddIni, mmIni, aaIni)) {
			alert('Data de in�cio inv�lida');
			return false;
		} else {
		// se a data inicial � v�lida, valida tamb�m a final
			var ddFim = document.fFiltro.dd_fim.value;
			var mmFim = document.fFiltro.mm_fim.value;
			var aaFim = document.fFiltro.aa_fim.value;
			if ( ddFim != '' & mmFim != '' & aaFim != ''){
				if ( ! isDate( ddFim, mmFim, aaFim)) {
					alert('Data de final inv�lida');
					return false;
				}
			}
		}
	} 
	document.fFiltro.action = 'resultRel.asp';
	document.fFiltro.target = 'lista'
}

function mudaSuper(){
	document.fFiltro.agencia_id.value = '';
	document.fFiltro.action = 'relAgencia.asp';
	document.fFiltro.target = 'fAgencia';
	document.fFiltro.submit();
}

//-->
</script>


		<table border="0" class="tabelaform" cellspacing="0" cellpadding="4" align="center" style="WIDTH: 770px">
			<tr valign="top">
				<%
				tituloMenu = "Alternativas de an�lise"
				%>
				<!--include virtual = "/internet/serv/aventure_se/acomp/menu_lateral.asp"-->
				<td><br>
					<table cellpadding="2" cellspacing="1" border="0" width="100%" class="escolha">
						<tr>
							<td class="td_titulo" colspan="2">Relat�rios</td>
						</tr>
						<tr>
							<td class="sub_titulo">&nbsp;</td>
							<td class="data"><% = DataCorrente()%></td>
						</tr>
					</table>
					<form name="fFiltro" method=post target=lista action="resultRel.asp" onsubmit="javascript: return validaForm();">
					<table border=0 cellpadding=2 cellspacing=2 width="100%" class="escolha">
						<tr>
							<td valign=top width="10%" class="td_label">Super</td>
							<td valign=top width="40%" class="td_dado">
							<%Set oConn = Server.CreateObject("ADODB.Connection")
								set StrConex = server.CreateObject("conexao_alianca_ole.Connection")
								
							'oConn.ConnectionString = strconex.Internet_Prod_Seguros
							oConn.ConnectString = strconex.Internet_Qld_Web_Seguros
							oConn.Open
							sSQL =	"select distinct a.cod_super, nom_super from seguros_db..agencia_tb a " & _
									"inner join siscot_db..agencia_tb b on a.agencia_id = b.agencia_id " & _
									" where a.cod_super is not null and a.cod_super <> '' order by nom_super"
							Set oRS = oConn.Execute(sSQL)%>							
							<select name=cod_super style="width=250px" onchange="javascript: mudaSuper();">
								<option value="">Todas</option>
								<%Do While not oRS.EOF
									Response.Write "<option value='" & oRS("cod_super") & "'>" & oRS("cod_super") & " - " & oRS("nom_super") & "</option>"
									oRS.movenext
								Loop
								Set oRS = Nothing%>
								</select>
							</td>
							<td valign=top width="10%" class="td_label">Ag�ncia</td>
							<td valign=top width="40%" class="td_dado">
								<iframe name="fAgencia" width=250 height=22 frameborder=0 src="relAgencia.asp"></iframe>
							</td>
						</tr>
						<tr>
							<td valign=top width="10%" class="td_label">Cliente</td>
							<td valign=top width="40%" class="td_dado"><input type=text name=cliente size=30 maxlength=60></td>
							<td valign=top width="10%" class="td_label">Data de in�cio</td>
							<td valign=top width="40%" class="td_dado">
								De <input type=text name=dd_ini size=1 maxlength=2> / <input type=text name=mm_ini size=1 maxlength=2> / <input type=text name=aa_ini size=2 maxlength=4 value="<%=Year(now())%>">
								<Br> at� <input type=text name=dd_fim size=1 maxlength=2> / <input type=text name=mm_fim size=1 maxlength=2> / <input type=text name=aa_fim size=2 maxlength=4 value="<%=Year(now())%>">
							</td>
						</tr>
						<tr><td colspan=4 align=center>
							<table border=0 cellpadding=0 cellspacing=0 width="100%">
								<tr><Td width=33% align=center><input type="submit" value="Pesquisar"  name=pesquisar></td>
									<Td width=34% align=center><input type="button" value="Exportar Excel" onclick="javascript:window.open('relXLS.asp', 'Excel');" target="_blank"></td>
									<Td width=33% align=center><input type="reset" value="Limpar" name=limpar></td></tr>
							</table>
						</td>
						</tr>
					</table>
					<input type=hidden name=agencia_id value="">
					</form>

					<script language="JavaScript">
						vWidth = screen.width;
							window.document.write ('<iframe name="lista" width=' +  eval(vWidth * 0.85) + ' height=' +  eval(vWidth * 0.30) + ' src="resultRel.asp" frameborder=no></iframe>');
					</script>

<%							oConn.Close
							Set oConn = Nothing
%>
			</td>
			</tr>
		</table>
	</body>
</html>