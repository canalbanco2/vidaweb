<HTML>
<HEAD></HEAD>

<BODY BGCOLOR="#FFFFFF" LINK="#0000FF" VLINK="#800080" TEXT="#000000" TOPMARGIN="0" LEFTMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0">
<!--include virtual = "/funcao/jscript.asp"-->
<!--include virtual = "/funcao/abre_janela.asp"-->
<!--include virtual = "/templates/bot_mens.asp"-->
<!--include virtual = "/internet/serv/aventure_se/acomp/objWorkflowNegocio.asp"-->
<LINK href="/estilo_internet.css" rel="STYLESHEET" title="style" type="text/css">
<LINK href="/internet/serv/aventure_se/acomp/estilo_acomp.css" rel="STYLESHEET" title="style" type="text/css">

<%If Ucase(Request.ServerVariables("REQUEST_METHOD")) = "GET" Then%>

	<script language="JavaScript"><!--
	function y2k(number) { return (number < 1000) ? number + 1900 : number; }

	function isDate (day,month,year) {
	// checks if date passed is valid
	// will accept dates in following format:
	// isDate(dd,mm,ccyy), or
	// isDate(dd,mm) - which defaults to the current year, or
	// isDate(dd) - which defaults to the current month and year.
	// Note, if passed the month must be between 1 and 12, and the
	// year in ccyy format.

	    var today = new Date();
	    year = ((!year) ? y2k(today.getYear()):year);
	    month = ((!month) ? today.getMonth():month-1);
	    if (!day) return false
	    var test = new Date(year,month,day);
	    if ( (y2k(test.getYear()) == year) &&
	         (month == test.getMonth()) &&
	         (day == test.getDate()) )
	        return true;
	    else
	        return false
	}

	function validaForm(){
		var ddIni = document.fFiltro.dd_ini.value;
		var mmIni = document.fFiltro.mm_ini.value;
		var aaIni = document.fFiltro.aa_ini.value;
		// valida a data inicial, caso tenha sido informada
		if ( ddIni != '' & mmIni != '' & aaIni != ''){
			if ( ! isDate( ddIni, mmIni, aaIni)) {
				alert('Data de in�cio inv�lida');
				return false;
			} else {
			// se a data inicial � v�lida, valida tamb�m a final
				var ddFim = document.fFiltro.dd_fim.value;
				var mmFim = document.fFiltro.mm_fim.value;
				var aaFim = document.fFiltro.aa_fim.value;
				if ( ddFim != '' & mmFim != '' & aaFim != ''){
					if ( ! isDate( ddFim, mmFim, aaFim)) {
						alert('Data de final inv�lida');
						return false;
					}
				}
			}
		}
		document.fFiltro.target = 'resultado';
		document.fFiltro.fl_excel.value = '0';
		document.fFiltro.pesquisar.value = 'Aguarde...';
		document.fFiltro.pesquisar.disabled = true;
	}
	
	function geraExcel()
	{
		var vOk = true;
		vOk =  validaForm();
		if (vOk != false)
		{
			document.fFiltro.target = '_blank';
			document.fFiltro.fl_excel.value = 1;
			document.fFiltro.submit();
		}
	}

	//-->
	</script>
	<%
		'recupera data inicial
		If Request("dd_ini") <> "" Then vDia = Request("dd_ini") Else vDia = Day(now()) End If
		If Request("mm_ini") <> "" Then vMes = Request("mm_ini") Else vMes = Month(now()) End If
		If Request("aa_ini") <> "" Then vAno = Request("aa_ini") Else vAno = Year(now()) End If

		'formata para sempre exibir como 9(2)
		If Len(vDia) = 1 then vDia = "0" & vDia End If
		If Len(vMes) = 1 then vMes = "0" & vMes End If
		
		'monta data padr�o SQL
		DataIni = vAno & "-" & vMes & "-" & vDia & " 00:00"

		'recupera data final
		if request("dd_fim") <> "" then vDiaFim = request("dd_fim") else vDiaFim = Day( DateAdd("d", 1, CDate(DataIni)) ) end if
		if request("mm_fim") <> "" then vMesFim = request("mm_fim") else vMesFim = Month( DateAdd("d", 1, CDate(DataIni)) ) end if
		if request("aa_fim") <> "" then vAnoFim = request("aa_fim") else vAnoFim = Year( DateAdd("d", 1, CDate(DataIni)) ) end if

		'formata para sempre exibir como 9(2)
		If Len(vDiaFim) = 1 then vDiaFim = "0" & vDiaFim End If
		If Len(vMesFim) = 1 then vMesFim = "0" & vMesFim End If
		DataFim = vAnoFim & "-" & vMesFim & "-" & vDiaFim & " 23:59"

	%>
		<div align=center>
		<hr width="95%">

			<form name="fFiltro" method=post target=resultado onsubmit="javascript: return validaForm();">
			<table border=0 cellpadding=2 cellspacing=2 width="96%" class="escolha">
				<tr>
					<td class="td_dado" colspan="2">Selecione as caracter�sticas desejadas:</td>
				</tr>
				<tr>
					<td valign=top width="10%" class="td_label">Data inicial</td>
					<td valign=top width="40%" class="td_dado">
						<input type=text name=dd_ini size=1 maxlength=2 value="<%=vDia%>"> / 
						<input type=text name=mm_ini size=1 maxlength=2 value="<%=vMes%>"> / 
						<input type=text name=aa_ini size=2 maxlength=4 value="<%=vAno%>">
					</td>
					<td valign=top width="10%" class="td_label">Data final</td>
					<td valign=top width="40%" class="td_dado">
						<input type=text name=dd_fim size=1 maxlength=2 value="<%=vDiaFim%>"> / 
						<input type=text name=mm_fim size=1 maxlength=2 value="<%=vMesFim%>"> / 
						<input type=text name=aa_fim size=2 maxlength=4 value="<%=vAnoFim%>">
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr><td colspan=4 align=center>
					<table border=0 cellpadding=0 cellspacing=0 width="100%">
						<tr><Td width=33% align=center><input type="submit" style="width:120px;" value="Pesquisar"  name=pesquisar></td>
							<Td width=34% align=center><input type="button" style="width:120px;" value="Exportar Excel" onclick="javascript: geraExcel();" target="_blank" name=cmdExcel></td>
							<Td width=33% align=center><input type="reset" style="width:120px;" value="Limpar" name=limpar></td></tr>
					</table>
				</td>
				</tr>
				<tr><td colspan=4 align=center><br>
					<script language="JavaScript">
						vWidth = screen.availWidth;
						vHeight = screen.availHeight;
						window.document.write ('<iframe name="resultado" width=' +  eval(vWidth * 0.88) + ' height=' +  eval(vHeight * 0.44) + ' src="" frameborder=no></iframe>');
					</script>
				</td></tr>
			</table>
			<input type=hidden name=fl_excel value=0>
			</form>
		</div>
		</body>
	</html>
<%Else%>

	<!--#include virtual = "/scripts/wbra_internet.asp"-->
	<%'recupera data inicial
	If Request("dd_ini") <> "" Then vDia = Request("dd_ini") Else vDia = Day(now()) End If
	If Request("mm_ini") <> "" Then vMes = Request("mm_ini") Else vMes = Month(now()) End If
	If Request("aa_ini") <> "" Then vAno = Request("aa_ini") Else vAno = Year(now()) End If

	'formata para sempre exibir como 9(2)
	If Len(vDia) = 1 then vDia = "0" & vDia End If
	If Len(vMes) = 1 then vMes = "0" & vMes End If
		
	'monta data padr�o SQL
	DataIni = vAno & "-" & vMes & "-" & vDia & " 00:00"

	'recupera data final
	if request("dd_fim") <> "" then vDiaFim = request("dd_fim") else vDiaFim = Day( DateAdd("d", 1, CDate(DataIni)) ) end if
	if request("mm_fim") <> "" then vMesFim = request("mm_fim") else vMesFim = Month( DateAdd("d", 1, CDate(DataIni)) ) end if
	if request("aa_fim") <> "" then vAnoFim = request("aa_fim") else vAnoFim = Year( DateAdd("d", 1, CDate(DataIni)) ) end if

	'formata para sempre exibir como 9(2)
	If Len(vDiaFim) = 1 then vDiaFim = "0" & vDiaFim End If
	If Len(vMesFim) = 1 then vMesFim = "0" & vMesFim End If
	DataFim = vAnoFim & "-" & vMesFim & "-" & vDiaFim & " 23:59"

	'caso as datas estejam trocadas, arruma a ordem
	If CDate(DataIni) > CDate(DataFim) Then
		DataTmp = DataIni
		DataIni = DataFim
		DataFim = DataTmp
	End If

	'verifica flag para gerar Excel
	If Request("fl_excel") = "1" then
		Response.ContentType = "application/xls"
		Response.AddHeader "Content-Disposition", "filename=AcompVistoria-" & vDia & vMes & vAno & "_" & vHora & vMin & ".xls;"
	End If
	
	sSQL =	"exec siscot_db..rel_avaliacao_laudo_sps '" & DataIni & "', '" & DataFim & "'"
	
	'Response.Write ssql
	Set oRs = Server.CreateObject("ADODB.Recordset")
	conex.CursorLocation = 3
	oRs.open ssql, conex
		
	Response.Write "<table width='100%' class='escolha' cellspacing='0' cellpadding='2' border='1' bordercolor='#EEEEEE'>" & Chr(13)
	Response.Write "<tr><td class='td_label' colspan=" & oRS.Fields.count & "><b>Acompanhamento de avalia��o de laudo</b>"
	Response.Write "    <br>De " & vDia & "/" & vMes & "/" & vAno & _
		" a " & vDiaFim & "/" & vMesFim & "/" & vAnoFim
	
	If Not oRS.EOF Then
		Response.Write "	<Tr>"
		Response.Write "		<td class='td_label' nowrap valign=top align=center>&nbsp;</td>" & Chr(13)
		Response.Write "		<td class='td_label' nowrap valign=top align=center><b>Saldo inicial</b></td>" & Chr(13)
		Response.Write "		<td class='td_label' nowrap valign=top align=center><b>Habilitadas no per�odo</b></td>" & Chr(13)
		Response.Write "		<td class='td_label' nowrap valign=top align=center><b>Total dispon�vel</b></td>" & Chr(13)
		Response.Write "		<td class='td_label' nowrap valign=top align=center><b>Liberadas</b></td>" & Chr(13)
		Response.Write "		<td class='td_label' nowrap valign=top align=center><b>Saldo final</b></td>" & Chr(13)
		Response.Write "	</tr>" & Chr(13)
	
		Do While not oRS.EOF
			Response.Write "<tr>"
			Response.Write "	<td class='td_label' nowrap valign=top align=center><b>Avalia��o</b></td>" & Chr(13)    
			For i = 2 to (oRS.Fields.Count - 1)
				Response.Write "	<td nowrap class='td_dado' valign=top align=left width='16" & Chr(37) & "'>"
				If IsNull(oRS(i)) Then Response.Write "&nbsp;" Else Response.Write oRS(i) End If
				Response.Write "</td>" & Chr(13)
			Next
			Response.Write "</tr>" & Chr(13)
			oRS.MoveNext
		Loop

		set oRs = oRs.NextRecordset
		Do While not oRS.EOF
			Response.Write "<tr>"
			Response.Write "	<td class='td_label' nowrap valign=top align=center><b>Cota��o</b></td>" & Chr(13)    

			For i = 2 to (oRS.Fields.Count - 1)
				Response.Write "	<td nowrap class='td_dado' valign=top align=left width='16" & Chr(37) & "'>"
				If IsNull(oRS(i)) Then Response.Write "&nbsp;" Else Response.Write oRS(i) End If
				Response.Write "</td>" & Chr(13)
			Next
			Response.Write "</tr>" & Chr(13)
			oRS.MoveNext
		Loop

	Else
		Response.Write "<p align=center>Nada encontrado.</p>"
	End If
	Response.Write "</table>" & Chr(13)


	Set oRs = Nothing
	conex.Close
	Set conex = Nothing
End If%>
<script language="JavaScript">
window.top.document.frames[0].fFiltro.pesquisar.value = "Pesquisar";
window.top.document.frames[0].fFiltro.pesquisar.disabled = false;
</script>