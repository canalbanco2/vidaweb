<!--include virtual = "/funcao/jscript.asp"-->
<!--include virtual = "/funcao/abre_janela.asp"-->
<!--#include virtual = "/funcao/data_corrente.asp"-->
<!--#include virtual = "/internet/serv/aventure_se/acomp/objWorkflowNegocio.asp"-->
<!--#include virtual = "/internet/serv/aventure_se/acomp/request_geral.asp"-->

<html>
	<head>
		<title>Companhia de Seguros Alian�a do Brasil</title>
		<LINK href="/estilo_internet.css" rel="STYLESHEET" title="style" type="text/css">
		<LINK href="/internet/serv/aventure_se/acomp/estilo_acomp.css" rel="STYLESHEET" title="style" type="text/css">
	</head>
	<BODY BGCOLOR="#FFFFFF" LINK="#0000FF" VLINK="#800080" TEXT="#000000" TOPMARGIN="0" LEFTMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0">

<script language="JavaScript"><!--
function y2k(number) { return (number < 1000) ? number + 1900 : number; }

function isDate (day,month,year) {
// checks if date passed is valid
// will accept dates in following format:
// isDate(dd,mm,ccyy), or
// isDate(dd,mm) - which defaults to the current year, or
// isDate(dd) - which defaults to the current month and year.
// Note, if passed the month must be between 1 and 12, and the
// year in ccyy format.

    var today = new Date();
    year = ((!year) ? y2k(today.getYear()):year);
    month = ((!month) ? today.getMonth():month-1);
    if (!day) return false
    var test = new Date(year,month,day);
    if ( (y2k(test.getYear()) == year) &&
         (month == test.getMonth()) &&
         (day == test.getDate()) )
        return true;
    else
        return false
}

function validaForm(){
	var ddIni = document.fFiltro.dd_ini.value;
	var mmIni = document.fFiltro.mm_ini.value;
	var aaIni = document.fFiltro.aa_ini.value;
	// valida a data inicial, caso tenha sido informada
	if ( ddIni != '' & mmIni != '' & aaIni != ''){
		if ( ! isDate( ddIni, mmIni, aaIni)) {
			alert('Data de in�cio inv�lida');
			return false;
		} else {
		// se a data inicial � v�lida, valida tamb�m a final
			var ddFim = document.fFiltro.dd_fim.value;
			var mmFim = document.fFiltro.mm_fim.value;
			var aaFim = document.fFiltro.aa_fim.value;
			if ( ddFim != '' & mmFim != '' & aaFim != ''){
				if ( ! isDate( ddFim, mmFim, aaFim)) {
					alert('Data de final inv�lida');
					return false;
				}
			}
		}
	} 
	document.fFiltro.action = 'resultRel.asp';
	document.fFiltro.target = 'lista'
}

function mudaSuper(){
	document.fFiltro.agencia_id.value = '';
	document.fFiltro.action = 'relAgencia.asp';
	document.fFiltro.target = 'fAgencia';
	document.fFiltro.submit();
}

//-->
</script>

					<form name="fFiltro" method=post target=lista action="resultRel.asp" onsubmit="javascript: return validaForm();">
					<table border=0 cellpadding=2 cellspacing=2 width="95%" class="escolha">
						<tr>
							<td valign=top width="10%" class="td_label">Data inicial</td>
							<td valign=top width="40%" class="td_dado">
							</td>
							<td valign=top width="40%" class="td_dado">
								De <input type=text name=dd_ini size=1 maxlength=2> / <input type=text name=mm_ini size=1 maxlength=2> / <input type=text name=aa_ini size=2 maxlength=4 value="<%=Year(now())%>">
								<Br> at� <input type=text name=dd_fim size=1 maxlength=2> / <input type=text name=mm_fim size=1 maxlength=2> / <input type=text name=aa_fim size=2 maxlength=4 value="<%=Year(now())%>">
							</td>
						</tr>
						<tr>
							<td valign=top width="10%" class="td_label">Cliente</td>
							<td valign=top width="40%" class="td_dado"><input type=text name=cliente size=30 maxlength=60></td>
							<td valign=top width="10%" class="td_label">Data de in�cio</td>
							<td valign=top width="40%" class="td_dado">
								De <input type=text name=dd_ini size=1 maxlength=2> / <input type=text name=mm_ini size=1 maxlength=2> / <input type=text name=aa_ini size=2 maxlength=4 value="<%=Year(now())%>">
								<Br> at� <input type=text name=dd_fim size=1 maxlength=2> / <input type=text name=mm_fim size=1 maxlength=2> / <input type=text name=aa_fim size=2 maxlength=4 value="<%=Year(now())%>">
							</td>
						</tr>
						<tr><td colspan=4 align=center>
							<table border=0 cellpadding=0 cellspacing=0 width="100%">
								<tr><Td width=33% align=center><input type="submit" value="Pesquisar"  name=pesquisar></td>
									<Td width=34% align=center><input type="button" value="Exportar Excel" onclick="javascript:window.open('relXLS.asp', 'Excel');" target="_blank" id=button1 name=button1></td>
									<Td width=33% align=center><input type="reset" value="Limpar" name=limpar></td></tr>
							</table>
						</td>
						</tr>
					</table>
					<input type=hidden name=agencia_id value="">
					</form>

<%
	sql = "SELECT DISTINCT c.agencia_id, a.wf_id as instancia, substring(c.cnpj_cpf,1,2) + '.' + substring(c.cnpj_cpf,3,3) + '.' + substring(c.cnpj_cpf,6,3) + '/' + substring(c.cnpj_cpf,9,4) + '-' + substring(c.cnpj_cpf,13,2) as cnpj_cliente, " & _
		"c.nome as cliente, convert(char(10),c.dt_agendamento,103)as dt_vencimento, b.nome as atividade, a.dt_hora_habilitacao, 'executada' as estado, " & _
		"datediff(day,a.dt_hora_habilitacao,a.dt_hora_estado)as qtde_dias " & _
		"FROM workflow_db..atividade_tb as a, " & _
		"meta_workflow_db..tp_atividade_tb as b, " & _
		"siscot_db..agendamento_tb as c, " & _
		"segab_db..unidade_tb as d, " & _
		"segab_db..usuario_unidade_tb as e, " & _
		"segab_db..usuario_tb as f " & _
		"WHERE a.tp_wf_id = 36 and " & _
		"a.ativ_estado in (5) and a.tp_wf_id = b.tp_wf_id and  a.tp_ativ_id = b.tp_ativ_id and  " & _
		"c.wf_id = a.wf_id and c.tp_seguro = 'R' and convert(varchar(6),c.agencia_id) =d.sigla and " & _
		"d.unidade_id = e.unidade_id and e.usuario_id = f.usuario_id and f.usuario_id in " & _
		"(SELECT usuario_id FROM segab_db..usuario_tb where email like '%consultor%') and a.tp_ativ_id not in (1,2) " & _
		"UNION " & _
	SELECT 
	DISTINCT
	c.agencia_id,
	a.wf_id as instancia,
	substring(c.cnpj_cpf,1,2) + '.' + substring(c.cnpj_cpf,3,3) + '.' + substring(c.cnpj_cpf,6,3) + '/' + substring(c.cnpj_cpf,9,4) + '-' + substring(c.cnpj_cpf,13,2) as cnpj_cliente,
	c.nome as cliente,
	convert(char(10),c.dt_agendamento,103)as dt_vencimento,
	b.nome as atividade,
	a.dt_hora_habilitacao,
	'habilitada' as estado,
	datediff(day,a.dt_hora_habilitacao,getdate())as qtde_dias
	FROM 
	workflow_db..atividade_tb as a,
	meta_workflow_db..tp_atividade_tb as b,
	siscot_db..agendamento_tb as c, 
	segab_db..unidade_tb as d,
	segab_db..usuario_unidade_tb as e,
	segab_db..usuario_tb as f
	
	WHERE 
	a.tp_wf_id = 36 and 
	a.ativ_estado in (1) and
	a.tp_wf_id = b.tp_wf_id and
	a.tp_ativ_id = b.tp_ativ_id and
	c.wf_id = a.wf_id and
	c.tp_seguro = 'R' and
	convert(varchar(6),c.agencia_id) =d.sigla and
	d.unidade_id = e.unidade_id and
	e.usuario_id = f.usuario_id and
	f.usuario_id in
	(
	SELECT usuario_id FROM segab_db..usuario_tb where email like '%consultor%'

%>

	</body>
</html>