 <%@Language="VBScript"%>

<%

Dim intColunas, intLinhas
Dim objConn, objRs, objTable


Set objConn = Server.CreateObject("ADODB.Connection")


With objConn
 .Provider = "Microsoft.Jet.OLEDB.4.0"
 .ConnectionString = "Data Source="&Server.MapPath("pasta.xls")&";Extended Properties=Excel 8.0;"
 .Open
End With

Set objTable = Server.CreateObject("ADOX.Catalog")
objTable.ActiveConnection = objConn

for j = 0 to objTable.tables.count - 1
 if lcase(objTable.tables(j).Type) = "table" Then

  set objRs = Server.CreateObject("ADODB.Recordset")
  objRs.CursorLocation = 3
  objRs.Open "SELECT * FROM ["&Cstr(Ucase(objTable.tables(j).Name))&"] ",objConn
  intCount = 0
  if Not objRs.Eof Then

   intColunas  = objRs.Fields.Count

   Response.Write ("<table border=""1"">" & vbNewline)
   
   If intCount = 0 Then
        Response.Write ("<tr>" & vbNewline)
        For i = 0 To (intColunas-1)
         Response.Write ("<td><font size=""1"" face=""verdana""> " & objRs.Fields(i).Name & "</td>" & vbNewline)
        next
        Response.Write ("</tr>" & vbNewline)
   End If

   Do While Not objRs.Eof  
        Response.Write ("<tr>" & vbNewline)
        For i = 0 To (intColunas-1)
         Response.Write ("<td><font size=""1"" face=""verdana""> " & objRs.Fields(i).Value & "</td>" & vbNewline)
        Next
        response.write ("</tr>" & vbNewline)
        objRs.MoveNext
   Loop

   Response.Write ("</table>" & vbNewline)  
   Response.Write ("Nome da Tabela(Planilha):" & Cstr(Ucase(objTable.tables(j).Name)) & vbNewline)
   Response.Write ("<br/>" & vbNewline)
   Response.Write ("Total Colunas: " & intColunas & vbNewline)
   Response.Write ("<br/>" & vbNewline)
   Response.Write ("Total Linhas: " & objRs.RecordCount +1 & vbNewline) ' o +1 é por causa da primeira linha que é o name da coluna

  End If  

  objRs.Close
  Set objRs = Nothing  

  Response.Write ("<br/>")
  Response.Write ("<br/>")

 end if
next

 

objConn.Close

Set objConn = Nothing
Set objTable = Nothing

%>
