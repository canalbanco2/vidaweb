<html>
<head>
<title>Ajuda - 2� via de pagamento</title>
</head>
<BODY BGCOLOR="#FFFFFF" BACKGROUND="/img/fundo_servicos.gif" LINK="#0000FF" VLINK="#800080" TEXT="#000000" TOPMARGIN="2" LEFTMARGIN="2" MARGINWIDTH="2" MARGINHEIGHT="2">
<TABLE align="center" background="" width="425" border="0" cellPadding="1" cellSpacing="2" borderColor="#FCF8D6">
<TR>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" 
            size="2"><STRONG>CPF</STRONG></FONT></TD>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" size="2">Informe o CPF se a ap�lice � de pessoa f�sica. N�o utilize h�fem, barra ou ponto.</FONT></TD></TR>
    <TR>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" 
            size="2"><STRONG>CGC</STRONG></FONT></TD>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" size="2">Informe o CGC se a ap�lice � de pessoa jur�dica. N�o utilize h�fem, barra ou ponto.</FONT></TD></TR>            
    <TR>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" 
            size="2"><STRONG>Ap�lice</STRONG></FONT></TD>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" 
            size="2">Informe o n�mero da ap�lice do seguro. Este n�mero encontra-se na ap�lice que voc� recebeu pelo correio.</FONT></TD></TR>
    <TR>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" 
            size="2"><STRONG>Proposta</STRONG></FONT></TD>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" size="2">Informe o n�mero da proposta do seguro. Este n�mero encontra-se na proposta que voc� assinou no momento da contrata��o do seguro.</FONT></TD></TR>
   <TR>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" 
            size="2" color=#ff0000><STRONG>Observa��o</STRONG></FONT></TD>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" size="2">Existem algumas situa��es que n�o � permitido imprimir a 2� via de pagamento:</FONT></TD></TR>
   <TR>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" 
            size="2"><STRONG></STRONG></FONT></TD>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" size="2">1. Ap�lices com d�bito autom�tico em conta corrente.</FONT></TD></TR>
   <TR>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" 
            size="2"><STRONG></STRONG></FONT></TD>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" size="2">2. Parcela com pagamento em d�lar.</FONT></TD></TR>
   <TR>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" 
            size="2"><STRONG></STRONG></FONT></TD>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" size="2">3. Parcela com mais de 15 dias da data de vencimento .</FONT></TD></TR>
   <TR>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" 
            size="2"><STRONG></STRONG></FONT></TD>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" size="2">4. Parcela com situa��o cancelada ou j� paga.</FONT></TD></TR>
   <TR>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" 
            size="2"><STRONG>Dica</STRONG></FONT></TD>
        <TD bgColor="#FCF8D6"><FONT face="Arial,Helvetica,Univers,Zurich BT" size="2">Se voc� n�o conseguiu acessar as informa��es do seu seguro, fale com o seu corretor.</FONT></TD></TR>
</TABLE>
</BODY>
</html>