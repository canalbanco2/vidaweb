<!--#include virtual="/scripts/wbrseguros.asp"-->
<%
'---- CursorTypeEnum Values ----
Const adOpenForwardOnly = 0
Const adOpenKeyset = 1
Const adOpenDynamic = 2
Const adOpenStatic = 3

'---- LockTypeEnum Values ----
Const adLockReadOnly = 1
Const adLockPessimistic = 2
Const adLockOptimistic = 3
Const adLockBatchOptimistic = 4

'const boletodados="BOLETO"
const tamanho=650

public tamanhodoc, nubanco, carteira, agencia, conta, digiconta, banco, imgbanco, bancono, digiagencia, diginumero
public gifbarra, ncodbarra, mcn, mac

sql = "select"
'rotina que pega as informa��es. Pode ser alterado esta parte.
tamanhodoc=11
nubanco="0019"
carteira="15"
agencia="04529"
conta="004052005"
banco="Banco do Brasil"
imgbanco="img/bb2.gif"
bancono="0019"
digiagencia=dac1129bb(agencia)
digiconta=dac1129bb(conta)

'codcedente=request("codcedente")
'sacado=ucase(request("sacado"))
'endsacado=ucase(request("endsacado"))
'bairro=request("bairro")
'cidade=request("cidade")
'estado=request("estado")
'cep=request("cep")

' dados fixos do sacado para demonstra��o. Aqui ir� o m�dulo do arquivo.
sacado="Alberto Medeiros Fritsch"
endsacado="Rua Senador Texeira n� 45"
bairro="Flamengo"
cidade="Rio de Janeiro"
estado="RJ"
cep="20950-000"

sacado=ucase(sacado)
endsacado=ucase(endsacado)
if bairro<>"" then endsacado=endsacado & " - " & ucase(bairro)
if cidade<>"" then endsacado=endsacado & " - " & ucase(cidade)
if estado<>"" then endsacado=endsacado & " - " & ucase(estado)
if cep<>"" then endsacado=endsacado & " - CEP: " & ucase(cep)

valor=560

'valor=request("valor")
if not isnumeric(valor) then
   call erro("Valor inv�lido!")
   response.end
end if
if valor>999999999999 then
   call erro("Valor do boleto acima de R$ 999.999.999.999,00")
   response.end
end if
if valor<5 then
   call erro("Valor do boleto abaixo de R$ 5,00")
   response.end
end if

' PEGA O N�MERO DO BOLETO A SER GERADO NO BANCO DE DADOS.
' No caso, est� com valor fixo.

nossonumero="55750238096"

' preenche o n�mero com 0.
For a = 1 To tamanhodoc - Len(Trim(nossonumero))
   nossonumero="0" & nossonumero
Next
diginumero=dac1129bb( nossonumero)
numdoc="DEMO" & right(nossonumero,4)
valor=replace(valor,".",",")
if not isnumeric(valor)then
   call erro("Valor indefinido!")
   response.end
end if
valor=formatnumber(valor,2)


dvence=day(date+5)
mvence=month(date+5)
avence=year(date+5)

if dvence<10 then dvence="0" & dvence
if mvence<10 then mvence="0" & mvence


'dvence=request("dvence")
'mvence=request("mvence")
'avence=request("avence")
vencimento=dvence & "/" & mvence & "/" & avence
if request("ndoc")="" then
  if not isdate(vencimento) then
     call erro("Data Inv�lida!")
     response.end
  end if
  if date>cdate(vencimento) then
     call erro("Data j� vencida!")
     response.end
  end if
end if
datadoc=date
especiedoc="DP"
aceite="N"
especie="R$"

'pega o cedente do banco de dados
'sql="SELECT * FROM CEDENTE WHERE CODIGO=" & trim(codcedente) & ""
'Set Conn = Server.CreateObject("ADODB.Connection")
'Set RS = Server.CreateObject("ADODB.RecordSet")
'Conn.Open "boldemo"
'RS.Open sql, Conn, adOpenKeyset,adLockReadOnly
'if rs.eof then
'   rs.close
'   call erro("Cedente n�o cadastrado: <b>" & nubanco & "</b>")
'   response.end
'end if

'obs1=rs("obs1")
'obs2=rs("obs2")
'obs3=rs("obs3")
'obs4=rs("obs4")
'obs5=rs("obs5")
'obs6=rs("obs6")
'cedente=trim(rs("nomefanta"))
'cedente2=trim(rs("razao")) & " - CGC: " & trim(rs("cgc"))
'cedente3=trim(rs("razao")) & " - CGC: " & trim(rs("cgc"))

'endereco=trim(rs("end1")) & "<br>" & trim(rs("end2")) & "<br>" & trim(rs("end3"))
'instru1=rs("instru1")
'instru2=rs("instru2")
'instru2=instru2 & " " & ucase(banco)
'instru3=rs("instru3")
'imgcedente=rs("gifcedente")
'rs.close

titboleto="Informa��es Adicionais"
obs1="Observa��o da primeira linha."
obs2="Observa��o da segunda linha."
obs3="Observa��o da terceira linha."
obs4="Observa��o da quarta linha."
obs5="Observa��o da quinta linha."
obs6="Observa��o da sexta linha."
cedente="Cia de Seguros Alian�a do Brasil"
cedente3=cedente
cedente2=cedente & " - CGC: 28.196.889/0001-43"

endereco="L�lio Gama 105 - 25� ao 31� andar - Centro<br>Rio de Janeiro - RJ - 20031-201<br>Tel: (21) 510-7433"
instru1="SR. CAIXA, N�O RECEBA AP�S O VENCIMENTO."
instru2="PAG�VEL PREFERENCIALMENTE NO BANCO DO BRASIL"
instru3="N�O COBRAR JUROS DE MORA"
imgcedente="img/brasil.gif"

' fun��o que gera o c�digo do boleto, segundo os dados definidos acima.
call faz001()

' fun��o que salva os dados do boleto gerado no banco de dados.
'call salva()

' fun��o que gera o boleto na tela do usu�rio.
call impboleto()

response.end

'****************
function faz001()
'****************
tudo=bancono & nossonumero & agencia & conta & carteira
parte1=mid(tudo,1,5)
parte2=mid(tudo,6,4)

parte3=mid(tudo,10,5)
parte4=mid(tudo,15,5)

parte5=mid(tudo,20,5)
parte6=mid(tudo,25,5)

digi1=dac10(parte1 & parte2)
digi2=dac10(parte3 & parte4)
digi3=dac10(parte5 & parte6)

showvalor=replace(valor,",","")
showvalor=replace(showvalor,".","")
enchevalor=showvalor
for yy=1 to 14-len(showvalor)
    enchevalor="0" & enchevalor
next
tdv=bancono & enchevalor & nossonumero & agencia & conta & carteira
dv=dac1129bb(tdv)
dv1=dac10(parte1 & parte2)
dv2=dac10(parte3 & parte4)
dv3=dac10(parte5 & parte6)
ncodbarra=parte1 & "." & parte2 & dv1 &" " & parte3 & "." &  parte4 & dv2 & " " & parte5 & "." & parte6 & dv3 & "  " & dv & "  " & showvalor
gifbarra=tdv
mcn=formatnumber(nossonumero,0)
mac= agencia & "-" & digiagencia & " / " & conta & "-" & digiconta
end function

'***************
function salva()
'***************
'grava o titulo no banco de dados
salvad=carteira & " / " & nossonumero & "-" & diginumero
sql2="SELECT * FROM TITULO"
Set Conn = Server.CreateObject("ADODB.Connection")
Set RS2 = Server.CreateObject("ADODB.RecordSet")
Conn.Open "boldemo"
RS2.Open sql2, Conn, adOpenKeyset, adLockOptimistic
rs2.addnew
rs2.update "cliente", codcedente
rs2.update "nomesacado",sacado
rs2.update "data",date
rs2.update "valor",valor
rs2.update "vencimento", vencimento

rs2.update "nossonum",salvad
rs2.update "docno", numdoc
rs2.update "codigotit", ncodbarra
rs2.update "codigobarra", gifbarra
rs2.update "de","Pagamento de teste gerado na WEB"
rs2.update "ip", eip
rs2.update "pagou",false
rs2.close
conn.close
end function

'************************
Function dacmod1129(arg1)
'************************
Local1 = Len(Trim(arg1))
local4 = 0
Dim local5(8)
For xx = 1 To 8
  local5(xx) = xx + 1
Next
local6 = 1
For Local3 = Local1 To 1 Step -1
    sz=Mid(arg1, Local3, 1) * local5(local6)
    local4 = local4 + sz
    If local6 = 8 Then local6=0
    local6 = local6 + 1
Next
ss = local4 Mod 11
ss2=ss
if ss2=0 or ss2=1 then
   local2=1
else
   local2=11-ss2
end if
dacmod1129 = Right(trim(local2), 1)
End Function

'*******************
Function DAC10(arg1)
'*******************
aa = Len(Trim(arg1)) / 2
If Int(aa) = aa Then
   arg1 = "0" + arg1
End If
ninicio = Len(Trim(arg1)) + 1
ntotal = 0
If (ninicio < 2) Then
   ninicio = 2
End If
ntotal=0
ccpoaux = "0" + Trim(arg1)
For x = ninicio To 1 Step -2
   cnumero = Mid(ccpoaux, x, 1)
   ntotal = ntotal + InStr("516273849", cnumero)
   ntotal = ntotal + Mid(ccpoaux, x - 1, 1)
next
DAC10 = InStr("987654321", Right(ntotal, 1))
End Function

'************************
Function dac1129bb(arg1)
'************************
Local1 = Len(Trim(arg1))
local4 = 0
Dim local5(8)
For xx = 1 To 8
  local5(xx) = xx + 1
Next
local6 = 1
For Local3 = Local1 To 1 Step -1
    sz=Mid(arg1, Local3, 1) * local5(local6)
    local4 = local4 + sz
    If local6 = 8 Then local6=0
    local6 = local6 + 1
Next
ss = local4 Mod 11
ss2=ss
if ss2=0 or ss2=1 then
   local2=1
else
   local2=11-ss2
end if
dac1129bb = Right(trim(local2), 1)
End Function

'*****************
function seguranca
'*****************
if mid(eip,1,12)<>"200.246.122." then
   response.redirect("http://www.node1.com.br")
   response.end
end if
end function

'*******************
function erro(texto)
'*******************
%>
<HTML><HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<META NAME="Author" CONTENT="Node 1 Internet"> 
<TITLE>M�dulo do Boleto Banc�rio</TITLE></HEAD>
<font face="Verdana, Arial" size=+1 color="#005a9f">
<b>M�dulo do Boleto Banc�rio</b>
</font color>
<br>
<font face="Arial" size=3 color=red>
<br>
<b>Desculpe, ocorreu o seguinte erro:</b>
<p>
<b><%=texto%></b>
<p>
<center>
<a href="javascript:history.back()">Voltar</a>
</center>
<p>
<br>
<br>
<center>
<font face="Arial" size=1 color=black>
<a href="http://www.node1.com.br/">Node 1 Internet</a> - 1999 - Todos os direitos reservados.
</center>
</font>
</td>
</table>
<br>
</html>
<%
end function

'*******************
function impboleto()
'*******************
%>
<HTML>
<HEAD>
<TITLE>Demo Boleto Banc�rio - Node 1 Internet</TITLE>
</HEAD>
<BODY BGCOLOR="#FFFFFF">
<CENTER>

<TABLE WIDTH="<%=tamanho%>" CELLSPACING=0 CELLPADDING=0 BORDER=0>
<TR>
<TD WIDTH=225><IMG SRC="<%=imgcedente%>"><br><br></TD>
<TD WIDTH=425 ALIGN=RIGHT><FONT FACE="Arial, Helvetica" SIZE="-1">
<%=cedente%><BR><%=endereco%></FONT></TD>
</TR>
<TR>
<TD VALIGN=BOTTOM WIDTH=225><IMG SRC="<%=imgbanco%>"></TD>
<TD width="425" ALIGN=RIGHT VALIGN=BOTTOM><FONT FACE="Arial, Helvetica" SIZE="2"><B>RECIBO DO SACADO</B></FONT></TD>
</TR>
</TABLE>

<TABLE WIDTH="<%=tamanho%>" BORDER=1 CELLSPACING=0 CELLPADDING=1>
<TR>
<TD><FONT FACE="Arial, Helvetica" SIZE="1">Cedente</FONT><BR>
<FONT FACE="Arial, Helvetica" SIZE="-1">
<%=cedente3%>
</FONT></TD>
<TD><FONT FACE="Arial, Helvetica" SIZE="1">Ag&ecirc;ncia / C&oacute;digo Cedente</FONT><BR><FONT FACE="Arial, Helvetica" SIZE="-1">
<%=mac%>
</FONT></TD>
<TD><FONT FACE="Arial, Helvetica" SIZE="1">Data Emiss&atilde;o</FONT><BR><FONT FACE="Arial, Helvetica" SIZE="-1"><%=datadoc%></FONT></TD>
<TD ALIGN=RIGHT><FONT FACE="Arial, Helvetica" SIZE="1">Vencimento</FONT><BR>
<FONT FACE="Arial, Helvetica" size="-1">
<B><%=vencimento%></B>
</FONT></TD>
</TR>
<TR>
<TD><FONT FACE="Arial, Helvetica" SIZE="1">Sacado</FONT><BR><FONT FACE="Arial, Helvetica" SIZE="-1"><b><%=sacado%></b></FONT></TD>
<TD><FONT FACE="Arial, Helvetica" SIZE="1">Nosso N�mero</FONT><BR><FONT FACE="Arial, Helvetica" SIZE="-1">
<b><%=mcn%>-<%=diginumero%></b>
</FONT></TD>
<TD><FONT FACE="Arial, Helvetica" SIZE="1">N�m Documento</FONT><BR><FONT FACE="Arial, Helvetica" SIZE="-1"><%=numdoc%></FONT></TD>
<TD ALIGN=RIGHT><FONT FACE="Arial, Helvetica" SIZE="1">Valor do Documento</FONT><BR>
<FONT FACE="Arial, Helvetica" size="2"><B><%=valor%></B></FONT></TD>
</TR>
<TR>
<TD COLSPAN=4 valign=top align=left>
<FONT FACE="Arial, Helvetica" SIZE="1">Observa��es:</FONT>
<br>
<FONT FACE="Arial, Helvetica" SIZE="2"><B><%=titboleto%></B></FONT>
<BR>
<FONT FACE="Arial, Helvetica" SIZE="1">
<b><%=obs1%><br>
<%=obs2%><br>
<%=obs3%><br>
<%=obs4%><br>
<%=obs5%><br>
<%=obs6%><br>
<%=obs7%></b>
</FONT>
</TD>
</TR>
</TABLE>
<TABLE WIDTH="<%=tamanho%>" CELLSPACING=0 CELLPADDING=0 BORDER=0>
<TR>
<TD align=right>
<FONT SIZE=-2>Autentica&ccedil;&atilde;o Mec&acirc;nica</FONT>
<BR>
<br>
</TD>
</TR>
</TABLE>

<img src="img/corte.gif" border=0 width="<%=tamanho%>">

<TABLE WIDTH="<%=tamanho%>" CELLSPACING=0 CELLPADDING=0 BORDER=0>
<TR>
<TD VALIGN=BOTTOM WIDTH=225><IMG SRC="<%=imgbanco%>"></TD>
<TD VALIGN=BOTTOM ALIGN=RIGHT WIDTH=445><TT><FONT FACE="LucidaTypewriter, Courier"><B><%=ncodbarra%></B></TT></FONT></TD>
</TR>
</TABLE>

<TABLE WIDTH="<%=tamanho%>" BORDER=1 CELLSPACING=0 CELLPADDING=1>
<TR>
<TD COLSPAN=5 WIDTH=500>
<FONT FACE="Arial, Helvetica" SIZE="1">
Local de Pagamento
</FONT>
<BR>
<FONT FACE="Arial, Helvetica" SIZE="-1">
AT� O VENCIMENTO PAG�VEL EM QUALQUER BANCO
</FONT>
</TD>
<TD WIDTH=170 ALIGN=RIGHT>
<FONT FACE="Arial, Helvetica" SIZE="1">
Vencimento
</FONT>
<BR>
<FONT FACE="Arial, Helvetica" size="2"><B><%=vencimento%><B></FONT></TD>
</TR>
<TR>
<TD COLSPAN=5 WIDTH=500><FONT FACE="Arial, Helvetica" SIZE="1">Cedente</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1><%=cedente2%></FONT></TD>
<TD WIDTH=170 ALIGN=RIGHT><FONT FACE="Arial, Helvetica" SIZE="1">Ag&ecirc;ncia / C&oacute;digo Cedente</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>
<%=agencia%>&nbsp;<%=conta%>-<%=digiconta%></FONT></TD>
</TR>
<TR>
<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Data Documento</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>
<%=datadoc%>
</FONT></TD>
<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">N�mero Documento</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>
<%=numdoc%></FONT></TD>
<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Tipo Docu.</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>
<%=especiedoc%>
</FONT></TD>
<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Aceite</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>
<%=aceite%>
</FONT></TD>
<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Data Processamento</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>
<%=datadoc%>
</FONT></TD>
<TD WIDTH=170 ALIGN=RIGHT valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Nosso Numero</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>
<%=mcn%>-<%=diginumero%>
</TD>
</TR>
<TR>
<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Uso Banco</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1><br></FONT></TD>
<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Carteira</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1><%=carteira%></FONT></TD>

<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Especie</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>
<%=especie%></FONT></TD>
<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Quantidade</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>&nbsp;</FONT></TD>
<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Valor</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>&nbsp;</FONT></TD>
<TD WIDTH=170 ALIGN=RIGHT valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Valor do Documento</FONT><BR>
<FONT FACE="Arial, Helvetica" size="2"><B><%=valor%><B></TD>
</TR>
<TR>
<TH COLSPAN=5 ROWSPAN=4 valign=top><P ALIGN=LEFT><FONT FACE="Arial, Helvetica" SIZE="1">Instru&ccedil;&otilde;es</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=2>
<%=instru1%>
<br>
<%=instru2%>
<br>
<%=instru3%>
</FONT></P></TH>
<TD WIDTH=170><FONT FACE="Arial, Helvetica" SIZE="1">(+) Outros Acr&eacute;cimos</FONT></TD>
</TR>
<TR>
<TD WIDTH=170><FONT FACE="Arial, Helvetica" SIZE="1">(-) Descontos/Abatimento</FONT></TD>
</TR>
<TR>
<TD WIDTH=170><FONT FACE="Arial, Helvetica" SIZE="1">(+) Mora/Multa</FONT></TD>
</TR>
<TR>
<TD WIDTH=170><FONT FACE="Arial, Helvetica" SIZE="1">(=) Valor Cobrado</FONT><BR>&nbsp;</TD>
</TR>
<TR>
<TD COLSPAN=6 valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Sacado</FONT><FONT FACE="Arial, Helvetica" SIZE="-1">
<BR>
<%=sacado%>
<BR>
<%=endsacado%>
<BR>
</FONT><FONT FACE="Arial, Helvetica" SIZE="1">Sacador/Cedente</FONT></TD>
</TR>
</TABLE>

<TABLE WIDTH="<%=tamnaho%>" CELLSPACING=0 CELLPADDING=0 BORDER=0>
<TR>
<TD width="<%=tamanho%>" align=right>
<FONT SIZE=-2>Autentica&ccedil;&atilde;o Mec&acirc;nica</FONT> / <FONT FACE="Arial, Helvetica" SIZE="1"><B>FICHA DE COMPENSA&Ccedil;&Atilde;O</B></FONT>
<br>
</td>
</tr>
<tr>
<td width="<%=tamanho%>" align=left>
<IMG SRC="geracodb.asp?valor=<%=gifbarra%>">
</TD>
</TR>
</TABLE>
<br>
<img src="img/corte.gif" border=0 width="<%=tamanho%>">
</center>
</BODY>
</HTML>
<%
end function
%>
<!--#include virtual="/funcao/close_conex.asp"-->
