<!--#include virtual="/scripts/wbrseguros.asp"-->
<!--#include virtual="/funcao/formatar_data.asp"-->
<!--#include virtual="/funcao/print.asp"-->
<%
num_apolice  = request("num_apolice")
cod_ramo     = request("cod_ramo")
endosso      = request("endosso")
cod_segurado = request("cod_segurado")
num_parcela  = request("num_parcela")
const tamanho=650
public tamanhodoc, nubanco, carteira, agencia, conta, digiconta, banco, imgbanco, bancono, digiagencia, diginumero
public gifbarra, ncodbarra, mcn, mac
sql = "cobranca_sps " & num_apolice & "," & endosso & "," & cod_ramo & "," & num_parcela
set rs = conex_seguros.execute(sql)
nossonumero     = left(right(rs("nosso_nro"),12),11)
nossonumero_v   = formatnumber(nossonumero,0) & "-" & right(rs("nosso_nro"),1)
dt_vencimento   = formatar_data(rs("dt_vencimento"))
dt_emissao      = formatar_data(rs("dt_emissao"))
dt_processamento= formatar_data(rs("dt_processamento"))
valor           = rs("val_prestacao")
cod_convenio    = rs("cod_convenio")
sql = "convenio_sps " & cod_convenio
set rs = conex_seguros.execute(sql)
tamanhodoc=11
nubanco="0019"
cedente  = rs("nome")
agencia_v = trim(rs("agencia"))
agencia  = trim(left(rs("agencia"),4))
carteira = trim(left(rs("carteira"),2))
conta    = left(right(trim(rs("conta_corrente")),9),8)
conta_v  = left(right(trim(rs("conta_corrente")),9),8) & "-" & right(trim(rs("conta_corrente")),1)
banco="Banco do Brasil"
imgbanco="img/bb2.gif"
bancono="0019"
digiagencia=dac1129bb(agencia)
digiconta=dac1129bb(conta)
sql = "cadastro_segurado_sps " & cod_segurado

set rs = conex_seguros.execute(sql)
sacado=rs("nom_segurado")
endsacado=rs("end_segurado")
bairro=rs("nom_bairro")
cidade=rs("nom_cidade")
estado=rs("cod_uf")
cep=rs("num_cep")
sacado=ucase(sacado)
endsacado=ucase(endsacado)
if bairro<>"" then endsacado=endsacado & " - " & ucase(bairro)
if cidade<>"" then endsacado=endsacado & " - " & ucase(cidade)
if estado<>"" then endsacado=endsacado & " - " & ucase(estado)
if cep<>"" then endsacado=endsacado & " - CEP: " & ucase(cep)
valor = replace(valor,",",".")
if not isnumeric(valor) then
   call erro("Valor inv�lido!")
   response.end
end if
if valor > 999999999999 then
   call erro("Valor do boleto acima de R$ 999.999.999.999,00")
   response.end
end if
if valor < 5 then
   call erro("Valor do boleto abaixo de R$ 5,00")
   response.end
end if
For a = 1 To tamanhodoc - Len(Trim(nossonumero))
   nossonumero="0" & nossonumero
Next
diginumero=dac1129bb(nossonumero)
numdoc=cod_ramo & num_apolice
valor=replace(valor,".",",")
if not isnumeric(valor)then
   call erro("Valor indefinido!")
   response.end
end if
valor=formatnumber(valor,2)
dvence=day(date+5)
mvence=month(date+5)
avence=year(date+5)
if dvence<10 then dvence="0" & dvence
if mvence<10 then mvence="0" & mvence
if request("ndoc")="" then
  if not isdate(dt_vencimento) then
     call erro("Data Inv�lida!")
     response.end
  end if
  if date>cdate(dt_vencimento) then
     call erro("Data j� vencida!")
     response.end
  end if
end if
especiedoc="DP"
aceite="N"
especie="R$"
titboleto="Informa��es Adicionais"
obs1 = "Em cumprimento �s normas da SUSEP, informamos que:"
obs2 = "* O n�o pagamento da primeira parcela implicar� no cancelamento da ap�lice."
obs3 = "* O n�o pagamento das demais parcelas implicar� no cancelamento da ap�lice, " &_
"observando o artigo primeiro da referida circular, "  
obs4="o qual faculta ao segurado o direito de restabelecer a cobertura da ap�lice."
'obs5="Observa��o da sexta linha."
'obs6="Observa��o da sexta linha."
'cedente="Cia de Seguros Alian�a do Brasil"
cedente3=cedente
cedente2=cedente & " <br> CGC: 28.196.889/0001-43"
endereco="Senador Dantas 105 - 29� andar - Centro<br>Rio de Janeiro - RJ - 20031-201<br>Tel: (21) 510-7000"
instru1="SR. CAIXA, N�O RECEBA AP�S 15 DIAS DO VENCIMENTO."
instru2="PAG�VEL PREFERENCIALMENTE NO BANCO DO BRASIL."
'instru3="N�O COBRAR JUROS DE MORA"
imgcedente="img/brasil.gif"

' fun��o que gera o c�digo do boleto, segundo os dados definidos acima.
call faz001()

' fun��o que salva os dados do boleto gerado no banco de dados.
'call salva()

' fun��o que gera o boleto na tela do usu�rio.
call impboleto()

response.end

'****************
function faz001()
'****************
tudo=bancono & nossonumero & agencia & conta & carteira
parte1=mid(tudo,1,5)
parte2=mid(tudo,6,4)

parte3=mid(tudo,10,5)
parte4=mid(tudo,15,5)

parte5=mid(tudo,20,5)
parte6=mid(tudo,25,5)

showvalor=replace(valor,",","")
showvalor=replace(showvalor,".","")
enchevalor=trim(showvalor)
for yy=1 to 14-len(showvalor)
    enchevalor="0" & enchevalor
next
tdv=bancono & enchevalor & nossonumero & agencia & conta & carteira
dv=dac1129bb(tdv)
dv1=dac10(parte1 & parte2)
dv2=dac10(parte3 & parte4)
dv3=dac10(parte5 & parte6)
'incluindo o digito verificador === Alessandro Ricardo Lima em 16/09/1999
tdv = left(tdv,4) & dv & mid(tdv,5,Len(tdv)-4)
'Response.Write "tdv = " & tdv & ", len =" & len(tdv) & "<br>"
'Response.Write "bancono = " & bancono & ", len =" & len(bancono) & "<br>"
'Response.Write "dv = " & dv & ", len =" & len(dv) & "<br>"
'Response.Write "enchevalor = " & enchevalor & ", len =" & len(enchevalor) & "<br>"
'Response.Write "nossonumero = " & nossonumero & ", len =" & len(nossonumero) & "<br>"
'Response.Write "agencia = " & agencia & ", len =" & len(agencia) & "<br>"
'Response.Write "conta = " & conta & ", len =" & len(conta) & "<br>"
'Response.Write "carteira = " & carteira & ", len =" & len(carteira) & "<br>"
'montagem do n�mero do c�digo de barra
ncodbarra=parte1 & "." & parte2 & dv1 &" " & parte3 & "." &  parte4 & dv2 & " " & parte5 & "." & parte6 & dv3 & "  " & dv & "  " & showvalor
gifbarra=tdv
'mcn=formatnumber(nossonumero,0)
'mac= agencia & "-" & digiagencia & " / " & conta & "-" & digiconta
mac = agencia_v & " / " & conta_v
end function

'***************
function salva()
'***************
'grava o titulo no banco de dados
salvad=carteira & " / " & nossonumero & "-" & diginumero
sql2="SELECT * FROM TITULO"
Set Conn = Server.CreateObject("ADODB.Connection")
Set RS2 = Server.CreateObject("ADODB.RecordSet")
Conn.Open "boldemo"
RS2.Open sql2, Conn, adOpenKeyset, adLockOptimistic
rs2.addnew
rs2.update "cliente", codcedente
rs2.update "nomesacado",sacado
rs2.update "data",date
rs2.update "valor",valor
rs2.update "vencimento", vencimento

rs2.update "nossonum",salvad
rs2.update "docno", numdoc
rs2.update "codigotit", ncodbarra
rs2.update "codigobarra", gifbarra
rs2.update "de","Pagamento de teste gerado na WEB"
rs2.update "ip", eip
rs2.update "pagou",false
rs2.close
conn.close
end function

'************************
Function dacmod1129(arg1)
'************************
Local1 = Len(Trim(arg1))
local4 = 0
Dim local5(8)
For xx = 1 To 8
  local5(xx) = xx + 1
Next
local6 = 1
For Local3 = Local1 To 1 Step -1
    sz=Mid(arg1, Local3, 1) * local5(local6)
    local4 = local4 + sz
    If local6 = 8 Then local6=0
    local6 = local6 + 1
Next
ss = local4 Mod 11
ss2=ss
if ss2=0 or ss2=1 then
   local2=1
else
   local2=11-ss2
end if
dacmod1129 = Right(trim(local2), 1)
End Function

'*******************
Function DAC10(arg1)
'*******************
aa = Len(Trim(arg1)) / 2
If Int(aa) = aa Then
   arg1 = "0" + arg1
End If
ninicio = Len(Trim(arg1)) + 1
ntotal = 0
If (ninicio < 2) Then
   ninicio = 2
End If
ntotal=0
ccpoaux = "0" + Trim(arg1)
For x = ninicio To 1 Step -2
   cnumero = Mid(ccpoaux, x, 1)
   ntotal = ntotal + InStr("516273849", cnumero)
   ntotal = ntotal + Mid(ccpoaux, x - 1, 1)
next
DAC10 = InStr("987654321", Right(ntotal, 1))
End Function

'************************
Function dac1129bb(arg1)
'************************
Local1 = Len(Trim(arg1))
local4 = 0
Dim local5(8)
For xx = 1 To 8
  local5(xx) = xx + 1
Next
local6 = 1
For Local3 = Local1 To 1 Step -1
    sz=Mid(arg1, Local3, 1) * (local5(local6))
    local4 = local4 + sz
    If local6 = 8 Then local6=0
    local6 = local6 + 1
Next
ss = local4 Mod 11
ss2=ss
if ss2=0 or ss2=1 then
   local2=1
else
   local2=11-ss2
end if
dac1129bb = Right(trim(local2), 1)
End Function

'*****************
function seguranca
'*****************
if mid(eip,1,12)<>"200.246.122." then
   response.redirect("http://www.node1.com.br")
   response.end
end if
end function

'*******************
function erro(texto)
'*******************
%>
<HTML><HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<META NAME="Author" CONTENT="Node 1 Internet"> 
<TITLE>2� Via de Pagamento<%=cedente%> </TITLE></HEAD>
<font face="Verdana, Arial" size=+1 color="#005a9f">
<b>2� Via de Pagamento</b>
</font color>
<br>
<font face="Arial" size=3 color=red>
<br>
<b>Desculpe, ocorreu o seguinte erro:</b>
<p>
<b><%=texto%></b>
<p>
<center>
<a href="javascript:window.close()">Fechar</a>
</center>
<p>
<br>
<br>
<center>
<font face="Arial" size=1 color=black>
<a href="http://www.node1.com.br/">Node 1 Internet</a> - 1999 - Todos os direitos reservados.
</center>
</font>
</td>
</table>
<br>
</html>
<%
end function

'*******************
function impboleto()
'*******************
%>
<HTML>
<HEAD>
<TITLE>Boleto Banc�rio <%=cedente%></TITLE>
</HEAD>
<BODY BGCOLOR="#FFFFFF">
<CENTER>

<TABLE WIDTH="<%=tamanho%>" CELLSPACING=0 CELLPADDING=0 BORDER=0>
<TR>
<TD WIDTH=225><!--IMG SRC="<%=imgcedente%>"--><br><br></TD>
<TD WIDTH=425 ALIGN=RIGHT><FONT FACE="Arial, Helvetica" SIZE="-1">
<%=cedente%><BR><%=endereco%></FONT></TD>
</TR>
<TR>
<TD VALIGN=BOTTOM WIDTH=225><IMG SRC="<%=imgbanco%>"></TD>
<TD width="425" ALIGN=RIGHT VALIGN=BOTTOM><FONT FACE="Arial, Helvetica" SIZE="2"><B>RECIBO DO SACADO</B></FONT></TD>
</TR>
</TABLE>

<TABLE WIDTH="<%=tamanho%>" BORDER=1 CELLSPACING=0 CELLPADDING=1>
<TR>
<TD><FONT FACE="Arial, Helvetica" SIZE="1">Cedente</FONT><BR>
<FONT FACE="Arial, Helvetica" SIZE="-1">
<%=cedente3%>
</FONT></TD>
<TD><FONT FACE="Arial, Helvetica" SIZE="1">Ag&ecirc;ncia / C&oacute;digo Cedente</FONT><BR><FONT FACE="Arial, Helvetica" SIZE="-1">
<%=mac%>
</FONT></TD>
<TD><FONT FACE="Arial, Helvetica" SIZE="1">Data Emiss&atilde;o</FONT><BR><FONT FACE="Arial, Helvetica" SIZE="-1"><%=dt_emissao%></FONT></TD>
<TD ALIGN=RIGHT><FONT FACE="Arial, Helvetica" SIZE="1">Vencimento</FONT><BR>
<FONT FACE="Arial, Helvetica" size="-1">
<B><%=dt_vencimento%></B>
</FONT></TD>
</TR>
<TR>
<TD><FONT FACE="Arial, Helvetica" SIZE="1">Sacado</FONT><BR><FONT FACE="Arial, Helvetica" SIZE="-1"><b><%=sacado%></b></FONT></TD>
<TD><FONT FACE="Arial, Helvetica" SIZE="1">Nosso N�mero</FONT><BR><FONT FACE="Arial, Helvetica" SIZE="-1">
<b><%=nossonumero_v%></b>
</FONT></TD>
<TD><FONT FACE="Arial, Helvetica" SIZE="1">N�m Documento</FONT><BR><FONT FACE="Arial, Helvetica" SIZE="-1"><%=numdoc%></FONT></TD>
<TD ALIGN=RIGHT><FONT FACE="Arial, Helvetica" SIZE="1">Valor do Documento</FONT><BR>
<FONT FACE="Arial, Helvetica" size="2"><B><%=valor%></B></FONT></TD>
</TR>
<TR>
<TD COLSPAN=4 valign=top align=left>
<FONT FACE="Arial, Helvetica" SIZE="1">Observa��es:</FONT>
<br>
<FONT FACE="Arial, Helvetica" SIZE="2"><B><%=titboleto%></B></FONT>
<BR>
<FONT FACE="Arial, Helvetica" SIZE="1">
<b><%=obs1%><br>
<%=obs2%><br>
<%=obs3%><br>
<%=obs4%><br>
<%=obs5%><br>
<%=obs6%><br>
<%=obs7%></b>
</FONT>
</TD>
</TR>
</TABLE>
<TABLE WIDTH="<%=tamanho%>" CELLSPACING=0 CELLPADDING=0 BORDER=0>
<TR>
<TD align=right>
<FONT SIZE=-2>Autentica&ccedil;&atilde;o Mec&acirc;nica</FONT>
<BR>
<br>
</TD>
</TR>
</TABLE>

<img src="img/corte.gif" border=0 width="<%=tamanho%>">

<TABLE WIDTH="<%=tamanho%>" CELLSPACING=0 CELLPADDING=0 BORDER=0>
<TR>
<TD VALIGN=BOTTOM WIDTH=225><IMG SRC="<%=imgbanco%>"></TD>
<TD VALIGN=BOTTOM ALIGN=RIGHT WIDTH=445><TT><FONT FACE="LucidaTypewriter, Courier"><B><%=ncodbarra%></B></TT></FONT></TD>
</TR>
</TABLE>

<TABLE WIDTH="<%=tamanho%>" BORDER=1 CELLSPACING=0 CELLPADDING=1>
<TR>
<TD COLSPAN=5 WIDTH=500>
<FONT FACE="Arial, Helvetica" SIZE="1">
Local de Pagamento
</FONT>
<BR>
<FONT FACE="Arial, Helvetica" SIZE="-1">
AT� O VENCIMENTO PAG�VEL EM QUALQUER BANCO
</FONT>
</TD>
<TD WIDTH=170 ALIGN=RIGHT>
<FONT FACE="Arial, Helvetica" SIZE="1">
Vencimento
</FONT>
<BR>
<FONT FACE="Arial, Helvetica" size="2"><B><%=dt_vencimento%><B></FONT></TD>
</TR>
<TR>
<TD COLSPAN=5 WIDTH=500><FONT FACE="Arial, Helvetica" SIZE="1">Cedente</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1><%=cedente2%></FONT></TD>
<TD WIDTH=170 ALIGN=RIGHT><FONT FACE="Arial, Helvetica" SIZE="1">Ag&ecirc;ncia / C&oacute;digo Cedente</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>
<%=mac%></FONT></TD>
</TR>
<TR>
<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Data Documento</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>
<%=dt_emissao%>
</FONT></TD>
<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">N�mero Documento</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>
<%=numdoc%></FONT></TD>
<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Tipo Docu.</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>
<%=especiedoc%>
</FONT></TD>
<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Aceite</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>
<%=aceite%>
</FONT></TD>
<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Data Processamento</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>
<%=dt_processamento%>
</FONT></TD>
<TD WIDTH=170 ALIGN=RIGHT valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Nosso Numero</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>
<%=nossonumero_v%>
</TD>
</TR>
<TR>
<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Uso Banco</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1><br></FONT></TD>
<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Carteira</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1><%=carteira%></FONT></TD>

<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Especie</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>
<%=especie%></FONT></TD>
<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Quantidade</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>&nbsp;</FONT></TD>
<TD valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Valor</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=-1>&nbsp;</FONT></TD>
<TD WIDTH=170 ALIGN=RIGHT valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Valor do Documento</FONT><BR>
<FONT FACE="Arial, Helvetica" size="2"><B><%=valor%><B></TD>
</TR>
<TR>
<TH COLSPAN=5 ROWSPAN=4 valign=top><P ALIGN=LEFT><FONT FACE="Arial, Helvetica" SIZE="1">Instru&ccedil;&otilde;es</FONT><BR><FONT FACE="Arial, Helvetica" SIZE=2>
<%=instru1%>
<br>
<%=instru2%>
<br>
<%=instru3%>
</FONT></P></TH>
<TD WIDTH=170><FONT FACE="Arial, Helvetica" SIZE="1">(+) Outros Acr&eacute;cimos</FONT></TD>
</TR>
<TR>
<TD WIDTH=170><FONT FACE="Arial, Helvetica" SIZE="1">(-) Descontos/Abatimento</FONT></TD>
</TR>
<TR>
<TD WIDTH=170><FONT FACE="Arial, Helvetica" SIZE="1">(+) Mora/Multa</FONT></TD>
</TR>
<TR>
<TD WIDTH=170><FONT FACE="Arial, Helvetica" SIZE="1">(=) Valor Cobrado</FONT><BR>&nbsp;</TD>
</TR>
<TR>
<TD COLSPAN=6 valign=top><FONT FACE="Arial, Helvetica" SIZE="1">Sacado</FONT><FONT FACE="Arial, Helvetica" SIZE="-1">
<BR>
<%=sacado%>
<BR>
<%=endsacado%>
<BR>
</FONT><FONT FACE="Arial, Helvetica" SIZE="1">Sacador/Cedente</FONT></TD>
</TR>
</TABLE>

<TABLE WIDTH="<%=tamnaho%>" CELLSPACING=0 CELLPADDING=0 BORDER=0>
<TR>
<TD width="<%=tamanho%>" align=right>
<FONT SIZE=-2>Autentica&ccedil;&atilde;o Mec&acirc;nica</FONT> / <FONT FACE="Arial, Helvetica" SIZE="1"><B>FICHA DE COMPENSA&Ccedil;&Atilde;O</B></FONT>
<br>
</td>
</tr>
<tr>
<td width="<%=tamanho%>" align=left>
<IMG SRC="geracodb.asp?valor=<%=gifbarra%>">
</TD>
</TR>
</TABLE>
<br>
<img src="img/corte.gif" border=0 width="<%=tamanho%>">
</center>
<form name="f" method="post" action="default.asp">
<table width="100%">
<tr>
    <td bgcolor="white" align="center">
         <input type="button" value="Imprimir" onclick="window.print()" >
         <input type="button" value="Fechar" onclick="fechar()" >
    </td>
</tr>
</table>
</form>
</BODY>
</HTML>
<SCRIPT LANGUAGE="javascript">
   function fechar()
   { parent.window.close() }
</SCRIPT>
<%
end function
%>
<!--#include virtual="/funcao/close_conex.asp"-->
