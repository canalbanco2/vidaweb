﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Pesquisa_cotacao_resultado.aspx.vb" Inherits="SEGW0203.pesquisa_cotacao_resultado" EnableViewState ="true"  EnableEventValidation="false" ValidateRequest="false" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Resultado da pesquisa</title>
    <link href="css/global.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript" src="js/principal.js"></script>

<script type="text/javascript">
    function AtualizarGrid()
    {
        document.getElementById('btnAtualizarGrid').click(); 
    }
    
    function launchModal()
    {
        //var win = window.open("upload.aspx","Upload de Arquivos","height=480,width=680,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,modal=yes");
        var win = window.open("upload.aspx","Upload_de_Arquivos","width=680, height=380");
        var timer = setInterval(function () {
            if (win.closed) {
                clearInterval(timer);
                document.getElementById('btnAtualizarGrid').click();
            }
        }, 1000);
    }
</script> 
   
</head>
<body>
    <form id="form1" action = "Pesquisa_cotacao_resultado.aspx" method = "post" runat="server">
        <asp:Panel runat="server" ID="pnltopo">
            <table background="img/imgbannertopofundo.png" border="0" cellpadding="0" cellspacing="0"
                width="100%">
                <tr>
                    <td align="right" height="60" style="background-position: left top; background-image: url(img/imgBannerTopo.gif);
                        background-repeat: no-repeat; width: 125px;" valign="top">
                    </td>
                </tr>
            </table>
            <table background="img/imgFundoMenuTopo.gif" border="0" cellpadding="0" cellspacing="0"
                height="26" width="100%">
                <tr>
                    <td>
                        <a class="TituloMenuTopo" href="JavaScript:window.parent.close();">&nbsp; &nbsp; &nbsp;Sair
                            &nbsp; &nbsp;&nbsp;</a> &nbsp;</td>
                    <td class="DadosSistemaUsuario" style="width: 250px; text-align: right">
                        <asp:Label ID="lblDataCorrente" runat="server"></asp:Label>&nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>

        <div id="corpo">
            <asp:Table ID="TableCotacao" runat="server" >
            <asp:TableRow >
                <asp:TableCell >
                    <asp:GridView ID="gridResultadoPesquisa"  PageSize = "15" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                    DataKeyNames="cotacao_web_controle_id" OnRowDataBound="gridResultadoPesquisa_RowDataBound" 
                    OnPageIndexChanging="gridResultadoPesquisa_PageIndexChanging" 
                    Caption="Cotações" CssClass = "corpo" >
                        <Columns>
                    
                        <asp:TemplateField HeaderText="Seleção" ItemStyle-Width="60px">
                            <headerstyle HorizontalAlign="Center" ForeColor="white" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:RadioButton ID="selecionaCotacao" GroupName="grpCotacao" runat="server" AutoPostBack="true"  
                                OnCheckedChanged="rdbSelecionaCotacao_OnCheckedChanged" /> 
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField AccessibleHeaderText="" DataField="cotacao_web_id" HeaderText=""  ItemStyle-HorizontalAlign = "Right" Visible="false" />
                        
                        <asp:BoundField AccessibleHeaderText="Nº do protocolo" DataField="protocolo_id" HeaderText="Nº do protocolo"  ItemStyle-HorizontalAlign = "Left" />
                        <asp:BoundField AccessibleHeaderText="Produto" DataField="produto_id" HeaderText="Produto" ItemStyle-HorizontalAlign = "Center" />
                        <asp:BoundField AccessibleHeaderText="Ramo" DataField="ramo_id" HeaderText="Ramo" ItemStyle-HorizontalAlign = "Center" />
                        <asp:BoundField AccessibleHeaderText="Segurado" DataField="segurado" HeaderText="Segurado" ItemStyle-HorizontalAlign = "Left" />
                        <asp:BoundField AccessibleHeaderText="Status" DataField="status_id" HeaderText="Status" ItemStyle-HorizontalAlign = "Left" />
                        <asp:BoundField AccessibleHeaderText="Status" DataField="descricao_status" HeaderText="Status da cotação" ItemStyle-HorizontalAlign = "Center" />
                        <asp:BoundField AccessibleHeaderText="Inicio da vigência" DataField="dt_vigencia_endosso" ItemStyle-HorizontalAlign = "Center"  
                            HeaderText="Inicio da vigência" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False"/>
                        <asp:BoundField AccessibleHeaderText="Data recebida unidade técnica" DataField="dt_recebida_ut" ItemStyle-HorizontalAlign = "Center" 
                            HeaderText="Data recebida unidade técnica" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False"/>
                        <asp:BoundField AccessibleHeaderText="Dias pendentes" DataField="dias_pendentes" HeaderText="Dias pendentes" ItemStyle-HorizontalAlign = "Center" />
                        
                       <asp:BoundField AccessibleHeaderText="Minuta" DataField="minuta" HeaderText="Minuta" ItemStyle-HorizontalAlign = "Left" />
                       <asp:TemplateField HeaderText="Minuta" ItemStyle-HorizontalAlign="Center">                                         
                            <ItemTemplate>
                                <asp:HyperLink ID="hplVisualiza_Minuta" runat="server"></asp:HyperLink>    
                            </ItemTemplate>
                       </asp:TemplateField>
                                               
                        <asp:BoundField AccessibleHeaderText="Login minuta" DataField="login_minuta" HeaderText="Login minuta" ItemStyle-HorizontalAlign = "Center" />
                        
                        <asp:BoundField AccessibleHeaderText="Minuta assinada" DataField="minuta_assinada" HeaderText="Minuta assinada" ItemStyle-HorizontalAlign = "Left" />
                        <asp:TemplateField HeaderText="Minuta Assinada" ItemStyle-HorizontalAlign="Center">                                         
                            <ItemTemplate>
                                <asp:HyperLink ID="hplVisualiza_Minuta_Assinada" runat="server"></asp:HyperLink>    
                            </ItemTemplate>
                       </asp:TemplateField>
                                            
                        <asp:BoundField AccessibleHeaderText="Login emissão" DataField="login_emissao" HeaderText="Login emissão" ItemStyle-HorizontalAlign = "Center" />
                        <asp:BoundField AccessibleHeaderText="Nº do endosso" DataField="endosso_id" HeaderText="Nº do endosso" ItemStyle-HorizontalAlign = "Center" />
                        
                        <asp:BoundField AccessibleHeaderText="Endosso" DataField="endosso" HeaderText="Endosso" ItemStyle-HorizontalAlign = "Left" />
                        <asp:TemplateField HeaderText="Endosso" ItemStyle-HorizontalAlign="Center">                                         
                            <ItemTemplate>
                                <asp:HyperLink ID="hplVisualiza_Endosso" runat="server"></asp:HyperLink>    
                            </ItemTemplate>
                       </asp:TemplateField>

                        <asp:BoundField AccessibleHeaderText="Boleto" DataField="boleto_1" HeaderText="Boleto" ItemStyle-HorizontalAlign = "Left" />
                        <asp:BoundField AccessibleHeaderText="Boleto" DataField="boleto_2" HeaderText="Boleto" ItemStyle-HorizontalAlign = "Left" />
                        <asp:BoundField AccessibleHeaderText="Boleto" DataField="boleto_3" HeaderText="Boleto" ItemStyle-HorizontalAlign = "Left" />
                        <asp:BoundField AccessibleHeaderText="Boleto" DataField="boleto_4" HeaderText="Boleto" ItemStyle-HorizontalAlign = "Left" />
                        <asp:BoundField AccessibleHeaderText="Boleto" DataField="boleto_5" HeaderText="Boleto" ItemStyle-HorizontalAlign = "Left" />
                        <asp:BoundField AccessibleHeaderText="Boleto" DataField="boleto_6" HeaderText="Boleto" ItemStyle-HorizontalAlign = "Left" />
                        <asp:BoundField AccessibleHeaderText="Boleto" DataField="boleto_7" HeaderText="Boleto" ItemStyle-HorizontalAlign = "Left" />
                        <asp:TemplateField HeaderText="Boleto" ItemStyle-Width="140" ItemStyle-HorizontalAlign="left">                                         
                            <ItemTemplate>
                                <asp:HyperLink ID="hplVisualiza_Boleto_1" runat="server"></asp:HyperLink>    
                                <asp:HyperLink ID="hplVisualiza_Boleto_2" runat="server"></asp:HyperLink>    
                                <asp:HyperLink ID="hplVisualiza_Boleto_3" runat="server"></asp:HyperLink>    
                                <asp:HyperLink ID="hplVisualiza_Boleto_4" runat="server"></asp:HyperLink>    
                                <asp:HyperLink ID="hplVisualiza_Boleto_5" runat="server"></asp:HyperLink>    
                                <asp:HyperLink ID="hplVisualiza_Boleto_6" runat="server"></asp:HyperLink>    
                                <asp:HyperLink ID="hplVisualiza_Boleto_7" runat="server"></asp:HyperLink>    
                            </ItemTemplate>
                       </asp:TemplateField>
                        </Columns>
                        <PagerStyle CssClass="grid_pager" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            Nenhuma cotação foi encontrada para os filtros informados, favor refazer a consulta!
                        </EmptyDataTemplate>
                    </asp:GridView>
                </asp:TableCell>
            </asp:TableRow>
         </asp:Table> 
         </div>
         
         <asp:Table  ID="MenuTecnico" runat="server"> 
            <asp:TableRow>
                <asp:TableCell>

                    <span class="upload-wrapper">                    
                        <asp:FileUpload ID="fuUploadMinutaTecnico" onchange="document.getElementById('form1').submit()" CssClass="upload-file" runat="server" />
                        <asp:Button ID="btnAnexar_MinutaTecnico" runat="server" Text="Anexar minuta" CssClass="upload-buton" />
                    </span>        
                    
                </asp:TableCell>
                
                <asp:TableCell>
                    <asp:Button ID="btnExcluir_MinutaTecnico" runat="server" Text="Excluir minuta" CssClass="Botao_consulta_ut"/>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Button ID="btnEnviar_MinutaTecnico" runat="server" Text="Enviar minuta" CssClass="Botao_pesquisa_2"/>
                </asp:TableCell>
                
                <asp:TableCell>
                    <asp:Button ID="btnExigenciaTecnico" runat="server" Text="Exigência" CssClass="Botao_pesquisa_2"/>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Button ID="btnRecusarTecnico" runat="server" Text="Recusar" CssClass="Botao_pesquisa_2"/>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Button ID="btnInspecaoTecnico" runat="server" Text="Inspeção" CssClass="Botao_pesquisa_2"/>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Button ID="btnEnviar_EmissaoTecnico" runat="server" Text="Enviar p/ emissão" CssClass="Botao_pesquisa_2"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <asp:Button ID="btnConsultarTecnico" runat="server" Text="Consultar" CssClass="Botao_pesquisa_2"/>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Button ID="btnConsultarExigenciaEmissaoUT" runat="server" Text="Consultar exigência emissão UT" CssClass="Botao_consulta_ut"/>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Button ID="btnConsultarExigenciaTecnico" runat="server" Text="Consultar exigência" CssClass="Botao_pesquisa_2"/>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Button ID="btnConsultarRecusaTecnico" runat="server" Text="Consultar recusa" CssClass="Botao_pesquisa_2"/>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Button ID="BtnConsultarDocumentosTecnico" runat="server" Text="Consultar documentos" CssClass="Botao_pesquisa_2"/>
                </asp:TableCell>                 
                <asp:TableCell></asp:TableCell>                
                <asp:TableCell></asp:TableCell>                
            </asp:TableRow>
            
         </asp:Table> 

         <asp:Table  ID="MenuCorretor" runat="server"> 
            <asp:TableRow>
                <asp:TableCell>
                    <asp:Button ID="btnConsultarCorretor" runat="server" Text="Consultar" CssClass="Botao_pesquisa_2"/>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Button ID="btnConsultarRecusaCorretor" runat="server" Text="Consultar recusa" CssClass="Botao_pesquisa_2"/>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Button ID="btnConsultarExigenciaCorretor" runat="server" Text="Consultar exigência" CssClass="Botao_pesquisa_2"/>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Button ID="btnImprimirCartaBenefCorretor" runat="server" Text="Imprimir Carta Benef." CssClass="Botao_pesquisa_2" Enabled = "false"/>
                </asp:TableCell>
                
                <asp:TableCell>

                    <span class="upload-wrapper">                    
                        <asp:FileUpload ID="fuUploadMinutaCorretor" onchange="document.getElementById('form1').submit()" CssClass="upload-file" runat="server" />
                        <asp:Button ID="btnAnexar_MinutaCorretor" runat="server" Text="Anexar minuta" CssClass="upload-buton" />
                    </span>        
                    
                </asp:TableCell>
                
                <asp:TableCell>
                    <asp:Button ID="btnRecalcularCorretor" runat="server" Text="Recalcular" CssClass="Botao_pesquisa_2"/>
                </asp:TableCell>
            </asp:TableRow>
       </asp:Table>

         <asp:Table  ID="MenuEmissao" runat="server"> 
            <asp:TableRow>
                <asp:TableCell>
                    <asp:Button ID="btnConsultarEmissao" runat="server" Text="Consultar" CssClass="Botao_pesquisa_2"/>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Button ID="btnRetornarUnidadeTecnica" runat="server" Text="Retornar p/ unid. técnica" CssClass="Botao_envio_ut"/>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Button ID="btnConcluir" runat="server" Text="Concluir" CssClass="Botao_pesquisa_2"/>
                </asp:TableCell>
                
                <asp:TableCell>

                    <span class="upload-wrapper">                    
                        <%--<asp:FileUpload ID="fuUploadEndosso" onchange="document.getElementById('form1').submit()" CssClass="upload-file" runat="server" />--%>
                        <%--<asp:Button ID="btnAnexar_Endosso" runat="server" Text="Anexar endosso" CssClass="upload-buton" />--%>
                        <asp:Button ID="btnAnexar_Endosso" runat="server" Text="Anexar endosso" CssClass="Botao_pesquisa_2" />
                    </span>        
                    
                </asp:TableCell>

                <asp:TableCell>

                    <span class="upload-wrapper">                    
                        <asp:FileUpload ID="fuUploadBoleto" onchange="document.getElementById('form1').submit()" CssClass="upload-file" runat="server" />
                        <asp:Button ID="btnAnexar_Boleto" runat="server" Text="Anexar boleto" CssClass="upload-buton" />
                    </span>        
                </asp:TableCell>
            </asp:TableRow>
       </asp:Table> 
         <asp:Button ID="btnAtualizarGrid" runat="server" Text="" CssClass="upload-file" />         
    </form>
</body>
</html>
