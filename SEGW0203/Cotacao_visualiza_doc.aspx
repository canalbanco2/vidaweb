﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Cotacao_visualiza_doc.aspx.vb" Inherits="SEGW0203.Cotacao_visualiza_doc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Visualização de documentos</title>
    <link href="css/global.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript" src="js/principal.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="conteudo">
         <table border="0" style="width: 1034px; height: 650px;" align="center">
            <tr>
                <td>
                    <table border="0" style="width: 1034px; height: 610px;">
                        <tr>
                            <td colspan="3" valign="top" >
                                <asp:Panel ID="PanelImg" runat="server" Height="600px" Width="1024px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="corpo">
                                    <img id="Img" alt="" src=""  runat="server" />
                                </asp:Panel>
                                <asp:Panel ID="PanelPdf" runat="server" Height="600px" Width="1024px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="corpo">
                                    <iframe src="" id="ifrMostrarPdf" Height="590px" Width="1014px" frameborder="0"></iframe>
                                </asp:Panel>
                                <asp:Panel ID="PanelPdf2" runat="server" Height="600px" Width="1024px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="corpo">
                                    <iframe src="" id="ifrMostrarPdf2" Height="590px" Width="1014px" frameborder="0"></iframe>
                                </asp:Panel>
                                <asp:Panel ID="PanelPdf3" runat="server" Height="600px" Width="1024px" Wrap="False" BorderStyle="Inset" ScrollBars="Auto" CssClass="corpo">
                                    <iframe src="" id="ifrMostrarPdf3" Height="590px" Width="1014px" frameborder="0"></iframe><br />
                                </asp:Panel>
                            </td> 
                        </tr>
                    </table>
               </td>
            </tr>
         </table>
        </div>
        <div id="menu">    
            <asp:Panel ID="pnlOpcoesCorretor" runat="server" Height="40px" Width="1024px" >
                <table border="0" style="width: 1034px; height: 40px;" >   
                  <tr>
                    <td align="left" >
                        <asp:Button ID="btnFechar" runat="server" Text="Fechar" CssClass="Botao_pesquisa_2"/>
                    </td>
                    <td align="center" >
                        <asp:Button ID="btnExcluirMinuta" runat="server" Text="Excluir minuta" CssClass="Botao_pesquisa_2"/>
                    </td>
                    <td align="right" >
                        <asp:Button ID="btnEnviarMinuta" runat="server" Text="Enviar minuta" CssClass="Botao_pesquisa_2"/>
                    </td>
                  </tr>      
                </table> 
            </asp:Panel>    
            <asp:Panel ID="pnlOpcaoEmissao" runat="server" Height="40px" Width="1024px" >
                <table border="0" style="width: 1034px; height: 40px;" >   
                  <tr>
                    <td align="right" >
                        <asp:Button ID="btnImprimirImg" runat="server" Text="Imprimir" CssClass="Botao_pesquisa_2" OnClientClick = "return PrintPanelimg();" />
                        <asp:Button ID="btnExcluirDoc" runat="server" Text="Excluir" CssClass="Botao_pesquisa_2" />
                        <asp:Button ID="btnExcluirDoc2" runat="server" Text="Excluir Doc 2" CssClass="Botao_pesquisa_2" />
                        <asp:Button ID="btnExcluirDoc3" runat="server" Text="Excluir Doc 3" CssClass="Botao_pesquisa_2" />
                    </td>
                  </tr>      
                </table> 
            </asp:Panel>    
        </div>
    </form>
</body>
</html>
