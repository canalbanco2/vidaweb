﻿Partial Public Class Cotacao_web_consulta_exigencia_emissao
    Inherits System.Web.UI.Page
    Dim linkseguro As New Alianca.Seguranca.Web.LinkSeguro

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        linkseguro.LerUsuario( _
                    Request.Url.Segments(Request.Url.Segments.Length - 1), _
                    Request.UserHostAddress, _
                    Request.QueryString("SLinkSeguro"))

        txtMotivoExigenciaEmissao.Text = RetornaMotivoExigenciaEmissao(Request.QueryString("cotacao_web_controle_id"))

    End Sub
    Protected Function RetornaMotivoExigenciaEmissao(ByVal cotacao_web_controle_id As Integer) As String

        Dim exigencia_emissao As Data.motivo_devolucao_utDAO = New Data.motivo_devolucao_utDAO(linkseguro)

        RetornaMotivoExigenciaEmissao = exigencia_emissao.PesquisarMotivos(cotacao_web_controle_id).Tables(0).Rows(0)("descricao").ToString

    End Function

    Private Sub btnSair_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "fechajanela();", True)
    End Sub

End Class