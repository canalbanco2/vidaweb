﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Pesquisa_cotacao.aspx.vb" Inherits="SEGW0203.Pesquisa_cotacao" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Pesquisa cotação</title>
    <link href="css/global.css" type="text/css" rel="stylesheet" />

<script>    
    function MarcarTodosTecnico(){
        document.getElementById('ChkSelecionarAguardandoAnaliseTecnico').checked = document.getElementById('ChkSelecionarTodosTecnico').checked 
        document.getElementById('ChkSelecioanrCotacaoExigenciaTecnico').checked = document.getElementById('ChkSelecionarTodosTecnico').checked 
        document.getElementById('ChkSelecionarCotacaoInspecaoTecnico').checked = document.getElementById('ChkSelecionarTodosTecnico').checked 
        document.getElementById('ChkSelecionarCotacaoRecusadaTecnico').checked = document.getElementById('ChkSelecionarTodosTecnico').checked 
    }

    function MarcarTodosCorretor(){
        document.getElementById('ChkSelecionarLiberadoCorretor').checked = document.getElementById('ChkSelecionarTodosCorretor').checked 
        document.getElementById('ChkSelecioanrCotacaoExigenciaCorretor').checked = document.getElementById('ChkSelecionarTodosCorretor').checked 
        document.getElementById('ChkSelecionarCotacaoRecusadaCorretor').checked = document.getElementById('ChkSelecionarTodosCorretor').checked 
        document.getElementById('ChkSelecionarCotacaoEmitidaCorretor').checked = document.getElementById('ChkSelecionarTodosCorretor').checked 
    }

    function MarcarTodosEmissao(){
        document.getElementById('ChkSelecionarEmitidosEmissao').checked = document.getElementById('ChkSelecionarTodosEmissao').checked 
        document.getElementById('ChkSelecionarPendentesEmissao').checked = document.getElementById('ChkSelecionarTodosEmissao').checked 
    }
    
    function LimparPesquisa(){
        document.getElementById('txtFiltro').value = ""
    }

    function MarcarTodosRastreio(){
        document.getElementById('ChkSelecionarEndossoEmitidoPrazoRastreio').checked = document.getElementById('ChkSelecionarTodosRastreio').checked 
        document.getElementById('ChkSelecionarEndossoEmitidoForaPrazoRastreio').checked = document.getElementById('ChkSelecionarTodosRastreio').checked 
        document.getElementById('ChkSelecionarCotacaoExigenciaRastreio').checked = document.getElementById('ChkSelecionarTodosRastreio').checked 
        document.getElementById('ChkSelecionarEndossoSemRetornoMinutaRastreio').checked = document.getElementById('ChkSelecionarTodosRastreio').checked 
        document.getElementById('ChkSelecionarEndossoInspecaoRastreio').checked = document.getElementById('ChkSelecionarTodosRastreio').checked 
    }
    
    function calendarShown(sender, args)
    {
        sender._popupBehavior._element.style.zIndex = 99999;
    }

</script>  
</head>
<body>
    <form id="form1" runat="server" >
    <div>
     <asp:Panel runat="server" ID="pnltopo">
        <table background="img/imgbannertopofundo.png" border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
                <td align="right" height="60" style="background-position: left top; background-image: url(img/imgBannerTopo.gif);
                    background-repeat: no-repeat" valign="top">
                </td>
            </tr>
        </table>
        <table background="img/imgFundoMenuTopo.gif" border="0" cellpadding="0" cellspacing="0"
            height="26" width="100%">
            <tr>
                <td>
                    <a class="TituloMenuTopo" href="JavaScript:window.parent.close();">&nbsp; &nbsp; &nbsp;Sair
                        &nbsp; &nbsp;&nbsp;</a> &nbsp;</td>
                <td class="DadosSistemaUsuario" style="width: 250px; text-align: right">
                    <asp:Label ID="lblDataCorrente" runat="server"></asp:Label>&nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>

      <asp:Panel runat="server" ID="pnlTipoSelecaoTecnico" CssClass="painelSelecao">
        <asp:CheckBox ID="ChkSelecionarTodosTecnico" runat="server" CssClass="Check_Opcao" Text=" Selecionar tudo" AutoPostBack="false" />         
        <hr />
        <asp:CheckBox ID="ChkSelecionarAguardandoAnaliseTecnico" runat="server" CssClass="Check_Opcao" Text=" Consultar aguardando análise" AutoPostBack="false" /> <br /><br />
        <asp:CheckBox ID="ChkSelecioanrCotacaoExigenciaTecnico" runat="server" CssClass="Check_Opcao" Text=" Consultar cotação em exigência" AutoPostBack="false" /> <br /><br />
        <asp:CheckBox ID="ChkSelecionarCotacaoInspecaoTecnico" runat="server" CssClass="Check_Opcao" Text=" Consultar em inspeção" AutoPostBack="false" /> <br /><br />
        <asp:CheckBox ID="ChkSelecionarCotacaoRecusadaTecnico" runat="server" CssClass="Check_Opcao" Text=" Consultar recusadas" AutoPostBack="false" /> 
      </asp:Panel>
      <asp:Panel runat="server" ID="pnlTipoSelecaoCorretor" CssClass="painelSelecao">
        <asp:CheckBox ID="ChkSelecionarTodosCorretor" runat="server" CssClass="Check_Opcao" Text=" Selecionar tudo" AutoPostBack="false" />         
        <hr />
        <asp:CheckBox ID="ChkSelecionarLiberadoCorretor" runat="server" CssClass="Check_Opcao" Text=" Consultar liberados para corretor" AutoPostBack="false" /> <br /><br />
        <asp:CheckBox ID="ChkSelecioanrCotacaoExigenciaCorretor" runat="server" CssClass="Check_Opcao" Text=" Consultar cotação em exigência" AutoPostBack="false" /> <br /><br />
        <asp:CheckBox ID="ChkSelecionarCotacaoRecusadaCorretor" runat="server" CssClass="Check_Opcao" Text=" Consultar recusadas" AutoPostBack="false" /> <br /><br />
        <asp:CheckBox ID="ChkSelecionarCotacaoEmitidaCorretor" runat="server" CssClass="Check_Opcao" Text=" Consultar por emitidas" AutoPostBack="false" /> 
      </asp:Panel>
      <asp:Panel runat="server" ID="pnlTipoSelecaoEmissao" CssClass="painelSelecaoEmissao">
        <asp:CheckBox ID="ChkSelecionarTodosEmissao" runat="server" CssClass="Check_Opcao" Text=" Selecionar tudo" AutoPostBack="false" />         
        <hr />
        <asp:CheckBox ID="ChkSelecionarEmitidosEmissao" runat="server" CssClass="Check_Opcao" Text=" Consultar endossos emitidos" AutoPostBack="false" /> <br /><br />
        <asp:CheckBox ID="ChkSelecionarPendentesEmissao" runat="server" CssClass="Check_Opcao" Text=" Consultar endossos pendentes" AutoPostBack="false" /> <br /><br />
      </asp:Panel>

      <asp:Panel runat="server" ID="pnlTipoSelecaoRastreio" CssClass="painelSelecaoRastreio">
        <asp:CheckBox ID="ChkSelecionarTodosRastreio" runat="server" CssClass="Check_Opcao" Text=" Selecionar tudo" AutoPostBack="false" />         
        <hr />
        <asp:CheckBox ID="ChkSelecionarEndossoEmitidoPrazoRastreio" runat="server" CssClass="Check_Opcao" Text=" Consultar endossos emitidos/atendidos no prazo" AutoPostBack="false" /> <br /><br />
        <asp:CheckBox ID="ChkSelecionarEndossoEmitidoForaPrazoRastreio" runat="server" CssClass="Check_Opcao" Text=" Consultar endossos emitidos/atendidos fora do prazo" AutoPostBack="false" /> <br /><br />
        <asp:CheckBox ID="ChkSelecionarCotacaoExigenciaRastreio" runat="server" CssClass="Check_Opcao" Text=" Consultar em exigência" AutoPostBack="false" /> <br /><br />
        <asp:CheckBox ID="ChkSelecionarEndossoSemRetornoMinutaRastreio" runat="server" CssClass="Check_Opcao" Text=" Consultar endossos sem retorno de minuta" AutoPostBack="false" /> <br /><br />
        <asp:CheckBox ID="ChkSelecionarEndossoInspecaoRastreio" runat="server" CssClass="Check_Opcao" Text=" Consultar endossos em inspeção" AutoPostBack="false" /> 
      </asp:Panel>

    </div>

    <div>
      <asp:Panel runat="server" ID="pnlFiltros" CssClass="painelfiltros">  
        <table border="0">
            <tr>
                <td align="right" width="120">
                    <asp:Label ID="Label4" runat="server" Text="Consulta por CNPJ : " CssClass="Texto_Normal"></asp:Label><br /><br />
                </td>
                <td colspan="2" align="left" valign="top">
                    <asp:TextBox ID="txtCnpj" runat="server" Columns="24" MaxLength="18" />
                    <cc1:MaskedEditExtender ID="txtCnpj_mascara"
                    TargetControlID="txtCnpj" 
                    Mask="99,999,999/9999-99"
                    MaskType="None"
                    ClearMaskOnLostFocus="false"
                    PromptCharacter=" "
                    runat="server" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" />
                    <br />
                </td>
            </tr>
            <tr>
                <td align="right" width="120">
                    <asp:Label ID="Label8" runat="server" Text="Consulta por ramo :" CssClass="Texto_Normal"></asp:Label><br /><br />
                </td>
                <td colspan="2" align="left" valign="top">
                    <asp:TextBox ID="txtRamo" runat="server" Columns="18" MaxLength="02" />
                    <br />
                </td>
            </tr>
            <tr>
                <td align="right" width="120">
                    <asp:Label ID="Label5" runat="server" Text="Filtro : " CssClass="Texto_Normal"></asp:Label><br />
                </td>
                <td align="left" width="230">
                    <asp:DropDownList ID="ddlOpcaoFiltros" runat="server" onchange="javascript:LimparPesquisa();">
                        <asp:ListItem Text="Consulta por número de protocolo" Value="1"></asp:ListItem> 
                        <asp:ListItem Text="Consulta por número da apólice" Value="2"></asp:ListItem> 
                        <asp:ListItem Text="Consulta por proposta AB" Value="3"></asp:ListItem> 
                        <asp:ListItem Text="Consulta por proposta BB" Value="4"></asp:ListItem> 
                        <asp:ListItem Text="Consulta por nome" Value="5"></asp:ListItem> 
                    </asp:DropDownList>
                    <br />
                </td>
                <td align="left" width="230">
                    <asp:TextBox ID="txtFiltro" runat="server" Columns="30" />
                    <br />
                </td>
            </tr>
        </table>  
      </asp:Panel>
    </div>
    <div>
      <asp:Panel runat="server" ID="pnlPeriodo" CssClass="painelPeriodo">  
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
        </asp:ScriptManager>
        <table border="0">
            <tr>
                <td colspan="2" align="left">
                    <asp:Label ID="Label1" runat="server" Text="Período" CssClass="Cabecalho"></asp:Label><br />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="Label2" runat="server" Text="Inicial" CssClass="Texto_Normal"></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="Label3" runat="server" Text="Final" CssClass="Texto_Normal"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="periodo_inicial" runat="server" Columns="12" MaxLength="10" />
                    <asp:Image ID="imgcalendario" ImageUrl="~/img/calendaricon.png" runat="server" />
                    <cc1:CalendarExtender ID="periodo_inicial_calendario" 
                    runat="server" 
                    OnClientShown="calendarShown"                    
                    Enabled="True" 
                    Format="dd/MM/yyyy" 
                    TargetControlID="periodo_inicial"
                    PopupButtonID="imgcalendario"/>
                    <cc1:MaskedEditExtender ID="periodo_inicial_mascara"
                    TargetControlID="periodo_inicial" 
                    Mask="99/99/9999"
                    MaskType="Date" 
                    runat="server" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" />
                    <cc1:MaskedEditValidator ID="periodo_inicial_mascara_valida" 
                    runat="server" 
                    ControlExtender="periodo_inicial_mascara" 
                    MinimumValue="01/01/1910"
                    ControlToValidate="periodo_inicial" 
                    Display="Dynamic" 
                    InvalidValueMessage="*" 
                    MaximumValue="31/12/2099" ErrorMessage="periodo_inicial_mascara_valida"/>   
                </td>
                <td>
                    <asp:TextBox ID="periodo_final" runat="server" Columns="12" MaxLength="10" />
                    <asp:Image ID="imgcalendario2" ImageUrl="~/img/calendaricon.png" runat="server" />
                    <cc1:CalendarExtender ID="periodo_final_calendario" 
                    runat="server" 
                    OnClientShown="calendarShown"
                    Enabled="True" 
                    Format="dd/MM/yyyy" 
                    TargetControlID="periodo_final"
                    PopupButtonID="imgcalendario2"/>
                    <cc1:MaskedEditExtender ID="periodo_final_mascara"
                    TargetControlID="periodo_final" 
                    Mask="99/99/9999"
                    MaskType="Date" 
                    runat="server" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" />
                    <cc1:MaskedEditValidator ID="periodo_final_mascara_valida" 
                    runat="server" 
                    ControlExtender="periodo_final_mascara" 
                    MinimumValue="01/01/1910"
                    ControlToValidate="periodo_final" 
                    Display="Dynamic" 
                    InvalidValueMessage="*" 
                    MaximumValue="31/12/2099" ErrorMessage="periodo_final_mascara_valida"/>   
                </td>
            </tr>
        </table>  
      </asp:Panel>      
        <br />
        <asp:Button ID="btnconfirmar_pesquisa" runat="server" Text="Pesquisar" CssClass="Botao_pesquisa" />
    </div>
    </form>
</body>
</html>
