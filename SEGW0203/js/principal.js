﻿
function carregarMascaras(){
    //Mascaras de telefone
    if(document.getElementById('txtTelefoneComercial')!=null){MascaraTelefone(document.getElementById('txtTelefoneComercial'));}
    if(document.getElementById('txtTelefoneResidencial')!=null){MascaraTelefone(document.getElementById('txtTelefoneResidencial'));}
    if(document.getElementById('txtTelefoneCelular')!=null){MascaraTelefone(document.getElementById('txtTelefoneCelular'));}
    if(document.getElementById('txtOutrosTelefones')!=null){MascaraTelefone(document.getElementById('txtOutrosTelefones'));}
    
    //Mascara do CNPJ
    if(document.getElementById('txtCnpjCorretora')!=null){MascaraCNPJ(document.getElementById('txtCnpjCorretora'));}
}

function calendarShown(sender, args)
{
    sender._popupBehavior._element.style.zIndex = 99999;
}

function MascaraMoeda(objTextBox, SeparadorMilesimo, SeparadorDecimal, e){
    var i = j = 0;
    var len = len2 = 0;
    var listaCaracteres = '0123456789';
    var aux = aux2 = '';
    var tecla=(window.event)?event.keyCode:e.which;
	
	if (tecla == 9 || tecla == 15 || tecla == 16) return true;
	
	aux = objTextBox.value.replace(",","").replace(".","");
	
	len = objTextBox.value.length;
	for(i = 0; i < len; i++)
        if ((aux.charAt(i) != '0') && (aux.charAt(i) != SeparadorDecimal)) break;

	
	aux += String.fromCharCode(tecla);
	len = aux.length;	
	for(i = 0; i < len; i++)
        if ((aux.charAt(i) != '0') && (aux.charAt(i) != SeparadorDecimal)) break;
	for(; i < len; i++){
		if(listaCaracteres.indexOf(aux.charAt(i)) > -1) {aux2 += aux.charAt(i);}
	}
    len = aux2.length;
    if (len == 0) objTextBox.value = '0'+ SeparadorDecimal + '00';
    if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux2;
    if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux2;
    if (len > 2) {
        aux = '';
        for (j = 0, i = len - 3; i >= 0; i--) {
            if (j == 3) {
                aux += SeparadorMilesimo;
                j = 0;
            }
            aux += aux2.charAt(i);
            j++;
        }
        objTextBox.value = '';
        len2 = aux.length;
        for (i = len2 - 1; i >= 0; i--)
        objTextBox.value += aux.charAt(i);
        objTextBox.value += SeparadorDecimal + aux2.substr(len - 2, len);
    }
    return false;
}

//mascara dinamica CPF/CNPJ conforme tamanho do campo
function MascaraCnpjCpf(objTextBox, e){
    var obj = objTextBox;
    if (obj.maxLength == 14){
     return(MascaraCPF(obj));
    }
    if (obj.maxLength == 18){
     return(MascaraCNPJ(obj));
    }
}
//adiciona mascara de cnpj
function MascaraCNPJ(objTextBox){
        var cnpj = objTextBox;
        if(mascaraInteiro(cnpj)==false){
                event.returnValue = false;
        }       
        return formataCampo(cnpj, '00.000.000/0000-00', event);
}
//adiciona mascara ao CPF
function MascaraCPF(objTextBox){
        var cpf = objTextBox;
        if(mascaraInteiro(cpf)==false){
                event.returnValue = false;
        }       
        return formataCampo(cpf, '000.000.000-00', event);
}

//mascara para campos de telefone
function MascaraTelefone(objTextBox){
    var telefone = objTextBox;
    if(mascaraInteiro(telefone)==false){
        event.returnValue = false;
    }
    if(telefone.value.length == 0){
        return false;
    }
    if(telefone.value.length <= 13){
        return formataCampo(telefone, '(00)0000-0000', event);
    }else{
        return formataCampo(telefone, '(00)00000-0000', event);
    }
}

//Aceita apenas digitos
function MascaraNumeros(objTextBox){
   if(mascaraInteiro(objTextBox)==false){
        event.returnValue = false;
    }
    return formataCampo(objTextBox, '0', event);
}

//formata campo conforme a máscara passada - generico para todas as validacoes de preenchimento
function formataCampo(campo, Mascara, evento) { 
        var boleanoMascara; 

        var Digitato = evento.keyCode;
        exp = /\D/g;
        campoSoNumeros = campo.value.toString().replace( exp, "" ); 

        var posicaoCampo = 0;    
        var NovoValorCampo="";
        var TamanhoMascara = campoSoNumeros.length;; 

        if (Digitato == 8) { //backspace
        TamanhoMascara--;
        }
        
        for(i=0; i<= TamanhoMascara; i++) { 
                boleanoMascara  = ((Mascara.charAt(i) == "-") || (Mascara.charAt(i) == ".")
                                                        || (Mascara.charAt(i) == "/")) 
                boleanoMascara  = boleanoMascara || ((Mascara.charAt(i) == "(") 
                                                        || (Mascara.charAt(i) == ")") || (Mascara.charAt(i) == " ")) 
                if (boleanoMascara) { 
                        NovoValorCampo += Mascara.charAt(i); 
                          TamanhoMascara++;
                }else{ 
                        NovoValorCampo += campoSoNumeros.charAt(posicaoCampo); 
                        posicaoCampo++; 
                }              
          }      
        campo.value = NovoValorCampo.substring(0,campo.maxLength);
        return true; 
}
//valida numero inteiro com mascara
function mascaraInteiro(){
        if (event.keyCode < 48 || event.keyCode > 57){
                event.returnValue = false;
                return false;
        }
        return true;
}

//verificar uso das funções abaixo
function verificaNumero(e) {
    e.preventDefault();
    var expre = /[^0-9]/g;
    if ($(this).val().match(expre))
        false;
}

function apoliceClick(obj, index)
{
    __doPostBack('gridApolice', 'Select$'+index);
    linhaGridClick(obj);
}

function linhaGridClick(obj)
{
    $(obj).parents("table").eq(0).find(".grid_apolice_linha td").each(function () {
        $(this).css("background-color","#EFF3FB");
    });
    $(obj).find("td").each(function () {
        $(this).css("background-color","#FFA");
    });
}


function abreJanela(pagina,tamWidth,tamHeigth){
	var tamW = (screen.width/2)-200;
	var tamH = (screen.height/2)-200;
	window.open(pagina,'',"resizable=1,scrollbars=1,width="+tamWidth+",height="+tamHeigth+",top="+tamH+",left="+tamW+",toolbar=0,location=0,directories=0,status=0,menubar=0");
	return false
}

function abreJanelaCotacao(pagina,tamWidth,tamHeigth){
	var tamW = 5;
	var tamH = 5;
	window.open(pagina,'',"resizable=1,scrollbars=1,width="+tamWidth+",height="+tamHeigth+",top="+tamH+",left="+tamW+",toolbar=0,location=0,directories=0,status=0,menubar=0");
	return false
}

function fechajanela(){
        window.open('', '_self', '');
        window.close(); 
}

function PrintPanelimg() {

    var printContent = document.getElementById("PanelImg");
    var windowUrl = 'about:blank';
    var uniqueName = new Date();
    var windowName = 'Print' + uniqueName.getTime();
    var printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=0,height=0');

    printWindow.document.write(printContent.innerHTML);
    printWindow.document.close();
    printWindow.focus();
    printWindow.print();
    printWindow.close();            
   return false;
}

function ImprimeCarta(conteudo){
    var windowUrl = 'about:blank';
    var uniqueName = new Date();
    var windowName = 'Print' + uniqueName.getTime();
    var printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=0,height=0');
 
    printWindow.document.write(conteudo);
    printWindow.document.close();
    printWindow.focus();
    printWindow.print();
    printWindow.close();            
    return false;
}


//inicio: gleison.pimentel - Nova Consultoria 05/11/2016 - 19089159 - Envio de documentos de cotação de endosso web – Visão Corretor
function getMessage(){  
 
        var rowscount = document.getElementById('hdiCountGrid').value;
  
        if(rowscount == 0) {
            document.getElementById ('hdiResposta').value='No';
            var ans;
                ans=window.confirm('Não foi anexado arquivos a esta cotação, deseja continuar?');
               
                if (ans==true)
                    {
                    document.getElementById ('hdiResposta').value='Yes';
                    }
        }
        else{
            document.getElementById ('hdiResposta').value='Yes';
        }   
}
//fim: gleison.pimentel - Nova Consultoria 05/11/2016 - 19089159 - Envio de documentos de cotação de endosso web – Visão Corretor
