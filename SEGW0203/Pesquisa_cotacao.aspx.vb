﻿Partial Public Class Pesquisa_cotacao
    Inherits System.Web.UI.Page
    Dim linkSeguro As New Alianca.Seguranca.Web.LinkSeguro

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        linkSeguro.LerUsuario( _
                    Request.Url.Segments(Request.Url.Segments.Length - 1), _
                    Request.UserHostAddress, _
                    Request.QueryString("SLinkSeguro"))

        lblDataCorrente.Text = Format(Date.Now, "dddd, dd " & "DE" & " MMMM " & "DE" & " yyyy").ToLower

        If Not IsPostBack Then
            Session("perfil") = Request.QueryString("perfil")
            If Request.QueryString("usuario_id") <> 0 Then
                Session("usuario_id") = Request.QueryString("usuario_id")
            Else
                Session("usuario_id") = vbNullString
            End If
        End If

        pnlTipoSelecaoTecnico.Visible = False
        pnlTipoSelecaoCorretor.Visible = False
        pnlTipoSelecaoEmissao.Visible = False
        pnlTipoSelecaoRastreio.Visible = False

        If Session("perfil") = "T" Then
            pnlTipoSelecaoTecnico.Visible = True
            ChkSelecionarTodosTecnico.Attributes.Add("onclick", "MarcarTodosTecnico();")
        End If

        If Session("perfil") = "C" Then
            pnlTipoSelecaoCorretor.Visible = True
            ChkSelecionarTodosCorretor.Attributes.Add("onclick", "MarcarTodosCorretor();")
        End If

        If Session("perfil") = "E" Then
            pnlTipoSelecaoEmissao.Visible = True
            ChkSelecionarTodosEmissao.Attributes.Add("onclick", "MarcarTodosEmissao();")
        End If

        If Session("perfil") = "R" Then
            pnlTipoSelecaoRastreio.Visible = True
            ChkSelecionarTodosRastreio.Attributes.Add("onclick", "MarcarTodosRastreio();")
        End If

    End Sub

    Protected Sub btnconfirmar_pesquisa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnconfirmar_pesquisa.Click
        Dim sColecaoStatus As String

        Session("status") = vbNullString
        Session("data_inicial") = vbNullString
        Session("data_final") = vbNullString
        Session("cnpj") = vbNullString
        Session("ramo") = vbNullString
        Session("tp_filtro") = vbNullString
        Session("filtro") = vbNullString
        Session("prazo") = vbNullString
        Session("minuta") = vbNullString
        sColecaoStatus = ""

        If ChkSelecionarAguardandoAnaliseTecnico.Checked Then
            sColecaoStatus &= ",1,2,10"
        End If
        If ChkSelecioanrCotacaoExigenciaTecnico.Checked Then
            sColecaoStatus &= ",4"
        End If
        If ChkSelecionarCotacaoInspecaoTecnico.Checked Then
            sColecaoStatus &= ",5"
        End If
        If ChkSelecionarCotacaoRecusadaTecnico.Checked Then
            sColecaoStatus &= ",8"
        End If

        If ChkSelecionarLiberadoCorretor.Checked Then
            sColecaoStatus &= ",3"
        End If
        If ChkSelecioanrCotacaoExigenciaCorretor.Checked Then
            sColecaoStatus &= ",4"
        End If
        If ChkSelecionarCotacaoRecusadaCorretor.Checked Then
            sColecaoStatus &= ",8"
        End If
        If ChkSelecionarCotacaoEmitidaCorretor.Checked Then
            sColecaoStatus &= ",7"
        End If

        If ChkSelecionarEmitidosEmissao.Checked Then
            sColecaoStatus &= ",7"
        End If
        If ChkSelecionarPendentesEmissao.Checked Then
            sColecaoStatus &= ",6"
        End If

        If ChkSelecionarEndossoEmitidoPrazoRastreio.Checked Then
            sColecaoStatus &= ",7"
            Session("prazo") = "dentro"
        End If
        If ChkSelecionarEndossoEmitidoForaPrazoRastreio.Checked Then
            sColecaoStatus &= ",7"
            Session("prazo") = "fora"
        End If

        If ChkSelecionarCotacaoExigenciaRastreio.Checked Then
            sColecaoStatus &= ",4"
        End If
        If ChkSelecionarEndossoSemRetornoMinutaRastreio.Checked Then
            Session("minuta") = "S"
        End If
        If ChkSelecionarEndossoInspecaoRastreio.Checked Then
            sColecaoStatus &= ",5"
        End If

        If ChkSelecionarEndossoEmitidoPrazoRastreio.Checked _
        And ChkSelecionarEndossoEmitidoForaPrazoRastreio.Checked Then
            Session("prazo") = vbNullString
            If ChkSelecionarEndossoSemRetornoMinutaRastreio.Checked Then
                Session("minuta") = vbNullString
            End If
        End If


        If sColecaoStatus <> "" Then
            Session("status") = Right(sColecaoStatus, sColecaoStatus.Length - 1)
        End If

        If txtCnpj.Text <> "  .   .   /    -  " Then
            Session("cnpj") = txtCnpj.Text.Replace(",", "").Replace(".", "").Replace("/", "").Replace("-", "")

            Dim vCnpj As valida_CNPJ_CPF = New valida_CNPJ_CPF
            vCnpj.cnpj = Session("cnpj")

            If vCnpj.isCnpjValido = False Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Cnpj digitado inválido!');", True)
                Exit Sub
            End If
        End If

        If txtRamo.Text.Length > 0 Then
            Session("ramo") = txtRamo.Text
        End If

        If txtFiltro.Text.Length > 0 Then
            Session("filtro") = txtFiltro.Text
            Session("tp_filtro") = ddlOpcaoFiltros.SelectedItem.Value
        End If

        If Session("filtro") <> vbNullString Then
            If ((Session("tp_filtro") <> 5) And (Not IsNumeric(Session("filtro")))) Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Valor digitado inválido para seleção!');", True)
                Exit Sub
            End If
        End If

        If ((Session("tp_filtro") = 5) And ((periodo_inicial.Text = String.Empty) Or (periodo_final.Text = String.Empty))) Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Para pesquisar pelo nome é obrigatório definir um periodo!');", True)
            Exit Sub
        End If

        If ((periodo_inicial.Text <> String.Empty) And (periodo_final.Text <> String.Empty)) Then
            Dim dtInicial As Date
            Dim dtFinal As Date
            dtInicial = Convert.ToDateTime(periodo_inicial.Text)
            dtFinal = Convert.ToDateTime(periodo_final.Text)

            If dtInicial > dtFinal Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Periodo inválido!');", True)
                Exit Sub
            Else
                Session("data_inicial") = dtInicial
                Session("data_final") = dtFinal
            End If
        ElseIf (((periodo_inicial.Text = String.Empty) And (periodo_final.Text <> String.Empty)) Or ((periodo_inicial.Text <> String.Empty) And (periodo_final.Text = String.Empty))) Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Periodo inválido!');", True)
            Exit Sub
        End If

        If Session("status") = vbNullString And _
            Session("data_inicial") = vbNullString And _
            Session("data_final") = vbNullString And _
            Session("cnpj") = vbNullString And _
            Session("ramo") = vbNullString And _
            Session("filtro") = vbNullString And _
            Session("minuta") = vbNullString Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Favor selecionar ou preencher alguma opção de filtro!');", True)
            Exit Sub
        End If

        If Session("perfil") = "T" _
        Or Session("perfil") = "C" _
        Or Session("perfil") = "E" Then
            Response.Redirect("Pesquisa_cotacao_resultado.aspx?SLinkSeguro=" & Request("SLinkSeguro"))
        Else
            If Session("perfil") = "R" Then
                Response.Redirect("Pesquisa_cotacao_rastreio.aspx?SLinkSeguro=" & Request("SLinkSeguro"))
            End If
        End If

    End Sub
End Class