﻿Imports System.Configuration

Partial Public Class WebForm1
    Inherits System.Web.UI.Page
    Dim linkSeguro As New Alianca.Seguranca.Web.LinkSeguro


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        linkSeguro.LerUsuario( _
            Request.Url.Segments(Request.Url.Segments.Length - 1), _
            Request.UserHostAddress, _
            Request.QueryString("SLinkSeguro"))

        If Not IsPostBack Then
            Session("usuario_id") = linkSeguro.Usuario_ID
        End If

        lblDataCorrente.Text = Format(Date.Now, "dddd, dd " & "DE" & " MMMM " & "DE" & " yyyy").ToLower

    End Sub

    Protected Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click

        If ((txtapolice.Text.Length = 0 Or txtRamo.Text.Length = 0) And txtNPropostaBB.Text.Length = 0 And txtNPropostaAB.Text.Length = 0) Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "alert('Favor informar Ramo e Apólice ou Proposta BB ou Proposta AB para a busca.');", True)
            Exit Sub
        End If

        Dim apolice As New Data.apoliceDAO(linkSeguro)
        Dim dt As New DataSet

        'Limpa a grid antes de carregar 
        gridApolice.DataSource = Nothing
        gridApolice.DataBind()

        'Carrega grid com dados pesquisados
        dt = apolice.PesquisarApolice(txtapolice.Text, txtRamo.Text, txtNPropostaBB.Text, txtNPropostaAB.Text, Session("usuario_id"))
        Dim lista As New List(Of apolice)
        Dim linha As DataRow
        For Each linha In dt.Tables(0).Rows
            Dim apoliceConsulta As New apolice()
            apoliceConsulta.agencia = linha.Item("agencia")
            apoliceConsulta.apolice_id = linha.Item("apolice_id")
            apoliceConsulta.corretor = linha.Item("corretor")
            apoliceConsulta.corretorID = linha.Item("corretor_id")
            apoliceConsulta.cpf_cnpj = linha.Item("cpf_cnpj")
            apoliceConsulta.dt_fim_vigencia = linha.Item("dt_fim_vigencia")
            apoliceConsulta.dt_inicio_vigencia = linha.Item("dt_inicio_vigencia")
            apoliceConsulta.produto_id = linha.Item("produto_id")
            apoliceConsulta.proposta_bb = linha.Item("proposta_bb")
            apoliceConsulta.proposta_id = linha.Item("proposta_id")
            apoliceConsulta.ramo_id = linha.Item("ramo_id")
            apoliceConsulta.segurado = linha.Item("segurado")
            apoliceConsulta.cliente_id = linha.Item("cliente_id")
            apoliceConsulta.corretor = linha.Item("corretor")
            lista.Add(apoliceConsulta)
        Next
        gridApolice.DataSource = lista
        gridApolice.DataBind()

        'Variavel de sessao com lista de apolices
        Session.Remove("Lista")
        Session.Add("Lista", lista)

        TableApolice.Visible = True

    End Sub

    Protected Sub gridApolice_RowDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.CssClass = "grid_apolice_linha"
        End If
    End Sub


    Protected Sub rdbSelecionaApolice_OnCheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rdb As RadioButton = DirectCast(sender, RadioButton)
        Dim linha As GridViewRow = DirectCast(rdb.NamingContainer, GridViewRow)
        Dim grid As GridView = DirectCast(linha.NamingContainer, GridView)

        If rdb.Checked Then
            For Each linha2 As GridViewRow In grid.Rows
                Dim rdb2 As RadioButton = CType(linha2.Cells(1).FindControl("selecionaApolice"), CheckBox)
                rdb2.Checked = False
                linha2.CssClass = "grid_apolice_linha"
            Next
            rdb.Checked = True
            linha.CssClass = "grid_apolice_linha_sel"
            grid.SelectedIndex = linha.DataItemIndex()
            Session.Add("indice", grid.SelectedIndex)
        End If
    End Sub

    Protected Sub btnConfirmar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmar.Click
        If gridApolice.SelectedIndex > -1 Then
            Response.Redirect("Apolice_cotacao.aspx?SLinkSeguro=" & Request.QueryString("SLinkSeguro") & "&acao=inclusao")
        Else
            Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "alert('Favor selecionar uma apólice para nova cotação de endosso.');", True)
        End If
    End Sub

End Class

