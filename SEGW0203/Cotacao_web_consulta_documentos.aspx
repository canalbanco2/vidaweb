<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Cotacao_web_consulta_documentos.aspx.vb"
    Inherits="SEGW0203.Cotacao_web_consulta_documentos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Consulta de documentos</title>
    <link href="css/global.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="javascript" src="js/principal.js"></script>

</head>
<body>
    <form id="form1" method="post" runat="server">
        <asp:Panel runat="server" ID="pnltopo">
            <table background="img/imgbannertopofundo.png" border="0" cellpadding="0" cellspacing="0"
                width="100%">
                <tr>
                    <td align="right" height="60" style="background-position: left top; background-image: url(img/imgBannerTopo.gif);
                        background-repeat: no-repeat" valign="top">
                    </td>
                </tr>
            </table>
            <table background="img/imgFundoMenuTopo.gif" border="0" cellpadding="0" cellspacing="0"
                height="26" width="100%">
                <tr>
                    <td>
                        <a class="TituloMenuTopo" href="JavaScript:window.parent.close();">&nbsp; &nbsp; &nbsp;Sair&nbsp;
                            &nbsp;&nbsp;</a>&nbsp;
                    </td>
                    <td class="DadosSistemaUsuario" style="width: 250px; text-align: right">
                        <asp:Label ID="lblDataCorrente" runat="server"></asp:Label>&nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div id="corpo">
            <asp:Table ID="TableCotacao" runat="server">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Label1" runat="server" Text="Visualiza��o de documentos da cota��o" CssClass="Cabecalho"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:GridView ID="gridConsultaDocumentos" runat="server" AutoGenerateColumns="False"
                            DataKeyNames="cotacao_web_controle_id" OnRowDataBound="gridConsultaDocumentos_RowDataBound"
                            Caption="Cota��es" CssClass="corpo">
                            <Columns>
                                <asp:BoundField AccessibleHeaderText="" DataField="cotacao_web_id" HeaderText=""
                                    ItemStyle-HorizontalAlign="Right" Visible="false" />
                                <asp:BoundField AccessibleHeaderText="N� do protocolo" DataField="protocolo_id" HeaderText="N� do protocolo"
                                    ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField AccessibleHeaderText="Produto" DataField="produto_id" HeaderText="Produto"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField AccessibleHeaderText="Ramo" DataField="ramo_id" HeaderText="Ramo"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField AccessibleHeaderText="Segurado" DataField="segurado" HeaderText="Segurado"
                                    ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField AccessibleHeaderText="Status" DataField="status_id" HeaderText="Status"
                                    ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField AccessibleHeaderText="Status" DataField="descricao_status" HeaderText="Status da cota��o"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField AccessibleHeaderText="Inicio da vig�ncia" DataField="dt_vigencia_endosso"
                                    ItemStyle-HorizontalAlign="Center" HeaderText="Inicio da vig�ncia" DataFormatString="{0:dd/MM/yyyy}"
                                    HtmlEncode="False" />
                                <asp:BoundField AccessibleHeaderText="Data recebida unidade t�cnica" DataField="dt_recebida_ut"
                                    ItemStyle-HorizontalAlign="Center" HeaderText="Data recebida unidade t�cnica"
                                    DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False" />
                                <asp:BoundField AccessibleHeaderText="Dias pendentes" DataField="dias_pendentes"
                                    HeaderText="Dias pendentes" ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                            <PagerStyle CssClass="grid_pager" HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                Nenhuma cota��o foi encontrada para os filtros informados, favor refazer a consulta!
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table runat="server" Height="300" Width="400">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="lbMSGexisteArquivoHeader" runat="server" Text="" CssClass="Cabecalho"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow ID="TableRow35" runat="server">
                                <asp:TableCell ID="TableCell91" runat="server">
                                    <asp:DataList ID="dlsDocumentos" runat="server" gridlines="None" RepeatColumns="2" RepeatDirection="Horizontal" CellPadding="3" CellSpacing="0" CssClass="CorpoDatalist">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hplVisualiza_Documento" runat="server"></asp:HyperLink>  
                                        </ItemTemplate>
                                    </asp:DataList>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="lbMSGexisteArquivoFooter" runat="server" Text="" CssClass="RodapeDocumentos"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>
    </form>
</body>
</html>
