﻿Imports System.Data.SqlClient
Namespace Data
    Public Class clausula_beneficiariaDAO
        'Inherits ConnectionHelper
        Public conexao As ConnectionHelper

        Public Sub New(ByVal linkSeguro As Alianca.Seguranca.Web.LinkSeguro)
            conexao = New ConnectionHelper(linkSeguro)
        End Sub

        Public Function Consultar(ByVal cotacao_web_id As Integer) As DataSet

            Dim params As New List(Of SqlParameter)()
            Dim param As New SqlParameter

            param = New SqlParameter("@cotacao_web_id", SqlDbType.Int)
            param.Value = CInt(cotacao_web_id)
            params.Add(param)

            Consultar = conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09197_SPS", params.ToArray())

        End Function

        Public Sub inserirItemClausulaBeneficiaria(ByVal num_transacao As Integer, ByVal cotacao_web_id As Integer, ByVal itemClausula As clausula_Beneficiaria, ByVal usuario As String)

            Dim params As New List(Of SqlParameter)()
            Dim param As New SqlParameter

            param = New SqlParameter("@cotacao_web_id", SqlDbType.Int)
            param.Value = CInt(cotacao_web_id)
            params.Add(param)

            param = New SqlParameter("@beneficiario", SqlDbType.NVarChar)
            param.Value = itemClausula.beneficiario
            params.Add(param)

            param = New SqlParameter("@cnpj_cpf", SqlDbType.NVarChar)
            param.Value = itemClausula.cnpj_cpf
            params.Add(param)

            param = New SqlParameter("@usuario", SqlDbType.NVarChar)
            param.Value = usuario
            params.Add(param)

            conexao.ExecuteSQL(num_transacao, CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09178_SPI", params.ToArray())

        End Sub

    End Class
End Namespace



