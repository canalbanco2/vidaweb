﻿Imports System.Data.SqlClient

Public Class cotacao_web
#Region "Atributos"
    Private _cotacao_web_id As Integer
    Private _seq_cotacao As Integer
    Private _protocolo_id As String
    Private _cliente_id As Integer

    'Dados para a carta
    Private _cliente_nome As String
    Private _cliente_cpfCnpj As String

    Private _proposta_id As Integer
    Private _ramo_id As Integer
    Private _apolice_id As Integer
    Private _corretor_id As Integer
    Private _endosso_id As Integer
    Private _status_cotacao_id As Integer
    Private _dt_vigencia_endosso As Date
    Private _solicitante As String
    Private _proposta_bb_id As Integer
    Private _alt_importancia_segurada As String

    '    Private _tp_alt_coberturas As String
    Private _alt_coberturas_inclusao As Boolean
    Private _alt_coberturas_exclusao As Boolean
    Private _alt_coberturas_alteracao As Boolean

    '    Private _tp_alt_local_risco As String
    Private _alt_local_risco_inclusao As Boolean
    Private _alt_local_risco_exclusao As Boolean
    Private _alt_local_risco_retificacao As Boolean
    Private _alt_local_risco_alteracao As Boolean

    '    Private _tp_alt_clausula_beneficiario As String
    Private _alt_clausula_beneficiario_inclusao As Boolean
    Private _alt_clausula_beneficiario_exclusao As Boolean
    Private _alt_clausula_beneficiario_retificacao As Boolean
    Private _alt_clausula_beneficiario_alteracao As Boolean

    Private _tp_alt_outros As String
    Private _dados_inc_locais As String
    Private _distancia_bombeiro_10_km As String
    Private _construcao_edificio As String
    Private _fiacao_eletrica As String

    '    Private _cobertura_edificio As String
    Private _cobertura_edificio_laje As Boolean
    Private _cobertura_edificio_telha As Boolean

    Private _cobertura_edificio_outros As String
    Private _sinistralidade_cinco_anos As String
    Private _inf_adicional As String
    Private _pessoa_contato_inspecao As String
    Private _email_contato_inspecao As String
    Private _inf_complementares As String
    Private _observacao As String
    Private _forma_pagto As String
    Private _num_parcelas_pagto As String
    Private _dt_inclusao As Date
    Private _dt_alteracao As Date
    Private _usuario As String
    Private _nome_Corretora As String
    Private _cnpj_corretora As String
    Private _dt_fim_vigencia_endosso As Date

    'TVIGNOLI:
    Public listaReducao As New List(Of reducao)
    Public listaClausulaBeneficiaria As New List(Of clausula_Beneficiaria)
    Public lista_sistema_protecao_incendio As New List(Of sistema_protecao_incendio)
    Public lista_sistema_protecao_roubo As New List(Of sistema_protecao_roubo)
    Public lista_telefone_contato_inspecao As New List(Of cotacao_tel_contato_inspecao)

#End Region

#Region "Propriedades"

    Public Property cotacao_web_id()
        Get
            Return _cotacao_web_id
        End Get
        Set(ByVal value)
            _cotacao_web_id = value
        End Set
    End Property

    Public Property seq_cotacao()
        Get
            Return _seq_cotacao
        End Get
        Set(ByVal value)
            _seq_cotacao = value
        End Set
    End Property

    Public Property protocolo_id()
        Get
            Return _protocolo_id
        End Get
        Set(ByVal value)
            _protocolo_id = value
        End Set
    End Property

    Public Property cliente_id()
        Get
            Return _cliente_id
        End Get
        Set(ByVal value)
            _cliente_id = value
        End Set
    End Property

    Public Property cliente_nome()
        Get
            Return _cliente_nome
        End Get
        Set(ByVal value)
            _cliente_nome = value
        End Set
    End Property

    Public Property cliente_cpfCnpj()
        Get
            Return _cliente_cpfCnpj
        End Get
        Set(ByVal value)
            _cliente_cpfCnpj = value
        End Set
    End Property

    Public Property proposta_id()
        Get
            Return _proposta_id
        End Get
        Set(ByVal value)
            _proposta_id = value
        End Set
    End Property

    Public Property ramo_id()
        Get
            Return _ramo_id
        End Get
        Set(ByVal value)
            _ramo_id = value
        End Set
    End Property

    Public Property apolice_id()
        Get
            Return _apolice_id
        End Get
        Set(ByVal value)
            _apolice_id = value
        End Set
    End Property

    Public Property corretor_id()
        Get
            Return _corretor_id
        End Get
        Set(ByVal value)
            _corretor_id = value
        End Set
    End Property

    Public Property endosso_id()
        Get
            Return _endosso_id
        End Get
        Set(ByVal value)
            _endosso_id = value
        End Set
    End Property

    Public Property status_cotacao_id()
        Get
            Return _status_cotacao_id
        End Get
        Set(ByVal value)
            _status_cotacao_id = value
        End Set
    End Property

    Public Property dt_vigencia_endosso()
        Get
            Return _dt_vigencia_endosso
        End Get
        Set(ByVal value)
            _dt_vigencia_endosso = value
        End Set
    End Property

    Public Property solicitante()
        Get
            Return _solicitante
        End Get
        Set(ByVal value)
            _solicitante = value
        End Set
    End Property

    Public Property proposta_bb_id()
        Get
            Return _proposta_bb_id
        End Get
        Set(ByVal value)
            _proposta_bb_id = value
        End Set
    End Property

    Public Property alt_importancia_segurada()
        Get
            Return _alt_importancia_segurada
        End Get
        Set(ByVal value)
            _alt_importancia_segurada = value
        End Set
    End Property

    Public Property alt_coberturas_inclusao()
        Get
            Return _alt_coberturas_inclusao
        End Get
        Set(ByVal value)
            _alt_coberturas_inclusao = value
        End Set
    End Property

    Public Property alt_coberturas_exclusao()
        Get
            Return _alt_coberturas_exclusao
        End Get
        Set(ByVal value)
            _alt_coberturas_exclusao = value
        End Set
    End Property

    Public Property alt_coberturas_alteracao()
        Get
            Return _alt_coberturas_alteracao
        End Get
        Set(ByVal value)
            _alt_coberturas_alteracao = value
        End Set
    End Property

    Public Property alt_local_risco_inclusao()
        Get
            Return _alt_local_risco_inclusao
        End Get
        Set(ByVal value)
            _alt_local_risco_inclusao = value
        End Set
    End Property

    Public Property alt_local_risco_exclusao()
        Get
            Return _alt_local_risco_exclusao
        End Get
        Set(ByVal value)
            _alt_local_risco_exclusao = value
        End Set
    End Property

    Public Property alt_local_risco_retificacao()
        Get
            Return _alt_local_risco_retificacao
        End Get
        Set(ByVal value)
            _alt_local_risco_retificacao = value
        End Set
    End Property

    Public Property alt_local_risco_alteracao()
        Get
            Return _alt_local_risco_alteracao
        End Get
        Set(ByVal value)
            _alt_local_risco_alteracao = value
        End Set
    End Property

    Public Property alt_clausula_beneficiario_inclusao()
        Get
            Return _alt_clausula_beneficiario_inclusao
        End Get
        Set(ByVal value)
            _alt_clausula_beneficiario_inclusao = value
        End Set
    End Property

    Public Property alt_clausula_beneficiario_exclusao()
        Get
            Return _alt_clausula_beneficiario_exclusao
        End Get
        Set(ByVal value)
            _alt_clausula_beneficiario_exclusao = value
        End Set
    End Property

    Public Property alt_clausula_beneficiario_retificacao()
        Get
            Return _alt_clausula_beneficiario_retificacao
        End Get
        Set(ByVal value)
            _alt_clausula_beneficiario_retificacao = value
        End Set
    End Property

    Public Property alt_clausula_beneficiario_alteracao()
        Get
            Return _alt_clausula_beneficiario_alteracao
        End Get
        Set(ByVal value)
            _alt_clausula_beneficiario_alteracao = value
        End Set
    End Property

    Public Property tp_alt_outros()
        Get
            Return _tp_alt_outros
        End Get
        Set(ByVal value)
            _tp_alt_outros = value
        End Set
    End Property

    Public Property dados_inc_locais()
        Get
            Return _dados_inc_locais
        End Get
        Set(ByVal value)
            _dados_inc_locais = value
        End Set
    End Property

    Public Property distancia_bombeiro_10_km()
        Get
            Return _distancia_bombeiro_10_km
        End Get
        Set(ByVal value)
            _distancia_bombeiro_10_km = value
        End Set
    End Property

    Public Property construcao_edificio()
        Get
            Return _construcao_edificio
        End Get
        Set(ByVal value)
            _construcao_edificio = value
        End Set
    End Property

    Public Property fiacao_eletrica()
        Get
            Return _fiacao_eletrica
        End Get
        Set(ByVal value)
            _fiacao_eletrica = value
        End Set
    End Property

    Public Property cobertura_edificio_laje()
        Get
            Return _cobertura_edificio_laje
        End Get
        Set(ByVal value)
            _cobertura_edificio_laje = value
        End Set
    End Property

    Public Property cobertura_edificio_telha()
        Get
            Return _cobertura_edificio_telha
        End Get
        Set(ByVal value)
            _cobertura_edificio_telha = value
        End Set
    End Property

    Public Property cobertura_edificio_outros()
        Get
            Return _cobertura_edificio_outros
        End Get
        Set(ByVal value)
            _cobertura_edificio_outros = value
        End Set
    End Property

    Public Property sinistralidade_cinco_anos()
        Get
            Return _sinistralidade_cinco_anos
        End Get
        Set(ByVal value)
            _sinistralidade_cinco_anos = value
        End Set
    End Property

    Public Property inf_adicional()
        Get
            Return _inf_adicional
        End Get
        Set(ByVal value)
            _inf_adicional = value
        End Set
    End Property

    Public Property pessoa_contato_inspecao()
        Get
            Return _pessoa_contato_inspecao
        End Get
        Set(ByVal value)
            _pessoa_contato_inspecao = value
        End Set
    End Property

    Public Property email_contato_inspecao()
        Get
            Return _email_contato_inspecao
        End Get
        Set(ByVal value)
            _email_contato_inspecao = value
        End Set
    End Property

    Public Property inf_complementares()
        Get
            Return _inf_complementares
        End Get
        Set(ByVal value)
            _inf_complementares = value
        End Set
    End Property

    Public Property observacao()
        Get
            Return _observacao
        End Get
        Set(ByVal value)
            _observacao = value
        End Set
    End Property

    Public Property forma_pagto()
        Get
            Return _forma_pagto
        End Get
        Set(ByVal value)
            _forma_pagto = value
        End Set
    End Property

    Public Property num_parcelas_pagto()
        Get
            Return _num_parcelas_pagto
        End Get
        Set(ByVal value)
            _num_parcelas_pagto = value
        End Set
    End Property

    Public Property dt_inclusao()
        Get
            Return _dt_inclusao
        End Get
        Set(ByVal value)
            _dt_inclusao = value
        End Set
    End Property

    Public Property dt_alteracao()
        Get
            Return _dt_alteracao
        End Get
        Set(ByVal value)
            _dt_alteracao = value
        End Set
    End Property

    Public Property usuario()
        Get
            Return _usuario
        End Get
        Set(ByVal value)
            _usuario = value
        End Set
    End Property

    Public Property nome_corretora()
        Get
            Return _nome_Corretora
        End Get
        Set(ByVal value)
            _nome_Corretora = value
        End Set
    End Property

    Public Property cnpj_corretora()
        Get
            Return _cnpj_corretora
        End Get
        Set(ByVal value)
            _cnpj_corretora = value
        End Set
    End Property

    Public Property dt_fim_vigencia_endosso()
        Get
            Return _dt_fim_vigencia_endosso
        End Get
        Set(ByVal value)
            _dt_fim_vigencia_endosso = value
        End Set
    End Property

#End Region

    Public Sub New()

    End Sub

    Public Sub inserirCotacao(ByVal possuiCarta As Boolean, ByVal usuario As String, ByVal linkSeguro As Alianca.Seguranca.Web.LinkSeguro, Optional ByVal cotacao_web_controle_id As Integer = 0)

        Dim cotacao_webDAO As New Data.cotacao_webDAO(linkSeguro)
        Dim transacao As Integer = cotacao_webDAO.conexao.BeginTransaction()
        Dim dt As DataSet

        Dim controle As New Data.cotacao_web_controleDAO(linkSeguro)

        Try

            If (cotacao_web_controle_id > 0) Then
                controle.RecalcularCotacao(cotacao_web_controle_id, usuario, transacao, linkSeguro)
            End If

            dt = cotacao_webDAO.InserirCotacao(Me, transacao, usuario)
            Me.cotacao_web_id = CInt(dt.Tables(0).Rows(0)("cotacao_web_id"))
            Me.protocolo_id = dt.Tables(0).Rows(0)("protocolo_id")
            Me.dt_inclusao = dt.Tables(0).Rows(0)("data")
            For Each item As reducao In listaReducao
                item.inserirItemReducao(transacao, Me.cotacao_web_id, usuario, linkSeguro)
            Next
            For Each item As clausula_Beneficiaria In listaClausulaBeneficiaria
                item.inserirItemClausulaBeneficiaria(transacao, Me.cotacao_web_id, usuario, linkSeguro)
            Next
            cotacao_webDAO.inserirItemProtecaoIncendio(transacao, Me.cotacao_web_id, Me.lista_sistema_protecao_incendio, usuario)
            cotacao_webDAO.inserirItemProtecaoRoubo(transacao, Me.cotacao_web_id, Me.lista_sistema_protecao_roubo, usuario)
            cotacao_webDAO.inserirTelefoneContatoInspecao(transacao, Me.cotacao_web_id, Me.lista_telefone_contato_inspecao, usuario)
            If (possuiCarta) Then
                Dim carta As New carta(Me.cotacao_web_id, Me.cliente_nome, Me.cliente_cpfCnpj, Me.ramo_id, Me.apolice_id, Me.dt_vigencia_endosso)
                Dim cartaDAO As New Data.cartaDAO(linkSeguro)
                cartaDAO.inserirCarta(transacao, carta, usuario)
            End If

            cotacao_webDAO.conexao.CommitTransaction(transacao)

        Catch ex As Exception
            cotacao_webDAO.conexao.RollBackTransaction(transacao)
            Throw ex
        End Try
    End Sub

End Class

