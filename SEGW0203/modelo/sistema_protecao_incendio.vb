﻿Public Class sistema_protecao_incendio

#Region "Atributos"
    Private _tp_protecao_incendio As String
#End Region

#Region "Propriedades"
    Public Property tp_protecao_incendio()
        Get
            Return _tp_protecao_incendio
        End Get
        Set(ByVal value)
            _tp_protecao_incendio = value
        End Set
    End Property
#End Region

End Class
