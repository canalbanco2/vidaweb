﻿Imports System.Data.SqlClient

Namespace Data

    Public Class motivo_devolucao_utDAO
        'Inherits ConnectionHelper
        Public conexao As ConnectionHelper

        Public Sub New(ByVal linkSeguro As Alianca.Seguranca.Web.LinkSeguro)
            conexao = New ConnectionHelper(linkSeguro)
        End Sub

        Public Function PesquisarMotivos() As DataSet

            PesquisarMotivos = conexao.ExecuteSQL(CommandType.Text, "gestao_cotacao_db.dbo.SEGS09203_SPS")

        End Function

        Public Function PesquisarMotivos(ByVal cotacao_web_controle_id As Integer) As DataSet

            PesquisarMotivos = conexao.ExecuteSQL(CommandType.Text, "gestao_cotacao_db.dbo.SEGS09207_SPS @cotacao_web_controle_id =" & cotacao_web_controle_id)

        End Function

    End Class

End Namespace