﻿Imports System.Data.SqlClient

Namespace Data

    Public Class cartaDAO
        'Inherits ConnectionHelper
        Public conexao As ConnectionHelper

        Public Sub New(ByVal linkSeguro As Alianca.Seguranca.Web.LinkSeguro)
            conexao = New ConnectionHelper(linkSeguro)
        End Sub

        Public Sub inserirCarta(ByVal num_transacao As Integer, ByVal carta As carta, ByVal usuario As String)

            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            '@cotacao_web_id INT
            param = New SqlParameter("@cotacao_web_id", SqlDbType.Int)
            param.Value = carta.cotacao_web_id
            params.Add(param)

            '@texto_carta VARCHAR(4000)
            param = New SqlParameter("@texto_carta", SqlDbType.NVarChar)
            param.Value = carta.texto_carta
            params.Add(param)

            '@usuario VARCHAR(20)
            param = New SqlParameter("@usuario", SqlDbType.NVarChar)
            param.Value = usuario
            params.Add(param)

            conexao.ExecuteSQL(num_transacao, CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09177_SPI", params.ToArray())

        End Sub

    End Class

End Namespace