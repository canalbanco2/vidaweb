﻿Public Class apolice

#Region "Atributos"
    Private _apolice_id As Integer
    Private _produto_id As Integer
    Private _ramo_id As Integer
    Private _segurado As String
    Private _cliente_id As Integer
    Private _cpf_cnpj As String
    Private _dt_inicio_vigencia As Date
    Private _dt_fim_vigencia As Date
    Private _agencia As String
    Private _proposta_id As Integer
    Private _corretor As String
    Private _corretorID As Integer
    Private _CNPJ_corretora As String
    Private _proposta_bb As Integer
#End Region

#Region "Propriedades"

    Public Property proposta_bb()
        Get
            Return _proposta_bb
        End Get
        Set(ByVal value)
            _proposta_bb = value
        End Set
    End Property

    Public Property CNPJ_corretora()
        Get
            Return _CNPJ_corretora
        End Get
        Set(ByVal value)
            _CNPJ_corretora = value
        End Set
    End Property

    Public Property corretorID()
        Get
            Return _corretorID
        End Get
        Set(ByVal value)
            _corretorID = value
        End Set
    End Property

    Public Property corretor()
        Get
            Return _corretor
        End Get
        Set(ByVal value)
            _corretor = value
        End Set
    End Property

    Public Property proposta_id()
        Get
            Return _proposta_id
        End Get
        Set(ByVal value)
            _proposta_id = value
        End Set
    End Property

    Public Property apolice_id()
        Get
            Return _apolice_id
        End Get
        Set(ByVal value)
            _apolice_id = value
        End Set
    End Property

    Public Property produto_id()
        Get
            Return _produto_id
        End Get
        Set(ByVal value)
            _produto_id = value
        End Set
    End Property

    Public Property ramo_id()
        Get
            Return _ramo_id
        End Get
        Set(ByVal value)
            _ramo_id = value
        End Set
    End Property

    Public Property segurado()
        Get
            Return _segurado
        End Get
        Set(ByVal value)
            _segurado = value
        End Set
    End Property

    Public Property cliente_id()
        Get
            Return _cliente_id
        End Get
        Set(ByVal value)
            _cliente_id = value
        End Set
    End Property

    Public Property cpf_cnpj()
        Get
            Return _cpf_cnpj
        End Get
        Set(ByVal value)
            _cpf_cnpj = value
        End Set
    End Property

    Public Property dt_inicio_vigencia()
        Get
            Return _dt_inicio_vigencia
        End Get
        Set(ByVal value)
            _dt_inicio_vigencia = value
        End Set
    End Property

    Public Property dt_fim_vigencia()
        Get
            Return _dt_fim_vigencia
        End Get
        Set(ByVal value)
            _dt_fim_vigencia = value
        End Set
    End Property

    Public Property agencia()
        Get
            Return _agencia
        End Get
        Set(ByVal value)
            _agencia = value
        End Set
    End Property

#End Region


End Class

