﻿

Imports System.Data.SqlClient

Namespace Data

    Public Class tipo_exigenciaDAO
        'Inherits ConnectionHelper
        Public conexao As ConnectionHelper

        Public Sub New(ByVal linkSeguro As Alianca.Seguranca.Web.LinkSeguro)
            conexao = New ConnectionHelper(linkSeguro)
        End Sub

        Public Function PesquisarExigencias() As DataSet

            PesquisarExigencias = conexao.ExecuteSQL(CommandType.Text, "gestao_cotacao_db.dbo.SEGS09201_SPS")

        End Function

        Public Function PesquisaExigenciaCotacao(ByVal cotacao_web_controle_id As Integer) As String

            PesquisaExigenciaCotacao = conexao.ExecuteSQL(CommandType.Text, "gestao_cotacao_db.dbo.SEGS09202_SPS @cotacao_web_controle_id =" & cotacao_web_controle_id).Tables(0).Rows(0)("descricao").ToString

        End Function

    End Class

End Namespace
