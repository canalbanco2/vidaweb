﻿Public Class reducao

#Region "Atributos"
    Private _local_reducao As String
    Private _cobertura As String
    Private _is_de As Decimal
    Private _is_para As Decimal
    Private _premio As Decimal
#End Region

#Region "Propriedades"
    Public Property premio()
        Get
            Return _premio
        End Get
        Set(ByVal value)
            _premio = value
        End Set
    End Property

    Public Property is_para()
        Get
            Return _is_para
        End Get
        Set(ByVal value)
            _is_para = value
        End Set
    End Property

    Public Property is_de()
        Get
            Return _is_de
        End Get
        Set(ByVal value)
            _is_de = value
        End Set
    End Property

    Public Property cobertura()
        Get
            Return _cobertura
        End Get
        Set(ByVal value)
            _cobertura = value
        End Set
    End Property

    Public Property local_reducao()
        Get
            Return _local_reducao
        End Get
        Set(ByVal value)
            _local_reducao = value
        End Set
    End Property
#End Region

#Region "Métodos"

    Public Sub inserirItemReducao(ByVal num_transacao As Integer, ByVal cotacao_web_id As Integer, ByVal usuario As String, ByVal linkSeguro As Alianca.Seguranca.Web.LinkSeguro)

        Dim obj_reducaoDAO As New Data.reducaoDAO(linkSeguro)
        obj_reducaoDAO.inserirItemReducao(num_transacao, cotacao_web_id, Me, usuario)

    End Sub

#End Region

End Class
