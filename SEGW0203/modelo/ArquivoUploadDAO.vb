﻿Imports System.Data.SqlClient

Namespace Data
    Public Class ArquivoUploadDAO
        'Inherits ConnectionHelper
        Public conexao As ConnectionHelper

        Public Sub New(ByVal linkSeguro As Alianca.Seguranca.Web.LinkSeguro)
            conexao = New ConnectionHelper(linkSeguro)
        End Sub

        Public Sub GravarUpload(ByVal au As ArquivoUpload, ByVal caminho_arquivo_acessar As String)

            If Right(caminho_arquivo_acessar, 1) <> "/" Then
                caminho_arquivo_acessar += "/"
            End If

            au.caminho = caminho_arquivo_acessar

            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            param = New SqlParameter("@cotacao_web_id", SqlDbType.Int)
            param.Value = au.cotacao_web_id
            params.Add(param)

            param = New SqlParameter("@tp_documento", SqlDbType.Int)
            param.Value = au.tp_documento
            params.Add(param)

            param = New SqlParameter("@caminho", SqlDbType.VarChar)
            param.Value = au.caminho
            params.Add(param)


            param = New SqlParameter("@nome_arquivo", SqlDbType.VarChar)
            param.Value = au.nome_arquivo
            params.Add(param)

            param = New SqlParameter("@usuario", SqlDbType.VarChar)
            param.Value = au.usuario
            params.Add(param)

            conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09187_SPI", params.ToArray())

        End Sub

        Public Function ExcluirDocumento(ByVal cotacao_web_controle_id As Integer, ByVal nome_arquivo As String) As Boolean
            Try
                Dim params As New List(Of SqlParameter)()
                Dim param As SqlParameter

                param = New SqlParameter("@cotacao_web_controle_id", SqlDbType.Int)
                param.Value = cotacao_web_controle_id
                params.Add(param)

                param = New SqlParameter("@nome_arquivo", SqlDbType.VarChar)
                param.Value = nome_arquivo
                params.Add(param)

                conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09215_SPD", params.ToArray())

                Return True
            Catch ex As Exception
                Return False
            End Try

        End Function

        Public Function RetornaTipoDocumento(ByVal cotacao_web_controle_id As Integer, ByVal nome_arquivo As String) As Integer

            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            param = New SqlParameter("@cotacao_web_controle_id", SqlDbType.Int)
            param.Value = cotacao_web_controle_id
            params.Add(param)

            param = New SqlParameter("@nome_arquivo", SqlDbType.VarChar)
            param.Value = nome_arquivo
            params.Add(param)

            RetornaTipoDocumento = conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09208_SPS", params.ToArray()).Tables(0).Rows(0)("tp_documento").ToString

        End Function

        Public Function RetornaDocumentosCorretor(ByVal cotacao_web_id As Integer) As DataSet

            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            param = New SqlParameter("@cotacao_web_id", SqlDbType.Int)
            param.Value = cotacao_web_id
            params.Add(param)

            RetornaDocumentosCorretor = conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS13301_SPS", params.ToArray())

        End Function
    End Class
End Namespace
