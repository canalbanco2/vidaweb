﻿Imports System.Data.SqlClient

Namespace Data
    Public Class cotacao_webDAO
        'Inherits ConnectionHelper
        Public conexao As ConnectionHelper

        Public Sub New()

        End Sub

        Public Sub New(ByVal linkSeguro As Alianca.Seguranca.Web.LinkSeguro)
            conexao = New ConnectionHelper(linkSeguro)
        End Sub

        Public Sub AtualizarStatus(ByVal cotacao_web_id As Integer, ByVal status_cotacao_id As Integer, ByVal usuario As String)

            Dim params As New List(Of SqlParameter)()
            Dim param As New SqlParameter

            param = New SqlParameter("@cotacao_web_id", SqlDbType.Int)
            param.Value = CInt(cotacao_web_id)
            params.Add(param)

            param = New SqlParameter("@status_cotacao_id", SqlDbType.Int)
            param.Value = CInt(status_cotacao_id)
            params.Add(param)

            param = New SqlParameter("@usuario", SqlDbType.NVarChar)
            param.Value = usuario
            params.Add(param)

            conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09190_SPU", params.ToArray())

        End Sub

        Public Sub AtualizarEndosso(ByVal cotacao_web_id As Integer, ByVal endosso_id As Integer, ByVal usuario As String)

            Dim params As New List(Of SqlParameter)()
            Dim param As New SqlParameter

            param = New SqlParameter("@cotacao_web_id", SqlDbType.Int)
            param.Value = CInt(cotacao_web_id)
            params.Add(param)

            param = New SqlParameter("@endosso_id", SqlDbType.Int)
            param.Value = CInt(endosso_id)
            params.Add(param)

            param = New SqlParameter("@usuario", SqlDbType.NVarChar)
            param.Value = usuario
            params.Add(param)

            conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09189_SPU", params.ToArray())

        End Sub

        Public Function ConsultarCotacao(ByVal cotacao_web_controle_id As Integer) As DataSet

            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            '@cotacao_web_id
            param = New SqlParameter("@cotacao_web_controle_id", SqlDbType.Int)
            param.Value = cotacao_web_controle_id
            params.Add(param)

            ConsultarCotacao = conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09198_SPS", params.ToArray())

        End Function

        Public Function InserirCotacao(ByVal cotacao As cotacao_web, ByVal num_transacao As Integer, ByVal usuario As String) As DataSet

            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            '@protocolo_id
            param = New SqlParameter("@protocolo_id", SqlDbType.NVarChar)
            param.Value = cotacao.protocolo_id
            params.Add(param)

            '@cliente_id
            param = New SqlParameter("@cliente_id", SqlDbType.Int)
            param.Value = CInt(cotacao.cliente_id)
            params.Add(param)

            'proposta_id
            param = New SqlParameter("@proposta_id", SqlDbType.Int)
            param.Value = CInt(cotacao.proposta_id)
            params.Add(param)

            'ramo_id
            param = New SqlParameter("@ramo_id", SqlDbType.Int)
            param.Value = CInt(cotacao.ramo_id)
            params.Add(param)

            'apolice_id
            If cotacao.apolice_id > 0 Then
                param = New SqlParameter("@apolice_id", SqlDbType.Int)
                param.Value = CInt(cotacao.apolice_id)
                params.Add(param)
            End If

            '@corretor_id
            param = New SqlParameter("@corretor_id", SqlDbType.Int)
            param.Value = CInt(cotacao.corretor_id)
            params.Add(param)

            '@endosso_id 
            'Não precisa, esta informação é fornecida somente no final da cotação

            'status_cotacao_id 
            param = New SqlParameter("@status_cotacao_id", SqlDbType.Int)
            param.Value = CInt(cotacao.status_cotacao_id)
            params.Add(param)

            'dt_vigencia_endosso SMALLDATETIME NOT NULL,
            param = New SqlParameter("@dt_vigencia_endosso", SqlDbType.DateTime)
            param.Value = CDate(cotacao.dt_vigencia_endosso)
            params.Add(param)

            'solicitante VARCHAR(60) NOT NULL,
            param = New SqlParameter("@solicitante", SqlDbType.Text)
            param.Value = cotacao.solicitante
            params.Add(param)

            'proposta_bb_id NUMERIC(9,0) NULL,
            param = New SqlParameter("@proposta_bb_id", SqlDbType.Int)
            param.Value = CInt(cotacao.proposta_bb_id)
            params.Add(param)

            'alt_importancia_segurada CHAR(01) NULL,
            param = New SqlParameter("@alt_importancia_segurada", SqlDbType.Text)
            param.Value = cotacao.alt_importancia_segurada
            params.Add(param)

            'alt_coberturas BIT NULL,
            param = New SqlParameter("@alt_coberturas_inclusao", SqlDbType.Bit)
            param.Value = cotacao.alt_coberturas_inclusao
            params.Add(param)
            param = New SqlParameter("@alt_coberturas_exclusao", SqlDbType.Bit)
            param.Value = cotacao.alt_coberturas_exclusao
            params.Add(param)
            param = New SqlParameter("@alt_coberturas_alteracao", SqlDbType.Bit)
            param.Value = cotacao.alt_coberturas_alteracao
            params.Add(param)

            'alt_local_risco BIT NULL,
            param = New SqlParameter("@alt_local_risco_inclusao", SqlDbType.Bit)
            param.Value = cotacao.alt_local_risco_inclusao
            params.Add(param)
            param = New SqlParameter("@alt_local_risco_exclusao", SqlDbType.Bit)
            param.Value = cotacao.alt_local_risco_exclusao
            params.Add(param)
            param = New SqlParameter("@alt_local_risco_retificacao", SqlDbType.Bit)
            param.Value = cotacao.alt_local_risco_retificacao
            params.Add(param)
            param = New SqlParameter("@alt_local_risco_alteracao", SqlDbType.Bit)
            param.Value = cotacao.alt_local_risco_alteracao
            params.Add(param)

            'alt_clausula_beneficiario BIT NULL,
            param = New SqlParameter("@alt_clausula_beneficiario_inclusao", SqlDbType.Bit)
            param.Value = cotacao.alt_clausula_beneficiario_inclusao
            params.Add(param)
            param = New SqlParameter("@alt_clausula_beneficiario_exclusao", SqlDbType.Bit)
            param.Value = cotacao.alt_clausula_beneficiario_exclusao
            params.Add(param)
            param = New SqlParameter("@alt_clausula_beneficiario_retificacao", SqlDbType.Bit)
            param.Value = cotacao.alt_clausula_beneficiario_retificacao
            params.Add(param)
            param = New SqlParameter("@alt_clausula_beneficiario_alteracao", SqlDbType.Bit)
            param.Value = cotacao.alt_clausula_beneficiario_alteracao
            params.Add(param)

            'alt_outros VARCHAR(500) NULL,
            param = New SqlParameter("@alt_outros", SqlDbType.Text)
            param.Value = cotacao.tp_alt_outros
            params.Add(param)

            'dados_inc_locais VARCHAR(4000) NULL,
            param = New SqlParameter("@dados_inc_locais", SqlDbType.Text)
            param.Value = cotacao.dados_inc_locais
            params.Add(param)

            'distancia_bombeiro_10_km CHAR(01) NULL,
            param = New SqlParameter("@distancia_bombeiro_10_km", SqlDbType.Text)
            param.Value = cotacao.distancia_bombeiro_10_km
            params.Add(param)

            'construcao_edificio CHAR(01) NULL,
            param = New SqlParameter("@construcao_edificio", SqlDbType.Text)
            param.Value = cotacao.construcao_edificio
            params.Add(param)

            'fiacao_eletrica CHAR(01) NULL,
            param = New SqlParameter("@fiacao_eletrica", SqlDbType.Text)
            param.Value = cotacao.fiacao_eletrica
            params.Add(param)

            'cobertura_edificio_telha BIT NULL,
            param = New SqlParameter("@cobertura_edificio_laje", SqlDbType.Bit)
            param.Value = cotacao.cobertura_edificio_laje
            params.Add(param)
            'cobertura_edificio_LAJE BIT NULL,
            param = New SqlParameter("@cobertura_edificio_telha", SqlDbType.Bit)
            param.Value = cotacao.cobertura_edificio_telha
            params.Add(param)

            'cobertura_edificio_outros VARCHAR(100) NULL,
            param = New SqlParameter("@cobertura_edificio_outros", SqlDbType.Text)
            param.Value = cotacao.cobertura_edificio_outros
            params.Add(param)

            'sinistralidade_cinco_anos VARCHAR(4000) NULL,
            param = New SqlParameter("@sinistralidade_cinco_anos", SqlDbType.Text)
            param.Value = cotacao.sinistralidade_cinco_anos
            params.Add(param)

            'inf_adicional VARCHAR(4000) NULL,
            param = New SqlParameter("@inf_adicional", SqlDbType.Text)
            param.Value = cotacao.inf_adicional
            params.Add(param)

            'pessoa_contato_inspecao VARCHAR(100) NULL,
            param = New SqlParameter("@pessoa_contato_inspecao", SqlDbType.Text)
            param.Value = cotacao.pessoa_contato_inspecao
            params.Add(param)

            'email_contato_inspecao VARCHAR(100) NULL,
            param = New SqlParameter("@email_contato_inspecao", SqlDbType.Text)
            param.Value = cotacao.email_contato_inspecao
            params.Add(param)

            'inf_complementares VARCHAR(4000) NULL,
            param = New SqlParameter("@inf_complementares", SqlDbType.Text)
            param.Value = cotacao.inf_complementares
            params.Add(param)

            'observacao VARCHAR(4000) NULL,
            param = New SqlParameter("@observacao", SqlDbType.Text)
            param.Value = cotacao.observacao
            params.Add(param)

            'forma_pagto CHAR(01) NULL,
            param = New SqlParameter("@forma_pagto", SqlDbType.Text)
            param.Value = cotacao.forma_pagto
            params.Add(param)

            'num_parcelas_pagto TINYINT NULL,
            param = New SqlParameter("@num_parcelas_pagto", SqlDbType.TinyInt)
            param.Value = cotacao.num_parcelas_pagto
            params.Add(param)

            'usuario VARCHAR(20) NOT NULL
            param = New SqlParameter("@usuario", SqlDbType.NVarChar)
            param.Value = usuario
            params.Add(param)

            'nome_corretora VARCHAR(60) NOT NULL
            param = New SqlParameter("@nome_corretora", SqlDbType.NVarChar)
            param.Value = cotacao.nome_corretora
            params.Add(param)

            'cnpj_corretora VARCHAR(14) NOT NULL
            param = New SqlParameter("@cnpj_corretora", SqlDbType.NVarChar)
            param.Value = cotacao.cnpj_corretora
            params.Add(param)

            'dt_fim_vigencia_endosso SMALLDATETIME NOT NULL,
            param = New SqlParameter("@dt_fim_vigencia_endosso", SqlDbType.DateTime)
            param.Value = CDate(cotacao.dt_fim_vigencia_endosso)
            params.Add(param)

            InserirCotacao = conexao.ExecuteSQL(num_transacao, CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09176_SPI", params.ToArray())

        End Function

        Public Sub inserirItemProtecaoIncendio(ByVal num_transacao As Integer, ByVal cotacao_web_id As Integer, ByVal lista_sistema_protecao_incendio As List(Of sistema_protecao_incendio), ByVal usuario As String)

            For Each item As sistema_protecao_incendio In lista_sistema_protecao_incendio

                Dim params As New List(Of SqlParameter)()
                Dim param As New SqlParameter

                param = New SqlParameter("@cotacao_web_id", SqlDbType.Int)
                param.Value = CInt(cotacao_web_id)
                params.Add(param)

                param = New SqlParameter("@tp_protecao_incendio", SqlDbType.NVarChar)
                param.Value = item.tp_protecao_incendio
                params.Add(param)

                param = New SqlParameter("@usuario", SqlDbType.NVarChar)
                param.Value = usuario
                params.Add(param)

                conexao.ExecuteSQL(num_transacao, CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09179_SPI", params.ToArray())
            Next

        End Sub

        Public Sub inserirItemProtecaoRoubo(ByVal num_transacao As Integer, ByVal cotacao_web_id As Integer, ByVal lista_sistema_protecao_roubo As List(Of sistema_protecao_roubo), ByVal usuario As String)

            For Each item As sistema_protecao_roubo In lista_sistema_protecao_roubo

                Dim params As New List(Of SqlParameter)()
                Dim param As New SqlParameter

                param = New SqlParameter("@cotacao_web_id", SqlDbType.Int)
                param.Value = CInt(cotacao_web_id)
                params.Add(param)

                param = New SqlParameter("@tp_protecao_roubo", SqlDbType.NVarChar)
                param.Value = item.tp_protecao_roubo
                params.Add(param)

                param = New SqlParameter("@usuario", SqlDbType.NVarChar)
                param.Value = usuario
                params.Add(param)

                conexao.ExecuteSQL(num_transacao, CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09180_SPI", params.ToArray())
            Next

        End Sub

        Public Sub inserirTelefoneContatoInspecao(ByVal num_transacao As Integer, ByVal cotacao_web_id As Integer, ByVal lista_telefone_contato_inspecao As List(Of cotacao_tel_contato_inspecao), ByVal usuario As String)

            For Each item As cotacao_tel_contato_inspecao In lista_telefone_contato_inspecao

                Dim params As New List(Of SqlParameter)()
                Dim param As New SqlParameter

                param = New SqlParameter("@cotacao_web_id", SqlDbType.Int)
                param.Value = CInt(cotacao_web_id)
                params.Add(param)

                param = New SqlParameter("@tp_telefone", SqlDbType.NVarChar)
                param.Value = item.tp_telefone
                params.Add(param)

                param = New SqlParameter("@DDD_contato_inspecao", SqlDbType.NVarChar)
                param.Value = item.DDD_contato_inspecao
                params.Add(param)

                param = New SqlParameter("@tel_contato_inspecao", SqlDbType.NVarChar)
                param.Value = item.tel_contato_inspecao
                params.Add(param)

                param = New SqlParameter("@usuario", SqlDbType.NVarChar)
                param.Value = usuario
                params.Add(param)

                conexao.ExecuteSQL(num_transacao, CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09181_SPI", params.ToArray())
            Next

        End Sub

        Public Function VerificaNumParcelas(ByVal cotacao_web_id As Integer) As Integer

            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            '@cotacao_web_id
            param = New SqlParameter("@cotacao_web_id", SqlDbType.Int)
            param.Value = cotacao_web_id
            params.Add(param)

            VerificaNumParcelas = conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09210_SPS", params.ToArray()).Tables(0).Rows(0)("Parcelas").ToString

        End Function

    End Class
End Namespace
