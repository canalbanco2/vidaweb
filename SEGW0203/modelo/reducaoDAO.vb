﻿Imports System.Data.SqlClient

Namespace Data
    Public Class reducaoDAO
        'Inherits ConnectionHelper
        Public conexao As ConnectionHelper

        Public Sub New(ByVal linkSeguro As Alianca.Seguranca.Web.LinkSeguro)
            conexao = New ConnectionHelper(linkSeguro)
        End Sub

        Public Function Consultar(ByVal cotacao_web_id As Integer) As DataSet

            Dim params As New List(Of SqlParameter)()
            Dim param As New SqlParameter

            param = New SqlParameter("@cotacao_web_id", SqlDbType.Int)
            param.Value = CInt(cotacao_web_id)
            params.Add(param)

            Consultar = conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09199_SPS", params.ToArray())

        End Function

        Public Sub inserirItemReducao(ByVal num_transacao As Integer, ByVal cotacao_web_id As Integer, ByVal itemReducao As reducao, ByVal usuario As String)

            Dim params As New List(Of SqlParameter)()
            Dim param As New SqlParameter

            param = New SqlParameter("@cotacao_web_id", SqlDbType.Int)
            param.Value = CInt(cotacao_web_id)
            params.Add(param)

            param = New SqlParameter("@local_reducao", SqlDbType.NVarChar)
            param.Value = itemReducao.local_reducao
            params.Add(param)

            param = New SqlParameter("@cobertura", SqlDbType.NVarChar)
            param.Value = itemReducao.cobertura
            params.Add(param)

            param = New SqlParameter("@is_de", SqlDbType.Money)
            param.Value = CDbl(itemReducao.is_de)
            params.Add(param)

            param = New SqlParameter("@is_para", SqlDbType.Money)
            param.Value = CDbl(itemReducao.is_para)
            params.Add(param)

            param = New SqlParameter("@premio", SqlDbType.Money)
            param.Value = CDbl(itemReducao.premio)
            params.Add(param)

            param = New SqlParameter("@usuario", SqlDbType.NVarChar)
            param.Value = usuario
            params.Add(param)

            conexao.ExecuteSQL(num_transacao, CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09182_SPI", params.ToArray())

        End Sub

    End Class
End Namespace

