﻿Imports System.Data.SqlClient

Namespace Data
    Public Class controle_recusa_cotacaoDAO
        'Inherits ConnectionHelper
        Public conexao As ConnectionHelper

        Public Sub New(ByVal linkSeguro As Alianca.Seguranca.Web.LinkSeguro)
            conexao = New ConnectionHelper(linkSeguro)
        End Sub

        Public Sub gravarMotivoRecusa(ByVal ctrl As controle_recusa_cotacao)

            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            param = New SqlParameter("@cotacao_web_controle_id", SqlDbType.Int)
            param.Value = ctrl.cotacao_web_controle_id
            params.Add(param)

            param = New SqlParameter("@tp_recusa_cotacao_id", SqlDbType.Int)
            param.Value = ctrl.tp_recusa_cotacao_id
            params.Add(param)

            param = New SqlParameter("@usuario", SqlDbType.VarChar)
            param.Value = ctrl.usuario
            params.Add(param)

            If ctrl.outros <> Nothing Then
                param = New SqlParameter("@outros", SqlDbType.VarChar)
                param.Value = ctrl.outros
                params.Add(param)
            End If

            conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09186_SPI", params.ToArray())

        End Sub
    End Class
End Namespace
