﻿Imports System.Data.SqlClient



Namespace Data

    Public Class apoliceDAO
        'Inherits ConnectionHelper
        Public conexao As ConnectionHelper

        Public Sub New(ByVal linkSeguro As Alianca.Seguranca.Web.LinkSeguro)
            conexao = New ConnectionHelper(linkSeguro)
        End Sub

        Public Function PesquisarApolice(ByVal apolice As String, ByVal ramo As String, ByVal propostaBB As String, ByVal Proposta As String, byval Usuario_id as String) As DataSet

            'Dim dsResultado As New DataSet
            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            If apolice <> "" Then
                param = New SqlParameter("@apolice_id", SqlDbType.Int)
                param.Value = CInt(apolice)
                params.Add(param)
            End If

            If ramo <> "" Then
                param = New SqlParameter("@ramo_id", SqlDbType.Int)
                param.Value = CInt(ramo)
                params.Add(param)
            End If

            If propostaBB <> "" Then
                param = New SqlParameter("@proposta_bb", SqlDbType.Int)
                param.Value = CInt(propostaBB)
                params.Add(param)
            End If

            If Proposta <> "" Then
                param = New SqlParameter("@proposta_id", SqlDbType.Int)
                param.Value = CInt(Proposta)
                params.Add(param)
            End If

            'MsgBox("Usuario Id: " & Usuario_id)

            Web.HttpContext.Current.Response.Write("Usuario Id: " & Usuario_id)
            If Usuario_id <> "" Then
                param = New SqlParameter("@usuario_id", SqlDbType.Int)
                param.Value = 7307 'CInt(Usuario_id)
                params.Add(param)
            End If


            PesquisarApolice = conexao.ExecuteSQL(CommandType.StoredProcedure, "[SISAB003].SEGUROS_DB.dbo.SEGS09191_SPS", params.ToArray())

        End Function

    End Class

End Namespace