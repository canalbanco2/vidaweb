﻿Imports System.Data.SqlClient

Namespace Data
    Public Class controle_motivo_devolucao_utDAO
        'Inherits ConnectionHelper
        Public conexao As ConnectionHelper

        Public Sub New(ByVal linkSeguro As Alianca.Seguranca.Web.LinkSeguro)
            conexao = New ConnectionHelper(linkSeguro)
        End Sub

        Public Sub gravarmotivoDevolucao(ByVal ctrl As controle_motivo_devolucao_ut)

            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            param = New SqlParameter("@cotacao_web_controle_id", SqlDbType.Int)
            param.Value = ctrl.cotacao_web_controle_id
            params.Add(param)

            param = New SqlParameter("@mot_devolucao_area_tecnica_id", SqlDbType.Int)
            param.Value = ctrl.mot_devolucao_area_tecnica_id
            params.Add(param)

            param = New SqlParameter("@usuario", SqlDbType.VarChar)
            param.Value = ctrl.usuario
            params.Add(param)

            If ctrl.outros <> Nothing Then
                param = New SqlParameter("@outros", SqlDbType.VarChar)
                param.Value = ctrl.outros
                params.Add(param)
            End If

            conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09185_SPI", params.ToArray())

        End Sub

    End Class
End Namespace
