﻿Imports Alianca.Seguranca.BancoDados
Imports System.Configuration
Imports system.Data.SqlClient

Namespace Data

    Public Class ConnectionHelper
        'Implements IDisposable

        Public Sub New()
            'Call ConfigurarConexao()
        End Sub

        Public Sub New(ByVal linkSeguro As Alianca.Seguranca.Web.LinkSeguro)
            Call ConfigurarConexao(linkSeguro)
        End Sub

        Private Sub ConfigurarConexao(ByVal linkSeguro As Alianca.Seguranca.Web.LinkSeguro)
            Dim ctrlAmbiente As New Alianca.Seguranca.Web.ControleAmbiente
            Dim ambiente As String = linkSeguro.Ambiente()
            'ctrlAmbiente.ObterAmbiente(Web.HttpContext.Current.Request.Url.AbsoluteUri.ToUpper)

            Try
                If Not cCon.configurado Then
                    cCon.ConfiguraConexao(Reflection.Assembly.GetExecutingAssembly(), ambiente)
                Else
                    cCon.ReConfiguraConexao(Reflection.Assembly.GetExecutingAssembly(), ambiente)
                End If

                cCon.ConnectionTimeout = cCon.TimeTimeout.Infinito

                'cCon.BancodeDados = ConfigurationManager.AppSettings("Banco").ToString()
            Catch ex As Exception
                Dim errMessage As String = ""
                Dim tempException As Exception = ex

                While (Not tempException Is Nothing)
                    errMessage += tempException.Message + Environment.NewLine + tempException.StackTrace + Environment.NewLine
                    tempException = tempException.InnerException
                End While

                Throw New Exception(errMessage)
            End Try
        End Sub

        Public Function ExecuteSQL(ByVal commandType As CommandType, ByVal SQL As String) As DataSet

            Return ExecuteSQL(commandType, SQL, Nothing)

        End Function

        Public Function ExecuteSQL(ByVal commandType As CommandType, ByVal SQL As String, ByVal param() As SqlParameter) As DataSet

            Try
                If Not param Is Nothing Then
                    Return cCon.ExecuteDataset(commandType, SQL, param)
                Else
                    Return cCon.ExecuteDataset(commandType, SQL)
                End If
            Catch ex As Exception
                Throw New Exception("Problema ao executar o comando no banco de dados.", ex)
            End Try

        End Function

        Public Function ExecuteSQL(ByVal num_transaction As Integer, ByVal commandType As CommandType, ByVal SQL As String, ByVal param() As SqlParameter) As DataSet

            Try
                Return cCon.ExecuteDataset(num_transaction, commandType, SQL, param)
            Catch ex As Exception
                Throw New Exception("Problema ao executar o comando no banco de dados: " & ex.InnerException.Message)
            End Try

        End Function

#Region "Controle de transacao"

        Public Function BeginTransaction() As Integer
            Try
                Return cCon.BeginTransaction()
            Catch ex As Exception
                Throw New Exception("Erro: " & ex.InnerException.Message)
            End Try
        End Function

        Public Shared Function RollBackTransaction(ByVal num_tran As Integer) As Integer
            Try
                Return cCon.RollBackTransaction(num_tran)
            Catch ex As Exception
                Throw New Exception("Erro: " & ex.InnerException.Message)
            End Try
        End Function

        Public Shared Function CommitTransaction(ByVal num_tran As Integer) As Integer
            Try
                Return cCon.CommitTransaction(num_tran)
            Catch ex As Exception
                Throw New Exception("Erro: " & ex.InnerException.Message)
            End Try
        End Function

#End Region

    End Class

End Namespace
