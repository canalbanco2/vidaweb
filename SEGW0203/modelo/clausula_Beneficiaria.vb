﻿Public Class clausula_Beneficiaria

#Region "Atributos"
    Private _beneficiario As String
    Private _cnpj_cpf As String
    Private _IndiceRadioButton As Integer
#End Region

#Region "Propriedades"
    Public Property IndiceRadioButton()
        Get
            Return _IndiceRadioButton
        End Get
        Set(ByVal value)
            _IndiceRadioButton = value
        End Set
    End Property

    Public Property cnpj_cpf()
        Get
            Return _cnpj_cpf
        End Get
        Set(ByVal value)
            _cnpj_cpf = value
        End Set
    End Property

    Public Property beneficiario()
        Get
            Return _beneficiario
        End Get
        Set(ByVal value)
            _beneficiario = value
        End Set
    End Property
#End Region

#Region "Métodos"

    Public Sub inserirItemClausulaBeneficiaria(ByVal num_transacao As Integer, ByVal cotacao_web_id As Integer, ByVal usuario As String, ByVal linkSeguro As Alianca.Seguranca.Web.LinkSeguro)
        Dim obj_clausula_beneficiariaDAO As New Data.clausula_beneficiariaDAO(linkSeguro)
        obj_clausula_beneficiariaDAO.inserirItemClausulaBeneficiaria(num_transacao, cotacao_web_id, Me, usuario)
    End Sub

#End Region
End Class
