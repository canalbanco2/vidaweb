﻿Public Class controle_motivo_devolucao_ut

#Region "Atributos"
    Private _cotacao_web_controle_id As Integer
    Private _mot_devolucao_area_tecnica_id As Integer
    Private _outros As String
    Private _dt_inclusao As Date
    Private _usuario As String
#End Region

#Region "Propriedades"
    Public Property cotacao_web_controle_id()
        Get
            Return _cotacao_web_controle_id
        End Get
        Set(ByVal value)
            _cotacao_web_controle_id = value
        End Set
    End Property

    Public Property mot_devolucao_area_tecnica_id()
        Get
            Return _mot_devolucao_area_tecnica_id
        End Get
        Set(ByVal value)
            _mot_devolucao_area_tecnica_id = value
        End Set
    End Property

    Public Property outros()
        Get
            Return _outros
        End Get
        Set(ByVal value)
            _outros = value
        End Set
    End Property

    Public Property dt_inclusao()
        Get
            Return _dt_inclusao
        End Get
        Set(ByVal value)
            _dt_inclusao = value
        End Set
    End Property

    Public Property usuario()
        Get
            Return _usuario
        End Get
        Set(ByVal value)
            _usuario = value
        End Set
    End Property
#End Region

    Public Sub New()

    End Sub
    Public Sub New(ByVal cotacao_web_controle_id As Integer, ByVal mot_devolucao_area_tecnica_id As Integer, ByVal outros As String, ByVal dt_inclusao As Date, ByVal usuario As String)

        Me.cotacao_web_controle_id = cotacao_web_controle_id
        Me._mot_devolucao_area_tecnica_id = mot_devolucao_area_tecnica_id
        Me.outros = outros
        Me.dt_inclusao = dt_inclusao
        Me.usuario = usuario

    End Sub

End Class

