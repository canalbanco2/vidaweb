﻿Public Class cotacao_tel_contato_inspecao

#Region "Atributos"
    Private _tp_telefone As String
    Private _DDD_contato_inspecao As String
    Private _tel_contato_inspecao As String
#End Region

#Region "Propriedades"
    Public Property tp_telefone()
        Get
            Return _tp_telefone
        End Get
        Set(ByVal value)
            _tp_telefone = value
        End Set
    End Property

    Public Property DDD_contato_inspecao()
        Get
            Return _DDD_contato_inspecao
        End Get
        Set(ByVal value)
            _DDD_contato_inspecao = value
        End Set
    End Property

    Public Property tel_contato_inspecao()
        Get
            Return _tel_contato_inspecao
        End Get
        Set(ByVal value)
            _tel_contato_inspecao = value
        End Set
    End Property
#End Region

    Public Sub New()

    End Sub

    Public Sub New(ByVal tipo_tel As String, ByVal DDD As String, ByVal TEL As String)
        _tp_telefone = tipo_tel
        _DDD_contato_inspecao = DDD
        _TEL_contato_inspecao = TEL
    End Sub

End Class
