﻿Public Class carta

#Region "Atributos"
    Private _cotacao_web_id As Integer
    Private _texto_carta As String

#End Region

#Region "Propriedades"
    Public Property cotacao_web_id()
        Get
            Return _cotacao_web_id
        End Get
        Set(ByVal value)
            _cotacao_web_id = value
        End Set
    End Property

    Public Property texto_carta()
        Get
            Return _texto_carta
        End Get
        Set(ByVal value)
            _texto_carta = value
        End Set
    End Property

#End Region

#Region "Metodos"

    Public Sub New(ByVal cotacao_web_id As Integer, ByVal cliente_nome As String, ByVal cliente_cpfCnpj As String, ByVal ramo_id As Integer, ByVal apolice_id As Integer, ByVal data As Date)



        _cotacao_web_id = cotacao_web_id

        _texto_carta = "<html><meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8""><body>"
        _texto_carta &= "<table width=""800"">"
        'customizar data:
        _texto_carta &= "<tr><td><b>CARTA DITEO/GEMPE nº PARAMETRO_ID/" & Format(data, "yyyy") & "</b></td></tr>"
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        'customizar a data:
        _texto_carta &= "<tr><td align=""right"">São Paulo, " & Format(data, "M") & " de " & data.Year
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        'Customizar o destinatário
        _texto_carta &= "<tr><td>A " & cliente_nome & "</td></tr>"
        'Customizar CNPJ:
        If cliente_cpfCnpj.Length = 11 Then
            _texto_carta &= "<tr><td>CPF: " & cliente_cpfCnpj.Substring(0, 3) & "." & cliente_cpfCnpj.Substring(3, 3) & "." & cliente_cpfCnpj.Substring(6, 3) & "-" & cliente_cpfCnpj.Substring(9, 2) & "</td></tr>"
        Else
            _texto_carta &= "<tr><td>CNPJ: " & cliente_cpfCnpj.Substring(0, 2) & "." & cliente_cpfCnpj.Substring(2, 3) & "." & cliente_cpfCnpj.Substring(5, 3) & "/" & cliente_cpfCnpj.Substring(8, 4) & "-" & cliente_cpfCnpj.Substring(12, 2) & "</td></tr>"
        End If
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        _texto_carta &= "<tr><td><b>REF.: Inclusão de Cláusula Benefeciária</b></td></tr>"
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        'Customizar a data
        _texto_carta &= "<tr><td>Em atenção à sua solicitação foi incluida na apólice nº " & ramo_id & "-" & apolice_id & " a ""Cláusula Benefeciária"", com vigência a partir de " & Format(data, "d")
        _texto_carta &= ", nos seguintes termos:</td></tr>"
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        _texto_carta &= "<tr><td>""CLÁUSULA BENEFICIÁRIA</td></tr>"
        _texto_carta &= "<tr><td>========================</td></tr>"
        _texto_carta &= "<tr><td>FICA ENTENDIDO E ACORDADO QUE ESTE SEGURO NÃO PODERÁ SER CANCELADO OU SOFRER QUALQUER ALTERAÇÃO SEM A PRÉVIA E EXPRESSA ANUÊNCIA DO BANCO DO BRASIL. NA QUALIDADE DE CREDOR HIPOTECÁRIO, PIGNORATÍCIO E/OU PROPRIETÁRIO COM ALIENAÇÃO FIDUCIÁRIA, A QUAL SERÁ PAGA TODA E QUALQUER INDENIZAÇÃO DEVIDA EM DECORRÊNCIA DESTE CONTRATO DE SEGURO, ATÉ O LIMITE DO SEU INTERESSE.""</td></tr>"
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        _texto_carta &= "<tr><td>Salientamos que os demais Termos, Cláusulas e Condições Gerais e/ou Especiais da apólice permanecem inalterados e que, em breve, você receberá o respectivo Endosso.</td></tr>"
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        _texto_carta &= "<tr><td>Em caso de dúvidas, favor entrar em contato com a nossa Central de Atendimento no (11) 3149-2650 ou email: asasegurosgerais@aliancadobrasil.com.br</td></tr>"
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        _texto_carta &= "<tr><td>Atenciosamente, </td></tr>"
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        _texto_carta &= "<tr><td>&nbsp;</td></tr>"
        _texto_carta &= "<tr><td>Aliança do Brasil Seguros S/A.</td></tr>"
        _texto_carta &= "</table>"
        _texto_carta &= "</body></html>"

    End Sub

#End Region

End Class
