﻿

Imports System.Data.SqlClient

Namespace Data
    Public Class cotacao_web_controleDAO
        'Inherits ConnectionHelper
        Public conexao As ConnectionHelper

        Public Sub New(ByVal linkSeguro As Alianca.Seguranca.Web.LinkSeguro)
            conexao = New ConnectionHelper(linkSeguro)
        End Sub
        Public Sub FecharControle(ByVal cotacao_web_controle_id As Integer, ByVal usuario As String)

            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            param = New SqlParameter("@cotacao_web_controle_id", SqlDbType.Int)
            param.Value = cotacao_web_controle_id
            params.Add(param)

            param = New SqlParameter("@usuario", SqlDbType.VarChar)
            param.Value = usuario
            params.Add(param)

            conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09188_SPU", params.ToArray())

        End Sub

        Public Sub FecharControle(ByVal numero_transacao As Integer, ByVal cotacao_web_controle_id As Integer, ByVal usuario As String)

            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            param = New SqlParameter("@cotacao_web_controle_id", SqlDbType.Int)
            param.Value = cotacao_web_controle_id
            params.Add(param)

            param = New SqlParameter("@usuario", SqlDbType.VarChar)
            param.Value = usuario
            params.Add(param)

            conexao.ExecuteSQL(numero_transacao, CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09188_SPU", params.ToArray())

        End Sub

        Public Sub recusarCotacao(ByVal cotacao_web_controle_id As Integer, ByVal usuario As String, ByVal linkseguro As Alianca.Seguranca.Web.LinkSeguro)

            FecharControle(cotacao_web_controle_id, usuario)
            gravarNovoControle(cotacao_web_controle_id, usuario, 5)

            Dim _cotacao_web_controle As cotacao_controle_web
            _cotacao_web_controle = carregarControleCotacaoWeb(cotacao_web_controle_id)
            Dim _cotacao_webDAO As New cotacao_webDAO(linkseguro)
            _cotacao_webDAO.AtualizarStatus(_cotacao_web_controle.cotacao_web_id, 8, usuario)

        End Sub

        Public Sub gravarNovoControle(ByVal cotacao_web_controle_id As Integer, ByVal usuario As String, ByVal tp_acao_cotacao_id As Integer)

            InserirNovoControle(cotacao_web_controle_id, usuario, tp_acao_cotacao_id, 0)

        End Sub

        Public Sub InserirNovoControle(ByVal cotacao_web_controle_id As Integer, ByVal usuario As String, ByVal tp_acao_cotacao_id As Integer, ByVal dias_sla As Integer)

            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            param = New SqlParameter("@cotacao_web_controle_id", SqlDbType.Int)
            param.Value = cotacao_web_controle_id
            params.Add(param)

            param = New SqlParameter("@tp_acao_cotacao_id", SqlDbType.Int)
            param.Value = tp_acao_cotacao_id
            params.Add(param)

            param = New SqlParameter("@usuario", SqlDbType.VarChar)
            param.Value = usuario
            params.Add(param)

            If dias_sla <> 0 Then
                param = New SqlParameter("@dias_sla", SqlDbType.Int)
                param.Value = dias_sla
                params.Add(param)
            End If

            conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09183_SPI", params.ToArray())

        End Sub

        Public Sub InserirNovoControle(ByVal numero_transacao As Integer, ByVal cotacao_web_controle_id As Integer, ByVal usuario As String, ByVal tp_acao_cotacao_id As Integer, ByVal dias_sla As Integer)

            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            param = New SqlParameter("@cotacao_web_controle_id", SqlDbType.Int)
            param.Value = cotacao_web_controle_id
            params.Add(param)

            param = New SqlParameter("@tp_acao_cotacao_id", SqlDbType.Int)
            param.Value = tp_acao_cotacao_id
            params.Add(param)

            param = New SqlParameter("@usuario", SqlDbType.VarChar)
            param.Value = usuario
            params.Add(param)

            If dias_sla <> 0 Then
                param = New SqlParameter("@dias_sla", SqlDbType.Int)
                param.Value = dias_sla
                params.Add(param)
            End If

            conexao.ExecuteSQL(numero_transacao, CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09183_SPI", params.ToArray())

        End Sub

        Public Sub gravarNovoControle(ByVal cotacao_web_controle_id As Integer, ByVal usuario As String, ByVal tp_acao_cotacao_id As Integer, ByVal dias_sla As Integer)

            InserirNovoControle(cotacao_web_controle_id, usuario, tp_acao_cotacao_id, dias_sla)

        End Sub

        Public Function carregarControleCotacaoWeb(ByVal cotacao_web_controle_id As Integer) As cotacao_controle_web
            Dim resultado As New DataSet
            Dim linha As DataRow
            Dim _cotacao_controle_web As New cotacao_controle_web

            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            param = New SqlParameter("@cotacao_web_controle_id", SqlDbType.Int)
            param.Value = cotacao_web_controle_id
            params.Add(param)

            resultado = conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09195_SPS", params.ToArray())

            If resultado.Tables(0).Rows.Count > 0 Then
                linha = resultado.Tables(0).Rows(0)
                _cotacao_controle_web.cotacao_web_controle_id = linha("cotacao_web_controle_id")
                _cotacao_controle_web.cotacao_web_id = linha("cotacao_web_id")
                _cotacao_controle_web.seq_acao = linha("seq_acao")
                _cotacao_controle_web.dias_sla = linha("dias_sla")
                _cotacao_controle_web.dt_acao = linha("dt_acao")
                _cotacao_controle_web.login_usuario = linha("login_usuario")
                _cotacao_controle_web.tp_acao_cotacao_id = linha("tp_acao_cotacao_id")
            End If

            Return _cotacao_controle_web

        End Function

        Public Sub AnexarMinuta(ByVal cotacao_web_controle_id As Integer, ByVal usuario As String)

            FecharControle(cotacao_web_controle_id, usuario)
            gravarNovoControle(cotacao_web_controle_id, usuario, 2)

        End Sub

        Public Sub ExcluirMinuta(ByVal cotacao_web_controle_id As Integer, ByVal usuario As String)

            FecharControle(cotacao_web_controle_id, usuario)
            gravarNovoControle(cotacao_web_controle_id, usuario, 3)

        End Sub
        Public Sub EnviarMinuta(ByVal cotacao_web_controle_id As Integer, ByVal usuario As String, ByVal dias_sla As Integer, ByVal linkseguro As Alianca.Seguranca.Web.LinkSeguro)

            FecharControle(cotacao_web_controle_id, usuario)
            gravarNovoControle(cotacao_web_controle_id, usuario, 4, dias_sla)

            Dim _cotacao_web_controle As cotacao_controle_web
            _cotacao_web_controle = carregarControleCotacaoWeb(cotacao_web_controle_id)

            Dim _cotacao_webDAO As New cotacao_webDAO(linkseguro)
            _cotacao_webDAO.AtualizarStatus(_cotacao_web_controle.cotacao_web_id, 3, usuario)

        End Sub
        Public Sub CriarExigenciaCotacao(ByVal cotacao_web_controle_id As Integer, ByVal usuario As String, ByVal linkseguro As Alianca.Seguranca.Web.LinkSeguro)

            FecharControle(cotacao_web_controle_id, usuario)
            gravarNovoControle(cotacao_web_controle_id, usuario, 6)

            Dim _cotacao_web_controle As cotacao_controle_web
            _cotacao_web_controle = carregarControleCotacaoWeb(cotacao_web_controle_id)
            Dim _cotacao_webDAO As New cotacao_webDAO(linkseguro)
            _cotacao_webDAO.AtualizarStatus(_cotacao_web_controle.cotacao_web_id, 4, usuario)
        End Sub

        Public Sub ColocarEmInspecao(ByVal cotacao_web_controle_id As Integer, ByVal usuario As String, ByVal linkseguro As Alianca.Seguranca.Web.LinkSeguro)

            FecharControle(cotacao_web_controle_id, usuario)
            gravarNovoControle(cotacao_web_controle_id, usuario, 7)

            Dim _cotacao_web_controle As cotacao_controle_web
            _cotacao_web_controle = carregarControleCotacaoWeb(cotacao_web_controle_id)
            Dim _cotacao_webDAO As New cotacao_webDAO(linkseguro)
            _cotacao_webDAO.AtualizarStatus(_cotacao_web_controle.cotacao_web_id, 5, usuario)

        End Sub

        Public Sub EnviarParaEmissao(ByVal cotacao_web_controle_id As Integer, ByVal usuario As String, ByVal linkseguro As Alianca.Seguranca.Web.LinkSeguro)

            FecharControle(cotacao_web_controle_id, usuario)
            gravarNovoControle(cotacao_web_controle_id, usuario, 8)

            Dim _cotacao_web_controle As cotacao_controle_web
            _cotacao_web_controle = carregarControleCotacaoWeb(cotacao_web_controle_id)
            Dim _cotacao_webDAO As New cotacao_webDAO(linkseguro)
            _cotacao_webDAO.AtualizarStatus(_cotacao_web_controle.cotacao_web_id, 6, usuario)

        End Sub

        Public Sub RecalcularCotacao(ByVal cotacao_web_controle_id As Integer, ByVal usuario As String, ByVal numero_transacao As Integer, ByVal linkseguro As Alianca.Seguranca.Web.LinkSeguro)

            FecharControle(numero_transacao, cotacao_web_controle_id, usuario)
            InserirNovoControle(numero_transacao, cotacao_web_controle_id, usuario, 15, 0)

            Dim _cotacao_web_controle As cotacao_controle_web
            _cotacao_web_controle = carregarControleCotacaoWeb(cotacao_web_controle_id)
            Dim _cotacao_webDAO As New cotacao_webDAO(linkseguro)
            _cotacao_webDAO.AtualizarStatus(_cotacao_web_controle.cotacao_web_id, 9, usuario)

        End Sub
        Public Sub AnexarMinutaAssinada(ByVal cotacao_web_controle_id As Integer, ByVal usuario As String)

            FecharControle(cotacao_web_controle_id, usuario)
            gravarNovoControle(cotacao_web_controle_id, usuario, 12)

        End Sub
        Public Sub ExcluirMinutaAssinada(ByVal cotacao_web_controle_id As Integer, ByVal usuario As String)

            FecharControle(cotacao_web_controle_id, usuario)
            gravarNovoControle(cotacao_web_controle_id, usuario, 13)

        End Sub
        Public Sub EnviarMinutaAssinada(ByVal cotacao_web_controle_id As Integer, ByVal usuario As String, ByVal linkseguro As Alianca.Seguranca.Web.LinkSeguro)

            FecharControle(cotacao_web_controle_id, usuario)
            gravarNovoControle(cotacao_web_controle_id, usuario, 14)

            Dim _cotacao_web_controle As cotacao_controle_web
            _cotacao_web_controle = carregarControleCotacaoWeb(cotacao_web_controle_id)
            Dim _cotacao_webDAO As New cotacao_webDAO(linkseguro)
            _cotacao_webDAO.AtualizarStatus(_cotacao_web_controle.cotacao_web_id, 2, usuario)

        End Sub

        Public Sub AnexarEndosso(ByVal cotacao_web_controle_id As Integer, ByVal usuario As String)

            FecharControle(cotacao_web_controle_id, usuario)
            gravarNovoControle(cotacao_web_controle_id, usuario, 18)

        End Sub

        Public Sub AnexarBoleto(ByVal cotacao_web_controle_id As Integer, ByVal usuario As String)

            FecharControle(cotacao_web_controle_id, usuario)
            gravarNovoControle(cotacao_web_controle_id, usuario, 19)

        End Sub

        Public Sub CriarMotivoDevolucao(ByVal cotacao_web_controle_id As Integer, ByVal usuario As String, ByVal linkseguro As Alianca.Seguranca.Web.LinkSeguro)

            FecharControle(cotacao_web_controle_id, usuario)
            gravarNovoControle(cotacao_web_controle_id, usuario, 16)

            Dim _cotacao_web_controle As cotacao_controle_web
            _cotacao_web_controle = carregarControleCotacaoWeb(cotacao_web_controle_id)
            Dim _cotacao_webDAO As New cotacao_webDAO(linkseguro)
            _cotacao_webDAO.AtualizarStatus(_cotacao_web_controle.cotacao_web_id, 10, usuario)
        End Sub

        Public Sub ConcluirCotacao(ByVal cotacao_web_controle_id As Integer, ByVal usuario As String, ByVal numero_Endosso As Integer, ByVal linkseguro As Alianca.Seguranca.Web.LinkSeguro)

            FecharControle(cotacao_web_controle_id, usuario)
            gravarNovoControle(cotacao_web_controle_id, usuario, 17)
            FecharControle(buscarNovoControle(cotacao_web_controle_id), usuario)

            Dim _cotacao_web_controle As cotacao_controle_web
            _cotacao_web_controle = carregarControleCotacaoWeb(cotacao_web_controle_id)
            Dim _cotacao_webDAO As New cotacao_webDAO(linkseguro)
            _cotacao_webDAO.AtualizarStatus(_cotacao_web_controle.cotacao_web_id, 7, usuario)
            _cotacao_webDAO.AtualizarEndosso(_cotacao_web_controle.cotacao_web_id, numero_Endosso, usuario)
        End Sub

        Public Function buscarNovoControle(ByVal cotacao_web_controle_id As Integer) As Integer
            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            param = New SqlParameter("@cotacao_web_controle_id", SqlDbType.Int)
            param.Value = cotacao_web_controle_id
            params.Add(param)

            buscarNovoControle = conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09192_SPS", params.ToArray()).Tables(0).Rows(0)("cotacao_web_controle_id").ToString

        End Function

    End Class
End Namespace
