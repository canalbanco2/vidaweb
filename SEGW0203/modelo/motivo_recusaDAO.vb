﻿Imports System.Data.SqlClient

Namespace Data

    Public Class motivo_recusaDAO
        'Inherits ConnectionHelper
        Public conexao As ConnectionHelper

        Public Sub New(ByVal linkSeguro As Alianca.Seguranca.Web.LinkSeguro)
            conexao = New ConnectionHelper(linkSeguro)
        End Sub
        Public Function PesquisarMotivos() As DataSet

            PesquisarMotivos = conexao.ExecuteSQL(CommandType.Text, "gestao_cotacao_db.dbo.SEGS09204_SPS")

        End Function

        Public Function PesquisarMotivosCotacao(ByVal cotacao_web_controle_id As Integer) As DataSet

            PesquisarMotivosCotacao = conexao.ExecuteSQL(CommandType.Text, "gestao_cotacao_db.dbo.SEGS09205_SPS @cotacao_web_controle_id=" & cotacao_web_controle_id)

        End Function

        Public Function PesquisarMotivosDaCotacao(ByVal cotacao_web_controle_id As Integer) As String

            PesquisarMotivosDaCotacao = conexao.ExecuteSQL(CommandType.Text, "gestao_cotacao_db.dbo.SEGS09196_SPS @cotacao_web_controle_id=" & cotacao_web_controle_id).Tables(0).Rows(0)("motivos").ToString

        End Function

    End Class

End Namespace
