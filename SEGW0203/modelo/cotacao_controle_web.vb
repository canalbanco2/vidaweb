﻿
Public Class cotacao_controle_web
#Region "Atributos"
    Private _cotacao_web_controle_id As Integer
    Private _cotacao_web_id As Integer
    Private _seq_acao As Integer
    Private _dias_sla As Integer
    Private _dt_acao As Date
    Private _login_usuario As String
    Private _dt_inclusao As Date
    Private _dt_alteracao As Date
    Private _usuario As String
    Private _tp_acao_cotacao_id As Integer
#End Region

#Region "Propriedades"
    Public Property cotacao_web_controle_id()
        Get
            Return _cotacao_web_controle_id
        End Get
        Set(ByVal value)
            _cotacao_web_controle_id = value
        End Set
    End Property

    Public Property cotacao_web_id()
        Get
            Return _cotacao_web_id
        End Get
        Set(ByVal value)
            _cotacao_web_id = value
        End Set
    End Property

    Public Property seq_acao()
        Get
            Return _seq_acao
        End Get
        Set(ByVal value)
            _seq_acao = value
        End Set
    End Property
    Public Property dias_sla()
        Get
            Return _dias_sla
        End Get
        Set(ByVal value)
            _dias_sla = value
        End Set
    End Property
    Public Property dt_acao()
        Get
            Return _dt_acao
        End Get
        Set(ByVal value)
            _dt_acao = value
        End Set
    End Property

    Public Property login_usuario()
        Get
            Return _login_usuario
        End Get
        Set(ByVal value)
            _login_usuario = value
        End Set
    End Property

    Public Property dt_inclusao()
        Get
            Return _dt_inclusao
        End Get
        Set(ByVal value)
            _dt_inclusao = value
        End Set
    End Property

    Public Property dt_alteracao()
        Get
            Return _dt_alteracao
        End Get
        Set(ByVal value)
            _dt_alteracao = value
        End Set
    End Property

    Public Property usuario()
        Get
            Return _usuario
        End Get
        Set(ByVal value)
            _usuario = value
        End Set
    End Property

    Public Property tp_acao_cotacao_id()
        Get
            Return _tp_acao_cotacao_id
        End Get
        Set(ByVal value)
            _tp_acao_cotacao_id = value
        End Set
    End Property

#End Region

    Public Sub New()

    End Sub

End Class
