Public Partial Class Cotacao_web_consulta_documentos
    Inherits System.Web.UI.Page

    Dim linkseguro As New Alianca.Seguranca.Web.LinkSeguro

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        linkseguro.LerUsuario( _
                    Request.Url.Segments(Request.Url.Segments.Length - 1), _
                    Request.UserHostAddress, _
                    Request.QueryString("SLinkSeguro"))

        Dim cotacao_web_controle_id As Integer
        Dim sCotacao_Web_Id As String

        sCotacao_Web_Id = String.Empty

        cotacao_web_controle_id = Request.QueryString("cotacao_web_controle_id")

        If Not IsPostBack Then

            Dim obj_cotacao_webDAO = New Data.cotacao_webDAO(linkseguro)

            Dim dt As DataSet
            Dim fg As Data.Funcoes_gerais = New Data.Funcoes_gerais(linkseguro)

            dt = obj_cotacao_webDAO.ConsultarCotacao(cotacao_web_controle_id)

            For Each linha As DataRow In dt.Tables(0).Rows

                sCotacao_Web_Id = linha("cotacao_web_id").ToString()
                gridConsultaDocumentos.DataSource = fg.ListaCotacaoEndosso(sCotacao_Web_Id)
                
            Next

            gridConsultaDocumentos.DataBind()

            dt = Nothing
            obj_cotacao_webDAO = Nothing
            fg = Nothing

            If sCotacao_Web_Id <> String.Empty Then

                BuscaDocumentos(sCotacao_Web_Id)

            End If

        End If

        lblDataCorrente.Text = Format(Date.Now, "dddd, dd " & "DE" & " MMMM " & "DE" & " yyyy").ToLower
    End Sub

    Protected Sub gridConsultaDocumentos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gridConsultaDocumentos.RowDataBound
        Select Case e.Row.RowType
            Case DataControlRowType.Header
                e.Row.Cells(5).Visible = False
                Exit Select
            Case DataControlRowType.DataRow
                e.Row.CssClass = "grid_apolice_linha_sel"
                e.Row.Cells(5).Visible = False
                Exit Select
            Case DataControlRowType.Footer
                e.Row.Cells(5).Visible = False
                Exit Select
        End Select
    End Sub

    Private Sub BuscaDocumentos(ByVal sCotacao_Web_Id As String)

        Dim UaDAO As Data.ArquivoUploadDAO = New Data.ArquivoUploadDAO(linkseguro)
        dlsDocumentos.DataSource = UaDAO.RetornaDocumentosCorretor(sCotacao_Web_Id)
        dlsDocumentos.DataBind()

        If dlsDocumentos.Items.Count > 0 Then
            lbMSGexisteArquivoHeader.Text = "Documentos anexados pelo corretor durante a cota��o:"
            lbMSGexisteArquivoFooter.Text = "Clique sobre o documento que deseja visualizar."
        Else
            lbMSGexisteArquivoHeader.Text = "N�o foram anexados documentos pelo corretor"
            lbMSGexisteArquivoFooter.Text = ""
        End If

    End Sub

    Private Sub dlsDocumentos_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlsDocumentos.ItemDataBound
        If e.Item.DataItem IsNot Nothing Then

            Dim v_oDocumento As HyperLink = DirectCast(e.Item.FindControl("hplVisualiza_Documento"), HyperLink)

            Dim rowView As DataRowView = CType(e.Item.DataItem, DataRowView)
            v_oDocumento.Text = rowView("nome_arquivo").ToString
            v_oDocumento.Target = "_blank"
            v_oDocumento.NavigateUrl = rowView("link").ToString
        End If
    End Sub
End Class