Partial Public Class _Protocolo
    Inherits System.Web.UI.Page
    Dim linkSeguro As New Alianca.Seguranca.Web.LinkSeguro

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblNumeroProtocolo.Text = Session("numero_protocolo_completo")
        
        linkSeguro.LerUsuario( _
                                Request.Url.Segments(Request.Url.Segments.Length - 1), _
                                Request.UserHostAddress, _
                                Request.QueryString("SLinkSeguro"))

        lblDataCorrente.Text = Format(Date.Now, "dddd, dd " & "DE" & " MMMM " & "DE" & " yyyy").ToLower

        'If Session("ImprimeCarta") = "S" Then
        '    Session("ImprimeCarta") = "I"
        '    btnimprime_carta.Attributes.Add("onclick", "return confirm('Deseja Imprimir a carta gerada agora?');")
        '    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Print", "window.onload = function(){document.getElementById('btnimprime_carta').click();}", True)
        'End If

    End Sub


    Protected Sub btnOk_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click

        Select Case Session("acao")
            Case "inclusao"
                Session.Clear()
                Response.Redirect("Pedido_cotacao.aspx?SLinkSeguro=" & Request.QueryString("SLinkSeguro"))
            Case "recalculo"
                Session.Remove("numero_protocolo_completo")
                Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "fechajanela();window.opener.AtualizarGrid();", True)
        End Select

    End Sub

    Private Sub btnimprime_carta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnimprime_carta.Click

        If Session("ImprimeCarta") = "I" Then
            Dim _funcoes As Data.Funcoes_gerais = New Data.Funcoes_gerais(linkSeguro)
            Dim carta As String

            carta = _funcoes.MontaCartaBenefeciario(Session("cotacao_web_id_carta"))

            Page.ClientScript.RegisterStartupScript(Me.GetType, "Imprimir", "ImprimeCarta('" & carta & "');", True)
            Session("ImprimeCarta") = "N"
        End If

    End Sub

End Class