'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.8800
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On



'''<summary>
'''_Protocolo class.
'''</summary>
'''<remarks>
'''Auto-generated class.
'''</remarks>
Partial Public Class _Protocolo

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''pnltopo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnltopo As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblDataCorrente control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDataCorrente As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Table1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Table1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''TableCell1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TableCell1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''lbl1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblNumeroProtocolo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNumeroProtocolo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lbl2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''TableRow1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TableRow1 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''TableCell3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TableCell3 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''btnOk control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnOk As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnimprime_carta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnimprime_carta As Global.System.Web.UI.WebControls.Button
End Class
