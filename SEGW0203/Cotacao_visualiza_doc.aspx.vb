﻿Imports System
Imports System.IO
Partial Public Class Cotacao_visualiza_doc
    Inherits System.Web.UI.Page

    Dim linkseguro As New Alianca.Seguranca.Web.LinkSeguro
    Dim cotacao_web_controle_id As Integer
    Dim caminho As String
    Dim nome_arquivo As String
    Dim tipo_arquivo As String
    Dim _funcoes As Data.Funcoes_gerais

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        linkseguro.LerUsuario( _
                    Request.Url.Segments(Request.Url.Segments.Length - 1), _
                    Request.UserHostAddress, _
                    Request.QueryString("SLinkSeguro"))

        Session("usuario") = linkseguro.Login_WEB

        _funcoes = New Data.Funcoes_gerais(linkseguro)

        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, linkseguro.Ambiente())
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção)
        Else '
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, linkseguro.Ambiente())
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção)
        End If

        cotacao_web_controle_id = Request.QueryString("cotacao_web_controle_id")

        PanelImg.Visible = False
        PanelPdf.Visible = False
        PanelPdf2.Visible = False
        PanelPdf3.Visible = False

        btnImprimirImg.Visible = False

        btnExcluirDoc.Visible = False
        btnExcluirDoc2.Visible = False
        btnExcluirDoc3.Visible = False


        caminho = Request.QueryString("caminho")

        If Request.QueryString("perfil") <> "E" Then
            If System.IO.Path.GetExtension(caminho).ToLower = ".pdf" Then
                PanelPdf.Visible = True
                Data.Funcoes_gerais.escreveScript("window.onload = function(){document.getElementById('ifrMostrarPdf').src='" & caminho.ToString & "';}")
                btnExcluirDoc.Visible = True
            Else
                PanelImg.Visible = True
                Img.Src = caminho
                btnImprimirImg.Visible = True
            End If
        Else
            ' Tratamento para mais de um arquivo quando for Emissão
            If (caminho.Contains("|")) Then
                Dim lks As String() = caminho.Split(New Char() {"|"c})

                If (lks.Length = 3) Then
                    PanelPdf.Visible = True
                    PanelPdf2.Visible = True
                    PanelPdf3.Visible = True

                    Data.Funcoes_gerais.escreveScript("window.onload = function(){document.getElementById('ifrMostrarPdf').src='" & lks(0).ToString & "';document.getElementById('ifrMostrarPdf2').src='" & lks(1).ToString & "';document.getElementById('ifrMostrarPdf3').src='" & lks(2).ToString & "';}")

                    btnExcluirDoc.Visible = True
                    btnExcluirDoc2.Visible = True
                    btnExcluirDoc3.Visible = True
                Else
                    If (lks.Length = 2) Then
                        PanelPdf.Visible = True
                        PanelPdf2.Visible = True

                        Data.Funcoes_gerais.escreveScript("window.onload = function(){document.getElementById('ifrMostrarPdf').src='" & lks(0).ToString & "';document.getElementById('ifrMostrarPdf2').src='" & lks(1).ToString & "';}")

                        btnExcluirDoc.Visible = True
                        btnExcluirDoc2.Visible = True
                    Else
                        PanelPdf.Visible = True

                        Data.Funcoes_gerais.escreveScript("window.onload = function(){document.getElementById('ifrMostrarPdf').src='" & lks(0).ToString & "';}")

                        btnExcluirDoc.Visible = True
                    End If
                End If
            Else
                If System.IO.Path.GetExtension(caminho).ToLower = ".pdf" Then
                    PanelPdf.Visible = True
                    Data.Funcoes_gerais.escreveScript("window.onload = function(){document.getElementById('ifrMostrarPdf').src='" & caminho.ToString & "';}")
                    btnExcluirDoc.Visible = True
                Else
                    PanelImg.Visible = True
                    Img.Src = caminho
                    btnImprimirImg.Visible = True
                End If
            End If
        End If

        pnlOpcoesCorretor.Visible = False
        pnlOpcaoEmissao.Visible = False

        If Request.QueryString("perfil") = "C" Then
            pnlOpcoesCorretor.Visible = True
        End If
        If Request.QueryString("perfil") = "E" Then
            pnlOpcaoEmissao.Visible = True
        End If

        Dim dt As New DataSet
        Dim obj_cotacao_webDAO = New Data.cotacao_webDAO(linkseguro)
        dt = obj_cotacao_webDAO.ConsultarCotacao(cotacao_web_controle_id)

        Dim iStatus_Cotacao_Id As Integer = dt.Tables(0).Rows(0)("status_cotacao_id")

        dt = Nothing
        obj_cotacao_webDAO = Nothing

        If ((iStatus_Cotacao_Id = 2) Or (iStatus_Cotacao_Id = 7)) Then
            btnExcluirMinuta.Enabled = False
            btnEnviarMinuta.Enabled = False
        End If

        If Request.QueryString("perfil") <> "E" Then
            nome_arquivo = System.IO.Path.GetFileName(caminho)

            Dim arquivo_exclui As Data.ArquivoUploadDAO = New Data.ArquivoUploadDAO(linkseguro)

            Select Case arquivo_exclui.RetornaTipoDocumento(cotacao_web_controle_id, nome_arquivo)
                Case 1
                    tipo_arquivo = "minuta"
                    Exit Select
                Case 2
                    tipo_arquivo = "minuta assinada"
                    Exit Select
                Case 3
                    tipo_arquivo = "endosso"
                    Exit Select
                Case 4
                    tipo_arquivo = "boleto"
                    Exit Select
            End Select

            btnExcluirMinuta.Attributes.Add("onclick", "return confirm('Confirma a exclusão da minuta assinada anexada?');")
            btnExcluirDoc.Attributes.Add("onclick", "return confirm('Confirma a exclusão do " & tipo_arquivo & " anexado?');")
        Else
            If (caminho.Contains("|")) Then
                Dim lks As String() = caminho.Split(New Char() {"|"c})

                btnExcluirDoc2.Attributes.Add("onclick", "return confirm('Confirma a exclusão do " & System.IO.Path.GetFileName(lks(1).ToString()) & " anexado?');")
            Else
                nome_arquivo = System.IO.Path.GetFileName(caminho)

                Dim arquivo_exclui As Data.ArquivoUploadDAO = New Data.ArquivoUploadDAO(linkseguro)

                Select Case arquivo_exclui.RetornaTipoDocumento(cotacao_web_controle_id, nome_arquivo)
                    Case 1
                        tipo_arquivo = "minuta"
                        Exit Select
                    Case 2
                        tipo_arquivo = "minuta assinada"
                        Exit Select
                    Case 3
                        tipo_arquivo = "endosso"
                        Exit Select
                    Case 4
                        tipo_arquivo = "boleto"
                        Exit Select
                End Select
                btnExcluirMinuta.Attributes.Add("onclick", "return confirm('Confirma a exclusão da minuta assinada anexada?');")
                btnExcluirDoc.Attributes.Add("onclick", "return confirm('Confirma a exclusão do " & tipo_arquivo & " anexado?');")
            End If
        End If


            'nome_arquivo = System.IO.Path.GetFileName(caminho)

            'Dim arquivo_exclui As Data.ArquivoUploadDAO = New Data.ArquivoUploadDAO(linkseguro)

            'Select Case arquivo_exclui.RetornaTipoDocumento(cotacao_web_controle_id, nome_arquivo)
            '    Case 1
            '        tipo_arquivo = "minuta"
            '        Exit Select
            '    Case 2
            '        tipo_arquivo = "minuta assinada"
            '        Exit Select
            '    Case 3
            '        tipo_arquivo = "endosso"
            '        Exit Select
            '    Case 4
            '        tipo_arquivo = "boleto"
            '        Exit Select
            'End Select

            'btnExcluirMinuta.Attributes.Add("onclick", "return confirm('Confirma a exclusão da minuta assinada anexada?');")
            'btnExcluirDoc.Attributes.Add("onclick", "return confirm('Confirma a exclusão do " & tipo_arquivo & " anexado?');")

    End Sub

    Private Sub btnFechar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFechar.Click
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "fechajanela();", True)
    End Sub

    Private Sub btnExcluirMinuta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcluirMinuta.Click
        'If MsgBox("Confirma a exclusão da minuta assinada anexada?", MsgBoxStyle.YesNoCancel, "Exclusão da minuta assinada anexada") = MsgBoxResult.Yes Then
        Dim minuta_exclui As Data.ArquivoUploadDAO = New Data.ArquivoUploadDAO(linkseguro)
        Dim nome_arquivo As String

        nome_arquivo = System.IO.Path.GetFileName(caminho)

        Dim msgAlerta As String

        If minuta_exclui.ExcluirDocumento(cotacao_web_controle_id, nome_arquivo) Then
            Dim ctrlDAO As Data.cotacao_web_controleDAO = New Data.cotacao_web_controleDAO(linkseguro)
            ctrlDAO.ExcluirMinutaAssinada(cotacao_web_controle_id, Session("usuario"))
            File.Delete(caminho.ToString.Replace(_funcoes.Busca_Caminho("ARQUIVOS_WEB_COTACAO", linkseguro.Ambiente()), _funcoes.Busca_Caminho("ARQUIVOS_COTACAO", linkseguro.Ambiente())).Replace("/", "\"))
            ctrlDAO = Nothing
            msgAlerta = "Minuta assinada da cotação excluida com sucesso!"
        Else
            msgAlerta = "Problemas ao excluir minuta assinada" & vbNewLine & "favor tentar novamente!"
        End If

        Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('" & msgAlerta & "');", True)
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "fechajanela();window.opener.AtualizarGrid();", True)
        'End If
    End Sub

    Private Sub btnEnviarMinuta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEnviarMinuta.Click
        Dim ctrlDAO As Data.cotacao_web_controleDAO = New Data.cotacao_web_controleDAO(linkseguro)

        ctrlDAO.EnviarMinutaAssinada(cotacao_web_controle_id, Session("usuario"), linkseguro)
        ctrlDAO = Nothing

        Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Minuta assinada da cotação enviada com sucesso!');", True)
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "fechajanela();window.opener.AtualizarGrid();", True)
    End Sub

    Private Sub btnExcluirDoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcluirDoc.Click
        'Dim nome_arquivo As String
        'Dim tipo_arquivo As String = String.Empty

        'nome_arquivo = System.IO.Path.GetFileName(caminho)

        Dim arquivo_exclui As Data.ArquivoUploadDAO = New Data.ArquivoUploadDAO(linkseguro)

        'Select Case arquivo_exclui.RetornaTipoDocumento(cotacao_web_controle_id, nome_arquivo)
        '    Case 1
        '        tipo_arquivo = "minuta"
        '        Exit Select
        '    Case 2
        '        tipo_arquivo = "minuta assinada"
        '        Exit Select
        '    Case 3
        '        tipo_arquivo = "endosso"
        '        Exit Select
        '    Case 4
        '        tipo_arquivo = "boleto"
        '        Exit Select
        'End Select

        'If MsgBox("Confirma a exclusão do " & tipo_arquivo & " anexado?", MsgBoxStyle.YesNoCancel, "Exclusão do " & tipo_arquivo & " anexado") = MsgBoxResult.Yes Then
        Dim msgAlerta As String

        If Request.QueryString("perfil") <> "E" Then
            If arquivo_exclui.ExcluirDocumento(cotacao_web_controle_id, nome_arquivo) Then
                File.Delete(caminho.ToString.Replace(_funcoes.Busca_Caminho("ARQUIVOS_WEB_COTACAO", linkseguro.Ambiente()), _funcoes.Busca_Caminho("ARQUIVOS_COTACAO", linkseguro.Ambiente())).Replace("/", "\"))
                msgAlerta = "O " & tipo_arquivo & " anexo da cotação foi excluido com sucesso!"
            Else
                msgAlerta = "Problemas ao excluir " & tipo_arquivo & "\nfavor tentar novamente!"
            End If
        Else
            If (caminho.Contains("|")) Then
                Dim lks As String() = caminho.Split(New Char() {"|"c})

                If (lks(0).ToString().Trim.Length > 0) Then
                    If arquivo_exclui.ExcluirDocumento(cotacao_web_controle_id, System.IO.Path.GetFileName(lks(0).ToString())) Then
                        File.Delete(lks(0).ToString.Replace(_funcoes.Busca_Caminho("ARQUIVOS_WEB_COTACAO", linkseguro.Ambiente()), _funcoes.Busca_Caminho("ARQUIVOS_COTACAO", linkseguro.Ambiente())).Replace("/", "\"))
                        msgAlerta = "O " & lks(0).ToString() & " anexo da cotação foi excluido com sucesso!"
                    Else
                        msgAlerta = "Problemas ao excluir " & lks(0).ToString() & "\nfavor tentar novamente!"
                    End If
                End If
            Else
                If arquivo_exclui.ExcluirDocumento(cotacao_web_controle_id, nome_arquivo) Then
                    File.Delete(caminho.ToString.Replace(_funcoes.Busca_Caminho("ARQUIVOS_WEB_COTACAO", linkseguro.Ambiente()), _funcoes.Busca_Caminho("ARQUIVOS_COTACAO", linkseguro.Ambiente())).Replace("/", "\"))
                    msgAlerta = "O " & tipo_arquivo & " anexo da cotação foi excluido com sucesso!"
                Else
                    msgAlerta = "Problemas ao excluir " & tipo_arquivo & "\nfavor tentar novamente!"
                End If
            End If

        End If

        Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('" & msgAlerta & "');", True)
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "fechajanela();window.opener.AtualizarGrid();", True)
        'End If
    End Sub

    Protected Sub btnExcluirDoc2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcluirDoc2.Click
        Dim msgAlerta As String = ""
        Dim arquivo_exclui As Data.ArquivoUploadDAO = New Data.ArquivoUploadDAO(linkseguro)

        If (caminho.Contains("|")) Then
            Dim lks As String() = caminho.Split(New Char() {"|"c})

            If (lks(1).ToString().Trim.Length > 0) Then
                If arquivo_exclui.ExcluirDocumento(cotacao_web_controle_id, System.IO.Path.GetFileName(lks(1).ToString())) Then
                    File.Delete(lks(1).ToString.Replace(_funcoes.Busca_Caminho("ARQUIVOS_WEB_COTACAO", linkseguro.Ambiente()), _funcoes.Busca_Caminho("ARQUIVOS_COTACAO", linkseguro.Ambiente())).Replace("/", "\"))
                    msgAlerta = "O " & lks(1).ToString() & " anexo da cotação foi excluido com sucesso!"
                Else
                    msgAlerta = "Problemas ao excluir " & lks(1).ToString() & "\nfavor tentar novamente!"
                End If
            End If
        End If

        Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('" & msgAlerta & "');", True)
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "fechajanela();window.opener.AtualizarGrid();", True)

    End Sub

    Protected Sub btnExcluirDoc3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcluirDoc3.Click
        Dim msgAlerta As String = ""
        Dim arquivo_exclui As Data.ArquivoUploadDAO = New Data.ArquivoUploadDAO(linkseguro)

        If (caminho.Contains("|")) Then
            Dim lks As String() = caminho.Split(New Char() {"|"c})

            If (lks(2).ToString().Trim.Length > 0) Then
                If arquivo_exclui.ExcluirDocumento(cotacao_web_controle_id, System.IO.Path.GetFileName(lks(2).ToString())) Then
                    File.Delete(lks(2).ToString.Replace(_funcoes.Busca_Caminho("ARQUIVOS_WEB_COTACAO", linkseguro.Ambiente()), _funcoes.Busca_Caminho("ARQUIVOS_COTACAO", linkseguro.Ambiente())).Replace("/", "\"))
                    msgAlerta = "O " & lks(2).ToString() & " anexo da cotação foi excluido com sucesso!"
                Else
                    msgAlerta = "Problemas ao excluir " & lks(2).ToString() & "\nfavor tentar novamente!"
                End If
            End If
        End If

        Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('" & msgAlerta & "');", True)
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "fechajanela();window.opener.AtualizarGrid();", True)
    End Sub
End Class