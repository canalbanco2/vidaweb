﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Cotacao_Web_concluir.aspx.vb" Inherits="SEGW0203.Cotacao_Web_concluir" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Conclusão da cotação web</title>
        <link href="css/global.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" language="javascript" src="js/principal.js"></script>
    
<script>    
    function ValidaConclusao(){
        if (isNaN(document.getElementById('txtNumero_Endosso').value){
            alert("Valor digitado inválido para numero do endosso!");
            return false;
        }else{
            return true;
        }
    }
</script>       
</head>
<body>
    <form id="form1" runat="server">
    <div>
      <asp:Panel runat="server" ID="pnlConcluir_Cotacao" CssClass="painelEnviar">  
        <table border="0">
            <tr>
                <td align="right" width="240">
                    <asp:Label ID="lblConcluir_Cotacao" runat="server" CssClass="Texto_Normal" Text="Numero do endosso emitido : "></asp:Label>
                </td>
                <td align="left" valign="top" width="100">
                    <asp:TextBox ID="txtNumero_Endosso" runat="server" Columns="10" MaxLength="08"></asp:TextBox>                
                </td>
            </tr>
            <tr>
                <td colspan="2" heigth="10">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right" valign="baseline">
                    <asp:Button ID="btnSair" runat="server" Text="Sair" CssClass="botao" />
                    &nbsp;
                    <asp:Button ID="btnconfirmar" runat="server" Text="Confirmar" CssClass="botao" />  
                </td>
            </tr>
        </table>  
      </asp:Panel>
    
    </div>
    </form>
</body>
</html>
