﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Pedido_cotacao.aspx.vb" Inherits="SEGW0203.WebForm1" EnableViewState ="true"  EnableEventValidation="false" ValidateRequest="false"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Busca apólice</title>
    <link href="css/global.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript" src="js/principal.js"></script>
    
</head>
<body>
    <form id="form1" runat="server">
         <asp:Panel runat="server" ID="pnltopo">
        <table background="img/imgbannertopofundo.png" border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
                <td align="right" height="60" style="background-position: left top; background-image: url(img/imgBannerTopo.gif);
                    background-repeat: no-repeat" valign="top">
                </td>
            </tr>
        </table>
        <table background="img/imgFundoMenuTopo.gif" border="0" cellpadding="0" cellspacing="0"
            height="26" width="100%">
            <tr>
                <td>
                    <a class="TituloMenuTopo" href="JavaScript:window.parent.close();">&nbsp; &nbsp; &nbsp;Sair
                        &nbsp; &nbsp;&nbsp;</a> &nbsp;</td>
                <td class="DadosSistemaUsuario" style="width: 250px; text-align: right">
                    <asp:Label ID="lblDataCorrente" runat="server"></asp:Label>&nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>

        <div id="corpo">
            <asp:Table ID="Table1" runat="server" Height="120px" Width="427px">
                <asp:TableRow runat="server" Height="30" >
                    <asp:TableCell runat="server" >
                        <asp:Label ID="lblRamo" runat="server" Text="Ramo / ap&#243;lice:"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell runat="server">
                        <asp:TextBox ID="txtRamo" runat="server" MaxLength = "3"  Width = "30" onKeyUp="JavaScript:MascaraNumeros(this);"></asp:TextBox> &nbsp; / &nbsp;
                        <asp:TextBox ID="txtapolice" runat="server" MaxLength = "9" Width = "80" onKeyUp="JavaScript:MascaraNumeros(this);"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" Height="30" >
                    <asp:TableCell ID="TableCell1" runat="server">
                        <asp:Label ID="lblNumPropostaBB" runat="server" Text="Número da proposta BB:"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell2" runat="server">
                        <asp:TextBox ID="txtNPropostaBB" runat="server" MaxLength = "9" onKeyUp="JavaScript:MascaraNumeros(this);"></asp:TextBox><br />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" Height="30" >
                   <asp:TableCell ID="TableCell3" runat="server">
                        <asp:Label ID="lblNumPropostaAB" runat="server" Text="Número da proposta AB:"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell4" runat="server">
                        <asp:TextBox ID="txtNPropostaAB" runat="server" MaxLength = "9" onKeyUp="JavaScript:MascaraNumeros(this);"></asp:TextBox><br />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat = "server" Height="15" >
                    <asp:TableCell ColumnSpan="2" HorizontalAlign ="left" >
                            <asp:Button ID="btnPesquisar" runat="server" Text="Pesquisar" CssClass="botao" /><br />     
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
                       
            <asp:Table ID="TableApolice" runat="server" Visible = "false" >
                <asp:TableRow >
                    <asp:TableCell>
                        <asp:GridView ID="gridApolice"  PageSize = "50" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        DataKeyNames="proposta_id" OnRowDataBound="gridApolice_RowDataBound" 
                        Caption="Apólices" CssClass = "corpo">
                            <Columns>
                            <asp:TemplateField HeaderText="Selecione" ItemStyle-Width="60px">
                                <headerstyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:RadioButton ID="selecionaApolice" GroupName="grpApolice" runat="server" AutoPostBack="true"  
                                    OnCheckedChanged="rdbSelecionaApolice_OnCheckedChanged" /> 
                                </ItemTemplate>
                            </asp:TemplateField>
                        
                            <asp:BoundField AccessibleHeaderText="Nº da apólice" DataField="apolice_id" HeaderText="Nº da apólice"  ItemStyle-HorizontalAlign = "Center" />
                            <asp:BoundField AccessibleHeaderText="Produto" DataField="produto_id" HeaderText="Produto" ItemStyle-HorizontalAlign = "Center" />
                            <asp:BoundField AccessibleHeaderText="Ramo" DataField="ramo_id" HeaderText="Ramo" ItemStyle-HorizontalAlign = "Center" />
                            <asp:BoundField AccessibleHeaderText="Segurado" DataField="segurado" HeaderText="Segurado" ItemStyle-HorizontalAlign = "Center" />
                            <asp:BoundField AccessibleHeaderText="CNPJ" DataField="cpf_cnpj" HeaderText="CNPJ" ItemStyle-HorizontalAlign = "Center" />
                            <asp:BoundField AccessibleHeaderText="Inicio da vigência" DataField="dt_inicio_vigencia" ItemStyle-HorizontalAlign = "Center"  
                                HeaderText="Inicio da vigência" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False"/>
                            <asp:BoundField AccessibleHeaderText="Fim de Vigência" DataField="dt_fim_vigencia" ItemStyle-HorizontalAlign = "Center" 
                                HeaderText="Fim de Vigência" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False"/>
                            <asp:BoundField AccessibleHeaderText="Nome da Agência" DataField="agencia" HeaderText="Nome da Agência" ItemStyle-HorizontalAlign = "Left" />
                            <asp:BoundField AccessibleHeaderText="Corretora" DataField="corretor" HeaderText="Corretora" ItemStyle-HorizontalAlign = "Center" />
                            </Columns>
                            <PagerStyle CssClass="grid_pager" HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                Não existe proposta para os filtros solicitados, favor alterar os filtros e pesquisar novamente.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="btnConfirmar" runat="server" Text="Confirmar" CssClass="botao"/>
                    </asp:TableCell>
                </asp:TableRow>
           </asp:Table> 
           <asp:GridView ID="gridDataBase" runat="server"/>
         </div>           
    </form>
</body>
</html>
