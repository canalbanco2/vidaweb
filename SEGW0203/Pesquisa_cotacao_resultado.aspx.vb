﻿Imports System
Imports System.IO
Imports System.Configuration

Partial Public Class Pesquisa_cotacao_resultado
    Inherits System.Web.UI.Page
    Dim linkSeguro As New Alianca.Seguranca.Web.LinkSeguro


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            linkSeguro.LerUsuario( _
                            Request.Url.Segments(Request.Url.Segments.Length - 1), _
                            Request.UserHostAddress, _
                            Request.QueryString("SLinkSeguro"))

            Session("usuario") = linkSeguro.Login_WEB
            Session("SLinkSeguro") = Request("SLinkSeguro")

            Session("controleClicado") = 0
            Session("indice") = 0
            Session("minutaAnexada") = String.Empty
            preencherGrid()
            habilitarBotoes(Nothing)
        Else
            Ler_LinkSeguro()
        End If

        lblDataCorrente.Text = Format(Date.Now, "dddd, dd " & "DE" & " MMMM " & "DE" & " yyyy").ToLower

        If Session("SLinkSeguro") = String.Empty _
         Or Session("usuario") = String.Empty Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "Atualizar", "window.alert('Sessão da página perdeu referencia, favor logar novamente!');fechajanela();", True)
        End If

        MenuTecnico.Visible = False
        MenuCorretor.Visible = False
        MenuEmissao.Visible = False

        If Session("perfil") = "T" Then
            MenuTecnico.Visible = True
        End If

        If Session("perfil") = "C" Then
            MenuCorretor.Visible = True
        End If

        If Session("perfil") = "E" Then
            MenuEmissao.Visible = True
        End If

        If fuUploadMinutaTecnico.HasFile Then
            EnviarArquivoServidor(fuUploadMinutaTecnico, "minuta")
        End If

        If fuUploadMinutaCorretor.HasFile Then
            EnviarArquivoServidor(fuUploadMinutaCorretor, "minutaassinada")
        End If

        'If fuUploadEndosso.HasFile Then
        '    EnviarArquivoServidor(fuUploadEndosso, "endosso")
        'End If

        If fuUploadBoleto.HasFile Then
            If Session("controleClicado") <> 0 Then
                Dim ctrlDAO As Data.cotacao_web_controleDAO = New Data.cotacao_web_controleDAO(linkSeguro)
                Dim _funcoes As Data.Funcoes_gerais = New Data.Funcoes_gerais(linkSeguro)
                Dim ctrlcot As cotacao_controle_web = ctrlDAO.carregarControleCotacaoWeb(Session("controleClicado"))
                Dim _cwebDao As Data.cotacao_webDAO = New Data.cotacao_webDAO(linkSeguro)
                If _funcoes.VerificarQtdeBoletosAnexos(ctrlcot.cotacao_web_id) = _cwebDao.VerificaNumParcelas(ctrlcot.cotacao_web_id) Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Número de boletos anexos atingiu o numero de parcelas definido!\nCaso necessário exclua arquivos anexados indevidamente\nantes de anexar novos documentos!');", True)
                    Exit Sub
                End If
            End If
            EnviarArquivoServidor(fuUploadBoleto, "boleto")
        End If

        btnInspecaoTecnico.Attributes.Add("onclick", "return confirm('Confirma pedido de inspeção\npara a cotação selecionada?');")
        btnExcluir_MinutaTecnico.Attributes.Add("onclick", "return confirm('Confirma a exclusão da minuta anexada?');")
        btnEnviar_EmissaoTecnico.Attributes.Add("onclick", "return confirm('Confirma liberação para emissão\nda cotação selecionada?');")

    End Sub

    Protected Sub gridResultadoPesquisa_RowDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim linkminuta As String
        Dim linkendosso As String
        Dim linkendosso_2 As String
        Dim linkendosso_3 As String
        Dim linkboleto_1 As String
        Dim linkboleto_2 As String
        Dim linkboleto_3 As String
        Dim linkboleto_4 As String
        Dim linkboleto_5 As String
        Dim linkboleto_6 As String
        Dim linkboleto_7 As String

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.CssClass = "grid_apolice_linha"

            If CInt(e.Row.Cells(10).Text) < 0 Then
                e.Row.Cells(10).Text = CInt(e.Row.Cells(10).Text) * -1
                e.Row.Cells(10).ForeColor = Drawing.Color.Red
            Else
                e.Row.Cells(10).ForeColor = Drawing.Color.Black
            End If

            e.Row.Cells(12).Enabled = False
            e.Row.Cells(15).Enabled = False
            e.Row.Cells(19).Enabled = False
            e.Row.Cells(27).Enabled = False

            linkminuta = String.Empty
            Dim rowView As DataRowView = CType(e.Row.DataItem, DataRowView)
            linkminuta = rowView("minuta")
            If linkminuta <> String.Empty Then
                Dim v_oMinuta As HyperLink = CType(e.Row.FindControl("hplVisualiza_Minuta"), HyperLink)
                v_oMinuta.ImageUrl = ".\img\Word-icon.png"
                v_oMinuta.Target = "_self"
                v_oMinuta.NavigateUrl = linkminuta
            End If
            linkminuta = String.Empty
            linkminuta = rowView("minuta_assinada")
            If linkminuta <> String.Empty Then
                Dim v_oMinutaAssinada As HyperLink = CType(e.Row.FindControl("hplVisualiza_Minuta_Assinada"), HyperLink)
                v_oMinutaAssinada.Target = "_blank"
                v_oMinutaAssinada.NavigateUrl = "Cotacao_visualiza_doc.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&caminho=" & linkminuta & "&cotacao_web_controle_id=" & rowView("cotacao_web_controle_id") & "&perfil=" & Session("perfil")

                If System.IO.Path.GetExtension(linkminuta).ToLower = ".pdf" Then
                    v_oMinutaAssinada.ImageUrl = ".\img\pdf-Icon.jpg"
                    If Session("perfil") <> "C" Then
                        v_oMinutaAssinada.NavigateUrl = linkminuta
                    End If
                Else
                    v_oMinutaAssinada.ImageUrl = ".\img\jpg-icon.png"
                End If
            End If

            Dim c_endossos As String = ""

            If (rowView("endosso") <> String.Empty) Then
                c_endossos = rowView("endosso")
            End If

            If (rowView("endosso_2") <> String.Empty) Then
                c_endossos = c_endossos + "|" + rowView("endosso_2").ToString()
            End If

            If (rowView("endosso_3") <> String.Empty) Then
                c_endossos = c_endossos + "|" + rowView("endosso_3").ToString()
            End If

            linkendosso = String.Empty
            linkendosso = rowView("endosso")
            If linkendosso <> String.Empty Then
                Dim v_oEndosso As HyperLink = CType(e.Row.FindControl("hplVisualiza_Endosso"), HyperLink)
                v_oEndosso.ImageUrl = ".\img\pdf-Icon.jpg"
                v_oEndosso.Target = "_blank"

                If (c_endossos.Trim().Length > 0) Then
                    v_oEndosso.NavigateUrl = "Cotacao_visualiza_doc.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&caminho=" & c_endossos & "&cotacao_web_controle_id=" & rowView("cotacao_web_controle_id") & "&perfil=" & Session("perfil")
                Else
                    v_oEndosso.NavigateUrl = "Cotacao_visualiza_doc.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&caminho=" & linkendosso & "&cotacao_web_controle_id=" & rowView("cotacao_web_controle_id") & "&perfil=" & Session("perfil")
                End If


                If Session("perfil") <> "E" Then
                    v_oEndosso.NavigateUrl = linkendosso
                End If
            End If

            'linkendosso_2 = String.Empty
            'linkendosso_2 = rowView("endosso_2")
            'If linkendosso_2 <> String.Empty Then
            '    Dim v_oEndosso As HyperLink = CType(e.Row.FindControl("hplVisualiza_Endosso"), HyperLink)
            '    v_oEndosso.ImageUrl = ".\img\pdf-Icon.jpg"
            '    v_oEndosso.Target = "_blank"
            '    v_oEndosso.NavigateUrl = "Cotacao_visualiza_doc.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&caminho=" & linkendosso & "&cotacao_web_controle_id=" & rowView("cotacao_web_controle_id") & "&perfil=" & Session("perfil")

            '    If Session("perfil") <> "E" Then
            '        v_oEndosso.NavigateUrl = linkendosso
            '    End If

            '    'Dim v_oEndosso_2 As HyperLink = CType(e.Row.FindControl("hplVisualiza_Endosso"), HyperLink)
            '    'v_oEndosso_2.ImageUrl = ".\img\pdf-Icon.jpg"
            '    'v_oEndosso_2.Target = "_blank"
            '    'v_oEndosso_2.NavigateUrl = "Cotacao_visualiza_doc.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&caminho=" & linkendosso_2 & "&cotacao_web_controle_id=" & rowView("cotacao_web_controle_id") & "&perfil=" & Session("perfil")

            '    'If Session("perfil") <> "E" Then
            '    '    v_oEndosso_2.NavigateUrl = linkendosso_2
            '    'End If
            'End If

            'linkendosso_3 = String.Empty
            'linkendosso_3 = rowView("endosso_3")
            'If linkendosso_3 <> String.Empty Then
            '    Dim v_oEndosso_3 As HyperLink = CType(e.Row.FindControl("hplVisualiza_Endosso_3"), HyperLink)
            '    v_oEndosso_3.ImageUrl = ".\img\pdf-Icon.jpg"
            '    v_oEndosso_3.Target = "_blank"
            '    v_oEndosso_3.NavigateUrl = "Cotacao_visualiza_doc.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&caminho=" & linkendosso_3 & "&cotacao_web_controle_id=" & rowView("cotacao_web_controle_id") & "&perfil=" & Session("perfil")

            '    If Session("perfil") <> "E" Then
            '        v_oEndosso_3.NavigateUrl = linkendosso_3
            '    End If
            'End If

            linkboleto_1 = String.Empty
            linkboleto_1 = rowView("boleto_1")
            If linkboleto_1 <> String.Empty Then
                Dim v_oBoleto_1 As HyperLink = CType(e.Row.FindControl("hplVisualiza_Boleto_1"), HyperLink)
                v_oBoleto_1.ImageUrl = ".\img\pdf-Icon-Bol.png"
                v_oBoleto_1.Target = "_blank"
                v_oBoleto_1.NavigateUrl = "Cotacao_visualiza_doc.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&caminho=" & linkboleto_1 & "&cotacao_web_controle_id=" & rowView("cotacao_web_controle_id") & "&perfil=" & Session("perfil")
                If Session("perfil") <> "E" Then
                    v_oBoleto_1.NavigateUrl = linkboleto_1
                End If
            End If
            linkboleto_2 = String.Empty
            linkboleto_2 = rowView("boleto_2")
            If linkboleto_2 <> String.Empty Then
                Dim v_oBoleto_2 As HyperLink = CType(e.Row.FindControl("hplVisualiza_Boleto_2"), HyperLink)
                v_oBoleto_2.ImageUrl = ".\img\pdf-Icon-Bol.png"
                v_oBoleto_2.Target = "_blank"
                v_oBoleto_2.NavigateUrl = "Cotacao_visualiza_doc.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&caminho=" & linkboleto_2 & "&cotacao_web_controle_id=" & rowView("cotacao_web_controle_id") & "&perfil=" & Session("perfil")
                If Session("perfil") <> "E" Then
                    v_oBoleto_2.NavigateUrl = linkboleto_2
                End If
            End If
            linkboleto_3 = String.Empty
            linkboleto_3 = rowView("boleto_3")
            If linkboleto_3 <> String.Empty Then
                Dim v_oBoleto_3 As HyperLink = CType(e.Row.FindControl("hplVisualiza_Boleto_3"), HyperLink)
                v_oBoleto_3.ImageUrl = ".\img\pdf-Icon-Bol.png"
                v_oBoleto_3.Target = "_blank"
                v_oBoleto_3.NavigateUrl = "Cotacao_visualiza_doc.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&caminho=" & linkboleto_3 & "&cotacao_web_controle_id=" & rowView("cotacao_web_controle_id") & "&perfil=" & Session("perfil")
                If Session("perfil") <> "E" Then
                    v_oBoleto_3.NavigateUrl = linkboleto_3
                End If
            End If
            linkboleto_4 = String.Empty
            linkboleto_4 = rowView("boleto_4")
            If linkboleto_4 <> String.Empty Then
                Dim v_oBoleto_4 As HyperLink = CType(e.Row.FindControl("hplVisualiza_Boleto_4"), HyperLink)
                v_oBoleto_4.ImageUrl = ".\img\pdf-Icon-Bol.png"
                v_oBoleto_4.Target = "_blank"
                v_oBoleto_4.NavigateUrl = "Cotacao_visualiza_doc.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&caminho=" & linkboleto_4 & "&cotacao_web_controle_id=" & rowView("cotacao_web_controle_id") & "&perfil=" & Session("perfil")
                If Session("perfil") <> "E" Then
                    v_oBoleto_4.NavigateUrl = linkboleto_4
                End If
            End If
            linkboleto_5 = String.Empty
            linkboleto_5 = rowView("boleto_5")
            If linkboleto_5 <> String.Empty Then
                Dim v_oBoleto_5 As HyperLink = CType(e.Row.FindControl("hplVisualiza_Boleto_5"), HyperLink)
                v_oBoleto_5.ImageUrl = ".\img\pdf-Icon-Bol.png"
                v_oBoleto_5.Target = "_blank"
                v_oBoleto_5.NavigateUrl = "Cotacao_visualiza_doc.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&caminho=" & linkboleto_5 & "&cotacao_web_controle_id=" & rowView("cotacao_web_controle_id") & "&perfil=" & Session("perfil")
                If Session("perfil") <> "E" Then
                    v_oBoleto_5.NavigateUrl = linkboleto_5
                End If
            End If
            linkboleto_6 = String.Empty
            linkboleto_6 = rowView("boleto_6")
            If linkboleto_6 <> String.Empty Then
                Dim v_oBoleto_6 As HyperLink = CType(e.Row.FindControl("hplVisualiza_Boleto_6"), HyperLink)
                v_oBoleto_6.ImageUrl = ".\img\pdf-Icon-Bol.png"
                v_oBoleto_6.Target = "_blank"
                v_oBoleto_6.NavigateUrl = "Cotacao_visualiza_doc.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&caminho=" & linkboleto_6 & "&cotacao_web_controle_id=" & rowView("cotacao_web_controle_id") & "&perfil=" & Session("perfil")
                If Session("perfil") <> "E" Then
                    v_oBoleto_6.NavigateUrl = linkboleto_6
                End If
            End If
            linkboleto_7 = String.Empty
            linkboleto_7 = rowView("boleto_7")
            If linkboleto_7 <> String.Empty Then
                Dim v_oBoleto_7 As HyperLink = CType(e.Row.FindControl("hplVisualiza_Boleto_7"), HyperLink)
                v_oBoleto_7.ImageUrl = ".\img\pdf-Icon-Bol.png"
                v_oBoleto_7.Target = "_blank"
                v_oBoleto_7.NavigateUrl = "Cotacao_visualiza_doc.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&caminho=" & linkboleto_7 & "&cotacao_web_controle_id=" & rowView("cotacao_web_controle_id") & "&perfil=" & Session("perfil")
                If Session("perfil") <> "E" Then
                    v_oBoleto_7.NavigateUrl = linkboleto_7
                End If
            End If
        End If

        Select Case e.Row.RowType
            Case DataControlRowType.Header
                e.Row.Cells(6).Visible = False
                e.Row.Cells(11).Visible = False
                e.Row.Cells(14).Visible = False
                e.Row.Cells(18).Visible = False
                e.Row.Cells(20).Visible = False
                e.Row.Cells(21).Visible = False
                e.Row.Cells(22).Visible = False
                e.Row.Cells(23).Visible = False
                e.Row.Cells(24).Visible = False
                e.Row.Cells(25).Visible = False
                e.Row.Cells(26).Visible = False
                Exit Select
            Case DataControlRowType.DataRow
                e.Row.Cells(6).Visible = False
                e.Row.Cells(11).Visible = False
                e.Row.Cells(14).Visible = False
                e.Row.Cells(18).Visible = False
                e.Row.Cells(20).Visible = False
                e.Row.Cells(21).Visible = False
                e.Row.Cells(22).Visible = False
                e.Row.Cells(23).Visible = False
                e.Row.Cells(24).Visible = False
                e.Row.Cells(25).Visible = False
                e.Row.Cells(26).Visible = False
                Exit Select
            Case DataControlRowType.Footer
                e.Row.Cells(6).Visible = False
                e.Row.Cells(11).Visible = False
                e.Row.Cells(14).Visible = False
                e.Row.Cells(18).Visible = False
                e.Row.Cells(20).Visible = False
                e.Row.Cells(21).Visible = False
                e.Row.Cells(22).Visible = False
                e.Row.Cells(23).Visible = False
                e.Row.Cells(24).Visible = False
                e.Row.Cells(25).Visible = False
                e.Row.Cells(26).Visible = False
                Exit Select
        End Select

        If Session("perfil") = "C" Then
            Select Case e.Row.RowType
                Case DataControlRowType.Header
                    e.Row.Cells(13).Visible = False
                    e.Row.Cells(16).Visible = False
                    Exit Select
                Case DataControlRowType.DataRow
                    e.Row.Cells(13).Visible = False
                    e.Row.Cells(16).Visible = False
                    Exit Select
                Case DataControlRowType.Footer
                    e.Row.Cells(13).Visible = False
                    e.Row.Cells(16).Visible = False
                    Exit Select
            End Select
        End If

    End Sub
    Protected Sub gridResultadoPesquisa_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)

        Dim pagina As Integer

        pagina = e.NewPageIndex

        NavegarGrid(pagina)

    End Sub
    Public Sub NavegarGrid(ByVal pagina As Integer)

        gridResultadoPesquisa.PageIndex = pagina
        Session("indice") = pagina
        preencherGrid()
        Session("controleClicado") = 0
        Session("minutaAnexada") = String.Empty
        habilitarBotoes(Nothing)

    End Sub
    Protected Sub rdbSelecionaCotacao_OnCheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rdb As RadioButton = DirectCast(sender, RadioButton)
        Dim linha As GridViewRow = DirectCast(rdb.NamingContainer, GridViewRow)
        Dim grid As GridView = DirectCast(linha.NamingContainer, GridView)

        If rdb.Checked Then
            For Each linha2 As GridViewRow In grid.Rows
                Dim rdb2 As RadioButton = CType(linha2.Cells(1).FindControl("selecionaCotacao"), CheckBox)
                rdb2.Checked = False
                linha2.CssClass = "grid_apolice_linha"
                linha2.Cells(12).Enabled = False
                linha2.Cells(15).Enabled = False
                linha2.Cells(19).Enabled = False
                linha2.Cells(27).Enabled = False
            Next
            rdb.Checked = True
            linha.CssClass = "grid_apolice_linha_sel"
            If Session("perfil") = "C" Then
                If linha.Cells(6).Text = "3" Then
                    linha.Cells(12).Enabled = True
                End If
            Else
                linha.Cells(12).Enabled = True
            End If
            'linha.Cells(12).Enabled = True
            linha.Cells(15).Enabled = True
            linha.Cells(19).Enabled = True
            linha.Cells(27).Enabled = True
            grid.SelectedIndex = linha.DataItemIndex() - (grid.PageIndex * grid.PageSize)
            Session("controleClicado") = CInt(grid.SelectedValue)
            Session("minutaAnexada") = IIf(linha.Cells(11).Text = "&nbsp;", "", linha.Cells(11).Text)
            Session("mEndosso") = "<B>Protocolo:</B> " + linha.Cells(2).Text.Trim() + "&nbsp;&nbsp;" + " <B>Segurado:</B> " + linha.Cells(5).Text.Trim()
            habilitarBotoes(linha)

        End If
    End Sub

    Protected Sub preencherGrid()

        gridResultadoPesquisa.DataSource = Nothing
        gridResultadoPesquisa.DataBind()
        Ler_LinkSeguro()

        Dim _funcoes As Data.Funcoes_gerais = New Data.Funcoes_gerais(linkSeguro)

        Dim protocolo As String = ""
        Dim apolice As String = ""
        Dim proposta_id As String = ""
        Dim proposta_bb As String = ""
        Dim nome As String = ""

        If Session("tp_filtro") = 1 Then
            protocolo = Session("filtro")
        End If

        If Session("tp_filtro") = 2 Then
            apolice = Session("filtro")
        End If

        If Session("tp_filtro") = 3 Then
            proposta_id = Session("filtro")
        End If

        If Session("tp_filtro") = 4 Then
            proposta_bb = Session("filtro")
        End If

        If Session("tp_filtro") = 5 Then
            nome = Session("filtro")
        End If

        gridResultadoPesquisa.DataSource = _funcoes.ListaCotacaoEndosso(Session("status"), Session("data_inicial"), Session("data_final"), Session("cnpj"), Session("ramo"), protocolo, apolice, proposta_id, proposta_bb, nome, Session("usuario_id"))
        gridResultadoPesquisa.DataBind()

    End Sub

    Private Sub btnRecusarTecnico_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRecusarTecnico.Click

        If Session("controleClicado") = 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Obrigatório escolher uma cotação!');", True)
            Exit Sub
        End If
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Abrir", "abreJanela('Cotacao_web_recusa.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&cotacao_web_controle_id=" & Session("controleClicado") & "',700,500);", True)
    End Sub

    Private Sub EnviarArquivoServidor(ByVal arquivo As FileUpload, ByVal tipo As String)

        If Session("controleClicado") = 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Obrigatório escolher uma cotação!');", True)
            Exit Sub
        End If

        Dim extensao As String
        Dim sTipoArquivos As String = ""
        Dim bPassouExtensao As Boolean = False

        extensao = System.IO.Path.GetExtension(arquivo.FileName).ToLower

        If tipo = "minuta" Then
            sTipoArquivos = "do word"
            If ((extensao = ".doc") Or (extensao = ".docx")) Then
                bPassouExtensao = True
            End If
        End If

        If tipo = "minutaassinada" Then
            sTipoArquivos = "jpeg ou pdf"
            If ((extensao = ".jpeg") Or (extensao = ".jpg") Or (extensao = ".pdf")) Then
                bPassouExtensao = True
            End If
        End If

        If ((tipo = "endosso") Or (tipo = "boleto")) Then
            sTipoArquivos = "pdf"
            If extensao = ".pdf" Then
                bPassouExtensao = True
            End If
        End If

        Dim tamanho As Decimal

        If (arquivo.HasFile) Then
            If bPassouExtensao Then
                tamanho = (arquivo.PostedFile.ContentLength / 1024) / 1024
                If tamanho > 2.0 Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Tamanho do arquivo ultrapassou o limite de 2 MB permitido,\nfavor tentar novamente!');", True)
                Else
                    Try
                        Ler_LinkSeguro()
                        Dim ctrlDAO As Data.cotacao_web_controleDAO = New Data.cotacao_web_controleDAO(linkSeguro)
                        Dim ctrlcot As cotacao_controle_web = ctrlDAO.carregarControleCotacaoWeb(Session("controleClicado"))

                        Dim dt As New DataSet
                        Dim obj_cotacao_webDAO = New Data.cotacao_webDAO(linkSeguro)
                        dt = obj_cotacao_webDAO.ConsultarCotacao(Session("controleClicado"))
                        Dim sApolice As String = dt.Tables(0).Rows(0)("apolice_id")
                        Dim sRamo As String = dt.Tables(0).Rows(0)("ramo_id")
                        dt = Nothing
                        obj_cotacao_webDAO = Nothing

                        Dim Ua As ArquivoUpload = New ArquivoUpload(ctrlcot.cotacao_web_id, tipo, arquivo.FileName, getCaminhoArquivo("ARQUIVOS_COTACAO", linkSeguro.Ambiente), Session("usuario"), sApolice, sRamo)
                        Dim UaDAO As Data.ArquivoUploadDAO = New Data.ArquivoUploadDAO(linkSeguro)
                        arquivo.SaveAs(Ua.caminho & Ua.nome_arquivo)
                        UaDAO.GravarUpload(Ua, getCaminhoArquivo("ARQUIVOS_WEB_COTACAO", linkSeguro.Ambiente))

                        If tipo = "minuta" Then
                            ctrlDAO.AnexarMinuta(Session("controleClicado"), Session("usuario"))
                            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Upload da minuta executado com sucesso!');", True)
                        End If
                        If tipo = "minutaassinada" Then
                            ctrlDAO.AnexarMinutaAssinada(Session("controleClicado"), Session("usuario"))
                            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Upload da minuta assinada executado com sucesso!');", True)
                        End If
                        If tipo = "endosso" Then
                            ctrlDAO.AnexarEndosso(Session("controleClicado"), Session("usuario"))
                            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Upload do endosso executado com sucesso!');", True)
                        End If
                        If tipo = "boleto" Then
                            ctrlDAO.AnexarBoleto(Session("controleClicado"), Session("usuario"))
                            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Upload do boleto executado com sucesso!');", True)
                        End If
                        ctrlDAO = Nothing
                        ctrlcot = Nothing
                        NavegarGrid(Session("indice"))
                    Catch ex As Exception
                        'Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Houve problemas na operação e não foi possivel finalizar o upload,\nfavor tentar novamente!');", True)
                        Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('" & ex.Message.ToString.Replace("'", "").Replace(vbCrLf, "").Replace("\", "\\") & "');", True)
                    End Try
                End If
            Else
                Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Arquivo com formato incompatível, favor importar arquivos com\nformato " & sTipoArquivos & "!');", True)
            End If
        Else
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('O referido arquivo não foi localizado no caminho informado, favor\nverificar o caminho e o nome correto!');", True)
        End If

    End Sub

    Private Sub btnExcluir_MinutaTecnico_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcluir_MinutaTecnico.Click

        Ler_LinkSeguro()

        If Session("controleClicado") = 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Obrigatório escolher uma cotação!');", True)
            Exit Sub
        End If
        If Session("minutaAnexada") = String.Empty Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Cotação sem minuta anexada!');", True)
            Exit Sub
        End If

        'If MsgBox("Confirma a exclusão da minuta anexada?", MsgBoxStyle.YesNoCancel, "Exclusão da minuta anexada") = MsgBoxResult.Yes Then
        Dim minuta_exclui As Data.ArquivoUploadDAO = New Data.ArquivoUploadDAO(linkSeguro)

        Dim nome_arquivo As String
        nome_arquivo = System.IO.Path.GetFileName(Session("minutaAnexada"))

        Dim msgAlerta As String

        If minuta_exclui.ExcluirDocumento(Session("controleClicado"), nome_arquivo) Then
            Dim ctrlDAO As Data.cotacao_web_controleDAO = New Data.cotacao_web_controleDAO(linkSeguro)
            ctrlDAO.ExcluirMinuta(Session("controleClicado"), Session("usuario"))
            ctrlDAO = Nothing
            File.Delete(Session("minutaAnexada").ToString.Replace(getCaminhoArquivo("ARQUIVOS_WEB_COTACAO", linkSeguro.Ambiente), getCaminhoArquivo("ARQUIVOS_COTACAO", linkSeguro.Ambiente)).Replace("/", "\"))
            msgAlerta = "Minuta da cotação excluida com sucesso!"
            NavegarGrid(Session("indice"))
        Else
            msgAlerta = "Problemas ao excluir minuta da cotação" & vbNewLine & "favor tentar novamente!"
        End If

        Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('" & msgAlerta & "');", True)
        'End If
    End Sub

    Private Sub btnEnviar_MinutaTecnico_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEnviar_MinutaTecnico.Click
        If Session("minutaAnexada") = String.Empty Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Cotação sem minuta anexada!');", True)
            Exit Sub
        End If

        If Session("controleClicado") = 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Obrigatório escolher uma cotação!');", True)
            Exit Sub
        End If
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Abrir", "abreJanela('Minuta_Enviar.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&cotacao_web_controle_id=" & Session("controleClicado") & "',350,100);", True)

    End Sub

    Private Sub btnExigenciaTecnico_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExigenciaTecnico.Click
        If Session("controleClicado") = 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Obrigatório escolher uma cotação!');", True)
            Exit Sub
        End If
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Abrir", "abreJanela('Cotacao_web_exigencia.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&cotacao_web_controle_id=" & Session("controleClicado") & "',700,220);", True)

    End Sub

    Private Sub btnInspecaoTecnico_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInspecaoTecnico.Click

        Ler_LinkSeguro()

        If Session("controleClicado") = 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Obrigatório escolher uma cotação!');", True)
            Exit Sub
        End If

        'If MsgBox("Confirma pedido de inspeção" & vbNewLine & "para a cotação selecionada?", MsgBoxStyle.YesNoCancel, "Solicita inspeção") = MsgBoxResult.Yes Then

        Dim ctrlDAO As Data.cotacao_web_controleDAO = New Data.cotacao_web_controleDAO(linkSeguro)
        ctrlDAO.ColocarEmInspecao(Session("controleClicado"), Session("usuario"), linkSeguro)
        ctrlDAO = Nothing

        Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Cotação definida para inspeção com sucesso!');", True)
        NavegarGrid(Session("indice"))
        'End If
    End Sub

    Private Sub btnEnviar_EmissaoTecnico_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEnviar_EmissaoTecnico.Click

        Ler_LinkSeguro()

        If Session("controleClicado") = 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Obrigatório escolher uma cotação!');", True)
            Exit Sub
        End If

        'If MsgBox("Confirma liberação para emissão" & vbNewLine & "da a cotação selecionada?", MsgBoxStyle.YesNoCancel, "Libera emissão") = MsgBoxResult.Yes Then
        Dim ctrlDAO As Data.cotacao_web_controleDAO = New Data.cotacao_web_controleDAO(linkSeguro)
        ctrlDAO.EnviarParaEmissao(Session("controleClicado"), Session("usuario"), linkSeguro)
        ctrlDAO = Nothing

        Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Cotação enviada para emissão com sucesso!');", True)
        NavegarGrid(Session("indice"))
        'End If
    End Sub

    Private Sub btnConsultarRecusaCorretor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsultarRecusaCorretor.Click
        If Session("controleClicado") = 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Obrigatório escolher uma cotação!');", True)
            Exit Sub
        End If
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Abrir", "abreJanela('Cotacao_web_consulta_recusa.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&cotacao_web_controle_id=" & Session("controleClicado") & "',380,185);", True)

    End Sub

    Private Sub btnConsultarExigenciaCorretor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsultarExigenciaCorretor.Click
        If Session("controleClicado") = 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Obrigatório escolher uma cotação!');", True)
            Exit Sub
        End If
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Abrir", "abreJanela('Cotacao_web_consulta_exigencia.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&cotacao_web_controle_id=" & Session("controleClicado") & "&perfil=" & Session("perfil") & "',380,185);", True)

    End Sub

    Private Sub btnRecalcularCorretor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRecalcularCorretor.Click

        Ler_LinkSeguro()

        If Session("controleClicado") = 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Obrigatório escolher uma cotação!');", True)
            Exit Sub
        End If

        Dim cotacao_web_controle_id As String
        cotacao_web_controle_id = Session("controleClicado")

        Dim dt As New DataSet
        Dim obj_cotacao_webDAO = New Data.cotacao_webDAO(linkSeguro)
        dt = obj_cotacao_webDAO.ConsultarCotacao(cotacao_web_controle_id)

        Dim iStatus_Cotacao_Id As Integer = dt.Tables(0).Rows(0)("status_cotacao_id")

        dt = Nothing
        obj_cotacao_webDAO = Nothing

        If iStatus_Cotacao_Id = 4 Then
            abrirJanelaCotacao("recalculo")
        Else
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Já aplicado recálculo para a cotação selecionada!');", True)
        End If

    End Sub

    Private Sub btnRetornarUnidadeTecnica_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRetornarUnidadeTecnica.Click
        If Session("controleClicado") = 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Obrigatório escolher uma cotação!');", True)
            Exit Sub
        End If
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Abrir", "abreJanela('Cotacao_web_retorna_ut.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&cotacao_web_controle_id=" & Session("controleClicado") & "',700,290);", True)

    End Sub

    Private Sub btnConcluir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConcluir.Click
        If Session("controleClicado") = 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Obrigatório escolher uma cotação!');", True)
            Exit Sub
        End If
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Abrir", "abreJanela('Cotacao_web_concluir.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&cotacao_web_controle_id=" & Session("controleClicado") & "',350,100);", True)

    End Sub

    Private Sub abrirJanelaCotacao(ByVal acao As String)
        If Session("controleClicado") = 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Obrigatório escolher uma cotação!');", True)
            Exit Sub
        End If

        Page.ClientScript.RegisterStartupScript(Me.GetType, "Abrir", "abreJanelaCotacao('Apolice_cotacao.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&acao=" & acao & "&cotacao_web_controle_id=" & Session("controleClicado") & "',1055,700);", True)

    End Sub

    Protected Sub habilitarBotoes(ByVal linha As GridViewRow)

        btnAnexar_Boleto.Enabled = False
        fuUploadBoleto.Enabled = False

        btnAnexar_Endosso.Enabled = False
        'fuUploadEndosso.Enabled = True

        btnAnexar_MinutaCorretor.Enabled = False
        fuUploadMinutaCorretor.Enabled = False

        btnAnexar_MinutaTecnico.Enabled = False
        fuUploadMinutaTecnico.Enabled = False

        btnConcluir.Enabled = False
        btnConsultarCorretor.Enabled = False
        btnConsultarEmissao.Enabled = False
        btnConsultarExigenciaCorretor.Enabled = False
        btnConsultarRecusaCorretor.Enabled = False
        btnConsultarTecnico.Enabled = False

        btnConsultarExigenciaEmissaoUT.Enabled = False

        btnConsultarExigenciaTecnico.Enabled = False
        btnConsultarRecusaTecnico.Enabled = False

        BtnConsultarDocumentosTecnico.Enabled = False

        btnEnviar_EmissaoTecnico.Enabled = False

        btnEnviar_MinutaTecnico.Enabled = False
        btnExcluir_MinutaTecnico.Enabled = False
        btnExigenciaTecnico.Enabled = False
        btnImprimirCartaBenefCorretor.Enabled = False
        btnInspecaoTecnico.Enabled = False
        btnRecalcularCorretor.Enabled = False
        btnRecusarTecnico.Enabled = False
        btnRetornarUnidadeTecnica.Enabled = False

        If Not (linha Is Nothing) Then
            If Session("perfil") = "T" Then
                btnConsultarTecnico.Enabled = True
                BtnConsultarDocumentosTecnico.Enabled = True
                If linha.Cells(6).Text = 1 _
                Or linha.Cells(6).Text = 2 _
                Or linha.Cells(6).Text = 5 _
                Or linha.Cells(6).Text = 10 Then
                    btnRecusarTecnico.Enabled = True
                    btnExigenciaTecnico.Enabled = True
                    If linha.Cells(11).Text = "&nbsp;" Then
                        fuUploadMinutaTecnico.Enabled = True
                        btnAnexar_MinutaTecnico.Enabled = True
                    Else
                        btnExcluir_MinutaTecnico.Enabled = True
                        btnEnviar_MinutaTecnico.Enabled = True
                    End If
                    If linha.Cells(6).Text <> 5 Then
                        btnInspecaoTecnico.Enabled = True
                    End If
                End If
                If linha.Cells(6).Text = 1 _
                Or linha.Cells(6).Text = 2 _
                Or linha.Cells(6).Text = 10 Then
                    btnEnviar_EmissaoTecnico.Enabled = True
                End If
                If linha.Cells(6).Text = 8 Then
                    btnConsultarRecusaTecnico.Enabled = True
                End If
                If linha.Cells(6).Text = 4 Then
                    btnConsultarExigenciaTecnico.Enabled = True
                End If
                If linha.Cells(6).Text = 10 Then
                    btnConsultarExigenciaEmissaoUT.Enabled = True
                End If
            End If

            If Session("perfil") = "C" Then
                btnConsultarCorretor.Enabled = True
                If linha.Cells(6).Text = 8 Then
                    btnConsultarRecusaCorretor.Enabled = True
                End If
                If linha.Cells(6).Text = 4 Then
                    btnConsultarExigenciaCorretor.Enabled = True
                    btnRecalcularCorretor.Enabled = True
                End If
                If linha.Cells(4).Text = 18 _
                Or linha.Cells(4).Text = 67 _
                Or linha.Cells(4).Text = 71 _
                Or linha.Cells(4).Text = 96 Then
                    If verificaCartaBenefeciaria() Then
                        btnImprimirCartaBenefCorretor.Enabled = True
                    End If
                End If
                If linha.Cells(6).Text = 3 Then
                    If linha.Cells(14).Text = "&nbsp;" Then
                        fuUploadMinutaCorretor.Enabled = True
                        btnAnexar_MinutaCorretor.Enabled = True
                    End If
                End If
            End If

            If Session("perfil") = "E" Then
                btnConsultarEmissao.Enabled = True
                If linha.Cells(6).Text = 6 Then
                    btnRetornarUnidadeTecnica.Enabled = True
                    btnConcluir.Enabled = True
                End If
                If linha.Cells(6).Text = 7 Then
                    If linha.Cells(18).Text = "&nbsp;" Then
                        ''fuUploadEndosso.Enabled = True
                        btnAnexar_Endosso.Enabled = True
                    End If
                    fuUploadBoleto.Enabled = True
                    btnAnexar_Boleto.Enabled = True
                End If
            End If
        End If

    End Sub

    Private Sub btnConsultarTecnico_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsultarTecnico.Click
        abrirJanelaCotacao("consulta")
    End Sub

    Private Sub btnConsultarEmissao_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsultarEmissao.Click
        abrirJanelaCotacao("consulta")
    End Sub

    Private Sub btnConsultarCorretor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsultarCorretor.Click
        abrirJanelaCotacao("consulta")
    End Sub
    Private Function verificaCartaBenefeciaria() As Boolean

        Ler_LinkSeguro()

        Dim _cotacao_web_controleDAO As Data.cotacao_web_controleDAO = New Data.cotacao_web_controleDAO(linkSeguro)
        Dim _cotacao_web_controle As cotacao_controle_web
        _cotacao_web_controle = _cotacao_web_controleDAO.carregarControleCotacaoWeb(Session("controleClicado"))
        Dim _funcoes As Data.Funcoes_gerais = New Data.Funcoes_gerais(linkSeguro)

        Return _funcoes.VerificarCartaBenefeciario(_cotacao_web_controle.cotacao_web_id)

    End Function

    Private Sub btnImprimirCartaBenefCorretor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImprimirCartaBenefCorretor.Click

        Ler_LinkSeguro()

        Dim _funcoes As Data.Funcoes_gerais = New Data.Funcoes_gerais(linkSeguro)
        Dim carta As String

        Dim _cotacao_web_controleDAO As Data.cotacao_web_controleDAO = New Data.cotacao_web_controleDAO(linkSeguro)
        Dim _cotacao_web_controle As cotacao_controle_web
        _cotacao_web_controle = _cotacao_web_controleDAO.carregarControleCotacaoWeb(Session("controleClicado"))

        carta = _funcoes.MontaCartaBenefeciario(_cotacao_web_controle.cotacao_web_id)

        'Page.ClientScript.RegisterStartupScript(Me.GetType, "Imprimir", "ImprimeCarta('" & carta & "');", True)

    End Sub

    Private Sub btnAtualizarGrid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAtualizarGrid.Click
        NavegarGrid(Session("indice"))
    End Sub

    Protected Function getCaminhoArquivo(ByVal sTipo_leitura As String, ByVal ambiente_id As Integer) As String

        Ler_LinkSeguro()

        Dim _funcoes As New Data.Funcoes_gerais(linkSeguro)

        Return _funcoes.Busca_Caminho(sTipo_leitura, ambiente_id)

    End Function

    Private Sub btnConsultarExigenciaEmissaoUT_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsultarExigenciaEmissaoUT.Click
        If Session("controleClicado") = 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Obrigatório escolher uma cotação!');", True)
            Exit Sub
        End If
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Abrir", "abreJanela('Cotacao_web_consulta_exigencia_emissao.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&cotacao_web_controle_id=" & Session("controleClicado") & "',380,185);", True)

    End Sub

    Private Sub btnConsultarExigenciaTecnico_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsultarExigenciaTecnico.Click
        If Session("controleClicado") = 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Obrigatório escolher uma cotação!');", True)
            Exit Sub
        End If
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Abrir", "abreJanela('Cotacao_web_consulta_exigencia.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&cotacao_web_controle_id=" & Session("controleClicado") & "&perfil=" & Session("perfil") & "',380,185);", True)

    End Sub

    Private Sub btnConsultarRecusaTecnico_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsultarRecusaTecnico.Click
        If Session("controleClicado") = 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Obrigatório escolher uma cotação!');", True)
            Exit Sub
        End If
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Abrir", "abreJanela('Cotacao_web_consulta_recusa.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&cotacao_web_controle_id=" & Session("controleClicado") & "',380,185);", True)

    End Sub
    Private Sub btnConsultarDocumentoTecnico_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnConsultarDocumentosTecnico.Click
        If Session("controleClicado") = 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Obrigatório escolher uma cotação!');", True)
            Exit Sub
        End If
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Abrir", "abreJanelaCotacao('Cotacao_web_consulta_documentos.aspx?SLinkSeguro=" & Session("SLinkSeguro") & "&cotacao_web_controle_id=" & Session("controleClicado") & "',1100,550);", True)

    End Sub

    Private Sub Ler_LinkSeguro()
        linkSeguro.LerUsuario( _
                Request.Url.Segments(Request.Url.Segments.Length - 1), _
                Request.UserHostAddress, _
                Session("SLinkSeguro"))
    End Sub

    Private Sub btnAnexar_Endosso_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnexar_Endosso.Click

        Session("cotacao_web_id") = 0
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "key", "launchModal();", True)
        'Page.ClientScript.RegisterStartupScript(Me.GetType, "Abrir", "window.open('upload.aspx','Upload de Arquivos','height=700,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no ,modal=yes');", True)
        '?SLinkSeguro=" & Session("SLinkSeguro") & "&cotacao_web_controle_id=" & Session("controleClicado") & "',700,500);", True)
    End Sub
End Class