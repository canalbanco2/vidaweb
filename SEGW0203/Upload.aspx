<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Upload.aspx.vb" Inherits="SEGW0203.Upload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Upload dos arquivos de endosso</title>
    <link href="css/global.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel ID="pnltopo" runat="server">
            <table background="img/imgbannertopofundo.png" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right" height="60" style="background-position: left top; background-image: url(img/imgBannerTopo.gif);
                        background-repeat: no-repeat" valign="top">
                    </td>
                </tr>
            </table>
            <table background="img/imgFundoMenuTopo.gif" border="0" cellpadding="0" cellspacing="0" height="26" width="100%">
                <tr>
                    <td>
                        <a class="TituloMenuTopo" href="JavaScript:window.parent.close();">&nbsp; &nbsp; &nbsp;Sair
                            &nbsp; &nbsp;&nbsp;</a> &nbsp;</td>
                    <td class="DadosSistemaUsuario" style="width: 250px; text-align: right">
                        <asp:Label ID="lblDataCorrente" runat="server"></asp:Label>&nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
        
        <div id="corpo">
        <br />
        <table background="img/imgFundoMenuTopo.gif" border="0" cellpadding="0" cellspacing="0" height="26" width="100%">
            <tr>
                <td>&nbsp;&nbsp;&nbsp;<asp:Label ID="lbTitulo" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;Selecionar arquivo(s) para Upload</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;<asp:FileUpload id="FileUpload1" runat="server" Width="448px"/><br /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;<asp:FileUpload id="FileUpload2" runat="server" Width="448px"/><br /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;<asp:FileUpload id="FileUpload3" runat="server" Width="448px"/><br /></td>
            </tr>
            <tr>
                <td style="height: 31px"><asp:Button ID="btnUpload" runat="server" Text="Enviar... " CssClass="Botao_pesquisa"/></td>
            </tr>
        </table>
        
        </div>
     
    </form>
</body>
</html>
