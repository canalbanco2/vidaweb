﻿Public Partial Class Cotacao_web_consulta_recusa
    Inherits System.Web.UI.Page
    Dim linkseguro As New Alianca.Seguranca.Web.LinkSeguro

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        linkseguro.LerUsuario( _
                            Request.Url.Segments(Request.Url.Segments.Length - 1), _
                            Request.UserHostAddress, _
                            Request.QueryString("SLinkSeguro"))

        txtMotivoRecusa.Text = RetornaMotivoRecusa(Request.QueryString("cotacao_web_controle_id"))

    End Sub

    Private Function RetornaMotivoRecusa(ByVal cotacao_web_controle_id As Integer) As String

        Dim motivo As Data.motivo_recusaDAO = New Data.motivo_recusaDAO(linkseguro)

        RetornaMotivoRecusa = motivo.PesquisarMotivosDaCotacao(cotacao_web_controle_id)

    End Function

    Private Sub btnSair_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "fechajanela();", True)
    End Sub
End Class