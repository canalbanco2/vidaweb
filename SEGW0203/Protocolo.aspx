﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="protocolo.aspx.vb" Inherits="SEGW0203._Protocolo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Protocolo</title>
    <link href="css/global.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript" src="js/principal.js"></script>
    <%--<script>window.onload = function(){document.getElementById("btnimprime_carta").click();}</script>--%>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel runat="server" ID="pnltopo">
        <table background="img/imgbannertopofundo.png" border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
                <td align="right" height="60" style="background-position: left top; background-image: url(img/imgBannerTopo.gif);
                    background-repeat: no-repeat" valign="top">
                </td>
            </tr>
        </table>
        <table background="img/imgFundoMenuTopo.gif" border="0" cellpadding="0" cellspacing="0"
            height="26" width="100%">
            <tr>
                <td>
                    <a class="TituloMenuTopo" href="JavaScript:window.parent.close();">&nbsp; &nbsp; &nbsp;Sair
                        &nbsp; &nbsp;&nbsp;</a> &nbsp;</td>
                <td class="DadosSistemaUsuario" style="width: 250px; text-align: right">
                    <asp:Label ID="lblDataCorrente" runat="server"></asp:Label>&nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
    <div id="corpo">
        <asp:Table ID="Table1" runat="server" Height="228px" Width="390px" HorizontalAlign = "Center" >
            <asp:TableRow runat="server">
                <asp:TableCell ID="TableCell1" runat="server" VerticalAlign = "Middle" >
                <asp:Label ID="lbl1" runat = "server" Text = "Protocolo Envio Pedido de Cotação de Endosso (Seguros Gerais)" />
                <br />
                <br />
                <br />
                <br />
                O número de protocolo é: 
                <br />
                <asp:Label ID="lblNumeroProtocolo" runat="server" Font-Size="X-Small" ></asp:Label>
                <br />
                <br />
                <br />
                <br />
                <asp:Label ID="lbl2" runat = "server" Text = "Para obter mais informações entrar em contato com o ASA - Atendimento a Solicitações do Atacado, telefone 011 3149-2650." />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="TableRow1" runat="server">
                <asp:TableCell ID="TableCell3" runat="server" HorizontalAlign = "Center" Height = "20px" >
                    <asp:Button ID="btnOk" runat="server" Text="Ok" OnClick = "btnOk_click" CssClass = "botao" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:Button ID="btnimprime_carta" runat="server" Text="" CssClass="upload-file" />
    </div>
    </form>
</body>
</html>
