Public Partial Class Upload
    Inherits System.Web.UI.Page
    Dim linkSeguro As New Alianca.Seguranca.Web.LinkSeguro

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim str As String = System.Web.HttpContext.Current.Session("mEndosso")

            If (Not String.IsNullOrEmpty(str)) Then
                lbTitulo.Text = Session("mEndosso").ToString()
            End If

            'Dim text As String

            'linkSeguro.LerUsuario( _
            '                Request.Url.Segments(Request.Url.Segments.Length - 1), _
            '                Request.UserHostAddress, _
            '                Request.QueryString("SLinkSeguro"))

            'text = "usuario: " + linkSeguro.Login_WEB + "\n"
            'text = text + "perfil: " + Session("perfil") + "\n"
            'text = text + "controle: " + Session("controleClicado").ToString()
        End If

    End Sub
    Private Sub Ler_LinkSeguro()
        linkSeguro.LerUsuario( _
                Request.Url.Segments(Request.Url.Segments.Length - 1), _
                Request.UserHostAddress, _
                Session("SLinkSeguro"))
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Try
            Dim fOk As Boolean = True
            Dim tamanho As Decimal
            Dim extensao As String
            Dim mensagem As String = ""
            Dim enviado As Boolean = False

            If ((Not FileUpload1.HasFile) And (Not FileUpload2.HasFile) And (Not FileUpload3.HasFile)) Then
                mensagem = "Por favor, informar um ou mais arquivos para realizar o upload \n"
            End If

            If (FileUpload1.HasFile) Then
                tamanho = (FileUpload1.PostedFile.ContentLength / 1024) / 1024

                If (tamanho > 4.0) Then
                    mensagem = "Arquivo " + FileUpload1.FileName + " ultrapassou o limite de tamanho de 4 MB \n"
                    fOk = False
                End If

                extensao = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower

                If extensao <> ".pdf" Then
                    mensagem = mensagem + "Arquivo " + FileUpload1.FileName + " n�o � permitidos, utilize apenas arquivos no formato PDF\n"
                    fOk = False
                End If
            End If

            If (FileUpload2.HasFile) Then
                tamanho = (FileUpload2.PostedFile.ContentLength / 1024) / 1024

                If (tamanho > 4.0) Then
                    mensagem = mensagem = "Arquivo " + FileUpload2.FileName + " ultrapassou o limite de tamanho de 4 MB \n"
                    fOk = False
                End If

                extensao = System.IO.Path.GetExtension(FileUpload2.FileName).ToLower

                If extensao <> ".pdf" Then
                    mensagem = mensagem + "Arquivo " + FileUpload2.FileName + " n�o � permitidos, utilize apenas arquivos no formato PDF\n"
                    fOk = False
                End If
            End If

            If (FileUpload3.HasFile) Then
                tamanho = (FileUpload3.PostedFile.ContentLength / 1024) / 1024

                If (tamanho > 4.0) Then
                    mensagem = mensagem = "Arquivo " + FileUpload3.FileName + " ultrapassou o limite de tamanho de 4 MB \n"
                    fOk = False
                End If

                extensao = System.IO.Path.GetExtension(FileUpload3.FileName).ToLower

                If extensao <> ".pdf" Then
                    mensagem = mensagem + "Arquivo " + FileUpload3.FileName + " n�o � permitidos, utilize apenas arquivos no formato PDF\n"
                    fOk = False
                End If
            End If


            If (fOk) Then
                Dim msg As String
                enviado = True

                If (FileUpload1.HasFile) Then
                    msg = EnviarArquivoServidor(FileUpload1)

                    If (msg <> "OK") Then
                        enviado = False
                        mensagem = mensagem + msg
                    End If
                End If

                If (FileUpload2.HasFile) Then
                    msg = EnviarArquivoServidor(FileUpload2)

                    If (msg <> "OK") Then
                        enviado = False
                        mensagem = mensagem + msg
                    End If
                End If

                If (FileUpload3.HasFile) Then
                    msg = EnviarArquivoServidor(FileUpload3)

                    If (msg <> "OK") Then
                        enviado = False
                        mensagem = mensagem + msg
                    End If
                End If
            Else
                enviado = False
            End If

            If enviado Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "Abrir", "window.alert('Upload realizado com sucesso !');window.setTimeout('window.close();',1500);", True)
            End If

        Catch ex As Exception
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('" + ex.Message.Trim() + "');", True)
        End Try
    End Sub

    Private Function EnviarArquivoServidor(ByVal arquivo As FileUpload) As String
        Dim msgRetorno As String = "OK"

        Ler_LinkSeguro()
        Dim ctrlDAO As Data.cotacao_web_controleDAO = New Data.cotacao_web_controleDAO(linkSeguro)
        Dim ctrlcot As cotacao_controle_web = ctrlDAO.carregarControleCotacaoWeb(Session("controleClicado"))

        Dim dt As New DataSet
        Dim obj_cotacao_webDAO = New Data.cotacao_webDAO(linkSeguro)
        dt = obj_cotacao_webDAO.ConsultarCotacao(Session("controleClicado"))
        Dim sApolice As String = dt.Tables(0).Rows(0)("apolice_id")
        Dim sRamo As String = dt.Tables(0).Rows(0)("ramo_id")
        dt = Nothing
        obj_cotacao_webDAO = Nothing

        Dim Ua As ArquivoUpload = New ArquivoUpload(ctrlcot.cotacao_web_id, "endosso", arquivo.FileName, getCaminhoArquivo("ARQUIVOS_COTACAO", linkSeguro.Ambiente), Session("usuario"), sApolice, sRamo)
        Dim UaDAO As Data.ArquivoUploadDAO = New Data.ArquivoUploadDAO(linkSeguro)

        arquivo.SaveAs(Replace(Ua.caminho, "\", "\\") & Ua.nome_arquivo)

        If (System.IO.File.Exists(Ua.caminho & Ua.nome_arquivo) = False) Then
            msgRetorno = "N�o foi poss�vel realizar o upload do arquivo " + arquivo.FileName
        End If

        ctrlDAO.AnexarEndosso(Session("controleClicado"), Session("usuario"))
        UaDAO.GravarUpload(Ua, getCaminhoArquivo("ARQUIVOS_WEB_COTACAO", linkSeguro.Ambiente))

        Return msgRetorno

    End Function

    Protected Function getCaminhoArquivo(ByVal sTipo_leitura As String, ByVal ambiente_id As Integer) As String

        Ler_LinkSeguro()

        Dim _funcoes As New Data.Funcoes_gerais(linkSeguro)

        Return _funcoes.Busca_Caminho(sTipo_leitura, ambiente_id)

    End Function
End Class