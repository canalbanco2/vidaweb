﻿Imports System.IO
Imports System.Data
Imports System.Drawing
Imports System.Configuration
Partial Public Class Pesquisa_cotacao_rastreio
    Inherits System.Web.UI.Page
    Dim linkSeguro As New Alianca.Seguranca.Web.LinkSeguro

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        linkSeguro.LerUsuario( _
                            Request.Url.Segments(Request.Url.Segments.Length - 1), _
                            Request.UserHostAddress, _
                            Request.QueryString("SLinkSeguro"))

        lblDataCorrente.Text = Format(Date.Now, "dddd, dd " & "DE" & " MMMM " & "DE" & " yyyy").ToLower
        
        If Not IsPostBack Then
            preencherGrid()
        End If

    End Sub

    Protected Sub preencherGrid()

        gridResultadoRastreio.DataSource = Nothing
        gridResultadoRastreio.DataBind()

        Dim _funcoes As Data.Funcoes_gerais = New Data.Funcoes_gerais(linkSeguro)

        Dim protocolo As String = ""
        Dim apolice As String = ""
        Dim proposta_id As String = ""
        Dim proposta_bb As String = ""
        Dim nome As String = ""

        If Session("tp_filtro") = 1 Then
            protocolo = Session("filtro")
        End If

        If Session("tp_filtro") = 2 Then
            apolice = Session("filtro")
        End If

        If Session("tp_filtro") = 3 Then
            proposta_id = Session("filtro")
        End If

        If Session("tp_filtro") = 4 Then
            proposta_bb = Session("filtro")
        End If

        If Session("tp_filtro") = 5 Then
            nome = Session("filtro")
        End If

        gridResultadoRastreio.DataSource = _funcoes.ListagemdeRastreio(Session("status"), Session("data_inicial"), Session("data_final"), Session("cnpj"), Session("ramo"), protocolo, apolice, proposta_id, proposta_bb, nome, Session("prazo"), Session("minuta"), Session("usuario_id"))
        gridResultadoRastreio.DataBind()

        If gridResultadoRastreio.Rows.Count = 0 Then
            btnExportarExcel.Visible = False
        End If

    End Sub
    Protected Sub gridResultadoRastreio_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        gridResultadoRastreio.PageIndex = e.NewPageIndex
        Me.preencherGrid()
    End Sub

    Private Sub btnExportarExcel_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportarExcel.ServerClick
        Response.Clear()
        Response.Buffer = True
        Response.Charset = "UTF-8"
        Response.ContentType = "application/vnd.ms-excel"
        Response.ContentEncoding = Encoding.Default
        Response.AppendHeader("content-disposition", "attachment;filename=Rastreio_" & Now.Day.ToString.PadLeft(2, "0") & Now.Month.ToString.PadLeft(2, "0") & Now.Year & "_" & Now.Hour.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0") & Now.Millisecond.ToString.PadLeft(2, "0") & "_" & ".xls")

        Using sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)

            gridResultadoRastreio.AllowPaging = False
            Me.preencherGrid()

            gridResultadoRastreio.HeaderRow.BackColor = Color.White
            For Each cell As TableCell In gridResultadoRastreio.HeaderRow.Cells
                cell.BackColor = gridResultadoRastreio.HeaderStyle.BackColor
            Next
            For Each row As GridViewRow In gridResultadoRastreio.Rows
                row.BackColor = Color.White
                For Each cell As TableCell In row.Cells
                    If row.RowIndex Mod 2 = 0 Then
                        cell.BackColor = gridResultadoRastreio.AlternatingRowStyle.BackColor
                    Else
                        cell.BackColor = gridResultadoRastreio.RowStyle.BackColor
                    End If
                    cell.CssClass = "textmode"
                Next
            Next

            gridResultadoRastreio.RenderControl(hw)

            Dim style As String = "<style> .textmode { mso-number-format:\@; } </style>"
            Response.Write(style)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.[End]()
        End Using
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
        ' Verifies that the control is rendered
    End Sub

    Private Sub gridResultadoRastreio_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gridResultadoRastreio.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            If CInt(e.Row.Cells(3).Text) < 0 Then
                e.Row.Cells(3).Text = CInt(e.Row.Cells(3).Text) * -1
                e.Row.Cells(3).ForeColor = Drawing.Color.Red
            Else
                e.Row.Cells(3).ForeColor = Drawing.Color.Black
            End If

        End If

    End Sub
End Class