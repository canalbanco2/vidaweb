﻿Public Partial Class Cotacao_web_recusa
    Inherits System.Web.UI.Page
    Dim linkseguro As New Alianca.Seguranca.Web.LinkSeguro

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        linkseguro.LerUsuario( _
                Request.Url.Segments(Request.Url.Segments.Length - 1), _
                Request.UserHostAddress, _
                Request.QueryString("SLinkSeguro"))

        Session("usuario") = linkseguro.Login_WEB

        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, linkseguro.Ambiente())
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção)
        Else '
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, linkseguro.Ambiente())
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção)
        End If

        txtOutros.Enabled = False

        If Not IsPostBack Then
            BuscarMotivosRecusa()
        End If

    End Sub

    Protected Sub gridopcoesrecusa_RowDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.CssClass = "grid_apolice_linha"
        End If
        Select Case e.Row.RowType
            Case DataControlRowType.Header
                e.Row.Cells(1).Visible = False
                Exit Select
            Case DataControlRowType.DataRow
                e.Row.Cells(1).Visible = False
                Exit Select
            Case DataControlRowType.Footer
                e.Row.Cells(1).Visible = False
                Exit Select
        End Select

    End Sub

    Protected Sub chkselecionaRecusa_OnCheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim chk As CheckBox = DirectCast(sender, CheckBox)
        Dim linha As GridViewRow = DirectCast(chk.NamingContainer, GridViewRow)
        Dim grid As GridView = DirectCast(linha.NamingContainer, GridView)

        If chk.Checked Then
            chk.Checked = True
            linha.CssClass = "grid_apolice_linha_sel"
            'linha.BackColor = Drawing.Color.Beige
            grid.SelectedIndex = linha.DataItemIndex()
        Else
            chk.Checked = False
            linha.CssClass = "grid_apolice_linha"
        End If

        txtOutros.Enabled = False

        For Each linha2 As GridViewRow In grid.Rows
            Dim chk2 As CheckBox = CType(linha2.Cells(1).FindControl("chkSelecionaRecusa"), CheckBox)
            If chk2.Checked And (CInt(linha2.Cells(1).Text) = 13) Then
                txtOutros.Enabled = True
            End If
        Next

    End Sub


    Private Sub BuscarMotivosRecusa()
        Dim Motivos As Data.motivo_recusaDAO
        Motivos = New Data.motivo_recusaDAO(linkseguro)

        gridopcoesrecusa.DataSource = Nothing
        gridopcoesrecusa.DataBind()

        gridopcoesrecusa.DataSource = Motivos.PesquisarMotivos
        gridopcoesrecusa.DataBind()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "fechajanela();", True)
    End Sub

    Private Sub btnConfirmar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirmar.Click
        Dim gravaControle As New Data.controle_recusa_cotacaoDAO(linkseguro)
        Dim atualizaControle As New Data.cotacao_web_controleDAO(linkseguro)
        Dim iGravouMotivoRecusa As Integer
        Dim iNovoControle As Integer

        iGravouMotivoRecusa = 0

        For Each linha As GridViewRow In gridopcoesrecusa.Rows
            Dim chk As CheckBox = CType(linha.Cells(1).FindControl("chkSelecionaRecusa"), CheckBox)
            If chk.Checked Then
                If iGravouMotivoRecusa = 0 Then
                    atualizaControle.recusarCotacao(Request.QueryString("cotacao_web_controle_id"), Session("usuario"), linkseguro)
                    iNovoControle = atualizaControle.buscarNovoControle(Request.QueryString("cotacao_web_controle_id"))
                End If
                Dim ctrl As New controle_recusa_cotacao
                ctrl.cotacao_web_controle_id = iNovoControle
                ctrl.tp_recusa_cotacao_id = CInt(linha.Cells(1).Text)
                If CInt(linha.Cells(1).Text) = 13 Then
                    ctrl.outros = txtOutros.Text
                Else
                    ctrl.outros = vbNullString
                End If
                ctrl.dt_inclusao = Now()
                ctrl.usuario = Session("usuario")
                gravaControle.gravarMotivoRecusa(ctrl)
                ctrl = Nothing
                iGravouMotivoRecusa += 1
            End If
        Next

        If iGravouMotivoRecusa > 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Motivos de recusa da cotação gravados com sucesso!');", True)
        End If

        gravaControle = Nothing
        atualizaControle = Nothing
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Atualizar", "fechajanela();window.opener.AtualizarGrid();", True)
    End Sub
End Class