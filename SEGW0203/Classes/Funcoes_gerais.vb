﻿
Imports System.Data.SqlClient

Namespace Data
    Public Class Funcoes_gerais
        'Inherits ConnectionHelper
        Public conexao As ConnectionHelper

        Public Sub New(ByVal linkSeguro As Alianca.Seguranca.Web.LinkSeguro)
            conexao = New ConnectionHelper(linkSeguro)
        End Sub

        Public Function buscar_Dias_Sla(ByVal tp_acao_cotacao_id As Integer) As Integer

            buscar_Dias_Sla = conexao.ExecuteSQL(CommandType.Text, "gestao_cotacao_db.dbo.SEGS09194_SPS " & tp_acao_cotacao_id).Tables(0).Rows(0)("dias_sla").ToString

        End Function

        Public Shared Sub escreveScript(ByVal valor As String)
            HttpContext.Current.Response.Write("<script  type='text/javascript' DEFER='DEFER'>" + valor + "</script>")
        End Sub

        Public Function VerificarCartaBenefeciario(ByVal cotacao_web_id As Integer)

            Dim iTemRegistro As Integer

            iTemRegistro = conexao.ExecuteSQL(CommandType.Text, "gestao_cotacao_db.dbo.SEGS09209_SPS " & cotacao_web_id).Tables(0).Rows(0)("Qtde").ToString

            If iTemRegistro > 0 Then
                VerificarCartaBenefeciario = True
            Else
                VerificarCartaBenefeciario = False
            End If

        End Function

        Public Function MontaCartaBenefeciario(ByVal cotacao_web_id As Integer) As String

            MontaCartaBenefeciario = conexao.ExecuteSQL(CommandType.Text, "gestao_cotacao_db.dbo.SEGS09206_SPS @cotacao_web_id = " & cotacao_web_id).Tables(0).Rows(0)("carta").ToString

        End Function

        Public Function BuscarAcoes() As DataSet

            BuscarAcoes = conexao.ExecuteSQL(CommandType.Text, "gestao_cotacao_db.dbo.SEGS09193_SPS")

        End Function

        Public Function VerificarQtdeBoletosAnexos(ByVal cotacao_web_id As Integer) As Integer

            VerificarQtdeBoletosAnexos = conexao.ExecuteSQL(CommandType.Text, "gestao_cotacao_db.dbo.SEGS09211_SPS " & cotacao_web_id).Tables(0).Rows(0)("Qtde").ToString

        End Function

        Public Function ListaCotacaoEndosso(ByVal filtroStatus As String, ByVal dt_inicial As String, ByVal dt_final As String, ByVal cnpj As String, ByVal ramo As String, ByVal protocolo As String, ByVal apolice As String, ByVal proposta_id As String, ByVal proposta_bb As String, ByVal nome As String, ByVal usuario_id As String) As DataSet

            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            '@filtro_status  
            If Not (filtroStatus Is Nothing) Then
                param = New SqlParameter("@filtro_status", SqlDbType.NVarChar)
                param.Value = filtroStatus
                params.Add(param)
            End If

            '@periodo_inicial
            If Not (dt_inicial Is Nothing) Then
                param = New SqlParameter("@periodo_inicial", SqlDbType.DateTime)
                param.Value = CDate(dt_inicial)
                params.Add(param)
            End If

            '@periodo_final
            If Not (dt_final Is Nothing) Then
                param = New SqlParameter("@periodo_final", SqlDbType.DateTime)
                param.Value = CDate(dt_final)
                params.Add(param)
            End If

            '@cnpj
            If Not (cnpj Is Nothing) Then
                param = New SqlParameter("@cnpj", SqlDbType.NVarChar)
                param.Value = cnpj
                params.Add(param)
            End If

            '@ramo
            If Not (ramo Is Nothing) Then
                param = New SqlParameter("@ramo", SqlDbType.Int)
                param.Value = CInt(ramo)
                params.Add(param)
            End If

            '@protocolo
            If protocolo.Length > 0 Then
                param = New SqlParameter("@protocolo", SqlDbType.NVarChar)
                param.Value = protocolo
                params.Add(param)
            End If

            '@apolice
            If apolice.Length > 0 Then
                param = New SqlParameter("@apolice", SqlDbType.BigInt)
                param.Value = CInt(apolice)
                params.Add(param)
            End If

            '@proposta_id
            If proposta_id.Length > 0 Then
                param = New SqlParameter("@proposta_id", SqlDbType.BigInt)
                param.Value = CInt(proposta_id)
                params.Add(param)
            End If

            '@proposta_bb
            If proposta_bb.Length > 0 Then
                param = New SqlParameter("@proposta_bb", SqlDbType.BigInt)
                param.Value = CInt(proposta_bb)
                params.Add(param)
            End If

            '@nome
            If nome.Length > 0 Then
                param = New SqlParameter("@nome", SqlDbType.VarChar)
                param.Value = nome
                params.Add(param)
            End If

            '@usuario_id
            If Not (usuario_id Is Nothing) Then
                param = New SqlParameter("@usuario_id", SqlDbType.Int)
                param.Value = usuario_id
                params.Add(param)
            End If

            ListaCotacaoEndosso = conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09212_SPS", params.ToArray())

        End Function

        Public Function BuscarLogins() As DataSet

            BuscarLogins = conexao.ExecuteSQL(CommandType.Text, "gestao_cotacao_db.dbo.SEGS09200_SPS")

        End Function

        Public Function BuscarAcaoporLogins(ByVal dt_inicial As String, ByVal dt_final As String, ByVal protocolo As String, ByVal login As String, ByVal tp_acao_cotacao_id As String) As DataSet

            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            If Not (dt_inicial Is Nothing) Then
                param = New SqlParameter("@dt_inicial", SqlDbType.DateTime)
                param.Value = CDate(dt_inicial)
                params.Add(param)
            End If

            If Not (dt_final Is Nothing) Then
                param = New SqlParameter("@dt_final", SqlDbType.DateTime)
                param.Value = CDate(dt_final)
                params.Add(param)
            End If

            If Not (protocolo Is Nothing) Then
                param = New SqlParameter("@protocolo", SqlDbType.VarChar)
                param.Value = protocolo
                params.Add(param)
            End If

            If Not (login Is Nothing) Then
                param = New SqlParameter("@login", SqlDbType.VarChar)
                param.Value = login
                params.Add(param)
            End If

            If Not (tp_acao_cotacao_id Is Nothing) Then
                param = New SqlParameter("@tp_acao_cotacao_id", SqlDbType.Int)
                param.Value = CInt(tp_acao_cotacao_id)
                params.Add(param)
            End If

            BuscarAcaoporLogins = conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09214_SPS", params.ToArray())

        End Function
        Public Function ListagemdeRastreio(ByVal filtroStatus As String, ByVal dt_inicial As String, ByVal dt_final As String, ByVal cnpj As String, ByVal ramo As String, ByVal protocolo As String, ByVal apolice As String, ByVal proposta_id As String, ByVal proposta_bb As String, ByVal nome As String, ByVal prazo As String, ByVal minuta As String, ByVal usuario_id As String) As DataSet

            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            '@filtro_status  
            If Not (filtroStatus Is Nothing) Then
                param = New SqlParameter("@filtro_status", SqlDbType.NVarChar)
                param.Value = filtroStatus
                params.Add(param)
            End If

            '@periodo_inicial
            If Not (dt_inicial Is Nothing) Then
                param = New SqlParameter("@periodo_inicial", SqlDbType.DateTime)
                param.Value = CDate(dt_inicial)
                params.Add(param)
            End If

            '@periodo_final
            If Not (dt_final Is Nothing) Then
                param = New SqlParameter("@periodo_final", SqlDbType.DateTime)
                param.Value = CDate(dt_final)
                params.Add(param)
            End If

            '@cnpj
            If Not (cnpj Is Nothing) Then
                param = New SqlParameter("@cnpj", SqlDbType.NVarChar)
                param.Value = cnpj
                params.Add(param)
            End If

            '@ramo
            If Not (ramo Is Nothing) Then
                param = New SqlParameter("@ramo", SqlDbType.Int)
                param.Value = CInt(ramo)
                params.Add(param)
            End If

            '@protocolo
            If protocolo.Length > 0 Then
                param = New SqlParameter("@protocolo", SqlDbType.NVarChar)
                param.Value = protocolo
                params.Add(param)
            End If

            '@apolice
            If apolice.Length > 0 Then
                param = New SqlParameter("@apolice", SqlDbType.BigInt)
                param.Value = CInt(apolice)
                params.Add(param)
            End If

            '@proposta_id
            If proposta_id.Length > 0 Then
                param = New SqlParameter("@proposta_id", SqlDbType.BigInt)
                param.Value = CInt(proposta_id)
                params.Add(param)
            End If

            '@proposta_bb
            If proposta_bb.Length > 0 Then
                param = New SqlParameter("@proposta_bb", SqlDbType.BigInt)
                param.Value = CInt(proposta_bb)
                params.Add(param)
            End If

            '@nome
            If nome.Length > 0 Then
                param = New SqlParameter("@nome", SqlDbType.VarChar)
                param.Value = nome
                params.Add(param)
            End If

            '@prazo
            If Not (prazo Is Nothing) Then
                param = New SqlParameter("@prazo", SqlDbType.VarChar)
                param.Value = prazo
                params.Add(param)
            End If

            '@minuta
            If Not (minuta Is Nothing) Then
                param = New SqlParameter("@minuta", SqlDbType.VarChar)
                param.Value = minuta
                params.Add(param)
            End If

            '@usuario_id
            If Not (usuario_id Is Nothing) Then
                param = New SqlParameter("@usuario_id", SqlDbType.Int)
                param.Value = usuario_id
                params.Add(param)
            End If

            ListagemdeRastreio = conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09213_SPS", params.ToArray())

        End Function

        Public Function ListaCotacaoEndosso(ByVal cotacao_web_id As String) As DataSet

            Dim params As New List(Of SqlParameter)()
            Dim param As SqlParameter

            '@cotacao_web_id  
            param = New SqlParameter("@cotacao_web_id", SqlDbType.Int)
            param.Value = cotacao_web_id
            params.Add(param)

            ListaCotacaoEndosso = conexao.ExecuteSQL(CommandType.StoredProcedure, "gestao_cotacao_db.dbo.SEGS09212_SPS", params.ToArray())

        End Function


        Public Function Busca_Caminho(ByVal sTipo_Leitura As String, ByVal iAmbiente_id As Integer) As String

            Busca_Caminho = conexao.ExecuteSQL(CommandType.Text, "[SISAB003].controle_sistema_db..valor_ambiente_sps 'SEGBR', '" & sTipo_Leitura & "', 'CAMINHO', " & iAmbiente_id).Tables(0).Rows(0)("valor").ToString

        End Function

        Public Shared Sub MensagemErro(ByVal valor As String)
            Web.HttpContext.Current.Response.Write("<table width=500><tr><td style='color:red; font-family: tahoma; font-size: 15'>Erro: " + valor + "<br><br>")
            Web.HttpContext.Current.Response.Write("<a onClick='javascript:history.go(-1);' style='cursor: hand;'><b><u>Clique Aqui</u></b></a> para voltar.</td></tr></table>")
            Web.HttpContext.Current.Response.End()
        End Sub

    End Class

End Namespace
