﻿Public Class ArquivoUpload
#Region "Atributos"
    Private _cotacao_web_documento_id As Integer
    Private _cotacao_web_id As Integer
    Private _tp_documento As Integer
    Private _caminho As String
    Private _nome_arquivo As String
    Private _usuario As String

    Const Minuta As Integer = 1
    Const MinutaAssinada As Integer = 2
    Const Endosso As Integer = 3
    Const Boleto As Integer = 4
    Const ArquivoCorretor As Integer = 5

#End Region

#Region "Propriedades"
    Public Property cotacao_web_documento_id() As Int32
        Get
            Return Me._cotacao_web_documento_id
        End Get
        Set(ByVal Value As Int32)
            Me._cotacao_web_documento_id = Value
        End Set
    End Property
    Public Property cotacao_web_id() As Integer
        Get
            Return Me._cotacao_web_id
        End Get
        Set(ByVal Value As Integer)
            Me._cotacao_web_id = Value
        End Set
    End Property
    Public Property tp_documento() As Integer
        Get
            Return Me._tp_documento
        End Get
        Set(ByVal Value As Integer)
            Me._tp_documento = Value
        End Set
    End Property
    Public Property caminho() As String
        Get
            Return Me._caminho
        End Get
        Set(ByVal Value As String)
            Me._caminho = Value
        End Set
    End Property
    Public Property nome_arquivo() As String
        Get
            Return Me._nome_arquivo
        End Get
        Set(ByVal Value As String)
            Me._nome_arquivo = Value
        End Set
    End Property
    Public Property usuario() As String
        Get
            Return Me._usuario
        End Get
        Set(ByVal Value As String)
            Me._usuario = Value
        End Set
    End Property
#End Region

    Public Sub New()

    End Sub

    Public Sub New(ByVal cotacao_web_id As Integer, ByVal tipo As String, ByVal nomeArquivo As String, ByVal caminho As String, ByVal usuario As String, ByVal Apolice As String, ByVal Ramo As String)
        Dim sTipo_arquivo As String = ""

        Me.cotacao_web_id = cotacao_web_id

        Select Case tipo
            Case "minuta"
                Me.tp_documento = Minuta
                sTipo_arquivo = "MM"
            Case "minutaassinada"
                Me.tp_documento = MinutaAssinada
                sTipo_arquivo = "MA"
            Case "endosso"
                Me.tp_documento = Endosso
                sTipo_arquivo = "EM"
            Case "boleto"
                Me.tp_documento = Boleto
                sTipo_arquivo = "BO"
                'inicio: sergio.pires - Nova Consultoria 23/01/2017 - 19089159 - Envio de documentos de cotação de endosso web – Visão Corretor
            Case "arquivocorretor"
                Me.tp_documento = ArquivoCorretor
                sTipo_arquivo = nomeArquivo.Substring(0, nomeArquivo.Length() - System.IO.Path.GetExtension(nomeArquivo).Length())
                'fim: sergio.pires - Nova Consultoria 23/01/2017 - 19089159 - Envio de documentos de cotação de endosso web – Visão Corretor
        End Select

        If Right(caminho, 1) <> "\" Then
            caminho += "\"
        End If

        'Me.nome_arquivo = sTipo_arquivo & "-" & Apolice & "-" & Ramo & "-" & Now.Day.ToString.PadLeft(2, "0") & Now.Month.ToString.PadLeft(2, "0") & Now.Year & Now.Hour.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0") & "_" & nomeArquivo
        Me.nome_arquivo = sTipo_arquivo & "-" & Apolice & "-" & Ramo & "-" & Now.Day.ToString.PadLeft(2, "0") & Now.Month.ToString.PadLeft(2, "0") & Now.Year & Now.Hour.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(1, "0") & Now.Millisecond.ToString.PadLeft(1, "0") & System.IO.Path.GetExtension(nomeArquivo).ToLower()
        Me.caminho = caminho
        Me.usuario = usuario

    End Sub

#Region "Métodos"

#End Region

End Class
