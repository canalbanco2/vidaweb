﻿Imports System.IO
Imports System.Data
Imports System.Drawing
Imports System.Configuration
Partial Public Class Pesquisa_login_acao
    Inherits System.Web.UI.Page
    Dim linkSeguro As New Alianca.Seguranca.Web.LinkSeguro

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        linkSeguro.LerUsuario( _
                                Request.Url.Segments(Request.Url.Segments.Length - 1), _
                                Request.UserHostAddress, _
                                Request.QueryString("SLinkSeguro"))

        lblDataCorrente.Text = Format(Date.Now, "dddd, dd " & "DE" & " MMMM " & "DE" & " yyyy").ToLower

        If Not IsPostBack Then
            TableAcaoLogin.Visible = False
            CarregarControles()
        End If

    End Sub

    Protected Sub CarregarControles()
        Dim _funcoes As Data.Funcoes_gerais = New Data.Funcoes_gerais(linkSeguro)
        Dim ds As DataSet

        ds = _funcoes.BuscarAcoes
        ddlAcao.DataSource = ds.Tables(0)
        ddlAcao.DataTextField = "descricao"
        ddlAcao.DataValueField = "tp_acao_cotacao_id"
        ddlAcao.DataBind()
        ddlAcao.Items.Insert(0, New ListItem("Todos", "0", True))
        ddlAcao.SelectedValue = "0"

        ds.Clear()
        
        ds = _funcoes.BuscarLogins
        ddlLogin.DataSource = ds.Tables(0)
        ddlLogin.DataTextField = "login_usuario"
        ddlLogin.DataValueField = "login_usuario"
        ddlLogin.DataBind()
        ddlLogin.Items.Insert(0, New ListItem("Todos", "Todos", True))
        ddlLogin.SelectedValue = "Todos"

        ds = Nothing
        
    End Sub

    Private Sub btnconfirmar_pesquisa_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnconfirmar_pesquisa.Click

        If ((periodo_inicial.Text <> String.Empty) And (periodo_final.Text <> String.Empty)) Then
            Dim dtInicial As Date
            Dim dtFinal As Date
            dtInicial = Convert.ToDateTime(periodo_inicial.Text)
            dtFinal = Convert.ToDateTime(periodo_final.Text)

            If dtInicial > dtFinal Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Periodo inválido!');", True)
                Exit Sub
            End If
        ElseIf (((periodo_inicial.Text = String.Empty) And (periodo_final.Text <> String.Empty)) Or ((periodo_inicial.Text <> String.Empty) And (periodo_final.Text = String.Empty))) Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Periodo inválido!');", True)
            Exit Sub
        End If

        If txtProtocolo.Text <> String.Empty Then
            If Not IsNumeric(txtProtocolo.Text) Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Número de protocolo digitado inválido!');", True)
                Exit Sub
            End If
        End If

        BuscarAcoes()

    End Sub

    Protected Sub BuscarAcoes()

        pnlPeriodo.Visible = False
        pnlFiltros.Visible = False
        btnconfirmar_pesquisa.Visible = False
        TableAcaoLogin.Visible = True

        Session("dt_inicial") = vbNullString
        Session("dt_final") = vbNullString
        Session("protocolo") = vbNullString
        Session("login") = vbNullString
        Session("tp_acao_cotacao_id") = vbNullString


        If ((periodo_inicial.Text <> String.Empty) And (periodo_final.Text <> String.Empty)) Then
            Session("dt_inicial") = Convert.ToDateTime(periodo_inicial.Text)
            Session("dt_final") = Convert.ToDateTime(periodo_final.Text)
        End If

        If txtProtocolo.Text <> String.Empty Then
            Session("protocolo") = txtProtocolo.Text
        End If

        If ddlAcao.SelectedValue <> "0" Then
            Session("tp_acao_cotacao_id") = ddlAcao.SelectedValue
        End If

        If ddlLogin.SelectedValue <> "Todos" Then
            Session("login") = ddlLogin.SelectedValue
        End If

        gridResultadoAcaoLogin.DataSource = Nothing
        gridResultadoAcaoLogin.DataBind()

        Dim _funcoes As Data.Funcoes_gerais = New Data.Funcoes_gerais(linkSeguro)
        gridResultadoAcaoLogin.DataSource = _funcoes.BuscarAcaoporLogins(Session("dt_inicial"), Session("dt_final"), Session("protocolo"), Session("login"), Session("tp_acao_cotacao_id"))
        gridResultadoAcaoLogin.DataBind()

        If gridResultadoAcaoLogin.Rows.Count = 0 Then
            btnExportarExcel.Visible = False
        End If

    End Sub
    Protected Sub gridResultadoAcaoLogin_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        gridResultadoAcaoLogin.PageIndex = e.NewPageIndex
        Me.BuscarAcoes()
    End Sub

    Private Sub btnExportarExcel_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportarExcel.ServerClick

        Response.Clear()
        Response.Buffer = True
        Response.Charset = "UTF-8"
        Response.ContentType = "application/vnd.ms-excel"
        Response.ContentEncoding = Encoding.Default
        Response.AppendHeader("content-disposition", "attachment;filename=Acao_Logins_" & Now.Day.ToString.PadLeft(2, "0") & Now.Month.ToString.PadLeft(2, "0") & Now.Year & "_" & Now.Hour.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0") & Now.Millisecond.ToString.PadLeft(2, "0") & "_" & ".xls")

        Using sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)

            gridResultadoAcaoLogin.AllowPaging = False
            Me.BuscarAcoes()

            gridResultadoAcaoLogin.HeaderRow.BackColor = Color.White
            For Each cell As TableCell In gridResultadoAcaoLogin.HeaderRow.Cells
                cell.BackColor = gridResultadoAcaoLogin.HeaderStyle.BackColor
            Next
            For Each row As GridViewRow In gridResultadoAcaoLogin.Rows
                row.BackColor = Color.White
                For Each cell As TableCell In row.Cells
                    If row.RowIndex Mod 2 = 0 Then
                        cell.BackColor = gridResultadoAcaoLogin.AlternatingRowStyle.BackColor
                    Else
                        cell.BackColor = gridResultadoAcaoLogin.RowStyle.BackColor
                    End If
                    cell.CssClass = "textmode"
                Next
            Next

            gridResultadoAcaoLogin.RenderControl(hw)

            Dim style As String = "<style> .textmode { mso-number-format:\@; } </style>"
            Response.Write(style)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.[End]()
        End Using
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
        ' Verifies that the control is rendered
    End Sub

End Class