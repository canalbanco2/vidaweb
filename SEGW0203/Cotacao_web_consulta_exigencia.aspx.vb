﻿Public Partial Class Cotacao_web_consulta_exigencia
    Inherits System.Web.UI.Page
    Dim linkseguro As New Alianca.Seguranca.Web.LinkSeguro

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        linkseguro.LerUsuario( _
                    Request.Url.Segments(Request.Url.Segments.Length - 1), _
                    Request.UserHostAddress, _
                    Request.QueryString("SLinkSeguro"))

        Session("usuario") = linkseguro.Login_WEB

        If Request.QueryString("perfil") = "C" Then
            btnRecalcular.Enabled = True
        Else
            btnRecalcular.Enabled = False
        End If

        txtMotivoExigencia.Text = RetornaMotivoExigencia(Request.QueryString("cotacao_web_controle_id"))
    End Sub
    Protected Function RetornaMotivoExigencia(ByVal cotacao_web_controle_id As Integer) As String

        Dim exigencia As Data.tipo_exigenciaDAO = New Data.tipo_exigenciaDAO(linkseguro)

        RetornaMotivoExigencia = exigencia.PesquisaExigenciaCotacao(cotacao_web_controle_id)

    End Function

    Private Sub btnSair_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "fechajanela();window.opener.AtualizarGrid();", True)
    End Sub

    Private Sub btnRecalcular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRecalcular.Click

        Dim cotacao_web_controle_id As String
        cotacao_web_controle_id = Request.QueryString("cotacao_web_controle_id")

        Dim dt As New DataSet
        Dim obj_cotacao_webDAO = New Data.cotacao_webDAO(linkseguro)
        dt = obj_cotacao_webDAO.ConsultarCotacao(cotacao_web_controle_id)

        Dim iStatus_Cotacao_Id As Integer = dt.Tables(0).Rows(0)("status_cotacao_id")

        dt = Nothing
        obj_cotacao_webDAO = Nothing

        If iStatus_Cotacao_Id = 4 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "Abrir", "abreJanela('Apolice_cotacao.aspx?SLinkSeguro=" & Request.QueryString("SLinkSeguro") & "&acao=recalculo&cotacao_web_controle_id=" & cotacao_web_controle_id & "',1024,916);", True)
        Else
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Já aplicado recálculo para a cotação selecionada!');", True)
        End If

    End Sub
End Class