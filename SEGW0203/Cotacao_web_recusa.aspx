﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Cotacao_web_recusa.aspx.vb" Inherits="SEGW0203.Cotacao_web_recusa" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Recusa cotação</title>
    <link href="css/global.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript" src="js/principal.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="corpo">
            <asp:Table ID="TableMotivoRecusa" runat="server" BorderWidth="0" CellPadding="0" CellSpacing="0" >
            <asp:TableRow>
                <asp:TableCell ColumnSpan="3">
                    <asp:GridView ID="gridopcoesrecusa"  PageSize = "20" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                    DataKeyNames="tp_recusa_cotacao_id" OnRowDataBound="gridopcoesrecusa_RowDataBound" ShowHeader="false" ShowFooter="false"
                    Caption="Motivo de recusa" CssClass = "corpo" >
                        <Columns>                    
                            <asp:TemplateField HeaderText="" ItemStyle-Width="30px">
                                <headerstyle HorizontalAlign="Center" ForeColor="black" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelecionaRecusa" runat="server" AutoPostBack="true" OnCheckedChanged="chkselecionaRecusa_OnCheckedChanged" /> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField AccessibleHeaderText="" DataField="tp_recusa_cotacao_id" HeaderText=""  ItemStyle-HorizontalAlign = "Right" />
                            <asp:BoundField AccessibleHeaderText="" DataField="descricao" HeaderText=""  ItemStyle-HorizontalAlign = "left" />         
                        </Columns>
                    </asp:GridView>
                    <asp:TextBox ID="txtOutros" runat="server" CssClass="txtOutros" MaxLength="200" /><br />
                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="Botao_Cancelar_Recusa"/>
                    <asp:Button ID="btnConfirmar" runat="server" Text="Confirmar" CssClass="Botao_Confirmar_Recusa"/>
                </asp:TableCell>
            </asp:TableRow>
            </asp:Table>                     
        </div>           
    </form>
</body>
</html>
