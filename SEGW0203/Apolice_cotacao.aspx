﻿<%@ Page Language="vb" AutoEventWireup="false"  CodeBehind="Apolice_cotacao.aspx.vb" Inherits="SEGW0203.Apolice_cotacao" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Cotação</title>
    <link href="css/global.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" language="javascript" src="js/principal.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script>    
    function calendarShown(sender, args){
        sender._popupBehavior._element.style.zIndex = 99999;
    }
</script>  

</head>
<body onload = "carregarMascaras()">
     <form id="Apolice_cotacao" runat="server">
     <asp:Panel runat="server" ID="pnltopo">
        <table background="img/imgbannertopofundo.png" border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
                <td align="right" height="60" style="background-position: left top; background-image: url(img/imgBannerTopo.gif);
                    background-repeat: no-repeat" valign="top">
                </td>
            </tr>
        </table>
        <table background="img/imgFundoMenuTopo.gif" border="0" cellpadding="0" cellspacing="0"
            height="26" width="100%">
            <tr>
                <td>
                    <a class="TituloMenuTopo" href="JavaScript:window.parent.close();">&nbsp; &nbsp; &nbsp;Sair
                        &nbsp; &nbsp;&nbsp;</a> &nbsp;</td>
                <td class="DadosSistemaUsuario" style="width: 250px; text-align: right">
                    <asp:Label ID="lblDataCorrente" runat="server"></asp:Label>&nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
    <div id="corpo">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"  >
        </asp:ScriptManager>
        <table id = "container_principal">
        <asp:Panel ID = "painelPrincipal" runat = "server" >
        <tr>
            <td colspan = "2">
                <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                    <%-- Primeira View --%>
                    <asp:View ID="View1" runat="server">
                        <asp:Table ID="DadosProposta" runat="server" >
                            <%-- Segurado --%>
                            <asp:TableRow ID="TableRow1" runat="server" >
                                <asp:TableCell ID="TableCell1" runat="server"  Width = "100" ColumnSpan = "1">
                                    <asp:Label ID="lblSegurado" runat="server" Text="Segurado: "></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell ID="TableCell2" runat="server"  ColumnSpan = "5">
                                    <asp:TextBox ID="txtSegurado" runat="server" Width = "400" Enabled ="false" ></asp:TextBox>
                                    <asp:HiddenField ID = "hdnClienteID" runat = "server" Visible = "false" />
                                    <asp:HiddenField ID = "hdnClienteNome" runat = "server" Visible = "false" />
                                </asp:TableCell>
                            </asp:TableRow>
                            
                            <%-- CPF /CNPJ Segurado --%>
                            <asp:TableRow ID="TableRow2" runat="server">
                                <asp:TableCell ID="TableCell3" runat="server" >
                                    <asp:Label ID="lblCpfCnpj" runat="server" Text="CPF / CNPJ: "></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell ID="TableCell4" runat="server" ColumnSpan = "5">
                                    <asp:TextBox ID="txtCpfCnpj" runat="server" Width = "400" Enabled ="false" ></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>

                            <%-- Apolice --%>
                            <asp:TableRow ID="TableRow3" runat="server">
                                <asp:TableCell ID="TableCell5" runat="server" >
                                    <asp:Label ID="lblApolice" runat="server" Text="Ap&#243;lice:"></asp:Label>
                                </asp:TableCell>                
                                <asp:TableCell ID="TableCell6" runat="server" ColumnSpan="3" >
                                    <asp:TextBox ID="txtApolice" runat="server" MaxLength = "20" Enabled = "false" Width="140" ></asp:TextBox> 
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <%-- Ramo --%>    
                                    <asp:Label ID="Label1" runat="server" Text="Ramo:" Width = "25"></asp:Label> &nbsp;&nbsp;
                                    <asp:TextBox ID="txtRamo" runat="server" Enabled = "false" Width = "87"></asp:TextBox>
                                </asp:TableCell>
                            
                            <%--Inicio Vigencia do endosso --%>
                                <asp:TableCell ID="TableCell9" runat="server" HorizontalAlign="Right" VerticalAlign="Bottom" >
                                    <asp:Label ID="lblVigEndosso" runat="server" Text="Início da vigência:" Width = "120"> </asp:Label>
                                </asp:TableCell>                
                                <asp:TableCell ID="TableCel20" runat="server" HorizontalAlign="Left" >
                                    <asp:TextBox ID="txtVigEndosso" runat="server" Width="80" />
                                    <asp:Image ID="imgcalendario" ImageUrl="~/img/calendaricon.png" runat="server" OnLoad = "ocultaCalendario" />
                                    <cc1:CalendarExtender ID="txtVigEndossoCalendario"    
                                    runat="server" 
                                    OnClientShown="calendarShown"
                                    Enabled="True" 
                                    Format="dd/MM/yyyy" 
                                    TargetControlID="txtVigEndosso"
                                    PopupButtonID="imgcalendario"/>
                                    
                                    <cc1:MaskedEditExtender ID="maskVigEndossoCalendario"
                                    TargetControlID="txtVigEndosso" 
                                    Mask="99/99/9999"
                                    MaskType="Date" 
                                    runat="server" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" />
                                    
                                    <cc1:MaskedEditValidator ID="maskValidator" 
                                    runat="server" 
                                    ControlExtender="maskVigEndossoCalendario" 
                                    MinimumValue="01/01/1910"
                                    ControlToValidate="txtVigEndosso" 
                                    Display="Dynamic" 
                                    InvalidValueMessage="*" 
                                    MaximumValue="31/12/2099"/>   
                                </asp:TableCell> 

                            <%--Fim Vigencia do endosso --%>
                                <asp:TableCell ID="TableCell7" runat="server" HorizontalAlign="Right" VerticalAlign="Bottom" >
                                    <asp:Label ID="Label3" runat="server" Text="Fim da vigência:" Width = "120"> </asp:Label>
                                </asp:TableCell>                
                                <asp:TableCell ID="TableCell8" runat="server" HorizontalAlign="Left">
                                    <asp:TextBox ID="txtFimVigEndosso" runat="server" Width="80" />
                                    <asp:Image ID="imgFimcalendario" ImageUrl="~/img/calendaricon.png" runat="server" OnLoad = "ocultaCalendario" />
                                    <cc1:CalendarExtender ID="txtFimVigEndossoCalendario"    
                                    runat="server" 
                                    OnClientShown="calendarShown"
                                    Enabled="True" 
                                    Format="dd/MM/yyyy" 
                                    TargetControlID="txtFimVigEndosso"
                                    PopupButtonID="imgFimcalendario"/>
                                    
                                    <cc1:MaskedEditExtender ID="maskFimVigEndossoCalendario"
                                    TargetControlID="txtFimVigEndosso" 
                                    Mask="99/99/9999"
                                    MaskType="Date" 
                                    runat="server" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" />
                                    
                                    <cc1:MaskedEditValidator ID="maskFimValidator" 
                                    runat="server" 
                                    ControlExtender="maskFimVigEndossoCalendario" 
                                    MinimumValue="01/01/1910"
                                    ControlToValidate="txtFimVigEndosso" 
                                    Display="Dynamic" 
                                    InvalidValueMessage="*" 
                                    MaximumValue="31/12/2099"/>   
                                </asp:TableCell> 

                            </asp:TableRow>
                            <%-- Corretor --%>
                            <%-- Alteração feita pelo Sergio Habilitando a edição dos campos corretor e cnpj corretor --%>
                            <asp:TableRow ID="TableRowCorretor" runat="server">
                                <asp:TableCell ID="TableCell11" runat="server" ColumnSpan = "1" Width = "10%">
                                     <asp:Label ID="lblCorretor" runat="server" text = "Corretor:"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell ID="TableCell10" runat="server" ColumnSpan = "5">
                                    <asp:TextBox ID="txtCorretor" runat="server" Width = "400" MaxLength="60" ></asp:TextBox>
                                    <asp:HiddenField ID = "hdnCorretorID" runat = "server" Visible = "false" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow ID="TableRowCNPJ" runat="server">
                                <asp:TableCell ID="TableCell12" runat="server" ColumnSpan = "1" Width = "10%">
                                     <asp:Label ID="lblCnpjCorretor" runat="server" text = "CNPJ:"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell ID="TableCell13" runat="server" ColumnSpan = "5">
                                    <asp:TextBox ID="txtCnpjCorretora" runat="server" Width = "400" MaxLength = "18" onKeyUp="JavaScript:MascaraCnpjCpf(this);" ></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            
                             <asp:TableRow ID="TableRowSolicitante" runat="server">
                                <%-- Solicitante --%>
                                <asp:TableCell ID="TableCell14" runat="server" ColumnSpan = "1" Width = "10%">
                                     <asp:Label ID="lblSolicitante" runat="server" text = "Solicitante:"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell ID="TableCell15" runat="server" ColumnSpan = "3">
                                    <asp:TextBox ID="txtSolicitante" runat="server" Width = "400" Enabled = "true" MaxLength = "60"></asp:TextBox>
                                </asp:TableCell>
                                <%-- Proposta BB --%>
                                <asp:TableCell ID="TableCell16" runat="server" ColumnSpan = "1" Width = "10%">
                                     <asp:Label ID="lblPropostaBB" runat="server" text = "Proposta BB:"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell ID="TableCell17" runat="server" ColumnSpan = "1">
                                    <asp:TextBox ID="txtPropostaBB" runat="server"  Enabled = "false" ></asp:TextBox>
                                    <asp:HiddenField ID = "hdnPropostaID" runat = "server" Visible = "false" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                          
                        <%-- 1. Tipo de alteração (identificar as alterações a serem efetuadas) --%>
                        <asp:Table ID="tableTipoAlteracao" runat="server" >
                        
                            <asp:TableRow ID="TableRow4" runat="server" >
                                <asp:TableCell ID="TableCell18" runat="server" ColumnSpan = "5" >
                                    <asp:Label ID = "lblTipoAlteracao" runat = "server"  text = "1. Tipo de alteração (identificar as alterações a serem efetuadas)"></asp:Label>        
                                </asp:TableCell> 
                            </asp:TableRow> 
                            
                            <asp:TableRow ID="TableRow5" runat="server" >
                                <asp:TableCell ID="TableCell19" runat="server"  Width = "100" >
                                    <asp:Label ID = "lbl11" runat = "server"  text = "1.1 IMPORTÂNCIA SEGURADA"></asp:Label>
                                </asp:TableCell> 
                                
                                <asp:TableCell ID="TableCell20" runat="server" >
                                    <asp:RadioButton ID="radioButtonElevacao" GroupName = "grupoImportaciaSegurada" runat = "server" Text = "Elevação" Checked = "false" />
                                </asp:TableCell> 
                                
                                <asp:TableCell ID="TableCell21" runat="server" ColumnSpan = "3" >
                                    <asp:RadioButton ID="radioButtonReducao" GroupName = "grupoImportaciaSegurada" runat = "server" Text = "Redução" Checked = "false" />
                                </asp:TableCell> 
                                
                            </asp:TableRow> 
                            
                            <asp:TableRow ID="TableRow6" runat="server" >
                                <asp:TableCell ID="TableCell24" runat="server"  Width = "100" >
                                    <asp:Label ID = "lbl12" runat = "server"  text = "1.2 COBERTURAS"></asp:Label>
                                </asp:TableCell> 
                                
                                <asp:TableCell ID="TableCell25" runat="server" >
                                    <asp:CheckBox runat = "server" ID = "checkCoberturaInclusao" Checked = "false" Text = "Inclusão" />
                                </asp:TableCell> 
                                
                                <asp:TableCell ID="TableCell26" runat="server" >
                                    <asp:CheckBox runat = "server" ID = "checkCoberturaExclusao" Checked = "false" Text = "Exclusão" />
                                </asp:TableCell> 
                                
                                <asp:TableCell ID="TableCell27" runat="server" ColumnSpan = "2" >
                                    <asp:CheckBox runat = "server" ID = "checkCoberturaAlteracao" Checked = "false" Text = "Alteração" />
                                </asp:TableCell> 
                                
                            </asp:TableRow> 
                            
                            <asp:TableRow ID="TableRow7" runat="server" >
                                <asp:TableCell ID="TableCell29" runat="server"  Width = "100" >
                                    <asp:Label ID = "lbl13" runat = "server"  text = "1.3 LOCAL(IS) DE RISCO(S)"></asp:Label>
                                </asp:TableCell> 
                                
                                <asp:TableCell ID="TableCell30" runat="server" >
                                    <asp:CheckBox runat = "server" ID = "CheckLocalRiscoInclusao" Checked = "false" Text = "Inclusão" />
                                </asp:TableCell> 
                                
                                <asp:TableCell ID="TableCell31" runat="server" >
                                    <asp:CheckBox runat = "server" ID = "CheckLocalRiscoExclusao" Checked = "false" Text = "Exclusão" />
                                </asp:TableCell> 
                                
                                <asp:TableCell ID="TableCell32" runat="server" >
                                    <asp:CheckBox runat = "server" ID = "CheckLocalRiscoRetificacao" Checked = "false" Text = "Retificação" />
                                </asp:TableCell> 
                                
                                <asp:TableCell ID="TableCell33" runat="server" >
                                    <asp:CheckBox runat = "server" ID = "CheckLocalRiscoAlteracao" Checked = "false" Text = "Alteração" />
                                </asp:TableCell> 
                            </asp:TableRow> 
                            
                            <asp:TableRow ID="TableRow8" runat="server" >
                                <asp:TableCell ID="TableCell34" runat="server"  Width = "100" >
                                    <asp:Label ID = "lblClausulaBenef" runat = "server"  text = "1.4 CLAUSULA BENEFICIÁRIA"></asp:Label>
                                </asp:TableCell> 
                                
                                <asp:TableCell ID="TableCell35" runat="server" >
                                    <asp:CheckBox runat = "server" ID = "CheckClausulaBeneficiariaInclusao" Checked = "false" Text = "Inclusão" />
                                </asp:TableCell> 
                                
                                <asp:TableCell ID="TableCell36" runat="server" >
                                    <asp:CheckBox runat = "server" ID = "CheckClausulaBeneficiariaExclusao" Checked = "false" Text = "Exclusão" />
                                </asp:TableCell> 
                                
                                <asp:TableCell ID="TableCell37" runat="server" >
                                    <asp:CheckBox runat = "server" ID = "CheckClausulaBeneficiariaRetificacao" Checked = "false" Text = "Retificação" />
                                </asp:TableCell> 
                                
                                <asp:TableCell ID="TableCell38" runat="server" >
                                    <asp:CheckBox runat = "server" ID = "CheckClausulaBeneficiariaAlteracao" Checked = "false" Text = "Alteração" />
                                </asp:TableCell> 
                            </asp:TableRow> 
                            
<%--                            <asp:TableRow ID="TableRow9" runat="server" >
                                <asp:TableCell ID="TableCell39" runat="server" ColumnSpan = "5" >
                                    <asp:Label ID = "Label2" runat = "server"  text = "1.5 Outros (especificar abaixo)"></asp:Label><br />
                                    <asp:TextBox ID = "txtOutrosTiposAlteracao" runat = "server" TextMode = "MultiLine"  Width = "600" Rows = "4"/>
                                </asp:TableCell> 
                            </asp:TableRow> --%>

                            <asp:TableRow ID="TableRow9" runat="server" >
                                <asp:TableCell ID="TableCell39" runat="server" ColumnSpan = "5" >
                                    
                                    <asp:Table ID="Tabela_" runat="server" >                                    
                                        <asp:TableRow ID="TableRow36" runat="server" >
                                            <asp:TableCell ID="TableCell92" runat="server" Width="600">
                                                <asp:Label ID = "Label2" runat = "server"  text = "1.5 Outros (especificar abaixo)"></asp:Label><br />
                                                <asp:TextBox ID = "txtOutrosTiposAlteracao" runat = "server" TextMode = "MultiLine"  Width = "600" Rows = "4"/>                                            
                                            </asp:TableCell> 
                                            <asp:TableCell ID="TableCell93" runat="server" VerticalAlign="Top" HorizontalAlign="Left">
                                                <span class="upload-wrapper">
                                                    <br />                    
                                                    <asp:FileUpload ID="fuUploadDocumento" onchange="document.getElementById('Apolice_cotacao').submit()" CssClass="upload-file" runat="server" />
                                                    <asp:Button ID="btnAnexar_Documento" runat="server" Text="Anexar Arquivo" CssClass="upload-buton" />
                                                </span> 
                                                <div>
                                                    <asp:DataList ID="dlsDocumentos_temp" runat="server" gridlines="None" RepeatColumns="2" RepeatDirection="Horizontal" CellPadding="3" CellSpacing="0" Width="600" CssClass="CorpoDatalist">
                                                        <ItemTemplate>
                                                            <asp:Label ID="ID" runat="server" Visible="false"></asp:Label>
                                                            <asp:HyperLink ID="hplVisualiza_Documento_Temp" runat="server"></asp:HyperLink>  
                                                            <asp:LinkButton ID="btn_delete" runat="server" CommandName="cmd_delete"><img src="img/delete-icon.png" width="12" height="12" border="0" /></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:DataList>                                                                                                     
                                                </div>         
                                                    <asp:HiddenField ID="hdiResposta" runat="server" />
                                                    <asp:HiddenField ID="hdiCountGrid" runat="server" />                                                                                    
                                            </asp:TableCell> 
                                        </asp:TableRow>                                    
                                    </asp:Table>

                                </asp:TableCell> 
                            </asp:TableRow>                             
                           
<%--                            <asp:TableRow ID="TableRow35" runat="server" >
                                <asp:TableCell ID="TableCell91" runat="server" ColumnSpan = "2" > 
                                    <span class="upload-wrapper">                    
                                        <asp:FileUpload ID="fuUploadDocumento" onchange="document.getElementById('Apolice_cotacao').submit()" CssClass="upload-file" runat="server" />
                                        <asp:Button ID="btnAnexar_Documento" runat="server" Text="Anexar Arquivo" CssClass="upload-buton" />
                                    </span>                                  
                                       <asp:GridView ID="gridDocumentos" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" onrowdeleting="gridDocumentos_RowDeleting" ShowHeader="false"  width="300px" >
                                         <columns>
                                                <asp:BoundField HeaderText="ID" DataField="ID" ItemStyle-Width="10" HeaderStyle-Width="10" Visible=false />
                                               <asp:TemplateField HeaderText="Visualizar" ItemStyle-HorizontalAlign="Center">                                         
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hplVisualiza_Documento" runat="server"></asp:HyperLink>    
                                                    </ItemTemplate>
                                               </asp:TemplateField>                                                
                                                <asp:CommandField ShowDeleteButton="true" ButtonType="Image" DeleteImageUrl="img/delete-icon.png" HeaderText="Deletar" ItemStyle-Width="10" HeaderStyle-Width="10"  />                                             
                                            </columns>
                                       </asp:GridView>  
                                    <asp:HiddenField ID="hdiResposta" runat="server" />
                                    <asp:HiddenField ID="hdiCountGrid" runat="server" />   
                                </asp:TableCell> 
                            </asp:TableRow> --%>
                            
                        </asp:Table> 
                
                <%-- 2 --%>
                
                    <asp:Table ID="table7" runat="server" >
                            <asp:TableRow ID="TableRow33" runat="server" >
                                <asp:TableCell ID="TableCell89" runat="server"  >
                                    <asp:Label ID = "Label25" runat = "server"  text = "2. ALTERAÇÃO"></asp:Label>
                                </asp:TableCell> 
                            </asp:TableRow> 
                            <asp:TableRow ID="TableRow34" runat="server" >
                                <asp:TableCell ID="TableCell90" runat="server"  >
                                    <table>
					                    <tr>
						                    <th style ="width:30%;">Local</th><th style ="width:25%;">Cobertura</th><th style ="width:15%;">I.S. de</th><th style ="width:15%;">I.S. para</th><th style ="width:15%;">Prêmio</th>
					                    </tr>
                                    </table>
                                    <div class = "auto" style ="height:80px;" >
                                    <asp:GridView ID="gridReducao" runat="server" AutoGenerateColumns="False" Enabled = "true" ShowHeader = "false" ShowFooter = "false" >
                                        <Columns>
                                            <asp:TemplateField ItemStyle-Width = "30%" ItemStyle-HorizontalAlign = "center" ItemStyle-Height="25" >
                                                <ItemTemplate>
                                                     <asp:TextBox ID="txtLocal" runat="server" Text='<%# Eval("local_reducao") %>' MaxLength = "50" Width="300" />
                                                </ItemTemplate>
                                            </asp:TemplateField> 
                                        </Columns>    
                                        <Columns>
                                            <asp:TemplateField ItemStyle-Width = "25%" ItemStyle-HorizontalAlign = "center" ItemStyle-Height="25" >
                                                <ItemTemplate>
                                                     <asp:TextBox ID="txtCobertura" runat="server" Text='<%# Eval("cobertura") %>' MaxLength = "20" Width="250" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>   
                                        <Columns>   
                                            <asp:TemplateField ItemStyle-Width = "15%" ItemStyle-HorizontalAlign = "center" ItemStyle-Height="25" >
                                                <ItemTemplate>
                                                     <asp:TextBox ID="txtISDe" runat="server" Text='<%# Eval("is_de") %>' onKeyUp="JavaScript:MascaraMoeda(this,'.', ',', event);" Width="140" MaxLength="18" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <Columns>   
                                            <asp:TemplateField ItemStyle-Width = "15%" ItemStyle-HorizontalAlign = "center" ItemStyle-Height="25" >
                                                <ItemTemplate>
                                                     <asp:TextBox ID="txtISPara" runat="server" Text='<%# Eval("is_para") %>' onKeyUp="JavaScript:MascaraMoeda(this,'.', ',', event);" Width="140" MaxLength="18" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                         </Columns>   
                                         <Columns>   
                                            <asp:TemplateField ItemStyle-Width = "15%" ItemStyle-HorizontalAlign = "center" ItemStyle-Height="25" >
                                                <ItemTemplate>
                                                     <asp:TextBox ID="txtPremio" runat="server" Text='<%# Eval("premio") %>' onKeyUp="JavaScript:MascaraMoeda(this,'.', ',', event);" Width="140" MaxLength="18" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
<%--                                        <EmptyDataTemplate>
                                            Nenhuma informação para exibir.
                                        </EmptyDataTemplate>
--%>                                    </asp:GridView>  
                                    </div>
                                    <asp:ImageButton ID="imgBtnAdicionaReducao" ImageUrl = "~/img/Plus.png" runat="server"  OnClick ="adicionaLinhaNaGrid" ToolTip = "Adiciona mais uma linha."/>
                                </asp:TableCell> 
                            </asp:TableRow> 
                    </asp:Table> 
                </asp:View>
                
                <%-- Segunda View --%>
                <asp:View ID="View2" runat="server">
                    <asp:Table ID="table1" runat="server" >
                        <asp:TableRow ID="TableRow10" runat="server" >
                            <asp:TableCell ID="TableCell40" runat="server">
                                <asp:Label ID = "LblDadosIncLocais" runat = "server"  text = "3. Dados para inclusão de locais: endereço completo, valor em risco e descrição da ocupação"></asp:Label><br />
                                <asp:TextBox ID = "txtDadosInclusaoLocais" runat = "server" TextMode = "MultiLine"  Width = "600" Rows = "4" MaxLength = "4000"/>
                            </asp:TableCell> 
                        </asp:TableRow> 
                    </asp:Table> 
                    
                    <asp:Table ID="table2" runat="server" >
                    
                        <%-- B --%>
                        <asp:TableRow ID="TableRow11" runat="server" >
                            <asp:TableCell ID="TableCell41" runat="server" Width = "220">
                                <asp:Label ID = "LblB" runat = "server"  text = "B. Sistemas para proteção contra incêndio"></asp:Label>        
                            </asp:TableCell> 
                            
                            <asp:TableCell ID="TableCell47" runat="server"  >
                                <asp:CheckBox runat = "server" ID = "CheckExtintores" Checked = "false" Text = "Extintores" />
                            </asp:TableCell> 
                            
                            <asp:TableCell ID="TableCell48" runat="server"   >
                                <asp:CheckBox runat = "server" ID = "CheckHidrantes" Checked = "false" Text = "Hidrantes" />
                            </asp:TableCell> 
                            
                            <asp:TableCell ID="TableCell49" runat="server" ColumnSpan = "2" >
                                <asp:CheckBox runat = "server" ID = "CheckSprinklers" Checked = "false" Text = "Sprinklers" />
                            </asp:TableCell> 
                                                   
                        </asp:TableRow> 
                        
                        <%-- C --%>
                        <asp:TableRow ID="TableRow12" runat="server" >
                            <asp:TableCell ID="TableCell42" runat="server" Width = "220">
                                <asp:Label ID = "Label5" runat = "server"  text = "C. Sistemas de proteção contra roubos"></asp:Label>
                            </asp:TableCell> 
                            
                            <asp:TableCell ID="TableCell51" runat="server"  >
                                <asp:CheckBox runat = "server" ID = "CheckGrades" Checked = "false" Text = "Grades" />
                            </asp:TableCell> 
                            
                            <asp:TableCell ID="TableCell52" runat="server"   >
                                <asp:CheckBox runat = "server" ID = "CheckAlarmos" Checked = "false" Text = "Alarmes" />
                            </asp:TableCell> 
                            
                            <asp:TableCell ID="TableCell53" runat="server" ColumnSpan = "2" >
                                <asp:CheckBox runat = "server" ID = "CheckVigilancia" Checked = "false" Text = "Vigilância" />
                            </asp:TableCell> 
                                
                        </asp:TableRow> 
                        
                        <%-- D --%>
                        <asp:TableRow ID="TableRow13" runat="server" >
                            <asp:TableCell ID="TableCell55" runat="server"  Width = "220">
                                <asp:Label ID = "lblD" runat = "server"  text = "D. Distância do corpo de bombeiros"></asp:Label>
                            </asp:TableCell> 
                            
                            <asp:TableCell ID="TableCell43" runat="server" >
                                <asp:RadioButton ID="radioDistanciaBombeiros1" GroupName = "grupoDistanciaBombeiros" runat = "server" Text = "Até 10Km" Checked = "false" />
                            </asp:TableCell> 
                            
                            <asp:TableCell ID="TableCell44" runat="server" ColumnSpan = "3" >
                                <asp:RadioButton ID="radioDistanciaBombeiros2" GroupName = "grupoDistanciaBombeiros" runat = "server" Text = "Acima de 10Km" Checked = "false" />
                            </asp:TableCell> 

                        </asp:TableRow> 
                    
                        <%-- E --%>
                        <asp:TableRow ID="TableRow14" runat="server" >
                            <asp:TableCell ID="TableCell45" runat="server"  Width = "220">
                                <asp:Label ID = "lblE" runat = "server"  text = "E. Tipo de construção do edifício"></asp:Label>
                            </asp:TableCell> 
                            
                            <asp:TableCell ID="TableCell46" runat="server" >
                                <asp:RadioButton ID="radioTipoConstrucaoEdificio1" GroupName = "grupoTipoCostrucao" runat = "server" Text = "Superior" Checked = "false" />
                            </asp:TableCell> 
                            
                            <asp:TableCell ID="TableCell50" runat="server" >
                                <asp:RadioButton ID="radioTipoConstrucaoEdificio2" GroupName = "grupoTipoCostrucao" runat = "server" Text = "Sólida" Checked = "false" />
                            </asp:TableCell> 
                            
                            <asp:TableCell ID="TableCell54" runat="server" >
                                <asp:RadioButton ID="radioTipoConstrucaoEdificio3" GroupName = "grupoTipoCostrucao" runat = "server" Text = "Mista" Checked = "false" />
                            </asp:TableCell> 
                            
                            <asp:TableCell ID="TableCell56" runat="server" >
                                <asp:RadioButton ID="radioTipoConstrucaoEdificio4" GroupName = "grupoTipoCostrucao" runat = "server" Text = "Inferior - madeira" Checked = "false" />
                            </asp:TableCell> 

                        </asp:TableRow> 
                        
                        <%-- F --%>
                        <asp:TableRow ID="TableRow15" runat="server" >
                            <asp:TableCell ID="TableCell57" runat="server"  Width = "220">
                                <asp:Label ID = "lblF" runat = "server"  text = "F. Informações sobre a fiação elétrica"></asp:Label>
                            </asp:TableCell> 
                            
                            <asp:TableCell ID="TableCell58" runat="server" >
                                <asp:RadioButton ID="radioFiacao1" GroupName = "grupoFiacao" runat = "server" Text = "Aparente" Checked = "false" />
                            </asp:TableCell> 
                            
                            <asp:TableCell ID="TableCell59" runat="server" ColumnSpan = "3" >
                                <asp:RadioButton ID="radioFiacao2" GroupName = "grupoFiacao" runat = "server" Text = "Embutida em conduites metálicos" Checked = "false" />
                            </asp:TableCell> 
                            
                        </asp:TableRow> 
                        
                        <%-- G --%>
                        <asp:TableRow ID="TableRow16" runat="server" >
                            <asp:TableCell ID="TableCell60" runat="server" Width = "220">
                                <asp:Label ID = "Label9" runat = "server"  text = "G. Cobertura do edifício"></asp:Label>
                            </asp:TableCell> 
                            
                            <asp:TableCell ID="TableCell61" runat="server"  >
                                <asp:CheckBox runat = "server" ID = "checkCoberturaEdificioLaje" Checked = "false" Text = "Laje" />
                            </asp:TableCell> 
                            
                            <asp:TableCell ID="TableCell62" runat="server"   >
                                <asp:CheckBox runat = "server" ID = "checkCoberturaEdificioTelha" Checked = "false" Text = "Telhas de barro/amianto" />
                            </asp:TableCell> 
                            
                            <asp:TableCell ID="TableCell63" runat="server"  Width = "120">
                                <asp:CheckBox runat = "server" ID = "checkCoberturaEdificioOutros" Checked = "false" Text = "Outros (especificar)" OnCheckedChanged ="habilitaCampoOutros" AutoPostBack = "true"  />
                            </asp:TableCell> 
                            <asp:TableCell ID="TableCell64" runat="server" >
                                <asp:TextBox ID = "txtCoberturaEdificio" runat = "server" TextMode = "MultiLine"  Width = "300" Rows = "3" Enabled = "false" />
                            </asp:TableCell> 
                        </asp:TableRow> 
                    
                        <%-- H --%>
                        <asp:TableRow ID="TableRow17" runat="server">
                            <asp:TableCell ID="TableCell65" runat="server" ColumnSpan = "5">
                                <asp:Label ID = "lblH" runat = "server"  text = "H. Sinistralidade nos últimos 5 anos (ocorrência e valor)"></asp:Label><br />
                                <asp:TextBox ID = "txtSinistralidade" runat = "server" TextMode = "MultiLine"  Width = "600" Rows = "4"/>
                            </asp:TableCell> 
                        </asp:TableRow> 
                        
                        <%-- I --%>
                        <asp:TableRow ID="TableRow18" runat="server">
                            <asp:TableCell ID="TableCell66" runat="server" ColumnSpan = "5">
                                <asp:Label ID = "Label10" runat = "server"  text = "I. Outros (em caso de dispor de informações adicionais)"></asp:Label><br />
                                <asp:TextBox ID = "txtInformacoesAdicionais" runat = "server" TextMode = "MultiLine"  Width = "600" Rows = "4"/>
                            </asp:TableCell> 
                        </asp:TableRow> 
                    
                        <%-- J --%>
                        <asp:TableRow ID="TableRow19" runat="server">
                            <asp:TableCell ID="TableCell67" runat="server" ColumnSpan = "5">
                                <asp:Label ID = "Label11" runat = "server"  text = "J. Dados para inspeção de risco"></asp:Label>
                            </asp:TableCell>     
                        </asp:TableRow> 
                        
                        <asp:TableRow ID="TableRow20" runat="server" >                                
                            <asp:TableCell ID="TableCell68" runat="server" Width = "220" HorizontalAlign = "Right">
                                <asp:Label ID = "Label12" runat = "server"  text = "Pessoa de contato no local"></asp:Label>
                            </asp:TableCell>     
                            <asp:TableCell ID="TableCell69" runat="server" ColumnSpan ="4" >
                                <asp:TextBox ID = "txtContato" runat = "server" TextMode = "SingleLine" />
                            </asp:TableCell>     
                        </asp:TableRow> 
                        
                        <asp:TableRow ID="TableRow21" runat="server">     
                        
                            <asp:TableCell ID="TableCell70" runat="server" Width = "220" HorizontalAlign = "Right">
                                <asp:Label ID = "Label13" runat = "server"  text = "Telefone comercial"></asp:Label>
                            </asp:TableCell>     
                            <asp:TableCell ID="TableCell71" runat="server" >
                                <asp:TextBox ID = "txtTelefoneComercial" runat = "server" TextMode = "SingleLine" onKeyUp="JavaScript:MascaraTelefone(this);"  MaxLength = "14"/>
                            </asp:TableCell>     
                            <asp:TableCell ID="TableCell72" runat="server" HorizontalAlign = "Right">
                                <asp:Label ID = "Label14" runat = "server"  text = "Telefone residencial"></asp:Label>
                            </asp:TableCell>     
                            <asp:TableCell ID="TableCell73" runat="server"  ColumnSpan = "2">
                                <asp:TextBox ID = "txtTelefoneResidencial" runat = "server" TextMode = "SingleLine" onKeyUp="JavaScript:MascaraTelefone(this);"  MaxLength = "14"/>
                            </asp:TableCell> 
                            
                        </asp:TableRow> 
                        
                        <asp:TableRow ID="TableRow22" runat="server" >                                
                            <asp:TableCell ID="TableCell74" runat="server" Width = "220" HorizontalAlign = "Right">
                                <asp:Label ID = "Label15" runat = "server"  text = "Telefone celular"></asp:Label>
                            </asp:TableCell>     
                            <asp:TableCell ID="TableCell75" runat="server" >
                                <asp:TextBox ID = "txtTelefoneCelular" runat = "server" TextMode = "SingleLine" onKeyUp="JavaScript:MascaraTelefone(this);"  MaxLength = "14"/>
                            </asp:TableCell>     
                            <asp:TableCell ID="TableCell76" runat="server" HorizontalAlign = "Right">
                                <asp:Label ID = "Label16" runat = "server"  text = "outros telefones"></asp:Label>
                            </asp:TableCell>     
                            <asp:TableCell ID="TableCell77" runat="server"  ColumnSpan = "2">
                                <asp:TextBox ID = "txtOutrosTelefones" runat = "server" TextMode = "SingleLine" onKeyUp="JavaScript:MascaraTelefone(this);"  MaxLength = "14"/>
                            </asp:TableCell>     
                        </asp:TableRow> 
                        
                        <asp:TableRow ID="TableRow23" runat="server" >                                
                            <asp:TableCell ID="TableCell78" runat="server" Width = "220"  HorizontalAlign = "Right" >
                                <asp:Label ID = "Label17" runat = "server"  text = "E-mail"></asp:Label>
                            </asp:TableCell>     
                            <asp:TableCell ID="TableCell79" runat="server" ColumnSpan ="4" >
                                <asp:TextBox ID = "txtEmail" runat = "server" TextMode = "SingleLine" />
                            </asp:TableCell>     
                        </asp:TableRow> 
                    </asp:Table>    
                                        
                </asp:View>
                
                <%-- Terceira View --%>
                <asp:View ID="View3" runat="server">
                    <asp:Table ID="Table3" runat="server" >
                        <%-- 4 --%>
                        <asp:TableRow ID="TableRow24" runat="server" >
                            <asp:TableCell ID="TableCell22" runat="server" >
                                <asp:Label ID="Label18" runat="server" Text="4. Clausula beneficiário"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow25" runat="server" >
                            <asp:TableCell ID="TableCell23" runat="server" >
                                <table>
					                <tr>
						                <th style ="width:30%;">Beneficiário</th><th style ="width:40%;">CNPJ / CPF</th><th style ="width:30%;">Valor CNPJ / CPF</th>
					                </tr>
                                </table>
                                <div class = "auto" style ="height:132px;">
                                    <asp:GridView ID="gridClausulaBeneficiaria" runat="server" AutoGenerateColumns="False" OnRowDataBound = "gridClausulaBeneficiario_RowDataBound" ShowFooter = "false"  ShowHeader = "false" >
                                            <Columns>
                                                <asp:TemplateField ItemStyle-Width="30%" ItemStyle-HorizontalAlign = "center">
                                                    <ItemTemplate>
                                                         <asp:TextBox ID="txtBeneficiario" runat="server" Text='<%# Eval("Beneficiario") %>' MaxLength = "200" Width="300" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Width="40%" ItemStyle-HorizontalAlign = "center" ItemStyle-VerticalAlign="Top" >
                                                    <ItemTemplate>
                                                    <asp:RadioButtonList ID = "radioListCpfCnpj" TextAlign = "Right" runat = "server" RepeatDirection ="Horizontal"
                                                     OnSelectedIndexChanged = "grdClausulaBeneficiaria_RadioSelectedIndexChanged" AutoPostBack="true"  Width="200">
                                                            <asp:ListItem Text ="CNPJ" value = "18"/>
                                                            <asp:ListItem Text ="CPF" value = "14"/>
                                                        </asp:RadioButtonList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Width="30%" ItemStyle-HorizontalAlign = "center">
                                                    <ItemTemplate>
                                                         <asp:TextBox ID="txtGridCnpjCpf" runat="server"  MaxLength = "18" Text='<%# Eval("cnpj_cpf") %>' onKeyUp="JavaScript:MascaraCnpjCpf(this);" Width="180" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
<%--                                            <EmptyDataTemplate>
                                                Nenhuma informação para exibir.
                                            </EmptyDataTemplate>
--%>                                    </asp:GridView>  
                                </div>          
                                <asp:ImageButton ID="imgBtnAdicionarLinha" ImageUrl = "~/img/Plus.png" runat="server"  OnClick ="adicionaLinhaNaGridClausulaBeneficiaria" ToolTip = "Adiciona mais uma linha."/>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    
                    <asp:Table ID="Table4" runat="server" >
                        <%-- 5 --%>
                        <asp:TableRow ID="TableRow26" runat="server" >
                            <asp:TableCell ID="TableCell28" runat="server" >
                                <asp:Label ID="Label19" runat="server" Text="5. Informações complementares (comentários)"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow27" runat="server" >
                            <asp:TableCell ID="TableCell80" runat="server" >
                                <asp:TextBox  ID = "txtInformacoesComplementares" runat = "server" Rows = "4" TextMode = "MultiLine" width = "600"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    
                    <asp:Table ID="Table5" runat="server" >
                        <%-- 6 --%>
                        <asp:TableRow ID="TableRow28" runat="server" >
                            <asp:TableCell ID="TableCell81" runat="server" >
                                <asp:Label ID="Label21" runat="server" Text="6. Observações"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow29" runat="server" >
                            <asp:TableCell ID="TableCell82" runat="server" >
                                <asp:TextBox  ID = "txtObservacoes" runat = "server" Rows = "4" TextMode = "MultiLine" width = "600"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    
                    <asp:Table ID="Table6" runat="server" >
                        <%-- 7 --%>
                        <asp:TableRow ID="TableRow30" runat="server">
                            <asp:TableCell ID="TableCell83" runat="server" ColumnSpan = "3" >
                                <asp:Label ID="Label22" runat="server" Text="7. Condições de pagamento"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow31" runat="server" >
                            <asp:TableCell ID="TableCell84" runat="server" HorizontalAlign = "Right" Width = "220">
                                <asp:Label ID="Label23" runat="server" Text="Forma de pagamento:"></asp:Label>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell85" runat="server" Width = "150">
                                <asp:RadioButton ID="rdbformaPagamento1" GroupName = "grupoFormaPagamento" runat = "server" Text = "1ª parcela a vista" Checked = "false" />
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell86" runat="server">
                                <asp:RadioButton ID="rdbformaPagamento2" GroupName = "grupoFormaPagamento" runat = "server" Text = "1ª parcela a 30 dias" Checked = "false" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow32" runat="server">
                            <asp:TableCell ID="TableCell87" runat="server" ColumnSpan = "1"  HorizontalAlign = "Right" Width = "220" >
                                <asp:Label ID="Label24" runat="server" Text="Número de parcelas:"></asp:Label>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell88" runat="server" ColumnSpan = "2" >
                                <asp:TextBox ID = "txtNumParcelas" runat = "server" TextMode = "SingleLine" Width = "20" MaxLength = "1" onKeyUp="JavaScript:MascaraNumeros(this);" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:View>
            </asp:MultiView>
            </td>
        </tr>
        </asp:Panel>
        <tr>
            <td align = "left" >
                <asp:Button ID="btnSair" runat="server" Text="Sair" CssClass="botao"/>
            </td>
            <td align = "right" >
                <asp:Button ID="btnAnterior" runat="server" Text="Anterior" CssClass="botao" Visible = "false"/>
                <asp:Button ID="btnProximo" runat="server" Text="Próximo" CssClass="botao" Visible = "True" />
                <asp:Button ID="btnConfirmar" runat="server" Text="Confirmar" CssClass="botao" Visible = "false" Enabled = "false" />
            </td>
        </tr>
    </table>
    
    </div>
       
    </form>
</body>
</html>
