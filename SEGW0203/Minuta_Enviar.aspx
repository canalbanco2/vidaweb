﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Minuta_Enviar.aspx.vb" Inherits="SEGW0203.Minuta_Enviar" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Envio da minuta</title>
    <link href="css/global.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" language="javascript" src="js/principal.js"></script>
    
<script>    
    function ValidaEnvio(){
        if (isNaN(document.getElementById('txtDias_Devolucao').value){
            alert("Valor digitado inválido para definição dos dias para devolução!");
            return false;
        }else{
            return true;
        }
    }
</script>       
</head>
<body>
    <form id="form1" runat="server" onsubmit="ValidaEnvio();" >
    <div>
      <asp:Panel runat="server" ID="pnlMinuta_Enviar" CssClass="painelEnviar">  
        <table border="0">
            <tr>
                <td align="right" width="300">
                    <asp:Label ID="lblDias_Devolucao" runat="server" CssClass="Texto_Normal" Text="Prazo para devolução da minuta(em dias) : "></asp:Label>
                </td>
                <td align="left" valign="top" width="40">
                    <asp:TextBox ID="txtDias_Devolucao" runat="server" Width="18" MaxLength="02"></asp:TextBox>                
                </td>
            </tr>
            <tr>
                <td colspan="2" heigth="10">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right" valign="baseline">
                    <asp:Button ID="btnSair" runat="server" Text="Sair" CssClass="botao" />
                    &nbsp;
                    <asp:Button ID="btnconfirmar" runat="server" Text="Confirmar" CssClass="botao" />  
                </td>
            </tr>
        </table>  
      </asp:Panel>

    </div>
    </form>
</body>
</html>
