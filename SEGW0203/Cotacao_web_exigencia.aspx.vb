﻿Public Partial Class Cotacao_web_exigencia
    Inherits System.Web.UI.Page
    Dim linkseguro As New Alianca.Seguranca.Web.LinkSeguro

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        linkseguro.LerUsuario( _
                Request.Url.Segments(Request.Url.Segments.Length - 1), _
                Request.UserHostAddress, _
                Request.QueryString("SLinkSeguro"))

        Session("usuario") = linkseguro.Login_WEB

        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, linkseguro.Ambiente())
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção)
        Else '
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, linkseguro.Ambiente())
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção)
        End If

        txtOutros.Enabled = False

        If Not IsPostBack Then
            Session("tp_exigencia_cotacao_id") = 0
            BuscarMotivosExigencia()
            Session("SLinkSeguro_Obj") = linkseguro
        End If

    End Sub

    Protected Sub gridopcoesexigencia_RowDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.CssClass = "grid_apolice_linha"
        End If
        Select Case e.Row.RowType
            Case DataControlRowType.Header
                e.Row.Cells(1).Visible = False
                Exit Select
            Case DataControlRowType.DataRow
                e.Row.Cells(1).Visible = False
                Exit Select
            Case DataControlRowType.Footer
                e.Row.Cells(1).Visible = False
                Exit Select
        End Select

    End Sub

    Protected Sub rdbSelecionaExigencia_OnCheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rdb As RadioButton = DirectCast(sender, RadioButton)
        Dim linha As GridViewRow = DirectCast(rdb.NamingContainer, GridViewRow)
        Dim grid As GridView = DirectCast(linha.NamingContainer, GridView)

        If rdb.Checked Then
            For Each linha2 As GridViewRow In grid.Rows
                Dim rdb2 As RadioButton = CType(linha2.Cells(1).FindControl("rdbSelecionaExigencia"), CheckBox)
                rdb2.Checked = False
                'linha2.BackColor = Drawing.Color.White
                linha2.CssClass = "grid_apolice_linha"
            Next
            rdb.Checked = True
            linha.CssClass = "grid_apolice_linha_sel"
            'linha.BackColor = Drawing.Color.Beige
            grid.SelectedIndex = linha.DataItemIndex()
            Session("tp_exigencia_cotacao_id") = CInt(grid.SelectedValue)
        End If

        txtOutros.Enabled = False

        For Each linha2 As GridViewRow In grid.Rows
            Dim chk2 As CheckBox = CType(linha2.Cells(1).FindControl("rdbSelecionaExigencia"), CheckBox)
            If chk2.Checked And (CInt(linha2.Cells(1).Text) = 4) Then
                txtOutros.Enabled = True
            End If
        Next

    End Sub


    Private Sub BuscarMotivosExigencia()
        Dim Exigencias As New Data.tipo_exigenciaDAO(linkseguro)

        gridopcoesexigencia.DataSource = Nothing
        gridopcoesexigencia.DataBind()

        gridopcoesexigencia.DataSource = Exigencias.PesquisarExigencias
        gridopcoesexigencia.DataBind()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "fechajanela();", True)
    End Sub

    Private Sub btnConfirmar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirmar.Click
        If Session("tp_exigencia_cotacao_id") <> 0 Then
            Dim gravaControle As New Data.controle_exigencia_cotacaoDAO(linkseguro)
            Dim atualizaControle As New Data.cotacao_web_controleDAO(linkseguro)
            Dim ctrl As New controle_exigencia_cotacao

            atualizaControle.CriarExigenciaCotacao(Request.QueryString("cotacao_web_controle_id"), Session("usuario"), Session("SLinkSeguro_Obj"))

            ctrl.cotacao_web_controle_id = atualizaControle.buscarNovoControle(Request.QueryString("cotacao_web_controle_id"))
            ctrl.tp_exigencia_cotacao_id = Session("tp_exigencia_cotacao_id")
            If Session("tp_exigencia_cotacao_id") = 4 Then
                ctrl.outros = txtOutros.Text
            Else
                ctrl.outros = vbNullString
            End If
            ctrl.dt_inclusao = Now()
            ctrl.usuario = Session("usuario")

            gravaControle.gravarExigencia(ctrl)

            ctrl = Nothing
            gravaControle = Nothing
            atualizaControle = Nothing
        End If
        
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "fechajanela();window.opener.AtualizarGrid();", True)
    End Sub
End Class