﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Pesquisa_login_acao.aspx.vb" Inherits="SEGW0203.Pesquisa_login_acao" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Pesquisa de logins por ação</title>
    <link href="css/global.css" type="text/css" rel="stylesheet" />
<script>    

    function calendarShown(sender, args){
        sender._popupBehavior._element.style.zIndex = 99999;
    }

</script>    

</head>
<body>
    <form id="form1" runat="server" >
    
    <asp:Panel runat="server" ID="pnltopo">
        <table background="img/imgbannertopofundo.png" border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
                <td align="right" height="60" style="background-position: left top; background-image: url(img/imgBannerTopo.gif);
                    background-repeat: no-repeat" valign="top">
                </td>
            </tr>
        </table>
        <table background="img/imgFundoMenuTopo.gif" border="0" cellpadding="0" cellspacing="0"
            height="26" width="100%">
            <tr>
                <td>
                    <a class="TituloMenuTopo" href="JavaScript:window.parent.close();">&nbsp; &nbsp; &nbsp;Sair
                        &nbsp; &nbsp;&nbsp;</a> &nbsp;</td>
                <td class="DadosSistemaUsuario" style="width: 250px; text-align: right">
                    <asp:Label ID="lblDataCorrente" runat="server"></asp:Label>&nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
            
    <div>
      <asp:Panel runat="server" ID="pnlFiltros" CssClass="painelfiltros">  
        <table border="0">
            <tr>
                <td align="right" width="320">
                    <asp:Label ID="Label4" runat="server" Text="Consulta por número de protocolo : " CssClass="Texto_Normal"></asp:Label><br /><br />
                </td>
                <td align="left" width="280" valign="top">
                    <asp:TextBox ID="txtProtocolo" runat="server" Columns="24" MaxLength="18" />
                    <br />
                </td>
            </tr>
            <tr>
                <td align="right" width="320">
                    <asp:Label ID="Label8" runat="server" Text="Login :" CssClass="Texto_Normal"></asp:Label><br /><br />
                </td>
                <td align="left" width="280" valign="top">
                        <asp:DropDownList ID="ddlLogin" runat="server" >
                        <asp:ListItem Text="Consulta por número de protocolo" Value="1"></asp:ListItem> 
                        <asp:ListItem Text="Consulta por número da apólice" Value="2"></asp:ListItem> 
                        <asp:ListItem Text="Consulta por proposta AB" Value="3"></asp:ListItem> 
                        <asp:ListItem Text="Consulta por proposta BB" Value="4"></asp:ListItem> 
                        <asp:ListItem Text="Consulta por nome" Value="5"></asp:ListItem> 
                    </asp:DropDownList>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="right" width="320">
                    <asp:Label ID="Label5" runat="server" Text="Ação : " CssClass="Texto_Normal"></asp:Label><br />
                </td>
                <td align="left" width="280" valign="top">
                    <asp:DropDownList ID="ddlAcao" runat="server" >
                    </asp:DropDownList>
                    <br />
                </td>
            </tr>
        </table>  
      </asp:Panel>
    </div>

    <div>
      <asp:Panel runat="server" ID="pnlPeriodo" CssClass="painelPeriodo">  
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
        </asp:ScriptManager>
        <table border="0">
            <tr>
                <td colspan="2" align="left">
                    <asp:Label ID="Label1" runat="server" Text="Período" CssClass="Cabecalho"></asp:Label><br />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="Label2" runat="server" Text="Inicial" CssClass="Texto_Normal"></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="Label3" runat="server" Text="Final" CssClass="Texto_Normal"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="periodo_inicial" runat="server" Columns="12" MaxLength="10" />
                    <asp:Image ID="imgcalendario" ImageUrl="img\calendaricon.png" runat="server" />
                    <cc1:CalendarExtender ID="periodo_inicial_calendario" 
                    runat="server" 
                    OnClientShown="calendarShown"
                    Enabled="True" 
                    Format="dd/MM/yyyy" 
                    TargetControlID="periodo_inicial"
                    PopupButtonID="imgcalendario"/>
                    <cc1:MaskedEditExtender ID="periodo_inicial_mascara"
                    TargetControlID="periodo_inicial" 
                    Mask="99/99/9999"
                    MaskType="Date" 
                    runat="server" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" />
                    <cc1:MaskedEditValidator ID="periodo_inicial_mascara_valida" 
                    runat="server" 
                    ControlExtender="periodo_inicial_mascara" 
                    MinimumValue="01/01/1910"
                    ControlToValidate="periodo_inicial" 
                    Display="Dynamic" 
                    InvalidValueMessage="*" 
                    MaximumValue="31/12/2099" ErrorMessage="periodo_inicial_mascara_valida"/>   
                </td>
                <td>
                    <asp:TextBox ID="periodo_final" runat="server" Columns="12" MaxLength="10" />
                    <asp:Image ID="imgcalendario2" ImageUrl="img\calendaricon.png" runat="server" />
                    <cc1:CalendarExtender ID="periodo_final_calendario" 
                    runat="server" 
                    OnClientShown="calendarShown"
                    Enabled="True" 
                    Format="dd/MM/yyyy" 
                    TargetControlID="periodo_final"
                    PopupButtonID="imgcalendario2"/>
                    <cc1:MaskedEditExtender ID="periodo_final_mascara"
                    TargetControlID="periodo_final" 
                    Mask="99/99/9999"
                    MaskType="Date" 
                    runat="server" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" />
                    <cc1:MaskedEditValidator ID="periodo_final_mascara_valida" 
                    runat="server" 
                    ControlExtender="periodo_final_mascara" 
                    MinimumValue="01/01/1910"
                    ControlToValidate="periodo_final" 
                    Display="Dynamic" 
                    InvalidValueMessage="*" 
                    MaximumValue="31/12/2099" ErrorMessage="periodo_final_mascara_valida"/>   
                </td>
            </tr>
        </table>  
      </asp:Panel>
            <br />
            <asp:Button ID="btnconfirmar_pesquisa" runat="server" Text="Pesquisar" CssClass="Botao_pesquisa" />
    </div>

    <div id="RelAcaoLogin">
        <asp:Table ID="TableAcaoLogin" runat="server" >
        <asp:TableRow >
            <asp:TableCell >
                <asp:GridView ID="gridResultadoAcaoLogin"  PageSize = "30" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                OnPageIndexChanging="gridResultadoAcaoLogin_PageIndexChanging"
                Caption="Ações por login" CssClass = "corpo" >
                    <Columns>
                    <asp:BoundField AccessibleHeaderText="Protocolo" DataField="protocolo_id" HeaderText="Protocolo"  ItemStyle-HorizontalAlign = "Center" />
                    <asp:BoundField AccessibleHeaderText="Login" DataField="login_usuario" HeaderText="Login" ItemStyle-HorizontalAlign = "Left" />
                    <asp:BoundField AccessibleHeaderText="Ação" DataField="descricao_acao" HeaderText="Ação" ItemStyle-HorizontalAlign = "Left" />
                    <asp:BoundField AccessibleHeaderText="Data da ação" DataField="dt_acao" ItemStyle-HorizontalAlign = "Center"  
                            HeaderText="Data da ação" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False"/>
                    </Columns>
                    <PagerStyle CssClass="grid_pager" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        Não rastreamos ações para os filtros solicitados, favor alterar os filtros e pesquisar novamente.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow >
        <asp:TableRow >
            <asp:TableCell HorizontalAlign="Right" >
                 <button id="btnExportarExcel" runat="server" class="Botao_Gerar_Excel">Exportar para excel &nbsp;<img src="./img/icon-excel.png" /></button>
            </asp:TableCell>
        </asp:TableRow>
     </asp:Table> 
    
    </div>

    </form>
</body>
</html>
