﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Cotacao_web_consulta_exigencia.aspx.vb" Inherits="SEGW0203.Cotacao_web_consulta_exigencia" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Consulta exigência</title>
    <link href="css/global.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" language="javascript" src="js/principal.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="corpo">
        <table border="0" width="330" class="corpo">
            <tr>
                <td align="left">
                    <asp:Label runat="server" ID="Label1" Text="Motivo da exigência" CssClass="Cabecalho" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox Wrap="true" runat="server" TextMode="MultiLine" ReadOnly="true" ID="txtMotivoExigencia" Rows="6" Columns="65"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Button ID="btnSair" runat="server" Text="Sair" CssClass="Botao_pesquisa_2" />&nbsp;
                    <asp:Button ID="btnRecalcular" runat="server" Text="Recalcular" CssClass="Botao_pesquisa_2" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
