﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Pesquisa_cotacao_rastreio.aspx.vb" Inherits="SEGW0203.Pesquisa_cotacao_rastreio" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Relatório de rastreamento</title>
    <link href="css/global.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript" src="js/principal.js"></script>
    </head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="pnltopo">
            <table background="img/imgbannertopofundo.png" border="0" cellpadding="0" cellspacing="0"
                width="100%">
                <tr>
                    <td align="right" height="60" style="background-position: left top; background-image: url(img/imgBannerTopo.gif);
                        background-repeat: no-repeat" valign="top">
                    </td>
                </tr>
            </table>
            <table background="img/imgFundoMenuTopo.gif" border="0" cellpadding="0" cellspacing="0"
                height="26" width="100%">
                <tr>
                    <td>
                        <a class="TituloMenuTopo" href="JavaScript:window.parent.close();">&nbsp; &nbsp; &nbsp;Sair
                            &nbsp; &nbsp;&nbsp;</a> &nbsp;</td>
                    <td class="DadosSistemaUsuario" style="width: 250px; text-align: right">
                        <asp:Label ID="lblDataCorrente" runat="server"></asp:Label>&nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div id="corpo">
            <asp:Table ID="TableRastreio" runat="server" >
            <asp:TableRow >
                <asp:TableCell >
                    <asp:GridView ID="gridResultadoRastreio"  PageSize = "30" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                    OnPageIndexChanging="gridResultadoRastreio_PageIndexChanging" 
                    Caption="Rastreio" CssClass = "corpo" >
                        <Columns>
                        <asp:BoundField AccessibleHeaderText="Protocolo" DataField="protocolo" HeaderText="Protocolo"  ItemStyle-HorizontalAlign = "Left" />
                        <asp:BoundField AccessibleHeaderText="Data de abertura" DataField="dt_abertura" ItemStyle-HorizontalAlign = "Center"  
                            HeaderText="Data de abertura" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False"/>
                        <asp:BoundField AccessibleHeaderText="Status" DataField="descricao_status" HeaderText="Status" ItemStyle-HorizontalAlign = "Center" />
                        <asp:BoundField AccessibleHeaderText="Prazo" DataField="prazo" HeaderText="Prazo" ItemStyle-HorizontalAlign = "Center" />
                        <asp:BoundField AccessibleHeaderText="Produto" DataField="produto_id" HeaderText="Produto" ItemStyle-HorizontalAlign = "Center" />
                        <asp:BoundField AccessibleHeaderText="Ramo" DataField="ramo_id" HeaderText="Ramo" ItemStyle-HorizontalAlign = "Center" />
                        <asp:BoundField AccessibleHeaderText="Proposta" DataField="proposta_id" HeaderText="Proposta" ItemStyle-HorizontalAlign = "Center" />
                        <asp:BoundField AccessibleHeaderText="Apólice" DataField="apolice_id" HeaderText="Apólice" ItemStyle-HorizontalAlign = "Center" />
                        <asp:BoundField AccessibleHeaderText="Nome do segurado" DataField="segurado" HeaderText="Nome do segurado" ItemStyle-HorizontalAlign = "Center" />
                        <asp:BoundField AccessibleHeaderText="Email do demandante/corretor" DataField="email_corretor" HeaderText="Email do demandante/corretor" ItemStyle-HorizontalAlign = "Center" />
                        <asp:BoundField AccessibleHeaderText="Nº do endosso" DataField="endosso_id" HeaderText="Nº do endosso" ItemStyle-HorizontalAlign = "Center" />
                        
                        </Columns>
                        <PagerStyle CssClass="grid_pager" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            Não rastreamos endossos para os filtros solicitados, favor alterar os filtros e pesquisar novamente.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </asp:TableCell>
            </asp:TableRow >
            <asp:TableRow >
                <asp:TableCell HorizontalAlign="Right" >
                     <button id="btnExportarExcel" runat="server" class="Botao_Gerar_Excel">Exportar para excel &nbsp;<img src="img/icon-excel.png" /></button>
                </asp:TableCell>
            </asp:TableRow>
         </asp:Table> 
    
    </div>
    </form>
</body>
</html>
