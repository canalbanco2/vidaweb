﻿Imports System.io

Partial Public Class Apolice_cotacao
    Inherits System.Web.UI.Page

    Dim linkseguro As New Alianca.Seguranca.Web.LinkSeguro

    Dim sLinkSeguro As String
    Dim sCaminhoTemporario As String
    Dim sCaminhoVisualiza As String

    Const AGUARDANDO_ANALISE As Integer = 1
    Const EMITIR As Integer = 6

    Dim myDataTable As DataTable
    Dim myDataColumn As DataColumn

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        linkseguro.LerUsuario( _
                    Request.Url.Segments(Request.Url.Segments.Length - 1), _
                    Request.UserHostAddress, _
                    Request.QueryString("SLinkSeguro"))

        sLinkSeguro = Request.QueryString("SLinkSeguro")


        sCaminhoTemporario = getCaminho("ARQUIVOS_COTACAO", linkseguro.Ambiente)

        If Right(sCaminhoTemporario, 1) <> "\" Then
            sCaminhoTemporario += "\temp"
        Else
            sCaminhoTemporario += "temp"
        End If

        If Not Directory.Exists(sCaminhoTemporario) Then

            Directory.CreateDirectory(sCaminhoTemporario)

        End If

        sCaminhoTemporario += "\"

        sCaminhoVisualiza = getCaminho("ARQUIVOS_WEB_COTACAO", linkseguro.Ambiente)

        If Right(sCaminhoTemporario, 1) <> "/" Then
            sCaminhoVisualiza += "/temp/"
        Else
            sCaminhoVisualiza += "temp/"
        End If

        If Not IsPostBack Then

            Session("usuario") = linkseguro.Login_WEB

            Select Case Request.QueryString("acao")
                Case "inclusao"
                    'Carregar tela com dados pesquisados na busca por apólices
                    Dim lista As New List(Of apolice)
                    lista = Session("Lista")
                    txtSegurado.Text = lista(Session("indice")).segurado
                    hdnClienteID.Value = lista(Session("indice")).cliente_id
                    hdnClienteNome.Value = lista(Session("indice")).segurado

                    txtCpfCnpj.Text = lista(Session("indice")).cpf_cnpj
                    txtApolice.Text = lista(Session("indice")).apolice_id
                    txtRamo.Text = lista(Session("indice")).ramo_id
                    'txtCorretor.Text = lista(Session("indice")).corretor
                    txtCorretor.Text = String.Empty
                    hdnCorretorID.Value = lista(Session("indice")).corretorID
                    'txtCnpjCorretora.Text = lista(Session("indice")).CNPJ_corretora
                    txtCnpjCorretora.Text = String.Empty
                    txtPropostaBB.Text = lista(Session("indice")).proposta_bb
                    hdnPropostaID.Value = lista(Session("indice")).proposta_id

                    'gridReducao:
                    carrega_GridReducao()

                    'gridCalusulaBeneficiaria:
                    carrega_GridClausulaBeneficiaria()

                    'Habilita o botão Confirmar
                    habilitaBotaoConfirmar(True)

                    'inicio: gleison.pimentel - Nova Consultoria 05/11/2016 - 19089159 - Envio de documentos de cotação de endosso web – Visão Corretor
                    If Session("dtDocumentos") Is Nothing Then
                        CriaColunasDocumentos()
                        Session("dtDocumentos") = myDataTable
                        hdiCountGrid.Value = 0
                        btnProximo.Attributes.Add("onclick", "getMessage();")
                    End If
                    'fim: gleison.pimentel - Nova Consultoria 05/11/2016 - 19089159 - Envio de documentos de cotação de endosso web – Visão Corretor

                    'habilitar envio de arquivos do corretor
                    fuUploadDocumento.Enabled = True

                Case "consulta"
                    'Carregar dados
                    Dim dt As New DataSet
                    Dim obj_cotacao_webDAO = New Data.cotacao_webDAO(linkseguro)
                    dt = obj_cotacao_webDAO.ConsultarCotacao(Request.QueryString("cotacao_web_controle_id"))
                    carregarCampos(dt)
                    carrega_GridReducao(dt.Tables(0).Rows(0)("cotacao_web_id"))
                    carrega_GridClausulaBeneficiaria(dt.Tables(0).Rows(0)("cotacao_web_id"))

                    'desabilitar todos os campos
                    painelPrincipal.Enabled = False

                    'desabilitar botão confirmar
                    habilitaBotaoConfirmar(False)

                    'desabilitar envio de arquivos do corretor
                    fuUploadDocumento.Enabled = False

                Case "recalculo"
                    'Carregar dados
                    Dim dt As New DataSet
                    Dim obj_cotacao_webDAO = New Data.cotacao_webDAO(linkseguro)
                    dt = obj_cotacao_webDAO.ConsultarCotacao(Request.QueryString("cotacao_web_controle_id"))
                    carregarCampos(dt)
                    Session.Add("protocolo_id", dt.Tables(0).Rows(0)("protocolo_id"))
                    carrega_GridReducao(dt.Tables(0).Rows(0)("cotacao_web_id"))
                    carrega_GridClausulaBeneficiaria(dt.Tables(0).Rows(0)("cotacao_web_id"))

                    'habilitar todos os campos
                    painelPrincipal.Enabled = True

                    'Habilita o botão Confirmar
                    habilitaBotaoConfirmar(True)

                    'inicio: sergio.pires - Nova Consultoria 25/01/2017 - 19089159 - Envio de documentos de cotação de endosso web – Visão Corretor
                    'No caso de recalculo permitir ao corretor inserir novos documentos.
                    If Session("dtDocumentos") Is Nothing Then
                        CriaColunasDocumentos()
                        Session("dtDocumentos") = myDataTable
                        hdiCountGrid.Value = 0
                        btnProximo.Attributes.Add("onclick", "getMessage();")
                    End If
                    'fim: sergio.pires - Nova Consultoria 25/01/2017 - 19089159 - Envio de documentos de cotação de endosso web – Visão Corretor

                    'habilitar envio de arquivos do corretor
                    fuUploadDocumento.Enabled = True

            End Select

        End If

        lblDataCorrente.Text = Format(Date.Now, "dddd, dd " & "DE" & " MMMM " & "DE" & " yyyy").ToLower

        If fuUploadDocumento.HasFile Then
            AnexarDocumentoGrid()
        End If

        If Session("usuario") = String.Empty Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "Atualizar", "window.alert('Sessão da página perdeu referencia, favor logar novamente!');fechajanela();", True)
        End If

    End Sub

    'inicio: gleison.pimentel - Nova Consultoria 05/11/2016 - 19089159 - Envio de documentos de cotação de endosso web – Visão Corretor
    Private Sub AnexarDocumentoGrid()

        Dim myDataRow As DataRow
        Dim wID As Integer = 0
        Dim wNome As String = ""
        Dim wCaminho As String = ""
        Dim wArquivo As FileUpload
        Dim dtDocumentos As DataTable = Session("dtDocumentos")

        If Not ValidaDocumentosGRID(dtDocumentos, fuUploadDocumento) Then
            Exit Sub
        End If

        wID = dtDocumentos.Rows.Count + 1
        wNome = fuUploadDocumento.FileName
        wCaminho = fuUploadDocumento.PostedFile.FileName
        wArquivo = fuUploadDocumento

        fuUploadDocumento.SaveAs(sCaminhoTemporario & wNome)

        myDataRow = dtDocumentos.NewRow()
        myDataRow("ID") = wID
        myDataRow("Nome") = wNome
        myDataRow("Caminho") = wCaminho
        myDataRow("Arquivo") = wArquivo

        dtDocumentos.Rows.Add(myDataRow)

        'gridDocumentos.DataSource = dtDocumentos
        'gridDocumentos.DataBind()
        dlsDocumentos_temp.DataSource = dtDocumentos
        dlsDocumentos_temp.DataBind()
        hdiCountGrid.Value = dtDocumentos.Rows.Count
        Session("dtDocumentos") = dtDocumentos
    End Sub

    Private Function ValidaDocumentosGRID(ByVal dtDocumentos As DataTable, ByVal Arquivo As FileUpload) As Boolean
        Dim extensao As String
        Dim sTipoArquivos As String = ""
        Dim bPassouExtensao As Boolean = False

        ''Valida total de arquivos anexados
        If dtDocumentos.Rows.Count >= 10 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Não é permitido anexar mais que 10 arquivos,\nfavor tentar novamente!');", True)
            Return False
        End If

        ''valida extensão
        extensao = System.IO.Path.GetExtension(Arquivo.FileName).ToLower
        If ((extensao = ".doc") Or (extensao = ".docx") Or (extensao = ".xls") Or (extensao = ".xlsx") Or (extensao = ".pdf")) Then
            bPassouExtensao = True
        End If

        If Not bPassouExtensao Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Tipo de arquivo selecionado difere do tipos de arquivos permitidos (pdf, xls, xlsx, doc e docx).');", True)
            Return False
        End If

        ''valida tamanho
        Dim tamanho As Decimal
        tamanho = (Arquivo.PostedFile.ContentLength / 1024) / 1024
        If tamanho > 5.0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Tamanho do arquivo ultrapassou o limite de 5 MB permitido,\nfavor tentar novamente!');", True)
            Return False
        End If

        ''valida total de caracteres do nome do arquivo
        If (Len(Arquivo.FileName) - Len(extensao)) > 20 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Nome do arquivo selecionado deve conter máximo de 20 caracteres,\nfavor tentar novamente!');", True)
            Return False
        End If

        ''verifica se ja existe arquivo no GRID
        Dim wExistenoGrid As Boolean = False
        For Each dr As DataRow In dtDocumentos.Rows
            If dr("Nome") = Arquivo.FileName Then
                wExistenoGrid = True
                Exit For
            End If
        Next
        If wExistenoGrid Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Arquivo selecionado já foi anexado a esta cotação,\nFavor escolher um outro arquivo.!');", True)
            Return False
        End If

        Return True

    End Function

    Private Sub DeletarDocumentoGrid(ByVal id As Integer)
        Dim dtDocumentos As DataTable = Session("dtDocumentos")

        For Each dr As DataRow In dtDocumentos.Rows
            If dr("ID") = id Then
                DeletarTemporaria(dr("Nome"))
                dtDocumentos.Rows.Remove(dr)
                Exit For
            End If
        Next

        Session("dtDocumentos") = dtDocumentos
        'gridDocumentos.DataSource = dtDocumentos
        'gridDocumentos.DataBind()
        dlsDocumentos_temp.DataSource = dtDocumentos
        dlsDocumentos_temp.DataBind()
        hdiCountGrid.Value = dtDocumentos.Rows.Count
    End Sub

    Private Function ExisteDocumentosAnexo() As Boolean
        Dim dtDocumentos As DataTable = Session("dtDocumentos")
        Return dtDocumentos.Rows.Count > 0
    End Function

    Private Sub DeletarTemporaria(ByVal sNomeArquivo As String)

        File.Delete(sCaminhoTemporario & sNomeArquivo)

    End Sub

    Private Sub dlsDocumentos_temp_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlsDocumentos_temp.ItemCommand
        If e.CommandName = "cmd_delete" Then
            Dim id As Integer = Convert.ToInt32(e.CommandArgument.ToString())

            DeletarDocumentoGrid(id)

        End If
    End Sub

    Private Sub dlsDocumentos_temp_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlsDocumentos_temp.ItemDataBound
        If e.Item.DataItem IsNot Nothing Then

            Dim deleteButton As LinkButton = DirectCast(e.Item.FindControl("btn_delete"), LinkButton)
            deleteButton.OnClientClick = "if (!window.confirm('Confirma a exclusão deste documento ?')) return false;"
            
            Dim v_oDocumento As HyperLink = DirectCast(e.Item.FindControl("hplVisualiza_Documento_Temp"), HyperLink)

            Dim rowView As DataRowView = CType(e.Item.DataItem, DataRowView)
            deleteButton.CommandArgument = rowView("ID").ToString
            v_oDocumento.Text = rowView("Nome").ToString
            v_oDocumento.Target = "_blank"
            v_oDocumento.NavigateUrl = sCaminhoVisualiza & rowView("Nome").ToString
        End If
    End Sub

    Private Sub CriaColunasDocumentos()
        myDataTable = New DataTable("Documentos")
        myDataTable.Rows.Clear()
        myDataTable.Columns.Clear()

        myDataColumn = New DataColumn
        myDataColumn.DataType = GetType(Integer)
        myDataColumn.ColumnName = "ID"
        myDataColumn.Caption = "ID"
        myDataColumn.Unique = False
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = GetType(String)
        myDataColumn.ColumnName = "Nome"
        myDataColumn.Caption = "Nome"
        myDataColumn.Unique = False
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = GetType(String)
        myDataColumn.ColumnName = "Caminho"
        myDataColumn.Caption = "Caminho"
        myDataColumn.Unique = False
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = GetType(FileUpload)
        myDataColumn.ColumnName = "Arquivo"
        myDataColumn.Caption = "Arquivo"
        myDataColumn.Unique = False
        myDataTable.Columns.Add(myDataColumn)

    End Sub

    Private Sub InserirAnexoDocumentum(ByVal obj_cotacao_web As cotacao_web)
        Try
            Dim dtDocumentos As DataTable = Session("dtDocumentos")

            If dtDocumentos.Rows.Count = 0 Then Exit Sub

            Dim arquivo As FileUpload

            For Each dr As DataRow In dtDocumentos.Rows

                arquivo = dr("Arquivo")

                Dim Ua As ArquivoUpload = New ArquivoUpload(obj_cotacao_web.cotacao_web_id, "arquivocorretor", arquivo.FileName, getCaminho("ARQUIVOS_COTACAO", linkseguro.Ambiente), Session("usuario"), obj_cotacao_web.apolice_id, obj_cotacao_web.ramo_id)
                Dim UaDAO As Data.ArquivoUploadDAO = New Data.ArquivoUploadDAO(linkseguro)

                File.Move(sCaminhoTemporario & dr("Nome"), Ua.caminho & Ua.nome_arquivo)

                UaDAO.GravarUpload(Ua, getCaminho("ARQUIVOS_WEB_COTACAO", linkseguro.Ambiente))

                Ua = Nothing
                UaDAO = Nothing
            Next

            arquivo = Nothing

        Catch ex As Exception
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Erro ao Anexar arquivos. " & Replace(ex.Message, "'", "") & "');", True)
            Exit Sub
        End Try
    End Sub

    'fim: gleison.pimentel - Nova Consultoria 05/11/2016 - 19089159 - Envio de documentos de cotação de endosso web – Visão Corretor

    Protected Sub ocultaCalendario(ByVal sender As Object, ByVal e As System.EventArgs)
        imgcalendario.Visible = painelPrincipal.Enabled
        imgFimcalendario.Visible = painelPrincipal.Enabled
    End Sub

    Protected Sub carrega_GridReducao(Optional ByVal cotacao_web_id As Integer = 0)

        Dim listaReducao As New List(Of reducao)

        If cotacao_web_id = 0 Then
            'Grid inicia com 4 linhas em branco
            listaReducao.Add(New reducao)
            listaReducao.Add(New reducao)
            listaReducao.Add(New reducao)
            listaReducao.Add(New reducao)
            gridReducao.DataSource = listaReducao
        Else
            Dim dt_reducao As DataSet
            Dim obj_reducaoDAO As New Data.reducaoDAO(linkseguro)
            dt_reducao = obj_reducaoDAO.Consultar(cotacao_web_id)
            gridReducao.DataSource = dt_reducao
        End If

        gridReducao.DataBind()

    End Sub

    Protected Sub carrega_GridClausulaBeneficiaria(Optional ByVal cotacao_web_id As Integer = 0)

        If cotacao_web_id = 0 Then
            'gridCalusulaBeneficiaria começa com 3 linhas vazias:
            Dim listaClausulaBeneficiaria As New List(Of clausula_Beneficiaria)
            listaClausulaBeneficiaria.Add(New clausula_Beneficiaria)
            listaClausulaBeneficiaria.Add(New clausula_Beneficiaria)
            listaClausulaBeneficiaria.Add(New clausula_Beneficiaria)
            gridClausulaBeneficiaria.DataSource = listaClausulaBeneficiaria
        Else
            Dim dt_clausula_beneficiaria As DataSet
            Dim obj_clausula_beneficiariaDAO As New Data.clausula_beneficiariaDAO(linkseguro)
            dt_clausula_beneficiaria = obj_clausula_beneficiariaDAO.Consultar(cotacao_web_id)
            gridClausulaBeneficiaria.DataSource = dt_clausula_beneficiaria
        End If

        gridClausulaBeneficiaria.DataBind()

    End Sub

    Protected Sub habilitaBotaoConfirmar(ByVal argumento As Boolean)

        btnConfirmar.Enabled = argumento

    End Sub

    Protected Sub carregarCampos(ByVal dt As DataSet)

        Dim COTACAO_WEB_TB As Integer = 0
        Dim CLIENTE_TB As Integer = 1
        Dim CORRETAGEM_TB As Integer = 2
        Dim COTACAO_SISPROT_ROUBO_TB As Integer = 3
        Dim COTACAO_SISPROT_INC_TB As Integer = 4
        Dim COTACAO_TEL_CONTATO_INSPECAO_TB As Integer = 5

        'Segurado (cliente_tb)
        txtSegurado.Text = dt.Tables(CLIENTE_TB).Rows(0)("nome")
        hdnClienteID.Value = dt.Tables(CLIENTE_TB).Rows(0)("cliente_id")
        hdnClienteNome.Value = dt.Tables(CLIENTE_TB).Rows(0)("nome")
        txtCpfCnpj.Text = dt.Tables(CLIENTE_TB).Rows(0)("cpf_cnpj")

        'Dados da cotação(cotacao_web_tb)
        txtApolice.Text = dt.Tables(COTACAO_WEB_TB).Rows(0)("apolice_id")
        txtRamo.Text = dt.Tables(COTACAO_WEB_TB).Rows(0)("ramo_id")
        txtVigEndosso.Text = dt.Tables(COTACAO_WEB_TB).Rows(0)("dt_vigencia_endosso")
        txtFimVigEndosso.Text = dt.Tables(COTACAO_WEB_TB).Rows(0)("dt_fim_vigencia_endosso")

        'Dados do corretor(
        txtCorretor.Text = dt.Tables(CORRETAGEM_TB).Rows(0)("nome")
        hdnCorretorID.Value = dt.Tables(COTACAO_WEB_TB).Rows(0)("corretor_id")
        txtCnpjCorretora.Text = dt.Tables(CORRETAGEM_TB).Rows(0)("CNPJ_corretora")

        txtSolicitante.Text = dt.Tables(COTACAO_WEB_TB).Rows(0)("solicitante")
        txtPropostaBB.Text = dt.Tables(COTACAO_WEB_TB).Rows(0)("proposta_bb_id")
        hdnPropostaID.Value = dt.Tables(COTACAO_WEB_TB).Rows(0)("proposta_id")

        '1. Tipo de alteração
        Select Case (dt.Tables(COTACAO_WEB_TB).Rows(0)("alt_importancia_segurada"))
            Case "E"
                radioButtonElevacao.Checked = True
            Case "R"
                radioButtonReducao.Checked = True
            Case Else
        End Select

        '1.2 Coberturas
        checkCoberturaInclusao.Checked = dt.Tables(COTACAO_WEB_TB).Rows(0)("alt_coberturas_inclusao")
        checkCoberturaExclusao.Checked = dt.Tables(COTACAO_WEB_TB).Rows(0)("alt_coberturas_exclusao")
        checkCoberturaAlteracao.Checked = dt.Tables(COTACAO_WEB_TB).Rows(0)("alt_coberturas_alteracao")

        '1.3 Local de risco
        CheckLocalRiscoInclusao.Checked = dt.Tables(COTACAO_WEB_TB).Rows(0)("alt_local_risco_inclusao")
        CheckLocalRiscoExclusao.Checked = dt.Tables(COTACAO_WEB_TB).Rows(0)("alt_local_risco_exclusao")
        CheckLocalRiscoRetificacao.Checked = dt.Tables(COTACAO_WEB_TB).Rows(0)("alt_local_risco_retificacao")
        CheckLocalRiscoAlteracao.Checked = dt.Tables(COTACAO_WEB_TB).Rows(0)("alt_local_risco_alteracao")

        '1.4 Cláusula beneficiária
        CheckClausulaBeneficiariaInclusao.Checked = dt.Tables(COTACAO_WEB_TB).Rows(0)("alt_clausula_beneficiario_inclusao")
        CheckClausulaBeneficiariaExclusao.Checked = dt.Tables(COTACAO_WEB_TB).Rows(0)("alt_clausula_beneficiario_exclusao")
        CheckClausulaBeneficiariaRetificacao.Checked = dt.Tables(COTACAO_WEB_TB).Rows(0)("alt_clausula_beneficiario_retificacao")
        CheckClausulaBeneficiariaAlteracao.Checked = dt.Tables(COTACAO_WEB_TB).Rows(0)("alt_clausula_beneficiario_alteracao")

        '1.5 Alteração outros
        txtOutrosTiposAlteracao.Text = dt.Tables(COTACAO_WEB_TB).Rows(0)("alt_outros")

        '2. Redução (GridReducao)                       
        'Método próprio para carga da grid

        '3. Dados para inclusão de locais
        txtDadosInclusaoLocais.Text = dt.Tables(COTACAO_WEB_TB).Rows(0)("dados_inc_locais")

        'B. Sistemas para proteção contra incêndio” 
        For Each row As DataRow In dt.Tables(COTACAO_SISPROT_INC_TB).Rows
            Select Case (row.Item("tp_protecao_incendio"))
                Case "E"
                    CheckExtintores.Checked = True
                Case "H"
                    CheckHidrantes.Checked = True
                Case "S"
                    CheckSprinklers.Checked = True
            End Select
        Next

        'C. Sistemas de proteção contra roubo
        For Each row As DataRow In dt.Tables(COTACAO_SISPROT_ROUBO_TB).Rows
            Select Case (row.Item("tp_protecao_roubo"))
                Case "G"
                    CheckGrades.Checked = True
                Case "A"
                    CheckAlarmos.Checked = True
                Case "V"
                    CheckVigilancia.Checked = True
            End Select
        Next

        'D. Distância do Corpo de bombeiros
        If (dt.Tables(COTACAO_WEB_TB).Rows(0)("distancia_bombeiro_10_km") = "I") Then
            radioDistanciaBombeiros1.Checked = True
        End If
        If (dt.Tables(COTACAO_WEB_TB).Rows(0)("distancia_bombeiro_10_km") = "S") Then
            radioDistanciaBombeiros2.Checked = True
        End If

        'E. Tipo de contrução do Edifício
        Select Case (dt.Tables(COTACAO_WEB_TB).Rows(0)("construcao_edificio"))
            Case "S"
                radioTipoConstrucaoEdificio1.Checked = True
            Case "O"
                radioTipoConstrucaoEdificio2.Checked = True
            Case "M"
                radioTipoConstrucaoEdificio3.Checked = True
            Case "I"
                radioTipoConstrucaoEdificio4.Checked = True
        End Select

        'F. Informações sobre a fiação elétrica
        Select Case (dt.Tables(COTACAO_WEB_TB).Rows(0)("fiacao_eletrica"))
            Case "A"
                radioFiacao1.Checked = True
            Case "E"
                radioFiacao2.Checked = True
        End Select

        'G. Cobertura do edifício
        checkCoberturaEdificioLaje.Checked = dt.Tables(COTACAO_WEB_TB).Rows(0)("cobertura_edificio_laje")
        checkCoberturaEdificioTelha.Checked = dt.Tables(COTACAO_WEB_TB).Rows(0)("cobertura_edificio_telha")
        txtCoberturaEdificio.Text = dt.Tables(COTACAO_WEB_TB).Rows(0)("cobertura_edificio_outros")

        If txtCoberturaEdificio.Text.Length > 0 Then
            checkCoberturaEdificioOutros.Checked = True
        End If

        'H. Sinistralidade nos últimos cinco anos (ocorrência e valor)
        txtSinistralidade.Text = dt.Tables(COTACAO_WEB_TB).Rows(0)("sinistralidade_cinco_anos")

        'I. Outros (em caso de dispor de informações adicionais)
        txtInformacoesAdicionais.Text = dt.Tables(COTACAO_WEB_TB).Rows(0)("inf_adicional")

        'J Dados para inspeção de risco
        txtContato.Text = dt.Tables(COTACAO_WEB_TB).Rows(0)("pessoa_contato_inspecao")
        For Each row As DataRow In dt.Tables(COTACAO_TEL_CONTATO_INSPECAO_TB).Rows
            Select Case (row.Item("tp_telefone"))
                Case "C"
                    txtTelefoneComercial.Text = row.Item("ddd_contato_inspecao") & row.Item("tel_contato_inspecao")
                Case "R"
                    txtTelefoneResidencial.Text = row.Item("ddd_contato_inspecao") & row.Item("tel_contato_inspecao")
                Case "E"
                    txtTelefoneCelular.Text = row.Item("ddd_contato_inspecao") & row.Item("tel_contato_inspecao")
                Case "O"
                    txtOutrosTelefones.Text = row.Item("ddd_contato_inspecao") & row.Item("tel_contato_inspecao")
            End Select
        Next
        txtEmail.Text = dt.Tables(COTACAO_WEB_TB).Rows(0)("email_contato_inspecao")

        '4. Clausula beneficiaria - gridClausulaBeneficiaria
        'Método proprio para carga da grid

        '5. Informações complementares (comentários)
        txtInformacoesComplementares.Text = dt.Tables(COTACAO_WEB_TB).Rows(0)("inf_complementares")

        '6. Observações
        txtObservacoes.Text = dt.Tables(COTACAO_WEB_TB).Rows(0)("observacao")

        '7. Condições de pagamento
        Select Case (dt.Tables(COTACAO_WEB_TB).Rows(0)("forma_pagto"))
            Case "V"
                rdbformaPagamento1.Checked = True
            Case "P"
                rdbformaPagamento2.Checked = True
        End Select
        txtNumParcelas.Text = dt.Tables(COTACAO_WEB_TB).Rows(0)("num_parcelas_pagto")

    End Sub

    Protected Sub btnProximo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProximo.Click

        controlaPaginacao(MultiView1.ActiveViewIndex + 1)

    End Sub

    Protected Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click

        controlaPaginacao(MultiView1.ActiveViewIndex - 1)

    End Sub

    Protected Sub controlaPaginacao(ByVal paginaDesejada As Integer)
        'inicio: gleison.pimentel - Nova Consultoria 05/11/2016 - 19089159 - Envio de documentos de cotação de endosso web – Visão Corretor

        If paginaDesejada = 1 Then
            If Me.hdiResposta.Value = "No" Then
                Exit Sub
            End If
        End If
        'fim: gleison.pimentel - Nova Consultoria 05/11/2016 - 19089159 - Envio de documentos de cotação de endosso web – Visão Corretor

        MultiView1.ActiveViewIndex = paginaDesejada

        Select Case MultiView1.ActiveViewIndex
            Case 0
                btnAnterior.Visible = False
                btnProximo.Visible = True
                btnConfirmar.Visible = False
            Case 1
                btnAnterior.Visible = True
                btnProximo.Visible = True
                btnConfirmar.Visible = False
            Case 2
                btnAnterior.Visible = True
                btnProximo.Visible = False
                btnConfirmar.Visible = True
        End Select

    End Sub

    Protected Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click

        Session.Remove("Lista")
        Session.Remove("Indice")

        Select Case Request.QueryString("acao")
            Case "inclusao"
                'Session.Clear()
                Response.Redirect("Pedido_cotacao.aspx?SLinkSeguro=" & Request.QueryString("SLinkSeguro"))
            Case Else
                Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "fechajanela();", True)
        End Select

    End Sub

    Protected Sub btnConfirmar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmar.Click

        Dim vigenciaEndosso As Date
        Dim fimVigenciaEndosso As Date
        Dim status_id As Integer
        Dim flagCarta As Boolean = False

        'Validação dos campos obrigatórios
        'Solicitante
        If txtSolicitante.Text = "" Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Favor informar o solicitante antes de salvar!');", True)
            controlaPaginacao(0)
            txtSolicitante.Focus()
            txtSolicitante.BackColor = Drawing.Color.Beige
            Exit Sub
        Else
            txtSolicitante.BackColor = Drawing.Color.White
        End If

        'Vigencia do endosso
        Try
            vigenciaEndosso = CDate(txtVigEndosso.Text)
            txtVigEndosso.BackColor = Drawing.Color.White
        Catch
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Favor informar uma data válida para o início da vigência do endosso!');", True)
            controlaPaginacao(0)
            txtVigEndosso.Focus()
            txtVigEndosso.BackColor = Drawing.Color.Beige
            Exit Sub
        End Try

        'Fim da Vigencia do endosso
        Try
            fimVigenciaEndosso = CDate(txtFimVigEndosso.Text)
            txtFimVigEndosso.BackColor = Drawing.Color.White
        Catch
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Favor informar uma data válida para fim da vigência do endosso!');", True)
            controlaPaginacao(0)
            txtFimVigEndosso.Focus()
            txtFimVigEndosso.BackColor = Drawing.Color.Beige
            Exit Sub
        End Try

        'Validando periodo de vigencia
        If vigenciaEndosso > fimVigenciaEndosso Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Data inicial não pode ser maior que data final da vigência!');", True)
            controlaPaginacao(0)
            txtVigEndosso.Focus()
            txtVigEndosso.BackColor = Drawing.Color.Beige
            txtFimVigEndosso.BackColor = Drawing.Color.Beige
            Exit Sub
        End If

        'Validar nome da corretora
        txtCorretor.BackColor = Drawing.Color.White
        If txtCorretor.Text.Length < 5 Then
            txtCorretor.BackColor = Drawing.Color.Beige
            txtCorretor.Focus()
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Favor informar um nome válido para a corretora!');", True)
            controlaPaginacao(0)
            Exit Sub
        End If

        'Validar CNPJ/CPF da corretora
        txtCnpjCorretora.BackColor = Drawing.Color.White
        Dim obj_valida_CNPJ_CPF_1 As New valida_CNPJ_CPF
        obj_valida_CNPJ_CPF_1.cnpj = txtCnpjCorretora.Text.Replace(".", "").Replace("-", "").Replace("/", "")
        If Not obj_valida_CNPJ_CPF_1.isCnpjValido Then
            txtCnpjCorretora.BackColor = Drawing.Color.Beige
            txtCnpjCorretora.Focus()
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Favor informar CNPJ válido para a corretora!');", True)
            controlaPaginacao(0)
            Exit Sub
        End If

        'Validar CNPJ/CPF
        Dim CnpjCpfErrado As Boolean = False
        For Each linha As GridViewRow In gridClausulaBeneficiaria.Rows
            Dim CnpjCpf As TextBox = DirectCast(linha.FindControl("txtGridCnpjCpf"), TextBox)
            CnpjCpf.BackColor = Drawing.Color.White
            If (CnpjCpf.Text <> "") Then
                Dim obj_valida_CNPJ_CPF As New valida_CNPJ_CPF
                If (DirectCast(linha.FindControl("radioListCpfCnpj"), RadioButtonList).SelectedIndex = 1) Then
                    'CPF:
                    obj_valida_CNPJ_CPF.cpf = CnpjCpf.Text.Replace(".", "").Replace("-", "")
                    If Not obj_valida_CNPJ_CPF.isCpfValido Then
                        CnpjCpf.BackColor = Drawing.Color.Beige
                        CnpjCpf.Focus()
                        CnpjCpfErrado = True
                    End If
                Else
                    'CNPJ:
                    obj_valida_CNPJ_CPF.cnpj = CnpjCpf.Text.Replace(".", "").Replace("-", "").Replace("/", "")
                    If Not obj_valida_CNPJ_CPF.isCnpjValido Then
                        CnpjCpf.BackColor = Drawing.Color.Beige
                        CnpjCpf.Focus()
                        CnpjCpfErrado = True
                    End If
                End If
            End If
        Next
        If CnpjCpfErrado Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Favor informar CPF ou CNPJ válido ou deixe o mesmo em branco antes de salvar!');", True)
            controlaPaginacao(2)
            Exit Sub
        End If

        'Forma de pagamento
        If (Not rdbformaPagamento1.Checked And Not rdbformaPagamento2.Checked) Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Favor selecionar uma das formas de pagamento e definir a quantidade de parcelas antes de salvar!');", True)
            controlaPaginacao(2)
            rdbformaPagamento1.Focus()
            rdbformaPagamento1.BackColor = Drawing.Color.Beige
            rdbformaPagamento2.BackColor = Drawing.Color.Beige
            Exit Sub
        Else
            rdbformaPagamento1.BackColor = Drawing.Color.Transparent
            rdbformaPagamento2.BackColor = Drawing.Color.Transparent
        End If

        'Numero de parcelas
        Try
            Dim parcelas As Integer = CInt(txtNumParcelas.Text)
            If ((parcelas <= 0) Or (parcelas > 7)) Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('O número de parcelas deve ser entre 1 e 7.');", True)
                controlaPaginacao(2)
                txtNumParcelas.Focus()
                txtNumParcelas.BackColor = Drawing.Color.Beige
                Exit Sub
            Else
                txtNumParcelas.BackColor = Drawing.Color.White
            End If
        Catch ex As Exception
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Favor preencher o número de parcelas para a forma de pagamento selecionada.');", True)
            controlaPaginacao(2)
            txtNumParcelas.Focus()
            txtNumParcelas.BackColor = Drawing.Color.Beige
            Exit Sub
        End Try

        'Ramo
        Dim ramo As Integer
        Try
            ramo = CInt(txtRamo.Text)
        Catch
            ramo = 0
        End Try

        Dim contadorBenecifiario As Integer = 0
        If (((ramo = 18) Or (ramo = 67) Or (ramo = 71) Or (ramo = 96)) And (CheckClausulaBeneficiariaInclusao.Checked)) Then
            For Each linha As GridViewRow In gridClausulaBeneficiaria.Rows
                If (DirectCast(linha.FindControl("txtBeneficiario"), TextBox).Text <> "") Then
                    contadorBenecifiario += 1
                End If
            Next
            If contadorBenecifiario = 0 Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Favor informar pelo menos um beneficiário antes de salvar a cotação!');", True)
                gridClausulaBeneficiaria.Focus()
                controlaPaginacao(2)
                Exit Sub
            End If
        End If

        If checkCoberturaEdificioOutros.Checked Then
            If (txtCoberturaEdificio.Text.Length = 0) Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Favor informar o tipo de cobertura do edifício, ou desmarcar a opção Outros.');", True)
                controlaPaginacao(1)
                txtCoberturaEdificio.BackColor = Drawing.Color.Beige
                txtCoberturaEdificio.Focus()
                Exit Sub
            Else
                txtCoberturaEdificio.BackColor = Drawing.Color.White
            End If
        End If

        For Each linha As GridViewRow In gridReducao.Rows
            If (DirectCast(linha.FindControl("txtLocal"), TextBox).Text <> "") Then
                If (DirectCast(linha.FindControl("txtISDe"), TextBox).Text.Length > 17) Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Valor de ISDe declarado muito alto\ncorrija antes de salvar a cotação.');", True)
                    controlaPaginacao(0)
                    gridReducao.Focus()
                    Exit Sub
                End If
                If (DirectCast(linha.FindControl("txtISPara"), TextBox).Text.Length > 17) Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Valor de ISPara declarado muito alto\ncorrija antes de salvar a cotação.');", True)
                    controlaPaginacao(0)
                    gridReducao.Focus()
                    Exit Sub
                End If
                If (DirectCast(linha.FindControl("txtPremio"), TextBox).Text.Length > 17) Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Valor de prêmio declarado muito alto\ncorrija antes de salvar a cotação.');", True)
                    controlaPaginacao(0)
                    gridReducao.Focus()
                    Exit Sub
                End If
            End If
        Next
        'FIM validação dos campos

        'Definição do fluxo'
        'Ramos 18, 67, 71 ou 96, se houver algum item 1.4 marcado e nenhum item 1.1 ou 1.2 ou 1.3 ou 1.5 marcado e o ramo diferente de 96 segue para emissão caso contrário para a equipe tecnica
        'Quando item 1.4 for inclusão gera a carta beneficiária
        If (((ramo = 18) Or (ramo = 67) Or (ramo = 71) Or (ramo = 96)) And _
            ((CheckClausulaBeneficiariaInclusao.Checked) Or (CheckClausulaBeneficiariaExclusao.Checked) Or (CheckClausulaBeneficiariaRetificacao.Checked) Or (CheckClausulaBeneficiariaAlteracao.Checked))) Then
            If (CheckClausulaBeneficiariaInclusao.Checked) Then
                'Emitir carta beneficiária
                flagCarta = True
            End If
            If ((ramo = 96) Or _
                (radioButtonElevacao.Checked) Or (radioButtonReducao.Checked) Or _
                (checkCoberturaInclusao.Checked) Or (checkCoberturaExclusao.Checked) Or (checkCoberturaAlteracao.Checked) Or _
                (CheckLocalRiscoInclusao.Checked) Or (CheckLocalRiscoExclusao.Checked) Or (CheckLocalRiscoRetificacao.Checked) Or (CheckLocalRiscoAlteracao.Checked) Or _
                (txtOutrosTiposAlteracao.Text <> String.Empty)) Then
                'Seguir para "Aguardando análise"
                status_id = AGUARDANDO_ANALISE
            Else
                'Seguir para estatus "Emissão"
                status_id = EMITIR
            End If
        Else
            'Seguir para status "Aguardando análise"
            status_id = AGUARDANDO_ANALISE
        End If

        'Início - PEGANDO DADOS PARA GRAVAR
        Dim obj_cotacao_web As New cotacao_web

        obj_cotacao_web.cliente_id = CInt(hdnClienteID.Value)

        If (flagCarta) Then
            obj_cotacao_web.cliente_nome = hdnClienteNome.Value
            obj_cotacao_web.cliente_cpfCnpj = txtCpfCnpj.Text
        End If

        obj_cotacao_web.apolice_id = CInt(txtApolice.Text)
        obj_cotacao_web.ramo_id = CInt(txtRamo.Text)
        obj_cotacao_web.dt_vigencia_endosso = vigenciaEndosso
        obj_cotacao_web.dt_fim_vigencia_endosso = fimVigenciaEndosso
        obj_cotacao_web.solicitante = txtSolicitante.Text
        obj_cotacao_web.corretor_id = CInt(hdnCorretorID.Value)
        obj_cotacao_web.proposta_bb_id = CInt(txtPropostaBB.Text)
        obj_cotacao_web.proposta_id = CInt(hdnPropostaID.Value)

        'Status
        obj_cotacao_web.status_cotacao_id = status_id

        '1. Tipo de alteração
        '1.1 Importância segurada
        If (radioButtonElevacao.Checked) Then
            obj_cotacao_web.alt_importancia_segurada = "E"
        End If
        If (radioButtonReducao.Checked) Then
            obj_cotacao_web.alt_importancia_segurada = "R"
        End If

        '1.2 Coberturas
        obj_cotacao_web.alt_coberturas_inclusao = checkCoberturaInclusao.Checked
        obj_cotacao_web.alt_coberturas_exclusao = checkCoberturaExclusao.Checked
        obj_cotacao_web.alt_coberturas_alteracao = checkCoberturaAlteracao.Checked

        '1.3 Local de risco
        obj_cotacao_web.alt_local_risco_inclusao = CheckLocalRiscoInclusao.Checked
        obj_cotacao_web.alt_local_risco_exclusao = CheckLocalRiscoExclusao.Checked
        obj_cotacao_web.alt_local_risco_retificacao = CheckLocalRiscoRetificacao.Checked
        obj_cotacao_web.alt_local_risco_alteracao = CheckLocalRiscoAlteracao.Checked

        '1.4 Cláusula beneficiária
        obj_cotacao_web.alt_clausula_beneficiario_inclusao = CheckClausulaBeneficiariaInclusao.Checked
        obj_cotacao_web.alt_clausula_beneficiario_exclusao = CheckClausulaBeneficiariaExclusao.Checked
        obj_cotacao_web.alt_clausula_beneficiario_retificacao = CheckClausulaBeneficiariaRetificacao.Checked
        obj_cotacao_web.alt_clausula_beneficiario_alteracao = CheckClausulaBeneficiariaAlteracao.Checked

        '1.5 Alteração outros
        obj_cotacao_web.tp_alt_outros = txtOutrosTiposAlteracao.Text

        '2. Redução (GridReducao)
        Dim listaReducao As New List(Of reducao)
        For Each linha As GridViewRow In gridReducao.Rows
            If (DirectCast(linha.FindControl("txtLocal"), TextBox).Text <> "") Then
                Dim itemReducao As New reducao
                itemReducao.local_reducao = DirectCast(linha.FindControl("txtLocal"), TextBox).Text
                itemReducao.cobertura = DirectCast(linha.FindControl("txtCobertura"), TextBox).Text
                itemReducao.is_de = DirectCast(linha.FindControl("txtISDe"), TextBox).Text
                itemReducao.is_para = DirectCast(linha.FindControl("txtISPara"), TextBox).Text
                itemReducao.premio = DirectCast(linha.FindControl("txtPremio"), TextBox).Text
                listaReducao.Add(itemReducao)
            End If
        Next
        obj_cotacao_web.listaReducao = listaReducao

        '3. Dados para inclusão de locais
        obj_cotacao_web.dados_inc_locais = txtDadosInclusaoLocais.Text

        'B. Sistemas para proteção contra incêndio” 
        If CheckExtintores.Checked Then
            Dim sistema As New sistema_protecao_incendio
            sistema.tp_protecao_incendio = "E"
            obj_cotacao_web.lista_sistema_protecao_incendio.Add(sistema)
        End If
        If CheckHidrantes.Checked Then
            Dim sistema As New sistema_protecao_incendio
            sistema.tp_protecao_incendio = "H"
            obj_cotacao_web.lista_sistema_protecao_incendio.Add(sistema)
        End If
        If CheckSprinklers.Checked Then
            Dim sistema As New sistema_protecao_incendio
            sistema.tp_protecao_incendio = "S"
            obj_cotacao_web.lista_sistema_protecao_incendio.Add(sistema)
        End If

        'C. Sistemas de proteção contra roubo
        If CheckGrades.Checked Then
            obj_cotacao_web.lista_sistema_protecao_roubo.Add(New sistema_protecao_roubo("G"))
        End If
        If CheckAlarmos.Checked Then
            obj_cotacao_web.lista_sistema_protecao_roubo.Add(New sistema_protecao_roubo("A"))
        End If
        If CheckVigilancia.Checked Then
            obj_cotacao_web.lista_sistema_protecao_roubo.Add(New sistema_protecao_roubo("V"))
        End If

        'D. Distância do Corpo de bombeiros
        If radioDistanciaBombeiros1.Checked Then
            obj_cotacao_web.distancia_bombeiro_10_km = "I"
        End If
        If radioDistanciaBombeiros2.Checked Then
            obj_cotacao_web.distancia_bombeiro_10_km = "S"
        End If

        'E. Tipo de contrução do Edifício
        If radioTipoConstrucaoEdificio1.Checked Then
            obj_cotacao_web.construcao_edificio = "S"
        End If
        If radioTipoConstrucaoEdificio2.Checked Then
            obj_cotacao_web.construcao_edificio = "O"
        End If
        If radioTipoConstrucaoEdificio3.Checked Then
            obj_cotacao_web.construcao_edificio = "M"
        End If
        If radioTipoConstrucaoEdificio4.Checked Then
            obj_cotacao_web.construcao_edificio = "I"
        End If

        'F. Informações sobre a fiação elétrica
        If radioFiacao1.Checked Then
            obj_cotacao_web.fiacao_eletrica = "A"
        End If
        If radioFiacao2.Checked Then
            obj_cotacao_web.fiacao_eletrica = "E"
        End If

        'G. Cobertura do edifício
        obj_cotacao_web.cobertura_edificio_laje = checkCoberturaEdificioLaje.Checked
        obj_cotacao_web.cobertura_edificio_telha = checkCoberturaEdificioTelha.Checked
        If checkCoberturaEdificioOutros.Checked Then
            obj_cotacao_web.cobertura_edificio_outros = txtCoberturaEdificio.Text
        End If

        'H. Sinistralidade nos últimos cinco anos (ocorrência e valor)
        obj_cotacao_web.sinistralidade_cinco_anos = txtSinistralidade.Text

        'I. Outros (em caso de dispor de informações adicionais)
        obj_cotacao_web.inf_adicional = txtInformacoesAdicionais.Text

        'J Dados para inspeção de risco
        obj_cotacao_web.pessoa_contato_inspecao = txtContato.Text
        Dim TEL As String
        If (txtTelefoneComercial.Text.Length >= 13) Then
            TEL = txtTelefoneComercial.Text.Replace("(", "").Replace(")", "").Replace("-", "")
            obj_cotacao_web.lista_telefone_contato_inspecao.Add(New cotacao_tel_contato_inspecao("C", TEL.Substring(0, 2), TEL.Substring(2, TEL.Length - 2)))
        End If
        'txtTelefoneResidencial.Text
        If (txtTelefoneResidencial.Text.Length >= 13) Then
            TEL = txtTelefoneResidencial.Text.Replace("(", "").Replace(")", "").Replace("-", "")
            obj_cotacao_web.lista_telefone_contato_inspecao.Add(New cotacao_tel_contato_inspecao("R", TEL.Substring(0, 2), TEL.Substring(2, TEL.Length - 2)))
        End If
        'txtTelefoneCelular.Text
        If (txtTelefoneCelular.Text.Length >= 13) Then
            TEL = txtTelefoneCelular.Text.Replace("(", "").Replace(")", "").Replace("-", "")
            obj_cotacao_web.lista_telefone_contato_inspecao.Add(New cotacao_tel_contato_inspecao("E", TEL.Substring(0, 2), TEL.Substring(2, TEL.Length - 2)))
        End If
        'txtOutrosTelefones.Text
        If (txtOutrosTelefones.Text.Length >= 13) Then
            TEL = txtOutrosTelefones.Text.Replace("(", "").Replace(")", "").Replace("-", "")
            obj_cotacao_web.lista_telefone_contato_inspecao.Add(New cotacao_tel_contato_inspecao("O", TEL.Substring(0, 2), TEL.Substring(2, TEL.Length - 2)))
        End If
        obj_cotacao_web.email_contato_inspecao = txtEmail.Text

        '4. Clausula beneficiaria - gridClausulaBeneficiaria
        Dim listaClausulaBeneficiaria As New List(Of clausula_Beneficiaria)
        For Each linha As GridViewRow In gridClausulaBeneficiaria.Rows
            If (DirectCast(linha.FindControl("txtBeneficiario"), TextBox).Text <> "") Then
                Dim itemClausulaBeneficiaria As New clausula_Beneficiaria
                itemClausulaBeneficiaria.beneficiario = DirectCast(linha.FindControl("txtBeneficiario"), TextBox).Text
                itemClausulaBeneficiaria.cnpj_cpf = DirectCast(linha.FindControl("txtGridCnpjCpf"), TextBox).Text.Replace(".", "").Replace("-", "").Replace("/", "")
                listaClausulaBeneficiaria.Add(itemClausulaBeneficiaria)
            End If
        Next
        obj_cotacao_web.listaClausulaBeneficiaria = listaClausulaBeneficiaria

        '5. Informações complementares (comentários)
        obj_cotacao_web.inf_complementares = txtInformacoesComplementares.Text

        '6. Observações
        obj_cotacao_web.observacao = txtObservacoes.Text

        '7. Condições de pagamento
        If rdbformaPagamento1.Checked Then
            obj_cotacao_web.forma_pagto = "V"
        Else
            obj_cotacao_web.forma_pagto = "P"
        End If
        obj_cotacao_web.num_parcelas_pagto = CInt(txtNumParcelas.Text)

        obj_cotacao_web.nome_corretora = txtCorretor.Text
        obj_cotacao_web.cnpj_corretora = txtCnpjCorretora.Text.Replace(".", "").Replace("-", "").Replace("/", "")
        'FIM - PEGANDO DADOS PARA GRAVAR

        'Gravação cotação:
        Try
            If (Request.QueryString("acao") = "recalculo") Then
                obj_cotacao_web.protocolo_id = Session("protocolo_id")
                obj_cotacao_web.inserirCotacao(flagCarta, Session("usuario"), linkseguro, Request.QueryString("cotacao_web_controle_id"))
            Else
                obj_cotacao_web.inserirCotacao(flagCarta, Session("usuario"), linkseguro)
            End If
            Session.Add("numero_protocolo_completo", obj_cotacao_web.protocolo_id)
            Session.Add("acao", Request.QueryString("acao"))
        Catch ex As Exception
            Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Erro ao gravar cotação. " & Replace(ex.Message, "'", "") & "');", True)
            Exit Sub
        End Try
        'FIM gravação cotação

        'Impressão da carta benefeciaria
        If (flagCarta) Then
            Session("ImprimeCarta") = "S"
            Session("cotacao_web_id_carta") = obj_cotacao_web.cotacao_web_id
        Else
            Session("ImprimeCarta") = "N"
            Session("cotacao_web_id_carta") = 0
        End If

        'inicio: gleison.pimentel - Nova Consultoria 05/11/2016 - 19089159 - Envio de documentos de cotação de endosso web – Visão Corretor
        InserirAnexoDocumentum(obj_cotacao_web)
        'fim: gleison.pimentel - Nova Consultoria 05/11/2016 - 19089159 - Envio de documentos de cotação de endosso web – Visão Corretor

        'Exibe protocolo gerado:
        Response.Redirect("protocolo.aspx?SLinkSeguro=" & Request.QueryString("SLinkSeguro"))

    End Sub

    Protected Sub habilitaCampoOutros(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles checkCoberturaEdificioOutros.CheckedChanged
        If checkCoberturaEdificioOutros.Checked Then
            txtCoberturaEdificio.Enabled = True
        Else
            txtCoberturaEdificio.Enabled = False
        End If
    End Sub

    Protected Sub adicionaLinhaNaGrid(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnAdicionaReducao.Click

        Dim listaReducao As New List(Of reducao)

        For Each linha As GridViewRow In gridReducao.Rows
            Dim itemReducao As New reducao
            itemReducao.local_reducao = DirectCast(linha.FindControl("txtLocal"), TextBox).Text
            itemReducao.cobertura = DirectCast(linha.FindControl("txtCobertura"), TextBox).Text
            itemReducao.is_de = DirectCast(linha.FindControl("txtISDe"), TextBox).Text
            itemReducao.is_para = DirectCast(linha.FindControl("txtISPara"), TextBox).Text
            itemReducao.premio = DirectCast(linha.FindControl("txtPremio"), TextBox).Text
            listaReducao.Add(itemReducao)
        Next

        Dim itemReducaoFooter As New reducao
        listaReducao.Add(itemReducaoFooter)
        gridReducao.DataSource = listaReducao
        gridReducao.DataBind()

    End Sub

    Protected Sub gridClausulaBeneficiario_RowDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim radioList As RadioButtonList = DirectCast(e.Row.FindControl("radioListCpfCnpj"), RadioButtonList)
            If radioList.SelectedIndex = -1 Then
                If (DirectCast(e.Row.FindControl("txtGridCnpjCpf"), TextBox).Text.Length) = 14 Then
                    radioList.SelectedIndex = 1
                Else
                    radioList.SelectedIndex = 0
                End If
            End If
            Dim txt As TextBox = DirectCast(e.Row.FindControl("txtGridCnpjCpf"), TextBox)
            txt.MaxLength = radioList.SelectedValue
        End If
    End Sub

    Protected Sub grdClausulaBeneficiaria_RadioSelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim radioList As RadioButtonList = DirectCast(sender, RadioButtonList)
        Dim linha As GridViewRow = DirectCast(radioList.NamingContainer, GridViewRow)
        Dim grid As GridView = DirectCast(linha.NamingContainer, GridView)

        Dim txt As TextBox = DirectCast(grid.Rows.Item(linha.DataItemIndex()).FindControl("txtGridCnpjCpf"), TextBox)
        txt.MaxLength = radioList.SelectedValue
        txt.Text = ""

    End Sub

    Protected Sub adicionaLinhaNaGridClausulaBeneficiaria(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnAdicionarLinha.Click

        Dim listaClausulaBeneficiaria As New List(Of clausula_Beneficiaria)

        For Each linha As GridViewRow In gridClausulaBeneficiaria.Rows
            Dim itemClausulaBeneficiaria As New clausula_Beneficiaria
            itemClausulaBeneficiaria.beneficiario = DirectCast(linha.FindControl("txtBeneficiario"), TextBox).Text
            itemClausulaBeneficiaria.cnpj_cpf = DirectCast(linha.FindControl("txtGridCnpjCpf"), TextBox).Text
            itemClausulaBeneficiaria.IndiceRadioButton = DirectCast(linha.FindControl("radioListCpfCnpj"), RadioButtonList).SelectedIndex
            listaClausulaBeneficiaria.Add(itemClausulaBeneficiaria)
        Next

        Dim itemClausulaBeneficiariaFooter As New clausula_Beneficiaria
        listaClausulaBeneficiaria.Add(itemClausulaBeneficiariaFooter)
        gridClausulaBeneficiaria.DataSource = listaClausulaBeneficiaria
        gridClausulaBeneficiaria.DataBind()

    End Sub

    'inicio: gleison.pimentel - Nova Consultoria 05/11/2016 - 19089159 - Envio de documentos de cotação de endosso web – Visão Corretor
    'Public Sub gridDocumentos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gridDocumentos.RowDataBound

    '    If e.Row.RowState = (DataControlRowState.Edit Or DataControlRowState.Alternate) Or e.Row.RowState = DataControlRowState.Edit Then
    '        Return
    '    End If

    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        'Referencia ao linkbutton delete
    '        Dim deleteButton As ImageButton = DirectCast(e.Row.Cells(2).Controls(0), ImageButton)
    '        deleteButton.OnClientClick = "if (!window.confirm('Confirma a exclusão deste documento ?')) return false;"

    '        Dim rowView As DataRowView = CType(e.Row.DataItem, DataRowView)
    '        Dim v_oDocumento As HyperLink = CType(e.Row.FindControl("hplVisualiza_Documento"), HyperLink)
    '        v_oDocumento.Text = rowView("Nome").ToString
    '        v_oDocumento.Target = "_blank"
    '        v_oDocumento.NavigateUrl = sCaminhoVisualiza & rowView("Nome").ToString
    '    End If
    'End Sub

    'Public Sub gridDocumentos_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gridDocumentos.RowDeleting
    '    Dim id As Integer = Convert.ToInt32(gridDocumentos.DataKeys(e.RowIndex).Value.ToString())
    '    DeletarDocumentoGrid(id)
    'End Sub

    Protected Function getCaminho(ByVal sTipo_leitura As String, ByVal ambiente_id As Integer) As String

        Dim _funcoes As New Data.Funcoes_gerais(linkseguro)

        Return _funcoes.Busca_Caminho(sTipo_leitura, ambiente_id)

    End Function
    'fim: gleison.pimentel - Nova Consultoria 05/11/2016 - 19089159 - Envio de documentos de cotação de endosso web – Visão Corretor
End Class