﻿Public Partial Class Cotacao_Web_concluir
    Inherits System.Web.UI.Page
    Dim linkseguro As New Alianca.Seguranca.Web.LinkSeguro

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        linkseguro.LerUsuario( _
                    Request.Url.Segments(Request.Url.Segments.Length - 1), _
                    Request.UserHostAddress, _
                    Request.QueryString("SLinkSeguro"))

        Session("usuario") = linkseguro.Login_WEB

        If Alianca.Seguranca.BancoDados.cCon.configurado Then
            Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, linkseguro.Ambiente())
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção)
        Else '
            Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, linkseguro.Ambiente())
            'Alianca.Seguranca.BancoDados.cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Desenvolvimento)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Qualidade)
            'Alianca.Seguranca.BancoDados.cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção)
        End If

    End Sub
    Private Sub btnSair_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "fechajanela();", True)
    End Sub
    Private Sub btnConfirmar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnconfirmar.Click
        Dim ctrlDAO As Data.cotacao_web_controleDAO = New Data.cotacao_web_controleDAO(linkseguro)

        ctrlDAO.ConcluirCotacao(Request.QueryString("cotacao_web_controle_id"), Session("usuario"), txtNumero_Endosso.Text, linkseguro)
        ctrlDAO = Nothing

        Page.ClientScript.RegisterStartupScript(Me.GetType, "OK", "window.alert('Cotação finalizada com sucesso!');", True)
        Page.ClientScript.RegisterStartupScript(Me.GetType, "Fechar", "fechajanela();window.opener.AtualizarGrid();", True)
    End Sub
End Class